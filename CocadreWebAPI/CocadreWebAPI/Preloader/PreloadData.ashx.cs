﻿using CassandraService.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Preloader
{
    public class PreloadData : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            ClientService client = new ClientService();
            client.AddCountryToTable();
            client.AddCountryIpToTable();
            client.WriteExpAToTable();
            client.AddTimezoneToTable();

            context.Response.Write("OK");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}