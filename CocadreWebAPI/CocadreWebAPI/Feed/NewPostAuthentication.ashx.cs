﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.IO;
using System.Web;

namespace CocadreWebAPI.Feed
{
    public class NewPostAuthentication : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            FeedAuthenticationResponse response = new FeedAuthenticationResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = cs.AuthenticateUserForPostingFeed(companyId, userId);

            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<FeedAuthenticationResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }
    }
}