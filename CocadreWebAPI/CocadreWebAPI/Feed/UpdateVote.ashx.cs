﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreWebAPI.Notification.Classes;
using log4net;
using System;
using System.IO;
using System.Web;

namespace CocadreWebAPI.Feed
{
    public class UpdateVote : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;

                bool isUpVote = request.IsUpVote;

                bool isVoteFeed = request.IsVoteFeed;
                bool isVoteComment = request.IsVoteComment;
                bool isVoteReply = request.IsVoteReply;
                
                if(isVoteFeed)
                {
                    string feedId = request.FeedId;
                    response = cs.UpdateFeedPoint(userId, feedId, companyId, isUpVote);
                }
                else if(isVoteComment)
                {
                    string feedId = request.FeedId;
                    string commentId = request.CommentId;
                    response = cs.UpdateCommentPoint(userId, feedId, commentId, companyId, isUpVote);
                }
                else if (isVoteReply)
                {
                    string feedId = request.FeedId;
                    string commentId = request.CommentId;
                    string replyId = request.ReplyId;
                    response = cs.UpdateReplyPoint(userId, feedId, commentId, replyId, companyId, isUpVote);
                }

                if (response.Success)
                {
                    PushNotification notification = new PushNotification();
                    notification.Push(response.TargetedUserId, response.NotificationText, response.NumberOfNotificationForTargetedUser, response.NotificationType, response.NotificationSubType, null, response.FeedId, response.CommentId, response.ReplyId);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<PointUpdateResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public bool IsVoteFeed { get; set; }
            public bool IsVoteComment { get; set; }
            public bool IsVoteReply { get; set; }
            public bool IsUpVote { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
        }
    }
}