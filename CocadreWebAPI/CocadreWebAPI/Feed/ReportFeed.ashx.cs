﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Feed
{
    public class ReportFeed : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            DashboardReportResponse response = new DashboardReportResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;
                string reason = request.Reason;

                bool isReportPost = request.IsReportPost;
                bool isReportComment = request.IsReportComment;
                bool isReportReply = request.IsReportReply;

                if(isReportPost)
                {
                    response = cs.ReportFeed(userId, reason, companyId, feedId, null, null, (int)Dashboard.ReportType.Feed);
                }
                else if(isReportComment)
                {
                    response = cs.ReportFeed(userId, reason, companyId, feedId, commentId, null, (int)Dashboard.ReportType.Comment);
                }
                else if(isReportReply)
                {
                    response = cs.ReportFeed(userId, reason, companyId, feedId, commentId, replyId, (int)Dashboard.ReportType.Reply);
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<DashboardReportResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
            public string Reason { get; set; }

            public bool IsReportPost { get; set; }
            public bool IsReportComment { get; set; }
            public bool IsReportReply { get; set; }
        }
    }
}