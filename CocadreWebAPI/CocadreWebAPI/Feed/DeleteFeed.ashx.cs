﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.IO;
using System.Reflection;
using System.Web;

namespace CocadreWebAPI.Feed
{
    public class DeleteFeed : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                Log.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                response = cs.DeleteFeed(request.FeedId, request.CompanyId, request.UserId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<FeedDeleteResponse>(response);

            Log.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
        }
    }
}