﻿using CassandraService.ServiceInterface;
using log4net;
using System;
using System.IO;
using System.Web;
using CassandraService.ServiceResponses;
using System.Web.Configuration;
using CocadreWebAPI.App_GlobalResources;
using CocadreWebAPI.Notification.Classes;
using System.Collections.Generic;
using PubNubMessaging.Core;

namespace CocadreWebAPI.Feed
{
    public class PostComment : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            CommentCreateResponse response = new CommentCreateResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string content = request.Content;

                response = cs.CreateCommentText(userId, companyId, feedId, content);

                if(response.Success)
                {
                    PushNotification notification = new PushNotification();
                    notification.Push(response.TargetedUserId, response.NotificationText, response.NumberOfNotificationForTargetedUser, response.NotificationType, response.NotificationSubType, null, response.FeedId, response.CommentId, null);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<CommentCreateResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string Content { get; set; }
       }
    }
}