﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CocadreWebAPI.Feed
{
    public class GetPersonnelFeed : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string requesterUserId = request.RequesterUserId;
                string ownerUserId = request.OwnerUserId;
                string companyId = request.CompanyId;
                string searchContent = request.SearchContent;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = cs.SelectPersonnelFeedPost(requesterUserId, ownerUserId, companyId, searchContent, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<FeedSelectResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string RequesterUserId { get; set; }
            public string OwnerUserId { get; set; }
            public string CompanyId { get; set; }
            public string SearchContent { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }
    }
}