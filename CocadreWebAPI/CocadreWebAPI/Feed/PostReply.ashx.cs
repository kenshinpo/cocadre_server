﻿using CassandraService.ServiceInterface;
using log4net;
using System;
using System.IO;
using System.Web;
using CassandraService.ServiceResponses;
using CocadreWebAPI.Notification.Classes;

namespace CocadreWebAPI.Feed
{
    public class PostReply : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            ReplyCreateResponse response = new ReplyCreateResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string content = request.Content;

                response = cs.CreateReplyText(userId, companyId, feedId, commentId, content);

                if (response.Success)
                {
                    PushNotification notification = new PushNotification();
                    notification.Push(response.TargetedUserId, response.NotificationText, response.NumberOfNotificationForTargetedUser, response.NotificationType, response.NotificationSubType, null, response.FeedId, response.CommentId, response.ReplyId);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<ReplyCreateResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string Content { get; set; }
       }
    }
}