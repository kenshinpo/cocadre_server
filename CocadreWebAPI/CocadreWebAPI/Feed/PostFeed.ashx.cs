﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreWebAPI.App_GlobalResources;
using CocadreWebAPI.Notification.Classes;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace CocadreWebAPI.Feed
{
    public class PostFeed : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string feedId = request.FeedId;
                int feedType = request.FeedType;
                string userId = request.UserId;
                string companyId = request.CompanyId;
                List<string> targetedDepartmentIds = request.TargetedDepartmentIds;
                List<string> targetedUserIds = request.TargetedUserIds;
                bool isForAdmin = request.IsForAdmin;

                if (targetedDepartmentIds != null)
                {
                    logger.Debug("Count for targetedDepartmentIds: " + targetedDepartmentIds.Count);
                }

                if (targetedUserIds != null)
                {
                    logger.Debug("Count for targetedUserIds: " + targetedUserIds.Count);
                }

                if(feedType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    string content = request.Content;
                    response = cs.CreateFeedTextPost(feedId, userId, companyId, content, targetedDepartmentIds, targetedUserIds, isForAdmin);
                }
                else if(feedType == Int16.Parse(FeedTypeCode.ImagePost))
                {
                    string caption = request.Caption;
                    List<string> imageUrls = request.ImageUrls;

                    response = cs.CreateFeedImagePost(feedId, userId, companyId, caption, imageUrls, targetedDepartmentIds, targetedUserIds, isForAdmin);

                }
                else if(feedType == Int16.Parse(FeedTypeCode.VideoPost))
                {
                    string caption = request.Caption;
                    string videoUrl = request.VideoUrl;
                    string videoThumbnailUrl = request.VideoThumbnailUrl;
                    response = cs.CreateFeedVideoPost(feedId, userId, companyId, caption, videoUrl, videoThumbnailUrl, targetedDepartmentIds, targetedUserIds, isForAdmin);
                }
                else if(feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                {
                    string caption = request.Caption;
                    string url = request.Url;
                    string urlTitle = request.UrlTitle;
                    string urlDescription = request.UrlDescription;
                    string urlSiteName = request.UrlSiteName;
                    string urlImageUrl = request.UrlImageUrl;
                    response = cs.CreateFeedSharedUrlPost(feedId, userId, companyId, caption, url, urlTitle, urlDescription, urlSiteName, urlImageUrl, targetedDepartmentIds, targetedUserIds, isForAdmin);
                }

                if(response.Success)
                {
                    PushNotification notification = new PushNotification();
                    foreach(Dictionary<string, object> targetedUser in response.TargetedUsers)
                    {
                        string targetedUserId = targetedUser["userId"].ToString();
                        string notificationText = targetedUser["notificationText"].ToString();
                        int numberOfNotification = Int16.Parse(targetedUser["notificationNumber"].ToString());
                        notification.Push(targetedUserId, notificationText, numberOfNotification, (int)CassandraService.Entity.Notification.NotificationType.Feed, (int)CassandraService.Entity.Notification.NotificationFeedSubType.NewPost, null, response.FeedId, null, null);
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<FeedCreateResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string FeedId { get; set; }
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public List<string> TargetedDepartmentIds { get; set; }
            public List<string> TargetedUserIds { get; set; }
            public bool IsForAdmin { get; set; }
            public int FeedType { get; set; }

            public string Content { get; set; }

            public string Caption { get; set; }

            public List<string> ImageUrls { get; set; }

            public string VideoUrl { get; set; }
            public string VideoThumbnailUrl { get; set; }

            public string Url { get; set; }
            public string UrlTitle { get; set; }
            public string UrlDescription { get; set; }
            public string UrlSiteName { get; set; }
            public string UrlImageUrl { get; set; }

       }
    }
}