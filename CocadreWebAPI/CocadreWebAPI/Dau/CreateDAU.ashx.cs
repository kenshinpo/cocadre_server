﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Dau
{
    public class CreateDAU : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            AnalyticsUpdateDailyActiveUserResponse response = new AnalyticsUpdateDailyActiveUserResponse();
            response.Success = false;
            try
            {
                Log.Debug("Creating DAU for yesterday");

                ClientService client = new ClientService();
                response = client.UpdateDailyActiveUser(DateTime.UtcNow.Date.AddDays(1));

                //for (int index = 30; index >= 0; index--)
                //{
                //    response = client.UpdateDailyActiveUser(DateTime.UtcNow.Date.AddDays(-index));
                //} 
            }
            catch (Exception ex)
            { 
                Log.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<AnalyticsUpdateDailyActiveUserResponse>(response);
            Log.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}