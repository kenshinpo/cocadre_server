﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Dau
{
    public class SelectDAU : IHttpHandler
    {

        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            AnalyticsSelectDetailDauResponse response = new AnalyticsSelectDetailDauResponse();
            response.Success = false;
            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                Log.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                Log.Debug("Select dau report");

                int timeActivityFrameBase = request.TimeActivityFrameBase;
                response = client.SelectDau("Udb0e704ee7244d21aea7930c27709fee", "C8b28091502514318bdcf48bec7c69129", (int)CassandraService.Entity.Analytic.DauType.LastThirtyDays, timeActivityFrameBase);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<AnalyticsSelectDetailDauResponse>(response);
            Log.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public int TimeActivityFrameBase { get; set; }
        }
    }
}