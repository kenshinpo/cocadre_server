﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Dau
{
    public class SelectBasicReport : IHttpHandler
    {

        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            AnalyticsSelectBasicResponse response = new AnalyticsSelectBasicResponse();
            response.Success = false;
            try
            {
                Log.Debug("Select basic report");

                ClientService client = new ClientService();
                response = client.SelectBasicReport("Udb0e704ee7244d21aea7930c27709fee", "C8b28091502514318bdcf48bec7c69129");
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<AnalyticsSelectBasicResponse>(response);
            Log.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}