﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Leaderboard
{
    public class GetLeaderboardByDepartment : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Log.Info("Processing get leaderboard by department");

            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                Log.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;

                AnalyticsSelectLeaderboardByDepartmentResponse response = client.SelectLeaderboardByDepartment(userId, companyId);

                string returnedJsonString = JsonHelper.JsonSerializer<AnalyticsSelectLeaderboardByDepartmentResponse>(response);

                context.Response.Write(returnedJsonString);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            timer.Stop();
            Log.Info("Time taken to process: " + timer.Elapsed.Milliseconds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }
    }
}