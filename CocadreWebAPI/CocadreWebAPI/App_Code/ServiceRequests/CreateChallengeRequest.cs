﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class CreateChallengeRequest
    {
        public CreateChallengeRequest(string companyId, string topicId, string topicCategoryId, string initiatorUserId, string opponentUserId)
        {
            this.companyId = companyId;
            this.topicId = topicId;
            this.topicCategoryId = topicCategoryId;
            this.initiatorUserId = initiatorUserId;
            this.opponentUserId = opponentUserId;
        }

        public CreateChallengeRequest(string channelName, string companyId, string topicId, string topicCategoryId, string initiatorUserId, string opponentUserId)
        {
            this.channelName = channelName;
            this.companyId = companyId;
            this.topicId = topicId;
            this.topicCategoryId = topicCategoryId;
            this.initiatorUserId = initiatorUserId;
            this.opponentUserId = opponentUserId;
        }

        public string channelName { get; set; }
        public string companyId { get; set; }
        public string topicId { get; set; }
        public string topicCategoryId { get; set; }
        public string initiatorUserId { get; set; }
        public string opponentUserId { get; set; }
    }
}