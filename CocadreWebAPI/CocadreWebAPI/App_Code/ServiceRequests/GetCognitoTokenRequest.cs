﻿using System;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    [Serializable]
    public class GetCognitoTokenRequest
    {
        public String CompanyId { get; set; }
        public String UserId { get; set; }
    }
}