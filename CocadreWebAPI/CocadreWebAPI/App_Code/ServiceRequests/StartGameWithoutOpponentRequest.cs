﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class StartGameWithoutOpponentRequest
    {
        public StartGameWithoutOpponentRequest (string challengeId, string companyId, string userId)
        {
            this.challengeId = challengeId;
            this.companyId = companyId;
            this.userId = userId;
        }

        public string challengeId { get; set; }
        public string companyId { get; set; }
        public string userId { get; set; }
    }
}