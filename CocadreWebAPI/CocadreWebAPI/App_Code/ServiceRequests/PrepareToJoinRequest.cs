﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class PrepareToJoinRequest
    {
        public string eventName { get; set; }
        public string challengeId { get; set; }
        public string companyId { get; set; }
        public string requesterId { get; set; }
    }
}