﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class PlayerIsReadyRequest
    {
        public PlayerIsReadyRequest(string companyId, string challengeId, string userId)
        {
            this.companyId = companyId;
            this.challengeId = challengeId;
            this.userId = userId;
        }

        public PlayerIsReadyRequest(string channelName, string companyId, string challengeId, string userId)
        {
            this.channelName = channelName;
            this.companyId = companyId;
            this.challengeId = challengeId;
            this.userId = userId;
        }

        public string channelName { get; set; }
        public string companyId { get; set; }
        public string challengeId { get; set; }
        public string userId { get; set; }
    }
}