﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class AnswerQuestionRequest
    {
        public AnswerQuestionRequest(string challengeId, string companyId, string questionId, string topicId, string userId, string answer, int round, float timeTaken, bool hasOpponentAnswered)
        {
            this.challengeId = challengeId;
            this.companyId = companyId;
            this.questionId = questionId;
            this.topicId = topicId;
            this.userId = userId;
            this.answer = answer;
            this.round = round;
            this.timeTaken = timeTaken;
            this.hasOpponentAnswered = hasOpponentAnswered;
        }

        public string channelName { get; set; }
        public string challengeId { get; set; }
        public string companyId { get; set; }
        public string questionId { get; set; }
        public string topicId { get; set; }
        public string userId { get; set; }
        public string answer { get; set; }
        public int round { get; set; }
        public float timeTaken { get; set; }
        public bool hasOpponentAnswered { get; set; }
    }
}