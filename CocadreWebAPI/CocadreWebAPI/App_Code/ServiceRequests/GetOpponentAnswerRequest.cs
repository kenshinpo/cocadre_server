﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class GetOpponentAnswerRequest
    {
        public string companyId { get; set; }
        public string challengeId { get; set; }
        public string userId { get; set; }
        public int round { get; set; }
    }
}