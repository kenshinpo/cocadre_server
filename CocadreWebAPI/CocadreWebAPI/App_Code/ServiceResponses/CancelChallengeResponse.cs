﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class CancelChallengeResponse : ErrorResponse
    {
        [DataMember]
        public string EventName { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string ChallengeId { get; set; }
    }
}