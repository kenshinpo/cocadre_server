﻿using CassandraService.Entity;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class CreateChallengeResponse : ErrorResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string EventName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChallengeChannel { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> Users { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Topic Topic { get; set; }
    }
}