﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class StartChallengeResponse : ErrorResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string EventName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HaveBothPlayersAccepted { get; set; }
    }
}