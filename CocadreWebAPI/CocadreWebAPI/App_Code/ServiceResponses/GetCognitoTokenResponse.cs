﻿using System;
using System.Runtime.Serialization;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class GetCognitoTokenResponse : ErrorResponse
    {
        [DataMember]
        public String Token { get; set; }

        [DataMember]
        public String RoleArn { get; set; }

        [DataMember]
        public String IdentityId { get; set; }

        [DataMember]
        public String IdentityPoolId { get; set; }
    }
}