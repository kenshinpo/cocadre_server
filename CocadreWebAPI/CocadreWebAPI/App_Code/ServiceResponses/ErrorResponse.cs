﻿using System;
using System.Runtime.Serialization;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    [Serializable]
    public class ErrorResponse
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public int ErrorCode { get; set; }

        [DataMember]
        public String ErrorMessage { get; set; }
    }
}