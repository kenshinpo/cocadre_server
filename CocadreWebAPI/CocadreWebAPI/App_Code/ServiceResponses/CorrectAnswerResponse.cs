﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class CorrectAnswerResponse : ErrorResponse
    {
        [DataMember]
        public string ChallengeId { get; set; }

        [DataMember]
        public string CorrectAnswer { get; set; }

        [DataMember]
        public string OpponentAnswer { get; set; }
    }
}