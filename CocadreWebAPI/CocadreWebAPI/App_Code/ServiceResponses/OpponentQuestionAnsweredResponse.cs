﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreWebAPI.App_Code.ServiceResponses
{
    public class OpponentQuestionAnsweredResponse : ErrorResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string ChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EventName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Round { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string OpponentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string OpponentAnswer { get; set; }

        [DataMember]
        public float TimeTakenByOpponent { get; set; }

        [DataMember]
        public bool IsCorrect { get; set; }
    }
}