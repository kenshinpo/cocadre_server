﻿using CocadreWebAPI.App_GlobalResources;
using CassandraService.ServiceInterface;
using CocadreWebAPI.PubnubLib.Classes;
using log4net;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using CassandraService.ServiceResponses;
using CassandraService.Entity;
using System.Web.Configuration;

namespace CocadreWebAPI.Account
{
    public class UpdateDeviceToken : IHttpHandler
    {
        private static string pubnubPublishKey = WebConfigurationManager.AppSettings["pubnub_publish_key"].ToString();
        private static string pubnubSubscribeKey = WebConfigurationManager.AppSettings["pubnub_subscribe_key"].ToString();
        private static string pubnubSecretKey = WebConfigurationManager.AppSettings["pubnub_secret_key"].ToString();

        private static Pubnub pubnub = new Pubnub(pubnubPublishKey, pubnubSubscribeKey, pubnubSecretKey);

        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;
                string newDeviceToken = request.DeviceToken;
                int newDeviceType = request.DeviceType;

                UserUpdateDeviceTokenResponse serviceResponse = client.UpdateUserDeviceToken(userId, companyId, newDeviceToken, newDeviceType);

                if (serviceResponse.Success)
                {
                    string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    PushTypeService pushType = PushTypeService.APNS;

                    User user = new User(); 

                    if (newDeviceType == 2)
                    {
                        pushType = PushTypeService.GCM;
                    }

                    pubnub.RegisterDeviceForPush<string>(clientChannel, pushType, newDeviceToken, RegisterDeviceForPushMessage, RegisterDeviceForPushErrorMessage);

                    if (!string.IsNullOrEmpty(serviceResponse.OutdatedDeviceToken) && !serviceResponse.OutdatedDeviceToken.Equals(request.DeviceToken))
                    {
                        PushTypeService outdatedPushType = PushTypeService.APNS;

                        int outdatedDeviceType = serviceResponse.OutdatedDeviceType;
                        string outdatedDeviceToken = serviceResponse.OutdatedDeviceToken;

                        if (outdatedDeviceType == 2)
                        {
                            outdatedPushType = PushTypeService.GCM;
                        }

                        pubnub.UnregisterDeviceForPush<string>(outdatedPushType, outdatedDeviceToken, UnregisterDeviceForPushMessage, UnregisterDeviceForPushErrorMessage);
                    }

                    serviceResponse.OutdatedDeviceToken = null;
                    serviceResponse.OutdatedDeviceType = 0;
                }

                string returnedJsonString = JsonHelper.JsonSerializer<UserUpdateDeviceTokenResponse>(serviceResponse);
                context.Response.Write(returnedJsonString);
                logger.Debug("Json output: " + returnedJsonString);
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }
        }

        private void RegisterDeviceForPushMessage(string message)
        {
            logger.Debug("Register device for push success: " + message);
        }

        private void RegisterDeviceForPushErrorMessage(PubnubClientError error)
        {
            logger.Error("Register device for push error: " + error.Message);
        }

        private void UnregisterDeviceForPushMessage(string message)
        {
            logger.Debug("Unregister device for push success: " + message);
        }

        private void UnregisterDeviceForPushErrorMessage(PubnubClientError error)
        {
            logger.Error("Unregister device for push error: " + error.Message);
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string DeviceToken { get; set; }
            public int DeviceType { get; set; }
        }
    }
}