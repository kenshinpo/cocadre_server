﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace CocadreWebAPI.Account
{
    public class GetColleagues : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            logger.Info("Processing get colleagues event");

            UserSelectAllByDepartmentResponse response = new UserSelectAllByDepartmentResponse();

            try
            {
                ClientService cs = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);
                string requesterUserId = request.RequesterUserId;
                string companyId = request.CompanyId;

                response = cs.SelectAllUsersByDepartment(requesterUserId, companyId);

                string returnedJsonString = JsonHelper.JsonSerializer<UserSelectAllByDepartmentResponse>(response);
                context.Response.Write(returnedJsonString);
                logger.Debug("Json output: " + returnedJsonString);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            timer.Stop();
            logger.Info("Time taken to process: " + timer.Elapsed.Milliseconds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
        }
    }
}