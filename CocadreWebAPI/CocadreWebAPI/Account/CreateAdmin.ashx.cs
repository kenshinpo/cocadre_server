﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Account
{
    public class CreateAdmin : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            UserCreateResponse response = new UserCreateResponse();

            try
            {
                ClientService cs = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string companyTitle = request.CompanyTitle;
                string companyLogoUrl = request.CompanyLogoUrl;
                string plainPassword = request.PlainPassword;
                string firstName = request.FirstName;
                string lastName = request.LastName;
                string email = request.Email;
                string profileImageUrl = request.ProfileImageUrl;
                string position = request.Position;

                response = cs.CreateAdmin(null, companyTitle, companyLogoUrl, null, plainPassword, firstName, lastName, email, profileImageUrl, position, null, null, null, null, null, null);
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<UserCreateResponse>(response);
            context.Response.Write(returnedJsonString);
            logger.Debug("Json output: " + returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string CompanyTitle { get; set; }
            public string CompanyLogoUrl { get; set; }
            public string PlainPassword { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string ProfileImageUrl { get; set; }
            public string Position { get; set; }
        }
    }
}