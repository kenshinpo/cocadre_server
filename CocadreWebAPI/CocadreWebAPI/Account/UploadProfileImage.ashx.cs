﻿using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Account
{
    public class UploadProfileImage : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            UserUploadProfileImageResponse response = new UserUploadProfileImageResponse();
            response.Success = false;

            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string newProfileImageUrl = request.NewProfileImageUrl;

                response = client.UploadProfileImage(userId, companyId, newProfileImageUrl, (int)Dashboard.ProfileApprovalState.Pending);
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            string returnedJsonString = JsonHelper.JsonSerializer<UserUploadProfileImageResponse>(response);
            logger.Debug("Json output: " + returnedJsonString);
            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string NewProfileImageUrl { get; set; }
        }
    }
}