﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreWebAPI.App_GlobalResources;
using log4net;
using PubNubMessaging.Core;
using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace CocadreWebAPI.Account
{
    public class GoLogoutAccount : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        private static string pubnubPublishKey = WebConfigurationManager.AppSettings["pubnub_publish_key"].ToString();
        private static string pubnubSubscribeKey = WebConfigurationManager.AppSettings["pubnub_subscribe_key"].ToString();
        private static string pubnubSecretKey = WebConfigurationManager.AppSettings["pubnub_secret_key"].ToString();

        private static Pubnub pubnub = new Pubnub(pubnubPublishKey, pubnubSubscribeKey, pubnubSecretKey);

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                ClientService cs = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);
                string userId = request.UserId;
                string companyId = request.CompanyId;

                UserUpdateLoginResponse serviceResponse = cs.UpdateUserLogin(userId, companyId, false);

                if (serviceResponse.Success)
                {
                    string groupChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);
                    string companyChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixCompany, companyId);
                    string clientChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { companyChannelName, clientChannelName }, groupChannelName, RemoveChannelMessage, RemoveChannelErrorMessage);
                    pubnub.RemoveChannelGroup<string>(string.Empty, groupChannelName, RemoveGroupMessage, RemoveGroupErrorMessage);

                    if (!string.IsNullOrEmpty(serviceResponse.DeviceToken) && serviceResponse.DeviceType != 0)
                    {
                        PushTypeService pushType = PushTypeService.APNS;

                        int deviceType = serviceResponse.DeviceType;
                        string deviceToken = serviceResponse.DeviceToken;

                        if (deviceType == 2)
                        {
                            pushType = PushTypeService.GCM;
                        }

                        pubnub.UnregisterDeviceForPush<string>(pushType, deviceToken, UnregisterDeviceForPushMessage, UnregisterDeviceForPushErrorMessage);
                    }
                }

                string returnedJsonString = JsonHelper.JsonSerializer<UserUpdateLoginResponse>(serviceResponse);
                context.Response.Write(returnedJsonString);
                logger.Debug("Json output: " + returnedJsonString);
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }
        }

        private void RemoveChannelMessage(string message)
        {
            logger.Debug("Remove channel is success: " + message);
        }

        private void RemoveChannelErrorMessage(PubnubClientError error)
        {
            logger.Error("Remove channel error: " + error.Message);
        }

        private void RemoveGroupMessage(string message)
        {
            logger.Debug("Remove group is success: " + message);
        }

        private void RemoveGroupErrorMessage(PubnubClientError error)
        {
            logger.Error("Remove group error: " + error.Message);
        }

        private void UnregisterDeviceForPushMessage(string message)
        {
            logger.Debug("Unregister device for push success: " + message);
        }

        private void UnregisterDeviceForPushErrorMessage(PubnubClientError error)
        {
            logger.Error("Unregister device for push error: " + error.Message);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }
    }
}