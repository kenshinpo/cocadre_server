﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreWebAPI.App_GlobalResources;
using log4net;
using PubNubMessaging.Core;
using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace CocadreWebAPI.Account
{
    public class GetAccount : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        private static string pubnubPublishKey = WebConfigurationManager.AppSettings["pubnub_publish_key"].ToString();
        private static string pubnubSubscribeKey = WebConfigurationManager.AppSettings["pubnub_subscribe_key"].ToString();
        private static string pubnubSecretKey = WebConfigurationManager.AppSettings["pubnub_secret_key"].ToString();

        private static Pubnub pubnub = new Pubnub(pubnubPublishKey, pubnubSubscribeKey, pubnubSecretKey);

        public void ProcessRequest(HttpContext context)
        {
            UserSelectTokenWithCompanyResponse tokenResponse = new UserSelectTokenWithCompanyResponse();

            try
            {
                ClientService cs = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;

                tokenResponse = cs.SelectUserTokenWithCompany(userId, companyId);

                if (tokenResponse.Success)
                {
                    string groupChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);
                    string companyChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixCompany, companyId);
                    string clientChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    pubnub.ChannelGroupGrantPresenceAccess<string>(groupChannelName, true, true, GrantClientAccessToChannelMessage, GrantClientAccessToChannelErrorMessage);
                    pubnub.ChannelGroupGrantAccess<string>(groupChannelName, true, true, GrantClientAccessToChannelMessage, GrantClientAccessToChannelErrorMessage);

                    foreach(Department department in tokenResponse.User.Departments)
                    {
                        string departmentChannelName = string.Format("{0}{1}", PubnubPrefix.PrefixDepartment, department.Id);
                        pubnub.AddChannelsToChannelGroup<string>(new string[] { departmentChannelName }, groupChannelName, AddChannelToGroupMessage, AddChannelToGroupErrorMessage);
                    }

                    pubnub.AddChannelsToChannelGroup<string>(new string[] { companyChannelName, clientChannelName }, groupChannelName, AddChannelToGroupMessage, AddChannelToGroupErrorMessage);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<UserSelectTokenWithCompanyResponse>(tokenResponse);
            context.Response.Write(returnedJsonString);
            logger.Debug("Json output: " + returnedJsonString);
        }

        private void AddChannelToGroupMessage(string message)
        {
            logger.Debug("Add channel to group success: " + message);
        }

        private void AddChannelToGroupErrorMessage(PubnubClientError error)
        {
            logger.Error("Add channel to group error: " + error.Message);
        }

        private void GrantServerAccessToChannelMessage(string message)
        {
            logger.Debug("Grant server access to channel success: " + message);
        }

        private void GrantServerAccessToChannelErrorMessage(PubnubClientError error)
        {
            logger.Error("Grant server access to channel error: " + error.Message);
        }

        private void GrantClientAccessToChannelMessage(string message)
        {
            logger.Debug("Grant client access to channel success: " + message);
        }

        private void GrantClientAccessToChannelErrorMessage(PubnubClientError error)
        {
            logger.Error("Grant client access to channel error: " + error.Message);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }


    }
}