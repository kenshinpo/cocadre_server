﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentity.Model;
using Amazon.Runtime;
using CocadreWebAPI.App_Code.ServiceResponses;
using CocadreWebAPI.App_GlobalResources;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;

namespace CocadreWebAPI.Account
{
    /// <summary> 
    /// GetCognitoToken 的摘要描述
    /// </summary>
    public class GetCognitoToken : Page, IHttpHandler
    {
        private static ILog Log = LogManager.GetLogger("CocadreWebAPILog");


        override
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                #region Step 1. Get input data.
                String inputData = String.Empty;

                using (StreamReader reader = new StreamReader(context.Request.InputStream))
                {
                    inputData = reader.ReadToEnd();
                    Log.Debug("Request:" + inputData);
                }
                #endregion

                #region Step 2. Json convert to object.
                if (String.IsNullOrEmpty(inputData))
                {
                    Log.Error("Input data is null or empty.");
                    context.Response.Write(JsonHelper.JsonSerializer<GetCognitoTokenResponse>(new GetCognitoTokenResponse { Success = false, ErrorCode = 999, ErrorMessage = "Input data is null or empty." }));
                    return;
                }

                GetCognitoTokenRequest request = null;
                try
                {
                    request = JsonConvert.DeserializeObject<GetCognitoTokenRequest>(inputData);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString(), ex);
                    context.Response.Write(JsonHelper.JsonSerializer<GetCognitoTokenResponse>(new GetCognitoTokenResponse { Success = false, ErrorCode = 999, ErrorMessage = "Cannot parse the request." }));
                    return;
                }
                #endregion

                #region Step 3. Check input data.
                if (String.IsNullOrEmpty(request.CompanyId))
                {
                    Log.Warn("CompanyId parameter is null or empty.");
                    context.Response.Write(JsonHelper.JsonSerializer<GetCognitoTokenResponse>(new GetCognitoTokenResponse { Success = false, ErrorCode = 999, ErrorMessage = "CompanyId parameter is null or empty." }));
                    return;
                }

                if (String.IsNullOrEmpty(request.UserId))
                {
                    Log.Warn("UserId parameter is null or empty.");
                    context.Response.Write(JsonHelper.JsonSerializer<GetCognitoTokenResponse>(new GetCognitoTokenResponse { Success = false, ErrorCode = 999, ErrorMessage = "CUserId parameter is null or empty." }));
                    return;
                }

                /* Check if the user is valid from DB. */
                /* Check if the user is valid from DB. */

                #endregion

                #region Step 4. Business logic. Get an Cognito token from AWS.
                AWSCredentials credentials = new BasicAWSCredentials(AwsConfig.AccessKey, AwsConfig.SecretKey);
                AmazonCognitoIdentityClient client = new AmazonCognitoIdentityClient(credentials, RegionEndpoint.APNortheast1);

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add(AwsConfig.CognitoLinkedLogin, request.CompanyId + ":" + request.UserId);

                GetOpenIdTokenForDeveloperIdentityRequest identityRequest = new GetOpenIdTokenForDeveloperIdentityRequest();
                identityRequest.IdentityPoolId = AwsConfig.CognitoIdentityPoolId;
                identityRequest.Logins = dic;
                identityRequest.TokenDuration = Convert.ToInt64(AwsConfig.CognitoTokenDuration);
                GetOpenIdTokenForDeveloperIdentityResponse identityResponse = client.GetOpenIdTokenForDeveloperIdentity(identityRequest);

                GetCognitoTokenResponse response = new GetCognitoTokenResponse { Success = true, Token = identityResponse.Token, RoleArn = WebConfigurationManager.AppSettings["AwsCognitoRoleArn"].ToString(), IdentityId = identityResponse.IdentityId };
                string returnedJsonString = JsonHelper.JsonSerializer<GetCognitoTokenResponse>(response);

                Log.Debug("Response: " + returnedJsonString);
                context.Response.Write(returnedJsonString);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                context.Response.Write(JsonHelper.JsonSerializer<GetCognitoTokenResponse>(new GetCognitoTokenResponse { Success = false, ErrorCode = 999, ErrorMessage = "CUserId parameter is null or empty." }));
            }
        }
    }
}