﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.IO;
using System.Web;

namespace CocadreWebAPI.Account
{
    public class GoLoginAccount : IHttpHandler
    {
        //private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            AuthenticationSelectUserResponse response = new AuthenticationSelectUserResponse();
            try
            {
                ClientService cs = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);
                string email = request.Email;
                string encryptedPassword = request.EncryptedPassword;
                response = cs.AuthenticateUserForLogin(email, encryptedPassword);
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
            }
            string returnedJsonString = JsonHelper.JsonSerializer<AuthenticationSelectUserResponse>(response);
            context.Response.Write(returnedJsonString);
            logger.Debug("Json output: " + returnedJsonString);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string Email { get; set; }
            public string EncryptedPassword { get; set; }
        }
    }
}