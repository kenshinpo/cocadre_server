﻿using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Account
{
    public class UpdatePassword : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;

            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string newPassword = request.NewPassword;
                string email = request.Email;

                if(!string.IsNullOrEmpty(userId))
                {
                    response = client.UpdatePasswordForUser(userId, email, companyId, newPassword);
                }
                else
                {
                    response = client.ResetPasswordForUser(email);
                }
                
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace.ToString());
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            string returnedJsonString = JsonHelper.JsonSerializer<AuthenticationUpdateResponse>(response);
            logger.Debug("Json output: " + returnedJsonString);
            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string NewPassword { get; set; }
            public string Email { get; set; }
        }
    }
}