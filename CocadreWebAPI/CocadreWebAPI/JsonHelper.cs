﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace CocadreWebAPI
{
    public class JsonHelper
    {
        public static string JsonSerializer<T>(T t)
        {
            #region
            //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            //MemoryStream ms = new MemoryStream();
            //ser.WriteObject(ms, t);
            //string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            //ms.Close();
            #endregion

            return JsonConvert.SerializeObject(t, new JsonSerializerSettings { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate });
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            #region
            //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            //MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            //T obj = (T)ser.ReadObject(ms);
            //return obj;
            #endregion

            return JsonConvert.DeserializeObject<T>(jsonString, new JsonSerializerSettings { DateFormatHandling = DateFormatHandling.MicrosoftDateFormat, DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate });
        }
    }
}