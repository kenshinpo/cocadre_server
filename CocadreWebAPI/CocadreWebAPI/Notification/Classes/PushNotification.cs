﻿using CocadreWebAPI.App_GlobalResources;
using log4net;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace CocadreWebAPI.Notification.Classes
{
    public class PushNotification
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        string pubnubPublishKey = WebConfigurationManager.AppSettings["pubnub_publish_key"].ToString();
        string pubnubSubscribeKey = WebConfigurationManager.AppSettings["pubnub_subscribe_key"].ToString();
        string pubnubSecretKey = WebConfigurationManager.AppSettings["pubnub_secret_key"].ToString();


        public void Push(string targetedUserId, string notificationText, int numberOfNewNotification, int notificationType, int notificationSubType, string challengeId, string feedId, string commentId, string replyId)
        {
            if(!string.IsNullOrEmpty(notificationText))
            {
                Pubnub pubnub = new Pubnub(pubnubPublishKey, pubnubSubscribeKey, pubnubSecretKey);

                string targetedClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, targetedUserId);

                #region Notification
                string alertMessage = WebConfigurationManager.AppSettings["notification_alert"];
                string sound = WebConfigurationManager.AppSettings["notification_alert_sound"];
                int badge = numberOfNewNotification;

                AppleNotification appleNotification = new AppleNotification();
                Aps aps = new Aps();

                aps.alert = notificationText;
                aps.badge = badge;
                aps.sound = sound;

                appleNotification.aps = aps;
                appleNotification.EventName = PubnubEvent.Notification;
                appleNotification.FeedId = feedId;
                appleNotification.CommentId = commentId;
                appleNotification.ReplyId = replyId;
                appleNotification.ChallengeId = challengeId;
                appleNotification.NotificationType = notificationType;
                appleNotification.NotificationSubType = notificationSubType;

                GoogleNotification googleNotification = new GoogleNotification();

                Data data = new Data();

                data.Alert = notificationText;
                data.Badge = badge;
                data.Sound = sound;
                data.EventName = PubnubEvent.Notification;
                data.FeedId = feedId;
                data.CommentId = commentId;
                data.ReplyId = replyId;
                data.ChallengeId = challengeId;
                data.NotificationType = notificationType;
                data.NotificationSubType = notificationSubType;

                googleNotification.data = data;

                Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                notificationDict.Add("pn_apns", appleNotification);
                notificationDict.Add("pn_gcm", googleNotification);

                pubnub.EnableDebugForPushPublish = true;

                Log.Debug("Publish to notify");
                Notify notify = new Notify
                {
                    EventName = PubnubEvent.Notification,
                    NotificationText = notificationText,
                    NumberOfNewNotification = numberOfNewNotification,
                    FeedId = feedId,
                    CommentId = commentId,
                    ReplyId = replyId,
                    ChallengeId = challengeId,
                    NotificationType = notificationType,
                    NotificationSubType = notificationSubType
                };

                notificationDict.Add("event", notify);

                pubnub.Publish<string>(targetedClientChannel, notificationDict, PushNotificationMessage, PushNotificationErrorMessage);
                #endregion

                //pubnub.Publish<string>(targetedClientChannel, notify, PublishToNotifyMessage, PublishToNotifyErrorMessage);
            }
            
        }

        #region Pubnub Callbacks
        private void PushNotificationErrorMessage(PubnubClientError error)
        {
            Log.Debug("Push notification message error: " + error.Message);
        }

        private void PushNotificationMessage(string message)
        {
            Log.Debug("Push notification message: " + message);
        }

        private void PublishToNotifyErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish notification message error: " + error.Message);
        }

        private void PublishToNotifyMessage(string message)
        {
            Log.Debug("Publish notification message: " + message);
        }
        #endregion
    }
}