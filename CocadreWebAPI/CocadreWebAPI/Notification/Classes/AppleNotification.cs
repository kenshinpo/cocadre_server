﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreWebAPI.Notification.Classes
{
    //[DataContract(Name = "pn_apns")]
    [DataContract]
    public class AppleNotification
    {
        [DataMember]
        public Aps aps { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NotificationType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NotificationSubType { get; set; }

        [DataMember]
        public string EventName { get; set; }
    }

    [DataContract]
    public class Aps
    {
        [DataMember]
        public string alert { get; set; }

        [DataMember]
        public int badge { get; set; }

        [DataMember]
        public string sound { get; set; }
    }


}