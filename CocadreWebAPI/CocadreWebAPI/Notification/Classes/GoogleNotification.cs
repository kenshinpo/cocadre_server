﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreWebAPI.Notification.Classes
{
    [DataContract]
    public class GoogleNotification
    {
        [DataMember]
        public Data data { get; set; }
    }

    [DataContract]
    public class Data
    {
        [DataMember]
        public string EventName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NotificationType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NotificationSubType { get; set; }

        [DataMember]
        public string Alert { get; set; }

        [DataMember]
        public int Badge { get; set; }

        [DataMember]
        public string Sound { get; set; }
    }
}