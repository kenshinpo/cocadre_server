﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CocadreWebAPI.Notification.Classes
{
    [DataContract]
    public class Notify
    {
        [DataMember]
        public string EventName { get; set; }

        [DataMember]
        public string NotificationText { get; set; }

        [DataMember]
        public int NumberOfNewNotification { get; set; }

        [DataMember]
        public int NotificationType { get; set; }

        [DataMember]
        public int NotificationSubType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ChallengeId { get; set; }
    }
}