﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Notification
{
    public class UpdateSeenNotification : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Log.Info("Processing update seen notification");

            NotificationUpdateSeenResponse response = new NotificationUpdateSeenResponse();
            response.Success = false;

            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                Log.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;
                int notificationType = request.NotificationType;
                int notificationSubType = request.NotificationSubType;
                string challengeId = request.ChallengeId;
                string feedId = request.FeedId;
                string commentId = request.CommentId;
                string replyId = request.ReplyId;
                string notificationId = request.NotificationId;

                response = client.UpdateSeenNotification(userId, companyId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<NotificationUpdateSeenResponse>(response);
            context.Response.Write(returnedJsonString);

            timer.Stop();
            Log.Info("Time taken to process: " + timer.Elapsed.Milliseconds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public int NotificationType { get; set; }
            public int NotificationSubType { get; set; }
            public string ChallengeId { get; set; }
            public string FeedId { get; set; }
            public string CommentId { get; set; }
            public string ReplyId { get; set; }
            public string NotificationId { get; set; }
        }
    }
}