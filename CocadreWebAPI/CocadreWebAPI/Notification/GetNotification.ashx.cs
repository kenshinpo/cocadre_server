﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Notification
{
    public class GetNotification : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Log.Info("Processing get notification");

            NotificationSelectResponse response = new NotificationSelectResponse();
            response.Success = false;

            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                Log.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;
                string searchContent = request.SearchContent;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectNotificationByUser(userId, companyId, 0, newestTimestamp, oldestTimestamp);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<NotificationSelectResponse>(response);
            context.Response.Write(returnedJsonString);

            timer.Stop();
            Log.Info("Time taken to process: " + timer.Elapsed.Milliseconds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
            public string SearchContent { get; set; }
            public int NumberOfPostsLoaded { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }
    }
}