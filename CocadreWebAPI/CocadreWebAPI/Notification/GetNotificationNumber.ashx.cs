﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Notification
{
    public class GetNotificationNumber : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Log.Info("Processing get notification number");

            NotificationSelectNumberResponse response = new NotificationSelectNumberResponse();
            response.NumberOfNotification = 0;
            response.Success = false;

            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                Log.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.UserId;
                string companyId = request.CompanyId;

                response = client.SelectNotificationNumberByUser(userId, companyId);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            string returnedJsonString = JsonHelper.JsonSerializer<NotificationSelectNumberResponse>(response);
            context.Response.Write(returnedJsonString);

            timer.Stop();
            Log.Info("Time taken to process: " + timer.Elapsed.Milliseconds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string UserId { get; set; }
            public string CompanyId { get; set; }
        }
    }
}