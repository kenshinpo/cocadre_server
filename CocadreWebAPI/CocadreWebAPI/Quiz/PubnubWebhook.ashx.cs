﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreWebAPI.App_Code.ServiceResponses;
using CocadreWebAPI.App_GlobalResources;
using log4net;
using PubNubMessaging.Core;
using System;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

namespace CocadreWebAPI.Quiz
{
    public class PubnubWebhook : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                Log.Debug("Json input: " + jsonInput);

                if (context.Request.ContentType.Equals("application/json"))
                {
                    Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                    string channel = HttpUtility.UrlDecode(request.channel);
                    string userId = request.uuid;

                    string action = request.action;

                    if (channel.Contains("COMPANY@"))
                    {
                        bool isActive = false;

                        if (action.Equals("join"))
                        {
                            Log.Debug("Joining channel");
                            isActive = true;
                        }
                        else
                        {
                            Log.Debug("Leaving channel");
                        }

                        string companyId = channel.Replace("COMPANY@", string.Empty);
                        cs.UpdateUserActivity(userId, companyId, isActive);
                        Log.Debug("Update activty completed");
                    }

                    else if (channel.Contains("CHALLENGE@"))
                    {
                        if (action.Equals("timeout"))
                        {
                            string pubnubPublishKey = WebConfigurationManager.AppSettings["pubnub_publish_key"].ToString();
                            string pubnubSubscribeKey = WebConfigurationManager.AppSettings["pubnub_subscribe_key"].ToString();
                            string pubnubSecretKey = WebConfigurationManager.AppSettings["pubnub_secret_key"].ToString();

                            Pubnub pubnub = new Pubnub(pubnubPublishKey, pubnubSubscribeKey, pubnubSecretKey);

                            Log.Debug("Leaving challenge channel");
                            string challengeId = channel.Replace("CHALLENGE@", string.Empty);

                            ChallengeInvalidateResponse serviceResponse = cs.InvalidateChallenge(userId, null, challengeId, (int)Topic.InvalidateChallengeReason.NetworkError);

                            if (serviceResponse.Success)
                            {
                                User notifiedUser = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.ChallengedUser : serviceResponse.InitiatedUser;
                                string killMessage = WebConfigurationManager.AppSettings["kill_message"].ToString();
                                string firstName = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.InitiatedUser.FirstName : serviceResponse.ChallengedUser.FirstName;

                                string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, notifiedUser.UserId);

                                CancelChallengeResponse response = new CancelChallengeResponse
                                {
                                    EventName = PubnubEvent.ChallengeKill,
                                    ChallengeId = challengeId,
                                    Message = string.Format(killMessage, firstName),
                                    Success = true
                                };

                                pubnub.Publish<string>(clientChannel, response, PublishToKillChallengeMessage, PublishToCancelKillErrorMessage);

                                string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                                string groupInitiatedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.InitiatedUser.UserId);
                                string groupChallengedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.ChallengedUser.UserId);

                                //pubnub.Unsubscribe<string>(challengeChannel, groupInitiatedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);
                                //pubnub.Unsubscribe<string>(challengeChannel, groupChallengedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                                pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupInitiatedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                                pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChallengedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                            }
                        }
                    }

                }
                else
                {
                    Log.Debug("This is a multipart data");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        private void PublishToKillChallengeMessage(string message)
        {
            Log.Debug("Publish to kill challenge success: " + message);
        }

        private void PublishToCancelKillErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to kill challenge error: " + error.Message);
        }

        //private void UnsubscribeGameChannelFromGroupMessage(string message)
        //{
        //    Log.Debug("Unsubscribe game challenge success: " + message);
        //}

        //private void UnsubscribeGameChannelFromGroupErrorMessage(PubnubClientError error)
        //{
        //    Log.Debug("Remove game challenge error: " + error.Message);
        //}

        private void RemoveGameChannelFromGroupMessage(string message)
        {
            Log.Debug("Remove game challenge success: " + message);
        }

        private void RemoveGameChannelFromGroupErrorMessage(PubnubClientError error)
        {
            Log.Debug("Remove game challenge error: " + error.Message);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string action { get; set; }
            public string uuid { get; set; }
            public string channel { get; set; }
        }

    }
}