﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Web;

namespace CocadreWebAPI.Quiz
{
    public class GetOpponentsForTopic : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Log.Info("Processing get opponents for topic event");

            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                Log.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                UserSelectAllBasicResponse serviceResponse = client.SelectAllUsersByTopicId(request.TopicId, request.TopicCategoryId, request.RequesterUserId, request.CompanyId, request.StartsWithName);
                string returnedJsonString = JsonHelper.JsonSerializer<UserSelectAllBasicResponse>(serviceResponse);
                context.Response.Write(returnedJsonString);
                Log.Debug("Json output: " + returnedJsonString);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            timer.Stop();
            Log.Info("Time taken to process: " + timer.Elapsed.Milliseconds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public class Request
        {
            public string CompanyId { get; set; }
            public string RequesterUserId { get; set; }
            public string TopicId { get; set; }
            public string TopicCategoryId { get; set; }
            public string StartsWithName {get; set;}
        }
    }
}