﻿using CocadreWebAPI.App_Code.ServiceResponses;
using CocadreWebAPI.App_GlobalResources;
using CocadreWebAPI.PubnubLib.Classes;
using log4net;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

namespace CocadreWebAPI.Quiz
{
    public class PubnubChallengeEvent : IHttpHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        private static string pubnubPublishKey = WebConfigurationManager.AppSettings["pubnub_publish_key"].ToString();
        private static string pubnubSubscribeKey = WebConfigurationManager.AppSettings["pubnub_subscribe_key"].ToString();
        private static string pubnubSecretKey = WebConfigurationManager.AppSettings["pubnub_secret_key"].ToString();

        public void ProcessRequest(HttpContext context)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();

            try
            {
                Pubnub pubnub = new Pubnub(pubnubPublishKey, pubnubSubscribeKey, pubnubSecretKey);
                PubnubHandler pubnubHandler = new PubnubHandler();

                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string receivedBody = reader.ReadToEnd();

                Log.Debug("Content: " + receivedBody);
                Event eventRequest = JsonHelper.JsonDeserialize<Event>(receivedBody);

                string eventName = eventRequest.EventName;

                if (eventName == PubnubEvent.ChallengeCreate)
                {
                    Log.Debug("Create challenge");
                    Log.Info("Processing create challenge event");

                    CreateChallengeRequest request = new CreateChallengeRequest(eventRequest.CompanyId, eventRequest.TopicId, eventRequest.TopicCategoryId, eventRequest.UserId, eventRequest.OpponentUserId);

                    pubnubHandler.CreateChallenge(pubnub, context, request);
                }
                else if (eventName == PubnubEvent.ChallengeFetchQuestions)
                {
                    Log.Debug("Prepare to fetch questions");
                    Log.Info("Processing fetched questions event");

                    GetQuestionsRequest request = new GetQuestionsRequest(eventRequest.CompanyId, eventRequest.ChallengeId, eventRequest.UserId);

                    pubnubHandler.FetchQuestions(context, request);
                }
                else if (eventName == PubnubEvent.ChallengeIsReady)
                {
                    Log.Debug("Ready for challenge");
                    Log.Info("Processing challenge ready event");

                    PlayerIsReadyRequest request = new PlayerIsReadyRequest(eventRequest.CompanyId, eventRequest.ChallengeId, eventRequest.UserId);

                    pubnubHandler.PlayerIsReady(context, pubnub, request);
                }
                else if (eventName == PubnubEvent.ChallengeQuestionAnswered)
                {
                    Log.Debug("Question answered");
                    Log.Info("Processing question answered event");

                    AnswerQuestionRequest request = new AnswerQuestionRequest(eventRequest.ChallengeId,
                                                                              eventRequest.CompanyId,
                                                                              eventRequest.QuestionId,
                                                                              eventRequest.TopicId,
                                                                              eventRequest.UserId,
                                                                              eventRequest.Answer,
                                                                              eventRequest.Round,
                                                                              eventRequest.TimeTaken,
                                                                              eventRequest.HasOpponentAnswered);
                    pubnubHandler.AnswerQuestion(context, pubnub, request);
                }
                else if (eventName == PubnubEvent.ChallengeStartWithoutOpponent)
                {
                    Log.Debug("Start game without opponent");
                    Log.Info("Processing Start game without opponent event");

                    StartGameWithoutOpponentRequest request = new StartGameWithoutOpponentRequest(eventRequest.ChallengeId,
                                                                                                  eventRequest.CompanyId,
                                                                                                  eventRequest.UserId);

                    pubnubHandler.StartGameWithoutOpponent(context, pubnub, request);
                }
                else if (eventName == PubnubEvent.ChallengePrepareToJoin)
                {
                    Log.Debug("Prepared to join");
                    Log.Info("Processing preparing to join");

                    PrepareToJoinRequest request = new PrepareToJoinRequest
                    {
                        eventName = eventRequest.EventName,
                        challengeId = eventRequest.ChallengeId,
                        companyId = eventRequest.CompanyId,
                        requesterId = eventRequest.UserId
                    };

                    pubnubHandler.GetChallengeForOffline(pubnub, context, request);
                }
                else if (eventName == PubnubEvent.ChallengeOfflineRoundStart)
                {
                    Log.Debug("Prepared to start next round offline");
                    Log.Info("Processing start next round offline");

                    GetOpponentAnswerRequest request = new GetOpponentAnswerRequest
                    {
                        userId = eventRequest.UserId,
                        companyId = eventRequest.CompanyId,
                        challengeId = eventRequest.ChallengeId,
                        round = eventRequest.Round
                    };

                    pubnubHandler.GetOpponentAnswer(context, request);
                }
                else if (eventName == PubnubEvent.ChallengeKill)
                {
                    Log.Debug("Prepared to kill challenge");
                    Log.Info("Processing kill challenge");

                    InvalidateChallengeRequest request = new InvalidateChallengeRequest
                    {
                        userId = eventRequest.UserId,
                        companyId = eventRequest.CompanyId,
                        challengeId = eventRequest.ChallengeId
                    };

                    pubnubHandler.KillChallenge(context, pubnub, request);
                }
                else if (eventName == PubnubEvent.ChallengeCancel)
                {
                    Log.Debug("Prepared to cancel challenge");
                    Log.Info("Processing cancel challenge");

                    InvalidateChallengeRequest request = new InvalidateChallengeRequest
                    {
                        userId = eventRequest.UserId,
                        companyId = eventRequest.CompanyId,
                        challengeId = eventRequest.ChallengeId,
                        fromInitiator = eventRequest.FromInitiator
                    };

                    pubnubHandler.CancelChallenge(context, pubnub, request);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            timer.Stop();
            Log.Info("Time taken to process: " + timer.Elapsed.Milliseconds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Event
        {
            public string EventName { get; set; }
            public string Channel { get; set; }
            public string UserId { get; set; }
            public bool HasOpponentAnswered { get; set; }

            public string CompanyId { get; set; }
            public string TopicId { get; set; }
            public string TopicCategoryId { get; set; }
            public string OpponentUserId { get; set; }
            public string ChallengeId { get; set; }
            public string Answer { get; set; }
            public float TimeTaken { get; set; }
            public string QuestionId { get; set; }
            public int Round { get; set; }
            public bool FromInitiator{ get; set; }
        }
    }
}