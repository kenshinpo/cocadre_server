﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.PubnubLib.Classes
{
    public class PubnubResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public string service { get; set; }
        public Payload payload { get; set; }
    }

    public class Payload
    {
        public string[] channels { get; set; }
    }
}