﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CocadreWebAPI.App_Code.ServiceResponses;
using CocadreWebAPI.App_GlobalResources;
using CocadreWebAPI.Notification.Classes;
using log4net;
using PubNubMessaging.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;

namespace CocadreWebAPI.PubnubLib.Classes
{
    public class PubnubHandler
    {
        private static readonly ILog Log = LogManager.GetLogger("CocadreWebAPILog");

        public void CreateChallenge(Pubnub pubnub, HttpContext context, CreateChallengeRequest request)
        {
            try
            {
                string initiatorUserId = request.initiatorUserId;
                string challengedUserId = request.opponentUserId;
                string companyId = request.companyId;
                string topicId = request.topicId;
                string topicCategoryId = request.topicCategoryId;

                ClientService client = new ClientService();

                ChallengeCreateResponse serviceResponse = client.CreateChallengeWithTopicId(companyId, initiatorUserId, challengedUserId, topicId, topicCategoryId);

                string returnedJsonString = string.Empty;

                if (serviceResponse.Success)
                {
                    Log.Debug("Challenge created: " + serviceResponse.ChallengeId);

                    CreateChallengeResponse response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = true,
                        ChallengeId = serviceResponse.ChallengeId,
                        Topic = serviceResponse.Topic,
                        Users = serviceResponse.Players
                    };
                    returnedJsonString = JsonHelper.JsonSerializer<CreateChallengeResponse>(response);

                    //string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, response.ChallengeId);

                    //string initiatorGroupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, initiatorUserId);

                    //string opponentGroupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, challengedUserId);
                    string opponentClientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, challengedUserId);


                    //// Adding challenge channel to both group channels
                    //pubnub.AddChannelsToChannelGroup<string>(new string[] { challengeChannel }, initiatorGroupChannel, AddChannelToGroupMessage, AddChannelToGroupErrorMessage);
                    //pubnub.AddChannelsToChannelGroup<string>(new string[] { challengeChannel }, opponentGroupChannel, AddChannelToGroupMessage, AddChannelToGroupErrorMessage);


                    #region Notification
                    string alertMessage = WebConfigurationManager.AppSettings["notification_alert"];
                    string initiatorFirstName = serviceResponse.Players[0].FirstName;
                    string initiatorLastName = serviceResponse.Players[0].LastName;
                    string initiatorFullName = string.Format("{0} {1}", initiatorFirstName, initiatorLastName);
                    string topicTitle = serviceResponse.Topic.TopicTitle;

                    string sound = WebConfigurationManager.AppSettings["notification_alert_sound"];
                    int badge = serviceResponse.NotificationBadgeForTargetedUser;

                    AppleNotification appleNotification = new AppleNotification();
                    Aps aps = new Aps();

                    aps.alert = string.Format(alertMessage, initiatorFullName, topicTitle);
                    aps.badge = badge;
                    aps.sound = sound;

                    appleNotification.aps = aps;
                    appleNotification.ChallengeId = response.ChallengeId;
                    appleNotification.EventName = PubnubEvent.ChallengePrepareToJoin;

                    GoogleNotification googleNotification = new GoogleNotification();

                    Data data = new Data();

                    data.Alert = string.Format(alertMessage, initiatorFullName, topicTitle);
                    data.Badge = badge;
                    data.Sound = sound;
                    data.ChallengeId = response.ChallengeId;
                    data.EventName = PubnubEvent.ChallengePrepareToJoin;

                    googleNotification.data = data;


                    Dictionary<string, object> notificationDict = new Dictionary<string, object>();
                    notificationDict.Add("pn_apns", appleNotification);
                    notificationDict.Add("pn_gcm", googleNotification);

                    pubnub.EnableDebugForPushPublish = true;
                    pubnub.Publish<string>(opponentClientChannel, notificationDict, PushNotificationMessage, PushNotificationErrorMessage);
                    #endregion

                    Log.Debug("Publish to opponent to join");

                    pubnub.Publish<string>(opponentClientChannel, response, PublishToJoinChallengeMessage, PublishToJoinChallengeErrorMessage);
                }
                else
                {
                    Log.Error("Challenge cannot be created");
                    Log.Error(serviceResponse.ErrorMessage);

                    CreateChallengeResponse response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    returnedJsonString = JsonHelper.JsonSerializer<CreateChallengeResponse>(response);
                }

                Log.Debug("ReturnedJsonString:" + returnedJsonString);

                context.Response.Write(returnedJsonString);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        public void GetChallengeForOffline(Pubnub pubnub, HttpContext context, PrepareToJoinRequest request)
        {
            try
            {

                string requesterId = request.requesterId;
                string companyId = request.companyId;
                string challengeId = request.challengeId;

                ClientService client = new ClientService();

                ChallengeSelectResponse serviceResponse = client.SelectChallengeWithChallengeId(companyId, challengeId, requesterId);

                string returnedJsonString = string.Empty;

                if (serviceResponse.Success)
                {
                    Log.Debug("Offline challenge found: " + serviceResponse.ChallengeId);

                    CreateChallengeResponse response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = true,
                        ChallengeId = serviceResponse.ChallengeId,
                        Topic = serviceResponse.Topic,
                        Users = serviceResponse.Players
                    };

                    returnedJsonString = JsonHelper.JsonSerializer<CreateChallengeResponse>(response);

                    string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, response.ChallengeId);
                    string requesterGroupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, requesterId);

                    pubnub.AddChannelsToChannelGroup<string>(new string[] { challengeChannel }, requesterGroupChannel, AddChannelToGroupMessage, AddChannelToGroupErrorMessage);

                }
                else
                {
                    CreateChallengeResponse response = new CreateChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengePrepareToJoin,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    returnedJsonString = JsonHelper.JsonSerializer<CreateChallengeResponse>(response);
                }

                context.Response.Write(returnedJsonString);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        public void FetchQuestions(HttpContext context, GetQuestionsRequest request)
        {
            try
            {
                ClientService client = new ClientService();
                QuestionSelectAllResponse serviceResponse = client.SelectAllQuestionsForChallenge(request.userId, request.companyId, request.challengeId);

                string returnedJsonString = JsonHelper.JsonSerializer<QuestionSelectAllResponse>(serviceResponse);

                context.Response.Write(returnedJsonString);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        public void PlayerIsReady(HttpContext context, Pubnub pubnub, PlayerIsReadyRequest request)
        {
            try
            {
                string userId = request.userId;
                string companyId = request.companyId;
                string challengeId = request.challengeId;

                ClientService client = new ClientService();
                ChallengeIsReadyResponse serviceResponse = client.SetPlayerReadyForChallenge(userId, companyId, challengeId);

                string returnedJsonString = JsonHelper.JsonSerializer<ChallengeIsReadyResponse>(serviceResponse);
                context.Response.Write(returnedJsonString);

                if (serviceResponse.Success)
                {
                    Log.Debug(string.Format("Player {0} is ready for challenge id: {1}", request.userId, request.challengeId));
                    Log.Debug("is_live: " + serviceResponse.IsLive);

                    string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                    string requesterGroupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, request.userId);

                    // Adding challenge channel to both requester channels
                    pubnub.AddChannelsToChannelGroup<string>(new string[] { challengeChannel }, requesterGroupChannel, AddChannelToGroupMessage, AddChannelToGroupErrorMessage);

                    // Non-live game
                    if (!serviceResponse.IsLive)
                    {
                        Log.Debug(string.Format("Challenge id: {0} is not live", request.challengeId));

                        string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                        SinglePlayerToStartResponse response = new SinglePlayerToStartResponse
                        {
                            EventName = PubnubEvent.ChallengeStartOffline,
                            ChallengeId = challengeId,
                            Success = true
                        };

                        pubnub.Publish<string>(clientChannel, response, PublishToJoinChallengeMessage, PublishToJoinChallengeErrorMessage);
                    }
                    // Live game
                    else
                    {
                        if (serviceResponse.HaveBothPlayersAccepted)
                        {
                            Log.Debug(string.Format("Challenge id: {0} is live", request.challengeId));

                            StartChallengeResponse response = new StartChallengeResponse
                            {
                                EventName = PubnubEvent.ChallengeStart,
                                ChallengeId = challengeId,
                                Success = true,
                                HaveBothPlayersAccepted = true
                            };

                            pubnub.Publish<string>(challengeChannel, response, PublishToJoinChallengeMessage, PublishToJoinChallengeErrorMessage);
                        }

                    }
                }
                else
                {
                    Log.Debug("Player is not ready for game");
                    Log.Error(serviceResponse.ErrorMessage);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        public void AnswerQuestion(HttpContext context, Pubnub pubnub, AnswerQuestionRequest request)
        {
            try
            {
                string userId = request.userId;
                string companyId = request.companyId;
                string challengeId = request.challengeId;
                string questionId = request.questionId;
                string topicId = request.topicId;
                string answer = request.answer;
                float timeTaken = request.timeTaken;
                int round = request.round;

                ClientService client = new ClientService();
                QuestionAnswerResponse serviceResponse = client.AnswerChallengeQuestion(challengeId,
                                                                                        companyId,
                                                                                        userId,
                                                                                        answer,
                                                                                        timeTaken,
                                                                                        questionId,
                                                                                        topicId,
                                                                                        round);
                if (serviceResponse.Success)
                {
                    Log.Debug("Question answered successfully");

                    // Current user 
                    CorrectAnswerResponse correctResponse = new CorrectAnswerResponse
                    {
                        ChallengeId = challengeId,
                        CorrectAnswer = serviceResponse.CorrectAnswer,
                        OpponentAnswer = serviceResponse.AnswerOfOpponent,
                        Success = true
                    };

                    string returnedJsonString = JsonHelper.JsonSerializer<CorrectAnswerResponse>(correctResponse);

                    context.Response.Write(returnedJsonString);

                    if (serviceResponse.IsLive)
                    {
                        Log.Debug("Question answered LIVE");

                        // Let opponent know current user answer
                        // Using current user play data
                        OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                        {
                            ChallengeId = challengeId,
                            EventName = PubnubEvent.ChallengeOpponentQuestionAnswered,
                            Success = true,
                            Round = round,
                            OpponentId = userId,
                            OpponentAnswer = answer,
                            TimeTakenByOpponent = timeTaken,
                            IsCorrect = serviceResponse.CorrectAnswer.Equals(request.answer) ? true : false
                        };

                        string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, serviceResponse.OpponentId);
                        pubnub.Publish<string>(opponentChannel, opponentResponse, PublishOpponentAnswerMessage, PublishOpponentAnswerErrorMessage);
                    }
                    else
                    {
                        Log.Debug("Question answered OFFLINE");

                        // Current user is challengedUser
                        if (!serviceResponse.IsInitiator)
                        {
                            // Back to challenged user
                            // Using previously stored opponent play data
                            Log.Debug("Challenged user answer");

                            if (serviceResponse.TimeTakenByOpponent > 0 && !request.hasOpponentAnswered)
                            {
                                Log.Debug("Offline round start does not have answer for challenged user");
                                Log.Debug("Initiated user answer: " + serviceResponse.AnswerOfOpponent);
                                Log.Debug("Correct answer: " + serviceResponse.CorrectAnswer);
                                Log.Debug("Is corrrect: " + serviceResponse.CorrectAnswer.Equals(serviceResponse.AnswerOfOpponent));

                                OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                                {
                                    ChallengeId = challengeId,
                                    EventName = PubnubEvent.ChallengeOpponentQuestionAnswered,
                                    Success = true,
                                    Round = round,
                                    OpponentId = serviceResponse.OpponentId,
                                    OpponentAnswer = serviceResponse.AnswerOfOpponent,
                                    TimeTakenByOpponent = serviceResponse.TimeTakenByOpponent,
                                    IsCorrect = serviceResponse.CorrectAnswer.Equals(serviceResponse.AnswerOfOpponent) ? true : false
                                };

                                string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);
                                pubnub.Publish<string>(opponentChannel, opponentResponse, PublishOpponentAnswerMessage, PublishOpponentAnswerErrorMessage);

                                string jsonString = JsonHelper.JsonSerializer<OpponentQuestionAnsweredResponse>(opponentResponse);
                                Log.Debug(string.Format("Returned to {0}, {1}: ", userId, jsonString));
                            }

                            if (serviceResponse.IsLastRound)
                            {
                                string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, serviceResponse.OpponentId);
                                PushNotification notification = new PushNotification();
                                notification.Push(serviceResponse.OpponentId,
                                                  serviceResponse.NotificationText,
                                                  serviceResponse.NumberOfNewNotification,
                                                  (int)CassandraService.Entity.Notification.NotificationType.Game,
                                                  (int)CassandraService.Entity.Notification.NotificationGameSubType.ChallengedUserCompletedChallenge,
                                                  challengeId, null, null, null);
                            }
                        }
                        else
                        {
                            Log.Debug("Initiated user answer");
                            Log.Debug("Challenger time: " + serviceResponse.TimeTakenByOpponent);

                            // If initiator took longer time than opponent for a particular question in offline mode
                            //&& timeTaken >= serviceResponse.TimeTakenByOpponent
                            //if (serviceResponse.TimeTakenByOpponent != 0)
                            //{
                            Log.Debug("Challenged user caught up to initiated user");
                            Log.Debug("Initiated user took longer time to answer than challenged user");

                            // Back to challenged user
                            OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                            {
                                ChallengeId = challengeId,
                                EventName = PubnubEvent.ChallengeOpponentQuestionAnswered,
                                Success = true,
                                Round = round,
                                OpponentId = userId,
                                OpponentAnswer = answer,
                                TimeTakenByOpponent = timeTaken,
                                IsCorrect = serviceResponse.CorrectAnswer.Equals(request.answer) ? true : false
                            };

                            string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, serviceResponse.OpponentId);
                            pubnub.Publish<string>(opponentChannel, opponentResponse, PublishOpponentAnswerMessage, PublishOpponentAnswerErrorMessage);

                            string jsonString = JsonHelper.JsonSerializer<OpponentQuestionAnsweredResponse>(opponentResponse);
                            Log.Debug(string.Format("Returned to {0}, {1}: ", serviceResponse.OpponentId, jsonString));
                            //}
                        }
                    }

                    if (serviceResponse.IsLastRound)
                    {
                        string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                        string groupChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, userId);

                        //Log.Debug("Unsubscribe challenge from channel: " + challengeChannel);
                        //pubnub.Unsubscribe<string>(challengeChannel, groupChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                        //Log.Debug("Remove challenge from channel: " + challengeChannel);
                        //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                    }

                }
                else
                {
                    Log.Error("Error answering question");
                    Log.Error(serviceResponse.ErrorMessage);

                    string opponentChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);

                    // Current user 
                    CorrectAnswerResponse correctResponse = new CorrectAnswerResponse
                    {
                        Success = false,
                        ChallengeId = challengeId,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    string returnedJsonString = JsonHelper.JsonSerializer<CorrectAnswerResponse>(correctResponse);

                    context.Response.Write(returnedJsonString);

                    OpponentQuestionAnsweredResponse opponentResponse = new OpponentQuestionAnsweredResponse
                    {
                        ChallengeId = challengeId,
                        EventName = PubnubEvent.ChallengeError,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    pubnub.Publish<string>(opponentChannel, opponentResponse, PublishOpponentAnswerMessage, PublishOpponentAnswerErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    ChallengeId = request.challengeId,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        public void StartGameWithoutOpponent(HttpContext context, Pubnub pubnub, StartGameWithoutOpponentRequest request)
        {
            try
            {
                string userId = request.userId;
                string companyId = request.companyId;
                string challengeId = request.challengeId;

                ClientService client = new ClientService();
                ChallengeStartWithoutOpponentResponse serviceResponse = client.StartChallengeWithoutOpponent(challengeId, userId, companyId);

                string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, userId);
                SinglePlayerToStartResponse response = null;

                if (serviceResponse.Success)
                {
                    Log.Debug("Start game without opponent is successful");

                    response = new SinglePlayerToStartResponse
                    {
                        EventName = PubnubEvent.ChallengeStartOffline,
                        ChallengeId = challengeId,
                        Success = true
                    };

                    string returnedJsonString = JsonHelper.JsonSerializer<SinglePlayerToStartResponse>(response);
                    Log.Debug("returnedJsonString: " + returnedJsonString);
                    context.Response.Write(returnedJsonString);

                    pubnub.Publish<string>(clientChannel, response, PublishToJoinChallengeMessage, PublishToJoinChallengeErrorMessage);
                }
                else
                {
                    Log.Error("Start game without opponent has error");
                    Log.Error(serviceResponse.ErrorMessage);

                    response = new SinglePlayerToStartResponse
                    {
                        EventName = PubnubEvent.ChallengeError,
                        ChallengeId = challengeId,
                        Success = false,
                        ErrorCode = serviceResponse.ErrorCode,
                        ErrorMessage = serviceResponse.ErrorMessage
                    };

                    string returnedJsonString = JsonHelper.JsonSerializer<SinglePlayerToStartResponse>(response);
                    Log.Debug("returnedJsonString: " + returnedJsonString);
                    context.Response.Write(returnedJsonString);

                    pubnub.Publish<string>(clientChannel, response, PublishToJoinChallengeMessage, PublishToJoinChallengeErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    ChallengeId = request.challengeId,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        public void GetOpponentAnswer(HttpContext context, GetOpponentAnswerRequest request)
        {
            try
            {
                string userId = request.userId;
                string companyId = request.companyId;
                string challengeId = request.challengeId;
                int round = request.round;

                ClientService client = new ClientService();
                ChallengeOfflineSelectOpponentAnswerResponse serviceResponse = client.SelectOpponentAnswerForOfflineGame(challengeId, userId, companyId, round);

                string returnedJsonString = JsonHelper.JsonSerializer<ChallengeOfflineSelectOpponentAnswerResponse>(serviceResponse);

                context.Response.Write(returnedJsonString);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    ChallengeId = request.challengeId,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        // For invalid/ leave challenge
        public void KillChallenge(HttpContext context, Pubnub pubnub, InvalidateChallengeRequest request)
        {
            try
            {
                string userId = request.userId;
                string companyId = request.companyId;
                string challengeId = request.challengeId;

                ClientService client = new ClientService();
                ChallengeInvalidateResponse serviceResponse = client.InvalidateChallenge(userId, companyId, challengeId, (int)Topic.InvalidateChallengeReason.InvalidChallenge);

                string returnedJsonString = JsonHelper.JsonSerializer<ChallengeInvalidateResponse>(serviceResponse);

                if (serviceResponse.Success)
                {
                    User notifiedUser = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.ChallengedUser : serviceResponse.InitiatedUser;
                    string killMessage = WebConfigurationManager.AppSettings["kill_message"].ToString();
                    string firstName = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.InitiatedUser.FirstName : serviceResponse.ChallengedUser.FirstName;

                    string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, notifiedUser.UserId);

                    CancelChallengeResponse response = new CancelChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengeKill,
                        ChallengeId = challengeId,
                        Message = string.Format(killMessage, firstName),
                        Success = true
                    };

                    pubnub.Publish<string>(clientChannel, response, PublishToKillChallengeMessage, PublishToCancelKillErrorMessage);

                    string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                    string groupInitiatedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.InitiatedUser.UserId);
                    string groupChallengedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.ChallengedUser.UserId);

                    //pubnub.Unsubscribe<string>(challengeChannel, groupInitiatedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);
                    //pbnub.Unsubscribe<string>(challengeChannel, groupChallengedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupInitiatedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChallengedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                }

                context.Response.Write(returnedJsonString);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    ChallengeId = request.challengeId,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        // Client input (cross, cancel button)
        public void CancelChallenge(HttpContext context, Pubnub pubnub, InvalidateChallengeRequest request)
        {
            try
            {
                string userId = request.userId;
                string companyId = request.companyId;
                string challengeId = request.challengeId;
                bool fromInitiator = request.fromInitiator;

                ClientService client = new ClientService();
                ChallengeInvalidateResponse serviceResponse = client.InvalidateChallenge(userId, companyId, challengeId, fromInitiator ? (int)Topic.InvalidateChallengeReason.CancelChallengeByInitiator : (int)Topic.InvalidateChallengeReason.CancelChallengeByChallenge);

                string returnedJsonString = JsonHelper.JsonSerializer<ChallengeInvalidateResponse>(serviceResponse);

                if (serviceResponse.Success)
                {
                    User notifiedUser = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.ChallengedUser : serviceResponse.InitiatedUser;
                    string cancelMessage = WebConfigurationManager.AppSettings["cancel_message"].ToString();
                    string firstName = userId == serviceResponse.InitiatedUser.UserId ? serviceResponse.InitiatedUser.FirstName : serviceResponse.ChallengedUser.FirstName;

                    string clientChannel = string.Format("{0}{1}", PubnubPrefix.PrefixClient, notifiedUser.UserId);
                    CancelChallengeResponse response = new CancelChallengeResponse
                    {
                        EventName = PubnubEvent.ChallengeCancel,
                        ChallengeId = challengeId,
                        Message = string.Format(cancelMessage, firstName),
                        Success = true
                    };
                    pubnub.Publish<string>(clientChannel, response, PublishToCancelChallengeMessage, PublishToCancelChallengeErrorMessage);

                    string challengeChannel = string.Format("{0}{1}", PubnubPrefix.PrefixChallenge, challengeId);
                    string groupInitiatedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.InitiatedUser.UserId);
                    string groupChallengedChannel = string.Format("{0}{1}", PubnubPrefix.PrefixGroup, serviceResponse.ChallengedUser.UserId);

                    //pubnub.Unsubscribe<string>(challengeChannel, groupInitiatedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);
                    //pubnub.Unsubscribe<string>(challengeChannel, groupChallengedChannel, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupMessage, UnsubscribeGameChannelFromGroupErrorMessage);

                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupInitiatedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                    //pubnub.RemoveChannelsFromChannelGroup<string>(new string[] { challengeChannel }, groupChallengedChannel, RemoveGameChannelFromGroupMessage, RemoveGameChannelFromGroupErrorMessage);
                }

                context.Response.Write(returnedJsonString);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                ExceptionErrorResponse response = new ExceptionErrorResponse
                {
                    EventName = PubnubEvent.ChallengeError,
                    ChallengeId = request.challengeId,
                    Success = false,
                    ErrorMessage = ex.ToString()
                };
                string returnedJsonString = JsonHelper.JsonSerializer<ExceptionErrorResponse>(response);
                context.Response.Write(returnedJsonString);
            }
        }

        #region Pubnub Callbacks
        private void PresenceCheckErrorMessage(PubnubClientError error)
        {
            Log.Debug("Presence check error: " + error.Message);
        }

        private void PushNotificationMessage(string message)
        {
            Log.Debug("Push notification message: " + message);
        }

        private void PushNotificationErrorMessage(PubnubClientError error)
        {
            Log.Debug("Push notification error: " + error.Message);
        }

        private void AddChannelToGroupMessage(string message)
        {
            Log.Debug("Add channel to group success: " + message);
        }

        private void AddChannelToGroupErrorMessage(PubnubClientError error)
        {
            Log.Debug("Add channel to group error: " + error.Message);
        }

        private void PublishToJoinChallengeMessage(string message)
        {
            Log.Debug("Publish to join channel to challenge success: " + message);
        }

        private void PublishToJoinChallengeErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to join channel to challenge error: " + error.Message);
        }

        private void PublishToCancelChallengeMessage(string message)
        {
            Log.Debug("Publish to cancel challenge success: " + message);
        }

        private void PublishToCancelChallengeErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to cancel challenge error: " + error.Message);
        }

        private void PublishToKillChallengeMessage(string message)
        {
            Log.Debug("Publish to kill challenge success: " + message);
        }

        private void PublishToCancelKillErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish to kill challenge error: " + error.Message);
        }

        private void PublishOpponentAnswerMessage(string message)
        {
            Log.Debug("Publish opponent answer success: " + message);
        }

        private void PublishOpponentAnswerErrorMessage(PubnubClientError error)
        {
            Log.Debug("Publish opponent answer error: " + error.Message);
        }

        private void UnsubscribeGameChannelFromGroupMessage(string message)
        {
            Log.Debug("Unsubscribe game challenge success: " + message);
        }

        private void UnsubscribeGameChannelFromGroupErrorMessage(PubnubClientError error)
        {
            Log.Debug("Unsubscribe game challenge error: " + error.Message);
        }

        private void RemoveGameChannelFromGroupMessage(string message)
        {
            Log.Debug("Remove game challenge success: " + message);
        }

        private void RemoveGameChannelFromGroupErrorMessage(PubnubClientError error)
        {
            Log.Debug("Remove game challenge error: " + error.Message);
        }
        #endregion
    }
}