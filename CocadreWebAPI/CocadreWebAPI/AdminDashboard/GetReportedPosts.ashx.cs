﻿using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.AdminDashboard
{
    public class GetReportedPosts : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            DashboardSelectReportedFeedResponse response = new DashboardSelectReportedFeedResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string companyId = request.CompanyId;
                string adminUserId = request.AdminUserId;
                int reportState = request.ReportState;

                response = cs.SelectReportedFeedPosts(adminUserId, companyId, reportState);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            string returnedJsonString = JsonHelper.JsonSerializer<DashboardSelectReportedFeedResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string CompanyId { get; set; }
            public string AdminUserId { get; set; }
            public int ReportState { get; set; }
        }
    }
}