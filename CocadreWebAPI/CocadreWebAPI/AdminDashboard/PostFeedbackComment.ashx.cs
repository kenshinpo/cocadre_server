﻿using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.AdminDashboard
{
    public class PostFeedbackComment : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");

        public void ProcessRequest(HttpContext context)
        {
            DashboardCreateFeedbackCommentResponse response = new DashboardCreateFeedbackCommentResponse();
            response.Success = false;

            try
            {
                ClientService cs = new ClientService();
                Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();

                logger.Debug("Json input: " + jsonInput);

                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string companyId = request.CompanyId;
                string adminUserId = request.AdminUserId;
                string feedId = request.FeedId;
                string comment = request.Comment;

                response = cs.CreateFeedbackComment(adminUserId, companyId, feedId, comment);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            string returnedJsonString = JsonHelper.JsonSerializer<DashboardCreateFeedbackCommentResponse>(response);

            logger.Debug("Json output: " + returnedJsonString);

            context.Response.Write(returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string AdminUserId { get; set; }
            public string Comment { get; set; }
            public string CompanyId { get; set; }
            public string FeedId { get; set; }
        }
    }
}