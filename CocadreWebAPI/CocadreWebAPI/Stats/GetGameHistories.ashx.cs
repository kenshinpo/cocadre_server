﻿using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Stats
{
    public class GetGameHistories : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");
        public void ProcessRequest(HttpContext context)
        {
            AnalyticsSelectAllGameHistoriesResponse response = new AnalyticsSelectAllGameHistoriesResponse();
            response.Success = false;
            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                int numberOfHistoryLoaded = request.NumberOfHistoryLoaded;
                DateTime? newestTimestamp = request.NewestTimestamp;
                DateTime? oldestTimestamp = request.OldestTimestamp;

                response = client.SelectAllGameHistoriesForUser(userId, companyId, numberOfHistoryLoaded, newestTimestamp, oldestTimestamp);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            string returnedJsonString = JsonHelper.JsonSerializer<AnalyticsSelectAllGameHistoriesResponse>(response);
            context.Response.Write(returnedJsonString);
            logger.Debug("Json output: " + returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public int NumberOfHistoryLoaded { get; set; }
            public DateTime? NewestTimestamp { get; set; }
            public DateTime? OldestTimestamp { get; set; }
        }
    }
}