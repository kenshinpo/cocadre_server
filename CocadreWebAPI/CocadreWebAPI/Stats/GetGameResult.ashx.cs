﻿using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CocadreWebAPI.Stats
{
    public class GetGameResult : IHttpHandler
    {
        private static readonly ILog logger = LogManager.GetLogger("CocadreWebAPILog");
        public void ProcessRequest(HttpContext context)
        {
            AnalyticsSelectGameResultResponse response = new AnalyticsSelectGameResultResponse();
            response.Success = false;
            try
            {
                ClientService client = new ClientService();
                System.IO.Stream jsonStream = context.Request.InputStream;
                StreamReader reader = new StreamReader(jsonStream);
                string jsonInput = reader.ReadToEnd();
                logger.Debug("Json input: " + jsonInput);
                Request request = JsonHelper.JsonDeserialize<Request>(jsonInput);

                string userId = request.RequesterUserId;
                string companyId = request.CompanyId;
                string challengeId = request.ChallengeId;

                response = client.SelectGameResultForChallenge(userId, companyId, challengeId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            string returnedJsonString = JsonHelper.JsonSerializer<AnalyticsSelectGameResultResponse>(response);
            context.Response.Write(returnedJsonString);
            logger.Debug("Json output: " + returnedJsonString);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class Request
        {
            public string RequesterUserId { get; set; }
            public string CompanyId { get; set; }
            public string ChallengeId { get; set; }
        }
    }
}