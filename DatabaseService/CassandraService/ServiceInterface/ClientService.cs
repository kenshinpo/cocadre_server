﻿using CassandraService.Entity;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;

namespace CassandraService.ServiceInterface
{
    public class ClientService
    {
        #region Admin
        public UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode)
        {
            return new User().CreateAdmin(companyId, companyTitle, companyLogoUrl, adminUserId, plainPassword, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode);
        }
        #endregion

        #region Preload Data
        public void WriteExpAToTable()
        {
            AnalyticQuiz analytic = new AnalyticQuiz();
            analytic.WriteExpAToTable();
        }

        public void AddCountryToTable()
        {
            new Country().AddCountryToTable();
        }

        public void AddCountryIpToTable()
        {
            new Country().AddCountryIpToTable();
        }

        public void AddTimezoneToTable()
        {
            new Country().AddTimezoneToTable();
        }

        public ServiceResponse SeedCompanyImageTable()
        {
            // this method selects all companies from 'company' table
            // for each company, each company image type's url is retrieved
            // each url is added to 'company_image' table, where one image type represents a single row
            // e.g. as each company has 8 image types, each company will create 8 rows in 'company_image' table

            return new Company().SeedCompanyImageTable();
        }
        #endregion

        #region Device Token
        public UserUpdateDeviceTokenResponse UpdateUserDeviceToken(string userId,
                                                                   string companyId,
                                                                   string deviceToken,
                                                                   int deviceType)
        {
            User user = new User();
            return user.UpdateUserDeviceToken(userId, companyId, deviceToken, deviceType);
        }
        #endregion

        #region Authentication
#warning Deprecated code in AuthenticateUserForLogin, replaced by AuthenticateEmailForLogin
        public AuthenticationSelectUserResponse AuthenticateUserForLogin(string email,
                                                                         string encryptedPassword)
        {
            Authenticator authenticator = new Authenticator();
            return authenticator.SelectAuthenticatedUser(email, encryptedPassword);
        }

        public AuthenticationSelectUserResponse AuthenticateEmailForLogin(string email,
                                                                          string encryptedPassword)
        {
            return new Authenticator().SelectAuthenticatedEmailWithAllCompanies(email, encryptedPassword);
        }

        public AuthenticationSelectUserByCompanyResponse AuthenticateLoginByCompany(string loginCompanyId,
                                                                           string loginUserId,
                                                                           string currentAuthenticationToken,
                                                                           string encryptedPassword)
        {
            return new Authenticator().AuthenticateLoginByCompany(loginCompanyId, loginUserId, currentAuthenticationToken, encryptedPassword);
        }

#warning Deprecated code in SelectUserWithTokenAndCompany, replaced by SelectUserWithCompany
        public UserSelectTokenWithCompanyResponse SelectUserTokenWithCompany(string userId,
                                                                             string companyId)
        {
            User user = new User();
            return user.SelectUserWithTokenAndCompany(userId, companyId);
        }

        public UserSelectWithCompanyResponse SelectUserWithCompany(string userId, string companyId)
        {
            return new User().SelectUserWithCompany(userId, companyId);
        }

        public UserSelectProfileResponse UpdateForClient(String companyId, String userId, String firstName, String lastName, String email, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, int? gender, DateTime? birthday)
        {
            return new User().UpdateForClient(companyId, userId, firstName, lastName, email, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentTitle, gender, birthday);
        }

        public AuthenticationUpdateResponse UpdatePasswordForUser(string requesterUserId, string email, string companyId, string newPassword)
        {
            return new Authenticator().UpdatePasswordForUser(requesterUserId, email, companyId, newPassword);
        }

        public AuthenticationUpdateResponse ResetPasswordForEmail(string email)
        {
            return new Authenticator().ResetPasswordForEmail(email);
        }

        public UserTokenValidityResponse CheckUserTokenValidity(string userToken, string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return new Authenticator().CheckUserTokenValidity(userToken);
            }
            return new Authenticator().CheckUserTokenValidity(userToken, userId);
        }

        public AuthenticationUpdateResponse ResetPasswordWithToken(string token)
        {
            return new Authenticator().ResetPasswordWithToken(token);
        }

        public AuthenticationSelectCompanyResponse SwitchCompanyByUser(string requesterUserId, string fromCompanyId)
        {
            return new Authenticator().SwitchCompanyByUser(requesterUserId, fromCompanyId);
        }

        #endregion

        #region Colleagues
        public UserSelectAllByDepartmentResponse SelectAllUsersByDepartment(string requesterUserId, string companyId)
        {
            User user = new User();
            return user.SelectAllUsersSortedByDepartment(requesterUserId, companyId);
        }
        #endregion

        #region DAU
        public void UpdateUserActivity(string userId,
                                       string companyId,
                                       bool isActive)
        {
            AnalyticUserActivity analytic = new AnalyticUserActivity();
            analytic.UpdateUserActivity(userId, companyId, isActive);
        }

        public UserUpdateLoginResponse UpdateUserLogin(string userId,
                                                       string companyId,
                                                       bool isLogin)
        {
            AnalyticUserActivity analytic = new AnalyticUserActivity();
            return analytic.UpdateUserLogin(userId, companyId, isLogin);
        }

        public AnalyticsUpdateDailyActiveUserResponse UpdateDailyActiveUser(DateTime today)
        {
            return new AnalyticDau().UpdateDailyActiveUser(today);
        }

        public AnalyticsSelectBasicResponse SelectSummaryReport(string adminUserId, string companyId)
        {
            return new AnalyticDau().SelectSummaryReport(adminUserId, companyId);
        }

        public AnalyticsSelectDetailDauResponse SelectDau(string adminUserId, string companyId, DateTime startUtcDate, DateTime endUtcDate)
        {
            return new AnalyticDau().SelectDau(adminUserId, companyId, startUtcDate, endUtcDate);
        }
        #endregion

        #region Brain
        public BrainSelectResponse SelectBrain(string requesterUserId, string companyId)
        {
            return new Brain().SelectBrain(requesterUserId, companyId);
        }

        public BrainSelectResponse SelectBrainy(string requesterUserId, string companyId)
        {
            return new Brain().SelectBrainy(requesterUserId, companyId);
        }

        public UserSelectStatsResponse SelectStatsForUser(string requesterUserId, string companyId)
        {
            return new AnalyticQuiz().SelectStatsForUser(requesterUserId, companyId);
        }
        #endregion

        #region Feed
        public FeedAuthenticationResponse AuthenticateUserForPostingFeed(string companyId,
                                                                         string userId)
        {
            Feed feed = new Feed();
            FeedAuthenticationResponse response = feed.AuthenticateUserForPostingFeed(companyId,
                                                                                      userId);


            return response;
        }

        public FeedCreateResponse CreateFeedTextPost(string feedId,
                                                     string creatorUserId,
                                                     string companyId,
                                                     string content,
                                                     List<string> targetedDepartmentIds,
                                                     List<string> targetedUserIds,
                                                     List<string> targetedGroupIds,
                                                     bool isForAdmin,
                                                     bool isSpecificNotification = false)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedTextPost(feedId,
                                                                  creatorUserId,
                                                                  companyId,
                                                                  content,
                                                                  targetedDepartmentIds,
                                                                  targetedUserIds,
                                                                  targetedGroupIds,
                                                                  isForAdmin,
                                                                  isSpecificNotification);


            return response;
        }

        public FeedCreateResponse CreateFeedImagePost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      List<string> imageUrls,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      List<string> targetedGroupIds,
                                                      bool isForAdmin,
                                                      bool isSpecificNotification = false)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedImagePost(feedId,
                                                                    creatorUserId,
                                                                    companyId,
                                                                    caption,
                                                                    imageUrls,
                                                                    targetedDepartmentIds,
                                                                    targetedUserIds,
                                                                    targetedGroupIds,
                                                                    isForAdmin,
                                                                    isSpecificNotification);


            return response;
        }

        public FeedCreateResponse CreateFeedVideoPost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      string videoUrl,
                                                      string videoThumbnailUrl,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      List<string> targetedGroupIds,
                                                      bool isForAdmin,
                                                      bool isSpecificNotification = false)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedVideoPost(feedId,
                                                                    creatorUserId,
                                                                    companyId,
                                                                    caption,
                                                                    videoUrl,
                                                                    videoThumbnailUrl,
                                                                    targetedDepartmentIds,
                                                                    targetedUserIds,
                                                                    targetedGroupIds,
                                                                    isForAdmin,
                                                                    isSpecificNotification);

            return response;
        }

        public FeedCreateResponse CreateFeedSharedUrlPost(string feedId,
                                                          string creatorUserId,
                                                          string companyId,
                                                          string caption,
                                                          string url,
                                                          string urlTitle,
                                                          string urlDescription,
                                                          string urlSiteName,
                                                          string urlImageUrl,
                                                          List<string> targetedDepartmentIds,
                                                          List<string> targetedUserIds,
                                                          List<string> targetedGroupIds,
                                                          bool isForAdmin,
                                                          bool isSpecificNotification = false)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedSharedUrlPost(feedId,
                                                                       creatorUserId,
                                                                       companyId,
                                                                       caption,
                                                                       url,
                                                                       urlTitle,
                                                                       urlDescription,
                                                                       urlSiteName,
                                                                       urlImageUrl,
                                                                       targetedDepartmentIds,
                                                                       targetedUserIds,
                                                                       targetedGroupIds,
                                                                       isForAdmin,
                                                                       isSpecificNotification);

            return response;
        }

        public CommentCreateResponse CreateCommentText(string creatorUserId,
                                                       string companyId,
                                                       string feedId,
                                                       string content)
        {
            Feed feed = new Feed();
            CommentCreateResponse response = feed.CreateCommentText(creatorUserId,
                                                                     companyId,
                                                                     feedId,
                                                                     content);

            return response;
        }

        public ReplyCreateResponse CreateReplyText(string creatorUserId,
                                                   string companyId,
                                                   string feedId,
                                                   string commentId,
                                                   string content)
        {
            Feed feed = new Feed();
            ReplyCreateResponse response = feed.CreateReplyText(creatorUserId,
                                                                companyId,
                                                                feedId,
                                                                commentId,
                                                                content);

            return response;
        }


        public FeedSelectResponse SelectCompanyFeedPost(string requesterUserId,
                                                        string companyId,
                                                        string searchContent,
                                                        int numberOfPostsLoaded,
                                                        DateTime? newestTimestamp,
                                                        DateTime? oldestTimestamp)
        {
            Feed feed = new Feed();
            FeedSelectResponse response = feed.SelectCompanyFeedPost(requesterUserId,
                                                                      companyId,
                                                                      searchContent,
                                                                      numberOfPostsLoaded,
                                                                      newestTimestamp,
                                                                      oldestTimestamp);

            return response;
        }

        public FeedSelectResponse SelectCompanyFeedPostWithSearch(string requesterUserId,
                                                                  string companyId,
                                                                  string searchContent,
                                                                  string searchPersonnel,
                                                                  string searchHashTag,
                                                                  int numberOfPostsLoaded,
                                                                  DateTime? newestTimestamp,
                                                                  DateTime? oldestTimestamp)
        {
            Feed feed = new Feed();
            FeedSelectResponse response = feed.SelectCompanyFeedPostWithSearch(requesterUserId,
                                                                               companyId,
                                                                               searchContent,
                                                                               searchPersonnel,
                                                                               searchHashTag,
                                                                               numberOfPostsLoaded,
                                                                               newestTimestamp,
                                                                               oldestTimestamp);
            return response;
        }

        public FeedSelectResponse SelectPersonnelFeedPost(string requesterUserId,
                                                          string ownerUserId,
                                                          string companyId,
                                                          string searchContent,
                                                          DateTime? newestTimestamp,
                                                          DateTime? oldestTimestamp)
        {
            Feed feed = new Feed();
            FeedSelectResponse response = feed.SelectPersonnelFeedPost(requesterUserId,
                                                                        ownerUserId,
                                                                        companyId,
                                                                        searchContent,
                                                                        newestTimestamp,
                                                                        oldestTimestamp);

            return response;
        }

        public CommentSelectResponse SelectFeedComment(string requesterUserId,
                                                       string feedId,
                                                       string companyId)
        {
            Feed feed = new Feed();
            CommentSelectResponse response = feed.SelectFeedComment(requesterUserId,
                                                                     feedId,
                                                                     companyId);

            return response;
        }

        public ReplySelectResponse SelectCommentReply(string requesterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId)
        {
            Feed feed = new Feed();
            ReplySelectResponse response = feed.SelectCommentReply(requesterUserId,
                                                                    feedId,
                                                                    commentId,
                                                                    companyId);

            return response;
        }

        public PointUpdateResponse UpdateFeedPoint(string voterUserId,
                                                   string feedId,
                                                   string companyId,
                                                   bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateFeedPoint(voterUserId,
                                                                feedId,
                                                                companyId,
                                                                isUpVote);

            return response;

        }

        public PointUpdateResponse UpdateCommentPoint(string voterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId,
                                                      bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateCommentPoint(voterUserId,
                                                                   feedId,
                                                                   commentId,
                                                                   companyId,
                                                                   isUpVote);

            return response;
        }

        public PointUpdateResponse UpdateReplyPoint(string voterUserId,
                                                    string feedId,
                                                    string commentId,
                                                    string replyId,
                                                    string companyId,
                                                    bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateReplyPoint(voterUserId,
                                                                  feedId,
                                                                  commentId,
                                                                  replyId,
                                                                  companyId,
                                                                  isUpVote);

            return response;
        }

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId)
        {
            return new Feed().DeleteFeed(feedId, companyId, ownerUserId, null);
        }

        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId)
        {
            return new Feed().DeleteComment(feedId, commentId, companyId, commentorUserId, null);
        }

        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId)
        {
            return new Feed().DeleteReply(feedId, commentId, replyId, companyId, commentorUserId, null);
        }

        public FeedSelectPrivacyResponse SelectFeedPrivacy(string requesterUserId,
                                                           string companyId,
                                                           string feedId)
        {
            return new Feed().SelectFeedPrivacy(requesterUserId, companyId, feedId);
        }

        public FeedUpdateResponse UpdateToFeedTextPost(string ownerUserId,
                                                       string companyId,
                                                       string currentFeedId,
                                                       string content,
                                                       List<string> targetedDepartmentIds,
                                                       List<string> targetedUserIds,
                                                       List<string> targetedGroupIds,
                                                       bool isSpecificNotification = false)
        {
            return new Feed().UpdateToFeedTextPost(ownerUserId, companyId, currentFeedId, content, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);
        }

        public FeedUpdateResponse UpdateToFeedImagePost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> updatedImageUrls,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds,
                                                        List<string> targetedGroupIds,
                                                        bool isSpecificNotification = false)
        {

            return new Feed().UpdateToFeedImagePost(ownerUserId, companyId, currentFeedId, caption, updatedImageUrls, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);
        }

        public FeedUpdateResponse EditToFeedImagePost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<Image> updatedImages,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds,
                                                        List<string> targetedGroupIds,
                                                        bool isSpecificNotification = false)
        {

            return new Feed().EditToFeedImagePost(ownerUserId, companyId, currentFeedId, caption, updatedImages, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);
        }

        public FeedUpdateResponse UpdateToFeedVideoPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds,
                                                        List<string> targetedGroupIds,
                                                        bool isSpecificNotification = false)
        {
            return new Feed().UpdateToFeedVideoPost(ownerUserId, companyId, currentFeedId, caption, targetedDepartmentIds, targetedUserIds, targetedGroupIds, isSpecificNotification);
        }

        public FeedUpdateResponse UpdateToSharedUrlPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds,
                                                        List<string> targetedGroupIds,
                                                        bool isSpecificNotification = false)
        {
            return new Feed().UpdateToSharedUrlPost(ownerUserId, companyId, currentFeedId, caption, targetedUserIds, targetedUserIds, targetedGroupIds, isSpecificNotification);
        }

        public FeedSelectUpVotersResponse SelectUpVoters(string requesterUserId, string companyId, string feedId, string commentId, string replyId)
        {
            return new Feed().SelectUpVoters(requesterUserId, companyId, feedId, commentId, replyId);
        }

        public FeedSelectAllHashTagResponse SelectAllHashTags(string companyId, string requesterUserId)
        {
            return new Feed().SelectAllHashTags(companyId, requesterUserId);
        }

        public FeedSearchHashTagResponse SearchHashTag(string companyId, string requesterUserId, string startsWith)
        {
            return new Feed().SearchHashTag(companyId, requesterUserId, startsWith);
        }

        public FeedUpdateImpressionResponse UpdateFeedImpression(string requesterUserId, string companyId, string feedId)
        {
            return new Feed().UpdateImpression(requesterUserId, companyId, feedId);
        }
        #endregion

        #region Progresses
        #region Feed Progresses
        public FeedImageProgressResponse CreateFeedImageProgress(
                        string feedId,
                        string imageName,
                        double progress,
                        bool completed,
                        bool uploaded,
                        int expectedCount,
                        string environment,
                        string companyId,
                        int feedType = -1,
                        string targetedDepartmentIds = null,
                        string targetedGroupIds = null,
                        string targetedUserIds = null,
                        string caption = null,
                        bool isSpecificNotification = false,
                        bool isForAdmin = false,
                        bool isVideoThumbnail = false,
                        bool isEdit = false,
                        int ordering = 0,
                        string userId = null,
                        string url = null,
                        string sharedUrl = null,
                        string urlTitle = null,
                        string urlDescription = null,
                        string urlSiteName = null)
        {
            // insert
            return new Feed().CreateFeedImageProgress(feedId, imageName, progress, completed, uploaded, expectedCount, environment, companyId, feedType, targetedDepartmentIds, targetedGroupIds, targetedUserIds, caption, isSpecificNotification, isForAdmin, isVideoThumbnail, isEdit, ordering, userId, url, sharedUrl, urlTitle, urlDescription, urlSiteName);
        }

        public FeedImageProgressesResponse SelectFeedImageProgresses(string feedId)
        {
            // select
            return new Feed().SelectFeedImageProgresses(feedId);
        }

        public FeedImageProgressResponse SelectFeedImageProgress(string feedId, string imageName)
        {
            // select
            return new Feed().SelectFeedImageProgress(feedId, imageName);
        }

        public FeedImageProgressResponse UpdateFeedImageProgress(FeedImageProgress feedImageProgress)
        {
            return new Feed().UpdateFeedImageProgress(feedImageProgress);
        }

        public FeedImageProgressResponse DeleteFeedImageProgress(FeedImageProgress feedImageProgress)
        {
            return new Feed().DeleteFeedImageProgress(feedImageProgress);
        }

        public FeedVideoProgressResponse CreateFeedVideoProgress(
                        string feedId,
                        string playlistName,
                        double progress,
                        bool completed,
                        bool uploaded,
                        string environment,
                        string companyId,
                        int feedType = -1,
                        string s3SrcKey = null,
                        string s3OutputKeyPrefix = null,
                        string targetedDepartmentIds = null,
                        string targetedGroupIds = null,
                        string targetedUserIds = null,
                        string caption = null,
                        bool isSpecificNotification = false,
                        bool isForAdmin = false,
                        string userId = null)
        {
            // insert
            return new Feed().CreateFeedVideoProgress(feedId, playlistName, progress, completed, uploaded, environment, companyId, feedType, s3SrcKey, s3OutputKeyPrefix, targetedDepartmentIds, targetedGroupIds, targetedUserIds, caption, isSpecificNotification, isForAdmin, userId);
        }

        public FeedVideoProgressResponse SelectFeedVideoProgress(string feedId)
        {
            // select
            return new Feed().SelectFeedVideoProgress(feedId);
        }

        public FeedVideoProgressResponse UpdateFeedVideoProgress(FeedVideoProgress feedVideoProgress)
        {
            return new Feed().UpdateFeedVideoProgress(feedVideoProgress);
        }
        #endregion
        #region User Progresses
        public UserImageProgressResponse CreateUserImageProgress(
                        string userId,
                        string imageName,
                        double progress,
                        bool completed,
                        bool uploaded,
                        string environment,
                        string companyId)
        {
            // insert
            return new User().CreateUserImageProgress(userId, imageName, progress, completed, uploaded, environment,
                companyId);
        }

        public UserImageProgressResponse SelectUserImageProgress(string userId, string imageName)
        {
            return new User().SelectUserImageProgress(userId, imageName);
        }

        public UserImageProgressResponse UpdateUserImageProgress(UserImageProgress userImageProgress)
        {
            return new User().UpdateUserImageProgress(userImageProgress);
        }
        #endregion
        #endregion

        #region Challenge
        public ChallengeInvalidateResponse InvalidateChallenge(string userId, string companyId, string challengeId, int reason)
        {
            return new Topic().InvalidateChallenge(userId, companyId, challengeId, reason);
        }

        public CategorySelectAllWithTopicResponse SelectAllTopicBasicByUserAndCategory(string requesterUserId,
                                                                                       string companyId)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByUserAndCategory(requesterUserId, companyId);
        }

        public CategorySelectAllWithTopicResponse SelectTopicsWithRank(string requesterUserId,
                                                                       string companyId)
        {
            return new Topic().SelectTopicsWithRank(requesterUserId, companyId);
        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByOpponent(string initiatorUserId,
                                                                         string challengedUserId,
                                                                         string companyId,
                                                                         string topicStartsWithName)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByOpponent(initiatorUserId, challengedUserId, companyId, topicStartsWithName);
        }

        public UserSelectAllBasicResponse SelectAllRecentlyPlayedUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId)
        {
            return new User().SelectAllRecentlyPlayedUsersByTopicId(topicId, categoryId, requesterUserId, companyId);
        }

        public UserSelectBasicResponse SelectRandomOpponentForTopicId(string topicId, string categoryId, string requesterUserId, string companyId)
        {
            return new User().SelectRandomOpponentForTopicId(topicId, categoryId, requesterUserId, companyId);
        }

        public UserSelectAllBasicResponse SelectAllUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId, string startsWithName)
        {
            User user = new User();
            return user.SelectAllUsersByTopicId(topicId, categoryId, requesterUserId, companyId, startsWithName);
        }

        public ChallengeCreateResponse CreateChallengeWithTopicId(string companyId,
                                                                  string initiatorUserId,
                                                                  string challengedUserId,
                                                                  string topicId,
                                                                  string categoryId)
        {
            Topic topic = new Topic();
            return topic.CreateChallengeWithTopicId(companyId, initiatorUserId, challengedUserId, topicId, categoryId);
        }

        public QuestionSelectAllResponse SelectAllQuestionsForChallenge(string requesterUserId,
                                                                        string companyId,
                                                                        string challengeId)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.SelectAllQuestionsForChallenge(requesterUserId, companyId, challengeId);
        }

        public QuestionAnswerResponse AnswerChallengeQuestion(string challengeId,
                                                              string companyId,
                                                              string userId,
                                                              string answer,
                                                              float timeTaken,
                                                              string questionId,
                                                              string topicId,
                                                              int round)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.AnswerChallengeQuestion(challengeId,
                                                    companyId,
                                                    userId,
                                                    answer,
                                                    timeTaken,
                                                    questionId,
                                                    topicId,
                                                    round);
        }

        public ChallengeSelectResponse SelectChallengeWithChallengeId(string companyId,
                                                                      string challengeId,
                                                                      string requesterUserId)
        {
            Topic topic = new Topic();
            return topic.SelectChallengeWithChallengeId(companyId, challengeId, requesterUserId);
        }

        public ChallengeStartWithoutOpponentResponse StartChallengeWithoutOpponent(string challengeId, string requesterUserId, string companyId)
        {
            Topic topic = new Topic();
            return topic.StartChallengeWithoutOpponent(challengeId, requesterUserId, companyId);
        }

        public ChallengeOfflineSelectOpponentAnswerResponse SelectOpponentAnswerForOfflineGame(string challengeId, string requesterUserId, string companyId, int round)
        {
            Topic topic = new Topic();
            return topic.SelectOpponentAnswerForOfflineGame(challengeId, requesterUserId, companyId, round);
        }

        public ChallengeIsReadyResponse SetPlayerReadyForChallenge(string requesterUserId,
                                                                   string companyId,
                                                                   string challengeId)
        {
            Topic topic = new Topic();
            return topic.SetPlayerReadyForChallenge(requesterUserId, companyId, challengeId);
        }

        public ChallengeSelectIncompleteResponse SelectIncompleteChallengesForUser(string requesterUserId, string companyId)
        {
            return new Brain().SelectIncompleteChallengesForUser(requesterUserId, companyId);
        }
        #endregion

        #region Responsive Survey
        public RSTopicSelectResponse SelectRSTopicByUser(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().SelectRSTopicByUser(requesterUserId, companyId, topicId, categoryId);
        }


        public RSCardAnswerResponse AnswerCard(string answeredByUserId, string companyId, string topicId, string topicCategoryId, string cardId, int rangeSelected, List<string> optionIds, RSOption customAnswer, bool isSkipped)
        {
            return new RSCard().AnswerCard(answeredByUserId, companyId, topicId, topicCategoryId, cardId, rangeSelected, optionIds, customAnswer, isSkipped);
        }

        public RSUpdateActivityResponse UpdateBounceActivity(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().UpdateBounceActivity(requesterUserId, companyId, topicId, categoryId);
        }

        public RSUpdateActivityResponse UpdateCompletionActivity(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().UpdateCompletionActivity(requesterUserId, companyId, topicId, categoryId);
        }

        public RSTopicUpdateFeedbackResponse CreateFeedback(string userId, string companyId, string topicId, string topicCategoryId, string feedback)
        {
            return new RSTopic().CreateFeedback(userId, companyId, topicId, topicCategoryId, feedback);
        }

        #endregion

        #region Leaderboard
        public AnalyticsSelectLeaderboardByCompanyResponse SelectLeaderboardByCompany(string requesterUserId, string companyId)
        {
            AnalyticQuiz analytic = new AnalyticQuiz();
            return analytic.SelectLeaderboardByCompany(requesterUserId, companyId);
        }

        public AnalyticsSelectLeaderboardByDepartmentResponse SelectLeaderboardByDepartment(string requesterUserId, string companyId)
        {
            AnalyticQuiz analytic = new AnalyticQuiz();
            return analytic.SelectLeaderboardByDepartment(requesterUserId, companyId);
        }
        #endregion

        #region Challenge stats
        public ChallengeSelectStatsResponse SelectStatsForChallenge(string topicId, string requesterUserId, string opponentUserId, string companyId)
        {
            AnalyticQuiz analytic = new AnalyticQuiz();
            return analytic.SelectStatsForChallenge(topicId, requesterUserId, opponentUserId, companyId);
        }

        public AnalyticsSelectAllGameHistoriesResponse SelectAllGameHistoriesForUser(string requesterUserId,
                                                                                string companyId,
                                                                                int numberOfHistoryLoaded,
                                                                                DateTime? newestTimestamp,
                                                                                DateTime? oldestTimestamp)
        {
            AnalyticQuiz analytic = new AnalyticQuiz();
            return analytic.SelectAllGameHistoriesForUser(requesterUserId, companyId, numberOfHistoryLoaded, newestTimestamp, oldestTimestamp);
        }

        public AnalyticsSelectGameResultResponse SelectGameResultForChallenge(string requesterUserId,
                                                                              string companyId,
                                                                              string challengeId)
        {
            AnalyticQuiz analytic = new AnalyticQuiz();
            return analytic.SelectGameResultForChallenge(requesterUserId, companyId, challengeId);
        }
        #endregion

        #region Notification
        public NotificationSelectAllResponse SelectNotificationByUser(string requesterUserId,
                                                                   string companyId,
                                                                   int numberOfPostsLoaded,
                                                                   DateTime? newestTimestamp,
                                                                   DateTime? oldestTimestamp)
        {
            return new Notification().SelectNotificationsByUser(requesterUserId, companyId, numberOfPostsLoaded, newestTimestamp, oldestTimestamp);
        }

        public NotificationSelectNumberResponse SelectNotificationNumberByUser(string currentUserId,
                                                                               string companyId)
        {
            return new Notification().SelectNotificationNumberByUser(currentUserId, companyId, null);
        }

        public NotificationUpdateSeenResponse UpdateSeenNotification(string currentUserId,
                                                                     string companyId,
                                                                     string notificationId)
        {
            return new Notification().UpdateSeenNotification(currentUserId, companyId, notificationId);
        }
        #endregion

        #region Dashboard
        public DashboardReportResponse ReportFeed(string reportedUserId, string reason, string companyId, string feedId, string commentId, string replyId, int reportType)
        {
            return new Dashboard().ReportFeed(reportedUserId, reason, companyId, feedId, commentId, replyId, reportType);
        }

        public DashboardSelectFeedbackResponse SelectFeedbackFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectFeedbackFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardSelectReportedFeedResponse SelectReportedFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectReportedFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardCreateFeedbackCommentResponse CreateFeedbackComment(string adminUserId,
                                                                            string companyId,
                                                                            string feedId,
                                                                            string content)
        {
            return new Dashboard().CreateFeedbackComment(adminUserId, companyId, feedId, content);
        }

        public UserUploadProfileImageResponse UploadProfileImage(string uploaderUserId, string companyId, string uploadedImageUrl, int approvalState)
        {
            return new User().UploadProfileImage(uploaderUserId, null, companyId, uploadedImageUrl, approvalState);
        }
        #endregion

        #region Event
        public EventSelectResponse SelectEventDetail(string requesterUserId,
                                                     string eventId,
                                                     string companyId)
        {
            return new Event().SelectEventByClient(requesterUserId, eventId, companyId, null, false);
        }
        #endregion

        #region App Setting
        public SelectAppSettingResponse SelectAppSetting(string requesterUserId, string companyId)
        {
            return new Setting().SelectAppSetting(requesterUserId, companyId);
        }

        public SelectAppNotificationResponse SelectNotificationSetting(string requesterUserId, string companyId)
        {
            return new Setting().SelectNotificationSetting(requesterUserId, companyId);
        }

        public UpdateAppSettingResponse UpdateAppSoundEffect(string requesterUserId, string companyId, bool isOn)
        {
            return new Setting().UpdateAppSoundEffect(requesterUserId, companyId, isOn);
        }

        public UpdateAppSettingResponse UpdateInGameMusic(string requesterUserId, string companyId, bool isOn)
        {
            return new Setting().UpdateInGameMusic(requesterUserId, companyId, isOn);
        }

        public UpdateAppSettingResponse UpdateNotification(string requesterUserId, string companyId, int gameNotification, int eventNotification, int feedNotification)
        {
            return new Setting().UpdateNotification(requesterUserId, companyId, gameNotification, eventNotification, feedNotification);
        }
        #endregion

        #region TestLoad
        public UserSelectWithCompanyResponse TestWithoutLoop(string userId, string companyId)
        {
            return new TestLoad().TestWithoutLoop(userId, companyId);
        }

        public CategorySelectAllWithTopicResponse TestWithLoop(string requesterUserId,
                                                               string companyId)
        {
            return new TestLoad().TestWithLoop(requesterUserId, companyId);
        }

        public BrainSelectResponse TestWithMultipleKeyspaces(string requesterUserId, string companyId)
        {
            return new TestLoad().TestWithMultipleKeyspaces(requesterUserId, companyId);
        }
        #endregion

        #region Pulse
        public PulseSelectAllResponse SelectAllByUser(string requesterUserId, string companyId)
        {
            return new Pulse().SelectAllByUser(requesterUserId, companyId);
        }

        public PulseUpdateSingleStatsResponse UpdatePulseSingleAction(string requesterUserId, string companyId, string pulseId, int action)
        {
            return new PulseSingle().UpdateSingleAction(requesterUserId, companyId, pulseId, action);
        }

        public PulseUpdateImpressionResponse UpdatePulseImpression(string requesterUserId, string companyId, string pulseId)
        {
            return new Pulse().UpdateImpression(requesterUserId, companyId, pulseId);
        }

        public PulseSelectAllResponse SelectFromInbox(string requesterUserId, string companyId, DateTime? lastDrawnTimestamp)
        {
            return new Pulse().SelectFromInbox(requesterUserId, companyId, lastDrawnTimestamp);
        }

        public PulseSelectAllResponse SelectFromTrashbin(string requesterUserId, string companyId, DateTime? lastDrawnTimestamp)
        {
            return new Pulse().SelectFromTrashbin(requesterUserId, companyId, lastDrawnTimestamp);
        }

        public PulseUpdateResponse MoveToTrashBin(string pulseId, int pulseType, string requesterUserId, string companyId)
        {
            return new Pulse().MoveToTrashBin(pulseId, pulseType, requesterUserId, companyId);
        }

        public PulseUpdateResponse RestoreToInbox(string pulseId, int pulseType, string requesterUserId, string companyId)
        {
            return new Pulse().RestoreToInbox(pulseId, pulseType, requesterUserId, companyId);
        }

        public PulseUpdateResponse RemoveFromTrashBin(string pulseId, int pulseType, string requesterUserId, string companyId)
        {
            return new Pulse().RemoveFromTrashBin(pulseId, pulseType, requesterUserId, companyId);
        }

        public PulseAnswerCardResponse AnswerCard(string answeredByUserId, string companyId, string deckId, string pulseId, int selectedRange = -1, string selectedOptionId = null, string customAnswer = null)
        {
            return new PulseDynamic().AnswerCard(answeredByUserId, companyId, deckId, pulseId, selectedRange, selectedOptionId, customAnswer);
        }

        public PulseSelectResponse SelectPulse(string requesterUserId, string companyId, string pulseId, int pulseType)
        {
            return new Pulse().SelectPulse(requesterUserId, companyId, pulseId, pulseType);
        }
        #endregion

        #region User
        public UserSelectProfileResponse SelectProfile(string requesterUserId, string targetedUserId, string companyId)
        {
            return new User().SelectProfile(requesterUserId, targetedUserId, companyId);
        }
        #endregion

        #region Country
        public CountryListResponse GetCountriesForPhone()
        {
            return new Country().GetCountriesForPhone();
        }
        #endregion

        #region Assessment
        public AssessmentSelectResponse SelectAssessmentByUser(string requesterUserId, string companyId, string assessmentId)
        {
            return new Assessment().SelectAssessmentByUser(requesterUserId, companyId, assessmentId, (int)Assessment.AssessmentQueryType.Basic);
        }

        public AssessmentSelectResponse TakeAssessmentByUser(string requesterUserId, string companyId, string assessmentId)
        {
            return new Assessment().SelectAssessmentByUser(requesterUserId, companyId, assessmentId, (int)Assessment.AssessmentQueryType.FullDetail);
        }

        public AssessmentAnswerCardResponse AnswerAssessmentCard(string answeredByUserId, string companyId, string assessmentId, string cardId, List<AssessmentCardOption> options)
        {
            return new AssessmentCard().AnswerAssessmentCard(answeredByUserId, companyId, assessmentId, cardId, options);
        }

        public AssessmentSelectResultByUserResponse SelectAssessmentResultByUser(string requesterUserId, string companyId, string assessmentId)
        {
            return new Assessment().SelectAssessmentResultByUser(requesterUserId, companyId, assessmentId);
        }

        public AssessmentSelectResultByUserResponse SelectColorBrainResultByUser(string requesterUserId, string targetedUserId, string companyId, string assessmentId)
        {
            return new AssessmentColoredBrain().SelectColorBrainResultByUser(requesterUserId, targetedUserId, companyId, assessmentId);
        }
        #endregion

        #region Group
        public GroupCreateResponse CreateGroup(string requesterUserId, string companyId, string groupName, List<string> groupMemberIds)
        {
            return new Group().CreateGroup(requesterUserId, companyId, groupName, groupMemberIds);
        }

        public GroupSelectResponse SelectGroup(string requesterUserId, string companyId, string groupId)
        {
            return new Group().SelectGroup(requesterUserId, companyId, groupId, (int)Group.GroupQueryType.FullDetail);
        }

        public GroupSelectAllResponse SelectAllGroupsByUser(string requesterUserId, string companyId)
        {
            return new Group().SelectAllGroupsByUser(requesterUserId, companyId);
        }

        public GroupUpdateResponse DeleteGroup(string requesterUserId, string companyId, string groupId)
        {
            return new Group().DeleteGroup(requesterUserId, companyId, groupId);
        }

        public GroupUpdateResponse UpdateGroup(string requesterUserId, string companyId, string groupId, string newGroupName, List<string> newGroupMemberIds)
        {
            return new Group().UpdateGroup(requesterUserId, companyId, groupId, newGroupName, newGroupMemberIds);
        }
        #endregion

        #region Mobile Learning
        public MLTopicSelectAllByCategoryResponse SelectAllMLTopicByUser(string requesterUserId, string companyId)
        {
            return new MLTopic().SelectAllBasicByUser(requesterUserId, companyId, true);
        }
        public MLTopicSelectResponse SelectMLTopicByUser(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new MLTopic().SelectMLTopicByUser(requesterUserId, companyId, topicId, categoryId);
        }

        public MLCardAnswerResponse AnswerMLCard(string answeredByUserId, string companyId, string topicId, string topicCategoryId, string cardId, List<string> optionIds)
        {
            return new MLCard().AnswerCard(answeredByUserId, companyId, topicId, topicCategoryId, cardId, optionIds);
        }

        public MLTopicUpdateFeedbackResponse CreateMLFeedback(string userId, string companyId, string topicId, string topicCategoryId, string feedback)
        {
            return new MLTopic().CreateFeedback(userId, companyId, topicId, topicCategoryId, feedback);
        }

        public MLSelectUserResultResponse SelectMLUserResult(string answeredByUserId, string companyId, string topicId, string topicCategoryId)
        {
            return new MLTopic().SelectUserResult(answeredByUserId, companyId, topicId, topicCategoryId);
        }

        public MLEducationDetailResponse SelectMLEducationByUser(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new MLEducation().GetDetailByUser(requesterUserId, companyId, topicId, categoryId);
        }

        public MLEducationCardAnswerResponse AnswerEducationCard(string answeredByUserId, string companyId, string educationId, string categoryId, string cardId, List<string> optionIds)
        {
            return new MLEducationCard().AnswerCard(answeredByUserId, companyId, educationId, categoryId, cardId, optionIds);
        }

        public MLEducationCompletedResponse CompletedEducation(string requesterUserId, string companyId, string educationId, string categoryId)
        {
            return new MLEducation().CompletedEducation(requesterUserId, companyId, educationId, categoryId);
        }

        public MLEducationListResponse GetEducationListByUser(string requesterUserId, string companyId, string containsName = null, string categoryId = null)
        {
            return new MLEducation().GetListByUser(requesterUserId, companyId, containsName, categoryId);
        }

        public MLEducationCategoryListResponse GetEducationCategoryListByUser(string requesterUserId, string companyId, string containsName = null)
        {
            return new MLEducationCategory().GetListByUser(requesterUserId, companyId, containsName);
        }
        #endregion

        #region Achievement
        public GamificationSelectAllAchievementResponse SelectAllAchievements(string requesterUserId, string companyId)
        {
            return new Gamification().SelectAllAchievementsByUser(requesterUserId, companyId);
        }

        public GamificationSelectAchievementResponse SelectAchievement(string requesterUserId, string companyId, string achievementId)
        {
            return new Gamification().SelectAchievementByUser(requesterUserId, companyId, achievementId);
        }

        public GamificationUpdateBadgeResponse UpdateBadge(string achievementId, string requesterUserId, string companyId, bool isRemoveBadge = false)
        {
            return new Gamification().UpdateBadge(achievementId, requesterUserId, companyId, isRemoveBadge);
        }

        public GamificationUpdateBadgeSeenResponse UpdateSeenBadge(string achievementId, string requesterUserId, string companyId)
        {
            return new Gamification().UpdateSeenBadge(achievementId, requesterUserId, companyId);
        }
        #endregion

        #region Appraisal 360
        public AppraisalSelectAllTemplatesResponse SelectAppraisalTemplates(string requesterUserId, string companyId)
        {
            return new Appraisal().SelectTemplatesByUser(requesterUserId, companyId);
        }

        public AppraisalSelectTemplateResponse SelectAppraisalTemplateCards(string requesterUserId, string companyId, string appraisalId)
        {
            return new Appraisal().SelectFullTemplateByUser(requesterUserId, companyId, appraisalId);
        }

        public AppraisalCategorySelectAllResponse SelectCreatedCategoriesForTemplate(string companyId, string creatorUserId, string templateAppraisalId)
        {
            return new AppraisalCategory().SelectUserDefinedCategoriesByAppraisal(companyId, creatorUserId, templateAppraisalId);
        }

        public AppraisalCardCreateTemplateResponse CreateAppraisalCardForTemplate(string creatorUserId, string companyId, string templateAppraisalId, string categoryId, string categoryTitle, string cardContent)
        {
            return new AppraisalCard().CreateTemplateCardByUser(creatorUserId, companyId, templateAppraisalId, categoryId, categoryTitle, cardContent);
        }

        public AppraisalCategorySelectResponse SelectCreatedApprasialCardsByCategory(string companyId, string creatorUserId, string templateAppraisalId, string categoryId)
        {
            return new AppraisalCategory().SelectUserDefinedCategoryByAppraisal(companyId, creatorUserId, templateAppraisalId, categoryId);
        }

        public AppraisalCreateResponse CreateCustomAppraisal(string creatorUserId,
                                                            string companyId,
                                                            string templateAppraisalId,
                                                            string title,
                                                            string description,
                                                            bool isSelfAssessmentNeeded,
                                                            bool isAnonymous,
                                                            int teamType,
                                                            List<string> participantIds,
                                                            DateTime startDate,
                                                            DateTime endDate,
                                                            List<AppraisalCard> cards)
        {
            return new Appraisal().CreateCustomAppraisal(creatorUserId, companyId, templateAppraisalId, title, description, isSelfAssessmentNeeded, isAnonymous, teamType, participantIds, startDate, endDate, cards);
        }

        public AppraisalSelectResponse SelectFullCustomAppraisalByUser(string requesterUserId, string companyId, string appraisalId)
        {
            return new Appraisal().SelectFullCustomAppraisalByUser(requesterUserId, companyId, appraisalId);
        }

        public AppraisalSelectResponse SelectCreatedCustomAppraisalByUser(string requesterUserId, string companyId, string appraisalId)
        {
            return new Appraisal().SelectFullCustomAppraisalByUser(requesterUserId, companyId, appraisalId, true);
        }

        public AppraisalSelectAllResponse SelectCreatedAppraisalsByCreator(string creatorUserId, string companyId)
        {
            return new Appraisal().SelectAppraisalsByCreator(creatorUserId, companyId);
        }

        public AppraisalAnswerCardResponse AnswerCard(string answeredByUserId, string companyId, string appraisalId, string cardId, int selectedRange = 0, int selectedOption = 0, string comment = null)
        {
            return new AppraisalCard().AnswerCard(answeredByUserId, companyId, appraisalId, cardId, selectedRange, selectedOption, comment);
        }

        public AppraisalSelectResponse SelectAppraisalResult(string createdByUserId, string companyId, string appraisalId)
        {
            return new Appraisal().SelectAppraisalResult(createdByUserId, companyId, appraisalId);
        }

        public AppraisalUpdateCategoryResponse UpdateTemplateCategoryTitleByUser(string requesterUserId, string companyId, string templateAppraisalId, string categoryId, string updatedTitle)
        {
            return new AppraisalCategory().UpdateTemplateCategoryTitleByUser(companyId, requesterUserId, templateAppraisalId, categoryId, updatedTitle);
        }

        public AppraisalUpdateCardResponse UpdateTemplateAppraisalCardByUser(string requesterUserId, string companyId, string templateAppraisalId, string categoryId, string cardId, string newCardContent)
        {
            return new AppraisalCard().UpdateTemplateCardByUser(requesterUserId, companyId, templateAppraisalId, categoryId, cardId, newCardContent);
        }

        public AppraisalUpdateCardResponse RemoveTemplateAppraisalCardByUser(string requesterUserId, string companyId, string templateAppraisalId, string categoryId, string cardId)
        {
            return new AppraisalCard().RemoveTemplateCardByUser(requesterUserId, companyId, templateAppraisalId, categoryId, cardId);
        }

        public AppraisalUpdateResponse UpdateCustomAppraisal(string appraisalId,
                                                             string creatorUserId,
                                                             string companyId,
                                                             string templateAppraisalId,
                                                             string newTitle,
                                                             string newDescription,
                                                             bool newIsSelfAssessmentNeeded,
                                                             bool newIsAnonymous,
                                                             int newTeamType,
                                                             List<string> newParticipantIds,
                                                             DateTime newStartDate,
                                                             DateTime newEndDate,
                                                             List<AppraisalCard> newCards)
        {
            return new Appraisal().UpdateCustomAppraisal(appraisalId, creatorUserId, companyId, templateAppraisalId, newTitle, newDescription, newIsSelfAssessmentNeeded, newIsAnonymous, newTeamType, newParticipantIds, newStartDate, newEndDate, newCards);
        }

        public AppraisalExportResponse ExportAppraisalResult(string createdByUserId, string companyId, string appraisalId)
        {
            return new Appraisal().ExportAppraisalResult(createdByUserId, companyId, appraisalId);
        }
        #endregion
    }
}
