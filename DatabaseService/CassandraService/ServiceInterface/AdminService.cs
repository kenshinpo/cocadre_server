﻿using CassandraService.Entity;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace CassandraService.ServiceInterface
{
    public class AdminService
    {
        Authenticator authenticator = new Authenticator();
        Department department = new Department();
        User user = new User();
        Country country = new Country();
        AccountKind accountKind = new AccountKind();

        #region Authenticator
        public AuthenticationUpdateResponse UpdatePasswordForManager(string userId, string email, string companyId, string newPassword, string currentPassword = null, int accountType = User.AccountType.CODE_NORMAL_USER)
        {
            return new Authenticator().UpdatePasswordForUser(userId, email, companyId, newPassword, currentPassword, accountType);
        }

        public AuthenticationUpdateResponse ResetPasswordForEmail(string email)
        {
            return new Authenticator().ResetPasswordForEmail(email);
        }

        public AuthenticationCheckEmailForCompanyResponse CheckEmailExistsForCompany(string adminUserId, string companyId, string email)
        {
            return new Authenticator().CheckEmailExistsForCompany(adminUserId, companyId, email);
        }

        public AuthenticationUpdateResponse ResetPasswordForUser(string adminUserId, string companyId, string userId)
        {
            return new Authenticator().ResetPasswordForUser(adminUserId, companyId, userId);
        }

        public AuthenticationUpdateResponse ReinviteUser(string adminUserId, string companyId, string userId)
        {
            return new Authenticator().ReinviteUser(adminUserId, companyId, userId);
        }

        public AuthenticationUpdateResponse ReinviteUsers(string adminUserId, string companyId)
        {
            return new Authenticator().ReinviteUsers(adminUserId, companyId);
        }
        #endregion

        #region For Company
        public GetCompanyPermissionResponse GetCompanyPermission(string companyId, string adminUserId)
        {
            return new Company().GetCompanyPermission(companyId, adminUserId);
        }

        public CompanyUpdatePermissionResponse UpdateCompanyPermisson(string companyId, string adminUserId, int permisssionType, List<string> allowDepartmentId)
        {
            return new Company().UpdateCompanyPermisson(companyId, adminUserId, permisssionType, allowDepartmentId);
        }

        public CompanyUpdateProfileResponse UpdateProfile(string adminUserId, string companyId, string newTitle, string newPrimaryAdminUserId, string newPrimaryAdminSecondaryEmail, string supportMessage, string zoneName, double timezoneOffset)
        {
            return new Company().UpdateProfile(adminUserId, companyId, newTitle, newPrimaryAdminUserId, newPrimaryAdminSecondaryEmail, supportMessage, zoneName, timezoneOffset);
        }

        public CompanySelectProfileResponse SelectProfile(string adminUserId, string companyId)
        {
            return new Company().SelectProfile(adminUserId, companyId);
        }

        public CompanySelectImageResponse SelectImage(string adminUserId, string companyId, int imageType)
        {
            return new Company().SelectEmailLogo(adminUserId, companyId, imageType);
        }

        public CompanyUpdateProfileResponse UpdateImage(string adminUserId, string companyId, string imageUrl, int imageType)
        {
            return new Company().UpdateImage(adminUserId, companyId, imageUrl, imageType);
        }

        public CompanySelectEmailTemplateResponse SelectEmailTemplate(string adminUserId, string companyId)
        {
            return new Company().SelectEmailTemplate(adminUserId, companyId);
        }

        public CompanyUpdateProfileResponse UpdateEmailTemplate(string adminUserId, string companyId, int updateType, string emailTitle, string emailDescription, string emailSupportInfo)
        {
            return new Company().UpdateEmailTemplate(adminUserId, companyId, updateType, emailTitle, emailDescription, emailSupportInfo);
        }

        public CompanyResponse GetCompany(String companyId)
        {
            return new Company().GetCompany(companyId);
        }

        public CompanyImageResponse CreateCompanyImage(
            string companyId,
            string url,
            int companyImageType,
            string adminUserId,
            //string prevSelectedCompanyImageId,
            bool isSelected = true)
        {
            return new Company().CreateCompanyImage(companyId, url, companyImageType, adminUserId, /*prevSelectedCompanyImageId,*/ isSelected);
        }

        public CompanyImageResponse SetSelectedCompanyImage(string companyId, string companyImageId, string adminUserId, int companyImageType)
        {
            return new Company().SetSelectedCompanyImage(companyId, companyImageId, adminUserId, companyImageType);
        }

        public CompanyImageResponse DeleteCompanyImage(string companyId, string companyImageId, string adminUserId, int companyImageType)
        {
            return new Company().DeleteCompanyImage(companyId, companyImageId, adminUserId, companyImageType);
        }

        public CompanyImagesResponse GetCompanyImagesDesc(string companyId, string adminUserId, int companyImageType, DateTime? timestampValue, int limit, bool isNextPage = true)
        {
            CompanyImagesResponse response = new CompanyImagesResponse();
            List<CompanyImageDesc> companyImages = new List<CompanyImageDesc>();

            CompanyImageResponse inUseResponse = new Company().GetInUseCompanyImage(companyId, adminUserId, companyImageType);
            if (inUseResponse.CompanyImage != null)
            {
                #region Get in use image
                CompanyImageDesc inUseImage = new CompanyImageDesc
                {
                    CompanyId = inUseResponse.CompanyImage.CompanyId,
                    CompanyImageId = inUseResponse.CompanyImage.CompanyImageId,
                    CompanyImageType = inUseResponse.CompanyImage.CompanyImageType,
                    CreatedByUserId = inUseResponse.CompanyImage.CreatedByUserId,
                    CreatedOnTimestamp = inUseResponse.CompanyImage.CreatedOnTimestamp,
                    IsSelected = inUseResponse.CompanyImage.IsSelected,
                    LastModifiedByUserId = inUseResponse.CompanyImage.LastModifiedByUserId,
                    LastModifiedTimestamp = inUseResponse.CompanyImage.LastModifiedTimestamp,
                    Url = inUseResponse.CompanyImage.Url
                };
                companyImages.Add(inUseImage);
                #endregion

                #region others images
                List<CompanyImageDesc> otherImages = new Company().GetCompanyImagesDesc(companyId, adminUserId, companyImageType, limit, timestampValue, isNextPage).CompanyImages.ToList();
                for (int i = 0; i < otherImages.Count; i++)
                {
                    if (inUseImage.CompanyImageId != otherImages[i].CompanyImageId)
                    {
                        companyImages.Add(otherImages[i]);
                    }

                    if (companyImages.Count == limit)
                    {
                        break;
                    }
                }

                response.CompanyImages = companyImages;
                #endregion
            }
            response.Success = true;
            return response;
        }

        public GetCompanyImageDetailResponse GetCompanyImageDetail(string companyId, string adminUserId, string imageId, int companyImageType)
        {
            return new Company().GetCompanyImageDetail(companyId, adminUserId, imageId, companyImageType);
        }

        public UpdateCopmaniesSetting UpdateAllCompaniesS3CorsSetting()
        {
            return new Company().UpdateAllCompaniesS3CorsSetting();
        }

        #endregion

        #region Preload Data
        public void WriteExpAToTable()
        {
            AnalyticQuiz analytic = new AnalyticQuiz();
            analytic.WriteExpAToTable();
        }

        public void AddCountryToTable()
        {
            new Country().AddCountryToTable();
        }

        public void AddCountryIpToTable()
        {
            new Country().AddCountryIpToTable();
        }

        public void AddTimezoneToTable()
        {
            new Country().AddTimezoneToTable();
        }
        #endregion

        #region Authenticator
        public LoginByManagerResponse LoginByManager(String email, String password)
        {
            return authenticator.LoginByManager(email, password);
        }

        public GetManagerInfoResponse GetManagerInfo(String managerId, String companyId)
        {
            return new Authenticator().GetManagerInfo(managerId, companyId);
        }

        public AccountKindListResponse GetAllAccountKind(String adminUserId, String companyId)
        {
            return accountKind.GetAllAccountKind(adminUserId, companyId);
        }

        public AccountKindListResponse GetAllAccountKind(String adminUserId, String companyId, Cassandra.ISession session)
        {
            return accountKind.GetAllAccountKind(adminUserId, companyId, session);
        }

        #endregion

        #region Department
        public DepartmentDetailResponse GetDepartmentDetail(String adminUserId, String companyId, String departmentId)
        {
            return department.GetDepartmentDetail(adminUserId, companyId, departmentId, Department.QUERY_TYPE_DETAIL);
        }

        public DepartmentListResponse GetAllDepartment(String adminUserId, String companyId)
        {
            return department.GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_DETAIL);
        }

        public DepartmentListResponse GetAllDepartment(String adminUserId, String companyId, Cassandra.ISession session)
        {
            return department.GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_DETAIL, session);
        }

        public DepartmentCreateResponse CreateDepartment(String adminUserId, String companyId, String departmentTitle)
        {
            return department.Create(adminUserId, companyId, departmentTitle);
        }

        public DepartmentUpdateResponse UpdateDepartment(String adminUserId, String companyId, String departmentId, String departmentTitle)
        {
            return department.Update(adminUserId, companyId, departmentId, departmentTitle);
        }

        public DepartmentDeleteResponse DeleteDepartment(String adminUserId, String companyId, String departmentId)
        {
            return department.Delete(adminUserId, companyId, departmentId);
        }
        #endregion

        #region For User
        public UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode)
        {
            return new User().CreateAdmin(companyId, companyTitle, companyLogoUrl, adminUserId, plainPassword, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode);
        }
        public UserListResponse GetAllUser(String adminUserId, String companyId, String departmentId, int userTypeCode, int userStatusCode, List<String> searchKeys, bool isPendingInvite, bool isPendingLogin)
        {
            return user.GetAllUserForAdmin(adminUserId, companyId, departmentId, userTypeCode, userStatusCode, searchKeys, isPendingInvite, isPendingLogin);
        }

        public UserListResponse GetAdmin(String adminUserId, String companyId)
        {
            return user.GetAdmin(adminUserId, companyId);
        }

        public UserDetailResponse GetUserDetail(String adminUserId, String companyId, String userId)
        {
            return user.GetUserDetail(adminUserId, companyId, userId);
        }

        public UserCreateResponse CreateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, string jobTitle, int gender, DateTime? birthday, bool isSendEmail = true, string plainPassword = null)
        {
            UserCreateResponse response = user.Create(adminUserId, companyId, userId, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentTitle, jobTitle, gender, birthday, isSendEmail, plainPassword);

            return response;
        }

        public UserUpdateResponse UpdateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, String jobTitle, int gender, DateTime? birthday)
        {
            return user.Update(adminUserId, companyId, userId, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentTitle, jobTitle, gender, birthday);
        }

        public UserUpdateStatusResponse UpdateUserStatus(String adminUserId, String companyId, String userId, int statusCode)
        {
            return user.UpdateStatus(adminUserId, companyId, userId, statusCode);
        }

        public UserUpdateTypeResponse UpdateUserType(String adminUserId, String companyId, String userId, int typeCode, List<int> accessModulesKey, DateTime? expiryDate)
        {
            return user.UpdateType(adminUserId, companyId, userId, typeCode, accessModulesKey, expiryDate);
        }

        public UserDeleteResponse Delete(string adminUserId, string companyId, string userId)
        {
            return user.Delete(adminUserId, companyId, userId);
        }

        public PostingSuspendedUserListResponse GetAllPostingSuspendedUser(String adminUserId, String companyId)
        {
            return new User().GetAllPostingSuspendedUser(adminUserId, companyId);
        }

        public UserListResponse SearchUser(String adminUserId, String companyId, String searchKey)
        {
            return new User().SearchUser(adminUserId, companyId, searchKey);
        }

        public UserListResponse SearchUserForPostingSuspend(String adminUserId, String companyId, String searchKey)
        {
            return new User().SearchUserForPostingSuspend(adminUserId, companyId, searchKey);
        }

        public PostingPermissionSuspendUserResponse PostingPermissionSuspendUsers(String adminUserId, String companyId, List<String> userIds)
        {
            return new User().PostingPermissionSuspendUsers(adminUserId, companyId, userIds);
        }

        public PostingPermissionSuspendUserResponse PostingPermissionSuspendUser(String adminUserId, String companyId, String userId)
        {
            return new User().PostingPermissionSuspendUser(adminUserId, companyId, userId);
        }

        public PostingPermissionRestoreUserResponse PostingPermissionResotreUser(String adminUserId, String companyId, String userId)
        {
            return new User().PostingPermissionRestoreUser(adminUserId, companyId, userId);
        }

        public ModeratorChangeRightsResponse ModeratorChangeRights(String adminUserId, String companyId, String userId, List<int> accessModulesKey, DateTime? expiryDate)
        {
            return new User().ModeratorChangeRights(adminUserId, companyId, userId, accessModulesKey, expiryDate);
        }

        public UserIdResponse GetUserId(string email, string company_id)
        {
            return user.GetUserId(email, company_id);
        }

        public BasicUserInfoResponse GetBasicUserInfo(string userId)
        {
            return user.GetBasicUserInfo(userId);
        }

        public UserAccountTypeResponse GetUserAccountType(string userId)
        {
            return user.GetUserAccountType(userId);
        }

        public ModeratorAccessRightsResponse GetModeratorAccessRights(String userId)
        {
            return user.GetModeratorAccessRights(userId);
        }

        public UserDetailResponse GetUserLastLoginDateTime(string companyId, string userId)
        {
            return new User().GetUserLastLoginDateTime(companyId, userId);
        }

        public AuthenticationUpdateResponse ForgotPassword(string email)
        {
            return new Authenticator().ResetPasswordForEmail(email);
        }

        public AuthenticationLogoutUserResponse LogoutUser(string userId, string companyId)
        {
            return new Authenticator().LogoutUser(userId, companyId);
        }

        public CompanySelectAllJobResponse SelectJobs(string companyId, string adminUserId)
        {
            return new Company().SelectJobs(companyId, adminUserId);
        }

        public CompanySelectAllJobResponse SelectJobsPersonnel(string companyId, string adminUserId)
        {
            return new Company().SelectJobs(companyId, adminUserId, (int)Company.CompanyJobQueryType.FullDetail);
        }
        #endregion

        #region Country
        public CountryListResponse GetAllCountry(String adminUserId, String companyId)
        {
            return country.GetAllCountry(adminUserId, companyId);
        }

        public CountryListResponse UpdateCountries()
        {
            return country.UpdateCountries();
        }
        #endregion

        #region Feed

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId, string adminUserId)
        {
            return new Feed().DeleteFeed(feedId, companyId, ownerUserId, adminUserId);
        }
        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId, string adminUserId)
        {
            return new Feed().DeleteComment(feedId, commentId, companyId, commentorUserId, adminUserId);
        }
        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId, string adminUserId)
        {
            return new Feed().DeleteReply(feedId, commentId, replyId, companyId, commentorUserId, adminUserId);
        }

        public FeedSelectAllHashTagResponse SelectAllFeedRecommendedHashTags(string adminUserId, string companyId)
        {
            return new Feed().SelectAllFeedRecommendedHashTags(adminUserId, companyId, 0);
        }

        public FeedCreateHashTagResponse CreateFeedRecommendedHashTag(string adminUserId, string companyId, string hashTag)
        {
            return new Feed().CreateFeedRecommendedHashTag(adminUserId, companyId, hashTag);
        }

        public FeedUpdateHashTagResponse UpdateFeedRecommendedHashTagOrdering(string adminUserId, string companyId, List<string> hashTags)
        {
            return new Feed().UpdateFeedRecommendedHashTagOrdering(adminUserId, companyId, hashTags);
        }

        public FeedUpdateHashTagResponse UpdateFeedRecommendedHashTag(string adminUserId, string companyId, string oldHashTag, string newHashTag)
        {
            return new Feed().UpdateFeedRecommendedHashTag(adminUserId, companyId, oldHashTag, newHashTag);
        }

        public FeedUpdateHashTagResponse DeleteFeedRecommendedHashTag(string adminUserId, string companyId, string oldHashTag)
        {
            return new Feed().DeleteFeedRecommendedHashTag(adminUserId, companyId, oldHashTag);
        }

        #endregion

        #region Challenge

        public CategorySelectResponse GetCategoryDetail(string adminUserId, string companyId, string categoryId)
        {
            return new TopicCategory().SelectCategory(categoryId, companyId, adminUserId);
        }

        public TopicSelectIconResponse SelectAllTopicIcons(string adminUserId,
                                                           string companyId)
        {
            return new Topic().SelectAllTopicIcons(adminUserId, companyId);
        }

        public CategoryCreateResponse CreateCategory(String adminUserId, String companyId, String categoryTitle)
        {
            return new TopicCategory().Create(adminUserId, companyId, categoryTitle);
        }

        public CategoryUpdateResponse UpdateCategory(String adminUserId, String companyId, String categoryId, String categoryTitle)
        {
            return new TopicCategory().Update(adminUserId, companyId, categoryId, categoryTitle);
        }

        public CategoryDeleteResponse DeleteCategory(String adminUserId, String companyId, String categoryId)
        {
            return new TopicCategory().Delete(adminUserId, companyId, categoryId);
        }

        public List<Topic.TopicStatus> SelectTopicStatusForDropdown()
        {
            return new Topic().SelectTopicStatusForDropdown();
        }

        public List<ChallengeQuestion.QuestionStatus> SelectQuestionStatusForDropdown()
        {
            return new ChallengeQuestion().SelectQuestionStatusForDropdown();
        }

        public List<Topic.DropdownNumberOfQuestions> SelectNumberOfQuestionsForDropdown()
        {
            return new Topic().SelectNumberOfQuestionsForDropdown();
        }

        public CategorySelectAllResponse SelectAllCategories(string adminUserId, string companyId)
        {
            TopicCategory category = new TopicCategory();

            return category.SelectAllCategories(adminUserId, companyId);
        }

        public CategorySelectAllResponse SelectAllCategoriesForDropdown(string adminUserId, string companyId)
        {
            TopicCategory category = new TopicCategory();

            return category.SelectAllCategoriesForDropdown(adminUserId, companyId);
        }

        public TopicCreateResponse CreateTopic(string adminUserId,
                                               string companyId,
                                               string topicTitle,
                                               string topicLogoUrl,
                                               string topicDescription,
                                               string categoryId,
                                               string categoryTitle,
                                               List<string> targetedDepartmentIds,
                                               int numberOfSelectedQuestions,
                                               bool isForEveryone)
        {
            Topic topic = new Topic();
            return topic.CreateTopic(adminUserId, companyId, topicTitle, topicLogoUrl, topicDescription, categoryId, categoryTitle, targetedDepartmentIds, numberOfSelectedQuestions, isForEveryone);
        }

        public QuestionCreateResponse CreateNewQuestion(string adminUserId,
                                                        string companyId,
                                                        string questionId,
                                                        string topicId,
                                                        string categoryId,
                                                        int questionType,
                                                        string questionContent,
                                                        string questionContentImageUrl,
                                                        string questionContentImageMd5,
                                                        string questionContentImageBackgroundColorCode,
                                                        int choiceType,
                                                        string firstChoiceContent,
                                                        string firstChoiceImageUrl,
                                                        string firstChoiceImageMd5,
                                                        string secondChoiceContent,
                                                        string secondChoiceImageUrl,
                                                        string secondChoiceImageMd5,
                                                        string thirdChoiceContent,
                                                        string thirdChoiceImageUrl,
                                                        string thirdChoiceImageMd5,
                                                        string fourthChoiceContent,
                                                        string fourthChoiceImageUrl,
                                                        string fourthChoiceImageMd5,
                                                        float timeAssignedForReading,
                                                        float timeAssignedForAnswering,
                                                        int difficultyLevel,
                                                        int scoreMultiplier,
                                                        double frequency,
                                                        int questionStatus)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.CreateNewQuestion(adminUserId,
                                             companyId,
                                             questionId,
                                             topicId,
                                             categoryId,
                                             questionType,
                                             questionContent,
                                             questionContentImageUrl,
                                             questionContentImageMd5,
                                             questionContentImageBackgroundColorCode,
                                             choiceType,
                                             firstChoiceContent,
                                             firstChoiceImageUrl,
                                             firstChoiceImageMd5,
                                             secondChoiceContent,
                                             secondChoiceImageUrl,
                                             secondChoiceImageMd5,
                                             thirdChoiceContent,
                                             thirdChoiceImageUrl,
                                             thirdChoiceImageMd5,
                                             fourthChoiceContent,
                                             fourthChoiceImageUrl,
                                             fourthChoiceImageMd5,
                                             timeAssignedForReading,
                                             timeAssignedForAnswering,
                                             difficultyLevel,
                                             scoreMultiplier,
                                             frequency,
                                             questionStatus);
        }

        public TopicSelectAllBasicResponse SelectTopicBasicByCategoryAndDepartment(string adminUserId,
                                                                                   string companyId,
                                                                                   string selectedTopicCategoryId,
                                                                                   string selectedDepartmentId,
                                                                                   string containsName = null)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, selectedTopicCategoryId, selectedDepartmentId, containsName);
        }


        public TopicSelectResponse SelectTopicDetail(string topicId,
                                                     string adminUserId,
                                                     string companyId,
                                                     string searchQuestionContent = null)
        {
            Topic topic = new Topic();
            return topic.SelectTopicDetail(topicId,
                                           adminUserId,
                                           companyId,
                                           searchQuestionContent);
        }

        public QuestionSelectResponse SelectQuestion(string questionId, string topicId, string companyId, string adminUserId)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.SelectQuestion(questionId,
                                           topicId,
                                           companyId,
                                           adminUserId);
        }

        public TopicUpdateResponse UpdateTopicStatus(string adminUserId,
                                                     string companyId,
                                                     string topicId,
                                                     string categoryId,
                                                     int status)
        {
            Topic topic = new Topic();
            return topic.UpdateTopicStatus(adminUserId,
                                           companyId,
                                           topicId,
                                           categoryId,
                                           status);
        }

        public QuestionUpdateResponse UpdateQuestionStatus(string adminUserId,
                                                           string companyId,
                                                           string topicId,
                                                           string categoryId,
                                                           string questionId,
                                                           int status)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.UpdateQuestionStatus(adminUserId, companyId, topicId, categoryId, questionId, status);
        }

        public TopicUpdateResponse UpdateTopic(string adminUserId,
                                               string companyId,
                                               string topicId,
                                               string newTitle,
                                               string newLogoUrl,
                                               string newDescription,
                                               string newCategoryId,
                                               string newCategoryTitle,
                                               int newStatus,
                                               List<string> newTargetedDepartmentIds,
                                               int newNumberOfSelectedQuestions,
                                               bool isForEveryone)
        {
            Topic topic = new Topic();
            return topic.UpdateTopic(adminUserId, companyId, topicId, newTitle, newLogoUrl, newDescription, newCategoryId, newCategoryTitle, newStatus, newTargetedDepartmentIds, newNumberOfSelectedQuestions, isForEveryone);
        }

        public QuestionUpdateResponse UpdateQuestion(string adminUserId,
                                                    string companyId,
                                                    string questionId,
                                                    string topicId,
                                                    string categoryId,
                                                    int questionType,
                                                    string questionContent,
                                                    string questionContentImageUrl,
                                                    string questionContentImageMd5,
                                                    string questionContentImageBackgroundColorCode,
                                                    int choiceType,
                                                    string firstChoiceContent,
                                                    string firstChoiceImageUrl,
                                                    string firstChoiceImageMd5,
                                                    string secondChoiceContent,
                                                    string secondChoiceImageUrl,
                                                    string secondChoiceImageMd5,
                                                    string thirdChoiceContent,
                                                    string thirdChoiceImageUrl,
                                                    string thirdChoiceImageMd5,
                                                    string fourthChoiceContent,
                                                    string fourthChoiceImageUrl,
                                                    string fourthChoiceImageMd5,
                                                    float timeAssignedForReading,
                                                    float timeAssignedForAnswering,
                                                    int difficultyLevel,
                                                    int scoreMultiplier,
                                                    double frequency,
                                                    int questionStatus)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.UpdateQuestion(adminUserId, companyId, questionId, topicId, categoryId, questionType, questionContent, questionContentImageUrl, questionContentImageMd5, questionContentImageBackgroundColorCode, choiceType, firstChoiceContent, firstChoiceImageUrl, firstChoiceImageMd5, secondChoiceContent, secondChoiceImageUrl, secondChoiceImageMd5, thirdChoiceContent, thirdChoiceImageUrl, thirdChoiceImageMd5, fourthChoiceContent, fourthChoiceImageUrl, fourthChoiceImageMd5, timeAssignedForReading, timeAssignedForAnswering, difficultyLevel, scoreMultiplier, frequency, questionStatus);
        }
        #endregion

        #region For Module
        public GetModeratorModulesResponse GetModeratorModules()
        {
            GetModeratorModulesResponse response = new GetModeratorModulesResponse();
            response.Success = true;
            response.Modules = Module.GetModules(User.AccountType.CODE_MODERATER);
            return response;
        }

        public CompanySystemModuleDeleteResponse DeleteSystemModule(string companyId, int moduleId)
        {
            return new Module().DeleteCompanySystemModule(companyId, moduleId);
        }

        public CompanySystemModuleCreateResponse CreateSystemModule(string companyId, int moduleId)
        {
            return new Module().CreateCompanySystemModule(companyId, moduleId);
            //return new Subscription().CreateAssessmentSubscription(adminUserId, companyId, assessmentId, numberOfLicences, durationType, visibleDaysAfterCompletion, numberOfRetake, status, startDate, endDate, targetedDepartmentIds, targetedUserIds);
        }


        #endregion

        #region Dashboard
        public DashboardSelectFeedbackResponse SelectFeedbackFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectFeedbackFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardSelectReportedFeedResponse SelectReportedFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectReportedFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardCreateFeedbackCommentResponse CreateFeedbackComment(string adminUserId,
                                                                            string companyId,
                                                                            string feedId,
                                                                            string content)
        {
            return new Dashboard().CreateFeedbackComment(adminUserId, companyId, feedId, content);
        }

        public DashboardUpdateFeedbackStatusResponse UpdateFeedbackPostStatus(string adminUserId, string companyId, string feedId, int status)
        {
            return new Dashboard().UpdateFeedbackPostStatus(adminUserId, companyId, feedId, status);
        }

        public DashboardSelectApprovalProfileResponse SelectProfileApprovalResponse(string adminUserId, string companyId)
        {
            return new Dashboard().SelectProfileApprovalResponse(adminUserId, companyId);
        }

        public DashboardUpdateApprovalProfileResponse UpdateProfileApprovalStatus(string adminUserId, string companyId, string approvalId, int newApprovalState)
        {
            return new Dashboard().UpdateProfileApprovalStatus(adminUserId, companyId, approvalId, newApprovalState);
        }

        public DashboardUpdateReportStatusResponse UpdateReportStatus(string adminUserId, string companyId, int reportType, string feedId, string reportPostId, int newStatus, string commentId = null, string replyId = null)
        {
            return new Dashboard().UpdateReportStatus(adminUserId, companyId, reportType, feedId, reportPostId, newStatus, commentId, replyId);
        }
        #endregion

        #region DAU
        public AnalyticsSelectBasicResponse SelectBasicReport(string adminUserId, string companyId)
        {
            return new AnalyticDau().SelectSummaryReport(adminUserId, companyId);
        }

        public AnalyticsSelectDetailDauResponse SelectDau(string adminUserId, string companyId, DateTime startUtcDate, DateTime endUtcDate)
        {
            return new AnalyticDau().SelectDau(adminUserId, companyId, startUtcDate, endUtcDate);
        }

        public AnalyticActivityResponse GetActivity(string adminUserId, string companyId, DateTime startDate, DateTime endDate, bool isGroupedByUser = true)
        {
            return new AnalyticUserActivity().GetActivity(adminUserId, companyId, startDate, endDate, isGroupedByUser);
        }
        #endregion

        #region MAU
        public AnalyticMonthlyActiveUserResponse SelectMau(string adminUserId, string companyId, DateTime startDateTime, DateTime endDateTime)
        {
            return new AnalyticUserActivity().GetMonthlyActiveUsers(adminUserId, companyId, startDateTime, endDateTime);
        }
        #endregion

        #region Event
        public EventCreateResponse CreateEvent(string adminUserId,
                                               string companyId,
                                               string title,
                                               string description,
                                               DateTime startTimestamp,
                                               DateTime endTimestamp,
                                               int eventType,
                                               int participantType,
                                               int bannerPrivacyType,
                                               DateTime bannerPrivacyDate,
                                               int scoringType,
                                               int calculationType,
                                               bool isDisplayResult,
                                               int extensionDaysForResult,
                                               int numberOfLeaderboard,
                                               string backgroundImageUrl,
                                               List<string> topicIds,
                                               List<string> departmentIds = null,
                                               List<string> userIds = null)
        {
            return new Event().Create(adminUserId, companyId, title, description, startTimestamp, endTimestamp, eventType, participantType, bannerPrivacyType, bannerPrivacyDate, scoringType, calculationType, isDisplayResult, extensionDaysForResult, numberOfLeaderboard, backgroundImageUrl, topicIds, departmentIds, userIds);
        }

        public EventSelectAllResponse SelectAllEvents(string adminUserId, string companyId, int eventStatus)
        {
            return new Event().SelectAllBasic(adminUserId, companyId, eventStatus);
        }

        public EventSelectResponse SelectEvent(string adminUserId,
                                               string eventId,
                                               string companyId)
        {
            return new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL);
        }

        public EventSelectTopicResponse SelectTopicsForEvent(string adminUserId, string companyId, int eventParticipantType, List<string> selectedUserIds, List<string> selectedDepartmentIds, string containsName = null)
        {
            return new Event().SelectTopicsForEvent(adminUserId, companyId, eventParticipantType, selectedUserIds, selectedDepartmentIds, containsName);
        }

        public EventSelectDepartmentResponse SelectDepartmentsForEvent(string adminUserId, string companyId, List<string> selectedTopicIds)
        {
            return new Event().SelectDepartmentsForEvent(adminUserId, companyId, selectedTopicIds);
        }

        public EventSelectUserResponse SelectUsersForEvent(string adminUserId, string companyId, List<string> selectedTopicIds, string containsName = null)
        {
            return new Event().SelectUsersForEvent(adminUserId, companyId, selectedTopicIds, containsName);
        }

        public EventUpdateResponse UpdateEvent(string eventId,
                                               string adminUserId,
                                               string companyId,
                                               string newTitle,
                                               string newDescription,
                                               DateTime newStartTimestamp,
                                               DateTime newEndTimestamp,
                                               int newEventType,
                                               int newParticipantType,
                                               int newBannerPrivacyType,
                                               DateTime newBannerVisibleDate,
                                               int newScoringType,
                                               int newCalculationType,
                                               bool newIsDisplayResult,
                                               int newExtensionDaysForResult,
                                               int newNumberOfLeaderboard,
                                               string newBackgroundImageUrl,
                                               List<string> newTopicIds,
                                               List<string> newDepartmentIds = null,
                                               List<string> newUserIds = null)
        {
            return new Event().Update(eventId, adminUserId, companyId, newTitle, newDescription, newStartTimestamp, newEndTimestamp, newEventType, newParticipantType, newBannerPrivacyType, newBannerVisibleDate, newScoringType, newCalculationType, newIsDisplayResult, newExtensionDaysForResult, newNumberOfLeaderboard, newBackgroundImageUrl, newTopicIds, newDepartmentIds, newUserIds);
        }

        public EventUpdateResponse SuspendEvent(string adminUserId, string companyId, string eventId)
        {
            return new Event().SuspendEvent(adminUserId, companyId, eventId);
        }

        public EventUpdateResponse RestoreEvent(string adminUserId, string companyId, string eventId)
        {
            return new Event().RestoreEvent(adminUserId, companyId, eventId);
        }

        public EventUpdateResponse DeleteEvent(string adminUserId, string companyId, string eventId)
        {
            return new Event().DeleteEvent(adminUserId, companyId, eventId);
        }

        public AnalyticSelectEventResultResponse SelectEventResult(string adminUserId, string companyId, string eventId)
        {
            return new Event().SelectResult(adminUserId, companyId, eventId);
        }
        #endregion

        #region Responsive Survey
        public RSCategoryCreateResponse CreateRSCategory(string companyId, string title, string adminUserId)
        {
            return new RSTopicCategory().CreateRSCategory(companyId, title, adminUserId);
        }
        public RSCategoryUpdateResponse UpdateRSCategory(string adminUserId, string companyId, string categoryId, string newTitle)
        {
            return new RSTopicCategory().UpdateRSCategory(adminUserId, companyId, categoryId, newTitle);
        }

        public RSCategoryUpdateResponse DeleteRSCategory(string adminUserId, string companyId, string categoryId)
        {
            return new RSTopicCategory().DeleteRSCategory(adminUserId, companyId, categoryId);
        }
        public RSTopicCreateResponse CreateRSTopic(string adminUserId,
                                                   string companyId,
                                                   string title,
                                                   string introduction,
                                                   string closingWords,
                                                   string categoryId,
                                                   string categoryTitle,
                                                   string topicIconUrl,
                                                   int status,
                                                   List<string> targetedDepartmentIds,
                                                   List<string> targetedUserIds,
                                                   DateTime startDate,
                                                   DateTime? endDate,
                                                   int anonymityCount)
        {
            return new RSTopic().CreateRSTopic(adminUserId, companyId, title, introduction, closingWords, categoryId, categoryTitle, topicIconUrl, status, targetedDepartmentIds, targetedUserIds, startDate, endDate, anonymityCount);
        }

        public RSTopicUpdateResponse UpdateRSTopic(string topicId,
                                                 string adminUserId,
                                                 string companyId,
                                                 string newTitle,
                                                 string newIntroduction,
                                                 string newClosingWords,
                                                 string newCategoryId,
                                                 string newCategoryTitle,
                                                 string newTopicIconUrl,
                                                 int newStatus,
                                                 List<string> newTargetedDepartmentIds,
                                                 List<string> newTargetedUserIds,
                                                 DateTime startDate,
                                                 DateTime? endDate,
                                                 int newAnonymityCount)
        {
            return new RSTopic().UpdateTopic(topicId, adminUserId, companyId, newTitle, newIntroduction, newClosingWords, newCategoryId, newCategoryTitle, newTopicIconUrl, newStatus, newTargetedDepartmentIds, newTargetedUserIds, startDate, endDate, newAnonymityCount);
        }

        public RSTopicUpdateResponse UpdateRSTopicStatus(string topicId, string categoryId, string adminUserId, string companyId, int updatedStatus)
        {
            return new RSTopic().UpdateTopicStatus(topicId, categoryId, adminUserId, companyId, updatedStatus);
        }

        public RSCategorySelectAllResponse SelectAllRSCategories(string adminUserId, string companyId, int queryType)
        {
            return new RSTopicCategory().SelectAllRSCategories(adminUserId, companyId, queryType);
        }

        public RSTopicSelectAllBasicResponse SelectAllRSTopicByCategory(string adminUserId, string companyId, string selectedCategoryId, string containsName)
        {
            return new RSTopic().SelectAllBasicByCategory(adminUserId, companyId, selectedCategoryId, containsName);
        }

        public RSCardCreateResponse CreateCard(string cardId,
                                              int type,
                                              string content,
                                              bool hasImage,
                                              List<RSImage> images,
                                              bool toBeSkipped,
                                              bool hasCustomAnswer,
                                              string customAnswerInstruction,
                                              bool isOptionRandomized,
                                              bool allowMultipleLines,
                                              bool hasPageBreak,
                                              int backgroundType,
                                              string note,
                                              string categoryId,
                                              string topicId,
                                              string adminUserId,
                                              string companyId,
                                              List<RSOption> options,
                                              int miniOptionToSelect,
                                              int maxOptionToSelect,
                                              int startRangePosition,
                                              int maxRange,
                                              string maxRangeLabel,
                                              int midRange,
                                              string midRangeLabel,
                                              int minRange,
                                              string minRangeLabel)
        {
            return new RSCard().CreateCard(cardId,
                                           type,
                                           content,
                                           hasImage,
                                           images,
                                           toBeSkipped,
                                           hasCustomAnswer,
                                           customAnswerInstruction,
                                           isOptionRandomized,
                                           allowMultipleLines,
                                           hasPageBreak,
                                           backgroundType,
                                           note,
                                           categoryId,
                                           topicId,
                                           adminUserId,
                                           companyId,
                                           options,
                                           miniOptionToSelect,
                                           maxOptionToSelect,
                                           startRangePosition,
                                           maxRange,
                                           maxRangeLabel,
                                           midRange,
                                           midRangeLabel,
                                           minRange,
                                           minRangeLabel
                                           );
        }

        public RSCardUpdateResponse UpdateCard(string cardId,
                                               int type,
                                               string content,
                                               bool hasImage,
                                               List<RSImage> images,
                                               bool toBeSkipped,
                                               bool hasCustomAnswer,
                                               string customAnswerInstruction,
                                               bool isOptionRandomized,
                                               bool allowMultipleLines,
                                               bool hasPageBreak,
                                               int backgroundType,
                                               string note,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               List<RSOption> options,
                                               int miniOptionToSelect,
                                               int maxOptionToSelect,
                                               int startRangePosition,
                                               int maxRange,
                                               string maxRangeLabel,
                                               int midRange,
                                               string midRangeLabel,
                                               int minRange,
                                               string minRangeLabel)
        {
            return new RSCard().UpdateCard(
                cardId,
                type,
                content,
                hasImage,
                images,
                toBeSkipped,
                hasCustomAnswer,
                customAnswerInstruction,
                isOptionRandomized,
                allowMultipleLines, hasPageBreak, backgroundType, note, categoryId, topicId, adminUserId, companyId, options,
                miniOptionToSelect, maxOptionToSelect,
                startRangePosition,
                maxRange, maxRangeLabel,
                midRange, midRangeLabel,
                minRange, minRangeLabel);
        }

        public RSCardUpdateResponse DeleteCard(string topicId, string categoryId, string cardId, string adminUserId, string companyId)
        {
            return new RSCard().DeleteCard(topicId, categoryId, cardId, adminUserId, companyId);
        }

        public RSTopicSelectResponse SelectFullDetailRSTopic(string adminUserId, string companyId, string topicId, string categoryId, string containsCardName = null)
        {
            return new RSTopic().SelectFullDetailRSTopic(adminUserId, companyId, topicId, categoryId, containsCardName);
        }

        public RSCardSelectResponse SelectFullDetailCard(string adminUserId, string companyId, string cardId, string topicId)
        {
            return new RSCard().SelectCard(adminUserId, companyId, cardId, topicId, (int)RSCard.RSCardQueryType.FullDetail);
        }

        public RSCardSelectAllResponse Preview(string topicId, string categoryId, string adminUserId, string companyId)
        {
            return new RSCard().SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)RSCard.RSCardQueryType.FullDetail);
        }

        public RSCardReorderResponse ReorderCard(string cardId, string topicId, string categoryId, string adminUserId, string companyId, int insertAtOrder)
        {
            return new RSCard().ReorderCard(cardId, topicId, categoryId, adminUserId, companyId, insertAtOrder);
        }

        public RSOptionSelectLogicResponse SelectLogicToNextCard(string topicId, string categoryId, string currentCardId, string adminUserId, string companyId)
        {
            return new RSOption().SelectLogicToNextCard(topicId, categoryId, currentCardId, adminUserId, companyId);
        }

        public AnalyticSelectRSResultOverviewResponse SelectResultOverview(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().SelectResultOverview(adminUserId, companyId, topicId, categoryId);
        }

        public AnalyticSelectRSCardResultResponse SelectCardResult(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSCard().SelectResult(adminUserId, companyId, topicId, categoryId);
        }

        public AnalyticSelectRSCustomAnswersResponse SelectCustomAnswersFromCard(string adminUserId, string companyId, string topicId, string categoryId, string cardId)
        {
            return new RSCard().SelectCustomAnswersFromCard(adminUserId, companyId, topicId, categoryId, cardId);
        }

        public AnalyticSelectRSResponderReportResponse SelectRespondersReport(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().SelectRespondersReport(adminUserId, companyId, topicId, categoryId);
        }

        public AnalyticSelectRSCardResultByUserResponse SelectCardResultByUser(string adminUserId, string companyId, string topicId, string categoryId, string answeredByUserId)
        {
            return new RSCard().SelectResultByUser(adminUserId, companyId, topicId, categoryId, answeredByUserId);
        }

        public AnalyticSelectRSOptionResultResponse SelectOptionResult(string adminUserId, string companyId, string topicId, string categoryId, string cardId, string optionId)
        {
            return new RSOption().SelectResult(adminUserId, companyId, topicId, categoryId, cardId, optionId);
        }

        public RSTopicSelectFeedbackResponse SelectFeedback(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().SelectFeedback(adminUserId, companyId, topicId, categoryId);
        }

        public AnalyticExportRSResultResponse ExportRSResult(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().ExportRSResult(adminUserId, companyId, topicId, categoryId);
        }

        #endregion

        #region Update Script
        public UpdateScriptResponse InsertValidStatusColumnToFeed(string adminUserId, string companyId)
        {
            return new UpdateScript().InsertValidStatusColumnToFeed(adminUserId, companyId);
        }

        public UpdateScriptResponse InsertNewColumnsAndTablesToChallenge()
        {
            return new UpdateScript().InsertNewColumnsAndTablesToChallenge();
        }

        public UpdateScriptResponse InsertTableChallengeByTopic()
        {
            return new UpdateScript().InsertTableChallengeByTopic();
        }

        public UpdateScriptResponse CreateNewTablesForUserToken()
        {
            return new UpdateScript().CreateNewTablesForUserToken();
        }

        public UpdateScriptResponse CreateNewTablesForNotification()
        {
            return new UpdateScript().CreateNewTablesForNotification();
        }

        public UpdateScriptResponse CreateNewColumnsForUser()
        {
            return new UpdateScript().CreateNewColumnsForUser();
        }

        public UpdateScriptResponse CreateNewColumnsForCompany()
        {
            return new UpdateScript().CreateNewColumnsForCompany();
        }

        public UpdateScriptResponse CreateMatchUpActivity()
        {
            return new UpdateScript().CreateMatchUpActivity();
        }

        public UpdateScriptResponse CreateMatchUpTopicAttempt()
        {
            return new UpdateScript().CreateMatchUpTopicAttempt();
        }

        public UpdateScriptResponse CreateMatchUpQuestionAttempt()
        {
            return new UpdateScript().CreateMatchUpQuestionAttempt();
        }

        public UpdateScriptResponse CreateMatchUpOptionAttempt()
        {
            return new UpdateScript().CreateMatchUpOptionAttempt();
        }

        public UpdateScriptResponse UpdateFeedUser()
        {
            return new UpdateScript().UpdateFeedUser();
        }

        public UpdateScriptResponse UpdateSurveyCategory()
        {
            return new UpdateScript().UpdateSurveyCategory();
        }

        public UpdateScriptResponse UpdateSurvey()
        {
            return new UpdateScript().UpdateSurvey();
        }

        public UpdateScriptResponse UpdateSurveyCard()
        {
            return new UpdateScript().UpdateSurveyCard();
        }

        public UpdateScriptResponse UpdateMatchupDifficulty()
        {
            return new UpdateScript().UpdateMatchupDifficulty();
        }

        public UpdateScriptResponse UpdateDynamicPulseCustomized()
        {
            return new UpdateScript().UpdateDynamicPulseCustomized();
        }

        public UpdateScriptResponse UpdateUserEmailLower()
        {
            return new UpdateScript().UpdateUserEmailLower();
        }

        public UpdateScriptResponse UpdatePhoneCountryForUser(string adminUserId, string companyId, string countryCode, string countryName)
        {
            return new UpdateScript().UpdatePhoneCountryForUser(adminUserId, companyId, countryCode, countryName);
        }

        public UpdateScriptResponse UpdateFeedPointByTimestamp()
        {
            return new UpdateScript().UpdateFeedPointByTimestamp();
        }

        public UpdateScriptResponse UpdatePrivacyForFeed()
        {
            return new UpdateScript().UpdatePrivacyForFeed();
        }

        public UpdateScriptResponse UpdateLogin()
        {
            return new UpdateScript().UpdateLogin();
        }

        public UpdateScriptResponse OptimizeNotification()
        {
            return new UpdateScript().OptimizeNotification();
        }

        public UpdateScriptResponse UpdateCompanyJobs()
        {
            return new UpdateScript().UpdateCompanyJob();
        }

        public UpdateScriptResponse UpdateUserLowestJob()
        {
            return new UpdateScript().UpdateUserLowestJob();
        }

        public UpdateScriptResponse UpdateAppraisalCommentRating()
        {
            return new UpdateScript().UpdateAppraisalCommentRating();
        }

        public UpdateScriptResponse UpdateDeletedUserJob()
        {
            return new UpdateScript().UpdateDeletedUserJob();
        }
        #endregion

        #region Fix Bugs
        public FixBugResponse FixSurveyCustomAnswerGrouping(string cardId)
        {
            return new FixBugScript().FixSurveyCustomAnswerGrouping(cardId);
        }

        public FixBugResponse FixSurveyCompletion()
        {
            return new FixBugScript().FixSurveyCompletion();
        }

        public FixBugResponse FixImpressionTally()
        {
            return new FixBugScript().FixImpressionTally();
        }

        public FixBugResponse RemoveAppraisalPulses()
        {
            return new FixBugScript().RemoveAppraisalPulses();
        }

        public FixBugResponse UpdateMissingDau(DateTime date)
        {
            return new FixBugScript().UpdateMissingDau(date);
;       }

        #endregion

        #region Quiz
        public AnalyticSelectMatchUpOverallResponse SelectMatchUpOverall(string adminUserId, string companyId)
        {
            return new Topic().SelectMatchUpOverall(adminUserId, companyId);
        }

        public AnalyticSelectTopicAttemptResponse SelectTopicAttempts(string adminUserId, string companyId, int metric, int limit)
        {
            return new Topic().SelectTopicAttempts(adminUserId, companyId, metric, limit);
        }

        public AnalyticSelectTopicAttemptResponse SelectDailyTopicActivities(string adminUserId, string companyId, string datestampString)
        {
            return new Topic().SelectDailyTopicActivities(adminUserId, companyId, datestampString);
        }

        public AnalyticSelectTopicOverallResponse SelectTopicOverall(string adminUserId, string companyId, string topicId)
        {
            return new Topic().SelectTopicOverall(adminUserId, companyId, topicId);
        }

        public AnalyticSelectQuestionAttemptResponse SelectQuestionAttempts(string adminUserId, string companyId, string topicId, int metric, int limit)
        {
            return new ChallengeQuestion().SelectQuestionAttempts(adminUserId, companyId, topicId, metric, limit);
        }

        public AnalyticSelectOptionAttemptResponse SelectOptionAttempts(string adminUserId, string companyId, string topicId, string questionId)
        {
            return new ChallengeQuestion().SelectOptionAttempts(adminUserId, companyId, topicId, questionId);
        }

        public List<ChallengeQuestion> SelectRandomQuestions(string topicId, string categoryId, string companyId)
        {
            return new Topic().SelectRandomQuestions(topicId, categoryId, companyId);
        }

        public AnalyticMatchUpBreakdownPersonnelReport SelectMatchupTopPersonnelOverview(string adminUserId, string companyId)
        {
            return new AnalyticQuiz().SelectMatchUpBreakdownPersonnelReport(adminUserId, companyId);
        }

        public AnalyticsSelectFullLeaderboardByCompanyResponse SelectFullLeaderboardByCompany(string adminUserId, string companyId)
        {
            return new AnalyticQuiz().SelectFullLeaderboardByCompany(adminUserId, companyId);
        }

        public AnalyticMatchUpGroupPersonnelReport SelectMatchUpGroupPersonnelReport(string adminUserId, string companyId)
        {
            return new AnalyticQuiz().SelectMatchUpGroupPersonnelReport(adminUserId, companyId);
        }

        public AnalyticLoginDetailResponse SelectLoginDetail(string adminUserId, string companyId, DateTime fromUtc, DateTime toUtc)
        {
            return new AnalyticDau().SelectLoginDetail(adminUserId, companyId, fromUtc, toUtc);
        }

        public TopicExportResponse ExportTopic(string managerId, string companyId, string topicId)
        {
            return new Topic().ExportTopic(managerId, companyId, topicId);
        }

        public MatchUpImportQuestionsResponse ImportQuestions(string managerId, string companyId, string topidId, string categoryId, string zipFileBase64String)
        {
            return new Topic().ImportTopicQuestions(managerId, companyId, topidId, categoryId, zipFileBase64String);
        }

        #endregion

        #region CleanUp
        public UserCleanUpResponse RemoveRecentlyDeletedUsers()
        {
            return new User().RemoveRecentlyDeletedUsers();
        }
        #endregion

        #region Gamification
        public GamificationBackupResponse SelectExperienceBackup(string adminUserId, string companyId, List<string> userIds, List<string> departmentIds)
        {
            return new Gamification().SelectExperienceBackup(adminUserId, companyId, userIds, departmentIds);
        }
        public GamificationResetResponse ResetExperience(string adminUserId, string companyId, List<string> userIds)
        {
            return new Gamification().ResetExperience(adminUserId, companyId, userIds);
        }

        public GamificationBackupResponse SelectLeaderboardBackup(string adminUserId, string companyId, List<string> userIds, List<string> departmentIds)
        {
            return new Gamification().SelectLeaderboardBackup(adminUserId, companyId, userIds, departmentIds);
        }

        public GamificationResetResponse ResetLeaderboard(string adminUserId, string companyId, List<string> topicIds)
        {
            return new Gamification().ResetLeaderboard(adminUserId, companyId, topicIds);
        }
        #endregion

        #region Pulse

        public PulseSelectBannerBackgroundResponse SelectBannerBackgroundUrls(string adminUserId, string companyId)
        {
            return new PulseSingle().SelectBannerBackgroundUrls(adminUserId, companyId);
        }

        public PulseCreateResponse CreateBanner(string pulseId,
                                                string adminUserId,
                                                string companyId,
                                                string title,
                                                bool isFilterApplied,
                                                string textColor,
                                                string backgroundUrl,
                                                string description,
                                                int status,
                                                DateTime startDate,
                                                DateTime? endDate,
                                                bool isPrioritized,
                                                List<string> targetedDepartmentIds,
                                                List<string> targetedUserIds)
        {
            return new PulseSingle().CreateBanner(pulseId,
                                            adminUserId,
                                            companyId,
                                            title,
                                            isFilterApplied,
                                            textColor,
                                            backgroundUrl,
                                            description,
                                            status,
                                            startDate,
                                            endDate,
                                            isPrioritized,
                                            targetedDepartmentIds,
                                            targetedUserIds);
        }

        public PulseCreateResponse CreateAnnouncement(string pulseId,
                                                      string adminUserId,
                                                      string companyId,
                                                      string title,
                                                      string description,
                                                      int status,
                                                      DateTime startDate,
                                                      DateTime? endDate,
                                                      bool isPrioritized,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds)
        {
            return new PulseSingle().CreateAnnouncement(pulseId,
                                                  adminUserId,
                                                  companyId,
                                                  title,
                                                  description,
                                                  status,
                                                  startDate,
                                                  endDate,
                                                  isPrioritized,
                                                  targetedDepartmentIds,
                                                  targetedUserIds);
        }

        public PulseSelectAllResponse SelectAllAnnouncementBasic(string adminUserId, string companyId, DateTime? filteredStartDate = null, DateTime? filteredEndDate = null, string containsName = null, int filteredProgress = 0)
        {
            return new PulseSingle().SelectAllSingleBasic(adminUserId, companyId, (int)Pulse.PulseTypeEnum.Announcement, filteredStartDate, filteredEndDate, containsName, filteredProgress);
        }

        public PulseSelectAllResponse SelectAllBannerBasic(string adminUserId, string companyId, DateTime? filteredStartDate = null, DateTime? filteredEndDate = null, string containsName = null, int filteredProgress = 0)
        {
            return new PulseSingle().SelectAllSingleBasic(adminUserId, companyId, (int)Pulse.PulseTypeEnum.Banner, filteredStartDate, filteredEndDate, containsName, filteredProgress);
        }

        public PulseSelectResponse SelectFullDetailAnnouncement(string adminUserId, string companyId, string pulseId)
        {
            return new PulseSingle().SelectFullDetailSinglePulse(adminUserId, companyId, pulseId);
        }

        public PulseSelectResponse SelectFullDetailBanner(string adminUserId, string companyId, string pulseId)
        {
            return new PulseSingle().SelectFullDetailSinglePulse(adminUserId, companyId, pulseId);
        }

        public PulseUpdateResponse DeleteSinglePulse(string adminUserId, string companyId, string pulseId)
        {
            return new PulseSingle().DeleteSinglePulse(adminUserId, companyId, pulseId);
        }

        public PulseUpdateResponse UpdateBanner(string pulseId,
                                                string adminUserId,
                                                string companyId,
                                                string newTitle,
                                                bool newIsFilterApplied,
                                                string newTextColor,
                                                string newBackgroundUrl,
                                                string newDescription,
                                                int newStatus,
                                                DateTime newStartDate,
                                                DateTime? newEndDate,
                                                bool newIsPrioritized,
                                                List<string> newTargetedDepartmentIds,
                                                List<string> newTargetedUserIds)
        {
            return new PulseSingle().UpdateBanner(pulseId, adminUserId, companyId, newTitle, newIsFilterApplied, newTextColor, newBackgroundUrl, newDescription, newStatus, newStartDate, newEndDate, newIsPrioritized, newTargetedDepartmentIds, newTargetedUserIds);
        }

        public PulseUpdateResponse UpdateAnnouncement(string pulseId,
                                                      string adminUserId,
                                                      string companyId,
                                                      string newTitle,
                                                      string newDescription,
                                                      int newStatus,
                                                      DateTime newStartDate,
                                                      DateTime? newEndDate,
                                                      bool newIsPrioritized,
                                                      List<string> newTargetedDepartmentIds,
                                                      List<string> newTargetedUserIds)
        {
            return new PulseSingle().UpdateAnnouncement(pulseId, adminUserId, companyId, newTitle, newDescription, newStatus, newStartDate, newEndDate, newIsPrioritized, newTargetedDepartmentIds, newTargetedUserIds);
        }

        public PulseUpdateResponse UpdatePulseFeedStatus(string adminUserId, string companyId, string pulseId, int updatedStatus)
        {
            return new PulseSingle().UpdatePulseSingleStatus(adminUserId, companyId, pulseId, updatedStatus);
        }

        public PulseSelectSingleAnalyticResponse SelectAnalyticForFeedPulse(string adminUserId, string companyId, string pulseId)
        {
            return new PulseSingle().SelectAnalyticForSinglePulse(adminUserId, companyId, pulseId);
        }

        public PulseDeckCreateResponse CreateDeck(string deckId,
                                                  string adminUserId,
                                                  string companyId,
                                                  string title,
                                                  bool isCompulsory,
                                                  int anonymityCount,
                                                  int publishMethodType,
                                                  int status,
                                                  DateTime startDate,
                                                  DateTime? endDate,
                                                  bool isPrioritized,
                                                  List<string> targetedDepartmentIds,
                                                  List<string> targetedUserIds,
                                                  int numberOfCardsPerTimeFrame = 0,
                                                  int perTimeFrameType = 0,
                                                  int periodFrameType = 0,
                                                  int durationPerCard = 0)
        {
            return new PulseDynamic().CreateDeck(deckId, adminUserId, companyId, title, isCompulsory, anonymityCount, publishMethodType, status, startDate, endDate, isPrioritized, targetedDepartmentIds, targetedUserIds, numberOfCardsPerTimeFrame, perTimeFrameType, periodFrameType, durationPerCard);
        }

        public PulseCreateResponse CreateDynamicCard(string cardId,
                                                     string deckId,
                                                     string adminUserId,
                                                     string companyId,
                                                     string title,
                                                     string description,
                                                     int cardType,
                                                     int questionType,
                                                     DateTime startDate,
                                                     int numberOfOptions = 0,
                                                     int rangeType = 0,
                                                     string imageUrl = null,
                                                     string minRangeLabel = null,
                                                     string maxRangeLabel = null,
                                                     List<DeckCardOption> options = null,
                                                     string parentCardId = null,
                                                     int parentOptionNumber = 0)
        {
            return new PulseDynamic().CreateDynamicCard(cardId, deckId, adminUserId, companyId, title, description, cardType, questionType, startDate, numberOfOptions, rangeType, imageUrl, minRangeLabel, maxRangeLabel, options, parentCardId, parentOptionNumber);
        }

        public PulseSelectAllDecksResponse SelectAllDeckBasic(string adminUserId,
                                                              string companyId,
                                                              DateTime? filteredStartDate = null,
                                                              DateTime? filteredEndDate = null,
                                                              string containsName = null,
                                                              int filteredProgress = 0,
                                                              int filteredType = 0)
        {
            return new PulseDynamic().SelectAllDeckBasic(adminUserId, companyId, filteredStartDate, filteredEndDate, containsName, filteredProgress, filteredType);
        }

        public PulseSelectDeckResponse SelectFullDetailDeck(string adminUserId, string companyId, string deckId, bool isOrderedByAscending, string containsText = null)
        {
            return new PulseDynamic().SelectFullDetailDeck(adminUserId, companyId, deckId, isOrderedByAscending, containsText);
        }

        public PulseUpdateResponse UpdateDeckStatus(string adminUserId, string companyId, string deckId, int updatedStatus)
        {
            return new PulseDynamic().UpdateDeckStatus(adminUserId, companyId, deckId, updatedStatus);
        }

        public PulseUpdateResponse UpdateDeck(string deckId,
                                              string adminUserId,
                                              string companyId,
                                              string newTitle,
                                              bool newIsCompulsory,
                                              int newAnonymityCount,
                                              int newStatus,
                                              DateTime newStartDate,
                                              DateTime? newEndDate,
                                              bool newIsPrioritized,
                                              List<string> newTargetedDepartmentIds,
                                              List<string> newTargetedUserIds,
                                              int newDurationPerCard = 0)
        {
            return new PulseDynamic().UpdateDeck(deckId, adminUserId, companyId, newTitle, newIsCompulsory, newAnonymityCount, newStatus, newStartDate, newEndDate, newIsPrioritized, newTargetedDepartmentIds, newTargetedUserIds, newDurationPerCard);
        }

        public PulseUpdateResponse DeleteDeck(string adminUserId, string companyId, string deckId)
        {
            return new PulseDynamic().DeleteDeck(adminUserId, companyId, deckId);
        }


        public PulseUpdateResponse DeleteCard(string adminUserId, string companyId, string deckId, string pulseId)
        {
            return new PulseDynamic().DeleteCard(adminUserId, companyId, deckId, pulseId);
        }

        public PulseSelectDeckAnalyticResponse SelectDeckAnalytic(string adminUserId, string companyId, string deckId, string searchId)
        {
            return new PulseDynamic().SelectDeckAnalytic(adminUserId, companyId, deckId, searchId);
        }

        public PulseSelectDeckCardAnalyticResponse SelectDeckCardAnalytic(string adminUserId, string companyId, string deckId, string pulseId, string searchId)
        {
            return new PulseDynamic().SelectDeckCardAnalytic(adminUserId, companyId, deckId, pulseId, searchId);
        }

        public PulseSelectDeckCardOptionAnalyticResponse SelectDeckCardOptionAnalytic(string adminUserId, string companyId, string deckId, string pulseId, string optionId)
        {
            return new PulseDynamic().SelectDeckCardOptionAnalytic(adminUserId, companyId, deckId, pulseId, optionId);
        }

        public PulseSelectDeckCardCustomAnswerAnalyticResponse SelectDeckCardCustomAnswerAnalytic(string adminUserId, string companyId, string deckId, string pulseId)
        {
            return new PulseDynamic().SelectDeckCardCustomAnswerAnalytic(adminUserId, companyId, deckId, pulseId);
        }

        public PulseSelectDeckAnalyticByUserResponse SelectDeckAnalyticByUser(string adminUserId,
                                                                              string companyId,
                                                                              string deckId,
                                                                              string answeredByUserId)
        {
            return new PulseDynamic().SelectDeckAnalyticByUser(adminUserId, companyId, deckId, answeredByUserId);
        }

        public PulseUpdateResponse ImportDeck(string adminUserId,
                                              string companyId,
                                              string currentDeckId,
                                              List<string> deckIds)
        {
            return new PulseDynamic().ImportDeck(adminUserId, companyId, currentDeckId, deckIds);
        }

        public PulseUpdateResponse UpdateDynamicCard(string cardId,
                                                     string deckId,
                                                     string adminUserId,
                                                     string companyId,
                                                     string newTitle,
                                                     string newDescription,
                                                     int newCardType,
                                                     int newQuestionType,
                                                     DateTime newStartDate,
                                                     int newNumberOfOptions = 0,
                                                     int newRangeType = 0,
                                                     string newImageUrl = null,
                                                     string newMinRangeLabel = null,
                                                     string newMaxRangeLabel = null,
                                                     List<DeckCardOption> newOptions = null)
        {
            return new PulseDynamic().UpdateDynamicCard(cardId, deckId, adminUserId, companyId, newTitle, newDescription, newCardType, newQuestionType, newStartDate, newNumberOfOptions, newRangeType, newImageUrl, newMinRangeLabel, newMaxRangeLabel, newOptions);
        }

        public PulseUpdateResponse UpdateDynamicCardStartDate(string updateCardId,
                                                              string deckId,
                                                              string adminUserId,
                                                              string companyId,
                                                              DateTime newStartDate,
                                                              bool isApplyToAllLaterCards)
        {
            return new PulseDynamic().UpdateDynamicCardStartDate(updateCardId, deckId, adminUserId, companyId, newStartDate, isApplyToAllLaterCards);
        }
        #endregion

        #region Setting
        public AutoApprovalProfilePhotoResponse UpdateAutoApprovalProfilePhoto(string companyId, string adminUserId, bool isAutoApproval)
        {
            return new Setting().UpdateAutoApprovalProfilePhoto(companyId, adminUserId, isAutoApproval);
        }

        public AutoApprovalProfilePhotoResponse GetAutoApprovalProfilePhoto(string companyId, string adminUserId)
        {
            return new Setting().GetAutoApprovalProfilePhoto(companyId, adminUserId);
        }
        #endregion

        #region Assessment
        public AssessmentCreateResponse CreateAssessment(string adminUserId,
                                                         string title,
                                                         string introduction,
                                                         string closingWords,
                                                         string iconUrl,
                                                         string coachId,
                                                         string coachName,
                                                         bool isDisplayCoachName,
                                                         bool isAnonymous,
                                                         List<AssessmentImage> imageBanners,
                                                         List<AssessmentTabulation> tabulations,
                                                         List<AssessmentAnswer> answers,
                                                         string videoUrl = null,
                                                         string videoTitle = null,
                                                         string videoDescription = null,
                                                         string videoThumbnailUrl = null,
                                                         string cssArea = null)
        {
            return new Assessment().CreateAssessment(adminUserId, title, introduction, closingWords, iconUrl, coachId, coachName, isDisplayCoachName, isAnonymous, imageBanners, tabulations, answers, videoUrl, videoTitle, videoDescription, videoThumbnailUrl, cssArea);
        }

        public AssessmentSelectAllResponse SelectAllAssessments(string adminUserId)
        {
            return new Assessment().SelectAllAssessments(adminUserId);
        }

        public AssessmentSelectResponse SelectFullDetailAssessment(string adminUserId, string assessmentId)
        {
            return new Assessment().SelectFullDetailAssessment(adminUserId, assessmentId);
        }

        public AssessmentSelectResponse PreviewAssessment(string adminUserId, string assessmentId)
        {
            return new Assessment().SelectFullDetailAssessment(adminUserId, assessmentId, true);
        }

        public AssessmentCreateCardResponse CreateAssessmentCard(string cardId,
                                                                int type,
                                                                string content,
                                                                List<AssessmentImage> images,
                                                                bool toBeSkipped,
                                                                bool isOptionRandomized,
                                                                bool hasPageBreak,
                                                                int backgoundType,
                                                                string note,
                                                                string assessmentId,
                                                                string adminUserId,
                                                                List<AssessmentCardOption> options,
                                                                int totalPointsAllocation = 0,
                                                                int miniOptionToSelect = 0,
                                                                int maxOptionToSelect = 0,
                                                                int startRangePosition = 0,
                                                                int maxRange = 0,
                                                                string maxRangeLabel = null,
                                                                int midRange = 0,
                                                                string midRangeLabel = null,
                                                                int minRange = 0,
                                                                string minRangeLabel = null)
        {
            return new AssessmentCard().CreateAssessmentCard(cardId, type, content, images, toBeSkipped, isOptionRandomized, hasPageBreak, backgoundType, note, assessmentId, adminUserId, options, totalPointsAllocation, miniOptionToSelect, maxOptionToSelect, startRangePosition, maxRange, maxRangeLabel, midRange, midRangeLabel, minRange, minRangeLabel);
        }

        public AssessmentSelectCardResponse SelectFullDetailAssessmentCard(string assessmentId, string cardId, string adminUserId)
        {
            return new AssessmentCard().SelectFullDetailAssessmentCard(assessmentId, cardId, adminUserId);
        }

        public AssessmentUpdateResponse UpdateAssessmentStatus(string adminUserId, string assessmentId, int newStatus)
        {
            return new Assessment().UpdateAssessmentStatus(adminUserId, assessmentId, newStatus);
        }

        public AssessmentUpdateResponse DeleteAssessment(string adminUserId, string companyId, string assessmentId)
        {
            return new Assessment().DeleteAssessment(adminUserId, companyId, assessmentId);
        }

        public AssessmentUpdateResponse DeleteAssessmentCard(string adminUserId, string assessmentId, string cardId)
        {
            return new AssessmentCard().DeleteCard(adminUserId, assessmentId, cardId);
        }

        public AssessmentUpdateResponse UpdateAssessment(string assessmentId,
                                                        string adminUserId,
                                                        string newTitle,
                                                        string newIntroduction,
                                                        string newClosingWords,
                                                        string newIconUrl,
                                                        string coachId,
                                                        string coachName,
                                                        bool newIsDisplayCoachName,
                                                        bool newIsAnonymous,
                                                        int newStatus,
                                                        List<AssessmentImage> newImageBanners,
                                                        List<AssessmentTabulation> newTabulations,
                                                        List<AssessmentAnswer> newAnswers,
                                                        string newVideoUrl = null,
                                                        string newVideoTitle = null,
                                                        string newVideoDescription = null,
                                                        string newVideoThumbnailUrl = null,
                                                        string newCssArea = null)
        {
            return new Assessment().UpdateAssessment(assessmentId, adminUserId, newTitle, newIntroduction, newClosingWords, newIconUrl, coachId, coachName, newIsDisplayCoachName, newIsAnonymous, newStatus, newImageBanners, newTabulations, newAnswers, newVideoUrl, newVideoTitle, newVideoDescription, newVideoThumbnailUrl, newCssArea);
        }

        public AssessmentUpdateResponse UpdateAssessmentCard(string cardId,
                                                     int newType,
                                                     string newContent,
                                                     List<AssessmentImage> newImages,
                                                     bool newToBeSkipped,
                                                     bool newIsOptionRandomized,
                                                     bool newHasPageBreak,
                                                     int newBackgoundType,
                                                     string newNote,
                                                     string assessmentId,
                                                     string adminUserId,
                                                     List<AssessmentCardOption> newOptions,
                                                     int newTotalPointsAllocation = 0,
                                                     int newMiniOptionToSelect = 0,
                                                     int newMaxOptionToSelect = 0,
                                                     int newStartRangePosition = 0,
                                                     int newMaxRange = 0,
                                                     string newMaxRangeLabel = null,
                                                     int newMidRange = 0,
                                                     string newMidRangeLabel = null,
                                                     int newMinRange = 0,
                                                     string newMinRangeLabel = null)
        {
            return new AssessmentCard().UpdateAssessmentCard(cardId, newType, newContent, newImages, newToBeSkipped, newIsOptionRandomized, newHasPageBreak, newBackgoundType, newNote, assessmentId, adminUserId, newOptions, newTotalPointsAllocation, newMiniOptionToSelect, newMaxOptionToSelect, newStartRangePosition, newMaxRange, newMaxRangeLabel, newMidRange, newMidRangeLabel, newMinRange, newMinRangeLabel);
        }

        #endregion

        #region Subscription
        public SubscriptionCreateResponse CreateAssessmentSubscription(string adminUserId,
                                                                       string companyId,
                                                                       string assessmentId,
                                                                       int numberOfLicences,
                                                                       int durationType,
                                                                       int visibleDaysAfterCompletion,
                                                                       int numberOfRetake,
                                                                       int status,
                                                                       DateTime? startDate,
                                                                       DateTime? endDate,
                                                                       List<string> targetedDepartmentIds,
                                                                       List<string> targetedUserIds)
        {
            return new Subscription().CreateAssessmentSubscription(adminUserId, companyId, assessmentId, numberOfLicences, durationType, visibleDaysAfterCompletion, numberOfRetake, status, startDate, endDate, targetedDepartmentIds, targetedUserIds);
        }

        public SubscriptionSelectAllResponse SelectAllAssessmentSubscription(string adminUserId)
        {
            return new Subscription().SelectAllAssessmentSubscription(adminUserId);
        }

        public SubscriptionSelectResponse SelectFullDetailAssessmentSubscription(string adminUserId, string companyId, string assessmentId)
        {
            return new Subscription().SelectFullDetailAssessmentSubscription(adminUserId, companyId, assessmentId);
        }

        public SubscriptionDeleteResponse DeleteSubscription(string adminUserId, string companyId, string assessmentId)
        {
            return new Subscription().DeleteSubscription(adminUserId, companyId, assessmentId);
        }
        #endregion

        #region Coach
        public CoachCreateResponse CreateCoach(string coachName, string adminUserId)
        {
            return new Coach().CreateCoach(coachName, adminUserId, true);
        }

        public CoachSelectAllResponse SelectAllCoaches(string adminUserId)
        {
            return new Coach().SelectAllCoaches(adminUserId);
        }

        public CoachUpdateResponse UpdateCoach(string adminUserId, string coachId, string newCoachName)
        {
            return new Coach().UpdateCoach(adminUserId, coachId, newCoachName);
        }

        public CoachUpdateResponse DeleteCoach(string adminUserId, string coachId)
        {
            return new Coach().DeleteCoach(adminUserId, coachId);
        }
        #endregion

        #region Mobile Learning
        public MLCategoryCreateResponse CreateMLCategory(string companyId, string title, string adminUserId)
        {
            return new MLTopicCategory().CreateMLCategory(companyId, title, adminUserId);
        }
        public MLCategoryUpdateResponse UpdateMLCategory(string adminUserId, string companyId, string categoryId, string newTitle)
        {
            return new MLTopicCategory().UpdateMLCategory(adminUserId, companyId, categoryId, newTitle);
        }

        public MLCategoryUpdateResponse DeleteMLCategory(string adminUserId, string companyId, string categoryId)
        {
            return new MLTopicCategory().DeleteMLCategory(adminUserId, companyId, categoryId);
        }
        public MLTopicCreateResponse CreateMLTopic(string adminUserId,
                                                   string companyId,
                                                   string title,
                                                   string introduction,
                                                   string instruction,
                                                   string closingWords,
                                                   string topicIconUrl,
                                                   string passingGrade,
                                                   string categoryId,
                                                   string categoryTitle,
                                                   int durationType,
                                                   List<string> targetedDepartmentIds,
                                                   List<string> targetedUserIds,
                                                   DateTime startDate,
                                                   DateTime? endDate,
                                                   bool isDisplayAuthorOnClient = true,
                                                   bool isRandomizedAllCards = false,
                                                   bool isAllowRetestForFailures = true,
                                                   bool isAllowRetestForPasses = true,
                                                   int allowReviewAnswerType = 1,
                                                   int displayAnswerType = 2)
        {
            return new MLTopic().CreateMLTopic(adminUserId, companyId, title, introduction, instruction, closingWords, topicIconUrl, passingGrade, categoryId, categoryTitle, durationType, targetedDepartmentIds, targetedUserIds, startDate, endDate, isDisplayAuthorOnClient, isRandomizedAllCards, isAllowRetestForFailures, isAllowRetestForPasses, allowReviewAnswerType, displayAnswerType);
        }

        public MLTopicUpdateResponse UpdateMLTopic(string topicId,
                                                    string adminUserId,
                                                    string companyId,
                                                    string newTitle,
                                                    string newIntroduction,
                                                    string newInstruction,
                                                    string newClosingWords,
                                                    string newTopicIconUrl,
                                                    string newPassingGrade,
                                                    string newCategoryId,
                                                    string newCategoryTitle,
                                                    int newDurationType,
                                                    int newStatus,
                                                    List<string> newTargetedDepartmentIds,
                                                    List<string> newTargetedUserIds,
                                                    DateTime newStartDate,
                                                    DateTime? newEndDate,
                                                    bool newIsDisplayAuthorOnClient = true,
                                                    bool newIsRandomizedAllCards = true,
                                                    bool newIsAllowRetestForFailures = true,
                                                    bool newIsAllowRetestForPasses = true,
                                                    int newAllowReviewAnswerType = 1,
                                                    int newDisplayAnswerType = 2)
        {
            return new MLTopic().UpdateTopic(topicId, adminUserId, companyId, newTitle, newIntroduction, newInstruction, newClosingWords, newTopicIconUrl, newPassingGrade, newCategoryId, newCategoryTitle, newDurationType, newStatus, newTargetedDepartmentIds, newTargetedUserIds, newStartDate, newEndDate, newIsDisplayAuthorOnClient, newIsRandomizedAllCards, newIsAllowRetestForFailures, newIsAllowRetestForPasses, newAllowReviewAnswerType, newDisplayAnswerType);
        }

        public MLTopicUpdateResponse UpdateMLTopicStatus(string topicId, string categoryId, string adminUserId, string companyId, int updatedStatus)
        {
            return new MLTopic().UpdateTopicStatus(topicId, categoryId, adminUserId, companyId, updatedStatus);
        }

        public MLCategorySelectAllResponse SelectAllMLCategories(string adminUserId, string companyId, int queryType)
        {
            return new MLTopicCategory().SelectAllMLCategories(adminUserId, companyId, queryType);
        }

        public MLTopicSelectAllBasicResponse SelectAllMLTopicByCategory(string adminUserId, string companyId, string selectedCategoryId = null, string containsName = null)
        {
            return new MLTopic().SelectAllBasicByCategory(adminUserId, companyId, selectedCategoryId, containsName);
        }

        public MLCardCreateResponse CreateMLCard(string cardId,
                                               int type,
                                               bool isOptionRandomized,
                                               bool hasPageBreak,
                                               int backgroundType,
                                               string note,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               string content,
                                               int scoringCalculationType = 1,
                                               int miniOptionToSelect = 0,
                                               int maxOptionToSelect = 0,
                                               string description = null,
                                               List<MLUploadedContent> uploadedContent = null,
                                               List<MLOption> options = null,
                                               List<MLPageContent> pagesContent = null)
        {
            return new MLCard().CreateMLCard(cardId,
                                             type,
                                             isOptionRandomized,
                                             hasPageBreak,
                                             backgroundType,
                                             note,
                                             categoryId,
                                             topicId,
                                             adminUserId,
                                             companyId,
                                             content,
                                             scoringCalculationType,
                                             miniOptionToSelect,
                                             maxOptionToSelect,
                                             description,
                                             uploadedContent,
                                             options,
                                             pagesContent);
        }

        public MLCardUpdateResponse UpdateMLCard(string cardId,
                                                 int newType,
                                                 bool newIsOptionRandomized,
                                                 bool newHasPageBreak,
                                                 int newBackgoundType,
                                                 string newNote,
                                                 string categoryId,
                                                 string topicId,
                                                 string adminUserId,
                                                 string companyId,
                                                 string newContent,
                                                 int newScoringCalculationType = 1,
                                                 int newMiniOptionToSelect = 0,
                                                 int newMaxOptionToSelect = 0,
                                                 string newDescription = null,
                                                 List<MLUploadedContent> newUploadedContent = null,
                                                 List<MLOption> newOptions = null,
                                                 List<MLPageContent> newPagesContent = null)
        {
            return new MLCard().UpdateCard(cardId,
                                           newType,
                                           newIsOptionRandomized,
                                           newHasPageBreak,
                                           newBackgoundType,
                                           newNote,
                                           categoryId,
                                           topicId,
                                           adminUserId,
                                           companyId,
                                           newContent,
                                           newScoringCalculationType,
                                           newMiniOptionToSelect,
                                           newMaxOptionToSelect,
                                           newDescription,
                                           newUploadedContent,
                                           newOptions,
                                           newPagesContent);
        }

        public MLCardUpdateResponse DeleteMLCard(string topicId, string categoryId, string cardId, string adminUserId, string companyId)
        {
            return new MLCard().DeleteCard(topicId, categoryId, cardId, adminUserId, companyId);
        }

        public MLTopicSelectResponse SelectFullDetailMLTopic(string adminUserId, string companyId, string topicId, string categoryId, string containsCardName = null)
        {
            return new MLTopic().SelectFullDetailMLTopic(adminUserId, companyId, topicId, categoryId, containsCardName);
        }

        public MLCardSelectResponse SelectFullDetailMLCard(string adminUserId, string companyId, string cardId, string topicId)
        {
            return new MLCard().SelectCard(adminUserId, companyId, cardId, topicId, (int)MLCard.MLCardQueryType.Full);
        }

        public MLCardSelectAllResponse PreviewML(string topicId, string categoryId, string adminUserId, string companyId)
        {
            return new MLCard().SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)MLCard.MLCardQueryType.Full);
        }

        public MLCardReorderResponse ReorderMLCard(string cardId, string topicId, string categoryId, string adminUserId, string companyId, int insertAtOrder)
        {
            return new MLCard().ReorderCard(cardId, topicId, categoryId, adminUserId, companyId, insertAtOrder);
        }

        public MLTopicSelectFeedbackResponse SelectMLFeedback(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new MLTopic().SelectFeedback(adminUserId, companyId, topicId, categoryId);
        }

        public MLSelectCandidateResultResponse SelectMLCandidateResult(string adminUserId, string companyId, string topicId, string categoryId, string searchId)
        {
            return new MLTopic().SelectCandidateResult(adminUserId, companyId, topicId, categoryId, searchId);
        }

        public MLSelectQuestionResultResponse SelectMLQuestionResult(string adminUserId, string companyId, string topicId, string categoryId, string searchId, string questionToggleType = "1")
        {
            return new MLTopic().SelectQuestionResult(adminUserId, companyId, topicId, categoryId, searchId, questionToggleType);
        }

        public MLSelectCardQuestionResultResponse SelectMLCardQuestionResult(string adminUserId, string companyId, string topicId, string categoryId, string cardId, string searchId)
        {
            return new MLCard().SelectCardQuestionResult(adminUserId, companyId, topicId, categoryId, cardId, searchId);
        }

        public MLSelectUserAttemptHistoryResponse SelectMLUserAttemptHistoryResult(string adminUserId, string companyId, string answeredByUserId, string topicId, string categoryId)
        {
            return new MLTopic().SelectUserAttemptHistoryResult(adminUserId, companyId, answeredByUserId, topicId, categoryId);
        }

        public MLSelectSortedCardAttemptHistoryResponse SelectSortedCardHistoryResult(string adminUserId, string companyId, string answeredByUserId, string topicId, string categoryId, string questionToggleType, string containsName = null)
        {
            return new MLCard().SelectSortedCardHistoryResult(adminUserId, companyId, answeredByUserId, topicId, categoryId, questionToggleType, containsName);
        }

        public MLEducationCategoryListResponse SelectAllMLEducationCategories(string adminUserId, string companyId, int queryType)
        {
            return new MLEducationCategory().GetList(adminUserId, companyId, queryType);
        }

        public MLEducationCategoryCreateResponse CreateMLEducationCategory(string companyId, string title, string adminUserId)
        {
            return new MLEducationCategory().Create(companyId, title, adminUserId);
        }

        public MLEducationCategoryUpdaateResponse UpdateMLEducationCategory(string adminUserId, string companyId, string categoryId, string newTitle)
        {
            return new MLEducationCategory().Update(adminUserId, companyId, categoryId, newTitle);
        }

        public MLEducationCategoryDeleteResponse DeleteMLEducationCategory(string adminUserId, string companyId, string categoryId)
        {
            return new MLEducationCategory().Delete(adminUserId, companyId, categoryId);
        }

        public MLEducationListResponse SelectAllMLEducaation(string adminUserId, string companyId, string selectedCategoryId = null, string containsName = null)
        {
            return new MLEducation().GetList(adminUserId, companyId, selectedCategoryId, containsName);
        }

        public MLEducationCreateResponse CreateMLEducation(string adminUserId,
                                           string companyId,
                                           string title,
                                           string description,
                                           string iconUrl,
                                           string categoryId,
                                           string categoryTitle,
                                           int durationType,
                                           List<string> targetedDepartmentIds,
                                           List<string> targetedUserIds,
                                           DateTime? startDate,
                                           DateTime? endDate,
                                           int displayAnswerType = 1,
                                           bool isDisplayAuthorOnClient = true,
                                           bool isAutoBreak = true)
        {
            return new MLEducation().Create(adminUserId, companyId, title, description, iconUrl, categoryId, categoryTitle, durationType, targetedDepartmentIds, targetedUserIds, startDate, endDate, displayAnswerType, isDisplayAuthorOnClient, isAutoBreak);
        }

        public MLEducationUpdateResponse UpdateMLEducationStatus(string educationId, string categoryId, string adminUserId, string companyId, int updatedStatus)
        {
            return new MLEducation().UpdateStatus(educationId, categoryId, adminUserId, companyId, updatedStatus);
        }

        public MLEducationUpdateResponse UpdateMLEducation(string educationId,
                                         string adminUserId,
                                         string companyId,
                                         string newTitle,
                                         string newDescription,
                                         string newIconUrl,
                                         string newCategoryId,
                                         string newCategoryTitle,
                                         int newDurationType,
                                         int newStatus,
                                         List<string> newTargetedDepartmentIds,
                                         List<string> newTargetedUserIds,
                                         DateTime newStartDate,
                                         DateTime? newEndDate,
                                         int newDisplayAnswerType = 2,
                                         bool newIsDisplayAuthorOnClient = true,
                                         bool newIsAutoBreak = true)
        {
            return new MLEducation().Update(educationId, adminUserId, companyId, newTitle, newDescription, newIconUrl, newCategoryId, newCategoryTitle, newDurationType, newStatus, newTargetedDepartmentIds, newTargetedUserIds, newStartDate, newEndDate, newDisplayAnswerType, newIsDisplayAuthorOnClient, newIsAutoBreak);
        }

        public MLEducationDetailResponse SelectFullDetailMLEducation(string companyId, string adminUserId, string categoryId, string educationId, string containsCardName = null)
        {
            return new MLEducation().GetDetail(companyId, adminUserId, categoryId, educationId, containsCardName);
        }

        public MLEducationCardCreateResponse CreateMLEducationCard(string cardId,
                                                int type,
                                                bool isOptionRandomized,
                                                int backgoundType,
                                                string categoryId,
                                                string educationId,
                                                string adminUserId,
                                                string companyId,
                                                string content,
                                                int minOptionToSelect = 0,
                                                int maxOptionToSelect = 0,
                                                string description = null,
                                                List<MLUploadedContent> uploadedContent = null,
                                                List<MLEducationOption> options = null,
                                                List<MLPageContent> pagesContent = null,
                                                int ordering = -1)
        {
            return new MLEducationCard().Create(cardId,
                                                 type,
                                                 isOptionRandomized,
                                                 backgoundType,
                                                 categoryId,
                                                 educationId,
                                                 adminUserId,
                                                 companyId,
                                                 content,
                                                 minOptionToSelect = 0,
                                                 maxOptionToSelect = 0,
                                                 description,
                                                 uploadedContent,
                                                 options,
                                                 pagesContent,
                                                 ordering);
        }

        public MLEducationCardDetailResponse SelectFullDetailMLEducationCard(string adminUserId, string companyId, string cardId, string educationId)
        {
            return new MLEducationCard().GetDetail(adminUserId, companyId, cardId, educationId, (int)MLEducationCard.QueryType.Full);
        }

        public MLEducationCardUpdateResponse DeleteMLEducationCard(string educationId, string categoryId, string cardId, string adminUserId, string companyId)
        {
            return new MLEducationCard().Delete(educationId, categoryId, cardId, adminUserId, companyId);
        }

        public MLEducationCardUpdateResponse UpdateMLEducationCard(string cardId,
                                       int newType,
                                       bool newIsOptionRandomized,
                                       int newBackgoundType,
                                       string categoryId,
                                       string educationId,
                                       string adminUserId,
                                       string companyId,
                                       string newContent,
                                       int newMinOptionToSelect = 0,
                                       int newMaxOptionToSelect = 0,
                                       string newDescription = null,
                                       List<MLUploadedContent> newUploadedContent = null,
                                       List<MLEducationOption> newOptions = null,
                                       List<MLPageContent> newPagesContent = null)
        {
            return new MLEducationCard().Update(cardId,
                                        newType,
                                        newIsOptionRandomized,
                                        newBackgoundType,
                                        categoryId,
                                        educationId,
                                        adminUserId,
                                        companyId,
                                        newContent,
                                        newMinOptionToSelect,
                                        newMaxOptionToSelect,
                                        newDescription,
                                        newUploadedContent,
                                        newOptions,
                                        newPagesContent);
        }

        public MLEducationAnalticResponse SelectEducationAnalyticAttendance(string companyId, string managerUserId, string categoryId, string educationId)
        {
            return new AnalyticMLEducation().GetAnalyticAttendance(companyId, managerUserId, categoryId, educationId);
        }

        public MLEducationAnalticResponse SelectEducationAnalyticUsers(string companyId, string managerUserId, string educationId, string deparmtnetId = null)
        {
            return new AnalyticMLEducation().GetAnalyticUsers(companyId, managerUserId, educationId, deparmtnetId);
        }

        public MLEducationAnalticResponse SelectEducationAnalyticUser(string companyId, string managerUserId, string categoryId, string educationId, string answeredUserId)
        {
            return new AnalyticMLEducation().GetAnalyticUser(companyId, managerUserId, categoryId, educationId, answeredUserId);
        }

        public MLAssignmentPrivicyResponse SelectAssignmentDepartmentPrivacy(string managerId, string companyId, List<string> targetEducationIds)
        {
            return new MLAssignment().GetDepartmentListForPrivacy(managerId, companyId, targetEducationIds);
        }

        public MLAssignmentPrivicyResponse SelectAssignmentPrivacy(string managerId, string companyId, List<string> targetEducationIds)
        {
            return new MLAssignment().GetPrivacy(managerId, companyId, targetEducationIds);
        }

        public MLAssignmentCreateResponse CreateAssignment(string managerId, string companyId, string educationId, int druationType, DateTime startDate, DateTime? endDate, int participantsType, int frequencyDays, List<string> targetedDepartmentIds, List<string> targetedUserIds)
        {
            return new MLAssignment().Create(managerId, companyId, educationId, druationType, startDate, endDate, participantsType, frequencyDays, targetedDepartmentIds, targetedUserIds);
        }

        public MLAssignmentListResponse CreateAssignments(string managerId, string companyId, List<string> educationIds, int druationType, DateTime startDate, DateTime? endDate, int participantsType, int frequencyDays, List<string> targetedDepartmentIds, List<string> targetedUserIds)
        {
            return new MLAssignment().CreateAssignments(managerId, companyId, educationIds, druationType, startDate, endDate, participantsType, frequencyDays, targetedDepartmentIds, targetedUserIds);
        }

        public MLAssignmentListResponse SelectAllMLAssignments(string managerId, string companyId, int progressType = 0, string containsEducationTitle = null)
        {
            return new MLAssignment().GetList(managerId, companyId, progressType, containsEducationTitle);
        }

        public MLAssignmentDeleteResponse DeleteMLAssignement(string managerId, string companyId, string assignmentId)
        {
            return new MLAssignment().Delete(managerId, companyId, assignmentId);
        }

        public MLAssignmentDetailResponse SelectAssignmentDetail(string managerId, string companyId, string assignmentId)
        {
            return new MLAssignment().GetDetail(managerId, companyId, assignmentId);
        }

        public MLAssignmentUpdateResponse UpdateMLAssignment(string managerId,
                                                 string companyId,
                                                 string assignmentId,
                                                 string newEducationId,
                                                 int newDruationType,
                                                 DateTime newStartDate,
                                                 DateTime? newEndDate,
                                                 int newParticipantsType,
                                                 int newFrequencyDays,
                                                 List<string> newTargetedDepartmentIds,
                                                 List<string> newTargetedUserIds)
        {
            return new MLAssignment().Update(managerId, companyId, assignmentId, newEducationId, newDruationType, newStartDate, newEndDate, newParticipantsType, newFrequencyDays, newTargetedDepartmentIds, newTargetedUserIds);
        }
        #endregion

        #region Achievement
        public GamificationCreateAchievementResponse CreateAchievement(string adminUserId, string companyId, string achievementId, string title, int templateType, bool isIconCustomized, string iconImageUrl, string iconColor, int type, int ruleType, List<string> topicIds, int playTimes = 0, int failTimes = 0)
        {
            return new Gamification().CreateAchievement(adminUserId, companyId, achievementId, title, templateType, isIconCustomized, iconImageUrl, iconColor, type, ruleType, topicIds, playTimes, failTimes);
        }

        public GamificationCreateAchievementResponse DeleteAchievement(string adminUserId, string companyId, string achievementId)
        {
            return new Gamification().DeleteAchievement(adminUserId, companyId, achievementId);
        }

        public GamificationUpdateAchievementResponse UpdateAchievement(string adminUserId, string companyId, string achievementId, string newTitle, int newTemplateType, bool newIsIconCustomized, string newIconImageUrl, string newIconColor, int newType, int newRuleType, List<string> newTopicIds, int newPlayTimes = 0, int newFailTimes = 0)
        {
            return new Gamification().UpdateAchievement(adminUserId, companyId, achievementId, newTitle, newTemplateType, newIsIconCustomized, newIconImageUrl, newIconColor, newType, newRuleType, newTopicIds, newPlayTimes, newFailTimes);
        }

        public GamificationSelectAllAchievementResponse ReorderingAchievements(string adminUserId, string companyId, List<string> achievementIds)
        {
            return new Gamification().ReorderingAchievements(adminUserId, companyId, achievementIds);
        }

        public GamificationSelectAllAchievementResponse SelectAllAchievements(string adminUserId, string companyId, string containsName = null)
        {
            return new Gamification().SelectAllAchievements(adminUserId, companyId, (int)Gamification.GamificationQueryType.FullDetail, containsName);
        }

        public GamificationSelectAchievementResponse SelectAchievement(string adminUserId, string companyId, string achievementId)
        {
            return new Gamification().SelectAchievement(adminUserId, companyId, achievementId);
        }

        public GamificationSelectTopicResponse SelectTopicsForAchievement(string adminUserId, string companyId, string containsName = null)
        {
            return new Gamification().SelectTopicsForAchievement(adminUserId, companyId, containsName);
        }

        public GamificationSelectAchievementResultResponse SelectAchievementResult(string adminUserId, string companyId, string achievementId, string searchId)
        {
            return new Gamification().SelectAchievementResult(adminUserId, companyId, achievementId, searchId);
        }

        public GamificationSelectUserAchievementResultResponse SelectUserAchievementResult(string adminUserId, string companyId, string achieverId)
        {
            return new Gamification().SelectUserAchievementResult(adminUserId, companyId, achieverId);
        }
        #endregion

        #region Appraisal 360
        public AppraisalSelectAllTemplatesResponse SelectAppraisalTemplates(string adminUserId, string companyId, int type)
        {
            return new Appraisal().SelectTemplatesByAdmin(adminUserId, companyId, type);
        }

        public AppraisalSelectTemplateResponse SelectAppraisalFullTemplateByAdmin(string adminUserId, string companyId, string appraisalId)
        {
            return new Appraisal().SelectFullTemplateByAdmin(adminUserId, companyId, appraisalId);
        }

        public AppraisalCreateTemplateResponse CreateAppraisalTemplate(string adminUserId,
                                                                       string companyId,
                                                                       int templateType,
                                                                       string title,
                                                                       string description,
                                                                       bool isPrioritized,
                                                                       int anonymity,
                                                                       bool isUserAllowToCreateQuestions,
                                                                       int groupTeamType,
                                                                       int questionLimit,
                                                                       List<string> targetedDepartmentIds,
                                                                       List<string> targetedUserIds,
                                                                       int publishMethod,
                                                                       DateTime startDate,
                                                                       DateTime? endDate)
        {
            return new Appraisal().CreateTemplate(adminUserId, companyId, templateType, title, description, isPrioritized, anonymity, isUserAllowToCreateQuestions, groupTeamType, questionLimit, targetedDepartmentIds, targetedUserIds, publishMethod, startDate, endDate);
        }

        public AppraisalCategoryCreateResponse CreateAppraisalTemplateCategory(string adminUserId, string companyId, string appraisalId, string title)
        {
            return new AppraisalCategory().CreateTemplateCategoryByAdmin(companyId, adminUserId, appraisalId, title);
        }

        public AppraisalCardCreateTemplateResponse CreateAppraisalTemplateCard(string adminUserId,
                                                                               string companyId,
                                                                               string appraisalId,
                                                                               string categoryId,
                                                                               string cardContent,
                                                                               int cardType,
                                                                               List<AppraisalCardOption> options)
        {
            return new AppraisalCard().CreateTemplateCardByAdmin(adminUserId, companyId, appraisalId, categoryId, cardContent, cardType, options);
        }

        public AppraisalUpdateResponse UpdateAppraisalStatus(string adminUserId, string companyId, string appraisalId, int updatedStatus)
        {
            return new Appraisal().UpdateStatus(adminUserId, companyId, appraisalId, updatedStatus);
        }

        public AppraisalUpdateCategoryResponse UpdateTemplateAppraisalCategoryTitle(string companyId, string adminUserId, string templateAppraisalId, string categoryId, string updatedTitle)
        {
            return new AppraisalCategory().UpdateTemplateCategoryTitleByAdmin(companyId, adminUserId, templateAppraisalId, categoryId, updatedTitle);
        }

        public AppraisalCardCreateTemplateResponse UpdateTemplateAppraisalCard(string cardId,
                                                                                string adminUserId,
                                                                                string companyId,
                                                                                string appraisalId,
                                                                                string categoryId,
                                                                                string newCardContent,
                                                                                int newCardType,
                                                                                List<AppraisalCardOption> newOptions)
        {
            return new AppraisalCard().UpdateTemplateCardByAdmin(cardId, adminUserId, companyId, appraisalId, categoryId, newCardContent, newCardType, newOptions);
        }

        public AppraisalUpdateCardResponse RemoveTemplateAppraisalCard(string adminUserId, string companyId, string templateAppraisalId, string categoryId, string cardId)
        {
            return new AppraisalCard().RemoveTemplateCardByAdmin(adminUserId, companyId, templateAppraisalId, categoryId, cardId);
        }

        public AppraisalUpdateCategoryResponse RemoveAppraisalCategory(string companyId, string adminUserId, string templateAppraisalId, string categoryId)
        {
            return new AppraisalCategory().RemoveCategoryByAdmin(companyId, adminUserId, templateAppraisalId, categoryId);
        }

        public AppraisalUpdateResponse UpdateAppraisalTemplate(string adminUserId,
                                                               string companyId,
                                                               string templateAppraisalId,
                                                               string newTitle,
                                                               string newDescription,
                                                               bool newIsPrioritized,
                                                               int newAnonymity,
                                                               bool newIsUserAllowToCreateQuestions,
                                                               int newGroupTeamType,
                                                               int newQuestionLimit,
                                                               List<string> newTargetedDepartmentIds,
                                                               List<string> newTargetedUserIds,
                                                               int newPublishMethod,
                                                               int newStatus,
                                                               DateTime newStartDate,
                                                               DateTime? newEndDate)
        {
            return new Appraisal().UpdateTemplate(adminUserId, companyId, templateAppraisalId, newTitle, newDescription, newIsPrioritized, newAnonymity, newIsUserAllowToCreateQuestions, newGroupTeamType, newQuestionLimit, newTargetedDepartmentIds, newTargetedUserIds, newPublishMethod, newStatus, newStartDate, newEndDate);
        }

        public AppraisalSelectResponse SelectAppraisalResult(string createdByUserId, string companyId, string appraisalId)
        {
            return new Appraisal().SelectAppraisalResult(createdByUserId, companyId, appraisalId);
        }

        public AppraisalSelectGroupTeamTypeResponse SelectGroupTeamTypes(string adminUserId,
                                                                         string companyId)
        {
            return new Appraisal().SelectGroupTeamTypes(adminUserId, companyId);
        }

        public AppraisalSelectOverallReportsResponse SelectAppraisalReportsByAdmin(string adminUserId, string companyId)
        {
            return new Appraisal().SelectReportsByAdmin(adminUserId, companyId);
        }
        #endregion

        #region Notification
        public TriggerScheduledNotificationResponse TriggerScheduledNotification(string scheduledId, string companyId)
        {
            return new Notification().TriggerScheduledNotification(scheduledId, companyId);
        }

        public HttpRequestGetScheduleResponse PullNotificationSchedules()
        {
            return new Notification().PullSchedules();
        }
        #endregion
    }
}
