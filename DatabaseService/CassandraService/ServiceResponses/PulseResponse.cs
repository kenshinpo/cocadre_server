﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;
using Cassandra;

namespace CassandraService.ServiceResponses
{

    public class PulseValidationResponse : ServiceResponse
    {
    }

    public class PulseCreateResponse : ServiceResponse
    {
        public PulseDynamic Pulse { get; set; }
    }

    public class PulseDeckCreateResponse : ServiceResponse
    {
        public string DeckId { get; set; }
    }

    public class PulseSelectResponse : ServiceResponse
    {
        public Pulse Pulse { get; set; }
    }

    public class PulseDeckSelectCardResponse : ServiceResponse
    {
        public PulseDynamic Pulse { get; set; }
    }

    public class PulseSelectDeckResponse : ServiceResponse
    {
        public PulseDeck Deck { get; set; }
    }

    public class PulseSelectAllResponse : ServiceResponse
    {
        public List<Pulse> Pulses { get; set; }
    }

    public class PulseSelectDeckAllCardsResponse : ServiceResponse
    {
        public List<PulseDynamic> Pulses { get; set; }
    }

    public class PulseSelectAllDecksResponse : ServiceResponse
    {
        public List<PulseDeck> Decks { get; set; }
    }

    public class PulseUpdateResponse : ServiceResponse
    {
    }

    public class PulseSelectFeedAnalyticResponse : ServiceResponse
    {
        public int ImpressionCount { get; set; }
        public int InboxCount { get; set; }
    }

    public class PulseUpdateSingleStatsResponse : ServiceResponse
    {
    }

    public class PulseUpdateImpressionResponse : ServiceResponse
    {
    }

    public class PulseSelectSingleAnalyticResponse : ServiceResponse
    {
        public Pulse Pulse { get; set; }
        public Chart LeftChart { get; set; }
        public Chart RightChart { get; set; }
        public AnalyticPulse.PersonnelList PersonnelList { get; set; }
    }

    public class PulseSelectDeckAnalyticResponse : ServiceResponse
    {
        public PulseDeck Deck { get; set; }
        public List<AnalyticPulse.PulseResponsiveSearchTerm> SearchTerms { get; set; }
        public AnalyticPulse.PulseResponsiveStats PulseStats { get; set; }
    }

    public class PulseSelectDeckCardAnalyticResponse : ServiceResponse
    {
        public AnalyticPulse.PulseResponsiveStats PulseStats { get; set; }

        public List<AnalyticPulse.PulseResponsiveSearchTerm> SearchTerms { get; set; }
    }

    public class PulseSelectDeckCardOptionAnalyticResponse : ServiceResponse
    {
        public PulseDeck Deck { get; set; }
        public PulseDynamic Pulse { get; set; }
        public List<Department> Departments { get; set; }
        public List<DeckCardOptionResponder> Responders { get; set; }

        public class DeckCardOptionResponder
        {
            public User Responder { get; set; }
            public string AnsweredOnTimestampString { get; set; }
        }
    }

    public class PulseSelectDeckCardCustomAnswerAnalyticResponse : ServiceResponse
    {
        public PulseDeck Deck { get; set; }
        public PulseDynamic Pulse { get; set; }
    }

    public class PulseSelectDeckAnalyticByUserResponse : ServiceResponse
    {
        public PulseDeck Deck { get; set; }
        public User AnsweredByUser { get; set; }
        public string LastUpdatedDateString { get; set; }
    }

    public class PulseSelectBannerBackgroundResponse : ServiceResponse
    {
        public List<string> BackgroundUrls { get; set; }
    }

    public class PulseAnswerCardResponse : ServiceResponse
    {
        public Pulse NextPulse { get; set; }
    }

    public class PulseUpdateWithStatementResponse : ServiceResponse
    {
        public List<BoundStatement> deleteStatements { get; set; }
        public List<BoundStatement> updateStatements { get; set; }
    }

    public class PulseSelectPrivacyResponse : ServiceResponse
    {
        public bool IsForEveryone { get; set; }
        public bool IsForDepartment { get; set; }
        public bool IsForUser { get; set; }
    }

    public class PulseCheckForCardCountOnDateResponse : ServiceResponse
    {
    }

    public class PulseMappingStatementResponse : ServiceResponse
    {
        public List<BoundStatement> Statements { get; set; }
    }

    public class PulseMapResponse : ServiceResponse
    {
        public Pulse Pulse { get; set; }
    }
}
