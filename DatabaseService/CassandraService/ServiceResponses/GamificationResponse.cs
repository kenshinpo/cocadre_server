﻿using CassandraService.Entity;
using System.Collections.Generic;

namespace CassandraService.ServiceResponses
{
    public class GamificationBackupResponse : ServiceResponse
    {
        public List<User> Users { get; set; }
    }

    public class GamificationResetResponse : ServiceResponse
    {
    }

    public class GamificationSelectAchievementResponse : ServiceResponse
    {
        public Gamification.Achievement Achievement { get; set; }
    }

    public class GamificationSelectAllAchievementResponse : ServiceResponse
    {
        public List<Gamification.Achievement> Achievements { get; set; }
    }

    public class GamificationCreateAchievementResponse : ServiceResponse
    {

    }

    public class GamificationUpdateAchievementResponse : ServiceResponse
    {

    }

    public class GamificationValidateAchievementResponse : ServiceResponse
    {

    }

    public class GamificationSelectTopicResponse : ServiceResponse
    {
        public List<Topic> Topics { get; set; }
    }

    public class GamificationSelectUnlockedAchievementResponse : ServiceResponse
    {
        public List<Gamification.Achievement> Achievements { get; set; }
        public Notification Notification { get; set; }
    }

    public class GamificationUpdateBadgeResponse : ServiceResponse
    {
    }

    public class GamificationUpdateBadgeSeenResponse : ServiceResponse
    {
    }

    public class GamificationSelectAchievementResultResponse : ServiceResponse
    {
        public Gamification.Achievement Achievement { get; set; }
        public List<AnalyticGamification.Achiever> Achievers { get; set; }
        public List<AnalyticGamification.GamificationSearchTerm> SearchTerms { get; set; } 
    }

    public class GamificationSelectUserAchievementResultResponse : ServiceResponse
    {
        public List<Gamification.Achievement> Achievements { get; set; }
        public User Achiever { get; set; }
        public int NumberOfUnlockedAchievements { get; set; }
    }
}
