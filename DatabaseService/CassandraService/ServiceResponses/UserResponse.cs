﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;

namespace CassandraService.ServiceResponses
{
    public class UserCreateResponse : ServiceResponse
    {

    }

    public class UserDeleteResponse : ServiceResponse
    {
    }

    public class UserDetailResponse : ServiceResponse
    {
        public User User;
    }

    public class UserListResponse : ServiceResponse
    {
        public List<User> Users;
    }

    public class UserSelectAllBasicResponse : ServiceResponse
    {
        public List<User> Users;
    }

    public class UserSelectAllByDepartmentResponse : ServiceResponse
    {
        public List<Department> Departments;
    }

    public class UserSelectBasicResponse : ServiceResponse
    {
        public User User;
    }

    public class UserSelectDeviceTokenResponse : ServiceResponse
    {
        public string DeviceToken { get; set; }

        public int DeviceType { get; set; }
    }

    /// <summary>
    /// For adding multiple users.
    /// </summary>
    public class UserCheckInputResponse : ServiceResponse
    {
        //public List<> Errors { get; set; }
    }

#warning Deprecated code in this UserSelectTokenWithCompanyResponse method (v2)
    public class UserSelectTokenWithCompanyResponse : ServiceResponse
    {
        public User User;
    }

    public class UserSelectWithCompanyResponse : ServiceResponse
    {
        public User User { get; set; }
        public List<Module> UnlockedModules { get; set; }
    }

    public class UserUpdateDeviceTokenResponse : ServiceResponse
    {
        public string OutdatedDeviceToken { get; set; }

        public int OutdatedDeviceType { get; set; }
    }

    public class UserUpdateLoginResponse : ServiceResponse
    {
        public string DeviceToken { get; set; }

        public int DeviceType { get; set; }
    }

    public class UserUpdateResponse : ServiceResponse
    {

    }

    public class UserUpdateStatusResponse : ServiceResponse
    {
    }

    public class UserUpdateTypeResponse : ServiceResponse
    {
    }

    public class PostingSuspendedUserListResponse : ServiceResponse
    {
        public List<User> Users { get; set; }
    }

    public class PostingPermissionSuspendUserResponse : ServiceResponse
    {
    }

    public class PostingPermissionRestoreUserResponse : ServiceResponse
    {
    }

    public class ModeratorChangeRightsResponse : ServiceResponse
    {

    }

    public class UserUploadProfileImageResponse : ServiceResponse
    {
        public int ProfileImageStatus { get; set; }
        public User UserDetail { get; set; }
    }

    public class UserSelectStatsResponse : ServiceResponse
    {
        public CassandraService.Entity.AnalyticQuiz.UserStats UserStats;
    }

    public class UserSelectUpdateResponse : ServiceResponse
    {
        public User User { get; set; }
        public Company Company { get; set; }
    }

    public class UserIdResponse : ServiceResponse
    {
        public string UserId;
    }

    public class BasicUserInfoResponse : ServiceResponse
    {
        //managerInfo.UserId = basicUserInfoResponse.UserId;
        //managerInfo.Email = basicUserInfoResponse.Email;
        //managerInfo.FirstName = basicUserInfoResponse.FirstName;
        //managerInfo.LastName = basicUserInfoResponse.LastName;
        //managerInfo.ProfileImageUrl = basicUserInfoResponse.ProfileImageUrl;

        public string UserId;
        public string Email;
        public string FirstName;
        public string LastName;
        public string ProfileImageUrl;
    }

    public class UserAccountTypeResponse : ServiceResponse
    {
        public CassandraService.Entity.User.AccountType AccountType;
        public int AccountStatus;
    }

    public class ModeratorAccessRightsResponse : ServiceResponse
    {
        public List<CassandraService.Entity.Module> Modules = null;
        public DateTime? AccessExpiryDate;

        public ModeratorAccessRightsResponse()
        {
            this.Modules = new List<Module>();
        }
    }

    public class UserCleanUpResponse : ServiceResponse
    {
    }

    public class UserImageProgressResponse : ServiceResponse
    {
        public UserImageProgress userImageProgress { get; set; }
    }

    public class UserImageProgressPubnubResponse : ServiceResponse
    {
        public string EventName;
        //public string UserId;
        public int Progress;
        public User UserDetails;
    }

    public class UserSelectProfileResponse : ServiceResponse
    {
        public User UserProfile { get; set; }
        public int NumberOfUnlockedAchievements { get; set; }
        public Gamification.Achievement Badge { get; set; }
        public Assessment ColoredBrainAssessment { get; set; }
        public string ColoredBrainAboutUser { get; set; }
        public string ColoredBrainFrameColor { get; set; } 
    }
}
