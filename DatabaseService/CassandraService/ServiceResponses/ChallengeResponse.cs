﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class ChallengeCreateResponse : ServiceResponse
    {
        public Topic Topic { get; set; }
        public List<User> Players { get; set; }
        public string ChallengeId { get; set; }
        public Notification Notification { get; set; }
    }

    public class ChallengeInvalidateResponse : ServiceResponse
    {
        public User InitiatedUser { get; set; }
        public User ChallengedUser { get; set; }
    }

    public class ChallengeIsReadyResponse : ServiceResponse
    {
        public bool HaveBothPlayersAccepted { get; set; }
        public bool IsLive { get; set; }
        public List<string> PlayerIds { get; set; }
    }

    public class ChallengeOfflineSelectOpponentAnswerResponse : ServiceResponse
    {
        public string ChallengeId { get; set; }
        public string OpponentUserId { get; set; }
        public bool HasOpponentAnswered { get; set; }
        public bool IsOpponentAnswerCorrect { get; set; }
        public float TimeTakenByOpponent { get; set; }
    }

    public class ChallengeSelectResponse : ServiceResponse
    {
        public Topic Topic { get; set; }
        public List<User> Players { get; set; }
        public string ChallengeId { get; set; }
    }

    public class ChallengeSelectStatsResponse : ServiceResponse
    {
        public AnalyticQuiz.Exp Exp { get; set; }
        public int OpponentLevel { get; set; }
        public List<User> TopPlayers { get; set; }
        public List<Gamification.Achievement> Achievements { get; set; }
        public Notification Notification { get; set; }
    }

    public class ChallengeStartWithoutOpponentResponse : ServiceResponse
    {
    }

    public class ChallengeSelectIncompleteResponse : ServiceResponse
    {
        public List<Challenge> Challenges { get; set; }
        public int TotalChallengesCount { get; set; }
    }
}
