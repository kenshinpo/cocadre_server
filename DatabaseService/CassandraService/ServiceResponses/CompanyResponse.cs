﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;
using Cassandra;

namespace CassandraService.ServiceResponses
{
    public class GetCompanyPermissionResponse : ServiceResponse
    {
        public Company Company { get; set; }
    }

    public class CompanySelectProfileResponse : ServiceResponse
    {
        public CompanyProfile CompanyProfile { get; set; }
        public List<Timezone> Timezones { get; set; }
    }

    public class CompanySelectEmailTemplateResponse : ServiceResponse
    {
        public string EmailSquareLogoUrl { get; set; }

        public string EmailPersonnelInvitationTitle { get; set; }

        public string EmailPersonnelInvitationDescription { get; set; }

        public string EmailPersonnelInvitationSupportInfo { get; set; }

        public string EmailAdminInvitationTitle { get; set; }

        public string EmailAdminInvitationDescription { get; set; }

        public string EmailAdminInvitationSupportInfo { get; set; }

        public string EmailForgotPasswordTitle { get; set; }

        public string EmailForgotPasswordDescription { get; set; }

        public string EmailForgotPasswordSupportInfo { get; set; }

        public string EmailResetPasswordTitle { get; set; }

        public string EmailResetPasswordDescription { get; set; }

        public string EmailResetPasswordSupportInfo { get; set; }
    }

    public class CompanyUpdatePermissionResponse : ServiceResponse
    {

    }

    public class CompanyUpdateProfileResponse : ServiceResponse
    {

    }

    public class CompanySelectImageResponse : ServiceResponse
    {
        public string ImageUrl { get; set; }
    }

    //public class CompanyImageResponse : ServiceResponse
    //{
    //    public string CompanyId { get; set; }
    //    public string ImageId { get; set; }
    //    public int ImageType { get; set; }
    //    public bool IsSelected { get; set; }
    //    public string ImageUrl { get; set; }
    //    public string CreatorId { get; set; }
    //    public DateTimeOffset CreatedTimestamp { get; set; }
    //    public string ModifierId { get; set; }
    //    public DateTimeOffset ModifiedTimestamp { get; set; }
    //}

    public class CompanySelectEmailDetailResponse : ServiceResponse
    {
        public string EmailLogoUrl { get; set; }
        public string EmailTitle { get; set; }
        public string EmailDescription { get; set; }
        public string EmailSupportInfo { get; set; }
    }


    public class CompanyResponse : ServiceResponse
    {
        public CassandraService.Entity.Company Company;
        public string CompanyId;
        public string CompanyLogoUrl;
        public string CompanyTitle;
        public string AdminImageUrl;
        public string PrimaryManagerId;

    }

    public class UpdateCopmaniesSetting : ServiceResponse
    {
        public List<Company> Companies { get; set; }
    }

    public class CompanyImageResponse : ServiceResponse
    {
        public CompanyImage CompanyImage { get; set; }
    }

    public class CompanyImagesResponse : ServiceResponse
    {
        public IEnumerable<CompanyImageDesc> CompanyImages;
    }

    public class GetCompanyImageDetailResponse : ServiceResponse
    {
        public CompanyImage CurrentImage { get; set; }
        public CompanyImage NextImage { get; set; }
        public CompanyImage PreviousImage { get; set; }
    }

    public class CompanySelectAllJobResponse : ServiceResponse
    {
        public List<Company.CompanyJob> Jobs { get; set; }
    }

    public class CompanyCreateJobStatementResponse : ServiceResponse
    {
        public List<BoundStatement> Statements { get; set; }
        public string JobId { get; set; }
    }

    public class CompanyUpdateUserJobStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public List<BoundStatement> DeleteStatements { get; set; }
    }

    public class CompanyCreateDefaultJobStatementResponse : ServiceResponse
    {
        public List<BoundStatement> Statements { get; set; }
        public string JobId { get; set; }
    }
}
