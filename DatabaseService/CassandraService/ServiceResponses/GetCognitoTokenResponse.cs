﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    [Serializable]
    public class GetCognitoTokenResponse
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public String ErrorMessage { get; set; }

        public String Token { get; set; }

        public String RoleArn { get; set; }
    }
}
