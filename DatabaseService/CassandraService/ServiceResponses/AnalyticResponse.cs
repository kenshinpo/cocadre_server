﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class AnalyticsSelectAllGameHistoriesResponse : ServiceResponse
    {
        public List<AnalyticQuiz.GameHistory> GameHistories { get; set; }
    }

    public class AnalyticsSelectBasicResponse : ServiceResponse
    {
        public CassandraService.Entity.AnalyticDau.BasicDau BasicDau { get; set; }
        public CassandraService.Entity.AnalyticQuiz.TopicAnalytics BasicTopicAttempts { get; set; }
    }

    public class AnalyticsSelectDetailDauResponse : ServiceResponse
    {
        public CassandraService.Entity.AnalyticDau.DetailDau DetailDau { get; set; }
        public CassandraService.Entity.AnalyticDau.DauChart DauChart { get; set; }
        public List<CassandraService.Entity.AnalyticDau.TimeActivity> TimeActivities { get; set; }
    }

    public class AnalyticsSelectGameResultResponse : ServiceResponse
    {
        public Topic Topic { get; set; }
        public List<User> Players { get; set; }
        public List<ChallengeQuestion> GameResult { get; set; }
        public int Outcome { get; set; }
    }

    public class AnalyticsSelectLeaderboardByCompanyResponse : ServiceResponse
    {
        public List<User> TopPersonnels { get; set; }
    }
    public class AnalyticsSelectFullLeaderboardByCompanyResponse : ServiceResponse
    {
        public List<AnalyticQuiz.TopPersonnelScore> ScoreChart { get; set; }
    }

    public class AnalyticsSelectLeaderboardByDepartmentResponse : ServiceResponse
    {
        public List<Department> TopDepartments { get; set; }
    }

    public class AnalyticsSelectLeaderboardByEventResponse : ServiceResponse
    {
        public List<CassandraService.Entity.AnalyticQuiz.EventLeaderboard> TopEventLeaderboard { get; set; }
        public List<CassandraService.Entity.AnalyticQuiz.EventLeaderboard> OutOfRankingLeaderboard { get; set; }
    }

    public class AnalyticsUpdateDailyActiveUserResponse : ServiceResponse
    {
    }

    public class AnalyticMonthlyActiveUserResponse : ServiceResponse
    {
        public List<AnalyticUserActivity.MAUReport> Reports { get; set; }
    }

    public class AnalyticActivityResponse : ServiceResponse
    {
        public List<AnalyticUserActivity.ActivityReport> Reports { get; set; }
        public List<AnalyticUserActivity.ActivityBreakdownReport> BreakdownReports { get; set; }
        public double TotalTimeTaken { get; set; }
        public string TotalTimeTakenString { get; set; }
        public double AverageTimeTakenPerUser { get; set; }
        public string AverageTimeTakenPerUserString { get; set; }
    }

    public class AnalyticSelectRSResultOverviewResponse : ServiceResponse
    {
        public AnalyticSurvey.RSAnalyticOverview Overview { get; set; }
    }

    public class AnalyticSelectRSCardResultResponse : ServiceResponse
    {
        public List<RSCard> CardResults { get; set; }
    }

    public class AnalyticsSelectRSCardsResponse : ServiceResponse
    {

    }

    public class AnalyticSelectRSCustomAnswersResponse : ServiceResponse
    {
        public RSCard Card { get; set; }
    }

    public class AnalyticSelectRSResponderReportResponse : ServiceResponse
    {
        public AnalyticSurvey.RSPersonnelCompletion PersonnelCompletion { get; set; }
        public List<AnalyticSurvey.RSReportPerUser> Reports { get; set; }
    }

    public class AnalyticSelectRSCardResultByUserResponse : ServiceResponse
    {
        public AnalyticSurvey.RSReportPerUser Report { get; set; }
        public List<RSCard> CardResults { get; set; }
    }

    public class AnalyticSelectRSOptionResultResponse : ServiceResponse
    {
        public RSCard Card { get; set; }
        public List<Department> Departments { get; set; }
        public List<User> Responders { get; set; }
    }

    public class AnalyticExportRSResultResponse : ServiceResponse
    {
        public AnalyticSurvey.RSAnalyticOverview Overview { get; set; }
         public List<RSCard> CardResults { get; set; }
    }

    public class AnalyticSelectMatchUpOverallResponse : ServiceResponse
    {
        public AnalyticQuiz.MatchUpOverView Overview { get; set; }
        public AnalyticQuiz.MatchUpHeatmap HeatMap { get; set; }
        public AnalyticQuiz.MatchUpDau Dau { get; set; }
        public List<AnalyticQuiz.MatchUpSortedTopic> Topics { get; set; }
    }

    public class AnalyticSelectQuestionAttemptResponse : ServiceResponse
    {
        public List<AnalyticQuiz.MatchUpSortedQuestion> Questions { get; set; }
    }

    public class AnalyticSelectTopicAttemptResponse : ServiceResponse
    {
        public List<AnalyticQuiz.MatchUpSortedTopic> Topics { get; set; }
        public int MaxCount { get; set; }
    }

    public class AnalyticSelectTopicOverallResponse : ServiceResponse
    {
        public AnalyticQuiz.QuestionAttempt QuestionAttempt { get; set; }
        public AnalyticQuiz.TopicSummary TopicSummary { get; set; }
        public AnalyticQuiz.TopicAnalytics Analytics { get; set; }
        public List<AnalyticQuiz.MatchUpSortedQuestion> Questions { get; set; }
    }

    public class AnalyticSelectOptionAttemptResponse : ServiceResponse
    {
        public AnalyticQuiz.MatchUpSortedQuestion Question { get; set; }
        public AnalyticQuiz.AnalyticOptionDetail Options { get; set; }
        public AnalyticQuiz.TopicAnalytics Analytics { get; set; }

    }

    public class AnalyticSelectEventResultResponse : ServiceResponse
    {
        public Event Event { get; set; }
        public List<AnalyticQuiz.AnalyticEventUserScore> Score { get; set; }
    }

    public class AnalyticMatchUpBreakdownPersonnelReport : ServiceResponse
    {
        public List<AnalyticQuiz.MatchUpPersonnelBreakdownReport> Reports { get; set; }
    }

    public class AnalyticMatchUpGroupPersonnelReport : ServiceResponse
    {
        public List<AnalyticQuiz.MatchUpPersonnelGroupByReport> Reports { get; set; }
    }

    public class AnalyticLoginDetailResponse : ServiceResponse
    {
        public List<AnalyticDau.LoginDetail> LoginDetails { get; set; }
    }
}
