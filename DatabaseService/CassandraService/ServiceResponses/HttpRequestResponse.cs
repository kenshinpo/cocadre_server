﻿using Cassandra;
using CassandraService.Entity;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CassandraService.ServiceResponses
{
    public class HttpGenerateAppraisalResponse : ServiceResponse
    {
        public ISession Session { get; set; }
        public string CompanyId { get; set; }
        public string CreatorUserId { get; set; }
        public string AppraisalTitle { get; set; }
        public string SignedUrl { get; set; }
    }

    public class HttpRequestGetScheduleResponse : ServiceResponse
    {
        [DataMember]
        public List<Notification.Schedule> Schedules { get; set; }
    }
}
