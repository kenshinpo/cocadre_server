﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class CategorySelectAllResponse : ServiceResponse
    {
        public List<TopicCategory> TopicCategories { get; set; }
    }

    public class CategorySelectAllWithTopicResponse : ServiceResponse
    {
        public List<TopicCategory> TopicCategories;
        public AnalyticQuiz.Exp UserExp;
    }

    public class CategorySelectResponse : ServiceResponse
    {
        public TopicCategory TopicCategory { get; set; }
    }

    public class CategoryUpdateResponse : ServiceResponse
    {
    }

    public class CategoryDeleteResponse : ServiceResponse
    {
    }

    public class CategoryCreateResponse : ServiceResponse
    {
        public string NewCategoryId { get; set; }
    }
}
