﻿using Cassandra;
using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class AssessmentCreateResponse : ServiceResponse
    {
        public string AssessmentId { get; set; }
    }

    public class AssessmentSelectResponse : ServiceResponse
    {
        public Assessment Assessment { get; set; }
    }

    public class AssessmentSelectAllResponse : ServiceResponse
    {
        public List<Assessment> Assessments { get; set; }
    }

    public class AssessmentValidationResponse : ServiceResponse
    {
    }

    public class AssessmentCreateStatementResponse : ServiceResponse
    {
        public List<BoundStatement> Statements { get; set; }
    }

    public class AssessmentUpdateResponse : ServiceResponse
    {
    }

    public class AssessmentUpdateWithStatementReponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public List<BoundStatement> DeleteStatements { get; set; }
    }

    public class AssessmentSelectAttemptByUserResponse : ServiceResponse
    {
        public int CurrentAttempt { get; set; }
        public int NextAttempt { get; set; }
        public bool IsCurrentAttemptCompleted { get; set; }
        public int NumberOfRetakesLeft{ get; set; }
        public DateTime? LastCompletedTimestamp { get; set; }
    }

    public class AssessmentSelectResultByUserResponse : ServiceResponse
    {
        public Assessment Assessment { get; set; }
        public List<AssessmentAnswer> Results { get; set; }
        public int NumberOfRetakesAvailable { get; set; }
        public int NumberOfRetakesLeft { get; set; }
    }

    public class AssessmentSelectColorBrainPersonalityResponse : ServiceResponse
    {
        public Assessment ColorBrainAssessment { get; set; }
        public AssessmentAnswer ColorBrainPersonalityResult { get; set; }
        public string ColoredBrainCode { get; set; }
        public string ColoredBrainAboutUser { get; set; }
        public string ColoredBrainFrameColor { get; set; }
    }

    #region Card
    public class AssessmentCreateCardResponse : ServiceResponse
    {
    }
    public class AssessmentSelectCardResponse : ServiceResponse
    {
        public AssessmentCard Card { get; set; }
    }
    public class AssessmentSelectAllCardsResponse : ServiceResponse
    {
        public List<AssessmentCard> Cards { get; set; }
    }
    public class AssessmentAnswerCardResponse : ServiceResponse
    {
    }

    public class AssessmentSelectCardOptionAnsweredByUserResponse : ServiceResponse
    {
        public bool IsSelected { get; set; }
        public int PointAllocated { get; set; }
    }
    #endregion

    #region Option
    public class AssessmentCreateOptionResponse : ServiceResponse
    {
        public List<BoundStatement> Statements { get; set; }
    }

    public class AssessmentSelectAllOptionResponse : ServiceResponse
    {
        public List<AssessmentCardOption> Options { get; set; }
    }
    #endregion
}
