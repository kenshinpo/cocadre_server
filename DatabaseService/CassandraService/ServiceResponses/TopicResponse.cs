﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class TopicCreateResponse : ServiceResponse
    {
        public string TopicId { get; set; }
    }

    public class TopicSelectAllBasicResponse : ServiceResponse
    {
        public List<Topic> Topics;
    }

    public class TopicSelectIconResponse : ServiceResponse
    {
        public List<string> TopicIconUrls;
    }

    public class TopicSelectResponse : ServiceResponse
    {
        public Topic Topic;
    }

    public class TopicUpdateResponse : ServiceResponse
    {
        public string NewTopicCategoryId { get; set; }
    }

    public class TopicSelectPrivacyResponse : ServiceResponse
    {
        public bool IsForEveryone { get; set; }
        public bool IsForDepartment { get; set; }
        public bool IsForUser { get; set; }
    }

    public class TopicExportResponse : ServiceResponse
    {
        [DataMember]
        public StreamContent ZipFileStream { get; set; }
        [DataMember]
        public string FileFullName { get; set; }
    }

    public class MatchUpImportQuestionsResponse : ServiceResponse
    {
        [DataMember]
        public Topic Topic { get; set; }
        [DataMember]
        public List<ChallengeQuestion> Questions { get; set; }
    }
}
