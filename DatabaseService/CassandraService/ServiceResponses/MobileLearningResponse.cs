﻿using Cassandra;
using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    #region Category
    public class MLCategorySelectAllResponse : ServiceResponse
    {
        public List<MLTopicCategory> Categories { get; set; }
    }

    public class MLCategorySelectAllWithTopicResponse : ServiceResponse
    {
    }

    public class MLCategorySelectResponse : ServiceResponse
    {
        public MLTopicCategory Category { get; set; }
    }

    public class MLCategoryUpdateResponse : ServiceResponse
    {
    }

    public class MLCategoryDeleteResponse : ServiceResponse
    {
    }

    public class MLCategoryCreateResponse : ServiceResponse
    {
    }
    #endregion

    #region Topic
    public class MLTopicSelectAllByCategoryResponse : ServiceResponse
    {
        public List<MLTopicCategory> MLCategories { get; set; }
    }

    public class MLTopicSelectAllBasicResponse : ServiceResponse
    {
        public List<MLTopic> Topics { get; set; }
    }

    public class MLTopicSelectIconResponse : ServiceResponse
    {
    }

    public class MLTopicUpdateResponse : ServiceResponse
    {
        public MLTopic Topic { get; set; }
    }

    public class MLTopicCreateResponse : ServiceResponse
    {
        public MLTopic Topic { get; set; }
    }

    public class MLTopicSelectResponse : ServiceResponse
    {
        public MLTopic Topic { get; set; }
    }

    public class MLTopicStartResponse : ServiceResponse
    {
        public bool IsStart { get; set; }
        public string Introduction { get; set; }
        public bool IsEnd { get; set; }
        public string ClosingWords { get; set; }
        public MLCard NextCard { get; set; }
    }

    public class MLTopicUpdateFeedbackResponse : ServiceResponse
    {

    }

    public class MLTopicSelectFeedbackResponse : ServiceResponse
    {
        public List<MLTopic.MLTopicFeedback> Feedbacks { get; set; }
    }
    #endregion

    #region Card
    public class MLCardCreateResponse : ServiceResponse
    {
    }

    public class MLCardSelectAllResponse : ServiceResponse
    {
        public List<MLCard> Cards { get; set; }
    }

    public class MLCardAnswerResponse : ServiceResponse
    {
    }

    public class MLCardSelectResponse : ServiceResponse
    {
        public MLCard Card { get; set; }
    }

    public class MLCardUpdateResponse : ServiceResponse
    {
    }

    public class MLCardReorderResponse : ServiceResponse
    {
    }

    public class MLCardCheckErrorResponse : ServiceResponse
    {
    }
    #endregion

    #region Option
    public class MLOptionCreateResponse : ServiceResponse
    {
        public List<BoundStatement> BoundStatements { get; set; }
        public List<string> UploadedUrls { get; set; }
    }

    public class MLOptionUpdateResponse : ServiceResponse
    {
        public List<BoundStatement> BoundStatements { get; set; }
    }
    #endregion

    #region Activity
    public class MLUpdateActivityResponse : ServiceResponse
    {
    }
    #endregion

    public class MLValidationResponse : ServiceResponse
    {
    }

    #region Analytic
    public class MLOptionUpdateAnswerResponse : ServiceResponse
    {
        public List<BoundStatement> DeleteStatements { get; set; }
        public List<BoundStatement> UpdateStatements { get; set; }
    }

    public class MLAnswerResponse : ServiceResponse
    {
    }

    public class MLUpdateAttemptResponse : ServiceResponse
    {
        public List<BoundStatement> DeleteStatements { get; set; }
        public List<BoundStatement> UpdateStatements { get; set; }
    }

    public class MLSelectUserResultResponse : ServiceResponse
    {
        public int TotalScore { get; set; }
        public int UserScore { get; set; }
        public bool IsPassed { get; set; }
        public bool IsPassingGradeUsingPercentage { get; set; }
    }

    public class MLSelectCandidateResultResponse : ServiceResponse
    {
        public MLTopic Topic { get; set; }
        public List<AnalyticLearning.MLFilteredDepartmentSearchTerm> SearchTerms { get; set; }
        public AnalyticLearning.MLTopicCandidateResult CandidateResult { get; set; }
    }

    public class MLSelectQuestionResultResponse : ServiceResponse
    {
        public MLTopic Topic { get; set; }
        public MLCard PreviewCard { get; set; }
        public List<AnalyticLearning.MLFilteredDepartmentSearchTerm> SearchTerms { get; set; }
        public List<AnalyticLearning.MLCardToggleType> CardToggleType { get; set; }
    }

    public class MLSelectUserAttemptHistoryResponse : ServiceResponse
    {
        public MLTopic Topic { get; set; }
        public User AnsweredByUser { get; set; }
        public AnalyticLearning.MLTopicAttempt Attempt { get; set; }
        public Chart ScoreChart { get; set; }
        public Chart RankChart { get; set; }
        public List<AnalyticLearning.MLCardToggleType> CardToggleType { get; set; }
    }

    public class MLSelectSortedCardAttemptHistoryResponse : ServiceResponse
    {
        public List<MLCard> Cards { get; set; }
        public List<AnalyticLearning.MLCardToggleType> CardToggleType { get; set; }
    }

    public class MLSelectSortedUserAttemptHistoryResponse : ServiceResponse
    {
        public List<MLCard> Cards { get; set; }
        public List<AnalyticLearning.MLFilteredDepartmentSearchTerm> SearchTerms { get; set; }
    }

    public class MLSelectCardQuestionResultResponse : ServiceResponse
    {
        public MLCard PreviewCard { get; set; }
        public List<AnalyticLearning.MLFilteredDepartmentSearchTerm> SearchTerms { get; set; }
    }
    #endregion

    #region Education
    public class MLEducationCategoryCreateResponse : ServiceResponse
    {

    }

    public class MLEducationCategoryDetailResponse : ServiceResponse
    {
        public MLEducationCategory Category { get; set; }
    }

    public class MLEducationCategoryListResponse : ServiceResponse
    {
        public List<MLEducationCategory> Categories { get; set; }
    }

    public class MLEducationCategoryUpdaateResponse : ServiceResponse
    {

    }

    public class MLEducationCategoryDeleteResponse : ServiceResponse
    {

    }

    public class MLEducationListResponse : ServiceResponse
    {
        public List<MLEducation> Educations { get; set; }
    }

    public class MLEducationCreateResponse : ServiceResponse
    {
        public MLEducation Education { get; set; }
    }

    public class MLEducationUpdateResponse : ServiceResponse
    {
        public MLEducation Education { get; set; }
    }

    public class MLEducationDetailResponse : ServiceResponse
    {
        public MLEducation Education { get; set; }
    }

    public class MLEducationCardCreateResponse : ServiceResponse
    {
        public MLEducation Education { get; set; }
    }

    public class MLEducationCardUpdateResponse : ServiceResponse
    {

    }


    public class MLEducationOptionsCreateResponse : ServiceResponse
    {
        public List<BoundStatement> BoundStatements { get; set; }
        public List<string> UploadedUrls { get; set; }
    }

    public class MLEducationOptionUpdateResponse : ServiceResponse
    {
        public List<BoundStatement> BoundStatements { get; set; }
    }

    public class MLEducationCardListResponse : ServiceResponse
    {
        public List<MLEducationCard> Cards { get; set; }
    }

    public class MLEducationCardDetailResponse : ServiceResponse
    {
        public MLEducationCard Card { get; set; }
    }

    public class MLEducationCardAnswerResponse : ServiceResponse
    {

    }

    public class MLEducationAnalyticStartActivityResponse : ServiceResponse
    {

    }

    public class MLEducationAnswerResponse : ServiceResponse
    {

    }

    public class MLEducationCompletedResponse : ServiceResponse
    {

    }

    public class MLEducationAnalticResponse : ServiceResponse
    {
        public MLEducation Education { get; set; }
        public List<Department> Departments { get; set; }
        public List<AnalyticMLEducation.Attendance> Attendants { get; set; }

        public List<User> Users { get; set; }

        public User AnsweredUser { get; set; }
    }
    #endregion

    #region Assignment
    public class MLAssignmentListResponse: ServiceResponse
    {
        public List<MLAssignment> Assignments { get; set; }
    }
         

    public class MLAssignmentCreateResponse : ServiceResponse
    {
        public MLAssignment Assignment { get; set; }
    }

    public class MLAssignmentDetailResponse : ServiceResponse
    {
        public MLAssignment Assignment { get; set; }
    }

    public class MLAssignmentPrivicyResponse : ServiceResponse
    {
        public List<Department> AvailableDepartments { get; set; }
        public List<Department> UnavailableDepartments { get; set; }
        public List<User> AvailableUsers { get; set; }
        public List<User> UnavailableUsers { get; set; }
    }

    public class MLAssignmentDeleteResponse : ServiceResponse
    {
        
    }

    public class MLAssignmentUpdateResponse : ServiceResponse
    {
        public MLAssignment Assignment { get; set; }
    }

    #endregion
}
