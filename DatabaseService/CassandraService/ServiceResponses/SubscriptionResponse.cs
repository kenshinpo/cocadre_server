﻿using Cassandra;
using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class SubscriptionCreateResponse : ServiceResponse
    {
    }

    public class SubscriptionSelectAllResponse : ServiceResponse
    {
        public List<Subscription> Subscriptions { get; set; }
    }

    public class SubscriptionSelectResponse : ServiceResponse
    {
        public Subscription Subscription { get; set; }
    }

    public class SubscriptionValidationResponse : ServiceResponse
    {
    }

    public class SubscriptionDeleteResponse : ServiceResponse
    {
    }
}
