﻿using CassandraService.Entity;
using System.Collections.Generic;

namespace CassandraService.ServiceResponses
{
    public class NotificationSelectNumberResponse : ServiceResponse
    {
        public int NumberOfNotification { get; set; }
    }

    public class NotificationSelectAllResponse : ServiceResponse
    {
        public List<Notification> Notifications { get; set; }
    }

    public class NotificationUpdateSeenResponse : ServiceResponse
    {
    }

    public class TriggerScheduledNotificationResponse : ServiceResponse
    {
        public List<Notification> PushNotifications { get; set; }
    }
}
