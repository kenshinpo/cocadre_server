﻿using System.Collections.Generic;
using CassandraService.Entity;

namespace CassandraService.ServiceResponses
{
    public class GetModeratorModulesResponse : ServiceResponse
    {
        public List<Module> Modules { get; set; }
    }

    public class CompanySystemModuleDeleteResponse : ServiceResponse
    {
    }

    public class CompanySystemModuleCreateResponse : ServiceResponse
    {
    }
}
