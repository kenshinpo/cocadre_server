﻿using CassandraService.Entity;
using System.Collections.Generic;

namespace CassandraService.ServiceResponses
{
    public class BrainSelectResponse : ServiceResponse
    {
        public AnalyticQuiz.Exp UserExp { get; set; }
        public List<Topic> RecentlyPlayedTopics { get; set; }
        public List<User> RecentOpponents { get; set; }
        public List<Challenge> LatestChallenges { get; set; }
        public List<Event> Events { get; set; }
        public List<RSTopicCategory> Surveys { get; set; }
        public List<MLTopicCategory> MLearnings { get; set; }
        public List<Assessment> Assessments { get; set; }
        public List<MLEducationCategory> Educations { get; set; }
        public int ChallengeCount { get; set; }
    }
}
