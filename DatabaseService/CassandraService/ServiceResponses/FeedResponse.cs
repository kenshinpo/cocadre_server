﻿using Cassandra;
using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class FeedAuthenticationResponse : ServiceResponse
    {
        public bool IsAuthenticatedToPost { get; set; }

        public string FeedId { get; set; }
    }

    public class FeedCreateResponse : ServiceResponse
    {
        public List<Notification> TargetedNotifications { get; set; }
        public string FeedId { get; set; }
    }

    public class FeedDeleteResponse : ServiceResponse
    {
    }

    public class FeedVisibilityResponse : ServiceResponse
    {
    }

    public class FeedSelectResponse : ServiceResponse
    {
        public List<Feed> Feeds { get; set; }
        public int NumberOfNewNotification { get; set; }
    }

    public class FeedImageProgressResponse : ServiceResponse
    {
        public FeedImageProgress feedImageProgress { get; set; }
    }

    public class FeedImageProgressesResponse : ServiceResponse
    {
        public IEnumerable<FeedImageProgress> feedImageProgresses { get; set; }
    }

    public class FeedVideoProgressResponse : ServiceResponse
    {
        public FeedVideoProgress feedVideoProgress { get; set; }
    }

    public class FeedVideoProgressPubnubResponse : ServiceResponse
    {
        public string EventName;
        public string FeedId;
        public int Progress;
    }

    public class FeedImageProgressPubnubResponse : ServiceResponse
    {
        public string EventName;
        public string FeedId;
        public List<string> ImageNamesList;
        public int Progress;
    }

    public class CommentCreateResponse : ServiceResponse
    {
        public List<Notification> Notifications { get; set; }
    }

    public class CommentSelectResponse : ServiceResponse
    {
        public Feed FeedPost { get; set; }
        public List<Comment> Comments { get; set; }
    }

    public class PointUpdateResponse : ServiceResponse
    {
        public int NegativePoints { get; set; }
        public int PositivePoints { get; set; }
        public bool HasVoted { get; set; }
        public bool IsUpVoted { get; set; }
        public Notification Notification { get; set; }
    }

    public class ReplyCreateResponse : ServiceResponse
    {
        public List<Notification> Notifications { get; set; }
    }

    public class ReplySelectResponse : ServiceResponse
    {
        public Comment Comment { get; set; }
        public List<Reply> Replies { get; set; }
    }

    public class FeedUpdateResponse : ServiceResponse
    {
        public List<Notification> TargetedNotifications { get; set; }
        public string FeedId { get; set; }
    }

    public class FeedSelectPrivacyResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public bool IsForEveryone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsForUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsTaggedForUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsForDepartment { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsForGroup { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> Users { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Department> Departments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Group> Groups { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> TaggedUsers { get; set; }
    }

    public class FeedSelectUpVotersResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<User> Voters { get; set; }
    }

    public class FeedCreateHashTagResponse : ServiceResponse
    {
    }

    public class FeedUpdateHashTagResponse : ServiceResponse
    {
    }

    public class FeedSelectAllHashTagResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<FeedHashTag> RecommendedTags { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<FeedHashTag> PopularTags { get; set; }
    }

    public class FeedSearchHashTagResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<FeedHashTag> RecommendedTags { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<FeedHashTag> PopularTags { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<FeedHashTag> Tags { get; set; }
    }

    public class FeedUpdateImpressionResponse : ServiceResponse
    {
    }
}
