﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;
using Cassandra;

namespace CassandraService.ServiceResponses
{
    public class CoachCreateResponse : ServiceResponse
    {
        public string CoachId { get; set; }
    }

    public class CoachSelectResponse : ServiceResponse
    {
        public Coach Coach { get; set; }
    }

    public class CoachSelectAllResponse : ServiceResponse
    {
        public List<Coach> Coaches { get; set; }
    }

    public class CoachUpdateResponse : ServiceResponse
    {
    }

    public class CoachUpdateWithStatementResponse : ServiceResponse
    {
        public List<BoundStatement> DeleteStatements { get; set; }
        public List<BoundStatement> UpdateStatements { get; set; }
    }
}
