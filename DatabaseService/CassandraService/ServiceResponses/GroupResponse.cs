﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;

namespace CassandraService.ServiceResponses
{
    public class GroupCreateResponse : ServiceResponse
    {

    }

    public class GroupSelectResponse : ServiceResponse
    {
        public Group Group { get; set; }
    }

    public class GroupSelectAllResponse : ServiceResponse
    {
        public List<Group> Groups { get; set; }
    }

    public class GroupUpdateResponse : ServiceResponse
    {
        public List<Group> Groups { get; set; }
    }
}
