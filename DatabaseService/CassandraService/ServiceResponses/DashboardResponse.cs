﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class DashboardCreateFeedbackCommentResponse : ServiceResponse
    {
        public List<Comment> Comments { get; set; }
    }

    public class DashboardReportResponse : ServiceResponse
    {
    }

    public class DashboardSelectApprovalProfileResponse : ServiceResponse
    {
        public List<ProfileImageApproval> ProfileImageApprovals { get; set; }
    }

    public class DashboardSelectFeedbackResponse : ServiceResponse
    {
        public List<Feed> Feedbacks { get; set; }
    }

    public class DashboardSelectReportedFeedResponse : ServiceResponse
    {
        public List<Feed> FeedPosts { get; set; }
    }

    public class DashboardUpdateApprovalProfileResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public User ApprovedUser { get; set; }
    }

    public class DashboardUpdateFeedbackStatusResponse : ServiceResponse
    {
    }

    public class DashboardUpdateReportStatusResponse : ServiceResponse
    {
        public List<Comment> UpdatedComments { get; set; }
        public List<Reply> UpdatedReplies { get; set; }
    }
}
