﻿using Cassandra;
using CassandraService.Entity;
using System.Collections.Generic;

namespace CassandraService.ServiceResponses
{
    #region Appraisal
    public class AppraisalCreateResponse : ServiceResponse
    {
        public string AppraisalId { get; set; }
    }
    public class AppraisalDeleteResponse : ServiceResponse
    {
        public List<BoundStatement> DeleteStatements { get; set; }
    }
    public class AppraisalCreateTemplateResponse : ServiceResponse
    {
        public string AppraisalId { get; set; }
    }

    public class AppraisalUpdateTemplateResponse: ServiceResponse
    {
    }

    public class AppraisalValidationResponse : ServiceResponse
    {
    }

    public class AppraisalSelectAllTemplatesResponse : ServiceResponse
    {
        public List<Appraisal> Templates { get; set; }
    }

    public class AppraisalSelectTemplateResponse : ServiceResponse
    {
        public Appraisal Template { get; set; }
    }

    public class AppraisalSelectResponse : ServiceResponse
    {
        public Appraisal Appraisal { get; set; }
    }

    public class AppraisalExportResponse : ServiceResponse
    {
    }

    public class AppraisalSelectAllResponse : ServiceResponse
    {
        public List<Appraisal> CreatedAppraisals { get; set; }
        public List<Appraisal> CompletedAppraisals { get; set; }
    }

    public class AppraisalUpdateResponse : ServiceResponse
    {
    }

    public class AppraisalUpdateStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public List<BoundStatement> DeleteStatements { get; set; }
    }

    public class AppraisalUpdatePrivacyStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public List<BoundStatement> DeleteStatements { get; set; }
    }

    public class AppraisalSelectGroupTeamTypeResponse : ServiceResponse
    {
        public List<Appraisal.AppraisalGroupTeamType> GroupTeamTypes { get; set; }
    }

    public class AppraisalSelectOverallReportsResponse : ServiceResponse
    {
        public List<Appraisal> Appraisals { get; set; }
        public List<Appraisal> TemplateAppraisals { get; set; }
    }
    #endregion

    #region Category
    public class AppraisalCategorySelectAllResponse : ServiceResponse
    {
        public List<AppraisalCategory> Categories { get; set; }
    }
    public class AppraisalCategorySelectResponse : ServiceResponse
    {
        public AppraisalCategory Category { get; set; }
    }
    public class AppraisalCategoryCreateStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public string CategoryId { get; set; }
    }

    public class AppraisalCategoryCreateResponse : ServiceResponse
    {
        public string CategoryId { get; set; }
    }

    public class AppraisalUpdateCategoryResponse : ServiceResponse
    {
    }

    public class AppraisalUpdateCategoryStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public List<BoundStatement> DeleteStatements { get; set; }
    }
    #endregion

    #region Card
    public class AppraisalCardSelectTypeResponse : ServiceResponse
    {
        public List<AppraisalCard.AppraisalCardType> Types { get; set; }
    }

    public class AppraisalCardSelectResponse : ServiceResponse
    {
        public List<AppraisalCard> Cards { get; set; }
    }

    public class AppraisalCardCreateStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public string NewCardId { get; set; }
    }

    public class AppraisalCardDeleteStatementResponse : ServiceResponse
    {
        public List<BoundStatement> DeleteStatements { get; set; }
    }

    public class AppraisalCardCreateTemplateResponse : ServiceResponse
    {
        public string CardId { get; set; }
        public string CategoryId { get; set; }
    }

    public class AppraisalAnswerCardResponse : ServiceResponse
    {
        public Notification CompletionNotification { get; set; }
    }

    public class AppraisalUpdateCardResponse : ServiceResponse
    {
    }

    public class AppraisalUpdateCardStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public List<BoundStatement> DeleteStatements { get; set; }
    }
    #endregion

    #region Option
    public class AppraisalOptionUpdateStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
        public List<BoundStatement> DeleteStatements { get; set; }
    }
    public class AppraisalOptionCreateStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
    }
    public class AppraisalOptionDeleteStatementResponse : ServiceResponse
    {
        public List<BoundStatement> DeleteStatements { get; set; }
    }
    public class AppraisalOptionSelectStatementResponse : ServiceResponse
    {
        public List<AppraisalCardOption> Options { get; set; }
    }
    #endregion

    #region Range
    public class AppraisalRangeCreateStatementResponse : ServiceResponse
    {
        public List<BoundStatement> UpdateStatements { get; set; }
    }

    public class AppraisalRangeDeleteStatementResponse : ServiceResponse
    {
        public List<BoundStatement> DeleteStatements { get; set; }
    }

    public class AppraisalRangeSelectResponse : ServiceResponse
    {
        public List<AppraisalCardRange> Ranges { get; set; }
    }
    #endregion
}
