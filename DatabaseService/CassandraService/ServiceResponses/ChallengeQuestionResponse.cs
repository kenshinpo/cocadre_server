﻿using CassandraService.Entity;
using System.Collections.Generic;
using static CassandraService.Entity.Gamification;

namespace CassandraService.ServiceResponses
{
    public class QuestionAnswerResponse : ServiceResponse
    {
        public string OpponentId { get; set; }
        public string CorrectAnswer { get; set; }
        public bool IsCorrect { get; set; }
        public bool IsInitiator { get; set; }
        public bool IsLive { get; set; }
        // Remove channel if last round
        public bool IsLastRound { get; set; }
        public string AnswerOfOpponent { get; set; }
        public float TimeTakenByOpponent { get; set; }
        public Notification Notification { get; set; }
    }

    public class QuestionCreateResponse : ServiceResponse
    {
    }

    public class QuestionSelectAllResponse : ServiceResponse
    {
        public string ChallengeId;
        public List<ChallengeQuestion> Questions;
    }

    public class QuestionSelectResponse : ServiceResponse
    {
        public ChallengeQuestion Question;
    }

    public class QuestionUpdateResponse : ServiceResponse
    {
    }
}
