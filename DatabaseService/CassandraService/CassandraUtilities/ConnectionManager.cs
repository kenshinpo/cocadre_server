﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Web.Hosting;
using Cassandra;
using CassandraService.GlobalResources;
using log4net;
using Cassandra.Mapping;
using CassandraService.Entity;

namespace CassandraService.CassandraUtilities
{
    public class ConnectionManager
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        private static readonly string serverName = WebConfigurationManager.AppSettings["cassandra_server_name"];
        private static readonly string mainKeyspace = WebConfigurationManager.AppSettings["cassandra_main_keyspace"];
        private static readonly string analyticKeyspace = WebConfigurationManager.AppSettings["cassandra_analytic_keyspace"];

        private static readonly string serverIp = WebConfigurationManager.AppSettings["cassandra_server_ip"];
        private static readonly string username = WebConfigurationManager.AppSettings["cassandra_username"];
        private static readonly string password = WebConfigurationManager.AppSettings["cassandra_password"];
        private static readonly string certFileName = WebConfigurationManager.AppSettings["cassandra_cert_filename"];
        private static readonly string certKey = WebConfigurationManager.AppSettings["cassandra_cert_key"];

        private static Cluster cluster;
        private static ISession mainSession = null, analyticSession = null;

        public ISession getMainSession()
        {
            if (mainSession == null)
            {
                ConnectToKeyspace(serverName, mainKeyspace);
            }
            return mainSession;
        }

        public ISession getAnalyticSession()
        {
            if (analyticSession == null)
            {
                ConnectToKeyspace(serverName, analyticKeyspace);
            }
            return analyticSession;
        }

        private void ConfigureMainMappings()
        {
            try
            {
                #region Progresses
                Log.Debug("Initialize Mapping Configs.");
                MappingConfiguration.Global.Define(
                   new Map<FeedImageProgress>()
                      .TableName("feed_image_progress")
                      .PartitionKey(u => u.FeedId)
                      .ClusteringKey(u => u.ImageName)
                      .Column(u => u.FeedId, cm => cm.WithName("feed_id"))
                      .Column(u => u.ImageName, cm => cm.WithName("image_name"))
                      .Column(u => u.UserId, cm => cm.WithName("user_id"))
                      .Column(u => u.Progress, cm => cm.WithName("progress"))
                      .Column(u => u.Completed, cm => cm.WithName("completed"))
                      .Column(u => u.Uploaded, cm => cm.WithName("uploaded"))
                      .Column(u => u.ExpectedCount, cm => cm.WithName("expected_count"))
                      .Column(u => u.Environment, cm => cm.WithName("environment"))
                      .Column(u => u.CompanyId, cm => cm.WithName("company_id"))
                      .Column(u => u.TargetedDepartmentIds, cm => cm.WithName("targeted_department_ids"))
                      .Column(u => u.TargetedGroupIds, cm => cm.WithName("targeted_group_ids"))
                      .Column(u => u.TargetedUserIds, cm => cm.WithName("targeted_user_ids"))
                      .Column(u => u.Caption, cm => cm.WithName("caption"))
                      .Column(u => u.FeedType, cm => cm.WithName("feed_type"))
                      .Column(u => u.IsSpecificNotification, cm => cm.WithName("is_specific_notification"))
                      .Column(u => u.IsForAdmin, cm => cm.WithName("is_for_admin"))
                      .Column(u => u.IsVideoThumbnail, cm => cm.WithName("is_video_thumbnail"))
                      .Column(u => u.IsEdit, cm => cm.WithName("is_edit"))
                      .Column(u => u.Ordering, cm => cm.WithName("ordering"))
                      .Column(u => u.Url, cm => cm.WithName("url"))
                      .Column(u => u.SharedUrl, cm => cm.WithName("shared_url"))
                      .Column(u => u.UrlTitle, cm => cm.WithName("url_title"))
                      .Column(u => u.UrlSiteName, cm => cm.WithName("url_site_name"))
                      .Column(u => u.UrlDescription, cm => cm.WithName("url_description"))
                      .Column(u => u.Moved, cm => cm.WithName("moved")),
                    new Map<FeedVideoProgress>()
                        .TableName("feed_video_progress")
                        .PartitionKey(u => u.FeedId)
                        .Column(u => u.FeedId, cm => cm.WithName("feed_id"))
                        .Column(u => u.PlaylistName, cm => cm.WithName("playlist_name"))
                        .Column(u => u.UserId, cm => cm.WithName("user_id"))
                        .Column(u => u.Progress, cm => cm.WithName("progress"))
                        .Column(u => u.Completed, cm => cm.WithName("completed"))
                        .Column(u => u.Uploaded, cm => cm.WithName("uploaded"))
                        .Column(u => u.S3SrcKey, cm => cm.WithName("s3_src_key"))
                        .Column(u => u.S3OutputKeyPrefix, cm => cm.WithName("s3_output_key_prefix"))
                        .Column(u => u.VideoUrl, cm => cm.WithName("video_url"))
                        .Column(u => u.Environment, cm => cm.WithName("environment"))
                        .Column(u => u.CompanyId, cm => cm.WithName("company_id"))
                        .Column(u => u.TargetedDepartmentIds, cm => cm.WithName("targeted_department_ids"))
                        .Column(u => u.TargetedGroupIds, cm => cm.WithName("targeted_group_ids"))
                        .Column(u => u.TargetedUserIds, cm => cm.WithName("targeted_user_ids"))
                        .Column(u => u.Caption, cm => cm.WithName("caption"))
                        .Column(u => u.FeedType, cm => cm.WithName("feed_type"))
                        .Column(u => u.IsSpecificNotification, cm => cm.WithName("is_specific_notification"))
                        .Column(u => u.IsForAdmin, cm => cm.WithName("is_for_admin")),
                    new Map<UserImageProgress>()
                        .TableName("user_image_progress")
                        .PartitionKey(u => u.UserId)
                        .ClusteringKey(u => u.ImageName)
                        .Column(u => u.UserId, cm => cm.WithName("user_id"))
                        .Column(u => u.ImageName, cm => cm.WithName("image_name"))
                        .Column(u => u.Progress, cm => cm.WithName("progress"))
                        .Column(u => u.Completed, cm => cm.WithName("completed"))
                        .Column(u => u.Uploaded, cm => cm.WithName("uploaded"))
                        .Column(u => u.Environment, cm => cm.WithName("environment"))
                        .Column(u => u.CompanyId, cm => cm.WithName("company_id")),
                    new Map<CompanyImage>()
                        .TableName("company_image")
                        .PartitionKey(u => u.CompanyId)
                        .ClusteringKey(u => u.CompanyImageId)
                        .Column(u => u.CompanyId, cm => cm.WithName("company_id"))
                        .Column(u => u.CompanyImageId, cm => cm.WithName("company_image_id"))
                        .Column(u => u.Url, cm => cm.WithName("url"))
                        .Column(u => u.CompanyImageType, cm => cm.WithName("company_image_type"))
                        .Column(u => u.IsSelected, cm => cm.WithName("is_selected"))
                        .Column(u => u.CreatedByUserId, cm => cm.WithName("created_by_user_id"))
                        .Column(u => u.CreatedOnTimestamp, cm => cm.WithName("created_on_timestamp"))
                        .Column(u => u.LastModifiedByUserId, cm => cm.WithName("last_modified_by_user_id"))
                        .Column(u => u.LastModifiedTimestamp, cm => cm.WithName("last_modified_timestamp")),
                    new Map<CompanyImageDesc>()
                        .TableName("company_image_by_timestamp_desc")
                        .PartitionKey(u => u.CompanyId)
                        .ClusteringKey(u => u.CompanyImageId)
                        .Column(u => u.CompanyId, cm => cm.WithName("company_id"))
                        .Column(u => u.CompanyImageId, cm => cm.WithName("company_image_id"))
                        .Column(u => u.Url, cm => cm.WithName("url"))
                        .Column(u => u.CompanyImageType, cm => cm.WithName("company_image_type"))
                        .Column(u => u.IsSelected, cm => cm.WithName("is_selected"))
                        .Column(u => u.CreatedByUserId, cm => cm.WithName("created_by_user_id"))
                        .Column(u => u.CreatedOnTimestamp, cm => cm.WithName("created_on_timestamp"))
                        .Column(u => u.LastModifiedByUserId, cm => cm.WithName("last_modified_by_user_id"))
                        .Column(u => u.LastModifiedTimestamp, cm => cm.WithName("last_modified_timestamp")),
                    new Map<Setting>()
                        .TableName("setting")
                        .PartitionKey(wye=> wye.CompanyId)
                        .ClusteringKey(wye => wye.Key)
                        .Column(wye => wye.CompanyId, kevin => kevin.WithName("company_id"))
                        .Column(wye => wye.Key, kevin => kevin.WithName("setting_key"))
                        .Column(wye => wye.Value, kevin => kevin.WithName("setting_value")));
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }         
        }

        private void ConnectToKeyspace(string serverName, string keyspace)
        {
            try
            {
                string[] addresses = serverIp.Split(',');

                if(cluster == null)
                {
                    Log.Debug("Cluster is null, need to reinit");
                    // Basic configuration
                    Builder builder = Cluster.Builder();
                    builder.AddContactPoints(addresses);

                    cluster = Cluster.Builder()
                        .WithCredentials(username, password)
                        .AddContactPoints(addresses).Build();

                    ConfigureMainMappings();
                }
                

                if (keyspace.Equals(mainKeyspace))
                {
                    mainSession = cluster.Connect(keyspace);
                }
                else
                {
                    analyticSession = cluster.Connect(keyspace);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            try
            {
                #region for file name
                //string cert_path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", certFileName);
                #endregion
                X509Certificate2 cert2 = new X509Certificate2(Files.Cluster_Ca_Certificate, certKey);

                X509Certificate2Collection xc = new X509Certificate2Collection(cert2);

                if (certificate.Equals(cert2))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return false;
        }

        public void Close()
        {
            if (mainSession != null)
            {
                mainSession.Dispose();
            }

            if (analyticSession != null)
            {
                analyticSession.Dispose();
            }

            cluster.Shutdown();
        }
    }
}
