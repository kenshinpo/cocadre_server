﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CassandraService.CassandraUtilities
{
    public class CQLGenerator
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum Comparison
        {
            Equals,
            GreaterThan,
            GreaterThanOrEquals,
            LessThan,
            LessThanOrEquals
        }

        public static string InsertStatement(string tableName, List<string> variables)
        {
            string statement = string.Empty;
            string value = " VALUES ";

            statement += string.Format("INSERT INTO {0} ", tableName);

            for (int index = 0; index < variables.Count(); index++)
            {
                string variable = variables[index];
                if (variables.Count() == 1)
                {
                    statement += string.Format("({0})", variable);
                    value += "(?)";
                }
                else
                {
                    if (index == variables.Count() - 1)
                    {
                        statement += string.Format("{0})", variable);
                        value += "?)";
                    }
                    else if (index == 0)
                    {
                        statement += string.Format("({0},", variable);
                        value += "(?,";
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                        value += "?,";
                    }
                }
            }
            return statement += value;
        }

        public static string SelectStatementWithComparison(string tableName,
                                                           List<string> selectVariables,
                                                           List<string> whereWithoutComparisonVariable,
                                                           string comparisonVariable,
                                                           Comparison comparison,
                                                           int limit)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (selectVariables == null || (selectVariables != null && selectVariables.Count == 0))
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < selectVariables.Count(); index++)
                {
                    string variable = selectVariables[index];
                    if (selectVariables.Count() == 1 || index == selectVariables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereWithoutComparisonVariable == null)
            {
                return statement;
            }

            for (int index = 0; index < whereWithoutComparisonVariable.Count(); index++)
            {
                string variable = whereWithoutComparisonVariable[index];
                where += string.Format("{0} = ? AND ", variable);
            }

            if (limit > 0)
            {
                where += string.Format("{0} {1} {2} {3}", comparisonVariable, GenerateComparisonStatement(comparison), " LIMIT ", limit);
            }
            else
            {
                where += string.Format("{0} {1}", comparisonVariable, GenerateComparisonStatement(comparison));
            }

            return statement += where;
        }

        public static string SelectStatementWithRangeComparison(string tableName,
                                                               List<string> selectVariables,
                                                               List<string> whereWithoutComparisonVariables,
                                                               string startVariable,
                                                               Comparison startComparison,
                                                               string endVariable,
                                                               Comparison endComparison,
                                                               int limit)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (selectVariables == null || (selectVariables != null && selectVariables.Count == 0))
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < selectVariables.Count(); index++)
                {
                    string variable = selectVariables[index];
                    if (selectVariables.Count() == 1 || index == selectVariables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereWithoutComparisonVariables == null)
            {
                return statement;
            }

            for (int index = 0; index < whereWithoutComparisonVariables.Count(); index++)
            {
                string variable = whereWithoutComparisonVariables[index];
                where += string.Format("{0} = ? AND ", variable);
            }

            if (limit > 0)
            {
                where += string.Format("{0} {1} AND ", startVariable, GenerateComparisonStatement(startComparison));
                where += string.Format("{0} {1} {2} {3} ", endVariable, GenerateComparisonStatement(endComparison), " LIMIT ", limit);
            }
            else
            {
                where += string.Format("{0} {1} AND ", startVariable, GenerateComparisonStatement(startComparison));
                where += string.Format("{0} {1} ", endVariable, GenerateComparisonStatement(endComparison));
            }

            return statement += where;
        }


        public static string SelectStatementWithDateComparison(string tableName,
                                                               List<string> selectVariables,
                                                               List<string> whereWithoutDateVariables,
                                                               string dateVariable,
                                                               Comparison dateComparison,
                                                               int limit)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (selectVariables == null || (selectVariables != null && selectVariables.Count == 0))
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < selectVariables.Count(); index++)
                {
                    string variable = selectVariables[index];
                    if (selectVariables.Count() == 1 || index == selectVariables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereWithoutDateVariables == null)
            {
                return statement;
            }

            for (int index = 0; index < whereWithoutDateVariables.Count(); index++)
            {
                string variable = whereWithoutDateVariables[index];
                where += string.Format("{0} = ? AND ", variable);
            }

            if (limit > 0)
            {
                where += string.Format("{0} {1} {2} {3}", dateVariable, GenerateComparisonStatement(dateComparison), " LIMIT ", limit);
            }
            else
            {
                where += string.Format("{0} {1}", dateVariable, GenerateComparisonStatement(dateComparison));
            }

            return statement += where;
        }

        public static string SelectStatementWithDateRangeComparison(string tableName,
                                                                    List<string> selectVariables,
                                                                    List<string> whereWithoutDateVariables,
                                                                    string startDateVariable,
                                                                    Comparison startDateComparison,
                                                                    string endDateVariable,
                                                                    Comparison endDateComparison,
                                                                    int limit)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (selectVariables == null || (selectVariables != null && selectVariables.Count == 0))
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < selectVariables.Count(); index++)
                {
                    string variable = selectVariables[index];
                    if (selectVariables.Count() == 1 || index == selectVariables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereWithoutDateVariables == null)
            {
                return statement;
            }

            for (int index = 0; index < whereWithoutDateVariables.Count(); index++)
            {
                string variable = whereWithoutDateVariables[index];
                where += string.Format("{0} = ? AND ", variable);
            }

            if (limit > 0)
            {
                where += string.Format("{0} {1} AND ", startDateVariable, GenerateComparisonStatement(startDateComparison));
                where += string.Format("{0} {1} {2} {3} ", endDateVariable, GenerateComparisonStatement(endDateComparison), " LIMIT ", limit);
            }
            else
            {
                where += string.Format("{0} {1} AND ", startDateVariable, GenerateComparisonStatement(startDateComparison));
                where += string.Format("{0} {1} ", endDateVariable, GenerateComparisonStatement(endDateComparison));
            }

            return statement += where;
        }

        private static string GenerateComparisonStatement(Comparison comparison)
        {
            string comparisonString = string.Empty;
            switch (comparison)
            {
                case Comparison.Equals:
                    comparisonString = " = ?";
                    break;
                case Comparison.GreaterThan:
                    comparisonString = " > ?";
                    break;
                case Comparison.GreaterThanOrEquals:
                    comparisonString = " >= ?";
                    break;
                case Comparison.LessThan:
                    comparisonString = " < ?";
                    break;
                case Comparison.LessThanOrEquals:
                    comparisonString = " <= ?";
                    break;
            }

            return comparisonString;
        }

        public static string SelectStatement(string tableName, List<string> variables, List<string> whereVariables)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (variables.Count() == 0)
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < variables.Count(); index++)
                {
                    string variable = variables[index];
                    if (variables.Count() == 1 || index == variables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }
            return statement += where;
        }

        public static string SelectStatementWithAllowFiltering(string tableName, List<string> variables, List<string> whereVariables)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (variables.Count() == 0)
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < variables.Count(); index++)
                {
                    string variable = variables[index];
                    if (variables.Count() == 1 || index == variables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }
            return statement += where + " ALLOW FILTERING";
        }

        public static string SelectStatementWithLimit(string tableName, List<string> variables, List<string> whereVariables, int limit)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (variables.Count() == 0)
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < variables.Count(); index++)
                {
                    string variable = variables[index];
                    if (variables.Count() == 1 || index == variables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }

            string limitStatement = limit == 0 ? string.Empty : string.Format(" LIMIT {0}", limit);

            return statement += where + limitStatement;
        }

        public static string SelectWithContainStatement(string tableName, List<string> variables, List<string> whereVariables, List<string> containVariables)
        {
            string statement = string.Empty;
            string where = " WHERE ";

            statement += "SELECT ";
            if (variables.Count() == 0)
            {
                statement += "* ";
            }
            else
            {
                for (int index = 0; index < variables.Count(); index++)
                {
                    string variable = variables[index];
                    if (variables.Count() == 1 || index == variables.Count() - 1)
                    {
                        statement += variable;
                    }
                    else
                    {
                        statement += string.Format("{0},", variable);
                    }
                }
            }
            statement += string.Format(" FROM {0} ", tableName);

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            string contain = string.Empty;
            if (containVariables.Count() > 0)
            {
                string startOfContain = whereVariables.LastOrDefault();
                for (int index = 0; index < containVariables.Count(); index++)
                {
                    string variable = containVariables[index];
                    if (containVariables.Count() == 1 || index == containVariables.Count() - 1)
                    {
                        contain += string.Format("{0} CONTAINS '{1}'", startOfContain, variable);
                    }
                    else
                    {
                        contain += string.Format("{0} CONTAINS '{1}' AND ", startOfContain, variable);
                    }
                }

            }


            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else if (index == whereVariables.Count() - 1)
                {
                    where += contain;
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }
            return statement += where;
        }

        public static string DeleteStatement(string tableName, List<string> whereVariables)
        {
            string statement = " DELETE ";
            string where = " WHERE ";

            statement += string.Format("FROM {0} ", tableName);

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }
            return statement += where;
        }

        public static string UpdateDateStatement(string tableName, List<string> whereVariables, string date_variable)
        {
            string statement = string.Format("UPDATE {0} ", tableName);
            string set = " SET ";
            set += string.Format("{0} = ?", date_variable);

            string where = " WHERE ";

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }
            return statement += set + where;
        }

        public static string UpdateStatement(string tableName, List<string> whereVariables, List<string> updatedVariables, List<string> updated_values)
        {
            string statement = string.Format("UPDATE {0} ", tableName);
            string set = " SET ";

            for (int index = 0; index < updatedVariables.Count(); index++)
            {
                string variable = updatedVariables[index];

                string value = "?";
                if (updated_values.ElementAtOrDefault(index) != null)
                {
                    value = updated_values[index];
                }

                if (updatedVariables.Count() == 1 || index == updatedVariables.Count() - 1)
                {
                    set += string.Format("{0} = {1}", variable, value);
                }
                else
                {
                    set += string.Format("{0} = {1},", variable, value);
                }
            }

            string where = " WHERE ";

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }
            return statement += set + where;
        }

        public static string UpdateIfExistsStatement(string tableName, List<string> whereVariables, List<string> updatedVariables, List<string> updated_values)
        {
            string statement = string.Format("UPDATE {0} ", tableName);
            string set = " SET ";

            for (int index = 0; index < updatedVariables.Count(); index++)
            {
                string variable = updatedVariables[index];

                string value = "?";
                if (updated_values.ElementAtOrDefault(index) != null)
                {
                    value = updated_values[index];
                }

                if (updatedVariables.Count() == 1 || index == updatedVariables.Count() - 1)
                {
                    set += string.Format("{0} = {1}", variable, value);
                }
                else
                {
                    set += string.Format("{0} = {1},", variable, value);
                }
            }

            string where = " WHERE ";

            if (whereVariables.Count == 0)
            {
                return statement;
            }

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }
            return statement += set + where + "IF EXISTS";
        }

        public static string UpdateCounterStatement(string tableName, List<string> whereVariables, List<string> counterVariables, List<int> counterValues)
        {
            string statement = string.Format("UPDATE {0} ", tableName);
            string set = " SET ";
            List<string> counterStringValues = new List<string>();

            foreach (int counterValue in counterValues)
            {
                string counterStringValue = string.Empty;

                if (counterValue < 0)
                {
                    counterStringValue = string.Format("- {0}", Math.Abs(counterValue));
                }
                else
                {
                    counterStringValue = string.Format("+ {0}", Math.Abs(counterValue));
                }

                counterStringValues.Add(counterStringValue);
            }

            for (int index = 0; index < counterVariables.Count(); index++)
            {
                string variable = counterVariables[index];
                string counterString = counterStringValues[index];

                if (counterVariables.Count() == 1 || index == counterVariables.Count() - 1)
                {
                    set += string.Format("{0} =  {1} {2}", variable, variable, counterString);
                }
                else
                {
                    set += string.Format("{0} =  {1} {2},", variable, variable, counterString);
                }
            }

            if (whereVariables == null || whereVariables.Count == 0)
            {
                return statement;
            }

            string where = " WHERE ";

            for (int index = 0; index < whereVariables.Count(); index++)
            {
                string variable = whereVariables[index];
                if (whereVariables.Count() == 1 || index == whereVariables.Count() - 1)
                {
                    where += string.Format("{0} = ?", variable);
                }
                else
                {
                    where += string.Format("{0} = ? AND ", variable);
                }
            }

            statement += set + where;

            return statement;
        }

        public static String CountStatement(String tableName, List<String> whereColumnName)
        {
            String statement = String.Format("SELECT COUNT(*) FROM {0} ", tableName);

            if (whereColumnName.Count == 0)
            {
                return statement;
            }

            String where = " WHERE ";

            for (int i = 0; i < whereColumnName.Count(); i++)
            {
                if (whereColumnName.Count() == 1 || i == whereColumnName.Count() - 1)
                {
                    where += String.Format("{0} = ?", whereColumnName[i]);
                }
                else
                {
                    where += String.Format("{0} = ? AND ", whereColumnName[i]);
                }
            }
            return statement + where;
        }

        public static string OrderBy(string variable, bool isAscending)
        {
            return string.Format("ORDER BY {0} {1}", variable, isAscending ? "ASC" : "DESC");
        }

        public string TruncateStatement(string tableName)
        {
            return string.Format("TRUNCATE {0}", tableName);
        }
    }
}
