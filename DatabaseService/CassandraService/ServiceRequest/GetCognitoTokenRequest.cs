﻿using System;

namespace CassandraService.ServiceRequest
{
    [Serializable]
    public class GetCognitoTokenRequest
    {
        public string CompanyId { get; set; }
        public string UserId { get; set; }
    }
}
