﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Utilities.AWS
{
    public class S3Helper
    {
        public static void UploadImageToS3(IAmazonS3 s3Client, Byte[] fileBytes, String bucketName, String folderName, String fileName, Dictionary<String, String> metadata)
        {
            try
            {
                using (var ms = new MemoryStream(fileBytes))
                {
                    TransferUtility fileTransferUtility = new TransferUtility(s3Client);

                    TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = bucketName + "/" + folderName,
                        InputStream = ms,
                        StorageClass = S3StorageClass.Standard,
                        PartSize = ms.Length,
                        Key = fileName,
                        CannedACL = S3CannedACL.PublicRead,
                    };

                    if (metadata.ContainsKey("Width"))
                    {
                        fileTransferUtilityRequest.Metadata.Add("Width", metadata["Width"]);
                    }
                    if (metadata.ContainsKey("Height"))
                    {
                        fileTransferUtilityRequest.Metadata.Add("Height", metadata["Height"]);
                    }
                    fileTransferUtility.Upload(fileTransferUtilityRequest);
                    GetBucketLocationResponse respnese = s3Client.GetBucketLocation(bucketName + "/" + folderName);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
