﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.Entity;
using CassandraService.ServiceResponses;
using CassandraService.GlobalResources;
using CassandraService.Utilities.EmailTemplates;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Utilities
{
    public class EmailHandler
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        private static string source = "CoCadre Team <support@cocadre.com>";
        private static string accessKey = "AKIAIRFRL6Z2OYHO4R3Q";
        private static string secretKey = "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";
        private static RegionEndpoint regionEndpoint = RegionEndpoint.USWest2;
        private static AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(accessKey, secretKey, regionEndpoint);

        private enum EmailTemplateEnum
        {
            InviteUser = 1,
            CreateAdmin = 2,
            PasswordReset = 3,
            ForgetPasswordByCompany = 4,
            ReportFeed = 5,
            Live360 = 6
        }

        private enum EmailSentStatusEnum
        {
            Sent = 1,
            Error = 2,
        }

        public static EmailSendResponse SendLive360ReportEmail(string toEmail, string companyId, string title, string description, string supportInfo, string logoUrl, string userName, string userDepartment, string appraisalTitle, string pdfUrl, ISession mainSession)
        {
            EmailTemplates.EmailTemplate template = new Live360ReportTemplate(title, description, supportInfo, logoUrl, userName, userDepartment, appraisalTitle, pdfUrl);
            template.Type = (int)EmailTemplateEnum.Live360;

            // Update log
            BatchStatement batch = new BatchStatement();
            DateTime currentTime = DateTime.UtcNow;
            string logId = UUIDGenerator.GenerateUniqueIDForEmailLog();

            PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("email", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_log", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_timestamp", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            mainSession.Execute(batch);

            return SendEmail(template, logId, toEmail, mainSession);
        }

        public static EmailSendResponse SendNewPasswordEmail(string toEmail, string companyId, string title, string description, string supportInfo, string logoUrl, string newPassword, ISession mainSession)
        {
            EmailTemplates.EmailTemplate template = new NewPasswordTemplate(title, description, supportInfo, logoUrl, toEmail, newPassword);
            template.Type = (int)EmailTemplateEnum.PasswordReset;

            // Update log
            BatchStatement batch = new BatchStatement();
            DateTime currentTime = DateTime.UtcNow;
            string logId = UUIDGenerator.GenerateUniqueIDForEmailLog();

            PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("email", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_log", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_timestamp", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            mainSession.Execute(batch);

            return SendEmail(template, logId, toEmail, mainSession);
        }

        public static EmailSendResponse SendNewAdminEmail(string toEmail, string companyId, string title, string description, string supportInfo, string logoUrl, string adminPassword, ISession mainSession)
        {
            EmailTemplates.EmailTemplate template = new NewAdminTemplate(title, description, supportInfo, logoUrl, toEmail, adminPassword);
            template.Type = (int)EmailTemplateEnum.CreateAdmin;

            // Update log
            BatchStatement batch = new BatchStatement();
            DateTime currentTime = DateTime.UtcNow;
            string logId = UUIDGenerator.GenerateUniqueIDForEmailLog();

            PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("email", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_log", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_timestamp", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            mainSession.Execute(batch);

            return SendEmail(template, logId, toEmail, mainSession);
        }

        public static EmailSendResponse SendNewUserEmail(string toEmail, string companyId, string title, string description, string supportInfo, string logoUrl, string userPassword, ISession mainSession)
        {
            EmailTemplates.EmailTemplate template = new NewUserTemplate(title, description, supportInfo, logoUrl, toEmail, userPassword);
            template.Type = (int)EmailTemplateEnum.InviteUser;

            // Update log
            BatchStatement batch = new BatchStatement();
            DateTime currentTime = DateTime.UtcNow;
            string logId = UUIDGenerator.GenerateUniqueIDForEmailLog();

            PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("email", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_log", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_timestamp", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
            batch.Add(ps.Bind(companyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

            mainSession.Execute(batch);

            return SendEmail(template, logId, toEmail, mainSession);
        }

        public static EmailSendResponse SendForgetPasswordEmail(string toEmail, string title, string description, string supportInfo, string logoUrl, List<Company> companies, ISession mainSession)
        {
            EmailTemplates.EmailTemplate template = new ForgetPasswordTemplate(title, description, supportInfo, logoUrl, companies);
            template.Type = (int)EmailTemplateEnum.ForgetPasswordByCompany;

            // Update log
            BatchStatement batch = new BatchStatement();
            DateTime currentTime = DateTime.UtcNow;
            string logId = UUIDGenerator.GenerateUniqueIDForEmailLog();
            foreach (Company company in companies)
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("email", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
                batch.Add(ps.Bind(company.CompanyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_log", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
                batch.Add(ps.Bind(company.CompanyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("email_by_timestamp", new List<string> { "company_id", "log_id", "type", "status", "to_email", "sent_on_timestamp" }));
                batch.Add(ps.Bind(company.CompanyId, logId, template.Type, (int)EmailSentStatusEnum.Sent, toEmail, currentTime));
            }


            mainSession.Execute(batch);

            return SendEmail(template, logId, toEmail, mainSession);
        }

        private static EmailSendResponse SendEmail(EmailTemplates.EmailTemplate template, string logId, string toEmail, ISession mainSession)
        {
            EmailSendResponse response = new EmailSendResponse();
            response.Success = false;
            try
            {
                Destination destination = new Destination(new List<String> { toEmail });
                Message message = new Message();
                message.Subject = new Content(template.Title);
                Body mBody = new Body();
                if (template.IsHtmlFormat)
                {
                    mBody.Html = new Content(template.Body);
                }
                else
                {
                    mBody.Text = new Content(template.Body);
                }
                message.Body = mBody;

                SendEmailRequest request = new SendEmailRequest(source, destination, message);
                client.SendEmail(request);

                response.Success = true;
            }
            catch (Exception ex)
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("email_by_log", new List<string>(), new List<string> { "log_id" }));
                RowSet emailRowSet = mainSession.Execute(ps.Bind(logId));

                BatchStatement batch = new BatchStatement();
                foreach(Row emailRow in emailRowSet)
                {
                    string companyId = emailRow.GetValue<string>("company_id");
                    DateTime sentOnTimestamp = emailRow.GetValue<DateTime>("sent_on_timestamp");

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("email", new List<string> { "company_id", "log_id" }, new List<string> { "status", "error_message" }, new List<string>()));
                    batch.Add(ps.Bind((int)EmailSentStatusEnum.Error, ex.ToString(), companyId, logId));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("email_by_log", new List<string> { "log_id", "company_id" }, new List<string> { "status", "error_message" }, new List<string>()));
                    batch.Add(ps.Bind((int)EmailSentStatusEnum.Error, ex.ToString(), companyId, logId));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("email_by_timestamp", new List<string> { "company_id", "sent_on_timestamp", "log_id" }, new List<string> { "status", "error_message" }, new List<string>()));
                    batch.Add(ps.Bind((int)EmailSentStatusEnum.Error, ex.ToString(), companyId, sentOnTimestamp, logId));
                }

                mainSession.Execute(batch);

                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;
            }

            return response;
        }
    }
}
