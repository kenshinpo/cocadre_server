﻿using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Utilities
{
    public class ImageHelper
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public static ImageFormat GetImageFormat(Image img)
        {
            try
            {
                if (img.RawFormat.Equals(ImageFormat.Jpeg))
                    return ImageFormat.Jpeg;
                if (img.RawFormat.Equals(ImageFormat.Bmp))
                    return ImageFormat.Bmp;
                if (img.RawFormat.Equals(ImageFormat.Png))
                    return ImageFormat.Png;
                if (img.RawFormat.Equals(ImageFormat.Emf))
                    return ImageFormat.Emf;
                if (img.RawFormat.Equals(ImageFormat.Exif))
                    return ImageFormat.Exif;
                if (img.RawFormat.Equals(ImageFormat.Gif))
                    return ImageFormat.Gif;
                if (img.RawFormat.Equals(ImageFormat.Icon))
                    return ImageFormat.Icon;
                if (img.RawFormat.Equals(ImageFormat.MemoryBmp))
                    return ImageFormat.MemoryBmp;
                if (img.RawFormat.Equals(ImageFormat.Tiff))
                    return ImageFormat.Tiff;
                else
                    return ImageFormat.Wmf;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }

        public static Image ResizeImage(Image imgToResize, Size size, ImageFormat enuType)
        {
            try
            {
                int sourceWidth = imgToResize.Width;
                int sourceHeight = imgToResize.Height;

                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;

                nPercentW = ((float)size.Width / (float)sourceWidth);
                nPercentH = ((float)size.Height / (float)sourceHeight);

                if (nPercentH < nPercentW)
                    nPercent = nPercentH;
                else
                    nPercent = nPercentW;

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                Bitmap b;

                if (enuType == ImageFormat.Png)
                    b = new Bitmap(destWidth, destHeight, PixelFormat.Format32bppArgb);
                else if (enuType == ImageFormat.Gif)
                    b = new Bitmap(destWidth, destHeight);
                else
                    b = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                Graphics g = Graphics.FromImage((Image)b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                g.Dispose();
                return (Image)b;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }
        public static byte[] ImageToByteArray(Image image, ImageFormat imageFormat)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                image.Save(ms, imageFormat);
                byte[] imageBytes = ms.ToArray();
                return imageBytes;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }
    }
}
