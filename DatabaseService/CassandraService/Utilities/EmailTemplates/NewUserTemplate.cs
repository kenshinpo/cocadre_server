﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Utilities.EmailTemplates
{
    public class NewUserTemplate : EmailTemplate
    {
        public NewUserTemplate(string title, string description, string supportInfo, string logoUrl, string userEmail, string userPassword)
        {
            Title = title;
            IsHtmlFormat = true;
            Body = @"
            <html>
                <head>
	                <title>" + title + @"</title>
	                <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
                </head>
                <body bgcolor=""#FFFFFF"" leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">
	                <table width=""800""  border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin:0 auto;"">
                        " + GenerateHeader(title, description, logoUrl) + @"
		                <tr>
                            <td style=""height: 33px; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/topshadow.png);""></td>
                        </tr>
		                <tr>
			                <td>
				                <table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					                <tr>
						                <td width=""26"" style=""max-width:26px; color:#444444; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png);"">
						                </td>
						                <td width=""746"" style=""max-width:746px;"">
							                <table width=""746"" style=""margin:auto; max-width:746px;"" border=""0"" cellpadding=""0"" cellspacing=""0"">
                                                <tr>
									                <td width=""100""></td>
									                <td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200""><p style=""padding-top:5px; padding-bottom:5px; margin:0px;"">Your Login ID</p></td>
									                <td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">
										                <a href=""#"" style=""color:#ffab33; text-decoration:none; cursor:text; padding-top:5px; padding-bottom:5px; margin:0px;"">" + userEmail + @"</a>
									                </td>
									                <td width=""100""></td>
								                </tr>
								                <tr>
									                <td width=""100""></td>
									                <td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200""><p style=""padding-top:5px; padding-bottom:5px; margin:0px;"">Your Password</p></td>
									                <td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">
										                <a href=""#"" style=""color:#ffab33; text-decoration:none; cursor:text; padding-top:5px; padding-bottom:5px; margin:0px;"">"+ userPassword + @"</a>
									                </td>
									                <td width=""100""></td>
								                </tr>
							                </table>
						                </td>		
						                <td width=""28"" style=""max-width:26px; color:#444444; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png)"">
						                </td>
					                </tr>
				                </table>
			                </td>
		                </tr>
		                <tr>
                            <td style=""height: 33px; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/botshadow.png);""></td>
                        </tr>
		                <tr>
			                <td width=""800"" style=""padding:0 16px;"">
				                <div style=""background:#f9f9f9; text-align:center; padding-top:10px; padding-bottom:10px"">
						                <p style=""font-family:sans-serif; font-size:12px; padding:0px 180px; margin:0; color:#ccc;"">
						                " + supportInfo + @"
						                </p>
				                </div>
			                </td>
		                </tr>
		                " + GenerateFooter() + @"
	                </table>
                </body>
         </html>";
        }
    }
}
