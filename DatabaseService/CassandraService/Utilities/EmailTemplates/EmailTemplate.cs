﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Utilities.EmailTemplates
{
    public class EmailTemplate
    {
        public string Title { get; set; }
        public bool IsHtmlFormat { get; set; }
        public string Body { get; set; }
        public int Type { get; set; }

        public string GenerateFooter()
        {
            return @"
            <tr>
			    <td width=""800"" height=""150"" align=""left"">
				    <table width=""800"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					    <tr>
						    <td width=""400"" height=""150"">
							    <table width=""400"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""text-align:right;"">
								    <tr>
									    <td width=""277"">
									    </td>
									    <td width=""123"" style=""text-align:center"">
										    <a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											    <img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/store_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										    </a>
									    </td>
								    </tr>
								    <tr>
									    <td width=""277"">
									    </td>
									    <td>
										    <a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:0;"">
											    <img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										    </a>
									    </td>
								    </tr>
							    </table>
						    </td>
						    <td width=""400"" height=""150"">
							    <table width=""400"" height=""150"" border=""0"" cellpadding=""1"" cellspacing=""0"" style=""text-align:left;"">
								    <tr>
									    <td width=""123"" style=""text-align:center"">
										    <a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											    <img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										    </a>
									    </td>
									    <td width=""277"">
									    </td>
								    </tr>
								    <tr>
									    <td width=""123"">
										    <a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:0;"">
											    <img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										    </a>
									    </td>
									    <td width=""277"">
									    </td>
								    </tr>
							    </table>
						    </td>
					    </tr>
				    </table>
			    </td>
		    </tr>
		    <tr>
			    <td width=""800"" height=""62"">
			    <p style=""font-family:sans-serif; font-size:12px; padding:0 80px; margin:0; color:#ccc; text-align:center;"">
				    <img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/coicon.jpg"" width=""17"" height=""16"" alt=""Cocadre"" style=""position:relative; top:3px; padding-right:3px;""> 
				    This e-mail is generated from CoCadre automated system. If you think this is spam or you did not request for this, please report to 
				    <a href=""mailto:support@cocadre.com"" style=""text-decoration:none; color:#0076ff;"">support@cocadre.com</a> 
				    for immediate action.
			    </p>
			    </td>
		    </tr>";
        }

        public string GenerateHeader(string title, string description, string logoUrl)
        {
            return @"
            <tr>
			    <td>
				    <img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/header.png"" width=""800"" height=""63"" alt=""Cocadre"" style=""display:block;"">
			    </td>
		    </tr>
		    <tr>
			    <td width=""800"" style=""padding:0 16px;"">
				    <table width=""768"" border=""0"" cellpadding=""0"" cellspacing=""0""  bgcolor=""#f9f9f9"" style=""background-color:#f9f9f9;"">
					    <tr>
						    <td width=""217"" style=""max-width:217;"">
						    </td>
						    <td style=""padding:15px 0;"" align=""right"">
							    <table width=""250"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								    <tr>
									    <td valign=""center"" height=""40"" style=""max-width:16px;""><img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/titlebar.jpg"" width=""13"" height=""40""></td>
									    <td valign=""center""><span style=""font-family:sans-serif; font-size:18px; margin:0; padding:0;"">" + title + @"</span></td>
								    </tr>
								    <tr>
									    <td colspan=""2"" valign=""bottom"">
									    <div style=""font-family:sans-serif; font-size:12px; margin-top:5px;"">" + description + @"</div>
									    </td>
								    </tr>
							    </table>
						    </td>
						    <td style=""padding:15px 0 15px 15px;"" align=""left"" width=""83"" style=""max-width:83px;"">
							    <div style=""padding:9px; border:1px solid #ccc; max-width:65px;"">
								    <img src=""" + logoUrl + @""" width=""65"" height=""65"" alt=""Welcome to Cocadre"">
							    </div>
						    </td>
						    <td width=""217"" style=""max-width:217;"">
						    </td>
					    </tr>
				    </table>
			    </td>
		    </tr>";
        }
    }
}
