﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Utilities.EmailTemplates
{
    public class ForgetPasswordTemplate : EmailTemplate
    {
        public ForgetPasswordTemplate(string title, string description, string supportInfo, string logoUrl, List<Company> companies)
        {
            Title = title;
            IsHtmlFormat = true;

            // Generate a list of companies' logos
            string companyLogos = string.Empty;
            foreach (Company company in companies)
            {
                companyLogos += @"
							<tr width=""270"" style=""max-width:270px;"" align=""center"">
								<td width=""270"" style=""max-width:270px;"">
									<a href=""" + company.ResetPasswordUrl + @""" style=""height:55px; width:270px; height:55px; background:white; text-decoration:none"">
										<div width=""270"" height=""55"">
											<img src=""" + company.CompanyLogoUrl + @""" width=""270"" style=""height:55px; width:270px; border-radius:15px; border:1px solid #ccc;"">
										</div>
										<p style=""font-family:sans-serif; font-size:12px; color:#ffab33; margin-bottom:10px;"">
											" + company.CompanyTitle + @"
										</p>
									</a>
								</td>
							</tr>";
            }

            Body = @"
            <html>
                <head>
	                <title>" + title + @"</title>
	                <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
                </head>
                <body bgcolor=""#FFFFFF"" leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">
	                <table width=""800""  border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin:0 auto;"">
                        " + GenerateHeader(title, description, logoUrl) + @"
		                <tr>
                            <td style=""height: 33px; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/topshadow.png);""></td>
                        </tr>
		                <tr>
			                <td>
				                <table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					                <tr>
						                <td width=""26"" style=""max-width:26px; color:#444444; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png);"">
						                </td>
						                <td width=""746"" style=""max-width:746px;"">
							                <table width=""746"" style=""margin:auto; max-width:746px;"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								                <tr>
									                <td width=""300"" style=""max-width:300px;"">
										                <table width=""300"" style=""margin:auto; max-width:300px;"" border=""0"" cellpadding=""0"" cellspacing=""0"">
											                <tr align=""center"">
												                <div height=""10"" style=""height:30px; text-align:center;"">
													                <p style=""font-family:sans-serif; font-size:12px; color:#ccc;"">
													                Select the account you want to reset:
													                </p>
												                </div>
											                </tr>" + companyLogos + @"
										                </table>
									                </td>
								                </tr>
							                </table>
						                </td>		
						                <td width=""28"" style=""max-width:26px; color:#444444; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png)"">
						                </td>
					                </tr>
				                </table>
			                </td>
		                </tr>
		                <tr>
                            <td style=""height: 33px; background-image:url(https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/botshadow.png);""></td>
                        </tr>
		                <tr>
			                <td width=""800"" style=""padding:0 16px;"">
				                <div style=""background:#f9f9f9; text-align:center; padding-top:10px; padding-bottom:10px"">
						                <p style=""font-family:sans-serif; font-size:12px; padding:0px 180px; margin:0; color:#ccc;"">
						                " + supportInfo + @"
						                </p>
				                </div>
			                </td>
		                </tr>
		                " + GenerateFooter() + @"
	                </table>
                </body>
         </html>";
        }
    }
}
