﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Web.Configuration;
using Amazon.CloudFront;
using CassandraService.GlobalResources;
using log4net;

namespace CassandraService.Utilities
{
    public class AWSUrlSigner
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        private static string keyPairId = "APKAI4AK6WL5DIXLCRKA";
        private static byte[] byteArray = Files.pk_APKAI4AK6WL5DIXLCRKA;

        public static string GetSignedUrl(string url)
        {
            string AwsShouldSignUrls = WebConfigurationManager.AppSettings["AwsShouldSignUrls"];
            string s3StartUrl = "://s3-ap-southeast-1.amazonaws.com/cocadre";
            string cdnStartUrl = "://cdn";
            string signedUrl = url;

            if (AwsShouldSignUrls.Equals("true") && !String.IsNullOrEmpty(url))
            {
                // check if url is point to s3 resource
                if (url.Contains(s3StartUrl))
                {
                    // yes, must convert to cdn url
                    url = url.Replace(s3StartUrl, cdnStartUrl);

                    string[] urlTokens = url.Split('/');
                    url = string.Format("{0}/{1}/{2}.cocadre.com", urlTokens[0], urlTokens[1], urlTokens[2]);

                    for (int i = 3; i < urlTokens.Length; i++)
                    {
                        url = url + "/" + urlTokens[i];
                    }
                }

                // check if url starts with https:, replace to http:
                if (url.Contains("https:"))
                {
                    url = url.Replace("https:", "http:");
                }

                try
                {
                    // initialization
                    DateTime expiresOn = DateTime.Now.AddMonths(1); // expiration time of 10 minutes

                    // sign the url
                    Stream stream = new MemoryStream(byteArray);
                    TextReader privateKey = new StreamReader(stream);

                    signedUrl = AmazonCloudFrontUrlSigner.GetCannedSignedURL(url, privateKey, keyPairId, expiresOn);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());

                    signedUrl = null;
                }
            }

            return signedUrl;
        }
    }
}
