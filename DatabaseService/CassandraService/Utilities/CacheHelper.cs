﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CocadreMVCWebAPI.Utilities;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

namespace CassandraService.Utilities
{
    public class CacheHelper
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");
        private static string cassandra_server_name = WebConfigurationManager.AppSettings["cassandra_server_name"];
        private static int dbNum = 0;

        public static User RetrieveUserByCompany(string companyId, string userId)
        {
            List<User> users = RetrieveUsersByCompany(companyId);

            if (users != null)
            {
                // find particular user based on userId
                foreach (User user in users)
                {
                    if (user.UserId == userId)
                    {
                        // found user
                        return user;
                    }
                }
            }

            return null;
        }

        public static List<User> RetrieveUsersByCompany(string companyId)
        {
            List<User> users = new List<User>();

            // start of cache execution
            if (cassandra_server_name.Equals("staging"))
            {
                dbNum = 1;
            }

            var cache = RedisConnectorHelper.Connection.GetDatabase(dbNum);

            // retrieve moduleDictionary with respect to moduleKey 'colleagues'
            RedisValue colleaguesModule = cache.StringGet(string.Format("colleagues-{0}", companyId));

            if (!colleaguesModule.IsNull)
            {
                // check if requested company id exists inside moduleDictionary
                var colleaguesModuleDictionary = JsonConvert.DeserializeObject<JObject>(colleaguesModule);

                if (colleaguesModuleDictionary.Property(companyId) != null)
                {
                    // companyId found
                    // retrieve all departments specific to this companyId
                    UserSelectAllByDepartmentResponse response =
                        JsonConvert.DeserializeObject<UserSelectAllByDepartmentResponse>(
                            colleaguesModuleDictionary.Property(companyId).Value.ToString());

                    if (response != null)
                    {
                        // company id exists
                        List<Department> departments = response.Departments;

                        foreach (Department department in departments)
                        {
                            // retrieve users from each department
                            List<User> departmentUsers = department.Users;

                            foreach (User user in departmentUsers)
                            {
                                // add each user to final list
                                users.Add(user);
                            }
                        }
                    }
                }
            }

            return users.Count > 0 ? users : null;
        }

        public static UserSelectAllByDepartmentResponse CheckCacheForColleagues(string companyId)
        {
            // start of cache execution
            if (cassandra_server_name.Equals("staging"))
            {
                dbNum = 1;
            }

            var cache = RedisConnectorHelper.Connection.GetDatabase(dbNum);

            //Debug.WriteLine("Checking cache...");

            // retrieve moduleDictionary with respect to moduleKey 'colleagues'
            RedisValue colleaguesModule = cache.StringGet(string.Format("colleagues-{0}", companyId));

            if (!colleaguesModule.IsNull)
            {
                //Debug.WriteLine("Colleagues module exists.");

                // check if requested company id exists inside moduleDictionary
                var colleaguesModuleDictionary = JsonConvert.DeserializeObject<JObject>(colleaguesModule);

                //Debug.WriteLine("colleaguesModuleDictionary {0}", colleaguesModuleDictionary);

                if (colleaguesModuleDictionary.Property(companyId) != null)
                {
                    // companyId found
                    // retrieve all departments specific to this companyId
                    UserSelectAllByDepartmentResponse response = JsonConvert.DeserializeObject<UserSelectAllByDepartmentResponse>(colleaguesModuleDictionary.Property(companyId).Value.ToString());

                    if (response != null)
                    {
                        return response;
                    }
                }

                //Debug.WriteLine("Company id: {0} not found in colleagues module dictionary.", companyId);
                Log.Debug("Cache updated to dbNum: " + dbNum);
            }
            // end of cache execution

            return null;
        }

        public static void UpdateCacheForUserStatus(string companyId, string departmentId, string userId, int statusCode, ISession session)
        {
            // for users that are recently deleted or deleted 
            // UserStatus.RecentlyDeleted
            // UserStatus.Deleted

            //Debug.WriteLine("statusCode is {0}", statusCode.ToString());

            //Debug.WriteLine(UserStatus.RecentlyDeleted);
            //Debug.WriteLine(UserStatus.Deleted);

            // start of cache execution
            if (cassandra_server_name.Equals("staging"))
            {
                dbNum = 1;
            }

            var cache = RedisConnectorHelper.Connection.GetDatabase(dbNum);

            // retrieve moduleDictionary with respect to moduleKey 'colleagues'
            RedisValue colleaguesModule = cache.StringGet(string.Format("colleagues-{0}", companyId));

            // construct json for caching
            if (colleaguesModule.IsNull)
            {
                // do nothing if cache doesn't exist
            }
            else
            {
                // remove only the single user
                var colleaguesModuleDictionary = JsonConvert.DeserializeObject<JObject>(colleaguesModule);
                UserSelectAllByDepartmentResponse response =
                    JsonConvert.DeserializeObject<UserSelectAllByDepartmentResponse>(
                        colleaguesModuleDictionary.Property(companyId).Value.ToString());

                if (response != null)
                {
                    // company id exists
                    List<Department> departments = response.Departments;

                    foreach (Department department in departments)
                    {
                        // look for the department
                        if (department.Id == departmentId)
                        {
                            // found! 
                            //Debug.WriteLine("department id {0}", department);

                            // remove from cache only if users are recently deleted or deleted
                            if (statusCode.ToString().Equals(UserStatus.RecentlyDeleted) ||
                                statusCode.ToString().Equals(UserStatus.Deleted))
                            {

                                List<User> users = department.Users;

                                for (int i = 0; i < users.Count; i++)
                                {
                                    User user = users[i];

                                    if (user.UserId == userId)
                                    {
                                        // found existing
                                        department.Users.RemoveAt(i);

                                        //Debug.WriteLine("Removed user!");

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                // if user was restored, add back into the cache
                                if (statusCode.ToString().Equals(UserStatus.Active))
                                {
                                    PreparedStatement psRestoredUser =
                                            session.Prepare(CQLGenerator.SelectStatement("user_by_department",
                                                new List<string> { "user_id", "position" }, new List<string> { "department_id", "user_id" }));
                                    BoundStatement bsRestoredUser = psRestoredUser.Bind(departmentId, userId);

                                    Row restoredUserRow = session.Execute(bsRestoredUser).FirstOrDefault();

                                    User restoredUser = new User();
                                    restoredUser = restoredUser.SelectUserBasic(userId, companyId, true, session).User;

                                    // set position property
                                    restoredUser.Position = restoredUserRow.GetValue<string>("position");

                                    department.Users.Add(restoredUser);

                                    //Debug.WriteLine("Restored user!");
                                }
                            }

                            // stop looking for dept
                            break;
                        }
                    }

                    // update cache
                    response.Departments = departments;

                    colleaguesModuleDictionary.Property(companyId).Value = JsonConvert.SerializeObject(response);

                    //Debug.WriteLine("colleaguesModule {0}", colleaguesModuleDictionary);

                    cache.StringSet(string.Format("colleagues-{0}", companyId), JsonConvert.SerializeObject(colleaguesModuleDictionary), TimeSpan.FromDays(3));
                    //Debug.WriteLine("Cache updated!");
                    Log.Debug("Cache updated to dbNum: " + dbNum);
                }
            }
        }

        public static void UpdateCacheForCreateDepartment(string companyId, string departmentId, string departmentTitle)
        {
            // start of cache execution
            if (cassandra_server_name.Equals("staging"))
            {
                dbNum = 1;
            }

            var cache = RedisConnectorHelper.Connection.GetDatabase(dbNum);

            RedisValue colleaguesModule = cache.StringGet(string.Format("colleagues-{0}", companyId));

            if (colleaguesModule.IsNull)
            {

            }
            else
            {
                // look for department in cache and update
                var colleaguesModuleDictionary = JsonConvert.DeserializeObject<JObject>(colleaguesModule);
                JProperty property = colleaguesModuleDictionary.Property(companyId);

                if (property == null)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(property.Value.ToString()))
                {
                    UserSelectAllByDepartmentResponse response =
                        JsonConvert.DeserializeObject<UserSelectAllByDepartmentResponse>(property.Value.ToString());

                    if (response != null)
                    {
                        // company id exists
                        List<Department> departments = response.Departments;

                        bool departmentExists = false;

                        foreach (Department department in departments)
                        {
                            // look for the department
                            if (department.Id == departmentId)
                            {
                                departmentExists = true;

                                break;
                            }
                        }

                        if (!departmentExists)
                        {
                            // save to create
                            Department newDepartment = new Department
                            {
                                Id = departmentId,
                                Title = departmentTitle,
                                Users = new List<User>()
                            };

                            departments.Add(newDepartment);

                            // update cache
                            response.Departments = departments;

                            colleaguesModuleDictionary.Property(companyId).Value = JsonConvert.SerializeObject(response);

                            cache.StringSet(string.Format("colleagues-{0}", companyId), JsonConvert.SerializeObject(colleaguesModuleDictionary), TimeSpan.FromDays(3));
                            Log.Debug("Cache updated to dbNum: " + dbNum);
                        }
                    }
                }
            }
        }

        public static void UpdateCacheForDepartment(string companyId, string departmentId, string departmentTitle, bool delete = false)
        {
            // start of cache execution
            if (cassandra_server_name.Equals("staging"))
            {
                dbNum = 1;
            }

            var cache = RedisConnectorHelper.Connection.GetDatabase(dbNum);

            RedisValue colleaguesModule = cache.StringGet(string.Format("colleagues-{0}", companyId));

            if (colleaguesModule.IsNull)
            {

            }
            else
            {
                // look for department in cache and update
                var colleaguesModuleDictionary = JsonConvert.DeserializeObject<JObject>(colleaguesModule);
                JProperty property = colleaguesModuleDictionary.Property(companyId);

                if (property == null)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(property.Value.ToString()))
                {
                    UserSelectAllByDepartmentResponse response =
                        JsonConvert.DeserializeObject<UserSelectAllByDepartmentResponse>(property.Value.ToString());

                    if (response != null)
                    {
                        // company id exists
                        List<Department> departments = response.Departments;

                        foreach (Department department in departments)
                        {
                            // look for the department
                            if (department.Id == departmentId)
                            {
                                // update department
                                department.Title = departmentTitle;

                                if (delete)
                                {
                                    // remove department from cache
                                    departments.Remove(department);
                                }

                                break;
                            }
                        }

                        // update cache
                        response.Departments = departments;

                        colleaguesModuleDictionary.Property(companyId).Value = JsonConvert.SerializeObject(response);

                        //Debug.WriteLine("colleaguesModule {0}", colleaguesModuleDictionary);
                        cache.StringSet(string.Format("colleagues-{0}", companyId), JsonConvert.SerializeObject(colleaguesModuleDictionary), TimeSpan.FromDays(3));
                        Log.Debug("Cache updated to dbNum: " + dbNum);
                    }
                }
            }
        }

        // used for both create and update colleague
        public static void UpdateCacheForColleague(string companyId, string departmentId, string departmentTitle, string userId, string email, string firstName, string lastName, string profileImageUrl, string position, string originalDepartmentId = null)
        {
            try
            {
                // start of cache execution
                if (cassandra_server_name.Equals("staging"))
                {
                    dbNum = 1;
                }

                var cache = RedisConnectorHelper.Connection.GetDatabase(dbNum);

                // retrieve moduleDictionary with respect to moduleKey 'colleagues'
                RedisValue colleaguesModule = cache.StringGet(string.Format("colleagues-{0}", companyId));

                // construct json for caching
                if (colleaguesModule.IsNull)
                {
                    // do nothing if cache doesn't exist
                }
                else
                {
                    // update only the single user
                    var colleaguesModuleDictionary = JsonConvert.DeserializeObject<JObject>(colleaguesModule);
                    JProperty property = colleaguesModuleDictionary.Property(companyId);
                    if (property == null)
                    {
                        return;
                    }
                    if (!string.IsNullOrEmpty(property.Value.ToString()))
                    {
                        UserSelectAllByDepartmentResponse response = JsonConvert.DeserializeObject<UserSelectAllByDepartmentResponse>(property.Value.ToString());

                        if (response != null)
                        {
                            // company id exists
                            List<Department> departments = response.Departments;

                            if (originalDepartmentId != null && !originalDepartmentId.Equals(departmentId))
                            {
                                // department has changed

                                foreach (Department department in departments)
                                {
                                    // goto to old department and delete user
                                    if (department.Id == originalDepartmentId)
                                    {
                                        // found! 
                                        //Debug.WriteLine("department id {0}", department);

                                        List<User> users = department.Users;
                                        User updateUser = new User();

                                        // is it new user?
                                        for (int i = 0; i < users.Count; i++)
                                        {
                                            User user = users[i];

                                            if (user.UserId == userId)
                                            {
                                                // found existing
                                                updateUser = user;

                                                for (int d = 0; d < department.Users.Count; d++)
                                                {
                                                    User departmentUser = department.Users[d];

                                                    if (departmentUser.UserId == userId)
                                                    {
                                                        // remove user from this department
                                                        department.Users.RemoveAt(d);

                                                        break;
                                                    }
                                                }

                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            bool departmentExists = false;

                            foreach (Department department in departments)
                            {
                                // look for the department
                                if (department.Id == departmentId)
                                {
                                    // found! 
                                    //Debug.WriteLine("department id {0}", department);
                                    departmentExists = true;

                                    List<User> users = department.Users;
                                    bool isNew = true;
                                    User updateUser = new User();

                                    // is it new user?
                                    for (int i = 0; i < users.Count; i++)
                                    {
                                        User user = users[i];

                                        if (user.UserId == userId)
                                        {
                                            // found existing
                                            updateUser = user;

                                            updateUser.Email = email;
                                            updateUser.FirstName = firstName;
                                            updateUser.LastName = lastName;
                                            updateUser.ProfileImageUrl = profileImageUrl;
                                            updateUser.Position = position;
                                            updateUser.Status = new User.AccountStatus(User.AccountStatus.CODE_ACTIVE);

                                            department.Users[i] = updateUser;

                                            isNew = false;
                                            break;
                                        }
                                    }

                                    if (isNew)
                                    {
                                        // yes this is a new user
                                        updateUser = new User
                                        {
                                            UserId = userId,
                                            Email = email,
                                            FirstName = firstName,
                                            LastName = lastName,
                                            ProfileImageUrl = profileImageUrl,
                                            Position = position,
                                            Status = new User.AccountStatus(User.AccountStatus.CODE_ACTIVE)
                                        };

                                        department.Users.Add(updateUser);
                                    }

                                    //Debug.WriteLine("user: {0}", updateUser);
                                    //Debug.WriteLine("Added to object.");

                                    break; // do not continue to search
                                }
                            }

                            if (!departmentExists)
                            {
                                // create new department
                                Department newDepartment = new Department
                                {
                                    Id = departmentId,
                                    Title = departmentTitle,
                                    Users = new List<User>()
                                };

                                // add new user to department
                                User newUser = new User
                                {
                                    UserId = userId,
                                    Email = email,
                                    FirstName = firstName,
                                    LastName = lastName,
                                    ProfileImageUrl = profileImageUrl,
                                    Position = position,
                                    Status = new User.AccountStatus(User.AccountStatus.CODE_ACTIVE)
                                };

                                newDepartment.Users.Add(newUser);
                                departments.Add(newDepartment);
                            }

                            // update cache
                            response.Departments = departments;

                            colleaguesModuleDictionary.Property(companyId).Value = JsonConvert.SerializeObject(response);

                            //Debug.WriteLine("colleaguesModule {0}", colleaguesModuleDictionary);
                            cache.StringSet(string.Format("colleagues-{0}", companyId), JsonConvert.SerializeObject(colleaguesModuleDictionary), TimeSpan.FromDays(3));
                            //Debug.WriteLine("Cache updated!");
                            Log.Debug("Cache updated to dbNum: " + dbNum);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Debug(ex.ToString());
            }

        }

        public static void UpdateCacheForColleagues(string companyId, UserSelectAllByDepartmentResponse updatedResponse)
        {
            try
            {
                // start of cache execution
                if (cassandra_server_name.Equals("staging"))
                {
                    dbNum = 1;
                }

                var cache = RedisConnectorHelper.Connection.GetDatabase(dbNum);

                // retrieve moduleDictionary with respect to moduleKey 'colleagues'
                RedisValue colleaguesModule = cache.StringGet(string.Format("colleagues-{0}", companyId));

                // construct json for caching
                if (colleaguesModule.IsNull) // colleagues module has not been created yet, this is the first time
                {
                    //Debug.WriteLine("Creating new module for the first time...");

                    var companyObject = new JObject();
                    companyObject.Add(companyId, JsonConvert.SerializeObject(updatedResponse));

                    JObject colleaguesObject = JObject.FromObject(companyObject);
                    // array of companyid keys with department values

                    //Debug.WriteLine("colleaguesObject: {0}", colleaguesObject);

                    // set to cache
                    cache.StringSet(string.Format("colleagues-{0}", companyId), JsonConvert.SerializeObject(colleaguesObject), TimeSpan.FromDays(3));
                }
                else // colleagues module already exists
                {
                    //Debug.WriteLine("Colleagues module already exists, will add to existing.");

                    // add to the colleagues module
                    var colleaguesModuleDictionary = JsonConvert.DeserializeObject<JObject>(colleaguesModule);
                    colleaguesModuleDictionary.Add(companyId, JsonConvert.SerializeObject(updatedResponse)); // add UserSelectAllByDepartmentResponse to current companyId key

                    //Debug.WriteLine("colleaguesModuleDictionary: {0}", colleaguesModuleDictionary);

                    // set to cache
                    cache.StringSet(string.Format("colleagues-{0}", companyId), JsonConvert.SerializeObject(colleaguesModuleDictionary), TimeSpan.FromDays(3));
                }

                //Debug.WriteLine("Caching done!");
                Log.Debug("Cache updated to dbNum: " + dbNum);
            }
            catch (Exception ex)
            {
                Log.Debug(ex.ToString());
            }

        }
    }

    public class CachedImagesResizeProgressResponse
    {
        public int ExpectedImagesCount;
        public List<CachedImageResponse> Images;
        public float TotalProgress;
        public bool ProcessCompleted;
    }

    public class CachedImageResponse
    {
        public string Name;
        public float Progress;
        public string FeedId;
    }
}
