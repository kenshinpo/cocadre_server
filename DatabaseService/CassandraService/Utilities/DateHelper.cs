﻿using Cassandra;
using CassandraService.CassandraUtilities;
using log4net;
using System;
using System.Linq;
using System.Collections.Generic;

namespace CassandraService.Utilities
{
    public class DateHelper
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long ConvertDateToLong(DateTime DateTimeUtc)
        {
            long dateLong = (long)Math.Round((DateTimeUtc - epoch).TotalMilliseconds);

            // Remove milliseconds
            dateLong = (dateLong / 1000) * 1000;

            return dateLong;
        }

        public static DateTime ConvertLongToDate(long dateLong)
        {
            DateTime dateUtc = epoch.AddMilliseconds(dateLong).ToUniversalTime();

            return dateUtc;
        }

        public static double SelectTimeOffsetForCompany(string companyId, ISession session)
        {
            double timezoneOffset = 0.0;

            try
            {
                PreparedStatement psCompany = session.Prepare(CQLGenerator.SelectStatement("company",
                       new List<string>(), new List<string> { "id" }));
                Row companyRow = session.Execute(psCompany.Bind(companyId)).FirstOrDefault();

                if (companyRow != null)
                {
                    timezoneOffset = companyRow["timezone_offset"] != null ? companyRow.GetValue<double>("timezone_offset") : 8.0;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return timezoneOffset;
        }

        public static T ConvertDateToTimezoneSpecific<T>(T time, string companyId, ISession session = null)
        {
            object timezoneConverted = new object();

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement psCompany = session.Prepare(CQLGenerator.SelectStatement("company",
                    new List<string>(), new List<string> { "id" }));
                Row companyRow = session.Execute(psCompany.Bind(companyId)).FirstOrDefault();

                if (companyRow != null)
                {
                    double timezoneOffset = companyRow.GetValue<double>("timezone_offset");

                    if (time.GetType() == typeof(DateTime))
                    {
                        DateTime currentTime = (DateTime)(object)time;
                        timezoneConverted = (object)currentTime.AddHours(timezoneOffset);
                    }
                    else if (time.GetType() == typeof(DateTimeOffset))
                    {
                        DateTimeOffset currentTime = (DateTimeOffset)(object)time;
                        timezoneConverted = (object)currentTime.AddHours(timezoneOffset);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return (T)(object)timezoneConverted;
        }
    }
}
