﻿using System;
using CassandraService.GlobalResources;

namespace CassandraService.Utilities
{
    /// <summary> 
    /// Generate unique IDs for DB
    /// </summary> 
    public class UUIDGenerator
    {
        #region Account
        public static String GenerateNumberPasswordForUser()
        {
            Random random = new Random();
            string id = random.Next(10000, 99999).ToString();
            return id;
        }

        public static String GenerateDatePasswordForUser()
        {
            //string id = DateTime.Now.ToString("ddMMyyyy");
            string id = "18042016";
            return id;
        }

        public static String GeneratePasswordForUser()
        {
            string id = Guid.NewGuid().ToString();
            return id.Replace("-", "").Substring(0, 6);
        }

        public static String GenerateResetPasswordTokenForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ResetPasswordToken + id.Replace("-", "");
        }

        public static String GenerateUniqueIDForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.User + id.Replace("-", "");
        }

        public static String GenerateUniqueIDForCompany()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Company + id.Replace("-", "");
        }

        public static String GenerateUniqueIDForCompanyMedia()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.CompanyMedia + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForDepartment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Department + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForCompanyJob()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.CompanyJob + id.Replace("-", "");
        }
        #endregion

        #region Feed
        public static string GenerateUniqueIDForFeedPrivacy()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedPrivacy + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeed()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Feed + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedComment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedComment + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedCommentReply()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedCommentReply + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedMedia()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedMedia + id.Replace("-", "");
        }
        #endregion

        #region Challenge
        public static string GenerateUniqueIDForTopicCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeTopicCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForTopic()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeTopic + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuestion()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeQuestion + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuizChallenge()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Challenge + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuizChallengeChoice()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeQuestionChoice + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForExpAllocation()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeExpAllocation + id.Replace("-", "");
        }
        #endregion

        #region Log
        public static string GenerateUniqueIDForLog()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Log + id.Replace("-", "");
        }
        #endregion

        #region User Token
        public static string GenerateUniqueTokenForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AuthenticationToken + id.Replace("-", "");
        }
        #endregion

        #region Notification
        public static string GenerateUniqueIDForFeedNewPostNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedNewPostNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedNewCommentNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedNewCommentNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedNewReplyNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedNewReplyNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedUpvotePostNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedUpvotePostNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedUpvoteCommentNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedUpvoteCommentNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedUpvoteReplyNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedUpvoteReplyNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedMentionedCommentNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedMentionedCommentNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedMentionedReplyNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedMentionedReplyNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForChallengeCompletedNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeCompletedNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForChallengeRequestNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeRequestNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForAchievementUnlockedNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AchievementUnlockedNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForAnnouncementNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.PulseAnnouncementNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForAppraisalNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.PulseAppraisalNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForScheduledNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ScheduledNotificationTask + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForAppraisalReportNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AppraisalResultNotification + id.Replace("-", "");
        }
        #endregion

        #region Dashboard
        public static string GenerateUniqueIDForReport()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Report + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForProfileApproval()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ProfileApproval + id.Replace("-", "");
        }
        #endregion

        #region Event
        public static string GenerateUniqueIDForEvent()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Event + id.Replace("-", "");
        }
        #endregion

        #region Responsive Survey
        public static string GenerateUniqueIDForRSTopic()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSTopic + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSCard()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSCard + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSOption(bool isCustom)
        {
            string id = Guid.NewGuid().ToString();

            if (isCustom)
            {
                return PrefixId.RSOptionCustom + id.Replace("-", "");
            }

            return PrefixId.RSOption + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSImage()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSImage + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSSimilar()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSSimilar + id.Replace("-", "");
        }
        #endregion

        #region Pulse
        public static string GenerateUniqueIDForApprasialPulse()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.PulseFeed + PrefixId.Appraisal + id.Replace("-", "");
        }
        public static string GenerateUniqueIDForPulse()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.PulseFeed + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForPulseDeck()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.PulseDynamicDeck + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForPulseDynamic()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.PulseDynamicCard + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForPulseSimilar()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.PulseSimilar + id.Replace("-", "");
        }
        #endregion

        #region Search
        public static string GenerateUniqueIDForSearch()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Search + id.Replace("-", "");
        }
        #endregion

        #region Coach
        public static String GenerateUniqueIDForCoach()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Coach + id.Replace("-", "");
        }
        #endregion

        #region Assessment
        public static String GenerateUniqueIDForAssessment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Assessment + id.Replace("-", "");
        }
        public static String GenerateUniqueIDForAssessmentCard()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AssessmentCard + id.Replace("-", "");
        }
        public static String GenerateUniqueIDForAssessmentTabulation()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AssessmentTabulation + id.Replace("-", "");
        }
        public static String GenerateUniqueIDForAssessmentAnswer()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AssessmentAnswer + id.Replace("-", "");
        }
        public static String GenerateUniqueIDForAssessmentCardOption()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AssessmentCardOption + id.Replace("-", "");
        }
        #endregion

        #region Group
        public static String GenerateUniqueIDForGroup()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Group + id.Replace("-", "");
        }
        #endregion

        #region Mobile Learning
        public static string GenerateUniqueIDForMLTopic()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLTopic + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLCard()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLCard + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLOption()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLOption + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLUploadedContent()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLUploadContent + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLEducation()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLEducation + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLEducationCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLEducationCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLEducationCard()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLEducationCard + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLEducationOption()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLEducationOption + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForMLAssignment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.MLAssignment + id.Replace("-", "");
        }
        #endregion

        #region Gamification
        public static string GenerateUniqueIDForAchievement()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Achievement + id.Replace("-", "");
        }
        #endregion

        #region Appraisal 360
        public static string GenerateUniqueIDForAppraisalCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AppraisalCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForAppraisalOption()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AppraisalCardOption + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForAppraisalCard()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AppraisalCard + id.Replace("-", "");
        }
        public static string GenerateUniqueIDForAppraisal()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Appraisal + id.Replace("-", "");
        }
        #endregion

        #region Email
        public static string GenerateUniqueIDForEmailLog()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.EmailLog + id.Replace("-", "");
        }
        #endregion
    }
}
