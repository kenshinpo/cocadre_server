﻿using System;
using System.Web.Configuration;
using StackExchange.Redis;

namespace CocadreMVCWebAPI.Utilities
{
    public class RedisConnectorHelper
    {
        private static Lazy<ConnectionMultiplexer> _lazyConnection = null;
        private static readonly string endpoint = WebConfigurationManager.AppSettings["redis_server_endpoint"];
        private static readonly string port = WebConfigurationManager.AppSettings["redis_server_port"];

        public static ConnectionMultiplexer Connection
        {
            get
            {
                if (_lazyConnection == null)
                {
                    _lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
                    {
                        var connectionString = string.Format("{0}:{1},{2},{3}", endpoint, port, "abortConnect=false", "syncTimeout=100000");

                        return ConnectionMultiplexer.Connect(connectionString);
                    });
                }

                return _lazyConnection.Value;
            }
        }
    }
}