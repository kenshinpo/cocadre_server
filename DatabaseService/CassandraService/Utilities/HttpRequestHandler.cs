﻿using Cassandra;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Utilities
{
    public class HttpRequestHandler
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        private static readonly HttpClient client = new HttpClient();

        public async void MakeTaskScheduleRequest(string scheduledId, string companyId, string groupName, DateTime notifiedTimestamp)
        {
            string apiEndpoint = WebConfigurationManager.AppSettings["scheduler_endpoint"];

            var values = new Dictionary<string, object>
                        {
                           { "ScheduledId", scheduledId },
                           { "CompanyId", companyId },
                           { "GroupName", groupName },
                           { "ToBeNotifiedAt", notifiedTimestamp }
                        };

            HttpContent content = new StringContent(JsonConvert.SerializeObject(values), Encoding.UTF8, "application/json");

            var response = await client.PostAsync(apiEndpoint, content);

            var responseString = await response.Content.ReadAsStringAsync();

            Log.Debug(responseString);
        }

        public async Task<HttpGenerateAppraisalResponse> MakeAppraisalReportRequest(string companyId, 
                                                                                    string creatorUserId,  
                                                                                    string appraisalTitle, 
                                                                                    string appraisalId,
                                                                                    ISession session)
        {
            HttpGenerateAppraisalResponse response = new HttpGenerateAppraisalResponse();
            response.Session = session;
            response.CompanyId = companyId;
            response.CreatorUserId = creatorUserId;
            response.AppraisalTitle = appraisalTitle;
            response.Success = false;
            try
            {
                string apiEndpoint = WebConfigurationManager.AppSettings["report_endpoint"];
                string environment = "development";
                if (WebConfigurationManager.AppSettings["cassandra_server_name"].Equals("prod"))
                {
                    environment = "production";
                }
                else if (WebConfigurationManager.AppSettings["cassandra_server_name"].Equals("staging"))
                {
                    environment = "staging";
                }

                var values = new Dictionary<string, string>
                        {
                           { "CompanyId", companyId },
                           { "CreatorUserId", creatorUserId },
                           { "AppraisalId", appraisalId },
                           { "Template", "live360" },
                           { "Environment", environment }
                        };

                string request = JsonConvert.SerializeObject(values);
                HttpContent content = new StringContent(request, Encoding.UTF8, "application/json");

                var httpResponse = await client.PostAsync(apiEndpoint, content);

                var responseString = await httpResponse.Content.ReadAsStringAsync();

                AppraisalHttpReportResponse jsonResponse = JsonConvert.DeserializeObject<AppraisalHttpReportResponse>(responseString);
                if (jsonResponse.Success)
                {
                    response.Success = true;
                    response.SignedUrl = jsonResponse.Url;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        [Serializable]
        [DataContract]
        public class AppraisalHttpReportResponse
        {
            [JsonProperty(PropertyName = "success")]
            public bool Success { get; set; }

            [JsonProperty(PropertyName = "signedUrl")]
            public string Url { get; set; }
        }
    }
}
