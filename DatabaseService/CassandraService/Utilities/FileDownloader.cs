﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace CassandraService.Utilities
{
    public class FileDownloader
    {
        #region Get image from Url
        /// <summary>
        /// Get image from Url
        /// </summary>
        /// <param name="strUrl">Image's Url path</param>
        /// <returns>return System.Drawing.Image object</returns>
        static public System.Drawing.Image GetImageFromURL(string strUrl)
        {
            System.Drawing.Image image = null;

            try
            {
                WebRequest webRequest = WebRequest.Create(strUrl);
                WebResponse webResponse = webRequest.GetResponse();
                Stream stream = webResponse.GetResponseStream();
                image = System.Drawing.Image.FromStream(stream);
                stream.Close();
                stream.Dispose();
                webResponse.Close();
                webResponse = null;
                webRequest = null;

            }
            catch (Exception ex)
            {
                throw new Exception("GetImageFromURL(string strUrl) method exception. Cannot grab image online. Url: " + strUrl);
            }
            return image;
        }
        #endregion
    }
}
