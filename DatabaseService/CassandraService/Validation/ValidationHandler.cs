﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.Utilities;
using log4net;

namespace CassandraService.Validation
{
    public class ValidationHandler
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        #region User/Account
        public ErrorStatus isValidatedAsAdmin(string adminUserId,
                                              string companyId,
                                              ISession session)
        {
            #region Step 1. Check SuperAdmin
            PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
            Row user = session.Execute(ps.Bind(adminUserId)).FirstOrDefault();
            if (user != null)
            {
                if (user.GetValue<int>("account_status") == User.AccountStatus.CODE_ACTIVE && user.GetValue<int>("account_type") == User.AccountType.CODE_SUPER_ADMIN)  // The user is Super Admin
                {
                    return null;
                }


                if (user.GetValue<int>("account_status") == User.AccountStatus.CODE_ACTIVE && user.GetValue<int>("account_type") == User.AccountType.CODE_COCADRE_ADMIN)  // The user is Cocadre Admin
                {
                    return null;
                }
            }
            #endregion

            #region Step 2. Check Admin & Moderater
            Row company_row = ValidateCompany(companyId, session);
            if (company_row != null)
            {
                Row user_row = ValidateUser(adminUserId, companyId, session);

                if (user_row != null)
                {
                    if (ValidateAdminAccountType(adminUserId, companyId, session))
                    {
                        return null;
                    }
                    else
                    {
                        return ErrorHandler.AccountIsNotAdmin;
                    }
                }
                else
                {
                    return ErrorHandler.InvalidUserAccount;
                }
            }
            return ErrorHandler.InvalidCompany;
            #endregion
        }

        public ErrorStatus isValidatedAsUser(string userId,
                                             string companyId,
                                            ISession session)
        {
            Row companyRow = ValidateCompany(companyId, session);

            if (companyRow != null)
            {
                Row userRow = ValidateUser(userId, companyId, session);

                if (userRow != null)
                {
                    return null;
                }
                else
                {
                    return ErrorHandler.InvalidUserAccount;
                }
            }

            return ErrorHandler.InvalidCompany;
        }

        public Row ValidateCompany(string companyId, ISession session)
        {
            Row companyRow = null;

            try
            {
                if (!string.IsNullOrEmpty(companyId))
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company",
                        new List<string>(), new List<string> { "id", "is_valid" }));
                    BoundStatement bs = ps.Bind(companyId, true);
                    companyRow = session.Execute(bs).FirstOrDefault();
                }
                else
                {
                    Log.Error("CompanyId is empty -> Cannot validate company");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return companyRow;
        }

        public Row ValidateDepartment(string departmentId,
                                      string companyId,
                                      ISession session)
        {
            Row departmentRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("department",
                                      new List<string>(), new List<string> { "id", "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(departmentId, companyId, true);
                departmentRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return departmentRow;
        }

        public Row ValidateUser(string userId,
                                string companyId,
                                ISession session)
        {
            Row userRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string>(), new List<string> { "user_id", "account_status" }));
                BoundStatement bs = ps.Bind(userId, User.AccountStatus.CODE_ACTIVE);
                Row userAccountTypeRow = session.Execute(bs).FirstOrDefault();
                if (userAccountTypeRow != null)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string> { "id" }));
                    bs = ps.Bind(userId);
                    userRow = session.Execute(bs).FirstOrDefault();
                    if (userRow != null)
                    {
                        return userRow;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return userRow;
        }

        public bool ValidateAdminAccountType(string userId, string companyId, ISession session)
        {
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { "account_type" }, new List<string> { "user_id" }));
                BoundStatement bs = ps.Bind(userId);
                Row admin_row = session.Execute(bs).FirstOrDefault();
                if (admin_row != null)
                {
                    if (admin_row.GetValue<int>("account_type") == User.AccountType.CODE_ADMIN || admin_row.GetValue<int>("account_type") == User.AccountType.CODE_SUPER_ADMIN)
                    {
                        return true;
                    }
                    else if (admin_row.GetValue<int>("account_type") == User.AccountType.CODE_MODERATER)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("moderator_access_management", new List<string> { }, new List<string> { "user_id" }));
                        Row row = session.Execute(ps.Bind(userId)).FirstOrDefault();
                        if (row != null)
                        {
                            DateTime dt = row.GetValue<DateTime?>("expired_timestamp") ?? DateTime.Now;
                            if (dt < DateTime.UtcNow)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return false;
        }
        #endregion

        #region Challenge Topic/Question/Category
        public ErrorStatus isValidatedAsNewTopic(string topicTitle, int selectedNumberOfQuestions)
        {
            if (string.IsNullOrEmpty(topicTitle))
            {
                return ErrorHandler.TopicTitleIsEmpty;
            }
            else if (selectedNumberOfQuestions < Int16.Parse(WebConfigurationManager.AppSettings["minimum_selected_questions_setting"].ToString()))
            {
                return ErrorHandler.TopicLessSelectedQuestions;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewTopicCategory(string topicCategoryTitle)
        {
            if (string.IsNullOrEmpty(topicCategoryTitle))
            {
                return ErrorHandler.TopicCategoryTitleIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionContent(string questionContent)
        {
            if (string.IsNullOrEmpty(questionContent))
            {
                return ErrorHandler.TopicQuestionIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionContent(string questionContentImageUrl, string questionContentMd5)
        {
            if (string.IsNullOrEmpty(questionContentImageUrl) || string.IsNullOrEmpty(questionContentMd5))
            {
                return ErrorHandler.TopicQuestionIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionChoiceContent(string firstChoiceContent, string secondChoiceContent, string thirdChoiceContent, string fourthChoiceContent)
        {
            if (string.IsNullOrEmpty(firstChoiceContent) || string.IsNullOrEmpty(secondChoiceContent) || string.IsNullOrEmpty(thirdChoiceContent) || string.IsNullOrEmpty(fourthChoiceContent))
            {
                return ErrorHandler.TopicQuestionChoiceIsEmpty;
            }

            return null;
        }

        public ErrorStatus isValidatedAsNewQuestionChoiceContent(string firstChoiceContentImageUrl, string firstChoiceContentMd5, string secondChoiceContentImageUrl, string secondChoiceContentMd5, string thirdChoiceContentImageUrl, string thirdChoiceContentMd5, string fourthChoiceContentImageUrl, string fourthChoiceContentMd5)
        {
            if ((string.IsNullOrEmpty(firstChoiceContentImageUrl) && string.IsNullOrEmpty(firstChoiceContentMd5)) || (string.IsNullOrEmpty(secondChoiceContentImageUrl) && string.IsNullOrEmpty(secondChoiceContentMd5)) || (string.IsNullOrEmpty(thirdChoiceContentImageUrl) && string.IsNullOrEmpty(thirdChoiceContentMd5)) || (string.IsNullOrEmpty(fourthChoiceContentImageUrl) && string.IsNullOrEmpty(fourthChoiceContentMd5)))
            {
                return ErrorHandler.TopicQuestionChoiceIsEmpty;
            }

            return null;
        }

        public Row ValidateTopicCategory(string companyId,
                                         string topicCategoryId,
                                         ISession session)
        {
            Row categoryRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("topic_category",
                                      new List<string>(), new List<string> { "id", "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(topicCategoryId, companyId, true);
                categoryRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return categoryRow;
        }

        public Row ValidateTopic(string companyId,
                                 string topicCategoryId,
                                 string topicId,
                                 ISession session)
        {
            Row topicRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("topic",
                                     new List<string>(), new List<string> { "id", "category_id" }));
                BoundStatement bs = ps.Bind(topicId, topicCategoryId);
                topicRow = session.Execute(bs).FirstOrDefault();

                if (topicRow != null)
                {
                    int status = topicRow.GetValue<int>("status");
                    if (status == Topic.TopicStatus.CODE_DELETED)
                    {
                        topicRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return topicRow;
        }

        public Row ValidateTopicQuestion(string questionId,
                                         string topicId,
                                         ISession session)
        {
            Row questionRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("challenge_question",
                                     new List<string>(), new List<string> { "id", "topic_id" }));
                BoundStatement bs = ps.Bind(questionId, topicId);
                questionRow = session.Execute(bs).FirstOrDefault();

                if (questionRow != null)
                {
                    int status = questionRow.GetValue<int>("status");
                    if (status == ChallengeQuestion.QuestionStatus.CODE_DELETED)
                    {
                        questionRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return questionRow;
        }

        public Row ValidateChallenge(string challengeId,
                                     string companyId,
                                     ISession session,
                                     bool checkForValidity = true)
        {
            Row historyRow = null;

            try
            {
                PreparedStatement ps = null;
                BoundStatement bs = null;

                if (!string.IsNullOrEmpty(companyId))
                {
                    if (checkForValidity)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("challenge_history",
                            new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                        bs = ps.Bind(companyId, challengeId, true);
                    }
                    else
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("challenge_history",
                            new List<string>(), new List<string> { "company_id", "id" }));
                        bs = ps.Bind(companyId, challengeId);
                    }

                }
                else
                {
                    Log.Debug("Pubnub timeout: Kill challenge event");
                    ps = session.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("challenge_history",
                                        new List<string>(), new List<string> { "id", "is_valid" }));
                    bs = ps.Bind(challengeId, true);
                }

                historyRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return historyRow;
        }
        #endregion

        #region Feed
        public ErrorStatus isValidatedAsUserToPostFeed(string userId,
                                               string companyId,
                                               ISession session)
        {
            Row companyRow = ValidateCompany(companyId, session);

            if (companyRow != null)
            {
                Row userRow = ValidateUser(userId, companyId, session);

                if (userRow != null)
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("setting",
                                      new List<string> { "setting_value" }, new List<string> { "company_id", "setting_key" }));
                    BoundStatement bs = ps.Bind(companyId, SettingKey.FeedPermission);
                    Row settingForFeedPost = session.Execute(bs).FirstOrDefault();

                    if (settingForFeedPost != null)
                    {
                        int feedPostUserType = Int16.Parse(settingForFeedPost.GetValue<string>("setting_value"));
                        if (feedPostUserType == Int16.Parse(PermissionCode.FeedPermissionCodeAdminOnly) || feedPostUserType == Int16.Parse(PermissionCode.FeedPermissionCodeAdminAndModerator))
                        {
                            ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                                     new List<string>(), new List<string> { "user_id" }));
                            bs = ps.Bind(userId);
                            Row accountRow = session.Execute(bs).FirstOrDefault();

                            if (accountRow != null)
                            {
                                int accountType = accountRow.GetValue<int>("account_type");
                                if (accountType <= User.AccountType.CODE_NORMAL_USER)
                                {
                                    return ErrorHandler.FeedPermissionNotGranted;
                                }
                            }

                            return null;
                        }
                        else
                        {
                            if (feedPostUserType == Int16.Parse(PermissionCode.FeedPermissionCodeAllPersonnel))
                            {
                                ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_user_suspension",
                                     new List<string>(), new List<string> { "user_id", "company_id" }));
                                bs = ps.Bind(userId, companyId);
                                Row suspensionRow = session.Execute(bs).FirstOrDefault();

                                if (suspensionRow != null)
                                {
                                    return ErrorHandler.FeedPermissionNotGranted;
                                }

                                return null;
                            }
                            else
                            {
#warning Need check for all departments. Currently only check for the first department in the list.
                                ps = session.Prepare(CQLGenerator.SelectStatement("department_by_user",
                                    new List<string>(), new List<string> { "user_id" }));
                                bs = ps.Bind(userId);

                                string departmentId = session.Execute(bs).FirstOrDefault().GetValue<string>("department_id");

                                ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_department_allowed",
                                     new List<string>(), new List<string> { "department_id", "company_id" }));
                                bs = ps.Bind(departmentId, companyId);
                                Row suspensionRow = session.Execute(bs).FirstOrDefault();

                                if (suspensionRow == null)
                                {
                                    return ErrorHandler.FeedPermissionNotGranted;
                                }

                                return null;
                            }
                        }
                    }

                    return ErrorHandler.SystemError;
                }
                else
                {
                    return ErrorHandler.InvalidUserAccount;
                }
            }

            return ErrorHandler.InvalidCompany;
        }

        public Row ValidateFeedPost(string feedId,
                                    string companyId,
                                    ISession session,
                                    bool isCheckForValid = true)
        {
            Row feedRow = null;

            try
            {
                PreparedStatement ps = null;
                BoundStatement bs = null;

                if (isCheckForValid)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                      new List<string>(), new List<string> { "company_id", "feed_id", "is_feed_valid" }));
                    bs = ps.Bind(companyId, feedId, true);
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                       new List<string>(), new List<string> { "company_id", "feed_id" }));
                    bs = ps.Bind(companyId, feedId);
                }

                feedRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return feedRow;
        }

        public Row ValidateFeedComment(string feedId,
                                       string commentId,
                                       ISession session,
                                       bool isCheckForValid = true)
        {
            Row commentByFeedRow = null;

            try
            {
                PreparedStatement ps = null;
                BoundStatement bs = null;

                if (isCheckForValid)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "comment_id", "feed_id", "is_comment_valid" }));
                    bs = ps.Bind(commentId, feedId, true);
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "comment_id", "feed_id" }));
                    bs = ps.Bind(commentId, feedId);
                }

                commentByFeedRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return commentByFeedRow;
        }

        public Row ValidateCommentReply(string commentId,
                                        string replyId,
                                        ISession session,
                                        bool isCheckForValid = true)
        {
            Row replyByFeedRow = null;

            try
            {
                PreparedStatement ps = null;
                BoundStatement bs = null;

                if (isCheckForValid)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id", "is_reply_valid" }));
                    bs = ps.Bind(commentId, replyId, true);
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id" }));
                    bs = ps.Bind(commentId, replyId);
                }

                replyByFeedRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return replyByFeedRow;
        }
        #endregion

        #region Event
        public Row ValidateEvent(string eventId,
                                 string companyId,
                                 ISession session,
                                 bool isCheckForValid = true)
        {
            Row eventRow = null;

            try
            {
                PreparedStatement ps = null;
                BoundStatement bs = null;

                if (isCheckForValid)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("event",
                     new List<string>(), new List<string> { "company_id", "event_id", "valid_status" }));
                    bs = ps.Bind(companyId, eventId, (int)Event.EventValidStatusEnum.Valid);

                    eventRow = session.Execute(bs).FirstOrDefault();
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("event",
                        new List<string>(), new List<string> { "company_id", "event_id" }));
                    bs = ps.Bind(companyId, eventId);
                    eventRow = session.Execute(bs).FirstOrDefault();

                    int validStatus = eventRow.GetValue<int>("valid_status");

                    if (validStatus == (int)Event.EventValidStatusEnum.Deleted)
                    {
                        eventRow = null;
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return eventRow;
        }
        #endregion

        #region Responsive Survey
        public Row ValidateRSTopicCategory(string companyId,
                                           string topicCategoryId,
                                           ISession session)
        {
            Row categoryRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_category",
                                      new List<string>(), new List<string> { "id", "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(topicCategoryId, companyId, true);
                categoryRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return categoryRow;
        }

        public Row ValidateRSTopic(string companyId,
                                   string topicCategoryId,
                                   string topicId,
                                   ISession session)
        {
            Row topicRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_topic",
                                     new List<string>(), new List<string> { "id", "category_id" }));
                BoundStatement bs = ps.Bind(topicId, topicCategoryId);
                topicRow = session.Execute(bs).FirstOrDefault();

                if (topicRow != null)
                {
                    int status = topicRow.GetValue<int>("status");
                    if (status == RSTopic.RSTopicStatus.CODE_DELETED)
                    {
                        topicRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return topicRow;
        }

        public Row ValidateRSTopicCard(string cardId,
                                       string topicId,
                                       ISession session)
        {
            Row cardRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_card",
                                     new List<string>(), new List<string> { "id", "rs_topic_id", "status" }));
                BoundStatement bs = ps.Bind(cardId, topicId, (int)RSCard.RSCardStatus.Active);
                cardRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return cardRow;
        }

        public bool ValidateLogicForCard(string cardId,
                                         string topicId,
                                         ISession session)
        {
            bool hasLogic = false;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic",
                                     new List<string>(), new List<string> { "card_id" }));
                Row toLogicRow = session.Execute(ps.Bind(cardId)).FirstOrDefault();

                if (toLogicRow != null)
                {
                    hasLogic = true;
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_from_logic",
                        new List<string>(), new List<string> { "card_id" }));
                    Row fromLogicRow = session.Execute(ps.Bind(cardId)).FirstOrDefault();
                    if (fromLogicRow != null)
                    {
                        hasLogic = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return hasLogic;
        }
        #endregion

        #region Pulse
        public Row ValidatePulseSingle(string companyId,
                                       string pulseId,
                                       ISession session)
        {
            Row pulseRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("pulse_single_by_company",
                                     new List<string>(), new List<string> { "company_id", "id" }));
                BoundStatement bs = ps.Bind(companyId, pulseId);
                pulseRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return pulseRow;
        }

        public Row ValidatePulseDeck(string companyId,
                                     string deckId,
                                     ISession session)
        {
            Row deckRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_deck",
                                     new List<string>(), new List<string> { "company_id", "id" }));
                BoundStatement bs = ps.Bind(companyId, deckId);
                deckRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return deckRow;
        }

        public Row ValidatePulseDeckCard(string deckId,
                                         string pulseId,
                                         ISession session)
        {
            Row deckRow = null;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck",
                   new List<string>(), new List<string> { "deck_id", "id" }));
                deckRow = session.Execute(ps.Bind(deckId, pulseId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return deckRow;
        }
        #endregion

        #region Assessment
        public Row ValidateAssessmentCoach(string coachId, ISession session)
        {
            Row coachRow = null;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_coach",
                   new List<string>(), new List<string> { "id", "is_valid" }));
                coachRow = session.Execute(ps.Bind(coachId, true)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return coachRow;
        }

        public Row ValidateAssessment(string assessmentId, ISession session)
        {
            Row assessmentRow = null;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment",
                   new List<string>(), new List<string> { "id" }));
                assessmentRow = session.Execute(ps.Bind(assessmentId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return assessmentRow;
        }

        public Row ValidateAssessmentCard(string assessmentId, string cardId, ISession session)
        {
            Row assessmentRow = null;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_card",
                   new List<string>(), new List<string> { "assessment_id", "id" }));
                assessmentRow = session.Execute(ps.Bind(assessmentId, cardId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return assessmentRow;
        }

        public Row ValidateAssessmentTabulation(string assessmentId, string tabulationId, ISession session)
        {
            Row assessmentTabulationRow = null;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_tabulation",
                   new List<string>(), new List<string> { "assessment_id", "id" }));
                assessmentTabulationRow = session.Execute(ps.Bind(assessmentId, tabulationId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return assessmentTabulationRow;
        }

        public Row ValidateAssessmentSubscription(string assessmentId, string companyId, ISession session)
        {
            Row subscriptionRow = null;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_subscription",
                   new List<string>(), new List<string> { "assessment_id", "company_id" }));
                subscriptionRow = session.Execute(ps.Bind(assessmentId, companyId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return subscriptionRow;
        }
        #endregion

        #region Group
        public Row ValidateGroup(string groupId, string userId, ISession session)
        {
            Row groupRow = null;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("group", new List<string>(), new List<string> { "id", "user_id" }));
                groupRow = session.Execute(ps.Bind(groupId, userId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return groupRow;
        }
        #endregion

        #region Mobile Learning
        public Row ValidateMLTopicCategory(string companyId,
                                           string topicCategoryId,
                                           ISession session)
        {
            Row categoryRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_category",
                                      new List<string>(), new List<string> { "id", "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(topicCategoryId, companyId, true);
                categoryRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return categoryRow;
        }

        public Row ValidateMLTopic(string companyId,
                                   string topicCategoryId,
                                   string topicId,
                                   ISession session)
        {
            Row topicRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_topic",
                                     new List<string>(), new List<string> { "id", "category_id" }));
                BoundStatement bs = ps.Bind(topicId, topicCategoryId);
                topicRow = session.Execute(bs).FirstOrDefault();

                if (topicRow != null)
                {
                    int status = topicRow.GetValue<int>("status");
                    if (status == (int)MLTopic.MLTopicStatusEnum.Deleted)
                    {
                        topicRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return topicRow;
        }

        public Row ValidateMLTopicCard(string cardId,
                                       string topicId,
                                       ISession session)
        {
            Row cardRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_card",
                                     new List<string>(), new List<string> { "id", "ml_topic_id", "status" }));
                BoundStatement bs = ps.Bind(cardId, topicId, (int)MLCard.MLCardStatus.Active);
                cardRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return cardRow;
        }

        public Row ValidateMLTopicCardOption(string cardId,
                                             string optionId,
                                             ISession session)
        {
            Row optionRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_card_option",
                                     new List<string>(), new List<string> { "card_id", "id" }));
                BoundStatement bs = ps.Bind(cardId, optionId);
                optionRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return optionRow;
        }

        public Row ValidateMLEducationCategory(string companyId, string categoryId, ISession session)
        {
            Row row = null;
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_category", new List<string>(), new List<string> { "id", "company_id", "is_valid" }));
                BoundStatement bs = ps.Bind(categoryId, companyId, true);
                row = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return row;
        }

        public Row ValidateMLEducation(string companyId,
                                  string educationCategoryId,
                                  string educationId,
                                  ISession session)
        {
            Row topicRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education",
                                     new List<string>(), new List<string> { "id", "category_id" }));
                BoundStatement bs = ps.Bind(educationId, educationCategoryId);
                topicRow = session.Execute(bs).FirstOrDefault();

                if (topicRow != null)
                {
                    int status = topicRow.GetValue<int>("status");
                    if (status == (int)MLEducation.MLEducationStatusEnum.Deleted)
                    {
                        topicRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return topicRow;
        }

        public Row ValidateMLEducationCard(string cardId, string educationId, ISession session)
        {
            Row cardRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card", new List<string>(), new List<string> { "id", "ml_education_id", "status" }));
                BoundStatement bs = ps.Bind(cardId, educationId, (int)MLEducationCard.MLCardStatus.Active);
                cardRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return cardRow;
        }

        public Row ValidateMLEducationCardOption(string cardId, string optionId, ISession session)
        {
            Row optionRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_option", new List<string>(), new List<string> { "card_id", "id" }));
                BoundStatement bs = ps.Bind(cardId, optionId);
                optionRow = session.Execute(bs).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return optionRow;
        }

        #endregion

        #region Achievement
        public Row ValidateAchievement(string companyId,
                                       string achievementId,
                                       ISession session)
        {
            Row achievementRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("achievement",
                    new List<string>(), new List<string> { "id", "company_id" }));
                achievementRow = session.Execute(ps.Bind(achievementId, companyId)).FirstOrDefault();
                if(achievementRow != null)
                {
                    if(achievementRow.GetValue<int>("status") < (int)Gamification.AchievementStatusEnum.Live)
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return achievementRow;
        }
        #endregion

        #region Appraisal 360
        public Row ValidateAppraisalUserCategory(string appraisalId,
                                                string categoryId,
                                                string creatorUserId,
                                                ISession session)
        {
            Row categoryRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_template_appraisal_360_category",
                                      new List<string>(), new List<string> { "appraisal_id", "created_by_user_id", "category_id" }));
                categoryRow = session.Execute(ps.Bind(appraisalId, creatorUserId, categoryId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return categoryRow;
        }

        public Row ValidateTemplateAppraisal(string companyId,
                                             string appraisalId,
                                             ISession session,
                                             bool checkForValid = true)
        {
            Row appraisalRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("template_appraisal_360",
                                     new List<string>(), new List<string> { "company_id", "appraisal_id" }));
                appraisalRow = session.Execute(ps.Bind(companyId, appraisalId)).FirstOrDefault();
                if(appraisalRow != null)
                {
                    if(appraisalRow.GetValue<int>("status") == (int)Appraisal.AppraisalStatusEnum.Deleted && checkForValid)
                    {
                        appraisalRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return appraisalRow;
        }

        public Row ValidateTemplateAppraisalCategory(string categoryId,
                                                     string appraisalId,
                                                     ISession session)
        {
            Row categoryRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_category",
                                     new List<string>(), new List<string> { "id", "appraisal_id" }));
                categoryRow = session.Execute(ps.Bind(categoryId, appraisalId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return categoryRow;
        }

        public Row ValidateTemplateAppraisalCard(string categoryId,
                                                 string cardId,
                                                 ISession session)
        {
            Row cardRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_card",
                                     new List<string>(), new List<string> { "card_id", "category_id" }));
                cardRow = session.Execute(ps.Bind(cardId, categoryId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return cardRow;
        }

        public Row ValidateCustomAppraisal(string companyId,
                                           string appraisalId,
                                           ISession session)
        {
            Row appraisalRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_appraisal_360",
                                     new List<string>(), new List<string> { "company_id", "appraisal_id" }));
                appraisalRow = session.Execute(ps.Bind(companyId, appraisalId)).FirstOrDefault();
                if (appraisalRow != null)
                {
                    if (appraisalRow.GetValue<int>("status") == (int)Appraisal.AppraisalStatusEnum.Deleted)
                    {
                        appraisalRow = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return appraisalRow;
        }

        public Row ValidateCustomAppraisalCard(string appraisalId,
                                               string cardId,
                                               ISession session)
        {
            Row cardRow = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card",
                                     new List<string>(), new List<string> { "appraisal_id", "card_id" }));
                cardRow = session.Execute(ps.Bind(appraisalId, cardId)).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return cardRow;
        }
        #endregion
    }
}
