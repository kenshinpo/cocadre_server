﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using Cassandra.Mapping;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    public class Feed
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int FeedType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedText FeedText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedImage FeedImage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedVideo FeedVideo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public FeedSharedUrl FeedSharedUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReportPostId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportPostState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ReportedPost> ReportedFeedPosts { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Comment> Comments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int DashboardFeedbackType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int DashboardReportedType { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public int NumberOfComments { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public bool IsForPersonnel { get; set; }

        [DataMember]
        public bool IsTaggedForPersonnel { get; set; }

        [DataMember]
        public bool IsForGroup { get; set; }

        [DataMember]
        public bool IsForOnlyMe { get; set; }

        [DataMember]
        public List<User> TaggedUsers { get; set; }

        [DataMember]
        public List<User> UpVoters { get; set; }

        [DataMember]
        public int UniqueImpression { get; set; }

        public enum VoteStatus
        {
            Reset = -999,
            Upvote = 1,
            Downvote = -1
        }

        public enum FeedValidStatus
        {
            Valid = 1,
            Hidden = -1,
            Deleted = -2
        }

        public FeedAuthenticationResponse AuthenticateUserForPostingFeed(string companyId,
            string userId)
        {
            FeedAuthenticationResponse response = new FeedAuthenticationResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(userId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    string feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                    response.FeedId = feedId;

                    response.IsAuthenticatedToPost = true;
                    response.Success = true;
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }

        public FeedCreateResponse CreateFeedTextPost(string feedId,
                                                     string creatorUserId,
                                                     string companyId,
                                                     string content,
                                                     List<string> targetedDepartmentIds,
                                                     List<string> targetedUserIds,
                                                     List<string> targetedGroupIds,
                                                     bool isForAdmin,
                                                     bool isSpecificNotification = false)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batch = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, mainSession);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        DateTime currentTime = DateTime.UtcNow;
                        long createdTimestamp = DateHelper.ConvertDateToLong(currentTime);

                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        //Extract tagged user ids
                        List<string> taggedUserIds = SearchTaggedUserIdsFromContent(content, companyId, vh, mainSession);

                        //Extract hashTag
                        List<string> hashTags = SearchHashedTagsFromContent(content);
                        if (hashTags.Count > 0)
                        {
                            CreateHashTag(feedId, createdTimestamp, companyId, hashTags, mainSession, analyticSession);
                        }

                        Dictionary<string, List<string>> privacyNotifyDict = CreateFeedPrivacy(creatorUserId,
                                                                                              feedId,
                                                                                              companyId,
                                                                                              Convert.ToInt16(FeedTypeCode.TextPost),
                                                                                              content,
                                                                                              targetedDepartmentIds,
                                                                                              targetedUserIds,
                                                                                              targetedGroupIds,
                                                                                              taggedUserIds,
                                                                                              isForAdmin,
                                                                                              createdTimestamp,
                                                                                              mainSession);

                        PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("feed_text",
                            new List<string>
                            {
                                "id",
                                "content",
                                "is_valid",
                                "valid_status",
                                "last_modified_by_user_id",
                                "last_modified_timestamp"
                            }));
                        batch.Add(ps.Bind(feedId, content, true, 1, creatorUserId, createdTimestamp));

                        mainSession.Execute(batch);

                        // Create notification for targeted users
                        if (isSpecificNotification)
                        {
                            response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.TextPost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }
                        else
                        {
                            response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.TextPost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }


                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, content, companyId, feedId, createdTimestamp,
                                mainSession);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed text post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedImagePost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      List<string> imageUrls,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      List<string> targetedGroupIds,
                                                      bool isForAdmin,
                                                      bool isSpecificNotification = false)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, mainSession);
                if (hasErrorAsUser == null)
                {
                    imageUrls = imageUrls.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();

                    if (imageUrls.Count > 0)
                    {
                        DateTime currentTime = DateTime.UtcNow;
                        long createdTimestamp = DateHelper.ConvertDateToLong(currentTime);

                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        //Extract tagged user ids
                        List<string> taggedUserIds = SearchTaggedUserIdsFromContent(caption, companyId, vh, mainSession);

                        //Extract hashTag
                        List<string> hashTags = SearchHashedTagsFromContent(caption);
                        if (hashTags.Count > 0)
                        {
                            CreateHashTag(feedId, createdTimestamp, companyId, hashTags, mainSession, analyticSession);
                        }

                        Dictionary<string, List<string>> privacyNotifyDict = CreateFeedPrivacy(creatorUserId,
                                                                                               feedId,
                                                                                               companyId,
                                                                                               Convert.ToInt16(FeedTypeCode.ImagePost),
                                                                                               caption,
                                                                                               targetedDepartmentIds,
                                                                                               targetedUserIds,
                                                                                               targetedGroupIds,
                                                                                               taggedUserIds,
                                                                                               isForAdmin,
                                                                                               createdTimestamp,
                                                                                               mainSession);

                        PreparedStatement preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("feed_image",
                            new List<string>
                            {
                                "id",
                                "caption",
                                "is_valid",
                                "valid_status",
                                "last_modified_by_user_id",
                                "last_modified_timestamp"
                            }));
                        updateBatch.Add(preparedStatement.Bind(feedId, caption, true, 1, creatorUserId,
                            createdTimestamp));

                        // Delete from feedImageUrl in case of multithread
                        preparedStatement = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_image_url", new List<string> { "feed_id" }));
                        deleteBatch.Add(preparedStatement.Bind(feedId));

                        int order = 1;
                        foreach (string imageUrl in imageUrls)
                        {
                            string imageId = UUIDGenerator.GenerateUniqueIDForFeedMedia();

                            preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("feed_image_url",
                                new List<string> { "id", "feed_id", "url", "in_order", "is_valid" }));
                            updateBatch.Add(preparedStatement.Bind(imageId, feedId, imageUrl, order, true));

                            order += 1;
                        }

                        mainSession.Execute(deleteBatch);
                        mainSession.Execute(updateBatch);

                        // Create notification for targeted users
                        if (isSpecificNotification)
                        {
                            response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.ImagePost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }
                        else
                        {
                            response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.ImagePost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }

                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, caption, companyId, feedId, createdTimestamp,
                                mainSession);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed image post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidImageContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidImageContent;
                        response.Success = false;
                    }

                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedVideoPost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      string videoUrl,
                                                      string videoThumbnailUrl,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      List<string> targetedGroupIds,
                                                      bool isForAdmin,
                                                      bool isSpecificNotification = false)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, mainSession);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(videoUrl))
                    {
                        DateTime currentTime = DateTime.UtcNow;
                        long createdTimestamp = DateHelper.ConvertDateToLong(currentTime);

                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        //Extract tagged user ids
                        List<string> taggedUserIds = SearchTaggedUserIdsFromContent(caption, companyId, vh, mainSession);

                        //Extract hashTag
                        List<string> hashTags = SearchHashedTagsFromContent(caption);
                        if (hashTags.Count > 0)
                        {
                            CreateHashTag(feedId, createdTimestamp, companyId, hashTags, mainSession, analyticSession);
                        }


                        Dictionary<string, List<string>> privacyNotifyDict = CreateFeedPrivacy(creatorUserId,
                                                                                               feedId,
                                                                                               companyId,
                                                                                               Convert.ToInt16(FeedTypeCode.VideoPost),
                                                                                               caption,
                                                                                               targetedDepartmentIds,
                                                                                               targetedUserIds,
                                                                                               targetedGroupIds,
                                                                                               taggedUserIds,
                                                                                               isForAdmin,
                                                                                               createdTimestamp,
                                                                                               mainSession);

                        PreparedStatement preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("feed_video",
                            new List<string>
                            {
                                "id",
                                "caption",
                                "video_url",
                                "video_thumbnail_url",
                                "is_valid",
                                "valid_status",
                                "last_modified_by_user_id",
                                "last_modified_timestamp"
                            }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, videoUrl, videoThumbnailUrl, true, 1,
                            creatorUserId, createdTimestamp));

                        mainSession.Execute(batchStatement);

                        // Create notification for targeted users
                        if (isSpecificNotification)
                        {
                            response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.VideoPost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }
                        else
                        {
                            response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.VideoPost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }


                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, caption, companyId, feedId, createdTimestamp,
                                mainSession);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed video post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidVideoContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidVideoContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateResponse CreateFeedSharedUrlPost(string feedId,
                                                          string creatorUserId,
                                                          string companyId,
                                                          string caption,
                                                          string url,
                                                          string urlTitle,
                                                          string urlDescription,
                                                          string urlSiteName,
                                                          string urlImageUrl,
                                                          List<string> targetedDepartmentIds,
                                                          List<string> targetedUserIds,
                                                          List<string> targetedGroupIds,
                                                          bool isForAdmin,
                                                          bool isSpecificNotification = false)
        {
            FeedCreateResponse response = new FeedCreateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, mainSession);
                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(url))
                    {
                        DateTime currentTime = DateTime.UtcNow;
                        long createdTimestamp = DateHelper.ConvertDateToLong(currentTime);

                        if (string.IsNullOrEmpty(feedId))
                        {
                            feedId = UUIDGenerator.GenerateUniqueIDForFeed();
                        }

                        //Extract tagged user ids
                        List<string> taggedUserIds = SearchTaggedUserIdsFromContent(caption, companyId, vh, mainSession);

                        //Extract hashTag
                        List<string> hashTags = SearchHashedTagsFromContent(caption);
                        if (hashTags.Count > 0)
                        {
                            CreateHashTag(feedId, createdTimestamp, companyId, hashTags, mainSession, analyticSession);
                        }

                        Dictionary<string, List<string>> privacyNotifyDict = CreateFeedPrivacy(creatorUserId,
                                                                                               feedId,
                                                                                               companyId,
                                                                                               Convert.ToInt16(FeedTypeCode.SharedUrlPost),
                                                                                               caption,
                                                                                               targetedDepartmentIds,
                                                                                               targetedUserIds,
                                                                                               targetedGroupIds,
                                                                                               taggedUserIds,
                                                                                               isForAdmin,
                                                                                               createdTimestamp,
                                                                                               mainSession);

                        PreparedStatement preparedStatement = mainSession.Prepare(CQLGenerator.InsertStatement("feed_shared_url",
                                new List<string>
                                {
                                    "id",
                                    "caption",
                                    "url",
                                    "url_title",
                                    "url_description",
                                    "url_site_name",
                                    "url_image_url",
                                    "is_valid",
                                    "valid_status",
                                    "last_modified_by_user_id",
                                    "last_modified_timestamp"
                                }));
                        batchStatement.Add(preparedStatement.Bind(feedId, caption, url, urlTitle, urlDescription,
                            urlSiteName, urlImageUrl, true, 1, creatorUserId, createdTimestamp));

                        mainSession.Execute(batchStatement);

                        // Create notification for targeted users
                        if (isSpecificNotification)
                        {
                            response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.SharedUrlPost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }
                        else
                        {
                            response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, feedId, Convert.ToInt16(FeedTypeCode.SharedUrlPost), creatorUserId, currentTime, privacyNotifyDict, mainSession));
                        }

                        // For feedback to admin
                        if (isForAdmin)
                        {
                            Dashboard dashboard = new Dashboard();
                            dashboard.CreateFeedbackToAdmin(creatorUserId, caption, companyId, feedId, createdTimestamp,
                                mainSession);
                        }

                        response.FeedId = feedId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Empty feed shared url post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                        response.Success = false;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<string> SearchTaggedUserIdsFromContent(string content, string companyId, ValidationHandler vh, ISession session)
        {
            List<string> taggedUserIds = new List<string>();
            try
            {
                int lengthOfUserId = 33;
                List<int> startPositions = Regex.Matches(content, "@").Cast<Match>().Select(m => m.Index).ToList();
                for (int index = 0; index < startPositions.Count; index++)
                {
                    int endPosition = content.Length - 1;
                    if (index + 1 < startPositions.Count)
                    {
                        endPosition = startPositions[index + 1] - 1;
                    }

                    int startPosition = startPositions[index] + 1;
                    int diff = endPosition - startPosition + 1;
                    if (diff >= lengthOfUserId)
                    {
                        string foundUserId = content.Substring(startPosition, lengthOfUserId);
                        Row userRow = vh.ValidateUser(foundUserId, companyId, session);
                        if (userRow != null && !taggedUserIds.Contains(foundUserId))
                        {
                            taggedUserIds.Add(foundUserId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return taggedUserIds;
        }

        public List<User> SelectTaggedUsersFromContent(string content, string companyId, ISession session)
        {
            List<User> taggedUsers = new List<User>();
            try
            {
                int lengthOfUserId = 33;
                User userManager = new User();

                List<int> startPositions = Regex.Matches(content, "@").Cast<Match>().Select(m => m.Index).ToList();
                for (int index = 0; index < startPositions.Count; index++)
                {
                    int endPosition = content.Length - 1;
                    if (index + 1 < startPositions.Count)
                    {
                        endPosition = startPositions[index + 1] - 1;
                    }

                    int startPosition = startPositions[index] + 1;
                    int diff = endPosition - startPosition + 1;
                    if (diff >= lengthOfUserId)
                    {
                        string foundUserId = content.Substring(startPosition, lengthOfUserId);

                        // Check from cache
                        User selectedUser = CacheHelper.RetrieveUserByCompany(companyId, foundUserId);
                        if (selectedUser == null)
                        {
                            selectedUser = userManager.SelectUserBasic(foundUserId, companyId, false, session, null, true).User;
                            if (selectedUser != null && selectedUser.Status.Code == User.AccountStatus.CODE_ACTIVE)
                            {
                                CacheHelper.UpdateCacheForColleague(companyId, selectedUser.Departments[0].Id, selectedUser.Departments[0].Title, selectedUser.UserId, selectedUser.Email, selectedUser.FirstName, selectedUser.LastName, selectedUser.ProfileImageUrl, selectedUser.Position);
                            }
                        }

                        //User selectedUser = userManager.SelectUserBasic(foundUserId, companyId, false, session, null, true).User;

                        if (selectedUser != null)
                        {
                            taggedUsers.Add(selectedUser);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return taggedUsers;
        }

        public string ReplaceContentWithUserNames(string content, string companyId, ISession session)
        {
            string tempContent = content;
            try
            {
                int lengthOfUserId = 33;
                User userManager = new User();

                List<int> startPositions = Regex.Matches(content, "@").Cast<Match>().Select(m => m.Index).ToList();
                for (int index = 0; index < startPositions.Count; index++)
                {
                    int endPosition = content.Length - 1;
                    if (index + 1 < startPositions.Count)
                    {
                        endPosition = startPositions[index + 1] - 1;
                    }

                    int startPosition = startPositions[index] + 1;
                    int diff = endPosition - startPosition + 1;
                    if (diff >= lengthOfUserId)
                    {
                        string foundUserId = content.Substring(startPosition, lengthOfUserId);

                        // Check from cache
                        User selectedUser = CacheHelper.RetrieveUserByCompany(companyId, foundUserId);
                        if (selectedUser == null)
                        {
                            selectedUser = userManager.SelectUserBasic(foundUserId, companyId, false, session, null, true).User;
                            if (selectedUser != null && selectedUser.Status.Code == User.AccountStatus.CODE_ACTIVE)
                            {
                                CacheHelper.UpdateCacheForColleague(companyId, selectedUser.Departments[0].Id, selectedUser.Departments[0].Title, selectedUser.UserId, selectedUser.Email, selectedUser.FirstName, selectedUser.LastName, selectedUser.ProfileImageUrl, selectedUser.Position);
                            }
                        }

                        if (selectedUser != null)
                        {
                            string fullName = string.Format("{0} {1}", selectedUser.FirstName, selectedUser.LastName);
                            tempContent = tempContent.Replace(string.Format("@{0}", foundUserId), string.Format("<b>{0}</b>", fullName));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return tempContent;
        }

        private List<string> SearchHashedTagsFromContent(string content)
        {
            List<string> hashedTags = new List<string>();
            try
            {
                //List<int> startPositions = Regex.Matches(content, "#").Cast<Match>().Select(m => m.Index).ToList();
                //for (int index = 0; index < startPositions.Count; index++)
                //{
                //    int endPosition = content.Length - 1;
                //    if (index + 1 < startPositions.Count)
                //    {
                //        endPosition = startPositions[index + 1] - 1;
                //    }

                //    int startPosition = startPositions[index] + 1;
                //    int length = endPosition - startPosition + 1;

                //    string hashTag = content.Substring(startPosition, length);

                //    bool hasSpace = false;

                //    if (startPosition - 2 >= 0)
                //    {
                //        string checkPreviousChar = content.Substring(startPosition - 2, 1);
                //        hasSpace = string.IsNullOrWhiteSpace(checkPreviousChar) ? true : false;
                //    }
                //    else
                //    {
                //        hasSpace = true;
                //    }

                //    hashTag = Regex.Replace(hashTag, "[^a-zA-Z0-9_]+", " ");
                //    hashTag = hashTag.Split(' ')[0].Trim();

                //    if (!hashedTags.Contains(hashTag) && hasSpace)
                //    {
                //        hashedTags.Add(hashTag);
                //    }
                //}

                //MatchCollection matches = Regex.Matches(content, @"(^#\w*)|(?: )(#[\w]+)(?:#)*");
                MatchCollection matches = Regex.Matches(content, @"(?<![\w])(#[\w]+)(?!@)\b");
                foreach (Match match in matches)
                {
                    hashedTags.Add(match.Groups.SyncRoot.ToString().Replace("#", "").Trim());
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return hashedTags;
        }

        public CommentCreateResponse CreateCommentText(string creatorUserId,
                                                       string companyId,
                                                       string feedId,
                                                       string content,
                                                       bool isAdmin = false,
                                                       ISession session = null)
        {
            CommentCreateResponse response = new CommentCreateResponse();
            response.Notifications = new List<Notification>();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();
                ErrorStatus hasErrorAsUser = null;
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    if (!isAdmin)
                    {
                        hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                    }
                    else
                    {
                        hasErrorAsUser = vh.isValidatedAsAdmin(creatorUserId, companyId, session);
                    }

                }

                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                        if (feedRow != null)
                        {
                            if (isAdmin)
                            {
                                creatorUserId = companyId;
                            }

                            string commentId = UUIDGenerator.GenerateUniqueIDForFeedComment();

                            DateTime currentDateTime = DateTime.UtcNow;
                            long currentDateTimeLong = DateHelper.ConvertDateToLong(currentDateTime);

                            PreparedStatement psCommentByFeed =
                                session.Prepare(CQLGenerator.InsertStatement("comment_by_feed",
                                    new List<string>
                                    {
                                        "comment_id",
                                        "feed_id",
                                        "comment_type",
                                        "commentor_user_id",
                                        "is_comment_valid",
                                        "comment_valid_status",
                                        "created_on_timestamp"
                                    }));
                            batch.Add(psCommentByFeed.Bind(commentId, feedId,
                                Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                            PreparedStatement psFeedCommentText =
                                session.Prepare(CQLGenerator.InsertStatement("feed_comment_text",
                                    new List<string>
                                    {
                                        "id",
                                        "feed_id",
                                        "content",
                                        "is_valid",
                                        "valid_status",
                                        "user_id",
                                        "created_on_timestamp",
                                        "last_modified_by_user_id",
                                        "last_modified_timestamp"
                                    }));
                            batch.Add(psFeedCommentText.Bind(commentId, feedId, content, true, 1, creatorUserId,
                                currentDateTimeLong, creatorUserId, currentDateTimeLong));

                            PreparedStatement psCommentByTimestamp =
                                session.Prepare(CQLGenerator.InsertStatement("comment_by_feed_timestamp_desc",
                                    new List<string>
                                    {
                                        "comment_id",
                                        "feed_id",
                                        "comment_type",
                                        "commentor_user_id",
                                        "is_comment_valid",
                                        "comment_valid_status",
                                        "created_on_timestamp"
                                    }));
                            batch.Add(psCommentByTimestamp.Bind(commentId, feedId,
                                Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                            psCommentByTimestamp =
                                session.Prepare(CQLGenerator.InsertStatement("comment_by_feed_timestamp_asc",
                                    new List<string>
                                    {
                                        "comment_id",
                                        "feed_id",
                                        "comment_type",
                                        "commentor_user_id",
                                        "is_comment_valid",
                                        "comment_valid_status",
                                        "created_on_timestamp"
                                    }));
                            batch.Add(psCommentByTimestamp.Bind(commentId, feedId,
                                Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                            session.Execute(batch);

                            UpdateFeedCommentCounter(feedId, 1, session);

                            // Create notification for targeted user
                            string ownerUserId = feedRow.GetValue<string>("owner_user_id");
                            bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                            int feedType = feedRow.GetValue<int>("feed_type");

                            if (!isPostedByAdmin)
                            {
                                if (!ownerUserId.Equals(creatorUserId))
                                {
                                    // Notification for owner
                                    response.Notifications.Add(GetNotification(companyId, feedId, string.Empty, string.Empty, creatorUserId, ownerUserId, currentDateTime, feedType, (int)Notification.NotificationFeedSubType.NewComment, session));
                                }
                            }
                            else
                            {
#warning Need loop through department users
                            }

                            // Get all mentioned users for notification
                            List<User> taggedUsers = SelectTaggedUsersFromContent(content, companyId, session);
                            foreach (User taggedUser in taggedUsers)
                            {
                                if (taggedUser.UserId.Equals(creatorUserId) || taggedUser.UserId.Equals(ownerUserId))
                                {
                                    continue;
                                }

                                FeedSelectPrivacyResponse privacyResponse = SelectFeedPrivacy(taggedUser.UserId, companyId, feedId, session, feedRow);
                                if (privacyResponse.Success)
                                {
                                    response.Notifications.Add(GetNotification(companyId, feedId, string.Empty, string.Empty, creatorUserId, taggedUser.UserId, currentDateTime, feedType, (int)Notification.NotificationFeedSubType.MentionedInComment, session));
                                }
                            }

                            response.Success = true;
                        }
                        else
                        {
                            Log.Error("Invalid feed post");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                            response.ErrorMessage = ErrorMessage.FeedInvalid;
                        }
                    }
                    else
                    {
                        Log.Error("Empty feed text comment");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextComment;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplyCreateResponse CreateReplyText(string creatorUserId,
            string companyId,
            string feedId,
            string commentId,
            string content,
            bool isAdmin = false)
        {
            ReplyCreateResponse response = new ReplyCreateResponse();
            response.Notifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();
                ErrorStatus hasErrorAsUser = null;

                if (!isAdmin)
                {
                    hasErrorAsUser = vh.isValidatedAsUserToPostFeed(creatorUserId, companyId, session);
                }
                else
                {
                    hasErrorAsUser = vh.isValidatedAsAdmin(creatorUserId, companyId, session);
                }

                if (hasErrorAsUser == null)
                {
                    if (!string.IsNullOrEmpty(content))
                    {
                        Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                        if (feedRow != null)
                        {
                            Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                            if (commentRow != null)
                            {
                                if (isAdmin)
                                {
                                    creatorUserId = companyId;
                                }

                                string replyId = UUIDGenerator.GenerateUniqueIDForFeedCommentReply();

                                DateTime currentDateTime = DateTime.UtcNow;
                                long currentDateTimeLong = DateHelper.ConvertDateToLong(currentDateTime);

                                PreparedStatement psCommentByFeed =
                                    session.Prepare(CQLGenerator.InsertStatement("reply_by_comment",
                                        new List<string>
                                        {
                                            "reply_id",
                                            "comment_id",
                                            "reply_type",
                                            "commentor_user_id",
                                            "is_reply_valid",
                                            "reply_valid_status",
                                            "created_on_timestamp"
                                        }));
                                batchStatement.Add(psCommentByFeed.Bind(replyId, commentId,
                                    Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                                PreparedStatement psFeedCommentText =
                                    session.Prepare(CQLGenerator.InsertStatement("feed_reply_text",
                                        new List<string>
                                        {
                                            "id",
                                            "comment_id",
                                            "content",
                                            "is_valid",
                                            "valid_status",
                                            "user_id",
                                            "created_on_timestamp",
                                            "last_modified_by_user_id",
                                            "last_modified_timestamp"
                                        }));
                                batchStatement.Add(psFeedCommentText.Bind(replyId, commentId, content, true, 1,
                                    creatorUserId, currentDateTimeLong, creatorUserId, currentDateTimeLong));

                                PreparedStatement psCommentByTimestampDesc =
                                    session.Prepare(CQLGenerator.InsertStatement("reply_by_comment_timestamp_desc",
                                        new List<string>
                                        {
                                            "reply_id",
                                            "comment_id",
                                            "reply_type",
                                            "commentor_user_id",
                                            "is_reply_valid",
                                            "reply_valid_status",
                                            "created_on_timestamp"
                                        }));
                                batchStatement.Add(psCommentByTimestampDesc.Bind(replyId, commentId,
                                    Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                                PreparedStatement psCommentByTimestampAsc =
                                    session.Prepare(CQLGenerator.InsertStatement("reply_by_comment_timestamp_asc",
                                        new List<string>
                                        {
                                            "reply_id",
                                            "comment_id",
                                            "reply_type",
                                            "commentor_user_id",
                                            "is_reply_valid",
                                            "reply_valid_status",
                                            "created_on_timestamp"
                                        }));
                                batchStatement.Add(psCommentByTimestampAsc.Bind(replyId, commentId,
                                    Int32.Parse(FeedTypeCode.TextPost), creatorUserId, true, 1, currentDateTimeLong));

                                session.Execute(batchStatement);

                                UpdateCommentReplyCounter(feedId, commentId, 1, session);

                                // Create notification for targeted user
                                // Check for privacy too
                                string ownerUserId = commentRow.GetValue<string>("commentor_user_id");
                                bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");
                                bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                                bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                                bool isForUser = feedRow.GetValue<bool>("is_for_user");
                                bool isTaggedForUser = feedRow.GetValue<bool>("is_tagged_for_user");
                                bool isForGroup = feedRow.GetValue<bool>("is_for_group");

                                PreparedStatement psDepartmentByUser = session.Prepare(CQLGenerator.SelectStatement("department_by_user",
                                    new List<string> { "department_id" }, new List<string> { "user_id" }));
                                BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(ownerUserId);
                                RowSet departmentByUserRowset = session.Execute(bsDepartmentByUser);

                                List<string> departmentIds = new List<string>();

                                foreach (Row departmentByUserRow in departmentByUserRowset)
                                {
                                    departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                                }

                                int feedType = feedRow.GetValue<int>("feed_type");
                                Notification notificationManager = new Notification();

                                if (!isPostedByAdmin)
                                {
                                    if (CheckPostForCurrentUser(ownerUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, session))
                                    {
                                        if (!ownerUserId.Equals(creatorUserId))
                                        {
                                            // Notification for owner
                                            response.Notifications.Add(GetNotification(companyId, feedId, commentId, string.Empty, creatorUserId, ownerUserId, currentDateTime, feedType, (int)Notification.NotificationFeedSubType.NewReply, session));
                                        }
                                    }
                                }
                                else
                                {
#warning Need loop through department users
                                }

                                // Get all mentioned users for notification
                                List<User> taggedUsers = SelectTaggedUsersFromContent(content, companyId, session);
                                foreach (User taggedUser in taggedUsers)
                                {
                                    if (taggedUser.UserId.Equals(creatorUserId) || taggedUser.UserId.Equals(ownerUserId))
                                    {
                                        continue;
                                    }

                                    FeedSelectPrivacyResponse privacyResponse = SelectFeedPrivacy(taggedUser.UserId, companyId, feedId, session, feedRow);
                                    if (privacyResponse.Success)
                                    {
                                        response.Notifications.Add(GetNotification(companyId, feedId, commentId, string.Empty, creatorUserId, ownerUserId, currentDateTime, feedType, (int)Notification.NotificationFeedSubType.MentionedInReply, session));
                                    }
                                }

                                response.Success = true;
                            }
                            else
                            {
                                Log.Error("Invalid feed comment");
                                response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                                response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                            }

                        }
                        else
                        {
                            Log.Error("Invalid feed post");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                            response.ErrorMessage = ErrorMessage.FeedInvalid;
                        }
                    }
                    else
                    {
                        Log.Error("Empty feed text comment");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTextComment;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public CommentSelectResponse SelectFeedComment(string requesterUserId, string feedId, string companyId)
        {
            CommentSelectResponse response = new CommentSelectResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);

                if (userRow != null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, mainSession);
                    if (feedRow != null)
                    {
                        int feedType = feedRow.GetValue<int>("feed_type");
                        string ownerUserId = feedRow.GetValue<string>("owner_user_id");
                        DateTime feedCreatedOnTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");

                        // Check for privacy
                        Department department = new Department();
                        bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                        bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                        bool isForUser = feedRow.GetValue<bool>("is_for_user");
                        bool isTaggedForUser = feedRow.GetValue<bool>("is_tagged_for_user");
                        bool isForGroup = feedRow.GetValue<bool>("is_for_group");
                        bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                        List<Department> departments = department.GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;
                        List<string> departmentIds = new List<string>();

                        foreach (Department depart in departments)
                        {
                            departmentIds.Add(depart.Id);
                        }

                        if (!CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, mainSession))
                        {
                            Log.Error(string.Format("User {0} not allowed to view feed post: {1}", requesterUserId, feedId));
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedPermissionNotGranted);
                            response.ErrorMessage = ErrorMessage.FeedPermissionNotGranted;
                            return response;
                        }

                        Feed feedPost = GetFeed(feedId, companyId, feedType, feedCreatedOnTimestamp, requesterUserId, new User().SelectUserBasic(ownerUserId, companyId, false, mainSession, null, true).User, mainSession, analyticSession);
                        if (feedPost != null)
                        {
                            if (isForGroup)
                            {
                                if (!ownerUserId.Equals(requesterUserId))
                                {
                                    isForGroup = false;
                                    isForUser = true;
                                }
                            }

                            feedPost.IsForEveryone = isForEveryone;
                            feedPost.IsForDepartment = isForDepartment;
                            feedPost.IsForPersonnel = isForUser;
                            feedPost.IsForGroup = isForGroup;
                            feedPost.IsTaggedForPersonnel = isTaggedForUser;
                        }

                        PreparedStatement psCommentByTimestamp = mainSession.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc",
                            new List<string> { "comment_id", "comment_type", "commentor_user_id" }, new List<string> { "feed_id", "is_comment_valid" }));
                        BoundStatement bsCommentByTimestamp = psCommentByTimestamp.Bind(feedId, true);

                        RowSet commentByTimestampRowset = mainSession.Execute(bsCommentByTimestamp);

                        List<Comment> comments = new List<Comment>();

                        foreach (Row commentByTimestampRow in commentByTimestampRowset)
                        {
                            string commentId = commentByTimestampRow.GetValue<string>("comment_id");
                            int commentType = commentByTimestampRow.GetValue<int>("comment_type");
                            string commentorUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                            User commentorUser =
                                new User().SelectUserBasic(commentorUserId, companyId, false, mainSession, null, true).User;
                            comments.Add(GetCommentWithLatestReply(companyId, feedId, commentId, commentType,
                                requesterUserId, commentorUser, mainSession));
                        }

                        response.Success = true;
                        response.FeedPost = feedPost;
                        response.Comments = comments;
                    }
                    else
                    {
                        Log.Error("Invalid feed");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ReplySelectResponse SelectCommentReply(string requesterUserId,
            string feedId,
            string commentId,
            string companyId)
        {
            ReplySelectResponse response = new ReplySelectResponse();
            response.Success = false;

            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();
            ValidationHandler vh = new ValidationHandler();

            try
            {
                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow != null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                        if (commentRow != null)
                        {
                            int commentType = commentRow.GetValue<int>("comment_type");
                            string commentorUserId = commentRow.GetValue<string>("commentor_user_id");

                            Comment comment = GetCommentWithLatestReply(companyId, feedId, commentId, commentType,
                                requesterUserId,
                                new User().SelectUserBasic(commentorUserId, companyId, false, session, null, true).User,
                                session);

                            PreparedStatement psReplyByTimestamp =
                                session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc",
                                    new List<string> { "reply_id", "reply_type", "commentor_user_id" },
                                    new List<string> { "comment_id", "is_reply_valid" }));
                            BoundStatement bsReplyByTimestamp = psReplyByTimestamp.Bind(commentId, true);

                            RowSet replyByTimestampRowset = session.Execute(bsReplyByTimestamp);

                            List<Reply> replies = new List<Reply>();

                            foreach (Row replyByTimestampRow in replyByTimestampRowset)
                            {
                                string replyId = replyByTimestampRow.GetValue<string>("reply_id");
                                int replyType = replyByTimestampRow.GetValue<int>("reply_type");
                                string repliedUserId = replyByTimestampRow.GetValue<string>("commentor_user_id");

                                User commentorUser =
                                    new User().SelectUserBasic(repliedUserId, companyId, false, session).User;
                                replies.Add(GetReply(companyId, feedId, commentId, replyId, replyType, requesterUserId,
                                    commentorUser, session));
                            }

                            response.Success = true;
                            response.Comment = comment;
                            response.Replies = replies;
                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId, string adminUserId, ISession mainSession = null, ISession analyticSession = null)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ValidationHandler vh = new ValidationHandler();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement psFeedPrivacy = null;
                BoundStatement bsFeedPrivacy = null;

                string modifiedUserId = string.Empty;

                if (!string.IsNullOrEmpty(ownerUserId))
                {
                    Row userRow = vh.ValidateUser(ownerUserId, companyId, mainSession);
                    if (userRow == null)
                    {
                        Log.Error("Invalid user");
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                        new List<string>(), new List<string> { "company_id", "feed_id", "owner_user_id" }));
                    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedId, ownerUserId);

                    modifiedUserId = ownerUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                        new List<string>(), new List<string> { "company_id", "feed_id" }));
                    bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedId);

                    modifiedUserId = adminUserId;
                }

                Row feedPrivacyRow = mainSession.Execute(bsFeedPrivacy).FirstOrDefault();

                if (feedPrivacyRow == null)
                {
                    Log.Error(string.Format("Feed post: {0} does not belong to userId: {1}", feedId, ownerUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isFeedValid = feedPrivacyRow.GetValue<bool>("is_feed_valid");
                    bool isForAdmin = feedPrivacyRow["is_for_admin"] != null
                        ? feedPrivacyRow.GetValue<bool>("is_for_admin")
                        : false;

                    if (!isFeedValid)
                    {
                        Log.Error("Feed is already invalid: " + feedId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement updateBatch = new BatchStatement();
                    //BatchStatement deleteBatch = new BatchStatement();

                    int feedType = feedPrivacyRow.GetValue<int>("feed_type");
                    DateTimeOffset createdTimestamp =
                        feedPrivacyRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_text";
                        isNeedToDeleteFromS3 = false;
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                    {
                        tableName = "feed_image";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                    {
                        tableName = "feed_video";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                    {
                        tableName = "feed_shared_url";
                    }

                    PreparedStatement psInvalidate = mainSession.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "id" },
                        new List<string>
                        {
                            "is_valid",
                            "valid_status",
                            "last_modified_by_user_id",
                            "last_modified_timestamp"
                        }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted,
                        modifiedUserId, DateTime.UtcNow, feedId);
                    updateBatch.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();

                        using (
                            IAmazonS3 s3Client =
                                AWSClientFactory.CreateAmazonS3Client(
                                    WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(),
                                    WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(),
                                    RegionEndpoint.APSoutheast1))
                        {
                            ListObjectsRequest listRequest = new ListObjectsRequest();
                            listRequest.BucketName = bucketName;
                            listRequest.Prefix = "feeds/" + feedId;

                            ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                            foreach (S3Object imageObject in listResponse.S3Objects)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = imageObject.Key;
                                s3Client.DeleteObject(deleteRequest);
                            }

                            DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                            deleteFolderRequest.BucketName = bucketName;
                            deleteFolderRequest.Key = "feeds/" + feedId;
                            s3Client.DeleteObject(deleteFolderRequest);
                        }
                    }

                    psInvalidate = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_privacy",
                        new List<string> { "company_id", "feed_id" },
                        new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, companyId, feedId);
                    updateBatch.Add(bsInvalidate);

                    psInvalidate =
                        mainSession.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                            new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" },
                            new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, companyId,
                        createdTimestamp, feedId);
                    updateBatch.Add(bsInvalidate);

                    psInvalidate = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" },
                        new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, companyId,
                        createdTimestamp, feedId);
                    updateBatch.Add(bsInvalidate);

                    // Update hash tag popularity
                    DeleteHashTag(feedId, companyId, mainSession, analyticSession);

                    //mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);

                    Dashboard dashboard = new Dashboard();

                    // Delete feedback
                    if (isForAdmin)
                    {
                        dashboard.DeleteFeedbackPost(companyId, feedId, mainSession);
                    }

                    // Delete report
                    dashboard.DeleteReport(mainSession, (int)Dashboard.ReportType.Feed, feedId, null, null, companyId,
                        createdTimestamp);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId,
            string commentorUserId, string adminUserId, ISession session = null)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psCommentPrivacy = null;
                BoundStatement bsCommentPrivacy = null;

                string modifiedUserId = string.Empty;

                Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                if (feedRow == null)
                {
                    Log.Error("Invalid FeedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!string.IsNullOrEmpty(commentorUserId))
                {
                    Row userRow = vh.ValidateUser(commentorUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid commentorUserId: " + commentorUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psCommentPrivacy = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "feed_id", "comment_id", "commentor_user_id" }));
                    bsCommentPrivacy = psCommentPrivacy.Bind(feedId, commentId, commentorUserId);

                    modifiedUserId = commentorUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psCommentPrivacy = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                        new List<string>(), new List<string> { "feed_id", "comment_id" }));
                    bsCommentPrivacy = psCommentPrivacy.Bind(feedId, commentId);

                    modifiedUserId = adminUserId;
                }

                Row commentRow = session.Execute(bsCommentPrivacy).FirstOrDefault();

                if (commentRow == null)
                {
                    Log.Error(string.Format("Comment post: {0} does not belong to userId: {1}", commentId,
                        commentorUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isCommentValid = commentRow.GetValue<bool>("is_comment_valid");

                    if (!isCommentValid)
                    {
                        Log.Error("Comment is already invalid: " + commentId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int commentType = commentRow.GetValue<int>("comment_type");
                    DateTimeOffset createdTimestamp = commentRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (commentType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_comment_text";
                        isNeedToDeleteFromS3 = false;
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "feed_id", "id" },
                        new List<string>
                        {
                            "is_valid",
                            "valid_status",
                            "last_modified_by_user_id",
                            "last_modified_timestamp"
                        }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted,
                        modifiedUserId, DateTime.UtcNow, feedId, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {

                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed",
                        new List<string> { "feed_id", "comment_id" },
                        new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, feedId, commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_asc",
                        new List<string> { "feed_id", "created_on_timestamp", "comment_id" },
                        new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, feedId, createdTimestamp,
                        commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_desc",
                        new List<string> { "feed_id", "created_on_timestamp", "comment_id" },
                        new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, feedId, createdTimestamp,
                        commentId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                        new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { -1 }));
                    bsInvalidate = psInvalidate.Bind(feedId);
                    session.Execute(bsInvalidate);

                    // Delete report
                    Dashboard dashboard = new Dashboard();
                    dashboard.DeleteReport(session, (int)Dashboard.ReportType.Comment, feedId, commentId, null,
                        companyId, null);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId,
            string commentorUserId, string adminUserId, ISession session = null)
        {
            FeedDeleteResponse response = new FeedDeleteResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psReplyPrivacy = null;
                BoundStatement bsReplyPrivacy = null;

                string modifiedUserId = string.Empty;

                Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                if (feedRow == null)
                {
                    Log.Error("Invalid FeedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                if (commentRow == null)
                {
                    Log.Error("Invalid CommentId: " + commentId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                    response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                    return response;
                }

                if (!string.IsNullOrEmpty(commentorUserId))
                {
                    Row userRow = vh.ValidateUser(commentorUserId, companyId, session);
                    if (userRow == null)
                    {
                        Log.Error("Invalid commentorUserId: " + commentorUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    psReplyPrivacy = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id", "commentor_user_id" }));
                    bsReplyPrivacy = psReplyPrivacy.Bind(commentId, replyId, commentorUserId);

                    modifiedUserId = commentorUserId;
                }
                else
                {
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    psReplyPrivacy = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                        new List<string>(), new List<string> { "comment_id", "reply_id" }));
                    bsReplyPrivacy = psReplyPrivacy.Bind(commentId, replyId);

                    modifiedUserId = adminUserId;
                }

                Row replyRow = session.Execute(bsReplyPrivacy).FirstOrDefault();

                if (replyRow == null)
                {
                    Log.Error(string.Format("Reply post: {0} does not belong to userId: {1}", replyId, commentorUserId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }
                else
                {
                    bool isReplyValid = replyRow.GetValue<bool>("is_reply_valid");

                    if (!isReplyValid)
                    {
                        Log.Error("Reply is already invalid: " + replyId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                    BatchStatement batchStatement = new BatchStatement();

                    int replyType = replyRow.GetValue<int>("reply_type");
                    DateTimeOffset createdTimestamp = replyRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    string tableName = string.Empty;
                    bool isNeedToDeleteFromS3 = true;
                    if (replyType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_reply_text";
                        isNeedToDeleteFromS3 = false;
                    }

                    PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "comment_id", "id" },
                        new List<string>
                        {
                            "is_valid",
                            "valid_status",
                            "last_modified_by_user_id",
                            "last_modified_timestamp"
                        }, new List<string>()));
                    BoundStatement bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted,
                        modifiedUserId, DateTime.UtcNow, commentId, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    // Delete from S3
                    if (isNeedToDeleteFromS3)
                    {

                    }

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment",
                        new List<string> { "comment_id", "reply_id" },
                        new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, commentId, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_desc",
                        new List<string> { "comment_id", "created_on_timestamp", "reply_id" },
                        new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, commentId,
                        createdTimestamp, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_asc",
                        new List<string> { "comment_id", "created_on_timestamp", "reply_id" },
                        new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                    bsInvalidate = psInvalidate.Bind(false, (int)Feed.FeedValidStatus.Deleted, commentId,
                        createdTimestamp, replyId);
                    batchStatement = batchStatement.Add(bsInvalidate);

                    session.Execute(batchStatement);

                    psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                        new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" }, new List<int> { -1 }));
                    bsInvalidate = psInvalidate.Bind(feedId, commentId);
                    session.Execute(bsInvalidate);

                    // Delete report
                    Dashboard dashboard = new Dashboard();
                    dashboard.DeleteReport(session, (int)Dashboard.ReportType.Comment, feedId, commentId, replyId,
                        companyId, null);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedVisibilityResponse ToggleFeedVisibilty(string feedId, string companyId, string adminUserId,
            Row feedPrivacyRow, bool isHidden, ISession session)
        {
            FeedVisibilityResponse response = new FeedVisibilityResponse();
            response.Success = false;

            try
            {
                string modifiedUserId = adminUserId;
                BatchStatement batchStatement = new BatchStatement();

                int feedType = feedPrivacyRow.GetValue<int>("feed_type");
                DateTimeOffset createdTimestamp = feedPrivacyRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                string tableName = string.Empty;
                if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    tableName = "feed_text";
                }
                else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                {
                    tableName = "feed_image";
                }
                else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                {
                    tableName = "feed_video";
                }
                else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                {
                    tableName = "feed_shared_url";
                }

                bool isValid = !isHidden;

                int validStatus = (int)Feed.FeedValidStatus.Valid;

                if (isHidden)
                {
                    validStatus = (int)Feed.FeedValidStatus.Hidden;
                }

                PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                    new List<string> { "id" },
                    new List<string> { "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" },
                    new List<string>()));
                BoundStatement bsInvalidate = psInvalidate.Bind(isValid, validStatus, modifiedUserId, DateTime.UtcNow,
                    feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy",
                    new List<string> { "company_id", "feed_id" }, new List<string> { "is_feed_valid", "feed_valid_status" },
                    new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, companyId, feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                    new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" },
                    new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, companyId, createdTimestamp, feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                    new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" },
                    new List<string> { "is_feed_valid", "feed_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, companyId, createdTimestamp, feedId);
                batchStatement = batchStatement.Add(bsInvalidate);

                session.Execute(batchStatement);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedVisibilityResponse ToggleCommentVisibilty(string feedId, string commentId, string adminUserId,
            Row commentRow, bool isHidden, ISession session)
        {
            FeedVisibilityResponse response = new FeedVisibilityResponse();
            response.Success = false;

            try
            {
                string modifiedUserId = adminUserId;
                BatchStatement batchStatement = new BatchStatement();

                int commentType = commentRow.GetValue<int>("comment_type");
                DateTimeOffset createdTimestamp = commentRow.GetValue<DateTimeOffset>("created_on_timestamp");

                string tableName = string.Empty;
                if (commentType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    tableName = "feed_comment_text";
                }

                bool isValid = !isHidden;

                int validStatus = (int)Feed.FeedValidStatus.Valid;

                if (isHidden)
                {
                    validStatus = (int)Feed.FeedValidStatus.Hidden;
                }

                PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                    new List<string> { "feed_id", "id" },
                    new List<string> { "is_valid", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" },
                    new List<string>()));
                BoundStatement bsInvalidate = psInvalidate.Bind(isValid, validStatus, modifiedUserId, DateTime.UtcNow,
                    feedId, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed",
                    new List<string> { "feed_id", "comment_id" },
                    new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, feedId, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_asc",
                    new List<string> { "feed_id", "created_on_timestamp", "comment_id" },
                    new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, feedId, createdTimestamp, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_desc",
                    new List<string> { "feed_id", "created_on_timestamp", "comment_id" },
                    new List<string> { "is_comment_valid", "comment_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, feedId, createdTimestamp, commentId);
                batchStatement = batchStatement.Add(bsInvalidate);

                session.Execute(batchStatement);

                psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                    new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { isHidden ? -1 : 1 }));
                bsInvalidate = psInvalidate.Bind(feedId);
                session.Execute(bsInvalidate);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedVisibilityResponse ToggleReplyVisibilty(string feedId, string commentId, string replyId,
            string adminUserId, Row replyRow, bool isHidden, ISession session)
        {
            FeedVisibilityResponse response = new FeedVisibilityResponse();
            response.Success = false;

            try
            {
                string modifiedUserId = adminUserId;
                BatchStatement batchStatement = new BatchStatement();

                int replyType = replyRow.GetValue<int>("reply_type");
                DateTimeOffset createdTimestamp = replyRow.GetValue<DateTimeOffset>("created_on_timestamp");

                string tableName = string.Empty;
                if (replyType == Int16.Parse(FeedTypeCode.TextPost))
                {
                    tableName = "feed_reply_text";
                }

                bool isValid = !isHidden;

                int validStatus = (int)Feed.FeedValidStatus.Valid;

                if (isHidden)
                {
                    validStatus = (int)Feed.FeedValidStatus.Hidden;
                }

                PreparedStatement psInvalidate = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                    new List<string> { "comment_id", "id" },
                    new List<string> { "is_valid", "validStatus", "last_modified_by_user_id", "last_modified_timestamp" },
                    new List<string>()));
                BoundStatement bsInvalidate = psInvalidate.Bind(isValid, validStatus, modifiedUserId, DateTime.UtcNow,
                    commentId, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment",
                    new List<string> { "comment_id", "reply_id" },
                    new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, commentId, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_desc",
                    new List<string> { "comment_id", "created_on_timestamp", "reply_id" },
                    new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, commentId, createdTimestamp, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                psInvalidate = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_asc",
                    new List<string> { "comment_id", "created_on_timestamp", "reply_id" },
                    new List<string> { "is_reply_valid", "reply_valid_status" }, new List<string>()));
                bsInvalidate = psInvalidate.Bind(isValid, validStatus, commentId, createdTimestamp, replyId);
                batchStatement = batchStatement.Add(bsInvalidate);

                session.Execute(batchStatement);

                psInvalidate = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" },
                    new List<int> { isHidden ? -1 : 1 }));
                bsInvalidate = psInvalidate.Bind(feedId, commentId);
                session.Execute(bsInvalidate);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }


        public PointUpdateResponse UpdateFeedPoint(string voterUserId,
            string feedId,
            string companyId,
            bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Notification = new Notification();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        int updateVoteResult = UpdateFeedPointCounter(voterUserId, feedId, isUpVote, session);

                        if (updateVoteResult == (int)VoteStatus.Upvote && isUpVote)
                        {
                            string ownerUserId = feedRow.GetValue<string>("owner_user_id");

                            if (!voterUserId.Equals(ownerUserId))
                            {
                                int feedType = feedRow.GetValue<int>("feed_type");
                                response.Notification = GetNotification(companyId, feedId, string.Empty, string.Empty, voterUserId, ownerUserId, DateTime.UtcNow, feedType, (int)Notification.NotificationFeedSubType.UpvotePost, session);
                            }

                        }

                        // Get pointers
                        Dictionary<string, object> pointDict = GetPointsForFeed(feedId, companyId, session);
                        int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : (int)pointDict["negativePoint"];
                        int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : (int)pointDict["positivePoint"];

                        response.Success = true;
                        response.HasVoted = updateVoteResult != (int)VoteStatus.Reset ? true : false;
                        response.IsUpVoted = updateVoteResult == (int)VoteStatus.Upvote ? true : false;
                        response.NegativePoints = negativePoints;
                        response.PositivePoints = positivePoints;

                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateCommentPoint(string voterUserId,
            string feedId,
            string commentId,
            string companyId,
            bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Notification = new Notification();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                        if (commentRow != null)
                        {
                            int updateVoteResult = UpdateCommentPointCounter(voterUserId, feedId, commentId, isUpVote,
                                session);
                            if (updateVoteResult == (int)VoteStatus.Upvote && isUpVote)
                            {
                                string ownerUserId = commentRow.GetValue<string>("commentor_user_id");
                                if (!voterUserId.Equals(ownerUserId))
                                {
                                    int feedType = feedRow.GetValue<int>("feed_type");
                                    response.Notification = GetNotification(companyId, feedId, commentId, string.Empty, voterUserId, ownerUserId, DateTime.UtcNow, feedType, (int)Notification.NotificationFeedSubType.UpvoteComment, session);
                                }

                            }
                            // Get pointers
                            Dictionary<string, object> pointDict = GetPointsForComment(feedId, commentId, session);
                            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : (int)pointDict["negativePoint"];
                            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : (int)pointDict["positivePoint"];

                            response.Success = true;
                            response.HasVoted = updateVoteResult != (int)VoteStatus.Reset ? true : false;
                            response.IsUpVoted = updateVoteResult == (int)VoteStatus.Upvote ? true : false;
                            response.NegativePoints = negativePoints;
                            response.PositivePoints = positivePoints;
                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PointUpdateResponse UpdateReplyPoint(string voterUserId,
            string feedId,
            string commentId,
            string replyId,
            string companyId,
            bool isUpVote)
        {
            PointUpdateResponse response = new PointUpdateResponse();
            response.Notification = new Notification();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(voterUserId, companyId, session);
                if (hasErrorAsUser == null)
                {
                    Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);
                    if (feedRow != null)
                    {
                        if (vh.ValidateFeedComment(feedId, commentId, session) != null)
                        {
                            Row replyRow = vh.ValidateCommentReply(commentId, replyId, session);
                            if (replyRow != null)
                            {
                                int updateVoteResult = UpdateReplyPointCounter(voterUserId, commentId, replyId, isUpVote,
                                    session);
                                if (updateVoteResult == (int)VoteStatus.Upvote && isUpVote)
                                {
                                    string ownerUserId = replyRow.GetValue<string>("commentor_user_id");
                                    if (!voterUserId.Equals(ownerUserId))
                                    {
                                        int feedType = feedRow.GetValue<int>("feed_type");
                                        response.Notification = GetNotification(companyId, feedId, commentId, replyId, voterUserId, ownerUserId, DateTime.UtcNow, feedType, (int)Notification.NotificationFeedSubType.UpvoteReply, session);
                                    }
                                }

                                // Get pointers
                                Dictionary<string, object> pointDict = GetPointsForReply(commentId, replyId, session);
                                int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : (int)pointDict["negativePoint"];
                                int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : (int)pointDict["positivePoint"];

                                response.Success = true;
                                response.HasVoted = updateVoteResult != (int)VoteStatus.Reset ? true : false;
                                response.IsUpVoted = updateVoteResult == (int)VoteStatus.Upvote ? true : false;
                                response.NegativePoints = negativePoints;
                                response.PositivePoints = positivePoints;
                            }
                            else
                            {
                                Log.Error("Invalid reply");
                                response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidReply);
                                response.ErrorMessage = ErrorMessage.FeedInvalidReply;
                            }

                        }
                        else
                        {
                            Log.Error("Invalid comment");
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid feed post");
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectPrivacyResponse SelectFeedPrivacy(string requesterUserId,
                                                           string companyId,
                                                           string feedId,
                                                           ISession session = null,
                                                           Row feedRow = null)
        {
            FeedSelectPrivacyResponse response = new FeedSelectPrivacyResponse();
            response.Users = new List<User>();
            response.Departments = new List<Department>();
            response.Groups = new List<Group>();
            response.TaggedUsers = new List<User>();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();

                    Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                    if (userRow == null)
                    {
                        Log.Error("Invalid userId: " + requesterUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    feedRow = vh.ValidateFeedPost(feedId, companyId, session);

                    if (feedRow == null)
                    {
                        Log.Error("Invalid feedId: " + feedId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                        response.ErrorMessage = ErrorMessage.FeedInvalid;
                        return response;
                    }

                }

                string ownerUserId = feedRow.GetValue<string>("owner_user_id");
                bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                bool isForUser = feedRow.GetValue<bool>("is_for_user");
                bool isTaggedForUser = feedRow.GetValue<bool>("is_tagged_for_user");
                bool isForGroup = feedRow.GetValue<bool>("is_for_group");
                bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                Department departmentManager = new Department();
                Group groupManager = new Group();
                User userManager = new User();

                List<Department> departments = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;
                List<string> departmentIds = new List<string>();

                foreach (Department depart in departments)
                {
                    departmentIds.Add(depart.Id);
                }

                if (!ownerUserId.Equals(requesterUserId))
                {
                    if (!CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, session))
                    {
                        Log.Error(string.Format("Feed post: {0} not available for user: {1}", feedId, requesterUserId));
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTargetedAudience);
                        response.ErrorMessage = ErrorMessage.FeedInvalidTargetedAudience;
                        return response;
                    }
                }

                PreparedStatement ps = null;
                if (isForGroup)
                {
                    if (requesterUserId.Equals(ownerUserId))
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_group",
                          new List<string>(), new List<string> { "feed_id" }));
                        RowSet groupRowset = session.Execute(ps.Bind(feedId));

                        foreach (Row groupRow in groupRowset)
                        {
                            string groupId = groupRow.GetValue<string>("group_id");
                            Group selectedGroup = groupManager.SelectGroup(ownerUserId, companyId, groupId, (int)Group.GroupQueryType.Basic, session).Group;

                            if (selectedGroup != null)
                            {
                                response.Groups.Add(selectedGroup);
                            }
                        }

                        response.Groups = response.Groups.OrderBy(g => g.GroupName).ToList();
                        response.IsForGroup = true;
                    }
                    else
                    {
                        isForGroup = false;
                        isForUser = true;

                        ps = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_group",
                            new List<string>(), new List<string> { "feed_id" }));
                        RowSet groupRowset = session.Execute(ps.Bind(feedId));

                        foreach (Row groupRow in groupRowset)
                        {
                            string groupId = groupRow.GetValue<string>("group_id");
                            Group selectedGroup = groupManager.SelectGroup(ownerUserId, companyId, groupId, (int)Group.GroupQueryType.FullDetail, session).Group;

                            if (selectedGroup != null)
                            {
                                response.Users.AddRange(selectedGroup.Members);
                            }
                        }

                        response.Users = response.Users.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
                        response.IsForUser = true;
                    }
                }

                if (isForDepartment)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_department",
                        new List<string>(), new List<string> { "feed_id" }));
                    RowSet departmentRowset = session.Execute(ps.Bind(feedId));

                    foreach (Row departmentRow in departmentRowset)
                    {
                        string departmentId = departmentRow.GetValue<string>("department_id");
                        Department selectedDepartment = departmentManager.GetDepartmentDetail(requesterUserId, companyId, departmentId, Department.QUERY_TYPE_BASIC, session).Department;

                        if (selectedDepartment != null)
                        {
                            response.Departments.Add(selectedDepartment);
                        }
                    }

                    response.Departments = response.Departments.OrderBy(d => d.Title).ToList();
                    response.IsForDepartment = true;
                }

                if (isForUser)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_user",
                        new List<string>(), new List<string> { "feed_id" }));
                    RowSet userRowset = session.Execute(ps.Bind(feedId));

                    foreach (Row targetedUserRow in userRowset)
                    {
                        string userId = targetedUserRow.GetValue<string>("user_id");
                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, session, null, true).User;

                        if (selectedUser != null)
                        {
                            response.Users.Add(selectedUser);
                        }
                    }

                    response.Users = response.Users.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
                    response.IsForUser = true;
                }

                if (isTaggedForUser)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_tagged_user",
                            new List<string>(), new List<string> { "feed_id" }));
                    RowSet userRowset = session.Execute(ps.Bind(feedId));

                    foreach (Row taggedUserRow in userRowset)
                    {
                        string userId = taggedUserRow.GetValue<string>("user_id");
                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, session, null, true).User;

                        if (selectedUser != null)
                        {
                            response.TaggedUsers.Add(selectedUser);
                        }
                    }

                    response.TaggedUsers = response.TaggedUsers.OrderBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
                    response.IsTaggedForUser = true;
                }

                response.IsForEveryone = isForEveryone;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse SelectCompanyFeedPostWithSearch(string requesterUserId,
                                                                  string companyId,
                                                                  string searchContent,
                                                                  string searchPersonnel,
                                                                  string searchHashTag,
                                                                  int numberOfPostsLoaded,
                                                                  DateTime? newestTimestamp,
                                                                  DateTime? oldestTimestamp)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Feeds = new List<Feed>();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["feed_limit"]);

                if (numberOfPostsLoaded > 0)
                {
                    limit += numberOfPostsLoaded;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                PreparedStatement psDepartmentByUser = mainSession.Prepare(CQLGenerator.SelectStatement("department_by_user",
                            new List<string> { "department_id" }, new List<string> { "user_id" }));
                BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(requesterUserId);
                RowSet departmentByUserRowset = mainSession.Execute(bsDepartmentByUser);

                List<string> departmentIds = new List<string>();

                foreach (Row departmentByUserRow in departmentByUserRowset)
                {
                    departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                }

                List<Feed> feedPosts = new List<Feed>();

                PreparedStatement psFeedPrivacy = null;
                BoundStatement bsFeedPrivacy = null;

                // Update analytic search with thread
                // Put the searchUserID as the search content
                if (!string.IsNullOrEmpty(searchPersonnel))
                {
                    searchContent = searchPersonnel;
                    Thread updateAnalyticThread = new Thread(() => new AnalyticSearch().UpdateSearchPersonnelAnalytics(companyId, requesterUserId, searchPersonnel, DateTime.UtcNow, analyticSession));
                    updateAnalyticThread.Start();
                }
                else if (!string.IsNullOrEmpty(searchHashTag))
                {
                    searchHashTag = searchHashTag.ToLower().Trim();
                    Thread updateAnalyticThread = new Thread(() => new AnalyticSearch().UpdateSearchHashTagAnalytics(companyId, requesterUserId, searchHashTag, DateTime.UtcNow, analyticSession));
                    updateAnalyticThread.Start();
                }
                else if (!string.IsNullOrEmpty(searchContent))
                {
                    Thread updateAnalyticThread = new Thread(() => new AnalyticSearch().UpdateSearchContentAnalytics(companyId, requesterUserId, searchContent, DateTime.UtcNow, analyticSession));
                    updateAnalyticThread.Start();
                }

                bool isSelectCompleted = false;
                while (isSelectCompleted == false)
                {
                    List<Row> allFeedRowList = new List<Row>();

                    if (oldestTimestamp != null)
                    {
                        DateTime currentDateTime = oldestTimestamp.Value;

                        if (!string.IsNullOrEmpty(searchHashTag))
                        {
                            PreparedStatement psHashTag = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("small_cap_tag_by_sorted_timestamp",
                                new List<string>(), new List<string> { "company_id", "small_cap_tag" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                            RowSet feedByHashTagRowset = mainSession.Execute(psHashTag.Bind(companyId, searchHashTag, currentDateTime));

                            foreach (Row feedByHashTagRow in feedByHashTagRowset)
                            {
                                string feedId = feedByHashTagRow.GetValue<string>("feed_id");
                                DateTime feedCreatedTimestamp = feedByHashTagRow.GetValue<DateTime>("feed_created_on_timestamp");

                                psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_desc",
                                    new List<string>(), new List<string> { "company_id", "feed_created_on_timestamp", "feed_id", "is_feed_valid" }));
                                bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedCreatedTimestamp, feedId, true);

                                Row feedRow = mainSession.Execute(bsFeedPrivacy).FirstOrDefault();
                                if (feedRow != null)
                                {
                                    allFeedRowList.Add(feedRow);
                                }
                            }
                        }
                        else
                        {
                            psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp_desc",
                                new List<string>(), new List<string> { "company_id", "is_feed_valid" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true, currentDateTime);

                            RowSet allFeedRowSet = mainSession.Execute(bsFeedPrivacy);
                            allFeedRowList = allFeedRowSet.GetRows().ToList();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(searchHashTag))
                        {
                            PreparedStatement psHashTag = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("small_cap_tag_by_sorted_timestamp",
                                new List<string>(), new List<string> { "company_id", "small_cap_tag" }, limit));
                            RowSet feedByHashTagRowset = mainSession.Execute(psHashTag.Bind(companyId, searchHashTag));

                            foreach (Row feedByHashTagRow in feedByHashTagRowset)
                            {
                                string feedId = feedByHashTagRow.GetValue<string>("feed_id");
                                DateTime feedCreatedTimestamp = feedByHashTagRow.GetValue<DateTime>("feed_created_on_timestamp");

                                psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_desc",
                                    new List<string>(), new List<string> { "company_id", "feed_created_on_timestamp", "feed_id", "is_feed_valid" }));
                                bsFeedPrivacy = psFeedPrivacy.Bind(companyId, feedCreatedTimestamp, feedId, true);

                                Row feedRow = mainSession.Execute(bsFeedPrivacy).FirstOrDefault();
                                if (feedRow != null)
                                {
                                    allFeedRowList.Add(feedRow);
                                }
                            }
                        }
                        else
                        {
                            psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp_desc",
                                new List<string>(), new List<string> { "company_id", "is_feed_valid" }, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true);

                            RowSet allFeedRowSet = mainSession.Execute(bsFeedPrivacy);
                            allFeedRowList = allFeedRowSet.GetRows().ToList();
                        }
                    }

                    foreach (Row allFeedRow in allFeedRowList)
                    {
                        bool isPostForCurrentUser = false;

                        int feedType = allFeedRow.GetValue<int>("feed_type");
                        string feedId = allFeedRow.GetValue<string>("feed_id");
                        string ownerUserId = allFeedRow.GetValue<string>("owner_user_id");
                        bool isForEveryone = allFeedRow.GetValue<bool>("is_for_everyone");
                        bool isForDepartment = allFeedRow.GetValue<bool>("is_for_department");
                        bool isForUser = allFeedRow.GetValue<bool>("is_for_user");
                        bool isTaggedForUser = allFeedRow.GetValue<bool>("is_tagged_for_user");
                        bool isForGroup = allFeedRow.GetValue<bool>("is_for_group");
                        bool isForOnlyMe = allFeedRow.GetValue<bool>("is_for_only_me");
                        DateTime feedCreatedOnTimestamp = allFeedRow.GetValue<DateTime>("feed_created_on_timestamp");

                        oldestTimestamp = feedCreatedOnTimestamp;

                        if (!string.IsNullOrEmpty(searchContent))
                        {
                            string feedTextContent = allFeedRow.GetValue<string>("feed_text_content");
                            bool isMatched = feedTextContent.Contains(searchContent);

                            if (ownerUserId.Equals(requesterUserId))
                            {
                                if (isMatched)
                                {
                                    isPostForCurrentUser = true;
                                }
                            }
                            else
                            {
                                if (isMatched)
                                {
                                    isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, mainSession);
                                }
                            }
                        }
                        else
                        {
                            if (ownerUserId.Equals(requesterUserId))
                            {
                                isPostForCurrentUser = true;
                            }
                            else
                            {
                                isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, mainSession);
                            }
                        }

                        if (isPostForCurrentUser)
                        {
                            User user = new User();
                            Feed feed = GetFeed(feedId, companyId, feedType, feedCreatedOnTimestamp, requesterUserId, user.SelectUserBasic(ownerUserId, companyId, false, mainSession, null, true).User, mainSession, analyticSession, true);

                            if (feed != null)
                            {
                                if (isForGroup)
                                {
                                    if (!ownerUserId.Equals(requesterUserId))
                                    {
                                        isForGroup = false;
                                        isForUser = true;
                                    }
                                }

                                feed.IsForEveryone = isForEveryone;
                                feed.IsForDepartment = isForDepartment;
                                feed.IsForPersonnel = isForUser;
                                feed.IsForGroup = isForGroup;
                                feed.IsTaggedForPersonnel = isTaggedForUser;
                                feed.IsForOnlyMe = isForOnlyMe;

                                feedPosts.Add(feed);

                                if (feedPosts.Count() == limit)
                                {
                                    isSelectCompleted = true;
                                    break;
                                }
                            }

                        }
                    } // for

                    if (allFeedRowList.Count() == 0)
                    {
                        isSelectCompleted = true;
                    }
                } // while

                response.Feeds = feedPosts;

                NotificationSelectNumberResponse notificationResponse =
                    new Notification().SelectNotificationNumberByUser(requesterUserId, companyId, mainSession);

                if (notificationResponse.Success)
                {
                    response.NumberOfNewNotification = notificationResponse.NumberOfNotification;
                }
                else
                {
                    response.NumberOfNewNotification = 0;
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

#warning: 'Deprecated code in SelectCompanyFeedPost, replaced by SelectCompanyFeedPostWithSearch'
        public FeedSelectResponse SelectCompanyFeedPost(string requesterUserId,
                                                        string companyId,
                                                        string searchContent,
                                                        int numberOfPostsLoaded,
                                                        DateTime? newestTimestamp,
                                                        DateTime? oldestTimestamp)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Feeds = new List<Feed>();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["feed_limit"]);

                if (numberOfPostsLoaded > 0)
                {
                    limit += numberOfPostsLoaded;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);

                if (userRow != null)
                {
                    PreparedStatement psDepartmentByUser = mainSession.Prepare(CQLGenerator.SelectStatement("department_by_user",
                            new List<string> { "department_id" }, new List<string> { "user_id" }));
                    BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(requesterUserId);
                    RowSet departmentByUserRowset = mainSession.Execute(bsDepartmentByUser);

                    List<string> departmentIds = new List<string>();

                    foreach (Row departmentByUserRow in departmentByUserRowset)
                    {
                        departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                    }

                    List<Feed> feedPosts = new List<Feed>();

                    PreparedStatement psFeedPrivacy = null;
                    BoundStatement bsFeedPrivacy = null;

                    bool isSelectCompleted = false;
                    while (isSelectCompleted == false)
                    {
                        if (oldestTimestamp != null)
                        {
                            DateTime currentDateTime = oldestTimestamp.Value;
                            psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp_desc",
                                null, new List<string> { "company_id", "is_feed_valid" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true, DateHelper.ConvertDateToLong(currentDateTime));
                        }
                        else
                        {
                            psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp_desc",
                                new List<string>(), new List<string> { "company_id", "is_feed_valid" }, limit));
                            bsFeedPrivacy = psFeedPrivacy.Bind(companyId, true);
                        }

                        RowSet allFeedRowSet = mainSession.Execute(bsFeedPrivacy);
                        List<Row> allFeedRowList = new List<Row>();
                        allFeedRowList = allFeedRowSet.GetRows().ToList();

                        foreach (Row allFeedRow in allFeedRowList)
                        {
                            bool isPostForCurrentUser = false;

                            int feedType = allFeedRow.GetValue<int>("feed_type");
                            string feedId = allFeedRow.GetValue<string>("feed_id");
                            string ownerUserId = allFeedRow.GetValue<string>("owner_user_id");
                            bool isForEveryone = allFeedRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = allFeedRow.GetValue<bool>("is_for_department");
                            bool isForUser = allFeedRow.GetValue<bool>("is_for_user");
                            bool isTaggedForUser = allFeedRow.GetValue<bool>("is_tagged_for_user");
                            bool isForGroup = allFeedRow.GetValue<bool>("is_for_group");
                            bool isForOnlyMe = allFeedRow.GetValue<bool>("is_for_only_me");
                            DateTime feedCreatedOnTimestamp = allFeedRow.GetValue<DateTime>("feed_created_on_timestamp");

                            oldestTimestamp = feedCreatedOnTimestamp;

                            if (!string.IsNullOrEmpty(searchContent))
                            {
                                string feedTextContent = allFeedRow.GetValue<string>("feed_text_content");
                                bool isMatched = feedTextContent.Contains(searchContent);

                                if (ownerUserId.Equals(requesterUserId))
                                {
                                    if (isMatched)
                                    {
                                        isPostForCurrentUser = true;
                                    }
                                }
                                else
                                {
                                    if (isMatched)
                                    {
                                        isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, mainSession);
                                    }
                                }
                            }
                            else
                            {
                                if (ownerUserId.Equals(requesterUserId))
                                {
                                    isPostForCurrentUser = true;
                                }
                                else
                                {
                                    isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, mainSession);
                                }
                            }

                            if (isPostForCurrentUser)
                            {
                                User user = new User();
                                Feed feed = GetFeed(feedId, companyId, feedType, feedCreatedOnTimestamp, requesterUserId, user.SelectUserBasic(ownerUserId, companyId, false, mainSession, null, true).User, mainSession, analyticSession, true);

                                if (feed != null)
                                {
                                    if (isForGroup)
                                    {
                                        if (!ownerUserId.Equals(requesterUserId))
                                        {
                                            isForGroup = false;
                                            isForUser = true;
                                        }
                                    }

                                    feed.IsForEveryone = isForEveryone;
                                    feed.IsForDepartment = isForDepartment;
                                    feed.IsForPersonnel = isForUser;
                                    feed.IsForGroup = isForGroup;
                                    feed.IsTaggedForPersonnel = isTaggedForUser;
                                    feed.IsForOnlyMe = isForOnlyMe;

                                    feedPosts.Add(feed);

                                    if (feedPosts.Count() == limit)
                                    {
                                        isSelectCompleted = true;
                                        break;
                                    }
                                }

                            }
                        } // for

                        if (allFeedRowList.Count() == 0)
                        {
                            isSelectCompleted = true;
                        }
                    } // while

                    response.Feeds = feedPosts;

                    NotificationSelectNumberResponse notificationResponse =
                        new Notification().SelectNotificationNumberByUser(requesterUserId, companyId, mainSession);

                    if (notificationResponse.Success)
                    {
                        response.NumberOfNewNotification = notificationResponse.NumberOfNotification;
                    }
                    else
                    {
                        response.NumberOfNewNotification = 0;
                    }

                    response.Success = true;
                }
                else
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectResponse SelectPersonnelFeedPost(string requesterUserId,
                                                          string ownerUserId,
                                                          string companyId,
                                                          string searchContent,
                                                          DateTime? newestTimestamp,
                                                          DateTime? oldestTimestamp)
        {
            FeedSelectResponse response = new FeedSelectResponse();
            response.Feeds = new List<Feed>();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["feed_limit"]);

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                BatchStatement batchStatement = new BatchStatement();

                Row requesterUserRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (requesterUserRow == null)
                {
                    Log.Error("Invalid requester user id: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                PreparedStatement psDepartmentByUser =
                    mainSession.Prepare(CQLGenerator.SelectStatement("department_by_user",
                        new List<string> { "department_id" }, new List<string> { "user_id" }));
                BoundStatement bsDepartmentByUser = psDepartmentByUser.Bind(requesterUserId);
                RowSet departmentByUserRowset = mainSession.Execute(bsDepartmentByUser);

                List<string> departmentIds = new List<string>();

                foreach (Row departmentByUserRow in departmentByUserRowset)
                {
                    departmentIds.Add(departmentByUserRow.GetValue<string>("department_id"));
                }

                List<Feed> feedPosts = new List<Feed>();

                PreparedStatement psFeedPrivacy = null;
                BoundStatement bsFeedPrivacy = null;

                bool isSelectCompleted = false;
                while (isSelectCompleted == false)
                {
                    if (oldestTimestamp != null)
                    {
                        DateTime currentDateTime = oldestTimestamp.Value;
                        psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("feed_privacy_by_company_timestamp_desc",
                            null, new List<string> { "company_id", "owner_user_id" }, "feed_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId,
                            DateHelper.ConvertDateToLong(currentDateTime));
                    }
                    else
                    {
                        psFeedPrivacy = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("feed_privacy_by_company_timestamp_desc",
                            new List<string>(), new List<string> { "company_id", "owner_user_id" }, limit));
                        bsFeedPrivacy = psFeedPrivacy.Bind(companyId, ownerUserId);
                    }

                    RowSet allFeedRowSet = mainSession.Execute(bsFeedPrivacy);
                    List<Row> allFeedRowList = new List<Row>();
                    allFeedRowList = allFeedRowSet.GetRows().ToList();

                    foreach (Row allFeedRow in allFeedRowList)
                    {
                        bool isFeedValid = allFeedRow.GetValue<bool>("is_feed_valid");
                        DateTime feedCreatedOnTimestamp = allFeedRow.GetValue<DateTime>("feed_created_on_timestamp");

                        oldestTimestamp = feedCreatedOnTimestamp;

                        if (isFeedValid)
                        {
                            bool isPostForCurrentUser = false;

                            int feedType = allFeedRow.GetValue<int>("feed_type");
                            string feedId = allFeedRow.GetValue<string>("feed_id");
                            bool isForEveryone = allFeedRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = allFeedRow.GetValue<bool>("is_for_department");
                            bool isForUser = allFeedRow.GetValue<bool>("is_for_user");
                            bool isTaggedForUser = allFeedRow.GetValue<bool>("is_tagged_for_user");
                            bool isForGroup = allFeedRow.GetValue<bool>("is_for_group");
                            bool isForOnlyMe = allFeedRow.GetValue<bool>("is_for_only_me");

                            if (!string.IsNullOrEmpty(searchContent))
                            {
                                string feedTextContent = allFeedRow.GetValue<string>("feed_text_content");
                                bool isMatched = feedTextContent.Contains(searchContent);

                                if (isMatched)
                                {
                                    isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, mainSession);
                                }
                            }
                            else
                            {
                                isPostForCurrentUser = CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, mainSession);
                            }

                            if (isPostForCurrentUser)
                            {
                                User user = new User();

                                Feed feed = GetFeed(feedId, companyId, feedType, feedCreatedOnTimestamp, requesterUserId, user.SelectUserBasic(ownerUserId, companyId, false, mainSession, null, true).User, mainSession, analyticSession, true);
                                if (feed != null)
                                {
                                    if (isForGroup)
                                    {
                                        if (!ownerUserId.Equals(requesterUserId))
                                        {
                                            isForGroup = false;
                                            isForUser = true;
                                        }
                                    }

                                    feed.IsForEveryone = isForEveryone;
                                    feed.IsForDepartment = isForDepartment;
                                    feed.IsForPersonnel = isForUser;
                                    feed.IsForGroup = isForGroup;
                                    feed.IsTaggedForPersonnel = isTaggedForUser;
                                    feed.IsForOnlyMe = isForOnlyMe;

                                    feedPosts.Add(feed);

                                    if (feedPosts.Count == limit)
                                    {
                                        isSelectCompleted = true;
                                        break;
                                    }
                                }

                            }
                        }

                    } // for

                    if (allFeedRowList.Count == 0)
                    {
                        isSelectCompleted = true;
                    }

                } // while

                response.Feeds = feedPosts;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool CheckPostForCurrentUser(string userId, string feedId, string feedOwnerUserId, bool isForEveryone, bool isForDepartment, bool isForUser, bool isTaggedForUser, bool isForGroup, List<string> departmentIds, ISession session)
        {
            bool isPostForCurrentUser = false;

            if (userId.Equals(feedOwnerUserId))
            {
                isPostForCurrentUser = true;
            }
            else
            {
                if (isForEveryone)
                {
                    isPostForCurrentUser = true;
                }
                else
                {
                    if (isForGroup)
                    {
                        PreparedStatement psGroup = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_group",
                            new List<string>(), new List<string> { "feed_id" }));
                        RowSet groupRowset = session.Execute(psGroup.Bind(feedId));
                        foreach (Row groupRow in groupRowset)
                        {
                            string groupId = groupRow.GetValue<string>("group_id");

                            psGroup = session.Prepare(CQLGenerator.SelectStatement("group_member",
                                new List<string>(), new List<string> { "group_id", "user_id" }));
                            Row memberRow = session.Execute(psGroup.Bind(groupId, userId)).FirstOrDefault();
                            if (memberRow != null)
                            {
                                isPostForCurrentUser = true;
                                break;
                            }
                        }
                    }

                    if (isForDepartment)
                    {
                        foreach (string departmentId in departmentIds)
                        {
                            PreparedStatement psFeedTargetedDepartment = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_department",
                                    new List<string>(), new List<string> { "feed_id", "department_id" }));
                            Row departmentPrivacyRow = session.Execute(psFeedTargetedDepartment.Bind(feedId, departmentId)).FirstOrDefault();

                            if (departmentPrivacyRow != null)
                            {
                                isPostForCurrentUser = true;
                                break;
                            }
                        }
                    }

                    if (!isPostForCurrentUser && isForUser)
                    {
                        PreparedStatement psFeedTargetedUser = session.Prepare(CQLGenerator.SelectStatement("feed_targeted_user",
                            new List<string>(), new List<string> { "feed_id", "user_id" }));
                        Row userPrivacyRow = session.Execute(psFeedTargetedUser.Bind(feedId, userId)).FirstOrDefault();

                        if (userPrivacyRow != null)
                        {
                            isPostForCurrentUser = true;
                        }
                    }

                    if (!isPostForCurrentUser && isTaggedForUser)
                    {
                        PreparedStatement psFeedTaggedUser = session.Prepare(CQLGenerator.SelectStatement("feed_tagged_user",
                               new List<string>(), new List<string> { "feed_id", "user_id" }));
                        Row userTaggedRow = session.Execute(psFeedTaggedUser.Bind(feedId, userId)).FirstOrDefault();

                        if (userTaggedRow != null)
                        {
                            isPostForCurrentUser = true;
                        }
                    }
                }
            }

            return isPostForCurrentUser;
        }

        private Dictionary<string, bool> CheckVoteForFeed(string userId, string feedId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psFeedPoint = session.Prepare(CQLGenerator.SelectStatement("feed_point",
                new List<string>(), new List<string> { "feed_id", "user_id" }));
            BoundStatement bsFeedPoint = psFeedPoint.Bind(feedId, userId);

            Row feedPointRow = session.Execute(bsFeedPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (feedPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = feedPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Dictionary<string, bool> CheckVoteForComment(string userId, string commentId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                new List<string>(), new List<string> { "comment_id", "user_id" }));
            BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId, userId);

            Row commentPointRow = session.Execute(bsCommentPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (commentPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = commentPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Dictionary<string, bool> CheckVoteForReply(string userId, string replyId, ISession session)
        {
            Dictionary<string, bool> resultDict = new Dictionary<string, bool>();

            PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                new List<string>(), new List<string> { "reply_id", "user_id" }));
            BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId, userId);

            Row replyPointRow = session.Execute(bsReplyPoint).FirstOrDefault();

            resultDict.Add("hasVoted", false);
            resultDict.Add("isUpVoted", false);

            if (replyPointRow != null)
            {
                resultDict["hasVoted"] = true;

                int value = replyPointRow.GetValue<int>("value");

                resultDict["isUpVoted"] = value == 1 ? true : false;
            }

            return resultDict;
        }

        private Comment GetLatestComment(string requesterUserId, string companyId, string feedId, ISession session)
        {
            Comment comment = null;
            PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithLimit("comment_by_feed_timestamp_desc",
                new List<string> { "comment_id", "comment_type", "commentor_user_id" }, new List<string> { "feed_id", "is_comment_valid" }, 1));
            Row commentByTimestampRow = session.Execute(ps.Bind(feedId, true)).FirstOrDefault();

            if (commentByTimestampRow != null)
            {
                string commentId = commentByTimestampRow.GetValue<string>("comment_id");
                int commentType = commentByTimestampRow.GetValue<int>("comment_type");
                string commentorUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                User commentorUser = new User().SelectUserBasic(commentorUserId, companyId, false, session, null, true).User;
                comment = GetCommentWithLatestReply(companyId, feedId, commentId, commentType, requesterUserId, commentorUser, session, false);
            }
            return comment;
        }

        private Comment GetCommentWithLatestReply(string companyId, string feedId, string commentId, int commentType, string requesterUserId, User commentorUser, ISession session, bool isRequiredLatestReply = true)
        {
            Comment comment = null;
            Reply latestReply = new Reply();
            int numberOfReplies = 0;

            if (isRequiredLatestReply)
            {
                // Get latest reply
                PreparedStatement psLatestReply = session.Prepare(CQLGenerator.SelectStatementWithLimit("reply_by_comment_timestamp_desc",
                        new List<string> { "reply_id", "reply_type", "commentor_user_id" },
                        new List<string> { "comment_id", "is_reply_valid" }, 1));
                BoundStatement bsLatestReply = psLatestReply.Bind(commentId, true);
                Row replyRow = session.Execute(bsLatestReply).FirstOrDefault();

                if (replyRow != null)
                {
                    string replyId = replyRow.GetValue<string>("reply_id");
                    int replyType = replyRow.GetValue<int>("reply_type");
                    string replyCommentorUserId = replyRow.GetValue<string>("commentor_user_id");
                    User replyCommentorUser =
                        new User().SelectUserBasic(replyCommentorUserId, companyId, false, session).User;

                    latestReply = GetReply(companyId, feedId, commentId, replyId, replyType, requesterUserId, replyCommentorUser, session);
                }

                // Get counter
                numberOfReplies = GetNumberOfRepliesForComment(feedId, commentId, session);
            }


            // Get pointers
            Dictionary<string, object> pointDict = GetPointsForComment(feedId, commentId, session);
            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : (int)pointDict["negativePoint"];
            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : (int)pointDict["positivePoint"];

            // Check for vote
            Dictionary<string, bool> voteDict = CheckVoteForComment(requesterUserId, commentId, session);
            bool hasVoted = voteDict["hasVoted"];
            bool isUpVoted = voteDict["isUpVoted"];

            if (commentType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psCommentText = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                    new List<string> { "user_id", "content", "created_on_timestamp" },
                    new List<string> { "id", "feed_id", "is_valid" }));
                BoundStatement bsCommentText = psCommentText.Bind(commentId, feedId, true);

                Row commentTextRow = session.Execute(bsCommentText).FirstOrDefault();

                if (commentTextRow != null)
                {
                    string content = commentTextRow.GetValue<string>("content");
                    string commentorUserId = commentTextRow.GetValue<string>("user_id");

                    DateTime createdOnTimestamp = commentTextRow.GetValue<DateTime>("created_on_timestamp");

                    comment = new Comment
                    {
                        FeedId = feedId,
                        CommentId = commentId,
                        CommentType = commentType,
                        CommentText = new CommentText
                        {
                            Content = content
                        },
                        Reply = latestReply,
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        NumberOfReplies = numberOfReplies,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = commentorUser,
                        CreatedOnTimestamp = createdOnTimestamp,
                        TaggedUsers = SelectTaggedUsersFromContent(content, companyId, session)
                    };
                }

            }

            return comment;
        }

        private Reply GetReply(string companyId, string feedId, string commentId, string replyId, int replyType, string requesterUserId, User commentorUser, ISession session)
        {

            Reply reply = null;

            // Get pointers
            Dictionary<string, object> pointDict = GetPointsForReply(commentId, replyId, session);
            int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : (int)pointDict["negativePoint"];
            int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : (int)pointDict["positivePoint"];

            // Check for vote
            Dictionary<string, bool> voteDict = CheckVoteForReply(requesterUserId, replyId, session);
            bool hasVoted = voteDict["hasVoted"];
            bool isUpVoted = voteDict["isUpVoted"];

            if (replyType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psReplyText = session.Prepare(CQLGenerator.SelectStatement("feed_reply_text",
                    new List<string> { "user_id", "content", "created_on_timestamp" },
                    new List<string> { "id", "comment_id", "is_valid" }));
                BoundStatement bsReplyText = psReplyText.Bind(replyId, commentId, true);

                Row replyTextRow = session.Execute(bsReplyText).FirstOrDefault();

                if (replyTextRow != null)
                {
                    string content = replyTextRow.GetValue<string>("content");
                    DateTime createdOnTimestamp = replyTextRow.GetValue<DateTime>("created_on_timestamp");

                    reply = new Reply
                    {
                        FeedId = feedId,
                        CommentId = commentId,
                        ReplyId = replyId,
                        ReplyType = replyType,
                        ReplyText = new ReplyText
                        {
                            Content = content
                        },
                        NegativePoints = negativePoints,
                        PositivePoints = positivePoints,
                        HasVoted = hasVoted,
                        IsUpVoted = isUpVoted,
                        User = commentorUser,
                        CreatedOnTimestamp = createdOnTimestamp,
                        TaggedUsers = SelectTaggedUsersFromContent(content, companyId, session)
                    };
                }

            }

            return reply;
        }


        public Feed GetFeed(string feedId, string companyId, int feedType, DateTime feedCreatedOnTimestamp, string requesterUserId, User ownerUser, ISession mainSession, ISession analyticSession, bool isLatestCommentRequired = false, bool checkForValid = true, bool isConvertToTimezone = false)
        {
            Feed feed = null;
            AWSUrlSigner awsUrlSigner = new AWSUrlSigner();

            // Get pointers
            int negativePoints = 0;
            int positivePoints = 0;
            List<User> upVoters = new List<User>();

            // Check for vote
            bool hasVoted = false;
            bool isUpVoted = false;

            FeedText feedText = null;
            FeedImage feedImage = null;
            FeedVideo feedVideo = null;
            FeedSharedUrl feedSharedUrl = null;

            bool isFeedAvailable = false;

            // Tagged Users
            List<User> taggedUsers = new List<User>();

            if (feedType == Int16.Parse(FeedTypeCode.TextPost))
            {
                PreparedStatement psFeedText = null;
                BoundStatement bsFeedText = null;

                if (checkForValid)
                {
                    psFeedText = mainSession.Prepare(CQLGenerator.SelectStatement("feed_text",
                        new List<string> { "content" }, new List<string> { "id", "is_valid" }));
                    bsFeedText = psFeedText.Bind(feedId, true);
                }
                else
                {
                    psFeedText = mainSession.Prepare(CQLGenerator.SelectStatement("feed_text",
                        new List<string> { "content" }, new List<string> { "id" }));
                    bsFeedText = psFeedText.Bind(feedId);
                }

                Row feedTextRow = mainSession.Execute(bsFeedText).FirstOrDefault();

                if (feedTextRow != null)
                {
                    string content = feedTextRow.GetValue<string>("content");

                    feedText = new FeedText
                    {
                        Content = content
                    };

                    taggedUsers = SelectTaggedUsersFromContent(content, companyId, mainSession);
                    isFeedAvailable = true;
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
            {
                PreparedStatement psFeedImage = null;
                BoundStatement bsFeedImage = null;

                if (checkForValid)
                {
                    psFeedImage = mainSession.Prepare(CQLGenerator.SelectStatement("feed_image",
                        new List<string> { "caption" }, new List<string> { "id", "is_valid" }));
                    bsFeedImage = psFeedImage.Bind(feedId, true);
                }
                else
                {
                    psFeedImage = mainSession.Prepare(CQLGenerator.SelectStatement("feed_image",
                        new List<string> { "caption" }, new List<string> { "id" }));
                    bsFeedImage = psFeedImage.Bind(feedId);
                }


                Row feedImageRow = mainSession.Execute(bsFeedImage).FirstOrDefault();

                if (feedImageRow != null)
                {
                    string caption = feedImageRow.GetValue<string>("caption");

                    PreparedStatement psFeedImageUrl = mainSession.Prepare(CQLGenerator.SelectStatement("feed_image_url",
                        new List<string>(), new List<string> { "feed_id", "is_valid" }));
                    BoundStatement bsFeedImageUrl = psFeedImageUrl.Bind(feedId, true);

                    RowSet feedImageUrlRowSet = mainSession.Execute(bsFeedImageUrl);

                    List<Image> images = new List<Image>();
                    foreach (Row feedImageUrlRow in feedImageUrlRowSet.GetRows())
                    {
                        string feedImageUrl = feedImageUrlRow.GetValue<string>("url");
                        string signedUrl = AWSUrlSigner.GetSignedUrl(feedImageUrl); // presigned url
                        string[] sizes = new string[3] { "small", "medium", "large" };
                        List<ResizedImage> resizedUrls = new List<ResizedImage>();

                        // create urls for other sizes (small, medium, large)
                        for (int i = 0; i < sizes.Length; i++)
                        {
                            string size = sizes[i];
                            string resizedFeedImageUrl = feedImageUrl.Replace("original", size);
                            string resizedSignedUrl = AWSUrlSigner.GetSignedUrl(resizedFeedImageUrl);

                            ResizedImage resizedImage = new ResizedImage();
                            resizedImage.Type = size;
                            resizedImage.Url = resizedSignedUrl;

                            resizedUrls.Add(resizedImage);
                        }

                        Image image = new Image
                        {
                            ImageId = feedImageUrlRow.GetValue<string>("id"),
                            Url = string.IsNullOrEmpty(signedUrl) ? feedImageUrl : signedUrl,
                            ResizedUrls = resizedUrls,
                            Order = feedImageUrlRow.GetValue<int>("in_order")
                        };

                        images.Add(image);
                    }

                    feedImage = new FeedImage
                    {
                        Caption = caption,
                        Images = images
                    };

                    taggedUsers = SelectTaggedUsersFromContent(caption, companyId, mainSession);
                    isFeedAvailable = true;
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
            {
                PreparedStatement psFeedVideo = null;
                BoundStatement bsFeedVideo = null;

                if (checkForValid)
                {
                    psFeedVideo = mainSession.Prepare(CQLGenerator.SelectStatement("feed_video",
                        new List<string> { "video_url", "video_thumbnail_url", "caption" },
                        new List<string> { "id", "is_valid" }));
                    bsFeedVideo = psFeedVideo.Bind(feedId, true);
                }
                else
                {
                    psFeedVideo = mainSession.Prepare(CQLGenerator.SelectStatement("feed_video",
                        new List<string> { "video_url", "video_thumbnail_url", "caption" }, new List<string> { "id" }));
                    bsFeedVideo = psFeedVideo.Bind(feedId);
                }


                Row feedVideoRow = mainSession.Execute(bsFeedVideo).FirstOrDefault();

                if (feedVideoRow != null)
                {
                    string caption = feedVideoRow.GetValue<string>("caption");
                    string videoUrl = feedVideoRow.GetValue<string>("video_url");
                    string signedUrl = AWSUrlSigner.GetSignedUrl(videoUrl);
                    string videoThumbnailUrl = feedVideoRow.GetValue<string>("video_thumbnail_url");
                    string signedVideoUrl = AWSUrlSigner.GetSignedUrl(videoThumbnailUrl);

                    string[] sizes = new string[3] { "small", "medium", "large" };
                    List<ResizedImage> resizedVideoThumbnailUrls = new List<ResizedImage>();

                    // create urls for other sizes (small, medium, large)
                    for (int i = 0; i < sizes.Length; i++)
                    {
                        string size = sizes[i];
                        string resizedFeedImageUrl = videoThumbnailUrl.Replace("original", size);
                        string resizedSignedUrl = AWSUrlSigner.GetSignedUrl(resizedFeedImageUrl);

                        ResizedImage resizedImage = new ResizedImage();
                        resizedImage.Type = size;
                        resizedImage.Url = resizedSignedUrl;

                        resizedVideoThumbnailUrls.Add(resizedImage);
                    }

                    feedVideo = new FeedVideo
                    {
                        Caption = caption,
                        VideoUrl = string.IsNullOrEmpty(signedUrl) ? videoUrl : signedUrl,
                        VideoThumbnailUrl = string.IsNullOrEmpty(signedVideoUrl) ? videoThumbnailUrl : signedVideoUrl,
                        ResizedVideoThumbnailUrls = resizedVideoThumbnailUrls
                    };

                    taggedUsers = SelectTaggedUsersFromContent(caption, companyId, mainSession);
                    isFeedAvailable = true;
                }

            }
            else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
            {
                PreparedStatement psFeedSharedUrl = null;
                BoundStatement bsFeedSharedUrl = null;

                if (checkForValid)
                {
                    psFeedSharedUrl = mainSession.Prepare(CQLGenerator.SelectStatement("feed_shared_url",
                        new List<string>
                        {
                            "caption",
                            "url",
                            "url_title",
                            "url_description",
                            "url_site_name",
                            "url_image_url"
                        }, new List<string> { "id", "is_valid" }));
                    bsFeedSharedUrl = psFeedSharedUrl.Bind(feedId, true);
                }
                else
                {
                    psFeedSharedUrl = mainSession.Prepare(CQLGenerator.SelectStatement("feed_shared_url",
                        new List<string>
                        {
                            "caption",
                            "url",
                            "url_title",
                            "url_description",
                            "url_site_name",
                            "url_image_url"
                        }, new List<string> { "id" }));
                    bsFeedSharedUrl = psFeedSharedUrl.Bind(feedId);
                }


                Row feedSharedUrlRow = mainSession.Execute(bsFeedSharedUrl).FirstOrDefault();

                if (feedSharedUrlRow != null)
                {
                    string caption = feedSharedUrlRow.GetValue<string>("caption");
                    string url = feedSharedUrlRow.GetValue<string>("url");
                    string title = feedSharedUrlRow.GetValue<string>("url_title");
                    string description = feedSharedUrlRow.GetValue<string>("url_description");
                    string siteName = feedSharedUrlRow.GetValue<string>("url_site_name");
                    string imageUrl = feedSharedUrlRow.GetValue<string>("url_image_url");
                    string signedUrl = AWSUrlSigner.GetSignedUrl(imageUrl);

                    string[] sizes = new string[3] { "small", "medium", "large" };
                    List<ResizedImage> resizedImageUrls = new List<ResizedImage>();

                    if (!String.IsNullOrEmpty(imageUrl))
                    {
                        // create urls for other sizes (small, medium, large)
                        for (int i = 0; i < sizes.Length; i++)
                        {
                            string size = sizes[i];
                            string resizedFeedImageUrl = imageUrl.Replace("original", size);
                            string resizedSignedUrl = AWSUrlSigner.GetSignedUrl(resizedFeedImageUrl);

                            ResizedImage resizedImage = new ResizedImage();
                            resizedImage.Type = size;
                            resizedImage.Url = resizedSignedUrl;

                            resizedImageUrls.Add(resizedImage);
                        }
                    }

                    feedSharedUrl = new FeedSharedUrl
                    {
                        Caption = caption,
                        Url = url,
                        Title = title,
                        Description = description,
                        SiteName = siteName,
                        ImageUrl = string.IsNullOrEmpty(signedUrl) ? imageUrl : signedUrl,
                        ResizedImageUrls = resizedImageUrls
                    };

                    taggedUsers = SelectTaggedUsersFromContent(caption, companyId, mainSession);
                    isFeedAvailable = true;
                }

            }

            if (isFeedAvailable)
            {
                // Get number of comments
                int numberOfComment = GetNumberOfCommentsForFeed(feedId, mainSession);

                // Get latest comment
                List<Comment> latestComment = new List<Comment>();

                // Get pointers
                Dictionary<string, object> pointDict = GetPointsForFeed(feedId, companyId, mainSession);
                negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : (int)pointDict["negativePoint"];
                positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : (int)pointDict["positivePoint"];
                upVoters = !pointDict.ContainsKey("upvoters") ? new List<User>() : (List<User>)pointDict["upvoters"];

                // Check for vote
                Dictionary<string, bool> voteDict = CheckVoteForFeed(requesterUserId, feedId, mainSession);
                hasVoted = voteDict["hasVoted"];
                isUpVoted = voteDict["isUpVoted"];

                if (isLatestCommentRequired)
                {
                    Comment selectedComment = GetLatestComment(requesterUserId, companyId, feedId, mainSession);
                    if (selectedComment != null)
                    {
                        latestComment.Add(selectedComment);
                    }
                }

                feed = new Feed
                {
                    FeedId = feedId,
                    FeedType = feedType,
                    FeedText = feedText,
                    FeedImage = feedImage,
                    FeedVideo = feedVideo,
                    FeedSharedUrl = feedSharedUrl,
                    NegativePoints = negativePoints,
                    PositivePoints = positivePoints,
                    NumberOfComments = numberOfComment,
                    HasVoted = hasVoted,
                    IsUpVoted = isUpVoted,
                    User = ownerUser,
                    CreatedOnTimestamp = !isConvertToTimezone ? feedCreatedOnTimestamp : DateHelper.ConvertDateToTimezoneSpecific(feedCreatedOnTimestamp, companyId, mainSession),
                    TaggedUsers = taggedUsers,
                    UpVoters = upVoters,
                    Comments = latestComment,
                    UniqueImpression = requesterUserId.Equals(ownerUser.UserId) ? SelectImpressionCount(positivePoints + negativePoints, feedId, mainSession, analyticSession) : -1
                };
            }


            return feed;
        }

        public Dictionary<string, int> ResetCommentPoint(string feedId, string commentId, int numberOfReplies,
            ISession session)
        {
            Dictionary<string, int> updatedPoints = new Dictionary<string, int>();

            try
            {
                PreparedStatement psCommentCounter = null;

                PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                    new List<string>(), new List<string> { "comment_id" }));
                BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId);
                RowSet commentPointRowSet = session.Execute(bsCommentPoint);

                int negativePoint = 0;
                int positivePoint = 0;

                foreach (Row commentPointRow in commentPointRowSet)
                {
                    int value = commentPointRow.GetValue<int>("value");

                    if (value > 0)
                    {
                        positivePoint++;
                    }
                    else
                    {
                        negativePoint++;
                    }
                }

                psCommentCounter = session.Prepare(CQLGenerator.DeleteStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id" }));
                session.Execute(psCommentCounter.Bind(feedId, commentId));

                psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter", new List<string> { "feed_id", "comment_id" },
                    new List<string> { "negative_point", "positive_point", "reply" }, new List<int> { negativePoint, positivePoint, numberOfReplies }));
                session.Execute(psCommentCounter.Bind(feedId, commentId));

                updatedPoints.Add("negativePoint", negativePoint);
                updatedPoints.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPoints;
        }

        public Dictionary<string, int> ResetReplyPoint(string commentId, string replyId, ISession session)
        {
            Dictionary<string, int> updatedPoints = new Dictionary<string, int>();

            try
            {
                PreparedStatement psReplyCounter = null;

                PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                    new List<string>(), new List<string> { "reply_id" }));
                BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId);
                RowSet replyPointRowSet = session.Execute(bsReplyPoint);

                int negativePoint = 0;
                int positivePoint = 0;

                foreach (Row replyPointRow in replyPointRowSet)
                {
                    int value = replyPointRow.GetValue<int>("value");

                    if (value > 0)
                    {
                        positivePoint++;
                    }
                    else
                    {
                        negativePoint++;
                    }
                }

                psReplyCounter = session.Prepare(CQLGenerator.DeleteStatement("reply_counter",
                    new List<string> { "comment_id", "reply_id" }));
                session.Execute(psReplyCounter.Bind(commentId, replyId));

                psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter", new List<string> { "comment_id", "reply_id" },
                    new List<string> { "negative_point", "positive_point" }, new List<int> { negativePoint, positivePoint }));
                session.Execute(psReplyCounter.Bind(commentId, replyId));

                updatedPoints.Add("negativePoint", negativePoint);
                updatedPoints.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPoints;
        }

        public Dictionary<string, object> GetPointsForFeed(string feedId, string companyId, ISession session)
        {
            Dictionary<string, object> pointDict = new Dictionary<string, object>();

            try
            {
                int negativePoint = 0;
                int positivePoint = 0;
                int numberOfComments = 0;
                List<User> upVoters = new List<User>();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string>(), new List<string> { "feed_id" }));
                Row feedCounterRow = session.Execute(ps.Bind(feedId)).FirstOrDefault();

                if (feedCounterRow != null)
                {
                    numberOfComments = feedCounterRow.GetValue<Nullable<long>>("comment") == null ? 0 : (int)feedCounterRow.GetValue<long>("comment");
                    negativePoint = feedCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("negative_point");
                    positivePoint = feedCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("positive_point");

                    // Clean up script
                    if (negativePoint < 0 || positivePoint < 0)
                    {
                        Dictionary<string, int> updatedPoints = ResetFeedPoint(feedId, numberOfComments, session);
                        negativePoint = updatedPoints["negativePoint"];
                        positivePoint = updatedPoints["positivePoint"];
                    }
                }

                if (positivePoint > 0)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatementWithLimit("feed_point_with_value_by_timestamp",
                        new List<string>(), new List<string> { "feed_id", "value" }, 5));
                    RowSet feedUpvoterRowset = session.Execute(ps.Bind(feedId, 1));

                    User userManager = new User();
                    foreach (Row feedUpVoterRow in feedUpvoterRowset)
                    {
                        string userId = feedUpVoterRow.GetValue<string>("user_id");
                        User voter = userManager.SelectUserBasic(userId, companyId, false, session, null, true).User;
                        if (voter != null && !upVoters.Contains(voter))
                        {
                            upVoters.Add(voter);
                        }
                    }
                }

                pointDict.Add("negativePoint", negativePoint);
                pointDict.Add("positivePoint", positivePoint);
                pointDict.Add("upvoters", upVoters);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        public Dictionary<string, object> GetPointsForComment(string feedId, string commentId, ISession session)
        {
            Dictionary<string, object> pointDict = new Dictionary<string, object>();

            try
            {
                int negativePoint = 0;
                int positivePoint = 0;

                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                    new List<string>(), new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);

                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();
                if (commentCounterRow != null)
                {
                    int numberOfReplies = commentCounterRow.GetValue<Nullable<long>>("reply") == null
                        ? 0
                        : (int)commentCounterRow.GetValue<long>("reply");
                    negativePoint = commentCounterRow.GetValue<Nullable<long>>("negative_point") == null
                        ? 0
                        : (int)commentCounterRow.GetValue<long>("negative_point");
                    positivePoint = commentCounterRow.GetValue<Nullable<long>>("positive_point") == null
                        ? 0
                        : (int)commentCounterRow.GetValue<long>("positive_point");

                    // Clean up script
                    if (negativePoint < 0 || positivePoint < 0)
                    {
                        Dictionary<string, int> updatedPoints = ResetCommentPoint(feedId, commentId, numberOfReplies,
                            session);
                        negativePoint = updatedPoints["negativePoint"];
                        positivePoint = updatedPoints["positivePoint"];
                    }
                }

                pointDict.Add("negativePoint", negativePoint);
                pointDict.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        public Dictionary<string, object> GetPointsForReply(string commentId, string replyId, ISession session)
        {
            Dictionary<string, object> pointDict = new Dictionary<string, object>();

            try
            {
                int negativePoint = 0;
                int positivePoint = 0;

                PreparedStatement psReplyCounter = session.Prepare(CQLGenerator.SelectStatement("reply_counter",
                    new List<string> { "negative_point", "positive_point" }, new List<string> { "comment_id", "reply_id" }));
                BoundStatement bsReplyCounter = psReplyCounter.Bind(commentId, replyId);

                Row replyCounterRow = session.Execute(bsReplyCounter).FirstOrDefault();
                if (replyCounterRow != null)
                {
                    negativePoint = replyCounterRow.GetValue<Nullable<long>>("negative_point") == null
                        ? 0
                        : (int)replyCounterRow.GetValue<long>("negative_point");
                    positivePoint = replyCounterRow.GetValue<Nullable<long>>("positive_point") == null
                        ? 0
                        : (int)replyCounterRow.GetValue<long>("positive_point");

                    // Clean up script
                    if (negativePoint < 0 || positivePoint < 0)
                    {
                        Dictionary<string, int> updatedPoints = ResetReplyPoint(commentId, replyId, session);
                        negativePoint = updatedPoints["negativePoint"];
                        positivePoint = updatedPoints["positivePoint"];
                    }
                }

                pointDict.Add("negativePoint", negativePoint);
                pointDict.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return pointDict;
        }

        public int GetNumberOfCommentsForFeed(string feedId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psFeedCounter = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string> { "comment" }, new List<string> { "feed_id" }));
                BoundStatement bsFeedCounter = psFeedCounter.Bind(feedId);

                Row feedCounterRow = session.Execute(bsFeedCounter).FirstOrDefault();
                if (feedCounterRow != null)
                {
                    number = feedCounterRow.GetValue<Nullable<long>>("comment") == null
                        ? 0
                        : (int)feedCounterRow.GetValue<long>("comment");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        public int GetNumberOfRepliesForComment(string feedId, string commentId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                    new List<string> { "reply" }, new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);

                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();
                if (commentCounterRow != null)
                {
                    number = commentCounterRow.GetValue<Nullable<long>>("reply") == null
                        ? 0
                        : (int)commentCounterRow.GetValue<long>("reply");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        private void CreateHashTag(string feedId, long createdTimestamp, string companyId, List<string> hashTags, ISession mainSession, ISession analyticSession)
        {
            try
            {
                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = null;

                List<FeedHashTag> smallCapHashTags = new List<FeedHashTag>();
                foreach (string hashTag in hashTags)
                {
                    string tirmHashTag = hashTag.Trim();

                    Log.Debug("Creating hashtag for " + tirmHashTag);

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("feed_by_tag", new List<string> { "feed_id", "company_id", "tag", "feed_created_on_timestamp" }));
                    batch.Add(ps.Bind(feedId, companyId, tirmHashTag, createdTimestamp));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("tag_by_feed", new List<string> { "feed_id", "company_id", "tag", "feed_created_on_timestamp" }));
                    batch.Add(ps.Bind(feedId, companyId, tirmHashTag, createdTimestamp));

                    string smallCapHashTag = tirmHashTag.ToLower();
                    if (smallCapHashTags.Count == 0 || smallCapHashTags.FirstOrDefault(t => t.HashTag.Equals(smallCapHashTag)) == null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.CountStatement("small_cap_tag_by_sorted_timestamp", new List<string> { "company_id", "small_cap_tag" }));
                        int currentCount = (int)mainSession.Execute(ps.Bind(companyId, smallCapHashTag)).FirstOrDefault().GetValue<long>("count");

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("small_cap_tag_by_sorted_timestamp", new List<string> { "feed_id", "company_id", "small_cap_tag", "feed_created_on_timestamp" }));
                        batch.Add(ps.Bind(feedId, companyId, smallCapHashTag, createdTimestamp));

                        smallCapHashTags.Add(new FeedHashTag
                        {
                            HashTag = smallCapHashTag,
                            Popularity = currentCount
                        });
                    }
                }

                mainSession.Execute(batch);

                // Update analytics for hashtags
                new AnalyticFeed().UpdateFeedHashTagPopularity(smallCapHashTags, companyId, 1, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void DeleteHashTag(string feedId, string companyId, ISession mainSession, ISession analyticSession)
        {
            try
            {
                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = null;

                List<FeedHashTag> smallCapHashTags = new List<FeedHashTag>();
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("tag_by_feed", new List<string>(), new List<string> { "company_id", "feed_id" }));
                RowSet hashTagRowset = mainSession.Execute(ps.Bind(companyId, feedId));
                foreach (Row hashTagRow in hashTagRowset)
                {
                    string tag = hashTagRow.GetValue<string>("tag");
                    string smallCapTag = tag.ToLower();
                    DateTime feedCreatedTimestamp = hashTagRow.GetValue<DateTime>("feed_created_on_timestamp");

                    Log.Debug("Removing hashtag for " + smallCapTag);

                    ps = mainSession.Prepare(CQLGenerator.CountStatement("small_cap_tag_by_sorted_timestamp", new List<string> { "company_id", "small_cap_tag" }));
                    int currentCount = (int)mainSession.Execute(ps.Bind(companyId, smallCapTag)).FirstOrDefault().GetValue<long>("count");

                    if (smallCapHashTags.Count == 0 || smallCapHashTags.FirstOrDefault(t => t.HashTag.Equals(smallCapTag)) == null)
                    {
                        smallCapHashTags.Add(new FeedHashTag
                        {
                            HashTag = smallCapTag,
                            Popularity = currentCount
                        });
                    }

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_by_tag", new List<string> { "company_id", "tag", "feed_id" }));
                    batch.Add(ps.Bind(companyId, tag, feedId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("small_cap_tag_by_sorted_timestamp", new List<string> { "company_id", "small_cap_tag", "feed_created_on_timestamp", "feed_id" }));
                    batch.Add(ps.Bind(companyId, smallCapTag, feedCreatedTimestamp, feedId));
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("tag_by_feed", new List<string> { "company_id", "feed_id" }));
                batch.Add(ps.Bind(companyId, feedId));

                mainSession.Execute(batch);

                // Update analytics for hashtags
                new AnalyticFeed().UpdateFeedHashTagPopularity(smallCapHashTags, companyId, -1, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private Dictionary<string, List<string>> CreateFeedPrivacy(string ownerUserId,
                                                                   string feedId,
                                                                   string companyId,
                                                                   int feedType,
                                                                   string feedTextContent,
                                                                   List<string> targetedDepartmentIds,
                                                                   List<string> targetedUserIds,
                                                                   List<string> targetedGroupIds,
                                                                   List<string> taggedUserIds,
                                                                   bool isForAdmin,
                                                                   long createdTimestamp,
                                                                   ISession session,
                                                                   bool isUpdate = false)
        {
            Dictionary<string, List<string>> notificationDict = new Dictionary<string, List<string>>();
            try
            {
                List<string> targetedIdsForDepartment = new List<string>();
                List<string> targetedIdsForUser = new List<string>();
                List<string> targetedIdsForMention = new List<string>();
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                bool isForDepartment = targetedDepartmentIds != null && targetedDepartmentIds.Count != 0 ? true : false;
                bool isForGroup = targetedGroupIds != null && targetedGroupIds.Count != 0 ? true : false;
                bool isForUser = targetedUserIds != null && targetedUserIds.Count != 0 ? true : false;
                bool isTaggedForUser = taggedUserIds != null && taggedUserIds.Count != 0 ? true : false;
                bool isForEveryone = !isForAdmin && !isForDepartment && !isForUser && !isForGroup ? true : false;
                bool isForOnlyMe = false;

                if (!isForEveryone)
                {
                    if (isForGroup)
                    {
                        Group groupManager = new Group();
                        foreach (string groupId in targetedGroupIds)
                        {
                            GroupSelectResponse selectResponse = groupManager.SelectGroup(ownerUserId, companyId, groupId, (int)Group.GroupQueryType.FullDetail, session);

                            if (selectResponse.Success)
                            {
                                Group selectedGroup = selectResponse.Group;
                                foreach (User member in selectedGroup.Members)
                                {
                                    if (!targetedIdsForUser.Contains(member.UserId))
                                    {
                                        targetedIdsForUser.Add(member.UserId);
                                    }
                                }

                                if (!string.IsNullOrEmpty(groupId.Trim()))
                                {
                                    ps = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_group",
                                        new List<string> { "feed_id", "group_id" }));
                                    updateBatch.Add(ps.Bind(feedId, groupId));
                                }
                            }
                        }
                    }

                    if (isForDepartment)
                    {
                        User user = new User();

                        foreach (string departmentId in targetedDepartmentIds)
                        {
                            if (!string.IsNullOrEmpty(departmentId.Trim()))
                            {
                                ps = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_department",
                                    new List<string> { "feed_id", "department_id" }));
                                updateBatch.Add(ps.Bind(feedId, departmentId));
                            }

                        }

                        targetedIdsForDepartment.AddRange(user.SelectAllUserIdsByDepartmentIds(targetedDepartmentIds, ownerUserId, companyId, session));
                    }

                    if (isForUser)
                    {
                        foreach (string userId in targetedUserIds)
                        {
                            if (!string.IsNullOrEmpty(userId.Trim()))
                            {
                                ps = session.Prepare(CQLGenerator.InsertStatement("feed_targeted_user",
                                    new List<string> { "feed_id", "user_id" }));
                                updateBatch.Add(ps.Bind(feedId, userId));

                                if (!targetedIdsForUser.Contains(userId))
                                {
                                    targetedIdsForUser.Add(userId);
                                }
                            }
                        }

                        if (!isForDepartment && !isForGroup && targetedUserIds.Count == 1 && targetedUserIds.Contains(ownerUserId))
                        {
                            isForOnlyMe = true;
                            //So that old api version still can work
                            //isForUser = false;
                        }
                    }
                }

                // Irregardless of isForEveryone or isForDepartment or isForUser or isForGroup
                if (taggedUserIds != null)
                {
                    foreach (string userId in taggedUserIds)
                    {
                        if (!string.IsNullOrEmpty(userId.Trim()))
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("feed_tagged_user",
                                new List<string> { "feed_id", "user_id" }));
                            updateBatch.Add(ps.Bind(feedId, userId));

                            if (!targetedIdsForMention.Contains(userId))
                            {
                                targetedIdsForMention.Add(userId);
                            }
                        }
                    }
                }

                // To ensure there is no duplication due to client spamming
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                    new List<string>(), new List<string> { "company_id", "feed_id" }));
                Row feedRow = session.Execute(ps.Bind(companyId, feedId)).FirstOrDefault();

                if (feedRow == null)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("feed_privacy",
                            new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "is_feed_valid", "feed_valid_status", "is_for_everyone", "is_for_department", "is_for_user", "is_for_only_me", "is_tagged_for_user", "is_for_group", "is_for_admin", "feed_created_on_timestamp", "is_posted_by_admin" }));
                    updateBatch.Add(ps.Bind(feedId, companyId, ownerUserId, feedType, true, 1, isForEveryone, isForDepartment, isForUser, isForOnlyMe, isTaggedForUser, isForGroup, isForAdmin, createdTimestamp, false));

                    ps = session.Prepare(CQLGenerator.InsertStatement("feed_privacy_by_company_timestamp_desc",
                        new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "feed_text_content", "is_feed_valid", "feed_valid_status", "is_for_everyone", "is_for_department", "is_for_user", "is_for_only_me", "is_tagged_for_user", "is_for_group", "is_for_admin", "feed_created_on_timestamp", "is_posted_by_admin" }));
                    updateBatch.Add(ps.Bind(feedId, companyId, ownerUserId, feedType, feedTextContent, true, 1, isForEveryone, isForDepartment, isForUser, isForOnlyMe, isTaggedForUser, isForGroup, isForAdmin, createdTimestamp, false));

                    ps = session.Prepare(CQLGenerator.InsertStatement("feed_privacy_by_company_timestamp_asc",
                        new List<string> { "feed_id", "company_id", "owner_user_id", "feed_type", "feed_text_content", "is_feed_valid", "feed_valid_status", "is_for_everyone", "is_for_department", "is_for_user", "is_for_only_me", "is_tagged_for_user", "is_for_group", "is_for_admin", "feed_created_on_timestamp", "is_posted_by_admin" }));
                    updateBatch.Add(ps.Bind(feedId, companyId, ownerUserId, feedType, feedTextContent, true, 1, isForEveryone, isForDepartment, isForUser, isForOnlyMe, isTaggedForUser, isForGroup, isForAdmin, createdTimestamp, false));
                }
                else
                {
                    // Update
                    if (isUpdate)
                    {
                        // Delete existing feed privacy
                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_targeted_department",
                            new List<string> { "feed_id" }));
                        deleteBatch.Add(ps.Bind(feedId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_targeted_group",
                            new List<string> { "feed_id" }));
                        deleteBatch.Add(ps.Bind(feedId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_targeted_user",
                            new List<string> { "feed_id" }));
                        deleteBatch.Add(ps.Bind(feedId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_tagged_user",
                            new List<string> { "feed_id" }));
                        deleteBatch.Add(ps.Bind(feedId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy", new List<string> { "feed_id", "company_id" }, new List<string> { "is_for_everyone", "is_for_department", "is_for_user", "is_tagged_for_user", "is_for_group", "feed_type" }, new List<string>()));
                        updateBatch.Add(ps.Bind(isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, feedType, feedId, companyId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                            new List<string> { "feed_id", "feed_created_on_timestamp", "company_id" },
                            new List<string> { "feed_text_content", "is_for_everyone", "is_for_department", "is_for_user", "is_tagged_for_user", "is_for_group", "feed_type" }, new List<string>()));
                        updateBatch.Add(ps.Bind(feedTextContent, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, feedType, feedId, createdTimestamp, companyId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                            new List<string> { "feed_id", "feed_created_on_timestamp", "company_id" },
                            new List<string> { "feed_text_content", "is_for_everyone", "is_for_department", "is_for_user", "is_tagged_for_user", "is_for_group", "feed_type" }, new List<string>()));
                        updateBatch.Add(ps.Bind(feedTextContent, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, feedType, feedId, createdTimestamp, companyId));
                    }
                    else
                    {
                        Log.Error("Feed got spammed by client: " + ownerUserId);
                    }
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                notificationDict.Add("AddedUsersInDepartment", targetedIdsForDepartment);
                notificationDict.Add("AddedUsers", targetedIdsForUser);
                notificationDict.Add("AddedUsersForMention", targetedIdsForMention);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return notificationDict;
        }

        private Dictionary<string, List<string>> UpdateFeedPrivacy(string ownerUserId,
                                                                  string feedId,
                                                                  string companyId,
                                                                  int feedType,
                                                                  string feedTextContent,
                                                                  List<string> newTargetedDepartmentIds,
                                                                  List<string> newTargetedUserIds,
                                                                  List<string> newTargetedGroupIds,
                                                                  List<string> newTaggedUserIds,
                                                                  long feedCreatedOnTimestamp,
                                                                  ISession session)
        {
            Dictionary<string, List<string>> updatedPrivacyDict = new Dictionary<string, List<string>>();

            try
            {
                // Existing notified users
                Notification notificationManager = new Notification();
                List<string> alreadyNotifiedUsers = notificationManager.SelectTargetedUserIdForFeedNotification(feedId, "NA", "NA", (int)Notification.NotificationFeedSubType.NewPostToUser, session);
                List<string> alreadyNotifiedUsersInDepartments = notificationManager.SelectTargetedUserIdForFeedNotification(feedId, "NA", "NA", (int)Notification.NotificationFeedSubType.NewPostToDepartment, session);
                List<string> alreadyNotifiedMentionedUsers = notificationManager.SelectTargetedUserIdForFeedNotification(feedId, "NA", "NA", (int)Notification.NotificationFeedSubType.MentionedInPost, session);

                Dictionary<string, List<string>> privacyDict = CreateFeedPrivacy(ownerUserId, feedId, companyId, feedType, feedTextContent, newTargetedDepartmentIds, newTargetedUserIds, newTargetedGroupIds, newTaggedUserIds, false, feedCreatedOnTimestamp, session, true);

                List<string> addedUsers = privacyDict["AddedUsers"].Except(alreadyNotifiedUsers).ToList();
                List<string> addedUsersInDepartment = privacyDict["AddedUsersInDepartment"].Except(alreadyNotifiedUsersInDepartments).ToList();
                List<string> addedUsersForMention = privacyDict["AddedUsersForMention"].Except(alreadyNotifiedMentionedUsers).ToList();

                List<string> removedUsers = alreadyNotifiedUsers.Except(privacyDict["AddedUsers"]).ToList();
                List<string> removedUsersInDepartment = alreadyNotifiedUsersInDepartments.Except(privacyDict["AddedUsersInDepartment"]).ToList();
                List<string> removedUsersForMention = alreadyNotifiedMentionedUsers.Except(privacyDict["AddedUsersForMention"]).ToList();

                updatedPrivacyDict.Add("AddedUsers", addedUsers);
                updatedPrivacyDict.Add("AddedUsersInDepartment", addedUsersInDepartment);
                updatedPrivacyDict.Add("AddedUsersForMention", addedUsersForMention);

                updatedPrivacyDict.Add("RemovedUsers", removedUsers);
                updatedPrivacyDict.Add("RemovedUsersInDepartment", removedUsersInDepartment);
                updatedPrivacyDict.Add("RemovedUsersForMention", removedUsersForMention);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPrivacyDict;
        }

        public void UpdateFeedCommentCounter(string feedId, int operatorValue, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = null;
                BoundStatement boundStatement = null;

                preparedStatement = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                    new List<string> { "feed_id" }, new List<string> { "comment" }, new List<int> { operatorValue }));
                boundStatement = preparedStatement.Bind(feedId);
                session.Execute(boundStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateCommentReplyCounter(string feedId, string commentId, int operatorValue, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = null;
                BoundStatement boundStatement = null;

                preparedStatement = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                    new List<string> { "feed_id", "comment_id" }, new List<string> { "reply" },
                    new List<int> { operatorValue }));
                boundStatement = preparedStatement.Bind(feedId, commentId);
                session.Execute(boundStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public int UpdateFeedPointCounter(string userId, string feedId, bool isUpVote, ISession session)
        {
            int result = (int)VoteStatus.Reset;

            try
            {
                string pointColumnName = string.Empty;
                int newVote = 1;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                    newVote = -1;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("feed_counter",
                    new List<string>(), new List<string> { "feed_id" }));
                Row feedCounterRow = session.Execute(ps.Bind(feedId)).FirstOrDefault();

                int numberOfComments = 0;
                int currentPositivePoints = 0;
                int currentNegativePoints = 0;

                if (feedCounterRow != null)
                {
                    numberOfComments = feedCounterRow.GetValue<Nullable<long>>("comment") == null ? 0 : (int)feedCounterRow.GetValue<long>("comment");
                    currentPositivePoints = feedCounterRow.GetValue<Nullable<long>>("positive_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("positive_point");
                    currentNegativePoints = feedCounterRow.GetValue<Nullable<long>>("negative_point") == null ? 0 : (int)feedCounterRow.GetValue<long>("negative_point");
                }

                DateTime currentTime = DateTime.UtcNow;

                ps = session.Prepare(CQLGenerator.SelectStatement("feed_point", new List<string>(), new List<string> { "feed_id", "user_id" }));

                Row feedPointRow = session.Execute(ps.Bind(feedId, userId)).FirstOrDefault();

                if (feedPointRow != null)
                {
                    DateTime votedTimestamp = feedPointRow.GetValue<DateTime>("voted_timestamp");
                    int currentVote = feedPointRow.GetValue<int>("value");

                    if (currentVote != newVote)
                    {
                        // Different vote so need to remove first vote
                        if (isUpVote && currentNegativePoints - 1 >= 0 || !isUpVote && currentPositivePoints - 1 >= 0)
                        {
                            // Add 1 and Remove 1
                            ps = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                                new List<string> { "feed_id" }, new List<string> { pointColumnName, isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive }, new List<int> { 1, -1 }));
                            session.Execute(ps.Bind(feedId));

                            ps = session.Prepare(CQLGenerator.UpdateStatement("feed_point",
                                new List<string> { "feed_id", "user_id" }, new List<string> { "value", "voted_timestamp" }, new List<string>()));
                            updateBatch.Add(ps.Bind(newVote, currentTime, feedId, userId));

                            ps = session.Prepare(CQLGenerator.DeleteStatement("feed_point_with_value_by_timestamp",
                                new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                            deleteBatch.Add(ps.Bind(feedId, userId, currentVote, votedTimestamp));

                            ps = session.Prepare(CQLGenerator.InsertStatement("feed_point_with_value_by_timestamp",
                                new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                            updateBatch.Add(ps.Bind(feedId, userId, newVote, currentTime));

                        }
                        else
                        {
                            Log.Error(string.Format("FeedId: {0} has negative points", feedId));

                            // Reset
                            ResetFeedPoint(feedId, numberOfComments, session);
                        }

                        result = newVote;
                    }
                    // Reset
                    else
                    {
                        // Remove voting
                        ps = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                            new List<string> { "feed_id" }, new List<string> { pointColumnName }, new List<int> { -1 }));
                        session.Execute(ps.Bind(feedId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_point",
                            new List<string> { "feed_id", "user_id" }));
                        deleteBatch.Add(ps.Bind(feedId, userId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("feed_point_with_value_by_timestamp",
                            new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                        deleteBatch.Add(ps.Bind(feedId, userId, currentVote, votedTimestamp));

                        result = (int)VoteStatus.Reset;
                    }
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter",
                        new List<string> { "feed_id" }, new List<string> { pointColumnName }, new List<int> { 1 }));
                    session.Execute(ps.Bind(feedId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("feed_point",
                        new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                    updateBatch.Add(ps.Bind(feedId, userId, newVote, currentTime));

                    ps = session.Prepare(CQLGenerator.InsertStatement("feed_point_with_value_by_timestamp",
                        new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                    updateBatch.Add(ps.Bind(feedId, userId, newVote, currentTime));

                    result = newVote;
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return result;
        }


        public int UpdateCommentPointCounter(string userId, string feedId, string commentId, bool isUpVote,
            ISession session)
        {
            int result = (int)VoteStatus.Reset;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psCommentCounter = session.Prepare(CQLGenerator.SelectStatement("comment_counter",
                    new List<string>(), new List<string> { "feed_id", "comment_id" }));
                BoundStatement bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                Row commentCounterRow = session.Execute(bsCommentCounter).FirstOrDefault();

                int numberOfReplies = 0;
                int currentPositivePoints = 0;
                int currentNegativePoints = 0;

                if (commentCounterRow != null)
                {
                    numberOfReplies = commentCounterRow.GetValue<Nullable<long>>("reply") == null
                        ? 0
                        : (int)commentCounterRow.GetValue<long>("reply");
                    currentPositivePoints = commentCounterRow.GetValue<Nullable<long>>("positive_point") == null
                        ? 0
                        : (int)commentCounterRow.GetValue<long>("positive_point");
                    currentNegativePoints = commentCounterRow.GetValue<Nullable<long>>("negative_point") == null
                        ? 0
                        : (int)commentCounterRow.GetValue<long>("negative_point");
                }

                PreparedStatement psCommentPoint = session.Prepare(CQLGenerator.SelectStatement("comment_point",
                    new List<string> { "value" }, new List<string> { "comment_id", "user_id" }));
                BoundStatement bsCommentPoint = psCommentPoint.Bind(commentId, userId);

                Row commentPointRow = session.Execute(bsCommentPoint).FirstOrDefault();

                if (commentPointRow != null)
                {
                    int currentVote = commentPointRow.GetValue<int>("value");
                    int newVote = 1;

                    if (!isUpVote)
                    {
                        newVote = -1;
                    }

                    if (currentVote != newVote)
                    {
                        if (isUpVote && currentNegativePoints - 1 >= 0 || !isUpVote && currentPositivePoints - 1 >= 0)
                        {
                            // Add 1
                            psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                                new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName, isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive },
                                new List<int> { 1, -1 }));
                            bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                            session.Execute(bsCommentCounter);

                            psCommentPoint = session.Prepare(CQLGenerator.UpdateStatement("comment_point",
                                new List<string> { "comment_id", "user_id" },
                                new List<string> { "value", "voted_timestamp" }, new List<string>()));
                            bsCommentPoint = psCommentPoint.Bind(isUpVote ? 1 : -1,
                                DateTime.UtcNow, commentId, userId);
                            session.Execute(bsCommentPoint);

                        }
                        else
                        {
                            Log.Error(string.Format("FeedId: {0} and CommentId: {1} has negative points", feedId,
                                commentId));

                            // Reset
                            ResetCommentPoint(feedId, commentId, numberOfReplies, session);
                        }

                        result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                    }
                    // Reset
                    else
                    {
                        // Remove 1
                        psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                            new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName },
                            new List<int> { -1 }));
                        bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                        session.Execute(bsCommentCounter);

                        psCommentPoint = session.Prepare(CQLGenerator.DeleteStatement("comment_point",
                            new List<string> { "comment_id", "user_id" }));
                        bsCommentPoint = psCommentPoint.Bind(commentId, userId);
                        session.Execute(bsCommentPoint);

                        result = (int)VoteStatus.Reset;

                    }
                }
                else
                {
                    psCommentCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("comment_counter",
                        new List<string> { "feed_id", "comment_id" }, new List<string> { pointColumnName },
                        new List<int> { 1 }));
                    bsCommentCounter = psCommentCounter.Bind(feedId, commentId);
                    session.Execute(bsCommentCounter);

                    psCommentPoint = session.Prepare(CQLGenerator.InsertStatement("comment_point",
                        new List<string> { "comment_id", "user_id", "value", "voted_timestamp" }));
                    bsCommentPoint = psCommentPoint.Bind(commentId, userId, isUpVote ? 1 : -1,
                        DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    session.Execute(bsCommentPoint);

                    result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return result;
        }

        public int UpdateReplyPointCounter(string userId, string commentId, string replyId, bool isUpVote,
            ISession session)
        {
            int result = (int)VoteStatus.Reset;

            try
            {
                string pointColumnName = string.Empty;

                if (isUpVote)
                {
                    pointColumnName = CassandraColumn.FeedPointPositive;
                }
                else
                {
                    pointColumnName = CassandraColumn.FeedPointNegative;
                }

                PreparedStatement psReplyCounter = session.Prepare(CQLGenerator.SelectStatement("reply_counter",
                    new List<string>(), new List<string> { "comment_id", "reply_id" }));
                BoundStatement bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                Row replyCounterRow = session.Execute(bsReplyCounter).FirstOrDefault();

                int currentPositivePoints = 0;
                int currentNegativePoints = 0;

                if (replyCounterRow != null)
                {
                    currentPositivePoints = replyCounterRow.GetValue<Nullable<long>>("positive_point") == null
                        ? 0
                        : (int)replyCounterRow.GetValue<long>("positive_point");
                    currentNegativePoints = replyCounterRow.GetValue<Nullable<long>>("negative_point") == null
                        ? 0
                        : (int)replyCounterRow.GetValue<long>("negative_point");
                }

                PreparedStatement psReplyPoint = session.Prepare(CQLGenerator.SelectStatement("reply_point",
                    new List<string> { "value" }, new List<string> { "reply_id", "user_id" }));
                BoundStatement bsReplyPoint = psReplyPoint.Bind(replyId, userId);

                Row replyPointRow = session.Execute(bsReplyPoint).FirstOrDefault();

                if (replyPointRow != null)
                {
                    int value = replyPointRow.GetValue<int>("value");
                    int currentVote = 1;

                    if (!isUpVote)
                    {
                        currentVote = -1;
                    }

                    if (value != currentVote)
                    {
                        if (isUpVote && currentNegativePoints - 1 >= 0 || !isUpVote && currentPositivePoints - 1 >= 0)
                        {
                            psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                                new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName, isUpVote ? CassandraColumn.FeedPointNegative : CassandraColumn.FeedPointPositive },
                                new List<int> { 1, -1 }));
                            bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                            session.Execute(bsReplyCounter);

                            psReplyPoint = session.Prepare(CQLGenerator.UpdateStatement("reply_point",
                                new List<string> { "reply_id", "user_id" }, new List<string> { "value", "voted_timestamp" },
                                new List<string>()));
                            bsReplyPoint = psReplyPoint.Bind(isUpVote ? 1 : -1,
                                DateHelper.ConvertDateToLong(DateTime.UtcNow), replyId, userId);
                            session.Execute(bsReplyPoint);
                        }
                        else
                        {
                            Log.Error(string.Format("CommentId: {0} and ReplyId: {1} has negative points", commentId,
                                replyId));

                            // Reset
                            ResetReplyPoint(commentId, replyId, session);
                        }

                        result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                    }
                    // Reset
                    else
                    {
                        // Remove 1
                        psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                            new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName },
                            new List<int> { -1 }));
                        bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                        session.Execute(bsReplyCounter);

                        psReplyPoint = session.Prepare(CQLGenerator.DeleteStatement("reply_point",
                            new List<string> { "reply_id", "user_id" }));
                        bsReplyPoint = psReplyPoint.Bind(replyId, userId);
                        session.Execute(bsReplyPoint);

                        result = (int)VoteStatus.Reset;

                    }
                }
                else
                {
                    psReplyCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("reply_counter",
                        new List<string> { "comment_id", "reply_id" }, new List<string> { pointColumnName },
                        new List<int> { 1 }));
                    bsReplyCounter = psReplyCounter.Bind(commentId, replyId);
                    session.Execute(bsReplyCounter);

                    psReplyPoint = session.Prepare(CQLGenerator.InsertStatement("reply_point",
                        new List<string> { "reply_id", "user_id", "value", "voted_timestamp" }));
                    bsReplyPoint = psReplyPoint.Bind(replyId, userId, isUpVote ? 1 : -1,
                        DateHelper.ConvertDateToLong(DateTime.UtcNow));
                    session.Execute(bsReplyPoint);

                    result = isUpVote ? (int)VoteStatus.Upvote : (int)VoteStatus.Downvote;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return result;
        }

        public FeedUpdateResponse UpdateToFeedTextPost(string ownerUserId,
                                                       string companyId,
                                                       string currentFeedId,
                                                       string content,
                                                       List<string> targetedDepartmentIds,
                                                       List<string> targetedUserIds,
                                                       List<string> targetedGroupIds,
                                                       bool isSpecificNotification = false)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, mainSession);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Empty feed text post");
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidTextContent);
                    response.ErrorMessage = ErrorMessage.FeedInvalidTextContent;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, mainSession);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                if (feedType != Convert.ToInt16(FeedTypeCode.TextPost))
                {
                    // Delete from relevant table
                    if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_image",
                            new List<string> { "id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_image_url",
                            new List<string> { "feed_id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_video",
                            new List<string> { "id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_shared_url",
                            new List<string> { "id" }));
                        deleteBatch.Add(ps.Bind(currentFeedId));
                    }

                    // Insert into text table
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("feed_text",
                        new List<string>
                        {
                            "id",
                            "content",
                            "is_valid",
                            "valid_status",
                            "last_modified_by_user_id",
                            "last_modified_timestamp"
                        }));
                    updateBatch.Add(ps.Bind(currentFeedId, content, true, 1, ownerUserId, createdTimeOffset));

                    // Delete from S3
                    String bucketName = "cocadre-" + companyId.ToLower();

                    using (
                        IAmazonS3 s3Client =
                            AWSClientFactory.CreateAmazonS3Client(
                                WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(),
                                WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(),
                                RegionEndpoint.APSoutheast1))
                    {
                        ListObjectsRequest listRequest = new ListObjectsRequest();
                        listRequest.BucketName = bucketName;
                        listRequest.Prefix = "feeds/" + currentFeedId;

                        ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                        foreach (S3Object imageObject in listResponse.S3Objects)
                        {
                            DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                            deleteRequest.BucketName = bucketName;
                            deleteRequest.Key = imageObject.Key;
                            s3Client.DeleteObject(deleteRequest);
                        }

                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = "feeds/" + currentFeedId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }
                }
                else
                {
                    // Update text table
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_text",
                        new List<string> { "id" }, new List<string> { "content", "last_modified_timestamp" },
                        new List<string>()));
                    updateBatch.Add(ps.Bind(content, DateTime.UtcNow, currentFeedId));
                }

                // Search tagged user ids from content
                List<string> taggedUserIds = SearchTaggedUserIdsFromContent(content, companyId, vh, mainSession);

                // Delete previous hash tag first
                DeleteHashTag(currentFeedId, companyId, mainSession, analyticSession);

                //Extract hashTag
                List<string> hashTags = SearchHashedTagsFromContent(content);
                if (hashTags.Count > 0)
                {
                    CreateHashTag(currentFeedId, DateHelper.ConvertDateToLong(createdTimestamp), companyId, hashTags, mainSession, analyticSession);
                }

                // Update privacy
                Dictionary<string, List<string>> updatedPrivacyDict = UpdateFeedPrivacy(ownerUserId,
                                                                                        currentFeedId,
                                                                                        companyId,
                                                                                        Int32.Parse(FeedTypeCode.TextPost),
                                                                                        content,
                                                                                        targetedDepartmentIds,
                                                                                        targetedUserIds,
                                                                                        targetedGroupIds,
                                                                                        taggedUserIds,
                                                                                        DateHelper.ConvertDateToLong(createdTimestamp),
                                                                                        mainSession);


                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                // Create notification for new targeted users
                Dictionary<string, List<string>> privacyNotifyDict = new Dictionary<string, List<string>>();
                privacyNotifyDict.Add("AddedUsers", updatedPrivacyDict["AddedUsers"]);
                privacyNotifyDict.Add("AddedUsersInDepartment", updatedPrivacyDict["AddedUsersInDepartment"]);
                privacyNotifyDict.Add("AddedUsersForMention", updatedPrivacyDict["AddedUsersForMention"]);

                if (isSpecificNotification)
                {
                    response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.TextPost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                }
                else
                {
                    response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.TextPost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                }

                // Remove targeted users due to change of privacy
                privacyNotifyDict = new Dictionary<string, List<string>>();
                privacyNotifyDict.Add("RemovedUsers", updatedPrivacyDict["RemovedUsers"]);
                privacyNotifyDict.Add("RemovedUsersInDepartment", updatedPrivacyDict["RemovedUsersInDepartment"]);
                privacyNotifyDict.Add("RemovedUsersForMention", updatedPrivacyDict["RemovedUsersForMention"]);

                RemoveNotificationForFeed(companyId, currentFeedId, feedType, ownerUserId, privacyNotifyDict, mainSession);
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedImagePost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> updatedImageUrls,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds,
                                                        List<string> targetedGroupIds,
                                                        bool isSpecificNotification = false)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, mainSession);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, mainSession);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                // Edit only caption of image
                if (feedType == Convert.ToInt16(FeedTypeCode.ImagePost))
                {
                    // Update image table
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_image",
                        new List<string> { "id" }, new List<string> { "caption", "last_modified_timestamp" },
                        new List<string>()));
                    updateBatch.Add(ps.Bind(caption, DateTime.UtcNow, currentFeedId));

                    // Delete from S3
                    String bucketName = "cocadre-" + companyId.ToLower();

                    using (
                        IAmazonS3 s3Client =
                            AWSClientFactory.CreateAmazonS3Client(
                                WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(),
                                WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(),
                                RegionEndpoint.APSoutheast1))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_image_url",
                            new List<string>(), new List<string> { "feed_id" }));
                        RowSet imageRowset = mainSession.Execute(ps.Bind(currentFeedId));

                        foreach (Row imageRow in imageRowset)
                        {
                            string url = imageRow.GetValue<string>("url");
                            string[] arrayUrl = url.Split('/');
                            string fileName = arrayUrl[arrayUrl.Count() - 1];
                            string fileExtension = fileName.Split('.')[1];
                            string actualFileName = fileName.Split('_')[0];

                            try
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_original." +
                                                    fileExtension;
                                s3Client.DeleteObject(deleteRequest);

                                deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_small." +
                                                    fileExtension;
                                s3Client.DeleteObject(deleteRequest);

                                deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_medium." +
                                                    fileExtension;
                                s3Client.DeleteObject(deleteRequest);

                                deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = "feeds/" + currentFeedId + "/" + actualFileName + "_large." +
                                                    fileExtension;
                                s3Client.DeleteObject(deleteRequest);
                            }
                            catch (AmazonS3Exception s3Exception)
                            {
                                Log.Error(s3Exception.Message, s3Exception.InnerException);
                            }
                        }
                    }

                    // Update image url table
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_image_url",
                        new List<string> { "feed_id" }));
                    deleteBatch.Add(ps.Bind(currentFeedId));

                    int order = 1;
                    foreach (string imageUrl in updatedImageUrls)
                    {
                        string imageId = UUIDGenerator.GenerateUniqueIDForFeedMedia();

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("feed_image_url",
                            new List<string> { "id", "feed_id", "url", "in_order", "is_valid" }));
                        updateBatch.Add(ps.Bind(imageId, currentFeedId, imageUrl, order, true));

                        order += 1;
                    }

                    // Search tagged user ids from content
                    List<string> taggedUserIds = SearchTaggedUserIdsFromContent(caption, companyId, vh, mainSession);

                    // Delete previous hash tag first
                    DeleteHashTag(currentFeedId, companyId, mainSession, analyticSession);

                    //Extract hashTag
                    List<string> hashTags = SearchHashedTagsFromContent(caption);
                    if (hashTags.Count > 0)
                    {
                        CreateHashTag(currentFeedId, DateHelper.ConvertDateToLong(createdTimestamp), companyId, hashTags, mainSession, analyticSession);
                    }

                    // Update privacy
                    Dictionary<string, List<string>> updatedPrivacyDict = UpdateFeedPrivacy(ownerUserId,
                                                                                            currentFeedId,
                                                                                            companyId,
                                                                                            Int32.Parse(FeedTypeCode.ImagePost),
                                                                                            caption,
                                                                                            targetedDepartmentIds,
                                                                                            targetedUserIds,
                                                                                            targetedGroupIds,
                                                                                            taggedUserIds,
                                                                                            DateHelper.ConvertDateToLong(createdTimestamp),
                                                                                            mainSession);


                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);

                    // Create notification for new targeted users
                    Dictionary<string, List<string>> privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("AddedUsers", updatedPrivacyDict["AddedUsers"]);
                    privacyNotifyDict.Add("AddedUsersInDepartment", updatedPrivacyDict["AddedUsersInDepartment"]);
                    privacyNotifyDict.Add("AddedUsersForMention", updatedPrivacyDict["AddedUsersForMention"]);

                    if (isSpecificNotification)
                    {
                        response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }
                    else
                    {
                        response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }

                    // Remove targeted users due to change of privacy
                    privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("RemovedUsers", updatedPrivacyDict["RemovedUsers"]);
                    privacyNotifyDict.Add("RemovedUsersInDepartment", updatedPrivacyDict["RemovedUsersInDepartment"]);
                    privacyNotifyDict.Add("RemovedUsersForMention", updatedPrivacyDict["RemovedUsersForMention"]);

                    RemoveNotificationForFeed(companyId, currentFeedId, feedType, ownerUserId, privacyNotifyDict, mainSession);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse EditToFeedImagePost(string ownerUserId,
                                                      string companyId,
                                                      string currentFeedId,
                                                      string caption,
                                                      List<Image> updatedImages,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      List<string> targetedGroupIds,
                                                      bool isSpecificNotification = false)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, mainSession);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;

                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, mainSession);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;

                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;

                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");

                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                // Edit only caption of image
                if (feedType == Convert.ToInt16(FeedTypeCode.ImagePost))
                {
                    // Check for feed image bug
                    if (updatedImages == null || updatedImages.Count == 0)
                    {
                        Log.Error(string.Format("{0}: {1}", ErrorMessage.FeedInvalidImageContent, currentFeedId));
                        response.ErrorCode = Convert.ToInt32(ErrorCode.FeedInvalidImageContent);
                        response.ErrorMessage = ErrorMessage.FeedInvalidImageContent;
                        return response;
                    }

                    // Update image table
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_image",
                        new List<string> { "id" }, new List<string> { "caption", "last_modified_timestamp" },
                        new List<string>()));
                    updateBatch.Add(ps.Bind(caption, DateTime.UtcNow, currentFeedId));

                    // retrieve feed_image_urls that correspond to the current feedid
                    PreparedStatement psFeedImageUrl = mainSession.Prepare(CQLGenerator.SelectStatement("feed_image_url",
                            new List<string>(), new List<string> { "feed_id", "is_valid" }));
                    BoundStatement bsFeedImageUrl = psFeedImageUrl.Bind(currentFeedId, true);

                    RowSet feedImageUrlRowSet = mainSession.Execute(bsFeedImageUrl);

                    List<Image> existingImagesList = new List<Image>();
                    foreach (Row feedImageUrlRow in feedImageUrlRowSet.GetRows())
                    {
                        Image image = new Image
                        {
                            ImageId = feedImageUrlRow.GetValue<string>("id"),
                            Url = feedImageUrlRow.GetValue<string>("url"),
                            Order = feedImageUrlRow.GetValue<int>("in_order")
                        };

                        existingImagesList.Add(image);
                    }

                    IAmazonS3 s3Client =
                        AWSClientFactory.CreateAmazonS3Client(
                            WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(),
                            WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(),
                            RegionEndpoint.APSoutheast1);

                    string regexPattern = @"^(?:.*/feeds/F\w+/)(\d+)(?:_original)";
                    var regex = new Regex(regexPattern);

                    foreach (Image existingImage in existingImagesList)
                    {
                        bool willDelete = true;

                        var existingMatch = regex.Match(existingImage.Url);
                        string existingImageName = existingMatch.Groups[1].Value;

                        foreach (Image updatedImage in updatedImages)
                        {
                            var updatedMatch = regex.Match(updatedImage.Url);
                            string updatedImageName = updatedMatch.Groups[1].Value;

                            if (existingImageName.Equals(updatedImageName))
                            {
                                willDelete = false;
                                break;
                            }
                        }

                        if (willDelete)
                        {
                            // s3 delete
                            string bucketName = "cocadre-" + companyId.ToLower();
                            string[] suffix = { "small", "medium", "large", "original" };

                            for (var i = 0; i < suffix.Length; i++)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key =
                                        string.Format("feeds/{0}/{1}_{2}.jpg", currentFeedId, existingImageName,
                                            suffix[i])
                                };
                                s3Client.DeleteObject(deleteRequest);
                            }
                        }
                    }

                    // Update image url table
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_image_url",
                        new List<string> { "feed_id" }));
                    deleteBatch.Add(ps.Bind(currentFeedId)); // delete all feed_image_url

                    foreach (Image image in updatedImages)
                    {
                        string imageId = UUIDGenerator.GenerateUniqueIDForFeedMedia();

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("feed_image_url",
                            new List<string> { "id", "feed_id", "url", "in_order", "is_valid" }));
                        updateBatch.Add(ps.Bind(imageId, currentFeedId, image.Url, image.Order, true));
                    }

                    // Search tagged user ids from content
                    List<string> taggedUserIds = SearchTaggedUserIdsFromContent(caption, companyId, vh, mainSession);

                    // Delete previous hash tag first
                    DeleteHashTag(currentFeedId, companyId, mainSession, analyticSession);

                    //Extract hashTag
                    List<string> hashTags = SearchHashedTagsFromContent(caption);
                    if (hashTags.Count > 0)
                    {
                        CreateHashTag(currentFeedId, DateHelper.ConvertDateToLong(createdTimestamp), companyId, hashTags, mainSession, analyticSession);
                    }

                    // Update privacy
                    Dictionary<string, List<string>> updatedPrivacyDict = UpdateFeedPrivacy(ownerUserId,
                                                                                            currentFeedId,
                                                                                            companyId,
                                                                                            Int32.Parse(FeedTypeCode.ImagePost),
                                                                                            caption,
                                                                                            targetedDepartmentIds,
                                                                                            targetedUserIds,
                                                                                            targetedGroupIds,
                                                                                            taggedUserIds,
                                                                                            DateHelper.ConvertDateToLong(createdTimestamp),
                                                                                            mainSession);


                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);

                    // Create notification for new targeted users
                    Dictionary<string, List<string>> privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("AddedUsers", updatedPrivacyDict["AddedUsers"]);
                    privacyNotifyDict.Add("AddedUsersInDepartment", updatedPrivacyDict["AddedUsersInDepartment"]);
                    privacyNotifyDict.Add("AddedUsersForMention", updatedPrivacyDict["AddedUsersForMention"]);

                    if (isSpecificNotification)
                    {
                        response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }
                    else
                    {
                        response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }

                    // Remove targeted users due to change of privacy
                    privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("RemovedUsers", updatedPrivacyDict["RemovedUsers"]);
                    privacyNotifyDict.Add("RemovedUsersInDepartment", updatedPrivacyDict["RemovedUsersInDepartment"]);
                    privacyNotifyDict.Add("RemovedUsersForMention", updatedPrivacyDict["RemovedUsersForMention"]);

                    RemoveNotificationForFeed(companyId, currentFeedId, feedType, ownerUserId, privacyNotifyDict, mainSession);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToFeedVideoPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds,
                                                        List<string> targetedGroupIds,
                                                        bool isSpecificNotification = false)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, mainSession);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, mainSession);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                // Edit only caption of video
                if (feedType == Convert.ToInt16(FeedTypeCode.VideoPost))
                {
                    // Update video table
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_video",
                        new List<string> { "id" }, new List<string> { "caption", "last_modified_timestamp" },
                        new List<string>()));
                    updateBatch.Add(ps.Bind(caption, DateTime.UtcNow, currentFeedId));

                    // Search tagged user ids from content
                    List<string> taggedUserIds = SearchTaggedUserIdsFromContent(caption, companyId, vh, mainSession);

                    // Delete previous hash tag first
                    DeleteHashTag(currentFeedId, companyId, mainSession, analyticSession);

                    //Extract hashTag
                    List<string> hashTags = SearchHashedTagsFromContent(caption);
                    if (hashTags.Count > 0)
                    {
                        CreateHashTag(currentFeedId, DateHelper.ConvertDateToLong(createdTimestamp), companyId, hashTags, mainSession, analyticSession);
                    }

                    // Update privacy
                    Dictionary<string, List<string>> updatedPrivacyDict = UpdateFeedPrivacy(ownerUserId,
                                                                                            currentFeedId,
                                                                                            companyId,
                                                                                            Int32.Parse(FeedTypeCode.VideoPost),
                                                                                            caption,
                                                                                            targetedDepartmentIds,
                                                                                            targetedUserIds,
                                                                                            targetedGroupIds,
                                                                                            taggedUserIds,
                                                                                            DateHelper.ConvertDateToLong(createdTimestamp),
                                                                                            mainSession);


                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);

                    // Create notification for new targeted users
                    Dictionary<string, List<string>> privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("AddedUsers", updatedPrivacyDict["AddedUsers"]);
                    privacyNotifyDict.Add("AddedUsersInDepartment", updatedPrivacyDict["AddedUsersInDepartment"]);
                    privacyNotifyDict.Add("AddedUsersForMention", updatedPrivacyDict["AddedUsersForMention"]);

                    if (isSpecificNotification)
                    {
                        response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }
                    else
                    {
                        response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }

                    // Remove targeted users due to change of privacy
                    privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("RemovedUsers", updatedPrivacyDict["RemovedUsers"]);
                    privacyNotifyDict.Add("RemovedUsersInDepartment", updatedPrivacyDict["RemovedUsersInDepartment"]);
                    privacyNotifyDict.Add("RemovedUsersForMention", updatedPrivacyDict["RemovedUsersForMention"]);

                    RemoveNotificationForFeed(companyId, currentFeedId, feedType, ownerUserId, privacyNotifyDict, mainSession);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateResponse UpdateToSharedUrlPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds,
                                                        List<string> targetedGroupIds,
                                                        bool isSpecificNotification = false)
        {
            FeedUpdateResponse response = new FeedUpdateResponse();
            response.TargetedNotifications = new List<Notification>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ErrorStatus hasErrorAsUser = vh.isValidatedAsUserToPostFeed(ownerUserId, companyId, mainSession);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user/permission");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(currentFeedId, companyId, mainSession);

                if (feedRow == null)
                {
                    Log.Error("Invalid feed post: " + currentFeedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (!feedRow.GetValue<string>("owner_user_id").Equals(ownerUserId))
                {
                    Log.Error(string.Format("Invalid owner: {0} for feed post: {1}", ownerUserId, currentFeedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidOwner);
                    response.ErrorMessage = ErrorMessage.FeedInvalidOwner;
                    return response;
                }

                int feedType = feedRow.GetValue<int>("feed_type");
                DateTime createdTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");
                DateTimeOffset createdTimeOffset = feedRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                PreparedStatement ps = null;

                // Edit only caption of url
                if (feedType == Convert.ToInt16(FeedTypeCode.SharedUrlPost))
                {
                    // Update url table
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_shared_url",
                        new List<string> { "id" }, new List<string> { "caption", "last_modified_timestamp" },
                        new List<string>()));
                    updateBatch.Add(ps.Bind(caption, DateTime.UtcNow, currentFeedId));

                    // Search tagged user ids from content
                    List<string> taggedUserIds = SearchTaggedUserIdsFromContent(caption, companyId, vh, mainSession);

                    // Delete previous hash tag first
                    DeleteHashTag(currentFeedId, companyId, mainSession, analyticSession);

                    //Extract hashTag
                    List<string> hashTags = SearchHashedTagsFromContent(caption);
                    if (hashTags.Count > 0)
                    {
                        CreateHashTag(currentFeedId, DateHelper.ConvertDateToLong(createdTimestamp), companyId, hashTags, mainSession, analyticSession);
                    }

                    // Update privacy
                    Dictionary<string, List<string>> updatedPrivacyDict = UpdateFeedPrivacy(ownerUserId,
                                                                                            currentFeedId,
                                                                                            companyId,
                                                                                            Int32.Parse(FeedTypeCode.SharedUrlPost),
                                                                                            caption,
                                                                                            targetedDepartmentIds,
                                                                                            targetedUserIds,
                                                                                            targetedGroupIds,
                                                                                            taggedUserIds,
                                                                                            DateHelper.ConvertDateToLong(createdTimestamp),
                                                                                            mainSession);


                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);

                    // Create notification for new targeted users
                    Dictionary<string, List<string>> privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("AddedUsers", updatedPrivacyDict["AddedUsers"]);
                    privacyNotifyDict.Add("AddedUsersInDepartment", updatedPrivacyDict["AddedUsersInDepartment"]);
                    privacyNotifyDict.Add("AddedUsersForMention", updatedPrivacyDict["AddedUsersForMention"]);

                    if (isSpecificNotification)
                    {
                        response.TargetedNotifications.AddRange(GetNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }
                    else
                    {
                        response.TargetedNotifications.AddRange(GetGeneralNotificationListForFeedCreation(companyId, currentFeedId, Convert.ToInt16(FeedTypeCode.ImagePost), ownerUserId, createdTimestamp, privacyNotifyDict, mainSession));
                    }

                    // Remove targeted users due to change of privacy
                    privacyNotifyDict = new Dictionary<string, List<string>>();
                    privacyNotifyDict.Add("RemovedUsers", updatedPrivacyDict["RemovedUsers"]);
                    privacyNotifyDict.Add("RemovedUsersInDepartment", updatedPrivacyDict["RemovedUsersInDepartment"]);
                    privacyNotifyDict.Add("RemovedUsersForMention", updatedPrivacyDict["RemovedUsersForMention"]);

                    RemoveNotificationForFeed(companyId, currentFeedId, feedType, ownerUserId, privacyNotifyDict, mainSession);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectUpVotersResponse SelectUpVoters(string requesterUserId, string companyId, string feedId, string commentId, string replyId)
        {
            FeedSelectUpVotersResponse response = new FeedSelectUpVotersResponse();
            response.Voters = new List<User>();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                ErrorStatus hasErrorAsUser = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (hasErrorAsUser != null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = hasErrorAsUser.ErrorCode;
                    response.ErrorMessage = hasErrorAsUser.ErrorMessage;
                    return response;
                }

                Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);

                if (feedRow == null)
                {
                    Log.Error("Invalid feedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                string ownerUserId = feedRow.GetValue<string>("owner_user_id");
                bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                bool isForUser = feedRow.GetValue<bool>("is_for_user");
                bool isTaggedForUser = feedRow.GetValue<bool>("is_tagged_for_user");
                bool isForGroup = feedRow.GetValue<bool>("is_for_group");
                bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                Department departmentManager = new Department();
                User userManager = new User();

                List<Department> departments =
                    departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;
                List<string> departmentIds = new List<string>();

                foreach (Department depart in departments)
                {
                    departmentIds.Add(depart.Id);
                }

                if (!CheckPostForCurrentUser(requesterUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, departmentIds, session))
                {
                    Log.Error("Feed privacy has been updated");
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                // Voters for feed
                PreparedStatement ps = null;
                RowSet voterRowset = null;
                if (string.IsNullOrEmpty(commentId) && string.IsNullOrEmpty(replyId))
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("feed_point", new List<string>(), new List<string> { "feed_id", "value" }));
                    voterRowset = session.Execute(ps.Bind(feedId, 1));
                }
                else
                {
                    // Voters for comment
                    if (string.IsNullOrEmpty(replyId))
                    {
                        Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);

                        if (commentRow == null)
                        {
                            Log.Error("Invalid comment " + commentId);
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                            response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.SelectStatement("comment_point", new List<string>(),
                                new List<string> { "comment_id", "value" }));
                        voterRowset = session.Execute(ps.Bind(commentId, 1));
                    }
                    // Voters for reply
                    else
                    {
                        Row replyRow = vh.ValidateCommentReply(commentId, replyId, session);

                        if (replyRow == null)
                        {
                            Log.Error("Invalid reply " + replyId);
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidReply);
                            response.ErrorMessage = ErrorMessage.FeedInvalidReply;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.SelectStatement("reply_point", new List<string>(),
                                new List<string> { "reply_id", "value" }));
                        voterRowset = session.Execute(ps.Bind(replyId, 1));
                    }
                }

                if (voterRowset != null)
                {
                    foreach (Row voterRow in voterRowset)
                    {
                        string userId = voterRow.GetValue<string>("user_id");
                        User voter = userManager.SelectUserBasic(userId, companyId, false, session, null, true).User;
                        if (voter != null)
                        {
                            response.Voters.Add(voter);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedImageProgressesResponse SelectFeedImageProgresses(string feedId, ISession session = null)
        {
            FeedImageProgressesResponse feedImageProgressesResponse = new FeedImageProgressesResponse();
            feedImageProgressesResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                /*
                // all the preparedstatements here
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("feed_image_progress", new List<string> { }, new List<string> { "feed_id", "image_name" }));
                BoundStatement bs = ps.Bind(feedId, imageName);
                Row row = session.Execute(bs).FirstOrDefault();
                */

                // try mapper to select 1
                IMapper mapper = new Mapper(session);
                IEnumerable<FeedImageProgress> feedImageProgresses = mapper.Fetch<FeedImageProgress>(CQLGenerator.SelectStatement("feed_image_progress", new List<string> { }, new List<string> { "feed_id" }), feedId);

                feedImageProgressesResponse.feedImageProgresses = feedImageProgresses;
                feedImageProgressesResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedImageProgressesResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedImageProgressesResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedImageProgressesResponse;
        }

        public FeedImageProgressResponse SelectFeedImageProgress(string feedId, string imageName, ISession session = null)
        {
            FeedImageProgressResponse feedImageProgressResponse = new FeedImageProgressResponse();
            feedImageProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                IMapper mapper = new Mapper(session);
                FeedImageProgress feedImageProgress = mapper.FirstOrDefault<FeedImageProgress>(CQLGenerator.SelectStatement("feed_image_progress", new List<string> { }, new List<string> { "feed_id", "image_name" }), feedId, imageName);

                feedImageProgressResponse.feedImageProgress = feedImageProgress;
                feedImageProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedImageProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedImageProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedImageProgressResponse;
        }

        public FeedImageProgressResponse DeleteFeedImageProgress(FeedImageProgress feedImageProgress, ISession session = null)
        {
            FeedImageProgressResponse feedImageProgressResponse = new FeedImageProgressResponse();
            feedImageProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                IMapper mapper = new Mapper(session);
                mapper.Delete(feedImageProgress);

                feedImageProgressResponse.feedImageProgress = feedImageProgress;
                feedImageProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedImageProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedImageProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedImageProgressResponse;
        }

        public FeedImageProgressResponse UpdateFeedImageProgress(FeedImageProgress feedImageProgress, ISession session = null)
        {
            FeedImageProgressResponse feedImageProgressResponse = new FeedImageProgressResponse();
            feedImageProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                IMapper mapper = new Mapper(session);
                mapper.Update<FeedImageProgress>(feedImageProgress);

                feedImageProgressResponse.feedImageProgress = feedImageProgress;
                feedImageProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedImageProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedImageProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedImageProgressResponse;
        }

        public FeedImageProgressResponse CreateFeedImageProgress(
                                    string feedId,
                                    string imageName,
                                    double progress,
                                    bool completed,
                                    bool uploaded,
                                    int expectedCount,
                                    string environment,
                                    string companyId,
                                    int feedType = -1,
                                    string targetedDepartmentIds = null,
                                    string targetedGroupIds = null,
                                    string targetedUserIds = null,
                                    string caption = null,
                                    bool isSpecificNotification = false,
                                    bool isForAdmin = false,
                                    bool isVideoThumbnail = false,
                                    bool isEdit = false,
                                    int ordering = 0,
                                    string userId = null,
                                    string url = null,
                                    string sharedUrl = null,
                                    string urlTitle = null,
                                    string urlDescription = null,
                                    string urlSiteName = null,
                                    ISession session = null)
        {
            FeedImageProgressResponse feedImageProgressResponse = new FeedImageProgressResponse();
            feedImageProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                // all the prepared statements here
                var newFeedImageProgress = new FeedImageProgress
                {
                    FeedId = feedId,
                    ImageName = imageName,
                    UserId = userId,
                    Progress = progress,
                    Completed = completed,
                    Uploaded = uploaded,
                    ExpectedCount = expectedCount,
                    Environment = environment,
                    CompanyId = companyId,
                    FeedType = feedType,
                    TargetedDepartmentIds = targetedDepartmentIds,
                    TargetedGroupIds = targetedGroupIds,
                    TargetedUserIds = targetedUserIds,
                    Caption = caption,
                    IsSpecificNotification = isSpecificNotification,
                    IsForAdmin = isForAdmin,
                    IsVideoThumbnail = isVideoThumbnail,
                    IsEdit = isEdit,
                    Ordering = ordering,
                    Url = url,
                    SharedUrl = sharedUrl,
                    UrlTitle = urlTitle,
                    UrlDescription = urlDescription,
                    UrlSiteName = urlSiteName,
                };

                IMapper mapper = new Mapper(session);
                mapper.Insert(newFeedImageProgress);

                feedImageProgressResponse.feedImageProgress = newFeedImageProgress;
                feedImageProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedImageProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedImageProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedImageProgressResponse;
        }

        public FeedVideoProgressResponse CreateFeedVideoProgress(
                                   string feedId,
                                   string playlistName,
                                   double progress,
                                   bool completed,
                                   bool uploaded,
                                   string environment,
                                   string companyId,
                                   int feedType = -1,
                                   string s3SrcKey = null,
                                   string s3OutputKeyPrefix = null,
                                   string targetedDepartmentIds = null,
                                   string targetedGroupIds = null,
                                   string targetedUserIds = null,
                                   string caption = null,
                                   bool isSpecificNotification = false,
                                   bool isForAdmin = false,
                                   string userId = null,
                                   ISession session = null)
        {
            FeedVideoProgressResponse feedVideoProgressResponse = new FeedVideoProgressResponse();
            feedVideoProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                // all the prepared statements here
                var newFeedVideoProgress = new FeedVideoProgress
                {
                    FeedId = feedId,
                    PlaylistName = playlistName,
                    UserId = userId,
                    Progress = progress,
                    Completed = completed,
                    Uploaded = uploaded,
                    Environment = environment,
                    FeedType = feedType,
                    CompanyId = companyId,
                    TargetedDepartmentIds = targetedDepartmentIds,
                    TargetedGroupIds = targetedGroupIds,
                    TargetedUserIds = targetedUserIds,
                    Caption = caption,
                    IsSpecificNotification = isSpecificNotification,
                    IsForAdmin = isForAdmin,
                    S3SrcKey = s3SrcKey,
                    S3OutputKeyPrefix = s3OutputKeyPrefix
                };

                IMapper mapper = new Mapper(session);
                mapper.Insert(newFeedVideoProgress);

                feedVideoProgressResponse.feedVideoProgress = newFeedVideoProgress;
                feedVideoProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedVideoProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedVideoProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedVideoProgressResponse;
        }

        public FeedVideoProgressResponse SelectFeedVideoProgress(string feedId, ISession session = null)
        {
            FeedVideoProgressResponse feedVideoProgressResponse = new FeedVideoProgressResponse();
            feedVideoProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                /*
                // all the preparedstatements here
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("feed_image_progress", new List<string> { }, new List<string> { "feed_id", "image_name" }));
                BoundStatement bs = ps.Bind(feedId, imageName);
                Row row = session.Execute(bs).FirstOrDefault();
                */

                // try mapper to select 1
                IMapper mapper = new Mapper(session);
                FeedVideoProgress feedVideoProgress = mapper.FirstOrDefault<FeedVideoProgress>(CQLGenerator.SelectStatement("feed_video_progress", new List<string> { }, new List<string> { "feed_id" }), feedId);

                feedVideoProgressResponse.feedVideoProgress = feedVideoProgress;
                feedVideoProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedVideoProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedVideoProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedVideoProgressResponse;
        }

        public FeedVideoProgressResponse UpdateFeedVideoProgress(FeedVideoProgress feedVideoProgress, ISession session = null)
        {
            FeedVideoProgressResponse feedVideoProgressResponse = new FeedVideoProgressResponse();
            feedVideoProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                IMapper mapper = new Mapper(session);
                mapper.Update<FeedVideoProgress>(feedVideoProgress);

                feedVideoProgressResponse.feedVideoProgress = feedVideoProgress;
                feedVideoProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                feedVideoProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                feedVideoProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return feedVideoProgressResponse;
        }

        public FeedSelectAllHashTagResponse SelectAllHashTags(string companyId, string requesterUserId, ISession mainSession = null, ISession analyticSession = null)
        {
            FeedSelectAllHashTagResponse response = new FeedSelectAllHashTagResponse();
            response.PopularTags = new List<FeedHashTag>();
            response.RecommendedTags = new List<FeedHashTag>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ValidationHandler vh = new ValidationHandler();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    Row companyRow = vh.ValidateCompany(companyId, mainSession);
                    if (companyRow == null)
                    {
                        Log.Error("Invalid company: " + companyId);
                        response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                        response.ErrorMessage = ErrorMessage.CompanyInvalid;
                        return response;
                    }

                    Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                    if (userRow == null)
                    {
                        Log.Error("Invalid user: " + requesterUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                int limit = 0;

                response.RecommendedTags = SelectAllFeedRecommendedHashTags(null, companyId, limit, mainSession).RecommendedTags;

                if (response.RecommendedTags.Count == 0)
                {
                    string recommendedFullString = DefaultResource.RecommendedTags;
                    string[] recommendedArray = recommendedFullString.Split(',');

                    foreach (string tag in recommendedArray)
                    {
                        response.RecommendedTags.Add(new FeedHashTag
                        {
                            HashTag = string.Format("#{0}", tag)
                        });
                    }
                }

                response.RecommendedTags = response.RecommendedTags.OrderBy(t => t.HashTag).ToList();

                response.PopularTags = new AnalyticFeed().SelectPopularTags(companyId, 0, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSearchHashTagResponse SearchHashTag(string companyId, string requesterUserId, string startsWith)
        {
            FeedSearchHashTagResponse response = new FeedSearchHashTagResponse();
            response.PopularTags = new List<FeedHashTag>();
            response.RecommendedTags = new List<FeedHashTag>();
            response.Tags = new List<FeedHashTag>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ValidationHandler vh = new ValidationHandler();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                Row companyRow = vh.ValidateCompany(companyId, mainSession);
                if (companyRow == null)
                {
                    Log.Error("Invalid company: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                PreparedStatement ps = null;
                startsWith = startsWith.Replace("#", "");
                startsWith = startsWith.ToLower();

                if (string.IsNullOrEmpty(startsWith))
                {
                    FeedSelectAllHashTagResponse allHashResponse = SelectAllHashTags(companyId, requesterUserId, mainSession, analyticSession);
                    response.PopularTags = allHashResponse.PopularTags;
                    response.RecommendedTags = allHashResponse.RecommendedTags;
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_by_tag", new List<string>(), new List<string> { "company_id" }));
                    RowSet allTagsRowset = mainSession.Execute(ps.Bind(companyId));

                    foreach (Row tagRow in allTagsRowset)
                    {
                        string tag = tagRow.GetValue<string>("tag");
                        tag = tag.ToLower();

                        if (tag.StartsWith(startsWith) && (response.Tags.Count == 0 || response.Tags.FirstOrDefault(t => t.HashTag.Replace("#", "").Equals(tag)) == null))
                        {
                            ps = mainSession.Prepare(CQLGenerator.CountStatement("small_cap_tag_by_sorted_timestamp", new List<string> { "company_id", "small_cap_tag" }));
                            int currentCount = (int)mainSession.Execute(ps.Bind(companyId, tag)).FirstOrDefault().GetValue<long>("count");

                            response.Tags.Add(new FeedHashTag
                            {
                                HashTag = string.Format("#{0}", tag),
                                Popularity = currentCount
                            });
                        }

                    }

                    response.Tags = response.Tags.OrderByDescending(t => t.Popularity).ToList();

                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedSelectAllHashTagResponse SelectAllFeedRecommendedHashTags(string adminUserId, string companyId, int limit, ISession mainSession = null)
        {
            FeedSelectAllHashTagResponse response = new FeedSelectAllHashTagResponse();
            response.RecommendedTags = new List<FeedHashTag>();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("recommended_tag_by_company_sorted", new List<string>(), new List<string> { "company_id" }, limit));
                RowSet recommendedRowset = mainSession.Execute(ps.Bind(companyId));

                foreach (Row recommendedRow in recommendedRowset)
                {
                    string tag = recommendedRow.GetValue<string>("tag");
                    int ordering = recommendedRow.GetValue<int>("ordering");
                    DateTime createdTime = recommendedRow.GetValue<DateTime>("created_on_timestamp");
                    response.RecommendedTags.Add(new FeedHashTag
                    {
                        HashTag = string.Format("#{0}", tag),
                        Ordering = ordering,
                        CreatedOnTimestamp = createdTime
                    });
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedCreateHashTagResponse CreateFeedRecommendedHashTag(string adminUserId, string companyId, string hashTag)
        {
            FeedCreateHashTagResponse response = new FeedCreateHashTagResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (!hashTag.ToCharArray()[0].Equals('#'))
                {
                    hashTag = "#" + hashTag;
                }

                hashTag = SearchHashedTagsFromContent(hashTag)[0];

                if (string.IsNullOrEmpty(hashTag))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.FeedHashTagInvalid);
                    response.ErrorMessage = ErrorMessage.FeedHashTagInvalid;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = null;

                ps = session.Prepare(CQLGenerator.SelectStatement("recommended_tag_by_company", new List<string>(), new List<string> { "company_id", "small_cap_tag" }));
                if (session.Execute(ps.Bind(companyId, hashTag.ToLower())).FirstOrDefault() != null)
                {
                    //response.Success = true;
                    // A-Po fix. 2016/09/07 12:40:00. 如果已經有的話，return not success.
                    response.ErrorCode = Convert.ToInt32(ErrorCode.HashtagSameKey);
                    response.ErrorMessage = ErrorMessage.HashtagSameKey;
                    return response;
                }

                ps = session.Prepare(CQLGenerator.CountStatement("recommended_tag_by_company", new List<string> { "company_id" }));
                int ordering = (int)session.Execute(ps.Bind(companyId)).FirstOrDefault().GetValue<long>("count") + 1;

                DateTime currentTime = DateTime.UtcNow;
                ps = session.Prepare(CQLGenerator.InsertStatement("recommended_tag_by_company", new List<string> { "company_id", "tag", "small_cap_tag", "ordering", "created_on_timestamp", "last_modified_by_admin_user_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(companyId, hashTag, hashTag.ToLower(), ordering, currentTime, adminUserId, currentTime));

                ps = session.Prepare(CQLGenerator.InsertStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "tag", "small_cap_tag", "ordering", "created_on_timestamp", "last_modified_by_admin_user_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(companyId, hashTag, hashTag.ToLower(), ordering, currentTime, adminUserId, currentTime));

                session.Execute(batch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateHashTagResponse UpdateFeedRecommendedHashTagOrdering(string adminUserId, string companyId, List<string> hashTags)
        {
            FeedUpdateHashTagResponse response = new FeedUpdateHashTagResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                ps = session.Prepare(CQLGenerator.CountStatement("recommended_tag_by_company", new List<string> { "company_id" }));
                int count = (int)session.Execute(ps.Bind(companyId)).FirstOrDefault().GetValue<long>("count");

                if (count != hashTags.Count)
                {
                    Log.Error(ErrorMessage.FeedHashTagWrongCountForReordering);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.FeedHashTagWrongCountForReordering);
                    response.ErrorMessage = ErrorMessage.FeedHashTagWrongCountForReordering;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                int newOrdering = 1;
                foreach (string hashTag in hashTags)
                {
                    string tag = hashTag.Replace("#", "");

                    ps = session.Prepare(CQLGenerator.SelectStatement("recommended_tag_by_company", new List<string>(), new List<string> { "company_id", "small_cap_tag" }));
                    Row hashRow = session.Execute(ps.Bind(companyId, tag.ToLower())).FirstOrDefault();
                    if (hashRow != null)
                    {
                        int currentOrdering = hashRow.GetValue<int>("ordering");

                        if (currentOrdering != newOrdering)
                        {
                            DateTime createdTime = hashRow.GetValue<DateTime>("created_on_timestamp");

                            ps = session.Prepare(CQLGenerator.UpdateStatement("recommended_tag_by_company",
                                new List<string> { "company_id", "small_cap_tag" }, new List<string> { "ordering", "last_modified_by_admin_user_id", "last_modified_timestamp" }, new List<string>()));
                            updateBatch.Add(ps.Bind(newOrdering, adminUserId, currentTime, companyId, tag.ToLower()));

                            ps = session.Prepare(CQLGenerator.DeleteStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "small_cap_tag", "ordering" }));
                            deleteBatch.Add(ps.Bind(companyId, tag.ToLower(), currentOrdering));

                            ps = session.Prepare(CQLGenerator.InsertStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "tag", "small_cap_tag", "ordering", "created_on_timestamp", "last_modified_by_admin_user_id", "last_modified_timestamp" }));
                            updateBatch.Add(ps.Bind(companyId, tag, tag.ToLower(), newOrdering, createdTime, adminUserId, currentTime));
                        }

                        newOrdering++;
                    }

                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateHashTagResponse UpdateFeedRecommendedHashTag(string adminUserId, string companyId, string oldHashTag, string newHashTag)
        {
            FeedUpdateHashTagResponse response = new FeedUpdateHashTagResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                DateTime currentTime = DateTime.UtcNow;
                if (!newHashTag.ToCharArray()[0].Equals('#'))
                {
                    newHashTag = "#" + newHashTag;
                }
                newHashTag = SearchHashedTagsFromContent(newHashTag)[0];

                if (string.IsNullOrEmpty(newHashTag))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.FeedHashTagInvalid);
                    response.ErrorMessage = ErrorMessage.FeedHashTagInvalid;
                    return response;
                }

                // Check new tag
                ps = session.Prepare(CQLGenerator.SelectStatement("recommended_tag_by_company", new List<string>(), new List<string> { "company_id", "small_cap_tag" }));
                Row selectedTagRow = session.Execute(ps.Bind(companyId, newHashTag.ToLower())).FirstOrDefault();
                if (selectedTagRow != null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.FeedHashTagAlreadyExisted);
                    response.ErrorMessage = ErrorMessage.FeedHashTagAlreadyExisted;
                    return response;
                }

                // Check old tag
                oldHashTag = oldHashTag.Replace("#", "");
                ps = session.Prepare(CQLGenerator.SelectStatement("recommended_tag_by_company", new List<string>(), new List<string> { "company_id", "small_cap_tag" }));
                Row currentTagRow = session.Execute(ps.Bind(companyId, oldHashTag.ToLower())).FirstOrDefault();
                if (currentTagRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.FeedHashTagDoesNotExists);
                    response.ErrorMessage = ErrorMessage.FeedHashTagDoesNotExists;
                    return response;
                }

                DateTime createdTime = currentTagRow.GetValue<DateTime>("created_on_timestamp");
                int currentOrdering = currentTagRow.GetValue<int>("ordering");

                ps = session.Prepare(CQLGenerator.DeleteStatement("recommended_tag_by_company", new List<string> { "company_id", "small_cap_tag" }));
                //deleteBatch.Add(ps.Bind(companyId, oldHashTag.ToLower(), currentOrdering)); A-Po fix.
                deleteBatch.Add(ps.Bind(companyId, oldHashTag.ToLower()));

                ps = session.Prepare(CQLGenerator.InsertStatement("recommended_tag_by_company", new List<string> { "company_id", "tag", "small_cap_tag", "ordering", "created_on_timestamp", "last_modified_by_admin_user_id", "last_modified_timestamp" }));
                updateBatch.Add(ps.Bind(companyId, newHashTag, newHashTag.ToLower(), currentOrdering, createdTime, adminUserId, currentTime));

                ps = session.Prepare(CQLGenerator.DeleteStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "small_cap_tag", "ordering" }));
                deleteBatch.Add(ps.Bind(companyId, oldHashTag.ToLower(), currentOrdering));

                ps = session.Prepare(CQLGenerator.InsertStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "tag", "small_cap_tag", "ordering", "created_on_timestamp", "last_modified_by_admin_user_id", "last_modified_timestamp" }));
                updateBatch.Add(ps.Bind(companyId, newHashTag, newHashTag.ToLower(), currentOrdering, createdTime, adminUserId, currentTime));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public FeedUpdateHashTagResponse DeleteFeedRecommendedHashTag(string adminUserId, string companyId, string oldHashTag)
        {
            FeedUpdateHashTagResponse response = new FeedUpdateHashTagResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                oldHashTag = oldHashTag.Replace("#", "");
                if (string.IsNullOrEmpty(oldHashTag))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.FeedHashTagInvalid);
                    response.ErrorMessage = ErrorMessage.FeedHashTagInvalid;
                    return response;
                }

                // Check old tag
                ps = session.Prepare(CQLGenerator.SelectStatement("recommended_tag_by_company", new List<string>(), new List<string> { "company_id", "small_cap_tag" }));
                Row currentTagRow = session.Execute(ps.Bind(companyId, oldHashTag.ToLower())).FirstOrDefault();
                if (currentTagRow == null)
                {
                    response.Success = false;
                    response.ErrorCode = Convert.ToInt32(ErrorCode.FeedHashTagDoesNotExists);
                    response.ErrorMessage = ErrorMessage.FeedHashTagDoesNotExists;
                    return response;
                }

                int currentOrdering = currentTagRow.GetValue<int>("ordering");

                ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("recommended_tag_by_company_sorted", new List<string>(), new List<string> { "company_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                RowSet tagOrderRowset = session.Execute(ps.Bind(companyId, currentOrdering));
                foreach (Row tagOrderRow in tagOrderRowset)
                {
                    string tag = tagOrderRow.GetValue<string>("tag");
                    DateTime createdTime = tagOrderRow.GetValue<DateTime>("created_on_timestamp");
                    DateTime modifiedTime = tagOrderRow.GetValue<DateTime>("last_modified_timestamp");
                    string lastModifiedUserId = tagOrderRow.GetValue<string>("last_modified_by_admin_user_id");
                    string smallCapTag = tagOrderRow.GetValue<string>("small_cap_tag");
                    int tagCurrentOrdering = tagOrderRow.GetValue<int>("ordering");
                    int newTagOrdering = tagCurrentOrdering - 1;

                    ps = session.Prepare(CQLGenerator.DeleteStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "small_cap_tag", "ordering" }));
                    deleteBatch.Add(ps.Bind(companyId, smallCapTag, tagCurrentOrdering));

                    ps = session.Prepare(CQLGenerator.InsertStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "tag", "small_cap_tag", "ordering", "created_on_timestamp", "last_modified_by_admin_user_id", "last_modified_timestamp" }));
                    updateBatch.Add(ps.Bind(companyId, tag, smallCapTag, newTagOrdering, createdTime, lastModifiedUserId, modifiedTime));

                    ps = session.Prepare(CQLGenerator.UpdateStatement("recommended_tag_by_company", new List<string> { "company_id", "small_cap_tag" }, new List<string> { "ordering" }, new List<string>()));
                    updateBatch.Add(ps.Bind(newTagOrdering, companyId, smallCapTag));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("recommended_tag_by_company_sorted", new List<string> { "company_id", "small_cap_tag", "ordering" }));
                deleteBatch.Add(ps.Bind(companyId, oldHashTag.ToLower(), currentOrdering));

                ps = session.Prepare(CQLGenerator.DeleteStatement("recommended_tag_by_company", new List<string> { "company_id", "small_cap_tag" }));
                deleteBatch.Add(ps.Bind(companyId, oldHashTag.ToLower()));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

#warning Deprecated code in GetGeneralNotificationListForFeedCreation, replaced by GetNotificationListForFeedCreation
        private List<Notification> GetGeneralNotificationListForFeedCreation(string companyId, string feedId, int feedType, string creatorUserId, DateTime createdTimestamp, Dictionary<string, List<string>> notificationDict, ISession mainSession)
        {
            List<Notification> result = new List<Notification>();

            List<string> userIdsForMention = notificationDict["AddedUsersForMention"];
            List<string> userIdsForUser = notificationDict["AddedUsers"];
            List<string> userIdsForDepartment = notificationDict["AddedUsersInDepartment"];
            List<string> userIds = new List<string>();

            userIdsForMention.Remove(creatorUserId);
            userIdsForUser.Remove(creatorUserId);
            userIdsForDepartment.Remove(creatorUserId);

            userIds.AddRange(userIdsForMention);
            userIds.AddRange(userIdsForUser);
            userIds.AddRange(userIdsForDepartment);

            // General
            foreach (string userId in userIds)
            {
                if (result.FirstOrDefault(n => n.TaggedUserId.Equals(userId)) == null)
                {
                    result.Add(GetNotification(companyId, feedId, string.Empty, string.Empty, creatorUserId, userId, createdTimestamp, feedType, (int)Notification.NotificationFeedSubType.NewPostToUser, mainSession));
                }
            }

            return result;
        }

        private List<Notification> GetNotificationListForFeedCreation(string companyId, string feedId, int feedType, string creatorUserId, DateTime createdTimestamp, Dictionary<string, List<string>> notificationDict, ISession mainSession)
        {
            List<Notification> result = new List<Notification>();

            List<string> userIdsForMention = notificationDict["AddedUsersForMention"];
            List<string> userIdsForUser = notificationDict["AddedUsers"];
            List<string> userIdsForDepartment = notificationDict["AddedUsersInDepartment"];

            userIdsForMention.Remove(creatorUserId);
            userIdsForUser.Remove(creatorUserId);
            userIdsForDepartment.Remove(creatorUserId);

            // Mention
            foreach (string userId in userIdsForMention)
            {
                result.Add(GetNotification(companyId, feedId, string.Empty, string.Empty, creatorUserId, userId, createdTimestamp, feedType, (int)Notification.NotificationFeedSubType.MentionedInPost, mainSession));
            }

            // User
            foreach (string userId in userIdsForUser)
            {
                if (result.FirstOrDefault(n => n.TaggedUserId.Equals(userId)) == null)
                {
                    result.Add(GetNotification(companyId, feedId, string.Empty, string.Empty, creatorUserId, userId, createdTimestamp, feedType, (int)Notification.NotificationFeedSubType.NewPostToUser, mainSession));
                }
            }

            // Department
            foreach (string userId in userIdsForDepartment)
            {
                if (result.FirstOrDefault(n => n.TaggedUserId.Equals(userId)) == null)
                {
                    result.Add(GetNotification(companyId, feedId, string.Empty, string.Empty, creatorUserId, userId, createdTimestamp, feedType, (int)Notification.NotificationFeedSubType.NewPostToDepartment, mainSession));
                }
            }

            return result;
        }

        private void RemoveNotificationForFeed(string companyId, string feedId, int feedType, string creatorUserId, Dictionary<string, List<string>> notificationDict, ISession mainSession)
        {
            List<string> userIdsForMention = notificationDict["RemovedUsersForMention"];
            List<string> userIdsForUser = notificationDict["RemovedUsers"];
            List<string> userIdsForDepartment = notificationDict["RemovedUsersInDepartment"];

            Notification notificationManager = new Notification();

            // Mention
            foreach (string userId in userIdsForMention)
            {
                notificationManager.DeleteFeedNotificationByUser(userId, (int)Notification.NotificationFeedSubType.MentionedInPost, feedId, "NA", "NA", mainSession);
            }

            // User
            foreach (string userId in userIdsForUser)
            {
                notificationManager.DeleteFeedNotificationByUser(userId, (int)Notification.NotificationFeedSubType.NewPostToUser, feedId, "NA", "NA", mainSession);
            }

            // Department
            foreach (string userId in userIdsForDepartment)
            {
                notificationManager.DeleteFeedNotificationByUser(userId, (int)Notification.NotificationFeedSubType.NewPostToDepartment, feedId, "NA", "NA", mainSession);
            }
        }

        private Notification GetNotification(string companyId, string feedId, string commentId, string replyId, string fromUserId, string targetedUserId, DateTime createdTimestamp, int feedType, int notificationSubType, ISession mainSession)
        {
            Notification notificationManager = new Notification();
            Notification notification = new Notification
            {
                NotificationText = notificationManager.CreateFeedNotification(fromUserId, companyId, targetedUserId, notificationSubType, feedType, feedId, commentId, replyId, createdTimestamp, mainSession),
                TaggedFeedId = feedId,
                TaggedCommentId = commentId,
                TaggedReplyId = replyId,
                Type = (int)Notification.NotificationType.Feed,
                SubType = notificationSubType,
                NumberOfNotificationForTargetedUser = notificationManager.SelectNotificationNumberByUser(targetedUserId, companyId, mainSession).NumberOfNotification,
                TaggedUserId = targetedUserId
            };

            return notification;
        }

        public FeedUpdateImpressionResponse UpdateImpression(string requesterUserId, string companyId, string feedId)
        {
            FeedUpdateImpressionResponse response = new FeedUpdateImpressionResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }
                else
                {
                    Thread updateThread = new Thread(() => UpdateImpressionThread(requesterUserId, companyId, feedId, vh, mainSession, analyticSession));
                    updateThread.Start();
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateImpressionThread(string requesterUserId, string companyId, string feedId, ValidationHandler vh, ISession mainSession, ISession analyticSession)
        {
            try
            {
                Row feedRow = vh.ValidateFeedPost(feedId, companyId, mainSession);
                if (feedRow != null)
                {
                    new AnalyticFeed().UpdateImpression(feedId, requesterUserId, analyticSession);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private int SelectImpressionCount(int totalVoters, string feedId, ISession mainSession, ISession analyticSession)
        {
            int impression = 0;
            PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("feed_unique_impression_counter",
                new List<string>(), new List<string> { "feed_id" }));
            Row impressionRow = analyticSession.Execute(ps.Bind(feedId)).FirstOrDefault();

            if (impressionRow != null)
            {
                impression = (int)impressionRow.GetValue<long>("impression");
            }

            int addedImpressionFromVoters = 0;

            // Do a clean up script
            if (impression < totalVoters)
            {
                Log.Debug("Clean up for impression not tally");
                addedImpressionFromVoters = AddedImpressionFromVoters(feedId, mainSession, analyticSession);
            }
            return impression + addedImpressionFromVoters;
        }

        #region Clean up
        private Dictionary<string, int> ResetFeedPoint(string feedId, int numberOfComments, ISession session)
        {
            Dictionary<string, int> updatedPoints = new Dictionary<string, int>();

            try
            {
                PreparedStatement psFeedCounter = null;

                PreparedStatement psFeedPoint = session.Prepare(CQLGenerator.SelectStatement("feed_point",
                    new List<string>(), new List<string> { "feed_id" }));
                BoundStatement bsFeedPoint = psFeedPoint.Bind(feedId);
                RowSet feedPointRowSet = session.Execute(bsFeedPoint);

                int negativePoint = 0;
                int positivePoint = 0;

                foreach (Row feedPointRow in feedPointRowSet)
                {
                    int value = feedPointRow.GetValue<int>("value");

                    if (value > 0)
                    {
                        positivePoint++;
                    }
                    else
                    {
                        negativePoint++;
                    }
                }

                Log.Debug("Reset feed counter");

                psFeedCounter = session.Prepare(CQLGenerator.DeleteStatement("feed_counter",
                    new List<string> { "feed_id" }));
                session.Execute(psFeedCounter.Bind(feedId));

                psFeedCounter = session.Prepare(CQLGenerator.UpdateCounterStatement("feed_counter", new List<string> { "feed_id" },
                    new List<string> { "negative_point", "positive_point", "comment" }, new List<int> { negativePoint, positivePoint, numberOfComments }));
                session.Execute(psFeedCounter.Bind(feedId));

                updatedPoints.Add("negativePoint", negativePoint);
                updatedPoints.Add("positivePoint", positivePoint);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return updatedPoints;
        }

        private int AddedImpressionFromVoters(string feedId, ISession mainSession, ISession analyticSession)
        {
            AnalyticFeed analyticManager = new AnalyticFeed();

            // Update from voters
            List<string> addedImpressionUserId = new List<string>();
            PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_point",
                new List<string>(), new List<string> { "feed_id" }));
            RowSet feedPointRowSet = mainSession.Execute(ps.Bind(feedId));
            foreach (Row feedPointRow in feedPointRowSet)
            {
                string userId = feedPointRow.GetValue<string>("user_id");
                DateTime votedTimestamp = feedPointRow.GetValue<DateTime>("voted_timestamp");

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("feed_unique_impression_stats",
                    new List<string>(), new List<string> { "feed_id", "seen_by_user_id" }));
                Row addedImpressionRow = analyticSession.Execute(ps.Bind(feedId, userId)).FirstOrDefault();
                if (addedImpressionRow == null && !addedImpressionUserId.Contains(userId))
                {
                    addedImpressionUserId.Add(userId);
                    analyticManager.UpdateImpression(feedId, userId, analyticSession, votedTimestamp);
                }
            }

            return addedImpressionUserId.Count;
        }
        #endregion
    }


    #region Feed type post
    public class FeedText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }

    public class FeedImage
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Image> Images { get; set; }
    }

    public class Image
    {
        [DataMember(EmitDefaultValue = false)]
        public string ImageId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Url { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ResizedImage> ResizedUrls { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Order { get; set; }
    }

    public class ResizedImage
    {
        [DataMember(EmitDefaultValue = false)]
        public string Type { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Url { get; set; }
    }

    public class FeedVideo
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VideoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string VideoThumbnailUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ResizedImage> ResizedVideoThumbnailUrls { get; set; }

    }

    public class FeedSharedUrl
    {
        [DataMember(EmitDefaultValue = false)]
        public string Caption { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Url { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SiteName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ResizedImage> ResizedImageUrls { get; set; }
    }

    #endregion

    #region Comment

    public class Comment
    {
        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CommentType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public CommentText CommentText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Reply Reply { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Reply> Replies { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public int NumberOfReplies { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReportPostId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ReportedPost> ReportedCommentPosts { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> TaggedUsers { get; set; }
    }

    public class CommentText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }

    #endregion

    #region Reply

    public class Reply
    {
        [DataMember(EmitDefaultValue = false)]
        public string FeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReplyType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ReplyText ReplyText { get; set; }

        [DataMember]
        public int NegativePoints { get; set; }

        [DataMember]
        public int PositivePoints { get; set; }

        [DataMember]
        public bool HasVoted { get; set; }

        [DataMember]
        public bool IsUpVoted { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReportPostId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ReportedPost> ReportedReplyPosts { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> TaggedUsers { get; set; }
    }

    public class ReplyText
    {
        [DataMember(EmitDefaultValue = false)]
        public string Content { get; set; }
    }

    #endregion

    #region ReportedPost

    public class ReportedPost
    {
        [DataMember(EmitDefaultValue = false)]
        public string ReportId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ReportState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Reason { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset ReportedTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User ReportedUser { get; set; }
    }

    #endregion

    #region Hash Tag
    public class FeedHashTag
    {
        [DataMember(EmitDefaultValue = false)]
        public string HashTag { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Popularity { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Ordering { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }
    }
    #endregion


    public class FeedImageProgress
    {
        // IMapper class
        public string FeedId { get; set; }
        public string ImageName { get; set; }
        public string UserId { get; set; }
        public double Progress { get; set; }
        public bool Completed { get; set; }
        public bool Uploaded { get; set; }
        public int ExpectedCount { get; set; }
        public string Environment { get; set; }
        public string CompanyId { get; set; }
        public string TargetedDepartmentIds { get; set; }
        public string TargetedUserIds { get; set; }
        public string TargetedGroupIds { get; set; }
        public bool IsSpecificNotification { get; set; }
        public string Caption { get; set; }
        public int FeedType { get; set; }
        public bool IsForAdmin { get; set; }
        public bool IsVideoThumbnail { get; set; }
        public bool IsEdit { get; set; }
        public int Ordering { get; set; }
        public string Url { get; set; }
        public string SharedUrl { get; set; }
        public string UrlTitle { get; set; }
        public string UrlSiteName { get; set; }
        public string UrlDescription { get; set; }
        public bool Moved { get; set; }
    }
    public class FeedVideoProgress
    {
        // IMapper class
        public string FeedId { get; set; }
        public string PlaylistName { get; set; }
        public string UserId { get; set; }
        public double Progress { get; set; }
        public bool Completed { get; set; }
        public bool Uploaded { get; set; }
        public string CompanyId { get; set; }
        public string Environment { get; set; }
        public string TargetedDepartmentIds { get; set; }
        public string TargetedUserIds { get; set; }
        public string TargetedGroupIds { get; set; }
        public bool IsSpecificNotification { get; set; }
        public string Caption { get; set; }
        public int FeedType { get; set; }
        public bool IsForAdmin { get; set; }
        public string S3SrcKey { get; set; }
        public string S3OutputKeyPrefix { get; set; }
        public string VideoUrl { get; set; }

    }
}