﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [DataContract]
    public class PulseSingle : Pulse
    {
        #region Motivational
        [DataMember(EmitDefaultValue = false)]
        public bool IsAppliedFilter { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TextColor { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BackgroundUrl { get; set; }
        #endregion

        public PulseSelectBannerBackgroundResponse SelectBannerBackgroundUrls(string adminUserId, string companyId, ISession mainSession = null)
        {
            PulseSelectBannerBackgroundResponse response = new PulseSelectBannerBackgroundResponse();
            response.BackgroundUrls = new List<string>();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    ISession session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }


                response.BackgroundUrls.Add(DefaultResource.PulseBannerDefault1);
                response.BackgroundUrls.Add(DefaultResource.PulseBannerDefault2);
                response.BackgroundUrls.Add(DefaultResource.PulseBannerDefault3);
                response.BackgroundUrls.Add(DefaultResource.PulseBannerDefault4);
                response.BackgroundUrls.Add(DefaultResource.PulseBannerDefault5);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        private PulseSingle CreateDefaultBanner(ISession session = null)
        {
            PulseSingle defaultBannerPulse = null;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                defaultBannerPulse = new PulseSingle
                {
                    PulseId = DefaultResource.PulseDefaultBannerId,
                    Title = DefaultResource.PulseDefaultBannerWelcome,
                    IsAppliedFilter = true,
                    TextColor = "#FFFFFF",
                    BackgroundUrl = SelectBannerBackgroundUrls(null, null, session).BackgroundUrls[0],
                    Description = DefaultResource.PulseDefaultBannerWelcome,
                    Status = (int)PulseStatusEnum.Active,
                    StartDate = DateTime.UtcNow,
                    IsPrioritized = false,
                    Actions = SelectActionForPulse((int)PulseTypeEnum.Banner),
                    PulseType = (int)PulseTypeEnum.Banner,
                    IsForEveryone = true,
                    IsForDepartment = false,
                    TargetedDepartments = null,
                    IsForUser = false,
                    TargetedUsers = null,
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return defaultBannerPulse;
        }

        public PulseCreateResponse CreateBanner(string pulseId,
                                               string adminUserId,
                                               string companyId,
                                               string title,
                                               bool isFilterApplied,
                                               string textColor,
                                               string backgroundUrl,
                                               string description,
                                               int status,
                                               DateTime startDate,
                                               DateTime? endDate,
                                               bool isPrioritized,
                                               List<string> targetedDepartmentIds,
                                               List<string> targetedUserIds,
                                               ISession session = null)
        {
            PulseCreateResponse response = new PulseCreateResponse();
            response.Success = false;
            try
            {
                if(session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }
                

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Pulse title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.PulseTitleIsEmpty;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;

                if (endDate.HasValue && endDate.Value == DateTime.MinValue)
                {
                    endDate = null;
                }

                PulseValidationResponse dateResponse = ValidateDate(startDate, endDate, currentTime);
                if (!dateResponse.Success)
                {
                    Log.Error(dateResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                    response.ErrorMessage = dateResponse.ErrorMessage;
                    return response;
                }

                PulseValidationResponse statusResponse = ValidateStatus(status);
                if (!statusResponse.Success)
                {
                    Log.Error(statusResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(statusResponse.ErrorCode);
                    response.ErrorMessage = statusResponse.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(pulseId))
                {
                    pulseId = UUIDGenerator.GenerateUniqueIDForPulse();
                }

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                isPrioritized = false;

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_banner",
                    new List<string> { "id", "company_id", "title", "is_filter_applied", "text_color", "background_url", "description", "status", "start_timestamp", "end_timestamp", "is_prioritized", "created_by_admin_id",
                        "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp"}));
                batchStatement.Add(ps.Bind(pulseId, companyId, title, isFilterApplied, textColor, backgroundUrl, description, status, startDate, endDate, isPrioritized, adminUserId, currentTime, adminUserId, currentTime));

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_single_by_company",
                   new List<string> { "id", "company_id", "pulse_type", "title", "status", "is_prioritized", "start_timestamp", "end_timestamp" }));
                batchStatement.Add(ps.Bind(pulseId, companyId, (int)PulseTypeEnum.Banner, title, status, isPrioritized, startDate, endDate));

                List<BoundStatement> privacyStatement = CreatePrivacy(pulseId, targetedDepartmentIds, targetedUserIds, session);
                foreach (BoundStatement bs in privacyStatement)
                {
                    batchStatement.Add(bs);
                }

                // Sorting
                if (status == (int)PulseStatusEnum.Active)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("banner_pulse_by_timestamp",
                     new List<string> { "pulse_id", "company_id", "start_timestamp" }));
                    batchStatement.Add(ps.Bind(pulseId, companyId, startDate));
                }

                session.Execute(batchStatement);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse UpdateBanner(string pulseId,
                                                string adminUserId,
                                                string companyId,
                                                string newTitle,
                                                bool newIsFilterApplied,
                                                string newTextColor,
                                                string newBackgroundUrl,
                                                string newDescription,
                                                int newStatus,
                                                DateTime newStartDate,
                                                DateTime? newEndDate,
                                                bool newIsPrioritized,
                                                List<string> newTargetedDepartmentIds,
                                                List<string> newTargetedUserIds)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, session);
                if (pulseRow == null)
                {
                    Log.Error("Invalid pulse: " + pulseId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                int currentStatus = pulseRow.GetValue<int>("status");

                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = pulseRow.GetValue<DateTime>("start_timestamp");
                DateTime? currentEndDate = pulseRow.GetValue<DateTime?>("end_timestamp");

                int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);

                PulseValidationResponse validateResponse = ValidateForUpdate(currentStartDate, currentEndDate, newStartDate, newEndDate, currentTime, currentStatus, progress, pulseId);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Pulse title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.PulseTitleIsEmpty;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();

                // Get current backgroundUrl
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("pulse_banner",
                    new List<string>(), new List<string> { "company_id", "id" }));
                Row bannerRow = session.Execute(ps.Bind(companyId, pulseId)).FirstOrDefault();

                List<string> defaultUrls = SelectBannerBackgroundUrls(adminUserId, companyId, session).BackgroundUrls;
                string currentBackgroundUrl = bannerRow.GetValue<string>("background_url");
                if (defaultUrls.Count > 0 && !defaultUrls.Contains(currentBackgroundUrl) && !currentBackgroundUrl.Equals(newBackgroundUrl))
                {
                    DeleteFromS3(companyId, pulseId, newBackgroundUrl);
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_banner",
                    new List<string> { "company_id", "id" }, new List<string> { "title", "is_filter_applied", "text_color", "background_url", "description", "status", "start_timestamp", "end_timestamp", "is_prioritized", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newIsFilterApplied, newTextColor, newBackgroundUrl, newDescription, newStatus, newStartDate, newEndDate, newIsPrioritized, adminUserId, DateTime.UtcNow, companyId, pulseId));

                ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_single_by_company",
                    new List<string> { "company_id", "id" }, new List<string> { "title", "start_timestamp", "end_timestamp", "is_prioritized" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newStartDate, newEndDate, newIsPrioritized, companyId, pulseId));

                if (progress == (int)ProgressStatusEnum.Upcoming || currentStatus == (int)RSTopic.RSTopicStatus.CODE_UNLISTED)
                {
                    List<BoundStatement> privacyStatement = UpdatePrivacy(pulseId, newTargetedDepartmentIds, newTargetedUserIds, session);
                    foreach (BoundStatement bs in privacyStatement)
                    {
                        updateBatch.Add(bs);
                    }

                    if (newStartDate != currentStartDate && (int)currentStatus == (int)PulseStatusEnum.Active)
                    {
                        int type = pulseRow.GetValue<int>("pulse_type");
                        bool previousIsPrioritized = pulseRow.GetValue<bool>("is_prioritized");
                        UpdatePulsePriority(companyId, pulseId, type, 0, false, false, currentStartDate, newStartDate, session);
                    }
                }

                session.Execute(updateBatch);

                if (currentStatus != newStatus)
                {
                    UpdatePulseSingleStatus(adminUserId, companyId, pulseId, newStatus, pulseRow, session);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseCreateResponse CreateAnnouncement(string pulseId,
                                                      string adminUserId,
                                                      string companyId,
                                                      string title,
                                                      string description,
                                                      int status,
                                                      DateTime startDate,
                                                      DateTime? endDate,
                                                      bool isPrioritized,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds)
        {
            PulseCreateResponse response = new PulseCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Pulse title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.PulseTitleIsEmpty;
                    return response;
                }

                // description is not compulsory. 2017/02/10
                //if (string.IsNullOrEmpty(description))
                //{
                //    Log.Error("Pulse description is missing");
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseDescriptionIsEmpty);
                //    response.ErrorMessage = ErrorMessage.PulseDescriptionIsEmpty;
                //    return response;
                //}

                DateTime currentTime = DateTime.UtcNow;

                if (endDate.HasValue && endDate.Value == DateTime.MinValue)
                {
                    endDate = null;
                }

                PulseValidationResponse dateResponse = ValidateDate(startDate, endDate, currentTime);
                if (!dateResponse.Success)
                {
                    Log.Error(dateResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                    response.ErrorMessage = dateResponse.ErrorMessage;
                    return response;
                }

                PulseValidationResponse statusResponse = ValidateStatus(status);
                if (!statusResponse.Success)
                {
                    Log.Error(statusResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(statusResponse.ErrorCode);
                    response.ErrorMessage = statusResponse.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(pulseId))
                {
                    pulseId = UUIDGenerator.GenerateUniqueIDForPulse();
                }

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_announcement",
                    new List<string> { "id", "company_id", "title", "description", "status", "start_timestamp", "end_timestamp", "is_prioritized", "created_by_admin_id",
                        "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp"}));
                batchStatement.Add(ps.Bind(pulseId, companyId, title, description, status, startDate, endDate, isPrioritized, adminUserId, currentTime, adminUserId, currentTime));

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_single_by_company",
                   new List<string> { "id", "company_id", "pulse_type", "title", "status", "is_prioritized", "start_timestamp", "end_timestamp" }));
                batchStatement.Add(ps.Bind(pulseId, companyId, (int)PulseTypeEnum.Announcement, title, status, isPrioritized, startDate, endDate));

                List<BoundStatement> privacyStatement = CreatePrivacy(pulseId, targetedDepartmentIds, targetedUserIds, session);
                foreach (BoundStatement bs in privacyStatement)
                {
                    batchStatement.Add(bs);
                }

                // Sorting
                if (status == (int)PulseStatusEnum.Active)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp",
                     new List<string> { "pulse_id", "company_id", "pulse_priority", "pulse_type", "is_prioritized", "start_timestamp" }));
                    batchStatement.Add(ps.Bind(pulseId, companyId, (int)PulsePriorityEnum.Announcement, (int)PulseTypeEnum.Announcement, isPrioritized, startDate));

                    //Make a scheduled notification
                    new Notification().CreateScheduledAnnouncementNotification(companyId, targetedDepartmentIds, targetedUserIds, pulseId, startDate, session);
                }

                session.Execute(batchStatement);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse UpdateAnnouncement(string pulseId,
                                                      string adminUserId,
                                                      string companyId,
                                                      string newTitle,
                                                      string newDescription,
                                                      int newStatus,
                                                      DateTime newStartDate,
                                                      DateTime? newEndDate,
                                                      bool newIsPrioritized,
                                                      List<string> newTargetedDepartmentIds,
                                                      List<string> newTargetedUserIds)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, session);
                if (pulseRow == null)
                {
                    Log.Error("Invalid pulse: " + pulseId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                int currentStatus = pulseRow.GetValue<int>("status");

                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = pulseRow.GetValue<DateTime>("start_timestamp");
                DateTime? currentEndDate = pulseRow.GetValue<DateTime?>("end_timestamp");
                bool currentIsPrioritized = pulseRow.GetValue<bool>("is_prioritized");

                int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);

                PulseValidationResponse validateResponse = ValidateForUpdate(currentStartDate, currentEndDate, newStartDate, newEndDate, currentTime, currentStatus, progress, pulseId);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Pulse title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.PulseTitleIsEmpty;
                    return response;
                }

                // description is not compulsory. 2017/02/10
                //if (string.IsNullOrEmpty(newDescription))
                //{
                //    Log.Error("Pulse description is missing");
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseDescriptionIsEmpty);
                //    response.ErrorMessage = ErrorMessage.PulseDescriptionIsEmpty;
                //    return response;
                //}

                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_announcement",
                    new List<string> { "company_id", "id" }, new List<string> { "title", "description", "status", "start_timestamp", "end_timestamp", "is_prioritized", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newDescription, newStatus, newStartDate, newEndDate, newIsPrioritized, adminUserId, DateTime.UtcNow, companyId, pulseId));

                ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_single_by_company",
                    new List<string> { "company_id", "id" }, new List<string> { "title", "start_timestamp", "end_timestamp", "is_prioritized" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newStartDate, newEndDate, newIsPrioritized, companyId, pulseId));

                if (progress == (int)ProgressStatusEnum.Upcoming || currentStatus == (int)PulseStatusEnum.Unlisted)
                {
                    List<BoundStatement> privacyStatement = UpdatePrivacy(pulseId, newTargetedDepartmentIds, newTargetedUserIds, session);
                    foreach (BoundStatement bs in privacyStatement)
                    {
                        updateBatch.Add(bs);
                    }

                    if((newStartDate != currentStartDate || currentIsPrioritized != newIsPrioritized) && (int)currentStatus == (int)PulseStatusEnum.Active)
                    {
                        int type = pulseRow.GetValue<int>("pulse_type");
                        UpdatePulsePriority(companyId, pulseId, type, (int)PulsePriorityEnum.Announcement, currentIsPrioritized, newIsPrioritized, currentStartDate, newStartDate, session);

                        //Make a scheduled notification
                        new Notification().UpdateScheduledAnnouncementNotification(companyId, newTargetedDepartmentIds, newTargetedUserIds, pulseId, newStartDate, session);
                    }
                }

                session.Execute(updateBatch);

                if (currentStatus != newStatus)
                {
                    UpdatePulseSingleStatus(adminUserId, companyId, pulseId, newStatus, pulseRow, session);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectAllResponse SelectAllSingleBasic(string adminUserId, string companyId, int pulseType, DateTime? filteredStartDate = null, DateTime? filteredEndDate = null, string containsName = null, int filteredProgress = 0)
        {
            PulseSelectAllResponse response = new PulseSelectAllResponse();
            response.Pulses = new List<Pulse>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_single_by_company", new List<string>(), new List<string> { "company_id", "pulse_type" }));
                RowSet pulseRowSet = mainSession.Execute(ps.Bind(companyId, pulseType));

                if (filteredStartDate.HasValue)
                {
                    filteredStartDate = filteredStartDate.Value.ToUniversalTime();
                }

                if (filteredEndDate.HasValue)
                {
                    filteredEndDate = filteredEndDate.Value.ToUniversalTime();
                }

                DateTime currentTime = DateTime.UtcNow;
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                foreach (Row pulseRow in pulseRowSet)
                {
                    string title = pulseRow.GetValue<string>("title");
                    int status = pulseRow.GetValue<int>("status");
                    string pulseId = pulseRow.GetValue<string>("id");
                    int type = pulseRow.GetValue<int>("pulse_type");
                    DateTime startDate = pulseRow.GetValue<DateTime>("start_timestamp");
                    DateTime? endDate = pulseRow.GetValue<DateTime?>("end_timestamp");
                    int progress = SelectProgress(startDate, endDate, currentTime);

                    if (filteredProgress != 0 && filteredProgress != progress)
                    {
                        continue;
                    }

                    if (filteredStartDate.HasValue)
                    {
                        if (startDate < filteredStartDate)
                        {
                            continue;
                        }
                    }

                    if (filteredEndDate.HasValue)
                    {
                        if (endDate.HasValue)
                        {
                            if (endDate > filteredEndDate)
                            {
                                continue;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(containsName))
                    {
                        if (!title.Trim().ToLower().Contains(containsName.Trim().ToLower()))
                        {
                            continue;
                        }
                    }

                    Pulse selectedPulse = SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.Basic, timezoneOffset, false, false, type, progress, mainSession).Pulse;
                    if (selectedPulse != null)
                    {
                        response.Pulses.Add(selectedPulse);
                    }
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectResponse SelectFullDetailSinglePulse(string adminUserId, string companyId, string pulseId)
        {
            PulseSelectResponse response = new PulseSelectResponse();
            response.Pulse = new Pulse();
            response.Success = false;

            try
            {

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                response = SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, timezoneOffset, false, true, 0, 0, mainSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectResponse SelectSinglePulse(string pulseId,
                                                     string companyId,
                                                     int query,
                                                     double timezoneOffset,
                                                     bool isCheckForLive = false,
                                                     bool isPrivacyRequired = false,
                                                     int type = 0,
                                                     int progress = 0,
                                                     ISession mainSession = null)
        {
            PulseSelectResponse response = new PulseSelectResponse();
            response.Pulse = null;
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                Row pulseRow = null;

                if (type == 0)
                {
                    ValidationHandler vh = new ValidationHandler();
                    pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                    if (pulseRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }

                    type = pulseRow.GetValue<int>("pulse_type");
                }

                bool isForEveryone = false;
                bool isForDepartment = false;
                bool isForUser = false;

                List<Department> selectedDepartments = new List<Department>();
                List<User> selectedUsers = new List<User>();

                if (isPrivacyRequired)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_privacy", new List<string>(), new List<string> { "pulse_id" }));
                    Row privacyRow = mainSession.Execute(ps.Bind(pulseId)).FirstOrDefault();

                    isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                    isForDepartment = privacyRow.GetValue<bool>("is_for_department");
                    isForUser = privacyRow.GetValue<bool>("is_for_user");

                    if (!isForEveryone)
                    {
                        if (isForDepartment)
                        {
                            Department departmentManager = new Department();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_targeted_department", new List<string>(), new List<string> { "pulse_id" }));
                            RowSet departmentRowset = mainSession.Execute(ps.Bind(pulseId));
                            foreach (Row departmentRow in departmentRowset)
                            {
                                string departmentId = departmentRow.GetValue<string>("department_id");
                                Department department = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;
                                if (department != null)
                                {
                                    selectedDepartments.Add(department);
                                }
                            }
                        }

                        if (isForUser)
                        {
                            User userManager = new User();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_targeted_user", new List<string>(), new List<string> { "pulse_id" }));
                            RowSet userRowset = mainSession.Execute(ps.Bind(pulseId));
                            foreach (Row userRow in userRowset)
                            {
                                string userId = userRow.GetValue<string>("user_id");
                                User user = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                                if (user != null)
                                {
                                    selectedUsers.Add(user);
                                }
                            }
                        }
                    }
                }

                DateTime currentTime = DateTime.UtcNow;
                switch (type)
                {
                    case (int)PulseTypeEnum.Banner:
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_banner", new List<string>(), new List<string> { "company_id", "id" }));
                        pulseRow = mainSession.Execute(ps.Bind(companyId, pulseId)).FirstOrDefault();

                        if (pulseRow != null)
                        {
                            int status = pulseRow.GetValue<int>("status");

                            if (status == (int)PulseStatusEnum.Deleted)
                            {
                                Log.Error("Invalid pulse: " + pulseId);
                                response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                                response.ErrorMessage = ErrorMessage.PulseInvalid;
                                return response;
                            }

                            DateTime startDate = pulseRow.GetValue<DateTime>("start_timestamp");
                            DateTime? endDate = pulseRow.GetValue<DateTime?>("end_timestamp");
                            if (progress == 0)
                            {
                                progress = SelectProgress(startDate, endDate, currentTime);
                            }

                            if (isCheckForLive && progress != (int)ProgressStatusEnum.Live)
                            {
                                return response;
                            }

                            bool isFilterApplied = false;
                            string textColor = null;
                            string backgroundUrl = null;
                            string description = null;
                            bool isPrioritized = false;
                            string title = pulseRow.GetValue<string>("title");

                            if (query == (int)PulseQueryEnum.FullDetail)
                            {
                                isFilterApplied = pulseRow.GetValue<bool>("is_filter_applied");
                                textColor = pulseRow.GetValue<string>("text_color");
                                backgroundUrl = pulseRow.GetValue<string>("background_url");
                                description = pulseRow.GetValue<string>("description");
                                isPrioritized = pulseRow.GetValue<bool>("is_prioritized");
                            }

                            response.Pulse = new PulseSingle
                            {
                                PulseId = pulseId,
                                Title = title,
                                IsAppliedFilter = isFilterApplied,
                                TextColor = textColor,
                                BackgroundUrl = backgroundUrl,
                                Description = description,
                                Status = status,
                                StartDate = startDate,
                                StartDateString = startDate.AddHours(timezoneOffset).ToString("dd MMM yyyy"),
                                EndDate = endDate.HasValue ? endDate.Value : endDate,
                                EndDateString = endDate.HasValue ? endDate.Value.AddHours(timezoneOffset).ToString("dd MMM yyyy") : string.Empty,
                                IsPrioritized = isPrioritized,
                                ProgressStatus = progress,
                                Actions = SelectActionForPulse(type),
                                PulseType = type,
                                IsForEveryone = isForEveryone,
                                IsForDepartment = isForDepartment,
                                TargetedDepartments = selectedDepartments,
                                IsForUser = isForUser,
                                TargetedUsers = selectedUsers,
                            };
                            response.Success = true;
                        }
                        break;
                    case (int)PulseTypeEnum.Announcement:
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_announcement", new List<string>(), new List<string> { "company_id", "id" }));
                        pulseRow = mainSession.Execute(ps.Bind(companyId, pulseId)).FirstOrDefault();

                        if (pulseRow != null)
                        {
                            int status = pulseRow.GetValue<int>("status");

                            if (status == (int)PulseStatusEnum.Deleted)
                            {
                                Log.Error("Invalid pulse: " + pulseId);
                                response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                                response.ErrorMessage = ErrorMessage.PulseInvalid;
                                return response;
                            }

                            DateTime startDate = pulseRow.GetValue<DateTime>("start_timestamp");
                            DateTime? endDate = pulseRow.GetValue<DateTime?>("end_timestamp");
                            if (progress == 0)
                            {
                                progress = SelectProgress(startDate, endDate, currentTime);
                            }

                            if (isCheckForLive && progress != (int)ProgressStatusEnum.Live)
                            {
                                return response;
                            }

                            string description = null;
                            bool isPrioritized = false;
                            string title = pulseRow.GetValue<string>("title");

                            if (query == (int)PulseQueryEnum.FullDetail)
                            {
                                description = pulseRow.GetValue<string>("description");
                                isPrioritized = pulseRow.GetValue<bool>("is_prioritized");
                            }

                            response.Pulse = new PulseSingle
                            {
                                PulseId = pulseId,
                                Title = title,
                                Description = description,
                                Status = status,
                                StartDate = startDate,
                                StartDateString = startDate.AddHours(timezoneOffset).ToString("dd MMM yyyy"),
                                EndDate = endDate,
                                EndDateString = endDate.HasValue ? endDate.Value.AddHours(timezoneOffset).ToString("dd MMM yyyy") : string.Empty,
                                IsPrioritized = isPrioritized,
                                ProgressStatus = progress,
                                Actions = SelectActionForPulse(type),
                                PulseType = type,
                                IsForEveryone = isForEveryone,
                                IsForDepartment = isForDepartment,
                                TargetedDepartments = selectedDepartments,
                                IsForUser = isForUser,
                                TargetedUsers = selectedUsers,
                            };
                            response.Success = true;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse DeleteSinglePulse(string adminUserId, string companyId, string pulseId, Row pulseRow = null, ISession mainSession = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                    if (pulseRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }
                }


                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                bool isPrioritize = pulseRow.GetValue<bool>("is_prioritized");
                DateTime startDate = pulseRow.GetValue<DateTime>("start_timestamp");
                int type = pulseRow.GetValue<int>("pulse_type");
                int priority = SelectPulsePriority(type);

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_single_by_company", new List<string> { "company_id", "id" }));
                deleteBatch.Add(ps.Bind(companyId, pulseId));

                if (type != (int)PulseTypeEnum.Banner)
                {
                    // Delete from sorting
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(companyId, isPrioritize, priority, startDate, pulseId));

                    // Delete from inbox
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_inbox_by_pulse", new List<string>(), new List<string> { "pulse_id" }));
                    RowSet pulseInboxRowset = mainSession.Execute(ps.Bind(pulseId));

                    foreach (Row pulseInboxRow in pulseInboxRowset)
                    {
                        string userId = pulseInboxRow.GetValue<string>("user_id");
                        DateTime savedTimestamp = pulseInboxRow.GetValue<DateTime>("saved_on_timestamp");

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_inbox_by_user", new List<string> { "pulse_id", "user_id" }));
                        deleteBatch.Add(ps.Bind(pulseId, userId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_inbox_by_save_timestamp", new List<string> { "pulse_id", "user_id", "saved_on_timestamp" }));
                        deleteBatch.Add(ps.Bind(pulseId, userId, savedTimestamp));
                    }

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_inbox_by_pulse", new List<string> { "pulse_id" }));
                    deleteBatch.Add(ps.Bind(pulseId));

                    // Delete from trashbin
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_trashbin_by_pulse", new List<string>(), new List<string> { "pulse_id" }));
                    RowSet pulseTrashRowset = mainSession.Execute(ps.Bind(pulseId));

                    foreach (Row pulseInboxRow in pulseTrashRowset)
                    {
                        string userId = pulseInboxRow.GetValue<string>("user_id");
                        DateTime deletedTimestamp = pulseInboxRow.GetValue<DateTime>("deleted_on_timestamp");

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_user", new List<string> { "pulse_id", "user_id" }));
                        deleteBatch.Add(ps.Bind(pulseId, userId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_delete_timestamp", new List<string> { "pulse_id", "user_id", "deleted_on_timestamp" }));
                        deleteBatch.Add(ps.Bind(pulseId, userId, deletedTimestamp));
                    }

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_inbox_by_pulse", new List<string> { "pulse_id" }));
                    deleteBatch.Add(ps.Bind(pulseId));
                }
                else
                {
                    // Delete from S3
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_banner", new List<string>(), new List<string> { "company_id", "id" }));
                    Row bannerRow = mainSession.Execute(ps.Bind(companyId, pulseId)).FirstOrDefault();

                    if (bannerRow != null)
                    {
                        DeleteFromS3(companyId, pulseId);
                    }
                }

                switch (type)
                {
                    case (int)PulseTypeEnum.Banner:
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_banner", new List<string> { "company_id", "id" }, new List<string> { "last_modified_by_admin_id", "last_modified_timestamp", "status" }, new List<string>()));
                        updateBatch.Add(ps.Bind(adminUserId, DateTime.UtcNow, (int)PulseStatusEnum.Deleted, companyId, pulseId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("banner_pulse_by_timestamp", new List<string> { "company_id", "start_timestamp", "pulse_id" }));
                        deleteBatch.Add(ps.Bind(companyId, startDate, pulseId));
                        break;

                    case (int)PulseTypeEnum.Announcement:
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_announcement", new List<string> { "company_id", "id" }, new List<string> { "last_modified_by_admin_id", "last_modified_timestamp", "status" }, new List<string>()));
                        updateBatch.Add(ps.Bind(adminUserId, DateTime.UtcNow, (int)PulseStatusEnum.Deleted, companyId, pulseId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                        deleteBatch.Add(ps.Bind(companyId, isPrioritize, (int)PulsePriorityEnum.Announcement, startDate, pulseId));
                        break;
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_single_by_company", new List<string> { "company_id", "id" }));
                deleteBatch.Add(ps.Bind(companyId, pulseId));

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse UpdatePulseSingleStatus(string adminUserId, string companyId, string pulseId, int updatedStatus, Row pulseRow = null, ISession mainSession = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                    if (pulseRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }
                }

                int currentStatus = pulseRow.GetValue<int>("status");

                if (currentStatus == (int)PulseStatusEnum.Deleted)
                {
                    Log.Error("Pulse already been deleted");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.PulseAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.PulseAlreadyDeleted;
                    return response;
                }

                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                bool isPrioritized = pulseRow.GetValue<bool>("is_prioritized");
                DateTime startTime = pulseRow.GetValue<DateTime>("start_timestamp");
                int type = pulseRow.GetValue<int>("pulse_type");
                int priority = SelectPulsePriority(type);

                if (updatedStatus == (int)PulseStatusEnum.Deleted)
                {
                    response = DeleteSinglePulse(adminUserId, companyId, pulseId, pulseRow, mainSession);
                    if (!response.Success)
                    {
                        return response;
                    }
                }
                else
                {
                    if (updatedStatus == (int)PulseStatusEnum.Hidden)
                    {
                        if (type != (int)PulseTypeEnum.Banner)
                        {
                            ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                            deleteBatch.Add(ps.Bind(companyId, isPrioritized, priority, startTime, pulseId));
                        }
                        else
                        {
                            ps = mainSession.Prepare(CQLGenerator.DeleteStatement("banner_pulse_by_timestamp", new List<string> { "company_id", "start_timestamp", "pulse_id" }));
                            deleteBatch.Add(ps.Bind(companyId, startTime, pulseId));
                        }

                    }
                    else if (updatedStatus == (int)PulseStatusEnum.Active)
                    {
                        // Active
                        if (type != (int)PulseTypeEnum.Banner)
                        {
                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id", "pulse_type" }));
                            updateBatch.Add(ps.Bind(companyId, isPrioritized, priority, startTime, pulseId, type));
                        }
                        else
                        {
                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("banner_pulse_by_timestamp", new List<string> { "company_id", "start_timestamp", "pulse_id" }));
                            updateBatch.Add(ps.Bind(companyId, startTime, pulseId));
                        }
                    }

                    // Update pulse_single_by_company
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_single_by_company", new List<string> { "company_id", "id" }, new List<string> { "status" }, new List<string>()));
                    deleteBatch.Add(ps.Bind(updatedStatus, companyId, pulseId));
                }

                switch (type)
                {
                    case (int)PulseTypeEnum.Banner:
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_banner", new List<string> { "company_id", "id" }, new List<string> { "last_modified_by_admin_id", "last_modified_timestamp", "status" }, new List<string>()));
                        updateBatch.Add(ps.Bind(adminUserId, DateTime.UtcNow, updatedStatus, companyId, pulseId));
                        break;

                    case (int)PulseTypeEnum.Announcement:
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_announcement", new List<string> { "company_id", "id" }, new List<string> { "last_modified_by_admin_id", "last_modified_timestamp", "status" }, new List<string>()));
                        updateBatch.Add(ps.Bind(adminUserId, DateTime.UtcNow, updatedStatus, companyId, pulseId));
                        break;
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectSingleAnalyticResponse SelectAnalyticForSinglePulse(string adminUserId, string companyId, string pulseId)
        {
            PulseSelectSingleAnalyticResponse response = new PulseSelectSingleAnalyticResponse();
            response.Pulse = new Pulse();
            response.LeftChart = new Chart();
            response.RightChart = new Chart();
            response.PersonnelList = new AnalyticPulse.PersonnelList();
            response.PersonnelList.Users = new List<AnalyticPulse.PulseUserAnalytic>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                if (pulseRow == null)
                {
                    Log.Error("Invalid pulse: " + pulseId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                int type = pulseRow.GetValue<int>("pulse_type");
                PulseSelectPrivacyResponse privacyResponse = SelectPulsePrivacy(pulseId, mainSession);

                List<User> targetedAudience = SelectPulseTargetedAudience(pulseId, companyId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, mainSession);

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                Pulse pulse = SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.Basic, timezoneOffset, true, false, type, 0, mainSession).Pulse;
                response = new AnalyticPulse().SelectAnalyticForSinglePulse(pulse, type, targetedAudience, timezoneOffset, mainSession, analyticSession);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public List<Pulse> SelectBannerPulses(string requesterUserId, string companyId, List<string>departmentIds, double timezoneOffset, int limit, ISession mainSession)
        {
            List<Pulse> bannerPulses = new List<Pulse>();
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("banner_pulse_by_timestamp", new List<string>(), new List<string> { "company_id" }));
                RowSet pulseRowSet = mainSession.Execute(ps.Bind(companyId));

                PulseSelectPrivacyResponse privacyResponse;

                foreach (Row pulseRow in pulseRowSet)
                {
                    string pulseId = pulseRow.GetValue<string>("pulse_id");

                    privacyResponse = SelectPulsePrivacy(pulseId, mainSession);

                    if(!CheckPulseForCurrentUser(requesterUserId, pulseId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, departmentIds, mainSession))
                    {
                        continue;
                    }

                    Pulse pulse = null;
                    pulse = SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, timezoneOffset, true, false, (int)PulseTypeEnum.Banner, 0, mainSession).Pulse;

                    if (pulse != null)
                    {
                        if (bannerPulses.Count >= limit)
                        {
                            break;
                        }

                        bannerPulses.Add(pulse);
                    }
                }

                if(bannerPulses.Count == 0)
                {
                    bannerPulses.Add(CreateDefaultBanner(mainSession));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return bannerPulses;
        }

        public PulseUpdateSingleStatsResponse UpdateSingleAction(string requesterUserId, string companyId, string pulseId, int action)
        {
            PulseUpdateSingleStatsResponse response = new PulseUpdateSingleStatsResponse();
            response.Success = false;
            try
            {
                if (!pulseId.Equals(DefaultResource.PulseDefaultBannerId))
                {
                    ConnectionManager cm = new ConnectionManager();
                    ISession mainSession = cm.getMainSession();
                    ISession analyticSession = cm.getAnalyticSession();

                    ValidationHandler vh = new ValidationHandler();

                    Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                    if (userRow == null)
                    {
                        Log.Error("Invalid user: " + requesterUserId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }

                    Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                    if (pulseRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Int16.Parse(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                    }

                    int type = pulseRow.GetValue<int>("pulse_type");
                    if (!CheckForSingleCorrectAction(pulseId, type, action, mainSession))
                    {
                        Log.Error("Invalid action: " + action);
                    }

                    Thread updateThread = new Thread(() => UpdateSingleActionThread(requesterUserId, companyId, pulseId, action, type, mainSession, analyticSession));
                    updateThread.Start();
                }
               
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void UpdateSingleActionThread(string requesterUserId, string companyId, string pulseId, int action, int type, ISession mainSession, ISession analyticSession)
        {
            try
            {
                new AnalyticPulse().UpdateSingleAction(pulseId, requesterUserId, action, type, analyticSession);

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();

                // Inbox or trash bin
                if (action == (int)Pulse.PulseActionEnum.SaveToInbox)
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_inbox_by_user", new List<string> { "user_id", "pulse_id", "pulse_type", "saved_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, pulseId, type, DateTime.UtcNow));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_inbox_by_pulse", new List<string> { "user_id", "pulse_id", "pulse_type", "saved_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, pulseId, type, DateTime.UtcNow));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_inbox_by_save_timestamp", new List<string> { "user_id", "pulse_id", "pulse_type", "saved_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, pulseId, type, DateTime.UtcNow));
                }
                else if (action == (int)Pulse.PulseActionEnum.Delete)
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_trashbin_by_user", new List<string> { "user_id", "pulse_id", "pulse_type", "deleted_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, pulseId, type, DateTime.UtcNow));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_trashbin_by_pulse", new List<string> { "user_id", "pulse_id", "pulse_type", "deleted_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, pulseId, type, DateTime.UtcNow));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_trashbin_by_delete_timestamp", new List<string> { "user_id", "pulse_id", "pulse_type", "deleted_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, pulseId, type, DateTime.UtcNow));
                }

                mainSession.Execute(updateBatch);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void UpdatePulsePriority(string companyId, string pulseId, int type, int priority, bool previousIsPrioritized, bool newIsPrioritized, DateTime previousStartDate, DateTime newStartDate, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                if (type != (int)PulseTypeEnum.Banner)
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(companyId, previousIsPrioritized, priority, previousStartDate, pulseId));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("banner_pulse_by_timestamp", new List<string> { "company_id", "start_timestamp", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(companyId, previousStartDate, pulseId));
                }

                if (type != (int)PulseTypeEnum.Banner)
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id", "pulse_type" }));
                    updateBatch.Add(ps.Bind(companyId, newIsPrioritized, priority, newStartDate, pulseId, type));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("banner_pulse_by_timestamp", new List<string> { "company_id", "start_timestamp", "pulse_id" }));
                    updateBatch.Add(ps.Bind(companyId, newStartDate, pulseId));
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}