﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System.Linq;
using System.Web.Configuration;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    public class Brain
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

#warning Deprecated code in SelectBrain, replaced by SelectBrainy
        public BrainSelectResponse SelectBrain(string requesterUserId, string companyId)
        {
            BrainSelectResponse response = new BrainSelectResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                AnalyticQuiz analytic = new AnalyticQuiz();
                AnalyticQuiz.Exp currentUserExp = analytic.SelectLevelOfUser(requesterUserId, analyticSession);

                string rank = "Unranked";

                PreparedStatement psLeaderboardByCompany = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_company_sorted",
                        new List<string>(), new List<string> { "company_id" }));
                BoundStatement bsLeaderboardByCompany = psLeaderboardByCompany.Bind(companyId);
                RowSet leaderboardByCompanyRowset = mainSession.Execute(bsLeaderboardByCompany);

                int index = 1;
                foreach (Row leaderboardByCompanyRow in leaderboardByCompanyRowset)
                {
                    string userId = leaderboardByCompanyRow.GetValue<string>("user_id");
                    if (userId.Equals(requesterUserId))
                    {
                        rank = index.ToString();
                    }
                    index++;
                }

                currentUserExp.Rank = rank;

                List<Topic> recentlyPlayedTopics = new List<Topic>();

                bool isCompleted = false;
                DateTimeOffset? startDate = null;
                int limit = Convert.ToInt16(WebConfigurationManager.AppSettings["topic_recently_limit"]);

                Topic topicManager = new Topic();

                while (!isCompleted)
                {
                    PreparedStatement psRecentCompletedTopic = null;
                    RowSet recentCompletedTopicRowset = null;
                    if (startDate == null)
                    {
                        psRecentCompletedTopic = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_topic_desc",
                            new List<string>(), new List<string> { "user_id" }));
                        recentCompletedTopicRowset = mainSession.Execute(psRecentCompletedTopic.Bind(requesterUserId));
                    }
                    else
                    {
                        psRecentCompletedTopic = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_history_by_topic_desc",
                           new List<string>(), new List<string> { "user_id" }, "completed_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        recentCompletedTopicRowset = mainSession.Execute(psRecentCompletedTopic.Bind(requesterUserId, startDate));
                    }

                    List<Row> recentCompletedTopicRowList = new List<Row>();

                    if (recentCompletedTopicRowset != null)
                    {
                        recentCompletedTopicRowList = recentCompletedTopicRowset.GetRows().ToList();

                        foreach (Row recentCompletedTopicRow in recentCompletedTopicRowList)
                        {
                            string topicId = recentCompletedTopicRow.GetValue<string>("topic_id");

                            DateTimeOffset completedTimestamp = recentCompletedTopicRow.GetValue<DateTimeOffset>("completed_on_timestamp");
                            startDate = completedTimestamp;

                            if (topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, mainSession))
                            {
                                if (recentlyPlayedTopics.FindIndex(topic => topic.TopicId.Equals(topicId)) < 0 && recentlyPlayedTopics.Count < limit)
                                {
                                    Topic topic = new Topic().SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, true).Topic;
                                    if (topic != null)
                                    {
                                        recentlyPlayedTopics.Add(topic);
                                        if (recentlyPlayedTopics.Count == limit)
                                        {
                                            isCompleted = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                    if (recentCompletedTopicRowList.Count == 0)
                    {
                        isCompleted = true;
                    }
                }


                List<User> recentlyPlayedOpponents = new List<User>();

                PreparedStatement psRecentOpponent = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_user_desc",
                       new List<string>(), new List<string> { "user_id" }));
                BoundStatement bsRecentOpponent = psRecentOpponent.Bind(requesterUserId);
                RowSet recentOpponentRowset = mainSession.Execute(bsRecentOpponent);

                int recentlyPlayedLimit = Convert.ToInt16(WebConfigurationManager.AppSettings["brain_recently_played_limit"]);
                foreach (Row recentOpponentRow in recentOpponentRowset)
                {
                    string userId = recentOpponentRow.GetValue<string>("opponent_user_id");

                    if (recentlyPlayedOpponents.FindIndex(user => user.UserId.Equals(userId)) < 0 && recentlyPlayedOpponents.Count < recentlyPlayedLimit)
                    {
                        User user = new User().SelectUserBasic(userId, companyId, false, mainSession).User;
                        if (user != null)
                        {
                            recentlyPlayedOpponents.Add(user);
                        }
                    }
                }


                List<Challenge> challenges = SelectIncompleteChallengesForUser(requesterUserId, companyId, Convert.ToInt16(WebConfigurationManager.AppSettings["brain_challenges_limit"]), mainSession).Challenges;

                List<Event> events = new List<Event>();
                Event eventManager = new Event();
                events = eventManager.SelectAllByUser(requesterUserId, companyId, mainSession).Events;

                List<RSTopicCategory> surveys = new List<RSTopicCategory>();
                RSTopic rsTopicManager = new RSTopic();
                surveys = rsTopicManager.SelectAllBasicByUser(requesterUserId, companyId, true, mainSession, analyticSession).RSCategories;

                response.ChallengeCount = challenges.Count();
                response.LatestChallenges = challenges;
                response.RecentlyPlayedTopics = recentlyPlayedTopics;
                response.RecentOpponents = recentlyPlayedOpponents;
                response.UserExp = currentUserExp;
                response.Events = events;
                response.Surveys = surveys;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public BrainSelectResponse SelectBrainy(string requesterUserId, string companyId)
        {
            BrainSelectResponse response = new BrainSelectResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                SelectBrainAsync(response, requesterUserId, companyId, mainSession, analyticSession);
                //SelectBrain(response, requesterUserId, companyId, mainSession, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void SelectBrainAsync(BrainSelectResponse response, string requesterUserId, string companyId, ISession mainSession, ISession analyticSession)
        {
            //Log.Debug("Main Start GetRecentlyPlayedTopicsAsync");
            Task<List<Topic>> recentlyPlayedTopics = Task.Run(() => GetRecentlyPlayedTopics(requesterUserId, companyId, mainSession));
            //Log.Debug("Main Start GetRecentlyPlayedOpponentsAsync");
            Task<List<User>> recentlyPlayedOpponentsTask = Task.Run(() => GetRecentlyPlayedOpponents(requesterUserId, companyId, mainSession));
            //Log.Debug("Main Start GetChallengesAsync");
            Task<Dictionary<string, object>> challengesDictTask = Task.Run(() => GetChallenges(requesterUserId, companyId, mainSession));
            //Log.Debug("Main Start GetEventsAsync");
            Task<List<Event>> eventsTask = Task.Run(() => GetEvents(requesterUserId, companyId, mainSession));
            //Log.Debug("Main Start GetSurveysAsync");
            Task<List<RSTopicCategory>> surveyTask = Task.Run(() => GetSurveys(requesterUserId, companyId, mainSession, analyticSession));
            //Log.Debug("Main Start GetMLTopicsAsync");
            Task<List<MLTopicCategory>> mLearningsTask = Task.Run(() => GetMLTopics(requesterUserId, companyId, mainSession, analyticSession));
            //Log.Debug("Main Start GetAssessmentsAsync");
            Task<List<Assessment>> assessmentTask = Task.Run(() => GetAssessments(requesterUserId, companyId, mainSession, analyticSession));
            //Log.Debug("Main Start GetEducationsAsync");
            Task<List<MLEducationCategory>> mlEducationTask = Task.Run(() => GetEducations(requesterUserId, companyId, mainSession));

            List<User> recentlyPlayedOpponents = recentlyPlayedOpponentsTask.Result;
            Dictionary<string, object> challengesDict = challengesDictTask.Result;
            List<Event> events = eventsTask.Result;
            List<RSTopicCategory> surveys = surveyTask.Result;
            List<MLTopicCategory> mLearnings = mLearningsTask.Result;
            List<Assessment> assessments = assessmentTask.Result;
            List<MLEducationCategory> educations = mlEducationTask.Result;

            response.RecentlyPlayedTopics = recentlyPlayedTopics.Result;
            response.ChallengeCount = (int)challengesDict["IncompleteCount"];
            response.LatestChallenges = (List<Challenge>)challengesDict["Challenges"];
            response.RecentOpponents = recentlyPlayedOpponents;
            response.Events = events;
            response.Surveys = surveys;
            response.MLearnings = mLearnings;
            response.Assessments = assessments;
            response.Educations = educations;

            response.Success = true;
        }

        public ChallengeSelectIncompleteResponse SelectIncompleteChallengesForUser(string requesterUserId, string companyId, int limit = 0, ISession mainSession = null)
        {
            ChallengeSelectIncompleteResponse response = new ChallengeSelectIncompleteResponse();
            response.Challenges = new List<Challenge>();
            response.TotalChallengesCount = 0;
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);
                    if (es != null)
                    {
                        Log.Error("Invalid userId: " + requesterUserId);
                        return response;
                    }
                }

                int numberOfChallenges = 0;
                Topic topicManager = new Topic();

                PreparedStatement psRecentChallenge = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_initiated_timestamp",
                        new List<string>(), new List<string> { "challenged_user_id" }));
                BoundStatement bsRecentChallenge = psRecentChallenge.Bind(requesterUserId);
                List<Row> recentChallengeRowList = mainSession.Execute(bsRecentChallenge).ToList();

                int brainChallengesLimit = limit;

                foreach (Row recentChallengeRow in recentChallengeRowList)
                {
                    string challengeId = recentChallengeRow.GetValue<string>("challenge_id");
                    string initiatorUserId = recentChallengeRow.GetValue<string>("initiated_user_id");
                    string challengeTopicId = recentChallengeRow.GetValue<string>("topic_id");
                    string challengeTopicCategoryId = recentChallengeRow.GetValue<string>("topic_category_id");
                    DateTimeOffset initiatedOnTimestamp = recentChallengeRow.GetValue<DateTimeOffset>("initiated_on_timestamp");

                    // Resolve issue where initiator = challenged user
                    if (initiatorUserId.Equals(requesterUserId))
                    {
                        Log.Error("Found challenger = initiator in recent challenge: " + challengeId);
                        topicManager.InvalidateChallenge(requesterUserId, companyId, challengeId, (int)Topic.InvalidateChallengeReason.InvalidChallenge, null, mainSession, false);
                        continue;
                    }

                    Row challengeRow = vh.ValidateChallenge(challengeId, companyId, mainSession);

                    if (challengeRow == null)
                    {
                        Log.Error("Invalid challengeId for brain: " + challengeId);
                        continue;
                    }

                    User initiatedUser = new User().SelectUserBasic(initiatorUserId, companyId, false, mainSession).User;

                    if (initiatedUser != null)
                    {
                        if (topicManager.CheckPrivacyForTopic(requesterUserId, companyId, challengeTopicId, mainSession))
                        {
                            Topic challengeTopic = new Topic().SelectTopicBasic(challengeTopicId, null, companyId, challengeTopicCategoryId, null, mainSession).Topic;

                            if (challengeTopic != null)
                            {
                                numberOfChallenges++;

                                Challenge latestChallenge = new Challenge
                                {
                                    Topic = challengeTopic,
                                    InitiatedUser = initiatedUser,
                                    ChallengeId = challengeId
                                };

                                if (brainChallengesLimit > 0)
                                {
                                    if (response.Challenges.Count < brainChallengesLimit)
                                    {
                                        response.Challenges.Add(latestChallenge);
                                    }
                                }
                                else
                                {
                                    response.Challenges.Add(latestChallenge);
                                }
                            }
                            else
                            {
                                psRecentChallenge = mainSession.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_initiated_timestamp",
                                                     new List<string> { "challenged_user_id", "initiated_on_timestamp", "challenge_id" }));
                                mainSession.Execute(psRecentChallenge.Bind(requesterUserId, initiatedOnTimestamp, challengeId));
                            }
                        }

                    }

                }
                response.TotalChallengesCount = recentChallengeRowList.Count();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void SelectBrain(BrainSelectResponse response, string requesterUserId, string companyId, ISession mainSession, ISession analyticSession)
        {
            List<Topic> recentlyPlayedTopics = GetRecentlyPlayedTopics(requesterUserId, companyId, mainSession);
            List<User> recentlyPlayedOpponents = GetRecentlyPlayedOpponents(requesterUserId, companyId, mainSession);
            Dictionary<string, object> challengesDict = GetChallenges(requesterUserId, companyId, mainSession);
            List<Event> events = GetEvents(requesterUserId, companyId, mainSession);
            List<RSTopicCategory> surveys = GetSurveys(requesterUserId, companyId, mainSession, analyticSession);
            List<MLTopicCategory> mLearnings = GetMLTopics(requesterUserId, companyId, mainSession, analyticSession);
            List<Assessment> assessments = GetAssessments(requesterUserId, companyId, mainSession, analyticSession);

            response.ChallengeCount = (int)challengesDict["IncompleteCount"];
            response.LatestChallenges = (List<Challenge>)challengesDict["Challenges"];
            response.RecentlyPlayedTopics = recentlyPlayedTopics;
            response.RecentOpponents = recentlyPlayedOpponents;
            response.Events = events;
            response.Surveys = surveys;
            response.MLearnings = mLearnings;
            response.Assessments = assessments;

            response.Success = true;
        }

        private List<Topic> GetRecentlyPlayedTopics(string requesterUserId, string companyId, ISession mainSession)
        {
            //Log.Debug("Start GetRecentlyPlayedTopics");
            List<Topic> recentlyPlayedTopics = new List<Topic>();

            try
            {
                bool isCompleted = false;
                DateTimeOffset? startDate = null;
                int limit = Convert.ToInt16(WebConfigurationManager.AppSettings["topic_recently_limit"]);

                Topic topicManager = new Topic();

                while (!isCompleted)
                {
                    PreparedStatement psRecentCompletedTopic = null;
                    RowSet recentCompletedTopicRowset = null;
                    if (startDate == null)
                    {
                        psRecentCompletedTopic = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_topic_desc",
                            new List<string>(), new List<string> { "user_id" }));
                        recentCompletedTopicRowset = mainSession.Execute(psRecentCompletedTopic.Bind(requesterUserId));
                    }
                    else
                    {
                        psRecentCompletedTopic = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_history_by_topic_desc",
                           new List<string>(), new List<string> { "user_id" }, "completed_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        recentCompletedTopicRowset = mainSession.Execute(psRecentCompletedTopic.Bind(requesterUserId, startDate));
                    }

                    List<Row> recentCompletedTopicRowList = new List<Row>();

                    if (recentCompletedTopicRowset != null)
                    {
                        recentCompletedTopicRowList = recentCompletedTopicRowset.GetRows().ToList();

                        foreach (Row recentCompletedTopicRow in recentCompletedTopicRowList)
                        {
                            string topicId = recentCompletedTopicRow.GetValue<string>("topic_id");

                            DateTimeOffset completedTimestamp = recentCompletedTopicRow.GetValue<DateTimeOffset>("completed_on_timestamp");
                            startDate = completedTimestamp;

                            if (topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, mainSession))
                            {
                                if (recentlyPlayedTopics.FindIndex(topic => topic.TopicId.Equals(topicId)) < 0 && recentlyPlayedTopics.Count < limit)
                                {
                                    Topic topic = new Topic().SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, true).Topic;
                                    if (topic != null)
                                    {
                                        recentlyPlayedTopics.Add(topic);
                                        if (recentlyPlayedTopics.Count == limit)
                                        {
                                            isCompleted = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                    if (recentCompletedTopicRowList.Count == 0)
                    {
                        isCompleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetRecentlyPlayedTopics");
            return recentlyPlayedTopics;
        }

        private List<User> GetRecentlyPlayedOpponents(string requesterUserId, string companyId, ISession mainSession)
        {
            //Log.Debug("Start GetRecentlyPlayedOpponents");
            List<User> recentlyPlayedOpponents = new List<User>();
            try
            {
                PreparedStatement psRecentOpponent = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_user_desc",
                    new List<string>(), new List<string> { "user_id" }));
                BoundStatement bsRecentOpponent = psRecentOpponent.Bind(requesterUserId);
                List<Row> recentOpponentRowset = mainSession.Execute(bsRecentOpponent).ToList();

                int recentlyPlayedLimit = Convert.ToInt16(WebConfigurationManager.AppSettings["brain_recently_played_limit"]);
                foreach (Row recentOpponentRow in recentOpponentRowset)
                {
                    string userId = recentOpponentRow.GetValue<string>("opponent_user_id");

                    if (userId.Equals(requesterUserId))
                    {
                        Log.Error("Found challenger = initiator in recently played opponent");
                        Log.Error("RequesterUserId: " + requesterUserId);
                        RemoveChallengerEqualInitiator(requesterUserId, companyId, recentOpponentRow, mainSession);
                        continue;
                    }

                    if (recentlyPlayedOpponents.FindIndex(user => user.UserId.Equals(userId)) < 0 && recentlyPlayedOpponents.Count < recentlyPlayedLimit)
                    {
                        User user = new User().SelectUserBasic(userId, companyId, false, mainSession).User;
                        if (user != null)
                        {
                            recentlyPlayedOpponents.Add(user);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetRecentlyPlayedOpponents");
            return recentlyPlayedOpponents;
        }

        private Dictionary<string, object> GetChallenges(string requesterUserId, string companyId, ISession mainSession)
        {
            //Log.Debug("Start GetChallenges");
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                List<Challenge> challenges = new List<Challenge>();
                int totalIncompleteChallengeCount = 0;
                ChallengeSelectIncompleteResponse incompletedResponse = SelectIncompleteChallengesForUser(requesterUserId, companyId, Convert.ToInt16(WebConfigurationManager.AppSettings["brain_challenges_limit"]), mainSession);
                if (incompletedResponse.Success)
                {
                    challenges = incompletedResponse.Challenges;
                    totalIncompleteChallengeCount = incompletedResponse.TotalChallengesCount;

                    dict.Add("Challenges", incompletedResponse.Challenges);
                    dict.Add("IncompleteCount", incompletedResponse.TotalChallengesCount);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetChallenges");
            return dict;
        }

        private List<Event> GetEvents(string requesterUserId, string companyId, ISession mainSession)
        {
            //Log.Debug("Start GetEvents");
            List<Event> events = new List<Event>();

            try
            {
                Event eventManager = new Event();
                events = eventManager.SelectAllByUser(requesterUserId, companyId, mainSession).Events;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetEvents");
            return events;
        }

        private List<RSTopicCategory> GetSurveys(string requesterUserId, string companyId, ISession mainSession, ISession analyticSession)
        {
            //Log.Debug("Start GetSurveys");
            List<RSTopicCategory> surveys = new List<RSTopicCategory>();
            try
            {
                surveys = new RSTopic().SelectAllBasicByUser(requesterUserId, companyId, true, mainSession, analyticSession).RSCategories;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetSurveys");
            return surveys;
        }

        private List<MLTopicCategory> GetMLTopics(string requesterUserId, string companyId, ISession mainSession, ISession analyticSession)
        {
            //Log.Debug("Start GetMLTopics");
            List<MLTopicCategory> mLearnings = new List<MLTopicCategory>();

            try
            {
                mLearnings = new MLTopic().SelectAllBasicByUser(requesterUserId, companyId, true, mainSession, analyticSession).MLCategories;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetMLTopics");
            return mLearnings;
        }

        private List<Assessment> GetAssessments(string requesterUserId, string companyId, ISession mainSession, ISession analyticSession)
        {
            //Log.Debug("Start GetAssessments");
            List<Assessment> assessments = new List<Assessment>();

            try
            {
                Assessment assessmentManager = new Assessment();
                assessments = assessmentManager.SelectAllAssessmentByUser(requesterUserId, companyId, mainSession, analyticSession).Assessments;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetAssessments");
            return assessments;
        }

        private List<MLEducationCategory> GetEducations(string requesterUserId, string companyId, ISession mainSession)
        {
            //Log.Debug("Start GetEducations");
            List<MLEducationCategory> educations = new List<MLEducationCategory>();

            try
            {
                MLEducationCategory categoryManager = new MLEducationCategory();
                educations = categoryManager.GetListByUser(requesterUserId, companyId, null, mainSession).Categories;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            //Log.Debug("End GetEducations");
            return educations;
        }

        public void RemoveChallengerEqualInitiator(string requesterUserId, string companyId, Row recentlyPlayedHistoryRow, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                Log.Debug("RemoveChallengerEqualInitiator");

                if (requesterUserId == null)
                {
                    Log.Debug("RequesterUserId is null");
                }

                string opponentId = recentlyPlayedHistoryRow.GetValue<string>("opponent_user_id");
                if (opponentId.Equals(requesterUserId))
                {
                    DateTime completedOnTimestamp = recentlyPlayedHistoryRow.GetValue<DateTime>("completed_on_timestamp");
                    string challengeId = recentlyPlayedHistoryRow.GetValue<string>("challenge_id");

                    new Topic().InvalidateChallenge(requesterUserId, companyId, challengeId, (int)Topic.InvalidateChallengeReason.InvalidChallenge, null, mainSession, false);

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_user_desc", new List<string> { "user_id", "completed_on_timestamp" }));
                    batch.Add(ps.Bind(requesterUserId, completedOnTimestamp));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_user_asc", new List<string> { "user_id", "completed_on_timestamp" }));
                    batch.Add(ps.Bind(requesterUserId, completedOnTimestamp));
                }

                mainSession.Execute(batch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }

    [Serializable]
    public class Challenge
    {
        [DataMember]
        public Topic Topic { get; set; }

        [DataMember]
        public User InitiatedUser { get; set; }

        [DataMember]
        public string ChallengeId { get; set; }
    }
}

