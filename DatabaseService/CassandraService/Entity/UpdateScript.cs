﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Threading;

namespace CassandraService.Entity
{
    public class UpdateScript
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public UpdateScriptResponse InsertValidStatusColumnToFeed(string adminUserId, string companyId)
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = null;
                BoundStatement bs = null;

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Feed Privacy
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedCompanyId = row.GetValue<string>("company_id");
                    string feedId = row.GetValue<string>("feed_id");
                    bool isValid = row.GetValue<bool>("is_feed_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy", new List<string> { "company_id", "feed_id" }, new List<string> { "feed_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedCompanyId, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Privacy Desc
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_desc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedCompanyId = row.GetValue<string>("company_id");
                    string feedId = row.GetValue<string>("feed_id");
                    bool isValid = row.GetValue<bool>("is_feed_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc", new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "feed_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedCompanyId, createdTimestamp, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Privacy Asc
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_asc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedCompanyId = row.GetValue<string>("company_id");
                    string feedId = row.GetValue<string>("feed_id");
                    bool isValid = row.GetValue<bool>("is_feed_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc", new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "feed_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedCompanyId, createdTimestamp, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Text
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_text", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_text", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Image
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_image", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_image", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Video
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_video", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_video", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Shared Url
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_shared_url", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_shared_url", new List<string> { "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Comment
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_comment_text", new List<string> { "feed_id", "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Comment by feed
                ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    bool isValid = row.GetValue<bool>("is_comment_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed", new List<string> { "feed_id", "comment_id" }, new List<string> { "comment_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Comment by feed asc
                ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    bool isValid = row.GetValue<bool>("is_comment_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_asc", new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "comment_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, createdTimestamp, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Comment by feed desc
                ps = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_desc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    bool isValid = row.GetValue<bool>("is_comment_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("comment_by_feed_timestamp_desc", new List<string> { "feed_id", "created_on_timestamp", "comment_id" }, new List<string> { "comment_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, feedId, createdTimestamp, commentId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Feed Reply
                ps = session.Prepare(CQLGenerator.SelectStatement("feed_reply_text", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("id");
                    bool isValid = row.GetValue<bool>("is_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("feed_reply_text", new List<string> { "comment_id", "id" }, new List<string> { "valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Reply by comment
                ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    bool isValid = row.GetValue<bool>("is_reply_valid");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment", new List<string> { "comment_id", "reply_id" }, new List<string> { "reply_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Reply by comment desc
                ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_desc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    bool isValid = row.GetValue<bool>("is_reply_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_desc", new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "reply_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, createdTimestamp, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Reply by comment asc
                ps = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc", new List<string>(), new List<string>()));
                rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    bool isValid = row.GetValue<bool>("is_reply_valid");
                    DateTimeOffset createdTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");

                    int validStatus = (int)Feed.FeedValidStatus.Valid;

                    if (!isValid)
                    {
                        validStatus = (int)Feed.FeedValidStatus.Deleted;
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("reply_by_comment_timestamp_asc", new List<string> { "comment_id", "created_on_timestamp", "reply_id" }, new List<string> { "reply_valid_status" }, new List<string>()));
                    bs = ps.Bind(validStatus, commentId, createdTimestamp, replyId);
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                batchStatement = new BatchStatement();

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse InsertNewColumnsAndTablesToChallenge()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = null;
                BoundStatement bs = null;

                // Challenge Questions
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_question", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string topicId = row.GetValue<string>("topic_id");
                    string challengeId = row.GetValue<string>("id");

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("challenge_question", new List<string> { "topic_id", "id" }, new List<string> { "base_score", "scoring_calculation_type" }, new List<string>()));
                    bs = ps.Bind((Int32.Parse(DefaultResource.ChallengeBaseScore)), (int)ChallengeQuestion.ChallengeScoringCalculationType.HalfBaseWithTimeBonus, topicId, challengeId);
                    batchStatement.Add(bs);
                }

                mainSession.Execute(batchStatement);
                batchStatement = new BatchStatement();

                // Full challenge history
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("full_challenge_history", new List<string>(), new List<string>()));
                rowSet = mainSession.Execute(ps.Bind());

                ChallengeQuestion question = new ChallengeQuestion();
                foreach (Row row in rowSet)
                {
                    string companyId = row.GetValue<string>("company_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    int order = row.GetValue<int>("question_order");

                    string questionId = row.GetValue<string>("question_id");
                    string topicId = row.GetValue<string>("topic_id");
                    string topicCategoryId = row.GetValue<string>("topic_category_id");

                    ChallengeQuestion updatedQuestion = question.SelectQuestion(questionId, topicId, companyId, null, mainSession).Question;
                    if (updatedQuestion != null)
                    {
                        List<string> playerIds = row.GetValue<List<string>>("players_ids");

                        bool bonusQuestion = updatedQuestion.DifficultyLevel == (int)ChallengeQuestion.QuestionDifficultyLevel.Hard ? true : false;

                        string answerId = row.GetValue<string>("answer");
                        string initiatorUserAnswerId = row.GetValue<string>("answer_of_initiated_user");
                        string challengedUserAnswerId = row.GetValue<string>("answer_of_challenged_user");

                        int scoreOfInitiatorUser = 0;
                        int scoreOfChallengedUser = 0;
                        int scoreMultiplier = row.GetValue<int>("score_multiplier");

                        float timeAssigned = row.GetValue<float>("time_answering");
                        float timeTakenByInitiatedUser = row.GetValue<float?>("time_taken_by_initiated_user") == null ? 0 : row.GetValue<float>("time_taken_by_initiated_user");
                        float timeTakenByChallengedUser = row.GetValue<float?>("time_taken_by_challenged_user") == null ? 0 : row.GetValue<float>("time_taken_by_challenged_user");

                        BatchStatement bsExp = new BatchStatement();
                        PreparedStatement psExp = null;

                        if (row.GetValue<int?>("base_score") == null)
                        {
                            if (!string.IsNullOrEmpty(initiatorUserAnswerId) && answerId.Equals(initiatorUserAnswerId))
                            {
                                scoreOfInitiatorUser = question.CalculateScore(timeAssigned, timeTakenByInitiatedUser, bonusQuestion, scoreMultiplier);

                                string userId = playerIds[0];
                                string allocationId = UUIDGenerator.GenerateUniqueIDForExpAllocation();

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfInitiatorUser, userId, DateTime.UtcNow));
                            }

                            if (!string.IsNullOrEmpty(challengedUserAnswerId) && answerId.Equals(challengedUserAnswerId))
                            {
                                scoreOfChallengedUser = question.CalculateScore(timeAssigned, timeTakenByChallengedUser, bonusQuestion, scoreMultiplier);

                                string userId = playerIds[1];
                                string allocationId = UUIDGenerator.GenerateUniqueIDForExpAllocation();

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));

                                psExp = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question_timestamp",
                                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                                bsExp.Add(psExp.Bind(allocationId, topicId, questionId, scoreOfChallengedUser, userId, DateTime.UtcNow));
                            }

                            analyticSession.Execute(bsExp);
                            bsExp = new BatchStatement();
                        }

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("full_challenge_history", new List<string> { "company_id", "challenge_id", "question_order" }, new List<string> { "base_score", "scoring_calculation_type", "score_of_initiated_user", "score_of_challenged_user", "answer_on_timestamp_by_initiated_user", "answer_on_timestamp_by_challenged_user" }, new List<string>()));
                        bs = ps.Bind((Int32.Parse(DefaultResource.ChallengeBaseScore)), (int)ChallengeQuestion.ChallengeScoringCalculationType.HalfBaseWithTimeBonus, scoreOfInitiatorUser, scoreOfChallengedUser, DateTime.UtcNow, DateTime.UtcNow, companyId, challengeId, order);
                        batchStatement.Add(bs);
                    }
                    else
                    {
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("full_challenge_history", new List<string> { "company_id", "challenge_id", "question_order" }, new List<string> { "base_score", "scoring_calculation_type" }, new List<string>()));
                        bs = ps.Bind((Int32.Parse(DefaultResource.ChallengeBaseScore)), (int)ChallengeQuestion.ChallengeScoringCalculationType.HalfBaseWithTimeBonus, companyId, challengeId, order);
                        batchStatement.Add(bs);
                    }

                }

                mainSession.Execute(batchStatement);
                batchStatement = new BatchStatement();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse InsertTableChallengeByTopic()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();

                PreparedStatement ps = null;
                BoundStatement bs = null;

                // Challenge Questions
                ps = session.Prepare(CQLGenerator.SelectStatement("challenge_question_stats_by_user", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string userId = row.GetValue<string>("user_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string topicId = row.GetValue<string>("topic_id");
                    string questionId = row.GetValue<string>("question_id");
                    bool isCorrect = row.GetValue<bool>("is_correct");
                    DateTimeOffset answeredTimestamp = row.GetValue<DateTimeOffset>("answered_timestamp");

                    ps = session.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_topic",
                        new List<string> { "user_id", "challenge_id", "topic_id", "question_id", "is_correct", "answered_timestamp" }));
                    bs = ps.Bind(userId, challengeId, topicId, questionId, isCorrect, answeredTimestamp);
                    session.Execute(bs);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewTablesForUserToken()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;

                ps = session.Prepare(CQLGenerator.SelectStatement("user_token", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string email = row.GetValue<string>("email");
                    string userToken = row.GetValue<string>("user_token");

                    ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email" }));
                    rowSet = session.Execute(ps.Bind(email));

                    foreach (Row userRow in rowSet)
                    {
                        BatchStatement bs = new BatchStatement();

                        string userId = userRow.GetValue<string>("user_id");
                        string companyId = userRow.GetValue<string>("company_id");

                        ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication_token",
                            new List<string> { "email", "user_token", "user_id", "company_id", "is_token_valid", "last_modified_timestamp" }));
                        bs.Add(ps.Bind(email, userToken, userId, companyId, false, DateTime.UtcNow));

                        ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication_token_by_user",
                            new List<string> { "email", "user_token", "user_id", "company_id", "is_token_valid", "last_modified_timestamp" }));
                        bs.Add(ps.Bind(email, userToken, userId, companyId, false, DateTime.UtcNow));

                        session.Execute(bs);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewTablesForNotification()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            string targetedUserId = string.Empty;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement("notification_privacy", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    batchStatement = new BatchStatement();

                    string notificationId = row.GetValue<string>("notification_id");
                    targetedUserId = row.GetValue<string>("targeted_user_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    int? feedType = row["feed_type"] == null ? null : row.GetValue<int?>("feed_type");
                    bool? isSeen = row.GetValue<bool?>("is_seen");
                    bool? isFetched = row.GetValue<bool?>("is_fetched");
                    int notificationSubType = row.GetValue<int>("notification_subtype");
                    int notificationType = row.GetValue<int>("notification_type");


                    if (row["notification_created_on_timestamp"] == null)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("notification_privacy",
                            new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }));
                        batchStatement.Add(ps.Bind(challengeId, feedId, commentId, replyId, targetedUserId, notificationType, notificationSubType, notificationId));
                        session.Execute(batchStatement);
                        break;
                    }

                    DateTimeOffset createdOnTimestampOffset = row.GetValue<DateTimeOffset>("notification_created_on_timestamp");
                    DateTime createdOnTimestamp = row.GetValue<DateTime>("notification_created_on_timestamp");

                    if (isFetched == null)
                    {
                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batchStatement.Add(ps.Bind(true, challengeId, feedId, commentId, replyId, targetedUserId, notificationType, notificationSubType, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_desc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batchStatement.Add(ps.Bind(true, targetedUserId, createdOnTimestampOffset, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_asc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batchStatement.Add(ps.Bind(true, targetedUserId, createdOnTimestampOffset, notificationId));

                        isFetched = true;
                    }

                    if (isSeen == null)
                    {
                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(ps.Bind(false, challengeId, feedId, commentId, replyId, targetedUserId, notificationType, notificationSubType, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_desc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(ps.Bind(false, targetedUserId, createdOnTimestampOffset, notificationId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_asc",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(ps.Bind(false, targetedUserId, createdOnTimestampOffset, notificationId));

                        isSeen = false;
                    }

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_challenge",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_feed",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_comment",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    ps = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_reply",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestampOffset));

                    session.Execute(batchStatement);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                Log.Error(targetedUserId);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewColumnsForUser()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string userId = row.GetValue<string>("id");
                    DateTimeOffset createdOnTimestamp = row.GetValue<DateTimeOffset>("created_on_timestamp");
                    ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic",
                        new List<string> { "id" }, new List<string> { "invited_on_timestamp", "is_email_sent" }, new List<string>()));
                    batchStatement.Add(ps.Bind(createdOnTimestamp, true, userId));
                }

                session.Execute(batchStatement);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateNewColumnsForCompany()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<string>(), new List<string>()));
                RowSet rowSet = session.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string companyId = row.GetValue<string>("id");
                    string companyName = row.GetValue<string>("title");
                    string primaryAdminUserId = row.GetValue<string>("created_by_user_id");

                    string defaultPersonalInvitationDescription = string.Format(DefaultResource.CompanyEmailPersonnelInvitationDescription, companyName);
                    string defaultAdminInvitationDescription = string.Format(DefaultResource.CompanyEmailAdminInvitationDescription, companyName);

                    string clientBannerUrl = DefaultResource.CompanyClientBannerUrl;
                    if (row["client_banner_url"] != null)
                    {
                        clientBannerUrl = row.GetValue<string>("client_banner_url");
                    }

                    string profilePopupBanner = DefaultResource.CompanyProfilePopupUrl;
                    if (row["profile_popup_banner_url"] != null)
                    {
                        profilePopupBanner = row.GetValue<string>("profile_popup_banner_url");
                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("company", new List<string> { "id" },
                        new List<string> { "primary_admin_user_id", "email_square_logo_url", "email_personnel_invitation_title", "email_personnel_invitation_description", "email_personnel_invitation_support_info",
                        "email_admin_invitation_title", "email_admin_invitation_description", "email_admin_invitation_support_info",
                        "email_forgot_password_title", "email_forgot_password_description", "email_forgot_password_support_info",
                        "email_reset_password_title", "email_reset_password_description", "email_reset_password_support_info",
                        "admin_website_header_logo_url",
                        "client_banner_url", "client_pull_refresh_banner_url", "client_matchup_banner_url", "client_profile_popup_banner_url"}, new List<string>()));

                    batchStatement.Add(ps.Bind(primaryAdminUserId, DefaultResource.CompanyEmailSquareImageUrl, DefaultResource.CompanyEmailPersonnelInvitationTitle, defaultPersonalInvitationDescription, DefaultResource.CompanyEmailPersonnelInvitationSupportInfo,
                        DefaultResource.CompanyEmailAdminInvitationTitle, defaultAdminInvitationDescription, DefaultResource.CompanyEmailAdminInvitationSupportInfo,
                        DefaultResource.CompanyEmailForgotPasswordTitle, DefaultResource.CompanyEmailForgotPasswordDescription, DefaultResource.CompanyEmailForgotPasswordSupportInfo,
                        DefaultResource.CompanyEmailResetPasswordTitle, DefaultResource.CompanyEmailResetPasswordDescription, DefaultResource.CompanyEmailResetPasswordSupportInfo,
                        DefaultResource.CompanyHeaderImageUrl,
                        clientBannerUrl, DefaultResource.CompanyClientPullRefreshImageUrl, DefaultResource.CompanyMatchupBannerUrl, profilePopupBanner, companyId));
                }

                session.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }

        public UpdateScriptResponse CreateMatchUpActivity()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;
                BatchStatement bs = new BatchStatement();
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string companyId = row.GetValue<string>("company_id");
                    string challengeId = row.GetValue<string>("id");
                    string topicId = row.GetValue<string>("topic_id");
                    string categoryId = row.GetValue<string>("topic_category_id");

                    if (string.IsNullOrEmpty(topicId))
                    {
                        // Delete from db
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("challenge_history", new List<string> { "company_id", "id" }));
                        mainSession.Execute(ps.Bind(companyId, challengeId));
                        continue;
                    }

                    if (string.IsNullOrEmpty(categoryId))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("topic", new List<string>(), new List<string> { "id" }));
                        Row topicRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                        if (topicRow != null)
                        {
                            categoryId = topicRow.GetValue<string>("category_id");
                            ps = mainSession.Prepare(CQLGenerator.UpdateStatement("challenge_history", new List<string> { "company_id", "id" }, new List<string> { "topic_category_id" }, new List<string>()));
                            mainSession.Execute(ps.Bind(categoryId, companyId, challengeId));
                        }
                    }


                    if (row["players_ids"] != null)
                    {
                        List<string> playerIds = row.GetValue<List<string>>("players_ids");
                        string initiatorUserId = playerIds[0];
                        string challengedUserId = playerIds[1];

                        if (row["initiated_user_started_on_timestamp"] != null)
                        {
                            DateTime endTimestamp = DateTime.MinValue;
                            DateTime startTimestamp = row.GetValue<DateTime>("initiated_user_started_on_timestamp");

                            if (row["initiated_user_completed_on_timestamp"] != null)
                            {
                                endTimestamp = row.GetValue<DateTime>("initiated_user_completed_on_timestamp");
                            }
                            else
                            {
                                if (row["invalidated_on_timestamp"] != null && row.GetValue<string>("invalidated_by_user_id").Equals(initiatorUserId))
                                {
                                    endTimestamp = row.GetValue<DateTime>("invalidated_on_timestamp");
                                }
                            }

                            if (endTimestamp != DateTime.MinValue)
                            {
                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_activity_log", new List<string> { "company_id", "challenge_id", "topic_id", "topic_category_id", "user_id", "start_timestamp", "end_timestamp" }));
                                bs.Add(ps.Bind(companyId, challengeId, topicId, categoryId, initiatorUserId, startTimestamp, endTimestamp));

                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_activity_log_by_start_timestamp", new List<string> { "company_id", "challenge_id", "topic_id", "topic_category_id", "user_id", "start_timestamp", "end_timestamp" }));
                                bs.Add(ps.Bind(companyId, challengeId, topicId, categoryId, initiatorUserId, startTimestamp, endTimestamp));

                                analyticSession.Execute(bs);
                                bs = new BatchStatement();
                            }

                        }

                        if (row["challenged_user_started_on_timestamp"] != null)
                        {
                            DateTime endTimestamp = DateTime.MinValue;
                            DateTime startTimestamp = row.GetValue<DateTime>("challenged_user_started_on_timestamp");

                            if (row["challenged_user_completed_on_timestamp"] != null)
                            {
                                endTimestamp = row.GetValue<DateTime>("challenged_user_completed_on_timestamp");
                            }
                            else
                            {
                                if (row["invalidated_on_timestamp"] != null && row.GetValue<string>("invalidated_by_user_id").Equals(challengedUserId))
                                {
                                    endTimestamp = row.GetValue<DateTime>("invalidated_on_timestamp");
                                }
                            }

                            if (endTimestamp != DateTime.MinValue)
                            {
                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_activity_log", new List<string> { "company_id", "challenge_id", "topic_id", "topic_category_id", "user_id", "start_timestamp", "end_timestamp" }));
                                bs.Add(ps.Bind(companyId, challengeId, topicId, categoryId, challengedUserId, startTimestamp, endTimestamp));

                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_activity_log_by_start_timestamp", new List<string> { "company_id", "challenge_id", "topic_id", "topic_category_id", "user_id", "start_timestamp", "end_timestamp" }));
                                bs.Add(ps.Bind(companyId, challengeId, topicId, categoryId, challengedUserId, startTimestamp, endTimestamp));

                                analyticSession.Execute(bs);
                                bs = new BatchStatement();
                            }

                        }
                    }

                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }

        public UpdateScriptResponse CreateMatchUpTopicAttempt()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("challenge_question_stats_by_topic", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                List<TopicAttempt> topicList = new List<TopicAttempt>();
                Topic topicManager = new Topic();
                foreach (Row row in rowSet)
                {
                    bool isCorrect = row.GetValue<bool>("is_correct");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string topicId = row.GetValue<string>("topic_id");
                    DateTime answeredTimestamp = row.GetValue<DateTime>("answered_timestamp");
                    answeredTimestamp = answeredTimestamp.Date;

                    int addedCorrect = 0;
                    int addedIncorrect = 0;

                    if (isCorrect)
                    {
                        addedCorrect = 1;
                    }
                    else
                    {
                        addedIncorrect = 1;
                    }

                    if (topicList.FirstOrDefault(t => t.TopicId == topicId) == null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("challenge_history", new List<string>(), new List<string> { "id" }));
                        Row challengeRow = mainSession.Execute(ps.Bind(challengeId)).FirstOrDefault();

                        if (challengeRow != null)
                        {
                            string companyId = challengeRow.GetValue<string>("company_id");

                            Topic foundTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, false).Topic;

                            if (foundTopic != null)
                            {
                                TopicAttempt topic = new TopicAttempt
                                {
                                    TopicId = topicId,
                                    CompanyId = companyId,
                                    Correct = addedCorrect,
                                    Incorrect = addedIncorrect
                                };

                                topicList.Add(topic);
                            }

                        }
                    }
                    else
                    {
                        TopicAttempt topic = (TopicAttempt)topicList.FirstOrDefault(t => t.TopicId == topicId);
                        topic.Incorrect += addedIncorrect;
                        topic.Correct += addedCorrect;

                    }

                }

                foreach (TopicAttempt topic in topicList)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log",
                        new List<string> { "company_id", "topic_id", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(topic.CompanyId, topic.TopicId, topic.Correct, topic.Incorrect));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_correct",
                        new List<string> { "company_id", "topic_id", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(topic.CompanyId, topic.TopicId, topic.Correct, topic.Incorrect));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_incorrect",
                        new List<string> { "company_id", "topic_id", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(topic.CompanyId, topic.TopicId, topic.Correct, topic.Incorrect));

                    analyticSession.Execute(updateBatch);
                    updateBatch = new BatchStatement();
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse CreateMatchUpQuestionAttempt()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("challenge_question_stats_by_topic", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                Thread thread = new Thread(() => UpdateQuestionThread(rowSet, mainSession, analyticSession));
                thread.Start();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateQuestionThread(RowSet rowSet, ISession mainSession, ISession analyticSession)
        {
            try
            {
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;
                List<QuestionAttempt> questionList = new List<QuestionAttempt>();
                List<UserAttempt> userList = new List<UserAttempt>();
                List<string> invalidTopics = new List<string>();
                List<string> invalidQuestions = new List<string>();

                Topic topicManager = new Topic();
                ChallengeQuestion questionManager = new ChallengeQuestion();
                foreach (Row row in rowSet)
                {
                    bool isCorrect = row.GetValue<bool>("is_correct");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string questionId = row.GetValue<string>("question_id");
                    string topicId = row.GetValue<string>("topic_id");
                    string userId = row.GetValue<string>("user_id");
                    DateTime answeredTimestamp = row.GetValue<DateTime>("answered_timestamp");
                    DateTime startTimestamp = row.GetValue<DateTime>("answered_timestamp");
                    answeredTimestamp = answeredTimestamp.Date;

                    int addedCorrect = 0;
                    int addedIncorrect = 0;

                    if (isCorrect)
                    {
                        addedCorrect = 1;
                    }
                    else
                    {
                        addedIncorrect = 1;
                    }

                    bool isValid = false;

                    if (!string.IsNullOrEmpty(questionId))
                    {
                        // Settle question attempt
                        if (questionList.FirstOrDefault(q => q.QuestionId == questionId) == null)
                        {
                            if (!invalidTopics.Contains(topicId) && !invalidQuestions.Contains(questionId))
                            {
                                ps = mainSession.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("challenge_history", new List<string>(), new List<string> { "id" }));
                                Row challengeRow = mainSession.Execute(ps.Bind(challengeId)).FirstOrDefault();

                                if (challengeRow != null)
                                {
                                    string companyId = challengeRow.GetValue<string>("company_id");

                                    Topic foundTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, false).Topic;

                                    if (foundTopic != null)
                                    {
                                        ChallengeQuestion foundQuestion = questionManager.SelectQuestion(questionId, topicId, companyId, null, mainSession).Question;
                                        if (foundQuestion != null)
                                        {
                                            QuestionAttempt question = new QuestionAttempt
                                            {
                                                TopicId = topicId,
                                                QuestionId = questionId,
                                                Correct = addedCorrect,
                                                Incorrect = addedIncorrect
                                            };

                                            isValid = true;
                                            questionList.Add(question);
                                        }
                                        else
                                        {
                                            invalidQuestions.Add(questionId);
                                        }

                                    }
                                    else
                                    {
                                        invalidTopics.Add(topicId);
                                    }

                                }
                            }

                        }
                        else
                        {
                            QuestionAttempt question = (QuestionAttempt)questionList.FirstOrDefault(q => q.QuestionId == questionId);
                            if (question != null)
                            {
                                question.Incorrect += addedIncorrect;
                                question.Correct += addedCorrect;
                                isValid = true;
                            }
                        }

                        if (isValid)
                        {
                            // Settle user attempt
                            if (userList.FirstOrDefault(u => u.UserId == userId) == null)
                            {
                                UserAttempt userAttempt = new UserAttempt
                                {
                                    UserId = userId,
                                    Attempts = new List<Attempt>()
                                };

                                userList.Add(userAttempt);
                            }

                            UserAttempt currentUser = userList.FirstOrDefault(u => u.UserId == userId);

                            if (currentUser.Attempts.FirstOrDefault(a => a.QuestionId == questionId) == null)
                            {
                                Attempt attempt = new Attempt
                                {
                                    TopicId = topicId,
                                    QuestionId = questionId,
                                    Timestamps = new List<AttemptTimestamp>()
                                };

                                currentUser.Attempts.Add(attempt);
                            }

                            Attempt currentAttempt = currentUser.Attempts.FirstOrDefault(a => a.QuestionId == questionId);
                            AttemptTimestamp timestamp = new AttemptTimestamp
                            {
                                Timestamp = startTimestamp,
                                IsCorrect = isCorrect,
                            };
                            currentAttempt.Timestamps.Add(timestamp);
                        }
                    }

                }

                foreach (QuestionAttempt question in questionList)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log",
                        new List<string> { "question_id", "topic_id", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(question.QuestionId, question.TopicId, question.Correct, question.Incorrect));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_correct",
                        new List<string> { "question_id", "topic_id", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(question.QuestionId, question.TopicId, question.Correct, question.Incorrect));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_incorrect",
                        new List<string> { "question_id", "topic_id", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(question.QuestionId, question.TopicId, question.Correct, question.Incorrect));

                    analyticSession.Execute(updateBatch);
                    updateBatch = new BatchStatement();
                }

                foreach (UserAttempt userAttempt in userList)
                {
                    string userId = userAttempt.UserId;
                    foreach (Attempt attempt in userAttempt.Attempts)
                    {
                        attempt.Timestamps = attempt.Timestamps.OrderBy(t => t.Timestamp).ToList();
                        string topicId = attempt.TopicId;
                        string questionId = attempt.QuestionId;

                        int tries = 1;
                        foreach (AttemptTimestamp timestamp in attempt.Timestamps)
                        {
                            bool isCorrect = timestamp.IsCorrect;
                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_user_attempt_by_question",
                                new List<string> { "topic_id", "question_id", "user_id", "tries", "is_correct" }));
                            updateBatch.Add(ps.Bind(topicId, questionId, userId, tries, isCorrect));

                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_user_attempt_by_tries",
                                new List<string> { "topic_id", "question_id", "user_id", "tries", "is_correct" }));
                            updateBatch.Add(ps.Bind(topicId, questionId, userId, tries, isCorrect));

                            tries++;
                        }
                        analyticSession.Execute(updateBatch);
                        updateBatch = new BatchStatement();
                    }
                }
                Log.Debug("---------------------------------------------------------------------------");
                Log.Debug("Update question attempt success!");
                Log.Debug("---------------------------------------------------------------------------");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public UpdateScriptResponse CreateMatchUpOptionAttempt()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;
                //ps = analyticSession.Prepare(CQLGenerator.SelectStatement("challenge_question_stats_by_topic", new List<string>(), new List<string>()));
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("full_challenge_history", new List<string>(), new List<string>()));
                RowSet rowSet = analyticSession.Execute(ps.Bind());

                Thread thread = new Thread(() => UpdateOptionThread(rowSet, mainSession, analyticSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        //private void UpdateOptionThread(RowSet rowSet, ISession mainSession, ISession analyticSession)
        //{
        //    try
        //    {
        //        BatchStatement updateBatch = new BatchStatement();
        //        PreparedStatement ps = null;
        //        List<QuestionAttempt> questionList = new List<QuestionAttempt>();
        //        Topic topicManager = new Topic();
        //        List<string> invalidTopics = new List<string>();
        //        List<string> invalidQuestions = new List<string>();

        //        ChallengeQuestion questionManager = new ChallengeQuestion();
        //        foreach (Row row in rowSet)
        //        {
        //            string challengeId = row.GetValue<string>("challenge_id");
        //            string questionId = row.GetValue<string>("question_id");
        //            string topicId = row.GetValue<string>("topic_id");
        //            string userId = row.GetValue<string>("user_id");
        //            string companyId = string.Empty;

        //            ps = mainSession.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("challenge_history", new List<string>(), new List<string> { "id" }));
        //            Row companyChallengeRow = mainSession.Execute(ps.Bind(challengeId)).FirstOrDefault();
        //            if(companyChallengeRow != null)
        //            {
        //                companyId = companyChallengeRow.GetValue<string>("company_id");
        //            }

        //            Row challengeRow = null;
        //            if (!string.IsNullOrEmpty(questionId))
        //            {
        //                // Find question
        //                if (questionList.FirstOrDefault(q => q.QuestionId == questionId) == null)
        //                {
        //                    if (!invalidTopics.Contains(topicId) && !invalidQuestions.Contains(questionId))
        //                    {
        //                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("full_challenge_history", new List<string>(), new List<string> { "company_id", "challenge_id", "question_id" }));
        //                        challengeRow = mainSession.Execute(ps.Bind(companyId, challengeId, questionId)).FirstOrDefault();

        //                        if (challengeRow != null)
        //                        {
        //                            Topic foundTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, false).Topic;

        //                            if (foundTopic != null)
        //                            {
        //                                ChallengeQuestion foundQuestion = questionManager.SelectQuestion(questionId, topicId, companyId, null, mainSession).Question;
        //                                if (foundQuestion != null)
        //                                {
        //                                    QuestionAttempt newQuestion = new QuestionAttempt
        //                                    {
        //                                        TopicId = topicId,
        //                                        QuestionId = questionId,
        //                                        Options = new List<OptionAttempt>()
        //                                    };

        //                                    questionList.Add(newQuestion);
        //                                }
        //                                else
        //                                {
        //                                    invalidQuestions.Add(questionId);
        //                                }

        //                            }
        //                            else
        //                            {
        //                                invalidTopics.Add(topicId);
        //                            }

        //                        }
        //                    }

        //                }

        //                // Settle option
        //                QuestionAttempt question = questionList.FirstOrDefault(q => q.QuestionId == questionId);
        //                if (question != null)
        //                {
        //                    if(challengeRow == null)
        //                    {
        //                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("full_challenge_history", new List<string>(), new List<string> { "company_id", "challenge_id", "question_id" }));
        //                        challengeRow = mainSession.Execute(ps.Bind(companyId, challengeId, questionId)).FirstOrDefault();
        //                    }

        //                    if (challengeRow != null)
        //                    {
        //                        List<string> playerIds = challengeRow.GetValue<List<string>>("players_ids");
        //                        if(playerIds.Contains(userId))
        //                        {
        //                            string selectedOptionId = string.Empty;
        //                            bool isInitiator = playerIds[0].Equals(userId) ? true : false;
        //                            if(isInitiator)
        //                            {
        //                                selectedOptionId = challengeRow.GetValue<string>("answer_of_initiated_user");
        //                            }
        //                            else
        //                            {
        //                                selectedOptionId = challengeRow.GetValue<string>("answer_of_challenged_user");
        //                            }

        //                            if(selectedOptionId != null && !selectedOptionId.Equals("-1"))
        //                            {
        //                                if(question.Options.FirstOrDefault(o => o.OptionId == selectedOptionId) == null)
        //                                {
        //                                    OptionAttempt newOption = new OptionAttempt
        //                                    {
        //                                        TopicId = question.TopicId,
        //                                        QuestionId = question.QuestionId,
        //                                        OptionId = selectedOptionId,
        //                                        Count = 1
        //                                    };

        //                                    question.Options.Add(newOption);
        //                                }
        //                                else
        //                                {
        //                                    OptionAttempt option = question.Options.FirstOrDefault(o => o.OptionId == selectedOptionId);
        //                                    option.Count++;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }

        //        }

        //        foreach (QuestionAttempt question in questionList)
        //        {
        //            string topicId = question.TopicId;
        //            string questionId = question.QuestionId;

        //            foreach(OptionAttempt option in question.Options)
        //            {
        //                string optionId = option.OptionId;
        //                int count = option.Count;
        //                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_option_attempt_log",
        //                    new List<string> { "topic_id", "question_id", "option_id", "attempt" }));
        //                updateBatch.Add(ps.Bind(topicId, questionId, optionId, count));
        //            }

        //            analyticSession.Execute(updateBatch);
        //            updateBatch = new BatchStatement();
        //        }


        //        Log.Debug("---------------------------------------------------------------------------");
        //        Log.Debug("Update option attempt success!");
        //        Log.Debug("---------------------------------------------------------------------------");
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.ToString(), ex);
        //    }

        //}

        private void UpdateOptionThread(RowSet rowSet, ISession mainSession, ISession analyticSession)
        {
            try
            {
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;
                List<OptionAttempt> optionList = new List<OptionAttempt>();
                Topic topicManager = new Topic();
                List<string> invalidTopics = new List<string>();
                List<string> invalidQuestions = new List<string>();

                ChallengeQuestion questionManager = new ChallengeQuestion();
                foreach (Row row in rowSet)
                {
                    int questionOrder = row.GetValue<int>("question_order");
                    string companyId = row.GetValue<string>("company_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string questionId = row.GetValue<string>("question_id");
                    string topicId = row.GetValue<string>("topic_id");
                    string categoryId = row.GetValue<string>("topic_category_id");

                    // 2 options
                    string optionIdByInitiator = row.GetValue<string>("answer_of_initiated_user");
                    string optionIdByChallenged = row.GetValue<string>("answer_of_challenged_user");

                    // Update database
                    if (string.IsNullOrEmpty(topicId))
                    {
                        // Delete from db
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("full_challenge_history", new List<string> { "company_id", "challenge_id" }));
                        mainSession.Execute(ps.Bind(companyId, challengeId));
                        continue;
                    }

                    if (string.IsNullOrEmpty(categoryId))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("topic", new List<string>(), new List<string> { "id" }));
                        Row topicRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                        if (topicRow != null)
                        {
                            categoryId = topicRow.GetValue<string>("category_id");
                            ps = mainSession.Prepare(CQLGenerator.UpdateStatement("full_challenge_history", new List<string> { "company_id", "challenge_id", "question_order" }, new List<string> { "topic_category_id" }, new List<string>()));
                            mainSession.Execute(ps.Bind(categoryId, companyId, challengeId, questionOrder));
                        }
                    }

                    // Initiator
                    if (!string.IsNullOrEmpty(optionIdByInitiator) && optionIdByInitiator.StartsWith("CQC"))
                    {
                        if (optionList.FirstOrDefault(o => o.OptionId == optionIdByInitiator) == null)
                        {
                            if (!invalidTopics.Contains(topicId) && !invalidQuestions.Contains(questionId))
                            {
                                Topic foundTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, false).Topic;
                                if (foundTopic != null)
                                {
                                    ChallengeQuestion foundQuestion = questionManager.SelectQuestion(questionId, topicId, companyId, null, mainSession).Question;
                                    if (foundQuestion != null)
                                    {
                                        OptionAttempt option = new OptionAttempt
                                        {
                                            TopicId = topicId,
                                            QuestionId = questionId,
                                            OptionId = optionIdByInitiator,
                                            Count = 1
                                        };
                                        optionList.Add(option);
                                    }
                                    else
                                    {
                                        invalidQuestions.Add(questionId);
                                    }

                                }
                                else
                                {
                                    invalidTopics.Add(topicId);
                                }
                            }

                        }
                        else
                        {
                            OptionAttempt option = (OptionAttempt)optionList.FirstOrDefault(o => o.OptionId == optionIdByInitiator);
                            option.Count += 1;
                        }
                    }


                    // Challenger
                    if (!string.IsNullOrEmpty(optionIdByChallenged) && optionIdByChallenged.StartsWith("CQC"))
                    {
                        if (optionList.FirstOrDefault(o => o.OptionId == optionIdByChallenged) == null)
                        {
                            if (!invalidTopics.Contains(topicId) && !invalidQuestions.Contains(questionId))
                            {
                                Topic foundTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, false).Topic;

                                if (foundTopic != null)
                                {
                                    ChallengeQuestion foundQuestion = questionManager.SelectQuestion(questionId, topicId, companyId, null, mainSession).Question;
                                    if (foundQuestion != null)
                                    {
                                        OptionAttempt option = new OptionAttempt
                                        {
                                            TopicId = topicId,
                                            QuestionId = questionId,
                                            OptionId = optionIdByChallenged,
                                            Count = 1
                                        };
                                        optionList.Add(option);
                                    }
                                    else
                                    {
                                        invalidQuestions.Add(questionId);
                                    }

                                }
                                else
                                {
                                    invalidTopics.Add(topicId);
                                }
                            }

                        }
                        else
                        {
                            OptionAttempt option = (OptionAttempt)optionList.FirstOrDefault(o => o.OptionId == optionIdByChallenged);
                            option.Count += 1;
                        }
                    }

                }

                foreach (OptionAttempt option in optionList)
                {
                    string topicId = option.TopicId;
                    string optionId = option.OptionId;
                    string questionId = option.QuestionId;
                    int count = option.Count;

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_option_attempt_log",
                                new List<string> { "topic_id", "question_id", "option_id", "attempt" }));
                    updateBatch.Add(ps.Bind(topicId, questionId, optionId, count));
                }

                analyticSession.Execute(updateBatch);

                Log.Debug("---------------------------------------------------------------------------");
                Log.Debug("Update option attempt success!");
                Log.Debug("---------------------------------------------------------------------------");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public UpdateScriptResponse UpdateFeedUser()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_targeted_user", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    string feedId = row.GetValue<string>("feed_id");
                    string userId = row.GetValue<string>("user_id");
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_targeted_user", new List<string> { "feed_id", "user_id" }, new List<string> { "is_tagged " }, new List<string>()));
                    mainSession.Execute(ps.Bind(false, feedId, userId));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateSurveyCategory()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_category", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    bool isForEvent = row.GetValue<bool>("is_for_event");
                    string companyId = row.GetValue<string>("company_id");
                    string categoryId = row.GetValue<string>("id");
                    string title = row.GetValue<string>("title");
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("rs_category", new List<string> { "company_id", "id" }, new List<string> { "is_for_event " }, new List<string>()));
                    updateBatch.Add(ps.Bind(false, companyId, categoryId));
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("rs_category_by_title", new List<string> { "company_id", "title", "rs_category_id" }, new List<string> { "is_for_event" }, new List<string>()));
                    updateBatch.Add(ps.Bind(false, companyId, title, categoryId));
                    mainSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateSurvey()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    DateTimeOffset startDate = row.GetValue<DateTimeOffset>("created_on_timestamp");
                    string topicId = row.GetValue<string>("id");
                    string categoryId = row.GetValue<string>("category_id");
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("rs_topic", new List<string> { "category_id", "id" }, new List<string> { "start_date", "is_anonymous" }, new List<string>()));
                    updateBatch.Add(ps.Bind(startDate, false, categoryId, topicId));
                    mainSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateSurveyCard()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    string topicId = row.GetValue<string>("rs_topic_id");
                    string cardId = row.GetValue<string>("id");
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "range_start_position", "mid_range" }, new List<string>()));
                    updateBatch.Add(ps.Bind(1, 0, topicId, cardId));
                    mainSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateMatchupDifficulty()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_topic_attempt_log", new List<string>(), new List<string>()));
                RowSet rowSet = analyticSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    string companyId = row.GetValue<string>("company_id");
                    string topicId = row.GetValue<string>("topic_id");
                    int correct = row.GetValue<int>("correct_attempt");
                    int incorrect = row.GetValue<int>("incorrect_attempt");

                    int total = correct + incorrect;
                    float correctRatio = (float)correct / total;
                    float incorrectRatio = (float)incorrect / total;

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("matchup_topic_attempt_log",
                        new List<string> { "company_id", "topic_id" }, new List<string> { "correct_ratio", "incorrect_ratio" }, new List<string>()));
                    updateBatch.Add(ps.Bind(correctRatio, incorrectRatio, companyId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_correct",
                        new List<string> { "company_id", "topic_id", "correct_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(companyId, topicId, correctRatio, correct, incorrect));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_incorrect",
                        new List<string> { "company_id", "topic_id", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(companyId, topicId, incorrectRatio, correct, incorrect));

                    analyticSession.Execute(updateBatch);
                }

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_question_attempt_log", new List<string>(), new List<string>()));
                rowSet = analyticSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    string questionId = row.GetValue<string>("question_id");
                    string topicId = row.GetValue<string>("topic_id");
                    int correct = row.GetValue<int>("correct_attempt");
                    int incorrect = row.GetValue<int>("incorrect_attempt");

                    int total = correct + incorrect;
                    float correctRatio = (float)correct / total;
                    float incorrectRatio = (float)incorrect / total;

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("matchup_question_attempt_log",
                        new List<string> { "question_id", "topic_id" }, new List<string> { "correct_ratio", "incorrect_ratio" }, new List<string>()));
                    updateBatch.Add(ps.Bind(correctRatio, incorrectRatio, questionId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_correct",
                        new List<string> { "question_id", "topic_id", "correct_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(questionId, topicId, correctRatio, correct, incorrect));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_incorrect",
                        new List<string> { "question_id", "topic_id", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(ps.Bind(questionId, topicId, incorrectRatio, correct, incorrect));

                    analyticSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateDynamicPulseCustomized()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    string deckId = row.GetValue<string>("deck_id");
                    string id = row.GetValue<string>("id");

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck",
                        new List<string> { "deck_id", "id" }, new List<string> { "is_start_date_customized" }, new List<string>()));
                    updateBatch.Add(ps.Bind(false, deckId, id));

                    mainSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateUserEmailLower()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    BatchStatement deleteBatch = new BatchStatement();
                    /*
                     * 	user_id text,
	                    email text,
	                    hashed_password text,
	                    salt text,
	                    company_id text,
	                    is_default boolean,
	                    created_by_user_id text,
	                    created_on_timestamp timestamp,
	                    last_modified_by_user_id text,
	                    last_modified_timestamp timestamp,
                        */
                    string userId = row.GetValue<string>("user_id");
                    string email = row.GetValue<string>("email");
                    string hashedPassword = row.GetValue<string>("hashed_password");
                    string salt = row.GetValue<string>("salt");
                    string companyId = row.GetValue<string>("company_id");
                    bool isDefault = row.GetValue<bool>("is_default");
                    string createdByUserId = row.GetValue<string>("created_by_user_id");
                    DateTime createdOnTimestamp = row.GetValue<DateTime>("created_on_timestamp");
                    string lastModifiedByUserId = row.GetValue<string>("last_modified_by_user_id");
                    DateTime lastModifiedTimestamp = row.GetValue<DateTime>("last_modified_timestamp");

                    if (email.Any(char.IsUpper))
                    {
                        string newEmail = email.ToLower().Trim();

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                        deleteBatch.Add(ps.Bind(email, userId));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_authentication",
                            new List<string> { "email", "user_id", "company_id", "created_by_user_id", "created_on_timestamp", "hashed_password", "last_modified_by_user_id", "last_modified_timestamp", "salt", "is_default" }));
                        updateBatch.Add(ps.Bind(newEmail, userId, companyId, createdByUserId, createdOnTimestamp, hashedPassword, lastModifiedByUserId, lastModifiedTimestamp, salt, isDefault));

                        //user_authentication_token_by_user
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_authentication_token_by_user", new List<string>(), new List<string> { "user_id" }));
                        RowSet tokenRowset = mainSession.Execute(ps.Bind(userId));
                        foreach (Row tokenRow in tokenRowset)
                        {
                            string token = tokenRow.GetValue<string>("user_token");

                            ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_authentication_token", new List<string> { "user_token", "user_id" }, new List<string> { "email" }, new List<string>()));
                            updateBatch.Add(ps.Bind(newEmail, userId, token));

                            ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_authentication_token_by_user", new List<string> { "user_id", "user_token" }, new List<string> { "email" }, new List<string>()));
                            updateBatch.Add(ps.Bind(newEmail, userId, token));
                        }

                        //reset_email_token_by_user
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("reset_email_token_by_user", new List<string>(), new List<string> { "user_id" }));
                        tokenRowset = mainSession.Execute(ps.Bind(userId));
                        foreach (Row tokenRow in tokenRowset)
                        {
                            string token = tokenRow.GetValue<string>("reset_token");
                            DateTime resetTimestamp = tokenRow.GetValue<DateTime>("reset_timestamp");

                            ps = mainSession.Prepare(CQLGenerator.UpdateStatement("reset_email_token", new List<string> { "reset_token", "reset_timestamp" }, new List<string> { "email" }, new List<string>()));
                            updateBatch.Add(ps.Bind(newEmail, token, resetTimestamp));

                            ps = mainSession.Prepare(CQLGenerator.UpdateStatement("reset_email_token_by_user", new List<string> { "user_id", "reset_timestamp" }, new List<string> { "email" }, new List<string>()));
                            updateBatch.Add(ps.Bind(newEmail, userId, resetTimestamp));
                        }

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "email" }, new List<string>()));
                        updateBatch.Add(ps.Bind(newEmail, userId));

                        mainSession.Execute(deleteBatch);
                        mainSession.Execute(updateBatch);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdatePhoneCountryForUser(string adminUserId, string companyId, string countryCode, string countryName)
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                User userManager = new User();

                List<User> users = userManager.GetAllUserForAdmin(adminUserId, companyId, null, 0, 0, null, false, false, null, mainSession).Users;

                BatchStatement updateBatch = new BatchStatement();

                foreach (User user in users)
                {
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_contact", new List<string> { "id" }, new List<string> { "phone_country_code", "phone_country_name" }, new List<string>()));
                    updateBatch.Add(ps.Bind(countryCode, countryName, user.UserId));
                }

                mainSession.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateFeedPointByTimestamp()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_point", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    DateTime votedTimestamp = row.GetValue<DateTime>("voted_timestamp");
                    int currentVote = row.GetValue<int>("value");
                    string feedId = row.GetValue<string>("feed_id");
                    string userId = row.GetValue<string>("user_id");

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("feed_point_with_value_by_timestamp",
                       new List<string> { "feed_id", "user_id", "value", "voted_timestamp" }));
                    batch.Add(ps.Bind(feedId, userId, currentVote, votedTimestamp));
                }

                mainSession.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdatePrivacyForFeed()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                List<string> taggedForUserFeedIds = new List<string>();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_targeted_user", new List<string>(), new List<string> { "is_tagged" }));
                RowSet taggedUserRowset = mainSession.Execute(ps.Bind(true));
                foreach (Row taggedUserRow in taggedUserRowset)
                {
                    string taggedUserId = taggedUserRow.GetValue<string>("user_id");
                    string feedId = taggedUserRow.GetValue<string>("feed_id");
                    taggedForUserFeedIds.Add(feedId);

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("feed_tagged_user", new List<string> { "feed_id", "user_id" }));
                    updateBatch.Add(ps.Bind(feedId, taggedUserId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("feed_tagged_user", new List<string> { "feed_id", "user_id" }));
                    deleteBatch.Add(ps.Bind(feedId, taggedUserId));
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                updateBatch = new BatchStatement();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    bool isForOnlyMe = false;
                    bool isGroup = false;

                    string feedId = row.GetValue<string>("feed_id");
                    string companyId = row.GetValue<string>("company_id");

                    if (row["is_for_group"] != null && row.GetValue<bool>("is_for_group"))
                    {
                        isGroup = true;
                    }

                    if (row["is_for_user"] != null && row.GetValue<bool>("is_for_user"))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_targeted_user", new List<string>(), new List<string> { "feed_id" }));
                        List<Row> targetedUserRowList = mainSession.Execute(ps.Bind(feedId)).ToList();
                        if (targetedUserRowList.Count == 1 && targetedUserRowList[0].GetValue<string>("user_id").Equals(row.GetValue<string>("owner_user_id")))
                        {
                            isForOnlyMe = true;
                        }
                    }

                    DateTime createdTimestamp = row.GetValue<DateTime>("feed_created_on_timestamp");
                    long createdTimestampLong = DateHelper.ConvertDateToLong(createdTimestamp);

                    bool isTaggedForUser = taggedForUserFeedIds.Contains(feedId) ? true : false;

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_privacy", new List<string> { "company_id", "feed_id" }, new List<string> { "is_for_group", "is_tagged_for_user", "is_for_only_me" }, new List<string>()));
                    updateBatch.Add(ps.Bind(isGroup, isTaggedForUser, isForOnlyMe, companyId, feedId));
                }

                mainSession.Execute(updateBatch);
                updateBatch = new BatchStatement();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_desc", new List<string>(), new List<string>()));
                rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    bool isForOnlyMe = false;
                    bool isGroup = false;

                    string feedId = row.GetValue<string>("feed_id");
                    string companyId = row.GetValue<string>("company_id");

                    if (row["is_for_group"] != null && row.GetValue<bool>("is_for_group"))
                    {
                        isGroup = true;
                    }

                    if (row["is_for_user"] != null && row.GetValue<bool>("is_for_user"))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_targeted_user", new List<string>(), new List<string> { "feed_id" }));
                        List<Row> targetedUserRowList = mainSession.Execute(ps.Bind(feedId)).ToList();
                        if (targetedUserRowList.Count == 1 && targetedUserRowList[0].GetValue<string>("user_id").Equals(row.GetValue<string>("owner_user_id")))
                        {
                            isForOnlyMe = true;
                        }
                    }

                    DateTime createdTimestamp = row.GetValue<DateTime>("feed_created_on_timestamp");
                    long createdTimestampLong = DateHelper.ConvertDateToLong(createdTimestamp);

                    bool isTaggedForUser = taggedForUserFeedIds.Contains(feedId) ? true : false;

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc", new List<string> { "company_id", "feed_id", "feed_created_on_timestamp" }, new List<string> { "is_for_group", "is_tagged_for_user", "is_for_only_me" }, new List<string>()));
                    updateBatch.Add(ps.Bind(isGroup, isTaggedForUser, isForOnlyMe, companyId, feedId, createdTimestamp));
                }


                mainSession.Execute(updateBatch);
                updateBatch = new BatchStatement();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy_by_company_timestamp_asc", new List<string>(), new List<string>()));
                rowSet = mainSession.Execute(ps.Bind());

                foreach (Row row in rowSet)
                {
                    bool isForOnlyMe = false;
                    bool isGroup = false;

                    string feedId = row.GetValue<string>("feed_id");
                    string companyId = row.GetValue<string>("company_id");

                    if (row["is_for_group"] != null && row.GetValue<bool>("is_for_group"))
                    {
                        isGroup = true;
                    }

                    if (row["is_for_user"] != null && row.GetValue<bool>("is_for_user"))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_targeted_user", new List<string>(), new List<string> { "feed_id" }));
                        List<Row> targetedUserRowList = mainSession.Execute(ps.Bind(feedId)).ToList();
                        if (targetedUserRowList.Count == 1 && targetedUserRowList[0].GetValue<string>("user_id").Equals(row.GetValue<string>("owner_user_id")))
                        {
                            isForOnlyMe = true;
                        }
                    }

                    DateTime createdTimestamp = row.GetValue<DateTime>("feed_created_on_timestamp");
                    long createdTimestampLong = DateHelper.ConvertDateToLong(createdTimestamp);

                    bool isTaggedForUser = taggedForUserFeedIds.Contains(feedId) ? true : false;

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc", new List<string> { "company_id", "feed_id", "feed_created_on_timestamp" }, new List<string> { "is_for_group", "is_tagged_for_user", "is_for_only_me" }, new List<string>()));
                    updateBatch.Add(ps.Bind(isGroup, isTaggedForUser, isForOnlyMe, companyId, feedId, createdTimestamp));
                }

                mainSession.Execute(updateBatch);
                updateBatch = new BatchStatement();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateScriptResponse UpdateLogin()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("user_login_log", new List<string>(), new List<string>()));
                RowSet rowSet = analyticSession.Execute(ps.Bind());

                Thread thread = new Thread(() => UpdateLoginThread(rowSet, analyticSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateLoginThread(RowSet rowSet, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;
                foreach (Row row in rowSet)
                {
                    string id = row.GetValue<string>("id");
                    string companyId = row.GetValue<string>("company_id");
                    string userId = row.GetValue<string>("user_id");
                    bool isLogin = row.GetValue<bool>("is_login");
                    DateTime loginDateStamp = row.GetValue<DateTime>("login_datestamp");
                    DateTime loginDate = row.GetValue<DateTime>("login_date_timestamp");

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("user_login_log_by_user",
                        new List<string> { "id", "company_id", "user_id", "is_login", "login_datestamp", "login_date_timestamp" }));
                    analyticSession.Execute(ps.Bind(id, companyId, userId, isLogin, loginDateStamp, loginDate));
                }

                Log.Error("Update login completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public UpdateScriptResponse OptimizeNotification()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("notification_privacy", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                Thread thread = new Thread(() => OptimizeNotificationThread(rowSet, mainSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void OptimizeNotificationThread(RowSet rowSet, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                foreach (Row row in rowSet)
                {
                    string targetedUserId = row.GetValue<string>("targeted_user_id");
                    string notificationId = row.GetValue<string>("notification_id");
                    string challengeId = row.GetValue<string>("challenge_id");
                    string achievementId = row.GetValue<string>("achievement_id");
                    string feedId = row.GetValue<string>("feed_id");
                    string commentId = row.GetValue<string>("comment_id");
                    string replyId = row.GetValue<string>("reply_id");
                    int feedType = row.GetValue<int>("feed_type");
                    bool isSeen = row.GetValue<bool>("is_seen");
                    bool? isFetched = row.GetValue<bool?>("is_fetched");
                    int notificationSubType = row.GetValue<int>("notification_subtype");
                    int notificationType = row.GetValue<int>("notification_type");
                    DateTime? createdOnTimestamp = row.GetValue<DateTime?>("notification_created_on_timestamp");

                    if(createdOnTimestamp == null)
                    {
                        continue;
                    }

                    if(isFetched == null)
                    {
                        isFetched = false;
                    }

                    if (notificationType == (int)Notification.NotificationType.Feed)
                    {
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        updateBatch.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_feed_grouping",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        updateBatch.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        updateBatch.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));
                    }
                    else if (notificationType == (int)Notification.NotificationType.Challenge)
                    {
                        if (!challengeId.Equals("NA"))
                        {
                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                                new List<string> { "notification_id", "targeted_user_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_challenge",
                                new List<string> { "notification_id", "targeted_user_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                                new List<string> { "notification_id", "targeted_user_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, challengeId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));
                        }
                        else
                        {
                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                                new List<string> { "notification_id", "targeted_user_id", "achievement_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, achievementId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_achievement",
                                new List<string> { "notification_id", "targeted_user_id", "achievement_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, achievementId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                                   new List<string> { "notification_id", "targeted_user_id", "achievement_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, achievementId, isSeen, isFetched, notificationType, notificationSubType, createdOnTimestamp));
                        }
                    }

                    mainSession.Execute(updateBatch);
                    updateBatch = new BatchStatement();
                }

                Log.Error("Update notification completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public UpdateScriptResponse UpdateCompanyJob()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("company", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                Thread thread = new Thread(() => UpdateCompanyJobThread(rowSet, mainSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateCompanyJobThread(RowSet rowSet, ISession mainSession)
        {
            try
            {
                Company companyManager = new Company();
                BatchStatement updateBatch = new BatchStatement();
                DateTime currentTime = DateTime.UtcNow;
                foreach (Row row in rowSet)
                {
                    string companyId = row.GetValue<string>("id");
                    string adminUserId = row.GetValue<string>("primary_admin_user_id");
                    
                    foreach(BoundStatement bs in companyManager.CreateDefaultJobsStatement(companyId, adminUserId, currentTime, mainSession).Statements)
                    {
                        updateBatch.Add(bs);
                    }

                    mainSession.Execute(updateBatch);
                    updateBatch = new BatchStatement();
                }

                Log.Error("Update company jobs completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public UpdateScriptResponse UpdateUserLowestJob()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                Thread thread = new Thread(() => UpdateUserLowestJobThread(rowSet, mainSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateUserLowestJobThread(RowSet rowSet, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = null;
                DateTime currentTime = DateTime.UtcNow;
                List<Dictionary<string, string>> jobs = new List<Dictionary<string, string>>();

                BatchStatement batch = new BatchStatement();
                Company companyManager = new Company();

                foreach (Row row in rowSet)
                {
                    string userId = row.GetValue<string>("id");
                    string email = row.GetValue<string>("email");

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "user_id" }));
                    Row authenticationRow = mainSession.Execute(ps.Bind(email, userId)).FirstOrDefault();

                    if(authenticationRow != null)
                    {
                        string companyId = authenticationRow.GetValue<string>("company_id");

                        string jobId = string.Empty;
                        Dictionary<string, string> companyDict = jobs.FirstOrDefault(c => c["CompanyId"].Equals(companyId));
                        if (companyDict == null)
                        {
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("company_job_by_level", new List<string>(), new List<string> { "company_id", "level" }));
                            Row companyRow = mainSession.Execute(ps.Bind(companyId, 1)).FirstOrDefault();
                            jobId = companyRow.GetValue<string>("id");

                            companyDict = new Dictionary<string, string>();
                            companyDict["CompanyId"] = companyId;
                            companyDict["JobId"] = jobId;

                            jobs.Add(companyDict);
                        }
                        else
                        {
                            jobId = companyDict["JobId"];
                        }

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "job_level_id" }, new List<string>()));
                        batch.Add(ps.Bind(jobId, userId));

                        foreach(BoundStatement bs in companyManager.UpdateUserJobStatement(companyId, userId, jobId, mainSession).UpdateStatements)
                        {
                            batch.Add(bs);
                        }

                        mainSession.Execute(batch);
                        batch = new BatchStatement();
                    }

                }

                Log.Error("Update user jobs completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public UpdateScriptResponse UpdateAppraisalCommentRating()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_range_answered_by_pulse_sorted", new List<string>(), new List<string>()));
                RowSet rowSet = analyticSession.Execute(ps.Bind());

                Thread thread = new Thread(() => UpdateAppraisalCommentRatingThread(rowSet, mainSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateAppraisalCommentRatingThread(RowSet rowSet, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                foreach (Row row in rowSet)
                {
                    string cardId = row.GetValue<string>("card_id");
                    int rated = row.GetValue<int>("range_value");
                    string answeredByUserId = row.GetValue<string>("answered_by_user_id");
                    DateTime answeredOnTimestamp = row.GetValue<DateTime>("answered_on_timestamp");

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_comment", new List<string>(), new List<string> { "card_id", "user_id" }));
                    Row commentRow = mainSession.Execute(ps.Bind(cardId, answeredByUserId)).FirstOrDefault();

                    if(commentRow != null)
                    {
                        continue;
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_comment", new List<string> { "card_id", "rated", "user_id", "comment", "created_on_timestamp" }));
                    batch.Add(ps.Bind(cardId, rated, answeredByUserId, null, answeredOnTimestamp));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_comment_by_timestamp", new List<string> { "card_id", "rated", "user_id", "comment", "created_on_timestamp" }));
                    batch.Add(ps.Bind(cardId, rated, answeredByUserId, null, answeredOnTimestamp));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_comment_by_user", new List<string> { "card_id", "rated", "user_id", "comment", "created_on_timestamp" }));
                    batch.Add(ps.Bind(cardId, rated, answeredByUserId, null, answeredOnTimestamp));

                    mainSession.Execute(batch);
                    batch = new BatchStatement();
                }

                Log.Error("Update appraisal comment rating completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public UpdateScriptResponse UpdateDeletedUserJob()
        {
            UpdateScriptResponse response = new UpdateScriptResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                PreparedStatement ps = null;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("company_job_user", new List<string>(), new List<string>()));
                RowSet rowSet = mainSession.Execute(ps.Bind());

                Thread thread = new Thread(() => UpdateDeletedUserJobThread(rowSet, mainSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateDeletedUserJobThread(RowSet rowSet, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                User userManager = new User();
                foreach (Row row in rowSet)
                {
                    string companyId = row.GetValue<string>("company_id");
                    string userId = row.GetValue<string>("user_id");
                    string jobId = row.GetValue<string>("job_id");

                    User selectedUser = userManager.SelectUserBasic(userId, companyId, false, mainSession).User;
                    if (selectedUser == null || (selectedUser != null && selectedUser.Status.Code == User.AccountStatus.CODE_DELETED))
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("company_job_user", new List<string> { "company_id", "user_id", "job_id" }));
                        batch = batch.Add(ps.Bind(companyId, userId, jobId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("company_job_user_by_job", new List<string> { "company_id", "user_id", "job_id" }));
                        batch = batch.Add(ps.Bind(companyId, userId, jobId));

                        mainSession.Execute(batch);
                        batch = new BatchStatement();

                        //Log.Debug($"UserId: {userId}, CompanyId: {companyId}, Name:{selectedUser.FirstName} {selectedUser.LastName}");
                    }
                }

                Log.Debug("Update deleted user job completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public class TopicAttempt
        {
            public string CompanyId { get; set; }
            public string TopicId { get; set; }
            public int Correct { get; set; }
            public int Incorrect { get; set; }
        }

        public class QuestionAttempt
        {
            public string TopicId { get; set; }
            public string QuestionId { get; set; }
            public int Correct { get; set; }
            public int Incorrect { get; set; }
            public List<OptionAttempt> Options { get; set; }
        }

        public class UserAttempt
        {
            public string UserId { get; set; }
            public List<Attempt> Attempts { get; set; }
        }

        public class Attempt
        {
            public string TopicId { get; set; }
            public string QuestionId { get; set; }
            public List<AttemptTimestamp> Timestamps { get; set; }
        }

        public class AttemptTimestamp
        {
            public bool IsCorrect { get; set; }
            public DateTime Timestamp { get; set; }
        }

        public class OptionAttempt
        {
            public string TopicId { get; set; }
            public string OptionId { get; set; }
            public string QuestionId { get; set; }
            public int Count { get; set; }
        }
    }
}