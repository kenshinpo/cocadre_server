﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class AppraisalCardOption
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum AppraisalCardOptionQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember]
        public int OptionNumber { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }


        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public int NumberOfSelection { get; set; }

        public AppraisalOptionCreateStatementResponse CreateOptions(string cardId, List<AppraisalCardOption> options, ISession mainSession)
        {
            AppraisalOptionCreateStatementResponse response = new AppraisalOptionCreateStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int number = 1;
                foreach (AppraisalCardOption option in options)
                {
                    if (string.IsNullOrEmpty(option.Description))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalOptionMissingDescription);
                        response.ErrorMessage = ErrorMessage.AppraisalOptionMissingDescription;
                        return response;
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360_card_option",
                        new List<string> { "card_id", "title", "description", "number" }));
                    response.UpdateStatements.Add(ps.Bind(cardId, option.Title, option.Description, number));
                    number++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalOptionCreateStatementResponse CreateCustomOptions(string cardId, List<AppraisalCardOption> options, ISession mainSession)
        {
            AppraisalOptionCreateStatementResponse response = new AppraisalOptionCreateStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int number = 1;
                foreach (AppraisalCardOption option in options)
                {
                    if (string.IsNullOrEmpty(option.Description))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalOptionMissingDescription);
                        response.ErrorMessage = ErrorMessage.AppraisalOptionMissingDescription;
                        return response;
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_option",
                        new List<string> { "card_id", "title", "description", "number" }));
                    response.UpdateStatements.Add(ps.Bind(cardId, option.Title, option.Description, number));
                    number++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalOptionDeleteStatementResponse DeleteCustomOptions(string cardId, ISession mainSession)
        {
            AppraisalOptionDeleteStatementResponse response = new AppraisalOptionDeleteStatementResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_card_option",
                    new List<string> { "card_id" }));
                response.DeleteStatements.Add(ps.Bind(cardId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalOptionUpdateStatementResponse RemoveOptions(string cardId, ISession mainSession)
        {
            AppraisalOptionUpdateStatementResponse response = new AppraisalOptionUpdateStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("template_appraisal_360_card_option", new List<string> { "card_id" }));
                response.DeleteStatements.Add(ps.Bind(cardId));
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalOptionSelectStatementResponse SelectTemplateOptions(string cardId, ISession mainSession)
        {
            AppraisalOptionSelectStatementResponse response = new AppraisalOptionSelectStatementResponse();
            response.Options = new List<AppraisalCardOption>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_card_option", new List<string>(), new List<string> { "card_id" }));
                RowSet optionRowSet = mainSession.Execute(ps.Bind(cardId));
                foreach(Row optionRow in optionRowSet)
                {
                    response.Options.Add(new AppraisalCardOption
                    {
                        Title = optionRow.GetValue<string>("title"),
                        Description = optionRow.GetValue<string>("description"),
                        OptionNumber = optionRow.GetValue<int>("number")
                    });
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalOptionSelectStatementResponse SelectCustomOptions(string cardId, ISession mainSession)
        {
            AppraisalOptionSelectStatementResponse response = new AppraisalOptionSelectStatementResponse();
            response.Options = new List<AppraisalCardOption>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_option", new List<string>(), new List<string> { "card_id" }));
                RowSet optionRowSet = mainSession.Execute(ps.Bind(cardId));
                foreach (Row optionRow in optionRowSet)
                {
                    response.Options.Add(new AppraisalCardOption
                    {
                        Title = optionRow.GetValue<string>("title"),
                        Description = optionRow.GetValue<string>("description"),
                        OptionNumber = optionRow.GetValue<int>("number")
                    });
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
