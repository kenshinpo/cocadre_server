﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLTopic
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum MLTopicQueryType
        {
            Basic = 1,
            WithoutPrivacy = 2,
            Full = 3
        }

        public enum MLTopicStatusEnum
        {
            Deleted = -1,
            Unlisted = 1,
            Active = 2,
            Hidden = 3
        }

        [DataContract]
        [Serializable]
        public class MLTopicFeedback
        {
            [DataMember]
            public string Feedback { get; set; }
            [DataMember]
            public DateTime CreatedTimestamp { get; set; }
            [DataMember]
            public string CreatedTimestampString { get; set; }
        }

        public enum ProgressStatusEnum
        {
            Upcoming = 1,
            Live = 2,
            Completed = 3
        }

        public enum DurationTypeEnum
        {
            Perpetual = 1,
            Schedule = 2
        }

        public enum ReviewAnswerTypeEnum
        {
            OnlyQuestionsAnsweredIncorrectly = 1,
            AllQuestions = 2,
            NoQuestion = 3
        }

        public enum DisplayAnswerTypeEnum
        {
            EndOfTestOnly = 1,
            EndOfTestOrAnytimeAfter = 2
        }

        [DataMember]
        public string TopicId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string IconUrl { get; set; }

        [DataMember]
        public string Introduction { get; set; }

        [DataMember]
        public string Instructions { get; set; }

        [DataMember]
        public string ClosingWords { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int DurationType { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public String StartDateString { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public String EndDateString { get; set; }

        [DataMember]
        public int NumberOfCards { get; set; }

        [DataMember]
        public List<MLCard> Cards { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember]
        public bool IsForUser { get; set; }

        [DataMember]
        public List<User> TargetedUsers { get; set; }

        [DataMember]
        public int ProgressStatus { get; set; }

        [DataMember]
        public MLTopicCategory MLTopicCategory { get; set; }

        [DataMember]
        public bool IsRandomizedAllCards { get; set; }

        [DataMember]
        public string PassingGrade { get; set; }

        [DataMember]
        public User Author { get; set; }

        [DataMember]
        public bool IsDisplayAuthorOnClient { get; set; }

        [DataMember]
        public bool IsAllowRetestForFailures { get; set; }

        [DataMember]
        public bool IsAllowRetestForPasses { get; set; }

        [DataMember]
        public int ReviewAnswerType { get; set; }

        [DataMember]
        public int DisplayAnswerType { get; set; }

        [DataMember]
        public bool IsPassingGradeUsingPercentage { get; set; }

        [DataMember]
        public int PassingGradeInt { get; set; }

        [DataMember]
        public int RequestUserProgress { get; set; }

        [DataMember]
        public AnalyticLearning.MLTopicAttempt TopicAttempt { get; set; }

        [DataMember]
        public MLCard LastAttemptedCard { get; set; }


        public MLTopicCreateResponse CreateMLTopic(string adminUserId,
                                                   string companyId,
                                                   string title,
                                                   string introduction,
                                                   string instruction,
                                                   string closingWords,
                                                   string topicIconUrl,
                                                   string passingGrade,
                                                   string categoryId,
                                                   string categoryTitle,
                                                   int durationType,
                                                   List<string> targetedDepartmentIds,
                                                   List<string> targetedUserIds,
                                                   DateTime? startDate,
                                                   DateTime? endDate,
                                                   bool isDisplayAuthorOnClient = true,
                                                   bool isRandomizedAllCards = false,
                                                   bool isAllowRetestForFailures = true,
                                                   bool isAllowRetestForPasses = true,
                                                   int allowReviewAnswerType = 1,
                                                   int displayAnswerType = 2)
        {
            MLTopicCreateResponse response = new MLTopicCreateResponse();
            response.Topic = null;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                MLValidationResponse validateFieldResponse = ValidateTopicFields(title, passingGrade, topicIconUrl, introduction, closingWords);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                DateTime currentDate = DateTime.UtcNow;
                MLValidationResponse validateDateResponse = ValidateDate(durationType, currentDate, startDate, endDate);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                // Generate IDs
                string topicId = UUIDGenerator.GenerateUniqueIDForMLTopic();
                MLTopicCategory categoryManager = new MLTopicCategory();
                if (string.IsNullOrEmpty(categoryId))
                {
                    categoryId = UUIDGenerator.GenerateUniqueIDForMLCategory();
                    MLCategoryCreateResponse categoryResponse = categoryManager.CreateMLCategory(companyId, categoryTitle, adminUserId, session, categoryId);

                    if (!categoryResponse.Success)
                    {
                        Log.Error(categoryResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(categoryResponse.ErrorCode);
                        response.ErrorMessage = categoryResponse.ErrorMessage;
                        return response;
                    }
                }
                else
                {
                    Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);
                    if (categoryRow == null)
                    {
                        Log.Error("Invalid MLCategoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                        return response;
                    }
                }

                BatchStatement batch = new BatchStatement();
                List<BoundStatement> privacyStatments = CreatePrivacy(topicId, targetedDepartmentIds, targetedUserIds, session);

                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("ml_topic",
                    new List<string> { "id", "category_id", "title", "introduction", "instruction", "closing_words", "icon_url", "status", "is_display_author_on_client", "passing_grade",
                                       "duration_type", "is_randomized_all_questions", "is_allow_retest_for_failure", "is_allow_retest_for_pass", "allow_review_answer_type", "display_answer_type",
                                       "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp", "start_date", "end_date" }));
                batch.Add(ps.Bind(topicId, categoryId, title, introduction, instruction, closingWords, topicIconUrl, (int)MLTopicStatusEnum.Unlisted, isDisplayAuthorOnClient, passingGrade, durationType, isRandomizedAllCards, isAllowRetestForFailures, IsAllowRetestForPasses, allowReviewAnswerType, displayAnswerType, adminUserId, currentDate, adminUserId, currentDate, startDate, endDate));

                ps = session.Prepare(CQLGenerator.InsertStatement("mltopic_by_mlcategory",
                    new List<string> { "ml_topic_id", "ml_topic_category_id" }));
                batch.Add(ps.Bind(topicId, categoryId));

                ps = session.Prepare(CQLGenerator.InsertStatement("mlcategory_by_mltopic",
                    new List<string> { "ml_topic_id", "ml_topic_category_id" }));
                batch.Add(ps.Bind(topicId, categoryId));

                foreach (BoundStatement bs in privacyStatments)
                {
                    batch.Add(bs);
                }

                session.Execute(batch);

                response.Topic = SelectMLTopic((int)MLTopicQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, categoryManager.SelectMLCategory(categoryId, companyId, session).Category, companyId, topicId, null, null, null, session);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private MLValidationResponse ValidateTopicFields(string title, string passingGrade, string topicIconUrl, string introduction, string closingWords)
        {
            MLValidationResponse response = new MLValidationResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(title))
                {
                    Log.Error(ErrorMessage.MLTopicTitleEmpty);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicTitleEmpty);
                    response.ErrorMessage = ErrorMessage.MLTopicTitleEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(topicIconUrl))
                {
                    Log.Error(ErrorMessage.MLTopicIconMissing);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicIconMissing);
                    response.ErrorMessage = ErrorMessage.MLTopicIconMissing;
                    return response;
                }

                if (string.IsNullOrEmpty(introduction))
                {
                    Log.Error(ErrorMessage.MLTopicIntroductionEmpty);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicIntroductionEmpty);
                    response.ErrorMessage = ErrorMessage.MLTopicIntroductionEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(closingWords))
                {
                    Log.Error(ErrorMessage.MLTopicClosingWordsEmpty);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicClosingWordsEmpty);
                    response.ErrorMessage = ErrorMessage.MLTopicClosingWordsEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(passingGrade))
                {
                    passingGrade = passingGrade.Trim();
                    Log.Error(ErrorMessage.MLTopicMissingPassingGrade);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicMissingPassingGrade);
                    response.ErrorMessage = ErrorMessage.MLTopicMissingPassingGrade;
                    return response;
                }
                else
                {
                    bool containsPercent = passingGrade.Contains("%");
                    passingGrade = Regex.Replace(passingGrade, "[^0-9]", "");
                    passingGrade += containsPercent ? "%" : "";
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private MLValidationResponse ValidateDate(int durationType, DateTime currentDate, DateTime? startDate, DateTime? endDate)
        {
            MLValidationResponse response = new MLValidationResponse();
            response.Success = false;
            try
            {
                if (startDate == null)
                {
                    Log.Error(ErrorMessage.MLDateIsNull);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                    response.ErrorMessage = ErrorMessage.MLDateIsNull;
                    return response;
                }

                // End date cannot be null for schedule
                if (durationType == (int)DurationTypeEnum.Schedule)
                {
                    if (endDate == null)
                    {
                        Log.Error(ErrorMessage.MLDateIsNull);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                        response.ErrorMessage = ErrorMessage.MLDateIsNull;
                        return response;
                    }

                    if (endDate <= currentDate)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanToday);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanToday;
                        return response;
                    }

                    if (startDate >= endDate)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanStartDate;
                        return response;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLTopicUpdateResponse UpdateTopic(string topicId,
                                                 string adminUserId,
                                                 string companyId,
                                                 string newTitle,
                                                 string newIntroduction,
                                                 string newInstruction,
                                                 string newClosingWords,
                                                 string newTopicIconUrl,
                                                 string newPassingGrade,
                                                 string newCategoryId,
                                                 string newCategoryTitle,
                                                 int newDurationType,
                                                 int newStatus,
                                                 List<string> newTargetedDepartmentIds,
                                                 List<string> newTargetedUserIds,
                                                 DateTime newStartDate,
                                                 DateTime? newEndDate,
                                                 bool newIsDisplayAuthorOnClient = true,
                                                 bool newIsRandomizedAllCards = true,
                                                 bool newIsAllowRetestForFailures = true,
                                                 bool newIsAllowRetestForPasses = true,
                                                 int newAllowReviewAnswerType = 1,
                                                 int newDisplayAnswerType = 2)
        {
            MLTopicUpdateResponse response = new MLTopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check category
                PreparedStatement ps = null;
                bool isUpdateCategory = false;
                string currentCategoryId = string.Empty;

                if (!string.IsNullOrEmpty(newCategoryId))
                {
                    Row rsCategoryRow = vh.ValidateMLTopicCategory(companyId, newCategoryId, session);
                    if (rsCategoryRow == null)
                    {
                        Log.Error("Invalid MLCategoryId: " + newCategoryId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                        return response;
                    }
                }

                // Fetch current categoryId
                ps = session.Prepare(CQLGenerator.SelectStatement("mlcategory_by_mltopic", new List<string>(), new List<string> { "ml_topic_id" }));
                Row categoryByTopicRow = session.Execute(ps.Bind(topicId)).FirstOrDefault();

                if (categoryByTopicRow == null)
                {
                    Log.Error("Topic already been deleted: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.MLTopicAlreadyDeleted;
                    return response;
                }
                else
                {
                    currentCategoryId = categoryByTopicRow.GetValue<string>("ml_topic_category_id");

                    if (!currentCategoryId.Equals(newCategoryId))
                    {
                        isUpdateCategory = true;
                    }
                }

                // Check topic
                Row topicRow = vh.ValidateMLTopic(companyId, currentCategoryId, topicId, session);
                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                // Check status first
                int currentStatus = topicRow.GetValue<int>("status");

                DateTime currentDate = DateTime.UtcNow;
                DateTime currentStartDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? currentEndDate = topicRow.GetValue<DateTime?>("end_date");
                int progress = SelectProgress(currentDate, currentStartDate, currentEndDate);

                if (currentStatus != (int)MLTopicStatusEnum.Unlisted)
                {
                    // Status active or hidden
                    // Check progress
                    // Change of start date
                    if (currentStartDate != newStartDate)
                    {
                        if (progress != (int)ProgressStatusEnum.Upcoming)
                        {
                            Log.Error("Survey in progress, start date cannot be changed: " + topicId);
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicStartDateCannotBeModified);
                            response.ErrorMessage = ErrorMessage.MLTopicStartDateCannotBeModified;
                            return response;
                        }
                    }
                }

                MLValidationResponse validateFieldResponse = ValidateTopicFields(newTitle, newPassingGrade, newTopicIconUrl, newInstruction, newClosingWords);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                int durationType = topicRow.GetValue<int>("duration_type");
                MLValidationResponse validateDateResponse = ValidateDate(durationType, currentDate, newStartDate, newEndDate);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                // if date validated
                MLTopicCategory categoryManager = new MLTopicCategory();
                if (string.IsNullOrEmpty(newCategoryId))
                {
                    newCategoryId = UUIDGenerator.GenerateUniqueIDForMLCategory();
                    MLCategoryCreateResponse categoryResponse = categoryManager.CreateMLCategory(companyId, newCategoryTitle, adminUserId, session, newCategoryId);

                    if (!categoryResponse.Success)
                    {
                        Log.Error(categoryResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(categoryResponse.ErrorCode);
                        response.ErrorMessage = categoryResponse.ErrorMessage;
                        return response;
                    }
                }

                #region Check pass score. Total score must be equal or bigger than pass score.
                int totalScore = 0;
                // Step 1. Get card type  = select one and multi choice.
                int cardCount = 0;
                ps = session.Prepare(CQLGenerator.SelectStatement("ml_card", new List<string>(), new List<string> { "ml_topic_id" }));
                RowSet rsCards = session.Execute(ps.Bind(topicId));
                if (rsCards != null)
                {
                    foreach (Row rCard in rsCards)
                    {
                        if (rCard.GetValue<int>("type") == (int)MLCard.MLCardType.SelectOne || rCard.GetValue<int>("type") == (int)MLCard.MLCardType.MultiChoice)
                        {
                            cardCount++;

                            ps = session.Prepare(CQLGenerator.SelectStatement("ml_card_option", new List<string>(), new List<string> { "card_id" }));
                            RowSet rsOptions = session.Execute(ps.Bind(rCard.GetValue<string>("id")));
                            if (rsOptions != null)
                            {
                                if (rCard.GetValue<int>("type") == (int)MLCard.MLCardType.SelectOne)
                                {
                                    foreach (Row rOption in rsOptions)
                                    {
                                        if (rOption.GetValue<int>("score") > 0)
                                        {
                                            totalScore += rOption.GetValue<int>("score");
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    int selectedMaxCount = rCard.GetValue<int>("max_option_to_select");
                                    List<MLOption> options = new List<MLOption>();
                                    foreach (Row rOption in rsOptions)
                                    {
                                        options.Add(new MLOption { OptionId = rOption.GetValue<string>("id"), Score = rOption.GetValue<int>("score") });
                                    }

                                    options = options.OrderByDescending(o => o.Score).ToList();

                                    for (int i = 0; i < selectedMaxCount; i++)
                                    {
                                        totalScore += options[i].Score;
                                    }
                                }
                            }
                        }
                    }
                }

                if (cardCount < 1 && (newStatus == (int)MLTopic.MLTopicStatusEnum.Active || newStatus == (int)MLTopic.MLTopicStatusEnum.Hidden))
                {
                    Log.Error(ErrorMessage.MLTopicNoAnyQuestions);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicNoAnyQuestions);
                    response.ErrorMessage = string.Format(ErrorMessage.MLTopicNoAnyQuestions);
                    return response;
                }

                if (!newPassingGrade.Contains("%")) // by score, not %.
                {
                    int passScore = Convert.ToInt16(newPassingGrade);

                    if (totalScore < passScore && (newStatus == (int)MLTopic.MLTopicStatusEnum.Active || newStatus == (int)MLTopic.MLTopicStatusEnum.Hidden))
                    {
                        Log.Error(string.Format(ErrorMessage.MLTopicTotalScoreIsSmallerThanPassScore, passScore - totalScore));
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicTotalScoreIsSmallerThanPassScore);
                        response.ErrorMessage = string.Format(ErrorMessage.MLTopicTotalScoreIsSmallerThanPassScore, passScore - totalScore);
                        return response;
                    }
                }
                #endregion


                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                DateTime createdTimestamp = topicRow.GetValue<DateTime>("created_on_timestamp");
                string createdAdminId = topicRow.GetValue<string>("created_by_admin_id");

                if (isUpdateCategory)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_topic", new List<string> { "category_id", "id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, topicId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_topic_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, currentStartDate, topicId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("mltopic_by_mlcategory", new List<string> { "ml_topic_category_id", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, topicId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("mlcategory_by_mltopic", new List<string> { "ml_topic_category_id", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, topicId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("mltopic_by_mlcategory",
                        new List<string> { "ml_topic_id", "ml_topic_category_id" }));
                    updateBatch.Add(ps.Bind(topicId, newCategoryId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("mlcategory_by_mltopic",
                        new List<string> { "ml_topic_id", "ml_topic_category_id" }));
                    updateBatch.Add(ps.Bind(topicId, newCategoryId));
                }

                // Active -> insert into timestamp
                if (newStatus == (int)MLTopicStatusEnum.Active)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_topic_by_timestamp",
                        new List<string> { "ml_category_id", "start_on_timestamp", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, currentStartDate, topicId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_topic_by_timestamp",
                        new List<string> { "ml_category_id", "ml_topic_id", "start_on_timestamp" }));
                    updateBatch.Add(ps.Bind(newCategoryId, topicId, newStartDate));
                }
                else
                {
                    // Not active -> remove from timestamp
                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_topic_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(newCategoryId, currentStartDate, topicId));
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("ml_topic",
                   new List<string> { "id", "category_id" }, new List<string> { "title", "introduction", "instruction", "closing_words", "icon_url", "status", "is_display_author_on_client", "passing_grade",
                                       "duration_type", "is_randomized_all_questions", "is_allow_retest_for_failure", "is_allow_retest_for_pass", "allow_review_answer_type", "display_answer_type", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp", "start_date", "end_date" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newIntroduction, newInstruction, newClosingWords, newTopicIconUrl, newStatus, newIsDisplayAuthorOnClient, newPassingGrade, newDurationType, newIsRandomizedAllCards, newIsAllowRetestForFailures, newIsAllowRetestForPasses, newAllowReviewAnswerType, newDisplayAnswerType, createdAdminId, createdTimestamp, adminUserId, currentDate, newStartDate, newEndDate, topicId, newCategoryId));

                if (progress == (int)ProgressStatusEnum.Upcoming || currentStatus == (int)MLTopicStatusEnum.Unlisted)
                {
                    List<BoundStatement> privacyStatement = UpdatePrivacy(topicId, newTargetedDepartmentIds, newTargetedUserIds, session);
                    foreach (BoundStatement bs in privacyStatement)
                    {
                        updateBatch.Add(bs);
                    }
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Topic = SelectMLTopic((int)MLTopicQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, categoryManager.SelectMLCategory(newCategoryId, companyId, session).Category, companyId, topicId, null, null, null, session);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public List<User> SelectTargetedAudience(string topicId, string companyId, string searchId, ISession mainSession)
        {
            List<User> targetedAudience = new List<User>();
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_privacy",
                    new List<string>(), new List<string> { "ml_topic_id" }));
                Row privacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                if (privacyRow != null)
                {
                    bool isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                    bool isForDepartment = privacyRow.GetValue<bool>("is_for_department");
                    bool isForUser = privacyRow.GetValue<bool>("is_for_user");

                    User userManager = new User();

                    if (isForEveryone)
                    {
                        if (string.IsNullOrEmpty(searchId))
                        {
                            targetedAudience = userManager.GetAllUserForAdmin(null, companyId, null, 0, (int)User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession).Users;
                        }
                        else
                        {
                            targetedAudience = userManager.SelectAllUsersByDepartmentIds(new List<string> { searchId }, null, companyId, mainSession).Users;
                        }
                    }
                    else
                    {
                        if (isForDepartment)
                        {
                            List<string> departmentIds = new List<string>();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_department", new List<string>(), new List<string> { "ml_topic_id" }));
                            RowSet departmentRowset = mainSession.Execute(ps.Bind(topicId));

                            foreach (Row departmentRow in departmentRowset)
                            {
                                string departmentId = departmentRow.GetValue<string>("department_id");
                                if (string.IsNullOrEmpty(searchId))
                                {
                                    departmentIds.Add(departmentId);
                                }
                                else
                                {
                                    if (departmentId.Equals(searchId))
                                    {
                                        departmentIds.Add(departmentId);
                                    }
                                }

                            }

                            targetedAudience = userManager.SelectAllUsersByDepartmentIds(departmentIds, null, companyId, mainSession).Users;
                        }

                        if (isForUser)
                        {
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_user", new List<string>(), new List<string> { "ml_topic_id" }));
                            RowSet userRowset = mainSession.Execute(ps.Bind(topicId));

                            foreach (Row userRow in userRowset)
                            {
                                string userId = userRow.GetValue<string>("user_id");
                                if (targetedAudience.FirstOrDefault(u => u.UserId == userId) == null)
                                {
                                    User user = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                                    if (user != null)
                                    {
                                        if (string.IsNullOrEmpty(searchId))
                                        {
                                            targetedAudience.Add(user);
                                        }
                                        else
                                        {
                                            if (user.Departments.FirstOrDefault(d => d.Id.Equals(searchId)) != null)
                                            {
                                                targetedAudience.Add(user);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return targetedAudience;
        }

        private List<BoundStatement> UpdatePrivacy(string topicId, List<string> newTargetedDepartmentIds, List<string> newTargetedUserIds, ISession session)
        {
            try
            {
                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_department",
                    new List<string>(), new List<string> { "ml_topic_id" }));
                RowSet departmentPrivacyRowset = session.Execute(ps.Bind(topicId));

                foreach (Row departmentPrivacyRow in departmentPrivacyRowset)
                {
                    string departmentId = departmentPrivacyRow.GetValue<string>("department_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_department_targeted_topic", new List<string> { "department_id", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(departmentId, topicId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_topic_targeted_department", new List<string> { "ml_topic_id" }));
                deleteBatch.Add(ps.Bind(topicId));

                ps = session.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_user",
                    new List<string>(), new List<string> { "ml_topic_id" }));
                RowSet userPrivacyRowset = session.Execute(ps.Bind(topicId));

                foreach (Row userPrivacyRow in userPrivacyRowset)
                {
                    string userId = userPrivacyRow.GetValue<string>("user_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_user_targeted_topic", new List<string> { "user_id", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(userId, topicId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_topic_targeted_user", new List<string> { "ml_topic_id" }));
                deleteBatch.Add(ps.Bind(topicId));

                session.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return CreatePrivacy(topicId, newTargetedDepartmentIds, newTargetedUserIds, session);
        }

        private List<BoundStatement> CreatePrivacy(string topicId, List<string> targetedDepartmentIds, List<string> targetedUserIds, ISession session)
        {
            List<BoundStatement> boundStatements = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;
                bool isForDepartment = targetedDepartmentIds == null || targetedDepartmentIds.Count == 0 ? false : true;
                bool isForUser = targetedUserIds == null || targetedUserIds.Count == 0 ? false : true;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_topic_privacy", new List<string> { "ml_topic_id", "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group" }));
                boundStatements.Add(ps.Bind(topicId, isForEveryone, isForDepartment, isForUser, false));

                foreach (string departmentId in targetedDepartmentIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_department_targeted_topic", new List<string> { "department_id", "ml_topic_id" }));
                    boundStatements.Add(ps.Bind(departmentId, topicId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_topic_targeted_department", new List<string> { "department_id", "ml_topic_id" }));
                    boundStatements.Add(ps.Bind(departmentId, topicId));
                }

                foreach (string userId in targetedUserIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_topic_targeted_user", new List<string> { "user_id", "ml_topic_id" }));
                    boundStatements.Add(ps.Bind(userId, topicId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_user_targeted_topic", new List<string> { "user_id", "ml_topic_id" }));
                    boundStatements.Add(ps.Bind(userId, topicId));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatements;
        }

        private bool CheckPrivacy(string topicId, string userId, bool isForEveryone, bool isForDepartment, bool isForUser, List<string> departmentIds, ISession session)
        {
            bool isForCurrentUser = false;

            if (isForEveryone)
            {
                isForCurrentUser = true;
            }
            else
            {
                if (isForDepartment)
                {
                    foreach (string departmentId in departmentIds)
                    {
                        PreparedStatement psTargetedDepartment = session.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_department",
                            new List<string>(), new List<string> { "ml_topic_id", "department_id" }));
                        Row departmentPrivacyRow = session.Execute(psTargetedDepartment.Bind(topicId, departmentId)).FirstOrDefault();

                        if (departmentPrivacyRow != null)
                        {
                            isForCurrentUser = true;
                            break;
                        }
                    }
                }

                if (!isForCurrentUser && isForUser)
                {
                    PreparedStatement psFeedTargetedUser = session.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_user",
                            new List<string>(), new List<string> { "ml_topic_id", "user_id" }));
                    Row userPrivacyRow = session.Execute(psFeedTargetedUser.Bind(topicId, userId)).FirstOrDefault();

                    if (userPrivacyRow != null)
                    {
                        isForCurrentUser = true;
                    }
                }
            }

            return isForCurrentUser;
        }

        public MLTopicSelectAllBasicResponse SelectAllBasicByCategory(string adminUserId, string companyId, string selectedCategoryId = null, string containsName = null)
        {
            MLTopicSelectAllBasicResponse response = new MLTopicSelectAllBasicResponse();
            response.Topics = new List<MLTopic>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                RowSet categoryRowset = null;

                if (string.IsNullOrEmpty(selectedCategoryId))
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_category", new List<string>(), new List<string> { "company_id", "is_valid" }));
                    categoryRowset = mainSession.Execute(ps.Bind(companyId, true));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_category", new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                    categoryRowset = mainSession.Execute(ps.Bind(companyId, selectedCategoryId, true));
                }

                foreach (Row categoryRow in categoryRowset)
                {
                    string categoryId = categoryRow.GetValue<string>("id");
                    string title = categoryRow.GetValue<string>("title");

                    MLTopicCategory category = new MLTopicCategory
                    {
                        CategoryId = categoryId,
                        Title = title
                    };

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("mltopic_by_mlcategory", new List<string>(), new List<string> { "ml_topic_category_id" }));
                    RowSet topicRowset = mainSession.Execute(ps.Bind(categoryId));

                    foreach (Row topicRow in topicRowset)
                    {
                        string topicId = topicRow.GetValue<string>("ml_topic_id");
                        MLTopic selectedTopic = SelectMLTopic((int)MLTopicQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, category, companyId, topicId, containsName, null, null, mainSession, analyticSession);

                        if (selectedTopic != null)
                        {
                            response.Topics.Add(selectedTopic);
                        }
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public int SelectProgress(DateTime currentDate, DateTime startDate, DateTime? endDate)
        {
            int progress = (int)ProgressStatusEnum.Upcoming;
            try
            {
                if (startDate <= currentDate)
                {
                    progress = (int)ProgressStatusEnum.Live;

                    if (endDate.HasValue && endDate <= currentDate)
                    {
                        progress = (int)ProgressStatusEnum.Completed;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return progress;
        }

        private MLTopic SelectMLTopic(int topicQueryType, int cardQueryType, MLTopicCategory category, string companyId, string topicId, string containsTopicName = null, string containsCardName = null, string answeredByUserId = null, ISession mainSession = null, ISession analyticSession = null)
        {
            MLTopic topic = null;

            try
            {
                PreparedStatement ps = null;

                if (category == null)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("mlcategory_by_mltopic",
                        new List<string> { "ml_topic_category_id" }, new List<string> { "ml_topic_id" }));
                    Row categoryByTopicRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                    if (categoryByTopicRow != null)
                    {
                        string categoryId = categoryByTopicRow.GetValue<string>("ml_topic_category_id");
                        category = new MLTopicCategory().SelectMLCategory(categoryId, companyId, mainSession).Category;
                    }
                    else
                    {
                        Log.Error("MLTopic does not have a MLCategory: " + topicId);
                    }
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic", new List<string>(), new List<string> { "category_id", "id" }));
                Row topicRow = mainSession.Execute(ps.Bind(category.CategoryId, topicId)).FirstOrDefault();

                int status = topicRow.GetValue<int>("status");

                // Check status
                if (status == (int)MLTopicStatusEnum.Deleted)
                {
                    return topic;
                }

                string title = topicRow.GetValue<string>("title");
                // Check for topic title
                if (!string.IsNullOrEmpty(containsTopicName))
                {
                    if (!title.ToLower().Contains(containsTopicName.ToLower()))
                    {
                        return topic;
                    }
                }

                // Check progress first
                int durationType = topicRow.GetValue<int>("duration_type");
                DateTime currentDate = DateTime.UtcNow;
                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");
                int progressStatus = SelectProgress(currentDate, startDate, endDate);

                string iconUrl = topicRow.GetValue<string>("icon_url");
                int numberOfCards = new MLCard().SelectNumberOfCards(topicId, mainSession);

                string introduction = topicRow.GetValue<string>("introduction");
                string instruction = topicRow.GetValue<string>("instruction");
                string closingWords = topicRow.GetValue<string>("closing_words");
                bool isForEveryone = false;
                bool isForDepartment = false;
                List<Department> selectedDepartments = new List<Department>();
                bool isForUser = false;
                List<User> selectedUsers = new List<User>();
                bool isDisplayAuthorOnClient = topicRow.GetValue<bool>("is_display_author_on_client");
                string passingGrade = topicRow.GetValue<string>("passing_grade");
                bool isUsingPercentage = false;

                if (passingGrade.Contains("%"))
                {
                    isUsingPercentage = true;
                }

                int passingGradeInt = -1;
                try
                {
                    passingGradeInt = Convert.ToInt16(passingGrade.Replace("%", ""));
                }
                catch (Exception ex)
                {
                    Log.Error("Evaluation Passing Grade can not convert to Int.");
                    passingGradeInt = -1;
                }


                List<MLCard> cards = new List<MLCard>();

                MLCard lastAttemptedCard = null;
                bool isRandomizedAllCards = false;
                bool isAllowRetestForFailures = topicRow.GetValue<bool>("is_allow_retest_for_failure");
                bool isAllowRetestForPasses = topicRow.GetValue<bool>("is_allow_retest_for_pass");
                int reviewAnswerType = topicRow.GetValue<int>("allow_review_answer_type");
                int displayAnswerType = topicRow.GetValue<int>("display_answer_type");
                AnalyticLearning.MLTopicAttempt topicAttempt = null;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_privacy", new List<string>(), new List<string> { "ml_topic_id" }));
                Row topicPrivacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");

                User author = new User().SelectUserBasic(topicRow.GetValue<string>("created_by_admin_id"), companyId, false, mainSession, null, true).User;

                if (topicQueryType > (int)MLTopicQueryType.Basic)
                {
                    if (!string.IsNullOrEmpty(answeredByUserId))
                    {
                        double timeoffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                        topicAttempt = new AnalyticLearning().SelectMLAttempt(topicId, answeredByUserId, timeoffset, analyticSession);

                        //// Check if can retake
                        //if((topicAttempt.ResultCode == (int)AnalyticLearning.MLTopicResultTypeEnum.Passed && !isAllowRetestForPasses) || (topicAttempt.ResultCode == (int)AnalyticLearning.MLTopicResultTypeEnum.Failed && !isAllowRetestForFailures))
                        //{
                        //    return topic;
                        //}
                    }

                    cards = new MLCard().SelectAllCards(topicId, category.CategoryId, null, companyId, cardQueryType, containsCardName, topicAttempt, answeredByUserId, mainSession, analyticSession).Cards;

                    if (!string.IsNullOrEmpty(answeredByUserId))
                    {
                        if (topicAttempt.CurrentIncompleteAttempt == null || topicAttempt.CurrentIncompleteAttempt.Attempt.Equals("NA"))
                        {
                            lastAttemptedCard = cards[0];
                        }
                        else
                        {
                            int unattemptedCount = (from card in cards
                                                    where card.HasAttemptedByUser == false
                                                    select card).Count();

                            if (unattemptedCount == cards.Count)
                            {
                                lastAttemptedCard = cards[0];
                            }
                            else
                            {
                                lastAttemptedCard = cards.FirstOrDefault(c => !c.HasAttemptedByUser && (c.Type == (int)MLCard.MLCardType.MultiChoice || c.Type == (int)MLCard.MLCardType.SelectOne));
                            }
                        }

                        // User quit on last card which does not need answering
                        if (lastAttemptedCard == null)
                        {
                            MLSelectUserResultResponse resultResponse = SelectUserResult(answeredByUserId, companyId, topicId, category.CategoryId, passingGrade, mainSession, analyticSession);
                            if (resultResponse.Success)
                            {
                                return SelectMLTopic(topicQueryType, cardQueryType, category, companyId, topicId, null, null, answeredByUserId, mainSession, analyticSession);
                            }
                            else
                            {
                                Log.Error("User did not complete previous attempt for ML Evaluation. Check!!");
                                Log.Error("UserId: " + answeredByUserId);
                                Log.Error("TopicId: " + topicId);
                                lastAttemptedCard = cards[0];
                            }
                        }
                    }

                    if (topicQueryType > (int)MLTopicQueryType.WithoutPrivacy)
                    {
                        if (!isForEveryone)
                        {
                            if (isForDepartment)
                            {
                                Department departmentManager = new Department();
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_department", new List<string>(), new List<string> { "ml_topic_id" }));
                                RowSet departmentRowset = mainSession.Execute(ps.Bind(topicId));
                                foreach (Row departmentRow in departmentRowset)
                                {
                                    string departmentId = departmentRow.GetValue<string>("department_id");
                                    Department department = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;
                                    if (department != null)
                                    {
                                        selectedDepartments.Add(department);
                                    }
                                }
                            }

                            if (isForUser)
                            {
                                User userManager = new User();
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_targeted_user", new List<string>(), new List<string> { "ml_topic_id" }));
                                RowSet userRowset = mainSession.Execute(ps.Bind(topicId));
                                foreach (Row userRow in userRowset)
                                {
                                    string userId = userRow.GetValue<string>("user_id");
                                    User user = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                                    if (user != null)
                                    {
                                        selectedUsers.Add(user);
                                    }
                                }
                            }
                        }
                    }
                }

                topic = new MLTopic
                {
                    TopicId = topicId,
                    Title = title,
                    IconUrl = iconUrl,
                    Introduction = introduction,
                    Instructions = instruction,
                    ClosingWords = closingWords,
                    MLTopicCategory = category,
                    IsForEveryone = isForEveryone,
                    IsForDepartment = isForDepartment,
                    TargetedDepartments = selectedDepartments,
                    IsForUser = isForUser,
                    TargetedUsers = selectedUsers,
                    PassingGrade = passingGrade,
                    Status = status,
                    DurationType = durationType,
                    IsRandomizedAllCards = isRandomizedAllCards,
                    IsAllowRetestForFailures = isAllowRetestForFailures,
                    IsAllowRetestForPasses = isAllowRetestForPasses,
                    ReviewAnswerType = reviewAnswerType,
                    DisplayAnswerType = displayAnswerType,
                    Cards = cards,
                    NumberOfCards = cards.Count,
                    StartDate = new DateTime(startDate.Ticks, DateTimeKind.Utc),
                    EndDate = endDate == null ? null : new DateTime?(new DateTime(endDate.Value.Ticks, DateTimeKind.Utc)),
                    ProgressStatus = progressStatus,
                    IsDisplayAuthorOnClient = isDisplayAuthorOnClient,
                    Author = author,
                    TopicAttempt = topicAttempt,
                    IsPassingGradeUsingPercentage = isUsingPercentage,
                    PassingGradeInt = passingGradeInt,
                    LastAttemptedCard = lastAttemptedCard
                };

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topic;
        }

        public MLTopicSelectResponse SelectMLTopicByUser(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            MLTopicSelectResponse response = new MLTopicSelectResponse();
            response.Topic = new MLTopic();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }
                MLTopicCategory category = new MLTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");

                // Track progress
                DateTime currentTime = DateTime.UtcNow;
                if (endDate.HasValue && endDate.Value <= currentTime)
                {
                    Log.Error("MLearning has ended: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEnded);
                    response.ErrorMessage = ErrorMessage.MLTopicEnded;
                    return response;
                }

                response.Topic = SelectMLTopic((int)MLTopicQueryType.Full, (int)MLCard.MLCardQueryType.Full, category, companyId, topicId, null, null, requesterUserId, mainSession, analyticSession);

                //if(response.Topic == null)
                //{
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.MLNotAllowToRetake);
                //    response.ErrorMessage = ErrorMessage.MLNotAllowToRetake;
                //    return response;
                //}

                // Update analytics
                new AnalyticLearning().StartMLActivity(requesterUserId, topicId, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLTopicSelectResponse SelectFullDetailMLTopic(string adminUserId, string companyId, string topicId, string categoryId, string containsCardName = null)
        {
            MLTopicSelectResponse response = new MLTopicSelectResponse();
            response.Topic = new MLTopic();
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                MLTopicCategory category = new MLTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }
                if (string.IsNullOrEmpty(containsCardName))
                {
                    response.Topic = SelectMLTopic((int)MLTopicQueryType.Full, (int)MLCard.MLCardQueryType.Basic, category, companyId, topicId, null, containsCardName, null, mainSession);
                }
                else
                {
                    response.Topic = SelectMLTopic((int)MLTopicQueryType.Full, (int)MLCard.MLCardQueryType.Full, category, companyId, topicId, null, containsCardName, null, mainSession);
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLTopicSelectAllByCategoryResponse SelectAllBasicByUser(string requesterUserId, string companyId, bool isShowCompleted = false, ISession mainSession = null, ISession analyticSession = null)
        {
            MLTopicSelectAllByCategoryResponse response = new MLTopicSelectAllByCategoryResponse();
            response.MLCategories = new List<MLTopicCategory>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ValidationHandler vh = new ValidationHandler();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_category_by_title", new List<string>(), new List<string> { "company_id" }));
                RowSet categoryByTitleRowset = mainSession.Execute(ps.Bind(companyId));

                AnalyticLearning analyticsManager = new AnalyticLearning();
                Department departmentManager = new Department();
                User userManager = new User();
                MLCard cardManager = new MLCard();

                foreach (Row categoryByTitleRow in categoryByTitleRowset)
                {
                    string categoryId = categoryByTitleRow.GetValue<string>("ml_category_id");

                    Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);

                    if (categoryRow == null)
                    {
                        continue;
                    }

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_by_timestamp", new List<string>(), new List<string> { "ml_category_id" }));
                    RowSet topicByCategoryRowset = mainSession.Execute(ps.Bind(categoryId));

                    string categoryTitle = categoryRow.GetValue<string>("title");
                    MLTopicCategory category = new MLTopicCategory
                    {
                        CategoryId = categoryId,
                        Title = categoryTitle
                    };
                    category.Topics = new List<MLTopic>();

                    DateTime currentTime = DateTime.UtcNow;
                    foreach (Row topicByCategoryRow in topicByCategoryRowset)
                    {
                        bool isCompleted = false;
                        string topicId = topicByCategoryRow.GetValue<string>("ml_topic_id");
                        Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                        if (topicRow == null)
                        {
                            continue;
                        }

                        // MLearning upcoming
                        if (currentTime < topicRow.GetValue<DateTime>("start_date"))
                        {
                            continue;
                        }

                        // Survey ended
                        if (topicRow["end_date"] != null && currentTime >= topicRow.GetValue<DateTime>("end_date"))
                        {
                            continue;
                        }

                        if (topicRow.GetValue<int>("status") != (int)MLTopicStatusEnum.Active)
                        {
                            continue;
                        }

                        if (cardManager.SelectNumberOfCards(topicId, mainSession) <= 0)
                        {
                            continue;
                        }

                        int requestUserProgress = analyticsManager.SelectProgressByUser(topicId, requesterUserId, analyticSession);

                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_topic_privacy", new List<string>(), new List<string> { "ml_topic_id" }));
                        Row privacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                        bool isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                        bool isForUser = privacyRow.GetValue<bool>("is_for_user");
                        bool isForDepartment = privacyRow.GetValue<bool>("is_for_department");

                        List<Department> departments = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;
                        List<string> departmentIds = new List<string>();

                        foreach (Department department in departments)
                        {
                            departmentIds.Add(department.Id);
                        }

                        if (!CheckPrivacy(topicId, requesterUserId, isForEveryone, isForDepartment, isForUser, departmentIds, mainSession))
                        {
                            continue;
                        }

                        MLTopic topic = SelectMLTopic((int)MLTopicQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, category, companyId, topicId, null, null, null, mainSession);
                        topic.RequestUserProgress = requestUserProgress;
                        category.Topics.Add(topic);
                    }

                    if (category.Topics.Count > 0)
                    {
                        response.MLCategories.Add(category);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLTopicUpdateResponse UpdateTopicStatus(string topicId, string categoryId, string adminUserId, string companyId, int updatedStatus)
        {
            MLTopicUpdateResponse response = new MLTopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }
                MLTopicCategory category = new MLTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                #region Check pass score. Total score must be equal or bigger than pass score.
                int totalScore = 0;
                // Step 1. Get card type  = select one and multi choice.
                List<MLCard> cards = new List<MLCard>();
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_card", new List<string>(), new List<string> { "ml_topic_id" }));
                RowSet rsCards = session.Execute(ps.Bind(topicId));
                int cardCount = 0;
                if (rsCards != null)
                {
                    foreach (Row rCard in rsCards)
                    {
                        cardCount++;
                        if (rCard.GetValue<int>("type") == (int)MLCard.MLCardType.SelectOne || rCard.GetValue<int>("type") == (int)MLCard.MLCardType.MultiChoice)
                        {
                            ps = session.Prepare(CQLGenerator.SelectStatement("ml_card_option", new List<string>(), new List<string> { "card_id" }));
                            RowSet rsOptions = session.Execute(ps.Bind(rCard.GetValue<string>("id")));
                            if (rsOptions != null)
                            {
                                if (rCard.GetValue<int>("type") == (int)MLCard.MLCardType.SelectOne)
                                {
                                    foreach (Row rOption in rsOptions)
                                    {
                                        if (rOption.GetValue<int>("score") > 0)
                                        {
                                            totalScore += rOption.GetValue<int>("score");
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    int selectedMaxCount = rCard.GetValue<int>("max_option_to_select");
                                    List<MLOption> options = new List<MLOption>();
                                    foreach (Row rOption in rsOptions)
                                    {
                                        options.Add(new MLOption { OptionId = rOption.GetValue<string>("id"), Score = rOption.GetValue<int>("score") });
                                    }

                                    options = options.OrderByDescending(o => o.Score).ToList();

                                    for (int i = 0; i < selectedMaxCount; i++)
                                    {
                                        totalScore += options[i].Score;
                                    }
                                }
                            }
                        }
                    }
                }

                if (cardCount < 1 && (updatedStatus == (int)MLTopic.MLTopicStatusEnum.Active || updatedStatus == (int)MLTopic.MLTopicStatusEnum.Hidden))
                {
                    Log.Error(ErrorMessage.MLCardCountMustMoreThenZero);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardCountMustMoreThenZero);
                    response.ErrorMessage = string.Format(ErrorMessage.MLCardCountMustMoreThenZero);
                    return response;
                }

                if (!topicRow.GetValue<string>("passing_grade").Contains("%")) // by score, not %.
                {
                    int passScore = Convert.ToInt16(topicRow.GetValue<string>("passing_grade"));

                    if (totalScore < passScore && (updatedStatus == (int)MLTopic.MLTopicStatusEnum.Active || updatedStatus == (int)MLTopic.MLTopicStatusEnum.Hidden))
                    {
                        Log.Error(string.Format(ErrorMessage.MLTopicTotalScoreIsSmallerThanPassScore, passScore - totalScore));
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicTotalScoreIsSmallerThanPassScore);
                        response.ErrorMessage = string.Format(ErrorMessage.MLTopicTotalScoreIsSmallerThanPassScore, passScore - totalScore);
                        return response;
                    }
                }
                #endregion

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                ps = null;

                int currentStatus = topicRow.GetValue<int>("status");

                if (currentStatus == (int)MLTopicStatusEnum.Deleted)
                {
                    Log.Error("Topic already been deleted");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.MLTopicAlreadyDeleted;
                    return response;
                }

                DateTime startTime = topicRow.GetValue<DateTime>("start_date");

                if (updatedStatus == (int)MLTopicStatusEnum.Deleted || updatedStatus == (int)MLTopicStatusEnum.Hidden)
                {
                    if (updatedStatus == (int)MLTopicStatusEnum.Deleted)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("mltopic_by_mlcategory", new List<string> { "ml_topic_category_id", "ml_topic_id" }));
                        deleteBatch.Add(ps.Bind(categoryId, topicId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("mlcategory_by_mltopic", new List<string> { "ml_topic_category_id", "ml_topic_id" }));
                        deleteBatch.Add(ps.Bind(categoryId, topicId));

                        // Delete all cards
                        new MLCard().DeleteAllCards(topicId, companyId, session);
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_topic_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_topic_id" }));
                    deleteBatch.Add(ps.Bind(categoryId, startTime, topicId));
                }
                else if (updatedStatus == (int)MLTopicStatusEnum.Active)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_topic_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_topic_id" }));
                    updateBatch.Add(ps.Bind(categoryId, startTime, topicId));
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("ml_topic", new List<string> { "category_id", "id" }, new List<string> { "status", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(updatedStatus, adminUserId, DateTime.UtcNow, categoryId, topicId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLSelectUserResultResponse SelectUserResult(string answeredByUserId, string companyId, string topicId, string topicCategoryId, string passingGrade = null, ISession mainSession = null, ISession analyticSession = null)
        {
            MLSelectUserResultResponse response = new MLSelectUserResultResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if(mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    Row topicRow = vh.ValidateMLTopic(companyId, topicCategoryId, topicId, mainSession);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                        response.ErrorMessage = ErrorMessage.TopicInvalid;
                        return response;
                    }

                    passingGrade = topicRow.GetValue<string>("passing_grade");
                }

                if(analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                AnalyticLearning analyticManager = new AnalyticLearning();
                int attempt = analyticManager.SelectCurrentAttemptByUser(analyticSession, answeredByUserId, topicId);
                if (attempt == 0)
                {
                    Log.Error(ErrorMessage.MLProgressNotCaptured);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLProgressNotCaptured);
                    response.ErrorMessage = ErrorMessage.MLProgressNotCaptured;
                    return response;
                }

                List<MLCard> cards = new MLCard().SelectAllCards(topicId, topicCategoryId, null, companyId, (int)MLCard.MLCardQueryType.Full, null, null, null, mainSession, analyticSession).Cards;
                response = analyticManager.SelectUserResult(answeredByUserId, companyId, attempt, topicId, passingGrade, cards, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLTopicUpdateFeedbackResponse CreateFeedback(string userId, string companyId, string topicId, string topicCategoryId, string feedback)
        {
            MLTopicUpdateFeedbackResponse response = new MLTopicUpdateFeedbackResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(userId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateMLTopic(companyId, topicCategoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                if (!string.IsNullOrEmpty(feedback.Trim()))
                {
                    PreparedStatement ps = mainSession.Prepare(CQLGenerator.UpdateStatement("ml_feedback",
                        new List<string> { "ml_topic_id", "user_id" }, new List<string> { "feedback", "created_on_timestamp" }, new List<string>()));
                    mainSession.Execute(ps.Bind(feedback.Trim(), DateTime.UtcNow, topicId, userId));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLTopicSelectFeedbackResponse SelectFeedback(string adminUserId, string companyId, string topicId, string categoryId)
        {
            MLTopicSelectFeedbackResponse response = new MLTopicSelectFeedbackResponse();
            response.Feedbacks = new List<MLTopicFeedback>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_feedback", new List<string>(), new List<string> { "ml_topic_id" }));
                RowSet feedbackRowSet = mainSession.Execute(ps.Bind(topicId));

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                foreach (Row feedbackRow in feedbackRowSet)
                {
                    string feedback = feedbackRow.GetValue<string>("feedback");
                    DateTime createdTimestamp = feedbackRow.GetValue<DateTime>("created_on_timestamp");
                    createdTimestamp = createdTimestamp.AddHours(timezoneOffset);
                    string createdTimestampString = createdTimestamp.ToString("dd/MM/yyyy");

                    MLTopicFeedback content = new MLTopicFeedback
                    {
                        Feedback = feedback,
                        CreatedTimestamp = createdTimestamp,
                        CreatedTimestampString = createdTimestampString
                    };

                    response.Feedbacks.Add(content);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLSelectCandidateResultResponse SelectCandidateResult(string adminUserId, string companyId, string topicId, string categoryId, string searchId)
        {
            MLSelectCandidateResultResponse response = new MLSelectCandidateResultResponse();
            response.CandidateResult = new AnalyticLearning.MLTopicCandidateResult();
            response.SearchTerms = new List<AnalyticLearning.MLFilteredDepartmentSearchTerm>();
            response.Topic = new MLTopic();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                MLTopicCategory category = new MLTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                // Get targeted audience
                List<User> allTargetedAudience = SelectTargetedAudience(topicId, companyId, null, mainSession);

                List<User> searchedAudience = !string.IsNullOrEmpty(searchId) ? SelectTargetedAudience(topicId, companyId, searchId, mainSession) : allTargetedAudience;

                // Get searchIds
                List<AnalyticLearning.MLFilteredDepartmentSearchTerm> searchTerms = new List<AnalyticLearning.MLFilteredDepartmentSearchTerm>();

                foreach (User user in allTargetedAudience)
                {
                    foreach (Department department in user.Departments)
                    {
                        if (!searchTerms.Any(s => s.Id.Equals(department.Id)))
                        {
                            searchTerms.Add(
                                new AnalyticLearning.MLFilteredDepartmentSearchTerm
                                {
                                    Id = department.Id,
                                    Content = department.Title,
                                    IsSelected = department.Id.Equals(searchId) ? true : false
                                }
                            );
                        }
                    }
                }

                searchTerms = searchTerms.OrderByDescending(s => s.IsSelected).ThenBy(s => s.Content).ToList();

                AnalyticLearning.MLFilteredDepartmentSearchTerm allDepartmentSearchTerm = new AnalyticLearning.MLFilteredDepartmentSearchTerm
                {
                    Id = string.Empty,
                    Content = "All Departments",
                    IsSelected = string.IsNullOrEmpty(searchId) ? true : false
                };

                if (string.IsNullOrEmpty(searchId))
                {
                    searchTerms.Insert(0, allDepartmentSearchTerm);
                }
                else
                {
                    searchTerms.Insert(1, allDepartmentSearchTerm);
                }

                response.SearchTerms = searchTerms;

                response.Topic = SelectMLTopic((int)MLTopicQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, category, companyId, topicId, null, null, null, mainSession, analyticSession);

                // Set up datetime string
                double timeoffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                response.Topic.StartDateString = response.Topic.StartDate.AddHours(timeoffset).ToString("dd/MM/yyyy");
                if (response.Topic.DurationType == (int)DurationTypeEnum.Schedule)
                {
                    response.Topic.EndDateString = response.Topic.EndDate.Value.AddHours(timeoffset).ToString("dd/MM/yyyy");
                }

                new AnalyticLearning().SelectCandidateResult(searchedAudience, response, timeoffset, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLSelectQuestionResultResponse SelectQuestionResult(string adminUserId, string companyId, string topicId, string categoryId, string searchId, string questionToggleType)
        {
            MLSelectQuestionResultResponse response = new MLSelectQuestionResultResponse();
            response.PreviewCard = null;
            response.SearchTerms = new List<AnalyticLearning.MLFilteredDepartmentSearchTerm>();
            response.Topic = new MLTopic();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                MLTopicCategory category = new MLTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                // Get targeted audience
                List<User> allTargetedAudience = SelectTargetedAudience(topicId, companyId, null, mainSession);

                List<User> searchedAudience = !string.IsNullOrEmpty(searchId) ? SelectTargetedAudience(topicId, companyId, searchId, mainSession) : allTargetedAudience;

                // Get searchIds
                List<AnalyticLearning.MLFilteredDepartmentSearchTerm> searchTerms = new List<AnalyticLearning.MLFilteredDepartmentSearchTerm>();

                foreach (User user in allTargetedAudience)
                {
                    foreach (Department department in user.Departments)
                    {
                        if (!searchTerms.Any(s => s.Id.Equals(department.Id)))
                        {
                            searchTerms.Add(
                                new AnalyticLearning.MLFilteredDepartmentSearchTerm
                                {
                                    Id = department.Id,
                                    Content = department.Title,
                                    IsSelected = department.Id.Equals(searchId) ? true : false
                                }
                            );
                        }
                    }
                }

                searchTerms = searchTerms.OrderByDescending(s => s.IsSelected).ThenBy(s => s.Content).ToList();

                AnalyticLearning.MLFilteredDepartmentSearchTerm allDepartmentSearchTerm = new AnalyticLearning.MLFilteredDepartmentSearchTerm
                {
                    Id = string.Empty,
                    Content = "All Departments",
                    IsSelected = string.IsNullOrEmpty(searchId) ? true : false
                };

                if (string.IsNullOrEmpty(searchId))
                {
                    searchTerms.Insert(0, allDepartmentSearchTerm);
                }
                else
                {
                    searchTerms.Insert(1, allDepartmentSearchTerm);
                }

                response.SearchTerms = searchTerms;

                response.Topic = SelectMLTopic((int)MLTopicQueryType.Full, (int)MLCard.MLCardQueryType.Basic, category, companyId, topicId, null, null, null, mainSession, analyticSession);

                response.CardToggleType = new List<AnalyticLearning.MLCardToggleType>
                {
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "All questions published",
                        IsSelected = questionToggleType.Equals("1") ? true : false,
                        Id = "1"
                    },
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "Hardest questions",
                        IsSelected = questionToggleType.Equals("2") ? true : false,
                        Id = "2"
                    },
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "Easiest questions",
                        IsSelected = questionToggleType.Equals("3") ? true : false,
                        Id = "3"
                    }
                };

                response.CardToggleType = response.CardToggleType.OrderByDescending(s => s.IsSelected).ThenBy(s => s.Content).ToList();

                // Set up datetime string
                double timeoffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                response.Topic.StartDateString = response.Topic.StartDate.AddHours(timeoffset).ToString("dd/MM/yyyy");
                if (response.Topic.DurationType == (int)DurationTypeEnum.Schedule)
                {
                    response.Topic.EndDateString = response.Topic.EndDate.Value.AddHours(timeoffset).ToString("dd/MM/yyyy");
                }

                new AnalyticLearning().SelectQuestionResult(searchedAudience, Convert.ToInt16(questionToggleType), response, analyticSession);

                if (response.Topic.Cards.Count > 0)
                {
                    response.PreviewCard = new MLCard().SelectCardQuestionResult(adminUserId, companyId, topicId, categoryId, response.Topic.Cards[0].CardId, null).PreviewCard;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLSelectUserAttemptHistoryResponse SelectUserAttemptHistoryResult(string adminUserId, string companyId, string answeredByUserId, string topicId, string categoryId, string containsName = null)
        {
            MLSelectUserAttemptHistoryResponse response = new MLSelectUserAttemptHistoryResponse();
            response.AnsweredByUser = new User();
            response.Topic = new MLTopic();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                MLTopicCategory category = new MLTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                Row userRow = vh.ValidateUser(answeredByUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid userId: " + answeredByUserId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                // Get user
                response.AnsweredByUser = new User().SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User;

                // Get targeted audience
                List<User> allTargetedAudience = SelectTargetedAudience(topicId, companyId, null, mainSession);

                response.Topic = SelectMLTopic((int)MLTopicQueryType.Full, (int)MLCard.MLCardQueryType.FullWithAnalytics, category, companyId, topicId, null, containsName, answeredByUserId, mainSession, analyticSession);

                response.CardToggleType = new List<AnalyticLearning.MLCardToggleType>
                {
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "All questions",
                        IsSelected = true,
                        Id = ((int)AnalyticLearning.MLCardToggleTypeEnum.AllQuestions).ToString()
                    },
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "Hardest questions",
                        IsSelected = false,
                        Id = ((int)AnalyticLearning.MLCardToggleTypeEnum.HardestQuestions).ToString()
                    },
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "Easiest questions",
                        IsSelected = false,
                        Id = ((int)AnalyticLearning.MLCardToggleTypeEnum.EasiestQuestions).ToString()
                    }
                };

                // Set up datetime string
                double timeoffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                response.Topic.StartDateString = response.Topic.StartDate.AddHours(timeoffset).ToString("dd/MM/yyyy");
                if (response.Topic.DurationType == (int)DurationTypeEnum.Schedule)
                {
                    response.Topic.EndDateString = response.Topic.EndDate.Value.AddHours(timeoffset).ToString("dd/MM/yyyy");
                }

                new AnalyticLearning().SelectUserAttemptHistoryResult(allTargetedAudience, response.AnsweredByUser, (int)AnalyticLearning.MLCardToggleTypeEnum.AllQuestions, response, timeoffset, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
