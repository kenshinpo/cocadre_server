﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLTopicCategory
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum MLCategoryQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember(EmitDefaultValue = false)]
        public string CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember]
        public int NumberOfMLTopics { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<MLTopic> Topics { get; set; }

        public MLCategoryCreateResponse CreateMLCategory(string companyId, string title, string adminUserId, ISession session = null, string categoryId = null)
        {
            MLCategoryCreateResponse response = new MLCategoryCreateResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Category title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLTopicCategoryTitleMissing);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryTitleMissing;
                    return response;
                }

                if (string.IsNullOrEmpty(categoryId))
                {
                    categoryId = UUIDGenerator.GenerateUniqueIDForMLCategory();
                }

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("ml_category", new List<string> { "id", "company_id", "title", "is_valid", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batchStatement.Add(ps.Bind(categoryId, companyId, title, true, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_category_by_title", new List<string> { "ml_category_id", "company_id", "title" }));
                batchStatement.Add(ps.Bind(categoryId, companyId, title));

                session.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLCategorySelectResponse SelectMLCategory(string categoryId, string companyId, ISession session = null)
        {
            MLCategorySelectResponse response = new MLCategorySelectResponse();
            response.Category = null;
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);

                if (categoryRow != null)
                {
                    MLTopicCategory category = new MLTopicCategory
                    {
                        CategoryId = categoryRow.GetValue<string>("id"),
                        Title = categoryRow.GetValue<string>("title")
                    };

                    response.Category = category;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLCategorySelectAllResponse SelectAllMLCategories(string adminUserId, string companyId, int queryType, ISession session = null)
        {
            MLCategorySelectAllResponse response = new MLCategorySelectAllResponse();
            response.Categories = new List<MLTopicCategory>();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_category_by_title", new List<string>(), new List<string> { "company_id" }));
                RowSet categoryRowset = session.Execute(ps.Bind(companyId));
                List<Row> categoryRowList = categoryRowset.ToList();

                if (categoryRowList.Count == 0)
                {
                    string eventCategoryId = UUIDGenerator.GenerateUniqueIDForMLCategory();
                    string title = "Event";
                    CreateMLCategory(companyId, title, adminUserId, session, eventCategoryId);
                    MLTopicCategory category = new MLTopicCategory
                    {
                        CategoryId = eventCategoryId,
                        Title = title,
                        NumberOfMLTopics = 0
                    };

                    response.Categories.Add(category);
                }
                else
                {
                    foreach (Row categoryRow in categoryRowList)
                    {
                        string categoryId = categoryRow.GetValue<string>("ml_category_id");
                        string title = categoryRow.GetValue<string>("title");

                        int numberOfTopics = 0;
                        if (queryType == (int)MLCategoryQueryType.FullDetail)
                        {
                            // Get number of topics
                            ps = session.Prepare(CQLGenerator.SelectStatement("ml_topic", new List<string>(), new List<string> { "category_id" }));
                            RowSet rs = session.Execute(ps.Bind(categoryId));
                            if (rs != null)
                            {
                                foreach (Row row in rs)
                                {
                                    if (row.GetValue<int>("status") != (int)MLTopic.MLTopicStatusEnum.Deleted)
                                    {
                                        numberOfTopics++;
                                    }
                                }
                            }
                        }

                        MLTopicCategory category = new MLTopicCategory
                        {
                            CategoryId = categoryId,
                            Title = title,
                            NumberOfMLTopics = numberOfTopics
                        };

                        response.Categories.Add(category);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public MLCategoryUpdateResponse UpdateMLCategory(string adminUserId, string companyId, string categoryId, string newTitle)
        {
            MLCategoryUpdateResponse response = new MLCategoryUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row rsCategoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);

                if (rsCategoryRow == null)
                {
                    Log.Error("MLTopic category is invalid: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Category title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLTopicCategoryTitleMissing);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryTitleMissing;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                string currentTitle = rsCategoryRow.GetValue<string>("title");

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("ml_category",
                    new List<string> { "company_id", "id" }, new List<string> { "title", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, adminUserId, DateTime.UtcNow, companyId, categoryId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_category_by_title",
                    new List<string> { "company_id", "title", "ml_category_id" }));
                deleteBatch.Add(ps.Bind(companyId, currentTitle, categoryId));

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_category_by_title",
                    new List<string> { "company_id", "title", "ml_category_id" }));
                updateBatch.Add(ps.Bind(companyId, newTitle, categoryId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLCategoryUpdateResponse DeleteMLCategory(string adminUserId, string companyId, string categoryId)
        {
            MLCategoryUpdateResponse response = new MLCategoryUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row rsCategoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);

                if (rsCategoryRow == null)
                {
                    Log.Error("MLTopic category is invalid: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithLimit("mltopic_by_mlcategory",
                    new List<string>(), new List<string> { "ml_topic_category_id" }, 1));

                Row categoryRelation = session.Execute(ps.Bind(categoryId)).FirstOrDefault();

                if (categoryRelation != null)
                {
                    Log.Error("Topics still exists in category: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLTopicCategoryCannotBeDeleted);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryCannotBeDeleted;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                string currentTitle = rsCategoryRow.GetValue<string>("title");

                ps = session.Prepare(CQLGenerator.DeleteStatement("mltopic_by_mlcategory",
                   new List<string> { "ml_topic_category_id" }));

                deleteBatch.Add(ps.Bind(categoryId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_category_by_title",
                   new List<string> { "company_id", "title", "ml_category_id" }));
                deleteBatch.Add(ps.Bind(companyId, currentTitle, categoryId));

                ps = session.Prepare(CQLGenerator.UpdateStatement("ml_category",
                    new List<string> { "company_id", "id" }, new List<string> { "is_valid", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(false, adminUserId, DateTime.UtcNow, companyId, categoryId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
