﻿using Cassandra;
using CassandraService.CassandraUtilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class IconCategory
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        public IconCategory Create(string managerId, string title)
        {
            #region Step 1. Verify data
            if (String.IsNullOrEmpty(title))
            {
                return null;
            }
            #endregion

            #region Step 2. Access database
            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();

            #endregion

            return null;
        }
    }
}
