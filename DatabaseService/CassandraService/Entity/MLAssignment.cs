﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLAssignment
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [Serializable]
        [DataContract]
        public class Privacy
        {
            [DataMember]
            public bool IsForEveryone { get; set; }

            [DataMember]
            public bool IsForDepartment { get; set; }

            [DataMember]
            public bool IsForUser { get; set; }

            [DataMember]
            public bool IsForCustomGroup { get; set; }

            [DataMember]
            public List<Department> TargetedDepartments { get; set; }

            [DataMember]
            public List<User> TargetedUsers { get; set; }
        }


        public enum AssignmentStatusType
        {
            Deleted = -1,
            Active = 1
        }

        public enum AssignmentDruationType
        {
            Prerpetual = 1,
            Schedule = 2
        }

        public enum AssignmentParticipantsType
        {
            FollowEducation = 1,
            Custom = 2
        }

        public enum AssignmentProgressType
        {
            Upcoming = 1,
            Live = 2,
            Completed = 3
        }

        [DataMember]
        public string AssignmentId { get; set; }

        [DataMember]
        public MLEducation Education { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int DruationType { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public String StartDateString { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public String EndDateString { get; set; }

        [DataMember]
        public int ParticipantsType { get; set; }

        [DataMember]
        public bool IsRemindful { get; set; }

        //[DataMember]
        //public bool IsTargetedDepartments { get; set; }


        //[DataMember]
        //public bool IsTargetedUsers { get; set; }

        [DataMember]
        public int FrequencyDays { get; set; }

        [DataMember]
        public Privacy PrivacySetting { get; set; }

        [DataMember]
        public int Progress { get; set; }

        public MLAssignmentListResponse CreateAssignments(string managerId,
                                                           string companyId,
                                                           List<string> educationIds,
                                                           int druationType,
                                                           DateTime startDate,
                                                           DateTime? endDate,
                                                           int participantsType,
                                                           int frequencyDays,
                                                           List<string> targetedDepartmentIds,
                                                           List<string> targetedUserIds)
        {
            MLAssignmentListResponse response = new MLAssignmentListResponse();
            response.Success = false;
            try
            {
                for (int i = 0; i < educationIds.Count; i++)
                {
                    MLAssignmentCreateResponse rspCreate = Create(managerId, companyId, educationIds[i], druationType, startDate, endDate, participantsType, frequencyDays, targetedDepartmentIds, targetedUserIds);
                    if (!rspCreate.Success)
                    {
                        Log.Error("Create assignement failed. EducationId: " + educationIds[i]);
                        response.ErrorCode = rspCreate.ErrorCode;
                        response.ErrorMessage = rspCreate.ErrorMessage;
                        return response;
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLAssignmentCreateResponse Create(string managerId,
                                                           string companyId,
                                                           string educationId,
                                                           int druationType,
                                                           DateTime startDate,
                                                           DateTime? endDate,
                                                           int participantsType,
                                                           int frequencyDays,
                                                           List<string> targetedDepartmentIds,
                                                           List<string> targetedUserIds,
                                                           ISession mainSession = null
            )
        {
            MLAssignmentCreateResponse response = new MLAssignmentCreateResponse();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                #region Step 1. Check data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(managerId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                DateTime currentDate = DateTime.UtcNow;
                if (startDate == null)
                {
                    Log.Error(ErrorMessage.MLDateIsNull);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                    response.ErrorMessage = ErrorMessage.MLDateIsNull;
                    return response;
                }


                if (druationType == (int)AssignmentDruationType.Schedule)
                {
                    if (!endDate.HasValue || endDate == null)
                    {
                        Log.Error(ErrorMessage.MLDateIsNull);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                        response.ErrorMessage = ErrorMessage.MLDateIsNull;
                        return response;
                    }

                    if (endDate <= currentDate)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanToday);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanToday;
                        return response;
                    }

                    if (startDate >= endDate)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanStartDate;
                        return response;
                    }
                }
                #endregion

                #region Step 2. Read/Write database
                // Generate IDs
                string assignmentId = UUIDGenerator.GenerateUniqueIDForMLAssignment();

                BatchStatement batch = new BatchStatement();
                #region ml_assignment
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment", new List<string> {
                    "id",
                    "ml_education_id",
                    "status",
                    "druation_type",
                    "start_date",
                    "end_date",
                    "participants_type",
                    "frequency_days",
                    "created_by_admin_id",
                    "created_on_timestamp",
                    "last_modified_by_admin_id",
                    "last_modified_timestamp" }));
                batch.Add(ps.Bind(
                    assignmentId,
                    educationId,
                    (int)AssignmentStatusType.Active,
                    druationType,
                    startDate,
                    endDate,
                    participantsType,
                    frequencyDays,
                    managerId,
                    currentDate,
                    managerId,
                    currentDate));
                #endregion

                #region ml_assignment_by_timestamp
                ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_by_timestamp", new List<string> {
                    "company_id",
                    "ml_assignment_id",
                    "start_on_timestamp"
                    }));
                batch.Add(ps.Bind(
                    companyId,
                    assignmentId,
                    startDate));
                #endregion

                #region ml_assignment_by_education
                ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_by_education", new List<string> {
                    "ml_education_id",
                    "ml_assignment_id"
                    }));
                batch.Add(ps.Bind(
                    educationId,
                    assignmentId));
                #endregion

                #region Privacy
                if (participantsType == (int)AssignmentParticipantsType.Custom)
                {
                    #region 在寫入資料庫前，比對 Education 的 privacy！
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_privacy", new List<string>(), new List<string> { "ml_education_id" }));
                    Row rEducationPrivacy = mainSession.Execute(ps.Bind(educationId)).FirstOrDefault();
                    if (rEducationPrivacy == null)
                    {
                        Log.Error(ErrorMessage.MLEducationMissPrivacy);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationMissPrivacy);
                        response.ErrorMessage = ErrorMessage.MLEducationMissPrivacy;
                        return response;
                    }
                    #endregion

                    #region ml_assignment_privacy
                    bool isForDepartment = false, isForUser = false;
                    if (targetedDepartmentIds != null && targetedDepartmentIds.Count > 0)
                    {
                        #region ml_department_targeted_assignment, ml_assignment_targeted_department
                        foreach (string departmentId in targetedDepartmentIds)
                        {
                            if (rEducationPrivacy.GetValue<bool>("is_for_department"))
                            {
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_department_targeted_education", new List<string>(), new List<string> { "department_id", "ml_education_id" }));
                                Row rowEducationDepartmentPrivacy = mainSession.Execute(ps.Bind(departmentId, educationId)).FirstOrDefault();
                                if (rowEducationDepartmentPrivacy == null)
                                {
                                    Log.Error(ErrorMessage.MLAssignmentPrivacyInvalid + "EducationId: " + educationId + ", DepartmentId: " + departmentId);
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentPrivacyInvalid);
                                    response.ErrorMessage = ErrorMessage.MLAssignmentPrivacyInvalid;
                                    return response;
                                }
                            }

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_department_targeted_assignment", new List<string> {
                                "ml_assignment_id",
                                "department_id"
                                }));
                            batch.Add(ps.Bind(
                                assignmentId,
                                departmentId));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_targeted_department", new List<string> {
                                "ml_assignment_id",
                                "department_id"
                                }));
                            batch.Add(ps.Bind(
                                assignmentId,
                                departmentId));
                        }
                        #endregion

                        isForDepartment = true;
                    }

                    if (targetedUserIds != null && targetedUserIds.Count > 0)
                    {
                        #region ml_user_targeted_assignment, ml_assignment_targeted_user
                        foreach (string userId in targetedUserIds)
                        {
                            if (rEducationPrivacy.GetValue<bool>("is_for_user"))
                            {
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_user_targeted_education", new List<string>(), new List<string> { "user_id", "ml_education_id" }));
                                Row rowEducationUserPrivacy = mainSession.Execute(ps.Bind(userId, educationId)).FirstOrDefault();
                                if (rowEducationUserPrivacy == null)
                                {
                                    Log.Error(ErrorMessage.MLAssignmentPrivacyInvalid + "EducationId: " + educationId + ", UserId: " + userId);
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentPrivacyInvalid);
                                    response.ErrorMessage = ErrorMessage.MLAssignmentPrivacyInvalid;
                                    return response;
                                }
                            }

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_user_targeted_assignment", new List<string> {
                                "ml_assignment_id",
                                "user_id"
                                }));
                            batch.Add(ps.Bind(
                                assignmentId,
                                userId));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_targeted_user", new List<string> {
                                "ml_assignment_id",
                                "user_id"
                                }));
                            batch.Add(ps.Bind(
                                assignmentId,
                                userId));
                        }
                        #endregion

                        isForUser = true;
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_privacy", new List<string> {
                    "ml_assignment_id",
                    "is_for_department",
                    "is_for_user"
                    }));
                    batch.Add(ps.Bind(
                        assignmentId,
                        isForDepartment,
                        isForUser));
                    #endregion
                }
                #endregion

                mainSession.Execute(batch);

                response.Assignment = GetDetail(managerId, companyId, assignmentId, mainSession).Assignment;
                response.Success = true;
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLAssignmentDetailResponse GetDetail(string managerId, string companyId, string assignmentId, ISession mainSession = null)
        {
            MLAssignmentDetailResponse response = new MLAssignmentDetailResponse();
            response.Success = false;

            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(managerId, companyId, mainSession);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }
                PreparedStatement ps = null;

                MLAssignment assignment = new MLAssignment();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment", new List<string>(), new List<string> { "id" }));
                Row rowAssignment = mainSession.Execute(ps.Bind(assignmentId)).FirstOrDefault();

                if (rowAssignment != null)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_by_timestamp", new List<string>(), new List<string> { "company_id", "start_on_timestamp", "ml_assignment_id" }));
                    Row rowAssignmentExist = mainSession.Execute(ps.Bind(companyId, rowAssignment.GetValue<DateTime>("start_date"), assignmentId)).FirstOrDefault();
                    if (rowAssignmentExist == null)
                    {
                        Log.Error(ErrorMessage.MLAssignmentInvalid);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentInvalid);
                        response.ErrorMessage = ErrorMessage.MLAssignmentInvalid;
                        return response;
                    }

                    assignment.AssignmentId = rowAssignment.GetValue<string>("id");

                    // Get CategoryId 
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_category_by_education", new List<string>(), new List<string> { "ml_education_id" }));
                    Row rowEducatonCategory = mainSession.Execute(ps.Bind(rowAssignment.GetValue<string>("ml_education_id"))).FirstOrDefault();

                    assignment.Education = new MLEducation().GetDetail(companyId, managerId, rowEducatonCategory.GetValue<string>("ml_category_id"), rowAssignment.GetValue<string>("ml_education_id"), null, (int)MLEducation.MLEducationQueryType.Basic, (int)MLEducationCard.QueryType.Basic, null, mainSession).Education;
                    assignment.Status = rowAssignment.GetValue<int>("status");
                    assignment.DruationType = rowAssignment.GetValue<int>("druation_type");
                    assignment.StartDate = rowAssignment.GetValue<DateTime>("start_date");
                    if (rowAssignment.IsNull("end_date"))
                    {
                        assignment.EndDate = null;
                    }
                    else
                    {
                        assignment.EndDate = rowAssignment.GetValue<DateTime>("end_date");
                    }

                    DateTime currentDateTime = DateTime.UtcNow;
                    if (currentDateTime < assignment.StartDate)
                    {
                        assignment.Progress = (int)MLAssignment.AssignmentProgressType.Upcoming;
                    }
                    else
                    {
                        if (assignment.DruationType == (int)AssignmentDruationType.Schedule && assignment.EndDate != null && assignment.EndDate < currentDateTime)
                        {
                            assignment.Progress = (int)MLAssignment.AssignmentProgressType.Completed;
                        }
                        else
                        {
                            assignment.Progress = (int)MLAssignment.AssignmentProgressType.Live;
                        }
                    }

                    assignment.ParticipantsType = rowAssignment.GetValue<int>("participants_type");
                    assignment.FrequencyDays = rowAssignment.GetValue<int>("frequency_days");
                    if (assignment.FrequencyDays > 0)
                    {
                        assignment.IsRemindful = true;
                    }
                    else
                    {
                        assignment.IsRemindful = false;
                    }

                    #region Privacy
                    Privacy privacySetting = new Privacy();
                    if (assignment.ParticipantsType == (int)AssignmentParticipantsType.FollowEducation) // Get Education privacy setting
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_privacy", new List<string>(), new List<string> { "ml_education_id" }));
                        Row rowEducationPrivacy = mainSession.Execute(ps.Bind(assignment.Education.EducationId)).FirstOrDefault();
                        if (rowEducationPrivacy == null)
                        {
                            Log.Error(ErrorMessage.MLEducationMissPrivacy);
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationMissPrivacy);
                            response.ErrorMessage = ErrorMessage.MLEducationMissPrivacy;
                            return response;
                        }

                        privacySetting.IsForEveryone = rowEducationPrivacy.GetValue<bool>("is_for_everyone");
                        privacySetting.IsForDepartment = rowEducationPrivacy.GetValue<bool>("is_for_department");
                        privacySetting.IsForUser = rowEducationPrivacy.GetValue<bool>("is_for_user");
                        privacySetting.IsForCustomGroup = rowEducationPrivacy.GetValue<bool>("is_for_custom_group");

                        if (privacySetting.IsForDepartment)
                        {
                            privacySetting.TargetedDepartments = new List<Department>();

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_department", new List<string>(), new List<string> { "ml_education_id" }));
                            RowSet rsTargetedDepartment = mainSession.Execute(ps.Bind(assignment.Education.EducationId));
                            if (rsTargetedDepartment != null)
                            {
                                foreach (Row rTargetedDepartment in rsTargetedDepartment)
                                {
                                    DepartmentDetailResponse departmentResponse = new Department().GetDepartmentDetail(managerId, companyId, rTargetedDepartment.GetValue<string>("department_id"), Department.QUERY_TYPE_BASIC, mainSession);
                                    if (departmentResponse.Success)
                                    {
                                        privacySetting.TargetedDepartments.Add(departmentResponse.Department);
                                    }
                                }
                            }
                        }

                        if (privacySetting.IsForUser)
                        {
                            privacySetting.TargetedUsers = new List<User>();

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_user", new List<string>(), new List<string> { "ml_education_id" }));
                            RowSet rsTargetedUser = mainSession.Execute(ps.Bind(assignment.Education.EducationId));
                            if (rsTargetedUser != null)
                            {
                                foreach (Row rTargetedUser in rsTargetedUser)
                                {
                                    UserDetailResponse userResponse = new User().GetUserDetail(managerId, companyId, rTargetedUser.GetValue<string>("user_id"));
                                    if (userResponse.Success)
                                    {
                                        privacySetting.TargetedUsers.Add(userResponse.User);
                                    }
                                }
                            }
                        }

                    }
                    else // Get custom privacy setting
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_privacy", new List<string>(), new List<string> { "ml_assignment_id" }));
                        Row rowAssignmentPrivacy = mainSession.Execute(ps.Bind(assignmentId)).FirstOrDefault();
                        if (rowAssignmentPrivacy == null)
                        {
                            Log.Error(ErrorMessage.MLAssignmentMissPrivacy);
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentMissPrivacy);
                            response.ErrorMessage = ErrorMessage.MLAssignmentMissPrivacy;
                            return response;
                        }

                        privacySetting.IsForEveryone = false;
                        privacySetting.IsForDepartment = rowAssignmentPrivacy.GetValue<bool>("is_for_department");
                        privacySetting.IsForUser = rowAssignmentPrivacy.GetValue<bool>("is_for_user");
                        privacySetting.IsForCustomGroup = false;

                        if (privacySetting.IsForDepartment)
                        {
                            privacySetting.TargetedDepartments = new List<Department>();

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_targeted_department", new List<string>(), new List<string> { "ml_assignment_id" }));
                            RowSet rsTargetedDepartment = mainSession.Execute(ps.Bind(assignmentId));
                            if (rsTargetedDepartment != null)
                            {
                                foreach (Row rTargetedDepartment in rsTargetedDepartment)
                                {
                                    DepartmentDetailResponse departmentResponse = new Department().GetDepartmentDetail(managerId, companyId, rTargetedDepartment.GetValue<string>("department_id"), Department.QUERY_TYPE_BASIC, mainSession);
                                    if (departmentResponse.Success)
                                    {
                                        privacySetting.TargetedDepartments.Add(departmentResponse.Department);
                                    }
                                }
                            }
                        }

                        if (privacySetting.IsForUser)
                        {
                            privacySetting.TargetedUsers = new List<User>();

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_targeted_user", new List<string>(), new List<string> { "ml_assignment_id" }));
                            RowSet rsTargetedUser = mainSession.Execute(ps.Bind(assignmentId));
                            if (rsTargetedUser != null)
                            {
                                foreach (Row rTargetedUser in rsTargetedUser)
                                {
                                    UserDetailResponse userResponse = new User().GetUserDetail(managerId, companyId, rTargetedUser.GetValue<string>("user_id"));
                                    if (userResponse.Success)
                                    {
                                        privacySetting.TargetedUsers.Add(userResponse.User);
                                    }
                                }
                            }
                        }
                    }

                    assignment.PrivacySetting = privacySetting;
                    #endregion

                    response.Assignment = assignment;
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLAssignmentListResponse GetList(string managerId, string companyId, int progressType = 0, string containsEducationTitle = null)
        {
            MLAssignmentListResponse response = new MLAssignmentListResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                PreparedStatement ps = null;

                #region Step 1. Validate account
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(managerId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 2. Get all assignements on the company
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_by_timestamp", new List<string>(), new List<string> { "company_id" }));
                RowSet rsAssignments = mainSession.Execute(ps.Bind(companyId));
                if (rsAssignments == null)
                {
                    response.Assignments = new List<MLAssignment>();
                    response.Success = true;
                }
                else
                {
                    List<MLAssignment> assignements = new List<MLAssignment>();
                    foreach (Row rAssignment in rsAssignments)
                    {
                        MLAssignmentDetailResponse responseAssignmentDetail = GetDetail(managerId, companyId, rAssignment.GetValue<string>("ml_assignment_id"), mainSession);
                        if (responseAssignmentDetail.Success)
                        {
                            if (progressType > 0)
                            {
                                if (responseAssignmentDetail.Assignment.Progress == progressType)
                                {
                                    if (string.IsNullOrEmpty(containsEducationTitle))
                                    {
                                        assignements.Add(responseAssignmentDetail.Assignment);
                                    }
                                    else
                                    {
                                        if (responseAssignmentDetail.Assignment.Education.Title.ToLower().Contains(containsEducationTitle))
                                        {
                                            assignements.Add(responseAssignmentDetail.Assignment);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(containsEducationTitle))
                                {
                                    assignements.Add(responseAssignmentDetail.Assignment);
                                }
                                else
                                {
                                    if (responseAssignmentDetail.Assignment.Education.Title.ToLower().Contains(containsEducationTitle))
                                    {
                                        assignements.Add(responseAssignmentDetail.Assignment);
                                    }
                                }
                            }
                        }
                    }
                    response.Assignments = assignements;
                    response.Success = true;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLAssignmentUpdateResponse Update(string managerId,
                                                 string companyId,
                                                 string assignmentId,
                                                 string newEducationId,
                                                 int newDruationType,
                                                 DateTime newStartDate,
                                                 DateTime? newEndDate,
                                                 int newParticipantsType,
                                                 int newFrequencyDays,
                                                 List<string> newTargetedDepartmentIds,
                                                 List<string> newTargetedUserIds,
                                                 ISession mainSession = null)
        {
            MLAssignmentUpdateResponse response = new MLAssignmentUpdateResponse();
            response.Success = false;

            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                #region Step 1. Check data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(managerId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                DateTime currentDateTime = DateTime.UtcNow;
                if (newStartDate == null)
                {
                    Log.Error(ErrorMessage.MLDateIsNull);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                    response.ErrorMessage = ErrorMessage.MLDateIsNull;
                    return response;
                }


                if (newDruationType == (int)AssignmentDruationType.Schedule)
                {
                    if (!newEndDate.HasValue || newEndDate == null)
                    {
                        Log.Error(ErrorMessage.MLDateIsNull);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                        response.ErrorMessage = ErrorMessage.MLDateIsNull;
                        return response;
                    }

                    if (newEndDate <= currentDateTime)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanToday);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanToday;
                        return response;
                    }

                    if (newStartDate >= newEndDate)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanStartDate;
                        return response;
                    }


                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment", new List<string>(), new List<string> { "id" }));
                Row rAssignment = mainSession.Execute(ps.Bind(assignmentId)).FirstOrDefault();
                if (rAssignment == null)
                {
                    Log.Error(ErrorMessage.MLAssignmentInvalid);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentInvalid);
                    response.ErrorMessage = ErrorMessage.MLAssignmentInvalid;
                    return response;
                }
                else
                {
                    if (rAssignment.GetValue<DateTime>("start_date") < currentDateTime) // This Assignement is live. Can not edit.
                    {
                        Log.Error(ErrorMessage.MLAssignmentIsLive);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentIsLive);
                        response.ErrorMessage = ErrorMessage.MLAssignmentIsLive;
                        return response;
                    }
                }
                #endregion

                #region Step 2. Read/Write database
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                #region ml_assignment
                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("ml_assignment",
                    new List<string> { "id" }, new List<string> { "druation_type", "end_date", "frequency_days", "last_modified_by_admin_id", "last_modified_timestamp", "ml_education_id", "participants_type", "start_date" }, new List<string>()));
                updateBatch.Add(ps.Bind(newDruationType, newEndDate, newFrequencyDays, managerId, currentDateTime, newEducationId, newParticipantsType, newStartDate,
                       assignmentId));
                #endregion

                #region ml_assignment_by_timestamp
                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_assignment_by_timestamp", new List<string> { "company_id", "start_on_timestamp", "ml_assignment_id" }));
                deleteBatch.Add(ps.Bind(companyId, rAssignment.GetValue<DateTime>("start_date"), assignmentId));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_by_timestamp", new List<string> { "company_id", "ml_assignment_id", "start_on_timestamp" }));
                updateBatch.Add(ps.Bind(companyId, assignmentId, newStartDate));
                #endregion

                #region ml_assignment_by_education
                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_assignment_by_education", new List<string> { "ml_education_id", "ml_assignment_id" }));
                deleteBatch.Add(ps.Bind(rAssignment.GetValue<string>("ml_education_id"), assignmentId));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_by_education", new List<string> { "ml_education_id", "ml_assignment_id" }));
                updateBatch.Add(ps.Bind(newEducationId, assignmentId));
                #endregion

                #region Privacy
                #region Delete participants_type, ml_department_targeted_assignment, ml_assignment_targeted_department, ml_user_targeted_assignment, ml_assignment_targeted_user.
                if (rAssignment.GetValue<int>("participants_type") == (int)AssignmentParticipantsType.Custom)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_privacy", new List<string>(), new List<string> { "ml_assignment_id" }));
                    Row rAssignmentPrivacy = mainSession.Execute(ps.Bind(assignmentId)).FirstOrDefault();
                    if (rAssignmentPrivacy == null)
                    {
                        Log.Error(ErrorMessage.MLAssignmentPrivacyInvalid);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentPrivacyInvalid);
                        response.ErrorMessage = ErrorMessage.MLAssignmentPrivacyInvalid;
                        return response;
                    }

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_assignment_privacy", new List<string> { "ml_assignment_id" }));
                    deleteBatch.Add(ps.Bind(assignmentId));

                    // Delete rows in ml_department_targeted_assignment and ml_assignment_targeted_department.
                    if (rAssignmentPrivacy.GetValue<bool>("is_for_department"))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_targeted_department", new List<string>(), new List<string> { "ml_assignment_id" }));
                        RowSet rsPrivacyDepartment = mainSession.Execute(ps.Bind(assignmentId));
                        if (rsPrivacyDepartment != null)
                        {
                            foreach (Row rPrivacyDepartment in rsPrivacyDepartment)
                            {
                                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_department_targeted_assignment", new List<string> { "department_id", "ml_assignment_id" }));
                                deleteBatch.Add(ps.Bind(rPrivacyDepartment.GetValue<string>("department_id"), assignmentId));

                                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_assignment_targeted_department", new List<string> { "department_id", "ml_assignment_id" }));
                                deleteBatch.Add(ps.Bind(rPrivacyDepartment.GetValue<string>("department_id"), assignmentId));
                            }
                        }
                    }

                    // Delete rows in ml_user_targeted_assignment and ml_assignment_targeted_user.
                    if (rAssignmentPrivacy.GetValue<bool>("is_for_user"))
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment_targeted_user", new List<string>(), new List<string> { "ml_assignment_id" }));
                        RowSet rsPrivacyUser = mainSession.Execute(ps.Bind(assignmentId));
                        if (rsPrivacyUser != null)
                        {
                            foreach (Row rPrivacyUser in rsPrivacyUser)
                            {
                                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_user_targeted_assignment", new List<string> { "user_id", "ml_assignment_id" }));
                                deleteBatch.Add(ps.Bind(rPrivacyUser.GetValue<string>("user_id"), assignmentId));

                                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_assignment_targeted_user", new List<string> { "user_id", "ml_assignment_id" }));
                                deleteBatch.Add(ps.Bind(rPrivacyUser.GetValue<string>("user_id"), assignmentId));
                            }
                        }
                    }



                }
                #endregion

                #region Insert participants_type, ml_department_targeted_assignment, ml_assignment_targeted_department, ml_user_targeted_assignment, ml_assignment_targeted_user.
                if (newParticipantsType == (int)AssignmentParticipantsType.Custom)
                {
                    #region 在寫入資料庫前，比對 Education 的 privacy！
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_privacy", new List<string>(), new List<string> { "ml_education_id" }));
                    Row rEducationPrivacy = mainSession.Execute(ps.Bind(newEducationId)).FirstOrDefault();
                    if (rEducationPrivacy == null)
                    {
                        Log.Error(ErrorMessage.MLEducationMissPrivacy);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationMissPrivacy);
                        response.ErrorMessage = ErrorMessage.MLEducationMissPrivacy;
                        return response;
                    }
                    #endregion

                    #region ml_assignment_privacy
                    bool isForDepartment = false, isForUser = false;
                    if (newTargetedDepartmentIds != null && newTargetedDepartmentIds.Count > 0)
                    {
                        #region ml_department_targeted_assignment, ml_assignment_targeted_department
                        foreach (string departmentId in newTargetedDepartmentIds)
                        {
                            if (rEducationPrivacy.GetValue<bool>("is_for_department"))
                            {
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_department_targeted_education", new List<string>(), new List<string> { "department_id", "ml_education_id" }));
                                Row rowEducationDepartmentPrivacy = mainSession.Execute(ps.Bind(departmentId, newEducationId)).FirstOrDefault();
                                if (rowEducationDepartmentPrivacy == null)
                                {
                                    Log.Error(ErrorMessage.MLAssignmentPrivacyInvalid + "EducationId: " + newEducationId + ", DepartmentId: " + departmentId);
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentPrivacyInvalid);
                                    response.ErrorMessage = ErrorMessage.MLAssignmentPrivacyInvalid;
                                    return response;
                                }
                            }

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_department_targeted_assignment", new List<string> {
                                "ml_assignment_id",
                                "department_id"
                                }));
                            updateBatch.Add(ps.Bind(
                                assignmentId,
                                departmentId));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_targeted_department", new List<string> {
                                "ml_assignment_id",
                                "department_id"
                                }));
                            updateBatch.Add(ps.Bind(
                                assignmentId,
                                departmentId));
                        }
                        #endregion

                        isForDepartment = true;
                    }

                    if (newTargetedUserIds != null && newTargetedUserIds.Count > 0)
                    {
                        #region ml_user_targeted_assignment, ml_assignment_targeted_user
                        foreach (string userId in newTargetedUserIds)
                        {
                            if (rEducationPrivacy.GetValue<bool>("is_for_user"))
                            {
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_user_targeted_education", new List<string>(), new List<string> { "user_id", "ml_education_id" }));
                                Row rowEducationUserPrivacy = mainSession.Execute(ps.Bind(userId, newEducationId)).FirstOrDefault();
                                if (rowEducationUserPrivacy == null)
                                {
                                    Log.Error(ErrorMessage.MLAssignmentPrivacyInvalid + "EducationId: " + newEducationId + ", UserId: " + userId);
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentPrivacyInvalid);
                                    response.ErrorMessage = ErrorMessage.MLAssignmentPrivacyInvalid;
                                    return response;
                                }
                            }

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_user_targeted_assignment", new List<string> {
                                "ml_assignment_id",
                                "user_id"
                                }));
                            updateBatch.Add(ps.Bind(
                                assignmentId,
                                userId));

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_targeted_user", new List<string> {
                                "ml_assignment_id",
                                "user_id"
                                }));
                            updateBatch.Add(ps.Bind(
                                assignmentId,
                                userId));
                        }
                        #endregion

                        isForUser = true;
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("ml_assignment_privacy", new List<string> {
                    "ml_assignment_id",
                    "is_for_department",
                    "is_for_user"
                    }));
                    updateBatch.Add(ps.Bind(
                        assignmentId,
                        isForDepartment,
                        isForUser));
                    #endregion
                }
                #endregion
                #endregion

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                response.Assignment = GetDetail(managerId, companyId, assignmentId, mainSession).Assignment;
                response.Success = true;

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLAssignmentPrivicyResponse GetDepartmentListForPrivacy(string managerId, string companyId, List<string> targetEducationIds, ISession mainSession = null)
        {
            MLAssignmentPrivicyResponse response = new MLAssignmentPrivicyResponse();
            response.Success = false;

            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                #region Step 1. Get all departments
                List<Department> allDepartments = new Department().GetAllDepartment(managerId, companyId, Department.QUERY_TYPE_BASIC, mainSession).Departments;
                #endregion

                #region Step 2. Get 每個 Education 的 department privacy.
                PreparedStatement ps = null;
                List<MLEducation> educations = new List<MLEducation>();
                for (int i = 0; i < targetEducationIds.Count; i++)
                {
                    MLEducation education = new MLEducation();
                    education.EducationId = targetEducationIds[i];

                    #region Step 2.1. Get targeted department
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_privacy", new List<string>(), new List<string> { "ml_education_id" }));
                    Row rPrivacy = mainSession.Execute(ps.Bind(targetEducationIds[i])).FirstOrDefault();
                    if (rPrivacy != null)
                    {
                        if (rPrivacy.GetValue<bool>("is_for_everyone"))
                        {
                            education.TargetedDepartments = allDepartments;
                        }
                        else if (rPrivacy.GetValue<bool>("is_for_department"))
                        {
                            education.TargetedDepartments = new List<Department>();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_department", new List<string>(), new List<string> { "ml_education_id" }));
                            RowSet rsDepartmentPrivacy = mainSession.Execute(ps.Bind(targetEducationIds[i]));
                            if (rsDepartmentPrivacy != null)
                            {
                                foreach (Row rDepartmentPrivacy in rsDepartmentPrivacy)
                                {
                                    DepartmentDetailResponse departmentResponse = new Department().GetDepartmentDetail(managerId, companyId, rDepartmentPrivacy.GetValue<string>("department_id"), Department.QUERY_TYPE_BASIC, mainSession);
                                    if (departmentResponse.Success)
                                    {
                                        education.TargetedDepartments.Add(departmentResponse.Department);
                                    }
                                    else
                                    {
                                        Log.Warn("Can not get the detail of this department. Department id: " + rDepartmentPrivacy.GetValue<string>("department_id"));
                                    }
                                }
                            }
                        }
                        else
                        {
                            education.TargetedDepartments = new List<Department>();
                        }
                        educations.Add(education);
                    }
                    else
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                        return response;
                    }
                    #endregion
                }
                #endregion

                #region Step 3. Get 聯集
                List<Department> intersectDepartments = new List<Department>();
                for (int i = 0; i < educations.Count; i++)
                {
                    if (i == 0)
                    {
                        intersectDepartments = educations[i].TargetedDepartments;
                    }
                    else
                    {
                        intersectDepartments = intersectDepartments.Intersect<Department>(educations[i].TargetedDepartments, new DepartmentComparer()).ToList();
                    }
                }
                #endregion

                #region Step 4. 整理資料
                response.AvailableDepartments = intersectDepartments;
                response.UnavailableDepartments = allDepartments.Except<Department>(intersectDepartments, new DepartmentComparer()).ToList();
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public MLAssignmentPrivicyResponse GetUserListForPrivacy(string managerId, string companyId, List<string> targetEducationIds, ISession mainSession = null)
        {
            MLAssignmentPrivicyResponse response = new MLAssignmentPrivicyResponse();
            response.Success = false;

            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                #region  Step 1. Get all users
                List<User> allUsers = new User().GetAllUserForAdmin(managerId, companyId, null, User.AccountType.CODE_ALL_USER, User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession).Users;
                #endregion

                #region Step 2. Get 每個 Education 的 user privacy.
                PreparedStatement ps = null;
                List<MLEducation> educations = new List<MLEducation>();
                for (int i = 0; i < targetEducationIds.Count; i++)
                {
                    MLEducation education = new MLEducation();
                    education.EducationId = targetEducationIds[i];

                    #region Step 2.1. Get targeted user
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_privacy", new List<string>(), new List<string> { "ml_education_id" }));
                    Row rPrivacy = mainSession.Execute(ps.Bind(targetEducationIds[i])).FirstOrDefault();
                    if (rPrivacy != null)
                    {
                        if (rPrivacy.GetValue<bool>("is_for_everyone"))
                        {
                            education.TargetedUsers = allUsers;
                        }
                        else if (rPrivacy.GetValue<bool>("is_for_user"))
                        {
                            education.TargetedUsers = new List<User>();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_user", new List<string>(), new List<string> { "ml_education_id" }));
                            RowSet rsUserPrivacy = mainSession.Execute(ps.Bind(targetEducationIds[i]));
                            if (rsUserPrivacy != null)
                            {
                                foreach (Row rUserPrivacy in rsUserPrivacy)
                                {
                                    UserDetailResponse userResponse = new User().GetUserDetail(managerId, companyId, rUserPrivacy.GetValue<string>("user_id"), mainSession);
                                    if (userResponse.Success)
                                    {
                                        education.TargetedUsers.Add(userResponse.User);
                                    }
                                    else
                                    {
                                        Log.Warn("Can not get the detail of this user. User id: " + rUserPrivacy.GetValue<string>("user_id"));
                                    }
                                }
                            }
                        }
                        else
                        {
                            education.TargetedUsers = new List<User>();
                        }
                        educations.Add(education);
                    }
                    else
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                        return response;
                    }
                    #endregion
                }
                #endregion

                #region Step 3. Get 聯集
                List<User> intersectList = new List<User>();
                for (int i = 0; i < educations.Count; i++)
                {
                    if (i == 0)
                    {
                        intersectList = educations[i].TargetedUsers;
                    }
                    else
                    {
                        intersectList = intersectList.Intersect<User>(educations[i].TargetedUsers, new UserComparer()).ToList();
                    }
                }
                #endregion

                #region Step 4. 整理資料
                response.AvailableUsers = intersectList;
                response.UnavailableUsers = allUsers.Except<User>(intersectList, new UserComparer()).ToList();
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLAssignmentPrivicyResponse GetPrivacy(string managerId, string companyId, List<string> targetEducationIds)
        {
            MLAssignmentPrivicyResponse response = new MLAssignmentPrivicyResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                MLAssignmentPrivicyResponse departmentPrivacyResponse = GetDepartmentListForPrivacy(managerId, companyId, targetEducationIds, mainSession);
                if (departmentPrivacyResponse.Success)
                {
                    response.AvailableDepartments = departmentPrivacyResponse.AvailableDepartments;
                    response.UnavailableDepartments = departmentPrivacyResponse.UnavailableDepartments;
                }
                else
                {
                    Log.Error(ErrorMessage.MLAssignmentPrivacyInvalid);
                    response.ErrorMessage = ErrorMessage.MLAssignmentPrivacyInvalid;
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentPrivacyInvalid);
                    return response;
                }


                MLAssignmentPrivicyResponse userPrivacyResponse = GetUserListForPrivacy(managerId, companyId, targetEducationIds, mainSession);
                if (userPrivacyResponse.Success)
                {
                    response.AvailableUsers = userPrivacyResponse.AvailableUsers;
                    response.UnavailableUsers = userPrivacyResponse.UnavailableUsers;
                }
                else
                {
                    Log.Error(ErrorMessage.MLAssignmentPrivacyInvalid);
                    response.ErrorMessage = ErrorMessage.MLAssignmentPrivacyInvalid;
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentPrivacyInvalid);
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public MLAssignmentDeleteResponse Delete(string managerId, string companyId, string assignmentId)
        {
            MLAssignmentDeleteResponse response = new MLAssignmentDeleteResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                #region Step 1. Validate account
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(managerId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 2. Change status to AssignmentStatusType.Deleted on ml_assignment table.
                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("ml_assignment", new List<string> { "id" }, new List<string> { "status", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind((int)MLAssignment.AssignmentStatusType.Deleted, managerId, DateTime.UtcNow, assignmentId));
                #endregion

                #region Step 3. Delete row on ml_assignment_by_timestamp table.
                // Get StartDate on this assignment.
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_assignment", new List<string>(), new List<string> { "id" }));
                Row rAssignment = mainSession.Execute(ps.Bind(assignmentId)).FirstOrDefault();
                if (rAssignment != null)
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("ml_assignment_by_timestamp", new List<string> { "company_id", "start_on_timestamp", "ml_assignment_id" }));
                    deleteBatch.Add(ps.Bind(companyId, rAssignment.GetValue<DateTime>("start_date"), assignmentId));

                    #region Delete Analytics ??
                    #endregion

                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.MLAssignmentInvalid);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLAssignmentInvalid);
                    response.ErrorMessage = ErrorMessage.MLAssignmentInvalid;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
