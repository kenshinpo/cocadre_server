﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    public class AnalyticDau
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum DauType
        {
            LastThirtyDays = 1,
            Weekly = 2,
            Monthly = 3
        }

        public enum DauComparableTimeFrame
        {
            Yesterday = 1,
            LastSevenDays = 2,
            LastThirtyDays = 3
        }

        public enum TimeActivityBaseType
        {
            OneMinute = 1,
            FifteenMinute = 2,
            ThiryMinute = 3,
            OneHour = 4
        }

        [DataContract]
        [Serializable]
        public class DetailDau
        {
            [DataMember]
            public int DauMetricType { get; set; }
            [DataMember]
            public List<DauColumn> DauColumns { get; set; }
            [DataMember]
            public DateTime CurrentDate { get; set; }
            [DataMember]
            public BasicDau CurrentDateDau { get; set; }
            [DataMember]
            public BasicDau LastSevenDaysDau { get; set; }
            [DataMember]
            public BasicDau LastThirtyDaysDau { get; set; }
            [DataMember]
            public int TotalCurrentUsers { get; set; }
            [DataMember]
            public string AverageTimeSpentPerUser { get; set; }
            [DataMember]
            public string TotalTimeSpent { get; set; }
            [DataMember]
            public int NumberOfIOS { get; set; }
            [DataMember]
            public int PercentageOfIOS { get; set; }
            [DataMember]
            public int NumberOfAndroid { get; set; }
            [DataMember]
            public int PercentageOfAndroid { get; set; }
        }

        [DataContract]
        [Serializable]
        public class DauColumn
        {
            [DataMember]
            public string XAxisDate { get; set; }
            [DataMember]
            public string XAxisDay { get; set; }
            [DataMember]
            public string XAxisMonth { get; set; }
            [DataMember]
            public string XAxisYear { get; set; }
            [DataMember]
            public string XAxisTitle { get; set; }
            [DataMember]
            public int NumberOfActiveUsers { get; set; }
            [DataMember]
            public int AverageHourSpent { get; set; }
        }

        [DataContract]
        [Serializable]
        public class DauChart
        {
            [DataMember]
            public string Date { get; set; }
            [DataMember]
            public int TotalCurrentUsers { get; set; }
            [DataMember]
            public int TotalUniqueActiveUsers { get; set; }
            [DataMember]
            public int AttendanceRatePercentage { get; set; }
            [DataMember]
            public string TotalHours { get; set; }
            [DataMember]
            public string AverageTimeSpentPerUser { get; set; }
        }

        [DataContract]
        [Serializable]
        public class TimeActivity
        {
            [DataMember]
            public string Time { get; set; }
            [DataMember]
            public int Minutes { get; set; }
            [DataMember]
            public int TotalActiveUsers { get; set; }
        }

        [DataContract]
        [Serializable]
        public class BasicDau
        {
            [DataMember]
            public DateTime CurrentDate { get; set; }
            [DataMember]
            public int TotalCurrentUsers { get; set; }
            [DataMember]
            public int TotalUniqueActiveUsers { get; set; }
            [DataMember]
            public int DifferenceInNumber { get; set; }
            [DataMember]
            public double DifferenceInPercentage { get; set; }
        }

        [DataContract]
        [Serializable]
        public class LoginDetail
        {
            [DataMember]
            public User User { get; set; }
            [DataMember]
            public DateTime? LastLoginDate { get; set; }
            [DataMember]
            public string LastLoginDateString { get; set; }
            [DataMember]
            public bool IsLogin { get; set; }
        }

        public AnalyticsSelectBasicResponse SelectSummaryReport(string adminUserId, string companyId)
        {
            AnalyticsSelectBasicResponse response = new AnalyticsSelectBasicResponse();
            response.BasicDau = new BasicDau();
            response.BasicTopicAttempts = new AnalyticQuiz.TopicAnalytics();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Basic DAU report
                DateTime currentDate = DateTime.UtcNow.Date.AddDays(-1);
                response.BasicDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.LastThirtyDays, currentDate, analyticSession);

                // Basic topic attempt report
                Topic topicManager = new Topic();
                List<Topic> topics = topicManager.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, null, null, null, false, true, mainSession).Topics;
                response.BasicTopicAttempts = new AnalyticQuiz().SelectBasicAttemptReport(topics, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private BasicDau SelectBasicDau(string companyId, int dauComparableTimeFrame, DateTime endDate, ISession analyticSession)
        {
            BasicDau basicDau = new BasicDau();

            try
            {
                DateTime currentDate = endDate;
                currentDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                currentDate = currentDate.AddSeconds(-currentDate.Second);

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("daily_active_user",
                    new List<string>(), new List<string> { "company_id", "datestamp" }));
                Row currentDateRow = analyticSession.Execute(ps.Bind(companyId, currentDate)).FirstOrDefault();

                if (currentDateRow == null)
                {
                    // No record for end date = future
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("daily_active_user",
                    new List<string>(), new List<string> { "company_id", "datestamp" }));
                    currentDateRow = analyticSession.Execute(ps.Bind(companyId, DateTime.UtcNow.Date.AddDays(-1))).FirstOrDefault();
                }

                int totalNumberOfUsers = currentDateRow.GetValue<int>("number_total_users");
                int numberOfDaysDifference = 0;

                if (dauComparableTimeFrame == (int)DauComparableTimeFrame.Yesterday)
                {
                    numberOfDaysDifference = 1;
                }
                else if (dauComparableTimeFrame == (int)DauComparableTimeFrame.LastSevenDays)
                {
                    numberOfDaysDifference = 7;
                }
                else if (dauComparableTimeFrame == (int)DauComparableTimeFrame.LastThirtyDays)
                {
                    numberOfDaysDifference = 30;
                }

                // Last x days
                DateTime previousDate = currentDate.AddDays(-numberOfDaysDifference);
                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("daily_active_user_by_user",
                    null, new List<string> { "company_id" }, "datestamp", CQLGenerator.Comparison.GreaterThan, "datestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                RowSet dauByUserRowSet = analyticSession.Execute(ps.Bind(companyId, previousDate, currentDate));

                int previousActiveSum = 0;
                List<string> userIdList = new List<string>();

                foreach (Row dauByUserRow in dauByUserRowSet)
                {
                    string userId = dauByUserRow.GetValue<string>("user_id");
                    if (!userIdList.Contains(userId))
                    {
                        userIdList.Add(userId);
                        previousActiveSum++;
                    }
                }

                // Last 2x days
                DateTime lastPreviousDate = previousDate.AddDays(-numberOfDaysDifference);
                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("daily_active_user_by_user",
                    null, new List<string> { "company_id" }, "datestamp", CQLGenerator.Comparison.GreaterThan, "datestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                dauByUserRowSet = analyticSession.Execute(ps.Bind(companyId, lastPreviousDate, previousDate));

                int lastPreviousActiveSum = 0;
                userIdList = new List<string>();

                foreach (Row dauByUserRow in dauByUserRowSet)
                {
                    string userId = dauByUserRow.GetValue<string>("user_id");
                    if (!userIdList.Contains(userId))
                    {
                        userIdList.Add(userId);
                        lastPreviousActiveSum++;
                    }
                }

                basicDau.CurrentDate = currentDate;
                basicDau.TotalCurrentUsers = totalNumberOfUsers;

                double difference = 0;
                double percentage = 0;

                difference = previousActiveSum - lastPreviousActiveSum;

                if (lastPreviousActiveSum != 0)
                {
                    percentage = (difference / lastPreviousActiveSum) * 100;
                }

                basicDau.DifferenceInNumber = (int)difference;
                basicDau.DifferenceInPercentage = percentage;
                basicDau.TotalUniqueActiveUsers = previousActiveSum;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return basicDau;
        }

        public AnalyticsSelectDetailDauResponse SelectDau(string adminUserId, string companyId, DateTime startUtcDate, DateTime endUtcDate)
        {
            AnalyticsSelectDetailDauResponse response = new AnalyticsSelectDetailDauResponse();
            response.DetailDau = new DetailDau();
            response.DauChart = new DauChart();
            response.TimeActivities = new List<TimeActivity>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Will always be thirty days
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                response.DetailDau = SelectDetailDauForThirtyDays(companyId, timezoneOffset, endUtcDate, analyticSession);
                response.DauChart.Date = "Last 30 days";
                response.DauChart.AverageTimeSpentPerUser = response.DetailDau.AverageTimeSpentPerUser;
                response.DauChart.TotalCurrentUsers = response.DetailDau.LastThirtyDaysDau.TotalCurrentUsers;
                response.DauChart.TotalUniqueActiveUsers = response.DetailDau.LastThirtyDaysDau.TotalUniqueActiveUsers;
                response.DauChart.TotalHours = response.DetailDau.TotalTimeSpent;
                response.DauChart.AttendanceRatePercentage = (int)(((double)response.DauChart.TotalUniqueActiveUsers / response.DauChart.TotalCurrentUsers) * 100.00);

                //response.TimeActivities = SelectTimeActivityForOneWeek(companyId, timeActivityFrameType, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<TimeActivity> SelectTimeActivityForOneWeek(string companyId, int timeActivityFrame, ISession analyticSession)
        {
            List<TimeActivity> timeActivities = new List<TimeActivity>();

            try
            {
                PreparedStatement preparedStatement = null;
                preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("daily_time_period",
                    new List<string>(), new List<string> { "company_id" }, 1));
                Row firstRow = analyticSession.Execute(preparedStatement.Bind(companyId)).FirstOrDefault();

                if (firstRow != null)
                {
                    int numberOfDays = 7;

                    DateTime currentDate = firstRow.GetValue<DateTime>("datestamp");
                    currentDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                    currentDate = currentDate.AddSeconds(-currentDate.Second);

                    int incremental = 0;
                    int totalMinutes = 60 * 24;
                    if (timeActivityFrame == (int)TimeActivityBaseType.OneMinute)
                    {
                        incremental = 1;
                    }
                    else if (timeActivityFrame == (int)TimeActivityBaseType.FifteenMinute)
                    {
                        incremental = 15;
                    }
                    else if (timeActivityFrame == (int)TimeActivityBaseType.ThiryMinute)
                    {
                        incremental = 30;
                    }
                    else if (timeActivityFrame == (int)TimeActivityBaseType.OneHour)
                    {
                        incremental = 60;
                    }

                    for (int addedMinutes = 0; addedMinutes <= totalMinutes - 1; addedMinutes += incremental)
                    {
                        int totalUsers = 0;
                        for (int day = 7; day >= 0; day--)
                        {
                            DateTime pastDate = currentDate.AddDays(-day);
                            pastDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                            pastDate = currentDate.AddSeconds(-currentDate.Second);

                            DateTime pastDateTime = pastDate.AddMinutes(addedMinutes);

                            preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("daily_time_period",
                                null, new List<string> { "company_id", "datestamp" }, "timestamp", CQLGenerator.Comparison.LessThanOrEquals, 1));
                            Row timeFrameRow = analyticSession.Execute(preparedStatement.Bind(companyId, pastDate, pastDateTime)).FirstOrDefault();

                            if (timeFrameRow != null)
                            {
                                int numberOfActiveUsersPerDay = timeFrameRow.GetValue<int>("number_active_user");
                                totalUsers += numberOfActiveUsersPerDay;
                            }
                        }

                        TimeSpan span = TimeSpan.FromMinutes(addedMinutes);

                        TimeActivity timeActivity = new TimeActivity
                        {
                            Time = string.Format("{0}:{1}", (int)span.TotalHours, span.Minutes),
                            Minutes = addedMinutes,
                            TotalActiveUsers = totalUsers / numberOfDays
                        };


                        timeActivities.Add(timeActivity);

                        // To include 23:59
                        if (timeActivityFrame != (int)TimeActivityBaseType.OneMinute && addedMinutes + incremental > totalMinutes - 1)
                        {
                            totalUsers = 0;
                            for (int day = 7; day >= 0; day--)
                            {
                                DateTime pastDate = currentDate.AddDays(-day);
                                pastDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                                pastDate = currentDate.AddSeconds(-currentDate.Second);

                                DateTime pastDateTime = pastDate.AddMinutes(totalMinutes - 1);

                                preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("daily_time_period",
                                    null, new List<string> { "company_id", "datestamp" }, "timestamp", CQLGenerator.Comparison.LessThanOrEquals, 1));
                                Row timeFrameRow = analyticSession.Execute(preparedStatement.Bind(companyId, pastDate, pastDateTime)).FirstOrDefault();

                                if (timeFrameRow != null)
                                {
                                    int numberOfActiveUsersPerDay = timeFrameRow.GetValue<int>("number_active_user");
                                    totalUsers += numberOfActiveUsersPerDay;
                                }
                            }

                            span = TimeSpan.FromMinutes(totalMinutes - 1);

                            timeActivity = new TimeActivity
                            {
                                Time = string.Format("{0}:{1}", (int)span.TotalHours, span.Minutes),
                                Minutes = totalMinutes - 1,
                                TotalActiveUsers = totalUsers / numberOfDays
                            };

                            timeActivities.Add(timeActivity);
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return timeActivities;
        }

        private DetailDau SelectDetailDauForThirtyDays(string companyId, double timezoneOffset, DateTime endUtcDate, ISession analyticSession)
        {
            DetailDau detailDau = new DetailDau();
            detailDau.DauColumns = new List<DauColumn>();
            detailDau.CurrentDateDau = new BasicDau();
            detailDau.LastSevenDaysDau = new BasicDau();
            detailDau.LastThirtyDaysDau = new BasicDau();

            int days = 30;

            try
            {
                DateTime endDate = endUtcDate;
                DateTime currentDate = DateTime.UtcNow.Date;
                DateTime startDate = endDate.AddDays(-days);

                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("daily_active_user",
                                       null, new List<string> { "company_id" }, "datestamp", CQLGenerator.Comparison.GreaterThan, "datestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                List<Row> dauRowList = analyticSession.Execute(ps.Bind(companyId, startDate, endDate)).ToList();

                if(dauRowList.Count > 0)
                {
                    double totalTimeTakenMin = 0;

                    detailDau.NumberOfIOS = 0;
                    detailDau.NumberOfAndroid = 0;

                    foreach (Row dauRow in dauRowList)
                    {
                        double timeTakenMin = dauRow.GetValue<double>("total_time_taken_min");
                        totalTimeTakenMin += timeTakenMin;

                        DateTime datestamp = dauRow.GetValue<DateTime>("datestamp");

                        DauColumn column = new DauColumn();
                        int numberOfActiveUsers = dauRow.GetValue<int>("number_active_users");

                        detailDau.NumberOfIOS += dauRow.GetValue<int>("number_os_ios");
                        detailDau.NumberOfAndroid += dauRow.GetValue<int>("number_os_android");

                        // Get the month of the last day
                        column.XAxisTitle = datestamp.ToString("MMMM");

                        double averageTimeTakenMin = 0.0;
                        column.AverageHourSpent = 0;
                        if (timeTakenMin > 0 && numberOfActiveUsers > 0)
                        {
                            averageTimeTakenMin = timeTakenMin / numberOfActiveUsers;
                            column.AverageHourSpent = (int)Math.Ceiling(averageTimeTakenMin / 60);
                        }

                        column.XAxisDate = datestamp.Date.ToString();
                        column.XAxisDay = datestamp.Day.ToString();
                        column.XAxisMonth = datestamp.Month.ToString();
                        column.XAxisYear = datestamp.Year.ToString();
                        column.NumberOfActiveUsers = numberOfActiveUsers;

                        detailDau.DauColumns.Add(column);
                    }

                    // Padding purposes
                    if (detailDau.DauColumns.Count < days)
                    {
                        DateTime paddingStartDate = dauRowList[0].GetValue<DateTime>("datestamp");
                        paddingStartDate = paddingStartDate.AddDays(1);

                        int numberOfDaysDifference = days - detailDau.DauColumns.Count;
                        for (int index = 1; index <= numberOfDaysDifference; index++)
                        {
                            DauColumn column = new DauColumn();
                            column.XAxisDate = paddingStartDate.Date.ToString();
                            column.XAxisDay = paddingStartDate.Day.ToString();
                            column.XAxisMonth = paddingStartDate.Month.ToString();
                            column.XAxisYear = paddingStartDate.Year.ToString();
                            column.NumberOfActiveUsers = 0;
                            column.AverageHourSpent = 0;

                            detailDau.DauColumns.Insert(0, column);
                            paddingStartDate = paddingStartDate.AddDays(1);
                        }
                    }

                    if (endUtcDate > currentDate)
                    {
                        endDate = currentDate;
                    }

                    // Non graph detail
                    detailDau.CurrentDateDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.Yesterday, endDate, analyticSession);
                    detailDau.LastSevenDaysDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.LastSevenDays, endDate, analyticSession);
                    detailDau.LastThirtyDaysDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.LastThirtyDays, endDate, analyticSession);

                    // Fomulating average time spent per user
                    double averageTimeTakenByUser = totalTimeTakenMin / detailDau.LastThirtyDaysDau.TotalUniqueActiveUsers;
                    detailDau.AverageTimeSpentPerUser = string.Format("{0}hrs {1}mins", (int)averageTimeTakenByUser / 60, (int)averageTimeTakenByUser % 60);

                    detailDau.TotalTimeSpent = string.Format("{0} man-hours", (int)Math.Ceiling(totalTimeTakenMin / 60));

                    detailDau.CurrentDate = detailDau.CurrentDateDau.CurrentDate;

                    // Formulating average device
                    detailDau.NumberOfIOS = detailDau.NumberOfIOS / days;
                    detailDau.NumberOfAndroid = detailDau.NumberOfAndroid / days;

                    if (detailDau.NumberOfIOS + detailDau.NumberOfAndroid > 0)
                    {
                        detailDau.PercentageOfIOS = (int)((double)detailDau.NumberOfIOS / (detailDau.NumberOfIOS + detailDau.NumberOfAndroid) * 100);
                        detailDau.PercentageOfAndroid = 100 - detailDau.PercentageOfIOS;
                    }

                    // Current number of users
                    detailDau.TotalCurrentUsers = detailDau.CurrentDateDau.TotalCurrentUsers;
                }
                else
                {
                    for (int index = 1; index <= days; index++)
                    {
                        DauColumn column = new DauColumn();
                        column.XAxisDate = endDate.Date.ToString();
                        column.XAxisDay = endDate.Day.ToString();
                        column.XAxisMonth = endDate.Month.ToString();
                        column.XAxisYear = endDate.Year.ToString();
                        column.NumberOfActiveUsers = 0;
                        column.AverageHourSpent = 0;

                        detailDau.DauColumns.Add(column);

                        endDate = endDate.AddDays(-1);
                    }

                    // Fomulating average time spent per user
                    detailDau.AverageTimeSpentPerUser = "0 mins";

                    detailDau.TotalTimeSpent = "0 man-hours";

                    detailDau.CurrentDate = endDate.AddHours(timezoneOffset).Date;

                    // Formulating average device
                    detailDau.NumberOfIOS = 0;
                    detailDau.NumberOfAndroid = 0;

                    detailDau.PercentageOfIOS = 0;
                    detailDau.PercentageOfAndroid = 0;

                    // Current number of users
                    detailDau.TotalCurrentUsers = 0;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return detailDau;
        }

        public AnalyticsUpdateDailyActiveUserResponse UpdateDailyActiveUser(DateTime today)
        {
            AnalyticsUpdateDailyActiveUserResponse response = new AnalyticsUpdateDailyActiveUserResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticsSession = cm.getAnalyticSession();

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("company",
                    new List<string>(), new List<string> { "is_valid" }));
                RowSet companyRowSet = mainSession.Execute(ps.Bind(true));

                // UTC (lack of one day)
                if (today == DateTime.MinValue)
                {
                    today = DateTime.UtcNow.Date.AddDays(1);
                }

                DateTime yesterday = today.Date.AddDays(-1);

                foreach (Row companyRow in companyRowSet)
                {
                    string companyId = companyRow.GetValue<string>("id");
                    ps = analyticsSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("user_activity_log",
                        null, new List<string> { "company_id" }, "activity_date_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "activity_date_timestamp", CQLGenerator.Comparison.LessThan, -1));
                    RowSet activityRowSet = analyticsSession.Execute(ps.Bind(companyId, yesterday, today));

                    int totalNumberOfUsers = 0;
                    // Total number of users
                    if (today == DateTime.UtcNow.Date.AddDays(1))
                    {
                        List<Department> departments = new Department().GetAllDepartment(null, companyId, Department.QUERY_TYPE_DETAIL, mainSession).Departments;

                        foreach (Department department in departments)
                        {
                            totalNumberOfUsers += (int)department.CountOfUsers;
                        }
                    }
                    else
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "company_id" }));
                        RowSet userRowSet = mainSession.Execute(ps.Bind(companyId));

                        List<Row> userRows = userRowSet.Where(r => r.GetValue<DateTime>("created_on_timestamp") < today).ToList();
                        totalNumberOfUsers = userRows.Count();
                    }


                    List<Dictionary<string, object>> userList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> timeFrameList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> userWithLastActiveList = new List<Dictionary<string, object>>();
                    double totalTimeTakenInMin = 0.0;

                    int numberOfActiveTimeFrameUsers = 0;

                    int numberOfActiveUsers = 0;
                    int numberOfIOS = 0;
                    int numberOfAndroid = 0;

                    foreach (Row activityRow in activityRowSet)
                    {
                        string userId = activityRow.GetValue<string>("user_id");
                        DateTime dateTimestamp = activityRow.GetValue<DateTime>("activity_date_timestamp").ToUniversalTime();
                        dateTimestamp = dateTimestamp.AddMilliseconds(-dateTimestamp.Millisecond);
                        dateTimestamp = dateTimestamp.AddSeconds(-dateTimestamp.Second);

                        bool isActive = activityRow.GetValue<bool>("is_active");

                        if (isActive)
                        {
                            if (!userList.Any(user => user["userId"].Equals(userId)))
                            {
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_device_token",
                                    new List<string>(), new List<string> { "user_id" }));
                                Row deviceRow = mainSession.Execute(ps.Bind(userId)).FirstOrDefault();

                                if (deviceRow != null)
                                {
                                    int deviceType = deviceRow.GetValue<int>("device_type");

                                    if (deviceType == (int)User.DeviceTokenType.iOS)
                                    {
                                        numberOfIOS++;
                                    }
                                    else
                                    {
                                        numberOfAndroid++;
                                    }

                                    numberOfActiveUsers++;

                                    Dictionary<string, object> userDict = new Dictionary<string, object>();
                                    userDict.Add("userId", userId);
                                    userDict.Add("deviceType", deviceType);
                                    userList.Add(userDict);
                                }
                            }

                            if (!userWithLastActiveList.Any(active => active["userId"].Equals(userId)))
                            {
                                Dictionary<string, object> userWithLastActive = new Dictionary<string, object>();
                                userWithLastActive.Add("userId", userId);
                                userWithLastActive.Add("lastActive", dateTimestamp);
                                userWithLastActiveList.Add(userWithLastActive);
                            }
                            else
                            {
                                Dictionary<string, object> userWithLastActive = userWithLastActiveList.First(active => ((string)active["userId"]).Equals(userId));
                                userWithLastActive["lastActive"] = dateTimestamp;
                            }


                            numberOfActiveTimeFrameUsers++;

                            if (!timeFrameList.Any(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp))
                            {
                                Dictionary<string, object> timeFrame = new Dictionary<string, object>();

                                timeFrame.Add("dateTimestamp", dateTimestamp);
                                timeFrame.Add("active", numberOfActiveTimeFrameUsers);

                                timeFrameList.Add(timeFrame);
                            }
                            else
                            {
                                Dictionary<string, object> timeFrameDict = timeFrameList.First(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp);
                                timeFrameDict["active"] = numberOfActiveTimeFrameUsers;
                            }
                        }
                        else
                        {
                            if (numberOfActiveTimeFrameUsers - 1 >= 0)
                            {
                                numberOfActiveTimeFrameUsers--;
                            }

                            if (!timeFrameList.Any(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp))
                            {
                                Dictionary<string, object> timeFrame = new Dictionary<string, object>();

                                timeFrame.Add("dateTimestamp", dateTimestamp);
                                timeFrame.Add("active", numberOfActiveTimeFrameUsers);

                                timeFrameList.Add(timeFrame);
                            }
                            else
                            {
                                Dictionary<string, object> timeFrameDict = timeFrameList.First(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp);
                                timeFrameDict["active"] = numberOfActiveTimeFrameUsers;
                            }

                            if (userWithLastActiveList.Any(active => ((string)active["userId"]).Equals(userId)))
                            {
                                // Calculate time taken
                                Dictionary<string, object> userWithLastActive = userWithLastActiveList.First(active => ((string)active["userId"]).Equals(userId));
                                double minTaken = dateTimestamp.Subtract((DateTime)userWithLastActive["lastActive"]).TotalMinutes;
                                totalTimeTakenInMin += minTaken;
                            }

                        }
                    }

                    //Write to db
                    BatchStatement batchStatement = new BatchStatement();

                    foreach (Dictionary<string, object> userDict in userList)
                    {
                        string userId = userDict["userId"].ToString();

                        ps = analyticsSession.Prepare(CQLGenerator.InsertStatement("daily_active_user_by_user",
                            new List<string> { "company_id", "datestamp", "user_id" }));
                        batchStatement.Add(ps.Bind(companyId, yesterday, userId));
                    }
                    analyticsSession.Execute(batchStatement);
                    batchStatement = new BatchStatement();

                    ps = analyticsSession.Prepare(CQLGenerator.InsertStatement("daily_active_user",
                        new List<string> { "company_id", "datestamp", "number_active_users", "number_total_users", "number_os_ios", "number_os_android", "total_time_taken_min" }));
                    batchStatement.Add(ps.Bind(companyId, yesterday, numberOfActiveUsers, totalNumberOfUsers, numberOfIOS, numberOfAndroid, totalTimeTakenInMin));

                    foreach (Dictionary<string, object> timeFrame in timeFrameList)
                    {
                        DateTime timestamp = (DateTime)timeFrame["dateTimestamp"];
                        int numberOfActiveUserForTimeframe = (int)timeFrame["active"];

                        ps = analyticsSession.Prepare(CQLGenerator.InsertStatement("daily_time_period",
                            new List<string> { "company_id", "datestamp", "timestamp", "number_active_user" }));
                        batchStatement.Add(ps.Bind(companyId, yesterday, timestamp, numberOfActiveUserForTimeframe));
                    }

                    analyticsSession.Execute(batchStatement);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticLoginDetailResponse SelectLoginDetail(string adminUserId, string companyId, DateTime fromUtc, DateTime toUtc)
        {
            AnalyticLoginDetailResponse response = new AnalyticLoginDetailResponse();
            response.LoginDetails = new List<LoginDetail>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ValidationHandler vh = new ValidationHandler();

                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                User userManager = new User();
                double offset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                string selectStatement = CQLGenerator.SelectStatementWithDateRangeComparison("user_login_log",
                   new List<string>(), new List<string> { "company_id" }, "login_date_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "login_date_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0);

                PreparedStatement ps = analyticSession.Prepare(selectStatement);

                RowSet loginRowSet = analyticSession.Execute(ps.Bind(companyId, fromUtc, toUtc));

                foreach (Row loginRow in loginRowSet)
                {
                    DateTime loginDate = loginRow.GetValue<DateTime>("login_date_timestamp");
                    loginDate = loginDate.AddHours(offset);
                    string loginDateString = loginDate.ToString("yyyy/MM/dd HH:mm:ss");

                    string userId = loginRow.GetValue<string>("user_id");
                    bool isLogin = loginRow.GetValue<bool>("is_login");

                    if (response.LoginDetails.FirstOrDefault(l => l.User.UserId.Equals(userId)) == null && isLogin)
                    {
                        User user = userManager.SelectUserBasic(userId, companyId, false, mainSession, null, true).User;

                        if (user != null)
                        {
                            if(user.Departments.Count == 0)
                            {
                                user.Departments.Add(new Department
                                {
                                    Title = "Deleted"
                                });
                            }

                            response.LoginDetails.Add(new LoginDetail
                            {
                                User = user,
                                LastLoginDate = loginDate,
                                LastLoginDateString = loginDateString,
                                IsLogin = isLogin
                            });
                        }
                    }                    
                }

                List<User> allUsers = userManager.GetAllUserForAdmin(adminUserId, companyId, null, 0, User.AccountStatus.CODE_ACTIVE, null, false, false, toUtc, mainSession).Users;
                List<User> haveNotLoginUsers = allUsers.Where(u => !response.LoginDetails.Any(l => l.User.UserId.Equals(u.UserId))).ToList();
                haveNotLoginUsers = haveNotLoginUsers.OrderBy(u => u.Departments[0].Title).ThenBy(u => u.FirstName).ThenBy(u => u.LastName).ToList();
                response.LoginDetails = response.LoginDetails.OrderBy(l => l.LastLoginDate).ToList();
                foreach(User notLoginUser in haveNotLoginUsers)
                {
                    response.LoginDetails.Add(new LoginDetail
                    {
                        User = notLoginUser,
                        LastLoginDate = null,
                        LastLoginDateString = "Not Login",
                        IsLogin = false
                    });
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }


    }
}
