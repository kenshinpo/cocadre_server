﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class Appraisal
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        private delegate Task<HttpGenerateAppraisalResponse> GenerateReportDelegate(string companyId, string creatorUserId, string appraisalTitle, string appraisalId, ISession session);

        public enum AppraisalQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum AppraisalTypeEnum
        {
            All = 0,
            Likert7 = 1,
            Bars = 2
        }

        public enum AppraisalDisplayModeEnum
        {
            PulseArea = 1,
            FullScreen = 2
        }

        public enum AppraisalAnonymityEnum
        {
            UserDefined = 1,
            ForcedAnonymous = 2,
            ForceNonAnonymous = 3
        }

        public enum AppraisalProgressEnum
        {
            Active = 1,
            Upcoming = 2,
            Completed = 3
        }

        public enum AppraisalPublishMethodEnum
        {
            Schedule = 1,
            Perpetual = 2
        }

        public enum AppraisalStatusEnum
        {
            Deleted = -1,
            Unlisted = 1,
            Active = 2,
            Hidden = 3
        }
        [DataMember]
        public string AppraisalId { get; set; }
        [DataMember]
        public string MappedPulseId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string IconUrl { get; set; }

        [DataMember]
        public int DisplayMode { get; set; }

        [DataMember]
        public int AppraisalType { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int Progress { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public int Anonymity { get; set; }

        [DataMember]
        public bool IsUserAllowToCreateQuestions { get; set; }

        [DataMember]
        public bool IsPrioritized { get; set; }

        [DataMember]
        public List<User> Participants { get; set; }

        [DataMember]
        public List<AppraisalCard> Cards { get; set; }

        [DataMember]
        public bool IsSelfAssessmentNeeded { get; set; }

        [DataMember]
        public bool IsAnonymous { get; set; }

        [DataMember]
        public int NumberOfParticipants { get; set; }

        [DataMember]
        public int NumberOfParticipantsCompleted { get; set; }

        [DataMember]
        public int PublishMethod { get; set; }

        [DataMember]
        public List<AppraisalCategory> Categories { get; set; }

        [DataMember]
        public List<AppraisalCategory> UserDefinedCategories { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<AppraisalCardRange> Likert7Scale { get; set; }

        [DataMember]
        public User CreatedByUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public AppraisalPrivacy Privacy { get; set; }

        [DataMember]
        public int NumberOfCards { get; set; }

        [DataMember]
        public DateTime? CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int GroupTeamType { get; set; }

        [DataMember]
        public int QuestionLimit { get; set; }

        [DataMember]
        public List<AppraisalTeamType> TeamTypes { get; set; }

        [DataMember]
        public AppraisalTeamType SelectedTeamType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<AppraisalGroupTeamType> GroupTeamTypes { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TemplateAppraisalId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ParticipantBreakdown> ParticipantsBreakdown { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string StartDateString { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string StartTimeString { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EndDateString { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EndTimeString { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Duration { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AverageTimeTaken { get; set; }

        [DataContract]
        public class AppraisalPrivacy
        {
            [DataMember]
            public bool IsForEveryone { get; set; }

            [DataMember]
            public bool IsForDepartment { get; set; }

            [DataMember]
            public bool IsForUser { get; set; }

            [DataMember]
            public bool IsForCustomGroup { get; set; }

            [DataMember]
            public List<Department> TargetedDepartments { get; set; }

            [DataMember]
            public List<User> TargetedUsers { get; set; }
        }

        [DataContract]
        public class AppraisalTeamType
        {
            [DataMember]
            public int Id { get; set; }

            [DataMember]
            public string Label { get; set; }

            [DataMember]
            public bool IsSelected { get; set; }
        }

        [DataContract]
        public class AppraisalGroupTeamType
        {
            [DataMember]
            public int Id { get; set; }

            [DataMember]
            public string GroupTitle { get; set; }

            [DataMember]
            public List<AppraisalTeamType> TeamTypes { get; set; }
        }

        [DataContract]
        public class ParticipantBreakdown
        {
            [DataMember]
            public User Participant { get; set; }

            [DataMember]
            public double TimeTaken { get; set; }

            [DataMember]
            public string TimeTakenString { get; set; }

            [DataMember]
            public List<AppraisalCard.ParticipantAnswerBreakdown> AnswersBreakdown { get; set; }
        }

        [DataContract]
        public class ReferencedCategoryBreakdown
        {
            [DataMember]
            public AppraisalCategory Category { get; set; }

            [DataMember]
            public int NumberOfTimesReferenced { get; set; }
        }

        public AppraisalSelectGroupTeamTypeResponse SelectGroupTeamTypes(string adminUserId,
                                                                         string companyId)
        {
            AppraisalSelectGroupTeamTypeResponse response = new AppraisalSelectGroupTeamTypeResponse();
            response.GroupTeamTypes = new List<AppraisalGroupTeamType>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Use group by in Cassandra 3.1 and up
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_group_team_type_by_company", new List<string>(), new List<string> { "company_id" }));
                RowSet groupTeamRowSet = mainSession.Execute(ps.Bind(companyId));
                foreach (Row groupTeamRow in groupTeamRowSet)
                {
                    int groupId = groupTeamRow.GetValue<int>("group_id");
                    AppraisalGroupTeamType groupTeam = response.GroupTeamTypes.FirstOrDefault(g => g.Id == groupId);
                    if (groupTeam == null)
                    {
                        groupTeam = new AppraisalGroupTeamType
                        {
                            Id = groupId,
                            GroupTitle = string.Empty,
                            TeamTypes = new List<AppraisalTeamType>()
                        };
                        response.GroupTeamTypes.Add(groupTeam);
                    }

                    groupTeam.TeamTypes.Add(new AppraisalTeamType
                    {
                        Id = groupTeamRow.GetValue<int>("team_type_id"),
                        Label = groupTeamRow.GetValue<string>("label")
                    });

                    groupTeam.GroupTitle = string.Join(",", groupTeam.TeamTypes.Select(t => t.Label).ToList());
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCreateTemplateResponse CreateTemplate(string adminUserId,
                                                              string companyId,
                                                              int templateType,
                                                              string title,
                                                              string description,
                                                              bool isPrioritized,
                                                              int anonymity,
                                                              bool isUserAllowToCreateQuestions,
                                                              int groupTeamType,
                                                              int questionLimit,
                                                              List<string> targetedDepartmentIds,
                                                              List<string> targetedUserIds,
                                                              int publishMethod,
                                                              DateTime startDate,
                                                              DateTime? endDate)
        {
            AppraisalCreateTemplateResponse response = new AppraisalCreateTemplateResponse();
            response.AppraisalId = string.Empty;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                DateTime currentDate = DateTime.UtcNow;
                if (endDate != null && endDate.HasValue)
                {
                    publishMethod = (int)AppraisalPublishMethodEnum.Schedule;
                }
                AppraisalValidationResponse validateFieldResponse = ValidateTemplateFields(companyId, title, publishMethod, groupTeamType, questionLimit, startDate, endDate, currentDate, mainSession);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);
                if (companySettingRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompanyNotOnboard);
                    response.ErrorMessage = ErrorMessage.AppraisalCompanyNotOnboard;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                string appraisalId = UUIDGenerator.GenerateUniqueIDForAppraisal();
                List<BoundStatement> bsPrivacyList = CreateTemplatePrivacy(appraisalId, targetedDepartmentIds, targetedUserIds, mainSession);
                foreach (BoundStatement bs in bsPrivacyList)
                {
                    batch.Add(bs);
                }

                int displayMode = (int)AppraisalDisplayModeEnum.PulseArea;
                if (templateType == (int)AppraisalTypeEnum.Bars)
                {
                    displayMode = (int)AppraisalDisplayModeEnum.FullScreen;
                    isUserAllowToCreateQuestions = false;
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360",
                    new List<string> { "company_id", "appraisal_id", "title", "description", "display_mode", "is_prioritized", "type", "anonymity", "is_user_allow_create_questions", "publish_method", "start_date", "end_date", "group_team_type", "question_limit", "status", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(companyId, appraisalId, title, description, displayMode, isPrioritized, templateType, anonymity, isUserAllowToCreateQuestions, publishMethod, startDate, endDate, groupTeamType, questionLimit, (int)AppraisalStatusEnum.Unlisted, adminUserId, currentDate, adminUserId, currentDate));

                mainSession.Execute(batch);
                response.AppraisalId = appraisalId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCreateResponse CreateCustomAppraisal(string creatorUserId,
                                                            string companyId,
                                                            string templateAppraisalId,
                                                            string title,
                                                            string description,
                                                            bool isSelfAssessmentNeeded,
                                                            bool isAnonymous,
                                                            int teamType,
                                                            List<string> participantIds,
                                                            DateTime startDate,
                                                            DateTime endDate,
                                                            List<AppraisalCard> cards,
                                                            Appraisal currentAppraisal = null)
        {
            AppraisalCreateResponse response = new AppraisalCreateResponse();
            response.AppraisalId = string.Empty;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(creatorUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);
                if (companySettingRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompanyNotOnboard);
                    response.ErrorMessage = ErrorMessage.AppraisalCompanyNotOnboard;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                DateTime currentDate = DateTime.UtcNow;
                DateTime createdTimestamp = currentDate;

                participantIds = participantIds.Distinct().ToList();
                participantIds.Remove(creatorUserId);
                AppraisalValidationResponse validateFieldResponse = ValidateFields(companyId, title, description, templateRow.GetValue<int>("group_team_type"), teamType, participantIds, isAnonymous, templateRow.GetValue<int>("question_limit"), cards.Count, startDate, endDate, currentDate, mainSession);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                List<string> privacyParticipants = new List<string>();
                privacyParticipants.AddRange(participantIds);

                if (isSelfAssessmentNeeded)
                {
                    privacyParticipants.Add(creatorUserId);
                }

                bool isUpdate = false;
                string appraisalId = UUIDGenerator.GenerateUniqueIDForAppraisal();
                string pulseId = UUIDGenerator.GenerateUniqueIDForApprasialPulse();
                if (currentAppraisal != null)
                {
                    isUpdate = true;
                    appraisalId = currentAppraisal.AppraisalId;
                    pulseId = currentAppraisal.MappedPulseId;
                    createdTimestamp = currentAppraisal.CreatedOnTimestamp.Value;
                }

                BatchStatement batch = new BatchStatement();
                foreach (BoundStatement bs in CreateAppraisalPrivacy(appraisalId, privacyParticipants, mainSession))
                {
                    batch.Add(bs);
                }

                bool isPrioritized = templateRow.GetValue<bool>("is_prioritized");
                int displayMode = templateRow.GetValue<int>("display_mode");
                int templateType = templateRow.GetValue<int>("type");
                int questionLimit = templateRow.GetValue<int>("question_limit");

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360",
                   new List<string> { "company_id", "appraisal_id", "referenced_template_id", "pulse_id", "title", "description", "display_mode", "is_prioritized", "type", "is_self_assessment_needed", "is_anonymous", "team_type", "question_limit", "number_of_participants", "start_date", "end_date", "status", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(companyId, appraisalId, templateAppraisalId, pulseId, title, description, displayMode, isPrioritized, templateType, isSelfAssessmentNeeded, isAnonymous, teamType, questionLimit, participantIds.Count, startDate, endDate, (int)AppraisalStatusEnum.Active, creatorUserId, createdTimestamp, creatorUserId, currentDate));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_by_start_date",
                    new List<string> { "company_id", "appraisal_id", "referenced_template_id", "pulse_id", "title", "description", "display_mode", "is_prioritized", "type", "is_self_assessment_needed", "is_anonymous", "team_type", "question_limit", "number_of_participants", "start_date", "end_date", "status", "created_by_user_id" }));
                batch.Add(ps.Bind(companyId, appraisalId, templateAppraisalId, pulseId, title, description, displayMode, isPrioritized, templateType, isSelfAssessmentNeeded, isAnonymous, teamType, questionLimit, participantIds.Count, startDate, endDate, (int)AppraisalStatusEnum.Active, creatorUserId));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_by_pulse",
                    new List<string> { "company_id", "appraisal_id", "referenced_template_id", "pulse_id", "title", "description", "display_mode", "is_prioritized", "type", "is_self_assessment_needed", "is_anonymous", "team_type", "question_limit", "number_of_participants", "start_date", "end_date", "status", "created_by_user_id" }));
                batch.Add(ps.Bind(companyId, appraisalId, templateAppraisalId, pulseId, title, description, displayMode, isPrioritized, templateType, isSelfAssessmentNeeded, isAnonymous, teamType, questionLimit, participantIds.Count, startDate, endDate, (int)AppraisalStatusEnum.Active, creatorUserId));

                AppraisalCard cardManager = new AppraisalCard();
                int ordering = 1;

                List<Dictionary<string, object>> customCategories = new List<Dictionary<string, object>>();
                foreach (AppraisalCard card in cards)
                {
                    string customCardId = UUIDGenerator.GenerateUniqueIDForAppraisalCard();
                    string referencedCardId = card.CardId;
                    string referencedCategoryId = card.CategoryId;

                    Dictionary<string, object> customCategory = customCategories.FirstOrDefault(c => c["ReferencedCategoryId"].ToString().Equals(referencedCategoryId));
                    if (customCategory == null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_category", new List<string>(), new List<string> { "appraisal_id", "id" }));
                        Row categoryRow = mainSession.Execute(ps.Bind(templateAppraisalId, referencedCategoryId)).FirstOrDefault();

                        if (categoryRow == null)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                            response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                            return response;
                        }

                        customCategory = new Dictionary<string, object>
                        {
                            { "CustomCategoryId", UUIDGenerator.GenerateUniqueIDForAppraisalCategory() },
                            { "ReferencedCategoryId", referencedCategoryId },
                            { "CategoryTitle", categoryRow.GetValue<string>("title")},
                            { "CreatedByAdmin", categoryRow.GetValue<bool>("is_created_by_admin")}
                        };

                        customCategories.Add(customCategory);
                    }

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_card", new List<string>(), new List<string> { "category_id", "card_id" }));
                    Row cardRow = mainSession.Execute(ps.Bind(referencedCategoryId, referencedCardId)).FirstOrDefault();
                    if (cardRow == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCard);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalidCard;
                        return response;
                    }

                    AppraisalCard referencedCard = cardManager.SelectTemplateCard(cardRow, companySettingRow, mainSession, (int)AppraisalCard.AppraisalCardQueryType.FullDetail);
                    AppraisalCardCreateStatementResponse createResponse = cardManager.CreateCustomCard(appraisalId, referencedCard.Type, referencedCard.Content, customCardId, (string)customCategory["CustomCategoryId"], templateAppraisalId, (string)customCategory["ReferencedCategoryId"], (string)customCategory["CategoryTitle"], referencedCard.CardId, (bool)customCategory["CreatedByAdmin"], referencedCard.IsCommentAllowed, ordering, referencedCard.NumberRanges, referencedCard.Options, mainSession);
                    foreach (BoundStatement bs in createResponse.UpdateStatements)
                    {
                        batch.Add(bs);
                    }

                    ordering++;
                }

                Pulse pulseManager = new Pulse();
                Notification notificationManager = new Notification();

                // Map to pulse
                foreach (BoundStatement bs in pulseManager.MapToPulse(pulseId, companyId, (int)Pulse.PulsePriorityEnum.Live360, (int)Pulse.PulseTypeEnum.Live360, isPrioritized, startDate, mainSession).Statements)
                {
                    batch.Add(bs);
                }

                if (!isUpdate)
                {
                    //Make a scheduled notification
                    notificationManager.CreateScheduledAppraisalNotification(companyId, creatorUserId, privacyParticipants, pulseId, appraisalId, startDate, endDate, mainSession);
                }
                else
                {
                    BatchStatement deleteBatch = new BatchStatement();
                    foreach (BoundStatement bs in DeleteCustomAppraisal(companyId, currentAppraisal, mainSession).DeleteStatements)
                    {
                        deleteBatch.Add(bs);
                    }
                    foreach (BoundStatement bs in pulseManager.DeleteMapFromPulse(pulseId, companyId, (int)Pulse.PulsePriorityEnum.Live360, (int)Pulse.PulseTypeEnum.Live360, currentAppraisal.IsPrioritized, currentAppraisal.StartDate, mainSession).Statements)
                    {
                        deleteBatch.Add(bs);
                    }
                    mainSession.Execute(deleteBatch);

                    //Update scheduled notification
                    notificationManager.UpdateScheduledAppraisalNotification(companyId, creatorUserId, privacyParticipants, pulseId, appraisalId, startDate, endDate, mainSession);
                }

                mainSession.Execute(batch);
                response.AppraisalId = appraisalId;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalUpdateResponse UpdateCustomAppraisal(string appraisalId,
                                                             string creatorUserId,
                                                             string companyId,
                                                             string templateAppraisalId,
                                                             string newTitle,
                                                             string newDescription,
                                                             bool newIsSelfAssessmentNeeded,
                                                             bool newIsAnonymous,
                                                             int newTeamType,
                                                             List<string> newParticipantIds,
                                                             DateTime newStartDate,
                                                             DateTime newEndDate,
                                                             List<AppraisalCard> newCards)
        {
            AppraisalUpdateResponse response = new AppraisalUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(creatorUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);
                if (companySettingRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompanyNotOnboard);
                    response.ErrorMessage = ErrorMessage.AppraisalCompanyNotOnboard;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row appraisalRow = vh.ValidateCustomAppraisal(companyId, appraisalId, mainSession);
                if (appraisalRow == null || !creatorUserId.Equals(appraisalRow.GetValue<string>("created_by_user_id")))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Appraisal currentAppraisal = SelectCustomAppraisal(companyId, appraisalRow, mainSession, (int)AppraisalQueryType.FullDetail);
                int numberOfCompletedUsers = new AnalyticAppraisal().SelectNumberOfCompletedParticipants(appraisalId, creatorUserId, analyticSession);
                bool isCompleted = numberOfCompletedUsers == currentAppraisal.Participants.Count ? true : false;
                int progress = SelectProgress(currentAppraisal.StartDate, currentAppraisal.EndDate);

                if (progress == (int)AppraisalProgressEnum.Upcoming && !isCompleted)
                {
                    DateTime createdOnTimestamp = appraisalRow.GetValue<DateTime>("created_on_timestamp");
                    AppraisalCreateResponse createResponse = CreateCustomAppraisal(creatorUserId, companyId, templateAppraisalId, newTitle, newDescription, newIsSelfAssessmentNeeded, newIsAnonymous, newTeamType, newParticipantIds, newStartDate, newEndDate, newCards, currentAppraisal);
                    response.Success = createResponse.Success;
                    response.ErrorCode = createResponse.ErrorCode;
                    response.ErrorMessage = createResponse.ErrorMessage;
                    return response;
                }
                else
                {
                    //Live and not completed
                    //Update end date, title and description, added participants and team type
                    PreparedStatement ps = null;
                    BatchStatement updateBatch = new BatchStatement();
                    DateTime currentTimestamp = new DateTime();
                    DateTime startDate = currentAppraisal.StartDate;
                    DateTime currentDate = DateTime.UtcNow;
                    string pulseId = currentAppraisal.MappedPulseId;
                    string createdByUserId = currentAppraisal.CreatedByUser.UserId;
                    List<string> currentParticipants = currentAppraisal.Participants.Select(p => p.UserId).ToList();
                    List<string> addedParticipants = newParticipantIds.Except(currentParticipants).ToList();
                    int newNumberOfPartipants = currentParticipants.Count + addedParticipants.Count;

                    AppraisalValidationResponse validateFieldResponse = ValidateUpdateFields(companyId, newTitle, newDescription, templateRow.GetValue<int>("group_team_type"), newTeamType, newParticipantIds, startDate, newEndDate, currentDate, mainSession);
                    if (!validateFieldResponse.Success)
                    {
                        response.ErrorCode = validateFieldResponse.ErrorCode;
                        response.ErrorMessage = validateFieldResponse.ErrorMessage;
                        return response;
                    }

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_appraisal_360", new List<string> { "company_id", "appraisal_id" },
                        new List<string> { "title", "description", "team_type", "number_of_participants", "end_date", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(newTitle, newDescription, newTeamType, newNumberOfPartipants, newEndDate, currentTimestamp, companyId, appraisalId));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_appraisal_360_by_start_date", new List<string> { "company_id", "created_by_user_id", "start_date", "appraisal_id" },
                        new List<string> { "title", "description", "team_type", "number_of_participants", "end_date" }, new List<string>()));
                    updateBatch.Add(ps.Bind(newTitle, newDescription, newTeamType, newNumberOfPartipants, newEndDate, companyId, createdByUserId, startDate, appraisalId));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_appraisal_360_by_pulse", new List<string> { "company_id", "pulse_id", "appraisal_id" },
                        new List<string> { "title", "description", "team_type", "number_of_participants", "end_date" }, new List<string>()));
                    updateBatch.Add(ps.Bind(newTitle, newDescription, newTeamType, newNumberOfPartipants, newEndDate, companyId, pulseId, appraisalId));

                    foreach (BoundStatement bs in CreateAppraisalPrivacy(appraisalId, addedParticipants, mainSession))
                    {
                        updateBatch.Add(bs);
                    }

                    mainSession.Execute(updateBatch);

                    //Make a scheduled notification
                    new Notification().CreateScheduledAppraisalNotification(companyId, createdByUserId, addedParticipants, pulseId, appraisalId, startDate, newEndDate, mainSession);

                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalDeleteResponse DeleteCustomAppraisal(string companyId, Appraisal customAppraisal, ISession mainSession)
        {
            AppraisalDeleteResponse response = new AppraisalDeleteResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                AppraisalCard cardManager = new AppraisalCard();

                string appraisalId = customAppraisal.AppraisalId;
                string createdByUserId = customAppraisal.CreatedByUser.UserId;
                string pulseId = customAppraisal.MappedPulseId;
                DateTime startDate = customAppraisal.StartDate;

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360",
                  new List<string> { "company_id", "appraisal_id" }));
                response.DeleteStatements.Add(ps.Bind(companyId, appraisalId));

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_by_start_date",
                    new List<string> { "company_id", "created_by_user_id", "start_date", "appraisal_id" }));
                response.DeleteStatements.Add(ps.Bind(companyId, createdByUserId, startDate, appraisalId));

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_by_pulse",
                    new List<string> { "company_id", "pulse_id" }));
                response.DeleteStatements.Add(ps.Bind(companyId, pulseId));

                foreach (BoundStatement bs in DeleteAppraisalPrivacy(appraisalId, mainSession))
                {
                    response.DeleteStatements.Add(bs);
                }

                foreach (AppraisalCard card in customAppraisal.Cards)
                {
                    foreach (BoundStatement bs in cardManager.DeleteCustomCard(appraisalId, card.Type, card.Ordering, card.CardId, mainSession).DeleteStatements)
                    {
                        response.DeleteStatements.Add(bs);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalUpdateResponse UpdateStatus(string adminUserId, string companyId, string appraisalId, int updatedStatus)
        {
            AppraisalUpdateResponse response = new AppraisalUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, appraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                AppraisalUpdateStatementResponse updateStatementResponse = UpdateStatusStatement(adminUserId, companyId, templateRow, updatedStatus, currentTime, mainSession);
                if (!updateStatementResponse.Success)
                {
                    response.ErrorCode = updateStatementResponse.ErrorCode;
                    response.ErrorMessage = updateStatementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updateStatementResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }

                foreach (BoundStatement bs in updateStatementResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalUpdateStatementResponse UpdateStatusStatement(string adminUserId, string companyId, Row templateRow, int updatedStatus, DateTime currentTime, ISession mainSession)
        {
            AppraisalUpdateStatementResponse response = new AppraisalUpdateStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                string appraisalId = templateRow.GetValue<string>("appraisal_id");
                DateTime startDate = templateRow.GetValue<DateTime>("start_date");
                if (updatedStatus == (int)AppraisalStatusEnum.Active)
                {
                    int numberOfCards = 0;
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_category", new List<string>(), new List<string> { "appraisal_id" }));
                    RowSet categoryRowSet = mainSession.Execute(ps.Bind(appraisalId));
                    foreach (Row categoryRow in categoryRowSet)
                    {
                        string categoryId = categoryRow.GetValue<string>("id");
                        ps = mainSession.Prepare(CQLGenerator.CountStatement("template_appraisal_360_card", new List<string> { "category_id" }));
                        numberOfCards = (int)mainSession.Execute(ps.Bind(categoryId)).FirstOrDefault().GetValue<long>("count");
                        if (numberOfCards > 0)
                        {
                            break;
                        }
                    }

                    if (numberOfCards == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCannotActive);
                        response.ErrorMessage = ErrorMessage.AppraisalCannotActive;
                        return response;
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360_by_start_date", new List<string> { "company_id", "appraisal_id", "title", "description", "display_mode", "is_prioritized", "type", "anonymity", "is_user_allow_create_questions", "status", "publish_method", "start_date", "end_date", "question_limit", "group_team_type" }));
                    response.UpdateStatements.Add(ps.Bind(companyId, appraisalId, templateRow.GetValue<string>("title"), templateRow.GetValue<string>("description"), templateRow.GetValue<int>("display_mode"), templateRow.GetValue<bool>("is_prioritized"), templateRow.GetValue<int>("type"), templateRow.GetValue<int>("anonymity"), templateRow.GetValue<bool>("is_user_allow_create_questions"), (int)AppraisalStatusEnum.Active, templateRow.GetValue<int>("publish_method"), templateRow.GetValue<DateTime>("start_date"), templateRow.GetValue<DateTime?>("end_date"), templateRow.GetValue<int>("question_limit"), templateRow.GetValue<int>("group_team_type")));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("template_appraisal_360_by_start_date", new List<string> { "company_id", "start_date", "appraisal_id" }));
                    response.DeleteStatements.Add(ps.Bind(companyId, startDate, appraisalId));
                }

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360", new List<string> { "company_id", "appraisal_id" }, new List<string> { "status", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(updatedStatus, adminUserId, currentTime, companyId, appraisalId));
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalSelectAllTemplatesResponse SelectTemplatesByAdmin(string adminUserId, string companyId, int type = (int)AppraisalTypeEnum.All)
        {
            AppraisalSelectAllTemplatesResponse response = new AppraisalSelectAllTemplatesResponse();
            response.Templates = new List<Appraisal>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);
                if (companySettingRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompanyNotOnboard);
                    response.ErrorMessage = ErrorMessage.AppraisalCompanyNotOnboard;
                    return response;
                }

                PreparedStatement ps = null;
                BoundStatement bs = null;

                if (type == (int)AppraisalTypeEnum.All)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360", new List<string>(), new List<string> { "company_id" }));
                    bs = ps.Bind(companyId);
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360", new List<string>(), new List<string> { "company_id", "type" }));
                    bs = ps.Bind(companyId, type);
                }

                RowSet templateRowSet = mainSession.Execute(bs);
                foreach (Row templateRow in templateRowSet)
                {
                    AppraisalCard cardManager = new AppraisalCard();
                    Appraisal template = SelectTemplate(companyId, templateRow, mainSession);
                    if (template != null)
                    {
                        template.Privacy = SelectTemplatePrivacy(template.AppraisalId, companyId, mainSession, (int)AppraisalQueryType.Basic);
                        template.NumberOfCards = cardManager.SelectAdminCreatedCardCount(companyId, template.AppraisalId, mainSession);
                        response.Templates.Add(template);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalSelectTemplateResponse SelectFullTemplateByAdmin(string adminUserId, string companyId, string appraisalId)
        {
            AppraisalSelectTemplateResponse response = new AppraisalSelectTemplateResponse();
            response.Template = new Appraisal();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, appraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                response.Template = SelectTemplate(companyId, templateRow, mainSession, (int)AppraisalQueryType.FullDetail, (int)AppraisalCard.AppraisalCardQueryType.Basic);
                response.Template.Privacy = SelectTemplatePrivacy(response.Template.AppraisalId, companyId, mainSession, (int)AppraisalQueryType.FullDetail);

                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);
                if (response.Template.AppraisalType == (int)AppraisalTypeEnum.Likert7)
                {
                    response.Template.Likert7Scale = new AppraisalCardRange().SelectLikert7(companySettingRow, mainSession).Ranges;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalSelectAllTemplatesResponse SelectTemplatesByUser(string requesterUserId, string companyId)
        {
            AppraisalSelectAllTemplatesResponse response = new AppraisalSelectAllTemplatesResponse();
            response.Templates = new List<Appraisal>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);
                if (companySettingRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompanyNotOnboard);
                    response.ErrorMessage = ErrorMessage.AppraisalCompanyNotOnboard;
                    return response;
                }

                List<string> departmentIds = new List<string>();
                User requesterUser = new User().SelectUserBasic(requesterUserId, companyId, true, mainSession, null, true).User;
                foreach (Department department in requesterUser.Departments)
                {
                    departmentIds.Add(department.Id);
                }

                DateTime currentTime = DateTime.UtcNow;
                ps = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("template_appraisal_360_by_start_date", new List<string>(), new List<string> { "company_id" }, "start_date", CQLGenerator.Comparison.LessThanOrEquals, 0));
                RowSet templateRowSet = mainSession.Execute(ps.Bind(companyId, currentTime));
                foreach (Row templateRow in templateRowSet)
                {
                    string appraisalId = templateRow.GetValue<string>("appraisal_id");
                    DateTime? endTime = templateRow.GetValue<DateTime?>("end_date");

                    if (endTime.HasValue && endTime.Value < currentTime)
                    {
                        continue;
                    }

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_privacy", new List<string>(), new List<string> { "appraisal_id" }));
                    Row privacyRow = mainSession.Execute(ps.Bind(appraisalId)).FirstOrDefault();

                    if (!CheckTemplatePrivacy(appraisalId, requesterUserId, privacyRow.GetValue<bool>("is_for_everyone"), privacyRow.GetValue<bool>("is_for_department"), privacyRow.GetValue<bool>("is_for_user"), departmentIds, mainSession))
                    {
                        continue;
                    }

                    Appraisal template = SelectTemplate(companyId, templateRow, mainSession);
                    if (template.AppraisalType == (int)AppraisalTypeEnum.Likert7)
                    {
                        template.IconUrl = companySettingRow.GetValue<string>("likert_7_icon");
                    }
                    else if (template.AppraisalType == (int)AppraisalTypeEnum.Bars)
                    {
                        template.IconUrl = companySettingRow.GetValue<string>("bars_icon");
                    }
                    response.Templates.Add(template);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalSelectTemplateResponse SelectFullTemplateByUser(string requesterUserId, string companyId, string appraisalId)
        {
            AppraisalSelectTemplateResponse response = new AppraisalSelectTemplateResponse();
            response.Template = new Appraisal();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, appraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                PreparedStatement ps = null;
                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);
                if (companySettingRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompanyNotOnboard);
                    response.ErrorMessage = ErrorMessage.AppraisalCompanyNotOnboard;
                    return response;
                }

                response.Template = SelectTemplate(companyId, templateRow, mainSession, (int)AppraisalQueryType.FullDetail, (int)AppraisalCard.AppraisalCardQueryType.FullDetail, true, requesterUserId);
                List<AppraisalCategory> categoryList = new List<AppraisalCategory>();
                categoryList.AddRange(response.Template.Categories);
                foreach (AppraisalCategory category in categoryList)
                {
                    if (category.Cards.Count == 0)
                    {
                        response.Template.Categories.Remove(category);
                    }
                }
                if (response.Template.AppraisalType == (int)AppraisalTypeEnum.Likert7)
                {
                    response.Template.Likert7Scale = new AppraisalCardRange().SelectLikert7(companySettingRow, mainSession).Ranges;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalSelectResponse SelectAppraisalResult(string createdByUserId, string companyId, string appraisalId)
        {
            AppraisalSelectResponse response = new AppraisalSelectResponse();
            response.Appraisal = new Appraisal();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(createdByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row appraisalRow = vh.ValidateCustomAppraisal(companyId, appraisalId, mainSession);
                if (appraisalRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                if (!createdByUserId.Equals(appraisalRow.GetValue<string>("created_by_user_id")))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                AppraisalValidationResponse validateResultResponse = ValidateResultSet(appraisalRow, analyticSession);
                if (!validateResultResponse.Success)
                {
                    response.ErrorCode = validateResultResponse.ErrorCode;
                    response.ErrorMessage = validateResultResponse.ErrorMessage;
                    return response;
                }

                Row companySettingRow = SelectAppraisalSetting(companyId, mainSession);

                // Get comparison difference from companySettingRow
                response.Appraisal = SelectCustomAppraisalResult(companyId, appraisalRow, companySettingRow.GetValue<double>("comparison_difference_value"), mainSession, analyticSession);

                if (response.Appraisal.AppraisalType == (int)AppraisalTypeEnum.Likert7)
                {
                    response.Appraisal.Likert7Scale = new AppraisalCardRange().SelectLikert7(companySettingRow, mainSession).Ranges;
                }

                // Convert cards per category per apparisal
                new AppraisalCategory().MapCardsToCategoryByAppraisal(response.Appraisal, true);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalExportResponse ExportAppraisalResult(string createdByUserId, string companyId, string appraisalId)
        {
            AppraisalExportResponse response = new AppraisalExportResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(createdByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row appraisalRow = vh.ValidateCustomAppraisal(companyId, appraisalId, mainSession);
                if (appraisalRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                if (!createdByUserId.Equals(appraisalRow.GetValue<string>("created_by_user_id")))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                AppraisalValidationResponse validateResultResponse = ValidateResultSet(appraisalRow, analyticSession);
                if (!validateResultResponse.Success)
                {
                    response.ErrorCode = validateResultResponse.ErrorCode;
                    response.ErrorMessage = validateResultResponse.ErrorMessage;
                    return response;
                }

                string appraisalTitle = appraisalRow.GetValue<string>("title");
                GenerateReportDelegate dTrigger = new GenerateReportDelegate(new HttpRequestHandler().MakeAppraisalReportRequest);
                dTrigger.BeginInvoke(companyId, createdByUserId, appraisalTitle, appraisalId, mainSession, new AsyncCallback(TriggerReportOnCallBack), dTrigger);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private void TriggerReportOnCallBack(IAsyncResult iar)
        {
            Log.Debug("Report on callback");

            // Cast the state object back to the delegate type
            GenerateReportDelegate reportDelegate = (GenerateReportDelegate)iar.AsyncState;

            // Call EndInvoke on the delegate to get the results
            HttpGenerateAppraisalResponse response = reportDelegate.EndInvoke(iar).Result;

            if (response.Success)
            {
                string createdByUserId = response.CreatorUserId;
                string companyId = response.CompanyId;
                ISession mainSession = response.Session;
                string appraisalTitle = response.AppraisalTitle;

                User creatorUser = new User().SelectUserBasic(createdByUserId, companyId, true, mainSession, null, true).User;
                string fullName = $"{creatorUser.FirstName} {creatorUser.LastName}";
                string departmentName = creatorUser.Departments.Count == 0 ? "No Department" : creatorUser.Departments[0].Title;

                CompanySelectEmailDetailResponse emailDetail = new Company().SelectEmailTemplate(companyId, (int)Company.CompanyEmailTemplate.Live360Report, mainSession);

                EmailSendResponse sendResponse = EmailHandler.SendLive360ReportEmail(creatorUser.Email, companyId, emailDetail.EmailTitle, emailDetail.EmailDescription, emailDetail.EmailSupportInfo, emailDetail.EmailLogoUrl, fullName, departmentName, response.AppraisalTitle, response.SignedUrl, mainSession);
                if (!sendResponse.Success)
                {
                    Log.Error(sendResponse.ErrorMessage);
                }
            }
            else
            {
                Log.Error(response.ErrorMessage);
            }
        }

        public AppraisalSelectResponse SelectFullCustomAppraisalByUser(string requesterUserId, string companyId, string appraisalId, bool isOwner = false)
        {
            AppraisalSelectResponse response = new AppraisalSelectResponse();
            response.Appraisal = new Appraisal();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row appraisalRow = vh.ValidateCustomAppraisal(companyId, appraisalId, mainSession);
                if (appraisalRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                if (!isOwner)
                {
                    if (!CheckAppraisalPrivacy(appraisalId, requesterUserId, mainSession))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                        return response;
                    }

                    response.Appraisal = SelectCustomAppraisal(companyId, appraisalRow, mainSession, (int)AppraisalQueryType.FullDetail, analyticSession, requesterUserId, true);
                }
                else
                {
                    if (!requesterUserId.Equals(appraisalRow.GetValue<string>("created_by_user_id")))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                        return response;
                    }

                    response.Appraisal = SelectCustomAppraisal(companyId, appraisalRow, mainSession, (int)AppraisalQueryType.FullDetail, analyticSession);

                    Row templateRow = vh.ValidateTemplateAppraisal(companyId, response.Appraisal.TemplateAppraisalId, mainSession, false);
                    Appraisal template = SelectTemplate(companyId, templateRow, mainSession);
                    response.Appraisal.Anonymity = template.Anonymity;

                    // Convert cards per category per apparisal
                    new AppraisalCategory().MapCardsToCategoryByAppraisal(response.Appraisal);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalSelectAllResponse SelectAppraisalsByCreator(string createdByUserId, string companyId)
        {
            AppraisalSelectAllResponse response = new AppraisalSelectAllResponse();
            response.CreatedAppraisals = new List<Appraisal>();
            response.CompletedAppraisals = new List<Appraisal>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(createdByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_by_start_date", new List<string>(), new List<string> { "company_id", "created_by_user_id" }));
                RowSet appraisalRowSet = mainSession.Execute(ps.Bind(companyId, createdByUserId));
                DateTime currentTime = DateTime.UtcNow;
                foreach (Row appraisalRow in appraisalRowSet)
                {
                    Appraisal createdAppraisal = SelectCustomAppraisal(companyId, appraisalRow, mainSession, (int)AppraisalQueryType.Basic, analyticSession);
                    if (createdAppraisal.NumberOfParticipants == createdAppraisal.NumberOfParticipantsCompleted || currentTime > createdAppraisal.EndDate)
                    {
                        response.CompletedAppraisals.Add(createdAppraisal);
                    }
                    else
                    {
                        response.CreatedAppraisals.Add(createdAppraisal);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PulseMapResponse MapToPulse(string pulseId, string requesterUserId, string companyId, DateTime? currentDate, ISession mainSession, ISession analyticSession, bool isCheckCompletion = true)
        {
            PulseMapResponse response = new PulseMapResponse();
            response.Pulse = null;
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_by_pulse", new List<string>(), new List<string> { "company_id", "pulse_id" }));
                Row appraisalRow = mainSession.Execute(ps.Bind(companyId, pulseId)).FirstOrDefault();

                if (appraisalRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                string appraisalId = appraisalRow.GetValue<string>("appraisal_id");

                if (new AnalyticAppraisal().CheckCompletion(appraisalId, requesterUserId, analyticSession) && isCheckCompletion)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompleted);
                    response.ErrorMessage = ErrorMessage.AppraisalCompleted;
                    return response;
                }

                if (CheckAppraisalPrivacy(appraisalId, requesterUserId, mainSession))
                {
                    Appraisal appraisal = SelectCustomAppraisal(companyId, appraisalRow, mainSession);

                    // Check for end date
                    if(currentDate != null && appraisal.EndDate < currentDate)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalEnded);
                        response.ErrorMessage = ErrorMessage.AppraisalEnded;
                        return response;
                    }

                    response.Pulse = new Pulse
                    {
                        PulseId = pulseId,
                        PulseType = (int)Pulse.PulseTypeEnum.Live360,
                        Appraisal = appraisal,
                        Actions = new Pulse().SelectActionForPulse((int)Pulse.PulseTypeEnum.Live360)
                    };
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public AppraisalUpdateResponse UpdateTemplate(string adminUserId,
                                                      string companyId,
                                                      string templateAppraisalId,
                                                      string newTitle,
                                                      string newDescription,
                                                      bool newIsPrioritized,
                                                      int newAnonymity,
                                                      bool newIsUserAllowToCreateQuestions,
                                                      int newGroupTeamType,
                                                      int newQuestionLimit,
                                                      List<string> newTargetedDepartmentIds,
                                                      List<string> newTargetedUserIds,
                                                      int newPublishMethod,
                                                      int newStatus,
                                                      DateTime newStartDate,
                                                      DateTime? newEndDate)
        {
            AppraisalUpdateResponse response = new AppraisalUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }
                int currentStatus = templateRow.GetValue<int>("status");
                DateTime currentStartDate = templateRow.GetValue<DateTime>("start_date");
                DateTime? currentEndDate = templateRow.GetValue<DateTime?>("end_date");
                int appraisalType = templateRow.GetValue<int>("type");

                DateTime currentDate = DateTime.UtcNow;
                AppraisalValidationResponse validateFieldResponse = ValidateTemplateFields(companyId, newTitle, newPublishMethod, newGroupTeamType, newQuestionLimit, newStartDate, newEndDate, currentDate, mainSession);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                AppraisalUpdatePrivacyStatementResponse updatePrivacyResponse = UpdateTemplatePrivacy(templateAppraisalId, newTargetedDepartmentIds, newTargetedUserIds, mainSession);
                if (!updatePrivacyResponse.Success)
                {
                    response.ErrorCode = updatePrivacyResponse.ErrorCode;
                    response.ErrorMessage = updatePrivacyResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updatePrivacyResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }

                foreach (BoundStatement bs in updatePrivacyResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                int currentProgress = SelectProgress(currentStartDate, currentEndDate);

                if (currentStatus == (int)AppraisalStatusEnum.Unlisted)
                {
                    // Edit all
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360", new List<string> { "company_id", "appraisal_id" },
                        new List<string> { "title", "description", "is_prioritized", "anonymity", "is_user_allow_create_questions", "publish_method", "start_date", "end_date", "last_modified_by_admin_id", "last_modified_timestamp", "group_team_type", "question_limit" }, new List<string>()));
                    updateBatch.Add(ps.Bind(newTitle, newDescription, newIsPrioritized, newAnonymity, newIsUserAllowToCreateQuestions, newPublishMethod, newStartDate, newEndDate, adminUserId, currentDate, newGroupTeamType, newQuestionLimit, companyId, templateAppraisalId));
                }
                else
                {
                    // Can only edit title and description
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360", new List<string> { "company_id", "appraisal_id" },
                        new List<string> { "title", "description", "publish_method", "end_date", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(newTitle, newDescription, newPublishMethod, newEndDate, adminUserId, currentDate, companyId, templateAppraisalId));

                    if (currentStatus == (int)AppraisalStatusEnum.Active)
                    {
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360_by_start_date", new List<string> { "company_id", "start_date", "appraisal_id" },
                            new List<string> { "title", "description" }, new List<string>()));
                        updateBatch.Add(ps.Bind(newTitle, newDescription, companyId, currentStartDate, templateAppraisalId));
                    }
                }

                mainSession.Execute(updateBatch);

                if (currentStatus != newStatus)
                {
                    updateBatch = new BatchStatement();
                    deleteBatch = new BatchStatement();

                    AppraisalUpdateStatementResponse updateStatementResponse = UpdateStatusStatement(adminUserId, companyId, templateRow, newStatus, currentDate, mainSession);
                    if (!updateStatementResponse.Success)
                    {
                        response.ErrorCode = updateStatementResponse.ErrorCode;
                        response.ErrorMessage = updateStatementResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in updateStatementResponse.DeleteStatements)
                    {
                        deleteBatch.Add(bs);
                    }

                    foreach (BoundStatement bs in updateStatementResponse.UpdateStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public Row SelectAppraisalSetting(string companyId, ISession mainSession)
        {
            Row companySettingRow = null;

            try
            {
                Module moduleManager = new Module();
                if (moduleManager.CheckModuleUnlocked(companyId, Module.MODULE_LIVE360, mainSession))
                {
                    DateTime currentTime = DateTime.UtcNow;
                    BatchStatement updateBatch = new BatchStatement();
                    PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("appraisal_360_group_team_type_by_company", new List<string>(), new List<string> { "company_id" }, 1));
                    Row groupRow = mainSession.Execute(ps.Bind(companyId)).FirstOrDefault();
                    if (groupRow == null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_group_team_type_by_company", new List<string> { "company_id", "group_id", "team_type_id",
                            "label", "created_on_timestamp", "last_modified_timestamp" }));
                        updateBatch.Add(ps.Bind(companyId, 1, 1, DefaultResource.AppraisalTeamTypeNA, currentTime, currentTime));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_group_team_type_by_company", new List<string> { "company_id", "group_id", "team_type_id",
                            "label", "created_on_timestamp", "last_modified_timestamp" }));
                        updateBatch.Add(ps.Bind(companyId, 1, 2, DefaultResource.AppraisalTeamTypeTransient, currentTime, currentTime));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_group_team_type_by_company", new List<string> { "company_id", "group_id", "team_type_id",
                            "label", "created_on_timestamp", "last_modified_timestamp" }));
                        updateBatch.Add(ps.Bind(companyId, 1, 3, DefaultResource.AppraisalTeamTypeIntact, currentTime, currentTime));

                        mainSession.Execute(updateBatch);
                    }

                    updateBatch = new BatchStatement();
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_setting_by_company", new List<string>(), new List<string> { "company_id" }));
                    companySettingRow = mainSession.Execute(ps.Bind(companyId)).FirstOrDefault();
                    if (companySettingRow == null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_setting_by_company", new List<string> { "company_id", "bars_option_number", "comparison_difference_value",
                            "likert_7_range_1_label", "likert_7_range_2_label", "likert_7_range_3_label", "likert_7_range_4_label", "likert_7_range_5_label", "likert_7_range_6_label", "likert_7_range_7_label",
                            "likert_7_range_1_bgcolor", "likert_7_range_2_bgcolor", "likert_7_range_3_bgcolor", "likert_7_range_4_bgcolor", "likert_7_range_5_bgcolor", "likert_7_range_6_bgcolor", "likert_7_range_7_bgcolor",
                            "bar_option_1_bgcolor", "bar_option_2_bgcolor", "bar_option_3_bgcolor", "bar_option_4_bgcolor", "bar_option_5_bgcolor", "bar_option_6_bgcolor", "bar_option_7_bgcolor",
                            "likert_7_icon", "bars_icon", "created_on_timestamp", "last_modified_timestamp"}));
                        updateBatch.Add(ps.Bind(companyId, Convert.ToInt32(DefaultResource.AppraisalDefaultBarsOptionNumber), Convert.ToDouble(DefaultResource.AppraisalDefaultComparisonDifferenceValue),
                            DefaultResource.AppraisalDefaultLikert7Range1Label, DefaultResource.AppraisalDefaultLikert7Range2Label, DefaultResource.AppraisalDefaultLikert7Range3Label, DefaultResource.AppraisalDefaultLikert7Range4Label, DefaultResource.AppraisalDefaultLikert7Range5Label, DefaultResource.AppraisalDefaultLikert7Range6Label, DefaultResource.AppraisalDefaultLikert7Range7Label,
                            DefaultResource.AppraisalDefaultLikert7Range1BgColor, DefaultResource.AppraisalDefaultLikert7Range2BgColor, DefaultResource.AppraisalDefaultLikert7Range3BgColor, DefaultResource.AppraisalDefaultLikert7Range4BgColor, DefaultResource.AppraisalDefaultLikert7Range5BgColor, DefaultResource.AppraisalDefaultLikert7Range6BgColor, DefaultResource.AppraisalDefaultLikert7Range7BgColor,
                            DefaultResource.AppraisalDefaultBarsOption1BgColor, DefaultResource.AppraisalDefaultBarsOption2BgColor, DefaultResource.AppraisalDefaultBarsOption3BgColor, DefaultResource.AppraisalDefaultBarsOption4BgColor, DefaultResource.AppraisalDefaultBarsOption5BgColor, DefaultResource.AppraisalDefaultBarsOption6BgColor, DefaultResource.AppraisalDefaultBarsOption7BgColor,
                            DefaultResource.ApprasialDefaultLikert7IconUrl, DefaultResource.ApprasialDefaultBarsIconUrl, currentTime, currentTime));

                        mainSession.Execute(updateBatch);

                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_setting_by_company", new List<string>(), new List<string> { "company_id" }));
                        companySettingRow = mainSession.Execute(ps.Bind(companyId)).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return companySettingRow;
        }

        public List<AppraisalTeamType> SelectTemplateTeamTypeByGroup(string companyId, string templateAppraisalId, ISession mainSession, int groupId = 0)
        {
            List<AppraisalTeamType> teamTypes = new List<AppraisalTeamType>();
            try
            {
                PreparedStatement ps = null;
                if (groupId != 0)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_group_team_type_by_company", new List<string>(), new List<string> { "company_id", "group_id" }));
                    RowSet teamTypeRowSet = mainSession.Execute(ps.Bind(companyId, groupId));
                    foreach (Row teamTypeRow in teamTypeRowSet)
                    {
                        teamTypes.Add(new AppraisalTeamType
                        {
                            Id = teamTypeRow.GetValue<int>("team_type_id"),
                            Label = teamTypeRow.GetValue<string>("label")
                        });
                    }
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360", new List<string>(), new List<string> { "company_id", "appraisal_id" }));
                    Row templateAppraisalRow = mainSession.Execute(ps.Bind(companyId, templateAppraisalId)).FirstOrDefault();
                    groupId = templateAppraisalRow.GetValue<int>("group_team_type");

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_group_team_type_by_company", new List<string>(), new List<string> { "company_id", "group_id" }));
                    RowSet teamTypeRowSet = mainSession.Execute(ps.Bind(companyId, groupId));
                    foreach (Row teamTypeRow in teamTypeRowSet)
                    {
                        teamTypes.Add(new AppraisalTeamType
                        {
                            Id = teamTypeRow.GetValue<int>("team_type_id"),
                            Label = teamTypeRow.GetValue<string>("label")
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return teamTypes;
        }

        public AppraisalSelectOverallReportsResponse SelectReportsByAdmin(string adminUserId, string companyId)
        {
            AppraisalSelectOverallReportsResponse response = new AppraisalSelectOverallReportsResponse();
            response.TemplateAppraisals = new List<Appraisal>();
            response.Appraisals = new List<Appraisal>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                AnalyticAppraisal analyticManager = new AnalyticAppraisal();
                AppraisalCategory categoryManager = new AppraisalCategory();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360", new List<string>(), new List<string> { "company_id" }));
                RowSet userAppraisalRowSet = mainSession.Execute(ps.Bind(companyId));
                double timeOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                foreach (Row userAppraisalRow in userAppraisalRowSet)
                {
                    string createdByUserId = userAppraisalRow.GetValue<string>("created_by_user_id");
                    AppraisalValidationResponse validateResult = ValidateResultSet(userAppraisalRow, analyticSession);
                    if (!validateResult.Success)
                    {
                        continue;
                    }

                    Appraisal result = SelectCustomAppraisalResult(companyId, userAppraisalRow, 0, mainSession, analyticSession, true);
                    result.StartDateString = result.StartDate.AddHours(timeOffset).ToString("ddMMyyyy");
                    result.StartTimeString = result.StartDate.AddHours(timeOffset).ToString("HHmm");
                    result.EndDateString = result.EndDate.Value.AddHours(timeOffset).ToString("ddMMyyyy");
                    result.EndTimeString = result.EndDate.Value.AddHours(timeOffset).ToString("HHmm");
                    result.Duration = GenerateTimeTakenString(result.StartDate, result.EndDate.Value);

                    if (response.TemplateAppraisals.FirstOrDefault(t => t.AppraisalId.Equals(result.TemplateAppraisalId)) == null)
                    {
                        Row templateRow = vh.ValidateTemplateAppraisal(companyId, result.TemplateAppraisalId, mainSession);
                        response.TemplateAppraisals.Add(SelectTemplate(companyId, templateRow, mainSession, (int)AppraisalQueryType.FullDetail, (int)AppraisalCard.AppraisalCardQueryType.Basic));
                    }

                    double totalTimeTaken = 0;
                    foreach (ParticipantBreakdown breakdown in result.ParticipantsBreakdown)
                    {
                        totalTimeTaken += breakdown.TimeTaken;
                    }
                    double averageTimeTaken = totalTimeTaken == 0 ? 0 : Math.Round(totalTimeTaken / (result.NumberOfParticipantsCompleted + (result.IsSelfAssessmentNeeded == true ? 1 : 0)));
                    result.AverageTimeTaken = GenerateTimeTakenString(averageTimeTaken);

                    categoryManager.MapCardsToCategoryByAppraisal(result, true);

                    response.Appraisals.Add(result);
                }

                response.Appraisals = response.Appraisals.OrderBy(a => a.StartDate).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private string GenerateTimeTakenString(DateTime startTimestamp, DateTime endTimestamp)
        {
            string timeTaken = string.Empty;
            try
            {
                double differenceInSeconds = (endTimestamp - startTimestamp).TotalSeconds;

                //TimeSpan timeSpan = (endTimestamp - startTimestamp);
                //if (differenceInSeconds < 0)
                //{
                //    timeSpan = (endTimestamp - startTimestamp).Negate();
                //}

                //int days = timeSpan.Days;
                //int hours = timeSpan.Hours;
                //int minutes = timeSpan.Minutes;
                //int seconds = timeSpan.Seconds;

                //if (days > 0)
                //{
                //    timeTaken += $"{days} days ";
                //}
                //if (hours > 0)
                //{
                //    timeTaken += $"{hours} hours ";
                //}
                //if (minutes > 0)
                //{
                //    timeTaken += $"{minutes} mins ";
                //}
                //if (seconds > 0)
                //{
                //    timeTaken += $"{seconds} secs ";
                //}

                //timeTaken = timeTaken.Trim();

                timeTaken = $"{Math.Abs(differenceInSeconds)}";
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return timeTaken;
        }

        private string GenerateTimeTakenString(double time)
        {
            string timeTaken = string.Empty;
            try
            {
                //TimeSpan timeSpan = TimeSpan.FromSeconds(time);

                //int days = timeSpan.Days;
                //int hours = timeSpan.Hours;
                //int minutes = timeSpan.Minutes;
                //int seconds = timeSpan.Seconds;

                //if (days > 0)
                //{
                //    timeTaken += $"{days} days ";
                //}
                //if (hours > 0)
                //{
                //    timeTaken += $"{hours} hours ";
                //}
                //if (minutes > 0)
                //{
                //    timeTaken += $"{minutes} mins ";
                //}
                //if (seconds >= 0)
                //{
                //    timeTaken += $"{seconds} secs ";
                //}

                //timeTaken = timeTaken.Trim();

                timeTaken = $"{time}";
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return timeTaken;
        }

        private Appraisal SelectTemplate(string companyId, Row templateRow, ISession mainSession = null, int appraisalQueryType = (int)AppraisalQueryType.Basic, int cardQueryType = (int)AppraisalCard.AppraisalCardQueryType.Basic, bool checkForValid = true, string customCreationUserId = null)
        {
            Appraisal appraisal = null;
            try
            {
                int status = templateRow.GetValue<int>("status");
                if (status == (int)AppraisalStatusEnum.Deleted)
                {
                    return appraisal;
                }

                appraisal = new Appraisal
                {
                    AppraisalId = templateRow.GetValue<string>("appraisal_id"),
                    Title = templateRow.GetValue<string>("title"),
                    Description = templateRow.GetValue<string>("description"),
                    DisplayMode = templateRow.GetValue<int>("display_mode"),
                    AppraisalType = templateRow.GetValue<int>("type"),
                    Anonymity = templateRow.GetValue<int>("anonymity"),
                    IsPrioritized = templateRow.GetValue<bool>("is_prioritized"),
                    IsUserAllowToCreateQuestions = templateRow.GetValue<bool>("is_user_allow_create_questions"),
                    PublishMethod = templateRow.GetValue<int>("publish_method"),
                    QuestionLimit = templateRow.GetValue<int>("question_limit"),
                    GroupTeamType = templateRow.GetValue<int>("group_team_type"),
                    StartDate = templateRow.GetValue<DateTime>("start_date"),
                    EndDate = templateRow.GetValue<DateTime?>("end_date"),
                    Status = status
                };

                appraisal.Progress = SelectProgress(appraisal.StartDate, appraisal.EndDate);
                appraisal.TeamTypes = SelectTemplateTeamTypeByGroup(companyId, appraisal.AppraisalId, mainSession, appraisal.GroupTeamType);

                if (appraisalQueryType == (int)AppraisalQueryType.FullDetail)
                {
                    appraisal.Categories = new AppraisalCategory().SelectAdminDefinedCategoriesByAppraisal(companyId, appraisal.AppraisalId, mainSession, cardQueryType).Categories;
                    if (!string.IsNullOrEmpty(customCreationUserId))
                    {
                        appraisal.UserDefinedCategories = new AppraisalCategory().SelectUserDefinedCategoriesByAppraisal(companyId, customCreationUserId, appraisal.AppraisalId, mainSession, true, (int)AppraisalCard.AppraisalCardQueryType.FullDetail).Categories;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return appraisal;
        }

        private int SelectProgress(DateTime startTime, DateTime? endTime)
        {
            int progress = (int)AppraisalProgressEnum.Upcoming;
            try
            {
                DateTime currentTime = DateTime.UtcNow;

                if (currentTime > startTime)
                {
                    progress = (int)AppraisalProgressEnum.Active;
                }

                if (endTime != null && endTime.HasValue)
                {
                    if (currentTime > endTime)
                    {
                        progress = (int)AppraisalProgressEnum.Completed;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return progress;
        }

        public AppraisalValidationResponse ValidateResultSet(Row appraisalRow, ISession analyticSession)
        {
            AppraisalValidationResponse response = new AppraisalValidationResponse();
            response.Success = false;
            try
            {
                string createdByUserId = appraisalRow.GetValue<string>("created_by_user_id");
                string appraisalId = appraisalRow.GetValue<string>("appraisal_id");
                bool isSelfAssessmentNeeded = appraisalRow.GetValue<bool>("is_self_assessment_needed");
                int numberOfParticipants = appraisalRow.GetValue<int>("number_of_participants");
                DateTime endDate = appraisalRow.GetValue<DateTime>("end_date");

                AnalyticAppraisal analyticManager = new AnalyticAppraisal();
                if (isSelfAssessmentNeeded)
                {
                    if (!analyticManager.CheckCompletion(appraisalId, createdByUserId, analyticSession))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalNotCompletedByCreator);
                        response.ErrorMessage = ErrorMessage.AppraisalNotCompletedByCreator;
                        return response;
                    }
                }

                int numberOfCompletedUsers = analyticManager.SelectNumberOfCompletedParticipants(appraisalId, createdByUserId, analyticSession);

                if (numberOfParticipants != numberOfCompletedUsers && DateTime.UtcNow < endDate)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalNotCompletedByCreator);
                    response.ErrorMessage = ErrorMessage.AppraisalNotCompletedByCreator;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private Appraisal SelectCustomAppraisal(string companyId, Row appraisalRow, ISession mainSession, int appraisalQueryType = (int)AppraisalQueryType.Basic, ISession analyticSession = null, string requesterUserId = null, bool isSelectUserAnswer = false)
        {
            Appraisal appraisal = null;
            try
            {
                appraisal = new Appraisal
                {
                    AppraisalId = appraisalRow.GetValue<string>("appraisal_id"),
                    TemplateAppraisalId = appraisalRow.GetValue<string>("referenced_template_id"),
                    MappedPulseId = appraisalRow.GetValue<string>("pulse_id"),
                    Title = appraisalRow.GetValue<string>("title"),
                    Description = appraisalRow.GetValue<string>("description"),
                    DisplayMode = appraisalRow.GetValue<int>("display_mode"),
                    IsPrioritized = appraisalRow.GetValue<bool>("is_prioritized"),
                    IsSelfAssessmentNeeded = appraisalRow.GetValue<bool>("is_self_assessment_needed"),
                    IsAnonymous = appraisalRow.GetValue<bool>("is_anonymous"),
                    AppraisalType = appraisalRow.GetValue<int>("type"),
                    StartDate = appraisalRow.GetValue<DateTime>("start_date"),
                    EndDate = appraisalRow.GetValue<DateTime>("end_date"),
                    QuestionLimit = appraisalRow.GetValue<int>("question_limit"),
                    NumberOfParticipants = appraisalRow.GetValue<int>("number_of_participants"),
                    CreatedByUser = new User().SelectUserBasic(appraisalRow.GetValue<string>("created_by_user_id"), companyId, true, mainSession, null, true).User,
                    CreatedOnTimestamp = appraisalRow.GetColumn("created_on_timestamp") != null ? appraisalRow.GetValue<DateTime?>("created_on_timestamp") : null
                };

                if (analyticSession != null)
                {
                    appraisal.NumberOfParticipantsCompleted = new AnalyticAppraisal().SelectNumberOfCompletedParticipants(appraisal.AppraisalId, appraisal.CreatedByUser.UserId, analyticSession);
                }

                if (appraisalQueryType == (int)AppraisalQueryType.FullDetail)
                {
                    appraisal.Participants = SelectParticipants(appraisal.AppraisalId, companyId, appraisal.CreatedByUser.UserId, mainSession);
                    appraisal.Cards = new AppraisalCard().SelectCardsFromCustomAppraisal(appraisal.AppraisalId, mainSession, analyticSession, requesterUserId, isSelectUserAnswer).Cards;

                    string templateAppraisalId = appraisalRow.GetValue<string>("referenced_template_id");
                    List<AppraisalTeamType> teamTypes = SelectTemplateTeamTypeByGroup(companyId, templateAppraisalId, mainSession);
                    teamTypes.FirstOrDefault(t => t.Id == appraisalRow.GetValue<int>("team_type")).IsSelected = true;
                    appraisal.TeamTypes = teamTypes;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return appraisal;
        }

        private Appraisal SelectCustomAppraisalResult(string companyId, Row appraisalRow, double comparisonDifference, ISession mainSession, ISession analyticSession, bool isBreakdownNeeded = false)
        {
            Appraisal appraisal = null;
            try
            {
                appraisal = new Appraisal
                {
                    AppraisalId = appraisalRow.GetValue<string>("appraisal_id"),
                    MappedPulseId = appraisalRow.GetValue<string>("pulse_id"),
                    TemplateAppraisalId = appraisalRow.GetValue<string>("referenced_template_id"),
                    Title = appraisalRow.GetValue<string>("title"),
                    Description = appraisalRow.GetValue<string>("description"),
                    DisplayMode = appraisalRow.GetValue<int>("display_mode"),
                    IsPrioritized = appraisalRow.GetValue<bool>("is_prioritized"),
                    IsSelfAssessmentNeeded = appraisalRow.GetValue<bool>("is_self_assessment_needed"),
                    IsAnonymous = appraisalRow.GetValue<bool>("is_anonymous"),
                    AppraisalType = appraisalRow.GetValue<int>("type"),
                    StartDate = appraisalRow.GetValue<DateTime>("start_date"),
                    EndDate = appraisalRow.GetValue<DateTime>("end_date"),
                    NumberOfParticipants = appraisalRow.GetValue<int>("number_of_participants"),
                    CreatedByUser = new User().SelectUserBasic(appraisalRow.GetValue<string>("created_by_user_id"), companyId, true, mainSession, null, true).User
                };

                List<AppraisalTeamType> teamTypes = SelectTemplateTeamTypeByGroup(companyId, appraisal.TemplateAppraisalId, mainSession);
                appraisal.SelectedTeamType = teamTypes.FirstOrDefault(t => t.Id == appraisalRow.GetValue<int>("team_type"));

                AnalyticAppraisal analyticManager = new AnalyticAppraisal();
                List<string> completedParticipantIds = analyticManager.SelectCompletedParticipants(appraisal.AppraisalId, appraisal.CreatedByUser.UserId, analyticSession);
                appraisal.NumberOfParticipantsCompleted = completedParticipantIds.Count;
                appraisal.Participants = SelectParticipants(appraisal.AppraisalId, companyId, appraisal.CreatedByUser.UserId, mainSession);

                appraisal.Cards = new AppraisalCard().SelectCardsResultFromCustomAppraisal(appraisal.AppraisalId, appraisal.CreatedByUser.UserId, appraisal.IsSelfAssessmentNeeded, comparisonDifference, completedParticipantIds, mainSession, analyticSession, isBreakdownNeeded).Cards;

                if (isBreakdownNeeded)
                {
                    appraisal.ParticipantsBreakdown = new List<ParticipantBreakdown>();
                    List<User> targetedParticipants = new List<User>();
                    targetedParticipants.AddRange(appraisal.Participants);

                    if (appraisal.IsSelfAssessmentNeeded)
                    {
                        targetedParticipants.Add(appraisal.CreatedByUser);
                    }

                    foreach (User participant in targetedParticipants)
                    {
                        List<AppraisalCard.ParticipantAnswerBreakdown> answerBreakdown = new List<AppraisalCard.ParticipantAnswerBreakdown>();
                        foreach (AppraisalCard card in appraisal.Cards)
                        {
                            AppraisalCard.ParticipantAnswerBreakdown breakdown = card.ResultBreakdown.FirstOrDefault(r => r.ParticipantId.Equals(participant.UserId));
                            if (breakdown != null)
                            {
                                AppraisalCard.AppraisalComment comment = card.Comments.FirstOrDefault(c => c.CommentedByUserId.Equals(participant.UserId));
                                if (comment != null)
                                {
                                    breakdown.Comment = comment.Content;
                                }
                                answerBreakdown.Add(breakdown);
                            }
                        }

                        Dictionary<string, object> timeDict = analyticManager.SelectTimeTakenByParticipant(participant.UserId, appraisal.AppraisalId, analyticSession);
                        appraisal.ParticipantsBreakdown.Add(new ParticipantBreakdown
                        {
                            Participant = participant,
                            TimeTakenString = (string)timeDict["TimeTakenString"],
                            TimeTaken = (double)timeDict["TimeTakenSec"],
                            AnswersBreakdown = answerBreakdown
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return appraisal;
        }

        private AppraisalValidationResponse ValidateTemplateFields(string companyId, string title, int publishMethod, int groupTeamType, int questionLimit, DateTime startDate, DateTime? endDate, DateTime currentDate, ISession mainSession)
        {
            AppraisalValidationResponse response = new AppraisalValidationResponse();
            response.Success = false;
            try
            {
                if (publishMethod == (int)AppraisalPublishMethodEnum.Schedule)
                {
                    if (endDate == null || !endDate.HasValue)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidDate);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalidDate;
                        return response;
                    }

                    if (endDate < currentDate)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.AppraisalEndDateEarlierThanToday;
                        return response;
                    }

                    if (endDate < startDate)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.AppraisalEndDateEarlierThanStartDate;
                        return response;
                    }

                }

                if (string.IsNullOrEmpty(title))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.AppraisalTitleIsEmpty;
                    return response;
                }

                if (questionLimit <= 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalQuestionLimitInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalQuestionLimitInvalid;
                    return response;
                }

                //PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_group_team_type_by_company", new List<string>(), new List<string> { "company_id", "group_id" }));
                //Row groupTeamRow = mainSession.Execute(ps.Bind(companyId, groupTeamType)).FirstOrDefault();
                //if (groupTeamRow == null)
                //{
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalGroupTeamTypeInvalid);
                //    response.ErrorMessage = ErrorMessage.AppraisalGroupTeamTypeInvalid;
                //    return response;
                //}

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalValidationResponse ValidateFields(string companyId, string title, string description, int templateGroupTeamType, int teamType, List<string> participantIds, bool isAnonymous, int questionLimit, int numberOfCards, DateTime startDate, DateTime endDate, DateTime currentDate, ISession mainSession)
        {
            AppraisalValidationResponse response = new AppraisalValidationResponse();
            response.Success = false;
            try
            {
                //if (!startDate.Kind.Equals(DateTimeKind.Utc) || !endDate.Kind.Equals(DateTimeKind.Utc))
                //{
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidDateFormat);
                //    response.ErrorMessage = ErrorMessage.AppraisalInvalidDateFormat;
                //    return response;
                //}

                // Check Date
                if (endDate < currentDate)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalEndDateEarlierThanToday);
                    response.ErrorMessage = ErrorMessage.AppraisalEndDateEarlierThanToday;
                    return response;
                }

                if (endDate < startDate)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalEndDateEarlierThanStartDate);
                    response.ErrorMessage = ErrorMessage.AppraisalEndDateEarlierThanStartDate;
                    return response;
                }

                // Check title
                if (string.IsNullOrEmpty(title))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.AppraisalTitleIsEmpty;
                    return response;
                }

                // Check description
                if (string.IsNullOrEmpty(description))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalDescriptionIsEmpty);
                    response.ErrorMessage = ErrorMessage.AppraisalDescriptionIsEmpty;
                    return response;
                }

                // Check participants
                User userManager = new User();
                foreach (string participantId in participantIds)
                {
                    User participant = userManager.SelectUserBasic(participantId, companyId, true, mainSession).User;
                    if (participant == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalTargetedUserIsInvalid);
                        response.ErrorMessage = ErrorMessage.AppraisalTargetedUserIsInvalid;
                        return response;
                    }
                }

                if (isAnonymous && participantIds.Count < 3)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalAnonymousCannotBeUnlocked);
                    response.ErrorMessage = ErrorMessage.AppraisalAnonymousCannotBeUnlocked;
                    return response;
                }

                // Check team type
                List<AppraisalTeamType> teamTypes = SelectTemplateTeamTypeByGroup(companyId, null, mainSession, templateGroupTeamType);
                if (teamTypes.FirstOrDefault(t => t.Id == teamType) == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalTeamTypeInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalTeamTypeInvalid;
                    return response;
                }

                // Check number of cards
                if (numberOfCards == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalNumberOfCardsZero);
                    response.ErrorMessage = ErrorMessage.AppraisalNumberOfCardsZero;
                    return response;
                }
                else
                {
                    if (numberOfCards > questionLimit)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalNumberOfCardsExceeded);
                        response.ErrorMessage = ErrorMessage.AppraisalNumberOfCardsExceeded;
                        return response;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalValidationResponse ValidateUpdateFields(string companyId, string title, string description, int templateGroupTeamType, int teamType, List<string> participantIds, DateTime startDate, DateTime endDate, DateTime currentDate, ISession mainSession)
        {
            AppraisalValidationResponse response = new AppraisalValidationResponse();
            response.Success = false;
            try
            {
                //if (!startDate.Kind.Equals(DateTimeKind.Utc) || !endDate.Kind.Equals(DateTimeKind.Utc))
                //{
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidDateFormat);
                //    response.ErrorMessage = ErrorMessage.AppraisalInvalidDateFormat;
                //    return response;
                //}

                // Check date
                if (endDate < currentDate)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalEndDateEarlierThanToday);
                    response.ErrorMessage = ErrorMessage.AppraisalEndDateEarlierThanToday;
                    return response;
                }

                if (endDate < startDate)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalEndDateEarlierThanStartDate);
                    response.ErrorMessage = ErrorMessage.AppraisalEndDateEarlierThanStartDate;
                    return response;
                }

                // Check title
                if (string.IsNullOrEmpty(title))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.AppraisalTitleIsEmpty;
                    return response;
                }

                // Check description
                if (string.IsNullOrEmpty(description))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalDescriptionIsEmpty);
                    response.ErrorMessage = ErrorMessage.AppraisalDescriptionIsEmpty;
                    return response;
                }

                // Check participants
                User userManager = new User();
                foreach (string participantId in participantIds)
                {
                    User participant = userManager.SelectUserBasic(participantId, companyId, true, mainSession).User;
                    if (participant == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalTargetedUserIsInvalid);
                        response.ErrorMessage = ErrorMessage.AppraisalTargetedUserIsInvalid;
                        return response;
                    }
                }

                // Check team type
                List<AppraisalTeamType> teamTypes = SelectTemplateTeamTypeByGroup(companyId, null, mainSession, templateGroupTeamType);
                if (teamTypes.FirstOrDefault(t => t.Id == teamType) == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalTeamTypeInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalTeamTypeInvalid;
                    return response;
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private List<User> SelectParticipants(string appraisalId, string companyId, string createdByUserId, ISession mainSession)
        {
            List<User> participants = new List<User>();
            try
            {
                User userManager = new User();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_targeted_user", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet participantRowSet = mainSession.Execute(ps.Bind(appraisalId));
                foreach (Row participantRow in participantRowSet)
                {
                    string participantUserId = participantRow.GetValue<string>("user_id");
                    if (!participantUserId.Equals(createdByUserId))
                    {
                        participants.Add(userManager.SelectUserBasic(participantUserId, companyId, false, mainSession, null, true).User);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return participants;
        }

        private List<BoundStatement> CreateAppraisalPrivacy(string appraisalId, List<string> targetedUserIds, ISession mainSession)
        {
            List<BoundStatement> bsList = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;
                foreach (string userId in targetedUserIds)
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_targeted_user", new List<string> { "user_id", "appraisal_id" }));
                    bsList.Add(ps.Bind(userId, appraisalId));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_user_targeted_appraisal", new List<string> { "user_id", "appraisal_id" }));
                    bsList.Add(ps.Bind(userId, appraisalId));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return bsList;
        }

        private List<BoundStatement> DeleteAppraisalPrivacy(string appraisalId, ISession mainSession)
        {
            List<BoundStatement> bsList = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_targeted_user", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet targetedUserRowSet = mainSession.Execute(ps.Bind(appraisalId));
                foreach (Row targetedUserRow in targetedUserRowSet)
                {
                    string userId = targetedUserRow.GetValue<string>("user_id");
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_user_targeted_appraisal", new List<string> { "user_id", "appraisal_id" }));
                    bsList.Add(ps.Bind(userId, appraisalId));
                }
                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_targeted_user", new List<string> { "appraisal_id" }));
                bsList.Add(ps.Bind(appraisalId));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return bsList;
        }

        public bool CheckAppraisalPrivacy(string appraisalId, string userId, ISession session)
        {
            bool isAppraisalForCurrentUser = false;
            PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_targeted_user",
                new List<string>(), new List<string> { "appraisal_id", "user_id" }));
            Row privacyRow = session.Execute(ps.Bind(appraisalId, userId)).FirstOrDefault();

            if (privacyRow != null)
            {
                isAppraisalForCurrentUser = true;
            }

            return isAppraisalForCurrentUser;
        }

        private List<BoundStatement> CreateTemplatePrivacy(string appraisalId, List<string> targetedDepartmentIds, List<string> targetedUserIds, ISession mainSession)
        {
            List<BoundStatement> bsList = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;
                bool isForDepartment = targetedDepartmentIds == null || targetedDepartmentIds.Count == 0 ? false : true;
                bool isForUser = targetedUserIds == null || targetedUserIds.Count == 0 ? false : true;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360_privacy", new List<string> { "appraisal_id", "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group" }));
                bsList.Add(ps.Bind(appraisalId, isForEveryone, isForDepartment, isForUser, false));

                if (isForDepartment)
                {
                    foreach (string departmentId in targetedDepartmentIds)
                    {
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_template_targeted_department", new List<string> { "department_id", "appraisal_id" }));
                        bsList.Add(ps.Bind(departmentId, appraisalId));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_department_targeted_template", new List<string> { "department_id", "appraisal_id" }));
                        bsList.Add(ps.Bind(departmentId, appraisalId));
                    }
                }

                if (isForUser)
                {
                    foreach (string userId in targetedUserIds)
                    {
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_template_targeted_user", new List<string> { "user_id", "appraisal_id" }));
                        bsList.Add(ps.Bind(userId, appraisalId));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("appraisal_360_user_targeted_template", new List<string> { "user_id", "appraisal_id" }));
                        bsList.Add(ps.Bind(userId, appraisalId));
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return bsList;
        }

        private AppraisalUpdatePrivacyStatementResponse UpdateTemplatePrivacy(string appraisalId, List<string> updatedTargetedDepartmentIds, List<string> updatedTargetedUserIds, ISession mainSession)
        {
            AppraisalUpdatePrivacyStatementResponse response = new AppraisalUpdatePrivacyStatementResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.DeleteStatement("appraisal_360_template_targeted_user", new List<string> { "appraisal_id" }));
                response.DeleteStatements.Add(ps.Bind(appraisalId));

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_template_targeted_user", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet userRowSet = mainSession.Execute(ps.Bind(appraisalId));
                foreach (Row userRow in userRowSet)
                {
                    string userId = userRow.GetValue<string>("user_id");
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("appraisal_360_user_targeted_template", new List<string> { "appraisal_id", "user_id" }));
                    response.DeleteStatements.Add(ps.Bind(appraisalId, userId));
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("appraisal_360_template_targeted_department", new List<string> { "appraisal_id" }));
                response.DeleteStatements.Add(ps.Bind(appraisalId));

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_template_targeted_department", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet departmentRowSet = mainSession.Execute(ps.Bind(appraisalId));
                foreach (Row departmentRow in departmentRowSet)
                {
                    string departmentId = departmentRow.GetValue<string>("department_id");
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("appraisal_360_department_targeted_template", new List<string> { "appraisal_id", "department_id" }));
                    response.DeleteStatements.Add(ps.Bind(appraisalId, departmentId));
                }

                response.UpdateStatements.AddRange(CreateTemplatePrivacy(appraisalId, updatedTargetedDepartmentIds, updatedTargetedUserIds, mainSession));
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalPrivacy SelectTemplatePrivacy(string appraisalId, string companyId, ISession mainSession, int queryType = (int)AppraisalQueryType.Basic)
        {
            AppraisalPrivacy privacy = new AppraisalPrivacy();
            privacy.TargetedDepartments = new List<Department>();
            privacy.TargetedUsers = new List<User>();
            try
            {
                PreparedStatement ps = null;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_privacy", new List<string>(), new List<string> { "appraisal_id" }));
                Row privacyRow = mainSession.Execute(ps.Bind(appraisalId)).FirstOrDefault();
                if (privacyRow != null)
                {
                    privacy.IsForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                    privacy.IsForDepartment = privacyRow.GetValue<bool>("is_for_department");
                    privacy.IsForUser = privacyRow.GetValue<bool>("is_for_user");
                    privacy.IsForCustomGroup = false;

                    if (queryType == (int)AppraisalQueryType.FullDetail)
                    {
                        if (privacy.IsForDepartment)
                        {
                            Department departmentManager = new Department();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_template_targeted_department", new List<string>(), new List<string> { "appraisal_id" }));
                            RowSet departmentRowSet = mainSession.Execute(ps.Bind(appraisalId));
                            foreach (Row departmentRow in departmentRowSet)
                            {
                                string departmentId = departmentRow.GetValue<string>("department_id");
                                Department selectedDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, (int)Department.QUERY_TYPE_BASIC, mainSession).Department;
                                if (selectedDepartment != null)
                                {
                                    privacy.TargetedDepartments.Add(selectedDepartment);
                                }
                            }
                        }

                        if (privacy.IsForUser)
                        {
                            User userManager = new User();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_template_targeted_user", new List<string>(), new List<string> { "appraisal_id" }));
                            RowSet userRowSet = mainSession.Execute(ps.Bind(appraisalId));
                            foreach (Row userRow in userRowSet)
                            {
                                string userId = userRow.GetValue<string>("user_id");
                                User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                                if (selectedUser != null)
                                {
                                    privacy.TargetedUsers.Add(selectedUser);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return privacy;
        }

        private bool CheckTemplatePrivacy(string appraisalId, string userId, bool isForEveryone, bool isForDepartment, bool isForUser, List<string> departmentIds, ISession mainSession)
        {
            bool isAppraisalForCurrentUser = false;

            if (isForEveryone)
            {
                isAppraisalForCurrentUser = true;
            }
            else
            {
                if (isForDepartment)
                {
                    foreach (string departmentId in departmentIds)
                    {
                        PreparedStatement psTargetedDepartment = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_template_targeted_department",
                            new List<string>(), new List<string> { "appraisal_id", "department_id" }));
                        Row departmentPrivacyRow = mainSession.Execute(psTargetedDepartment.Bind(appraisalId, departmentId)).FirstOrDefault();

                        if (departmentPrivacyRow != null)
                        {
                            isAppraisalForCurrentUser = true;
                            break;
                        }
                    }
                }

                if (!isAppraisalForCurrentUser && isForUser)
                {
                    PreparedStatement psTargetedUser = mainSession.Prepare(CQLGenerator.SelectStatement("appraisal_360_template_targeted_user",
                            new List<string>(), new List<string> { "appraisal_id", "user_id" }));
                    Row userPrivacyRow = mainSession.Execute(psTargetedUser.Bind(appraisalId, userId)).FirstOrDefault();

                    if (userPrivacyRow != null)
                    {
                        isAppraisalForCurrentUser = true;
                    }
                }
            }

            return isAppraisalForCurrentUser;
        }
    }

}
