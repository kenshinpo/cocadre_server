﻿using Cassandra;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;
using CassandraService.CassandraUtilities;
using CassandraService.Validation;
using CassandraService.Utilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using System.Web.Configuration;
using System.IO;
using System.Text;
using System.IO.Compression;
using Microsoft.VisualBasic.FileIO;
using CassandraService.ServiceRequest;
using Newtonsoft.Json;
using System.Net;
using Amazon.Runtime;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using CassandraService.Utilities.AWS;
using System.Security.Cryptography;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class Topic
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember]
        public const int CODE_CHALLENGE = 1;
        [DataMember]
        public const int CODE_COURSEWARE = 2;

        [DataMember(EmitDefaultValue = false)]
        public string TopicId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TopicTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TopicLogoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public TopicStatus Status { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TopicType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SelectedNumberOfQuestions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalNumberOfQuestions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TopicDescription { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public TopicCategory TopicCategory { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsForEveryone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CreatedOnTimestampString { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime LastModifiedTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ChallengeQuestion> Questions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsInLiveEvent { get; set; }

        public enum TopicCSVFieldTypeEnum
        {
            QuestionType = 0,
            Content = 1,
            ChoiceType = 2,
            CorrectAnswer = 3,
            SecondAnswer = 4,
            ThirdAnswer = 5,
            FourthAnswer = 6,
            ReadingTime = 7,
            AnsweringTime = 8,
            DifficultyLevel = 9,
            ScoreMultiplier = 10,
            Frequency = 11
        }

        [Serializable]
        [DataContract]
        public class TopicStatus
        {
            public const int CODE_DELETED = -1;
            public const int CODE_UNLISTED = 1;
            public const int CODE_ACTIVE = 2;
            public const int CODE_HIDDEN = 3;

            [DataMember]
            public int Code { get; private set; }
            [DataMember]
            public string Title { get; private set; }

            public TopicStatus(int code)
            {
                switch (code)
                {
                    case CODE_DELETED:
                        Code = CODE_DELETED;
                        Title = "Deleted";
                        break;
                    case CODE_UNLISTED:
                        Code = CODE_UNLISTED;
                        Title = "Unlisted";
                        break;
                    case CODE_ACTIVE:
                        Code = CODE_ACTIVE;
                        Title = "Active";
                        break;
                    case CODE_HIDDEN:
                        Code = CODE_HIDDEN;
                        Title = "Hidden";
                        break;
                    default:
                        break;
                }
            }
        }

        [Serializable]
        public class DropdownNumberOfQuestions
        {
            [DataMember]
            public int Value { get; set; }
            [DataMember]
            public String DisplayText { get; set; }
        }

        [Serializable]
        public enum InvalidateChallengeReason
        {
            CancelChallengeByInitiator = 1,
            CancelChallengeByChallenge = 2,
            InvalidChallenge = 3,
            NetworkError = 4,
        }

        public List<TopicStatus> SelectTopicStatusForDropdown()
        {
            List<TopicStatus> topicStatuses = new List<TopicStatus>();
            try
            {
                topicStatuses.Add(new TopicStatus(TopicStatus.CODE_ACTIVE));
                topicStatuses.Add(new TopicStatus(TopicStatus.CODE_UNLISTED));
                topicStatuses.Add(new TopicStatus(TopicStatus.CODE_HIDDEN));
                topicStatuses = topicStatuses.OrderBy(x => x.Code).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicStatuses;
        }

        public List<DropdownNumberOfQuestions> SelectNumberOfQuestionsForDropdown()
        {
            List<DropdownNumberOfQuestions> numbers = new List<DropdownNumberOfQuestions>();
            int minimum = Int32.Parse(WebConfigurationManager.AppSettings["minimum_selected_questions_setting"].ToString());
            int maximum = Int32.Parse(WebConfigurationManager.AppSettings["maximum_selected_questions_setting"].ToString());
            int recommended = Int32.Parse(WebConfigurationManager.AppSettings["recommended_selected_questions_setting"].ToString());

            for (int index = minimum; index <= maximum; index++)
            {
                DropdownNumberOfQuestions number = new DropdownNumberOfQuestions();
                string displayText = index.ToString();
                if (index == recommended)
                {
                    displayText = string.Format("Recommended - {0} questions", index);
                }

                number.DisplayText = displayText;
                number.Value = index;

                numbers.Add(number);
                numbers = numbers.OrderBy(x => x.Value).ToList();
            }

            return numbers;
        }

        public TopicSelectIconResponse SelectAllTopicIcons(string adminUserId,
                                                           string companyId)
        {
            TopicSelectIconResponse response = new TopicSelectIconResponse();
            response.TopicIconUrls = new List<string>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psTopicIcon = session.Prepare(CQLGenerator.SelectStatement("default_topic_icon", new List<string>(), new List<string> { "icon_pk" }));
                RowSet topicIconRowset = session.Execute(psTopicIcon.Bind("iconpk"));

                foreach (Row topicIconRow in topicIconRowset)
                {
                    response.TopicIconUrls.Add(topicIconRow.GetValue<string>("icon_url"));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public TopicCreateResponse CreateTopic(string adminUserId,
                                               string companyId,
                                               string topicTitle,
                                               string topicLogoUrl,
                                               string topicDescription,
                                               string categoryId,
                                               string categoryTitle,
                                               List<string> targetedDepartmentIds,
                                               int numberOfSelectedQuestions,
                                               bool isForEveryone)
        {
            TopicCreateResponse response = new TopicCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                DateTime currentDate = DateTime.UtcNow;
                BatchStatement batchStatement = new BatchStatement();

                if (string.IsNullOrEmpty(categoryId))
                {
                    if (!string.IsNullOrEmpty(categoryTitle))
                    {
                        categoryId = UUIDGenerator.GenerateUniqueIDForTopicCategory();
                        PreparedStatement psCategory = session.Prepare(CQLGenerator.InsertStatement("topic_category",
                            new List<string> { "id", "title", "company_id", "is_valid", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                        batchStatement = batchStatement.Add(psCategory.Bind(categoryId, categoryTitle, companyId, true, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate)));

                        PreparedStatement psCategoryByTitle = session.Prepare(CQLGenerator.InsertStatement("topic_category_by_company_title",
                            new List<string> { "topic_category_id", "topic_category_title", "company_id" }));
                        batchStatement = batchStatement.Add(psCategoryByTitle.Bind(categoryId, categoryTitle, companyId));
                    }
                    else
                    {
                        response.ErrorCode = Int32.Parse(ErrorCode.CategoryMissingTitle);
                        response.ErrorMessage = ErrorMessage.CategoryMissingTitle;
                        return response;
                    }
                }
                else
                {
                    Row categoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);
                    if (categoryRow == null)
                    {
                        response.ErrorCode = Int32.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }
                }

                string topicId = UUIDGenerator.GenerateUniqueIDForTopic();

                bool isForDepartment = targetedDepartmentIds.Count > 0 ? true : false;
                isForEveryone = !isForDepartment ? true : false;

                PreparedStatement psTopic = session.Prepare(CQLGenerator.InsertStatement("topic",
                        new List<string> { "id", "category_id", "title", "logo_url", "description", "type", "status", "selected_number_of_questions", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));

                PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.InsertStatement("topic_privacy",
                    new List<string> { "topic_id", "company_id", "topic_type", "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group" }));

                PreparedStatement psTopicByCategory = session.Prepare(CQLGenerator.InsertStatement("topic_by_category",
                    new List<string> { "topic_id", "topic_category_id" }));
                PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.InsertStatement("category_by_topic",
                    new List<string> { "topic_id", "topic_category_id" }));

                batchStatement = batchStatement
                   .Add(psTopic.Bind(topicId, categoryId, topicTitle, topicLogoUrl, topicDescription, CODE_CHALLENGE, TopicStatus.CODE_UNLISTED, numberOfSelectedQuestions, adminUserId, DateHelper.ConvertDateToLong(currentDate), adminUserId, DateHelper.ConvertDateToLong(currentDate)))
                   .Add(psTopicPrivacy.Bind(topicId, companyId, CODE_CHALLENGE, isForEveryone, isForDepartment, false, false))
                   .Add(psTopicByCategory.Bind(topicId, categoryId))
                   .Add(psCategoryByTopic.Bind(topicId, categoryId));


                foreach (string departmentId in targetedDepartmentIds)
                {
                    Row departmentRow = vh.ValidateDepartment(departmentId, companyId, session);
                    if (departmentRow != null)
                    {
                        PreparedStatement psDepartment = session.Prepare(CQLGenerator.InsertStatement("topic_targeted_department",
                            new List<string> { "topic_id", "department_id" }));
                        batchStatement = batchStatement.Add(psDepartment.Bind(topicId, departmentId));

                        psDepartment = session.Prepare(CQLGenerator.InsertStatement("department_targeted_topic",
                            new List<string> { "topic_id", "department_id" }));
                        batchStatement = batchStatement.Add(psDepartment.Bind(topicId, departmentId));

                    }
                }

                session.Execute(batchStatement);
                response.TopicId = topicId;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }



        public TopicSelectResponse SelectTopicBasic(string topicId,
                                                    string adminUserId,
                                                    string companyId,
                                                    string topicCategoryId,
                                                    TopicCategory topicCategory,
                                                    ISession session = null,
                                                    string containsName = null,
                                                    bool isCheckForActive = false,
                                                    bool isCheckForValid = true)
        {
            TopicSelectResponse response = new TopicSelectResponse();
            response.Topic = null;
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (topicCategory == null && !string.IsNullOrEmpty(topicCategoryId))
                {
                    topicCategory = new TopicCategory().SelectCategory(topicCategoryId, companyId, adminUserId, session).TopicCategory;
                }
                else if (topicCategory == null && string.IsNullOrEmpty(topicCategoryId))
                {
                    PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.SelectStatement("category_by_topic",
                        new List<string> { "topic_category_id" }, new List<string> { "topic_id" }));
                    BoundStatement bsCategoryByTopic = psCategoryByTopic.Bind(topicId);
                    Row categoryByTopicRow = session.Execute(bsCategoryByTopic).FirstOrDefault();

                    if (categoryByTopicRow != null)
                    {
                        topicCategoryId = categoryByTopicRow.GetValue<string>("topic_category_id");
                        topicCategory = new TopicCategory().SelectCategory(topicCategoryId, companyId, adminUserId, session).TopicCategory;
                    }
                    else
                    {
                        Log.Error("Topic does not have a category: " + topicId);
                    }
                }

                PreparedStatement psTopic = null;
                BoundStatement bsTopic = null;

                if (topicCategory != null)
                {
                    psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                    new List<string>(), new List<string> { "category_id", "id" }));
                    bsTopic = psTopic.Bind(topicCategoryId, topicId);
                    Row topicRow = session.Execute(bsTopic).FirstOrDefault();

                    if (topicRow != null)
                    {
                        int status = topicRow.GetValue<int>("status");
                        string topicTitle = topicRow.GetValue<string>("title");

                        bool isFound = true;

                        if (!string.IsNullOrEmpty(containsName))
                        {
                            isFound = topicTitle.ToLower().Contains(containsName.ToLower());
                        }

                        if (isFound)
                        {
                            if (isCheckForValid)
                            {
                                if (status <= TopicStatus.CODE_DELETED)
                                {
                                    return response;
                                }
                            }

                            if (isCheckForActive)
                            {
                                if (status != TopicStatus.CODE_ACTIVE)
                                {
                                    return response;
                                }
                            }

                            string logoUrl = topicRow.GetValue<string>("logo_url");
                            string description = topicRow.GetValue<string>("description");
                            int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                            int totalNumberOfQuestions = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ALL_EXCEPT_DELETE, session);
                            DateTime createdTime = topicRow.GetValue<DateTime>("created_on_timestamp");

                            Event eventManager = new Event();
                            double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, session);
                            response.Topic = new Topic
                            {
                                TopicId = topicId,
                                TopicLogoUrl = logoUrl,
                                TopicTitle = topicTitle,
                                TopicDescription = description,
                                Status = new TopicStatus(status),
                                TopicCategory = topicCategory,
                                TotalNumberOfQuestions = totalNumberOfQuestions,
                                SelectedNumberOfQuestions = selectedNumberOfQuestions,
                                IsInLiveEvent = eventManager.CheckEventInProgress(null, companyId, topicId, session).isEventInProgress,
                                CreatedOnTimestamp = createdTime.AddHours(timezoneOffset),
                                CreatedOnTimestampString = createdTime.AddHours(timezoneOffset).ToString("dd/MM/yyyy hh:mm tt")
                            };
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

#warning Deprecated code in SelectAllTopicBasicByUserAndCategory, replaced by SelectTopicsWithRank
        public CategorySelectAllWithTopicResponse SelectAllTopicBasicByUserAndCategory(string requesterUserId,
                                                                                       string companyId)
        {
            CategorySelectAllWithTopicResponse response = new CategorySelectAllWithTopicResponse();
            response.TopicCategories = new List<TopicCategory>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                List<Department> departments = new Department().GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;

                // Fetch all valid topic category sorted by title
                PreparedStatement psCategoryByTitle = session.Prepare(CQLGenerator.SelectStatement("topic_category_by_company_title",
                            new List<string>(), new List<string> { "company_id" }));
                BoundStatement bsCategoryByTitle = psCategoryByTitle.Bind(companyId);
                RowSet categoryByTitleRowset = session.Execute(bsCategoryByTitle);

                Event eventManager = new Event();

                foreach (Row categoryByTitleRow in categoryByTitleRowset)
                {
                    string categoryId = categoryByTitleRow.GetValue<string>("topic_category_id");
                    string categoryTitle = categoryByTitleRow.GetValue<string>("topic_category_title");

                    TopicCategory category = new TopicCategory
                    {
                        Id = categoryId,
                        Title = categoryTitle,
                        Topics = new List<Topic>()
                    };

                    // Fetch all valid topic in category sorted by timestamp
                    PreparedStatement psTopicByTimestamp = session.Prepare(CQLGenerator.SelectStatement("topic_by_company_category_timestamp",
                        new List<string>(), new List<string> { "company_id", "topic_category_id" }));
                    BoundStatement bsTopicByTimestamp = psTopicByTimestamp.Bind(companyId, categoryId);
                    RowSet topicByTimestampRowset = session.Execute(bsTopicByTimestamp);

                    foreach (Row topicByTimestampRow in topicByTimestampRowset)
                    {
                        string topicId = topicByTimestampRow.GetValue<string>("topic_id");

                        if (CheckTopicForCurrentUser(companyId, topicId, departments, session))
                        {
                            // Select topic
                            PreparedStatement psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                                new List<string>(), new List<string> { "category_id", "id" }));
                            BoundStatement bsTopic = psTopic.Bind(category.Id, topicId);
                            Row topicRow = session.Execute(bsTopic).FirstOrDefault();

                            if (topicRow != null)
                            {
                                int status = topicRow.GetValue<int>("status");

                                if (status == TopicStatus.CODE_ACTIVE)
                                {

                                    string topicDescription = topicRow.GetValue<string>("description");
                                    string topicTitle = topicRow.GetValue<string>("title");
                                    string logoUrl = topicRow.GetValue<string>("logo_url");
                                    category.Topics.Add(new Topic
                                    {
                                        TopicId = topicId,
                                        TopicTitle = topicTitle,
                                        TopicDescription = topicDescription,
                                        TopicLogoUrl = logoUrl,
                                        IsInLiveEvent = eventManager.CheckEventInProgress(null, companyId, topicId, session).isEventInProgress
                                    });
                                }
                            }
                            else
                            {
                                Log.Error(string.Format("Topic table having error with -> topicId: {0}, categoryId: {1}", topicId, categoryId));
                            }

                        }
                    }

                    if (category.Topics.Count > 0)
                    {
                        response.TopicCategories.Add(category);
                    }

                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public CategorySelectAllWithTopicResponse SelectTopicsWithRank(string requesterUserId,
                                                                       string companyId)
        {
            CategorySelectAllWithTopicResponse response = new CategorySelectAllWithTopicResponse();
            response.UserExp = new AnalyticQuiz.Exp();
            response.TopicCategories = new List<TopicCategory>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Getting rank
                AnalyticQuiz analytic = new AnalyticQuiz();
                AnalyticQuiz.Exp currentUserExp = analytic.SelectLevelOfUser(requesterUserId, analyticSession);

                string rank = "Unranked";

                PreparedStatement psLeaderboardByCompany = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_company_sorted",
                        new List<string>(), new List<string> { "company_id" }));
                BoundStatement bsLeaderboardByCompany = psLeaderboardByCompany.Bind(companyId);
                RowSet leaderboardByCompanyRowset = mainSession.Execute(bsLeaderboardByCompany);

                int index = 1;
                foreach (Row leaderboardByCompanyRow in leaderboardByCompanyRowset)
                {
                    string userId = leaderboardByCompanyRow.GetValue<string>("user_id");
                    if (userId.Equals(requesterUserId))
                    {
                        rank = index.ToString();
                    }
                    index++;
                }

                currentUserExp.Rank = rank;
                response.UserExp = currentUserExp;

                List<Department> departments = new Department().GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;

                // Fetch all valid topic category sorted by title
                PreparedStatement psCategoryByTitle = mainSession.Prepare(CQLGenerator.SelectStatement("topic_category_by_company_title",
                            new List<string>(), new List<string> { "company_id" }));
                BoundStatement bsCategoryByTitle = psCategoryByTitle.Bind(companyId);
                RowSet categoryByTitleRowset = mainSession.Execute(bsCategoryByTitle);

                Event eventManager = new Event();

                foreach (Row categoryByTitleRow in categoryByTitleRowset)
                {
                    string categoryId = categoryByTitleRow.GetValue<string>("topic_category_id");
                    string categoryTitle = categoryByTitleRow.GetValue<string>("topic_category_title");

                    TopicCategory category = new TopicCategory
                    {
                        Id = categoryId,
                        Title = categoryTitle,
                        Topics = new List<Topic>()
                    };

                    // Fetch all valid topic in category sorted by timestamp
                    PreparedStatement psTopicByTimestamp = mainSession.Prepare(CQLGenerator.SelectStatement("topic_by_company_category_timestamp",
                        new List<string>(), new List<string> { "company_id", "topic_category_id" }));
                    BoundStatement bsTopicByTimestamp = psTopicByTimestamp.Bind(companyId, categoryId);
                    RowSet topicByTimestampRowset = mainSession.Execute(bsTopicByTimestamp);

                    foreach (Row topicByTimestampRow in topicByTimestampRowset)
                    {
                        string topicId = topicByTimestampRow.GetValue<string>("topic_id");

                        if (CheckTopicForCurrentUser(companyId, topicId, departments, mainSession))
                        {
                            // Select topic
                            PreparedStatement psTopic = mainSession.Prepare(CQLGenerator.SelectStatement("topic",
                                new List<string>(), new List<string> { "category_id", "id" }));
                            BoundStatement bsTopic = psTopic.Bind(category.Id, topicId);
                            Row topicRow = mainSession.Execute(bsTopic).FirstOrDefault();

                            if (topicRow != null)
                            {
                                int status = topicRow.GetValue<int>("status");

                                if (status == TopicStatus.CODE_ACTIVE)
                                {

                                    string topicDescription = topicRow.GetValue<string>("description");
                                    string topicTitle = topicRow.GetValue<string>("title");
                                    string logoUrl = topicRow.GetValue<string>("logo_url");
                                    category.Topics.Add(new Topic
                                    {
                                        TopicId = topicId,
                                        TopicTitle = topicTitle,
                                        TopicDescription = topicDescription,
                                        TopicLogoUrl = logoUrl,
                                        IsInLiveEvent = eventManager.CheckEventInProgress(null, companyId, topicId, mainSession).isEventInProgress
                                    });
                                }
                            }
                            else
                            {
                                Log.Error(string.Format("Topic table having error with -> topicId: {0}, categoryId: {1}", topicId, categoryId));
                            }

                        }
                    }

                    if (category.Topics.Count > 0)
                    {
                        response.TopicCategories.Add(category);
                    }

                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByOpponent(string initiatorUserId,
                                                                         string challengedUserId,
                                                                         string companyId,
                                                                         string topicStartsWithName)
        {
            TopicSelectAllBasicResponse response = new TopicSelectAllBasicResponse();
            response.Topics = new List<Topic>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(initiatorUserId, companyId, session);

                if (es != null)
                {
                    Log.Error("Initiator user not valid");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                es = vh.isValidatedAsUser(challengedUserId, companyId, session);

                if (es != null)
                {
                    Log.Error("Challenge user not valid");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserOpponentInvalid);
                    response.ErrorMessage = ErrorMessage.UserOpponentInvalid;
                    return response;
                }

                // Get all topics that target to department only
                Department department = new Department();
                List<Department> initiatorDepartments = department.GetAllDepartmentByUserId(initiatorUserId, companyId, session).Departments;
                List<Department> challengedDepartments = department.GetAllDepartmentByUserId(challengedUserId, companyId, session).Departments;

                List<string> initiatorTopicIds = SelectTopicIdByDepartments(initiatorDepartments, session);
                List<string> challengedTopicIds = SelectTopicIdByDepartments(challengedDepartments, session);

                List<string> visibleTopicIds = initiatorTopicIds.Intersect(challengedTopicIds).ToList();

                // Get all topics targeted to everyone
                visibleTopicIds.AddRange(SelectTopicIdForEveryone(companyId, session));

                foreach (string visibleTopicId in visibleTopicIds)
                {
                    Topic topicFound = SelectTopicBasic(visibleTopicId, null, companyId, null, null, session, topicStartsWithName).Topic;

                    if (topicFound != null && topicFound.Status.Code == (int)TopicStatus.CODE_ACTIVE)
                    {
                        response.Topics.Add(topicFound);
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        private List<string> SelectTopicIdForEveryone(string companyId, ISession session)
        {
            List<string> topicIds = new List<string>();

            try
            {
                PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.SelectStatement("topic_privacy",
                            new List<string>(), new List<string> { "company_id", "is_for_everyone" }));
                BoundStatement bsTopicPrivacy = psTopicPrivacy.Bind(companyId, true);
                RowSet privacyRowset = session.Execute(bsTopicPrivacy);

                foreach (Row privacyRow in privacyRowset)
                {
                    string topicId = privacyRow.GetValue<string>("topic_id");
                    topicIds.Add(topicId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicIds;
        }

        private List<string> SelectTopicIdByDepartments(List<Department> departments, ISession session)
        {
            List<string> topicIds = new List<string>();

            try
            {
                foreach (Department department in departments)
                {
                    string departmentId = department.Id;

                    // Fetch all valid topic by department
                    PreparedStatement psTopicByDepartment = session.Prepare(CQLGenerator.SelectStatement("department_targeted_topic",
                        new List<string>(), new List<string> { "department_id" }));
                    BoundStatement bsTopicByDepartment = psTopicByDepartment.Bind(departmentId);
                    RowSet topicByDepartmentRowset = session.Execute(bsTopicByDepartment);

                    foreach (Row topicByDepartmentRow in topicByDepartmentRowset)
                    {
                        string topicId = topicByDepartmentRow.GetValue<string>("topic_id");
                        topicIds.Add(topicId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicIds;
        }

        public TopicSelectResponse SelectTopicDetail(string topicId,
                                                     string adminUserId,
                                                     string companyId,
                                                     string searchQuestionContent = null,
                                                     ISession session = null)
        {
            TopicSelectResponse response = new TopicSelectResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }


                PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.SelectStatement("category_by_topic",
                                       new List<string> { "topic_category_id" }, new List<string> { "topic_id" }));
                BoundStatement bsCategoryByTopic = psCategoryByTopic.Bind(topicId);
                Row categoryByTopicRow = session.Execute(bsCategoryByTopic).FirstOrDefault();

                string topicCategoryId = categoryByTopicRow.GetValue<string>("topic_category_id");
                TopicCategory topicCategory = new TopicCategory().SelectCategory(topicCategoryId, companyId, adminUserId, session).TopicCategory;

                PreparedStatement psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                    new List<string> { "title", "logo_url", "description", "type", "selected_number_of_questions", "status" }, new List<string> { "category_id", "id" }));
                BoundStatement bsTopic = psTopic.Bind(topicCategoryId, topicId);
                Row topicRow = session.Execute(bsTopic).FirstOrDefault();

                if (topicRow != null)
                {
                    int status = topicRow.GetValue<int>("status");

                    if (status > TopicStatus.CODE_DELETED)
                    {
                        string topicTitle = topicRow.GetValue<string>("title");
                        string logoUrl = topicRow.GetValue<string>("logo_url");
                        string description = topicRow.GetValue<string>("description");
                        int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                        int totalNumberOfQuestions = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ALL_EXCEPT_DELETE, session);

                        PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.SelectStatement("topic_privacy",
                            new List<string>(), new List<string> { "company_id", "topic_id" }));
                        BoundStatement bsTopicPrivacy = psTopicPrivacy.Bind(companyId, topicId);
                        Row privacyRowset = session.Execute(bsTopicPrivacy).FirstOrDefault();

                        if (privacyRowset != null)
                        {
                            bool isForEveryone = privacyRowset.GetValue<bool>("is_for_everyone");

                            List<Department> departments = new Department().GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_BASIC, session).Departments;

                            if (!isForEveryone)
                            {
                                bool isForDepartment = privacyRowset.GetValue<bool>("is_for_department");

                                if (isForDepartment)
                                {
                                    PreparedStatement psDepartment = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department",
                                    new List<string> { "department_id" }, new List<string> { "topic_id" }));
                                    BoundStatement bsDepartment = psDepartment.Bind(topicId);
                                    RowSet departmentRowset = session.Execute(bsDepartment);

                                    departments = new Department().GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_BASIC, session).Departments;

                                    foreach (Row departmentRow in departmentRowset)
                                    {
                                        string departmentId = departmentRow.GetValue<string>("department_id");

                                        Department targetedDepartment = departments.Where(department => department.Id.Equals(departmentId)).FirstOrDefault();

                                        if (targetedDepartment != null)
                                        {
                                            targetedDepartment.IsTargetedForTopic = true;
                                        }
                                    }
                                }

                            }

                            bool isInLiveEvent = false;
                            Event eventManager = new Event();
                            if (eventManager.CheckEventInProgress(adminUserId, companyId, topicId, session).isEventInProgress)
                            {
                                isInLiveEvent = true;
                            }

                            response.Topic = new Topic
                            {
                                TopicId = topicId,
                                TopicLogoUrl = logoUrl,
                                TopicTitle = topicTitle,
                                TopicDescription = description,
                                Status = new TopicStatus(status),
                                TopicCategory = topicCategory,
                                TotalNumberOfQuestions = totalNumberOfQuestions,
                                SelectedNumberOfQuestions = selectedNumberOfQuestions,
                                IsForEveryone = isForEveryone,
                                TargetedDepartments = departments,
                                Questions = new ChallengeQuestion().SelectAllQuestionsForTopic(adminUserId, companyId, topicId, topicCategoryId, session, searchQuestionContent).Questions,
                                IsInLiveEvent = isInLiveEvent
                            };

                            response.Success = true;
                        }


                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByCategoryAndDepartment(string adminUserId,
                                                                                      string companyId,
                                                                                      string selectedTopicCategoryId,
                                                                                      string selectedDepartmentId,
                                                                                      string containsName = null,
                                                                                      bool isCheckForActive = false,
                                                                                      bool isCheckForValid = true,
                                                                                      ISession session = null)
        {
            TopicSelectAllBasicResponse response = new TopicSelectAllBasicResponse();
            response.Topics = new List<Topic>();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }


                List<string> filteredIdsForCategoryList = new List<string>();
                List<string> filteredIdsForDepartmentList = new List<string>();
                List<string> filteredIdsList = new List<string>();

                bool isFilterApplied = false;

                TopicCategory topicCategory = null;


                if (!string.IsNullOrEmpty(selectedDepartmentId))
                {
                    filteredIdsForDepartmentList = SelectTopicIdByDepartmentId(selectedDepartmentId, session);
                    isFilterApplied = true;
                }

                if (!string.IsNullOrEmpty(selectedTopicCategoryId))
                {
                    filteredIdsForCategoryList = SelectTopicIdByCategory(selectedTopicCategoryId, session);
                    isFilterApplied = true;
                    topicCategory = new TopicCategory().SelectCategory(selectedTopicCategoryId, companyId, adminUserId, session).TopicCategory;
                }

                if (filteredIdsForCategoryList.Count > 0 && filteredIdsForDepartmentList.Count == 0)
                {
                    filteredIdsList = filteredIdsForCategoryList;
                }
                else if (filteredIdsForDepartmentList.Count > 0 && filteredIdsForCategoryList.Count == 0)
                {
                    filteredIdsList = filteredIdsForDepartmentList;
                }
                else
                {
                    filteredIdsList = filteredIdsForCategoryList.Intersect(filteredIdsForDepartmentList).ToList();
                }

                // No filter is applied
                if (!isFilterApplied)
                {
                    foreach (TopicCategory category in new TopicCategory().SelectAllCategoriesForDropdown(adminUserId, companyId, isCheckForValid, session).TopicCategories)
                    {
                        PreparedStatement psTopic = session.Prepare(CQLGenerator.SelectStatement("topic",
                            new List<string>(), new List<string> { "category_id" }));
                        BoundStatement bsTopic = psTopic.Bind(category.Id);
                        RowSet topicRowset = session.Execute(bsTopic);

                        foreach (Row topicRow in topicRowset)
                        {
                            string topicId = topicRow.GetValue<string>("id");
                            Topic topic = SelectTopicBasic(topicId, adminUserId, companyId, category.Id, category, session, containsName, isCheckForActive, isCheckForValid).Topic;
                            if (topic != null)
                            {
                                response.Topics.Add(topic);
                            }

                            // OLD
                            //int status = topicRow.GetValue<int>("status");
                            //if (status > TopicStatus.CODE_DELETED)
                            //{
                            //    string topicId = topicRow.GetValue<string>("id");
                            //    string topicTitle = topicRow.GetValue<string>("title");
                            //    string logoUrl = topicRow.GetValue<string>("logo_url");
                            //    int totalNumberOfQuestions = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ALL_EXCEPT_DELETE, session);

                            //    response.Topics.Add(new Topic
                            //    {
                            //        TopicId = topicId,
                            //        TopicLogoUrl = logoUrl,
                            //        TopicTitle = topicTitle,
                            //        Status = new TopicStatus(status),
                            //        TopicCategory = category,
                            //        TotalNumberOfQuestions = totalNumberOfQuestions
                            //    });
                            //}

                        }
                    }
                }
                else
                {
                    foreach (string topicId in filteredIdsList)
                    {
                        Topic topic = SelectTopicBasic(topicId, adminUserId, companyId, selectedTopicCategoryId, topicCategory, session, containsName).Topic;
                        if (topic != null)
                        {
                            response.Topics.Add(topic);
                        }
                    }

                    // Include topics meant for everyone
                    List<Topic> topicsForEveryone = SelectAllTopicBasicByEveryone(adminUserId, companyId, session, containsName, selectedTopicCategoryId).Topics;
                    IEqualityComparer<Topic> topicComparer = new PropertyComparer<Topic>("TopicId");
                    response.Topics = response.Topics.Union(topicsForEveryone).Distinct(topicComparer).ToList();

                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByEveryone(string adminUserId,
                                                                         string companyId,
                                                                         ISession session = null,
                                                                         string containsName = null,
                                                                         string topicCategoryId = null)
        {
            TopicSelectAllBasicResponse response = new TopicSelectAllBasicResponse();
            response.Topics = new List<Topic>();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.SelectStatement("topic_privacy",
                    new List<string>(), new List<string> { "company_id", "is_for_everyone" }));
                RowSet topicPrivacyRowset = session.Execute(psTopicPrivacy.Bind(companyId, true));

                foreach (Row topicPrivacyRow in topicPrivacyRowset)
                {
                    string topicId = topicPrivacyRow.GetValue<string>("topic_id");
                    Topic topic = SelectTopicBasic(topicId, adminUserId, companyId, null, null, session, containsName, true).Topic;

                    if (topic != null)
                    {
                        if (!string.IsNullOrEmpty(topicCategoryId))
                        {
                            if (topic.TopicCategory.Id == topicCategoryId)
                            {
                                response.Topics.Add(topic);
                            }
                        }
                        else
                        {
                            response.Topics.Add(topic);
                        }

                    }
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public bool CheckTopicForCurrentUser(string companyId, string topicId, List<Department> departments, ISession session)
        {
            try
            {
                PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.SelectStatement("topic_privacy",
                            new List<string>(), new List<string> { "company_id", "topic_id" }));
                BoundStatement bsTopicPrivacy = psTopicPrivacy.Bind(companyId, topicId);
                Row privacyRowset = session.Execute(bsTopicPrivacy).FirstOrDefault();

                bool isForEveryone = privacyRowset.GetValue<bool>("is_for_everyone");

                if (isForEveryone)
                {
                    return true;
                }

                bool isForDepartment = privacyRowset.GetValue<bool>("is_for_department");

                if (isForDepartment)
                {
                    foreach (Department department in departments)
                    {
                        string departmentId = department.Id;

                        PreparedStatement psTopicByDepartment = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department",
                            new List<string>(), new List<string> { "department_id", "topic_id" }));
                        BoundStatement bsTopicByDepartment = psTopicByDepartment.Bind(departmentId, topicId);
                        Row topicByDepartmentRow = session.Execute(bsTopicByDepartment).FirstOrDefault();

                        if (topicByDepartmentRow != null)
                        {
                            return true;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);

            }

            return false;
        }

        private List<Department> SelectDepartmentByTopicId(string topicId,
                                                           string companyId,
                                                           ISession session)
        {
            List<Department> departments = new List<Department>();

            try
            {
                PreparedStatement psDepartmentByTopic = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department",
                                              new List<string> { "department_id" }, new List<string> { "topic_id" }));
                BoundStatement bsDepartmentByTopic = psDepartmentByTopic.Bind(topicId);
                RowSet topicByDepartmentRowset = session.Execute(bsDepartmentByTopic);

                foreach (Row topicByDepartmentRow in topicByDepartmentRowset)
                {
                    Department department = new Department();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return departments;
        }

        private List<string> SelectTopicIdByDepartmentId(string departmentId,
                                                        ISession session)
        {
            List<string> topicIds = new List<string>();

            try
            {
                PreparedStatement psTopicByDepartment = null;
                BoundStatement bsTopicByDepartment = null;

                psTopicByDepartment = session.Prepare(CQLGenerator.SelectStatement("department_targeted_topic",
                                               new List<string> { "topic_id" }, new List<string> { "department_id" }));
                bsTopicByDepartment = psTopicByDepartment.Bind(departmentId);
                RowSet topicByDepartmentRowset = session.Execute(bsTopicByDepartment);

                foreach (Row topicByDepartmentRow in topicByDepartmentRowset)
                {
                    topicIds.Add(topicByDepartmentRow.GetValue<string>("topic_id"));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicIds;
        }

        private List<string> SelectTopicIdByCategory(string categoryId,
                                                     ISession session)
        {
            List<string> topicIds = new List<string>();

            try
            {
                PreparedStatement psTopicByCategory = null;
                BoundStatement bsTopicByCategory = null;

                psTopicByCategory = session.Prepare(CQLGenerator.SelectStatement("topic_by_category",
                                              new List<string> { "topic_id" }, new List<string> { "topic_category_id" }));
                bsTopicByCategory = psTopicByCategory.Bind(categoryId);
                RowSet topicRowset = session.Execute(bsTopicByCategory);

                foreach (Row topicRow in topicRowset)
                {
                    topicIds.Add(topicRow.GetValue<string>("topic_id"));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicIds;
        }

        public TopicUpdateResponse UpdateTopicStatus(string adminUserId,
                                                     string companyId,
                                                     string topicId,
                                                     string categoryId,
                                                     int status,
                                                     ISession session = null,
                                                     Row topicRow = null)
        {
            TopicUpdateResponse response = new TopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (session == null)
                {
                    session = cm.getMainSession();
                }

                if (topicRow == null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    // Check topic category
                    Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Int32.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }

                    // Check topic row
                    topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Int32.Parse(ErrorCode.TopicInvalid);
                        response.ErrorMessage = ErrorMessage.TopicInvalid;
                        return response;
                    }
                }

                Event eventManager = new Event();
                if (eventManager.CheckEventInProgress(adminUserId, companyId, topicId, session).isEventInProgress)
                {
                    Log.Error("Topic is currently in an on-going event");
                    response.ErrorCode = Int32.Parse(ErrorCode.TopicUpdateFailedDueToLiveEvent);
                    response.ErrorMessage = ErrorMessage.TopicUpdateFailedDueToLiveEvent;
                    return response;
                }

                PreparedStatement psTopicByTitle = null;
                PreparedStatement psTopicByTimestamp = null;
                PreparedStatement psTopic = null;
                //PreparedStatement psTopicPrivacy = null;

                BatchStatement batchStatement = new BatchStatement();

                string topicTitle = topicRow.GetValue<string>("title");
                DateTime createdTimestamp = topicRow.GetValue<DateTime>("created_on_timestamp");

                ISession analyticSession = cm.getAnalyticSession();
                AnalyticQuiz analytic = new AnalyticQuiz();

                if (status == (int)TopicStatus.CODE_ACTIVE)
                {
                    int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                    int numberOfActiveQuestion = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, session);

                    if (numberOfActiveQuestion < selectedNumberOfQuestions)
                    {
                        response.ErrorCode = Convert.ToInt16(ErrorCode.TopicLessActiveQuestions);
                        response.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                        return response;
                    }

                    psTopicByTitle = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_title",
                        new List<string> { "topic_id", "topic_title", "company_id" }));

                    psTopicByTimestamp = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_category_timestamp",
                        new List<string> { "topic_id", "topic_category_id", "company_id", "topic_created_on_timestamp" }));

                    analytic.UnhideFromLeaderboard(false, true, companyId, analyticSession, null, null, topicId);
                }
                // DELETED/ UNLISTED/ HIDDEN
                else
                {
                    psTopicByTitle = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_title",
                           new List<string> { "topic_id", "topic_title", "company_id" }));

                    psTopicByTimestamp = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_category_timestamp",
                        new List<string> { "topic_id", "topic_category_id", "company_id", "topic_created_on_timestamp" }));

#warning Deleted: remove privacy
                    if (status == TopicStatus.CODE_DELETED)
                    {
                        PreparedStatement psTopicByCategory = session.Prepare(CQLGenerator.DeleteStatement("topic_by_category",
                          new List<string> { "topic_id", "topic_category_id" }));

                        PreparedStatement psCategoryByTopic = session.Prepare(CQLGenerator.DeleteStatement("category_by_topic",
                          new List<string> { "topic_id", "topic_category_id" }));

                        PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.DeleteStatement("topic_privacy",
                            new List<string> { "company_id", "topic_id" }));

                        batchStatement = batchStatement
                            .Add(psTopicByCategory.Bind(topicId, categoryId))
                            .Add(psCategoryByTopic.Bind(topicId, categoryId))
                            .Add(psTopicPrivacy.Bind(companyId, topicId));

                        analytic.RemoveFromLeaderboard(false, true, companyId, analyticSession, null, null, topicId);

                        // Remove from matchup attempts
                        analytic.RemoveFromMatchupTopicAttempt(topicId, companyId, analyticSession);
                        analytic.RemoveFromMatchupQuestionAttempt(topicId, null, analyticSession);
                        analytic.RemoveFromMatchupUserAttempt(topicId, null, analyticSession);
                        analytic.RemoveFromMatchupOptionAttempt(topicId, null, analyticSession);
                    }
                    else if (status == TopicStatus.CODE_HIDDEN)
                    {
                        analytic.HideFromLeaderboard(false, true, companyId, analyticSession, null, null, topicId);
                    }
                }

                psTopic = session.Prepare(CQLGenerator.UpdateStatement("topic",
                    new List<string> { "category_id", "id" }, new List<string> { "status" }, new List<string>()));

                batchStatement = batchStatement
                    .Add(psTopicByTitle.Bind(topicId, topicTitle, companyId))
                    .Add(psTopicByTimestamp.Bind(topicId, categoryId, companyId, createdTimestamp))
                    .Add(psTopic.Bind(status, categoryId, topicId));

                session.Execute(batchStatement);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        // Applies not to delete status
        public TopicUpdateResponse UpdateTopic(string adminUserId,
                                               string companyId,
                                               string topicId,
                                               string newTitle,
                                               string newLogoUrl,
                                               string newDescription,
                                               string newCategoryId,
                                               string newCategoryTitle,
                                               int newStatus,
                                               List<string> newTargetedDepartmentIds,
                                               int newNumberOfSelectedQuestions,
                                               bool isForEveryone)
        {
            TopicUpdateResponse response = new TopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (!string.IsNullOrEmpty(newCategoryId))
                {
                    // Check topic category
                    Row topicCategoryRow = vh.ValidateTopicCategory(companyId, newCategoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + newCategoryId);
                        response.ErrorCode = Int32.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }
                }

                PreparedStatement psCategory = null;
                BoundStatement bsCategory = null;

                // Check for old categoryId
                psCategory = session.Prepare(CQLGenerator.SelectStatement("category_by_topic", new List<string> { "topic_category_id" }, new List<string> { "topic_id" }));
                bsCategory = psCategory.Bind(topicId);
                string oldCategoryId = session.Execute(bsCategory).FirstOrDefault().GetValue<string>("topic_category_id");

                // Check topic row
                Row topicRow = vh.ValidateTopic(companyId, oldCategoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int32.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                Event eventManager = new Event();
                if (eventManager.CheckEventInProgress(adminUserId, companyId, topicId, session).isEventInProgress)
                {
                    Log.Error("Topic is currently in an on-going event");
                    response.ErrorCode = Int32.Parse(ErrorCode.TopicUpdateFailedDueToLiveEvent);
                    response.ErrorMessage = ErrorMessage.TopicUpdateFailedDueToLiveEvent;
                    return response;
                }

                if (newStatus != (int)TopicStatus.CODE_DELETED)
                {
                    // Check status first
                    bool isUpdateStatus = true;
                    int currentTopicStatus = topicRow.GetValue<int>("status");

                    if (currentTopicStatus == newStatus)
                    {
                        isUpdateStatus = false;
                    }
                    else
                    {
                        if (newStatus == (int)TopicStatus.CODE_ACTIVE)
                        {
                            //int selectedNumberOfQuestions = topicRow.GetValue<int>("selected_number_of_questions");
                            int numberOfActiveQuestion = new ChallengeQuestion().SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, session);

                            if (numberOfActiveQuestion < newNumberOfSelectedQuestions)
                            {
                                response.ErrorCode = Convert.ToInt16(ErrorCode.TopicLessActiveQuestions);
                                response.ErrorMessage = ErrorMessage.TopicLessActiveQuestions;
                                return response;
                            }
                        }
                    }


                    bool isUpdateCategory = true;
                    bool isForDepartment = false;
                    bool isForUser = false;

                    if (!string.IsNullOrEmpty(newCategoryId))
                    {
                        // Change of category
                        if (oldCategoryId.Equals(newCategoryId))
                        {
                            isUpdateCategory = false;
                        }
                    }
                    else
                    {
                        // Need to create a new category
                        CategoryCreateResponse createCategoryResponse = new TopicCategory().Create(adminUserId, companyId, newCategoryTitle, session);
                        if (!createCategoryResponse.Success)
                        {
                            response.ErrorCode = Convert.ToInt16(createCategoryResponse.ErrorCode);
                            response.ErrorMessage = createCategoryResponse.ErrorMessage;
                            return response;
                        }

                        newCategoryId = createCategoryResponse.NewCategoryId;
                    }

                    bool isUpdateDepartment = true;

                    // Check for old department list
                    PreparedStatement psDepartment = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_department", new List<string> { "department_id" }, new List<string> { "topic_id" }));
                    BoundStatement bsDepartment = psDepartment.Bind(topicId);
                    RowSet departmentRowset = session.Execute(bsDepartment);

                    List<string> oldDepartmentIds = new List<string>();

                    foreach (Row departmentRow in departmentRowset)
                    {
                        oldDepartmentIds.Add(departmentRow.GetValue<string>("department_id"));
                    }

                    if (isForEveryone)
                    {
                        if (oldDepartmentIds.Count > 0)
                        {
                            isUpdateDepartment = true;
                        }
                    }
                    else
                    {
                        if (newTargetedDepartmentIds.Count > 0)
                        {
                            isForDepartment = true;
                            bool isDepartmentListEqual = newTargetedDepartmentIds.All(oldDepartmentIds.Contains) && newTargetedDepartmentIds.Count == oldDepartmentIds.Count;
                            isUpdateDepartment = isDepartmentListEqual ? false : true;
                        }

                    }

                    // Check for old title
                    string oldTopicTitle = topicRow.GetValue<string>("title");
                    string oldCreatedUserId = topicRow.GetValue<string>("created_by_user_id");
                    DateTimeOffset oldCreatedTimestamp = topicRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    bool isUpdateTitle = !newTitle.Equals(oldTopicTitle);

                    BatchStatement deleteBatchStatement = new BatchStatement();
                    BatchStatement updateBatchStatement = new BatchStatement();

                    if (isUpdateCategory)
                    {
                        //Delete old category id relationship
                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("topic_by_category", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(oldCategoryId, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("category_by_topic", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(oldCategoryId, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_category_timestamp", new List<string> { "company_id", "topic_category_id", "topic_created_on_timestamp", "topic_id" }));
                        bsCategory = psCategory.Bind(companyId, oldCategoryId, oldCreatedTimestamp, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.DeleteStatement("topic", new List<string> { "category_id", "id" }));
                        bsCategory = psCategory.Bind(oldCategoryId, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsCategory);

                        //Add new category id relationship
                        psCategory = session.Prepare(CQLGenerator.InsertStatement("topic_by_category", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(newCategoryId, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.InsertStatement("category_by_topic", new List<string> { "topic_category_id", "topic_id" }));
                        bsCategory = psCategory.Bind(newCategoryId, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsCategory);

                        psCategory = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_category_timestamp", new List<string> { "company_id", "topic_category_id", "topic_created_on_timestamp", "topic_id" }));
                        bsCategory = psCategory.Bind(companyId, newCategoryId, oldCreatedTimestamp, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsCategory);
                    }

                    if (isUpdateDepartment)
                    {
                        //Delete old department id relationship
                        foreach (string oldDeparmentId in oldDepartmentIds)
                        {
                            psDepartment = session.Prepare(CQLGenerator.DeleteStatement("department_targeted_topic", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(oldDeparmentId, topicId);
                            deleteBatchStatement = deleteBatchStatement.Add(bsDepartment);

                            psDepartment = session.Prepare(CQLGenerator.DeleteStatement("topic_targeted_department", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(oldDeparmentId, topicId);
                            deleteBatchStatement = deleteBatchStatement.Add(bsDepartment);
                        }

                        // Add new department id relationship
                        foreach (string newDepartmentId in newTargetedDepartmentIds)
                        {
                            psDepartment = session.Prepare(CQLGenerator.InsertStatement("department_targeted_topic", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(newDepartmentId, topicId);
                            updateBatchStatement = updateBatchStatement.Add(bsDepartment);

                            psDepartment = session.Prepare(CQLGenerator.InsertStatement("topic_targeted_department", new List<string> { "department_id", "topic_id" }));
                            bsDepartment = psDepartment.Bind(newDepartmentId, topicId);
                            updateBatchStatement = updateBatchStatement.Add(bsDepartment);
                        }
                    }

                    if (isUpdateTitle)
                    {
                        //Delete topic by company title relationship
                        PreparedStatement psTopicByTitle = session.Prepare(CQLGenerator.DeleteStatement("topic_by_company_title", new List<string> { "company_id", "topic_title", "topic_id" }));
                        BoundStatement bsTopicByTitle = psTopicByTitle.Bind(companyId, newTitle, topicId);
                        deleteBatchStatement = deleteBatchStatement.Add(bsTopicByTitle);

                        // Add topic by company title relationship
                        psTopicByTitle = session.Prepare(CQLGenerator.InsertStatement("topic_by_company_title", new List<string> { "company_id", "topic_title", "topic_id" }));
                        bsTopicByTitle = psTopicByTitle.Bind(companyId, newTitle, topicId);
                        updateBatchStatement = updateBatchStatement.Add(bsTopicByTitle);
                    }

                    PreparedStatement psTopic = session.Prepare(CQLGenerator.UpdateStatement("topic",
                        new List<string> { "category_id", "id" }, new List<string> { "title", "logo_url", "description", "type", "status", "selected_number_of_questions", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    BoundStatement bsTopic = psTopic.Bind(newTitle, newLogoUrl, newDescription, CODE_CHALLENGE, newStatus, newNumberOfSelectedQuestions, oldCreatedUserId, oldCreatedTimestamp, adminUserId, DateTime.UtcNow, newCategoryId, topicId);
                    updateBatchStatement = updateBatchStatement.Add(bsTopic);

                    psTopic = session.Prepare(CQLGenerator.UpdateStatement("topic_privacy",
                        new List<string> { "company_id", "topic_id" }, new List<string> { "topic_type", "is_for_everyone", "is_for_department", "is_for_user" }, new List<string>()));
                    bsTopic = psTopic.Bind(CODE_CHALLENGE, isForEveryone, isForDepartment, isForUser, companyId, topicId);
                    updateBatchStatement = updateBatchStatement.Add(bsTopic);

                    session.Execute(deleteBatchStatement);
                    session.Execute(updateBatchStatement);

                    if (isUpdateStatus)
                    {
                        // Need to get the current number of questions
                        topicRow = vh.ValidateTopic(companyId, oldCategoryId, topicId, session);
                        UpdateTopicStatus(adminUserId, companyId, topicId, newCategoryId, newStatus, session, topicRow);
                    }

                    response.Success = true;
                    response.NewTopicCategoryId = newCategoryId;
                }
                else
                {
                    UpdateTopicStatus(adminUserId, companyId, topicId, newCategoryId, TopicStatus.CODE_DELETED, session, topicRow);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ChallengeCreateResponse CreateChallengeWithTopicId(string companyId,
                                                                  string initiatorUserId,
                                                                  string challengedUserId,
                                                                  string topicId,
                                                                  string categoryId)
        {
            ChallengeCreateResponse response = new ChallengeCreateResponse();
            response.Notification = new Notification();
            response.Players = new List<User>();
            response.Topic = new Topic();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                // Check players
                if (initiatorUserId.Equals(challengedUserId))
                {
                    Log.Error("Initiator cannot challenged himself");
                    response.ErrorCode = Int32.Parse(ErrorCode.ChallengeSamePlayerError);
                    response.ErrorMessage = ErrorMessage.ChallengeSamePlayerError;
                    return response;
                }

                User user = new User();
                User initiatorUser = user.SelectUserBasic(initiatorUserId, companyId, true, session).User;
                User challengedUser = user.SelectUserBasic(challengedUserId, companyId, true, session).User;

                if (initiatorUser == null)
                {
                    Log.Error("Invalid initiator userId: " + initiatorUserId);
                    response.ErrorCode = Int32.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                if (!CheckPrivacyForTopic(initiatorUserId, companyId, topicId, session))
                {
                    Log.Error("Topic privacy has been updated: " + topicId);
                    response.ErrorCode = Int32.Parse(ErrorCode.TopicPrivacyNotAllowed);
                    response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowed;
                    return response;
                }

                if (challengedUser == null)
                {
                    Log.Error("Invalid challenged userId: " + challengedUserId);
                    response.ErrorCode = Int32.Parse(ErrorCode.UserOpponentInvalid);
                    response.ErrorMessage = ErrorMessage.UserOpponentInvalid;
                    return response;
                }

                if (!CheckPrivacyForTopic(challengedUserId, companyId, topicId, session))
                {
                    Log.Error("Topic privacy has been updated: " + topicId);
                    response.ErrorCode = Int32.Parse(ErrorCode.TopicPrivacyNotAllowedToOther);
                    response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowedToOther;
                    return response;
                }

                Topic challengedTopic = SelectTopicBasic(topicId, null, companyId, categoryId, null, session).Topic;

                if (challengedTopic == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int32.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                response.Players.Add(initiatorUser);
                response.Players.Add(challengedUser);

                response.Topic = challengedTopic;

                List<ChallengeQuestion> questions = new ChallengeQuestion().CreateChallengeQuestionsWithTopic(challengedTopic, companyId, session);

                string challengeId = UUIDGenerator.GenerateUniqueIDForQuizChallenge();
                PreparedStatement psHistory = session.Prepare(CQLGenerator.InsertStatement("challenge_history",
                    new List<string> { "id", "topic_id", "topic_category_id", "company_id", "players_ids", "selected_number_of_questions", "is_live", "is_valid" }));

                List<string> player_ids = new List<string>();
                BatchStatement batchStatement = new BatchStatement().Add(psHistory.Bind(challengeId, topicId, categoryId, companyId, new List<string> { initiatorUserId, challengedUserId }, challengedTopic.SelectedNumberOfQuestions, true, true));

                PreparedStatement psFullHistory = null;
                int order = 1;
                foreach (ChallengeQuestion question in questions)
                {
                    psFullHistory = session.Prepare(CQLGenerator.InsertStatement("full_challenge_history",
                                        new List<string> { "challenge_id",
                                                           "company_id",
                                                           "topic_id",
                                                           "topic_category_id",
                                                           "question_id",
                                                           "players_ids",
                                                           "type",
                                                           "question_order",
                                                           "content",
                                                           "content_image_url",
                                                           "content_image_md5",
                                                           "content_image_background_color_code",
                                                           "choice_type",

                                                           "first_choice_id",
                                                           "first_choice",
                                                           "first_choice_image_url",
                                                           "first_choice_image_md5",

                                                           "second_choice_id",
                                                           "second_choice",
                                                           "second_choice_image_url",
                                                           "second_choice_image_md5",

                                                           "third_choice_id",
                                                           "third_choice",
                                                           "third_choice_image_url",
                                                           "third_choice_image_md5",

                                                           "fourth_choice_id",
                                                           "fourth_choice",
                                                           "fourth_choice_image_url",
                                                           "fourth_choice_image_md5",

                                                           "answer",
                                                           "time_answering",
                                                           "time_reading",
                                                           "difficulty_level",

                                                           "score_multiplier",
                                                           "base_score",
                                                           "scoring_calculation_type"}));
                    batchStatement = batchStatement.Add(psFullHistory.Bind(
                        challengeId,
                        companyId,
                        topicId,
                        categoryId,
                        question.Id,
                        new List<string> { initiatorUserId, challengedUserId },
                        question.QuestionType,
                        order++,
                        question.Content, question.ContentImageUrl, question.ContentImageMd5, question.ContentImageBackgroundColorCode,
                        question.ChoiceType,
                        question.FirstChoiceId, question.FirstChoice, question.FirstChoiceContentImageUrl, question.FirstChoiceContentImageMd5,
                        question.SecondChoiceId, question.SecondChoice, question.SecondChoiceContentImageUrl, question.SecondChoiceContentImageMd5,
                        question.ThirdChoiceId, question.ThirdChoice, question.ThirdChoiceContentImageUrl, question.ThirdChoiceContentImageMd5,
                        question.FourthChoiceId, question.FourthChoice, question.FourthChoiceContentImageUrl, question.FourthChoiceContentImageMd5,
                        question.CorrectAnswer,
                        question.TimeAssignedForAnswering, question.TimeAssignedForReading,
                        question.DifficultyLevel,
                        question.ScoreMultiplier,
                        question.BaseScore,
                        question.ScoringCalculationType));
                }

                session.Execute(batchStatement);

                //Notification notification = new Notification();
                //notification.CreateGameNotification(challengedUserId, Notification.NotificationType.ChallengedToGame, challengeId, initiatorUserId, topicId, session);

                Notification notificationManager = new Notification();
                response.Notification = new Notification
                {
                    TaggedUserId = challengedUserId,
                    NotificationText = notificationManager.GenerateChallengePushNotificationText(initiatorUserId, companyId, challengedTopic.TopicTitle, (int)Notification.NotificationGameSubType.NewChallenge, session),
                    NumberOfNotificationForTargetedUser = notificationManager.SelectNotificationNumberByUser(challengedUserId, companyId, session).NumberOfNotification + 1,
                    SubType = (int)Notification.NotificationGameSubType.NewChallenge,
                    Type = (int)Notification.NotificationType.Challenge,
                    TaggedChallengeId = challengeId
                };

                response.ChallengeId = challengeId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public List<ChallengeQuestion> SelectRandomQuestions(string topicId, string categoryId, string companyId)
        {
            List<ChallengeQuestion> questions = new List<ChallengeQuestion>();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Topic selectedTopic = SelectTopicBasic(topicId, null, companyId, categoryId, null, session).Topic;

                if (selectedTopic == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                }

                questions = new ChallengeQuestion().SelectRandomQuestions(selectedTopic, companyId, session);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return questions;
        }

        public ChallengeSelectResponse SelectChallengeWithChallengeId(string companyId,
                                                                      string challengeId,
                                                                      string requesterUserId)
        {
            ChallengeSelectResponse response = new ChallengeSelectResponse();
            response.Players = new List<User>();
            response.Topic = new Topic();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null)
                {
                    string topicId = historyRow.GetValue<string>("topic_id");

                    Topic topicManager = new Topic();
                    if (!topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, session))
                    {
                        Log.Error("Topic privacy has been updated: " + topicId);
                        response.ErrorCode = Int32.Parse(ErrorCode.TopicPrivacyNotAllowedToOther);
                        response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowedToOther;
                        return response;
                    }

                    if (historyRow["completed_on_timestamp"] != null)
                    {
                        Log.Error("Challenge has already been completed: " + challengeId);
                        response.ErrorCode = Int32.Parse(ErrorCode.ChallengeAlreadyCompleted);
                        response.ErrorMessage = ErrorMessage.ChallengeAlreadyCompleted;
                        return response;
                    }
                    else
                    {
                        if (historyRow["challenged_user_started_on_timestamp"] != null && historyRow["initiated_user_started_on_timestamp"] != null)
                        {
                            Log.Error("Timeout error for challenge: " + challengeId);
                            response.ErrorCode = Int32.Parse(ErrorCode.ChallengeInvalid);
                            response.ErrorMessage = ErrorMessage.ChallengeInvalid;

                            InvalidateChallenge(requesterUserId, companyId, challengeId, (int)InvalidateChallengeReason.NetworkError, historyRow, session);

                            return response;
                        }
                    }

                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        string initiatorUserId = playerIds[0];
                        string challengedUserId = playerIds[1];

                        // Check players
                        User user = new User();
                        User initiatorUser = user.SelectUserBasic(initiatorUserId, companyId, true, session).User;
                        User challengedUser = user.SelectUserBasic(challengedUserId, companyId, true, session).User;

                        if (initiatorUser == null)
                        {
                            InvalidateChallenge(requesterUserId, companyId, challengeId, (int)InvalidateChallengeReason.InvalidChallenge, historyRow, session);

                            Log.Error("Invalid userId: " + initiatorUserId);
                            response.ErrorCode = Int32.Parse(ErrorCode.UserMatchUpOpponentInvalid);
                            response.ErrorMessage = ErrorMessage.UserMatchUpOpponentInvalid;
                            return response;
                        }

                        if (challengedUser == null)
                        {
                            InvalidateChallenge(requesterUserId, companyId, challengeId, (int)InvalidateChallengeReason.InvalidChallenge, historyRow, session);

                            Log.Error("Invalid userId: " + challengedUserId);
                            response.ErrorCode = Int32.Parse(ErrorCode.UserMatchUpOpponentInvalid);
                            response.ErrorMessage = ErrorMessage.UserMatchUpOpponentInvalid;
                            return response;
                        }

                        string topicCategoryId = historyRow.GetValue<string>("topic_category_id");

                        Topic challengedTopic = SelectTopicBasic(topicId, null, companyId, topicCategoryId, null, session).Topic;

                        if (challengedTopic == null)
                        {
                            Log.Error("Invalid topicId: " + topicId);
                            response.ErrorCode = Int32.Parse(ErrorCode.TopicInvalid);
                            response.ErrorMessage = ErrorMessage.TopicInvalid;
                            return response;
                        }

                        response.Players.Add(initiatorUser);
                        response.Players.Add(challengedUser);

                        response.Topic = challengedTopic;
                        response.ChallengeId = challengeId;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public ChallengeStartWithoutOpponentResponse StartChallengeWithoutOpponent(string challengeId, string requesterUserId, string companyId)
        {
            ChallengeStartWithoutOpponentResponse response = new ChallengeStartWithoutOpponentResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    string topicId = historyRow.GetValue<string>("topic_id");
                    string categoryId = historyRow.GetValue<string>("topic_id");

                    Topic topicManager = new Topic();
                    if (!topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, session))
                    {
                        Log.Error("Topic privacy has been updated: " + topicId);
                        response.ErrorCode = Int32.Parse(ErrorCode.TopicPrivacyNotAllowedToOther);
                        response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowedToOther;
                        return response;
                    }

                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        PreparedStatement psChallenge = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                        new List<string> { "id", "company_id" }, new List<string> { "is_live" }, new List<string>()));
                        BoundStatement bsChallenge = psChallenge.Bind(false, challengeId, companyId);
                        session.Execute(bsChallenge);

                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;

            }

            return response;
        }

        public ChallengeOfflineSelectOpponentAnswerResponse SelectOpponentAnswerForOfflineGame(string challengeId, string requesterUserId, string companyId, int round)
        {
            ChallengeOfflineSelectOpponentAnswerResponse response = new ChallengeOfflineSelectOpponentAnswerResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    string topicId = historyRow.GetValue<string>("topic_id");

                    Topic topicManager = new Topic();
                    if (!topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, session))
                    {
                        Log.Error("Topic privacy has been updated: " + topicId);
                        response.ErrorCode = Int32.Parse(ErrorCode.TopicPrivacyNotAllowedToOther);
                        response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowedToOther;
                        return response;
                    }

                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        bool isCurrentUserInitiator = true ? playerIds.First().Equals(requesterUserId) : false;

                        PreparedStatement psFullHistory = session.Prepare(CQLGenerator.SelectStatement("full_challenge_history",
                            new List<string> { "answer", "answer_of_challenged_user", "time_taken_by_challenged_user", "answer_of_initiated_user", "time_taken_by_initiated_user" }, new List<string> { "company_id", "challenge_id", "question_order" }));
                        BoundStatement bsFullHistory = psFullHistory.Bind(companyId, challengeId, round);

                        Row fullHistoryRow = session.Execute(bsFullHistory).FirstOrDefault();

                        if (fullHistoryRow != null)
                        {
                            bool isCorrectAnswer = false;
                            float timeTaken = 0.0f;
                            string opponentUserId = null;
                            bool hasOpponentAnswered = false;

                            string questionAnswer = fullHistoryRow.GetValue<string>("answer");

                            if (isCurrentUserInitiator)
                            {
                                // Take challenged user
                                string challengedUserAnswer = fullHistoryRow.GetValue<string>("answer_of_challenged_user");
                                if (!string.IsNullOrEmpty(challengedUserAnswer))
                                {
                                    hasOpponentAnswered = true;
                                    isCorrectAnswer = questionAnswer.Equals(challengedUserAnswer);
                                    timeTaken = fullHistoryRow.GetValue<float>("time_taken_by_challenged_user");
                                    opponentUserId = playerIds[1];
                                }

                            }
                            else
                            {
                                // Take initiated user
                                string initiatedUserAnswer = fullHistoryRow.GetValue<string>("answer_of_initiated_user");

                                if (!string.IsNullOrEmpty(initiatedUserAnswer))
                                {
                                    hasOpponentAnswered = true;
                                    isCorrectAnswer = questionAnswer.Equals(initiatedUserAnswer);
                                    timeTaken = fullHistoryRow.GetValue<float>("time_taken_by_initiated_user");
                                    opponentUserId = playerIds[0];
                                }
                            }

                            response.Success = true;
                            response.ChallengeId = challengeId;
                            response.IsOpponentAnswerCorrect = isCorrectAnswer;
                            response.TimeTakenByOpponent = timeTaken;
                            response.OpponentUserId = opponentUserId;
                            response.HasOpponentAnswered = hasOpponentAnswered;
                        }
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;

            }

            return response;
        }

        public ChallengeIsReadyResponse SetPlayerReadyForChallenge(string requesterUserId,
                                                                   string companyId,
                                                                   string challengeId)
        {
            ChallengeIsReadyResponse response = new ChallengeIsReadyResponse();
            response.PlayerIds = new List<string>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row historyRow = vh.ValidateChallenge(challengeId, companyId, session);

                if (historyRow != null && historyRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    string topicId = historyRow.GetValue<string>("topic_id");
                    string topicCategoryId = historyRow.GetValue<string>("topic_category_id");

                    Topic topicManager = new Topic();
                    if (!topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, session))
                    {
                        Log.Error("Topic privacy has been updated: " + topicId);
                        response.ErrorCode = Int32.Parse(ErrorCode.TopicPrivacyNotAllowedToOther);
                        response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowedToOther;
                        return response;
                    }

                    List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");

                    if (playerIds.Contains(requesterUserId))
                    {
                        bool isInitiator = true ? playerIds.FindIndex(user => user.Equals(requesterUserId)) == 0 : false;
                        bool isLive = historyRow.GetValue<bool>("is_live");
                        DateTimeOffset now = DateTime.UtcNow;
                        DateTimeOffset initiatedUserTimestamp = historyRow["initiated_user_started_on_timestamp"] != null ? historyRow.GetValue<DateTimeOffset>("initiated_user_started_on_timestamp") : now;
                        DateTimeOffset challengedUserTimestamp = historyRow["challenged_user_started_on_timestamp"] != null ? historyRow.GetValue<DateTimeOffset>("challenged_user_started_on_timestamp") : now;

                        bool haveBothPlayersAccepted = false;
                        DateTime currentTime = DateTime.UtcNow;
                        DateTimeOffset currentTimeOffset = currentTime;

                        PreparedStatement psHistory = null;
                        BatchStatement batchStatement = new BatchStatement();

                        if (isInitiator)
                        {
                            psHistory = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                new List<string> { "company_id", "id" }, new List<string> { "initiated_user_started_on_timestamp" }, new List<string>()));
                            batchStatement = batchStatement.Add(psHistory.Bind(currentTimeOffset, companyId, challengeId));

                            if (challengedUserTimestamp != now)
                            {
                                haveBothPlayersAccepted = true;
                            }
                            else
                            {
                                // Create a request for challenge if challenger have not accept
                                string categoryId = historyRow.GetValue<string>("topic_category_id");

                                string initiatorUserId = playerIds[0];
                                string challengedUserId = playerIds[1];
                                PreparedStatement psHistoryByTimestamp = session.Prepare(CQLGenerator.InsertStatement("challenge_history_by_initiated_timestamp",
                                    new List<string> { "challenge_id", "challenged_user_id", "initiated_user_id", "topic_id", "topic_category_id", "initiated_on_timestamp" }));
                                batchStatement = batchStatement.Add(psHistoryByTimestamp.Bind(challengeId, challengedUserId, initiatorUserId, topicId, categoryId, currentTimeOffset));

                                // Game Notification (Challenger user)
                                Topic challengedTopic = SelectTopicBasic(topicId, null, companyId, categoryId, null, session).Topic;
                                Notification notification = new Notification();
                                notification.CreateChallengeNotification(initiatorUserId, challengedUserId, companyId, (int)Notification.NotificationGameSubType.NewChallenge, challengeId, challengedTopic.TopicTitle, currentTime, session);
                            }
                        }
                        else
                        {
                            psHistory = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                                new List<string> { "company_id", "id" }, new List<string> { "challenged_user_started_on_timestamp" }, new List<string>()));
                            batchStatement = batchStatement.Add(psHistory.Bind(currentTimeOffset, companyId, challengeId));

                            if (initiatedUserTimestamp != now)
                            {
                                haveBothPlayersAccepted = true;

                                // Delete request once accepted
                                PreparedStatement psHistoryByTimestamp = session.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_initiated_timestamp",
                                    new List<string> { "challenge_id", "challenged_user_id", "initiated_on_timestamp" }));
                                batchStatement = batchStatement.Add(psHistoryByTimestamp.Bind(challengeId, requesterUserId, initiatedUserTimestamp));
                            }
                        }

                        session.Execute(batchStatement);

                        response.Success = true;
                        response.HaveBothPlayersAccepted = haveBothPlayersAccepted;
                        response.IsLive = isLive;
                        response.PlayerIds = playerIds;
                    }
                    else
                    {
                        Log.Error("Invalid playerId: " + requesterUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalidPlayer);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    }
                }
                else
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public ChallengeInvalidateResponse InvalidateChallenge(string userId, string companyId, string challengeId, int reason, Row historyRow = null, ISession session = null, bool isRequiredInvalidity = true)
        {
            ChallengeInvalidateResponse response = new ChallengeInvalidateResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                ValidationHandler vh = new ValidationHandler();

                string reasonString = string.Empty;
                switch (reason)
                {
                    case (int)InvalidateChallengeReason.CancelChallengeByChallenge:
                        reasonString = "Cancelled by challenger";
                        break;
                    case (int)InvalidateChallengeReason.CancelChallengeByInitiator:
                        reasonString = "Cancelled by initiator";
                        break;
                    case (int)InvalidateChallengeReason.InvalidChallenge:
                        reasonString = "Invalid challenge";
                        break;
                    case (int)InvalidateChallengeReason.NetworkError:
                        reasonString = "Pubnub timeout event";
                        break;
                }

                Log.Debug(string.Format("Invalidate challenge: {0} by userId: {1} due to <{2}>", challengeId, userId, reasonString));

                if (historyRow == null)
                {
                    historyRow = vh.ValidateChallenge(challengeId, companyId, session, isRequiredInvalidity);

                    if (historyRow == null)
                    {
                        Log.Error("Challenge already invalidated: " + challengeId);
                        response.ErrorCode = Int32.Parse(ErrorCode.ChallengeInvalid);
                        response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(companyId))
                {
                    companyId = historyRow.GetValue<string>("company_id");
                }

                List<string> playerIds = historyRow.GetValue<List<string>>("players_ids");
                if (!playerIds.Contains(userId))
                {
                    Log.Error("User does not participate in this challenge: " + userId);
                    response.ErrorCode = Int32.Parse(ErrorCode.ChallengeInvalidPlayer);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    return response;
                }

                string initiatorUserId = playerIds[0];
                string challengedUserId = playerIds[1];

                if (historyRow["completed_on_timestamp"] != null)
                {
                    Log.Error("Challenge already completed: " + challengeId);
                    response.ErrorCode = Int32.Parse(ErrorCode.ChallengeAlreadyCompleted);
                    response.ErrorMessage = ErrorMessage.ChallengeAlreadyCompleted;
                    return response;
                }
                else
                {
                    bool isLive = historyRow.GetValue<bool>("is_live");

                    if (!isLive)
                    {
                        if (userId.Equals(initiatorUserId))
                        {
                            if (historyRow["initiated_user_completed_on_timestamp"] != null && !initiatorUserId.Equals(challengedUserId))
                            {
                                Log.Error("Challenge already completed by initiated user: " + initiatorUserId);
                                response.ErrorCode = Int32.Parse(ErrorCode.ChallengeAlreadyCompleted);
                                response.ErrorMessage = ErrorMessage.ChallengeAlreadyCompleted;
                                return response;
                            }
                        }
                        else if (userId.Equals(challengedUserId))
                        {
                            if (historyRow["challenged_user_completed_on_timestamp"] != null && !initiatorUserId.Equals(challengedUserId))
                            {
                                Log.Error("Challenge already completed by challenged user: " + challengeId);
                                response.ErrorCode = Int32.Parse(ErrorCode.ChallengeAlreadyCompleted);
                                response.ErrorMessage = ErrorMessage.ChallengeAlreadyCompleted;
                                return response;
                            }
                        }
                    }
                }


                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement batchStatement = new BatchStatement();

                DateTimeOffset initiatedUserTimestamp = historyRow["initiated_user_started_on_timestamp"] != null ? historyRow.GetValue<DateTimeOffset>("initiated_user_started_on_timestamp") : currentTime;

                PreparedStatement psChallenge = null;

                if (initiatedUserTimestamp != currentTime)
                {
                    psChallenge = session.Prepare(CQLGenerator.UpdateStatement("challenge_history",
                        new List<string> { "id", "company_id" }, new List<string> { "is_valid", "reason", "invalidated_by_user_id", "invalidated_on_timestamp" }, new List<string>()));
                    batchStatement.Add(psChallenge.Bind(false, reason, userId, currentTime, challengeId, companyId));

                    // Delete request once invalidated
                    PreparedStatement psHistoryByTimestamp = session.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_initiated_timestamp",
                        new List<string> { "challenge_id", "challenged_user_id", "initiated_on_timestamp" }));
                    batchStatement.Add(psHistoryByTimestamp.Bind(challengeId, challengedUserId, initiatedUserTimestamp));

                    // Update end timestamp for match up activity
                    DateTime? startTimestamp = null;

                    if (initiatorUserId.Equals(userId))
                    {
                        startTimestamp = historyRow.GetValue<DateTime?>("initiated_user_started_on_timestamp");
                    }
                    else
                    {
                        startTimestamp = historyRow.GetValue<DateTime?>("challenged_user_started_on_timestamp");
                    }

                    if (startTimestamp != null)
                    {
                        Log.Debug("Invalidate challenge: update matchup activity");
                        string topicId = historyRow.GetValue<string>("topic_id");
                        string topicCategoryId = historyRow.GetValue<string>("topic_category_id");
                        new AnalyticQuiz().UpdateMatchUpActivityLog(challengeId, userId, companyId, topicId, topicCategoryId, startTimestamp.Value, null);
                    }
                }
                else
                {
                    psChallenge = session.Prepare(CQLGenerator.DeleteStatement("challenge_history",
                        new List<string> { "company_id", "id" }));
                    batchStatement.Add(psChallenge.Bind(companyId, challengeId));
                }

                session.Execute(batchStatement);

                User user = new User();
                response.InitiatedUser = user.SelectUserBasic(initiatorUserId, companyId, false, session).User;
                response.ChallengedUser = user.SelectUserBasic(challengedUserId, companyId, false, session).User;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Int32.Parse(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool CheckPrivacyForTopic(string requesterUserId, string companyId, string topicId, ISession session)
        {
            bool isAllowed = false;

            try
            {
                PreparedStatement psTopicPrivacy = session.Prepare(CQLGenerator.SelectStatement("topic_privacy",
                    new List<string>(), new List<string> { "company_id", "topic_id" }));
                Row topicPrivacyRow = session.Execute(psTopicPrivacy.Bind(companyId, topicId)).FirstOrDefault();

                if (topicPrivacyRow != null)
                {
                    bool isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                    bool isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                    bool isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");

                    if (isForEveryone)
                    {
                        isAllowed = true;
                    }
                    else
                    {
                        if (isForDepartment)
                        {
                            Department departmentManager = new Department();
                            List<Department> userDepartments = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;
                            List<Department> topicDepartments = departmentManager.GetAllDepartmentByTopicId(topicId, companyId, session).Departments;
                            List<string> commonList = userDepartments.Select(s1 => s1.Id).ToList().Intersect(topicDepartments.Select(s2 => s2.Id).ToList()).ToList();

                            if (commonList.Count > 0)
                            {
                                isAllowed = true;
                            }
                        }
                        else if (isForUser)
                        {
                            PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_user", new List<string> { }, new List<string> { "topic_id" }));
                            RowSet userByTopicRowset = session.Execute(ps.Bind(topicId));

                            foreach (Row userByTopicRow in userByTopicRowset)
                            {
                                string userId = userByTopicRow.GetValue<string>("user_id");

                                if (userId.Equals(requesterUserId))
                                {
                                    isAllowed = true;
                                    break;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isAllowed;
        }

        public TopicSelectPrivacyResponse SelectPrivacy(string topicId, string companyId, ISession mainSession)
        {
            TopicSelectPrivacyResponse response = new TopicSelectPrivacyResponse();
            response.IsForEveryone = false;
            response.IsForDepartment = false;
            response.IsForUser = false;
            try
            {
                PreparedStatement psTopicPrivacy = mainSession.Prepare(CQLGenerator.SelectStatement("topic_privacy",
                   new List<string>(), new List<string> { "company_id", "topic_id" }));
                Row topicPrivacyRow = mainSession.Execute(psTopicPrivacy.Bind(companyId, topicId)).FirstOrDefault();

                if (topicPrivacyRow != null)
                {
                    bool isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                    bool isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                    bool isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");

                    response.IsForEveryone = isForEveryone;
                    response.IsForDepartment = isForDepartment;
                    response.IsForUser = isForUser;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectMatchUpOverallResponse SelectMatchUpOverall(string adminUserId, string companyId)
        {
            AnalyticSelectMatchUpOverallResponse response = new AnalyticSelectMatchUpOverallResponse();
            response.HeatMap = new AnalyticQuiz.MatchUpHeatmap();
            response.HeatMap.Days = new List<AnalyticQuiz.MatchUpHeatmapPerDay>();
            response.Overview = new AnalyticQuiz.MatchUpOverView();
            response.Dau = new AnalyticQuiz.MatchUpDau();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;

                int totalTopic = 0;
                int totalQuestion = 0;

                TopicCategory categoryManager = new TopicCategory();
                ChallengeQuestion questionManager = new ChallengeQuestion();
                foreach (TopicCategory category in categoryManager.SelectAllCategoriesForDropdown(adminUserId, companyId, true, mainSession).TopicCategories)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("topic_by_category",
                        new List<string>(), new List<string> { "topic_category_id" }));
                    RowSet topicRowset = mainSession.Execute(ps.Bind(category.Id));

                    foreach (Row topicRow in topicRowset)
                    {
                        string topicId = topicRow.GetValue<string>("topic_id");
                        totalTopic++;
                        totalQuestion += questionManager.SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ALL_EXCEPT_DELETE, mainSession);
                    }
                }

                response = new AnalyticQuiz().SelectMatchUpOverall(companyId, totalTopic, totalQuestion, (int)AnalyticQuiz.MatchUpImprovementTimeFrame.ThirtyDays, mainSession, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }

        public AnalyticSelectTopicAttemptResponse SelectTopicAttempts(string adminUserId, string companyId, int metric, int limit)
        {
            AnalyticSelectTopicAttemptResponse response = new AnalyticSelectTopicAttemptResponse();
            response.Topics = new List<AnalyticQuiz.MatchUpSortedTopic>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                response = new AnalyticQuiz().SelectTopicAttempts(companyId, metric, mainSession, limit, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }

        public AnalyticSelectTopicAttemptResponse SelectDailyTopicActivities(string adminUserId, string companyId, string datestampString)
        {
            AnalyticSelectTopicAttemptResponse response = new AnalyticSelectTopicAttemptResponse();
            response.Topics = new List<AnalyticQuiz.MatchUpSortedTopic>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                DateTime date = DateTime.Parse(datestampString);
                response = new AnalyticQuiz().SelectDailyTopicActivities(companyId, date, mainSession, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectTopicOverallResponse SelectTopicOverall(string adminUserId, string companyId, string topicId)
        {
            AnalyticSelectTopicOverallResponse response = new AnalyticSelectTopicOverallResponse();
            response.Analytics = new AnalyticQuiz.TopicAnalytics();
            response.Analytics.Attempts = new List<AnalyticQuiz.TopicAttempt>();
            response.QuestionAttempt = new AnalyticQuiz.QuestionAttempt();
            response.TopicSummary = new AnalyticQuiz.TopicSummary();
            response.Questions = new List<AnalyticQuiz.MatchUpSortedQuestion>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Topic selectedTopic = new Topic().SelectTopicBasic(topicId, adminUserId, companyId, null, null, mainSession).Topic;
                if (selectedTopic == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                response = new AnalyticQuiz().SelectTopicOverall(selectedTopic, companyId, SelectTotalTargetedAudience(topicId, companyId, mainSession), mainSession, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public TopicExportResponse ExportTopic(string managerId, string companyId, string topicId)
        {
            TopicExportResponse response = new TopicExportResponse();
            response.Success = false;

            try
            {
                #region Step 1. Get Topic Detail
                TopicSelectResponse responseTopic = SelectTopicDetail(topicId, managerId, companyId);
                Topic topic = null;
                if (!responseTopic.Success)
                {
                    Log.Error(responseTopic.ErrorMessage);
                    response.ErrorMessage = responseTopic.ErrorMessage;
                    response.ErrorCode = responseTopic.ErrorCode;
                }
                else
                {
                    topic = responseTopic.Topic;
                    topic.Questions = topic.Questions.OrderBy(q => q.CreatedOnTimestamp).ToList();
                }
                #endregion

                // temp folder path format:         root\TempFiles\{GUID}\{Topic.Title}\
                // temp csv file path format:       root\TempFiles\{GUID}\{Topic.Title}\{Topic.Title}.csv
                // temp image files path format:    root\TempFiles\{GUID}\{Topic.Title}\images\{index}.jpg
                // temp zip file path format:       root\TempFiles\{GUID}\{Topic.Title}.zip
                string guid = Guid.NewGuid().ToString().Replace("-", "");
                string tempFolderPath = HttpContext.Current.Server.MapPath("~/") + @"TempFiles\" + guid + @"\";
                string filesFolderPath = tempFolderPath + topic.TopicTitle + @"\";

                #region Step 2. Create CSV file
                string fileName = topic.TopicTitle;
                string fileExtension = ".csv";

                FileStream fs = null;
                string filePath = filesFolderPath + fileName + fileExtension;
                if (!Directory.Exists(filesFolderPath))
                {
                    Directory.CreateDirectory(filesFolderPath);
                }
                fs = File.Create(filePath);

                if (fs != null) // Dispose FileStream
                {
                    fs.Dispose();
                }
                #endregion

                #region Step 3. Write to CSV file.
                StringBuilder strColumn = new StringBuilder();
                StringBuilder strValue = new StringBuilder();
                StreamWriter sw = null;
                sw = new StreamWriter(filePath);

                #region Step 3.1 Topic detail for column
                strColumn.Append("Topic Title");
                strColumn.Append(",");
                strColumn.Append("Topic Description");
                strColumn.Append(",");
                strColumn.Append("Topic Privacy");
                strColumn.Append(",");
                strColumn.Append("Targeted Departments");
                strColumn.Append(",");
                strColumn.Append("No of questions per game");

                sw.WriteLine(strColumn);
                #endregion

                #region Step 3.2 Topic detail for value
                strValue.Append(topic.TopicTitle);
                strValue.Append(",");
                strValue.Append(topic.TopicDescription);
                strValue.Append(",");

                if (topic.IsForEveryone)
                {
                    strValue.Append("For everyone");
                }
                else
                {
                    if (topic.TargetedDepartments.Count > 0)
                    {
                        if (topic.TargetedDepartments.Count == 1)
                        {
                            strValue.Append("For one department only");
                        }
                        else
                        {
                            strValue.Append("For departments only");
                        }
                    }
                }
                strValue.Append(",");

                if (topic.IsForEveryone)
                {
                    strValue.Append("All departments");
                }
                else
                {
                    for (int i = 0; i < topic.TargetedDepartments.Count; i++)
                    {
                        strValue.Append(topic.TargetedDepartments[i].Title);
                        strValue.Append(";");
                    }
                    strValue.Remove(strValue.Length - 1, 1);
                }
                strValue.Append(",");

                strValue.Append(topic.SelectedNumberOfQuestions);

                sw.WriteLine(strValue);

                sw.WriteLine();
                #endregion

                #region Step 3.3 Question detail for column
                strColumn.Clear();
                strColumn.Append("Question Type");
                strColumn.Append(",");
                strColumn.Append("Question Content");
                strColumn.Append(",");
                strColumn.Append("Choice Type");
                strColumn.Append(",");
                strColumn.Append("Correct Answer");
                strColumn.Append(",");
                strColumn.Append("Second Answer");
                strColumn.Append(",");
                strColumn.Append("Third Answer");
                strColumn.Append(",");
                strColumn.Append("Fourth Answer");
                strColumn.Append(",");
                strColumn.Append("Reading Time");
                strColumn.Append(",");
                strColumn.Append("Answering Time");
                strColumn.Append(",");
                strColumn.Append("Difficulty level");
                strColumn.Append(",");
                strColumn.Append("Score Multiplier");
                strColumn.Append(",");
                strColumn.Append("Frequency");
                sw.WriteLine(strColumn);
                #endregion

                #region Step 3.4 Question detail for value
                for (int i = 0; i < topic.Questions.Count; i++)
                {
                    strValue.Clear();
                    strValue.Append(topic.Questions[i].QuestionType);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].Content);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].ChoiceType);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].FirstChoice);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].SecondChoice);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].ThirdChoice);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].FourthChoice);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].TimeAssignedForReading);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].TimeAssignedForAnswering);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].DifficultyLevel);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].ScoreMultiplier);
                    strValue.Append(",");
                    strValue.Append(topic.Questions[i].Frequency);
                    sw.WriteLine(strValue);
                }

                if (sw != null)
                {
                    sw.Dispose();
                }
                #endregion

                #endregion

                #region Step 4. Download image of questions
                string imgFolderPath = filesFolderPath + @"images\";
                fileName = "";
                if (!Directory.Exists(imgFolderPath))
                {
                    Directory.CreateDirectory(imgFolderPath);
                }

                System.Drawing.Image image = null;
                fileExtension = ".jpg";
                for (int i = 0; i < topic.Questions.Count; i++)
                {
                    if (topic.Questions[i].QuestionType == ChallengeQuestion.QUESTION_TYPE_PICTURE && !string.IsNullOrEmpty(topic.Questions[i].ContentImageUrl))
                    {
                        image = FileDownloader.GetImageFromURL(topic.Questions[i].ContentImageUrl);
                        fileName = Convert.ToString(i + 1);
                        filePath = imgFolderPath + fileName + fileExtension;
                        image.Save(filePath);
                    }
                }
                #endregion

                #region Step 5. Zip folder
                fileExtension = ".zip";
                filePath = tempFolderPath + topic.TopicTitle + fileExtension;
                ZipFile.CreateFromDirectory(filesFolderPath, filePath);
                #endregion

                #region Step 6. Convet zip file to StreamContent
                fs = new FileStream(filePath, FileMode.Open);
                Byte[] data = new Byte[fs.Length];
                fs.Read(data, 0, data.Length);
                fs.Close();
                MemoryStream ms = new MemoryStream(data);
                response.ZipFileStream = new System.Net.Http.StreamContent(ms);
                #endregion

                #region Step 7. Delete temp files
                Directory.Delete(tempFolderPath, true);
                #endregion
                response.FileFullName = topic.TopicTitle + fileExtension;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MatchUpImportQuestionsResponse ImportTopicQuestions(string managerId, string companyId, string topidId, string categoryId, string zipFileBase64String)
        {
            MatchUpImportQuestionsResponse response = new MatchUpImportQuestionsResponse();
            response.Success = true;
            try
            {
                // temp zip file path format:       root\TempFiles\{GUID}\{Topic.Id}.zip
                // temp folder path format:         root\TempFiles\{GUID}\{Topic.Id}\
                // temp csv file path format:       root\TempFiles\{GUID}\{Topic.Id}\{Topic.Title}.csv
                // temp image files path format:    root\TempFiles\{GUID}\{Topic.Id}\images\{index}.jpg

                string guid = Guid.NewGuid().ToString().Replace("-", "");
                string tempFolderPath = HttpContext.Current.Server.MapPath("~/") + @"TempFiles\" + guid + @"\";
                string filesFolderPath = tempFolderPath + topidId + @"\";
                string fileName = "";
                string fileExtension = "";
                string filePath = "";
                string imgBase64String = string.Empty;
                string imgFormat = string.Empty;
                int imgWidth = 0;
                int imgHeight = 0;
                StringBuilder sbMD5 = new StringBuilder();

                #region Step 1. Save zip file to server.
                if (!Directory.Exists(tempFolderPath))
                {
                    Directory.CreateDirectory(tempFolderPath);
                }

                fileName = topidId;
                fileExtension = ".zip";
                byte[] zipFileBytes = Convert.FromBase64String(zipFileBase64String);
                filePath = tempFolderPath + fileName + fileExtension;
                File.WriteAllBytes(filePath, zipFileBytes);
                #endregion

                #region Step 2. Unzip file.
                if (!Directory.Exists(filesFolderPath))
                {
                    Directory.CreateDirectory(filesFolderPath);
                }
                ZipFile.ExtractToDirectory(filePath, filesFolderPath);
                #endregion

                #region Step 3. Read CSV file. Bulid List<ChallengeQuestion> object and check data. 
                List<ChallengeQuestion> questions = new List<ChallengeQuestion>();
                string[] fileList = Directory.GetFiles(filesFolderPath, "*.csv");
                if (fileList.Length == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.ChallengeImportFileError);
                    response.ErrorMessage = ErrorMessage.ChallengeImportFileError;
                    return response;
                }
                filePath = fileList[0];

                using (TextFieldParser parser = new TextFieldParser(filePath))
                {
                    // image path list
                    fileList = Directory.GetFiles(filesFolderPath + @"images\");

                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    int rowIndex = 1, questionIndex = 1;
                    while (!parser.EndOfData)
                    {
                        string[] fields = parser.ReadFields();

                        if (rowIndex > 3)
                        {
                            // When QuestionType is QUESTION_TYPE_PICTURE, need to read and convert image to base64 string.
                            imgBase64String = string.Empty;

                            if (Convert.ToInt16(fields[0]) == ChallengeQuestion.QUESTION_TYPE_PICTURE)
                            {
                                for (int i = 0; i < fileList.Length; i++)
                                {
                                    if (fileList[i].Contains(@"\" + questionIndex + ".")) // 檢查 image 的 path 是否有 "\{questionIndex}."
                                    {
                                        using (System.Drawing.Image image = System.Drawing.Image.FromFile(fileList[i]))
                                        {
                                            using (MemoryStream m = new MemoryStream())
                                            {
                                                image.Save(m, ImageHelper.GetImageFormat(image));
                                                byte[] imageBytes = m.ToArray();
                                                imgBase64String = Convert.ToBase64String(imageBytes);
                                                imgFormat = fileList[i].Substring(fileList[i].IndexOf('.') + 1);
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

                            ChallengeQuestion question = new ChallengeQuestion().BuildQuestionForImport(Convert.ToInt16(fields[(int)TopicCSVFieldTypeEnum.QuestionType]),
                                fields[(int)TopicCSVFieldTypeEnum.Content],
                                imgBase64String,
                                imgFormat,
                                1, //Convert.ToInt16(fields[(int)TopicCSVFieldTypeEnum.ChoiceType]), // Hard Code
                                fields[(int)TopicCSVFieldTypeEnum.CorrectAnswer],
                                fields[(int)TopicCSVFieldTypeEnum.SecondAnswer],
                                fields[(int)TopicCSVFieldTypeEnum.ThirdAnswer],
                                fields[(int)TopicCSVFieldTypeEnum.FourthAnswer],
                                Convert.ToSingle(fields[(int)TopicCSVFieldTypeEnum.ReadingTime]),
                                Convert.ToSingle(fields[(int)TopicCSVFieldTypeEnum.AnsweringTime]),
                                Convert.ToInt16(fields[(int)TopicCSVFieldTypeEnum.DifficultyLevel]),
                                Convert.ToInt16(fields[(int)TopicCSVFieldTypeEnum.ScoreMultiplier]),
                                1 //Convert.ToDouble(fields[(int)TopicCSVFieldTypeEnum.Frequency]) // Hard Code
                                );

                            questions.Add(question);
                            if (question.IsInputDataValid == false)
                            {
                                response.Success = false;
                            }
                            questionIndex++;
                        }
                        rowIndex++;
                    }
                }

                response.Questions = questions;

                if (!response.Success)
                {
                    Log.Error(ErrorMessage.ChallengeImportInputError);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.ChallengeImportInputError);
                    response.ErrorMessage = ErrorMessage.ChallengeImportInputError;
                    return response;
                }
                #endregion

                #region  Step 4. Upload question's image to S3.

                #region Step 4.1 Get AWS Cognito token and RoleArn from WebAPI server.
                GetCognitoTokenRequest request = new GetCognitoTokenRequest();
                request.CompanyId = companyId;
                request.UserId = managerId;
                byte[] postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(request));
                HttpWebRequest httpRequest = HttpWebRequest.Create(WebConfigurationManager.AppSettings["WebAPI_Url_GetCognitoToken"].ToString()) as HttpWebRequest;
                httpRequest.Method = "POST";
                httpRequest.Timeout = 30000;
                httpRequest.ContentType = "application/json";
                httpRequest.ContentLength = postData.Length;
                using (Stream st = httpRequest.GetRequestStream())
                {
                    st.Write(postData, 0, postData.Length);
                }
                string result = "";
                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }

                GetCognitoTokenResponse responseGetCognitoToken = JsonConvert.DeserializeObject<GetCognitoTokenResponse>(result);
                #endregion

                #region Step 4.2. Create IAmazonS3 by Cognito token and RoleArn.
                AnonymousAWSCredentials cred = new AnonymousAWSCredentials();
                AmazonSecurityTokenServiceClient stsClient = new AmazonSecurityTokenServiceClient(cred, RegionEndpoint.APSoutheast1);
                AssumeRoleWithWebIdentityRequest stsReq = new AssumeRoleWithWebIdentityRequest();
                stsReq.RoleArn = responseGetCognitoToken.RoleArn;
                stsReq.RoleSessionName = "AdminWebsiteSession";
                stsReq.WebIdentityToken = responseGetCognitoToken.Token;
                AssumeRoleWithWebIdentityResponse stsResp = stsClient.AssumeRoleWithWebIdentity(stsReq);
                IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(stsResp.Credentials, RegionEndpoint.APSoutheast1);
                #endregion

                for (int i = 0; i < questions.Count; i++)
                {
                    if (questions[i].QuestionType == (int)ChallengeQuestion.QUESTION_TYPE_PICTURE)
                    {
                        if (string.IsNullOrEmpty(questions[i].ContentImgBase64String))
                        {
                            Log.Error(ErrorMessage.ChallengeImageIsInvalid);
                            response.Success = false;
                            response.ErrorCode = Convert.ToInt32(ErrorCode.ChallengeImageIsInvalid);
                            response.ErrorMessage = ErrorMessage.ChallengeImageIsInvalid;
                            return response;
                        }

                        #region Step 4.3. Checking if the folder exists. (/cocadre-{CompanyId}/topics/{TopicId}/{QuestionId}/{yyyyMMddHHmmssfff.jpg})
                        String bucketName = "cocadre-" + companyId.ToLower();
                        String folderName = "topics/" + topidId + "/" + questions[i].Id;

                        ListObjectsRequest findFolderRequest = new ListObjectsRequest();
                        findFolderRequest.BucketName = bucketName;
                        findFolderRequest.Delimiter = "/";
                        findFolderRequest.Prefix = folderName;
                        ListObjectsResponse findFolderResponse = s3Client.ListObjects(findFolderRequest);
                        List<String> commonPrefixes = findFolderResponse.CommonPrefixes;
                        Boolean folderExists = commonPrefixes.Any();
                        #endregion

                        #region Step 4.4. 如果沒有資料夾，則建立資料夾。
                        if (!folderExists)
                        {
                            PutObjectRequest folderRequest = new PutObjectRequest();

                            folderRequest.BucketName = bucketName;
                            String folderKey = folderName + "/";
                            folderRequest.Key = folderKey;
                            folderRequest.InputStream = new MemoryStream(new byte[0]);
                            PutObjectResponse folderResponse = s3Client.PutObject(folderRequest);
                        }
                        #endregion

                        #region Step 4.5. 上傳原始圖檔。
                        imgBase64String = questions[i].ContentImgBase64String.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");

                        string dummyData = imgBase64String.Trim().Replace("%", "").Replace(",", "").Replace(" ", "+");
                        if (dummyData.Length % 4 > 0)
                        {
                            dummyData = dummyData.PadRight(dummyData.Length + 4 - dummyData.Length % 4, '=');
                        }
                        imgBase64String = dummyData;

                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        MemoryStream ms = new MemoryStream(imgBytes);
                        System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                        imgWidth = originImage.Width;
                        imgHeight = originImage.Height;
                        String imgfileName = PROCESS_TIMESTAMP + "_original" + "." + imgFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(imgWidth));
                        metadatas.Add("Height", Convert.ToString(imgHeight));
                        S3Helper.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, imgfileName, metadatas);
                        #endregion

                        #region Step 4.6. 如果原圖比 1920 * 1920 還大，就做 1920 * 1920 的縮圖。否則直接存原圖。
                        System.Drawing.Image newImage;
                        if (imgWidth > 1920 || imgHeight > 1920)
                        {
                            newImage = ImageHelper.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageHelper.GetImageFormat(originImage));
                            imgfileName = PROCESS_TIMESTAMP + "_large" + "." + imgFormat.ToLower();
                            metadatas.Clear();
                            metadatas.Add("Width", Convert.ToString(newImage.Width));
                            metadatas.Add("Height", Convert.ToString(newImage.Height));
                            S3Helper.UploadImageToS3(s3Client, ImageHelper.ImageToByteArray(newImage, ImageHelper.GetImageFormat(originImage)), bucketName, folderName, imgfileName, metadatas);
                        }
                        else
                        {
                            imgfileName = PROCESS_TIMESTAMP + "_large" + "." + imgFormat.ToLower();
                            S3Helper.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, imgfileName, metadatas);
                        }
                        #endregion

                        #region Step 4.7. 如果原圖比 1080 * 1080 還大，就做 1080 * 1080 的縮圖。否則直接存原圖。
                        if (imgWidth > 1080 || imgHeight > 1080)
                        {
                            newImage = ImageHelper.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageHelper.GetImageFormat(originImage));
                            imgfileName = PROCESS_TIMESTAMP + "_medium" + "." + imgFormat.ToLower();
                            metadatas.Clear();
                            metadatas.Add("Width", Convert.ToString(newImage.Width));
                            metadatas.Add("Height", Convert.ToString(newImage.Height));
                            S3Helper.UploadImageToS3(s3Client, ImageHelper.ImageToByteArray(newImage, ImageHelper.GetImageFormat(originImage)), bucketName, folderName, imgfileName, metadatas);
                        }
                        else
                        {
                            imgfileName = PROCESS_TIMESTAMP + "_medium" + "." + imgFormat.ToLower();
                            S3Helper.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, imgfileName, metadatas);
                        }
                        #endregion

                        #region Step 4.8. 如果原圖比 540 * 540 還大，就做 540 * 540 的縮圖。否則直接存原圖。
                        if (imgWidth > 540 || imgHeight > 540)
                        {
                            newImage = ImageHelper.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageHelper.GetImageFormat(originImage));
                            imgfileName = PROCESS_TIMESTAMP + "_small" + "." + imgFormat.ToLower();
                            metadatas.Clear();
                            metadatas.Add("Width", Convert.ToString(newImage.Width));
                            metadatas.Add("Height", Convert.ToString(newImage.Height));
                            S3Helper.UploadImageToS3(s3Client, ImageHelper.ImageToByteArray(newImage, ImageHelper.GetImageFormat(originImage)), bucketName, folderName, imgfileName, metadatas);
                        }
                        else
                        {
                            imgfileName = PROCESS_TIMESTAMP + "_small" + "." + imgFormat.ToLower();
                            S3Helper.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        }


                        #region Step 4.9. Get profile image url
                        String imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + PROCESS_TIMESTAMP + "_original." + imgFormat.ToLower();
                        #endregion

                        #region Step 4.10 Get
                        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                        byte[] hash = md5.ComputeHash(imgBytes);
                        sbMD5.Clear();
                        for (int k = 0; k < hash.Length; k++)
                        {
                            sbMD5.Append(hash[k].ToString("X2"));
                        }
                        #endregion

                        questions[i].ContentImageUrl = imageUrl;
                        questions[i].ContentImageMd5 = sbMD5.ToString();
                    }
                    #endregion

                }
                #endregion

                #region Step 5. Write question to database.
                DateTime currentDateTime = DateTime.UtcNow;
                for (int i = 0; i < questions.Count; i++)
                {
                    QuestionCreateResponse responseCreateQuestion = new ChallengeQuestion().CreateNewQuestion(managerId,
                        companyId,
                        questions[i].Id,
                        topidId,
                        categoryId,
                        questions[i].QuestionType,
                        questions[i].Content,
                        questions[i].ContentImageUrl,
                        questions[i].ContentImageMd5,
                        questions[i].ContentImageBackgroundColorCode,
                        questions[i].ChoiceType,
                        questions[i].FirstChoice,
                        questions[i].FirstChoiceContentImageUrl,
                        questions[i].FirstChoiceContentImageMd5,
                        questions[i].SecondChoice,
                        questions[i].SecondChoiceContentImageUrl,
                        questions[i].SecondChoiceContentImageMd5,
                        questions[i].ThirdChoice,
                        questions[i].ThirdChoiceContentImageUrl,
                        questions[i].ThirdChoiceContentImageMd5,
                        questions[i].FourthChoice,
                        questions[i].FourthChoiceContentImageUrl,
                        questions[i].FourthChoiceContentImageMd5,
                        questions[i].TimeAssignedForReading,
                        questions[i].TimeAssignedForAnswering,
                        questions[i].DifficultyLevel,
                        questions[i].ScoreMultiplier,
                        questions[i].Frequency,
                        questions[i].Status,
                        currentDateTime.AddMilliseconds(i)
                        );

                    if (!responseCreateQuestion.Success)
                    {
                        response.Success = false;
                        response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                        response.ErrorMessage = ErrorMessage.SystemError;
                        break;
                    }
                }
                #endregion

                #region Step 6. Delete temp files.
                Directory.Delete(tempFolderPath, true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private int SelectTotalTargetedAudience(string topicId, string companyId, ISession mainSession)
        {
            int totalTargetedAudience = 0;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("topic_privacy",
                            new List<string>(), new List<string> { "company_id", "topic_id" }));
                Row privacyRowset = mainSession.Execute(ps.Bind(companyId, topicId)).FirstOrDefault();

                bool isForEveryone = privacyRowset.GetValue<bool>("is_for_everyone");
                bool isForDepartment = privacyRowset.GetValue<bool>("is_for_department");
                bool isForUser = privacyRowset.GetValue<bool>("is_for_user");

                List<User> targetedAudience = new List<User>();

                User userManager = new User();
                if (isForEveryone)
                {
                    targetedAudience = userManager.GetAllUserForAdmin(null, companyId, string.Empty, 0, User.AccountStatus.CODE_ACTIVE, new List<string>(), false, false, null, mainSession).Users;
                }
                else
                {
                    List<string> targetedUserIds = new List<string>();

                    if (isForDepartment)
                    {
                        List<string> departmentIds = new List<string>();
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("topic_targeted_department", new List<string>(), new List<string> { "topic_id" }));
                        RowSet departmentRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row departmentRow in departmentRowset)
                        {
                            string departmentId = departmentRow.GetValue<string>("department_id");
                            departmentIds.Add(departmentId);
                        }

                        targetedUserIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, null, companyId, mainSession);
                    }

                    if (isForUser)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("topic_targeted_user", new List<string>(), new List<string> { "topic_id" }));
                        RowSet userRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row userRow in userRowset)
                        {
                            string userId = userRow.GetValue<string>("user_id");
                            if (!targetedUserIds.Contains(userId))
                            {
                                targetedUserIds.Add(userId);
                            }
                        }
                    }

                    foreach (string targetedUserId in targetedUserIds)
                    {
                        User targetedUser = userManager.SelectUserBasic(targetedUserId, companyId, false, mainSession, null, true).User;
                        if (targetedUser != null)
                        {
                            targetedAudience.Add(targetedUser);
                        }
                    }
                }

                totalTargetedAudience = targetedAudience.Count();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return totalTargetedAudience;
        }


    }
}