﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using Amazon.S3;
using Amazon;
using System.Web.Configuration;
using Amazon.Runtime;
using Amazon.S3.Model;
using Cassandra.Mapping;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class Company
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum CompanyJobQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [Serializable]
        public class PermissionType
        {
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ADMIN_ONLY = 1;

            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ADMIN_AND_MODERATER = 2;

            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ALL_PERSONNEL = 3;

            [DataMember(EmitDefaultValue = false)]
            public const int CODE_DEPARTMENT = 4;

            [DataMember(EmitDefaultValue = false)]
            public int Code { get; private set; }

            [DataMember(EmitDefaultValue = false)]
            public string Title { get; private set; }

            public PermissionType(int code)
            {
                switch (code)
                {
                    case CODE_ADMIN_ONLY:
                        Code = CODE_ADMIN_ONLY;
                        Title = "Admin Only";
                        break;
                    case CODE_ADMIN_AND_MODERATER:
                        Code = CODE_ADMIN_AND_MODERATER;
                        Title = "Admin & Moderater";
                        break;
                    case CODE_ALL_PERSONNEL:
                        Code = CODE_ALL_PERSONNEL;
                        Title = "All Personnel";
                        break;
                    case CODE_DEPARTMENT:
                        Code = CODE_DEPARTMENT;
                        Title = "Selected Department Only";
                        break;

                    default:
                        break;
                }
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyLogoUrl { get; set; }

        // Website
        [DataMember(EmitDefaultValue = false)]
        public string AdminImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PrimaryManagerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AdminProfileImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SecondaryEmail { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SupportMessage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CountryTimeZone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ZoneName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public double TimeZoneOffset { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AdminWebsiteHeaderLogoUrl { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public string EmailLogoUrl { get; set; }


        // Client
        [DataMember(Name = "CompanyBannerUrl", EmitDefaultValue = false)]
        public string CompanyBannerUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyPullRefreshBannerUrl { get; set; }

        [DataMember(Name = "MatchupBannerUrl", EmitDefaultValue = false)]
        public string MatchupBannerUrl { get; set; }

        [DataMember(Name = "ProfilePopupBannerUrl", EmitDefaultValue = false)]
        public string ProfilePopupBannerUrl { get; set; }

        [DataMember]
        public bool IsPasswordDefault { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User Manager { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PermissionType CompanyPermissionType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> CompanyPermissionDepartments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ResetPasswordUrl { get; set; }

        #region ForLogin
        [DataMember]
        public int NumberOfNotifications { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UserId { get; set; }
        #endregion

        [Serializable]
        public enum CompanyImageType
        {

            [EnumMember]
            AdminProfile = 1,

            [EnumMember]
            EmailSquare = 2,

            [EnumMember]
            WebsiteHeader = 3,

            [EnumMember]
            LoginSelection = 4,

            [EnumMember]
            ClientBanner = 5,

            [EnumMember]
            ClientPullRefresh = 6,

            [EnumMember]
            ClientMatchUp = 7,

            [EnumMember]
            ClientPopUp = 8,
        }

        [Serializable]
        public enum CompanyEmailTemplate
        {

            [EnumMember]
            PersonnelInvite = 1,

            [EnumMember]
            AdminInvite = 2,

            [EnumMember]
            ForgotPassword = 3,

            [EnumMember]
            ResettedPassword = 4,

            [EnumMember]
            Live360Report = 5
        }

        [Serializable]
        public class CompanyJob
        {
            [DataMember]
            public string JobId { get; set; }
            [DataMember]
            public int Level { get; set; }
            [DataMember]
            public string Title { get; set; }
            [DataMember]
            public int NumberOfPersonnels { get; set; }
        }

        public CompanyImageResponse CreateCompanyImage(
            string companyId,
            string url,
            int companyImageType,
            string adminUserId,
            //string prevSelectedCompanyImageId,
            bool isSelected = true)
        {
            CompanyImageResponse companyImageResponse = new CompanyImageResponse();
            companyImageResponse.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                IMapper mapper = new Mapper(session);

                #region Verify data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    companyImageResponse.ErrorCode = es.ErrorCode;
                    companyImageResponse.ErrorMessage = es.ErrorMessage;
                    return companyImageResponse;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    companyImageResponse.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return companyImageResponse;
                }
                #endregion

                #region Get in use image
                string prevSelectedCompanyImageId = string.Empty;
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company_image", new List<string> { }, new List<string> { "company_id", "company_image_type" }));
                BoundStatement bs = ps.Bind(companyId, companyImageType);
                RowSet rs = session.Execute(bs);
                foreach (Row r in rs)
                {
                    if (r.GetValue<bool>("is_selected"))
                    {
                        prevSelectedCompanyImageId = r.GetValue<string>("company_image_id");
                        break;
                    }
                }
                #endregion


                if (!String.IsNullOrEmpty(prevSelectedCompanyImageId))
                {
                    var selectedCompanyImage = mapper.Single<CompanyImage>("WHERE company_id = ? AND company_image_id = ?", companyId, prevSelectedCompanyImageId);

                    // if exists, set previously selected company image to be isSelected = false
                    mapper.Update<CompanyImage>(
                        "SET is_selected = ? WHERE company_id = ? AND company_image_id = ?", false, companyId,
                        prevSelectedCompanyImageId);
                    mapper.Update<CompanyImageDesc>(
                        "SET is_selected = ? WHERE company_id = ? AND created_on_timestamp = ? AND company_image_id = ?", false, companyId, selectedCompanyImage.CreatedOnTimestamp,
                        prevSelectedCompanyImageId);
                }

                // create new company image
                String companyImageId = UUIDGenerator.GenerateUniqueIDForCompanyMedia();

                DateTime dtNoW = DateTime.UtcNow;

                var newCompanyImage = new CompanyImage
                {
                    CompanyId = companyId,
                    CompanyImageId = companyImageId,
                    CompanyImageType = companyImageType,
                    IsSelected = isSelected,
                    Url = url,
                    CreatedByUserId = adminUserId,
                    CreatedOnTimestamp = dtNoW,
                    LastModifiedByUserId = adminUserId,
                    LastModifiedTimestamp = dtNoW
                };

                var newCompanyImageDesc = new CompanyImageDesc
                {
                    CompanyId = companyId,
                    CompanyImageId = companyImageId,
                    CompanyImageType = companyImageType,
                    IsSelected = isSelected,
                    Url = url,
                    CreatedByUserId = adminUserId,
                    CreatedOnTimestamp = dtNoW,
                    LastModifiedByUserId = adminUserId,
                    LastModifiedTimestamp = dtNoW
                };

                mapper.Insert<CompanyImage>(newCompanyImage);
                mapper.Insert<CompanyImageDesc>(newCompanyImageDesc);

                companyImageResponse.CompanyImage = newCompanyImage;

                // update company table but don't delete from s3 the prev image
                UpdateImage(adminUserId, companyId, newCompanyImage.Url, newCompanyImage.CompanyImageType, false);

                companyImageResponse.Success = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());

                Log.Error(ex.ToString(), ex);
                companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                companyImageResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return companyImageResponse;
        }

        public CompanyImageResponse SetSelectedCompanyImage(string companyId, string companyImageId, string adminUserId, int companyImageType, bool isSelected = true, bool deletePrev = false)
        {
            CompanyImageResponse companyImageResponse = new CompanyImageResponse();
            companyImageResponse.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                IMapper mapper = new Mapper(session);

                #region Verify Data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    companyImageResponse.ErrorCode = es.ErrorCode;
                    companyImageResponse.ErrorMessage = es.ErrorMessage;
                    return companyImageResponse;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    companyImageResponse.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return companyImageResponse;
                }
                #endregion

                #region Get in use image
                string prevSelectedCompanyImageId = string.Empty;
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company_image", new List<string> { }, new List<string> { "company_id", "company_image_type" }));
                BoundStatement bs = ps.Bind(companyId, companyImageType);
                RowSet rs = session.Execute(bs);
                foreach (Row r in rs)
                {
                    if (r.GetValue<bool>("is_selected"))
                    {
                        prevSelectedCompanyImageId = r.GetValue<string>("company_image_id");
                        break;
                    }
                }
                #endregion

                var companyImage = mapper.Single<CompanyImage>("WHERE company_id = ? AND company_image_id = ?",
                    companyId, companyImageId);

                if (companyImage != null)
                {
                    if (!String.IsNullOrEmpty(prevSelectedCompanyImageId))
                    {
                        var selectedCompanyImage = mapper.Single<CompanyImage>("WHERE company_id = ? AND company_image_id = ?", companyId, prevSelectedCompanyImageId);

                        // if exists, set previously selected company image to be isSelected = false
                        mapper.Update<CompanyImage>(
                            "SET is_selected = ? WHERE company_id = ? AND company_image_id = ?", false, companyId,
                            prevSelectedCompanyImageId);

                        mapper.Update<CompanyImageDesc>(
                            "SET is_selected = ? WHERE company_id = ? AND created_on_timestamp = ? AND company_image_id = ?", false, companyId, selectedCompanyImage.CreatedOnTimestamp,
                            prevSelectedCompanyImageId);
                    }

                    // start the update
                    mapper.Update<CompanyImage>(
                        "SET is_selected = ?, last_modified_by_user_id = ?, last_modified_timestamp = ? WHERE company_id = ? AND company_image_id = ?", true, adminUserId, DateTime.UtcNow, companyId,
                        companyImageId);
                    // set the same for companyImageDesc
                    mapper.Update<CompanyImageDesc>(
                        "SET is_selected = ?, last_modified_by_user_id = ?, last_modified_timestamp = ? WHERE company_id = ? AND created_on_timestamp = ? AND company_image_id = ?", true, adminUserId, DateTime.UtcNow, companyId, companyImage.CreatedOnTimestamp,
                        companyImageId);

                    companyImageResponse.CompanyImage = companyImage;

                    // update company table but don't delete from s3 the prev image
                    UpdateImage(adminUserId, companyId, companyImage.Url, companyImage.CompanyImageType, deletePrev);

                    companyImageResponse.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    companyImageResponse.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());

                Log.Error(ex.ToString(), ex);
                companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                companyImageResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return companyImageResponse;
        }

        public CompanyImageResponse DeleteCompanyImage(string companyId, string companyImageId, string adminUserId, int companyImageType)
        {
            CompanyImageResponse companyImageResponse = new CompanyImageResponse();
            companyImageResponse.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                IMapper mapper = new Mapper(session);

                #region Verify data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    companyImageResponse.ErrorCode = es.ErrorCode;
                    companyImageResponse.ErrorMessage = es.ErrorMessage;
                    return companyImageResponse;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    companyImageResponse.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return companyImageResponse;
                }
                #endregion

                var companyImage = mapper.Single<CompanyImage>("WHERE company_id = ? AND company_image_id = ?",
                    companyId, companyImageId);

                if (companyImage != null)
                {
                    mapper.Delete<CompanyImageDesc>(
                        "WHERE company_id = ? AND created_on_timestamp = ? AND company_image_id = ?",
                        companyImage.CompanyId,
                        companyImage.CreatedOnTimestamp, companyImage.CompanyImageId);
                    mapper.Delete(companyImage);

                    companyImageResponse.CompanyImage = companyImage;

                    // check if there's any images of this type left
                    CompanyImageDesc companyImageDesc = mapper.FirstOrDefault<CompanyImageDesc>(
                        "WHERE company_id = ? AND company_image_type = ?", companyId,
                        companyImage.CompanyImageType);

                    if (companyImageDesc != null)
                    {
                        if (companyImage.IsSelected)
                        {
                            // yes: set latest one to current dp
                            SetSelectedCompanyImage(companyId, companyImageDesc.CompanyImageId, adminUserId, companyImageType, true, true);
                        }
                        else
                        {
                            UpdateImage(adminUserId, companyId, companyImageDesc.Url, companyImageDesc.CompanyImageType, false); // update company table
                            #region Delete file on S3
                            String bucketName = "cocadre-" + companyId.ToLower();
                            using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                            {
                                string path = companyImage.Url.Replace("https://", "");
                                string[] splitPath = path.Split('/');
                                string imageFolder = splitPath[splitPath.Count() - 2];
                                string imageName = splitPath[splitPath.Count() - 1];

                                #region v1.0.0
                                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "company/" + imageName
                                };
                                s3Client.DeleteObject(deleteObjectRequest);
                                #endregion

                                #region v2.0.0
                                // _original
                                deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "company/" + imageFolder + "/" + imageName
                                };
                                s3Client.DeleteObject(deleteObjectRequest);

                                // _large
                                deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "company/" + imageFolder + "/" + imageName.Replace("_original", "_large")
                                };
                                s3Client.DeleteObject(deleteObjectRequest);

                                // _medium
                                deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "company/" + imageFolder + "/" + imageName.Replace("_original", "_medium")
                                };
                                s3Client.DeleteObject(deleteObjectRequest);

                                // _small
                                deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "company/" + imageFolder + "/" + imageName.Replace("_original", "_small")
                                };
                                s3Client.DeleteObject(deleteObjectRequest);
                                #endregion
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        // no: set default as current dp
                        // update company table with default dp and delete from s3 the prev image
                        UpdateImage(adminUserId, companyId, null, companyImage.CompanyImageType);
                    }

                    companyImageResponse.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    companyImageResponse.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                companyImageResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                companyImageResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return companyImageResponse;
        }

        public CompanyImagesResponse GetCompanyImagesDesc(string companyId, string adminUserId, int companyImageType, int limit, DateTime? timestampValue, bool isNextPage = true)
        {
            CompanyImagesResponse companyImagesResponse = new CompanyImagesResponse();
            companyImagesResponse.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                IMapper mapper = new Mapper(session);

                #region Verify data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    companyImagesResponse.ErrorCode = es.ErrorCode;
                    companyImagesResponse.ErrorMessage = es.ErrorMessage;
                    return companyImagesResponse;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    companyImagesResponse.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    companyImagesResponse.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return companyImagesResponse;
                }
                #endregion

                IEnumerable<CompanyImageDesc> companyImageDescs;

                if (isNextPage)
                {
                    companyImageDescs =
                         mapper.Fetch<CompanyImageDesc>(
                             "WHERE company_id = ? AND company_image_type = ? AND created_on_timestamp > ? LIMIT ?", companyId,
                             companyImageType, timestampValue, limit);
                }
                else
                {
                    companyImageDescs =
                        mapper.Fetch<CompanyImageDesc>(
                            "WHERE company_id = ? AND company_image_type = ? AND created_on_timestamp < ? LIMIT ?", companyId,
                            companyImageType, timestampValue, limit);

                }

                companyImagesResponse.CompanyImages = companyImageDescs;
                companyImagesResponse.Success = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Log.Error(ex.ToString(), ex);
                companyImagesResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                companyImagesResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return companyImagesResponse;
        }

        public GetCompanyImageDetailResponse GetCompanyImageDetail(string companyId, string adminUserId, string imageId, int companyImageType)
        {
            GetCompanyImageDetailResponse response = new GetCompanyImageDetailResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                IMapper mapper = new Mapper(session);
                List<CompanyImage> images = new List<CompanyImage>();

                #region Verify data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }
                #endregion

                #region Get the in use image
                CompanyImage inUseImage = mapper.Single<CompanyImage>("WHERE company_id = ? AND company_image_type = ? AND is_selected = ? ALLOW FILTERING",
                  companyId,
                  companyImageType,
                  true);

                if (inUseImage == null)
                {
                    response.CurrentImage = null;
                    response.PreviousImage = null;
                    response.NextImage = null;
                    response.Success = true;
                    return response;
                }

                images.Add(inUseImage);
                #endregion

                #region others images
                List<CompanyImageDesc> otherImages = mapper.Fetch<CompanyImageDesc>("WHERE company_id = ? AND company_image_type = ?",
                    companyId,
                    companyImageType
                    ).ToList();

                for (int i = 0; i < otherImages.Count; i++)
                {
                    if (inUseImage.CompanyImageId != otherImages[i].CompanyImageId)
                    {
                        images.Add(new CompanyImage
                        {
                            CompanyId = otherImages[i].CompanyId,
                            CompanyImageId = otherImages[i].CompanyImageId,
                            CompanyImageType = otherImages[i].CompanyImageType,
                            CreatedByUserId = otherImages[i].CreatedByUserId,
                            CreatedOnTimestamp = otherImages[i].CreatedOnTimestamp,
                            IsSelected = otherImages[i].IsSelected,
                            LastModifiedByUserId = otherImages[i].LastModifiedByUserId,
                            LastModifiedTimestamp = otherImages[i].LastModifiedTimestamp,
                            Url = otherImages[i].Url
                        });
                    }
                }
                #endregion

                #region Get image
                int index = images.FindIndex(i => i.CompanyImageId == imageId);

                response.CurrentImage = images[index];

                if (index == 0) // current is in use image
                {
                    response.PreviousImage = null;
                    if (images.Count > 1)
                    {
                        response.NextImage = images[index + 1];
                    }
                    else
                    {
                        response.NextImage = null;
                    }
                }
                else // current is not in use image
                {
                    response.PreviousImage = images[index - 1];
                    if (index == images.Count - 1)
                    {
                        response.NextImage = null;
                    }
                    else
                    {
                        response.NextImage = images[index + 1];
                    }
                }
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyImageResponse GetInUseCompanyImage(string companyId, string adminUserId, int companyImageType)
        {
            CompanyImageResponse response = new CompanyImageResponse();
            response.Success = false;
            response.CompanyImage = null;

            string inUseCompanyImageId = string.Empty;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company_image", new List<string> { }, new List<string> { "company_id", "company_image_type" }));
                BoundStatement bs = ps.Bind(companyId, companyImageType);
                RowSet rs = session.Execute(bs);
                foreach (Row r in rs)
                {
                    if (r.GetValue<bool>("is_selected"))
                    {
                        CompanyImage ci = new CompanyImage
                        {
                            CompanyId = r.GetValue<string>("company_id"),
                            CompanyImageId = r.GetValue<string>("company_image_id"),
                            CompanyImageType = r.GetValue<int>("company_image_type"),
                            CreatedByUserId = r.GetValue<string>("created_by_user_id"),
                            CreatedOnTimestamp = r.GetValue<DateTimeOffset>("created_on_timestamp").DateTime,
                            IsSelected = r.GetValue<bool>("is_selected"),
                            LastModifiedByUserId = r.GetValue<string>("last_modified_by_user_id"),
                            LastModifiedTimestamp = r.GetValue<DateTimeOffset>("last_modified_timestamp").DateTime,
                            Url = r.GetValue<string>("url")
                        };

                        response.CompanyImage = ci;
                        break;
                    }
                }
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GetCompanyPermissionResponse GetCompanyPermission(string companyId, string adminUserId)
        {
            GetCompanyPermissionResponse response = new GetCompanyPermissionResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("setting", new List<string> { }, new List<string> { "company_id", "setting_key" }));
                BoundStatement bs = ps.Bind(companyId, "feed_permission_type");
                Row row = session.Execute(bs).FirstOrDefault();
                if (row != null)
                {
                    Company company = new Company();
                    List<string> listDepartmentId = new List<string>();
                    company.CompanyPermissionType = new PermissionType(Convert.ToInt16(row.GetValue<string>("setting_value")));

                    if (Convert.ToInt16(row.GetValue<string>("setting_value")) == Company.PermissionType.CODE_DEPARTMENT)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_department_allowed", new List<string> { }, new List<string> { "company_id" }));
                        bs = ps.Bind(companyId);
                        RowSet rowSet = session.Execute(bs);
                        foreach (Row item in rowSet)
                        {
                            listDepartmentId.Add(item.GetValue<string>("department_id"));
                        }
                        company.CompanyPermissionDepartments = listDepartmentId;
                    }

                    response.Company = company;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.CompanyPermissionError);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyPermissionError);
                    response.ErrorMessage = ErrorMessage.CompanyPermissionError;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdatePermissionResponse UpdateCompanyPermisson(string companyId, string adminUserId, int permisssionType, List<string> allowDepartmentId)
        {
            CompanyUpdatePermissionResponse response = new CompanyUpdatePermissionResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. write data.
                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("setting",
                              new List<string> { "company_id", "setting_key" }, new List<string> { "setting_value" }, new List<string>()));
                BoundStatement bs = ps.Bind(Convert.ToString(permisssionType), companyId, "feed_permission_type");
                session.Execute(bs);

                // Delete > Insert permission_feed_department_allowed
                BatchStatement batchStatement = new BatchStatement();
                ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_department_allowed", new List<string> { }, new List<string> { "company_id" }));
                bs = ps.Bind(companyId);
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    foreach (Row item in rowSet)
                    {
                        PreparedStatement ps_permission_feed_department_allowed = session.Prepare(CQLGenerator.DeleteStatement("permission_feed_department_allowed", new List<string> { "company_id", "department_id" }));
                        BoundStatement bs_permission_feed_department_allowed = ps_permission_feed_department_allowed.Bind(companyId, item.GetValue<string>("department_id"));
                        batchStatement.Add(bs_permission_feed_department_allowed);
                    }
                    session.Execute(batchStatement);
                }

                if (allowDepartmentId.Count > 0)
                {
                    batchStatement = new BatchStatement();
                    foreach (string departmentId in allowDepartmentId)
                    {
                        PreparedStatement ps_permission_feed_department_allowed = session.Prepare(CQLGenerator.InsertStatement("permission_feed_department_allowed", new List<string> { "company_id", "department_id", "last_assigned_by_user_id", "last_assigned_on_timestamp" }));
                        batchStatement.Add(ps_permission_feed_department_allowed.Bind(companyId, departmentId, adminUserId, DateTime.UtcNow));
                    }
                }

                session.Execute(batchStatement);
                response.Success = true;
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdateProfileResponse UpdateProfile(string adminUserId, string companyId, string newTitle, string newPrimaryAdminUserId, string newPrimaryAdminSecondaryEmail, string supportMessage, string zoneName, double timezoneOffset)
        {
            CompanyUpdateProfileResponse response = new CompanyUpdateProfileResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(newPrimaryAdminUserId))
                {
                    Log.Error("Primary admin is not valid");
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyMissingPrimaryAdmin);
                    response.ErrorMessage = ErrorMessage.CompanyMissingPrimaryAdmin;
                    return response;
                }

                if (string.IsNullOrEmpty(supportMessage))
                {
                    Log.Error("Support message is empty");
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyMissingSupportMessage);
                    response.ErrorMessage = ErrorMessage.CompanyMissingSupportMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(zoneName))
                {
                    Log.Error("Zone name is invalid");
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyMissingTimezoneCountryName);
                    response.ErrorMessage = ErrorMessage.CompanyMissingTimezoneCountryName;
                    return response;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                string currentPrimaryAdminUserId = companyRow.GetValue<string>("primary_admin_user_id");

                if (string.IsNullOrEmpty(currentPrimaryAdminUserId))
                {
                    currentPrimaryAdminUserId = companyRow.GetValue<string>("created_by_user_id");
                }

                string currentPrimaryAdminSecondaryEmail = companyRow.GetValue<string>("secondary_email");

                if (!currentPrimaryAdminUserId.Equals(adminUserId) && !currentPrimaryAdminUserId.Equals(newPrimaryAdminUserId)) //&& !currentPrimaryAdminSecondaryEmail.Equals(newPrimaryAdminSecondaryEmail))
                {
                    Log.Error("Admin not authorized: " + adminUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyAdminUserNotPrimaryAdmin);
                    response.ErrorMessage = ErrorMessage.CompanyAdminUserNotPrimaryAdmin;
                    return response;
                }

                #region Check new primary admin account status.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs = ps.Bind(newPrimaryAdminUserId);
                Row row = session.Execute(bs).FirstOrDefault();
                if (row != null)
                {
                    if (row.GetValue<int>("account_status") < 0)
                    {
                        Log.Error("Admin account is not valid");
                        response.ErrorCode = Int16.Parse(ErrorCode.UserNoAdmin);
                        response.ErrorMessage = ErrorMessage.UserNoAdmin;
                        return response;
                    }
                }
                else
                {
                    Log.Error("Admin account is not valid");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserNoAdmin);
                    response.ErrorMessage = ErrorMessage.UserNoAdmin;
                    return response;
                }
                #endregion

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("company",
                    new List<string> { "id" }, new List<string> { "title", "support_message", "secondary_email", "country_timezone", "zone_name", "timezone_offset", "primary_admin_user_id", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(preparedStatement.Bind(newTitle, supportMessage, newPrimaryAdminSecondaryEmail, null, zoneName, timezoneOffset, newPrimaryAdminUserId, adminUserId, DateTime.UtcNow, companyId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySelectProfileResponse SelectProfile(string adminUserId, string companyId)
        {
            CompanySelectProfileResponse response = new CompanySelectProfileResponse();
            response.Timezones = new List<Timezone>();
            response.CompanyProfile = new CompanyProfile();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                string primaryAdminUserId = string.Empty;

                if (companyRow["primary_admin_user_id"] == null)
                {
                    string createdByUserId = companyRow.GetValue<string>("created_by_user_id");

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("company", new List<string> { "id" }, new List<string> { "primary_admin_user_id" }, new List<string>()));
                    session.Execute(ps.Bind(createdByUserId, companyId));

                    primaryAdminUserId = createdByUserId;
                }
                else
                {
                    primaryAdminUserId = companyRow.GetValue<string>("primary_admin_user_id");
                }

                response.CompanyProfile.Title = companyRow.GetValue<string>("title");
                response.CompanyProfile.PrimaryAdminUser = new User().SelectUserBasic(primaryAdminUserId, companyId, true, session).User;
                response.CompanyProfile.PrimaryAdminSecondaryEmail = companyRow["secondary_email"] == null ? string.Empty : companyRow.GetValue<string>("secondary_email");
                response.CompanyProfile.IsPrimaryAdmin = primaryAdminUserId.Equals(adminUserId);
                response.CompanyProfile.SupportMessage = companyRow["support_message"] == null ? string.Empty : companyRow.GetValue<string>("support_message");
                response.CompanyProfile.CountryTimezone = companyRow["country_timezone"] == null ? string.Empty : companyRow.GetValue<string>("country_timezone");
                response.CompanyProfile.ZoneName = companyRow["zone_name"] == null ? string.Empty : companyRow.GetValue<string>("zone_name");
                response.CompanyProfile.TimezoneOffset = companyRow["timezone_offset"] == null ? 0.0 : companyRow.GetValue<double>("timezone_offset");

                PreparedStatement psTimezone = session.Prepare(CQLGenerator.SelectStatement("timezone",
                    new List<string>(), new List<string> { "zone_pk" }));
                RowSet timezoneRowset = session.Execute(psTimezone.Bind("timezone"));

                foreach (Row timezoneRow in timezoneRowset)
                {
                    Timezone timezone = new Timezone
                    {
                        Offset = timezoneRow.GetValue<double>("offset"),
                        ZoneName = timezoneRow.GetValue<string>("zone_name"),
                        Title = timezoneRow.GetValue<string>("title"),
                        IsSelected = timezoneRow.GetValue<string>("zone_name").Equals(response.CompanyProfile.ZoneName) ? true : false
                    };
                    response.Timezones.Add(timezone);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public ServiceResponse SeedCompanyImageTable()
        {
            ServiceResponse response = new ServiceResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                RowSet rowSet = session.Execute(string.Format(
                    "SELECT * FROM company;"));

                if (rowSet != null)
                {
                    Debug.WriteLine("rowSet is not null");

                    foreach (Row row in rowSet) // each row is one company
                    {
                        // create a company image for each image for each company
                        string companyId = row.GetValue<string>("id");

                        string adminProfileImageUrl = row.GetValue<string>("admin_profile_image_url");
                        string adminWebsiteHeaderUrl = row.GetValue<string>("admin_website_header_logo_url");
                        string clientBannerUrl = row.GetValue<string>("client_banner_url");
                        string clientMatchupBannerUrl = row.GetValue<string>("client_matchup_banner_url");
                        string clientProfilePopupBannerUrl = row.GetValue<string>("client_profile_popup_banner_url");
                        string clientPullRefreshBannerUrl = row.GetValue<string>("client_pull_refresh_banner_url");
                        string emailSquareLogoUrl = row.GetValue<string>("email_square_logo_url");
                        string companyLogoUrl = row.GetValue<string>("logo_url");

                        string primaryManagerId = row.GetValue<string>("created_by_user_id");

                        // admin profile image url
                        CreateCompanyImage(companyId, adminProfileImageUrl, (int)CompanyImageType.AdminProfile,
                            primaryManagerId, true);

                        // admin website header logo url
                        CreateCompanyImage(companyId, adminWebsiteHeaderUrl, (int)CompanyImageType.WebsiteHeader,
                            primaryManagerId, true);

                        // client banner url
                        CreateCompanyImage(companyId, clientBannerUrl, (int)CompanyImageType.ClientBanner,
                            primaryManagerId, true);

                        // client banner matchup url
                        CreateCompanyImage(companyId, clientMatchupBannerUrl, (int)CompanyImageType.ClientMatchUp,
                            primaryManagerId, true);

                        // client profile popup banner url
                        CreateCompanyImage(companyId, clientProfilePopupBannerUrl, (int)CompanyImageType.ClientPopUp,
                            primaryManagerId, true);

                        // client pull refresh banner url
                        CreateCompanyImage(companyId, clientPullRefreshBannerUrl, (int)CompanyImageType.ClientPullRefresh,
                            primaryManagerId, true);

                        // email square logo url
                        CreateCompanyImage(companyId, emailSquareLogoUrl, (int)CompanyImageType.EmailSquare,
                            primaryManagerId, true);

                        // logo url
                        CreateCompanyImage(companyId, companyLogoUrl, (int)CompanyImageType.LoginSelection,
                            primaryManagerId, true);
                    }

                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    response.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public CompanySelectImageResponse SelectEmailLogo(string adminUserId, string companyId, int imageType, ISession session = null, Row companyRow = null)
        {
            CompanySelectImageResponse response = new CompanySelectImageResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow == null)
                    {
                        Log.Error("Invalid companyId: " + companyId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                        response.ErrorMessage = ErrorMessage.CompanyInvalid;
                        return response;
                    }
                }

                string defaultImageUrl = string.Empty;
                string column = string.Empty;

                switch (imageType)
                {
                    case (int)CompanyImageType.AdminProfile:
                        defaultImageUrl = DefaultResource.CompanyAdminPhotoUrl;
                        column = "admin_profile_image_url";
                        break;

                    case (int)CompanyImageType.EmailSquare:
                        defaultImageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                        column = "email_square_logo_url";
                        break;

                    case (int)CompanyImageType.WebsiteHeader:
                        defaultImageUrl = DefaultResource.CompanyHeaderImageUrl;
                        column = "admin_website_header_logo_url";
                        break;

                    case (int)CompanyImageType.LoginSelection:
                        defaultImageUrl = DefaultResource.CompanyLogoUrl;
                        column = "logo_url";
                        break;

                    case (int)CompanyImageType.ClientBanner:
                        defaultImageUrl = DefaultResource.CompanyClientBannerUrl;
                        column = "client_banner_url";
                        break;

                    case (int)CompanyImageType.ClientPullRefresh:
                        defaultImageUrl = DefaultResource.CompanyClientPullRefreshImageUrl;
                        column = "client_pull_refresh_banner_url";
                        break;

                    case (int)CompanyImageType.ClientMatchUp:
                        defaultImageUrl = DefaultResource.CompanyMatchupBannerUrl;
                        column = "client_matchup_banner_url";
                        break;
                    case (int)CompanyImageType.ClientPopUp:
                        defaultImageUrl = DefaultResource.CompanyProfilePopupUrl;
                        column = "client_profile_popup_banner_url";
                        break;
                }

                response.ImageUrl = companyRow[column] == null ? defaultImageUrl : companyRow.GetValue<string>(column);
                response.Success = true;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdateProfileResponse UpdateImage(string adminUserId, string companyId, string imageUrl, int imageType, bool deleteImage = true)
        {
            CompanyUpdateProfileResponse response = new CompanyUpdateProfileResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                string currentImageUrl = string.Empty;
                string defaultImageUrl = string.Empty;
                string column = string.Empty;

                switch (imageType)
                {
                    case (int)CompanyImageType.AdminProfile:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyAdminPhotoUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyAdminPhotoUrl;
                        column = "admin_profile_image_url";
                        break;

                    case (int)CompanyImageType.EmailSquare:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                        column = "email_square_logo_url";
                        break;

                    case (int)CompanyImageType.WebsiteHeader:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyHeaderImageUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyHeaderImageUrl;
                        column = "admin_website_header_logo_url";
                        break;

                    case (int)CompanyImageType.LoginSelection:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyLogoUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyLogoUrl;
                        column = "logo_url";
                        break;

                    case (int)CompanyImageType.ClientBanner:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyClientBannerUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyClientBannerUrl;
                        column = "client_banner_url";
                        break;

                    case (int)CompanyImageType.ClientPullRefresh:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyClientPullRefreshImageUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyClientPullRefreshImageUrl;
                        column = "client_pull_refresh_banner_url";
                        break;

                    case (int)CompanyImageType.ClientMatchUp:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyMatchupBannerUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyMatchupBannerUrl;
                        column = "client_matchup_banner_url";
                        break;
                    case (int)CompanyImageType.ClientPopUp:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyProfilePopupUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyProfilePopupUrl;
                        column = "client_profile_popup_banner_url";
                        break;
                }


                currentImageUrl = companyRow.GetValue<string>(column);

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("company",
                    new List<string> { "id" }, new List<string> { column, "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(preparedStatement.Bind(imageUrl, adminUserId, DateTime.UtcNow, companyId));

                response.Success = true;

                // Remove from S3
                if (deleteImage && !currentImageUrl.Equals(defaultImageUrl) && !currentImageUrl.Equals(imageUrl))
                {
                    String bucketName = "cocadre-" + companyId.ToLower();
                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        string path = currentImageUrl.Replace("https://", "");
                        string[] splitPath = path.Split('/');
                        string imageFolder = splitPath[splitPath.Count() - 2];
                        string imageName = splitPath[splitPath.Count() - 1];

                        #region v1.0.0
                        DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = "company/" + imageName
                        };
                        s3Client.DeleteObject(deleteObjectRequest);
                        #endregion

                        #region v2.0.0
                        // _original
                        deleteObjectRequest = new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = "company/" + imageFolder + "/" + imageName
                        };
                        s3Client.DeleteObject(deleteObjectRequest);

                        // _large
                        deleteObjectRequest = new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = "company/" + imageFolder + "/" + imageName.Replace("_original", "_large")
                        };
                        s3Client.DeleteObject(deleteObjectRequest);

                        // _medium
                        deleteObjectRequest = new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = "company/" + imageFolder + "/" + imageName.Replace("_original", "_medium")
                        };
                        s3Client.DeleteObject(deleteObjectRequest);

                        // _small
                        deleteObjectRequest = new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = "company/" + imageFolder + "/" + imageName.Replace("_original", "_small")
                        };
                        s3Client.DeleteObject(deleteObjectRequest);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySelectEmailTemplateResponse SelectEmailTemplate(string adminUserId, string companyId)
        {
            CompanySelectEmailTemplateResponse response = new CompanySelectEmailTemplateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                response.EmailSquareLogoUrl = SelectEmailLogo(adminUserId, companyId, (int)CompanyImageType.EmailSquare, session, companyRow).ImageUrl;

                response.EmailPersonnelInvitationTitle = companyRow.GetValue<string>("email_personnel_invitation_title");
                response.EmailPersonnelInvitationDescription = companyRow.GetValue<string>("email_personnel_invitation_description");
                response.EmailPersonnelInvitationSupportInfo = companyRow.GetValue<string>("email_personnel_invitation_support_info");

                response.EmailAdminInvitationTitle = companyRow.GetValue<string>("email_admin_invitation_title");
                response.EmailAdminInvitationDescription = companyRow.GetValue<string>("email_admin_invitation_description");
                response.EmailAdminInvitationSupportInfo = companyRow.GetValue<string>("email_admin_invitation_support_info");

                response.EmailForgotPasswordTitle = companyRow.GetValue<string>("email_forgot_password_title");
                response.EmailForgotPasswordDescription = companyRow.GetValue<string>("email_forgot_password_description");
                response.EmailForgotPasswordSupportInfo = companyRow.GetValue<string>("email_forgot_password_support_info");

                response.EmailResetPasswordTitle = companyRow.GetValue<string>("email_reset_password_title");
                response.EmailResetPasswordDescription = companyRow.GetValue<string>("email_reset_password_description");
                response.EmailResetPasswordSupportInfo = companyRow.GetValue<string>("email_reset_password_support_info");

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySelectEmailDetailResponse SelectEmailTemplate(string companyId, int emailType, ISession session = null, Row companyRow = null)
        {
            CompanySelectEmailDetailResponse response = new CompanySelectEmailDetailResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                if (companyRow == null)
                {
                    companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow == null)
                    {
                        Log.Error("Invalid companyId: " + companyId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                        response.ErrorMessage = ErrorMessage.CompanyInvalid;
                        return response;
                    }
                }

                response.EmailLogoUrl = SelectEmailLogo(null, companyId, (int)CompanyImageType.EmailSquare, session, companyRow).ImageUrl;

                switch (emailType)
                {
                    case (int)CompanyEmailTemplate.PersonnelInvite:
                        response.EmailTitle = companyRow.GetValue<string>("email_personnel_invitation_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_personnel_invitation_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_personnel_invitation_support_info");
                        break;
                    case (int)CompanyEmailTemplate.AdminInvite:
                        response.EmailTitle = companyRow.GetValue<string>("email_admin_invitation_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_admin_invitation_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_admin_invitation_support_info");
                        break;
                    case (int)CompanyEmailTemplate.ForgotPassword:
                        response.EmailTitle = companyRow.GetValue<string>("email_forgot_password_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_forgot_password_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_forgot_password_support_info");
                        break;
                    case (int)CompanyEmailTemplate.ResettedPassword:
                        response.EmailTitle = companyRow.GetValue<string>("email_reset_password_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_reset_password_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_reset_password_support_info");
                        break;
                    case (int)CompanyEmailTemplate.Live360Report:
                        response.EmailTitle = DefaultResource.CompanyEmailAppraisalTitle;
                        response.EmailDescription = DefaultResource.CompanyEmailAppraisalDescription;
                        response.EmailSupportInfo = DefaultResource.CompanyEmailAppraisalSupportInfo;
                        break;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdateProfileResponse UpdateEmailTemplate(string adminUserId, string companyId, int updateType, string emailTitle, string emailDescription, string emailSupportInfo)
        {
            CompanyUpdateProfileResponse response = new CompanyUpdateProfileResponse();
            response.Success = false;
            try
            {
                string titleColumn = string.Empty;
                string descriptionColumn = string.Empty;
                string supportColumn = string.Empty;

                switch (updateType)
                {
                    case (int)CompanyEmailTemplate.PersonnelInvite:
                        titleColumn = "email_personnel_invitation_title";
                        descriptionColumn = "email_personnel_invitation_description";
                        supportColumn = "email_personnel_invitation_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailPersonnelInvitationTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailPersonnelInvitationDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailPersonnelInvitationSupportInfo;
                        }
                        break;
                    case (int)CompanyEmailTemplate.AdminInvite:
                        titleColumn = "email_admin_invitation_title";
                        descriptionColumn = "email_admin_invitation_description";
                        supportColumn = "email_admin_invitation_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailAdminInvitationTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailAdminInvitationDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailAdminInvitationSupportInfo;
                        }
                        break;
                    case (int)CompanyEmailTemplate.ForgotPassword:
                        titleColumn = "email_forgot_password_title";
                        descriptionColumn = "email_forgot_password_description";
                        supportColumn = "email_forgot_password_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailForgotPasswordTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailForgotPasswordDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailForgotPasswordSupportInfo;
                        }
                        break;
                    case (int)CompanyEmailTemplate.ResettedPassword:
                        titleColumn = "email_reset_password_title";
                        descriptionColumn = "email_reset_password_description";
                        supportColumn = "email_reset_password_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailResetPasswordTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailResetPasswordDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailResetPasswordSupportInfo;
                        }
                        break;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("company",
                    new List<string> { "id" }, new List<string> { titleColumn, descriptionColumn, supportColumn, "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(preparedStatement.Bind(emailTitle, emailDescription, emailSupportInfo, adminUserId, DateTime.UtcNow, companyId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyResponse GetCompany(string companyId, ISession session = null)
        {

            CompanyResponse response = new CompanyResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                #region Step 1.1 Check Admin account's validation.
                //ValidationHandler vh = new ValidationHandler();
                //ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                //if (es != null)
                //{
                //    Log.Error(es.ErrorMessage);
                //    response.ErrorCode = es.ErrorCode;
                //    response.ErrorMessage = es.ErrorMessage;
                //    return response;
                //}
                #endregion
                #endregion

                #region Step 2. Read database.
                RowSet rowSet = session.Execute(string.Format(
                    "SELECT * FROM Company WHERE id = '{0}';", companyId));

                if (rowSet != null)
                {
                    Row row = rowSet.First();

                    //response.AccountType = row.GetValue<int>("account_type");
                    //response.AccountStatus = row.GetValue<int>("account_status");
                    //public string CompanyId;
                    //public string CompanyLogoUrl;
                    //public string CompanyTitle;
                    //public string AdminImageUrl;
                    //public string PrimaryManagerId;
                    //response.CompanyId = row.GetValue<string>("id");
                    //response.CompanyLogoUrl = row.GetValue<string>("logo_url");
                    //response.CompanyTitle = row.GetValue<string>("title");
                    //response.AdminImageUrl  = row.GetValue<string>("admin_profile_image_url");
                    //response.PrimaryManagerId = row.GetValue<string>("created_by_user_id");

                    string primaryManagerId = "";
                    if (string.IsNullOrEmpty(row.GetValue<string>("primary_admin_user_id")))
                    {
                        primaryManagerId = row.GetValue<string>("created_by_user_id");
                    }
                    else
                    {
                        primaryManagerId = row.GetValue<string>("primary_admin_user_id");
                    }

                    response.Company = new Company
                    {
                        CompanyId = row.GetValue<string>("id"),
                        CompanyLogoUrl = row.GetValue<string>("logo_url"),
                        CompanyTitle = row.GetValue<string>("title"),
                        AdminImageUrl = row.GetValue<string>("admin_profile_image_url"),
                        PrimaryManagerId = primaryManagerId,
                        TimeZoneOffset = row.GetValue<double>("timezone_offset")
                    };

                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    response.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UpdateCopmaniesSetting UpdateAllCompaniesS3CorsSetting()
        {
            UpdateCopmaniesSetting response = new UpdateCopmaniesSetting();
            response.Success = false;

            try
            {
                #region Step 1. Get all companies
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company",
                    new List<string>(), new List<string> { }));
                RowSet rsCompanies = session.Execute(ps.Bind());
                if (rsCompanies != null)
                {
                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        foreach (Row rCompany in rsCompanies)
                        {
                            #region Step 2. Delete old CORS setting
                            string bucketName = "cocadre-" + rCompany.GetValue<string>("id").ToLower();
                            DeleteCORSConfigurationRequest requestDelCORS = new DeleteCORSConfigurationRequest { BucketName = bucketName };
                            s3Client.DeleteCORSConfiguration(requestDelCORS);
                            #endregion

                            #region Step 3. Add new CORS setting
                            CORSConfiguration configCORS = new CORSConfiguration
                            {
                                Rules = new System.Collections.Generic.List<CORSRule>
                                {
                                    new CORSRule
                                    {
                                        Id = "CORSDefaultRule",
                                        AllowedMethods = new List<string> {"GET"},
                                        AllowedOrigins = new List<string> {"*"},
                                        MaxAgeSeconds = 3000,
                                        AllowedHeaders = new List<string> { "Authorization" }
                                    }
                                }
                            };
                            PutCORSConfigurationRequest requestAddCORS = new PutCORSConfigurationRequest
                            {
                                BucketName = bucketName,
                                Configuration = configCORS
                            };

                            s3Client.PutCORSConfiguration(requestAddCORS);
                            #endregion
                        }
                        response.Success = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyCreateDefaultJobStatementResponse CreateDefaultJobsStatement(string companyId, string adminUserId, DateTime currentTime, ISession mainSession)
        {
            CompanyCreateDefaultJobStatementResponse response = new CompanyCreateDefaultJobStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                CompanyCreateJobStatementResponse contributorResponse = CreateJobStatement(companyId, adminUserId, DefaultResource.CompanyJobIndividualContributor, currentTime, mainSession, 1);
                foreach (BoundStatement bs in contributorResponse.Statements)
                {
                    response.Statements.Add(bs);
                }
                response.JobId = contributorResponse.JobId;

                foreach (BoundStatement bs in CreateJobStatement(companyId, adminUserId, DefaultResource.CompanyJobManager, currentTime, mainSession, 2).Statements)
                {
                    response.Statements.Add(bs);
                }

                foreach (BoundStatement bs in CreateJobStatement(companyId, adminUserId, DefaultResource.CompanyJobManagerOfManager, currentTime, mainSession, 3).Statements)
                {
                    response.Statements.Add(bs);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyJob SelectUserJob(string companyId, string userId, ISession mainSession, string jobId = null)
        {
            CompanyJob job = new CompanyJob();
            try
            {
                PreparedStatement ps = null;
                if (string.IsNullOrEmpty(jobId))
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("company_job_user", new List<string>(), new List<string> { "company_id", "user_id" }));
                    Row userJobRow = mainSession.Execute(ps.Bind(companyId, userId)).FirstOrDefault();
                    if (userJobRow != null)
                    {
                        jobId = userJobRow.GetValue<string>("job_id");
                    }
                }

                if (!string.IsNullOrEmpty(jobId))
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("company_job", new List<string>(), new List<string> { "company_id", "id" }));
                    Row companyJobRow = mainSession.Execute(ps.Bind(companyId, jobId)).FirstOrDefault();

                    if (companyJobRow != null)
                    {
                        job = GetJob(companyJobRow, mainSession);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return job;
        }

        public CompanySelectAllJobResponse SelectJobs(string companyId, string adminUserId, int queryType = (int)CompanyJobQueryType.Basic, ISession mainSession = null)
        {
            CompanySelectAllJobResponse response = new CompanySelectAllJobResponse();
            response.Jobs = new List<CompanyJob>();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("company_job_by_level", new List<string>(), new List<string> { "company_id" }));
                RowSet companyRowSet = mainSession.Execute(ps.Bind(companyId));
                foreach (Row companyRow in companyRowSet)
                {
                    response.Jobs.Add(GetJob(companyRow, mainSession, queryType));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private CompanyJob GetJob(Row companyJobRow, ISession mainSession, int queryType = (int)CompanyJobQueryType.Basic)
        {
            PreparedStatement ps = null;
            int numberNumberOfPersonnel = 0;
            string jobId = companyJobRow.GetValue<string>("id");
            string companyId = companyJobRow.GetValue<string>("company_id");

            if (queryType == (int)CompanyJobQueryType.FullDetail)
            {
                ps = mainSession.Prepare(CQLGenerator.CountStatement("company_job_user_by_job", new List<string> { "company_id", "job_id" }));
                numberNumberOfPersonnel = (int)mainSession.Execute(ps.Bind(companyId, jobId)).FirstOrDefault().GetValue<long>("count");
            }

            return new CompanyJob
            {
                JobId = companyJobRow.GetValue<string>("id"),
                Level = companyJobRow.GetValue<int>("level"),
                Title = companyJobRow.GetValue<string>("title"),
                NumberOfPersonnels = numberNumberOfPersonnel
            };
        }

        public CompanyCreateJobStatementResponse CreateJobStatement(string companyId, string adminUserId, string title, DateTime currentTime, ISession mainSession, int level = 0)
        {
            CompanyCreateJobStatementResponse response = new CompanyCreateJobStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.JobId = string.Empty;
            response.Success = false;
            try
            {
                PreparedStatement ps = null;

                if (string.IsNullOrEmpty(title))
                {
                    return response;
                }

                title = title.Trim();
                string companyJobId = UUIDGenerator.GenerateUniqueIDForCompanyJob();
                string lowerTitle = title.ToLower();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("company_job_by_lower_title", new List<string>(), new List<string> { "company_id", "lower_title" }));
                Row jobRow = mainSession.Execute(ps.Bind(companyId, lowerTitle)).FirstOrDefault();
                if (jobRow != null)
                {
                    companyJobId = jobRow.GetValue<string>("id");
                }
                else
                {
                    if (level == 0)
                    {
                        ps = mainSession.Prepare(CQLGenerator.CountStatement("company_job", new List<string> { "company_id" }));
                        level = (int)mainSession.Execute(ps.Bind(companyId)).FirstOrDefault().GetValue<long>("count") + 1;
                    }

                    companyJobId = UUIDGenerator.GenerateUniqueIDForCompanyJob();
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("company_job", new List<string> { "company_id", "id", "level", "title", "created_by_user_id", "last_modified_by_user_id", "created_on_timestamp", "last_modified_timestamp" }));
                    response.Statements.Add(ps.Bind(companyId, companyJobId, level, title, adminUserId, adminUserId, currentTime, currentTime));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("company_job_by_lower_title", new List<string> { "company_id", "id", "level", "title", "lower_title" }));
                    response.Statements.Add(ps.Bind(companyId, companyJobId, level, title, lowerTitle));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("company_job_by_level", new List<string> { "company_id", "id", "level", "title" }));
                    response.Statements.Add(ps.Bind(companyId, companyJobId, level, title));
                }

                response.JobId = companyJobId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdateUserJobStatementResponse UpdateUserJobStatement(string companyId, string userId, string jobId, ISession mainSession, string previousJobId = null)
        {
            CompanyUpdateUserJobStatementResponse response = new CompanyUpdateUserJobStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(jobId))
                {
                    return response;
                }

                PreparedStatement ps = null;
                if (!string.IsNullOrEmpty(previousJobId))
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("company_job_user", new List<string> { "company_id", "user_id", "job_id" }));
                    response.DeleteStatements.Add(ps.Bind(companyId, userId, previousJobId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("company_job_user_by_job", new List<string> { "company_id", "user_id", "job_id" }));
                    response.DeleteStatements.Add(ps.Bind(companyId, userId, previousJobId));
                }

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("company_job_user", new List<string> { "company_id", "user_id", "job_id" }));
                response.UpdateStatements.Add(ps.Bind(companyId, userId, jobId));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("company_job_user_by_job", new List<string> { "company_id", "user_id", "job_id" }));
                response.UpdateStatements.Add(ps.Bind(companyId, userId, jobId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }

    #region

    public class CompanyImage
    {
        // IMapper class
        public string CompanyImageId { get; set; }
        public string CompanyId { get; set; }
        public string Url { get; set; }
        public int CompanyImageType { get; set; }
        public bool IsSelected { get; set; }
        public string CreatedByUserId { get; set; }
        public DateTime CreatedOnTimestamp { get; set; }
        public string LastModifiedByUserId { get; set; }
        public DateTime LastModifiedTimestamp { get; set; }
    }

    public class CompanyImageDesc
    {
        // IMapper class
        public string CompanyImageId { get; set; }
        public string CompanyId { get; set; }
        public string Url { get; set; }
        public int CompanyImageType { get; set; }
        public bool IsSelected { get; set; }
        public string CreatedByUserId { get; set; }
        public DateTime CreatedOnTimestamp { get; set; }
        public string LastModifiedByUserId { get; set; }
        public DateTime LastModifiedTimestamp { get; set; }
    }

    [Serializable]
    public class CompanyProfile
    {
        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsPrimaryAdmin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User PrimaryAdminUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PrimaryAdminSecondaryEmail { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SupportMessage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CountryTimezone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ZoneName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public double TimezoneOffset { get; set; }
    }

    [Serializable]
    public class CompanyPersonalization
    {
        [DataMember(EmitDefaultValue = false)]
        public string AdminImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyLogoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ClientBannerUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MatchupBannerUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProfilePopupBannerUrl { get; set; }
    }

    [Serializable]
    public class Timezone
    {
        [DataMember(EmitDefaultValue = false)]
        public double Offset { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ZoneName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsSelected { get; set; }
    }
    #endregion
}