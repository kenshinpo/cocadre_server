﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class RSCard
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum RSCardQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum RSCardType
        {
            SelectOne = 1,
            MultiChoice = 2,
            Text = 3,
            NumberRange = 4,
            DropList = 5,
            Instructional = 6
        }

        public enum RSCardStatus
        {
            Deleted = -1,
            Active = 1,
            Hidden = 2
        }

        public enum RSCardStartRangePosition
        {
            Minimum = 1,
            Middle = 2,
            Maximum = 3
        }

        [DataMember]
        public string CardId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public bool HasToLogic { get; set; }

        [DataMember]
        public bool HasFromLogic { get; set; }

        [DataMember]
        public bool HasImage { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public List<RSImage> CardImages { get; set; }

        [DataMember]
        public bool ToBeSkipped { get; set; }

        [DataMember]
        public bool HasCustomAnswer { get; set; }

        [DataMember]
        public string CustomAnswerInstruction { get; set; }

        [DataMember]
        public bool IsOptionRandomized { get; set; }

        [DataMember]
        public int MiniOptionToSelect { get; set; }

        [DataMember]
        public int MaxOptionToSelect { get; set; }

        [DataMember]
        public bool AllowMultipleLines { get; set; }

        [DataMember]
        public bool HasPageBreak { get; set; }

        [DataMember]
        public int Paging { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int BackgroundType { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public List<RSOption> Options { get; set; }

        [DataMember]
        public List<RSOption> CustomAnswers { get; set; }

        [DataMember]
        public int MaxRange { get; set; }

        [DataMember]
        public string MaxRangeLabel { get; set; }

        [DataMember]
        public int MidRange { get; set; }

        [DataMember]
        public string MidRangeLabel { get; set; }

        [DataMember]
        public int MinRange { get; set; }

        [DataMember]
        public string MinRangeLabel { get; set; }

        [DataMember]
        public int SelectedRange { get; set; }

        [DataMember]
        public RSOption CustomAnswer { get; set; }

        [DataMember]
        public int TotalResponses { get; set; }

        [DataMember]
        public int StartRangePosition { get; set; }

        public string GenerateCardID()
        {
            return UUIDGenerator.GenerateUniqueIDForRSCard();
        }

        public RSCardCreateResponse CreateCard(string cardId,
                                               int type,
                                               string content,
                                               bool hasImage,
                                               List<RSImage> images,
                                               bool toBeSkipped,
                                               bool hasCustomAnswer,
                                               string customAnswerInstruction,
                                               bool isOptionRandomized,
                                               bool allowMultipleLines,
                                               bool hasPageBreak,
                                               int backgoundType,
                                               string note,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               List<RSOption> options,
                                               int miniOptionToSelect = 0,
                                               int maxOptionToSelect = 0,
                                               int startRangePosition = 0,
                                               int maxRange = 0,
                                               string maxRangeLabel = null,
                                               int midRange = 0,
                                               string midRangeLabel = null,
                                               int minRange = 0,
                                               string minRangeLabel = null,
                                               int ordering = -1,
                                               bool isIncludeOptionLogic = false)
        {
            RSCardCreateResponse response = new RSCardCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                // Check content
                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Invalid content");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardContentEmpty);
                    response.ErrorMessage = ErrorMessage.RSCardContentEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(cardId))
                {
                    cardId = UUIDGenerator.GenerateUniqueIDForRSCard();
                }

                RSOption optionManagement = new RSOption();
                RSOptionCreateResponse optionResponse = new RSOptionCreateResponse();
                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                // Setting to default
                switch (type)
                {
                    case (int)RSCardType.SelectOne:
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)RSCardType.Text:
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        isOptionRandomized = false;
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)RSCardType.NumberRange:
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        isOptionRandomized = false;
                        break;
                    case (int)RSCardType.DropList:
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)RSCardType.Instructional:
                        toBeSkipped = false;
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        isOptionRandomized = false;
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                }

                if (type == (int)RSCardType.SelectOne || type == (int)RSCardType.MultiChoice || type == (int)RSCardType.DropList)
                {
                    if (type == (int)RSCardType.MultiChoice)
                    {
                        if (miniOptionToSelect <= 0 || maxOptionToSelect == 0 || maxOptionToSelect < miniOptionToSelect || miniOptionToSelect > options.Count || maxOptionToSelect > options.Count)
                        {
                            Log.Error("Mismatch in option selection values");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSOptionSelectionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionSelectionRangeIsInvalid;
                            return response;
                        }
                    }
                    optionResponse = optionManagement.CreateOptions(adminUserId, cardId, ordering, topicId, options, session, isIncludeOptionLogic);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.updateStatements)
                    {
                        batchStatement.Add(bs);
                    }
                }

                if (type == (int)RSCardType.NumberRange)
                {
                    if (maxRange > 20 || minRange < -20)
                    {
                        Log.Error("Range value out of bounds");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSTopicRangeOutOfBound);
                        response.ErrorMessage = ErrorMessage.RSTopicRangeOutOfBound;
                        return response;
                    }

                    if (maxRange <= minRange)
                    {
                        Log.Error("Mismatch in range values");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSOptionRangeIsInvalid);
                        response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                        return response;
                    }

                    if (startRangePosition == (int)RSCardStartRangePosition.Middle)
                    {
                        if (string.IsNullOrEmpty(minRangeLabel.Trim()) || string.IsNullOrEmpty(midRangeLabel.Trim()) || string.IsNullOrEmpty(maxRangeLabel.Trim()))
                        {
                            Log.Error("Empty field for label");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSTopicRangeLabelEmpty);
                            response.ErrorMessage = ErrorMessage.RSTopicRangeLabelEmpty;
                            return response;
                        }

                        //if (minRange != midRange - (maxRange - midRange))
                        //{
                        //    Log.Error("Mismatch in range values");
                        //    response.ErrorCode = Int16.Parse(ErrorCode.RSOptionRangeIsInvalid);
                        //    response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                        //    return response;
                        //}
                    }
                    else
                    {
                        midRange = 0;
                    }
                }

                ps = session.Prepare(CQLGenerator.CountStatement("rs_card_order", new List<string> { "rs_topic_id" }));
                if (ordering == -1)
                {
                    ordering = (int)session.Execute(ps.Bind(topicId)).FirstOrDefault().GetValue<long>("count") + 1;
                }


                ps = session.Prepare(CQLGenerator.InsertStatement("rs_card",
                    new List<string> { "rs_topic_id", "id", "type", "has_image", "content", "to_be_skipped", "has_custom_answer", "custom_answer_instruction", "is_option_randomized", "mini_option_to_select", "max_option_to_select", "allow_multiple_lines", "has_page_break", "ordering", "background_image_type", "note", "max_range", "min_range", "status", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp", "range_start_position", "max_range_label", "mid_range", "mid_range_label", "min_range_label" }));
                batchStatement.Add(ps.Bind(topicId, cardId, type, hasImage, content, toBeSkipped, hasCustomAnswer, customAnswerInstruction, isOptionRandomized, miniOptionToSelect, maxOptionToSelect, allowMultipleLines, hasPageBreak, ordering, backgoundType, note, maxRange, minRange, (int)RSCardStatus.Active, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow, startRangePosition, maxRangeLabel, midRange, midRangeLabel, minRangeLabel));

                if (hasImage)
                {
                    foreach (RSImage image in images)
                    {
                        string imageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                        string imageUrl = image.ImageUrl;

                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            Log.Error("Missing card image url");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSCardImageUrlMissing);
                            response.ErrorMessage = ErrorMessage.RSCardImageUrlMissing;
                            return response;
                        }

                        int order = image.Ordering;

                        ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_image",
                            new List<string> { "card_id", "image_id", "image_url", "ordering" }));
                        batchStatement.Add(ps.Bind(cardId, imageId, imageUrl, order));
                    }
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order",
                    new List<string> { "rs_topic_id", "card_id", "ordering" }));
                batchStatement.Add(ps.Bind(topicId, cardId, ordering));

                session.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardUpdateResponse UpdateCard(string cardId,
                                               int type,
                                               string content,
                                               bool hasImage,
                                               List<RSImage> images,
                                               bool toBeSkipped,
                                               bool hasCustomAnswer,
                                               string customAnswerInstruction,
                                               bool isOptionRandomized,
                                               bool allowMultipleLines,
                                               bool hasPageBreak,
                                               int backgoundType,
                                               string note,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               List<RSOption> options,
                                               int miniOptionToSelect = 0,
                                               int maxOptionToSelect = 0,
                                               int startRangePosition = 0,
                                               int maxRange = 0,
                                               string maxRangeLabel = null,
                                               int midRange = 0,
                                               string midRangeLabel = null,
                                               int minRange = 0,
                                               string minRangeLabel = null)
        {
            RSCardUpdateResponse response = new RSCardUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                int topicStatus = topicRow.GetValue<int>("status");
                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");
                DateTime currentTime = DateTime.UtcNow;
                int progress = new RSTopic().SelectProgress(startDate, endDate, currentTime);

                // Check card row
                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                // Check content
                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Invalid content");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardContentEmpty);
                    response.ErrorMessage = ErrorMessage.RSCardContentEmpty;
                    return response;
                }

                if (type == (int)RSCardType.MultiChoice)
                {
                    if (miniOptionToSelect < 0 || maxOptionToSelect == 0 || maxOptionToSelect < miniOptionToSelect)
                    {
                        Log.Error("Mismatch in option selection values");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSOptionSelectionRangeIsInvalid);
                        response.ErrorMessage = ErrorMessage.RSOptionSelectionRangeIsInvalid;
                        return response;
                    }
                }

                if (topicStatus == (int)RSTopic.RSTopicStatus.CODE_UNLISTED || progress == (int)RSTopic.ProgressStatusEnum.Upcoming)
                {
                    // Check card for error
                    int currentOrder = cardRow.GetValue<int>("ordering");
                    RSCardCheckErrorResponse checkCardResponse = CheckCardForError(topicId,
                                                                                   currentOrder,
                                                                                   type,
                                                                                   miniOptionToSelect,
                                                                                   maxOptionToSelect,
                                                                                   startRangePosition,
                                                                                   maxRange,
                                                                                   midRange,
                                                                                   minRange,
                                                                                   maxRangeLabel,
                                                                                   midRangeLabel,
                                                                                   minRangeLabel,
                                                                                   options,
                                                                                   session);

                    if (!checkCardResponse.Success)
                    {
                        response.ErrorCode = checkCardResponse.ErrorCode;
                        response.ErrorMessage = checkCardResponse.ErrorMessage;
                        return response;
                    }

                    // Get all images url
                    List<string> newImageUrls = new List<string>();
                    foreach (RSImage image in images)
                    {
                        newImageUrls.Add(image.ImageUrl);
                    }

                    foreach (RSOption option in options)
                    {
                        if (option.HasImage || option.Images != null)
                        {
                            foreach (RSImage image in option.Images)
                            {
                                newImageUrls.Add(image.ImageUrl);
                            }
                        }
                    }

                    // Recreate card
                    DeleteCard(topicId, categoryId, cardId, adminUserId, companyId, false, session, newImageUrls);
                    RSCardCreateResponse createResponse = CreateCard(cardId,
                                                                       type,
                                                                       content,
                                                                       hasImage,
                                                                       images,
                                                                       toBeSkipped,
                                                                       hasCustomAnswer,
                                                                       customAnswerInstruction,
                                                                       isOptionRandomized,
                                                                       allowMultipleLines,
                                                                       hasPageBreak,
                                                                       backgoundType,
                                                                       note,
                                                                       categoryId,
                                                                       topicId,
                                                                       adminUserId,
                                                                       companyId,
                                                                       options,
                                                                       miniOptionToSelect,
                                                                       maxOptionToSelect,
                                                                       startRangePosition,
                                                                       maxRange,
                                                                       maxRangeLabel,
                                                                       midRange,
                                                                       midRangeLabel,
                                                                       minRange,
                                                                       minRangeLabel,
                                                                       currentOrder,
                                                                       true);

                    response.Success = createResponse.Success;
                    response.ErrorCode = createResponse.ErrorCode;
                    response.ErrorMessage = createResponse.ErrorMessage;
                }
                else
                {
                    BatchStatement deleteBatch = new BatchStatement();
                    BatchStatement updateBatch = new BatchStatement();

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "has_image", "content", "note", "has_page_break", "mini_option_to_select", "max_option_to_select", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(hasImage, content, note, hasPageBreak, miniOptionToSelect, maxOptionToSelect, adminUserId, DateTime.UtcNow, topicId, cardId));

                    // Update option
                    int currentCardOrdering = cardRow.GetValue<int>("ordering");
                    RSOptionUpdateResponse optionResponse = new RSOption().UpdateOptions(cardId, topicId, currentCardOrdering, options, companyId, session);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.boundStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    // Delete from S3
                    bool currentHasImage = cardRow.GetValue<bool>("has_image");
                    List<string> currentImageUrls = new List<string>();
                    if (currentHasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_image", new List<string>(), new List<string> { "card_id" }));
                        RowSet currentImageRowset = session.Execute(ps.Bind(cardId));

                        foreach (Row currentImageRow in currentImageRowset)
                        {
                            string imageUrl = currentImageRow.GetValue<string>("image_url");
                            currentImageUrls.Add(imageUrl);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_image", new List<string> { "card_id" }));
                        deleteBatch.Add(ps.Bind(cardId));
                    }

                    if (hasImage && images != null && images.Count > 0)
                    {
                        int order = 1;
                        foreach (RSImage image in images)
                        {
                            string imageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                            string imageUrl = image.ImageUrl;

                            if (string.IsNullOrEmpty(imageUrl))
                            {
                                Log.Error("Missing card image url");
                                response.ErrorCode = Int16.Parse(ErrorCode.RSCardImageUrlMissing);
                                response.ErrorMessage = ErrorMessage.RSCardImageUrlMissing;
                                return response;
                            }

                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_image",
                                new List<string> { "card_id", "image_id", "image_url", "ordering" }));
                            updateBatch.Add(ps.Bind(cardId, imageId, imageUrl, order));

                            order++;

                            if (currentImageUrls.Contains(imageUrl))
                            {
                                currentImageUrls.Remove(imageUrl);
                            }
                        }
                    }

                    if (currentImageUrls.Count > 0)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();
                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            // Remaining image url to be remove
                            foreach (string currentImageUrl in currentImageUrls)
                            {
                                string path = currentImageUrl.Replace("https://", "");
                                string[] splitPath = path.Split('/');
                                string imageName = splitPath[splitPath.Count() - 1];

                                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "surveys/" + topicId + "/" + cardId + "/" + imageName
                                };

                                s3Client.DeleteObject(deleteObjectRequest);
                            }
                        }
                    }

                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                if (ex is FormatException)
                {
                    Log.Error(ex.ToString(), ex);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicRangeOutOfBound);
                    response.ErrorMessage = ErrorMessage.RSTopicRangeOutOfBound;
                }
                else
                {
                    Log.Error(ex.ToString(), ex);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                    response.ErrorMessage = ErrorMessage.SystemError;
                }
            }
            return response;
        }

        private RSCardCheckErrorResponse CheckCardForError(string topicId,
                                                           int currentCardOrdering,
                                                           int type,
                                                           int miniOptionToSelect,
                                                           int maxOptionToSelect,
                                                           int startRangePosition,
                                                           int maxRange,
                                                           int midRange,
                                                           int minRange,
                                                           string maxRangeLabel,
                                                           string midRangeLabel,
                                                           string minRangeLabel,
                                                           List<RSOption> options,
                                                           ISession session)
        {
            RSCardCheckErrorResponse response = new RSCardCheckErrorResponse();
            response.Success = false;
            try
            {
                if (type == (int)RSCardType.SelectOne || type == (int)RSCardType.MultiChoice || type == (int)RSCardType.DropList)
                {
                    if (type == (int)RSCardType.MultiChoice)
                    {
                        if (miniOptionToSelect < 0 || maxOptionToSelect == 0 || maxOptionToSelect < miniOptionToSelect)
                        {
                            Log.Error("Mismatch in option selection values");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSOptionSelectionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionSelectionRangeIsInvalid;
                            return response;
                        }
                    }

                    if (options != null || options.Count > 0)
                    {
                        RSOption optionManager = new RSOption();
                        foreach (RSOption option in options)
                        {
                            bool optionHasImage = option.HasImage;

                            // Check if option is empty
                            if (string.IsNullOrEmpty(option.Content))
                            {
                                Log.Error("Invalid option content");
                                response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionContentEmpty);
                                response.ErrorMessage = ErrorMessage.RSOptionContentEmpty;
                                return response;
                            }

                            if (optionHasImage)
                            {
                                foreach (RSImage image in option.Images)
                                {
                                    string imageUrl = image.ImageUrl;

                                    if (string.IsNullOrEmpty(imageUrl))
                                    {
                                        Log.Error("Invalid option image url");
                                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionImageUrlMissing);
                                        response.ErrorMessage = ErrorMessage.RSOptionImageUrlMissing;
                                        return response;
                                    }
                                }
                            }

                            // Check next question linkage
                            if (!string.IsNullOrEmpty(option.NextCardId))
                            {
                                RSOptionCheckLogicResponse logicResponse = optionManager.CheckLogicForNextCard(topicId, currentCardOrdering, option, session);
                                if (!logicResponse.Success)
                                {
                                    Log.Error(logicResponse.ErrorMessage);
                                    response.ErrorCode = logicResponse.ErrorCode;
                                    response.ErrorMessage = logicResponse.ErrorMessage;
                                    return response;
                                }
                            }
                        }
                    }
                }

                else if (type == (int)RSCardType.NumberRange)
                {
                    if (maxRange > 20 || minRange < -20)
                    {
                        Log.Error("Range value out of bounds");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSTopicRangeOutOfBound);
                        response.ErrorMessage = ErrorMessage.RSTopicRangeOutOfBound;
                        return response;
                    }

                    if (maxRange <= minRange)
                    {
                        Log.Error("Mismatch in range values");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSOptionRangeIsInvalid);
                        response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                        return response;
                    }

                    if (startRangePosition == (int)RSCardStartRangePosition.Middle)
                    {
                        if (string.IsNullOrEmpty(minRangeLabel.Trim()) || string.IsNullOrEmpty(midRangeLabel.Trim()) || string.IsNullOrEmpty(maxRangeLabel.Trim()))
                        {
                            Log.Error("Empty field for label");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSTopicRangeLabelEmpty);
                            response.ErrorMessage = ErrorMessage.RSTopicRangeLabelEmpty;
                            return response;
                        }

                        if (minRange != midRange - (maxRange - midRange))
                        {
                            Log.Error("Mismatch in range values");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSOptionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                            return response;
                        }
                    }
                    else
                    {
                        midRange = 0;
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public RSCardUpdateResponse DeleteAllCards(string topicId, string companyId, ISession session)
        {
            RSCardUpdateResponse response = new RSCardUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardRowset = session.Execute(ps.Bind(topicId));

                String bucketName = "cocadre-" + companyId.ToLower();
                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    foreach (Row cardRow in cardRowset)
                    {
                        deleteBatch = new BatchStatement();
                        string cardId = cardRow.GetValue<string>("id");
                        bool hasImage = cardRow.GetValue<bool>("has_image");
                        int order = cardRow.GetValue<int>("ordering");
                        if (hasImage)
                        {
                            // Remove from S3
                            ListObjectsRequest listRequest = new ListObjectsRequest();
                            listRequest.BucketName = bucketName;
                            listRequest.Prefix = "surveys/" + topicId + "/" + cardId;

                            ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                            foreach (S3Object imageObject in listResponse.S3Objects)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = imageObject.Key;
                                s3Client.DeleteObject(deleteRequest);
                            }

                            DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                            deleteFolderRequest.BucketName = bucketName;
                            deleteFolderRequest.Key = "surveys/" + topicId + "/" + cardId;
                            s3Client.DeleteObject(deleteFolderRequest);

                            ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_image", new List<string> { "card_id" }));
                            deleteBatch.Add(ps.Bind(cardId));
                        }

                        RSOptionUpdateResponse optionResponse = new RSOption().DeleteAllOptions(topicId, cardId, companyId, session, null);
                        if (!optionResponse.Success)
                        {
                            Log.Error(optionResponse.ErrorMessage);
                            response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                            response.ErrorMessage = optionResponse.ErrorMessage;
                            return response;
                        }

                        foreach (BoundStatement bs in optionResponse.boundStatements)
                        {
                            deleteBatch.Add(bs);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card", new List<string> { "rs_topic_id", "id" }));
                        deleteBatch.Add(ps.Bind(topicId, cardId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(topicId, order, cardId));

                        session.Execute(deleteBatch);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardUpdateResponse DeleteCard(string topicId, string categoryId, string cardId, string adminUserId, string companyId, bool isReorderingNeeded = true, ISession session = null, List<string> newImageFileNames = null)
        {
            RSCardUpdateResponse response = new RSCardUpdateResponse();
            response.Success = false;

            try
            {
                int currentOrder = 0;

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    // Check topic category
                    Row topicCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }

                    // Check topic row
                    Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                        response.ErrorMessage = ErrorMessage.TopicInvalid;
                        return response;
                    }

                    // Check card row
                    Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, session);

                    if (cardRow == null)
                    {
                        Log.Error("Card already been deleted: " + cardId);
                        response.ErrorCode = Int16.Parse(ErrorCode.RSCardAlreadyBeenDeleted);
                        response.ErrorMessage = ErrorMessage.RSCardAlreadyBeenDeleted;
                        return response;
                    }

                    currentOrder = cardRow.GetValue<int>("ordering");

                    if (CheckCardForFromLogic(cardId, session))
                    {
                        Log.Error("Card has from logic");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSCardHasLogic);
                        response.ErrorMessage = ErrorMessage.RSCardHasLogic;
                        return response;
                    }

                    if (!IsAbleToDelete(topicId, currentOrder, session))
                    {
                        Log.Error("Previous card does not have page break");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSNoPageBreakForDeleteLogic);
                        response.ErrorMessage = ErrorMessage.RSNoPageBreakForDeleteLogic;
                        return response;
                    }
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Remove from S3
                String bucketName = "cocadre-" + companyId.ToLower();

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;
                    listRequest.Prefix = "surveys/" + topicId + "/" + cardId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        if (imageObject.Size <= 0)
                        {
                            continue;
                        }

                        if (newImageFileNames != null && newImageFileNames.Count > 0)
                        {
                            //surveys/RST943cee78090948d086af5e30d051a250/RSCf60dd0247a12406ea824e7d59c5007cc/1_20160510101139774_original.jpeg
                            string[] stringToken = imageObject.Key.Split('/');
                            string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                            string[] fileNames = fileNamePath.Split('_');
                            string fileName = fileNames[0] + "_" + fileNames[1];

                            if (newImageFileNames.Any(newFileName => newFileName.Contains(fileName)))
                            {
                                continue;
                            }
                        }

                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }

                    if (isReorderingNeeded)
                    {
                        // Delete folder if this is not update
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = "surveys/" + topicId + "/" + cardId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_image", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                RSOptionUpdateResponse optionResponse = new RSOption().DeleteAllOptions(topicId, cardId, companyId, session, newImageFileNames);
                if (!optionResponse.Success)
                {
                    Log.Error(optionResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                    response.ErrorMessage = optionResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in optionResponse.boundStatements)
                {
                    deleteBatch.Add(bs);
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card", new List<string> { "rs_topic_id", "id" }));
                deleteBatch.Add(ps.Bind(topicId, cardId));

                // Reorder card
                if (isReorderingNeeded)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                    RowSet orderRowset = session.Execute(ps.Bind(topicId, currentOrder));

                    foreach (Row orderRow in orderRowset)
                    {
                        int order = orderRow.GetValue<int>("ordering");
                        string nextCardId = orderRow.GetValue<string>("card_id");

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(topicId, order, nextCardId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                        updateBatch.Add(ps.Bind(order - 1, topicId, nextCardId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                        updateBatch.Add(ps.Bind(topicId, order - 1, nextCardId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(topicId, currentOrder, cardId));
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public int SelectNumberOfCards(string topicId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psCard = session.Prepare(CQLGenerator.CountStatement("rs_card",
                    new List<string> { "rs_topic_id", "status" }));
                Row cardRow = session.Execute(psCard.Bind(topicId, (int)RSCardStatus.Active)).FirstOrDefault();

                if (cardRow != null)
                {
                    number = (int)cardRow.GetValue<long>("count");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        public RSCardSelectAllResponse SelectAllCardsByUser(string requesterUserId, string topicId, ISession mainSession, ISession analyticSession)
        {
            RSCardSelectAllResponse response = new RSCardSelectAllResponse();
            response.Cards = new List<RSCard>();
            response.Success = false;
            try
            {
                // Select card details using rs_card_order
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardRowset = mainSession.Execute(ps.Bind(topicId));

                int page = 1;
                foreach (Row cardRow in cardRowset)
                {
                    string cardId = cardRow.GetValue<string>("card_id");
                    int ordering = cardRow.GetValue<int>("ordering");

                    RSCard selectedCard = SelectCard(requesterUserId, null, cardId, topicId, (int)RSCardQueryType.FullDetail, null, true, mainSession, analyticSession).Card;

                    if (selectedCard != null)
                    {
                        selectedCard.Ordering = ordering;
                        selectedCard.Paging = page;
                        if (selectedCard.HasPageBreak)
                        {
                            page++;
                        }
                        response.Cards.Add(selectedCard);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardSelectAllResponse SelectAllCards(string topicId, string categoryId, string adminUserId, string companyId, int queryType, string containsName = null, ISession session = null)
        {
            RSCardSelectAllResponse response = new RSCardSelectAllResponse();
            response.Cards = new List<RSCard>();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);
                    if (categoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                        return response;
                    }

                    Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);
                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                        response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                        return response;
                    }

                    response.Introduction = topicRow.GetValue<string>("introduction");
                    response.ClosingWords = topicRow.GetValue<string>("closing_words");
                    response.TopicTitle = topicRow.GetValue<string>("title");
                    response.TopicIconUrl = topicRow.GetValue<string>("icon_url");
                }


                if (!string.IsNullOrEmpty(containsName))
                {
                    containsName = containsName.Trim();
                }

                // Select card details using rs_card_order
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardRowset = session.Execute(ps.Bind(topicId));

                int page = 1;
                foreach (Row cardRow in cardRowset)
                {
                    string cardId = cardRow.GetValue<string>("card_id");
                    int ordering = cardRow.GetValue<int>("ordering");

                    RSCard selectedCard = SelectCard(adminUserId, companyId, cardId, topicId, queryType, null, false, session, null).Card;

                    if (selectedCard != null)
                    {
                        selectedCard.Ordering = ordering;
                        selectedCard.Paging = page;

                        if (selectedCard.HasPageBreak)
                        {
                            page++;
                        }

                        if (!string.IsNullOrEmpty(containsName))
                        {
                            if (selectedCard.Content.ToLower().Contains(containsName.ToLower()))
                            {
                                response.Cards.Add(selectedCard);
                            }
                        }
                        else
                        {
                            response.Cards.Add(selectedCard);
                        }

                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardSelectResponse SelectCard(string requesterUserId, string companyId, string cardId, string topicId, int queryType, Row cardRow = null, bool getAnswer = false, ISession session = null, ISession analyticSession = null)
        {
            RSCardSelectResponse response = new RSCardSelectResponse();
            response.Card = null;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (session == null)
                {
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(requesterUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                // cardRow can be already fetched from other methods
                PreparedStatement ps = null;
                if (cardRow == null)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id", "id", "status" }));
                    cardRow = session.Execute(ps.Bind(topicId, cardId, (int)RSCardStatus.Active)).FirstOrDefault();

                    if (cardRow == null)
                    {
                        Log.Error("Invalid cardId: " + cardId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardInvalid);
                        response.ErrorMessage = ErrorMessage.RSCardInvalid;
                        return response;
                    }
                }

                // Get Basic Details
                int ordering = cardRow.GetValue<int>("ordering");
                string content = cardRow.GetValue<string>("content");
                bool hasImage = cardRow.GetValue<bool>("has_image");
                int type = cardRow.GetValue<int>("type");
                bool hasPageBreak = cardRow.GetValue<bool>("has_page_break");
                bool hasToLogic = false;
                bool hasFromLogic = false;
                List<RSImage> cardImages = new List<RSImage>();
                List<RSOption> options = new List<RSOption>();


                bool isAllowToBeSkipped = false;
                bool hasCustomAnswer = cardRow.GetValue<bool>("has_custom_answer");
                string customAnswerInstruction = string.Empty;
                bool isOptionRandomized = false;
                int miniOption = 0;
                int maxOption = 0;
                bool isAllowMultipleLines = false;
                int backgroundType = 1;
                string note = string.Empty;
                int page = 1;
                int startRangePosition = 0;
                int maxRange = 0;
                string maxRangeLabel = null;
                int midRange = 0;
                string midRangeLabel = null;
                int minRange = 0;
                string minRangeLabel = null;

                int selectedRange = -1;
                RSOption customAnswer = null;

                if (hasImage)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_image", new List<string>(), new List<string> { "card_id" }));
                    RowSet cardImageRowset = session.Execute(ps.Bind(cardId));

                    foreach (Row cardImageRow in cardImageRowset)
                    {
                        string imageId = cardImageRow.GetValue<string>("image_id");
                        string imageUrl = cardImageRow.GetValue<string>("image_url");
                        int order = cardImageRow.GetValue<int>("ordering");

                        RSImage image = new RSImage
                        {
                            ImageId = imageId,
                            ImageUrl = imageUrl,
                            Ordering = order
                        };

                        cardImages.Add(image);
                    }

                    cardImages = cardImages.OrderBy(o => o.Ordering).ToList();
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id" }));
                RowSet cardLogicRowset = session.Execute(ps.Bind(cardId));
                List<Row> cardToLogicList = cardLogicRowset.ToList();

                if (cardToLogicList.Count() > 0)
                {
                    hasToLogic = true;
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_from_logic", new List<string>(), new List<string> { "card_id" }));
                cardLogicRowset = session.Execute(ps.Bind(cardId));
                List<Row> cardFromLogicList = cardLogicRowset.ToList();

                if (cardFromLogicList.Count() > 0)
                {
                    hasFromLogic = true;
                }

                RSOption optionManager = new RSOption();
                AnalyticSurvey analyticManager = new AnalyticSurvey();

                // Get Full Details
                if (queryType == (int)RSCardQueryType.FullDetail)
                {
                    isAllowToBeSkipped = cardRow.GetValue<bool>("to_be_skipped");
                    customAnswerInstruction = cardRow.GetValue<string>("custom_answer_instruction");
                    isOptionRandomized = cardRow.GetValue<bool>("is_option_randomized");
                    miniOption = cardRow.GetValue<int>("mini_option_to_select");
                    maxOption = cardRow.GetValue<int>("max_option_to_select");
                    isAllowMultipleLines = cardRow.GetValue<bool>("allow_multiple_lines");
                    backgroundType = cardRow.GetValue<int>("background_image_type");
                    note = cardRow.GetValue<string>("note");
                    page = FindPageForCard(topicId, ordering, session);

                    if (type != (int)RSCardType.Instructional && type != (int)RSCardType.Text && type != (int)RSCardType.NumberRange)
                    {
                        options = optionManager.SelectOptions(topicId, companyId, cardId, session, analyticSession, requesterUserId, getAnswer);

                        if (hasCustomAnswer && getAnswer)
                        {
                            customAnswer = analyticManager.SelectCustomAnswerFromCardByUser(topicId, cardId, requesterUserId, session, analyticSession);
                        }
                    }
                    else
                    {
                        if (type == (int)RSCardType.NumberRange)
                        {
                            startRangePosition = cardRow.GetValue<int>("range_start_position");
                            maxRange = cardRow.GetValue<int>("max_range");
                            maxRangeLabel = cardRow.GetValue<string>("max_range_label");
                            midRange = cardRow.GetValue<int>("mid_range");
                            midRangeLabel = cardRow.GetValue<string>("mid_range_label");
                            minRange = cardRow.GetValue<int>("min_range");
                            minRangeLabel = cardRow.GetValue<string>("min_range_label");

                            // Get answer
                            if (getAnswer)
                            {
                                selectedRange = analyticManager.SelectRangeFromCard(cardId, requesterUserId, analyticSession);
                            }
                        }
                        else if (type == (int)RSCardType.Text)
                        {
                            // Get answer
                            if (getAnswer)
                            {
                                customAnswer = analyticManager.SelectCustomAnswerFromCardByUser(topicId, cardId, requesterUserId, session, analyticSession);
                            }

                        }
                    }

                }

                response.Card = new RSCard
                {
                    CardId = cardId,
                    Type = type,
                    HasImage = hasImage,
                    Content = content,
                    HasToLogic = hasToLogic,
                    HasFromLogic = hasFromLogic,
                    CardImages = cardImages,
                    Options = options,
                    ToBeSkipped = isAllowToBeSkipped,
                    HasCustomAnswer = hasCustomAnswer,
                    CustomAnswerInstruction = customAnswerInstruction,
                    IsOptionRandomized = isOptionRandomized,
                    MiniOptionToSelect = miniOption,
                    MaxOptionToSelect = maxOption,
                    AllowMultipleLines = isAllowMultipleLines,
                    HasPageBreak = hasPageBreak,
                    Ordering = ordering,
                    Paging = page,
                    BackgroundType = backgroundType,
                    Note = note,

                    StartRangePosition = startRangePosition,
                    MaxRange = maxRange,
                    MaxRangeLabel = maxRangeLabel,
                    MidRange = midRange,
                    MidRangeLabel = midRangeLabel,
                    MinRange = minRange,
                    MinRangeLabel = minRangeLabel,

                    SelectedRange = selectedRange,
                    CustomAnswer = customAnswer
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSCardAnswerResponse AnswerCard(string answeredByUserId, string companyId, string topicId, string topicCategoryId, string cardId, int rangeSelected, List<string> optionIds, RSOption customAnswer, bool isSkipped)
        {
            RSCardAnswerResponse response = new RSCardAnswerResponse();
            response.NextCards = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, topicCategoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                // Check if user has already completed
                AnalyticSurvey analyticsManager = new AnalyticSurvey();
                if (analyticsManager.CheckRSCompleted(topicId, answeredByUserId, analyticSession))
                {
                    Log.Error("Survey already completed by user: " + answeredByUserId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicAlreadyCompleted);
                    response.ErrorMessage = ErrorMessage.RSTopicAlreadyCompleted;
                    return response;
                }

                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, mainSession);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                bool hasCustomAnswer = cardRow.GetValue<bool>("has_custom_answer");
                int currentOrder = cardRow.GetValue<int>("ordering");
                int cardType = cardRow.GetValue<int>("type");
                bool isAllowToSkip = cardRow.GetValue<bool>("to_be_skipped");
                int minOptionToSelect = cardRow.GetValue<int>("mini_option_to_select");
                int maxOptionToSelect = cardRow.GetValue<int>("max_option_to_select");

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                bool hasTextAnswer = false;
                RSOption previousCustomAnswer = new RSOption();

                if (cardType == (int)RSCard.RSCardType.NumberRange)
                {
                    int maxRange = cardRow.GetValue<int>("max_range");
                    int minRange = cardRow.GetValue<int>("min_range");

                    if (rangeSelected == Convert.ToInt32(DefaultResource.RSCardRangeNotSelected))
                    {
                        if (!isAllowToSkip)
                        {
                            Log.Error(ErrorMessage.RSOptionRangeIsInvalid);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                            return response;
                        }
                    }
                    else
                    {
                        if (rangeSelected < minRange || rangeSelected > maxRange)
                        {
                            Log.Error(ErrorMessage.RSOptionRangeIsInvalid);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                            return response;
                        }
                    }
                }
                else if (cardType == (int)RSCard.RSCardType.SelectOne)
                {
                    if (optionIds == null || optionIds != null && optionIds.Count == 0)
                    {
                        if (customAnswer == null || customAnswer != null && string.IsNullOrEmpty(customAnswer.Content.Trim()))
                        {
                            if (!isAllowToSkip)
                            {
                                Log.Error(ErrorMessage.RSCustomAnswerNotFilled);
                                response.ErrorCode = Convert.ToInt16(ErrorCode.RSCustomAnswerNotFilled);
                                response.ErrorMessage = ErrorMessage.RSCustomAnswerNotFilled;
                                return response;
                            }
                        }
                        else
                        {
                            hasTextAnswer = true;

                            customAnswer.Content = customAnswer.Content.Trim();

                            Log.Debug("RS -> Custom answer response: ");
                            Log.Debug(customAnswer.Content);
                        }
                    }
                    else
                    {
                        optionIds = optionIds.Take(1).ToList();
                    }

                    // Still have to check for previous custom answer
                    if (hasCustomAnswer)
                    {
                        // Check previously created text response
                        // Overwrite content if text response existed
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "card_id" }));
                        Row customOptionRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                        if (customOptionRow != null)
                        {
                            previousCustomAnswer.OptionId = customOptionRow.GetValue<string>("option_id");
                        }
                        if (customAnswer != null)
                        {
                            customAnswer.OptionId = UUIDGenerator.GenerateUniqueIDForRSOption(true);
                        }

                        RSOptionCreateResponse optionResponse = new RSOption().CreateCustomOption(answeredByUserId, cardId, previousCustomAnswer, customAnswer, currentTime, mainSession);

                        if (!optionResponse.Success)
                        {
                            Log.Error(optionResponse.ErrorMessage);
                            response.ErrorCode = optionResponse.ErrorCode;
                            response.ErrorMessage = optionResponse.ErrorMessage;
                            return response;
                        }

                        foreach (BoundStatement bs in optionResponse.updateStatements)
                        {
                            updateBatch.Add(bs);
                        }

                        foreach (BoundStatement bs in optionResponse.deleteStatements)
                        {
                            deleteBatch.Add(bs);
                        }

                        if (previousCustomAnswer != null && !string.IsNullOrEmpty(optionResponse.previousCustomAnswerId))
                        {
                            previousCustomAnswer.OptionId = optionResponse.previousCustomAnswerId;
                            previousCustomAnswer.Content = optionResponse.previousCustomAnswer;
                            previousCustomAnswer.CreatedOnTimestamp = optionResponse.previousCreatedOnTimestamp;
                        }

                        mainSession.Execute(deleteBatch);
                        mainSession.Execute(updateBatch);
                    }
                }
                else if (cardType == (int)RSCard.RSCardType.MultiChoice)
                {
                    if (optionIds == null || optionIds != null && optionIds.Count == 0)
                    {
                        if (!isAllowToSkip)
                        {
                            Log.Error(ErrorMessage.RSOptionSelectionIsInvalid);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionSelectionIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionSelectionIsInvalid;
                            return response;
                        }
                    }
                    else
                    {
                        if (optionIds.Count < minOptionToSelect)
                        {
                            Log.Error(ErrorMessage.RSOptionSelectionIsInvalid);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionSelectionIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionSelectionIsInvalid;
                            return response;
                        }

                        if (optionIds.Count > maxOptionToSelect)
                        {
                            optionIds = optionIds.Take(maxOptionToSelect).ToList();
                        }
                    }
                }
                else if (cardType == (int)RSCard.RSCardType.Text)
                {
                    hasTextAnswer = true;

                    if (customAnswer == null || customAnswer != null && string.IsNullOrEmpty(customAnswer.Content.Trim()))
                    {
                        if (!isAllowToSkip)
                        {
                            Log.Error(ErrorMessage.RSCustomAnswerNotFilled);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSCustomAnswerNotFilled);
                            response.ErrorMessage = ErrorMessage.RSCustomAnswerNotFilled;
                            return response;
                        }
                        else
                        {
                            isAllowToSkip = true;
                        }

                        Log.Debug("RS -> No text response, skipped");
                    }
                    else
                    {
                        customAnswer.Content = customAnswer.Content.Trim();

                        Log.Debug("RS -> Text response: ");
                        Log.Debug(customAnswer.Content);
                    }

                    // Check previously created text response
                    // Overwrite content if text response existed
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "card_id" }));
                    Row customOptionRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                    if (customOptionRow != null)
                    {
                        previousCustomAnswer.OptionId = customOptionRow.GetValue<string>("option_id");
                    }

                    if (customAnswer != null)
                    {
                        customAnswer.OptionId = UUIDGenerator.GenerateUniqueIDForRSOption(true);
                    }

                    RSOptionCreateResponse optionResponse = new RSOption().CreateCustomOption(answeredByUserId, cardId, previousCustomAnswer, customAnswer, currentTime, mainSession);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = optionResponse.ErrorCode;
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.updateStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    foreach (BoundStatement bs in optionResponse.deleteStatements)
                    {
                        deleteBatch.Add(bs);
                    }

                    if (previousCustomAnswer != null && !string.IsNullOrEmpty(optionResponse.previousCustomAnswerId))
                    {
                        previousCustomAnswer.OptionId = optionResponse.previousCustomAnswerId;
                        previousCustomAnswer.Content = optionResponse.previousCustomAnswer;
                        previousCustomAnswer.CreatedOnTimestamp = optionResponse.previousCreatedOnTimestamp;
                    }

                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);
                }

                analyticsManager.AnswerRSCard(answeredByUserId, topicId, cardId, cardType, hasCustomAnswer, hasTextAnswer, optionIds, customAnswer, previousCustomAnswer, rangeSelected, currentTime, isSkipped, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSCardReorderResponse ReorderCard(string cardId, string topicId, string categoryId, string adminUserId, string companyId, int insertAtOrder)
        {
            RSCardReorderResponse response = new RSCardReorderResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                int topicStatus = topicRow.GetValue<int>("status");
                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");
                DateTime currentTime = DateTime.UtcNow;
                int progress = new RSTopic().SelectProgress(startDate, endDate, currentTime);

                if (topicStatus != (int)RSTopic.RSTopicStatus.CODE_UNLISTED && progress != (int)RSTopic.ProgressStatusEnum.Upcoming)
                {
                    Log.Error("Topic is not unlisted and not upcoming: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardReorderingNotAllowed);
                    response.ErrorMessage = ErrorMessage.RSCardReorderingNotAllowed;
                    return response;
                }

                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                bool currentHasPageBreak = cardRow.GetValue<bool>("has_page_break");
                int currentOrder = cardRow.GetValue<int>("ordering");

                if (currentOrder != insertAtOrder)
                {
                    if (vh.ValidateLogicForCard(cardId, topicId, session))
                    {
                        Log.Error("Card has logic");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSCardHasLogic);
                        response.ErrorMessage = ErrorMessage.RSCardHasLogic;
                        return response;
                    }

                    if (!IsAbleToReorder(topicId, currentHasPageBreak, insertAtOrder, session))
                    {
                        Log.Error("The next card has logic but modified card does not have a page break");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSNoPageBreakForOrderLogic);
                        response.ErrorMessage = ErrorMessage.RSNoPageBreakForOrderLogic;
                        return response;
                    }

                    BatchStatement updateBatch = new BatchStatement();
                    BatchStatement deleteBatch = new BatchStatement();

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                    updateBatch.Add(ps.Bind(insertAtOrder, topicId, cardId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(topicId, currentOrder, cardId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                    updateBatch.Add(ps.Bind(topicId, insertAtOrder, cardId));

                    // Ignore the top range
                    if (currentOrder > insertAtOrder)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatementWithRangeComparison("rs_card_order",
                            new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThanOrEquals, "ordering", CQLGenerator.Comparison.LessThan, 0));
                        RowSet orderRowset = session.Execute(ps.Bind(topicId, insertAtOrder, currentOrder));

                        foreach (Row orderRow in orderRowset)
                        {
                            string updateCardId = orderRow.GetValue<string>("card_id");
                            int updateCardOrder = orderRow.GetValue<int>("ordering");

                            ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            deleteBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));

                            updateCardOrder++;

                            ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                            updateBatch.Add(ps.Bind(updateCardOrder, topicId, updateCardId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            updateBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));
                        }
                    }
                    // Ignore the bottom range
                    else
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatementWithRangeComparison("rs_card_order",
                            new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, "ordering", CQLGenerator.Comparison.LessThanOrEquals, 0));
                        RowSet orderRowset = session.Execute(ps.Bind(topicId, currentOrder, insertAtOrder));

                        foreach (Row orderRow in orderRowset)
                        {
                            string updateCardId = orderRow.GetValue<string>("card_id");
                            int updateCardOrder = orderRow.GetValue<int>("ordering");

                            ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            deleteBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));

                            updateCardOrder--;

                            ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                            updateBatch.Add(ps.Bind(updateCardOrder, topicId, updateCardId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            updateBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));
                        }
                    }

                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public int FindPageForCard(string topicId, int cardOrder, ISession session)
        {
            int page = 1;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.LessThan, 0));
                RowSet orderRowset = session.Execute(ps.Bind(topicId, cardOrder));

                foreach (Row orderRow in orderRowset)
                {
                    string cardId = orderRow.GetValue<string>("card_id");
                    RSCard card = SelectCard(null, null, cardId, topicId, (int)RSCardQueryType.Basic, null, false, session, null).Card;

                    if (card != null && card.HasPageBreak)
                    {
                        page++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return page;
        }

        public bool IsAbleToReorder(string topicId, bool modifiedCardHasPageBreak, int insertAtOrder, ISession session)
        {
            bool result = true;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id", "ordering" }));
                Row cardOrderRow = session.Execute(ps.Bind(topicId, insertAtOrder)).FirstOrDefault();

                if (cardOrderRow != null)
                {
                    string currentCardId = cardOrderRow.GetValue<string>("card_id");
                    bool currentCardHasFromLogic = CheckCardForFromLogic(currentCardId, session);

                    if (currentCardHasFromLogic && !modifiedCardHasPageBreak)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }

        public bool IsAbleToDelete(string topicId, int deleteOrder, ISession session)
        {
            bool result = true;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id", "ordering" }));
                Row nextCardOrderRow = session.Execute(ps.Bind(topicId, deleteOrder + 1)).FirstOrDefault();

                if (nextCardOrderRow != null)
                {
                    string nextCardId = nextCardOrderRow.GetValue<string>("card_id");
                    bool nextCardHasFromLogic = CheckCardForFromLogic(nextCardId, session);

                    if (nextCardHasFromLogic)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id", "ordering" }));
                        Row previousCardOrderRow = session.Execute(ps.Bind(topicId, deleteOrder - 1)).FirstOrDefault();
                        if (previousCardOrderRow != null)
                        {
                            string previousCardId = previousCardOrderRow.GetValue<string>("card_id");
                            ps = session.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id", "id" }));
                            Row previousCardRow = session.Execute(ps.Bind(topicId, previousCardId)).FirstOrDefault();
                            if (previousCardRow != null)
                            {
                                if (!previousCardRow.GetValue<bool>("has_page_break"))
                                {
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }


        public bool CheckCardForFromLogic(string cardId, ISession session)
        {
            bool result = false;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_from_logic", new List<string>(), new List<string> { "card_id" }));

                if (session.Execute(ps.Bind(cardId)).Count() > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }

        public bool CheckCardForTologic(string cardId, ISession session)
        {
            bool result = false;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id" }));

                if (session.Execute(ps.Bind(cardId)).Count() > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }

        public AnalyticSelectRSCardResultResponse SelectResult(string adminUserId, string companyId, string topicId, string categoryId)
        {
            AnalyticSelectRSCardResultResponse response = new AnalyticSelectRSCardResultResponse();
            response.CardResults = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                List<RSCard> cards = SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)RSCard.RSCardQueryType.FullDetail, null, mainSession).Cards;

                // Fetch result per card
                AnalyticSurvey analyticManager = new AnalyticSurvey();
                foreach (RSCard card in cards)
                {
                    analyticManager.SelectRSCardResult(card, analyticSession, mainSession);
                }

                response.CardResults = cards;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectRSCardResultByUserResponse SelectResultByUser(string adminUserId, string companyId, string topicId, string categoryId, string answeredByUserId)
        {
            AnalyticSelectRSCardResultByUserResponse response = new AnalyticSelectRSCardResultByUserResponse();
            response.CardResults = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                bool isAnonymous = topicRow.GetValue<bool>("is_anonymous");
                if (!isAnonymous)
                {
                    User answeredByUser = new User().SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User;
                    if (answeredByUser == null)
                    {
                        Log.Error("Invalid userId: " + answeredByUser);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                    List<RSCard> cards = SelectAllCardsByUser(answeredByUserId, topicId, mainSession, analyticSession).Cards;

                    // Clean up
                    foreach (RSCard card in cards)
                    {
                        if (card.Type == (int)RSCard.RSCardType.MultiChoice || card.Type == (int)RSCard.RSCardType.SelectOne || card.Type == (int)RSCard.RSCardType.DropList)
                        {
                            for (int index = 0; index < card.Options.Count; index++)
                            {
                                if (!card.Options[index].IsSelected)
                                {
                                    card.Options.Remove(card.Options[index]);
                                    index--;
                                }
                            }
                        }
                    }

                    response.Report = new AnalyticSurvey().SelectRSRespondersReport(topicId, isAnonymous, new List<User> { answeredByUser }, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), mainSession, analyticSession).Reports[0];
                    response.CardResults = cards;
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectRSCustomAnswersResponse SelectCustomAnswersFromCard(string adminUserId, string companyId, string topicId, string categoryId, string cardId)
        {
            AnalyticSelectRSCustomAnswersResponse response = new AnalyticSelectRSCustomAnswersResponse();
            response.Card = new RSCard();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                // Anonymous
                bool isAnonymous = topicRow.GetValue<bool>("is_anonymous");

                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, mainSession);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                RSCard card = SelectCard(adminUserId, companyId, cardId, topicId, (int)RSCard.RSCardQueryType.FullDetail, cardRow, false, mainSession, analyticSession).Card;

                // Fetch result
                AnalyticSurvey analyticManager = new AnalyticSurvey();
                analyticManager.SelectRSCustomAnswersResultFromCard(card, isAnonymous, companyId, analyticSession, mainSession);

                response.Card = card;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }

    [Serializable]
    [DataContract]
    public class RSImage
    {
        [DataMember]
        public string ImageId { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public int Ordering { get; set; }
    }
}
