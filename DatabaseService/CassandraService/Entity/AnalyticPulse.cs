﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.ServiceResponses;
using CassandraService.GlobalResources;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using CassandraService.Utilities;
using static CassandraService.Entity.Chart;

namespace CassandraService.Entity
{
    public class AnalyticPulse
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataContract]
        public class PersonnelList
        {
            [DataMember]
            public string Title { get; set; }
            [DataMember]
            public bool IsActionRequired { get; set; }
            [DataMember]
            public List<PulseUserAnalytic> Users { get; set; }
        }

        [DataContract]
        public class PulseUserAnalytic
        {
            [DataMember]
            public string ActionName { get; set; }
            [DataMember]
            public int ActionCode { get; set; }
            [DataMember]
            public DateTime FirstSeenDate { get; set; }
            [DataMember]
            public string FirstSeenDateString { get; set; }
            [DataMember]
            public User User { get; set; }
        }

        [DataContract]
        public class PulseResponsiveStats
        {
            [DataMember]
            public PulseDynamic Pulse { get; set; }
            [DataMember]
            public Chart Chart { get; set; }
        }

        [DataContract]
        public class PulseResponsiveSearchTerm
        {
            [DataMember]
            public string Content { get; set; }
            [DataMember]
            public string Id { get; set; }
            [DataMember]
            public bool IsSelected { get; set; }
        }

        public int SelectSingleActionTakenByUser(string pulseId, int pulseType, string requesterUserId, ISession analyticSession)
        {
            int actionTaken = (int)Pulse.PulseActionEnum.DoNothing;
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("pulse_single_stats", new List<string>(), new List<string> { "action_taken_by_user_id", "pulse_id" }));
                Row completedRow = analyticSession.Execute(ps.Bind(requesterUserId, pulseId)).FirstOrDefault();

                if (completedRow != null)
                {
                    actionTaken = completedRow.GetValue<int>("action");
                    if (pulseType == (int)Pulse.PulseTypeEnum.Announcement)
                    {
                        if (actionTaken != (int)Pulse.PulseActionEnum.Acknowledge)
                        {
                            actionTaken = (int)Pulse.PulseActionEnum.DoNothing;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return actionTaken;
        }

        public PulseUpdateSingleStatsResponse UpdateSingleAction(string pulseId, string userId, int action, int pulseType, ISession analyticSession)
        {
            PulseUpdateSingleStatsResponse response = new PulseUpdateSingleStatsResponse();
            response.Success = false;

            try
            {
                if (!pulseId.Equals(DefaultResource.PulseDefaultBannerId))
                {
                    PreparedStatement ps = null;
                    BatchStatement updateBatch = new BatchStatement();

                    int subAction = 0;

                    Log.Debug("Update single action: " + action);

                    if (pulseType == (int)Pulse.PulseTypeEnum.Announcement)
                    {
                        if (action != (int)Pulse.PulseActionEnum.Skip)
                        {
                            action = (int)Pulse.PulseActionEnum.Acknowledge;
                            subAction = (int)Pulse.PulseActionEnum.SaveToInbox;
                        }
                    }

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("pulse_single_stats",
                        new List<string> { "pulse_id", "action_taken_by_user_id", "action", "sub_action", "action_taken_on_timestamp" }));
                    updateBatch.Add(ps.Bind(pulseId, userId, action, subAction, DateTime.UtcNow));

                    analyticSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public PulseUpdateImpressionResponse UpdateImpression(string pulseId, string userId, ISession analyticSession)
        {
            PulseUpdateImpressionResponse response = new PulseUpdateImpressionResponse();
            response.Success = false;

            try
            {
                if (!pulseId.Equals(DefaultResource.PulseDefaultBannerId))
                {
                    PreparedStatement ps = null;
                    BatchStatement updateBatch = new BatchStatement();

                    DateTime currentTime = DateTime.UtcNow;

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("pulse_unique_impression_stats", new List<string>(), new List<string> { "pulse_id", "seen_by_user_id" }));
                    Row impressionRow = analyticSession.Execute(ps.Bind(pulseId, userId)).FirstOrDefault();

                    if (impressionRow != null)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("pulse_unique_impression_stats", new List<string> { "pulse_id", "seen_by_user_id" }, new List<string> { "last_seen_on_timestamp" }, new List<string>()));
                        updateBatch.Add(ps.Bind(currentTime, pulseId, userId));
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("pulse_unique_impression_stats", new List<string> { "pulse_id", "seen_by_user_id", "first_seen_on_timestamp", "last_seen_on_timestamp" }));
                        updateBatch.Add(ps.Bind(pulseId, userId, currentTime, currentTime));
                    }

                    ps = analyticSession.Prepare(CQLGenerator.UpdateCounterStatement("pulse_impression_stats", new List<string> { "pulse_id" }, new List<string> { "impression" }, new List<int> { 1 }));
                    analyticSession.Execute(ps.Bind(pulseId));

                    analyticSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public PulseSelectSingleAnalyticResponse SelectAnalyticForSinglePulse(Pulse pulse, int type, List<User> targetedAudience, double timezoneOffset, ISession mainSession, ISession analyticSession)
        {
            PulseSelectSingleAnalyticResponse response = new PulseSelectSingleAnalyticResponse();
            response.LeftChart = new Chart();
            response.RightChart = new Chart();
            response.PersonnelList = new PersonnelList();
            response.PersonnelList.Users = new List<PulseUserAnalytic>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                string pulseId = pulse.PulseId;

                switch (type)
                {
                    case (int)Pulse.PulseTypeEnum.Banner:
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("pulse_impression_stats", new List<string>(), new List<string> { "pulse_id" }));
                        int impressionCount = (int)analyticSession.Execute(ps.Bind(pulseId)).FirstOrDefault().GetValue<long>("impression");

                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("pulse_unique_impression_stats", new List<string> { "pulse_id" }));
                        int uniqueReachCount = (int)analyticSession.Execute(ps.Bind(pulseId)).FirstOrDefault().GetValue<long>("count");

                        response.LeftChart = new Chart
                        {
                            Title = "Accumulated Impressions All Time",
                            MaxCount = impressionCount,
                            Type = (int)ChartType.Bar,
                            Stats = new List<Stat>
                            {
                                new Stat
                                {
                                    Count = impressionCount,
                                    Name = string.Empty,
                                    Percentage = 100
                                }
                            }
                        };

                        int percentage = targetedAudience.Count == 0 ? 0 : (int)Math.Ceiling(((double)uniqueReachCount / targetedAudience.Count) * 100);
                        response.RightChart = new Chart
                        {
                            Title = "Unique Personnel Impressions",
                            MaxCount = targetedAudience.Count,
                            Type = (int)ChartType.Pie,
                            Stats = new List<Stat>
                            {
                                new Stat
                                {
                                    Count = uniqueReachCount,
                                    Name = "Personnel",
                                    Percentage = percentage
                                }
                            }
                        };

                        response.PersonnelList.IsActionRequired = false;
                        break;
                    case (int)Pulse.PulseTypeEnum.Announcement:
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("pulse_single_stats", new List<string> { "pulse_id", "action" }));
                        int acknowledgeCount = (int)analyticSession.Execute(ps.Bind(pulseId, (int)Pulse.PulseActionEnum.Acknowledge)).FirstOrDefault().GetValue<long>("count");

                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("pulse_single_stats", new List<string> { "pulse_id", "action" }));
                        int skipCount = (int)analyticSession.Execute(ps.Bind(pulseId, (int)Pulse.PulseActionEnum.Skip)).FirstOrDefault().GetValue<long>("count");

                        int acknowledgePercentage = targetedAudience.Count == 0 ? 0 : (int)Math.Ceiling(((double)acknowledgeCount / targetedAudience.Count) * 100);
                        int skipPercentage = targetedAudience.Count == 0 ? 0 : (int)Math.Ceiling(((double)skipCount / targetedAudience.Count) * 100);
                        int absentCount = targetedAudience.Count - acknowledgeCount - skipCount;
                        int absentPercentage = 100 - acknowledgePercentage - skipPercentage;

                        response.LeftChart = new Chart
                        {
                            Title = string.Empty,
                            MaxCount = targetedAudience.Count,
                            Type = (int)ChartType.Bar,
                            Stats = new List<Stat>
                            {
                                new Stat
                                {
                                    Count = acknowledgeCount,
                                    Name = DefaultResource.PulseActionAcknowledge,
                                    Percentage = acknowledgePercentage
                                },
                                new Stat
                                {
                                    Count = skipCount,
                                    Name = DefaultResource.PulseActionSkipCard,
                                    Percentage = skipPercentage
                                },
                                new Stat
                                {
                                    Count = absentCount,
                                    Name = "Absent",
                                    Percentage = absentPercentage
                                }

                            }
                        };

                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("pulse_single_stats", new List<string> { "pulse_id", "sub_action" }));
                        int savedCount = (int)analyticSession.Execute(ps.Bind(pulseId, (int)Pulse.PulseActionEnum.SaveToInbox)).FirstOrDefault().GetValue<long>("count");

                        response.RightChart = new Chart
                        {
                            Title = "Save",
                            MaxCount = targetedAudience.Count,
                            Type = (int)ChartType.OneToOne,
                            Stats = new List<Stat>
                            {
                                new Stat
                                {
                                    Count = savedCount,
                                    Name = string.Empty,
                                    Percentage = 0,
                                }
                            },
                            Disclaimer = "Note. Number doesn't remove those who had saved and later deleted."
                        };

                        response.PersonnelList.IsActionRequired = true;
                        break;
                }

                response.Pulse = pulse;
                response.PersonnelList.Title = "Responders List";
                response.PersonnelList.Users = SelectUsersForSingleAnalytic(pulseId, timezoneOffset, targetedAudience, type, analyticSession);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<PulseUserAnalytic> SelectUsersForSingleAnalytic(string pulseId, double timezoneOffset, List<User> targetedAudience, int type, ISession analyticSession)
        {
            List<PulseUserAnalytic> users = new List<PulseUserAnalytic>();
            try
            {
                PreparedStatement ps = null;
                if (type == (int)Pulse.PulseTypeEnum.Banner)
                {
                    foreach (User user in targetedAudience)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("pulse_unique_impression_stats", new List<string>(), new List<string> { "pulse_id", "seen_by_user_id" }));
                        Row userRow = analyticSession.Execute(ps.Bind(pulseId, user.UserId)).FirstOrDefault();

                        if (userRow != null)
                        {
                            DateTime firstSeenDate = userRow.GetValue<DateTime>("first_seen_on_timestamp");
                            firstSeenDate = firstSeenDate.AddHours(timezoneOffset);

                            PulseUserAnalytic analyticUser = new PulseUserAnalytic
                            {
                                User = user,
                                FirstSeenDate = firstSeenDate,
                                FirstSeenDateString = firstSeenDate.ToString("dd MMM yyyy")
                            };

                            users.Add(analyticUser);
                        }
                    }
                }
                else
                {
                    foreach (User user in targetedAudience)
                    {
                        PulseUserAnalytic analyticUser = new PulseUserAnalytic
                        {
                            ActionCode = (int)Pulse.PulseActionEnum.DoNothing,
                            ActionName = "Absent",
                            FirstSeenDateString = "-",
                            User = user
                        };

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("pulse_single_stats", new List<string>(), new List<string> { "pulse_id", "action_taken_by_user_id" }));
                        Row userRow = analyticSession.Execute(ps.Bind(pulseId, user.UserId)).FirstOrDefault();

                        if (userRow != null)
                        {
                            int action = userRow.GetValue<int>("action");
                            DateTime actionTakenDate = userRow.GetValue<DateTime>("action_taken_on_timestamp");
                            actionTakenDate = actionTakenDate.AddHours(timezoneOffset);
                            string actionName = action == (int)Pulse.PulseActionEnum.Acknowledge ? DefaultResource.PulseActionAcknowledge : DefaultResource.PulseActionSkipCard;

                            analyticUser.ActionCode = action;
                            analyticUser.ActionName = actionName;
                            analyticUser.FirstSeenDate = actionTakenDate;
                            analyticUser.FirstSeenDateString = actionTakenDate.ToString("dd MMM yyyy");
                        }

                        users.Add(analyticUser);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return users;
        }

        public void AnswerCard(string answeredByUserId,
                               string pulseId,
                               int cardType,
                               DateTime currentTime,
                               int rangeSelected = -1,
                               string selectedOptionId = null,
                               string customAnswer = null,
                               ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Check if number range
                if (cardType == (int)PulseDynamic.DeckCardTypeEnum.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_range_answered_by_user",
                       new List<string>(), new List<string> { "answered_by_user_id", "pulse_id" }));
                    Row rangeRow = analyticSession.Execute(ps.Bind(answeredByUserId, pulseId)).FirstOrDefault();

                    if (rangeRow != null)
                    {
                        int previousRangeSelected = rangeRow.GetValue<int>("range_value");

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("deck_range_answered_by_user", new List<string> { "pulse_id", "range_value", "answered_by_user_id" }));
                        deleteBatch.Add(ps.Bind(pulseId, previousRangeSelected, answeredByUserId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("deck_range_answered_by_pulse", new List<string> { "pulse_id", "range_value", "answered_by_user_id" }));
                        deleteBatch.Add(ps.Bind(pulseId, previousRangeSelected, answeredByUserId));
                    }

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_range_answered_by_user", new List<string> { "pulse_id", "range_value", "answered_by_user_id", "answered_on_timestamp" }));
                    updateBatch.Add(ps.Bind(pulseId, rangeSelected, answeredByUserId, currentTime));
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_range_answered_by_pulse", new List<string> { "pulse_id", "range_value", "answered_by_user_id", "answered_on_timestamp" }));
                    updateBatch.Add(ps.Bind(pulseId, rangeSelected, answeredByUserId, currentTime));
                }
                // Select one
                else if (cardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOne || cardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOneWithLogic)
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("deck_option_answered_by_user",
                        new List<string> { "answered_by_user_id", "pulse_id" }, new List<string> { "option_id", "answered_on_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(selectedOptionId, currentTime, answeredByUserId, pulseId));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("deck_option_answered_by_pulse",
                        new List<string> { "answered_by_user_id", "pulse_id" }, new List<string> { "option_id", "answered_on_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(selectedOptionId, currentTime, answeredByUserId, pulseId));
                }
                // Text
                else if (cardType == (int)PulseDynamic.DeckCardTypeEnum.Text)
                {
                    string customOptionId = pulseId + "_" + answeredByUserId;

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "pulse_id", "option_id" }));
                    Row customAnswerRow = analyticSession.Execute(ps.Bind(answeredByUserId, pulseId, customOptionId)).FirstOrDefault();
                    if (customAnswerRow != null)
                    {
                        // Remove previously answered pulse
                        RemoveCustomAnswer(analyticSession, answeredByUserId, pulseId, customAnswerRow.GetValue<string>("content"));
                    }

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_custom_option_created_by_user", new List<string> { "pulse_id", "option_id", "content", "created_by_user_id", "created_on_timestamp" }));
                    updateBatch.Add(ps.Bind(pulseId, customOptionId, customAnswer, answeredByUserId, currentTime));
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_custom_option_created_by_pulse", new List<string> { "pulse_id", "option_id", "content", "created_by_user_id", "created_on_timestamp" }));
                    updateBatch.Add(ps.Bind(pulseId, customOptionId, customAnswer, answeredByUserId, currentTime));

                    string lowerCustomAnswer = customAnswer.ToLower().Trim();
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_by_answer", new List<string>(), new List<string> { "custom_answer", "pulse_id" }));
                    Row similarRow = analyticSession.Execute(ps.Bind(lowerCustomAnswer, pulseId)).FirstOrDefault();

                    string similarId = string.Empty;
                    int number = 0;
                    if (similarRow != null)
                    {
                        similarId = similarRow.GetValue<string>("similar_id");
                        DateTimeOffset lastUpdatedTime = similarRow.GetValue<DateTimeOffset>("created_on_timestamp");
                        number = similarRow.GetValue<int>("number");

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("deck_custom_option_sort_by_number_timestamp", new List<string> { "pulse_id", "number", "created_on_timestamp", "custom_answer" }));
                        deleteBatch.Add(ps.Bind(pulseId, number, lastUpdatedTime, lowerCustomAnswer));

                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("deck_custom_option_by_answer", new List<string> { "custom_answer", "pulse_id" }, new List<string> { "number", "created_on_timestamp" }, new List<string>()));
                        updateBatch.Add(ps.Bind(number + 1, currentTime, lowerCustomAnswer, pulseId));
                    }
                    else
                    {
                        similarId = UUIDGenerator.GenerateUniqueIDForPulseSimilar();

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_custom_option_by_answer", new List<string> { "similar_id", "pulse_id", "custom_answer", "number", "created_on_timestamp" }));
                        updateBatch.Add(ps.Bind(similarId, pulseId, lowerCustomAnswer, number + 1, currentTime));
                    }

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_custom_option_sort_by_number_timestamp", new List<string> { "pulse_id", "number", "created_on_timestamp", "custom_answer" }));
                    updateBatch.Add(ps.Bind(pulseId, number + 1, currentTime, lowerCustomAnswer));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_custom_option_by_similar", new List<string> { "similar_id", "option_id", "content", "created_by_user_id", "created_on_timestamp" }));
                    updateBatch.Add(ps.Bind(similarId, customOptionId, customAnswer, answeredByUserId, currentTime));
                }

                analyticSession.Execute(deleteBatch);
                analyticSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public bool CheckDynamicCardCompleted(string answeredByUserId, string pulseId, Row cardRow, ISession analyticSession = null, ISession mainSession = null, string deckId = null)
        {
            bool isCompleted = false;
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = null;
                int cardType = cardRow.GetValue<int>("card_type");
                if (cardType == (int)PulseDynamic.DeckCardTypeEnum.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_range_answered_by_user",
                       new List<string>(), new List<string> { "answered_by_user_id", "pulse_id" }));
                    Row rangeRow = analyticSession.Execute(ps.Bind(answeredByUserId, pulseId)).FirstOrDefault();
                    if (rangeRow != null)
                    {
                        isCompleted = true;
                    }
                }
                else if (cardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOne || cardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOneWithLogic)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_option_answered_by_user",
                        new List<string>(), new List<string> { "answered_by_user_id", "pulse_id" }));
                    Row selectOneRow = analyticSession.Execute(ps.Bind(answeredByUserId, pulseId)).FirstOrDefault();
                    if (selectOneRow != null)
                    {
                        // Need to check all children to see if it is really completed
                        if (cardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOneWithLogic)
                        {
                            string selectedOptionId = selectOneRow.GetValue<string>("option_id");
                            string optionNumber = selectedOptionId.Replace(pulseId + "_", "");
                            string nextPulseId = cardRow.GetValue<string>(string.Format("option_{0}_next_card_id", optionNumber));
                            if (!string.IsNullOrEmpty(nextPulseId) && !nextPulseId.Equals(DefaultResource.PulseDynamicEndCardId))
                            {
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck", new List<string>(), new List<string> { "deck_id", "id" }));
                                cardRow = mainSession.Execute(ps.Bind(deckId, nextPulseId)).FirstOrDefault();
                                return CheckDynamicCardCompleted(answeredByUserId, nextPulseId, cardRow, analyticSession, mainSession, deckId);
                            }
                        }

                        isCompleted = true;
                    }
                }
                else if (cardType == (int)PulseDynamic.DeckCardTypeEnum.Text)
                {
                    string customOptionId = pulseId + "_" + answeredByUserId;

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "pulse_id", "option_id" }));
                    Row customAnswerRow = analyticSession.Execute(ps.Bind(answeredByUserId, pulseId, customOptionId)).FirstOrDefault();
                    if (customAnswerRow != null)
                    {
                        isCompleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isCompleted;
        }

        private void RemoveCustomAnswer(ISession analyticSession, string answeredByUserId, string pulseId, string previousCustomAnswer)
        {
            try
            {
                // Delete custom answer
                previousCustomAnswer = previousCustomAnswer.ToLower().Trim();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_by_answer",
                    new List<string>(), new List<string> { "custom_answer", "pulse_id" }));
                Row answerRow = analyticSession.Execute(ps.Bind(previousCustomAnswer, pulseId)).FirstOrDefault();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                if (answerRow != null)
                {
                    string similiarId = answerRow.GetValue<string>("similar_id");
                    int numberOfSimiliar = answerRow.GetValue<int>("number");
                    DateTimeOffset createdDate = answerRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("deck_custom_option_sort_by_number_timestamp", new List<string> { "pulse_id", "number", "created_on_timestamp", "custom_answer" }));
                    deleteBatch.Add(ps.Bind(pulseId, numberOfSimiliar, createdDate, previousCustomAnswer));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("deck_custom_option_by_similar", new List<string> { "similar_id", "created_on_timestamp", "created_by_user_id" }));
                    deleteBatch.Add(ps.Bind(similiarId, createdDate, answeredByUserId));

                    if (numberOfSimiliar > 1)
                    {
                        numberOfSimiliar--;
                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("deck_custom_option_by_answer",
                            new List<string> { "custom_answer", "card_id" }, new List<string> { "number", "created_on_timestamp" }, new List<string>()));
                        updateBatch.Add(ps.Bind(numberOfSimiliar, createdDate, previousCustomAnswer, pulseId));

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("deck_custom_option_sort_by_number_timestamp", new List<string> { "pulse_id", "number", "created_on_timestamp", "custom_answer" }));
                        updateBatch.Add(ps.Bind(pulseId, numberOfSimiliar, createdDate, previousCustomAnswer));
                    }
                    else if (numberOfSimiliar == 1)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("deck_custom_option_by_answer", new List<string> { "pulse_id", "custom_answer" }));
                        deleteBatch.Add(ps.Bind(pulseId, previousCustomAnswer));
                    }

                    // Need to execute delete first
                    analyticSession.Execute(deleteBatch);
                    analyticSession.Execute(updateBatch);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectDynamicCardResult(PulseResponsiveStats stats, string companyId, string searchId, int targetedAudienceCount, ISession mainSession, ISession analyticSession)
        {
            try
            {
                PulseDynamic card = stats.Pulse;
                Chart chart = new Chart();
                stats.Chart = chart;

                ConnectionManager cm = new ConnectionManager();
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                Department departmentManager = new Department();

                PreparedStatement ps = null;

                int totalResponses = 0;

                if (card.CardType == (int)PulseDynamic.DeckCardTypeEnum.NumberRange)
                {
                    List<DeckCardOption> rangeOptions = new List<DeckCardOption>();
                    int maxRange = 0;
                    int minRange = 0;
                    int incremental = 1;
                    switch (card.RangeType)
                    {
                        case (int)PulseDynamic.CardRangeTypeEnum.OneTo3:
                            minRange = 1;
                            maxRange = 3;
                            break;
                        case (int)PulseDynamic.CardRangeTypeEnum.OneTo5:
                            minRange = 1;
                            maxRange = 5;
                            break;
                        case (int)PulseDynamic.CardRangeTypeEnum.OneTo7:
                            minRange = 1;
                            maxRange = 7;
                            break;
                        case (int)PulseDynamic.CardRangeTypeEnum.OneTo10:
                            minRange = 1;
                            maxRange = 10;
                            break;
                        case (int)PulseDynamic.CardRangeTypeEnum.ZeroTo100Percent:
                            minRange = 0;
                            maxRange = 100;
                            incremental = 10;
                            break;
                    }

                    int index = minRange;
                    while (index <= maxRange)
                    {
                        rangeOptions.Add(
                            new DeckCardOption
                            {
                                OptionId = index.ToString(),
                                Content = index.ToString(),
                                NumberOfSelection = 0
                            }
                        );

                        index += incremental;
                    }

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_range_answered_by_pulse",
                        new List<string>(), new List<string> { "pulse_id" }));

                    RowSet rangeRowset = analyticSession.Execute(ps.Bind(card.PulseId));
                    foreach (Row rangeRow in rangeRowset)
                    {
                        int rangeValue = rangeRow.GetValue<int>("range_value");
                        string answeredUserId = rangeRow.GetValue<string>("answered_by_user_id");

                        DeckCardOption selectedOption = rangeOptions.FirstOrDefault(o => o.OptionId.Equals(rangeValue.ToString()));

                        bool isToBeIncluded = false;
                        if (string.IsNullOrEmpty(searchId))
                        {
                            isToBeIncluded = true;
                        }
                        else
                        {
                            if (departmentManager.GetAllDepartmentByUserId(answeredUserId, companyId, mainSession).Departments.FirstOrDefault(d => d.Id.Equals(searchId)) != null)
                            {
                                isToBeIncluded = true;
                            }
                        }

                        if (isToBeIncluded)
                        {
                            if (selectedOption != null)
                            {
                                selectedOption.NumberOfSelection++;
                            }

                            totalResponses++;
                        }
                    }

                    rangeOptions = rangeOptions.OrderByDescending(o => Convert.ToInt32(o.OptionId)).ToList();

                    foreach (DeckCardOption option in rangeOptions)
                    {
                        option.PercentageOfSelection = totalResponses == 0 ? 0 : (int)Math.Round(((double)option.NumberOfSelection / totalResponses * 100));
                    }

                    card.Options = rangeOptions;
                }
                else
                {
                    if (card.CardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOne || card.CardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOneWithLogic)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_option_answered_by_pulse",
                            new List<string>(), new List<string> { "pulse_id" }));

                        RowSet optionRowset = analyticSession.Execute(ps.Bind(card.PulseId));
                        foreach (Row rangeRow in optionRowset)
                        {
                            string optionId = rangeRow.GetValue<string>("option_id");
                            string answeredUserId = rangeRow.GetValue<string>("answered_by_user_id");

                            DeckCardOption selectedOption = card.Options.FirstOrDefault(o => o.OptionId.Equals(optionId));

                            bool isToBeIncluded = false;
                            if (string.IsNullOrEmpty(searchId))
                            {
                                isToBeIncluded = true;
                            }
                            else
                            {
                                if (departmentManager.GetAllDepartmentByUserId(answeredUserId, companyId, mainSession).Departments.FirstOrDefault(d => d.Id.Equals(searchId)) != null)
                                {
                                    isToBeIncluded = true;
                                }
                            }

                            if (isToBeIncluded)
                            {
                                selectedOption.NumberOfSelection++;
                                totalResponses++;
                            }
                        }

                        card.Options = card.Options.OrderByDescending(option => option.NumberOfSelection).ToList();

                        foreach (DeckCardOption option in card.Options)
                        {
                            option.PercentageOfSelection = totalResponses == 0 ? 0 : (int)Math.Round(((double)option.NumberOfSelection / totalResponses * 100));
                        }
                    }
                    else if (card.CardType == (int)PulseDynamic.DeckCardTypeEnum.Text)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_sort_by_number_timestamp",
                                    new List<string>(), new List<string> { "pulse_id" }));
                        RowSet customOptionSortRowset = analyticSession.Execute(ps.Bind(card.PulseId));

                        card.TextAnswers = new List<DeckTextAnswer>();

                        int index = 1;
                        foreach (Row customOptionSortRow in customOptionSortRowset)
                        {
                            int numberOfSelection = 0;
                            string customAnswer = customOptionSortRow.GetValue<string>("custom_answer");
                            string capCustomAnswer = char.ToUpper(customAnswer[0]) + customAnswer.Substring(1);

                            if (string.IsNullOrEmpty(searchId))
                            {
                                numberOfSelection = customOptionSortRow.GetValue<int>("number");
                                DeckTextAnswer textAnswer = new DeckTextAnswer
                                {
                                    TextId = index.ToString(),
                                    Content = capCustomAnswer,
                                    NumberOfAlike = numberOfSelection
                                };

                                totalResponses += numberOfSelection;
                                card.TextAnswers.Add(textAnswer);
                                index++;
                            }
                            else
                            {
                                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_by_answer",
                                   new List<string>(), new List<string> { "custom_answer", "pulse_id" }));
                                Row answerRow = analyticSession.Execute(ps.Bind(customAnswer, card.PulseId)).FirstOrDefault();

                                string similarId = answerRow.GetValue<string>("similar_id");

                                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_by_similar",
                                    new List<string>(), new List<string> { "similar_id" }));
                                RowSet customRowset = analyticSession.Execute(ps.Bind(similarId));

                                bool isUpdateIndex = false;
                                foreach (Row customRow in customRowset)
                                {
                                    string answeredUserId = customRow.GetValue<string>("created_by_user_id");
                                    if (departmentManager.GetAllDepartmentByUserId(answeredUserId, companyId, mainSession).Departments.FirstOrDefault(d => d.Id.Equals(searchId)) != null)
                                    {
                                        DeckTextAnswer selectedTextAnswer = card.TextAnswers.FirstOrDefault(ta => ta.Content.Equals(capCustomAnswer));

                                        if (selectedTextAnswer != null)
                                        {
                                            selectedTextAnswer.NumberOfAlike++;
                                        }
                                        else
                                        {
                                            DeckTextAnswer textAnswer = new DeckTextAnswer
                                            {
                                                TextId = index.ToString(),
                                                Content = capCustomAnswer,
                                                NumberOfAlike = 1
                                            };

                                            card.TextAnswers.Add(textAnswer);
                                        }

                                        totalResponses++;
                                        isUpdateIndex = true;
                                    }
                                }

                                if (isUpdateIndex)
                                {
                                    index++;
                                }
                            }

                        }

                        // Take TOP 6 smiliar and then by timestamp
                        //SelectSortedTextAnswer(card, analyticSession);
                    }
                }

                // Chart
                chart.MaxCount = targetedAudienceCount;
                chart.Title = "Respondents";
                chart.Type = (int)ChartType.Pie;
                chart.Stats = new List<Stat>
                {
                    new Stat
                    {
                        Count = totalResponses,
                        Name = "Personnel",
                        Percentage = targetedAudienceCount == 0 ? 0 : (int)Math.Ceiling(((double)totalResponses / targetedAudienceCount * 100))
                    }
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void SelectSortedTextAnswer(PulseDynamic card, ISession analyticSession)
        {
            try
            {
                int limit = 6;

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("deck_custom_option_sort_by_number_timestamp",
                            new List<string>(), new List<string> { "pulse_id" }, limit));
                RowSet customOptionSortRowset = analyticSession.Execute(ps.Bind(card.PulseId));

                card.TextAnswers = new List<DeckTextAnswer>();

                int index = 1;
                foreach (Row customOptionSortRow in customOptionSortRowset)
                {
                    int numberOfSelection = customOptionSortRow.GetValue<int>("number");
                    string customAnswer = customOptionSortRow.GetValue<string>("custom_answer");
                    customAnswer = char.ToUpper(customAnswer[0]) + customAnswer.Substring(1);

                    DeckTextAnswer textAnswer = new DeckTextAnswer
                    {
                        TextId = index.ToString(),
                        Content = customAnswer,
                        NumberOfAlike = numberOfSelection
                    };

                    card.TextAnswers.Add(textAnswer);
                    index++;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectDynamicCardOptionResult(string companyId, string pulseId, int cardType, string optionId, double timeOffset, PulseSelectDeckCardOptionAnalyticResponse response, ISession analyticSession, ISession mainSession)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                RowSet optionResultRowset = null;
                if (cardType != (int)PulseDynamic.DeckCardTypeEnum.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_option_answered_by_pulse", new List<string>(), new List<string> { "pulse_id", "option_id" }));
                    optionResultRowset = analyticSession.Execute(ps.Bind(pulseId, optionId));
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_range_answered_by_pulse", new List<string>(), new List<string> { "pulse_id", "range_value" }));
                    optionResultRowset = analyticSession.Execute(ps.Bind(pulseId, Convert.ToInt32(optionId)));
                }

                User userManager = new User();
                foreach (Row optionResultRow in optionResultRowset)
                {
                    string answeredByUserId = optionResultRow.GetValue<string>("answered_by_user_id");
                    DateTime answeredOnTimestamp = optionResultRow.GetValue<DateTime>("answered_on_timestamp");
                    User answeredUser = userManager.SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User;

                    if (answeredUser != null)
                    {
                        if (answeredUser.Status.Code == User.AccountStatus.CODE_DELETED)
                        {
                            if (response.Departments.Any(d => d.Id.Equals("Deleted")))
                            {
                                Department department = response.Departments.FirstOrDefault(d => d.Id.Equals("Deleted"));
                                department.CountOfUsers++;
                            }
                            else
                            {
                                Department department = new Department
                                {
                                    Id = "Deleted",
                                    CountOfUsers = 1,
                                    Title = "Deleted"
                                };
                                response.Departments.Add(department);
                            }

                            answeredUser.Departments = new List<Department>
                            {
                                new Department
                                {
                                    Id = "Deleted",
                                    CountOfUsers = 1,
                                    Title ="Deleted"
                                }
                            };
                        }
                        else
                        {
                            if (response.Departments.Any(d => d.Id == answeredUser.Departments[0].Id))
                            {
                                Department department = response.Departments.FirstOrDefault(d => d.Id == answeredUser.Departments[0].Id);
                                department.CountOfUsers++;
                            }
                            else
                            {
                                Department department = new Department
                                {
                                    Id = answeredUser.Departments[0].Id,
                                    CountOfUsers = 1,
                                    Title = answeredUser.Departments[0].Title
                                };
                                response.Departments.Add(department);
                            }
                        }

                        response.Responders.Add(new PulseSelectDeckCardOptionAnalyticResponse.DeckCardOptionResponder
                        {
                            Responder = answeredUser,
                            AnsweredOnTimestampString = answeredOnTimestamp.AddHours(timeOffset).ToString("dd MMMM yyyy")
                        });
                    }

                }

                response.Departments = response.Departments.OrderByDescending(department => department.CountOfUsers).ToList();
                response.Responders = response.Responders.OrderBy(r => r.Responder.FirstName).ToList();
                response.Pulse.TotalRespondents = response.Responders.Count();
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectDeckCardCustomAnswersResult(PulseDynamic pulse, bool isAnonymous, string companyId, ISession analyticSession, ISession mainSession)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_sort_by_number_timestamp",
                            new List<string>(), new List<string> { "pulse_id" }));
                RowSet customOptionSortRowset = analyticSession.Execute(ps.Bind(pulse.PulseId));

                pulse.TextAnswers = new List<DeckTextAnswer>();

                User userManager = new User();

                int numberOfResponses = 0;
                foreach (Row customOptionSortRow in customOptionSortRowset)
                {
                    string lowerCustomAnswer = customOptionSortRow.GetValue<string>("custom_answer");

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_by_answer",
                            new List<string>(), new List<string> { "custom_answer", "pulse_id" }));
                    Row similarRow = analyticSession.Execute(ps.Bind(lowerCustomAnswer, pulse.PulseId)).FirstOrDefault();

                    if (similarRow != null)
                    {
                        string similarId = similarRow.GetValue<string>("similar_id");
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_by_similar",
                           new List<string>(), new List<string> { "similar_id" }));
                        RowSet similarOptionRowset = analyticSession.Execute(ps.Bind(similarId));

                        foreach (Row similarOptionRow in similarOptionRowset)
                        {
                            string optionId = similarOptionRow.GetValue<string>("option_id");
                            string answeredByUserId = similarOptionRow.GetValue<string>("created_by_user_id");
                            string customAnswer = similarOptionRow.GetValue<string>("content");

                            DeckTextAnswer textAnswer = new DeckTextAnswer
                            {
                                TextId = optionId,
                                Content = customAnswer,
                                NumberOfAlike = 0,
                                AnsweredByUser = isAnonymous ? null : userManager.SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User
                            };

                            pulse.TextAnswers.Add(textAnswer);
                            numberOfResponses++;
                        }
                    }
                }

                pulse.TotalRespondents = numberOfResponses;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectDeckAnalyticByUser(PulseSelectDeckAnalyticByUserResponse response, string answeredUserId, double timezoneOffset, ISession analyticSession)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = null;

                PulseDeck deck = response.Deck;
                DateTime? lastUpdatedTime = null;

                foreach (PulseDynamic pulse in deck.Pulses)
                {
                    DateTime? answeredTime = null;
                    if (pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOne || pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOneWithLogic)
                    {
                        pulse.SelectedOption = new DeckCardOption
                        {
                            OptionId = string.Empty,
                            Content = string.Empty
                        };

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_option_answered_by_user",
                          new List<string>(), new List<string> { "answered_by_user_id", "pulse_id" }));
                        Row optionRow = analyticSession.Execute(ps.Bind(answeredUserId, pulse.PulseId)).FirstOrDefault();
                        if (optionRow != null)
                        {
                            string optionId = optionRow.GetValue<string>("option_id");
                            int optionNumber = Convert.ToInt16(optionId.Replace(pulse.PulseId + "_", ""));
                            answeredTime = optionRow.GetValue<DateTime?>("answered_on_timestamp");

                            switch (optionNumber)
                            {
                                case 1:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option1Id,
                                        Content = pulse.Option1Content
                                    };
                                    break;
                                case 2:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option2Id,
                                        Content = pulse.Option2Content
                                    };
                                    break;
                                case 3:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option3Id,
                                        Content = pulse.Option3Content
                                    };
                                    break;
                                case 4:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option4Id,
                                        Content = pulse.Option4Content
                                    };
                                    break;
                                case 5:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option5Id,
                                        Content = pulse.Option5Content
                                    };
                                    break;
                                case 6:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option6Id,
                                        Content = pulse.Option6Content
                                    };
                                    break;
                                case 7:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option7Id,
                                        Content = pulse.Option7Content
                                    };
                                    break;
                                case 8:
                                    pulse.SelectedOption = new DeckCardOption
                                    {
                                        OptionId = pulse.Option8Id,
                                        Content = pulse.Option8Content
                                    };
                                    break;
                            }
                        }
                    }
                    else if (pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.NumberRange)
                    {
                        pulse.SelectedRange = Convert.ToInt32(DefaultResource.DeckCardRangeNotSelected);

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_range_answered_by_user",
                         new List<string>(), new List<string> { "answered_by_user_id", "pulse_id" }));
                        Row rangeRow = analyticSession.Execute(ps.Bind(answeredUserId, pulse.PulseId)).FirstOrDefault();

                        if (rangeRow != null)
                        {
                            pulse.SelectedRange = rangeRow.GetValue<int>("range_value");
                            answeredTime = rangeRow.GetValue<DateTime?>("answered_on_timestamp");
                        }
                    }
                    else if (pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.Text)
                    {
                        pulse.TextAnswer = new DeckTextAnswer
                        {
                            TextId = string.Empty,
                            Content = string.Empty
                        };

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("deck_custom_option_created_by_user",
                         new List<string>(), new List<string> { "created_by_user_id", "pulse_id" }));
                        Row textRow = analyticSession.Execute(ps.Bind(answeredUserId, pulse.PulseId)).FirstOrDefault();

                        if (textRow != null)
                        {
                            pulse.TextAnswer = new DeckTextAnswer
                            {
                                TextId = textRow.GetValue<string>("option_id"),
                                Content = textRow.GetValue<string>("content")
                            };

                            answeredTime = textRow.GetValue<DateTime?>("created_on_timestamp");
                        }
                    }

                    if (!lastUpdatedTime.HasValue)
                    {
                        if (answeredTime.HasValue)
                        {
                            lastUpdatedTime = answeredTime;
                        }
                    }
                    else
                    {
                        if (answeredTime.HasValue && answeredTime > lastUpdatedTime)
                        {
                            lastUpdatedTime = answeredTime;
                        }
                    }
                }

                response.LastUpdatedDateString = lastUpdatedTime.HasValue ? lastUpdatedTime.Value.AddHours(timezoneOffset).ToString("dd/MM/yyyy") : string.Empty;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectRespondentChartPerCard(PulseDeck deck, int targetedAudienceCount, string companyId, string searchId, ISession analyticSession, ISession mainSession)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;

                foreach (PulseDynamic pulse in deck.Pulses)
                {
                    string tableName = string.Empty;
                    string columnName = string.Empty;

                    if (pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOne || pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.SelectOneWithLogic)
                    {
                        tableName = "deck_option_answered_by_pulse";
                        columnName = "answered_by_user_id";
                    }
                    else if (pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.NumberRange)
                    {
                        tableName = "deck_range_answered_by_pulse";
                        columnName = "answered_by_user_id";
                    }
                    else if (pulse.CardType == (int)PulseDynamic.DeckCardTypeEnum.Text)
                    {
                        tableName = "deck_custom_option_created_by_pulse";
                        columnName = "created_by_user_id";
                    }

                    int count = 0;
                    if (string.IsNullOrEmpty(searchId))
                    {
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement(tableName, new List<string> { "pulse_id" }));
                        Row optionRow = analyticSession.Execute(ps.Bind(pulse.PulseId)).FirstOrDefault();
                        count = (int)optionRow.GetValue<long>("count");
                        pulse.RespondentChart = new Chart
                        {
                            Type = (int)ChartType.Bar,
                            MaxCount = targetedAudienceCount,
                            Stats = new List<Stat>
                            {
                                new Stat
                                {
                                    Count = count,
                                    Percentage = targetedAudienceCount == 0 ? 0 : (int)Math.Ceiling(((double)count / targetedAudienceCount) * 100)
                                }
                            }
                        };
                    }
                    else
                    {
                        Department departmentManger = new Department();

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement(tableName, new List<string>(), new List<string> { "pulse_id" }));
                        RowSet optionRowSet = analyticSession.Execute(ps.Bind(pulse.PulseId));

                        foreach (Row optionRow in optionRowSet)
                        {
                            string answeredByUserId = optionRow.GetValue<string>(columnName);
                            List<Department> departments = departmentManger.GetAllDepartmentByUserId(answeredByUserId, companyId, mainSession).Departments;

                            if (departments.FirstOrDefault(d => d.Id.Equals(searchId)) != null)
                            {
                                count++;
                            }
                        }

                        pulse.RespondentChart = new Chart
                        {
                            Type = (int)ChartType.Bar,
                            MaxCount = targetedAudienceCount,
                            Stats = new List<Stat>
                            {
                                new Stat
                                {
                                    Count = count,
                                    Percentage = targetedAudienceCount == 0 ? 0 : (int)Math.Ceiling(((double)count / targetedAudienceCount) * 100)
                                }
                            }
                        };
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

    }
}
