﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    /// <summary>
    /// Hard Code mode.
    /// </summary>
    [Serializable]
    [DataContract]
    public class Module
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        #region Company modules
        public const int MODULE_PERSONNEL = 1;          // For CompanyAdmin
        public const int MODULE_MATCHUP = 2;            // For CompanyAdmin and Moderator
        public const int MODULE_FEED = 3;               // For CompanyAdmin
        public const int MODULE_DASHBOARD = 4;          // For CompanyAdmin and Moderator
        public const int MODULE_ANALYTICS = 5;          // For CompanyAdmin and Moderator
        public const int MODULE_COMPANY_PROFILE = 6;    // For CompanyAdmin
        public const int MODULE_EVENT = 7;              // For CompanyAdmin and Moderator
        public const int MODULE_SURVEY = 8;             // For CompanyAdmin and Moderator
        public const int MODULE_DYNAMIC_PULSE = 9;      // For CompanyAdmin and Moderator
        public const int MODULE_SETTING = 10;           // For CompanyAdmin
        public const int MODULE_MARTET_PLACE = 11;      // For CompanyAdmin
        public const int MODULE_ADMIN_LOG = 12;         // For CompanyAdmin
        public const int MODULE_GAMIFICATION = 13;      // For CompanyAdmin and Moderator
        public const int MODULE_ANNOUNCEMENT = 14;      // For CompanyAdmin and Moderator
        public const int MODULE_BILLING = 15;           // For CompanyAdmin
        public const int MODULE_MLEARNING = 16;         // For CompanyAdmin
        public const int MODULE_LIVE360 = 17;           // For CompanyAdmin
        #endregion

        #region System modules
        public const int MODULE_ASSESSMENT = 901;       // For SuperAdmin and CocadreAdmin
        public const int MODULE_MODULE = 902;           // For SuperAdmin and CocadreAdmin
        #endregion

        [DataMember]
        public int Key { get; set; }
        [DataMember]
        public bool IsUsing { get; set; }
        [DataMember]
        public String Title { get; set; }
        [DataMember]
        public String IconUrl { get; set; }
        [DataMember]
        public string IconUrlOn { get; set; }
        [DataMember]
        public string IconUrlOff { get; set; }
        [DataMember]
        public String Description { get; set; }
        [DataMember]
        public String RightsDescription { get; set; }
        [DataMember]
        public String RootPath { get; set; }

        public Module()
        {
        }

        public Module(int key)
        {
            Key = key;
            switch (key)
            {
                case MODULE_PERSONNEL:
                    IsUsing = true;
                    Title = "Personnel";
                    IconUrl = "icon-personnel.png";
                    Description = @"Add, rename and manage users";
                    RightsDescription = @"";
                    RootPath = "Personnel";
                    break;

                case MODULE_MATCHUP:
                    IsUsing = true;
                    Title = "Match Up";
                    IconUrl = "icon-quiz.png";
                    Description = @"Create Topic and Questions";
                    RightsDescription = @"Add, modify and delete Quiz category, topic and question";
                    RootPath = "MatchUp";
                    break;

                case MODULE_FEED:
                    IsUsing = true;
                    Title = "Feed";
                    IconUrl = "icon-feed.png";
                    Description = @"Enable users to post on Feed";
                    RightsDescription = @"";
                    RootPath = "Feed";
                    break;

                case MODULE_DASHBOARD:
                    IsUsing = true;
                    Title = "Dashboard";
                    IconUrl = "icon-dashboard.png";
                    Description = @"Manager notifications and dashboard";
                    RightsDescription = @"View the alert section of the ""Reported Post"" (including private post), respond to and delete comments or posts. Approve profile photos and reply feedbacks";
                    RootPath = "Dashboard";
                    break;

                case MODULE_ANALYTICS:
                    IsUsing = true;
                    Title = "Analytics";
                    IconUrl = "icon-analytics.png";
                    Description = @"Track usage of services";
                    RightsDescription = @"Access to Analytic reports";
                    RootPath = "Analytics";
                    break;

                case MODULE_COMPANY_PROFILE:
                    IsUsing = true;
                    Title = "Company Profile";
                    IconUrl = "icon-companyprofile.png";
                    Description = @"Update information about your company";
                    RightsDescription = @"";
                    RootPath = "CompanyProfile";
                    break;

                case MODULE_ADMIN_LOG:
                    IsUsing = false;
                    Title = "Admin Log";
                    IconUrl = "icon-adminlog.png";
                    Description = @"History";
                    RightsDescription = @"";
                    RootPath = "AdminLog";
                    break;

                case MODULE_SETTING:
                    IsUsing = true;
                    Title = "Setting";
                    IconUrl = "icon-support.png";
                    Description = @"Console setting";
                    RightsDescription = @"";
                    RootPath = "Setting";
                    break;

                case MODULE_ANNOUNCEMENT:
                    IsUsing = true;
                    Title = "Announcement";
                    IconUrl = "icon-announcement.png";
                    Description = @"Post announcements and notifications";
                    RightsDescription = @"Post or modify announcements and notifications as a moderator";
                    RootPath = "Announcement";
                    break;

                case MODULE_BILLING:
                    IsUsing = false;
                    Title = "Billing";
                    IconUrl = "icon-billing.png";
                    Description = @"View charges and manage licenses";
                    RightsDescription = @"";
                    RootPath = "Billing";
                    break;

                case MODULE_MARTET_PLACE:
                    IsUsing = false;
                    Title = "Marketplace";
                    IconUrl = "icon-marketplace.png";
                    Description = @"Cocadre settings and support, help";
                    RightsDescription = @"";
                    RootPath = "Marketplace";
                    break;

                case MODULE_EVENT:
                    IsUsing = true;
                    Title = "Event";
                    IconUrl = "icon-event.png";
                    Description = @"Manage all events";
                    RightsDescription = @"Create Quiz Event and view ongoing result before the end of the event";
                    RootPath = "Event";
                    break;

                case MODULE_GAMIFICATION:
                    IsUsing = true;
                    Title = "Gamification";
                    IconUrl = "icon-gamification.png";
                    Description = @"Gamification settings";
                    RightsDescription = @"Change, reset and backup game-related components of CoCadre";
                    RootPath = "Gamification";
                    break;

                case MODULE_SURVEY:
                    IsUsing = true;
                    Title = "Responsive Survey";
                    IconUrl = "icon-survey.png";
                    Description = @"Manage your Responsive Survey";
                    RightsDescription = @"Survey creation and report";
                    RootPath = "Survey";
                    break;

                case MODULE_DYNAMIC_PULSE:
                    IsUsing = true;
                    Title = "Dynamic Pulse";
                    IconUrl = "icon-dynamicpulse.png";
                    Description = @"Dynamic Pulse Survey";
                    RightsDescription = @"Survey creation and report";
                    RootPath = "DynamicPulse";
                    break;

                case MODULE_ASSESSMENT:
                    IsUsing = true;
                    Title = "Assessment";
                    IconUrl = "icon-assessment.png";
                    Description = @"Assessment Pulse Survey";
                    RightsDescription = @"Assessment creation and report";
                    RootPath = "Assessment";
                    break;

                case MODULE_MLEARNING:
                    IsUsing = true;
                    Title = "M Learning";
                    IconUrl = "icon-mlearning.png";
                    Description = @"Mobile Learning Console";
                    RightsDescription = @"Bite-size learning & tests";
                    RootPath = "MLearning";
                    break;

                case MODULE_MODULE:
                    IsUsing = true;
                    Title = "Activation";
                    IconUrl = "icon-assessment.png";
                    Description = @"Activation console";
                    RightsDescription = @"Activation console";
                    RootPath = "Module";
                    break;

                case MODULE_LIVE360:
                    IsUsing = true;
                    Title = "Live360";
                    IconUrl = "icon-live360.png";
                    Description = @"Live360 console";
                    RightsDescription = @"Live360 console";
                    RootPath = "Live360";
                    break;

                default:

                    break;
            }
        }

        public static List<Module> GetModules(int accountTypeCode, string companyId = null)
        {
            List<Module> modules = new List<Module>();
            if (accountTypeCode == User.AccountType.CODE_SUPER_ADMIN || accountTypeCode == User.AccountType.CODE_COCADRE_ADMIN || accountTypeCode == User.AccountType.CODE_ADMIN)
            {   
                modules.Add(new Module(MODULE_PERSONNEL));
                modules.Add(new Module(MODULE_MATCHUP));
                modules.Add(new Module(MODULE_FEED));
                modules.Add(new Module(MODULE_DASHBOARD));
                modules.Add(new Module(MODULE_ANALYTICS));
                modules.Add(new Module(MODULE_COMPANY_PROFILE));
                modules.Add(new Module(MODULE_EVENT));
                modules.Add(new Module(MODULE_SURVEY));
                modules.Add(new Module(MODULE_DYNAMIC_PULSE));
                modules.Add(new Module(MODULE_SETTING));
                modules.Add(new Module(MODULE_GAMIFICATION));
                modules.Add(new Module(MODULE_MLEARNING));
                modules.Add(new Module(MODULE_LIVE360));
            }
            else if (accountTypeCode == User.AccountType.CODE_ADMIN)
            {
                List<Module> ms = new Module().GetAllModules(companyId);
                for (int i = 0; i < ms.Count; i++)
                {
                    if (ms[i].IsUsing)
                    {
                        modules.Add(ms[i]);
                    }
                }
            }
            else if (accountTypeCode == User.AccountType.CODE_MODERATER)
            {
                List<Module> ms = new Module().GetAllModules(companyId);
                for (int i = 0; i < ms.Count; i++)
                {
                    if (ms[i].IsUsing)
                    {
                        if (ms[i].Key == MODULE_MATCHUP || ms[i].Key == MODULE_DASHBOARD || ms[i].Key == MODULE_ANALYTICS || ms[i].Key == MODULE_EVENT || ms[i].Key == MODULE_SURVEY || ms[i].Key == MODULE_DYNAMIC_PULSE || ms[i].Key == MODULE_MLEARNING)
                        {
                            modules.Add(ms[i]);
                        }
                    }
                }
            }
            return modules;
        }

        public static List<Module> GetSystemModules(int accountTypeCode)
        {
            List<Module> modules = new List<Module>();

            if (accountTypeCode == User.AccountType.CODE_SUPER_ADMIN || accountTypeCode == User.AccountType.CODE_COCADRE_ADMIN)
            {
                modules.Add(new Module(MODULE_ASSESSMENT));
                modules.Add(new Module(MODULE_MODULE));
            }

            return modules;
        }

        public List<Module> GetAllModules()
        {
            List<Module> modules = new List<Module>();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                PreparedStatement ps = null;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("system_module", new List<string>(), new List<string> { }));
                RowSet rsModules = mainSession.Execute(ps.Bind());

                if (rsModules != null)
                {
                    foreach (Row rModule in rsModules)
                    {
                        Module module = new Module
                        {
                            Key = rModule.GetValue<int>("id"),
                            Title = rModule.GetValue<string>("title"),
                            Description = rModule.GetValue<string>("description"),
                            RightsDescription = rModule.GetValue<string>("rights_description"),
                            IconUrlOn = rModule.GetValue<string>("icon_url_on"),
                            IconUrlOff = rModule.GetValue<string>("icon_url_off"),
                            IsUsing = rModule.GetValue<bool>("is_using"),
                            RootPath = rModule.GetValue<string>("root_path")
                        };
                        modules.Add(module);
                    }
                }
                else
                {
                    // Error!
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return modules;
        }

        public List<Module> GetAllModules(string companyId, ISession mainSession = null)
        {
            List<Module> modules = new List<Module>();
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;

                // Step 1: Get a list of all modules
                #region Step 1: Get a list of all modules
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("system_module", new List<string>(), new List<string> { }));
                RowSet rsModules = mainSession.Execute(ps.Bind());

                if (rsModules != null)
                {
                    foreach (Row rModule in rsModules)
                    {
                        Module module = new Module
                        {
                            Key = rModule.GetValue<int>("id"),
                            Title = rModule.GetValue<string>("title"),
                            Description = rModule.GetValue<string>("description"),
                            RightsDescription = rModule.GetValue<string>("rights_description"),
                            IconUrl = rModule.GetValue<string>("icon_url_on"),
                            IconUrlOn = rModule.GetValue<string>("icon_url_on"),
                            IconUrlOff = rModule.GetValue<string>("icon_url_off"),
                            IsUsing = rModule.GetValue<bool>("is_using"),
                            RootPath = rModule.GetValue<string>("root_path")
                        };
                        modules.Add(module);
                    }
                }
                else
                {
                    // Error!
                }
                #endregion 

                // Step 2: Get a list of module ids mapped to company
                #region Step 2: Get a list of module ids mapped to company
                // Row companySettingRow = mainSession.Execute(ps.Bind(companyId)).FirstOrDefault();
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("system_module_by_company", new List<string>(), new List<string> { "company_id" }));
                RowSet rsCompanyModules = mainSession.Execute(ps.Bind(companyId));

                List<int> module_id_list = new List<int>();
                if (rsCompanyModules != null)
                {
                    foreach (Row rModule in rsCompanyModules)
                    {
                        module_id_list.Add(rModule.GetValue<int>("module_id"));
                    }
                }
                else
                {
                    // Error!
                }
                #endregion 

                // Step 3: Get the exact modules based on module
                List<Module> company_modules = new List<Module>();
                foreach (int moduleId in module_id_list)
                {
                    Module foundModule = modules.Find(r => r.Key == moduleId);
                    if (foundModule != null)
                    {
                        company_modules.Add(foundModule);
                    }
                }
                modules = company_modules;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return modules;
        }

        public CompanySystemModuleDeleteResponse DeleteCompanySystemModule(string companyId, int moduleId)
        {
            CompanySystemModuleDeleteResponse response = new CompanySystemModuleDeleteResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.DeleteStatement("system_module_by_company", new List<string> { "company_id", "module_id" }));
                batch.Add(ps.Bind(companyId, moduleId));
                session.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySystemModuleCreateResponse CreateCompanySystemModule(string companyId, int moduleId)
        {
            CompanySystemModuleCreateResponse response = new CompanySystemModuleCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("system_module_by_company", new List<string> { "company_id", "module_id" }));
                batch.Add(ps.Bind(companyId, moduleId));
                session.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public bool CheckModuleUnlocked(string companyId, int moduleId, ISession mainSession)
        {
            bool isUnlocked = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("system_module_by_company", new List<string>(), new List<string> { "company_id", "module_id" }));
                Row moduleRow = mainSession.Execute(ps.Bind(companyId, moduleId)).FirstOrDefault();
                if (moduleRow != null)
                {
                    isUnlocked = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isUnlocked;
        }
    }
}
