﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class Coach : User
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember]
        public List<Assessment> Assessments { get; set; }

        [DataMember]
        public int NumberOfAssessments { get; set; }

        public CoachCreateResponse CreateCoach(string coachName, string adminUserId, bool returnErrorIfNameFound = false, ISession session = null)
        {
            CoachCreateResponse response = new CoachCreateResponse();
            response.CoachId = string.Empty;
            response.Success = false;
            try
            {
                // Check for admin
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement ps = null;
                coachName = coachName.Trim();
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_coach", new List<string>(), new List<string>()));
                RowSet coachRowset = session.Execute(ps.Bind());

                Row coachRow = coachRowset.FirstOrDefault(r => string.Format("{0} {1}", r.GetValue<string>("first_name"), r.GetValue<string>("last_name")).ToLower().Equals(coachName.ToLower()));
                if (coachRow != null)
                {
                    if (returnErrorIfNameFound)
                    {
                        response.ErrorCode = Convert.ToInt16(ErrorCode.CoachNameTaken);
                        response.ErrorMessage = ErrorMessage.CoachNameTaken;
                        return response;
                    }

                    response.CoachId = coachRow.GetValue<string>("id");
                }
                else
                {
                    DateTime currentTime = DateTime.UtcNow;
                    string coachId = UUIDGenerator.GenerateUniqueIDForCoach();
                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_coach", new List<string> { "id", "first_name", "created_by_user_id", "last_modified_by_user_id", "created_on_timestamp", "last_modified_timestamp", "is_valid" }));
                    session.Execute(ps.Bind(coachId, coachName, adminUserId, adminUserId, currentTime, currentTime, true));

                    response.CoachId = coachId;
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CoachUpdateWithStatementResponse LinkCoachToAssessment(string assessmentId, string newCoachId, ISession session, string currentCoachId = null)
        {
            CoachUpdateWithStatementResponse response = new CoachUpdateWithStatementResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;

                if (!string.IsNullOrEmpty(currentCoachId))
                {
                    response.DeleteStatements.Add(RemoveLinkToAssessment(assessmentId, currentCoachId, session));
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("assessment_by_coach", new List<string> { "assessment_id", "coach_id" }));
                response.UpdateStatements.Add(ps.Bind(assessmentId, newCoachId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public BoundStatement RemoveLinkToAssessment(string assessmentId, string coachId, ISession session)
        {
            BoundStatement bs = null;
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_by_coach", new List<string> { "assessment_id", "coach_id" }));
                bs = ps.Bind(assessmentId, coachId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return bs;
        }

        public CoachSelectResponse SelectCoach(string adminUserId, string coachId, ISession session = null)
        {
            CoachSelectResponse response = new CoachSelectResponse();
            response.Coach = new Coach();
            response.Success = false;
            try
            {
                // Check for admin
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_coach", new List<string>(), new List<string> { "id", "is_valid" }));
                Row coachRow = session.Execute(ps.Bind(coachId, true)).FirstOrDefault();

                if (coachRow != null)
                {
                    response.Coach = new Coach
                    {
                        UserId = coachRow.GetValue<string>("id"),
                        FirstName = coachRow.GetValue<string>("first_name")
                    };
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CoachSelectAllResponse SelectAllCoaches(string adminUserId, ISession session = null)
        {
            CoachSelectAllResponse response = new CoachSelectAllResponse();
            response.Coaches = new List<Coach>();
            response.Success = false;
            try
            {
                // Check for admin
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_coach", new List<string>(), new List<string> { "is_valid" }));
                RowSet coachRowset = session.Execute(ps.Bind(true));

                foreach (Row coachRow in coachRowset)
                {
                    string userId = coachRow.GetValue<string>("id");
                    ps = session.Prepare(CQLGenerator.CountStatement("assessment_by_coach", new List<string> { "coach_id" }));
                    int count = (int)session.Execute(ps.Bind(userId)).FirstOrDefault().GetValue<long>("count");
                    Coach coach = new Coach
                    {
                        UserId = userId,
                        FirstName = coachRow.GetValue<string>("first_name"),
                        NumberOfAssessments = count
                    };
                    response.Coaches.Add(coach);
                }
                response.Coaches = response.Coaches.OrderBy(c => c.FirstName).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CoachUpdateResponse UpdateCoach(string adminUserId, string coachId, string newCoachName, ISession session = null)
        {
            CoachUpdateResponse response = new CoachUpdateResponse();
            response.Success = false;
            try
            {
                // Check for admin
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement ps = null;
                newCoachName = newCoachName.Trim();

                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_coach", new List<string>(), new List<string>()));
                RowSet coachRowset = session.Execute(ps.Bind());
                List<Row> coachRowList = coachRowset.ToList();

                if (coachRowList.FirstOrDefault(r => r.GetValue<string>("id").Equals(coachId)) == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CoachInvalid);
                    response.ErrorMessage = ErrorMessage.CoachInvalid;
                    return response;
                }

                Row sameNameCoachRow = coachRowset.FirstOrDefault(r => string.Format("{0} {1}", r.GetValue<string>("first_name"), r.GetValue<string>("last_name")).ToLower().Equals(newCoachName.ToLower()));
                if (sameNameCoachRow != null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CoachNameTaken);
                    response.ErrorMessage = ErrorMessage.CoachNameTaken;
                    return response;
                }

                ps = session.Prepare(CQLGenerator.UpdateIfExistsStatement("assessment_coach", new List<string> { "id" }, new List<string> { "first_name", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(ps.Bind(newCoachName, adminUserId, DateTime.UtcNow, coachId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CoachUpdateResponse DeleteCoach(string adminUserId, string coachId, ISession session = null)
        {
            CoachUpdateResponse response = new CoachUpdateResponse();
            response.Success = false;
            try
            {
                // Check for admin
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement ps = null;

                Coach selectedCoach = SelectCoach(adminUserId, coachId, session).Coach;
                if (selectedCoach == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CoachInvalid);
                    response.ErrorMessage = ErrorMessage.CoachInvalid;
                    return response;
                }

                if (selectedCoach.NumberOfAssessments > 0)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CoachCannotDelete);
                    response.ErrorMessage = ErrorMessage.CoachCannotDelete;
                    return response;
                }

                ps = session.Prepare(CQLGenerator.UpdateIfExistsStatement("assessment_coach", new List<string> { "id" }, new List<string> { "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(ps.Bind(false, adminUserId, DateTime.UtcNow, coachId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}
