﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLEducationCategory
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum MLCategoryQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember(EmitDefaultValue = false)]
        public string CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember]
        public int NumberOfMLEducation { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<MLEducation> Educations { get; set; }

        public MLEducationCategoryCreateResponse Create(string companyId, string title, string adminUserId, ISession session = null, string categoryId = null)
        {
            MLEducationCategoryCreateResponse response = new MLEducationCategoryCreateResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Category title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLEducationCategoryTitleMissing);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryTitleMissing;
                    return response;
                }

                if (string.IsNullOrEmpty(categoryId))
                {
                    categoryId = UUIDGenerator.GenerateUniqueIDForMLEducationCategory();
                }

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_category", new List<string> { "id", "company_id", "title", "is_valid", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batchStatement.Add(ps.Bind(categoryId, companyId, title, true, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_category_by_title", new List<string> { "ml_education_category_id", "company_id", "title" }));
                batchStatement.Add(ps.Bind(categoryId, companyId, title));

                session.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCategoryDetailResponse GetDetail(string categoryId, string companyId, ISession session = null)
        {
            MLEducationCategoryDetailResponse response = new MLEducationCategoryDetailResponse();
            response.Category = null;
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);

                if (categoryRow != null)
                {
                    MLEducationCategory category = new MLEducationCategory
                    {
                        CategoryId = categoryRow.GetValue<string>("id"),
                        Title = categoryRow.GetValue<string>("title")
                    };

                    response.Category = category;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCategoryListResponse GetList(string adminUserId, string companyId, int queryType, ISession session = null)
        {
            MLEducationCategoryListResponse response = new MLEducationCategoryListResponse();
            response.Categories = new List<MLEducationCategory>();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_category_by_title", new List<string>(), new List<string> { "company_id" }));
                RowSet categoryRowset = session.Execute(ps.Bind(companyId));
                List<Row> categoryRowList = categoryRowset.ToList();

                if (categoryRowList.Count == 0)
                {
                    string eventCategoryId = UUIDGenerator.GenerateUniqueIDForMLCategory();
                    string title = "Event";
                    Create(companyId, title, adminUserId, session, eventCategoryId);
                    MLEducationCategory category = new MLEducationCategory
                    {
                        CategoryId = eventCategoryId,
                        Title = title,
                        NumberOfMLEducation = 0
                    };

                    response.Categories.Add(category);
                }
                else
                {
                    foreach (Row categoryRow in categoryRowList)
                    {
                        string categoryId = categoryRow.GetValue<string>("ml_education_category_id");
                        string title = categoryRow.GetValue<string>("title");

                        int numberOfTopics = 0;
                        if (queryType == (int)MLCategoryQueryType.FullDetail)
                        {
                            // Get number of topics
                            ps = session.Prepare(CQLGenerator.SelectStatement("ml_education", new List<string>(), new List<string> { "category_id" }));
                            RowSet rs = session.Execute(ps.Bind(categoryId));
                            if (rs != null)
                            {
                                foreach (Row row in rs)
                                {
                                    if (row.GetValue<int>("status") != (int)MLTopic.MLTopicStatusEnum.Deleted)
                                    {
                                        numberOfTopics++;
                                    }
                                }
                            }
                        }

                        MLEducationCategory category = new MLEducationCategory
                        {
                            CategoryId = categoryId,
                            Title = title,
                            NumberOfMLEducation = numberOfTopics
                        };

                        response.Categories.Add(category);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public MLEducationCategoryUpdaateResponse Update(string adminUserId, string companyId, string categoryId, string newTitle)
        {
            MLEducationCategoryUpdaateResponse response = new MLEducationCategoryUpdaateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row rsCategoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);

                if (rsCategoryRow == null)
                {
                    Log.Error("ML education category is invalid: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLEducationCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Category title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLEducationCategoryTitleMissing);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryTitleMissing;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                string currentTitle = rsCategoryRow.GetValue<string>("title");

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("ml_education_category",
                    new List<string> { "company_id", "id" }, new List<string> { "title", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, adminUserId, DateTime.UtcNow, companyId, categoryId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_category_by_title",
                    new List<string> { "company_id", "title", "ml_education_category_id" }));
                deleteBatch.Add(ps.Bind(companyId, currentTitle, categoryId));

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_category_by_title",
                    new List<string> { "company_id", "title", "ml_education_category_id" }));
                updateBatch.Add(ps.Bind(companyId, newTitle, categoryId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCategoryDeleteResponse Delete(string adminUserId, string companyId, string categoryId)
        {
            MLEducationCategoryDeleteResponse response = new MLEducationCategoryDeleteResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row rsCategoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);

                if (rsCategoryRow == null)
                {
                    Log.Error("MLTopic category is invalid: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLEducationCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithLimit("ml_education_by_category",
                    new List<string>(), new List<string> { "ml_category_id" }, 1));

                Row categoryRelation = session.Execute(ps.Bind(categoryId)).FirstOrDefault();

                if (categoryRelation != null)
                {
                    Log.Error("Education still exists in category: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.MLEducationCategoryCannotBeDeleted);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryCannotBeDeleted;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                string currentTitle = rsCategoryRow.GetValue<string>("title");

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_by_category",
                   new List<string> { "ml_category_id" }));

                deleteBatch.Add(ps.Bind(categoryId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_category_by_title",
                   new List<string> { "company_id", "title", "ml_education_category_id" }));
                deleteBatch.Add(ps.Bind(companyId, currentTitle, categoryId));

                ps = session.Prepare(CQLGenerator.UpdateStatement("ml_education_category",
                    new List<string> { "company_id", "id" }, new List<string> { "is_valid", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(false, adminUserId, DateTime.UtcNow, companyId, categoryId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationCategoryListResponse GetListByUser(string requestUserId, string companyId, string containsName = null, ISession mainSession = null)
        {
            MLEducationCategoryListResponse response = new MLEducationCategoryListResponse();
            response.Categories = new List<MLEducationCategory>();
            response.Success = false;

            try
            {
                if(mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(requestUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_category_by_title", new List<string>(), new List<string> { "company_id" }));
                RowSet rsCategory = mainSession.Execute(ps.Bind(companyId));
                if (rsCategory != null)
                {
                    foreach (Row rowCategory in rsCategory)
                    {
                        MLEducationListResponse responseList = new MLEducation().GetListByUser(
                            requestUserId,
                            companyId,
                            containsName,
                            rowCategory.GetValue<string>("ml_education_category_id"),
                            mainSession
                            );

                        if (responseList.Success && responseList.Educations != null && responseList.Educations.Count > 0)
                        {
                            response.Categories.Add(new MLEducationCategory
                            {
                                CategoryId = rowCategory.GetValue<string>("ml_education_category_id"),
                                Title = rowCategory.GetValue<string>("title"),
                                NumberOfMLEducation = responseList.Educations.Count,
                                Educations = responseList.Educations
                            });
                        }
                    }

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
