﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    public class Gamification
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum GamificationResultTypeEnum
        {
            Draw = 0,
            Lost = -1,
            Win = 1,
        }

        public enum GamificationQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum AchievementTypeEnum
        {
            PlayToGet = 1,
            FailButGet = 2,
            PerfectAndGet = 3
        }

        public enum AchievementRuleTypeEnum
        {
            AllTopics = 1,
            SpecificTopic = 2,
            MultipleTopicsByEither = 3,
            MultipleTopicsByEach = 4,
            WinningStreak = 5,
            LosingStreak = 6
        }

        public enum AchievementIconTemplateType
        {
            WaveyWithSolidLine = 1,
            Curvey = 2,
            WaveyWithDashedLine = 3,
        }

        public enum AchievementStatusEnum
        {
            Live = 1,
            Deleted = -1
        }

        [DataContract]
        public class Achievement
        {
            [DataMember]
            public string Id { get; set; }

            [DataMember]
            public string Title { get; set; }

            [DataMember]
            public string TypeDescription { get; set; }
            [DataMember]
            public string RuleDescription { get; set; }

            [DataMember]
            public int Type { get; set; }

            [DataMember]
            public int RuleType { get; set; }

            [DataMember]
            public int FailTimes { get; set; }

            [DataMember]
            public int PlayTimes { get; set; }

            [DataMember]
            public string IconImageUrl { get; set; }

            [DataMember]
            public string IconColor { get; set; }

            [DataMember]
            public int TemplateType { get; set; }

            [DataMember]
            public bool IsIconCustomized { get; set; }

            [DataMember]
            public int Ordering { get; set; }

            [DataMember]
            public List<Topic> Topics { get; set; }

            [DataMember]
            public bool IsUnlocked { get; set; }

            [DataMember]
            public bool IsSeen { get; set; }

            [DataMember]
            public int NumberOfAchievers { get; set; }

            [DataMember]
            public DateTime? UnlockedOnTimestamp { get; set; }
            [DataMember]
            public string UnlockedOnTimestampString { get; set; }

            [DataMember]
            public DateTime? IsSeenOnTimestamp { get; set; }

            [DataMember]
            public bool IsSetAsBadge { get; set; }

            [DataMember]
            public DateTime CreatedOnTimestamp { get; set; }

            [DataMember]
            public string CreatedOnTimestampString { get; set; }
        }

        public GamificationBackupResponse SelectExperienceBackup(string adminUserId, string companyId, List<string> userIds, List<string> departmentIds)
        {
            GamificationBackupResponse response = new GamificationBackupResponse();
            response.Users = new List<User>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                User userManager = new User();
                AnalyticQuiz analytics = new AnalyticQuiz();
                List<User> users = new List<User>();

                // Targeted userids
                if (userIds != null && userIds.Count > 0)
                {
                    foreach (string userId in userIds)
                    {
                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                        if (selectedUser != null)
                        {
                            if (users.FirstOrDefault(u => u.UserId == userId) == null)
                            {
                                AnalyticQuiz.Exp exp = analytics.SelectLevelOfUser(userId, analyticSession);
                                selectedUser.Level = exp.CurrentLevel;
                                selectedUser.Experience = exp.CurrentExp;
                                users.Add(selectedUser);
                            }
                        }
                    }
                }
                // Targeted departmentids
                else if (departmentIds != null && departmentIds.Count > 0)
                {
                    userIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, adminUserId, companyId, mainSession, null);
                    foreach (string userId in userIds)
                    {
                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                        if (selectedUser != null)
                        {
                            if (users.FirstOrDefault(u => u.UserId == userId) == null)
                            {
                                AnalyticQuiz.Exp exp = analytics.SelectLevelOfUser(userId, analyticSession);
                                selectedUser.Level = exp.CurrentLevel;
                                selectedUser.Experience = exp.CurrentExp;
                                users.Add(selectedUser);
                            }
                        }
                    }
                }
                // Targeted company
                else
                {
                    users = userManager.GetAllUserForAdmin(adminUserId, companyId, null, 0, User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession).Users;

                    foreach (User user in users)
                    {
                        AnalyticQuiz.Exp exp = analytics.SelectLevelOfUser(user.UserId, analyticSession);
                        user.Level = exp.CurrentLevel;
                        user.Experience = exp.CurrentExp;
                    }
                }

                response.Users = users.OrderByDescending(u => u.Experience).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationResetResponse ResetExperience(string adminUserId, string companyId, List<string> userIds)
        {
            GamificationResetResponse response = new GamificationResetResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                User userManager = new User();
                AnalyticQuiz analytics = new AnalyticQuiz();

                if (userIds == null || userIds != null && userIds.Count == 0)
                {
                    userIds = new List<string>();

                    List<User> users = userManager.GetAllUserForAdmin(adminUserId, companyId, null, 0, User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession).Users;

                    foreach (User user in users)
                    {
                        userIds.Add(user.UserId);
                    }
                }

                Thread resetThread = new Thread(() => analytics.ResetExperiencePoints(adminUserId, companyId, userIds, mainSession, analyticSession));
                resetThread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public DateTime? SelectLastResetTimestampForLeaderboard(string companyId, ISession mainSession)
        {
            DateTime? resetTimestamp = null;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("gamification_log",
                        new List<string>(), new List<string> { "company_id" }));
                Row gamificationLogRow = mainSession.Execute(ps.Bind(companyId)).FirstOrDefault();

                if (gamificationLogRow != null)
                {
                    if (gamificationLogRow["last_resetted_leaderboard_timestamp"] != null)
                    {
                        resetTimestamp = gamificationLogRow.GetValue<DateTime>("last_resetted_leaderboard_timestamp");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return resetTimestamp;
        }

        public GamificationBackupResponse SelectLeaderboardBackup(string adminUserId, string companyId, List<string> userIds, List<string> departmentIds)
        {
            GamificationBackupResponse response = new GamificationBackupResponse();
            response.Users = new List<User>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                User userManager = new User();
                AnalyticQuiz analytics = new AnalyticQuiz();
                List<User> users = new List<User>();

                // Targeted userids
                if (userIds != null && userIds.Count > 0)
                {
                    foreach (string userId in userIds)
                    {
                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                        if (selectedUser != null)
                        {
                            if (users.FirstOrDefault(u => u.UserId == userId) == null)
                            {
                                selectedUser.TotalNumberOfUniqueCorrects = analytics.SelectNumberOfUniqueCorrectByUser(userId, analyticSession);
                                users.Add(selectedUser);
                            }
                        }
                    }
                }
                // Targeted departmentids
                else if (departmentIds != null && departmentIds.Count > 0)
                {
                    userIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, adminUserId, companyId, mainSession, null);
                    foreach (string userId in userIds)
                    {
                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                        if (selectedUser != null)
                        {
                            if (users.FirstOrDefault(u => u.UserId == userId) == null)
                            {
                                selectedUser.TotalNumberOfUniqueCorrects = analytics.SelectNumberOfUniqueCorrectByUser(userId, analyticSession);
                                users.Add(selectedUser);
                            }
                        }
                    }
                }
                // Targeted company
                else
                {
                    users = userManager.GetAllUserForAdmin(adminUserId, companyId, null, 0, 0, null, false, false, null, mainSession).Users;

                    foreach (User user in users)
                    {
                        user.TotalNumberOfUniqueCorrects = analytics.SelectNumberOfUniqueCorrectByUser(user.UserId, analyticSession);
                    }
                }

                response.Users = users.OrderByDescending(u => u.TotalNumberOfUniqueCorrects).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationResetResponse ResetLeaderboard(string adminUserId, string companyId, List<string> topicIds)
        {
            // Existing event leaderboard will not be cleared
            // Table only stores event as a whole and not event and topic
            GamificationResetResponse response = new GamificationResetResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Topic topicManager = new Topic();
                AnalyticQuiz analytics = new AnalyticQuiz();

                if (topicIds == null || topicIds != null && topicIds.Count == 0)
                {
                    Thread resetUserChallengeStatsThread = new Thread(() => analytics.ResetUserChallengeStats(companyId, analyticSession));
                    resetUserChallengeStatsThread.Start();

                    topicIds = new List<string>();

                    List<Topic> topics = topicManager.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, null, null, null, false, false, mainSession).Topics;

                    foreach (Topic topic in topics)
                    {
                        topicIds.Add(topic.TopicId);
                    }
                }

                Thread resetLeaderboardThread = new Thread(() => analytics.ResetLeaderboard(adminUserId, companyId, topicIds, mainSession, analyticSession));
                resetLeaderboardThread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationCreateAchievementResponse CreateAchievement(string adminUserId, string companyId, string achievementId, string title, int templateType, bool isIconCustomized, string iconImageUrl, string iconColor, int type, int ruleType, List<string> topicIds, int playTimes = 0, int failTimes = 0)
        {
            GamificationCreateAchievementResponse response = new GamificationCreateAchievementResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (isIconCustomized)
                {
                    templateType = 0;
                    iconColor = null;
                }

                if (templateType > 0)
                {
                    isIconCustomized = false;
                }

                if (type == (int)AchievementTypeEnum.FailButGet)
                {
                    playTimes = 0;
                    ruleType = (int)AchievementRuleTypeEnum.LosingStreak;
                }
                else
                {
                    failTimes = 0;
                }

                if (ruleType == (int)AchievementRuleTypeEnum.SpecificTopic)
                {
                    topicIds = topicIds.Take(1).ToList();
                }

                GamificationValidateAchievementResponse validateResponse = ValidateAchievementFields(companyId, title, templateType, isIconCustomized, type, ruleType, iconImageUrl, iconColor, topicIds, playTimes, failTimes, mainSession);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.CountStatement("achievement_order", new List<string> { "company_id" }));
                int ordering = (int)mainSession.Execute(ps.Bind(companyId)).FirstOrDefault().GetValue<long>("count") + 1;

                if (string.IsNullOrEmpty(achievementId))
                {
                    achievementId = UUIDGenerator.GenerateUniqueIDForAchievement();
                }

                DateTime currentTime = DateTime.UtcNow;
                ps = mainSession.Prepare(CQLGenerator.InsertStatement("achievement", new List<string> { "id", "company_id", "title", "type", "icon_image_url", "color", "ordering", "icon_template_type", "is_icon_customized", "rule_type", "play_times", "fail_times", "status", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(achievementId, companyId, title, type, iconImageUrl, iconColor, ordering, templateType, isIconCustomized, ruleType, playTimes, failTimes, (int)AchievementStatusEnum.Live, adminUserId, currentTime, adminUserId, currentTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("achievement_order", new List<string> { "company_id", "achievement_id", "ordering" }));
                batch.Add(ps.Bind(companyId, achievementId, ordering));

                if (topicIds != null)
                {
                    foreach (string topicId in topicIds)
                    {
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("achievement_by_topic", new List<string> { "topic_id", "achievement_id" }));
                        batch.Add(ps.Bind(topicId, achievementId));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("topic_by_achievement", new List<string> { "topic_id", "achievement_id" }));
                        batch.Add(ps.Bind(topicId, achievementId));
                    }

                }


                mainSession.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationCreateAchievementResponse DeleteAchievement(string adminUserId, string companyId, string achievementId)
        {
            GamificationCreateAchievementResponse response = new GamificationCreateAchievementResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row achievementRow = vh.ValidateAchievement(companyId, achievementId, session);
                if (achievementRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalid);
                    response.ErrorMessage = ErrorMessage.AchievementInvalid;
                    return response;
                }

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                int currentOrder = achievementRow.GetValue<int>("ordering");

                ps = session.Prepare(CQLGenerator.UpdateStatement("achievement", new List<string> { "company_id", "id" }, new List<string> { "status", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind((int)AchievementStatusEnum.Deleted, adminUserId, DateTime.UtcNow, companyId, achievementId));

                ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("achievement_order", new List<string>(), new List<string> { "company_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                RowSet orderRowset = session.Execute(ps.Bind(companyId, currentOrder));

                foreach (Row orderRow in orderRowset)
                {
                    int order = orderRow.GetValue<int>("ordering");
                    string nextAchievementId = orderRow.GetValue<string>("achievement_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("achievement_order", new List<string> { "company_id", "ordering", "achievement_id" }));
                    deleteBatch.Add(ps.Bind(companyId, order, nextAchievementId));

                    ps = session.Prepare(CQLGenerator.UpdateStatement("achievement", new List<string> { "company_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                    updateBatch.Add(ps.Bind(order - 1, companyId, nextAchievementId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("achievement_order", new List<string> { "company_id", "ordering", "achievement_id" }));
                    updateBatch.Add(ps.Bind(companyId, order - 1, nextAchievementId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("achievement_order", new List<string> { "company_id", "ordering", "achievement_id" }));
                deleteBatch.Add(ps.Bind(companyId, currentOrder, achievementId));

                ps = session.Prepare(CQLGenerator.SelectStatement("topic_by_achievement", new List<string>(), new List<string> { "achievement_id" }));
                RowSet topicByAchievementRowset = session.Execute(ps.Bind(achievementId));
                foreach (Row topicByAchievementRow in topicByAchievementRowset)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("achievement_by_topic", new List<string> { "topic_id", "achievement_id" }));
                    deleteBatch.Add(ps.Bind(topicByAchievementRow.GetValue<string>("topic_id"), achievementId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("topic_by_achievement", new List<string> { "achievement_id" }));
                deleteBatch.Add(ps.Bind(achievementId));

                // Remove from S3
                RemoveFromS3(achievementId, companyId);

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                // Do not delete from analytics for now

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationUpdateAchievementResponse UpdateAchievement(string adminUserId, string companyId, string achievementId, string newTitle, int newTemplateType, bool newIsIconCustomized, string newIconImageUrl, string newIconColor, int newType, int newRuleType, List<string> newTopicIds, int newPlayTimes = 0, int newFailTimes = 0)
        {
            GamificationUpdateAchievementResponse response = new GamificationUpdateAchievementResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row currentAchievementRow = vh.ValidateAchievement(companyId, achievementId, mainSession);
                if (currentAchievementRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalid);
                    response.ErrorMessage = ErrorMessage.AchievementInvalid;
                    return response;
                }

                if (newIsIconCustomized)
                {
                    newTemplateType = 0;
                    newIconColor = null;
                }

                if (newTemplateType > 0)
                {
                    newIsIconCustomized = false;
                }

                if (newType == (int)AchievementTypeEnum.FailButGet)
                {
                    newPlayTimes = 0;
                    newRuleType = (int)AchievementRuleTypeEnum.LosingStreak;
                }
                else
                {
                    newFailTimes = 0;
                }

                if (newRuleType == (int)AchievementRuleTypeEnum.SpecificTopic)
                {
                    newTopicIds = newTopicIds.Take(1).ToList();
                }

                GamificationValidateAchievementResponse validateResponse = ValidateAchievementFields(companyId, newTitle, newTemplateType, newIsIconCustomized, newType, newRuleType, newIconImageUrl, newIconColor, newTopicIds, newPlayTimes, newFailTimes, mainSession);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                if (!currentAchievementRow.GetValue<string>("icon_image_url").Equals(newIconImageUrl))
                {
                    RemoveFromS3(achievementId, companyId, currentAchievementRow.GetValue<string>("icon_image_url"));
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("topic_by_achievement", new List<string>(), new List<string> { "achievement_id" }));
                RowSet topicByAchievementRowset = mainSession.Execute(ps.Bind(achievementId));
                foreach (Row topicByAchievementRow in topicByAchievementRowset)
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("achievement_by_topic", new List<string> { "topic_id", "achievement_id" }));
                    deleteBatch.Add(ps.Bind(topicByAchievementRow.GetValue<string>("topic_id"), achievementId));
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("topic_by_achievement", new List<string> { "achievement_id" }));
                deleteBatch.Add(ps.Bind(achievementId));

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("achievement", new List<string> { "id", "company_id" }, new List<string> { "title", "type", "icon_image_url", "color", "icon_template_type", "is_icon_customized", "rule_type", "play_times", "fail_times", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newType, newIconImageUrl, newIconColor, newTemplateType, newIsIconCustomized, newRuleType, newPlayTimes, newFailTimes, adminUserId, DateTime.UtcNow, achievementId, companyId));


                if (newTopicIds != null)
                {
                    foreach (string topicId in newTopicIds)
                    {
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("achievement_by_topic", new List<string> { "topic_id", "achievement_id" }));
                        updateBatch.Add(ps.Bind(topicId, achievementId));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("topic_by_achievement", new List<string> { "topic_id", "achievement_id" }));
                        updateBatch.Add(ps.Bind(topicId, achievementId));
                    }

                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationSelectAllAchievementResponse ReorderingAchievements(string adminUserId, string companyId, List<string> achievementIds)
        {
            GamificationSelectAllAchievementResponse response = new GamificationSelectAllAchievementResponse();
            response.Achievements = new List<Achievement>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }



                #region update achievement table
                PreparedStatement ps = new PreparedStatement();
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentDateTime = DateTime.UtcNow;
                for (int i = 0; i < achievementIds.Count; i++)
                {
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("achievement", new List<string> { "id", "company_id" }, new List<string> { "ordering", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(i + 1, adminUserId, currentDateTime, achievementIds[i], companyId));
                }
                #endregion

                #region update achievement_order table
                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("achievement_order", new List<string> { "company_id" }));
                deleteBatch.Add(ps.Bind(companyId));
                mainSession.Execute(deleteBatch);

                for (int i = 0; i < achievementIds.Count; i++)
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("achievement_order", new List<string> { "company_id", "ordering", "achievement_id" }));
                    updateBatch.Add(ps.Bind(companyId, i + 1, achievementIds[i]));
                }

                mainSession.Execute(updateBatch);
                #endregion

                #region Get new achievement list
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("achievement_order", new List<string>(), new List<string> { "company_id" }));
                RowSet achievementRowset = mainSession.Execute(ps.Bind(companyId));
                foreach (Row achievementRow in achievementRowset)
                {
                    string achievementId = achievementRow.GetValue<string>("achievement_id");
                    Achievement selectedAchievement = SelectAchievement(achievementId, companyId, (int)GamificationQueryType.FullDetail, mainSession, null, null).Achievement;
                    if (selectedAchievement != null)
                    {
                        response.Achievements.Add(selectedAchievement);
                    }
                }
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationSelectAllAchievementResponse SelectAllAchievements(string adminUserId, string companyId, int queryType, string containsName = null, ISession mainSession = null, ISession analyticSession = null)
        {
            GamificationSelectAllAchievementResponse response = new GamificationSelectAllAchievementResponse();
            response.Achievements = new List<Achievement>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("achievement_order", new List<string>(), new List<string> { "company_id" }));
                RowSet achievementRowset = mainSession.Execute(ps.Bind(companyId));
                foreach (Row achievementRow in achievementRowset)
                {
                    string achievementId = achievementRow.GetValue<string>("achievement_id");
                    Achievement selectedAchievement = SelectAchievement(achievementId, companyId, queryType, mainSession, analyticSession, containsName).Achievement;
                    if (selectedAchievement != null)
                    {
                        response.Achievements.Add(selectedAchievement);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationSelectAllAchievementResponse SelectAllAchievementsByUser(string requesterUserId, string companyId, int queryType = (int)GamificationQueryType.Basic, ISession mainSession = null, ISession analyticSession = null)
        {
            GamificationSelectAllAchievementResponse response = new GamificationSelectAllAchievementResponse();
            response.Achievements = new List<Achievement>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("achievement_order", new List<string>(), new List<string> { "company_id" }));
                RowSet achievementRowset = mainSession.Execute(ps.Bind(companyId));
                foreach (Row achievementRow in achievementRowset)
                {
                    string achievementId = achievementRow.GetValue<string>("achievement_id");
                    Achievement selectedAchievement = SelectAchievement(achievementId, companyId, queryType, mainSession, analyticSession, null, requesterUserId).Achievement;
                    if (selectedAchievement != null)
                    {
                        response.Achievements.Add(selectedAchievement);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationSelectAchievementResponse SelectAchievement(string adminUserId, string companyId, string achievementId, ISession mainSession = null, ISession analyticSession = null, int queryType = (int)GamificationQueryType.FullDetail)
        {
            GamificationSelectAchievementResponse response = new GamificationSelectAchievementResponse();
            response.Achievement = new Achievement();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                response = SelectAchievement(achievementId, companyId, queryType, mainSession, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationSelectAchievementResponse SelectAchievementByUser(string requesterUserId, string companyId, string achievementId, ISession mainSession = null, ISession analyticSession = null)
        {
            GamificationSelectAchievementResponse response = new GamificationSelectAchievementResponse();
            response.Achievement = new Achievement();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                response = SelectAchievement(achievementId, companyId, (int)GamificationQueryType.FullDetail, mainSession, analyticSession, null, requesterUserId);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationSelectAchievementResultResponse SelectAchievementResult(string adminUserId, string companyId, string achievementId, string searchId = null)
        {
            GamificationSelectAchievementResultResponse response = new GamificationSelectAchievementResultResponse();
            response.Success = false;
            try
            {

                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                GamificationSelectAchievementResponse selectResponse = SelectAchievement(achievementId, companyId, (int)GamificationQueryType.Basic, mainSession, analyticSession);
                if (!selectResponse.Success)
                {
                    response.ErrorCode = selectResponse.ErrorCode;
                    response.ErrorMessage = selectResponse.ErrorMessage;
                    return response;
                }

                double timeoffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                response.Achievement = selectResponse.Achievement;
                response.Achievement.CreatedOnTimestampString = response.Achievement.CreatedOnTimestamp.AddHours(timeoffset).ToString("dd/MM/yyyy");
                Dictionary<string, object> dict = new AnalyticGamification().SelectAchieversForAchievement(achievementId, companyId, searchId, timeoffset, mainSession, analyticSession);
                response.Achievers = (List<AnalyticGamification.Achiever>)dict["Achievers"];
                response.Achievement.NumberOfAchievers = response.Achievers.Count;
                response.SearchTerms = (List<AnalyticGamification.GamificationSearchTerm>)dict["SearchTerms"];

                if (string.IsNullOrEmpty(searchId))
                {
                    response.SearchTerms.Insert(0, new AnalyticGamification.GamificationSearchTerm
                    {
                        Content = "All Departments",
                        Id = string.Empty,
                        IsSelected = true
                    });
                }
                else
                {
                    response.SearchTerms = response.SearchTerms.OrderByDescending(s => s.IsSelected).ToList();
                    response.SearchTerms.Insert(1, new AnalyticGamification.GamificationSearchTerm
                    {
                        Content = "All Departments",
                        Id = string.Empty,
                        IsSelected = false
                    });
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public GamificationSelectUserAchievementResultResponse SelectUserAchievementResult(string adminUserId, string companyId, string achieverId)
        {
            GamificationSelectUserAchievementResultResponse response = new GamificationSelectUserAchievementResultResponse();
            response.Achievements = new List<Achievement>();
            response.Achiever = new User();
            response.NumberOfUnlockedAchievements = 0;
            response.Success = false;
            try
            {

                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                es = vh.isValidatedAsUser(achieverId, companyId, mainSession);
                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                response.Achiever = new User().SelectUserBasic(achieverId, companyId, true, mainSession, null, true).User;

                double timeoffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                response.Achievements = new AnalyticGamification().SelectUnlockedAchievementsByUser(achieverId, companyId, timeoffset, mainSession, analyticSession);
                response.NumberOfUnlockedAchievements = response.Achievements.Count;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private GamificationSelectAchievementResponse SelectAchievement(string achievementId, string companyId, int queryType, ISession mainSession, ISession analyticSession, string containsName = null, string unlockedByUserId = null)
        {
            GamificationSelectAchievementResponse response = new GamificationSelectAchievementResponse();
            response.Achievement = null;
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                Row achievementRow = vh.ValidateAchievement(companyId, achievementId, mainSession);
                if (achievementRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalid);
                    response.ErrorMessage = ErrorMessage.AchievementInvalid;
                    return response;
                }

                string title = achievementRow.GetValue<string>("title");

                if (!string.IsNullOrEmpty(containsName))
                {
                    if (!title.ToLower().Contains(containsName.ToLower()))
                    {
                        return response;
                    }
                }

                int ruleType = achievementRow.GetValue<int>("rule_type");
                int type = achievementRow.GetValue<int>("type");
                int playTimes = achievementRow["play_times"] == null ? 0 : achievementRow.GetValue<int>("play_times");
                int failTimes = achievementRow["fail_times"] == null ? 0 : achievementRow.GetValue<int>("fail_times");
                DateTime createdOnTimestamp = achievementRow.GetValue<DateTime>("created_on_timestamp");

                List<Topic> topics = new List<Topic>();
                string specificTopicTitle = null;
                if (queryType == (int)GamificationQueryType.FullDetail || ruleType == (int)AchievementRuleTypeEnum.SpecificTopic)
                {
                    Topic topicManager = new Topic();
                    PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("topic_by_achievement", new List<string>(), new List<string> { "achievement_id" }));
                    RowSet topicRowset = mainSession.Execute(ps.Bind(achievementId));
                    foreach (Row topicRow in topicRowset)
                    {
                        string topicId = topicRow.GetValue<string>("topic_id");
                        Topic selectedTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, false, true).Topic;
                        if (selectedTopic != null)
                        {
                            // For description
                            if (ruleType == (int)AchievementRuleTypeEnum.SpecificTopic)
                            {
                                specificTopicTitle = selectedTopic.TopicTitle;
                            }

                            if (queryType == (int)GamificationQueryType.FullDetail)
                            {
                                topics.Add(selectedTopic);
                            }
                        }
                    }
                }
                Dictionary<string, string> dict = GetDescriptionForAchievement(type, ruleType, playTimes, failTimes, specificTopicTitle);
                string typeDescription = dict["TypeDescription"];
                string ruleDescription = dict["RuleDescription"];

                bool isUnlocked = false;
                bool isSeen = false;
                DateTime? unlockedOnTimestamp = null;
                DateTime? isSeenOnTimestamp = null;
                bool isSetAsBadge = false;
                int numberOfAchievers = 0;

                AnalyticGamification analyticManager = new AnalyticGamification();
                if (!string.IsNullOrEmpty(unlockedByUserId))
                {
                    Dictionary<string, object> details = analyticManager.SelectUnlockedDetailsForAchievementByUser(achievementId, unlockedByUserId, analyticSession);
                    unlockedOnTimestamp = (DateTime?)details["UnlockedTimestamp"];
                    isSeenOnTimestamp = (DateTime?)details["SeenTimestamp"];
                    isSetAsBadge = (bool)details["IsSetBadge"];

                    if (unlockedOnTimestamp != null)
                    {
                        isUnlocked = true;
                    }

                    if (isSeenOnTimestamp != null)
                    {
                        isSeen = true;
                    }
                }
                else
                {
                    numberOfAchievers = analyticManager.SelectAchieverCountForAchievement(achievementId, analyticSession);
                }


                response.Achievement = new Achievement
                {
                    Id = achievementRow.GetValue<string>("id"),
                    Title = title,
                    Type = type,
                    IconImageUrl = achievementRow.GetValue<string>("icon_image_url"),
                    IconColor = achievementRow.GetValue<string>("color"),
                    IsIconCustomized = achievementRow.GetValue<bool>("is_icon_customized"),
                    Ordering = achievementRow.GetValue<int>("ordering"),
                    TypeDescription = typeDescription,
                    RuleDescription = ruleDescription,
                    RuleType = ruleType,
                    PlayTimes = playTimes,
                    FailTimes = failTimes,
                    IsUnlocked = isUnlocked,
                    IsSetAsBadge = isSetAsBadge,
                    UnlockedOnTimestamp = unlockedOnTimestamp,
                    IsSeen = isSeen,
                    IsSeenOnTimestamp = isSeenOnTimestamp,
                    NumberOfAchievers = numberOfAchievers,
                    Topics = topics,
                    CreatedOnTimestamp = createdOnTimestamp,
                    TemplateType = achievementRow.GetValue<int>("icon_template_type")
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationSelectTopicResponse SelectTopicsForAchievement(string adminUserId, string companyId, string containsName = null, ISession session = null)
        {
            GamificationSelectTopicResponse response = new GamificationSelectTopicResponse();
            response.Topics = new List<Topic>();
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                Topic topicManager = new Topic();
                List<Topic> allTopics = topicManager.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, null, null, containsName, true, true, session).Topics;
                allTopics = allTopics.OrderBy(t => t.TopicTitle).ToList();
                response.Topics = allTopics;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private Dictionary<string, string> GetDescriptionForAchievement(int type, int ruleType, int playTimes, int failTimes, string specificTopicTitle = null)
        {
            string typeDescription = string.Empty;
            string ruleDescription = string.Empty;

            switch (type)
            {
                case (int)AchievementTypeEnum.PlayToGet:
                    switch (ruleType)
                    {
                        case (int)AchievementRuleTypeEnum.AllTopics:
                            typeDescription = string.Format(AchievementDescription.PlayToGetAll, playTimes);
                            break;
                        case (int)AchievementRuleTypeEnum.SpecificTopic:
                            typeDescription = string.Format(AchievementDescription.PlayToGetSpecific, playTimes);
                            ruleDescription = string.Format(AchievementDescription.SpecificTopicRuleType, specificTopicTitle);
                            break;
                        case (int)AchievementRuleTypeEnum.MultipleTopicsByEach:
                            typeDescription = string.Format(AchievementDescription.PlayToGetMultipleEach, playTimes);
                            ruleDescription = AchievementDescription.MultipleTopicRuleType;
                            break;
                        case (int)AchievementRuleTypeEnum.MultipleTopicsByEither:
                            typeDescription = string.Format(AchievementDescription.PlayToGetMultipleEither, playTimes);
                            ruleDescription = AchievementDescription.MultipleTopicRuleType;
                            break;
                    }
                    break;
                case (int)AchievementTypeEnum.PerfectAndGet:
                    switch (ruleType)
                    {
                        case (int)AchievementRuleTypeEnum.SpecificTopic:
                            typeDescription = AchievementDescription.PerfectAndGetSpecific;
                            ruleDescription = string.Format(AchievementDescription.SpecificTopicRuleType, specificTopicTitle);
                            break;
                        case (int)AchievementRuleTypeEnum.MultipleTopicsByEach:
                            typeDescription = AchievementDescription.PerfectAndGetMultipleEach;
                            ruleDescription = AchievementDescription.MultipleTopicRuleType;
                            break;
                        case (int)AchievementRuleTypeEnum.MultipleTopicsByEither:
                            typeDescription = AchievementDescription.PerfectAndGetMultipleEither;
                            ruleDescription = AchievementDescription.MultipleTopicRuleType;
                            break;
                    }
                    break;
                case (int)AchievementTypeEnum.FailButGet:
                    typeDescription = AchievementDescription.FailButGet;
                    ruleDescription = string.Format(AchievementDescription.LosingStreakRuleType, failTimes);
                    break;
            }

            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("TypeDescription", typeDescription);
            dict.Add("RuleDescription", ruleDescription);

            return dict;
        }

        public void UpdatePerfectAndGet(string companyId, string userId, string challengeId, string topicId, string questionId, DateTime currentTime, ISession mainSession, ISession analyticSession)
        {
            try
            {
                AnalyticGamification analytic = new AnalyticGamification();

                List<Achievement> achievements = SelectAllAchievements(null, companyId, (int)GamificationQueryType.FullDetail, null, mainSession, analyticSession).Achievements.Where(a => a.Type == (int)AchievementTypeEnum.PerfectAndGet).ToList();

                foreach (Achievement achievement in achievements)
                {
                    if (achievement.Topics.FirstOrDefault(t => t.TopicId.Equals(topicId)) != null)
                    {
                        analytic.UpdatePerfectAndGet(userId, achievement.Id, topicId, challengeId, questionId, analyticSession);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdatePlayAndGet(string companyId, string userId, string challengeId, string topicId, DateTime currentTime, ISession mainSession, ISession analyticSession)
        {
            try
            {
                AnalyticGamification analytic = new AnalyticGamification();

                List<Achievement> achievements = SelectAllAchievements(null, companyId, (int)GamificationQueryType.FullDetail, null, mainSession, analyticSession).Achievements.Where(a => a.Type == (int)AchievementTypeEnum.PlayToGet).ToList();
                foreach (Achievement achievement in achievements)
                {
                    if (achievement.RuleType == (int)Gamification.AchievementRuleTypeEnum.AllTopics)
                    {
                        analytic.UpdatePlayAndGet(userId, achievement.Id, topicId, challengeId, currentTime, analyticSession);
                    }
                    else
                    {
                        if (achievement.Topics.FirstOrDefault(t => t.TopicId.Equals(topicId)) != null)
                        {
                            analytic.UpdatePlayAndGet(userId, achievement.Id, topicId, challengeId, currentTime, analyticSession);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateStreak(string companyId, string initiatorUserId, string challengedUserId, string challengeId, string topicId, DateTime currentTime, int initiatorUserResult, int challengedUserResult, ISession mainSession, ISession analyticSession)
        {
            try
            {
                // For now only losing streak

                AnalyticGamification analytic = new AnalyticGamification();

                List<Achievement> achievements = SelectAllAchievements(null, companyId, (int)GamificationQueryType.FullDetail, null, mainSession, analyticSession).Achievements.Where(a => a.Type == (int)AchievementTypeEnum.FailButGet).ToList();
                foreach (Achievement achievement in achievements)
                {
                    // No need see topic
                    //if (achievement.Topics.FirstOrDefault(t => t.TopicId.Equals(topicId)) != null)
                    //{
                    //    analytic.UpdateStreak(initiatorUserId, achievement.Id, topicId, challengeId, currentTime, initiatorUserResult, true, analyticSession);
                    //    analytic.UpdateStreak(challengedUserId, achievement.Id, topicId, challengeId, currentTime, challengedUserResult, true, analyticSession);
                    //}

                    analytic.UpdateStreak(initiatorUserId, achievement.Id, topicId, challengeId, currentTime, initiatorUserResult, true, analyticSession);
                    analytic.UpdateStreak(challengedUserId, achievement.Id, topicId, challengeId, currentTime, challengedUserResult, true, analyticSession);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public GamificationSelectUnlockedAchievementResponse SelectNewUnlockedAchievementByUser(string userId, string companyId, ISession mainSession, ISession analyticSession)
        {
            GamificationSelectUnlockedAchievementResponse response = new GamificationSelectUnlockedAchievementResponse();
            response.Notification = new Notification();
            response.Achievements = new List<Achievement>();
            response.Success = false;
            try
            {
                Notification notificationManager = new Notification();
                Department departmentManager = new Department();
                ChallengeQuestion questionManager = new ChallengeQuestion();
                AnalyticGamification analytic = new AnalyticGamification();

                DateTime currentTime = DateTime.UtcNow;

                List<Achievement> lockedAchievements = SelectAllAchievementsByUser(userId, companyId, (int)GamificationQueryType.FullDetail, mainSession, analyticSession).Achievements.Where(a => a.IsUnlocked == false).ToList();
                foreach (Achievement lockedAchievement in lockedAchievements)
                {
                    string achievementId = lockedAchievement.Id;
                    int failTimes = lockedAchievement.FailTimes;
                    int playTimes = lockedAchievement.PlayTimes;

                    bool willBeUnlocked = false;

                    switch (lockedAchievement.Type)
                    {
                        case (int)AchievementTypeEnum.PlayToGet:
                            switch (lockedAchievement.RuleType)
                            {
                                case (int)AchievementRuleTypeEnum.AllTopics:
                                    if (playTimes <= analytic.SelectMaxPlayCountByAchievement(achievementId, userId, analyticSession))
                                    {
                                        willBeUnlocked = true;
                                    }
                                    break;
                                case (int)AchievementRuleTypeEnum.SpecificTopic:
                                case (int)AchievementRuleTypeEnum.MultipleTopicsByEither:
                                    foreach (Topic topic in lockedAchievement.Topics)
                                    {
                                        string topicId = topic.TopicId;
                                        if (playTimes <= analytic.SelectPlayCountByTopic(achievementId, userId, topicId, analyticSession))
                                        {
                                            willBeUnlocked = true;
                                            break;
                                        }
                                    }
                                    break;
                                case (int)AchievementRuleTypeEnum.MultipleTopicsByEach:
                                    bool isCountLess = false;
                                    foreach (Topic topic in lockedAchievement.Topics)
                                    {
                                        string topicId = topic.TopicId;
                                        if (playTimes > analytic.SelectPlayCountByTopic(achievementId, userId, topicId, analyticSession))
                                        {
                                            isCountLess = true;
                                            break;
                                        }
                                    }

                                    if (!isCountLess)
                                    {
                                        willBeUnlocked = true;
                                    }
                                    break;
                            }
                            break;
                        case (int)AchievementTypeEnum.PerfectAndGet:
                            switch (lockedAchievement.RuleType)
                            {
                                case (int)AchievementRuleTypeEnum.SpecificTopic:
                                case (int)AchievementRuleTypeEnum.MultipleTopicsByEither:
                                    foreach (Topic topic in lockedAchievement.Topics)
                                    {
                                        string topicId = topic.TopicId;
                                        int totalQuestions = questionManager.SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, mainSession);
                                        if (totalQuestions <= analytic.SelectCorrectCountByTopic(achievementId, userId, topicId, analyticSession))
                                        {
                                            willBeUnlocked = true;
                                            break;
                                        }
                                    }
                                    break;
                                case (int)AchievementRuleTypeEnum.MultipleTopicsByEach:
                                    foreach (Topic topic in lockedAchievement.Topics)
                                    {
                                        string topicId = topic.TopicId;
                                        int totalQuestions = questionManager.SelectNumberOfQuestions(topicId, ChallengeQuestion.QuestionStatus.CODE_ACTIVE, mainSession);
                                        if (totalQuestions > analytic.SelectCorrectCountByTopic(achievementId, userId, topicId, analyticSession))
                                        {
                                            break;
                                        }
                                    }
                                    break;
                            }
                            break;
                        case (int)AchievementTypeEnum.FailButGet:
                            if (failTimes <= analytic.SelectStreakCount(achievementId, userId, true, analyticSession))
                            {
                                willBeUnlocked = true;
                            }
                            break;
                    }

                    if (willBeUnlocked)
                    {
                        Log.Debug("Unlocked achievement: " + lockedAchievement.Title);

                        lockedAchievement.UnlockedOnTimestamp = currentTime;
                        lockedAchievement.IsSeen = false;
                        lockedAchievement.IsUnlocked = true;

                        UnlockAchievement(achievementId, userId, companyId, departmentManager.GetAllDepartmentByUserId(userId, companyId, mainSession).Departments[0].Id, currentTime, analyticSession);

                        Log.Debug("Create achievement unlocked notification");
                        notificationManager.CreateAchievementNotification(userId, achievementId, currentTime, mainSession);

                        response.Achievements.Add(lockedAchievement);
                    }
                }

                if (response.Achievements.Count > 0)
                {
                    response.Notification = new Notification
                    {
                        TaggedUserId = userId,
                        NotificationText = NotificationMessage.NotificationAchievementUnlockedGeneral,
                        NumberOfNotificationForTargetedUser = notificationManager.SelectNotificationNumberByUser(userId, companyId, mainSession).NumberOfNotification,
                        Type = (int)Notification.NotificationType.Challenge,
                        SubType = (int)Notification.NotificationGameSubType.AchievementUnlocked
                    };
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationUpdateBadgeResponse UpdateBadge(string achievementId, string requesterUserId, string companyId, bool isRemoveBadge = false)
        {
            GamificationUpdateBadgeResponse response = new GamificationUpdateBadgeResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row achievementRow = vh.ValidateAchievement(companyId, achievementId, mainSession);
                if (achievementRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalid);
                    response.ErrorMessage = ErrorMessage.AchievementInvalid;
                    return response;
                }

                AnalyticGamification analytic = new AnalyticGamification();
                Dictionary<string, object> dict = analytic.SelectUnlockedDetailsForAchievementByUser(achievementId, requesterUserId, analyticSession);
                if ((DateTime?)dict["UnlockedTimestamp"] == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementNotUnlockedYet);
                    response.ErrorMessage = ErrorMessage.AchievementNotUnlockedYet;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("achievement_unlocked_by_user", new List<string>(), new List<string> { "user_id", "is_set_badge" }));
                Row badgeRow = analyticSession.Execute(ps.Bind(requesterUserId, true)).FirstOrDefault();

                if (badgeRow != null)
                {
                    string badgeAchievementId = badgeRow.GetValue<string>("achievement_id");
                    if (badgeAchievementId.Equals(achievementId) && !isRemoveBadge)
                    {
                        response.Success = true;
                        return response;
                    }

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("achievement_unlocked_by_user", new List<string> { "user_id", "achievement_id" }, new List<string> { "is_set_badge", "modified_badge_on_timestamp" }, new List<string>()));
                    batch.Add(ps.Bind(false, currentTime, requesterUserId, badgeAchievementId));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("user_achievement_unlocked_by_achievement", new List<string> { "user_id", "achievement_id" }, new List<string> { "is_set_badge", "modified_badge_on_timestamp" }, new List<string>()));
                    batch.Add(ps.Bind(false, currentTime, requesterUserId, badgeAchievementId));
                }

                if (!isRemoveBadge)
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("achievement_unlocked_by_user", new List<string> { "user_id", "achievement_id" }, new List<string> { "is_set_badge", "modified_badge_on_timestamp" }, new List<string>()));
                    batch.Add(ps.Bind(true, currentTime, requesterUserId, achievementId));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("user_achievement_unlocked_by_achievement", new List<string> { "user_id", "achievement_id" }, new List<string> { "is_set_badge", "modified_badge_on_timestamp" }, new List<string>()));
                    batch.Add(ps.Bind(true, currentTime, requesterUserId, achievementId));
                }

                analyticSession.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GamificationUpdateBadgeSeenResponse UpdateSeenBadge(string achievementId, string requesterUserId, string companyId)
        {
            GamificationUpdateBadgeSeenResponse response = new GamificationUpdateBadgeSeenResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row achievementRow = vh.ValidateAchievement(companyId, achievementId, mainSession);
                if (achievementRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalid);
                    response.ErrorMessage = ErrorMessage.AchievementInvalid;
                    return response;
                }

                AnalyticGamification analytic = new AnalyticGamification();
                Dictionary<string, object> dict = analytic.SelectUnlockedDetailsForAchievementByUser(achievementId, requesterUserId, analyticSession);
                if ((DateTime?)dict["UnlockedTimestamp"] == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementNotUnlockedYet);
                    response.ErrorMessage = ErrorMessage.AchievementNotUnlockedYet;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                BatchStatement batch = new BatchStatement();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("achievement_unlocked_by_user", new List<string> { "user_id", "achievement_id" }, new List<string> { "is_seen_on_timestamp" }, new List<string>()));
                batch.Add(ps.Bind(currentTime, requesterUserId, achievementId));

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("user_achievement_unlocked_by_achievement", new List<string> { "user_id", "achievement_id" }, new List<string> { "is_seen_on_timestamp" }, new List<string>()));
                batch.Add(ps.Bind(currentTime, requesterUserId, achievementId));

                analyticSession.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UnlockAchievement(string achievementId, string userId, string companyId, string departmentId, DateTime unlockedTimestamp, ISession analyticSession)
        {
            try
            {
                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.InsertStatement("achievement_unlocked_by_user", new List<string> { "achievement_id", "company_id", "user_id", "department_id", "unlocked_on_timestamp", "is_seen_on_timestamp", "is_set_badge" }));
                batch.Add(ps.Bind(achievementId, companyId, userId, departmentId, unlockedTimestamp, null, false));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("user_achievement_unlocked_by_achievement", new List<string> { "achievement_id", "company_id", "user_id", "department_id", "unlocked_on_timestamp", "is_seen_on_timestamp", "is_set_badge" }));
                batch.Add(ps.Bind(achievementId, companyId, userId, departmentId, unlockedTimestamp, null, false));

                analyticSession.Execute(batch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void InitDefaultFields(bool isIconCustomized, int templateType, int type, int ruleType, string iconColor, List<string> topicIds, int playTimes, int failTimes)
        {
            if (isIconCustomized)
            {
                templateType = 0;
                iconColor = null;
            }

            if (templateType > 0)
            {
                isIconCustomized = false;
            }

            if (type == (int)AchievementTypeEnum.FailButGet)
            {
                playTimes = 0;
                ruleType = (int)AchievementRuleTypeEnum.LosingStreak;
            }
            else
            {
                failTimes = 0;
            }

            if (ruleType == (int)AchievementRuleTypeEnum.SpecificTopic)
            {
                topicIds = topicIds.Take(1).ToList();
            }
        }

        private GamificationValidateAchievementResponse ValidateAchievementFields(string companyId, string title, int templateType, bool isIconCustomized, int type, int ruleType, string iconUrl, string iconColor, List<string> topicIds, int playTimes, int failTimes, ISession session)
        {
            GamificationValidateAchievementResponse response = new GamificationValidateAchievementResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(title))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidTitle);
                    response.ErrorMessage = ErrorMessage.AchievementInvalidTitle;
                    return response;
                }

                if (isIconCustomized && templateType > 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementMismatchTemplate);
                    response.ErrorMessage = ErrorMessage.AchievementMismatchTemplate;
                    return response;
                }

                if (!isIconCustomized)
                {
                    if (templateType <= 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidTemplate);
                        response.ErrorMessage = ErrorMessage.AchievementInvalidTemplate;
                        return response;
                    }

                    if (string.IsNullOrEmpty(iconColor))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidIconColor);
                        response.ErrorMessage = ErrorMessage.AchievementInvalidIconColor;
                        return response;
                    }
                }

                if (topicIds == null || topicIds != null && topicIds.Count == 0)
                {
                    if (ruleType != (int)AchievementRuleTypeEnum.AllTopics && ruleType != (int)AchievementRuleTypeEnum.LosingStreak && ruleType != (int)AchievementRuleTypeEnum.WinningStreak)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementTopicIsEmpty);
                        response.ErrorMessage = ErrorMessage.AchievementTopicIsEmpty;
                        return response;
                    }
                }
                else
                {
                    List<Topic> allTopics = SelectTopicsForAchievement(null, companyId, null, session).Topics;
                    if (topicIds.Except(allTopics.Select(t => t.TopicId).ToList()).ToList().Count > 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementMismatchTopic);
                        response.ErrorMessage = ErrorMessage.AchievementMismatchTopic;
                        return response;
                    }
                }


                if (string.IsNullOrEmpty(iconUrl))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidIconUrl);
                    response.ErrorMessage = ErrorMessage.AchievementInvalidIconUrl;
                    return response;
                }

                switch (type)
                {
                    case (int)AchievementTypeEnum.PlayToGet:
                        switch (ruleType)
                        {
                            case (int)AchievementRuleTypeEnum.AllTopics:
                            case (int)AchievementRuleTypeEnum.SpecificTopic:
                            case (int)AchievementRuleTypeEnum.MultipleTopicsByEach:
                            case (int)AchievementRuleTypeEnum.MultipleTopicsByEither:
                                if (playTimes <= 0)
                                {
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidPlayTimes);
                                    response.ErrorMessage = ErrorMessage.AchievementInvalidPlayTimes;
                                    return response;
                                }
                                break;
                            default:
                                response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidRule);
                                response.ErrorMessage = ErrorMessage.AchievementInvalidRule;
                                return response;
                        }

                        break;
                    case (int)AchievementTypeEnum.PerfectAndGet:
                        break;
                    case (int)AchievementTypeEnum.FailButGet:
                        switch (ruleType)
                        {
                            case (int)AchievementRuleTypeEnum.WinningStreak:
                                if (playTimes <= 0)
                                {
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidPlayTimes);
                                    response.ErrorMessage = ErrorMessage.AchievementInvalidPlayTimes;
                                    return response;
                                }
                                break;
                            case (int)AchievementRuleTypeEnum.LosingStreak:
                                if (failTimes <= 0)
                                {
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidFailTimes);
                                    response.ErrorMessage = ErrorMessage.AchievementInvalidFailTimes;
                                    return response;
                                }
                                break;
                            default:
                                response.ErrorCode = Convert.ToInt32(ErrorCode.AchievementInvalidRule);
                                response.ErrorMessage = ErrorMessage.AchievementInvalidRule;
                                return response;
                        }
                        break;
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private void RemoveFromS3(string achievementId, string companyId, string deleteFileName = null)
        {
            String bucketName = "cocadre-" + companyId.ToLower();
            using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
            {
                ListObjectsRequest listRequest = new ListObjectsRequest();
                listRequest.BucketName = bucketName;
                //gamifications/achievements/badges/GA0eecf32b454c4859ae7770cb4da8c17a/
                if (string.IsNullOrEmpty(deleteFileName)) // Delete achievement
                {
                    listRequest.Prefix = "gamifications/achievements/badges/" + achievementId;
                }
                else // Delete older badge
                {
                    listRequest.Prefix = "gamifications/achievements/badges/" + achievementId + "/" + deleteFileName;
                }


                ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                foreach (S3Object imageObject in listResponse.S3Objects)
                {
                    DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                    deleteRequest.BucketName = bucketName;
                    deleteRequest.Key = imageObject.Key;
                    s3Client.DeleteObject(deleteRequest);
                }


                if (string.IsNullOrEmpty(deleteFileName)) // Delete achievement
                {
                    DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                    deleteFolderRequest.BucketName = bucketName;
                    deleteFolderRequest.Key = "gamifications/achievements/badges/" + achievementId;
                    s3Client.DeleteObject(deleteFolderRequest);
                }
            }
        }
    }
}