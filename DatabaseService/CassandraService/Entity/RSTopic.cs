﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class RSTopic
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum RSTopicQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [Serializable]
        public class RSTopicStatus
        {
            public const int CODE_DELETED = -1;
            public const int CODE_UNLISTED = 1;
            public const int CODE_ACTIVE = 2;
            public const int CODE_HIDDEN = 3;

            #region survey status
            /*
             * Deleted  = -1
             * Unlisted / Upcoming  = 1
             * Unlisted / Live = 2
             * Unlisted / Completed = 3 
             * Active / Upcoming  = 4
             * Active / Live = 5
             * Active / Completed = 6
             * Hidden / Upcoming  = 7
             * Hidden / Live = 8
             * Hidden / Completed = 9
             */
            #endregion

            [DataMember]
            public int Code { get; private set; }
            [DataMember]
            public String Title { get; private set; }

            public RSTopicStatus(int code)
            {
                switch (code)
                {
                    case CODE_DELETED:
                        Code = CODE_DELETED;
                        Title = "Deleted";
                        break;
                    case CODE_UNLISTED:
                        Code = CODE_UNLISTED;
                        Title = "Unlisted";
                        break;
                    case CODE_ACTIVE:
                        Code = CODE_ACTIVE;
                        Title = "Active";
                        break;
                    case CODE_HIDDEN:
                        Code = CODE_HIDDEN;
                        Title = "Hidden";
                        break;
                    default:
                        break;
                }
            }
        }

        [DataContract]
        [Serializable]
        public class RSTopicFeedback
        {
            [DataMember]
            public string Feedback { get; set; }
            [DataMember]
            public DateTime CreatedTimestamp { get; set; }
            [DataMember]
            public string CreatedTimestampString { get; set; }
        }

        public enum ProgressStatusEnum
        {
            Upcoming = 1,
            Live = 2,
            Completed = 3
        }

        [DataMember]
        public string TopicId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string IconUrl { get; set; }

        [DataMember]
        public string Introduction { get; set; }

        [DataMember]
        public string ClosingWords { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int ProgressStatus { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public bool IsAnonymous { get; set; }

        [DataMember]
        public int AnonymityCount { get; set; }

        [DataMember]
        public int NumberOfCards { get; set; }

        [DataMember]
        public List<RSCard> Cards { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember]
        public bool IsForUser { get; set; }

        [DataMember]
        public List<User> TargetedUsers { get; set; }

        [DataMember]
        public Progress Progress { get; set; }

        [DataMember]
        public RSTopicCategory RSCategory { get; set; }

        [DataMember]
        public bool IsRandomizedAllCards { get; set; }

        [DataMember]
        public bool IsShowProgressBar { get; set; }

        [DataMember]
        public bool IsAllowToReturnPreviousCard { get; set; }

        [DataMember]
        public bool IsAllowFeedback { get; set; }

        [DataMember]
        public bool IsAllowViewHistory { get; set; }

        [DataMember]
        public User Author { get; set; }

        [DataMember]
        public bool IsCompleted { get; set; }


        public List<RSTopicStatus> SelectRSTopicStatusForDropdown()
        {
            List<RSTopicStatus> topicStatuses = new List<RSTopicStatus>();
            try
            {
                topicStatuses.Add(new RSTopicStatus(RSTopicStatus.CODE_UNLISTED));
                topicStatuses.Add(new RSTopicStatus(RSTopicStatus.CODE_HIDDEN));
                topicStatuses.Add(new RSTopicStatus(RSTopicStatus.CODE_ACTIVE));
                topicStatuses = topicStatuses.OrderBy(x => x.Code).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicStatuses;
        }

        public RSTopicCreateResponse CreateRSTopic(string adminUserId,
                                                   string companyId,
                                                   string title,
                                                   string introduction,
                                                   string closingWords,
                                                   string categoryId,
                                                   string categoryTitle,
                                                   string topicIconUrl,
                                                   int status,
                                                   List<string> targetedDepartmentIds,
                                                   List<string> targetedUserIds,
                                                   DateTime startDate,
                                                   DateTime? endDate,
                                                   int anonymityCount,
                                                   bool isRandomizedAllQuestion = true,
                                                   bool isShowProgressBar = true,
                                                   bool isAllowReturnPrevious = true,
                                                   bool isAllowFeedback = true,
                                                   bool isAllowViewHistory = true)
        {
            RSTopicCreateResponse response = new RSTopicCreateResponse();
            response.Topic = null;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (status != RSTopicStatus.CODE_UNLISTED)
                {
                    Log.Error("Active topic should have at least 1 question");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicActiveNoQuestions);
                    response.ErrorMessage = ErrorMessage.RSTopicActiveNoQuestions;
                    return response;
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicTitleEmpty);
                    response.ErrorMessage = ErrorMessage.RSTopicTitleEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(topicIconUrl))
                {
                    Log.Error("Missing topic icon url");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicIconMissing);
                    response.ErrorMessage = ErrorMessage.RSTopicIconMissing;
                    return response;
                }

                if (string.IsNullOrEmpty(introduction))
                {
                    Log.Error("Introduction is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicIntroductionEmpty);
                    response.ErrorMessage = ErrorMessage.RSTopicIntroductionEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(closingWords))
                {
                    Log.Error("Closing words is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicClosingWordsEmpty);
                    response.ErrorMessage = ErrorMessage.RSTopicClosingWordsEmpty;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                startDate = startDate.ToUniversalTime();

                if (endDate != null)
                {
                    endDate = endDate.Value.ToUniversalTime();
                    if (endDate <= currentTime)
                    {
                        Log.Error("Survey must end before current date");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.RSTopicEndDateEarlierThanToday;
                        return response;
                    }

                    if (startDate >= endDate)
                    {
                        Log.Error("Survey must start before end date");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.RSTopicEndDateEarlierThanStartDate;
                        return response;
                    }
                }

                string topicId = UUIDGenerator.GenerateUniqueIDForRSTopic();

                if (string.IsNullOrEmpty(categoryId))
                {
                    categoryId = UUIDGenerator.GenerateUniqueIDForRSCategory();
                    RSCategoryCreateResponse categoryResponse = new RSTopicCategory().CreateRSCategory(companyId, categoryTitle, adminUserId, session, categoryId, false);

                    if (!categoryResponse.Success)
                    {
                        Log.Error(categoryResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt16(categoryResponse.ErrorCode);
                        response.ErrorMessage = categoryResponse.ErrorMessage;
                        return response;
                    }
                }
                else
                {
                    Row rsCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);
                    if (rsCategoryRow == null)
                    {
                        Log.Error("Invalid RSCategoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                        return response;
                    }
                }

                BatchStatement batchStatement = new BatchStatement();
                List<BoundStatement> privacyStatments = CreatePrivacy(topicId, targetedDepartmentIds, targetedUserIds, session);

                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("rs_topic",
                    new List<string> { "id", "category_id", "title", "introduction", "closing_words", "icon_url", "status", "is_randomized_all_questions", "is_show_progress_bar", "is_allow_return_previous_question", "is_allow_feedback_and_suggestion", "is_allow_to_view_history", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp", "start_date", "end_date", "is_anonymous", "anonymity_count" }));
                batchStatement.Add(ps.Bind(topicId, categoryId, title, introduction, closingWords, topicIconUrl, status, isRandomizedAllQuestion, isShowProgressBar, isAllowReturnPrevious, isAllowFeedback, isAllowViewHistory, adminUserId, currentTime, adminUserId, currentTime, startDate, endDate, (anonymityCount > 0), anonymityCount));

                ps = session.Prepare(CQLGenerator.InsertStatement("rstopic_by_rscategory",
                    new List<string> { "rs_topic_id", "rs_topic_category_id" }));
                batchStatement.Add(ps.Bind(topicId, categoryId));

                ps = session.Prepare(CQLGenerator.InsertStatement("rscategory_by_rstopic",
                    new List<string> { "rs_topic_id", "rs_topic_category_id" }));
                batchStatement.Add(ps.Bind(topicId, categoryId));

                //Only unlisted is allowed in creation
                //if (status == (int)RSTopicStatus.CODE_ACTIVE)
                //{
                //    ps = session.Prepare(CQLGenerator.InsertStatement("rs_topic_by_timestamp",
                //        new List<string> { "rs_category_id", "rs_topic_id", "created_on_timestamp" }));
                //    batchStatement.Add(ps.Bind(categoryId, topicId, currentTime));
                //}

                foreach (BoundStatement bs in privacyStatments)
                {
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);

                response.Topic = SelectBasicRSTopic(adminUserId, companyId, categoryId, null, topicId, false, null, session);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSTopicUpdateResponse UpdateTopic(string topicId,
                                                 string adminUserId,
                                                 string companyId,
                                                 string newTitle,
                                                 string newIntroduction,
                                                 string newClosingWords,
                                                 string newCategoryId,
                                                 string newCategoryTitle,
                                                 string newTopicIconUrl,
                                                 int newStatus,
                                                 List<string> newTargetedDepartmentIds,
                                                 List<string> newTargetedUserIds,
                                                 DateTime newStartDate,
                                                 DateTime? newEndDate,
                                                 int newAnonymityCount,
                                                 bool newIsRandomizedAllQuestion = true,
                                                 bool newIsShowProgressBar = true,
                                                 bool newIsAllowReturnPrevious = true,
                                                 bool newIsAllowFeedback = true,
                                                 bool newIsAllowViewHistory = true)
        {
            RSTopicUpdateResponse response = new RSTopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicTitleEmpty);
                    response.ErrorMessage = ErrorMessage.RSTopicTitleEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(newTopicIconUrl))
                {
                    Log.Error("Missing topic icon url");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicIconMissing);
                    response.ErrorMessage = ErrorMessage.RSTopicIconMissing;
                    return response;
                }

                if (string.IsNullOrEmpty(newIntroduction))
                {
                    Log.Error("Introduction is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicIntroductionEmpty);
                    response.ErrorMessage = ErrorMessage.RSTopicIntroductionEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(newClosingWords))
                {
                    Log.Error("Closing words is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicClosingWordsEmpty);
                    response.ErrorMessage = ErrorMessage.RSTopicClosingWordsEmpty;
                    return response;
                }

                // Check category
                PreparedStatement ps = null;
                bool isUpdateCategory = false;
                string currentCategoryId = string.Empty;

                if (!string.IsNullOrEmpty(newCategoryId))
                {
                    Row rsCategoryRow = vh.ValidateRSTopicCategory(companyId, newCategoryId, session);
                    if (rsCategoryRow == null)
                    {
                        Log.Error("Invalid RSCategoryId: " + newCategoryId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                        return response;
                    }

                    // Fetch current categoryId
                    ps = session.Prepare(CQLGenerator.SelectStatement("rscategory_by_rstopic", new List<string>(), new List<string> { "rs_topic_id" }));
                    Row categoryByTopicRow = session.Execute(ps.Bind(topicId)).FirstOrDefault();

                    if (categoryByTopicRow == null)
                    {
                        Log.Error("Topic already been deleted: " + topicId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicAlreadyDeleted);
                        response.ErrorMessage = ErrorMessage.RSTopicAlreadyDeleted;
                        return response;
                    }
                    else
                    {
                        currentCategoryId = categoryByTopicRow.GetValue<string>("rs_topic_category_id");

                        if (!currentCategoryId.Equals(newCategoryId))
                        {
                            isUpdateCategory = true;
                        }
                    }
                }

                // Check topic
                Row topicRow = vh.ValidateRSTopic(companyId, currentCategoryId, topicId, session);
                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                // Check status first
                int currentStatus = topicRow.GetValue<int>("status");

                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = topicRow.GetValue<DateTime>("start_date");
                currentStartDate = currentStartDate.AddSeconds(-currentStartDate.Second);
                currentStartDate = currentStartDate.AddMilliseconds(-currentStartDate.Millisecond);
                DateTime? currentEndDate = topicRow.GetValue<DateTime?>("end_date");

                if (currentEndDate.HasValue)
                {
                    currentEndDate = currentEndDate.Value.AddSeconds(-currentEndDate.Value.Second);
                    currentEndDate = currentEndDate.Value.AddMilliseconds(-currentEndDate.Value.Millisecond);
                }

                int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);

                if (currentStatus != (int)RSTopicStatus.CODE_UNLISTED)
                {
                    // Status active or hidden
                    // Check progress
                    newStartDate = newStartDate.ToUniversalTime();
                    // Change of start date
                    if (currentStartDate != newStartDate)
                    {
                        if (progress != (int)ProgressStatusEnum.Upcoming)
                        {
                            Log.Error("Survey in progress, start date cannot be changed: " + topicId);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicStartDateCannotBeModified);
                            response.ErrorMessage = ErrorMessage.RSTopicStartDateCannotBeModified;
                            return response;
                        }
                    }
                }

                // Change of end date
                if (currentEndDate != newEndDate)
                {
                    if (newEndDate.HasValue)
                    {
                        newEndDate = newEndDate.Value.ToUniversalTime();

                        if (newEndDate <= currentTime)
                        {
                            Log.Error("Survey must end after current date");
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicEndDateEarlierThanToday);
                            response.ErrorMessage = ErrorMessage.RSTopicEndDateEarlierThanToday;
                            return response;
                        }

                        if (newStartDate >= newEndDate)
                        {
                            Log.Error("Survey must start before end date");
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicEndDateEarlierThanStartDate);
                            response.ErrorMessage = ErrorMessage.RSTopicEndDateEarlierThanStartDate;
                            return response;
                        }
                    }
                }

                // if date validated
                if (string.IsNullOrEmpty(newCategoryId))
                {
                    newCategoryId = UUIDGenerator.GenerateUniqueIDForRSCategory();
                    RSCategoryCreateResponse categoryResponse = new RSTopicCategory().CreateRSCategory(companyId, newCategoryTitle, adminUserId, session, newCategoryId, false);

                    if (!categoryResponse.Success)
                    {
                        Log.Error(categoryResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt16(categoryResponse.ErrorCode);
                        response.ErrorMessage = categoryResponse.ErrorMessage;
                        return response;
                    }
                }


                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                DateTimeOffset createdTimestamp = topicRow.GetValue<DateTimeOffset>("created_on_timestamp");
                string createdAdminId = topicRow.GetValue<string>("created_by_admin_id");

                if (isUpdateCategory)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_topic", new List<string> { "category_id", "id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, topicId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_topic_by_timestamp", new List<string> { "rs_category_id", "created_on_timestamp", "rs_topic_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, createdTimestamp, topicId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rstopic_by_rscategory", new List<string> { "rs_topic_category_id", "rs_topic_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, topicId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rscategory_by_rstopic", new List<string> { "rs_topic_category_id", "rs_topic_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, topicId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("rstopic_by_rscategory",
                        new List<string> { "rs_topic_id", "rs_topic_category_id" }));
                    updateBatch.Add(ps.Bind(topicId, newCategoryId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("rscategory_by_rstopic",
                        new List<string> { "rs_topic_id", "rs_topic_category_id" }));
                    updateBatch.Add(ps.Bind(topicId, newCategoryId));
                }

                // Active -> insert into timestamp
                if (newStatus == (int)RSTopicStatus.CODE_ACTIVE)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_topic_by_timestamp",
                        new List<string> { "rs_category_id", "rs_topic_id", "created_on_timestamp" }));
                    updateBatch.Add(ps.Bind(newCategoryId, topicId, createdTimestamp));
                }
                else
                {
                    // Not active -> remove from timestamp
                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_topic_by_timestamp", new List<string> { "rs_category_id", "created_on_timestamp", "rs_topic_id" }));
                    deleteBatch.Add(ps.Bind(newCategoryId, createdTimestamp, topicId));
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("rs_topic",
                   new List<string> { "id", "category_id" }, new List<string> { "title", "introduction", "closing_words", "icon_url", "status", "is_randomized_all_questions", "is_show_progress_bar", "is_allow_return_previous_question", "is_allow_feedback_and_suggestion", "is_allow_to_view_history", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp", "start_date", "end_date", "is_anonymous", "anonymity_count" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newIntroduction, newClosingWords, newTopicIconUrl, newStatus, newIsRandomizedAllQuestion, newIsShowProgressBar, newIsAllowReturnPrevious, newIsAllowFeedback, newIsAllowViewHistory, adminUserId, createdTimestamp, adminUserId, currentTime, newStartDate, newEndDate, (newAnonymityCount > 0), newAnonymityCount, topicId, newCategoryId));

                if (progress == (int)ProgressStatusEnum.Upcoming || currentStatus == (int)RSTopic.RSTopicStatus.CODE_UNLISTED)
                {
                    List<BoundStatement> privacyStatement = UpdatePrivacy(topicId, newTargetedDepartmentIds, newTargetedUserIds, session);
                    foreach (BoundStatement bs in privacyStatement)
                    {
                        updateBatch.Add(bs);
                    }
                }


                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            Log.Debug("要return");
            return response;
        }

        private List<BoundStatement> UpdatePrivacy(string topicId, List<string> newTargetedDepartmentIds, List<string> newTargetedUserIds, ISession session)
        {
            try
            {
                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_department",
                    new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet departmentPrivacyRowset = session.Execute(ps.Bind(topicId));

                foreach (Row departmentPrivacyRow in departmentPrivacyRowset)
                {
                    string departmentId = departmentPrivacyRow.GetValue<string>("department_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_department_targeted_topic", new List<string> { "department_id", "rs_topic_id" }));
                    deleteBatch.Add(ps.Bind(departmentId, topicId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_topic_targeted_department", new List<string> { "rs_topic_id" }));
                deleteBatch.Add(ps.Bind(topicId));

                ps = session.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_user",
                    new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet userPrivacyRowset = session.Execute(ps.Bind(topicId));

                foreach (Row userPrivacyRow in userPrivacyRowset)
                {
                    string userId = userPrivacyRow.GetValue<string>("user_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_user_targeted_topic", new List<string> { "user_id", "rs_topic_id" }));
                    deleteBatch.Add(ps.Bind(userId, topicId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_topic_targeted_user", new List<string> { "rs_topic_id" }));
                deleteBatch.Add(ps.Bind(topicId));

                session.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return CreatePrivacy(topicId, newTargetedDepartmentIds, newTargetedUserIds, session);
        }

        private List<BoundStatement> CreatePrivacy(string topicId, List<string> targetedDepartmentIds, List<string> targetedUserIds, ISession session)
        {
            List<BoundStatement> boundStatements = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;
                bool isForDepartment = targetedDepartmentIds == null || targetedDepartmentIds.Count == 0 ? false : true;
                bool isForUser = targetedUserIds == null || targetedUserIds.Count == 0 ? false : true;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                ps = session.Prepare(CQLGenerator.InsertStatement("rs_topic_privacy", new List<string> { "rs_topic_id", "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group" }));
                boundStatements.Add(ps.Bind(topicId, isForEveryone, isForDepartment, isForUser, false));

                foreach (string departmentId in targetedDepartmentIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_department_targeted_topic", new List<string> { "department_id", "rs_topic_id" }));
                    boundStatements.Add(ps.Bind(departmentId, topicId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_topic_targeted_department", new List<string> { "department_id", "rs_topic_id" }));
                    boundStatements.Add(ps.Bind(departmentId, topicId));
                }

                foreach (string userId in targetedUserIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_topic_targeted_user", new List<string> { "user_id", "rs_topic_id" }));
                    boundStatements.Add(ps.Bind(userId, topicId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_user_targeted_topic", new List<string> { "user_id", "rs_topic_id" }));
                    boundStatements.Add(ps.Bind(userId, topicId));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatements;
        }

        private bool CheckPrivacy(string topicId, string userId, bool isForEveryone, bool isForDepartment, bool isForUser, List<string> departmentIds, ISession session)
        {
            bool isSurveyForCurrentUser = false;

            if (isForEveryone)
            {
                isSurveyForCurrentUser = true;
            }
            else
            {
                if (isForDepartment)
                {
                    foreach (string departmentId in departmentIds)
                    {
                        PreparedStatement psTargetedDepartment = session.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_department",
                            new List<string>(), new List<string> { "rs_topic_id", "department_id" }));
                        Row departmentPrivacyRow = session.Execute(psTargetedDepartment.Bind(topicId, departmentId)).FirstOrDefault();

                        if (departmentPrivacyRow != null)
                        {
                            isSurveyForCurrentUser = true;
                            break;
                        }
                    }
                }

                if (!isSurveyForCurrentUser && isForUser)
                {
                    PreparedStatement psFeedTargetedUser = session.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_user",
                            new List<string>(), new List<string> { "rs_topic_id", "user_id" }));
                    Row userPrivacyRow = session.Execute(psFeedTargetedUser.Bind(topicId, userId)).FirstOrDefault();

                    if (userPrivacyRow != null)
                    {
                        isSurveyForCurrentUser = true;
                    }
                }
            }

            return isSurveyForCurrentUser;
        }

        public RSTopicSelectAllBasicResponse SelectAllBasicByCategory(string adminUserId, string companyId, string selectedCategoryId = null, string containsName = null)
        {
            RSTopicSelectAllBasicResponse response = new RSTopicSelectAllBasicResponse();
            response.Topics = new List<RSTopic>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                RowSet categoryRowset = null;

                if (string.IsNullOrEmpty(selectedCategoryId))
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_category", new List<string>(), new List<string> { "company_id", "is_valid" }));
                    categoryRowset = mainSession.Execute(ps.Bind(companyId, true));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_category", new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                    categoryRowset = mainSession.Execute(ps.Bind(companyId, selectedCategoryId, true));
                }

                foreach (Row categoryRow in categoryRowset)
                {
                    string categoryId = categoryRow.GetValue<string>("id");
                    string title = categoryRow.GetValue<string>("title");

                    RSTopicCategory category = new RSTopicCategory
                    {
                        CategoryId = categoryId,
                        Title = title
                    };

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rstopic_by_rscategory", new List<string>(), new List<string> { "rs_topic_category_id" }));
                    RowSet topicRowset = mainSession.Execute(ps.Bind(categoryId));

                    foreach (Row topicRow in topicRowset)
                    {
                        string topicId = topicRow.GetValue<string>("rs_topic_id");
                        RSTopic selectedTopic = SelectBasicRSTopic(adminUserId, companyId, category.CategoryId, category, topicId, true, containsName, mainSession, analyticSession);

                        if (selectedTopic != null)
                        {
                            response.Topics.Add(selectedTopic);
                        }
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSTopic SelectBasicRSTopic(string requesterUserId, string companyId, string categoryId, RSTopicCategory category, string topicId, bool isCompletionDetailNeeded = false, string containsName = null, ISession mainSession = null, ISession analyticSession = null)
        {
            RSTopic topic = null;
            try
            {
                if (category == null && !string.IsNullOrEmpty(categoryId))
                {
                    category = new RSTopicCategory().SelectRSCategory(categoryId, companyId, mainSession).Category;
                }
                else if (category == null && string.IsNullOrEmpty(categoryId))
                {
                    PreparedStatement psCategoryByTopic = mainSession.Prepare(CQLGenerator.SelectStatement("rscategory_by_rstopic",
                        new List<string> { "rs_topic_category_id" }, new List<string> { "rs_topic_id" }));
                    BoundStatement bsCategoryByTopic = psCategoryByTopic.Bind(topicId);
                    Row categoryByTopicRow = mainSession.Execute(bsCategoryByTopic).FirstOrDefault();

                    if (categoryByTopicRow != null)
                    {
                        categoryId = categoryByTopicRow.GetValue<string>("rs_topic_category_id");
                        category = new RSTopicCategory().SelectRSCategory(categoryId, companyId, mainSession).Category;
                    }
                    else
                    {
                        Log.Error("RSTopic does not have a RSCategory: " + topicId);
                    }
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic", new List<string>(), new List<string> { "category_id", "id" }));
                Row topicRow = mainSession.Execute(ps.Bind(categoryId, topicId)).FirstOrDefault();

                int status = topicRow.GetValue<int>("status");

                if (status == (int)RSTopicStatus.CODE_DELETED)
                {
                    return topic;
                }

                string title = topicRow.GetValue<string>("title");

                // Check progress first
                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? currentEndDate = topicRow.GetValue<DateTime?>("end_date");
                int progressStatus = (int)ProgressStatusEnum.Upcoming;

                if (currentStartDate <= currentTime)
                {
                    progressStatus = (int)ProgressStatusEnum.Live;
                }

                if (currentEndDate.HasValue)
                {
                    if (currentEndDate <= currentTime)
                    {
                        progressStatus = (int)ProgressStatusEnum.Completed;
                    }
                }

                if (!string.IsNullOrEmpty(containsName))
                {
                    if (!title.ToLower().Contains(containsName.ToLower()))
                    {
                        return topic;
                    }
                }

                bool isAnonymous = topicRow.GetValue<bool>("is_anonymous");
                string iconUrl = topicRow.GetValue<string>("icon_url");
                int anonymityCount = 0;
                if (topicRow.IsNull("anonymity_count"))
                {
                    if (isAnonymous)
                    {
                        anonymityCount = 1;
                    }

                    #region Update null value of anonymity_count column.
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("rs_topic",
                   new List<string> { "id", "category_id" }, new List<string> { "anonymity_count" }, new List<string>()));
                    BatchStatement updateBatch = new BatchStatement();
                    updateBatch.Add(ps.Bind(anonymityCount, topicId, categoryId));
                    mainSession.Execute(updateBatch);
                    #endregion
                }
                else
                {
                    anonymityCount = topicRow.GetValue<int>("anonymity_count");
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_privacy", new List<string>(), new List<string> { "rs_topic_id" }));
                Row topicPrivacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                bool isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                bool isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");


                Progress progress = new Progress();

                if (isCompletionDetailNeeded)
                {
                    if (analyticSession == null)
                    {
                        ConnectionManager cm = new ConnectionManager();
                        analyticSession = cm.getAnalyticSession();
                    }

                    //progress =  SelectProgress(topicId, companyId, isForEveryone, isForDepartment, isForUser, mainSession, analyticSession);
                }

                int numberOfCards = new RSCard().SelectNumberOfCards(topicId, mainSession);

                topic = new RSTopic
                {
                    TopicId = topicId,
                    Title = title,
                    IconUrl = iconUrl,
                    RSCategory = category,
                    NumberOfCards = numberOfCards,
                    Status = status,
                    IsForEveryone = isForEveryone,
                    IsForDepartment = isForDepartment,
                    IsForUser = isForUser,
                    Progress = progress,
                    ProgressStatus = progressStatus,
                    IsAnonymous = isAnonymous,
                    AnonymityCount = anonymityCount
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topic;
        }

        public RSTopicSelectResponse SelectRSTopicByUser(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            RSTopicSelectResponse response = new RSTopicSelectResponse();
            response.Topic = new RSTopic();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }
                RSTopicCategory category = new RSTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                AnalyticSurvey analyticsManager = new AnalyticSurvey();
                if (analyticsManager.CheckRSCompleted(topicId, requesterUserId, analyticSession))
                {
                    Log.Error("Survey already completed by user: " + requesterUserId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicAlreadyCompleted);
                    response.ErrorMessage = ErrorMessage.RSTopicAlreadyCompleted;
                    return response;
                }

                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");

                // Track progress
                DateTime currentTime = DateTime.UtcNow;
                if (endDate.HasValue && endDate.Value <= currentTime)
                {
                    Log.Error("Survey has ended: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicEnded);
                    response.ErrorMessage = ErrorMessage.RSTopicEnded;
                    return response;
                }


                int status = topicRow.GetValue<int>("status");
                string iconUrl = topicRow.GetValue<string>("icon_url");
                string title = topicRow.GetValue<string>("title");
                string introduction = topicRow.GetValue<string>("introduction");
                string closingWords = topicRow.GetValue<string>("closing_words");
                bool isRandomizedAllCards = topicRow.GetValue<bool>("is_randomized_all_questions");
                bool isShowProgressBar = topicRow.GetValue<bool>("is_show_progress_bar");
                bool isAllowToReturnToPreviousCard = topicRow.GetValue<bool>("is_allow_return_previous_question");
                bool isAllowFeedback = topicRow.GetValue<bool>("is_allow_feedback_and_suggestion");
                bool isAllowViewHistory = topicRow.GetValue<bool>("is_allow_to_view_history");
                bool isAnonymous = topicRow.GetValue<bool>("is_anonymous");
                int anonymityCount = 0;
                if (topicRow.IsNull("anonymity_count"))
                {
                    if (isAnonymous)
                    {
                        anonymityCount = 1;
                    }

                    #region Update null value of anonymity_count column.
                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("rs_topic",
                    new List<string> { "id", "category_id" }, new List<string> { "anonymity_count" }, new List<string>()));
                    BatchStatement updateBatch = new BatchStatement();
                    updateBatch.Add(ps.Bind(anonymityCount, topicId, categoryId));
                    session.Execute(updateBatch);
                    #endregion

                }
                else
                {
                    anonymityCount = topicRow.GetValue<int>("anonymity_count");
                }


                List<RSCard> cards = new RSCard().SelectAllCardsByUser(requesterUserId, topicId, session, analyticSession).Cards;
                response.Topic = new RSTopic
                {
                    TopicId = topicId,
                    Title = title,
                    IconUrl = iconUrl,
                    Introduction = introduction,
                    ClosingWords = closingWords,
                    RSCategory = category,
                    Status = status,
                    IsRandomizedAllCards = isRandomizedAllCards,
                    IsAllowToReturnPreviousCard = isAllowToReturnToPreviousCard,
                    IsShowProgressBar = isShowProgressBar,
                    IsAllowFeedback = isAllowFeedback,
                    IsAllowViewHistory = isAllowViewHistory,
                    Cards = cards,
                    NumberOfCards = cards.Count(),
                    IsAnonymous = isAnonymous,
                    AnonymityCount = anonymityCount
                };

                // Update analytics
                analyticsManager.UpdateRSStartProgressByUser(requesterUserId, topicId, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSTopicSelectResponse SelectFullDetailRSTopic(string adminUserId, string companyId, string topicId, string categoryId, string containsCardName = null, ISession session = null)
        {
            RSTopicSelectResponse response = new RSTopicSelectResponse();
            response.Topic = new RSTopic();
            try
            {
                ValidationHandler vh = new ValidationHandler();
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }
                RSTopicCategory category = new RSTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                int status = topicRow.GetValue<int>("status");
                string iconUrl = topicRow.GetValue<string>("icon_url");
                string title = topicRow.GetValue<string>("title");
                string introduction = topicRow.GetValue<string>("introduction");
                string closingWords = topicRow.GetValue<string>("closing_words");
                bool isAnonymous = topicRow.GetValue<bool>("is_anonymous");
                int anonymityCount = 0;
                if (topicRow.IsNull("anonymity_count"))
                {
                    if (isAnonymous)
                    {
                        anonymityCount = 1;
                    }
                }
                else
                {
                    anonymityCount = topicRow.GetValue<int>("anonymity_count");
                }


                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");
                int progressStatus = (int)ProgressStatusEnum.Upcoming;

                // Track progress
                DateTime currentTime = DateTime.UtcNow;
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, session);
                if (endDate.HasValue && endDate.Value <= currentTime)
                {
                    progressStatus = (int)ProgressStatusEnum.Completed;
                    endDate = endDate.Value.AddHours(timezoneOffset);
                }
                else
                {
                    if (startDate <= currentTime)
                    {
                        progressStatus = (int)ProgressStatusEnum.Live;
                        if (endDate.HasValue)
                        {
                            endDate = endDate.Value.AddHours(timezoneOffset);
                        }
                    }
                }

                startDate = startDate.AddHours(timezoneOffset);

                bool isRandomizedAllCards = topicRow.GetValue<bool>("is_randomized_all_questions");
                bool isShowProgressBar = topicRow.GetValue<bool>("is_show_progress_bar");
                bool isAllowToReturnToPreviousCard = topicRow.GetValue<bool>("is_allow_return_previous_question");
                bool isAllowFeedback = topicRow.GetValue<bool>("is_allow_feedback_and_suggestion");
                bool isAllowViewHistory = topicRow.GetValue<bool>("is_allow_to_view_history");

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_topic_privacy", new List<string>(), new List<string> { "rs_topic_id" }));
                Row topicPrivacyRow = session.Execute(ps.Bind(topicId)).FirstOrDefault();

                bool isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                bool isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");

                List<Department> selectedDepartments = new List<Department>();
                List<User> selectedUsers = new List<User>();

                if (!isForEveryone)
                {
                    if (isForDepartment)
                    {
                        Department departmentManager = new Department();
                        ps = session.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_department", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet departmentRowset = session.Execute(ps.Bind(topicId));
                        foreach (Row departmentRow in departmentRowset)
                        {
                            string departmentId = departmentRow.GetValue<string>("department_id");
                            Department department = departmentManager.GetDepartmentDetail(adminUserId, companyId, departmentId, Department.QUERY_TYPE_BASIC, session).Department;
                            if (department != null)
                            {
                                selectedDepartments.Add(department);
                            }
                        }
                    }

                    if (isForUser)
                    {
                        User userManager = new User();
                        ps = session.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_user", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet userRowset = session.Execute(ps.Bind(topicId));
                        foreach (Row userRow in userRowset)
                        {
                            string userId = userRow.GetValue<string>("user_id");
                            User user = userManager.SelectUserBasic(userId, companyId, true, session).User;
                            if (user != null)
                            {
                                selectedUsers.Add(user);
                            }
                        }
                    }
                }

                response.Topic = new RSTopic
                {
                    TopicId = topicId,
                    Title = title,
                    IconUrl = iconUrl,
                    Introduction = introduction,
                    ClosingWords = closingWords,
                    RSCategory = category,
                    IsForEveryone = isForEveryone,
                    IsForDepartment = isForDepartment,
                    TargetedDepartments = selectedDepartments,
                    IsForUser = isForUser,
                    TargetedUsers = selectedUsers,
                    Status = status,
                    IsRandomizedAllCards = isRandomizedAllCards,
                    IsAllowToReturnPreviousCard = isAllowToReturnToPreviousCard,
                    IsShowProgressBar = isShowProgressBar,
                    IsAllowFeedback = isAllowFeedback,
                    IsAllowViewHistory = isAllowViewHistory,
                    Cards = new RSCard().SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)RSCard.RSCardQueryType.Basic, containsCardName, session).Cards,
                    StartDate = startDate,
                    EndDate = endDate,
                    IsAnonymous = isAnonymous,
                    AnonymityCount = anonymityCount,
                    ProgressStatus = progressStatus
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public Progress SelectCompletionProgress(string topicId, string companyId, bool isForEveryone, bool isForDepartment, bool isForUser, ISession mainSession, ISession analyticSession)
        {
            User userManager = new User();

            int totalNumberOfUsers = 0;

            if (isForEveryone)
            {
                totalNumberOfUsers = userManager.GetAllUserForAdmin(null, companyId, null, 0, (int)User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession).Users.Count();
            }
            else
            {
                PreparedStatement ps = null;
                List<string> targetedUserIds = new List<string>();

                if (isForDepartment)
                {
                    List<string> departmentIds = new List<string>();
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_department", new List<string>(), new List<string> { "rs_topic_id" }));
                    RowSet departmentRowset = mainSession.Execute(ps.Bind(topicId));

                    foreach (Row departmentRow in departmentRowset)
                    {
                        string departmentId = departmentRow.GetValue<string>("department_id");
                        departmentIds.Add(departmentId);
                    }

                    targetedUserIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, null, companyId, mainSession);
                }

                if (isForUser)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_user", new List<string>(), new List<string> { "rs_topic_id" }));
                    RowSet userRowset = mainSession.Execute(ps.Bind(topicId));

                    foreach (Row userRow in userRowset)
                    {
                        string userId = userRow.GetValue<string>("user_id");
                        if (!targetedUserIds.Contains(userId))
                        {
                            targetedUserIds.Add(userId);
                        }
                    }
                }

                totalNumberOfUsers = targetedUserIds.Count();
            }

            int numberOfCompletedUser = new AnalyticSurvey().SelectCompletedNumberOfUsersForRSTopic(topicId, analyticSession);

            Progress progress = new Progress
            {
                NumberOfCompletedUsers = numberOfCompletedUser,
                TotalNumberOfTargetedUsers = totalNumberOfUsers,
                Percentage = (int)((double)numberOfCompletedUser / totalNumberOfUsers * 100)
            };

            return progress;
        }

        public RSTopicSelectAllByCategoryResponse SelectAllBasicByUser(string requesterUserId, string companyId, bool isShowCompleted = false, ISession mainSession = null, ISession analyticSession = null)
        {
            RSTopicSelectAllByCategoryResponse response = new RSTopicSelectAllByCategoryResponse();
            response.RSCategories = new List<RSTopicCategory>();
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_category_by_title", new List<string>(), new List<string> { "company_id", "is_for_event" }));
                RowSet categoryByTitleRowset = mainSession.Execute(ps.Bind(companyId, false));

                AnalyticSurvey analyticsManager = new AnalyticSurvey();
                Department departmentManager = new Department();
                User userManager = new User();
                RSCard cardManager = new RSCard();

                foreach (Row categoryByTitleRow in categoryByTitleRowset)
                {
                    string categoryId = categoryByTitleRow.GetValue<string>("rs_category_id");

                    Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);

                    if (categoryRow == null)
                    {
                        continue;
                    }

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_by_timestamp", new List<string>(), new List<string> { "rs_category_id" }));
                    RowSet topicByCategoryRowset = mainSession.Execute(ps.Bind(categoryId));

                    string categoryTitle = categoryRow.GetValue<string>("title");
                    RSTopicCategory category = new RSTopicCategory
                    {
                        CategoryId = categoryId,
                        Title = categoryTitle
                    };
                    category.Topics = new List<RSTopic>();

                    DateTime currentTime = DateTime.UtcNow;
                    foreach (Row topicByCategoryRow in topicByCategoryRowset)
                    {
                        bool isCompleted = false;
                        string topicId = topicByCategoryRow.GetValue<string>("rs_topic_id");
                        Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                        if (topicRow == null)
                        {
                            continue;
                        }

                        // Survey upcoming
                        if (currentTime < topicRow.GetValue<DateTime>("start_date"))
                        {
                            continue;
                        }

                        // Survey ended
                        if (topicRow["end_date"] != null && currentTime >= topicRow.GetValue<DateTime>("end_date"))
                        {
                            continue;
                        }

                        if (topicRow.GetValue<int>("status") != RSTopic.RSTopicStatus.CODE_ACTIVE)
                        {
                            continue;
                        }

                        if (cardManager.SelectNumberOfCards(topicId, mainSession) <= 0)
                        {
                            continue;
                        }

                        if (analyticsManager.CheckRSCompleted(topicId, requesterUserId, analyticSession))
                        {
                            isCompleted = true;

                            if (!isShowCompleted)
                            {
                                continue;
                            }
                        }

                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_privacy", new List<string>(), new List<string> { "rs_topic_id" }));
                        Row privacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                        bool isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                        bool isForUser = privacyRow.GetValue<bool>("is_for_user");
                        bool isForDepartment = privacyRow.GetValue<bool>("is_for_department");

                        List<Department> departments = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;
                        List<string> departmentIds = new List<string>();

                        foreach (Department department in departments)
                        {
                            departmentIds.Add(department.Id);
                        }

                        if (!CheckPrivacy(topicId, requesterUserId, isForEveryone, isForDepartment, isForUser, departmentIds, mainSession))
                        {
                            continue;
                        }

                        string iconUrl = topicRow.GetValue<string>("icon_url");
                        string title = topicRow.GetValue<string>("title");
                        string introduction = topicRow.GetValue<string>("introduction");
                        string creatorUserId = topicRow.GetValue<string>("created_by_admin_id");
                        bool isAnonymous = topicRow.GetValue<bool>("is_anonymous");
                        RSTopic topic = new RSTopic
                        {
                            TopicId = topicId,
                            Introduction = introduction,
                            IconUrl = iconUrl,
                            Title = title,
                            Author = userManager.SelectUserBasic(creatorUserId, companyId, false, mainSession).User,
                            IsCompleted = isCompleted,
                            IsAnonymous = isAnonymous
                        };

                        category.Topics.Add(topic);
                    }

                    if (category.Topics.Count > 0)
                    {
                        response.RSCategories.Add(category);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSTopicUpdateResponse UpdateTopicStatus(string topicId, string categoryId, string adminUserId, string companyId, int updatedStatus)
        {
            RSTopicUpdateResponse response = new RSTopicUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }
                RSTopicCategory category = new RSTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                int currentStatus = topicRow.GetValue<int>("status");

                if (currentStatus == (int)RSTopicStatus.CODE_DELETED)
                {
                    Log.Error("Topic already been deleted");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.RSTopicAlreadyDeleted;
                    return response;
                }

                DateTimeOffset createdTimestamp = topicRow.GetValue<DateTimeOffset>("created_on_timestamp");

                if (updatedStatus == (int)RSTopicStatus.CODE_DELETED || updatedStatus == (int)RSTopicStatus.CODE_HIDDEN)
                {
                    if (updatedStatus == (int)RSTopicStatus.CODE_DELETED)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("rstopic_by_rscategory", new List<string> { "rs_topic_category_id", "rs_topic_id" }));
                        deleteBatch.Add(ps.Bind(categoryId, topicId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rscategory_by_rstopic", new List<string> { "rs_topic_category_id", "rs_topic_id" }));
                        deleteBatch.Add(ps.Bind(categoryId, topicId));

                        // Delete all cards
                        new RSCard().DeleteAllCards(topicId, companyId, session);
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_topic_by_timestamp", new List<string> { "rs_category_id", "created_on_timestamp", "rs_topic_id" }));
                    deleteBatch.Add(ps.Bind(categoryId, createdTimestamp, topicId));
                }
                else if (updatedStatus == (int)RSTopicStatus.CODE_ACTIVE)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_topic_by_timestamp", new List<string> { "rs_category_id", "created_on_timestamp", "rs_topic_id" }));
                    updateBatch.Add(ps.Bind(categoryId, createdTimestamp, topicId));
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("rs_topic", new List<string> { "category_id", "id" }, new List<string> { "status", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(updatedStatus, adminUserId, DateTime.UtcNow, categoryId, topicId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateBounceActivity(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }
                RSTopicCategory category = new RSTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                AnalyticSurvey analyticManager = new AnalyticSurvey();
                response = analyticManager.UpdateRSBounceActivity(requesterUserId, topicId, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateCompletionActivity(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }
                RSTopicCategory category = new RSTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                AnalyticSurvey analyticManager = new AnalyticSurvey();
                if (analyticManager.CheckRSCompleted(topicId, requesterUserId, analyticSession))
                {
                    Log.Error("Survey already completed by user: " + requesterUserId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicAlreadyCompleted);
                    response.ErrorMessage = ErrorMessage.RSTopicAlreadyCompleted;
                    return response;
                }

                List<RSCard> cards = new RSCard().SelectAllCards(topicId, null, null, null, (int)RSCard.RSCardQueryType.FullDetail, null, mainSession).Cards;
                response = analyticManager.UpdateRSCompletionActivity(requesterUserId, topicId, cards, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectRSResultOverviewResponse SelectResultOverview(string adminUserId, string companyId, string topicId, string categoryId)
        {
            AnalyticSelectRSResultOverviewResponse response = new AnalyticSelectRSResultOverviewResponse();
            response.Overview = new AnalyticSurvey.RSAnalyticOverview();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                PreparedStatement ps = null;

                // Get total targeted audience
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_privacy", new List<string>(), new List<string> { "rs_topic_id" }));
                Row topicPrivacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();
                bool isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                bool isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");

                int totalTargetedAudience = 0;

                User userManager = new User();
                if (isForEveryone)
                {
                    totalTargetedAudience = userManager.GetAllUserForAdmin(adminUserId, companyId, string.Empty, 0, User.AccountStatus.CODE_ACTIVE, new List<string>(), false, false, null, mainSession).Users.Count();
                }
                else
                {
                    List<string> targetedUserIds = new List<string>();

                    if (isForDepartment)
                    {
                        List<string> departmentIds = new List<string>();
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_department", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet departmentRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row departmentRow in departmentRowset)
                        {
                            string departmentId = departmentRow.GetValue<string>("department_id");
                            departmentIds.Add(departmentId);
                        }

                        targetedUserIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, null, companyId, mainSession);
                    }

                    if (isForUser)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_user", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet userRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row userRow in userRowset)
                        {
                            string userId = userRow.GetValue<string>("user_id");
                            if (!targetedUserIds.Contains(userId))
                            {
                                targetedUserIds.Add(userId);
                            }
                        }
                    }

                    totalTargetedAudience = targetedUserIds.Count();
                }

                // Get total pages and cards
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardOrderRowset = mainSession.Execute(ps.Bind(topicId));

                int numberOfPages = 1;
                int numberOfCards = 0;
                foreach (Row cardOrderRow in cardOrderRowset)
                {
                    string cardId = cardOrderRow.GetValue<string>("card_id");
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id", "id" }));
                    Row cardRow = mainSession.Execute(ps.Bind(topicId, cardId)).FirstOrDefault();
                    if (cardRow != null)
                    {
                        bool hasPageBreak = cardRow.GetValue<bool>("has_page_break");
                        if (hasPageBreak)
                        {
                            numberOfPages++;
                        }
                        numberOfCards++;
                    }
                }

                AnalyticSurvey analyticManager = new AnalyticSurvey();
                response.Overview = analyticManager.SelectRSResultOverview(topicId, numberOfPages, numberOfCards, totalTargetedAudience, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), analyticSession);

                // Get topic
                response.Overview.Topic = SelectBasicRSTopic(adminUserId, companyId, categoryId, null, topicId, false, null, mainSession, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectRSResponderReportResponse SelectRespondersReport(string adminUserId, string companyId, string topicId, string categoryId)
        {
            AnalyticSelectRSResponderReportResponse response = new AnalyticSelectRSResponderReportResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                //Anonymous
                bool isAnonymous = topicRow.GetValue<bool>("is_anonymous");
                int anonymityCount = 0;
                if (topicRow.IsNull("anonymity_count"))
                {
                    if (isAnonymous)
                    {
                        anonymityCount = 1;
                    }
                }
                else
                {
                    anonymityCount = topicRow.GetValue<int>("anonymity_count");
                }

                // Get total targeted audience
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_privacy", new List<string>(), new List<string> { "rs_topic_id" }));
                Row topicPrivacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();
                bool isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                bool isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");

                List<User> targetedAudience = new List<User>();

                User userManager = new User();
                if (isForEveryone)
                {
                    targetedAudience = userManager.GetAllUserForAdmin(adminUserId, companyId, string.Empty, 0, User.AccountStatus.CODE_ACTIVE, new List<string>(), false, false, null, mainSession).Users;
                }
                else
                {
                    List<string> targetedUserIds = new List<string>();

                    if (isForDepartment)
                    {
                        List<string> departmentIds = new List<string>();
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_department", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet departmentRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row departmentRow in departmentRowset)
                        {
                            string departmentId = departmentRow.GetValue<string>("department_id");
                            departmentIds.Add(departmentId);
                        }

                        targetedUserIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, null, companyId, mainSession);
                    }

                    if (isForUser)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_user", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet userRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row userRow in userRowset)
                        {
                            string userId = userRow.GetValue<string>("user_id");
                            if (!targetedUserIds.Contains(userId))
                            {
                                targetedUserIds.Add(userId);
                            }
                        }
                    }

                    foreach (string targetedUserId in targetedUserIds)
                    {
                        User targetedUser = userManager.SelectUserBasic(targetedUserId, companyId, false, mainSession, null, true).User;
                        if (targetedUser != null)
                        {
                            targetedAudience.Add(targetedUser);
                        }
                    }
                }

                AnalyticSurvey analyticManager = new AnalyticSurvey();
                response = analyticManager.SelectRSRespondersReport(topicId, isAnonymous, targetedAudience, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), mainSession, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public int SelectProgress(DateTime startDate, DateTime? endDate, DateTime currentTime)
        {
            int progress = (int)ProgressStatusEnum.Upcoming;
            try
            {
                if (startDate <= currentTime)
                {
                    progress = (int)ProgressStatusEnum.Live;

                    if (endDate.HasValue && endDate <= currentTime)
                    {
                        progress = (int)ProgressStatusEnum.Completed;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return progress;
        }

        public RSTopicUpdateFeedbackResponse CreateFeedback(string userId, string companyId, string topicId, string topicCategoryId, string feedback)
        {
            RSTopicUpdateFeedbackResponse response = new RSTopicUpdateFeedbackResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(userId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, topicCategoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                if (!string.IsNullOrEmpty(feedback.Trim()))
                {
                    PreparedStatement ps = mainSession.Prepare(CQLGenerator.UpdateStatement("rs_feedback",
                        new List<string> { "rs_topic_id", "user_id" }, new List<string> { "feedback", "created_on_timestamp" }, new List<string>()));
                    mainSession.Execute(ps.Bind(feedback.Trim(), DateTime.UtcNow, topicId, userId));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSTopicSelectFeedbackResponse SelectFeedback(string adminUserId, string companyId, string topicId, string categoryId)
        {
            RSTopicSelectFeedbackResponse response = new RSTopicSelectFeedbackResponse();
            response.Feedbacks = new List<RSTopicFeedback>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_feedback", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet feedbackRowSet = mainSession.Execute(ps.Bind(topicId));

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                foreach (Row feedbackRow in feedbackRowSet)
                {
                    string feedback = feedbackRow.GetValue<string>("feedback");
                    DateTime createdTimestamp = feedbackRow.GetValue<DateTime>("created_on_timestamp");
                    createdTimestamp = createdTimestamp.AddHours(timezoneOffset);
                    string createdTimestampString = createdTimestamp.ToString("dd/MM/yyyy");

                    RSTopicFeedback content = new RSTopicFeedback
                    {
                        Feedback = feedback,
                        CreatedTimestamp = createdTimestamp,
                        CreatedTimestampString = createdTimestampString
                    };

                    response.Feedbacks.Add(content);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticExportRSResultResponse ExportRSResult(string adminUserId, string companyId, string topicId, string categoryId)
        {
            AnalyticExportRSResultResponse response = new AnalyticExportRSResultResponse();
            response.Overview = new AnalyticSurvey.RSAnalyticOverview();
            response.Overview.Topic = new RSTopic();
            response.CardResults = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                PreparedStatement ps = null;

                // Get total targeted audience
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_privacy", new List<string>(), new List<string> { "rs_topic_id" }));
                Row topicPrivacyRow = mainSession.Execute(ps.Bind(topicId)).FirstOrDefault();
                bool isForEveryone = topicPrivacyRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = topicPrivacyRow.GetValue<bool>("is_for_department");
                bool isForUser = topicPrivacyRow.GetValue<bool>("is_for_user");

                int totalTargetedAudience = 0;

                User userManager = new User();
                if (isForEveryone)
                {
                    totalTargetedAudience = userManager.GetAllUserForAdmin(adminUserId, companyId, string.Empty, 0, User.AccountStatus.CODE_ACTIVE, new List<string>(), false, false, null, mainSession).Users.Count();
                }
                else
                {
                    List<string> targetedUserIds = new List<string>();

                    if (isForDepartment)
                    {
                        List<string> departmentIds = new List<string>();
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_department", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet departmentRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row departmentRow in departmentRowset)
                        {
                            string departmentId = departmentRow.GetValue<string>("department_id");
                            departmentIds.Add(departmentId);
                        }

                        targetedUserIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, null, companyId, mainSession);
                    }

                    if (isForUser)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_topic_targeted_user", new List<string>(), new List<string> { "rs_topic_id" }));
                        RowSet userRowset = mainSession.Execute(ps.Bind(topicId));

                        foreach (Row userRow in userRowset)
                        {
                            string userId = userRow.GetValue<string>("user_id");
                            if (!targetedUserIds.Contains(userId))
                            {
                                targetedUserIds.Add(userId);
                            }
                        }
                    }

                    totalTargetedAudience = targetedUserIds.Count();
                }

                // Get total pages and cards
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardOrderRowset = mainSession.Execute(ps.Bind(topicId));

                int numberOfPages = 1;
                int numberOfCards = 0;
                foreach (Row cardOrderRow in cardOrderRowset)
                {
                    string cardId = cardOrderRow.GetValue<string>("card_id");
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id", "id" }));
                    Row cardRow = mainSession.Execute(ps.Bind(topicId, cardId)).FirstOrDefault();
                    if (cardRow != null)
                    {
                        bool hasPageBreak = cardRow.GetValue<bool>("has_page_break");
                        if (hasPageBreak)
                        {
                            numberOfPages++;
                        }
                        numberOfCards++;
                    }
                }

                AnalyticSurvey analyticManager = new AnalyticSurvey();
                response.Overview = analyticManager.SelectRSResultOverview(topicId, numberOfPages, numberOfCards, totalTargetedAudience, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), analyticSession);

                // Get topic
                response.Overview.Topic = SelectBasicRSTopic(adminUserId, companyId, categoryId, null, topicId, false, null, mainSession, analyticSession);

                List<RSCard> cards = new RSCard().SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)RSCard.RSCardQueryType.FullDetail, null, mainSession).Cards;
                foreach (RSCard card in cards)
                {
                    analyticManager.SelectRSCardResultForExport(companyId, response.Overview.Topic.IsAnonymous, card, analyticSession, mainSession);
                }
                response.CardResults = cards;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }

    public class Progress
    {
        [DataMember]
        public int NumberOfCompletedUsers { get; set; }
        [DataMember]
        public int TotalNumberOfTargetedUsers { get; set; }
        [DataMember]
        public float Percentage { get; set; }
    }
}
