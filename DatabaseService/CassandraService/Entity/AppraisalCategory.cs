﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class AppraisalCategory
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum AppraisalCategoryQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember(EmitDefaultValue = false)]
        public string CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ReferencedCategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public double AverageRating { get; set; }

        [DataMember]
        public bool IsReferencedFromAdmin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<AppraisalCard> Cards { get; set; }


        public AppraisalCategoryCreateResponse CreateTemplateCategoryByUser(string templateAppraisalId, string title, string userId, ISession mainSession)
        {
            AppraisalCategoryCreateResponse response = new AppraisalCategoryCreateResponse();
            response.Success = false;
            try
            {
                AppraisalCategoryCreateStatementResponse statementResponse = CreateAppraisalCategory(templateAppraisalId, title, userId, false, mainSession);
                if (!statementResponse.Success)
                {
                    response.ErrorCode = statementResponse.ErrorCode;
                    response.ErrorMessage = statementResponse.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                foreach (BoundStatement bs in statementResponse.UpdateStatements)
                {
                    batch.Add(bs);
                }
                mainSession.Execute(batch);
                response.CategoryId = statementResponse.CategoryId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCategoryCreateResponse CreateTemplateCategoryByAdmin(string companyId, string adminUserId, string appraisalId, string title)
        {
            AppraisalCategoryCreateResponse response = new AppraisalCategoryCreateResponse();
            response.CategoryId = null;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, appraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                AppraisalCategory searchedCategory = SearchTemplateCategoryByAdmin(appraisalId, title, mainSession);
                if (searchedCategory != null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCategoryExisted);
                    response.ErrorMessage = ErrorMessage.AppraisalCategoryExisted;
                    return response;
                }

                AppraisalCategoryCreateStatementResponse statementResponse = CreateAppraisalCategory(appraisalId, title, adminUserId, true, mainSession);
                if (!statementResponse.Success)
                {
                    response.ErrorCode = statementResponse.ErrorCode;
                    response.ErrorMessage = statementResponse.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                foreach (BoundStatement bs in statementResponse.UpdateStatements)
                {
                    batch.Add(bs);
                }

                mainSession.Execute(batch);

                response.CategoryId = statementResponse.CategoryId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCategorySelectAllResponse SelectAdminDefinedCategoriesByAppraisal(string companyId, string appraisalId, ISession mainSession, int cardQueryType = (int)AppraisalCard.AppraisalCardQueryType.Basic)
        {
            AppraisalCategorySelectAllResponse response = new AppraisalCategorySelectAllResponse();
            response.Categories = new List<AppraisalCategory>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("admin_template_appraisal_360_category_sorted", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet adminCategoryRowSet = mainSession.Execute(ps.Bind(appraisalId));
                foreach (Row categoryRow in adminCategoryRowSet)
                {
                    AppraisalCategory category = SelectCategory(companyId, categoryRow, mainSession, true, cardQueryType);
                    if (category != null)
                    {
                        category.IsReferencedFromAdmin = true;
                        response.Categories.Add(category);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        public AppraisalCategorySelectAllResponse SelectUserDefinedCategoriesByAppraisal(string companyId, string creatorUserId, string templateAppraisalId, ISession mainSession = null, bool isFetchCards = false, int cardQueryType = (int)AppraisalCard.AppraisalCardQueryType.Basic)
        {
            AppraisalCategorySelectAllResponse response = new AppraisalCategorySelectAllResponse();
            response.Categories = new List<AppraisalCategory>();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(creatorUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                    if (templateRow == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                        return response;
                    }
                }               

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_template_appraisal_360_category_by_title", new List<string>(), new List<string> { "appraisal_id", "created_by_user_id" }));
                RowSet adminCategoryRowSet = mainSession.Execute(ps.Bind(templateAppraisalId, creatorUserId));
                foreach (Row categoryRow in adminCategoryRowSet)
                {
                    AppraisalCategory category = SelectCategory(companyId, categoryRow, mainSession, isFetchCards, cardQueryType);
                    response.Categories.Add(category);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        public AppraisalCategorySelectResponse SelectUserDefinedCategoryByAppraisal(string companyId, string creatorUserId, string templateAppraisalId, string categoryId)
        {
            AppraisalCategorySelectResponse response = new AppraisalCategorySelectResponse();
            response.Category = new AppraisalCategory();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(creatorUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row categoryRow = vh.ValidateAppraisalUserCategory(templateAppraisalId, categoryId, creatorUserId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                AppraisalCategory category = SelectCategory(companyId, categoryRow, mainSession, true, (int)AppraisalCard.AppraisalCardQueryType.FullDetail);
                response.Category = category;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        public AppraisalCategory SearchTemplateCategoryByUser(string appraisalId, string title, string creatorUserId, ISession mainSession)
        {
            AppraisalCategory category = null;
            try
            {
                title = title.Trim();

                string lowerCapCategoryTitle = title.ToLower();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_template_appraisal_360_category_by_title", new List<string>(), new List<string> { "appraisal_id", "created_by_user_id", "lower_cap_title" }));
                Row createdCategoryByUserRow = mainSession.Execute(ps.Bind(appraisalId, creatorUserId, lowerCapCategoryTitle)).FirstOrDefault();
                if (createdCategoryByUserRow != null)
                {
                    category = new AppraisalCategory();
                    category.CategoryId = createdCategoryByUserRow.GetValue<string>("category_id");
                    category.Title = title;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return category;
        }

        public AppraisalCategory SearchTemplateCategoryByAdmin(string appraisalId, string title, ISession mainSession)
        {
            AppraisalCategory category = null;
            try
            {
                title = title.Trim();
                string lowerCapCategoryTitle = title.ToLower();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("admin_template_appraisal_360_category_by_title", new List<string>(), new List<string> { "appraisal_id", "lower_cap_title" }));
                Row createdCategoryByAdminRow = mainSession.Execute(ps.Bind(appraisalId, lowerCapCategoryTitle)).FirstOrDefault();
                if (createdCategoryByAdminRow != null)
                {
                    category = new AppraisalCategory();
                    category.CategoryId = createdCategoryByAdminRow.GetValue<string>("category_id");
                    category.Title = title;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return category;
        }

        public void MapCardsToCategoryByAppraisal(Appraisal appraisal, bool isForResult = false)
        {
            List<AppraisalCategory> categories = new List<AppraisalCategory>();
            try
            {
                AppraisalCategory previousCategory = null;
                int numberOfCardPerCategory = 0;
                double totalRating = 0;
                for (int index = 0; index < appraisal.Cards.Count; index++)
                {
                    AppraisalCard card = appraisal.Cards[index];
                    AppraisalCategory category = categories.FirstOrDefault(c => c.CategoryId.Equals(card.CategoryId));
                    if (category == null)
                    {
                        category = new AppraisalCategory
                        {
                            CategoryId = card.CategoryId,
                            Title = card.CategoryTitle,
                            ReferencedCategoryId = card.ReferencedCategoryId,
                            Cards = new List<AppraisalCard>(),
                            IsReferencedFromAdmin = card.IsReferencedFromAdmin
                        };

                        categories.Add(category);

                        if (previousCategory != null)
                        {
                            previousCategory.AverageRating = numberOfCardPerCategory == 0 ? 0 : Math.Round(numberOfCardPerCategory == 0 ? 0 : (double)totalRating / numberOfCardPerCategory, 1);
                            numberOfCardPerCategory = 0;
                            totalRating = 0;
                            previousCategory = null;
                        }
                    }

                    if (previousCategory == null)
                    {
                        previousCategory = category;
                    }

                    if (isForResult)
                    {
                        numberOfCardPerCategory++;
                        totalRating += card.AverageRating;

                        List<AppraisalCard.AppraisalComment> commentList = new List<AppraisalCard.AppraisalComment>();
                        commentList.AddRange(card.Comments);

                        if (!card.IsCommentAllowed)
                        {
                            card.Comments = null;
                        }
                        else
                        {
                            if (appraisal.IsAnonymous)
                            {
                                foreach (AppraisalCard.AppraisalComment comment in commentList)
                                {
                                    if (comment.CommentedByUserId.Equals(appraisal.CreatedByUser.UserId))
                                    {
                                        card.Comments.Remove(comment);
                                        continue;
                                    }

                                    comment.CommentedByUserId = string.Empty;
                                    comment.CommentedByUser = new User
                                    {
                                        ProfileImageUrl = DefaultResource.AnonymousProfileUrl,
                                        FirstName = "Anonymous Review"
                                    };
                                }
                            }
                            else
                            {
                                foreach (AppraisalCard.AppraisalComment comment in commentList)
                                {
                                    if (comment.CommentedByUserId.Equals(appraisal.CreatedByUser.UserId))
                                    {
                                        card.Comments.Remove(comment);
                                        continue;
                                    }
                                    comment.CommentedByUser = appraisal.Participants.FirstOrDefault(p => p.UserId.Equals(comment.CommentedByUserId));
                                }
                            }
                        }

                        if (index == appraisal.Cards.Count - 1)
                        {
                            previousCategory.AverageRating = Math.Round(numberOfCardPerCategory == 0 ? 0 : (double)totalRating / numberOfCardPerCategory, 1);
                        }
                    }

                    category.Cards.Add(card);
                }

                appraisal.Categories = categories;
                appraisal.Cards = null;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AppraisalUpdateCategoryResponse UpdateTemplateCategoryTitleByUser(string companyId, string requesterUserId, string templateAppraisalId, string categoryId, string updatedTitle)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row categoryRow = vh.ValidateAppraisalUserCategory(templateAppraisalId, categoryId, requesterUserId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                AppraisalValidationResponse validateFieldResponse = ValidateFields(updatedTitle);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                string originalLowerCapTitle = categoryRow.GetValue<string>("lower_cap_title");
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalUpdateCategoryStatementResponse updateStatementResponse = UpdateCategory(requesterUserId, templateAppraisalId, categoryId, originalLowerCapTitle, updatedTitle, currentTime, mainSession);
                if (!updateStatementResponse.Success)
                {
                    response.ErrorCode = updateStatementResponse.ErrorCode;
                    response.ErrorMessage = updateStatementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updateStatementResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }
                foreach (BoundStatement bs in updateStatementResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalUpdateCategoryResponse UpdateTemplateCategoryTitleByAdmin(string companyId, string adminUserId, string templateAppraisalId, string categoryId, string updatedTitle)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row categoryRow = vh.ValidateTemplateAppraisalCategory(categoryId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                AppraisalValidationResponse validateFieldResponse = ValidateFields(updatedTitle);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                string originalLowerCapTitle = categoryRow.GetValue<string>("title").ToLower();
                int currentOrdering = categoryRow.GetValue<int>("ordering");
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalUpdateCategoryStatementResponse updateStatementResponse = UpdateCategory(adminUserId, templateAppraisalId, categoryId, originalLowerCapTitle, updatedTitle, currentTime, mainSession, true, currentOrdering);
                if (!updateStatementResponse.Success)
                {
                    response.ErrorCode = updateStatementResponse.ErrorCode;
                    response.ErrorMessage = updateStatementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updateStatementResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }
                foreach (BoundStatement bs in updateStatementResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalUpdateCategoryResponse RemoveCategoryByAdmin(string companyId, string adminUserId, string templateAppraisalId, string categoryId)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row categoryRow = vh.ValidateTemplateAppraisalCategory(categoryId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                // Check if there are cards in this category
                List<AppraisalCard> cards = new AppraisalCard().SelectCardsFromCategory(categoryId, companyId, mainSession).Cards;
                if (cards.Count > 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCategoryStillHaveCards);
                    response.ErrorMessage = ErrorMessage.AppraisalCategoryStillHaveCards;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalUpdateCategoryStatementResponse removeStatementResponse = RemoveCategory(categoryRow, mainSession);
                if (!removeStatementResponse.Success)
                {
                    response.ErrorCode = removeStatementResponse.ErrorCode;
                    response.ErrorMessage = removeStatementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in removeStatementResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }
                foreach (BoundStatement bs in removeStatementResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalUpdateCategoryStatementResponse RemoveCategory(Row categoryRow, ISession mainSession)
        {
            AppraisalUpdateCategoryStatementResponse response = new AppraisalUpdateCategoryStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                string categoryId = categoryRow.GetValue<string>("id");
                string title = categoryRow.GetValue<string>("title");
                bool isCreatedByAdmin = categoryRow.GetValue<bool>("is_created_by_admin");
                string appraisalId = categoryRow.GetValue<string>("appraisal_id");
                int ordering = categoryRow.GetValue<int>("ordering");
                string lowerCapTitle = title.ToLower();

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.DeleteStatement("template_appraisal_360_category", new List<string> { "appraisal_id", "id" }));
                response.DeleteStatements.Add(ps.Bind(appraisalId, categoryId));

                if (isCreatedByAdmin)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatementWithComparison("admin_template_appraisal_360_category_sorted", new List<string>(), new List<string> { "appraisal_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                    RowSet orderingRowSet = mainSession.Execute(ps.Bind(appraisalId, ordering));
                    foreach (Row orderingRow in orderingRowSet)
                    {
                        string updatedCategoryId = orderingRow.GetValue<string>("category_id");
                        int updatedRowOrder = orderingRow.GetValue<int>("ordering");
                        string updatedTitle = orderingRow.GetValue<string>("title");

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("admin_template_appraisal_360_category_sorted", new List<string> { "appraisal_id", "ordering", "category_id" }));
                        response.DeleteStatements.Add(ps.Bind(appraisalId, updatedRowOrder, updatedCategoryId));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("admin_template_appraisal_360_category_sorted", new List<string> { "category_id", "title", "appraisal_id", "ordering" }));
                        response.UpdateStatements.Add(ps.Bind(updatedCategoryId, updatedTitle, appraisalId, updatedRowOrder - 1));

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360_category", new List<string> { "appraisal_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                        response.UpdateStatements.Add(ps.Bind(updatedRowOrder - 1, appraisalId, updatedCategoryId));
                    }

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("admin_template_appraisal_360_category_sorted", new List<string> { "appraisal_id", "ordering", "category_id" }));
                    response.DeleteStatements.Add(ps.Bind(appraisalId, ordering, categoryId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("admin_template_appraisal_360_category_by_title", new List<string> { "appraisal_id", "lower_cap_title", "category_id" }));
                    response.DeleteStatements.Add(ps.Bind(appraisalId, lowerCapTitle, categoryId));
                }
                else
                {
                    string createdByUserId = categoryRow.GetValue<string>("created_by_user_id");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_template_appraisal_360_category_by_title", new List<string> { "appraisal_id", "created_by_user_id", "lower_cap_title", "category_id" }));
                    response.DeleteStatements.Add(ps.Bind(appraisalId, createdByUserId, lowerCapTitle, categoryId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_template_appraisal_360_category", new List<string> { "appraisal_id", "created_by_user_id", "category_id" }));
                    response.DeleteStatements.Add(ps.Bind(appraisalId, createdByUserId, categoryId));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalUpdateCategoryStatementResponse UpdateCategory(string updatedByUserId, string templateAppraisalId, string categoryId, string originalLowerCapTitle, string newTitle, DateTime currentTime, ISession mainSession, bool isCreatedByAdmin = false, int currentOrdering = 0)
        {
            AppraisalUpdateCategoryStatementResponse response = new AppraisalUpdateCategoryStatementResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                if (!isCreatedByAdmin)
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_template_appraisal_360_category_by_title", new List<string> { "appraisal_id", "created_by_user_id", "lower_cap_title", "category_id" }));
                    response.DeleteStatements.Add(ps.Bind(templateAppraisalId, updatedByUserId, originalLowerCapTitle, categoryId));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_template_appraisal_360_category_by_title", new List<string> { "category_id", "appraisal_id", "title", "lower_cap_title", "created_by_user_id" }));
                    response.UpdateStatements.Add(ps.Bind(categoryId, templateAppraisalId, newTitle, newTitle.ToLower(), updatedByUserId));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_template_appraisal_360_category", new List<string> { "appraisal_id", "created_by_user_id", "category_id" }, new List<string> { "title", "lower_cap_title" }, new List<string>()));
                    response.UpdateStatements.Add(ps.Bind(newTitle, newTitle.ToLower(), templateAppraisalId, updatedByUserId, categoryId));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("admin_template_appraisal_360_category_by_title", new List<string> { "appraisal_id", "lower_cap_title", "category_id" }));
                    response.DeleteStatements.Add(ps.Bind(templateAppraisalId, originalLowerCapTitle, categoryId));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("admin_template_appraisal_360_category_by_title", new List<string> { "category_id", "title", "lower_cap_title", "appraisal_id", "ordering" }));
                    response.UpdateStatements.Add(ps.Bind(categoryId, newTitle, newTitle.ToLower(), templateAppraisalId, currentOrdering));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("admin_template_appraisal_360_category_sorted", new List<string> { "appraisal_id", "ordering", "category_id" }, new List<string> { "title" }, new List<string>()));
                    response.UpdateStatements.Add(ps.Bind(newTitle, templateAppraisalId, currentOrdering, categoryId));
                }

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360_category", new List<string> { "appraisal_id", "id" }, new List<string> { "title", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(newTitle, updatedByUserId, currentTime, templateAppraisalId, categoryId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalCategory SelectCategory(string companyId, Row categoryRow, ISession mainSession, bool isFetchCards = false, int cardQueryType = (int)AppraisalCard.AppraisalCardQueryType.Basic)
        {
            AppraisalCategory category = null;
            try
            {
                category = new AppraisalCategory
                {
                    CategoryId = categoryRow.GetValue<string>("category_id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                if (isFetchCards)
                {
                    category.Cards = new AppraisalCard().SelectCardsFromCategory(category.CategoryId, companyId, mainSession, cardQueryType).Cards;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return category;
        }

        private AppraisalCategoryCreateStatementResponse CreateAppraisalCategory(string appraisalId, string title, string userId, bool isCreatedByAdmin, ISession mainSession)
        {
            AppraisalCategoryCreateStatementResponse response = new AppraisalCategoryCreateStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.CategoryId = null;
            response.Success = false;
            try
            {
                title = title.Trim();
                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Category title is empty");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidTitle);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidTitle;
                    return response;
                }

                // Make first char of the title upper cap
                if (title.Length == 1)
                {
                    title = title.ToUpper();
                }
                else
                {
                    title = title.Remove(1).ToUpper() + title.Substring(1);
                }

                string categoryId = UUIDGenerator.GenerateUniqueIDForAppraisalCategory();

                DateTime currentDate = DateTime.UtcNow;
                PreparedStatement ps = null;

                int ordering = 0;
                if (!isCreatedByAdmin)
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_template_appraisal_360_category_by_title",
                        new List<string> { "category_id", "appraisal_id", "title", "lower_cap_title", "created_by_user_id" }));
                    response.UpdateStatements.Add(ps.Bind(categoryId, appraisalId, title, title.ToLower(), userId));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_template_appraisal_360_category",
                        new List<string> { "category_id", "appraisal_id", "title", "lower_cap_title", "created_by_user_id" }));
                    response.UpdateStatements.Add(ps.Bind(categoryId, appraisalId, title, title.ToLower(), userId));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.CountStatement("admin_template_appraisal_360_category_sorted",
                        new List<string> { "appraisal_id" }));
                    ordering = (int)mainSession.Execute(ps.Bind(appraisalId)).FirstOrDefault().GetValue<long>("count");

                    ordering = ordering + 1;

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("admin_template_appraisal_360_category_sorted",
                        new List<string> { "category_id", "title", "appraisal_id", "ordering" }));
                    response.UpdateStatements.Add(ps.Bind(categoryId, title, appraisalId, ordering));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("admin_template_appraisal_360_category_by_title",
                        new List<string> { "category_id", "title", "lower_cap_title", "appraisal_id", "ordering" }));
                    response.UpdateStatements.Add(ps.Bind(categoryId, title, title.ToLower(), appraisalId, ordering));
                }

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360_category",
                    new List<string> { "id", "appraisal_id", "title", "ordering", "is_created_by_admin", "is_valid", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                response.UpdateStatements.Add(ps.Bind(categoryId, appraisalId, title, ordering, isCreatedByAdmin, true, userId, currentDate, userId, currentDate));

                response.CategoryId = categoryId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalValidationResponse ValidateFields(string categoryTitle)
        {
            AppraisalValidationResponse response = new AppraisalValidationResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(categoryTitle))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCategoryMissingTitle);
                    response.ErrorMessage = ErrorMessage.AppraisalCategoryMissingTitle;
                    return response;
                }
                else
                {
                    categoryTitle = categoryTitle.Trim();
                    if (string.IsNullOrEmpty(categoryTitle))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCategoryMissingTitle);
                        response.ErrorMessage = ErrorMessage.AppraisalCategoryMissingTitle;
                        return response;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}
