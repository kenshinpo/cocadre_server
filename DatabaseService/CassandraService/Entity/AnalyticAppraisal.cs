﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CassandraService.Entity
{
    public class AnalyticAppraisal
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public void AnswerCard(string appraisalId, string answeredByUserId, int cardType, string cardId, int selectedRange, int selectedOption, DateTime currentTime, ISession analyticSession, bool isCompleted = false)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                string tableName = string.Empty;
                string tableSortedName = string.Empty;
                string variableName = string.Empty;
                int variableValue = 0;

                if (cardType == (int)AppraisalCard.AppraisalCardTypeEnum.NumberRange)
                {
                    tableName = "appraisal_range_answered_by_user";
                    tableSortedName = "appraisal_range_answered_by_pulse_sorted";
                    variableName = "range_value";
                    variableValue = selectedRange;
                }
                else if (cardType == (int)AppraisalCard.AppraisalCardTypeEnum.SelectOne)
                {
                    tableName = "appraisal_option_answered_by_user";
                    tableSortedName = "appraisal_user_answered_by_option_sorted";
                    variableName = "option_number";
                    variableValue = selectedOption;
                }

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement(tableName, new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                Row row = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();
                if (row != null)
                {
                    int value = row.GetValue<int>(variableName);
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement(tableSortedName, new List<string> { "answered_by_user_id", "card_id", variableName }));
                    deleteBatch.Add(ps.Bind(answeredByUserId, cardId, value));
                }

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement(tableName, new List<string> { "answered_by_user_id", "card_id", variableName, "answered_on_timestamp" }));
                updateBatch.Add(ps.Bind(answeredByUserId, cardId, variableValue, currentTime));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement(tableSortedName, new List<string> { "answered_by_user_id", "card_id", variableName, "answered_on_timestamp" }));
                updateBatch.Add(ps.Bind(answeredByUserId, cardId, variableValue, currentTime));

                updateBatch.Add(UpdateProgress(appraisalId, answeredByUserId, currentTime, analyticSession, isCompleted));

                analyticSession.Execute(deleteBatch);
                analyticSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void GetSelectedAnswerByCard(AppraisalCard card, string answeredByUserId, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;
                if (card.Type == (int)AppraisalCard.AppraisalCardTypeEnum.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_range_answered_by_user", new List<string>(), new List<string> { "card_id", "answered_by_user_id" }));
                    Row rangeAnswerRow = analyticSession.Execute(ps.Bind(card.CardId, answeredByUserId)).FirstOrDefault();
                    if (rangeAnswerRow != null)
                    {
                        int rangeValue = rangeAnswerRow.GetValue<int>("range_value");
                        AppraisalCardRange range = card.NumberRanges.FirstOrDefault(r => r.RangeNumber == rangeValue);
                        if (range != null)
                        {
                            range.IsSelected = true;
                        }
                    }
                }
                else if (card.Type == (int)AppraisalCard.AppraisalCardTypeEnum.SelectOne)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_option_answered_by_user", new List<string>(), new List<string> { "card_id", "answered_by_user_id" }));
                    Row optionAnswerRow = analyticSession.Execute(ps.Bind(card.CardId, answeredByUserId)).FirstOrDefault();
                    if (optionAnswerRow != null)
                    {
                        int optionNumber = optionAnswerRow.GetValue<int>("option_number");
                        AppraisalCardOption option = card.Options.FirstOrDefault(o => o.OptionNumber == optionNumber);
                        if (option != null)
                        {
                            option.IsSelected = true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void GetResultByCard(AppraisalCard card, string creatorUserId, bool isSelfAssessmentNeeded, double comparisonDifference, List<string> completedParticipantsIds, ISession analyticSession, bool isBreakdownNeeded = false)
        {
            try
            {
                PreparedStatement ps = null;
                int numberOfParticipants = 0;
                int totalRating = 0;
                int selfRating = 0;

                card.ResultBreakdown = new List<AppraisalCard.ParticipantAnswerBreakdown>();

                if (card.Type == (int)AppraisalCard.AppraisalCardTypeEnum.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_range_answered_by_pulse_sorted", new List<string>(), new List<string> { "card_id" }));
                    RowSet rangeAnswerRowSet = analyticSession.Execute(ps.Bind(card.CardId));
                    foreach (Row rangeAnswerRow in rangeAnswerRowSet)
                    {
                        string answeredByUserId = rangeAnswerRow.GetValue<string>("answered_by_user_id");

                        int rangeValue = rangeAnswerRow.GetValue<int>("range_value");
                        AppraisalCardRange range = card.NumberRanges.FirstOrDefault(r => r.RangeNumber == rangeValue);

                        if (answeredByUserId.Equals(creatorUserId))
                        {
                            if (isSelfAssessmentNeeded)
                            {
                                selfRating = rangeValue;

                                if (isBreakdownNeeded)
                                {
                                    card.ResultBreakdown.Add(GenerateBreakdown(card, answeredByUserId, rangeValue));
                                }
                            }
                        }
                        else
                        {
                            if (range != null)
                            {
                                if (completedParticipantsIds.Contains(answeredByUserId))
                                {
                                    range.NumberOfSelection++;
                                    totalRating += rangeValue;
                                    numberOfParticipants++;

                                    if (isBreakdownNeeded)
                                    {
                                        card.ResultBreakdown.Add(GenerateBreakdown(card, answeredByUserId, rangeValue));
                                    }
                                }
                            }
                        }
                    }
                    card.NumberRanges.RemoveAll(r => r.NumberOfSelection == 0);
                    card.NumberRanges = card.NumberRanges.OrderByDescending(r => r.RangeNumber).ToList();

                    card.AverageRating = Math.Round(numberOfParticipants == 0 ? 0 : (double)totalRating / numberOfParticipants, 1);
                }
                else if (card.Type == (int)AppraisalCard.AppraisalCardTypeEnum.SelectOne)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_user_answered_by_option_sorted", new List<string>(), new List<string> { "card_id" }));
                    RowSet optionAnswerRowSet = analyticSession.Execute(ps.Bind(card.CardId));
                    foreach (Row optionAnswerRow in optionAnswerRowSet)
                    {
                        string answeredByUserId = optionAnswerRow.GetValue<string>("answered_by_user_id");

                        int optionValue = optionAnswerRow.GetValue<int>("option_number");
                        AppraisalCardOption option = card.Options.FirstOrDefault(o => o.OptionNumber == optionValue);

                        if (answeredByUserId.Equals(creatorUserId))
                        {
                            if (isSelfAssessmentNeeded)
                            {
                                selfRating = optionValue;

                                if (isBreakdownNeeded)
                                {
                                    card.ResultBreakdown.Add(GenerateBreakdown(card, answeredByUserId, optionValue));
                                }
                            }
                        }
                        else
                        {
                            if (option != null)
                            {
                                if (completedParticipantsIds.Contains(answeredByUserId))
                                {
                                    option.NumberOfSelection++;
                                    totalRating += optionValue;
                                    numberOfParticipants++;

                                    if (isBreakdownNeeded)
                                    {
                                        card.ResultBreakdown.Add(GenerateBreakdown(card, answeredByUserId, optionValue));
                                    }
                                }
                            }
                        }
                    }

                    card.AverageRating = Math.Round(numberOfParticipants == 0 ? 0 : (double)totalRating / numberOfParticipants, 1);

                    card.OptionResult = card.Options.FirstOrDefault(o => o.OptionNumber == Math.Abs(Math.Round(-card.AverageRating)));

                    card.Options.RemoveAll(o => o.NumberOfSelection == 0);
                    card.Options = card.Options.OrderByDescending(o => o.OptionNumber).ToList();
                }

                card.SelfRating = selfRating;

                if (isSelfAssessmentNeeded)
                {
                    double difference = card.SelfRating - card.AverageRating;
                    if (Math.Abs(difference) > comparisonDifference)
                    {
                        bool isPositive = difference > 0 ? false : true;
                        card.ResultFeedback = new AppraisalCard.ComparisonFeedback
                        {
                            IsPositive = isPositive,
                            Feedback = isPositive ? DefaultResource.AppraisalResultPositive : DefaultResource.AppraisalResultNegative
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        private AppraisalCard.ParticipantAnswerBreakdown GenerateBreakdown(AppraisalCard card, string answeredByUserId, int score)
        {
            return new AppraisalCard.ParticipantAnswerBreakdown
            {
                CardId = card.CardId,
                ReferencedCardId = card.ReferencedCardId,
                ParticipantId = answeredByUserId,
                SelectedScore = score,
                IsReferencedFromAdmin = card.IsReferencedFromAdmin
            };
        }

        public bool CheckCompletion(string appraisalId, string answeredByUserId, ISession analyticSession)
        {
            bool isCompleted = false;
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_completion_progress", new List<string>(), new List<string> { "appraisal_id", "answered_by_user_id" }));
                Row progressRow = analyticSession.Execute(ps.Bind(appraisalId, answeredByUserId)).FirstOrDefault();
                if (progressRow != null)
                {
                    DateTime? completedTimestamp = progressRow.GetValue<DateTime?>("completed_on_timestamp");
                    if (completedTimestamp != null)
                    {
                        isCompleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return isCompleted;
        }

        public int SelectNumberOfCompletedParticipants(string appraisalId, string createdByUserId, ISession analyticSession)
        {
            int numberOfCompletedParticipants = 0;
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_completion_progress", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet completionRowSet = analyticSession.Execute(ps.Bind(appraisalId));
                foreach (Row completionRow in completionRowSet)
                {
                    if (!completionRow.IsNull("completed_on_timestamp") && !completionRow.GetValue<string>("answered_by_user_id").Equals(createdByUserId))
                    {
                        numberOfCompletedParticipants++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return numberOfCompletedParticipants;
        }

        public List<string> SelectCompletedParticipants(string appraisalId, string createdByUserId, ISession analyticSession)
        {
            List<string> participantIds = new List<string>();
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_completion_progress", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet completionRowSet = analyticSession.Execute(ps.Bind(appraisalId));
                foreach (Row completionRow in completionRowSet)
                {
                    if (!completionRow.IsNull("completed_on_timestamp") && !completionRow.GetValue<string>("answered_by_user_id").Equals(createdByUserId))
                    {
                        participantIds.Add(completionRow.GetValue<string>("answered_by_user_id"));
                    }
                }

                participantIds = participantIds.Distinct().ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return participantIds;
        }

        public Dictionary<string, object> SelectTimeTakenByParticipant(string participantId, string appraisalId, ISession analyticSession)
        {
            Dictionary<string, object> timeDict = new Dictionary<string, object>();
            timeDict["TimeTakenSec"] = 0.0;
            timeDict["TimeTakenString"] = "Incomplete";
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_completion_progress", new List<string>(), new List<string> { "appraisal_id", "answered_by_user_id" }));
                Row progressRow = analyticSession.Execute(ps.Bind(appraisalId, participantId)).FirstOrDefault();
                if(progressRow != null)
                {
                    if (progressRow.IsNull("completed_on_timestamp"))
                    {
                        return timeDict;
                    }

                    DateTime completedTimestamp = progressRow.GetValue<DateTime>("completed_on_timestamp");
                    DateTime startedTimestamp = progressRow.GetValue<DateTime>("started_on_timestamp");

                    timeDict = GenerateTimeTakenString(startedTimestamp, completedTimestamp);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return timeDict;
        }

        private Dictionary<string, object> GenerateTimeTakenString(DateTime startTimestamp, DateTime endTimestamp)
        {
            Dictionary<string, object> timeDict = new Dictionary<string, object>();
            try
            {
                string timeTaken = string.Empty;

                double differenceInSeconds = (endTimestamp - startTimestamp).TotalSeconds;

                //TimeSpan timeSpan = (endTimestamp - startTimestamp);
                //if (differenceInSeconds < 0)
                //{
                //    timeSpan = (endTimestamp - startTimestamp).Negate();
                //    differenceInSeconds = Math.Abs(differenceInSeconds);
                //}

                //int days = timeSpan.Days;
                //int hours = timeSpan.Hours;
                //int minutes = timeSpan.Minutes;
                //int seconds = timeSpan.Seconds;

                //if (days > 0)
                //{
                //    timeTaken += $"{days} days ";
                //}
                //if (hours > 0)
                //{
                //    timeTaken += $"{hours} hours ";
                //}
                //if (minutes > 0)
                //{
                //    timeTaken += $"{minutes} mins ";
                //}
                //if (seconds >= 0)
                //{
                //    timeTaken += $"{seconds} secs ";
                //}

                //timeTaken = timeTaken.Trim();

                timeDict["TimeTakenSec"] = Math.Abs(differenceInSeconds);
                timeDict["TimeTakenString"] = $"{Math.Abs(differenceInSeconds)}";
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return timeDict;
        }

        private BoundStatement UpdateProgress(string appraisalId, string answeredByUserId, DateTime currentTime, ISession analyticSession, bool isCompleted = false)
        {
            BoundStatement bs = null;
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("appraisal_completion_progress", new List<string>(), new List<string> { "appraisal_id", "answered_by_user_id" }));
                Row progressRow = analyticSession.Execute(ps.Bind(appraisalId, answeredByUserId)).FirstOrDefault();
                if (progressRow == null)
                {
                    if (isCompleted)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("appraisal_completion_progress", new List<string> { "appraisal_id", "answered_by_user_id", "started_on_timestamp", "last_updated_on_timestamp", "completed_on_timestamp" }));
                        bs = ps.Bind(appraisalId, answeredByUserId, currentTime, currentTime, currentTime);
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("appraisal_completion_progress", new List<string> { "appraisal_id", "answered_by_user_id", "started_on_timestamp", "last_updated_on_timestamp" }));
                        bs = ps.Bind(appraisalId, answeredByUserId, currentTime, currentTime);
                    }
                }
                else
                {
                    if (isCompleted)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("appraisal_completion_progress", new List<string> { "appraisal_id", "answered_by_user_id" }, new List<string> { "last_updated_on_timestamp", "completed_on_timestamp" }, new List<string>()));
                        bs = ps.Bind(currentTime, currentTime, appraisalId, answeredByUserId);
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("appraisal_completion_progress", new List<string> { "appraisal_id", "answered_by_user_id" }, new List<string> { "last_updated_on_timestamp" }, new List<string>()));
                        bs = ps.Bind(currentTime, appraisalId, answeredByUserId);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return bs;
        }
    }
}
