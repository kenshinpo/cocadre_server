﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Web.Configuration;
using System.Web.Hosting;

namespace CassandraService.Entity
{
    public class AnalyticQuiz
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum ChallengeStatType
        {
            Draw = 1,
            Lost = 2,
            NetworkError = 3,
            Surrender = 4,
            Win = 5,
        }

        public enum ResultOutcome
        {
            Lost = -1,
            Win = 1,
            Draw = 2,
            Incompleted = 3
        }

        public enum MatchUpImprovementTimeFrame
        {
            OneDay = 1,
            SevenDays = 2,
            ThirtyDays = 3
        }

        public enum DayFrame
        {
            Mon = 1,
            Tue = 2,
            Wed = 3,
            Thurs = 4,
            Fri = 5,
            Sat = 6,
            Sun = 7
        }

        public enum TopicDifficultyMetric
        {
            Hard = 1,
            Easy = 2
        }


        [DataContract]
        [Serializable]
        public class Exp
        {
            //[DataMember]
            //public int PreviousLevel { get; set; }
            //[DataMember]
            //public int PreviousExp { get; set; }
            [DataMember]
            public int CurrentLevel { get; set; }
            [DataMember]
            public int CurrentExp { get; set; }
            [DataMember]
            public int NextLevel { get; set; }
            [DataMember]
            public int NextExpToReach { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string Rank { get; set; }
        }

        [DataContract]
        [Serializable]
        public class GameHistory
        {
            [DataMember]
            public string ChallengeId { get; set; }
            [DataMember]
            public User Opponent { get; set; }
            [DataMember]
            public Topic Topic { get; set; }
            [DataMember]
            public int Outcome { get; set; }
            [DataMember]
            public DateTime CompletedOnTimestamp { get; set; }
        }

        [DataContract]
        [Serializable]
        public class UserStats
        {
            [DataMember]
            public int WinningPercentage { get; set; }
            [DataMember]
            public int DrawPercentage { get; set; }
            [DataMember]
            public int LosingPercentage { get; set; }
            [DataMember]
            public List<TopicStats> Topics { get; set; }
        }

        [DataContract]
        [Serializable]
        public class TopicStats
        {
            [DataMember]
            public Topic Topic { get; set; }
            [DataMember]
            public int CompletionPercentage { get; set; }
        }

        [DataContract]
        [Serializable]
        public class EventLeaderboard
        {
            [DataMember(EmitDefaultValue = false)]
            public User User { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public Department Department { get; set; }

            [DataMember]
            public int Score { get; set; }

            [DataMember]
            public int Rank { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpOverView
        {
            [DataMember]
            public int TotalTopics { get; set; }

            [DataMember]
            public int TotalQuestions { get; set; }

            [DataMember]
            public int TotalMatchUps { get; set; }

            [DataMember]
            public int TotalUniqueUsers { get; set; }

            [DataMember]
            public double AverageTopicAttempts { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpHeatmap
        {
            [DataMember]
            public List<MatchUpHeatmapPerDay> Days { get; set; }

            [DataMember]
            public int MaxCount { get; set; }
            [DataMember]
            public int MinCount { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpHeatmapPerDay
        {
            [DataMember]
            public string DayString { get; set; }

            [DataMember]
            public int Day { get; set; }

            [DataMember]
            public List<MatchUpHeatmapPerHour> Hours { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpHeatmapPerHour
        {
            [DataMember]
            public int Hour { get; set; }

            [DataMember]
            public string HourString { get; set; }

            [DataMember]
            public int AttemptCount { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpDau
        {
            [DataMember]
            public List<MatchUpDauPerDay> Reports { get; set; }

            [DataMember]
            public int MaxTimeTaken { get; set; }

            [DataMember]
            public int MaxTimeTakenMin { get; set; }

            [DataMember]
            public int MaxUserCount { get; set; }

            [DataMember]
            public ComparableMatchUpDau Today { get; set; }

            [DataMember]
            public ComparableMatchUpDau LastSevenDays { get; set; }

            [DataMember]
            public ComparableMatchUpDau LastThirtyDays { get; set; }

            [DataMember]
            public int AverageTimeSpent { get; set; }
            [DataMember]
            public string AverageTimeSpentString { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpDauPerDay
        {
            [DataMember]
            public string DateString { get; set; }

            [DataMember]
            public DateTime Date { get; set; }

            [DataMember]
            public int UserCount { get; set; }

            [DataMember]
            public int TimeTaken { get; set; }
            [DataMember]
            public int TimeTakenMin { get; set; }

            [DataMember]
            public int AverageTimeTaken { get; set; }
            [DataMember]
            public int AverageTimeTakenMin { get; set; }
        }

        [DataContract]
        [Serializable]
        public class ComparableMatchUpDau
        {
            [DataMember]
            public int TotalUniqueUsers { get; set; }

            [DataMember]
            public int DifferenceInNumber { get; set; }

            [DataMember]
            public double DifferenceInPercentage { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpSortedTopic
        {
            [DataMember]
            public Topic Topic { get; set; }

            [DataMember]
            public int Correct { get; set; }

            [DataMember]
            public int Incorrect { get; set; }


            [DataMember]
            public int TotalUniqueUsers { get; set; }

            public List<string> TotalUniqueUserIds { get; set; }

            [DataMember]
            public int TotalAttempts { get; set; }
        }

        [DataContract]
        [Serializable]
        public class QuestionAttempt
        {
            [DataMember]
            public int Correct { get; set; }

            [DataMember]
            public int Incorrect { get; set; }
        }

        [DataContract]
        [Serializable]
        public class TopicSummary
        {
            [DataMember]
            public Topic Topic { get; set; }

            [DataMember]
            public int TotalTargetedAudience { get; set; }

            [DataMember]
            public int TotalUserEngaged { get; set; }

            [DataMember]
            public int TotalMatchUp { get; set; }
            [DataMember]
            public double AverageMatchUpEngaged { get; set; }
        }

        [DataContract]
        [Serializable]
        public class TopicAnalytics
        {
            [DataMember]
            public List<TopicAttempt> Attempts { get; set; }

            [DataMember]
            public int MaxCorrect { get; set; }

            [DataMember]
            public int MaxIncorrect { get; set; }

            [DataMember]
            public int FirstAttemptCorrect { get; set; }

            [DataMember]
            public int TotalFailedAttempt { get; set; }

            [DataMember]
            public int SecondAttemptCorrect { get; set; }

            [DataMember]
            public int ThirdAboveAttemptCorrect { get; set; }

            [DataMember]
            public int SecondAttemptIncorrect { get; set; }

            [DataMember]
            public int ThirdAboveAttemptIncorrect { get; set; }
        }

        [DataContract]
        [Serializable]
        public class TopicAttempt
        {
            [DataMember]
            public int Correct { get; set; }

            [DataMember]
            public int Incorrect { get; set; }

            [DataMember]
            public string Attempt { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpSortedQuestion
        {
            [DataMember]
            public ChallengeQuestion Question { get; set; }

            [DataMember]
            public int Correct { get; set; }

            [DataMember]
            public int Incorrect { get; set; }
        }

        [DataContract]
        [Serializable]
        public class AnalyticOption
        {
            [DataMember]
            public string OptionId { get; set; }

            [DataMember]
            public string Content { get; set; }

            [DataMember]
            public string ContentImageUrl { get; set; }

            [DataMember]
            public int SelectedCount { get; set; }
        }

        [DataContract]
        [Serializable]
        public class AnalyticOptionDetail
        {
            [DataMember]
            public List<AnalyticOption> Options { get; set; }
            [DataMember]
            public int MaxCount { get; set; }
        }

        [DataContract]
        [Serializable]
        public class AnalyticEventUserScore
        {
            [DataMember]
            public User User { get; set; }
            [DataMember]
            public int Score { get; set; }
            [DataMember]
            public DateTime Timestamp { get; set; }
            [DataMember]
            public string TimestampString { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpPersonnelBreakdownReport
        {
            public Topic Topic { get; set; }
            public User User { get; set; }
            public int PlayedTimes { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MatchUpPersonnelGroupByReport
        {
            public User User { get; set; }
            public int PlayedTimes { get; set; }
        }

        [DataContract]
        [Serializable]
        public class TopPersonnelScore
        {
            public User User { get; set; }
            public int NumberOfQuestionAnsweredCorrectly { get; set; }
        }

        public void WriteExpAToTable()
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();

                string expAFileName = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "exp-a.csv");
                TextFieldParser parser = new TextFieldParser(expAFileName);
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();

                BatchStatement batchStatement = new BatchStatement();

                List<Dictionary<string, int>> expList = new List<Dictionary<string, int>>();
                while (!parser.EndOfData)
                {
                    Dictionary<string, int> expDict = new Dictionary<string, int>();
                    string[] fields = parser.ReadFields();

                    int level = Int32.Parse(fields[0].Trim());
                    int exp = Int32.Parse(fields[1].Trim());

                    expDict.Add("level", level);
                    expDict.Add("exp", exp);
                    expList.Add(expDict);
                }

                for (int index = 0; index < expList.Count; index++)
                {
                    PreparedStatement psExp = session.Prepare(CQLGenerator.InsertStatement("experience_scheme_a",
                        new List<string> { "level", "from_exp", "to_exp" }));

                    if (index + 1 < expList.Count)
                    {
                        batchStatement = batchStatement.Add(psExp.Bind(expList[index]["level"], expList[index]["exp"], expList[index + 1]["exp"] - 1));
                    }
                    else
                    {
                        batchStatement = batchStatement.Add(psExp.Bind(expList[index]["level"], expList[index]["exp"], expList[index]["exp"]));
                    }
                }

                session.Execute(batchStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void UpdateExpOfUser(string userId, string topicId, string questionId, int score, DateTimeOffset timeAllocated, ISession analyticSession)
        {
            try
            {
                BatchStatement bs = new BatchStatement();
                string allocationId = UUIDGenerator.GenerateUniqueIDForExpAllocation();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.UpdateCounterStatement("experience_by_user",
                    new List<string> { "user_id" }, new List<string> { "exp" }, new List<int> { score }));
                analyticSession.Execute(ps.Bind(userId));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(ps.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user_timestamp",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(ps.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(ps.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic_timestamp",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(ps.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(ps.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question_timestamp",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(ps.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                analyticSession.Execute(bs);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

        }

        public ChallengeSelectStatsResponse SelectStatsForChallenge(string topicId, string requesterUserId, string opponentUserId, string companyId)
        {
            ChallengeSelectStatsResponse response = new ChallengeSelectStatsResponse();
            response.Achievements = new List<Gamification.Achievement>();
            response.Notification = new Notification();
            response.Exp = new Exp();
            response.TopPlayers = new List<User>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                Exp currentUserExp = SelectLevelOfUser(requesterUserId, analyticSession);

                Exp opponentExp = SelectLevelOfUser(opponentUserId, analyticSession);
                response.OpponentLevel = opponentExp.CurrentLevel;

                int limit = Int16.Parse(WebConfigurationManager.AppSettings["leaderboard_by_topic"]);

                PreparedStatement psLeaderboard = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("user_leaderboard_by_topic_sorted", new List<string> { "user_id" }, new List<string> { "topic_id", "is_visible" }, limit));
                BoundStatement bsLeaderboard = psLeaderboard.Bind(topicId, true);
                RowSet leaderboardRowset = analyticSession.Execute(bsLeaderboard);

                foreach (Row leaderboardRow in leaderboardRowset)
                {
                    string userId = leaderboardRow.GetValue<string>("user_id");
                    Exp leaderboardUserExp = SelectLevelOfUser(userId, analyticSession);

                    int level = leaderboardUserExp.CurrentLevel;

                    User player = new User().SelectUserBasic(userId, companyId, false, mainSession).User;
                    player.Level = level;
                    response.TopPlayers.Add(player);
                }

                GamificationSelectUnlockedAchievementResponse unlockedResponse = new Gamification().SelectNewUnlockedAchievementByUser(requesterUserId, companyId, mainSession, analyticSession);
                response.Achievements = unlockedResponse.Achievements;
                response.Notification = unlockedResponse.Notification;
                response.Exp = currentUserExp;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectStatsResponse SelectStatsForUser(string requesterUserId, string companyId)
        {
            UserSelectStatsResponse response = new UserSelectStatsResponse();
            response.UserStats = new UserStats();
            response.UserStats.Topics = new List<TopicStats>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("challenge_total_stats_counter_by_user",
                    new List<string>(), new List<string> { "company_id", "user_id" }));
                Row totalStatsRow = session.Execute(preparedStatement.Bind(companyId, requesterUserId)).FirstOrDefault();

                if (totalStatsRow != null)
                {
                    long draws = totalStatsRow["total_draws"] != null ? totalStatsRow.GetValue<long>("total_draws") : 0;
                    long losses = totalStatsRow["total_losses"] != null ? totalStatsRow.GetValue<long>("total_losses") : 0;
                    long wins = totalStatsRow["total_win"] != null ? totalStatsRow.GetValue<long>("total_win") : 0;
                    long totalGamesPlayed = draws + losses + wins;

                    decimal winPercentage = wins == 0 ? 0 : ((decimal)wins / totalGamesPlayed);
                    decimal losePercentage = losses == 0 ? 0 : ((decimal)losses / totalGamesPlayed);

                    response.UserStats.WinningPercentage = (int)(winPercentage * 100);
                    response.UserStats.LosingPercentage = (int)(losePercentage * 100);

                    response.UserStats.DrawPercentage = 0;
                    if(response.UserStats.WinningPercentage != 0 || response.UserStats.LosingPercentage != 0)
                    {
                        response.UserStats.DrawPercentage = 100 - response.UserStats.WinningPercentage - response.UserStats.LosingPercentage;
                    }
                }

                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                    new List<string>(), new List<string> { "user_id", "is_visible" }));
                RowSet topicStatsRowSet = session.Execute(preparedStatement.Bind(requesterUserId, true));

                Topic topic = new Topic();
                foreach (Row topicStatsRow in topicStatsRowSet)
                {
                    string topicId = topicStatsRow.GetValue<string>("topic_id");
                    int numberAnsweredCorrectly = topicStatsRow.GetValue<int>("number_answered_correctly");

                    Topic selectedTopic = topic.SelectTopicBasic(topicId, null, companyId, null, null, mainSession).Topic;

                    if (selectedTopic != null)
                    {
                        TopicStats topicStats = new TopicStats();
                        topicStats.Topic = selectedTopic;
                        decimal percentage = ((decimal)numberAnsweredCorrectly / selectedTopic.TotalNumberOfQuestions);
                        topicStats.CompletionPercentage = (int)(percentage * 100);

                        response.UserStats.Topics.Add(topicStats);
                    }
                }

                int limit = Convert.ToInt16(WebConfigurationManager.AppSettings["challenge_topic_stats_limit"]);
                if (response.UserStats.Topics.Count > 5)
                {
                    response.UserStats.Topics = response.UserStats.Topics.OrderByDescending(stats => stats.CompletionPercentage).ToList().GetRange(0, limit);
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public Exp SelectLevelOfUser(string userId, ISession session = null)
        {
            Exp levelExp = new Exp();

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                PreparedStatement psExpByUser = session.Prepare(CQLGenerator.SelectStatement("experience_by_user", new List<string> { "exp" }, new List<string> { "user_id" }));
                BoundStatement bsExpByUser = psExpByUser.Bind(userId);
                Row expByUserRow = session.Execute(bsExpByUser).FirstOrDefault();

                int expPoint = 0;

                if (expByUserRow != null)
                {
                    expPoint = (int)expByUserRow.GetValue<long>("exp");
                }

                PreparedStatement psExp = session.Prepare("SELECT * FROM experience_scheme_a WHERE from_exp <= ? ALLOW FILTERING");
                BoundStatement bsExp = psExp.Bind(expPoint);
                RowSet expRowset = session.Execute(bsExp);

                List<Dictionary<string, int>> expList = new List<Dictionary<string, int>>();

                foreach (Row expRow in expRowset)
                {
                    Dictionary<string, int> expDict = new Dictionary<string, int>();
                    expDict.Add("level", expRow.GetValue<int>("level"));
                    expDict.Add("fromExp", expRow.GetValue<int>("from_exp"));
                    expDict.Add("toExp", expRow.GetValue<int>("to_exp"));

                    expList.Add(expDict);
                }

                int currentLevel = 0;
                int currentExp = 0;
                int nextLevel = 0;
                int nextExpToReach = 0;

                expList = expList.OrderByDescending(expDict => expDict["fromExp"]).ToList();
                currentLevel = expList[0]["level"];
                currentExp = expPoint;
                nextLevel = currentLevel + 1;
                nextExpToReach = expList[0]["toExp"] + 1;

                levelExp.CurrentLevel = currentLevel;
                levelExp.CurrentExp = currentExp;
                levelExp.NextLevel = nextLevel;
                levelExp.NextExpToReach = nextExpToReach;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return levelExp;
        }

        public int SelectNumberOfUniqueCorrectByUser(string userId, ISession session = null)
        {
            int correct = 0;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user", new List<string>(), new List<string> { "user_id" }));
                Row leaderboardRow = session.Execute(ps.Bind(userId)).FirstOrDefault();

                if (leaderboardRow != null)
                {
                    correct = leaderboardRow.GetValue<int>("number_answered_correctly");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return correct;
        }


        public void UpdateLeaderboardStatsByQuestion(string userId, string topicId, string questionId, string selectedAnswer, string challengeId, string mainDepartmentId, string companyId, bool isCorrect, DateTimeOffset currentTime, DateTimeOffset? lastResettedDate, ISession analyticsSession, ISession mainSession)
        {
            try
            {
                if (analyticsSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticsSession = cm.getAnalyticSession();
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
                    new List<string>(), new List<string> { "user_id", "question_id" }, 1));
                BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
                Row challengeStatsRow = analyticsSession.Execute(bsChallengeStats).FirstOrDefault();

                // Comparing resetted date
                if (challengeStatsRow != null && lastResettedDate.HasValue)
                {
                    DateTimeOffset answeredTimestamp = challengeStatsRow.GetValue<DateTimeOffset>("answered_timestamp");
                    if (lastResettedDate > answeredTimestamp)
                    {
                        challengeStatsRow = null;
                    }
                }

                psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_user",
                    new List<string> { "user_id", "topic_id", "challenge_id", "question_id", "is_correct", "answered_timestamp" }));
                bsChallengeStats = psChallengeStats.Bind(userId, topicId, challengeId, questionId, isCorrect, currentTime);
                updateBatch.Add(bsChallengeStats);

                psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_topic",
                    new List<string> { "user_id", "topic_id", "challenge_id", "question_id", "is_correct", "answered_timestamp" }));
                bsChallengeStats = psChallengeStats.Bind(userId, topicId, challengeId, questionId, isCorrect, currentTime);
                updateBatch.Add(bsChallengeStats);


                // Update match up topic attempt
                int addedCorrect = 0;
                int addedIncorrect = 0;

                if (isCorrect)
                {
                    addedCorrect = 1;
                }
                else
                {
                    addedIncorrect = 1;
                }

                psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatement("matchup_topic_attempt_log", new List<string>(), new List<string> { "company_id", "topic_id" }));
                Row attemptRow = analyticsSession.Execute(psChallengeStats.Bind(companyId, topicId)).FirstOrDefault();

                if (attemptRow != null)
                {
                    int currentCorrect = attemptRow.GetValue<int>("correct_attempt");
                    int currentIncorrect = attemptRow.GetValue<int>("incorrect_attempt");
                    int currentTotal = currentCorrect + currentIncorrect;
                    float currentCorrectRatio = attemptRow.GetValue<float>("correct_ratio");
                    float currentIncorrectRatio = attemptRow.GetValue<float>("incorrect_ratio");

                    int newCorrect = currentCorrect + addedCorrect;
                    int newIncorrect = currentIncorrect + addedIncorrect;
                    int newTotal = newCorrect + newIncorrect;
                    float newCorrectRatio = (float)newCorrect / newTotal;
                    float newIncorrectRatio = (float)newIncorrect / newTotal;

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.UpdateStatement("matchup_topic_attempt_log",
                        new List<string> { "company_id", "topic_id" }, new List<string> { "correct_attempt", "incorrect_attempt", "correct_ratio", "incorrect_ratio" }, new List<string>()));
                    updateBatch.Add(psChallengeStats.Bind(newCorrect, newIncorrect, newCorrectRatio, newIncorrectRatio, companyId, topicId));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.DeleteStatement("matchup_topic_attempt_log_by_correct",
                        new List<string> { "company_id", "topic_id", "correct_ratio" }));
                    deleteBatch.Add(psChallengeStats.Bind(companyId, topicId, currentCorrectRatio));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.DeleteStatement("matchup_topic_attempt_log_by_incorrect",
                        new List<string> { "company_id", "topic_id", "incorrect_ratio" }));
                    deleteBatch.Add(psChallengeStats.Bind(companyId, topicId, currentIncorrectRatio));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_correct",
                        new List<string> { "company_id", "topic_id", "correct_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(companyId, topicId, newCorrectRatio, newCorrect, newIncorrect));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_incorrect",
                        new List<string> { "company_id", "topic_id", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(companyId, topicId, newIncorrectRatio, newCorrect, newIncorrect));

                }
                else
                {
                    int newTotal = addedCorrect + addedIncorrect;
                    float newCorrectRatio = (float)addedCorrect / newTotal;
                    float newIncorrectRatio = (float)addedIncorrect / newTotal;

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log",
                        new List<string> { "company_id", "topic_id", "correct_attempt", "incorrect_attempt", "correct_ratio", "incorrect_ratio" }));
                    updateBatch.Add(psChallengeStats.Bind(companyId, topicId, addedCorrect, addedIncorrect, newCorrectRatio, newIncorrectRatio));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_correct",
                        new List<string> { "company_id", "topic_id", "correct_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(companyId, topicId, newCorrectRatio, addedCorrect, addedIncorrect));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_topic_attempt_log_by_incorrect",
                        new List<string> { "company_id", "topic_id", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(companyId, topicId, newIncorrectRatio, addedCorrect, addedIncorrect));

                }

                // Update match up question attempt
                psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatement("matchup_question_attempt_log", new List<string>(), new List<string> { "topic_id", "question_id" }));
                attemptRow = analyticsSession.Execute(psChallengeStats.Bind(topicId, questionId)).FirstOrDefault();

                if (attemptRow != null)
                {
                    int currentCorrect = attemptRow.GetValue<int>("correct_attempt");
                    int currentIncorrect = attemptRow.GetValue<int>("incorrect_attempt");
                    int currentTotal = currentCorrect + currentIncorrect;
                    float currentCorrectRatio = attemptRow.GetValue<float>("correct_ratio");
                    float currentIncorrectRatio = attemptRow.GetValue<float>("incorrect_ratio");

                    int newCorrect = currentCorrect + addedCorrect;
                    int newIncorrect = currentIncorrect + addedIncorrect;
                    int newTotal = newCorrect + newIncorrect;
                    float newCorrectRatio = (float)newCorrect / newTotal;
                    float newIncorrectRatio = (float)newIncorrect / newTotal;

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.UpdateStatement("matchup_question_attempt_log",
                        new List<string> { "topic_id", "question_id" }, new List<string> { "correct_attempt", "incorrect_attempt", "correct_ratio", "incorrect_ratio" }, new List<string>()));
                    updateBatch.Add(psChallengeStats.Bind(newCorrect, newIncorrect, newCorrectRatio, newIncorrectRatio, topicId, questionId));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_correct",
                        new List<string> { "topic_id", "question_id", "correct_ratio" }));
                    deleteBatch.Add(psChallengeStats.Bind(topicId, questionId, currentCorrectRatio));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_incorrect",
                        new List<string> { "topic_id", "question_id", "incorrect_ratio" }));
                    deleteBatch.Add(psChallengeStats.Bind(topicId, questionId, currentIncorrectRatio));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_correct",
                        new List<string> { "topic_id", "question_id", "correct_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(topicId, questionId, newCorrectRatio, newCorrect, newIncorrect));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_incorrect",
                        new List<string> { "topic_id", "question_id", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(topicId, questionId, newIncorrectRatio, newCorrect, newIncorrect));

                }
                else
                {
                    int newTotal = addedCorrect + addedIncorrect;
                    float newCorrectRatio = (float)addedCorrect / newTotal;
                    float newIncorrectRatio = (float)addedIncorrect / newTotal;

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log",
                        new List<string> { "topic_id", "question_id", "correct_attempt", "incorrect_attempt", "correct_ratio", "incorrect_ratio" }));
                    updateBatch.Add(psChallengeStats.Bind(topicId, questionId, addedCorrect, addedIncorrect, newCorrectRatio, newIncorrectRatio));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_correct",
                        new List<string> { "topic_id", "question_id", "correct_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(topicId, questionId, newCorrectRatio, addedCorrect, addedIncorrect));

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_incorrect",
                        new List<string> { "topic_id", "question_id", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(topicId, questionId, newIncorrectRatio, addedCorrect, addedIncorrect));

                }

                // Update user attempt
                psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatementWithLimit("matchup_user_attempt_by_question", new List<string>(), new List<string> { "topic_id", "question_id", "user_id" }, 1));
                attemptRow = analyticsSession.Execute(psChallengeStats.Bind(topicId, questionId, userId)).FirstOrDefault();

                int tries = 1;
                if (attemptRow != null)
                {
                    tries = attemptRow.GetValue<int>("tries") + 1;
                }

                psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_user_attempt_by_question",
                    new List<string> { "topic_id", "question_id", "user_id", "tries", "is_correct" }));
                updateBatch.Add(psChallengeStats.Bind(topicId, questionId, userId, tries, isCorrect));

                psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_user_attempt_by_tries",
                    new List<string> { "topic_id", "question_id", "user_id", "tries", "is_correct" }));
                updateBatch.Add(psChallengeStats.Bind(topicId, questionId, userId, tries, isCorrect));

                // Update option attempt
                psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatement("matchup_option_attempt_log", new List<string>(), new List<string> { "topic_id", "question_id", "option_id" }));
                attemptRow = analyticsSession.Execute(psChallengeStats.Bind(topicId, questionId, selectedAnswer)).FirstOrDefault();

                if (attemptRow != null)
                {
                    int attempt = attemptRow.GetValue<int>("attempt") + 1;

                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.UpdateStatement("matchup_option_attempt_log",
                       new List<string> { "topic_id", "question_id", "option_id" }, new List<string> { "attempt" }, new List<string>()));
                    updateBatch.Add(psChallengeStats.Bind(attempt, topicId, questionId, selectedAnswer));
                }
                else
                {
                    psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("matchup_option_attempt_log",
                      new List<string> { "topic_id", "question_id", "option_id", "attempt" }));
                    updateBatch.Add(psChallengeStats.Bind(topicId, questionId, selectedAnswer, 1));
                }


                analyticsSession.Execute(deleteBatch);
                analyticsSession.Execute(updateBatch);

                // Get events associated to user
                Event eventManager = new Event();
                List<Event> selectedEvents = eventManager.SelectLiveEventsByTopicIdAndUserId(userId, mainDepartmentId, companyId, topicId, mainSession);

                // Question is answered before by this current user
                if (challengeStatsRow != null)
                {
                    bool isAnsweredCorrectlyPreviously = challengeStatsRow.GetValue<bool>("is_correct");
                    // Now correct
                    if (isCorrect)
                    {
                        // Previously answer wrongly, but now correct
                        if (!isAnsweredCorrectlyPreviously)
                        {
                            UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, 1, analyticsSession);
                        }

                        //UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession);

                        Thread updateEventLeaderboardThread = new Thread(() => UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession));
                        updateEventLeaderboardThread.Start();
                    }
                    // Now wrong
                    else
                    {
                        // Previously answer correctly, but now wrong
                        if (isAnsweredCorrectlyPreviously)
                        {
                            UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, -1, analyticsSession);

                            //UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, -1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession);

                            Thread updateEventLeaderboardThread = new Thread(() => UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, -1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession));
                            updateEventLeaderboardThread.Start();
                        }

                    }
                }
                // Question is not answered before by this current user
                else
                {
                    if (isCorrect)
                    {

                        UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, 1, analyticsSession);

                        //UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, false, analyticsSession, mainSession);

                        Thread updateEventLeaderboardThread = new Thread(() => UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, false, analyticsSession, mainSession));
                        updateEventLeaderboardThread.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveLeaderboardStatsByQuestion(string userId, string topicId, string questionId, string challengeId, string mainDepartmentId, string companyId, bool isCorrect, DateTimeOffset currentTime, ISession analyticsSession, ISession mainSession)
        {
            try
            {
                if (analyticsSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticsSession = cm.getAnalyticSession();
                }

                PreparedStatement psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
                    new List<string>(), new List<string> { "user_id", "question_id" }, 1));
                BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
                Row challengeStatsRow = analyticsSession.Execute(bsChallengeStats).FirstOrDefault();

                // Get events associated to user
                Event eventManager = new Event();
                List<Event> selectedEvents = eventManager.SelectLiveEventsByTopicIdAndUserId(userId, mainDepartmentId, companyId, topicId, mainSession);

                // Question is answered before by this current user
                if (challengeStatsRow != null)
                {
                    bool isAnsweredCorrectlyPreviously = challengeStatsRow.GetValue<bool>("is_correct");

                    // Previously answer correctly, but now wrong
                    if (isAnsweredCorrectlyPreviously)
                    {
                        UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, -1, analyticsSession);
                    }

                    // Update event
                    RemoveEventLeaderboardByQuestion(userId, selectedEvents, mainDepartmentId, companyId, topicId, questionId, isAnsweredCorrectlyPreviously, analyticsSession, mainSession);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }


        private void UpdateLeaderboard(string userId, string topicId, string companyId, string mainDepartmentId, int value, ISession session)
        {
            try
            {
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                DateTimeOffset currentTime = DateTime.UtcNow;

                //------------------------- Topic leaderboard by user ------------------------
                PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id", "topic_id" }));
                BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
                Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByUser = 0;
                DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                // Update
                if (numberAnswerCorrectlyByUser + value <= 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
                        new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(0, currentTime, userId, topicId));
                }
                else
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
                        new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser + value, currentTime, userId, topicId));

                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + value, currentTime, topicId, true));

                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + value, currentTime, topicId, true));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();

                //------------------------- Company leaderboard by user ------------------------
                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                bsLeaderBoard = psLeaderBoard.Bind(userId);
                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByCompany = 0;
                DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                  new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                // Update
                if (numberAnswerCorrectlyByCompany + value <= 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                        new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, userId));
                }
                else
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                        new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + value, currentTime, userId));

                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + value, currentTime, companyId));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();

                //------------------------- Company leaderboard by department ------------------------
                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                    new List<string>(), new List<string> { "department_id" }));
                bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByDepartment = 0;
                DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                // Update
                if (numberAnswerCorrectlyByDepartment + value <= 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                        new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, mainDepartmentId));
                }
                else
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                        new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + value, currentTime, mainDepartmentId));

                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                        new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + value, currentTime, mainDepartmentId));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();


                //------------------------- Department leaderboard by user ------------------------
                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                    new List<string>(), new List<string> { "user_id" }));
                bsLeaderBoard = psLeaderBoard.Bind(userId);
                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByDepartmentUser = 0;
                DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                // Update
                if (numberAnswerCorrectlyByDepartmentUser + value <= 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                        new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, 0, currentTime, userId));
                }
                else
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                        new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, userId));

                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        // COMBINE WITH UPDATE EVENTLEADERBOARD!!!!!
        private void RemoveEventLeaderboardByQuestion(string userId, List<Event> events, string mainDepartmentId, string companyId, string topicid, string questionId, bool isAnswerCorrectlyPreviously, ISession analyticSession, ISession mainSession)
        {
            try
            {
                foreach (Event selectedEvent in events)
                {
                    string eventId = selectedEvent.EventId;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;
                    DateTimeOffset startTimeOffset = selectedEvent.StartTimestamp;
                    DateTimeOffset endTimeOffset = selectedEvent.EndTimestamp;

                    // Check scoring and calculation
                    // Scoring
                    // Top scorer vs Top department

                    // Calculation type
                    // Accumulative
                    // Unique
                    // Total Accumulative vs Total unique
                    // Avg accumulative vs avg unique

                    BatchStatement deleteStatements = new BatchStatement();
                    BatchStatement updateStatements = new BatchStatement();

                    DateTimeOffset currentTime = DateTime.UtcNow;

                    int scoreToDeduct = 0;
                    if (scoringType == (int)Event.ScoringTypeEnum.TopScorer)
                    {
                        Log.Debug("Top Scorer");

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.Accumulative)
                        {
                            // Get the total number of correct times from start to end date
                            PreparedStatement psScoreboard = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("challenge_question_stats_by_user",
                                new List<string>(), new List<string> { "user_id", "question_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "answered_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                            BoundStatement bsScoreboard = psScoreboard.Bind(userId, questionId, true, startTimeOffset, endTimeOffset);
                            RowSet scoreboardRowset = analyticSession.Execute(bsScoreboard);

                            scoreToDeduct = -scoreboardRowset.ToList().Count();

                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.Unique)
                        {
                            // Unique does not take in previously correct answer
                            if (!isAnswerCorrectlyPreviously)
                            {
                                Log.Debug("Unique -> Continue: answer wrongly previously");
                                continue;
                            }

                            scoreToDeduct = -1;
                        }

                        //------------------------- Event leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id", "event_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, eventId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByUser = 0;
                        DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        // Update
                        if (numberAnswerCorrectlyByUser + scoreToDeduct <= 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user",
                                new List<string> { "user_id", "event_id" }, new List<string> { "department_id", "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, 0, currentTime, userId, eventId));
                        }
                        else
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user",
                                new List<string> { "user_id", "event_id" }, new List<string> { "department_id", "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByUser + scoreToDeduct, currentTime, userId, eventId));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_leaderboard_by_user_sorted",
                               new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + scoreToDeduct, currentTime, eventId, true));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_event_sorted",
                                new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + scoreToDeduct, currentTime, eventId, true));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        Log.Debug("Update eventleaderboard completed");
                    }
                    else if (scoringType == (int)Event.ScoringTypeEnum.TopDepartment)
                    {

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative)
                        {
                            // Get the total number of correct times from start to end date
                            PreparedStatement psScoreboard = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("challenge_question_stats_by_user",
                                new List<string>(), new List<string> { "user_id", "question_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "answered_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                            BoundStatement bsScoreboard = psScoreboard.Bind(userId, questionId, true, startTimeOffset, endTimeOffset);
                            RowSet scoreboardRowset = analyticSession.Execute(bsScoreboard);

                            scoreToDeduct = -scoreboardRowset.ToList().Count();
                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalUnique || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            // Unique does not take in previously correct answer
                            if (!isAnswerCorrectlyPreviously)
                            {
                                Log.Debug("Unique -> Continue: answer wrongly previously");
                                continue;
                            }

                            scoreToDeduct = -1;
                        }

                        //------------------------- Department user leaderboard by event ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id", "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId, userId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByDepartmentUser = 0;
                        DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                            new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                        // Update
                        if (numberAnswerCorrectlyByDepartmentUser + scoreToDeduct <= 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_user_leaderboard_by_department",
                                new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(0, currentTime, mainDepartmentId, eventId, userId));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_department_leaderboard_by_user",
                                new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(0, currentTime, mainDepartmentId, eventId, userId));
                        }
                        else
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_user_leaderboard_by_department",
                                new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + scoreToDeduct, currentTime, mainDepartmentId, eventId, userId));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_department_leaderboard_by_user",
                                new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + scoreToDeduct, currentTime, mainDepartmentId, eventId, userId));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                                new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser + scoreToDeduct, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();

                        //------------------------- Event leaderboard by department ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                           new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        float numberAnswerCorrectlyByDepartment = 0f;
                        DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<float>("score");
                            lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                        // Update
                        float newScore = 0f;

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        RowSet leaderBoardRowset = analyticSession.Execute(bsLeaderBoard);

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, mainDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        if (newScore <= 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                                new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(0, currentTime, mainDepartmentId, eventId));
                        }
                        else
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                                new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                            updateStatements.Add(psLeaderBoard.Bind(newScore, currentTime, mainDepartmentId, eventId));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, newScore, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        private void UpdateEventLeaderboard(string userId, List<Event> events, string mainDepartmentId, string companyId, int value, bool isAnswerCorrectlyPreviously, ISession analyticSession, ISession mainSession)
        {
            try
            {
                foreach (Event selectedEvent in events)
                {
                    string eventId = selectedEvent.EventId;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Check scoring and calculation
                    // Scoring
                    // Top scorer vs Top department

                    // Calculation type
                    // Accumulative
                    // Unique
                    // Total Accumulative vs Total unique
                    // Avg accumulative vs avg unique

                    BatchStatement deleteStatements = new BatchStatement();
                    BatchStatement updateStatements = new BatchStatement();

                    DateTimeOffset currentTime = DateTime.UtcNow;

                    if (scoringType == (int)Event.ScoringTypeEnum.TopScorer)
                    {
                        Log.Debug("Top Scorer");

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.Accumulative)
                        {
                            // Accumulative does not consider previously wronged question
                            if (value < 0)
                            {
                                Log.Debug("Accumlative -> Continue: answer wrongly");
                                continue;
                            }
                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.Unique)
                        {
                            // Unique does not take in previously correct answer
                            if (value > 0 && isAnswerCorrectlyPreviously)
                            {
                                Log.Debug("Unique -> Continue: answer correctly");
                                continue;
                            }
                        }

                        //------------------------- Event leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id", "event_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, eventId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByUser = 0;
                        DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        // Update
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user",
                           new List<string> { "user_id", "event_id" }, new List<string> { "department_id", "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByUser + value, currentTime, userId, eventId));

                        if (numberAnswerCorrectlyByUser + value > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_leaderboard_by_user_sorted",
                                new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + value, currentTime, eventId, true));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_event_sorted",
                                new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + value, currentTime, eventId, true));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        Log.Debug("Update eventleaderboard completed");
                    }
                    else if (scoringType == (int)Event.ScoringTypeEnum.TopDepartment)
                    {

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative)
                        {
                            // Accumulative does not consider previously wronged question
                            if (value < 0)
                            {
                                continue;
                            }
                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalUnique || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            // Unique does not take in previously correct answer
                            if (value > 0 && isAnswerCorrectlyPreviously)
                            {
                                continue;
                            }
                        }

                        //------------------------- Department user leaderboard by event ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id", "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId, userId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByDepartmentUser = 0;
                        DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                            new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                        // Update
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_user_leaderboard_by_department",
                            new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId, eventId, userId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_department_leaderboard_by_user",
                            new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId, eventId, userId));

                        if (numberAnswerCorrectlyByDepartmentUser + value > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                                new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();

                        //------------------------- Event leaderboard by department ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                           new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        float numberAnswerCorrectlyByDepartment = 0f;
                        DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<float>("score");
                            lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                        // Update
                        float newScore = 0f;

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        RowSet leaderBoardRowset = analyticSession.Execute(bsLeaderBoard);

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, mainDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(newScore, currentTime, mainDepartmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, newScore, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                    }


                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public void InsertLeaderboardForEvent(string eventId, int scoringType, int scoreCalculationType, List<string> topicIds, string adminUserId, string companyId, List<string> participantUserIds, DateTime startTime, ISession analyticSession, ISession mainSession)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                Event selectedEvent = new Event
                {
                    EventId = eventId,
                    ScoringType = scoringType,
                    CalculationType = scoreCalculationType
                };

                foreach (string topicId in topicIds)
                {
                    // Get all question ids
                    PreparedStatement psQuestion = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_question",
                        new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsQuestion = psQuestion.Bind(topicId);
                    RowSet questionRowset = mainSession.Execute(bsQuestion);

                    List<string> selectedQuestionIds = new List<string>();

                    foreach (Row questionRow in questionRowset)
                    {
                        int status = questionRow.GetValue<int>("status");

                        if (status == ChallengeQuestion.QuestionStatus.CODE_ACTIVE)
                        {
                            string questionId = questionRow.GetValue<string>("id");

                            foreach (string participantId in participantUserIds)
                            {
                                if (scoreCalculationType == (int)Event.CalculationTypeEnum.Accumulative ||
                                    scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative ||
                                    scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative)
                                {
                                    PreparedStatement psQuestionbyTopic = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_question_stats_by_topic",
                                        new List<string>(), new List<string> { "topic_id", "question_id", "user_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                                    BoundStatement bsQuestionbyTopic = psQuestionbyTopic.Bind(topicId, questionId, participantId, true, startTime);
                                    RowSet questionByTopicRowset = analyticSession.Execute(bsQuestionbyTopic);

                                    List<Department> userDepartments = new Department().GetAllDepartmentByUserId(participantId, companyId, mainSession).Departments;
                                    string mainDepartmentId = userDepartments[0].Id;

                                    foreach (Row questionByTopicRow in questionByTopicRowset)
                                    {
                                        UpdateEventLeaderboard(participantId, new List<Event> { selectedEvent }, mainDepartmentId, companyId, 1, true, analyticSession, mainSession);
                                    }
                                }
                                else
                                {
                                    PreparedStatement psQuestionbyTopic = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_question_stats_by_topic",
                                        new List<string>(), new List<string> { "topic_id", "question_id", "user_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, 1));
                                    BoundStatement bsQuestionbyTopic = psQuestion.Bind(topicId, questionId, participantId, true, startTime);
                                    RowSet questionByTopicRowset = mainSession.Execute(bsQuestionbyTopic);

                                    List<Department> userDepartments = new Department().GetAllDepartmentByUserId(participantId, companyId, mainSession).Departments;
                                    string mainDepartmentId = userDepartments[0].Id;

                                    foreach (Row questionByTopicRow in questionByTopicRowset)
                                    {
                                        UpdateEventLeaderboard(participantId, new List<Event> { selectedEvent }, mainDepartmentId, companyId, 1, false, analyticSession, mainSession);
                                    }
                                }
                            }
                        }
                    }
                }

                Log.Debug("Thread for update event leaderboard done");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromEventLeaderboard(string adminUserId, string companyId, string userId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                // This method only involves when delete user
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Event leaderboard by user ------------------------
                PreparedStatement psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                RowSet leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(userId, score, updatedTimestamp, eventId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(userId, score, updatedTimestamp, eventId));
                }

                analyticSession.Execute(deleteStatements);
                deleteStatements = new BatchStatement();

                //------------------------- Event department leaderboard by user ------------------------
                psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    string departmentId = leaderBoardByUserRow.GetValue<string>("department_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    Event selectedEvent = new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL, (int)Event.EventStatusEnum.Ignored, mainSession).Event;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Need to delete this first
                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(departmentId, eventId, score, updatedTimestamp, userId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department",
                        new List<string> { "department_id", "event_id", "user_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(departmentId, eventId, userId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_department_leaderboard_by_user",
                        new List<string> { "department_id", "event_id", "user_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(departmentId, eventId, userId));

                    analyticSession.Execute(deleteStatements);
                    deleteStatements = new BatchStatement();

                    //------------------------- Department leaderboard by event ------------------------
                    PreparedStatement psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                      new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardByDepartmentRow = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId)).FirstOrDefault();

                    if (leaderBoardByDepartmentRow != null)
                    {
                        float departmentScore = leaderBoardByDepartmentRow.GetValue<float>("score");
                        DateTimeOffset updatedDepartmentTimestamp = leaderBoardByDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "department_id", "event_id", "score", "last_updated_timestamp" }));
                        deleteStatements.Add(psLeaderBoardByDepartment.Bind(departmentId, eventId, score, updatedTimestamp));

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoardByDepartment.Bind(newScore, currentTime, departmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoardByDepartment.Bind(eventId, newScore, currentTime, departmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                        deleteStatements = new BatchStatement();
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is removed, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                            new List<string> { "user_id", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, userId));
                            }

                        }

                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            mainDepartmentId = leaderBoardRow.GetValue<string>("department_id");
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, 0, currentTime, userId));
                            }
                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, mainDepartmentId));
                            }
                        }


                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                            new List<string> { "user_id", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, userId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, 0, currentTime, userId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

#warning Delete event analytics if event is deleted
        public void DeleteEventLeaderboard(string eventId, ISession analyticSession = null)
        {

        }

        public void HideFromEventLeaderboard(string adminUserId, string companyId, string userId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                // This method only involves when suspending user
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Event leaderboard by user ------------------------
                PreparedStatement psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                RowSet leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(false, userId, score, updatedTimestamp, eventId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_event_sorted",
                        new List<string> { "event_id", "score", "last_updated_timestamp", "user_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(false, eventId, score, updatedTimestamp, userId));
                }

                analyticSession.Execute(updateStatements);
                updateStatements = new BatchStatement();

                //------------------------- Event department leaderboard by user ------------------------
                psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    string departmentId = leaderBoardByUserRow.GetValue<string>("department_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    Event selectedEvent = new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL, (int)Event.EventStatusEnum.Ignored, mainSession).Event;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Need to delete this first
                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id", }));
                    analyticSession.Execute(psLeaderBoardByUser.Bind(departmentId, eventId, score, updatedTimestamp, userId));

                    //------------------------- Department leaderboard by event ------------------------
                    PreparedStatement psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                      new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardByDepartmentRow = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId)).FirstOrDefault();

                    if (leaderBoardByDepartmentRow != null)
                    {
                        float departmentScore = leaderBoardByDepartmentRow.GetValue<float>("score");
                        DateTimeOffset updatedDepartmentTimestamp = leaderBoardByDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "department_id", "event_id", "score", "last_updated_timestamp" }));
                        deleteStatements.Add(psLeaderBoardByDepartment.Bind(departmentId, eventId, score, updatedTimestamp));

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoardByDepartment.Bind(newScore, currentTime, departmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoardByDepartment.Bind(eventId, newScore, currentTime, departmentId));
                        }

                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
        public void HideFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is hidden, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, userId));
                            }

                        }

                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            mainDepartmentId = leaderBoardRow.GetValue<string>("department_id");
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, 0, currentTime, userId));
                            }
                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, userId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, 0, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                            else
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, 0, currentTime, userId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public void UnhideFromEventLeaderboard(string adminUserId, string companyId, string userId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                // This method only involves when suspending user
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Event leaderboard by user ------------------------
                PreparedStatement psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                RowSet leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(true, userId, score, updatedTimestamp, eventId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_event_sorted",
                        new List<string> { "event_id", "score", "last_updated_timestamp", "user_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(true, eventId, score, updatedTimestamp, userId));
                }

                analyticSession.Execute(updateStatements);
                updateStatements = new BatchStatement();

                //------------------------- Event department leaderboard by user ------------------------
                psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    string departmentId = leaderBoardByUserRow.GetValue<string>("department_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    Event selectedEvent = new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL, (int)Event.EventStatusEnum.Ignored, mainSession).Event;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Need to insert this first
                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id", }));
                    analyticSession.Execute(psLeaderBoardByUser.Bind(departmentId, eventId, score, updatedTimestamp, userId));

                    //------------------------- Department leaderboard by event ------------------------
                    PreparedStatement psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                      new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardByDepartmentRow = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId)).FirstOrDefault();

                    if (leaderBoardByDepartmentRow != null)
                    {
                        float departmentScore = leaderBoardByDepartmentRow.GetValue<float>("score");
                        DateTimeOffset updatedDepartmentTimestamp = leaderBoardByDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "department_id", "event_id", "score", "last_updated_timestamp" }));
                        deleteStatements.Add(psLeaderBoardByDepartment.Bind(departmentId, eventId, score, updatedTimestamp));

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoardByDepartment.Bind(newScore, currentTime, departmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoardByDepartment.Bind(eventId, newScore, currentTime, departmentId));
                        }

                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UnhideFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is hidden, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, companyId));

                        }

                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            mainDepartmentId = leaderBoardRow.GetValue<string>("department_id");
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, companyId));

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SwitchLeaderboardForDepartment(string userId, string companyId, string currentDepartmentId, string newDepartmentId, ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }


                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Department leaderboard by user ------------------------
                PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id", "department_id" }));
                BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, currentDepartmentId);
                Row leaderBoardByUserRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                // Only if the user has contributed some points to that department
                if (leaderBoardByUserRow != null)
                {
                    int numberAnswerCorrectlyByDepartmentUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                    DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    if (numberAnswerCorrectlyByDepartmentUser > 0)
                    {
                        // Current department points
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                               new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(currentDepartmentId);

                        Row currentDepartmentRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyForCurrentDepartment = currentDepartmentRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByCurrentDepartment = currentDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        // New department points
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                               new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(newDepartmentId);

                        Row newDepartmentRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyForNewDepartment = 0;
                        DateTimeOffset lastUpdatedTimestampByNewDepartment = currentTime;

                        if (newDepartmentRow != null)
                        {
                            numberAnswerCorrectlyForNewDepartment = newDepartmentRow.GetValue<int>("number_answered_correctly");
                            lastUpdatedTimestampByNewDepartment = newDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        //------------------------- Company leaderboard by department (Current Department) ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                                   new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser, currentTime, currentDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment, lastUpdatedTimestampByCurrentDepartment, currentDepartmentId));

                        if (numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser, currentTime, currentDepartmentId));
                        }

                        //------------------------- Company leaderboard by department (New Department) ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                                   new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment + numberAnswerCorrectlyByDepartmentUser, currentTime, newDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment, lastUpdatedTimestampByNewDepartment, newDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                                   new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment + numberAnswerCorrectlyByDepartmentUser, currentTime, newDepartmentId));

                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(newDepartmentId, numberAnswerCorrectlyByDepartmentUser, userId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                   new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, currentDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, newDepartmentId));


                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SwitchEventLeaderboardForDepartment(string userId, string companyId, string currentDepartmentId, string newDepartmentId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                Event eventManager = new Event();

                DateTimeOffset currentTime = DateTime.UtcNow;

                // Update everyone/personnel targeted events
                PreparedStatement psLeaderboardByUser = null;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                #region Personnel Events
                List<Event> personnelEvents = eventManager.SelectLivePersonnelEventsByUserId(userId, companyId, mainSession).Events;
                foreach (Event personnelEvent in personnelEvents)
                {
                    string eventId = personnelEvent.EventId;
                    psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                        new List<string>(), new List<string> { "user_id", "event_id" }));
                    Row leaderboardByUserRow = analyticSession.Execute(psLeaderboardByUser.Bind(userId, eventId)).FirstOrDefault();

                    if (leaderboardByUserRow != null)
                    {
                        int score = leaderboardByUserRow.GetValue<int>("score");
                        bool isVisible = leaderboardByUserRow.GetValue<bool>("is_visible");
                        DateTimeOffset updatedTimestamp = leaderboardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                           new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user",
                            new List<string> { "user_id", "event_id" }, new List<string> { "department_id" }, new List<string>()));
                        updateStatements.Add(psLeaderboardByUser.Bind(newDepartmentId, userId, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("event_leaderboard_by_user_sorted",
                          new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", "department_id", "is_visible" }));
                        updateStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId, newDepartmentId, isVisible));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_event_sorted",
                          new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", "department_id", "is_visible" }));
                        updateStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId, newDepartmentId, isVisible));

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
                #endregion

                #region Department events
                // Update department targeted events
                List<Event> currentDepartmentEvents = eventManager.SelectLiveDepartmentEventsByDepartmentId(null, currentDepartmentId, companyId, mainSession).Events;
                List<Event> newDepartmentEvents = eventManager.SelectLiveDepartmentEventsByDepartmentId(null, newDepartmentId, companyId, mainSession).Events;

                #region Remove from current department events
                //Remove from current department events
                foreach (Event currentDepartmentEvent in currentDepartmentEvents)
                {
                    string eventId = currentDepartmentEvent.EventId;
                    DateTimeOffset startTimeOffset = currentDepartmentEvent.StartTimestamp;
                    DateTimeOffset endTimeOffset = currentDepartmentEvent.EndTimestamp;

                    int scoringType = currentDepartmentEvent.ScoringType;
                    int scoreCalculationType = currentDepartmentEvent.CalculationType;

                    psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                        new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderboardByUserRow = analyticSession.Execute(psLeaderboardByUser.Bind(currentDepartmentId, eventId)).FirstOrDefault();

                    if (leaderboardByUserRow != null)
                    {
                        float score = leaderboardByUserRow.GetValue<float>("score");
                        DateTimeOffset updatedTimestamp = leaderboardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_department",
                           new List<string> { "department_id", "event_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(eventId, score, updatedTimestamp, currentDepartmentId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department",
                            new List<string> { "department_id", "event_id", "user_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId, userId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_department_leaderboard_by_user",
                            new List<string> { "department_id", "event_id", "user_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId, userId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id", "user_id" }));
                        leaderboardByUserRow = analyticSession.Execute(psLeaderboardByUser.Bind(currentDepartmentId, eventId, userId)).FirstOrDefault();

                        if (leaderboardByUserRow != null)
                        {
                            score = leaderboardByUserRow.GetValue<float>("score");
                            updatedTimestamp = leaderboardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                                new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id" }));
                            deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId, score, updatedTimestamp, userId));
                        }

                        analyticSession.Execute(deleteStatements);
                        deleteStatements = new BatchStatement();

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderboardByUser.Bind(currentDepartmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, currentDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderboardByUser.Bind(newScore, currentTime, currentDepartmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderboardByUser.Bind(eventId, newScore, currentTime, currentDepartmentId));
                        }

                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                    }
                }
                #endregion

                #region Insert to new department events
                // Add to new department events
                foreach (Event newDepartmentEvent in newDepartmentEvents)
                {
                    string eventId = newDepartmentEvent.EventId;
                    DateTimeOffset startTimeOffset = newDepartmentEvent.StartTimestamp;
                    DateTimeOffset endTimeOffset = newDepartmentEvent.EndTimestamp;

                    int scoringType = newDepartmentEvent.ScoringType;
                    int scoreCalculationType = newDepartmentEvent.CalculationType;

                    int scoreToAdd = 0;
                    if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative)
                    {
                        foreach (Topic topicInEvent in newDepartmentEvent.Topics)
                        {
                            foreach (ChallengeQuestion question in topicInEvent.Questions)
                            {
                                string questionId = question.Id;

                                // Get the total number of correct times from start to end date
                                PreparedStatement psScoreboard = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("challenge_question_stats_by_user",
                                    new List<string>(), new List<string> { "user_id", "question_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "answered_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                                BoundStatement bsScoreboard = psScoreboard.Bind(userId, questionId, true, startTimeOffset, endTimeOffset);
                                RowSet scoreboardRowset = analyticSession.Execute(bsScoreboard);

                                scoreToAdd += scoreboardRowset.ToList().Count();
                            }

                        }

                    }
                    else if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalUnique || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                    {
                        foreach (Topic topicInEvent in newDepartmentEvent.Topics)
                        {
                            foreach (ChallengeQuestion question in topicInEvent.Questions)
                            {
                                string questionId = question.Id;

                                // Unique does not take in previously correct answer
                                PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
                                    new List<string>(), new List<string> { "user_id", "question_id" }, 1));
                                BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
                                Row challengeStatsRow = analyticSession.Execute(bsChallengeStats).FirstOrDefault();

                                if (challengeStatsRow != null & challengeStatsRow.GetValue<bool>("is_correct"))
                                {
                                    scoreToAdd += 1;
                                }
                            }
                        }
                    }

                    //------------------------- Event user leaderboard by department ------------------------
                    // Update
                    PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_user_leaderboard_by_department",
                        new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(scoreToAdd, currentTime, newDepartmentId, eventId, userId));

                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(eventId, userId, scoreToAdd, currentTime, newDepartmentId));

                    analyticSession.Execute(updateStatements);

                    updateStatements = new BatchStatement();

                    //------------------------- Event leaderboard by department ------------------------
                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                       new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardRow = analyticSession.Execute(psLeaderBoard.Bind(newDepartmentId, eventId)).FirstOrDefault();

                    float numberAnswerCorrectlyByDepartment = 0f;
                    DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                    if (leaderBoardRow != null)
                    {
                        numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<float>("score");
                        lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                    }

                    // Delete
                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                        new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                    deleteStatements.Add(psLeaderBoard.Bind(eventId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, newDepartmentId));

                    // Update
                    float newScore = 0f;

                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                        new List<string>(), new List<string> { "department_id", "event_id" }));
                    RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoard.Bind(newDepartmentId, eventId));

                    foreach (Row leaderboardRow in leaderBoardRowset)
                    {
                        int scoreByUser = leaderboardRow.GetValue<int>("score");
                        newScore += scoreByUser;
                    }

                    if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                    {
                        int numberOfPersonnel = 0;
                        Department departmentManager = new Department();
                        Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, newDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                        numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                        newScore = newScore / numberOfPersonnel;
                    }

                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                        new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(newScore, currentTime, newDepartmentId, eventId));

                    if (newScore > 0)
                    {
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        updateStatements.Add(psLeaderBoard.Bind(eventId, newScore, currentTime, newDepartmentId));
                    }

                    analyticSession.Execute(deleteStatements);
                    analyticSession.Execute(updateStatements);

                }
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateChallengeStats(string companyId, string userId, ChallengeStatType stat, ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                string columnStatName = string.Empty;

                switch (stat)
                {
                    case ChallengeStatType.Draw:
                        columnStatName = "total_draws";
                        break;
                    case ChallengeStatType.Lost:
                        columnStatName = "total_losses";
                        break;
                    case ChallengeStatType.NetworkError:
                        columnStatName = "total_network_errors";
                        break;
                    case ChallengeStatType.Surrender:
                        columnStatName = "total_surrenders";
                        break;
                    case ChallengeStatType.Win:
                        columnStatName = "total_win";
                        break;

                }

                PreparedStatement psChallengeStats = session.Prepare(CQLGenerator.UpdateCounterStatement("challenge_total_stats_counter_by_user",
                    new List<string> { "company_id", "user_id" }, new List<string> { columnStatName, "total_games_played" }, new List<int> { 1, 1 }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(companyId, userId);

                session.Execute(bChallengeStats);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AnalyticsSelectLeaderboardByCompanyResponse SelectLeaderboardByCompany(string requesterUserId, string companyId)
        {
            AnalyticsSelectLeaderboardByCompanyResponse response = new AnalyticsSelectLeaderboardByCompanyResponse();
            response.TopPersonnels = new List<User>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                if (vh.ValidateCompany(companyId, mainSession) == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_company_sorted",
                    new List<string>(), new List<string> { "company_id" }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(companyId);

                RowSet challengeStatsRowSet = analyticSession.Execute(bChallengeStats);

                User user = new User();
                List<string> cleanUpList = new List<string>();
                foreach (Row challengeStatsRow in challengeStatsRowSet)
                {
                    string userId = challengeStatsRow.GetValue<string>("user_id");

                    if (!cleanUpList.Contains(userId))
                    {
                        User rankedUser = user.SelectUserBasic(userId, companyId, true, mainSession).User;

                        if (rankedUser != null)
                        {
                            response.TopPersonnels.Add(rankedUser);
                        }

                        cleanUpList.Add(userId);
                    }
                    else
                    {
                        Log.Error(string.Format("UserId: {0} duplicated for user leaderboard by company.", userId));
                        int numberAnsweredCorrectly = challengeStatsRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestamp = challengeStatsRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psChallengeStats = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                        analyticSession.Execute(psChallengeStats.Bind(companyId, numberAnsweredCorrectly, lastUpdatedTimestamp, userId));
                    }

                }

                int limit = Convert.ToInt16(WebConfigurationManager.AppSettings["user_leaderboard_by_company"]);

                if (response.TopPersonnels.Count > limit)
                {
                    response.TopPersonnels = response.TopPersonnels.Take(limit).ToList();
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectFullLeaderboardByCompanyResponse SelectFullLeaderboardByCompany(string adminUserId, string companyId)
        {
            AnalyticsSelectFullLeaderboardByCompanyResponse response = new AnalyticsSelectFullLeaderboardByCompanyResponse();
            response.ScoreChart = new List<TopPersonnelScore>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ValidationHandler vh = new ValidationHandler();

                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_company_sorted",
                   new List<string>(), new List<string> { "company_id" }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(companyId);

                RowSet challengeStatsRowSet = analyticSession.Execute(bChallengeStats);

                User user = new User();
                List<string> cleanUpList = new List<string>();
                foreach (Row challengeStatsRow in challengeStatsRowSet)
                {
                    int numberAnsweredCorrectly = challengeStatsRow.GetValue<int>("number_answered_correctly");
                    string userId = challengeStatsRow.GetValue<string>("user_id");

                    if (!cleanUpList.Contains(userId))
                    {
                        User rankedUser = user.SelectUserBasic(userId, companyId, true, mainSession).User;

                        if (rankedUser != null)
                        {
                            response.ScoreChart.Add(new TopPersonnelScore
                            {
                                User = rankedUser,
                                NumberOfQuestionAnsweredCorrectly = numberAnsweredCorrectly
                            });
                        }

                        cleanUpList.Add(userId);
                    }
                    else
                    {
                        Log.Error(string.Format("UserId: {0} duplicated for user leaderboard by company.", userId));
                        DateTimeOffset lastUpdatedTimestamp = challengeStatsRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psChallengeStats = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                        analyticSession.Execute(psChallengeStats.Bind(companyId, numberAnsweredCorrectly, lastUpdatedTimestamp, userId));
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectLeaderboardByDepartmentResponse SelectLeaderboardByDepartment(string requesterUserId, string companyId)
        {
            AnalyticsSelectLeaderboardByDepartmentResponse response = new AnalyticsSelectLeaderboardByDepartmentResponse();
            response.TopDepartments = new List<Department>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                if (vh.ValidateCompany(companyId, mainSession) == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                bool reload = true;

                while (reload)
                {
                    PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_company_sorted",
                    new List<string>(), new List<string> { "company_id" }));
                    BoundStatement bChallengeStats = psChallengeStats.Bind(companyId);

                    RowSet challengeStatsRowSet = analyticSession.Execute(bChallengeStats);
                    List<Row> challengeStatsRowList = challengeStatsRowSet.ToList();

                    if (challengeStatsRowList.Count == 0)
                    {
                        break;
                    }

                    User user = new User();
                    Department department = new Department();

                    List<string> cleanUpDepartmentList = new List<string>();

                    foreach (Row challengeStatsRow in challengeStatsRowList)
                    {
                        string departmentId = challengeStatsRow.GetValue<string>("department_id");

                        if (!cleanUpDepartmentList.Contains(departmentId))
                        {
                            Department departmentLeaderboard = department.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;

                            if (departmentLeaderboard != null)
                            {
                                departmentLeaderboard.Users = new List<User>();

                                int limit = Int16.Parse(WebConfigurationManager.AppSettings["user_leaderboard_by_department"]);

                                psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("user_leaderboard_by_department_sorted",
                                    new List<string>(), new List<string> { "department_id" }, limit));
                                bChallengeStats = psChallengeStats.Bind(departmentId);
                                RowSet departmentStatsRowSet = analyticSession.Execute(bChallengeStats);

                                List<string> cleanUpUserList = new List<string>();
                                bool isUserInDepartmentDuplicated = false;
                                foreach (Row departmentStatsRow in departmentStatsRowSet)
                                {
                                    string userId = departmentStatsRow.GetValue<string>("user_id");

                                    if (!cleanUpUserList.Contains(userId))
                                    {
                                        User rankedUser = user.SelectUserBasic(userId, companyId, true, mainSession).User;

                                        if (rankedUser != null)
                                        {
                                            departmentLeaderboard.Users.Add(rankedUser);
                                        }

                                        cleanUpUserList.Add(userId);
                                    }
                                    // Clean up
                                    else
                                    {
                                        isUserInDepartmentDuplicated = true;

                                        Log.Error(string.Format("UserId: {0} duplicated for user leaderboard by department: {1}.", userId, departmentId));

                                        int totalNumberAnsweredCorrectly = 0;

                                        // Looping through records
                                        PreparedStatement psUserLeaderboard = analyticSession.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
                                            new List<string>(), new List<string> { "user_id" }));
                                        RowSet userLeaderboardRowSet = analyticSession.Execute(psUserLeaderboard.Bind(userId));

                                        DateTimeOffset? latestTimestamp = null;
                                        foreach (Row userLeaderboardRow in userLeaderboardRowSet)
                                        {
                                            DateTimeOffset lastUpdatedTimestamp = userLeaderboardRow.GetValue<DateTime>("last_updated_timestamp");
                                            int numberAnsweredCorrectly = userLeaderboardRow.GetValue<int>("number_answered_correctly");
                                            if (latestTimestamp == null || (latestTimestamp != null && latestTimestamp.Value < latestTimestamp))
                                            {
                                                latestTimestamp = lastUpdatedTimestamp;
                                            }

                                            totalNumberAnsweredCorrectly += numberAnsweredCorrectly;
                                        }

                                        PreparedStatement psDepartmentByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_department_sorted",
                                            new List<string>(), new List<string> { "department_id" }));
                                        RowSet departmentByUserRowSet = analyticSession.Execute(psDepartmentByUser.Bind(departmentLeaderboard.Id));

                                        BatchStatement batchStatement = new BatchStatement();
                                        foreach (Row departmentByUserRow in departmentByUserRowSet)
                                        {
                                            string departmentUserId = departmentByUserRow.GetValue<string>("user_id");

                                            if (userId.Equals(departmentUserId))
                                            {
                                                DateTimeOffset lastUpdatedTimestamp = departmentByUserRow.GetValue<DateTime>("last_updated_timestamp");
                                                int numberAnsweredCorrectly = departmentByUserRow.GetValue<int>("number_answered_correctly");

                                                psDepartmentByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                                    new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                                batchStatement.Add(psDepartmentByUser.Bind(departmentLeaderboard.Id, numberAnsweredCorrectly, lastUpdatedTimestamp, userId));
                                            }
                                        }

                                        psDepartmentByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_user",
                                                    new List<string> { "user_id" }));
                                        batchStatement.Add(psDepartmentByUser.Bind(userId));

                                        // Remove 
                                        analyticSession.Execute(batchStatement);

                                        batchStatement = new BatchStatement();

                                        psDepartmentByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                                   new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                        batchStatement.Add(psDepartmentByUser.Bind(departmentLeaderboard.Id, totalNumberAnsweredCorrectly, latestTimestamp.Value, userId));

                                        psDepartmentByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_user",
                                            new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                        batchStatement.Add(psDepartmentByUser.Bind(departmentLeaderboard.Id, totalNumberAnsweredCorrectly, latestTimestamp.Value, userId));

                                        // Add back
                                        analyticSession.Execute(batchStatement);

                                        // Recalcuate
                                        RecalculateDepartmentScore(analyticSession, companyId, departmentId);
                                        break;
                                    }


                                }

                                if (isUserInDepartmentDuplicated)
                                {
                                    reload = true;
                                    cleanUpUserList = new List<string>();
                                    response.TopDepartments = new List<Department>();
                                    cleanUpDepartmentList = new List<string>();
                                    break;
                                }

                                response.TopDepartments.Add(departmentLeaderboard);
                                cleanUpDepartmentList.Add(departmentId);
                            }

                            reload = false;
                        }
                        else
                        {
                            reload = true;
                            response.TopDepartments = new List<Department>();

                            Log.Error(string.Format("DepartmentId: {0} duplicated for user leaderboard by department.", departmentId));

                            RecalculateDepartmentScore(analyticSession, companyId, departmentId);

                            break;
                        }

                    }
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectLeaderboardByEventResponse SelectLeaderboardByEvent(string requesterUserId, string companyId, string eventId, int numberOfLeaderboard, int participantType, int scoringType, int calculationType, ISession mainSession, ISession analyticSession)
        {
            AnalyticsSelectLeaderboardByEventResponse response = new AnalyticsSelectLeaderboardByEventResponse();
            response.TopEventLeaderboard = new List<EventLeaderboard>();
            response.OutOfRankingLeaderboard = new List<EventLeaderboard>();
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                User userManager = new User();
                Department departmentManager = new Department();

                if (scoringType == (int)Event.ScoringTypeEnum.TopScorer)
                {
                    PreparedStatement psUserByEvent = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_event_sorted",
                       new List<string>(), new List<string> { "event_id", "is_visible" }));
                    RowSet userByEventRowset = analyticSession.Execute(psUserByEvent.Bind(eventId, true));
                    List<Row> userByEventList = userByEventRowset.ToList();

                    int rank = 1;
                    bool isUserFoundWithinLeaderboard = false;

                    int countDownLimit = userByEventList.Count;

                    for (int index = 0; index < countDownLimit; index++)
                    {
                        Row userByEventRow = userByEventList[index];

                        string userId = userByEventRow.GetValue<string>("user_id");
                        string departmentId = userByEventRow.GetValue<string>("department_id");
                        int score = userByEventRow.GetValue<int>("score");

                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                        Department selectedDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;

                        if (selectedUser != null && selectedDepartment != null)
                        {
                            selectedUser.Departments = new List<Department> { selectedDepartment };
                            EventLeaderboard eventLeaderboard = new EventLeaderboard
                            {
                                User = selectedUser,
                                Score = score,
                                Rank = rank
                            };

                            if (response.TopEventLeaderboard.Count < numberOfLeaderboard)
                            {
                                if (userId.Equals(requesterUserId))
                                {
                                    isUserFoundWithinLeaderboard = true;
                                }

                                response.TopEventLeaderboard.Add(eventLeaderboard);
                            }
                            else
                            {
                                if (isUserFoundWithinLeaderboard)
                                {
                                    break;
                                }

                                // Out of ranking
                                if (userId.Equals(requesterUserId))
                                {
                                    int limitForOutOfRank = Convert.ToInt16(WebConfigurationManager.AppSettings["event_leaderboard_out_of_ranking_limit"]);

                                    int rankForCurrentUser = rank;

                                    int lowerLimit = 0;
                                    int upperLimit = 0;

                                    if (rankForCurrentUser - limitForOutOfRank > numberOfLeaderboard)
                                    {
                                        lowerLimit = rankForCurrentUser - limitForOutOfRank - 1;
                                    }
                                    else
                                    {
                                        lowerLimit = numberOfLeaderboard;
                                    }

                                    rank = lowerLimit + 1;

                                    upperLimit = rankForCurrentUser + limitForOutOfRank < countDownLimit ? rankForCurrentUser + limitForOutOfRank : countDownLimit;

                                    for (int lowerRankIndex = lowerLimit; lowerRankIndex < upperLimit; lowerRankIndex++)
                                    {
                                        userByEventRow = userByEventList[lowerRankIndex];

                                        userId = userByEventRow.GetValue<string>("user_id");
                                        departmentId = userByEventRow.GetValue<string>("department_id");
                                        score = userByEventRow.GetValue<int>("score");

                                        selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                                        selectedDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;

                                        if (selectedUser != null && selectedDepartment != null)
                                        {
                                            eventLeaderboard = new EventLeaderboard
                                            {
                                                User = selectedUser,
                                                Score = score,
                                                Rank = lowerRankIndex + 1
                                            };

                                            response.OutOfRankingLeaderboard.Add(eventLeaderboard);
                                            rank++;
                                        }

                                    }
                                }
                            }

                            rank++;
                        }
                    }



                }
                else if (scoringType == (int)Event.ScoringTypeEnum.TopDepartment)
                {
                    PreparedStatement psUserByEvent = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_event_sorted",
                      new List<string>(), new List<string> { "event_id" }));
                    RowSet departmentByEventRowset = analyticSession.Execute(psUserByEvent.Bind(eventId));
                    List<Row> departmentByEventList = departmentByEventRowset.ToList();

                    int rank = 1;
                    foreach (Row departmentByEventRow in departmentByEventList)
                    {
                        float score = departmentByEventRow.GetValue<float>("score");
                        string departmentId = departmentByEventRow.GetValue<string>("department_id");
                        DateTimeOffset lastUpdatedTimestamp = departmentByEventRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        Department selectedDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;
                        if (selectedDepartment != null)
                        {
                            EventLeaderboard eventLeaderboard = new EventLeaderboard
                            {
                                Department = selectedDepartment,
                                Score = (int)Math.Round(score),
                                Rank = rank
                            };

                            if (response.TopEventLeaderboard.Where(e => e.Department.Id.Equals(selectedDepartment.Id)).FirstOrDefault() == null)
                            {
                                rank++;
                                response.TopEventLeaderboard.Add(eventLeaderboard);
                            }
                            else
                            {
                                // Clean up
                                BatchStatement deleteBatch = new BatchStatement();
                                BatchStatement updateBatch = new BatchStatement();
                                psUserByEvent = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted", new List<string> { "event_id" }));
                                deleteBatch.Add(psUserByEvent.Bind(eventId));

                                psUserByEvent = analyticSession.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("event_leaderboard_by_department",
                                    new List<string>(), new List<string> { "event_id" }));
                                RowSet updateEventRowSet = analyticSession.Execute(psUserByEvent.Bind(eventId));
                                foreach (Row updateEventRow in updateEventRowSet)
                                {
                                    score = departmentByEventRow.GetValue<float>("score");
                                    departmentId = departmentByEventRow.GetValue<string>("department_id");
                                    lastUpdatedTimestamp = departmentByEventRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                                    psUserByEvent = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted", new List<string> { "department_id", "event_id", "score", "last_updated_timestamp" }));
                                    updateBatch.Add(psUserByEvent.Bind(departmentId, eventId, score, lastUpdatedTimestamp));
                                }

                                analyticSession.Execute(deleteBatch);
                                analyticSession.Execute(updateBatch);

                                response = SelectLeaderboardByEvent(requesterUserId, companyId, eventId, numberOfLeaderboard, participantType, scoringType, calculationType, mainSession, analyticSession);
                            }
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void RecalculateDepartmentScore(ISession analyticSession, string companyId, string departmentId)
        {
            try
            {
                PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_department_sorted",
                                   new List<string>(), new List<string> { "department_id" }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(departmentId);
                RowSet departmentStatsRowSet = analyticSession.Execute(bChallengeStats);

                DateTimeOffset? latestDatetime = null;
                int totalNumberOfCorrectAnswers = 0;

                foreach (Row departmentStatsRow in departmentStatsRowSet)
                {
                    string userId = departmentStatsRow.GetValue<string>("user_id");
                    int numberAnswered = departmentStatsRow.GetValue<int>("number_answered_correctly");
                    DateTimeOffset dateTimestamp = departmentStatsRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    totalNumberOfCorrectAnswers += numberAnswered;

                    if (latestDatetime == null)
                    {
                        latestDatetime = dateTimestamp;
                    }
                    else
                    {
                        if (dateTimestamp > latestDatetime.Value)
                        {
                            latestDatetime = dateTimestamp;
                        }
                    }
                }

                psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_company_sorted",
                    new List<string>(), new List<string> { "company_id" }));
                bChallengeStats = psChallengeStats.Bind(companyId);

                RowSet cleanUpRowSet = analyticSession.Execute(bChallengeStats);

                PreparedStatement psUpdate = null;
                BatchStatement bsDelete = new BatchStatement();
                BatchStatement bsInsert = new BatchStatement();

                foreach (Row cleanUpRow in cleanUpRowSet)
                {
                    string cleanUpDepartmentId = cleanUpRow.GetValue<string>("department_id");
                    if (cleanUpDepartmentId.Equals(departmentId))
                    {
                        DateTimeOffset lastUpdatedTimestamp = cleanUpRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        int totalNumberOfCorrect = cleanUpRow.GetValue<int>("number_answered_correctly");
                        psUpdate = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        bsDelete.Add(psUpdate.Bind(companyId, totalNumberOfCorrect, lastUpdatedTimestamp, departmentId));
                    }
                }

                psUpdate = analyticSession.Prepare(CQLGenerator.DeleteStatement("company_leaderboard_by_department",
                            new List<string> { "department_id" }));
                bsDelete.Add(psUpdate.Bind(departmentId));

                analyticSession.Execute(bsDelete);

                if(latestDatetime == null)
                {

                }

                psUpdate = analyticSession.Prepare(CQLGenerator.InsertStatement("company_leaderboard_by_department",
                            new List<string> { "department_id", "company_id", "last_updated_timestamp", "number_answered_correctly" }));
                bsInsert.Add(psUpdate.Bind(departmentId, companyId, latestDatetime, totalNumberOfCorrectAnswers));

                psUpdate = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                    new List<string> { "department_id", "company_id", "last_updated_timestamp", "number_answered_correctly" }));
                bsInsert.Add(psUpdate.Bind(departmentId, companyId, latestDatetime, totalNumberOfCorrectAnswers));

                analyticSession.Execute(bsInsert);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AnalyticsSelectAllGameHistoriesResponse SelectAllGameHistoriesForUser(string requesterUserId,
                                                                                    string companyId,
                                                                                    int numberOfHistoryLoaded,
                                                                                    DateTime? newestTimestamp,
                                                                                    DateTime? oldestTimestamp)
        {
            AnalyticsSelectAllGameHistoriesResponse response = new AnalyticsSelectAllGameHistoriesResponse();
            response.GameHistories = new List<GameHistory>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                int limit = Int16.Parse(WebConfigurationManager.AppSettings["game_history_limit"]);
                //int limit = 0;

                if (numberOfHistoryLoaded > 0)
                {
                    limit += numberOfHistoryLoaded;
                }

                bool isSelectCompleted = false;

                User user = new User();

                PreparedStatement psHistory = null;
                BoundStatement bsHistory = null;

                List<GameHistory> gameHistories = new List<GameHistory>();

                while (isSelectCompleted == false)
                {
                    if (oldestTimestamp != null)
                    {
                        DateTime currentDateTime = oldestTimestamp.Value;
                        psHistory = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_history_by_user_desc",
                            null, new List<string> { "user_id" }, "completed_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        bsHistory = psHistory.Bind(requesterUserId, DateHelper.ConvertDateToLong(currentDateTime));
                    }
                    else
                    {
                        psHistory = session.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_history_by_user_desc",
                            new List<string>(), new List<string> { "user_id" }, limit));
                        bsHistory = psHistory.Bind(requesterUserId);
                    }

                    RowSet completedHistoryRowSet = session.Execute(bsHistory);
                    List<Row> completedHistoryRowList = new List<Row>();
                    completedHistoryRowList = completedHistoryRowSet.GetRows().ToList();

                    foreach (Row completedHistoryRow in completedHistoryRowList)
                    {
                        string challengeId = completedHistoryRow.GetValue<string>("challenge_id");

                        Row challengeRow = vh.ValidateChallenge(challengeId, companyId, session);

                        DateTime? completedOnTimestamp = completedHistoryRow.GetValue<DateTime?>("completed_on_timestamp");
                        oldestTimestamp = completedOnTimestamp;

                        if (challengeRow == null)
                        {
                            continue;
                        }


                        string opponentUserId = completedHistoryRow.GetValue<string>("opponent_user_id");

                        User opponentUser = user.SelectUserBasic(opponentUserId, companyId, false, session).User;

                        psHistory = session.Prepare(CQLGenerator.SelectStatement("challenge_history",
                            new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                        bsHistory = psHistory.Bind(companyId, challengeId, true);
                        Row historyRow = session.Execute(bsHistory).FirstOrDefault();

                        if (historyRow == null)
                        {
                            continue;
                        }

                        string challengeTopicId = historyRow.GetValue<string>("topic_id");
                        string challengeTopicCategoryId = historyRow.GetValue<string>("topic_category_id");

                        Topic challengeTopic = new Topic().SelectTopicBasic(challengeTopicId, null, companyId, challengeTopicCategoryId, null, session).Topic;
                        if (challengeTopic == null)
                        {
                            continue;
                        }

                        string winnerId = historyRow.GetValue<string>("winner_id");
                        int outcome = (int)ResultOutcome.Draw;

                        if (!string.IsNullOrEmpty(winnerId))
                        {
                            if (winnerId.Equals(requesterUserId))
                            {
                                outcome = (int)ResultOutcome.Win;
                            }
                            else
                            {
                                outcome = (int)ResultOutcome.Lost;
                            }
                        }
                        else
                        {
                            if (historyRow["completed_on_timestamp"] == null)
                            {
                                outcome = (int)ResultOutcome.Incompleted;
                            }
                        }

                        GameHistory gameHistory = new GameHistory
                        {
                            Opponent = opponentUser,
                            CompletedOnTimestamp = completedOnTimestamp.Value,
                            ChallengeId = challengeId,
                            Topic = challengeTopic,
                            Outcome = outcome
                        };

                        response.GameHistories.Add(gameHistory);

                        if (gameHistories.Count == limit)
                        {
                            isSelectCompleted = true;
                            break;
                        }
                    }// for

                    if (completedHistoryRowList.Count == 0)
                    {
                        isSelectCompleted = true;
                    }

                }// while

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectGameResultResponse SelectGameResultForChallenge(string requesterUserId,
                                                                              string companyId,
                                                                              string challengeId)
        {
            AnalyticsSelectGameResultResponse response = new AnalyticsSelectGameResultResponse();
            response.Players = new List<User>();
            response.GameResult = new List<ChallengeQuestion>();
            response.Outcome = (int)ResultOutcome.Draw;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row challengeRow = vh.ValidateChallenge(challengeId, companyId, session);
                if (challengeRow == null)
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                    return response;
                }

                List<string> playerIds = challengeRow.GetValue<List<string>>("players_ids");
                if (!playerIds.Contains(requesterUserId))
                {
                    Log.Error("Invalid player for challenge");
                    response.ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalidPlayer);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    return response;
                }

                User user = new User();
                foreach (string playerId in playerIds)
                {
                    User player = user.SelectUserBasic(playerId, companyId, false, session).User;
                    response.Players.Add(player);
                }

                string winnerId = challengeRow["winner_id"] == null ? string.Empty : challengeRow.GetValue<string>("winner_id");
                if (!string.IsNullOrEmpty(winnerId))
                {
                    if (winnerId.Equals(requesterUserId))
                    {
                        response.Outcome = (int)ResultOutcome.Win;
                    }
                    else
                    {
                        response.Outcome = (int)ResultOutcome.Lost;
                    }
                }
                else
                {
                    if (challengeRow["completed_on_timestamp"] == null)
                    {
                        response.Outcome = (int)ResultOutcome.Incompleted;
                    }
                }

                string topicId = challengeRow.GetValue<string>("topic_id");
                string topicCategoryId = challengeRow.GetValue<string>("topic_category_id");

                response.Topic = new Topic().SelectTopicBasic(topicId, null, companyId, topicCategoryId, null, session).Topic;

                PreparedStatement psFullChallengeHistory = session.Prepare(CQLGenerator.SelectStatement("full_challenge_history",
                    new List<string>(), new List<string> { "company_id", "challenge_id" }));
                RowSet fullChallengeHistoryRowSet = session.Execute(psFullChallengeHistory.Bind(companyId, challengeId));

                foreach (Row fullChallengeHistoryRow in fullChallengeHistoryRowSet)
                {
                    ChallengeQuestion question = new ChallengeQuestion();

                    string questionId = fullChallengeHistoryRow.GetValue<string>("question_id");
                    int questionType = fullChallengeHistoryRow.GetValue<int>("type");
                    int choiceType = fullChallengeHistoryRow.GetValue<int>("choice_type");

                    string content = fullChallengeHistoryRow.GetValue<string>("content");
                    string contentImageUrl = fullChallengeHistoryRow.GetValue<string>("content_image_url");
                    string contentImageMd5 = fullChallengeHistoryRow.GetValue<string>("content_image_md5");
                    string contentImageBackgroundColorCode = fullChallengeHistoryRow["content_image_background_color_code"] != null ? fullChallengeHistoryRow.GetValue<string>("content_image_background_color_code") : "#000000";

                    string firstChoiceId = fullChallengeHistoryRow.GetValue<string>("first_choice_id");
                    string firstChoice = fullChallengeHistoryRow.GetValue<string>("first_choice");
                    string firstChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("first_choice_image_url");
                    string firstChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("first_choice_image_md5");

                    string secondChoiceId = fullChallengeHistoryRow.GetValue<string>("second_choice_id");
                    string secondChoice = fullChallengeHistoryRow.GetValue<string>("second_choice");
                    string secondChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("second_choice_image_url");
                    string secondChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("second_choice_image_md5");

                    string thirdChoiceId = fullChallengeHistoryRow.GetValue<string>("third_choice_id");
                    string thirdChoice = fullChallengeHistoryRow.GetValue<string>("third_choice");
                    string thirdChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("third_choice_image_url");
                    string thirdChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("third_choice_image_md5");

                    string fourthChoiceId = fullChallengeHistoryRow.GetValue<string>("fourth_choice_id");
                    string fourthChoice = fullChallengeHistoryRow.GetValue<string>("fourth_choice");
                    string fourthChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("fourth_choice_image_url");
                    string fourthChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("fourth_choice_image_md5");

                    float timeAnswering = fullChallengeHistoryRow.GetValue<float>("time_answering");
                    float timeReading = fullChallengeHistoryRow.GetValue<float>("time_reading");

                    int scoreMultiplier = fullChallengeHistoryRow.GetValue<int>("score_multiplier");

                    string correctAnswer = fullChallengeHistoryRow.GetValue<string>("answer");

                    string initiatedUserAnswer = fullChallengeHistoryRow["answer_of_initiated_user"] == null ? string.Empty : fullChallengeHistoryRow.GetValue<string>("answer_of_initiated_user");
                    string challengedUserAnswer = fullChallengeHistoryRow["answer_of_initiated_user"] == null ? string.Empty : fullChallengeHistoryRow.GetValue<string>("answer_of_challenged_user");

                    float timeTakenByInitiatedUser = fullChallengeHistoryRow["time_taken_by_initiated_user"] == null ? 0 : fullChallengeHistoryRow.GetValue<float>("time_taken_by_initiated_user");
                    float timeTakenByChallengedUser = fullChallengeHistoryRow["time_taken_by_challenged_user"] == null ? 0 : fullChallengeHistoryRow.GetValue<float>("time_taken_by_challenged_user");

                    if (questionType == ChallengeQuestion.QUESTION_TYPE_LONG || questionType == ChallengeQuestion.QUESTION_TYPE_NORMAL)
                    {

                        if (choiceType == ChallengeQuestion.CHOICE_TYPE_TEXT)
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoice = firstChoice,

                                SecondChoiceId = secondChoiceId,
                                SecondChoice = secondChoice,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoice = thirdChoice,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoice = fourthChoice,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                        else
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoiceContentImageUrl = firstChoiceImageUrl,
                                FirstChoiceContentImageMd5 = firstChoiceImageMd5,

                                SecondChoiceId = secondChoiceId,
                                SecondChoiceContentImageUrl = secondChoiceImageUrl,
                                SecondChoiceContentImageMd5 = secondChoiceImageMd5,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoiceContentImageUrl = thirdChoiceImageUrl,
                                ThirdChoiceContentImageMd5 = thirdChoiceImageMd5,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoiceContentImageUrl = fourthChoiceImageUrl,
                                FourthChoiceContentImageMd5 = fourthChoiceImageMd5,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                    }
                    else
                    {
                        if (choiceType == ChallengeQuestion.CHOICE_TYPE_TEXT)
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ContentImageUrl = contentImageUrl,
                                ContentImageMd5 = contentImageMd5,
                                ContentImageBackgroundColorCode = contentImageBackgroundColorCode,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoice = firstChoice,

                                SecondChoiceId = secondChoiceId,
                                SecondChoice = secondChoice,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoice = thirdChoice,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoice = fourthChoice,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                        else
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ContentImageUrl = contentImageUrl,
                                ContentImageMd5 = contentImageMd5,
                                ContentImageBackgroundColorCode = contentImageBackgroundColorCode,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoiceContentImageUrl = firstChoiceImageUrl,
                                FirstChoiceContentImageMd5 = firstChoiceImageMd5,

                                SecondChoiceId = secondChoiceId,
                                SecondChoiceContentImageUrl = secondChoiceImageUrl,
                                SecondChoiceContentImageMd5 = secondChoiceImageMd5,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoiceContentImageUrl = thirdChoiceImageUrl,
                                ThirdChoiceContentImageMd5 = thirdChoiceImageMd5,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoiceContentImageUrl = fourthChoiceImageUrl,
                                FourthChoiceContentImageMd5 = fourthChoiceImageMd5,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                    }

                    response.GameResult.Add(question);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void RemoveFromMatchupTopicAttempt(string topicId, string companyId, ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_topic_attempt_log", new List<string>(), new List<string> { "company_id", "topic_id" }));
                Row attemptRow = analyticSession.Execute(ps.Bind(companyId, topicId)).FirstOrDefault();

                if (attemptRow != null)
                {
                    int correct = attemptRow.GetValue<int>("correct_attempt");
                    int incorrect = attemptRow.GetValue<int>("incorrect_attempt");

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_topic_attempt_log",
                        new List<string> { "company_id", "topic_id" }));
                    deleteBatch.Add(ps.Bind(companyId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_topic_attempt_log_by_correct",
                        new List<string> { "company_id", "topic_id", "correct_attempt" }));
                    deleteBatch.Add(ps.Bind(companyId, topicId, correct));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_topic_attempt_log_by_incorrect",
                        new List<string> { "company_id", "topic_id", "incorrect_attempt" }));
                    deleteBatch.Add(ps.Bind(companyId, topicId, incorrect));

                    analyticSession.Execute(deleteBatch);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromMatchupQuestionAttempt(string topicId, string questionId = null, ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                if (string.IsNullOrEmpty(questionId))
                {
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log",
                        new List<string> { "topic_id" }));
                    deleteBatch.Add(ps.Bind(topicId));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_correct",
                        new List<string> { "topic_id" }));
                    deleteBatch.Add(ps.Bind(topicId));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_incorrect",
                        new List<string> { "topic_id" }));
                    deleteBatch.Add(ps.Bind(topicId));
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_question_attempt_log", new List<string>(), new List<string> { "topic_id", "question_id" }));
                    Row attemptRow = analyticSession.Execute(ps.Bind(topicId, questionId)).FirstOrDefault();

                    if (attemptRow != null)
                    {
                        int correct = attemptRow.GetValue<int>("correct_attempt");
                        int incorrect = attemptRow.GetValue<int>("incorrect_attempt");

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log",
                            new List<string> { "topic_id", "question_id" }));
                        deleteBatch.Add(ps.Bind(topicId, questionId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_correct",
                            new List<string> { "topic_id", "question_id", "correct_attempt" }));
                        deleteBatch.Add(ps.Bind(topicId, questionId, correct));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_incorrect",
                            new List<string> { "topic_id", "question_id", "incorrect_attempt" }));
                        deleteBatch.Add(ps.Bind(topicId, questionId, incorrect));
                    }

                }

                analyticSession.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromMatchupUserAttempt(string topicId, string questionId = null, ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                if (string.IsNullOrEmpty(questionId))
                {
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_user_attempt_by_question",
                        new List<string> { "topic_id" }));
                    deleteBatch.Add(ps.Bind(topicId));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_user_attempt_by_tries",
                        new List<string> { "topic_id" }));
                    deleteBatch.Add(ps.Bind(topicId));
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_user_attempt_by_question", new List<string>(), new List<string> { "topic_id", "question_id" }));
                    RowSet attemptRowset = analyticSession.Execute(ps.Bind(topicId, questionId));

                    foreach (Row attemptRow in attemptRowset)
                    {
                        int tries = attemptRow.GetValue<int>("tries");
                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_user_attempt_by_tries",
                            new List<string> { "topic_id", "tries", "question_id" }));
                        deleteBatch.Add(ps.Bind(topicId, tries, questionId));
                    }
                    analyticSession.Execute(deleteBatch);
                    deleteBatch = new BatchStatement();

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_user_attempt_by_question",
                        new List<string> { "topic_id", "question_id" }));
                    deleteBatch.Add(ps.Bind(topicId, questionId));
                }

                analyticSession.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromMatchupOptionAttempt(string topicId, string questionId = null, ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                if (string.IsNullOrEmpty(questionId))
                {
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_option_attempt_log",
                        new List<string> { "topic_id" }));
                    deleteBatch.Add(ps.Bind(topicId));
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_option_attempt_log",
                        new List<string> { "topic_id", "question_id" }));
                    deleteBatch.Add(ps.Bind(topicId, questionId));
                }

                analyticSession.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }


        public void UpdateMatchUpActivityLog(string challengeId, string userId, string companyId, string topicId, string categoryId, DateTime startTimestamp, ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                DateTime endTimestamp = DateTime.UtcNow;
                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_activity_log", new List<string>(), new List<string> { "challenge_id", "user_id" }));
                Row matchUpRow = analyticSession.Execute(ps.Bind(challengeId, userId)).FirstOrDefault();

                if (matchUpRow == null)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_activity_log",
                        new List<string> { "challenge_id", "user_id", "company_id", "topic_id", "topic_category_id", "start_timestamp", "end_timestamp" }));
                    batchStatement.Add(ps.Bind(challengeId, userId, companyId, topicId, categoryId, startTimestamp, endTimestamp));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_activity_log_by_start_timestamp",
                        new List<string> { "challenge_id", "user_id", "company_id", "topic_id", "topic_category_id", "start_timestamp", "end_timestamp" }));
                    batchStatement.Add(ps.Bind(challengeId, userId, companyId, topicId, categoryId, startTimestamp, endTimestamp));

                    analyticSession.Execute(batchStatement);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public AnalyticSelectMatchUpOverallResponse SelectMatchUpOverall(string companyId, int totalTopic, int totalQuestions, int timeFrame, ISession mainSession = null, ISession analyticSession = null)
        {
            AnalyticSelectMatchUpOverallResponse response = new AnalyticSelectMatchUpOverallResponse();
            response.HeatMap = new AnalyticQuiz.MatchUpHeatmap();
            response.HeatMap.Days = new List<AnalyticQuiz.MatchUpHeatmapPerDay>();
            response.Overview = new MatchUpOverView();
            response.Dau = new MatchUpDau();
            response.Dau.Reports = new List<MatchUpDauPerDay>();
            response.Topics = new List<MatchUpSortedTopic>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                int days = 1;
                switch (timeFrame)
                {
                    case (int)MatchUpImprovementTimeFrame.OneDay:
                        days = 1;
                        break;
                    case (int)MatchUpImprovementTimeFrame.SevenDays:
                        days = 7;
                        break;
                    case (int)MatchUpImprovementTimeFrame.ThirtyDays:
                        days = 30;
                        break;
                }

                // Variables
                List<string> totalUniqueUsers = new List<string>();
                List<string> totalUniqueMatchUp = new List<string>();
                int totalTimeTakenInSec = 0;
                response.Dau.MaxTimeTaken = 0;
                response.Dau.MaxUserCount = 0;

                // Initialized heat map for 7 days
                for (int index = 1; index <= 7; index++)
                {
                    MatchUpHeatmapPerDay heatMapPerDay = new MatchUpHeatmapPerDay
                    {
                        Day = index,
                        DayString = ((DayFrame)index).ToString(),
                        Hours = new List<MatchUpHeatmapPerHour>()
                    };

                    for (int hour = 1; hour <= 24; hour++)
                    {
                        string hourString = (hour - 1).ToString().PadLeft(2, '0') + "00 - " + (hour - 1).ToString().PadLeft(2, '0') + "59";
                        MatchUpHeatmapPerHour hourPerDay = new MatchUpHeatmapPerHour
                        {
                            Hour = hour,
                            HourString = hourString,
                            AttemptCount = 0
                        };

                        heatMapPerDay.Hours.Add(hourPerDay);
                    }

                    response.HeatMap.Days.Add(heatMapPerDay);
                }

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                DateTime currentDate = DateTime.UtcNow.AddHours(timezoneOffset).Date;

                response.HeatMap.MaxCount = -1;
                response.HeatMap.MinCount = -1;

                for (int day = days; day > 0; day--)
                {
                    DateTime startDate = currentDate.AddDays(-day);
                    DateTime endDate = startDate.AddDays(1);

                    List<string> uniqueUsersPerDay = new List<string>();

                    MatchUpDauPerDay dauPerDay = new MatchUpDauPerDay
                    {
                        Date = startDate,
                        DateString = startDate.ToString("dd MMM yyyy"),
                        UserCount = 0,
                        TimeTaken = 0
                    };

                    response.Dau.Reports.Add(dauPerDay);

                    int dayOfWeek = (int)startDate.DayOfWeek;

                    // Sunday is 7
                    if (dayOfWeek == 0)
                    {
                        dayOfWeek = 7;
                    }

                    MatchUpHeatmapPerDay heatMapPerDay = response.HeatMap.Days.FirstOrDefault(mapPerDay => mapPerDay.Day == dayOfWeek);

                    for (int hour = 0; hour < 24; hour++)
                    {
                        DateTime startHour = startDate.AddHours(hour - timezoneOffset);
                        DateTime endHour = startDate.AddHours(hour - timezoneOffset + 1);

                        PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("matchup_activity_log_by_start_timestamp", new List<string>(), new List<string> { "company_id" }, "start_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "start_timestamp", CQLGenerator.Comparison.LessThan, 0));
                        List<Row> hourList = analyticSession.Execute(ps.Bind(companyId, startHour, endHour)).ToList();

                        MatchUpHeatmapPerHour hourPerDay = heatMapPerDay.Hours.FirstOrDefault(h => h.Hour == hour + 1);
                        hourPerDay.AttemptCount += hourList.Count();

                        if (response.HeatMap.MaxCount < hourPerDay.AttemptCount)
                        {
                            response.HeatMap.MaxCount = hourPerDay.AttemptCount;
                        }

                        if (response.HeatMap.MinCount == -1)
                        {
                            response.HeatMap.MinCount = hourPerDay.AttemptCount;
                        }
                        else if (response.HeatMap.MinCount > hourPerDay.AttemptCount)
                        {
                            response.HeatMap.MinCount = hourPerDay.AttemptCount;
                        }

                        // Per hour
                        foreach (Row activityRow in hourList)
                        {
                            DateTimeOffset startTime = activityRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime();
                            DateTimeOffset endTime = activityRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime();
                            string userId = activityRow.GetValue<string>("user_id");
                            string challengeId = activityRow.GetValue<string>("challenge_id");

                            int timeTakenPerDay = endTime.Subtract(startTime).Seconds;
                            dauPerDay.TimeTaken += timeTakenPerDay;

                            totalTimeTakenInSec += timeTakenPerDay;

                            if (!uniqueUsersPerDay.Contains(userId))
                            {
                                uniqueUsersPerDay.Add(userId);
                                dauPerDay.UserCount += 1;
                            }

                            if (!totalUniqueUsers.Contains(userId))
                            {
                                totalUniqueUsers.Add(userId);
                            }

                            if (!totalUniqueMatchUp.Contains(challengeId))
                            {
                                totalUniqueMatchUp.Add(challengeId);
                            }
                        }
                    }

                    dauPerDay.TimeTakenMin = (int)Math.Floor((double)dauPerDay.TimeTaken / 60);

                    dauPerDay.AverageTimeTaken = uniqueUsersPerDay.Count() == 0 ? 0 : dauPerDay.TimeTaken / uniqueUsersPerDay.Count();
                    dauPerDay.AverageTimeTakenMin = (int)Math.Floor((double)dauPerDay.AverageTimeTaken / 60);

                    if (response.Dau.MaxTimeTaken < dauPerDay.AverageTimeTaken)
                    {
                        response.Dau.MaxTimeTaken = dauPerDay.AverageTimeTaken;
                        response.Dau.MaxTimeTakenMin = (int)Math.Ceiling((double)dauPerDay.AverageTimeTaken / 60);
                    }

                    if (response.Dau.MaxUserCount < dauPerDay.UserCount)
                    {
                        response.Dau.MaxUserCount = dauPerDay.UserCount;
                    }
                }

                response.Overview.TotalUniqueUsers = totalUniqueUsers.Count();
                response.Overview.TotalMatchUps = totalUniqueMatchUp.Count();
                response.Overview.TotalQuestions = totalQuestions;
                response.Overview.TotalTopics = totalTopic;
                response.Overview.AverageTopicAttempts = totalUniqueUsers.Count() == 0 ? 0 : (double)totalUniqueMatchUp.Count() / totalUniqueUsers.Count();

                // Using yesterday data
                currentDate = currentDate.AddDays(-1);
                response.Dau.Today = SelectComparableDau(companyId, (int)MatchUpImprovementTimeFrame.OneDay, analyticSession, currentDate);
                response.Dau.LastSevenDays = SelectComparableDau(companyId, (int)MatchUpImprovementTimeFrame.SevenDays, analyticSession, currentDate);
                response.Dau.LastThirtyDays = SelectComparableDau(companyId, (int)MatchUpImprovementTimeFrame.ThirtyDays, analyticSession, currentDate);
                response.Dau.AverageTimeSpent = totalUniqueUsers.Count() != 0 ? totalTimeTakenInSec / totalUniqueUsers.Count() : 0;

                int hourInt = (int)Math.Floor((double)response.Dau.AverageTimeSpent / (60 * 60));
                int minInt = (int)Math.Floor((double)(response.Dau.AverageTimeSpent / 60) % 60);
                int secInt = response.Dau.AverageTimeSpent % 60;
                if (hourInt > 0)
                {
                    response.Dau.AverageTimeSpentString = string.Format("{0}hr {1}min {2}sec", hourInt, minInt, secInt);
                }
                else
                {
                    response.Dau.AverageTimeSpentString = string.Format("{0}min {1}sec", minInt, secInt);
                }


                // Top 5 Topic sorted by difficulty
                response.Topics = SelectTopicAttempts(companyId, (int)TopicDifficultyMetric.Hard, mainSession, 5, analyticSession).Topics;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectTopicAttemptResponse SelectTopicAttempts(string companyId, int metric, ISession mainSession = null, int limit = 0, ISession analyticSession = null)
        {
            AnalyticSelectTopicAttemptResponse response = new AnalyticSelectTopicAttemptResponse();
            response.Topics = new List<MatchUpSortedTopic>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                if (metric == (int)TopicDifficultyMetric.Hard)
                {
                    if (limit > 0)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("matchup_topic_attempt_log_by_incorrect", new List<string>(), new List<string> { "company_id" }, limit));
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_topic_attempt_log_by_incorrect", new List<string>(), new List<string> { "company_id" }));
                    }

                }
                else if (metric == (int)TopicDifficultyMetric.Easy)
                {
                    if (limit > 0)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("matchup_topic_attempt_log_by_correct", new List<string>(), new List<string> { "company_id" }, limit));
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_topic_attempt_log_by_correct", new List<string>(), new List<string> { "company_id" }));
                    }
                }

                RowSet topicRowset = analyticSession.Execute(ps.Bind(companyId));

                Topic topicManager = new Topic();
                foreach (Row topicRow in topicRowset)
                {
                    string topicId = topicRow.GetValue<string>("topic_id");
                    int correct = topicRow.GetValue<int>("correct_attempt");
                    int incorrect = topicRow.GetValue<int>("incorrect_attempt");

                    Topic selectedTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, false).Topic;

                    if (selectedTopic != null)
                    {
                        MatchUpSortedTopic topic = new MatchUpSortedTopic
                        {
                            Topic = selectedTopic,
                            Correct = correct,
                            Incorrect = incorrect
                        };

                        response.Topics.Add(topic);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AnalyticSelectTopicAttemptResponse SelectDailyTopicActivities(string companyId, DateTime datestamp, ISession mainSession = null, ISession analyticSession = null)
        {
            AnalyticSelectTopicAttemptResponse response = new AnalyticSelectTopicAttemptResponse();
            response.Topics = new List<MatchUpSortedTopic>();
            response.MaxCount = 0;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("matchup_activity_log_by_start_timestamp",
                    null, new List<string> { "company_id" }, "start_timestamp", CQLGenerator.Comparison.GreaterThan, "start_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                RowSet dauByUserRowSet = analyticSession.Execute(ps.Bind(companyId, datestamp, datestamp.AddDays(1)));

                Topic topicManager = new Topic();
                foreach (Row dauByUserRow in dauByUserRowSet)
                {
                    string userId = dauByUserRow.GetValue<string>("user_id");
                    string topicId = dauByUserRow.GetValue<string>("topic_id");

                    if (response.Topics.FirstOrDefault(t => t.Topic.TopicId == topicId) == null)
                    {
                        Topic questionTopic = topicManager.SelectTopicBasic(topicId, null, companyId, null, null, mainSession).Topic;

                        if (questionTopic != null)
                        {
                            MatchUpSortedTopic topic = new MatchUpSortedTopic
                            {
                                Topic = questionTopic,
                                TotalUniqueUserIds = new List<string> { userId },
                                TotalAttempts = 1,
                                TotalUniqueUsers = 1
                            };

                            response.Topics.Add(topic);
                        }

                        if (response.MaxCount < 1)
                        {
                            response.MaxCount = 1;
                        }

                    }
                    else
                    {
                        MatchUpSortedTopic selectedTopic = response.Topics.FirstOrDefault(t => t.Topic.TopicId == topicId);

                        if (!selectedTopic.TotalUniqueUserIds.Contains(userId))
                        {
                            selectedTopic.TotalUniqueUserIds.Add(userId);
                            selectedTopic.TotalUniqueUsers++;
                        }

                        selectedTopic.TotalAttempts++;

                        if (response.MaxCount < selectedTopic.TotalAttempts)
                        {
                            response.MaxCount = selectedTopic.TotalAttempts;
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private ComparableMatchUpDau SelectComparableDau(string companyId, int dauComparableTimeFrame, ISession analyticSession, DateTime startDate)
        {
            ComparableMatchUpDau basicDau = new ComparableMatchUpDau();

            try
            {
                PreparedStatement ps = null;
                int numberOfDaysDifference = 0;

                if (dauComparableTimeFrame == (int)MatchUpImprovementTimeFrame.OneDay)
                {
                    numberOfDaysDifference = 1;
                }
                else if (dauComparableTimeFrame == (int)MatchUpImprovementTimeFrame.SevenDays)
                {
                    numberOfDaysDifference = 7;
                }
                else if (dauComparableTimeFrame == (int)MatchUpImprovementTimeFrame.ThirtyDays)
                {
                    numberOfDaysDifference = 30;
                }

                // Last x days
                DateTime previousDate = startDate.AddDays(-numberOfDaysDifference);
                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("matchup_activity_log_by_start_timestamp",
                    null, new List<string> { "company_id" }, "start_timestamp", CQLGenerator.Comparison.GreaterThan, "start_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                RowSet dauByUserRowSet = analyticSession.Execute(ps.Bind(companyId, previousDate, startDate));

                int previousActiveSum = 0;
                List<string> userIdList = new List<string>();

                foreach (Row dauByUserRow in dauByUserRowSet)
                {
                    string userId = dauByUserRow.GetValue<string>("user_id");
                    if (!userIdList.Contains(userId))
                    {
                        userIdList.Add(userId);
                        previousActiveSum++;
                    }
                }

                // Last 2x days
                DateTime lastPreviousDate = previousDate.AddDays(-numberOfDaysDifference);
                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("matchup_activity_log_by_start_timestamp",
                    null, new List<string> { "company_id" }, "start_timestamp", CQLGenerator.Comparison.GreaterThan, "start_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                dauByUserRowSet = analyticSession.Execute(ps.Bind(companyId, lastPreviousDate, previousDate));

                int lastPreviousActiveSum = 0;
                userIdList = new List<string>();

                foreach (Row dauByUserRow in dauByUserRowSet)
                {
                    string userId = dauByUserRow.GetValue<string>("user_id");
                    if (!userIdList.Contains(userId))
                    {
                        userIdList.Add(userId);
                        lastPreviousActiveSum++;
                    }
                }

                double difference = 0;
                double percentage = 0;

                difference = previousActiveSum - lastPreviousActiveSum;

                if (lastPreviousActiveSum != 0)
                {
                    percentage = (difference / lastPreviousActiveSum) * 100;
                }


                basicDau.DifferenceInNumber = (int)difference;
                basicDau.DifferenceInPercentage = Math.Round(percentage, 2);
                basicDau.TotalUniqueUsers = previousActiveSum;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return basicDau;
        }

        public TopicAnalytics SelectBasicAttemptReport(List<Topic> topics, ISession analyticSession = null)
        {
            TopicAnalytics topicAttempt = new TopicAnalytics();
            try
            {
                int firstAttemptCorrect = 0;
                int totalFailedAttempt = 0;
                int secondAttemptCorrect = 0;
                int thirdAboveAttemptCorrect = 0;
                int secondAttemptIncorrect = 0;
                int thirdAboveAttemptIncorrect = 0;

                int maxCorrect = 0;
                int maxIncorrect = 0;

                PreparedStatement ps = null;

                foreach (Topic topic in topics)
                {
                    string topicId = topic.TopicId;
                    for (int tries = 1; tries <= 3; tries++)
                    {
                        int correct = 0;
                        int incorrect = 0;

                        if (tries == 3)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithComparison("matchup_user_attempt_by_tries", new List<string>(), new List<string> { "topic_id", "is_correct" }, "tries", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                            correct = analyticSession.Execute(ps.Bind(topicId, true, 3)).ToList().Count();

                            ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithComparison("matchup_user_attempt_by_tries", new List<string>(), new List<string> { "topic_id", "is_correct" }, "tries", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                            incorrect = analyticSession.Execute(ps.Bind(topicId, false, 3)).ToList().Count();

                            totalFailedAttempt += incorrect;
                            thirdAboveAttemptCorrect += correct;
                            thirdAboveAttemptIncorrect += incorrect;
                        }
                        else
                        {
                            ps = analyticSession.Prepare(CQLGenerator.CountStatement("matchup_user_attempt_by_tries", new List<string> { "topic_id", "tries", "is_correct" }));
                            correct = (int)analyticSession.Execute(ps.Bind(topicId, tries, true)).FirstOrDefault().GetValue<long>("count");

                            ps = analyticSession.Prepare(CQLGenerator.CountStatement("matchup_user_attempt_by_tries", new List<string> { "topic_id", "tries", "is_correct" }));
                            incorrect = (int)analyticSession.Execute(ps.Bind(topicId, tries, false)).FirstOrDefault().GetValue<long>("count");

                            totalFailedAttempt += incorrect;

                            if (tries == 1)
                            {
                                firstAttemptCorrect += correct;
                            }
                            else if (tries == 2)
                            {
                                secondAttemptCorrect += correct;
                                secondAttemptIncorrect += incorrect;
                            }
                        }

                        if (maxCorrect < correct)
                        {
                            maxCorrect = correct;
                        }

                        if (maxIncorrect < incorrect)
                        {
                            maxIncorrect = incorrect;
                        }
                    }
                }


                topicAttempt.FirstAttemptCorrect = firstAttemptCorrect;
                topicAttempt.TotalFailedAttempt = totalFailedAttempt;
                topicAttempt.SecondAttemptCorrect = secondAttemptCorrect;
                topicAttempt.ThirdAboveAttemptCorrect = thirdAboveAttemptCorrect;
                topicAttempt.SecondAttemptIncorrect = secondAttemptIncorrect;
                topicAttempt.ThirdAboveAttemptIncorrect = thirdAboveAttemptIncorrect;
                topicAttempt.MaxCorrect = maxCorrect;
                topicAttempt.MaxIncorrect = maxIncorrect;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return topicAttempt;
        }

        public AnalyticSelectTopicOverallResponse SelectTopicOverall(Topic topic, string companyId, int totalTargetedAudience, ISession mainSession = null, ISession analyticSession = null)
        {
            AnalyticSelectTopicOverallResponse response = new AnalyticSelectTopicOverallResponse();
            response.Analytics = new TopicAnalytics();
            response.Analytics.Attempts = new List<TopicAttempt>();
            response.QuestionAttempt = new QuestionAttempt();
            response.TopicSummary = new TopicSummary();
            response.Questions = new List<MatchUpSortedQuestion>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                // Getting summary
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_activity_log_by_start_timestamp", new List<string>(), new List<string> { "company_id", "topic_id" }));
                RowSet challengeRowset = analyticSession.Execute(ps.Bind(companyId, topic.TopicId));

                List<string> uniqueChallenges = new List<string>();
                List<string> uniqueUsers = new List<string>();
                foreach (Row challengeRow in challengeRowset)
                {
                    string challengeId = challengeRow.GetValue<string>("challenge_id");
                    string userId = challengeRow.GetValue<string>("user_id");

                    if (!uniqueChallenges.Contains(challengeId))
                    {
                        uniqueChallenges.Add(challengeId);
                    }

                    if (!uniqueUsers.Contains(userId))
                    {
                        uniqueUsers.Add(userId);
                    }
                }

                response.TopicSummary.Topic = topic;
                response.TopicSummary.TotalTargetedAudience = totalTargetedAudience;
                response.TopicSummary.TotalUserEngaged = uniqueUsers.Count();
                response.TopicSummary.TotalMatchUp = uniqueChallenges.Count();
                response.TopicSummary.AverageMatchUpEngaged = (double)response.TopicSummary.TotalMatchUp / response.TopicSummary.TotalUserEngaged;

                // Getting question attempts
                ChallengeQuestion questionManager = new ChallengeQuestion();
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_question_attempt_log", new List<string>(), new List<string> { "topic_id" }));
                RowSet questionRowset = analyticSession.Execute(ps.Bind(topic.TopicId));

                int totalCorrect = 0;
                int totalIncorrect = 0;

                foreach (Row questionRow in questionRowset)
                {
                    string questionId = questionRow.GetValue<string>("question_id");
                    ChallengeQuestion selectedQuestion = questionManager.SelectQuestion(questionId, topic.TopicId, companyId, null, mainSession).Question;

                    if (selectedQuestion != null)
                    {
                        int correct = questionRow.GetValue<int>("correct_attempt");
                        int incorrect = questionRow.GetValue<int>("incorrect_attempt");

                        totalCorrect += correct;
                        totalIncorrect += incorrect;
                    }
                }
                response.QuestionAttempt.Correct = totalCorrect;
                response.QuestionAttempt.Incorrect = totalIncorrect;

                // Getting analytics
                int firstAttemptCorrect = 0;
                int totalFailedAttempt = 0;
                int secondAttemptCorrect = 0;
                int thirdAboveAttemptCorrect = 0;
                int secondAttemptIncorrect = 0;
                int thirdAboveAttemptIncorrect = 0;

                int maxCorrect = 0;
                int maxIncorrect = 0;

                for (int tries = 1; tries <= 6; tries++)
                {
                    string attemptString = string.Empty;

                    int correct = 0;
                    int incorrect = 0;

                    if (tries == 6)
                    {
                        attemptString = string.Format("{0}th & above attempt", tries);

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithComparison("matchup_user_attempt_by_tries", new List<string>(), new List<string> { "topic_id", "is_correct" }, "tries", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                        correct = analyticSession.Execute(ps.Bind(topic.TopicId, true, 6)).ToList().Count();

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithComparison("matchup_user_attempt_by_tries", new List<string>(), new List<string> { "topic_id", "is_correct" }, "tries", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                        incorrect = analyticSession.Execute(ps.Bind(topic.TopicId, false, 6)).ToList().Count();

                        totalFailedAttempt += incorrect;
                        thirdAboveAttemptCorrect += correct;
                        thirdAboveAttemptIncorrect += incorrect;
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("matchup_user_attempt_by_tries", new List<string> { "topic_id", "tries", "is_correct" }));
                        correct = (int)analyticSession.Execute(ps.Bind(topic.TopicId, tries, true)).FirstOrDefault().GetValue<long>("count");

                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("matchup_user_attempt_by_tries", new List<string> { "topic_id", "tries", "is_correct" }));
                        incorrect = (int)analyticSession.Execute(ps.Bind(topic.TopicId, tries, false)).FirstOrDefault().GetValue<long>("count");

                        totalFailedAttempt += incorrect;

                        if (tries == 1)
                        {
                            attemptString = string.Format("1st attempt");
                            firstAttemptCorrect = correct;
                        }
                        else if (tries == 2)
                        {
                            attemptString = string.Format("2nd attempt");
                            secondAttemptCorrect = correct;
                            secondAttemptIncorrect = incorrect;
                        }
                        else if (tries == 3)
                        {
                            attemptString = string.Format("3rd attempt");
                            thirdAboveAttemptCorrect += correct;
                            thirdAboveAttemptIncorrect += incorrect;
                        }
                        else if (tries <= 5)
                        {
                            attemptString = string.Format("{0}th attempt", tries);
                            thirdAboveAttemptCorrect += correct;
                            thirdAboveAttemptIncorrect += incorrect;
                        }
                    }

                    if (maxCorrect < correct)
                    {
                        maxCorrect = correct;
                    }

                    if (maxIncorrect < incorrect)
                    {
                        maxIncorrect = incorrect;
                    }

                    TopicAttempt attempt = new TopicAttempt
                    {
                        Attempt = attemptString,
                        Correct = correct,
                        Incorrect = incorrect
                    };

                    response.Analytics.Attempts.Add(attempt);
                }

                response.Analytics.FirstAttemptCorrect = firstAttemptCorrect;
                response.Analytics.TotalFailedAttempt = totalFailedAttempt;
                response.Analytics.SecondAttemptCorrect = secondAttemptCorrect;
                response.Analytics.ThirdAboveAttemptCorrect = thirdAboveAttemptCorrect;
                response.Analytics.SecondAttemptIncorrect = secondAttemptIncorrect;
                response.Analytics.ThirdAboveAttemptIncorrect = thirdAboveAttemptIncorrect;
                response.Analytics.MaxCorrect = maxCorrect;
                response.Analytics.MaxIncorrect = maxIncorrect;

                // Getting question attempts
                response.Questions = SelectQuestionAttempts(topic.TopicId, companyId, (int)TopicDifficultyMetric.Hard, mainSession, 5, analyticSession).Questions;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AnalyticSelectQuestionAttemptResponse SelectQuestionAttempts(string topicId, string companyId, int metric, ISession mainSession = null, int limit = 0, ISession analyticSession = null)
        {
            AnalyticSelectQuestionAttemptResponse response = new AnalyticSelectQuestionAttemptResponse();
            response.Questions = new List<MatchUpSortedQuestion>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                if (metric == (int)TopicDifficultyMetric.Hard)
                {
                    if (limit > 0)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("matchup_question_attempt_log_by_incorrect", new List<string>(), new List<string> { "topic_id" }, limit));
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_question_attempt_log_by_incorrect", new List<string>(), new List<string> { "topic_id" }));
                    }

                }
                else if (metric == (int)TopicDifficultyMetric.Easy)
                {
                    if (limit > 0)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("matchup_question_attempt_log_by_correct", new List<string>(), new List<string> { "topic_id" }, limit));
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_question_attempt_log_by_correct", new List<string>(), new List<string> { "topic_id" }));
                    }
                }

                RowSet questionRowset = analyticSession.Execute(ps.Bind(topicId));

                ChallengeQuestion questionManager = new ChallengeQuestion();
                foreach (Row questionRow in questionRowset)
                {
                    string questionId = questionRow.GetValue<string>("question_id");
                    int correct = questionRow.GetValue<int>("correct_attempt");
                    int incorrect = questionRow.GetValue<int>("incorrect_attempt");

                    ChallengeQuestion selectedQuestion = questionManager.SelectQuestion(questionId, topicId, companyId, null, mainSession).Question;

                    if (selectedQuestion != null)
                    {
                        MatchUpSortedQuestion question = new MatchUpSortedQuestion
                        {
                            Question = selectedQuestion,
                            Correct = correct,
                            Incorrect = incorrect
                        };

                        response.Questions.Add(question);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AnalyticSelectOptionAttemptResponse SelectOptionAttempts(string topicId, ChallengeQuestion question, ISession mainSession = null, ISession analyticSession = null)
        {
            AnalyticSelectOptionAttemptResponse response = new AnalyticSelectOptionAttemptResponse();
            response.Question = new MatchUpSortedQuestion();
            response.Options = new AnalyticOptionDetail();
            response.Options.Options = new List<AnalyticOption>();
            response.Analytics = new TopicAnalytics();
            response.Analytics.Attempts = new List<TopicAttempt>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                AnalyticOption option = new AnalyticOption
                {
                    OptionId = question.FirstChoiceId,
                    Content = question.FirstChoice,
                    ContentImageUrl = question.FirstChoiceContentImageUrl,
                    SelectedCount = 0
                };
                response.Options.Options.Add(option);

                option = new AnalyticOption
                {
                    OptionId = question.SecondChoiceId,
                    Content = question.SecondChoice,
                    ContentImageUrl = question.SecondChoiceContentImageUrl,
                    SelectedCount = 0
                };
                response.Options.Options.Add(option);

                option = new AnalyticOption
                {
                    OptionId = question.ThirdChoiceId,
                    Content = question.ThirdChoice,
                    ContentImageUrl = question.ThirdChoiceContentImageUrl,
                    SelectedCount = 0
                };
                response.Options.Options.Add(option);

                option = new AnalyticOption
                {
                    OptionId = question.FourthChoiceId,
                    Content = question.FourthChoice,
                    ContentImageUrl = question.FourthChoiceContentImageUrl,
                    SelectedCount = 0
                };
                response.Options.Options.Add(option);

                PreparedStatement ps = null;

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_option_attempt_log", new List<string>(), new List<string> { "topic_id", "question_id" }));
                RowSet attemptRowset = analyticSession.Execute(ps.Bind(topicId, question.Id));

                int maxCount = 0;
                foreach (Row attemptRow in attemptRowset)
                {
                    string optionId = attemptRow.GetValue<string>("option_id");
                    int attempt = attemptRow.GetValue<int>("attempt");

                    if (response.Options.Options.FirstOrDefault(o => o.OptionId == optionId) != null)
                    {
                        AnalyticOption selectedOption = response.Options.Options.FirstOrDefault(o => o.OptionId == optionId);
                        selectedOption.SelectedCount = attempt;

                        if (maxCount < attempt)
                        {
                            maxCount = attempt;
                        }
                    }
                }

                response.Options.MaxCount = maxCount;

                // Getting question attempts
                response.Question.Question = question;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("matchup_question_attempt_log", new List<string>(), new List<string> { "topic_id", "question_id" }));
                Row questionRow = analyticSession.Execute(ps.Bind(topicId, question.Id)).FirstOrDefault();
                response.Question.Correct = questionRow.GetValue<int>("correct_attempt");
                response.Question.Incorrect = questionRow.GetValue<int>("incorrect_attempt");

                // Getting analytics
                int totalCorrect = 0;
                int firstAttemptCorrect = 0;
                int totalFailedAttempt = 0;
                int secondAttemptCorrect = 0;
                int thirdAboveAttemptCorrect = 0;
                int secondAttemptIncorrect = 0;
                int thirdAboveAttemptIncorrect = 0;

                int maxCorrect = 0;
                int maxIncorrect = 0;

                for (int tries = 1; tries <= 4; tries++)
                {
                    string attemptString = string.Empty;

                    int correct = 0;
                    int incorrect = 0;

                    if (tries == 4)
                    {
                        attemptString = string.Format("{0}th & above attempt", tries);

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithComparison("matchup_user_attempt_by_tries", new List<string>(), new List<string> { "topic_id", "is_correct" }, "tries", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                        List<Row> correctRows = analyticSession.Execute(ps.Bind(topicId, true, 4)).ToList();
                        correct = correctRows.Where(r => r.GetValue<string>("question_id").Equals(question.Id)).ToList().Count();

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithComparison("matchup_user_attempt_by_tries", new List<string>(), new List<string> { "topic_id", "is_correct" }, "tries", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                        List<Row> incorrectRows = analyticSession.Execute(ps.Bind(topicId, false, 4)).ToList();
                        incorrect = incorrectRows.Where(r => r.GetValue<string>("question_id").Equals(question.Id)).ToList().Count();

                        totalFailedAttempt += incorrect;
                        thirdAboveAttemptCorrect += correct;
                        thirdAboveAttemptIncorrect += incorrect;
                        totalCorrect += correct;
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("matchup_user_attempt_by_tries", new List<string> { "topic_id", "tries", "question_id", "is_correct" }));
                        correct = (int)analyticSession.Execute(ps.Bind(topicId, tries, question.Id, true)).FirstOrDefault().GetValue<long>("count");

                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("matchup_user_attempt_by_tries", new List<string> { "topic_id", "tries", "question_id", "is_correct" }));
                        incorrect = (int)analyticSession.Execute(ps.Bind(topicId, tries, question.Id, false)).FirstOrDefault().GetValue<long>("count");

                        totalFailedAttempt += incorrect;
                        totalCorrect += correct;

                        if (tries == 1)
                        {
                            attemptString = string.Format("1st attempt");
                            firstAttemptCorrect = correct;
                        }
                        else if (tries == 2)
                        {
                            attemptString = string.Format("2nd attempt");
                            secondAttemptCorrect = correct;
                            secondAttemptIncorrect = incorrect;
                        }
                        else if (tries == 3)
                        {
                            attemptString = string.Format("3rd attempt");
                            thirdAboveAttemptCorrect += correct;
                            thirdAboveAttemptIncorrect += incorrect;
                        }
                    }

                    if (maxCorrect < correct)
                    {
                        maxCorrect = correct;
                    }

                    if (maxIncorrect < incorrect)
                    {
                        maxIncorrect = incorrect;
                    }

                    TopicAttempt attempt = new TopicAttempt
                    {
                        Attempt = attemptString,
                        Correct = correct,
                        Incorrect = incorrect
                    };

                    response.Analytics.Attempts.Add(attempt);
                }

                response.Analytics.FirstAttemptCorrect = firstAttemptCorrect;
                response.Analytics.TotalFailedAttempt = totalFailedAttempt;
                response.Analytics.SecondAttemptCorrect = secondAttemptCorrect;
                response.Analytics.ThirdAboveAttemptCorrect = thirdAboveAttemptCorrect;
                response.Analytics.SecondAttemptIncorrect = secondAttemptIncorrect;
                response.Analytics.ThirdAboveAttemptIncorrect = thirdAboveAttemptIncorrect;
                response.Analytics.MaxCorrect = maxCorrect;
                response.Analytics.MaxIncorrect = maxIncorrect;

                // Auto correct
                if (response.Question.Correct != totalCorrect)
                {
                    response.Question.Correct = totalCorrect;
                }

                if (response.Options.Options[0].SelectedCount != totalCorrect)
                {
                    response.Options.Options[0].SelectedCount = totalCorrect;
                }


                //if(response.Question.Correct != response.Options.Options[0].SelectedCount)
                //{
                //    BatchStatement updateBatch = new BatchStatement();
                //    BatchStatement deleteBatch = new BatchStatement();

                //    string questionId = question.Id;
                //    int correctAttempt = response.Options.Options[0].SelectedCount;

                //    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("matchup_question_attempt_log",
                //                           new List<string> { "topic_id", "question_id" }, new List<string> { "correct_attempt" }, new List<string>()));
                //    updateBatch.Add(ps.Bind(correctAttempt, topicId, questionId));

                //    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_correct",
                //        new List<string> { "topic_id", "question_id", "correct_attempt" }));
                //    deleteBatch.Add(ps.Bind(topicId, questionId, correctAttempt));

                //    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("matchup_question_attempt_log_by_incorrect",
                //        new List<string> { "topic_id", "question_id", "incorrect_attempt" }));
                //    deleteBatch.Add(ps.Bind(topicId, questionId, response.Question.Incorrect));

                //    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_correct",
                //        new List<string> { "topic_id", "question_id", "correct_attempt", "incorrect_attempt" }));
                //    updateBatch.Add(ps.Bind(topicId, questionId, correctAttempt, response.Question.Incorrect));

                //    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("matchup_question_attempt_log_by_incorrect",
                //        new List<string> { "topic_id", "question_id", "correct_attempt", "incorrect_attempt" }));
                //    updateBatch.Add(ps.Bind(topicId, questionId, correctAttempt, response.Question.Incorrect));

                //    analyticSession.Execute(deleteBatch);
                //    analyticSession.Execute(updateBatch);

                //    response.Question.Correct = response.Options.Options[0].SelectedCount;
                //}

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public void ResetExperiencePoints(string adminUserId, string companyId, List<string> userIds, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;

                foreach (string userId in userIds)
                {
                    BatchStatement deleteBatch = new BatchStatement();
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("experience_allocation_by_user", new List<string>(), new List<string> { "user_id" }));
                    RowSet expUserRowset = analyticSession.Execute(ps.Bind(userId));

                    foreach (Row expUserRow in expUserRowset)
                    {
                        string allocationId = expUserRow.GetValue<string>("allocation_id");
                        DateTimeOffset timestamp = expUserRow.GetValue<DateTimeOffset>("allocated_on_timestamp");
                        string questionId = expUserRow.GetValue<string>("question_id");
                        string topicId = expUserRow.GetValue<string>("topic_id");

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("experience_allocation_by_topic", new List<string> { "topic_id", "allocation_id" }));
                        deleteBatch.Add(ps.Bind(topicId, allocationId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("experience_allocation_by_topic_timestamp", new List<string> { "topic_id", "allocated_on_timestamp", "allocation_id" }));
                        deleteBatch.Add(ps.Bind(topicId, timestamp, allocationId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("experience_allocation_by_question", new List<string> { "question_id", "allocation_id" }));
                        deleteBatch.Add(ps.Bind(questionId, allocationId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("experience_allocation_by_question_timestamp", new List<string> { "question_id", "allocated_on_timestamp", "allocation_id" }));
                        deleteBatch.Add(ps.Bind(questionId, timestamp, allocationId));
                    }

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("experience_allocation_by_user_timestamp", new List<string> { "user_id" }));
                    deleteBatch.Add(ps.Bind(userId));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("experience_allocation_by_user", new List<string> { "user_id" }));
                    deleteBatch.Add(ps.Bind(userId));

                    analyticSession.Execute(deleteBatch);

                    // Cannot delete from counter table (same key cannot add back to existing table)
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("experience_by_user", new List<string>(), new List<string> { "user_id" }));
                    Row expByUserRow = analyticSession.Execute(ps.Bind(userId)).FirstOrDefault();
                    if(expByUserRow != null)
                    {
                        int totalExp = (int)expByUserRow.GetValue<long>("exp");
                        ps = analyticSession.Prepare(CQLGenerator.UpdateCounterStatement("experience_by_user", new List<string> { "user_id" }, new List<string> { "exp" }, new List<int> { -totalExp }));
                        analyticSession.Execute(ps.Bind(userId));
                    }
                }

                // Update history table
                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("gamification_log",
                    new List<string> { "company_id" }, new List<string> { "last_resetted_exp_timestamp", "last_exp_reset_by_admin" }, new List<string>()));
                mainSession.Execute(ps.Bind(DateTime.UtcNow, adminUserId, companyId));

                Log.Debug(string.Format("Experience reset for company: {0} done", companyId));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void ResetUserChallengeStats(string companyId, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;

                // Leaderboard
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("challenge_total_stats_counter_by_user", new List<string>(), new List<string> { "company_id" }));
                RowSet challengeStatsRowSet = analyticSession.Execute(ps.Bind(companyId));
                foreach(Row challengeStatsRow in challengeStatsRowSet)
                {
                    string userId = challengeStatsRow.GetValue<string>("user_id");
                    int draws = challengeStatsRow.IsNull("total_draws") ? 0 : (int)challengeStatsRow.GetValue<long>("total_draws");
                    int loses = challengeStatsRow.IsNull("total_losses") ? 0 : (int)challengeStatsRow.GetValue<long>("total_losses");
                    int networkError = challengeStatsRow.IsNull("total_network_errors") ? 0 : (int)challengeStatsRow.GetValue<long>("total_network_errors");
                    int surrenders = challengeStatsRow.IsNull("total_surrenders") ? 0 : (int)challengeStatsRow.GetValue<long>("total_surrenders");
                    int win = challengeStatsRow.IsNull("total_win") ? 0 : (int)challengeStatsRow.GetValue<long>("total_win");
                    int totalPlays = challengeStatsRow.IsNull("total_games_played") ? 0 : (int)challengeStatsRow.GetValue<long>("total_games_played");
                    ps = analyticSession.Prepare(CQLGenerator.UpdateCounterStatement("challenge_total_stats_counter_by_user", new List<string> { "company_id", "user_id" },
                        new List<string> { "total_draws", "total_losses", "total_network_errors", "total_surrenders", "total_win", "total_games_played" }, new List<int> { -draws, -loses, -networkError, -surrenders, -win, -totalPlays }));
                    analyticSession.Execute(ps.Bind(companyId, userId));
                }

                Log.Debug(string.Format("User challenge stats reset for company: {0} done", companyId));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void ResetLeaderboard(string adminUserId, string companyId, List<string> topicIds, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }


                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                DateTime currentTime = DateTime.UtcNow;

                foreach (string topicId in topicIds)
                {
                    // Leaderboard
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted", new List<string>(), new List<string> { "topic_id" }));
                    RowSet leaderboardTopicRowset = analyticSession.Execute(ps.Bind(topicId));

                    foreach (Row leaderboardTopicRow in leaderboardTopicRowset)
                    {
                        int correctForTopic = leaderboardTopicRow.GetValue<int>("number_answered_correctly");
                        string userId = leaderboardTopicRow.GetValue<string>("user_id");
                        DateTimeOffset lastTopicUpdatedTimestamp = leaderboardTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        // Topic leaderboard
                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user", new List<string> { "topic_id", "user_id" }));
                        deleteBatch.Add(ps.Bind(topicId, userId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted", new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteBatch.Add(ps.Bind(userId, correctForTopic, lastTopicUpdatedTimestamp, topicId));

                        // Company leaderboard
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user", new List<string>(), new List<string> { "user_id" }));
                        Row userLeaderboardRow = analyticSession.Execute(ps.Bind(userId)).FirstOrDefault();

                        if (userLeaderboardRow != null)
                        {
                            int totalCorrect = userLeaderboardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastCompanyUpdatedTimestamp = userLeaderboardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted", new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                            deleteBatch.Add(ps.Bind(companyId, totalCorrect, lastCompanyUpdatedTimestamp, userId));

                            if (totalCorrect - correctForTopic > 0)
                            {
                                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user", new List<string> { "user_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateBatch.Add(ps.Bind(totalCorrect - correctForTopic, currentTime, userId));

                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted", new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                updateBatch.Add(ps.Bind(companyId, totalCorrect - correctForTopic, currentTime, userId));
                            }
                            else
                            {
                                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user", new List<string> { "user_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                updateBatch.Add(ps.Bind(0, currentTime, userId));
                            }

                            // Department leaderboard
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user", new List<string>(), new List<string> { "user_id" }));
                            Row departmentUserLeaderboardRow = analyticSession.Execute(ps.Bind(userId)).FirstOrDefault();
                            if (departmentUserLeaderboardRow != null)
                            {
                                // Department by user
                                string departmentId = departmentUserLeaderboardRow.GetValue<string>("department_id");
                                DateTimeOffset lastDepartmentUserUpdatedTimestamp = departmentUserLeaderboardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                                int departmentCorrect = userLeaderboardRow.GetValue<int>("number_answered_correctly");

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted", new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                deleteBatch.Add(ps.Bind(departmentId, departmentCorrect, lastDepartmentUserUpdatedTimestamp, userId));

                                if (departmentCorrect - correctForTopic > 0)
                                {
                                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user", new List<string> { "user_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                    updateBatch.Add(ps.Bind(departmentCorrect - correctForTopic, currentTime, userId));

                                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted", new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                    updateBatch.Add(ps.Bind(departmentId, departmentCorrect - correctForTopic, currentTime, userId));
                                }
                                else
                                {
                                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user", new List<string> { "user_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                    updateBatch.Add(ps.Bind(0, currentTime, userId));
                                }

                                // Department by company
                                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department", new List<string>(), new List<string> { "department_id" }));
                                Row departmentCompanyLeaderboardRow = analyticSession.Execute(ps.Bind(departmentId)).FirstOrDefault();
                                DateTimeOffset lastCompanyUserUpdatedTimestamp = departmentCompanyLeaderboardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                                int companyCorrect = departmentCompanyLeaderboardRow.GetValue<int>("number_answered_correctly");

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted", new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                deleteBatch.Add(ps.Bind(companyId, companyCorrect, lastCompanyUserUpdatedTimestamp, departmentId));

                                if (companyCorrect - correctForTopic > 0)
                                {
                                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department", new List<string> { "department_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                    deleteBatch.Add(ps.Bind(companyCorrect - correctForTopic, currentTime, departmentId));

                                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted", new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                    updateBatch.Add(ps.Bind(companyId, companyCorrect - correctForTopic, currentTime, departmentId));
                                }
                                else
                                {
                                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department", new List<string> { "department_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                                    deleteBatch.Add(ps.Bind(0, currentTime, departmentId));
                                }
                            }
                        }
                    }

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted", new List<string> { "topic_id" }));
                    deleteBatch.Add(ps.Bind(topicId));

                    analyticSession.Execute(deleteBatch);
                    analyticSession.Execute(updateBatch);

                    deleteBatch = new BatchStatement();
                    updateBatch = new BatchStatement();
                }

                // Update history table
                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("gamification_log",
                    new List<string> { "company_id" }, new List<string> { "last_resetted_leaderboard_timestamp", "last_leaderboard_reset_by_admin" }, new List<string>()));
                updateBatch.Add(ps.Bind(currentTime, adminUserId, companyId));

                mainSession.Execute(updateBatch);

                Log.Debug(string.Format("Leaderboard reset for company: {0} done", companyId));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AnalyticSelectEventResultResponse SelectEventResult(string companyId, Event selectedEvent, double timezoneOffset, ISession mainSession, ISession analyticSession)
        {
            AnalyticSelectEventResultResponse response = new AnalyticSelectEventResultResponse();
            response.Event = new Event();
            response.Score = new List<AnalyticEventUserScore>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_event_sorted", new List<string>(), new List<string> { "event_id" }));
                RowSet leaderboardTopicRowset = analyticSession.Execute(ps.Bind(selectedEvent.EventId));

                User userManager = new User();
                foreach (Row leaderboardTopicRow in leaderboardTopicRowset)
                {
                    int score = leaderboardTopicRow.GetValue<int>("score");
                    DateTime lastUpdatedTimestamp = leaderboardTopicRow.GetValue<DateTime>("last_updated_timestamp");
                    lastUpdatedTimestamp = lastUpdatedTimestamp.AddHours(timezoneOffset);
                    string userId = leaderboardTopicRow.GetValue<string>("user_id");

                    User scoredUser = userManager.SelectUserBasic(userId, companyId, false, mainSession, null, true).User;
                    if (scoredUser != null)
                    {
                        AnalyticEventUserScore scoring = new AnalyticEventUserScore
                        {
                            User = scoredUser,
                            Score = score,
                            Timestamp = lastUpdatedTimestamp,
                            TimestampString = lastUpdatedTimestamp.ToString("dd-MM-yyyy HH:mm:ss")
                        };

                        response.Score.Add(scoring);
                    }

                }

                response.Event = selectedEvent;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AnalyticMatchUpBreakdownPersonnelReport SelectMatchUpBreakdownPersonnelReport(string adminUserId, string companyId, ISession mainSession = null, ISession analyticsSession = null)
        {
            AnalyticMatchUpBreakdownPersonnelReport response = new AnalyticMatchUpBreakdownPersonnelReport();
            response.Reports = new List<MatchUpPersonnelBreakdownReport>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                if(mainSession == null)
                {
                    mainSession = cm.getMainSession();
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }
                
                if(analyticsSession == null)
                {
                    analyticsSession = cm.getAnalyticSession();
                }
                
                Topic topicManager = new Topic();
                List<Topic> topics = topicManager.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, null, null, null, false, true, mainSession).Topics;

                User userManager = new User();
                PreparedStatement ps = null;
                foreach(Topic topic in topics)
                {
                    ps = analyticsSession.Prepare(CQLGenerator.SelectStatement("matchup_user_attempt_by_tries",
                           new List<string>(), new List<string> { "topic_id" }));
                    RowSet triesRowset = analyticsSession.Execute(ps.Bind(topic.TopicId));

                    foreach(Row triesRow in triesRowset)
                    {
                        int tries = triesRow.GetValue<int>("tries");
                        string userId = triesRow.GetValue<string>("user_id");

                        MatchUpPersonnelBreakdownReport selectedReport = response.Reports.FirstOrDefault(r => r.User.UserId.Equals(userId) && r.Topic.TopicId.Equals(topic.TopicId));
                        if (selectedReport == null)
                        {
                            User selectedUser = userManager.SelectUserBasic(userId, companyId, false, mainSession).User;
                            if(selectedUser != null)
                            {
                                response.Reports.Add(new MatchUpPersonnelBreakdownReport
                                {
                                    User = selectedUser,
                                    Topic = topic,
                                    PlayedTimes = tries
                                });
                            }
                        }
                        else
                        {
                            if (selectedReport.PlayedTimes < tries)
                            {
                                selectedReport.PlayedTimes = tries;
                            }
                        }
                    }
                }

                response.Reports = response.Reports.OrderBy(r => r.User.FirstName).ThenBy(r => r.User.LastName).ThenBy(r => r.User.UserId).ThenByDescending(r => r.PlayedTimes).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticMatchUpGroupPersonnelReport SelectMatchUpGroupPersonnelReport(string adminUserId, string companyId)
        {
            AnalyticMatchUpGroupPersonnelReport response = new AnalyticMatchUpGroupPersonnelReport();
            response.Reports = new List<MatchUpPersonnelGroupByReport>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticsSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                AnalyticMatchUpBreakdownPersonnelReport breakdownResponse = SelectMatchUpBreakdownPersonnelReport(adminUserId, companyId, mainSession, analyticsSession);
                if(breakdownResponse.Success)
                {
                    foreach (MatchUpPersonnelBreakdownReport report in breakdownResponse.Reports)
                    {
                        MatchUpPersonnelGroupByReport selectedReport = response.Reports.FirstOrDefault(r => r.User.UserId.Equals(report.User.UserId));
                        if (selectedReport == null)
                        {
                            response.Reports.Add(new MatchUpPersonnelGroupByReport
                            {
                                User = report.User,
                                PlayedTimes = report.PlayedTimes
                            });
                        }
                        else
                        {
                            selectedReport.PlayedTimes += report.PlayedTimes;
                        }
                    }

                    response.Reports = response.Reports.OrderByDescending(r => r.PlayedTimes).ToList();
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
