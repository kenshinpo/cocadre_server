﻿using Cassandra;
using CassandraService.CassandraUtilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CassandraService.Entity
{
    public class AnalyticFeed
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public void UpdateFeedHashTagPopularity(List<FeedHashTag> tags, string companyId, int valueToUpdate, ISession analyticSession = null)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                foreach(FeedHashTag tag in tags)
                {
                    int updatedCount = tag.Popularity + valueToUpdate;

                    if(updatedCount < 0)
                    {
                        updatedCount = 0;
                    }

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("small_cap_tag_by_popularity", new List<string> { "company_id", "popularity", "small_cap_tag" }));
                    deleteBatch.Add(ps.Bind(companyId, tag.Popularity, tag.HashTag));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("small_cap_tag_by_popularity", new List<string> { "company_id", "popularity", "small_cap_tag" }));
                    updateBatch.Add(ps.Bind(companyId, updatedCount, tag.HashTag));
                }

                analyticSession.Execute(deleteBatch);
                analyticSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public List<FeedHashTag> SelectPopularTags(string companyId, int limit = 0, ISession analyticSession = null)
        {
            List<FeedHashTag> popularTags = new List<FeedHashTag>();

            try
            {
                PreparedStatement ps = null;

                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithComparison("small_cap_tag_by_popularity", new List<string>(),  new List<string> { "company_id" }, "popularity", CQLGenerator.Comparison.GreaterThan, limit));
                RowSet popularRowset = analyticSession.Execute(ps.Bind(companyId, 0));

                foreach(Row popularRow in popularRowset)
                {
                    string smallCapTag = popularRow.GetValue<string>("small_cap_tag");
                    int popularity = popularRow.GetValue<int>("popularity");
                    popularTags.Add(new FeedHashTag
                    {
                        HashTag = string.Format("#{0}", smallCapTag),
                        Popularity = popularity
                    });
                }

                popularTags = popularTags.OrderByDescending(t => t.Popularity).ThenBy(t => t.HashTag).ToList();

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return popularTags;
        }

        public void UpdateImpression(string feedId, string seenByUserId, ISession analyticSession, DateTime? currentTime = null)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();

                if (currentTime == null)
                {
                    currentTime = DateTime.UtcNow;
                }

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("feed_unique_impression_stats", new List<string>(), new List<string> { "feed_id", "seen_by_user_id" }));
                Row impressionRow = analyticSession.Execute(ps.Bind(feedId, seenByUserId)).FirstOrDefault();

                if (impressionRow != null)
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("feed_unique_impression_stats", new List<string> { "feed_id", "seen_by_user_id" }, new List<string> { "last_seen_on_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, feedId, seenByUserId));
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("feed_unique_impression_stats", new List<string> { "feed_id", "seen_by_user_id", "first_seen_on_timestamp", "last_seen_on_timestamp" }));
                    updateBatch.Add(ps.Bind(feedId, seenByUserId, currentTime, currentTime));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateCounterStatement("feed_unique_impression_counter", new List<string> { "feed_id" }, new List<string> { "impression" }, new List<int> { 1 }));
                    analyticSession.Execute(ps.Bind(feedId));
                }

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("feed_impression_stats", new List<string> { "feed_id", "seen_by_user_id", "seen_on_timestamp" }));
                updateBatch.Add(ps.Bind(feedId, seenByUserId, currentTime));

                ps = analyticSession.Prepare(CQLGenerator.UpdateCounterStatement("feed_impression_counter", new List<string> { "feed_id" }, new List<string> { "impression" }, new List<int> { 1 }));
                analyticSession.Execute(ps.Bind(feedId));

                analyticSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}
