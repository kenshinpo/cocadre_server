﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Threading;

namespace CassandraService.Entity
{
    [DataContract]
    public class PulseDynamic : Pulse
    {
        private static List<string> alphabetList = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H" };

        [DataMember]
        public string DeckId { get; set; }

        [DataMember]
        public int NumberOfOptions { get; set; }

        [DataMember]
        public string Ordering { get; set; }

        [DataMember]
        public float FloatOrdering { get; set; }

        [DataMember]
        public bool IsComplusory { get; set; }

        [DataMember]
        public bool IsAnonymous { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public int CardType { get; set; }

        [DataMember]
        public int QuestionType { get; set; }

        [DataMember]
        public int LogicLevel { get; set; }

        [DataMember]
        public bool IsCustomized { get; set; }

        [DataMember]
        public string Option1Id { get; set; }
        [DataMember]
        public string Option2Id { get; set; }
        [DataMember]
        public string Option3Id { get; set; }
        [DataMember]
        public string Option4Id { get; set; }
        [DataMember]
        public string Option5Id { get; set; }
        [DataMember]
        public string Option6Id { get; set; }
        [DataMember]
        public string Option7Id { get; set; }
        [DataMember]
        public string Option8Id { get; set; }

        [DataMember]
        public string Option1Content { get; set; }
        [DataMember]
        public string Option2Content { get; set; }
        [DataMember]
        public string Option3Content { get; set; }
        [DataMember]
        public string Option4Content { get; set; }
        [DataMember]
        public string Option5Content { get; set; }
        [DataMember]
        public string Option6Content { get; set; }
        [DataMember]
        public string Option7Content { get; set; }
        [DataMember]
        public string Option8Content { get; set; }

        [DataMember]
        public string Option1NextCardId { get; set; }
        [DataMember]
        public string Option2NextCardId { get; set; }
        [DataMember]
        public string Option3NextCardId { get; set; }
        [DataMember]
        public string Option4NextCardId { get; set; }
        [DataMember]
        public string Option5NextCardId { get; set; }
        [DataMember]
        public string Option6NextCardId { get; set; }
        [DataMember]
        public string Option7NextCardId { get; set; }
        [DataMember]
        public string Option8NextCardId { get; set; }

        [DataMember]
        public List<DeckCardOption> Options { get; set; }

        [DataMember]
        public int RangeType { get; set; }
        [DataMember]
        public string MinRangeLabel { get; set; }
        [DataMember]
        public string MaxRangeLabel { get; set; }

        [DataMember]
        public PulseDynamic ParentCard { get; set; }
        [DataMember]
        public DeckCardOption ParentOption { get; set; }

        [DataMember]
        public List<DeckTextAnswer> TextAnswers { get; set; }

        [DataMember]
        public int SelectedRange { get; set; }

        [DataMember]
        public DeckCardOption SelectedOption { get; set; }

        [DataMember]
        public DeckTextAnswer TextAnswer { get; set; }

        [DataMember]
        public bool HasOutgoingLogic { get; set; }
        [DataMember]
        public bool HasIncomingLogic { get; set; }

        [DataMember]
        public int TotalRespondents { get; set; }

        [DataMember]
        public Chart RespondentChart { get; set; }

        [DataMember]
        public DateTime UpdatedStartDate { get; set; }

        [DataMember]
        public List<string> ContainsText { get; set; }

        [DataMember]
        public int AnonymityCount { get; set; }

        public enum PublishMethodTypeEnum
        {
            Schedule = 1,
            Routine = 2
        }

        public enum DeckCardTypeEnum
        {
            SelectOne = 1,
            NumberRange = 2,
            Text = 3,
            SelectOneWithLogic = 4
        }

        public enum CardQuestionTypeEnum
        {
            NothingSelected = 1,
            CommunicationAndTransparency = 2,
            Company360 = 3,
            Culture = 4,
            PersonalDevelopment = 5,
            Recognition = 6,
            LeadershipTeam = 7,
            TeamPeers = 8,
            IndividualPerformance = 9,
            TalentRetention = 10
        }

        public enum CardRangeTypeEnum
        {
            OneTo3 = 1,
            OneTo5 = 2,
            OneTo7 = 3,
            OneTo10 = 4,
            ZeroTo100Percent = 5
        }

        public enum DeckPerTimeFrameTypeEnum
        {
            PerDay = 1,
            PerWeek = 2,
            PerMonth = 3
        }

        public enum PeriodFrameForWeekEnum
        {
            Mon = 1,
            Tue = 2,
            Wed = 3,
            Thurs = 4,
            Fri = 5,
            Sat = 6,
            Sun = 7
        }

        public enum PeriodFrameForMonthEnum
        {
            FirstWorkDay = 1,
            LastWorkDay = 2,
            ParticularDay = 3
        }

        public PulseDeckCreateResponse CreateDeck(string deckId,
                                                  string adminUserId,
                                                  string companyId,
                                                  string title,
                                                  bool isCompulsory,
                                                  int anonymityCount,
                                                  int publishMethodType,
                                                  int status,
                                                  DateTime startDate,
                                                  DateTime? endDate,
                                                  bool isPrioritized,
                                                  List<string> targetedDepartmentIds,
                                                  List<string> targetedUserIds,
                                                  int numberOfCardsPerTimeFrame = 0,
                                                  int perTimeFrameType = 0,
                                                  int periodFrameType = 0,
                                                  int durationPerCard = 0)
        {
            PulseDeckCreateResponse response = new PulseDeckCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Deck title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.DeckTitleIsEmpty;
                    return response;
                }

                if (publishMethodType == (int)PublishMethodTypeEnum.Routine)
                {
                    // Do checking
                    PulseValidationResponse routineScheduleResponse = ValidateRoutineSchedule(perTimeFrameType, periodFrameType, numberOfCardsPerTimeFrame);
                    if (!routineScheduleResponse.Success)
                    {
                        Log.Error(routineScheduleResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(routineScheduleResponse.ErrorCode);
                        response.ErrorMessage = routineScheduleResponse.ErrorMessage;
                        return response;
                    }

                    PulseValidationResponse durationResponse = ValidateDurationPerCard(durationPerCard);
                    if (!durationResponse.Success)
                    {
                        Log.Error(durationResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(durationResponse.ErrorCode);
                        response.ErrorMessage = durationResponse.ErrorMessage;
                        return response;
                    }
                }
                else
                {
                    numberOfCardsPerTimeFrame = 0;
                    periodFrameType = 0;
                    perTimeFrameType = 0;
                    durationPerCard = 0;
                }

                DateTime currentTime = DateTime.UtcNow;

                if (endDate.HasValue && endDate.Value == DateTime.MinValue)
                {
                    endDate = null;
                }

                startDate = startDate.AddSeconds(-startDate.Second);
                startDate = startDate.AddMilliseconds(-startDate.Millisecond);
                PulseValidationResponse dateResponse = ValidateDate(startDate, endDate, currentTime);
                if (!dateResponse.Success)
                {
                    Log.Error(dateResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                    response.ErrorMessage = dateResponse.ErrorMessage;
                    return response;
                }

                PulseValidationResponse statusResponse = ValidateStatus(status);
                if (!statusResponse.Success)
                {
                    Log.Error(statusResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(statusResponse.ErrorCode);
                    response.ErrorMessage = statusResponse.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(deckId))
                {
                    deckId = UUIDGenerator.GenerateUniqueIDForPulseDeck();
                }

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                // Hard code to true always
                isCompulsory = true;

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_dynamic_deck",
                   new List<string> { "id", "company_id", "is_complusory", "publish_method_type", "number_of_cards_per_time_frame", "per_time_frame_type", "period_frame_type", "duration_per_card", "is_anonymous", "title", "status", "start_timestamp", "end_timestamp", "is_prioritized", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp", "anonymity_count" }));
                batchStatement.Add(ps.Bind(deckId, companyId, isCompulsory, publishMethodType, numberOfCardsPerTimeFrame, perTimeFrameType, periodFrameType, durationPerCard, (anonymityCount > 0), title, status, startDate, endDate, isPrioritized, adminUserId, currentTime, adminUserId, currentTime, anonymityCount));

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_dynamic_deck_by_company",
                   new List<string> { "deck_id", "company_id", "publish_method_type", "title", "status", "is_prioritized", "start_timestamp", "end_timestamp" }));
                batchStatement.Add(ps.Bind(deckId, companyId, publishMethodType, title, status, isPrioritized, startDate, endDate));

                List<BoundStatement> privacyStatement = CreatePrivacy(deckId, targetedDepartmentIds, targetedUserIds, session, false);
                foreach (BoundStatement bs in privacyStatement)
                {
                    batchStatement.Add(bs);
                }

                session.Execute(batchStatement);
                response.DeckId = deckId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseValidationResponse ValidateRoutineSchedule(int perTimeFrameType, int periodFrameType, int numberOfCardsPerTimeFrame)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                if (numberOfCardsPerTimeFrame == 0)
                {
                    Log.Error(ErrorMessage.DeckInvalidNumberOfCardPerTimeFrame);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalidNumberOfCardPerTimeFrame);
                    response.ErrorMessage = ErrorMessage.DeckInvalidNumberOfCardPerTimeFrame;
                    return response;
                }

                if (perTimeFrameType == 0)
                {
                    Log.Error(ErrorMessage.DeckInvalidPerTimeFrame);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalidPerTimeFrame);
                    response.ErrorMessage = ErrorMessage.DeckInvalidPerTimeFrame;
                    return response;
                }

                bool hasErrorForPeriod = false;

                switch (perTimeFrameType)
                {
                    case (int)DeckPerTimeFrameTypeEnum.PerDay:
                        periodFrameType = 0;
                        break;
                    case (int)DeckPerTimeFrameTypeEnum.PerWeek:
                        if (periodFrameType < (int)PeriodFrameForWeekEnum.Mon || periodFrameType > (int)PeriodFrameForWeekEnum.Sun)
                        {
                            hasErrorForPeriod = true;
                        }
                        break;
                    case (int)DeckPerTimeFrameTypeEnum.PerMonth:
                        if (periodFrameType < (int)PeriodFrameForMonthEnum.FirstWorkDay || periodFrameType > (int)PeriodFrameForMonthEnum.ParticularDay)
                        {
                            hasErrorForPeriod = true;
                        }
                        break;
                }

                if (hasErrorForPeriod)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalidPeriodFrameType);
                    response.ErrorMessage = ErrorMessage.DeckInvalidPeriodFrameType;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PulseSelectAllDecksResponse SelectAllDeckBasic(string adminUserId,
                                                              string companyId,
                                                              DateTime? filteredStartDate = null,
                                                              DateTime? filteredEndDate = null,
                                                              string containsName = null,
                                                              int filteredProgress = 0,
                                                              int filteredPublishedType = 0)
        {
            PulseSelectAllDecksResponse response = new PulseSelectAllDecksResponse();
            response.Decks = new List<PulseDeck>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_deck", new List<string>(), new List<string> { "company_id" }));
                RowSet deckRowSet = mainSession.Execute(ps.Bind(companyId));

                if (filteredStartDate.HasValue)
                {
                    filteredStartDate = filteredStartDate.Value.ToUniversalTime();
                }

                if (filteredEndDate.HasValue)
                {
                    filteredEndDate = filteredEndDate.Value.ToUniversalTime();
                }

                DateTime currentTime = DateTime.UtcNow;
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                foreach (Row deckRow in deckRowSet)
                {
                    string title = deckRow.GetValue<string>("title");
                    int status = deckRow.GetValue<int>("status");
                    string deckId = deckRow.GetValue<string>("id");
                    int publishedType = deckRow.GetValue<int>("publish_method_type");
                    DateTime startDate = deckRow.GetValue<DateTime>("start_timestamp");
                    DateTime? endDate = deckRow.GetValue<DateTime?>("end_timestamp");
                    int progress = SelectProgress(startDate, endDate, currentTime);

                    if (filteredProgress != 0 && filteredProgress != progress)
                    {
                        continue;
                    }

                    if (filteredPublishedType != 0 && filteredPublishedType != publishedType)
                    {
                        continue;
                    }

                    if (filteredStartDate.HasValue)
                    {
                        if (startDate < filteredStartDate)
                        {
                            continue;
                        }
                    }

                    if (filteredEndDate.HasValue)
                    {
                        if (endDate.HasValue)
                        {
                            if (endDate > filteredEndDate)
                            {
                                continue;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(containsName))
                    {
                        if (!title.Trim().ToLower().Contains(containsName.Trim().ToLower()))
                        {
                            continue;
                        }
                    }

                    PulseDeck selectedDeck = SelectDeck(deckId, companyId, (int)PulseQueryEnum.Basic, timezoneOffset, null, false, false, progress, true, false, mainSession, deckRow).Deck;
                    if (selectedDeck != null)
                    {
                        response.Decks.Add(selectedDeck);
                    }
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseCreateResponse CreateDynamicCard(string cardId,
                                                     string deckId,
                                                     string adminUserId,
                                                     string companyId,
                                                     string title,
                                                     string description,
                                                     int cardType,
                                                     int questionType,
                                                     DateTime startDate,
                                                     int numberOfOptions = 0,
                                                     int rangeType = 0,
                                                     string imageUrl = null,
                                                     string minRangeLabel = null,
                                                     string maxRangeLabel = null,
                                                     List<DeckCardOption> options = null,
                                                     string parentCardId = null,
                                                     int parentOptionNumber = 0,
                                                     float ordering = 0,
                                                     int logicLevel = 0,
                                                     ISession session = null,
                                                     Row deckRow = null)
        {
            PulseCreateResponse response = new PulseCreateResponse();
            response.Pulse = new PulseDynamic();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    deckRow = vh.ValidatePulseDeck(companyId, deckId, session);
                    if (deckRow == null)
                    {
                        Log.Error("Pulse deck does not exists");
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                        response.ErrorMessage = ErrorMessage.DeckInvalid;
                        return response;
                    }
                }

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime deckStartTimestamp = deckRow.GetValue<DateTime>("start_timestamp");
                DateTime? deckEndDate = deckRow.GetValue<DateTime?>("end_timestamp");
                DateTime? cardEndDate = null;

                if (string.IsNullOrEmpty(cardId))
                {
                    cardId = UUIDGenerator.GenerateUniqueIDForPulseDynamic();
                }


                string parentOptionId = string.Empty;
                // Get ordering first
                // Need to check parent card first
                if (string.IsNullOrEmpty(parentCardId))
                {
                    if (logicLevel == 0)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_order", new List<string>(), new List<string> { "deck_id" }));
                        List<Row> orderRowset = session.Execute(ps.Bind(deckId)).ToList();
                        if (orderRowset.Count > 0)
                        {
                            if (ordering == 0)
                            {
                                Row lastRow = orderRowset[orderRowset.Count() - 1];
                                ordering = lastRow.GetValue<float>("ordering");
                                ordering = (int)ordering + 1;
                            }
                        }
                        else
                        {
                            ordering = 1;
                        }
                    }
                }
                else
                {
                    // For the actual implementation at admin portal
                    Row parentCardRow = vh.ValidatePulseDeckCard(deckId, parentCardId, session);
                    if (parentCardRow == null)
                    {
                        Log.Error("Invalid pulseId: " + parentCardId);
                        response.ErrorCode = Int16.Parse(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }

                    int numberOfOptionsForParent = parentCardRow.GetValue<int>("number_of_options");
                    parentOptionId = parentCardId + "_" + parentOptionNumber;
                    if (parentOptionNumber <= 0 || parentOptionNumber > numberOfOptionsForParent)
                    {
                        response.ErrorCode = Int16.Parse(ErrorCode.DeckCardInvalidOption);
                        response.ErrorMessage = ErrorMessage.DeckCardInvalidOption;
                        return response;
                    }

                    if (logicLevel == 0 && ordering == 0)
                    {
                        float parentOrder = parentCardRow.GetValue<float>("ordering");
                        int parentLogicLevel = parentCardRow.GetValue<int>("logic_level");
                        int delimiter = (int)Math.Pow(10, parentLogicLevel);
                        logicLevel = parentLogicLevel + 1;
                        ordering = parentOrder + (float)parentOptionNumber / delimiter;
                    }

                    ps = session.Prepare(CQLGenerator.InsertStatement("dynamic_option_to_logic",
                        new List<string> { "pulse_id", "option_id", "next_pulse_id" }));
                    updateBatch.Add(ps.Bind(parentCardId, parentOptionId, cardId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("dynamic_option_from_logic",
                        new List<string> { "pulse_id", "from_option_id", "from_pulse_id" }));
                    updateBatch.Add(ps.Bind(cardId, parentOptionId, parentCardId));

                    // Update
                    ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" }, new List<string> { "option_" + parentOptionNumber + "_next_card_id", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(cardId, adminUserId, DateTime.UtcNow, deckId, parentCardId));
                }

                int deckStatus = deckRow.GetValue<int>("status");
                bool isPrioritized = deckRow.GetValue<bool>("is_prioritized");
                int durationPerCard = deckRow.GetValue<int>("duration_per_card");
                int publishMethodType = deckRow.GetValue<int>("publish_method_type");
                int anonymityCount = 0;
                if (deckRow.IsNull("anonymity_count"))
                {
                    if (deckRow.GetValue<bool>("is_anonymous"))
                    {
                        anonymityCount = 1;

                        #region Update null value of anonymity_count column.
                        ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                        new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                        BatchStatement bsUpdate = new BatchStatement();
                        bsUpdate.Add(ps.Bind(anonymityCount, deckRow.GetValue<bool>("company_id"), deckRow.GetValue<bool>("id")));
                        session.Execute(bsUpdate);
                        #endregion
                    }
                }
                else
                {
                    anonymityCount = deckRow.GetValue<int>("anonymity_count");
                }

                if (publishMethodType == (int)PublishMethodTypeEnum.Schedule)
                {
                    startDate = deckStartTimestamp.AddMilliseconds(1);

                    // Need reorder the rest such that it is in reversed order
                    List<PulseDynamic> allCards = SelectAllCards(deckId, adminUserId, companyId, null, true, false, false, session, deckRow).Pulses;

                    for (int index = 0; index < allCards.Count; index++)
                    {
                        
                        PulseDynamic updatingCard = allCards[index];

                        DateTime oldTimestamp = updatingCard.StartDate;

                        updatingCard.StartDate = updatingCard.StartDate.AddSeconds(-updatingCard.StartDate.Second);
                        updatingCard.StartDate = updatingCard.StartDate.AddMilliseconds(-updatingCard.StartDate.Millisecond);

                        updatingCard.UpdatedStartDate = updatingCard.StartDate.AddMilliseconds((allCards.Count + 1) - ((int)updatingCard.FloatOrdering - 1));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck",
                            new List<string> { "deck_id", "id" }, new List<string> { "start_timestamp" }, new List<string>()));
                        updateBatch.Add(ps.Bind(updatingCard.UpdatedStartDate, deckId, updatingCard.PulseId));

                        if (updatingCard.LogicLevel <= 1 && deckStatus == (int)PulseStatusEnum.Active)
                        {
                            // Update pulse_by_priority_with_timestamp
                            ps = session.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                            deleteBatch.Add(ps.Bind(companyId, isPrioritized, (int)PulsePriorityEnum.Dynamic, oldTimestamp, updatingCard.PulseId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id", "deck_id", "pulse_type" }));
                            updateBatch.Add(ps.Bind(companyId, isPrioritized, (int)PulsePriorityEnum.Dynamic, updatingCard.UpdatedStartDate, updatingCard.PulseId, deckId, updatingCard.PulseType));
                        }
                    }

                    cardEndDate = deckEndDate;
                }
                else
                {
                    cardEndDate = null;

                    int numberOfCardsPerTimeFrame = deckRow.GetValue<int>("number_of_cards_per_time_frame");
                    int perTimeFrameType = deckRow.GetValue<int>("per_time_frame_type");

                    startDate = startDate.Date;
                    startDate = startDate.AddHours(deckStartTimestamp.Hour);
                    startDate = startDate.AddMinutes(deckStartTimestamp.Minute);

                    if (startDate < deckStartTimestamp)
                    {
                        Log.Error(ErrorMessage.DeckCardStartDateEarlierThanDeckStartDate);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckCardStartDateEarlierThanDeckStartDate);
                        response.ErrorMessage = ErrorMessage.DeckCardStartDateEarlierThanDeckStartDate;
                        return response;
                    }

                    PulseCheckForCardCountOnDateResponse checkCardCountResponse = CheckForCardCount(deckId, cardId, logicLevel, numberOfCardsPerTimeFrame, startDate, session);
                    if (!checkCardCountResponse.Success)
                    {
                        response.ErrorCode = checkCardCountResponse.ErrorCode;
                        response.ErrorMessage = checkCardCountResponse.ErrorMessage;
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Pulse title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.PulseTitleIsEmpty;
                    return response;
                }

                // description is not compulsory. 2017/02/10
                //if (string.IsNullOrEmpty(description))
                //{
                //    Log.Error("Pulse description is missing");
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseDescriptionIsEmpty);
                //    response.ErrorMessage = ErrorMessage.PulseDescriptionIsEmpty;
                //    return response;
                //}

                DateTime currentTime = DateTime.UtcNow;

                PulseValidationResponse dateResponse = ValidateDate(startDate, cardEndDate, currentTime);
                if (!dateResponse.Success)
                {
                    Log.Error(dateResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                    response.ErrorMessage = dateResponse.ErrorMessage;
                    return response;
                }

                string option1Id = null;
                string option2Id = null;
                string option3Id = null;
                string option4Id = null;
                string option5Id = null;
                string option6Id = null;
                string option7Id = null;
                string option8Id = null;

                string option1Content = null;
                string option2Content = null;
                string option3Content = null;
                string option4Content = null;
                string option5Content = null;
                string option6Content = null;
                string option7Content = null;
                string option8Content = null;

                string option1NextCardId = null;
                string option2NextCardId = null;
                string option3NextCardId = null;
                string option4NextCardId = null;
                string option5NextCardId = null;
                string option6NextCardId = null;
                string option7NextCardId = null;
                string option8NextCardId = null;

                bool hasLogic = cardType == (int)DeckCardTypeEnum.SelectOneWithLogic ? true : false;

                if (string.IsNullOrEmpty(imageUrl))
                {
                    imageUrl = null;
                }

                // Init
                if (cardType != (int)DeckCardTypeEnum.NumberRange)
                {
                    rangeType = 0;
                    maxRangeLabel = null;
                    minRangeLabel = null;
                }

                if (cardType == (int)DeckCardTypeEnum.SelectOneWithLogic && logicLevel == 0)
                {
                    logicLevel = 1;
                }

                if (cardType == (int)DeckCardTypeEnum.SelectOne || cardType == (int)DeckCardTypeEnum.SelectOneWithLogic)
                {
                    PulseValidationResponse optionResponse = ValidateOptionContent(numberOfOptions, options, hasLogic);
                    if (!optionResponse.Success)
                    {
                        Log.Error(dateResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    int optionNumber = 1;
                    foreach (DeckCardOption option in options)
                    {
                        string optionId = cardId + "_" + optionNumber;
                        if (hasLogic)
                        {
                            if (logicLevel == Convert.ToInt16(DefaultResource.PulseDynamicMaxLogicLevel) || option.NextPulse.PulseId == null)
                            {
                                option.NextPulse.PulseId = DefaultResource.PulseDynamicEndCardId;
                            }
                            else
                            {
                                #region Postman creation
                                // For postman creation
                                if (string.IsNullOrEmpty(parentCardId))
                                {
                                    string fromCardId = cardId;
                                    string toCardId = option.NextPulse.PulseId;

                                    // Recusive
                                    PulseCreateResponse recursiveCreateResponse = CreateDynamicCard(toCardId,
                                                                                                    deckId,
                                                                                                    adminUserId,
                                                                                                    companyId,
                                                                                                    option.NextPulse.Title,
                                                                                                    option.NextPulse.Description,
                                                                                                    option.NextPulse.CardType,
                                                                                                    option.NextPulse.QuestionType,
                                                                                                    option.NextPulse.StartDate,
                                                                                                    option.NextPulse.NumberOfOptions,
                                                                                                    option.NextPulse.RangeType,
                                                                                                    option.NextPulse.ImageUrl,
                                                                                                    option.NextPulse.MinRangeLabel,
                                                                                                    option.NextPulse.MaxRangeLabel,
                                                                                                    option.NextPulse.Options,
                                                                                                    null,
                                                                                                    0,
                                                                                                    ordering + (float)optionNumber / (int)Math.Pow(10, logicLevel),
                                                                                                    logicLevel + 1,
                                                                                                    session,
                                                                                                    deckRow);

                                    if (recursiveCreateResponse.Success)
                                    {
                                        option.NextPulse.PulseId = toCardId;

                                        ps = session.Prepare(CQLGenerator.InsertStatement("dynamic_option_to_logic",
                                            new List<string> { "pulse_id", "option_id", "next_pulse_id" }));
                                        updateBatch.Add(ps.Bind(fromCardId, optionId, toCardId));

                                        ps = session.Prepare(CQLGenerator.InsertStatement("dynamic_option_from_logic",
                                            new List<string> { "pulse_id", "from_option_id", "from_pulse_id" }));
                                        updateBatch.Add(ps.Bind(toCardId, optionId, fromCardId));
                                    }
                                    else
                                    {
                                        response.ErrorCode = recursiveCreateResponse.ErrorCode;
                                        response.ErrorMessage = recursiveCreateResponse.ErrorMessage;
                                        return response;
                                    }
                                }
                                #endregion
                            }
                        }

                        switch (optionNumber)
                        {
                            case 1:
                                option1Id = optionId;
                                option1Content = option.Content;
                                option1NextCardId = option.NextPulse.PulseId;
                                break;
                            case 2:
                                option2Id = optionId;
                                option2Content = option.Content;
                                option2NextCardId = option.NextPulse.PulseId;
                                break;
                            case 3:
                                option3Id = optionId;
                                option3Content = option.Content;
                                option3NextCardId = option.NextPulse.PulseId;
                                break;
                            case 4:
                                option4Id = optionId;
                                option4Content = option.Content;
                                option4NextCardId = option.NextPulse.PulseId;
                                break;
                            case 5:
                                option5Id = optionId;
                                option5Content = option.Content;
                                option5NextCardId = option.NextPulse.PulseId;
                                break;
                            case 6:
                                option6Id = optionId;
                                option6Content = option.Content;
                                option6NextCardId = option.NextPulse.PulseId;
                                break;
                            case 7:
                                option7Id = optionId;
                                option7Content = option.Content;
                                option7NextCardId = option.NextPulse.PulseId;
                                break;
                            case 8:
                                option8Id = optionId;
                                option8Content = option.Content;
                                option8NextCardId = option.NextPulse.PulseId;
                                break;
                        }
                        optionNumber++;
                    }
                }
                else
                {
                    if (cardType == (int)DeckCardTypeEnum.NumberRange)
                    {
                        if (!Enum.IsDefined(typeof(CardRangeTypeEnum), rangeType))
                        {
                            Log.Error(ErrorMessage.DeckInvalidRangeType);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.DeckInvalidRangeType);
                            response.ErrorMessage = ErrorMessage.DeckInvalidRangeType;
                        }

                        if (string.IsNullOrEmpty(minRangeLabel))
                        {
                            Log.Error(ErrorMessage.DeckMinRangeLabelIsEmpty);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.DeckMinRangeLabelIsEmpty);
                            response.ErrorMessage = ErrorMessage.DeckMinRangeLabelIsEmpty;
                        }

                        if (string.IsNullOrEmpty(maxRangeLabel))
                        {
                            Log.Error(ErrorMessage.DeckMaxRangeLabelIsEmpty);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.DeckMaxRangeLabelIsEmpty);
                            response.ErrorMessage = ErrorMessage.DeckMaxRangeLabelIsEmpty;
                        }
                    }
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_dynamic_card_by_deck",
                    new List<string> { "id", "deck_id", "card_type", "question_type", "title", "description", "image_url", "ordering", "logic_level", "number_of_options",
                        "option_1_id", "option_1_content", "option_1_next_card_id",
                        "option_2_id", "option_2_content", "option_2_next_card_id",
                        "option_3_id", "option_3_content", "option_3_next_card_id",
                        "option_4_id", "option_4_content", "option_4_next_card_id",
                        "option_5_id", "option_5_content", "option_5_next_card_id",
                        "option_6_id", "option_6_content", "option_6_next_card_id",
                        "option_7_id", "option_7_content", "option_7_next_card_id",
                        "option_8_id", "option_8_content", "option_8_next_card_id",
                        "range_type", "max_range_label", "min_range_label",
                        "start_timestamp", "end_timestamp", "duration_day",
                        "is_start_date_customized",
                        "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                updateBatch.Add(ps.Bind(cardId, deckId, cardType, questionType, title, description, imageUrl, ordering, logicLevel, numberOfOptions,
                                           option1Id, option1Content, option1NextCardId,
                                           option2Id, option2Content, option2NextCardId,
                                           option3Id, option3Content, option3NextCardId,
                                           option4Id, option4Content, option4NextCardId,
                                           option5Id, option5Content, option5NextCardId,
                                           option6Id, option6Content, option6NextCardId,
                                           option7Id, option7Content, option7NextCardId,
                                           option8Id, option8Content, option8NextCardId,
                                           rangeType, maxRangeLabel, minRangeLabel,
                                           startDate, cardEndDate, durationPerCard,
                                           false,
                                           adminUserId, currentTime, adminUserId, currentTime));

                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_dynamic_order",
                    new List<string> { "deck_id", "pulse_id", "ordering" }));
                updateBatch.Add(ps.Bind(deckId, cardId, ordering));

                // Sorting
                if (deckStatus == (int)PulseStatusEnum.Active)
                {
                    if (!hasLogic)
                    {
                        // Must be no logic and logic level is 0
                        if (logicLevel == 0)
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp",
                                new List<string> { "pulse_id", "company_id", "deck_id", "pulse_priority", "pulse_type", "is_prioritized", "start_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, companyId, deckId, (int)PulsePriorityEnum.Dynamic, (int)PulseTypeEnum.Dynamic, isPrioritized, startDate));
                        }

                    }
                    else
                    {
                        // Must be the base of the logic cards which is 1
                        if (logicLevel == 1)
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp",
                                new List<string> { "pulse_id", "company_id", "deck_id", "pulse_priority", "pulse_type", "is_prioritized", "start_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, companyId, deckId, (int)PulsePriorityEnum.Dynamic, (int)PulseTypeEnum.Dynamic, isPrioritized, startDate));
                        }
                    }

                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Pulse = SelectCard(deckId, cardId, null, publishMethodType, false, anonymityCount, false, false, false, null, session).Pulse;
                if (!string.IsNullOrEmpty(parentCardId))
                {
                    response.Pulse.ParentCard = SelectCard(deckId, parentCardId, null, publishMethodType, false, anonymityCount, false, false, false, null, session).Pulse;
                    switch (parentOptionNumber)
                    {
                        case 1:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option1Content
                            };
                            break;
                        case 2:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option2Content
                            };
                            break;
                        case 3:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option3Content
                            };
                            break;
                        case 4:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option4Content
                            };
                            break;
                        case 5:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option5Content
                            };
                            break;
                        case 6:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option6Content
                            };
                            break;
                        case 7:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option7Content
                            };
                            break;
                        case 8:
                            response.Pulse.ParentOption = new DeckCardOption
                            {
                                OptionId = parentOptionId,
                                Content = response.Pulse.ParentCard.Option8Content
                            };
                            break;
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse UpdateDynamicCard(string cardId,
                                                     string deckId,
                                                     string adminUserId,
                                                     string companyId,
                                                     string newTitle,
                                                     string newDescription,
                                                     int newCardType,
                                                     int newQuestionType,
                                                     DateTime newStartDate,
                                                     int newNumberOfOptions = 0,
                                                     int newRangeType = 0,
                                                     string newImageUrl = null,
                                                     string newMinRangeLabel = null,
                                                     string newMaxRangeLabel = null,
                                                     List<DeckCardOption> newOptions = null,
                                                     ISession session = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, session);
                if (deckRow == null)
                {
                    Log.Error("Pulse deck does not exists: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                Row pulseRow = vh.ValidatePulseDeckCard(deckId, cardId, session);
                if (pulseRow == null)
                {
                    Log.Error("Pulse deck card does not exists: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                // Date variable
                DateTime deckStartTimestamp = deckRow.GetValue<DateTime>("start_timestamp");
                DateTime? deckEndDate = deckRow.GetValue<DateTime?>("end_timestamp");

                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = pulseRow.GetValue<DateTime>("start_timestamp");
                DateTime? currentEndDate = null;

                int deckStatus = deckRow.GetValue<int>("status");
                bool isPrioritized = deckRow.GetValue<bool>("is_prioritized");

                int durationPerCard = deckRow.GetValue<int>("duration_per_card");
                int publishMethodType = deckRow.GetValue<int>("publish_method_type");

                float currentOrdering = pulseRow.GetValue<float>("ordering");
                if (publishMethodType == (int)PublishMethodTypeEnum.Schedule)
                {
                    // Schedule cannot change start date
                    newStartDate = currentStartDate;
                    currentEndDate = deckEndDate;

                    int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);
                    if (progress != (int)ProgressStatusEnum.Upcoming && deckStatus != (int)PulseStatusEnum.Unlisted)
                    {
                        Log.Error(ErrorMessage.PulseCannotBeEdited);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseCannotBeEdited);
                        response.ErrorMessage = ErrorMessage.PulseCannotBeEdited;
                        return response;
                    }
                }
                else
                {
                    int logicLevel = pulseRow.GetValue<int>("logic_level");
                    int durationDay = pulseRow.GetValue<int>("duration_day");
                    if (durationDay > 0)
                    {
                        currentEndDate = currentStartDate.AddDays(durationDay);
                    }
                    else
                    {
                        currentEndDate = null;
                    }


                    int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);
                    if (progress != (int)ProgressStatusEnum.Upcoming)
                    {
                        Log.Error(ErrorMessage.PulseCannotBeEdited);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseCannotBeEdited);
                        response.ErrorMessage = ErrorMessage.PulseCannotBeEdited;
                        return response;
                    }

                    int numberOfCardsPerTimeFrame = deckRow.GetValue<int>("number_of_cards_per_time_frame");
                    int perTimeFrameType = deckRow.GetValue<int>("per_time_frame_type");

                    newStartDate = newStartDate.Date;
                    newStartDate = newStartDate.AddHours(deckStartTimestamp.Hour);
                    newStartDate = newStartDate.AddMinutes(deckStartTimestamp.Minute);

                    if (newStartDate < deckStartTimestamp)
                    {
                        Log.Error(ErrorMessage.DeckCardStartDateEarlierThanDeckStartDate);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckCardStartDateEarlierThanDeckStartDate);
                        response.ErrorMessage = ErrorMessage.DeckCardStartDateEarlierThanDeckStartDate;
                        return response;
                    }

                    PulseCheckForCardCountOnDateResponse checkCardCountResponse = CheckForCardCount(deckId, cardId, logicLevel, numberOfCardsPerTimeFrame, newStartDate, session);
                    if (!checkCardCountResponse.Success)
                    {
                        response.ErrorCode = checkCardCountResponse.ErrorCode;
                        response.ErrorMessage = checkCardCountResponse.ErrorMessage;
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Pulse title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.PulseTitleIsEmpty;
                    return response;
                }

                // description is not compulsory. 2017/02/10
                //if (string.IsNullOrEmpty(newDescription))
                //{
                //    Log.Error("Pulse description is missing");
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseDescriptionIsEmpty);
                //    response.ErrorMessage = ErrorMessage.PulseDescriptionIsEmpty;
                //    return response;
                //}

                PulseValidationResponse dateResponse = ValidateDate(newStartDate, deckEndDate, currentTime);
                if (!dateResponse.Success)
                {
                    Log.Error(dateResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                    response.ErrorMessage = dateResponse.ErrorMessage;
                    return response;
                }

                int currentLogicLevel = pulseRow.GetValue<int>("logic_level");
                int currentCardType = pulseRow.GetValue<int>("card_type");

                bool hasLogic = newCardType == (int)DeckCardTypeEnum.SelectOneWithLogic ? true : false;
                if (hasLogic)
                {
                    PulseValidationResponse optionResponse = ValidateOptionContent(newNumberOfOptions, newOptions, hasLogic);
                    if (!optionResponse.Success)
                    {
                        Log.Error(dateResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }
                }

                //Option.Pulse is always null

                // Check if current type is logic
                if (currentCardType == (int)DeckCardTypeEnum.SelectOneWithLogic)
                {
                    // Remove logic
                    RemoveChildOptionLogics(deckId, cardId, session);

                    // Check for previous logic 
                    int currentNumberOfOptions = pulseRow.GetValue<int>("number_of_options");
                    for (int index = 0; index < currentNumberOfOptions; index++)
                    {
                        string nextPulseId = pulseRow.GetValue<string>(string.Format("option_{0}_next_card_id", index + 1));
                        if ((!string.IsNullOrEmpty(nextPulseId) && !nextPulseId.Equals(DefaultResource.PulseDynamicEndCardId)) && string.IsNullOrEmpty(newOptions[index].NextPulseId))
                        {
                            DeleteCard(adminUserId, companyId, deckId, nextPulseId, deckRow, session);
                        }
                    }

                    if (newCardType != (int)DeckCardTypeEnum.SelectOneWithLogic)
                    {
                        RemoveAllChildren(pulseRow, deckId, companyId, deckRow, vh, session);
                    }
                }

                string option1Id = null;
                string option2Id = null;
                string option3Id = null;
                string option4Id = null;
                string option5Id = null;
                string option6Id = null;
                string option7Id = null;
                string option8Id = null;

                string option1Content = null;
                string option2Content = null;
                string option3Content = null;
                string option4Content = null;
                string option5Content = null;
                string option6Content = null;
                string option7Content = null;
                string option8Content = null;

                string option1NextCardId = null;
                string option2NextCardId = null;
                string option3NextCardId = null;
                string option4NextCardId = null;
                string option5NextCardId = null;
                string option6NextCardId = null;
                string option7NextCardId = null;
                string option8NextCardId = null;

                string currentImageUrl = pulseRow.GetValue<string>("image_url");

                // Init
                if (string.IsNullOrEmpty(newImageUrl))
                {
                    newImageUrl = null;
                }

                if (newCardType != (int)DeckCardTypeEnum.NumberRange)
                {
                    newRangeType = 0;
                    newMaxRangeLabel = null;
                    newMinRangeLabel = null;
                }

                if (newCardType == (int)DeckCardTypeEnum.SelectOneWithLogic && currentLogicLevel == 0)
                {
                    currentLogicLevel = 1;
                }

                if (newCardType == (int)DeckCardTypeEnum.SelectOne || newCardType == (int)DeckCardTypeEnum.SelectOneWithLogic)
                {
                    int optionNumber = 1;
                    foreach (DeckCardOption option in newOptions)
                    {
                        string optionId = cardId + "_" + optionNumber;
                        if (hasLogic)
                        {
                            if (currentLogicLevel == Convert.ToInt16(DefaultResource.PulseDynamicMaxLogicLevel) || string.IsNullOrEmpty(option.NextPulseId))
                            {
                                option.NextPulseId = DefaultResource.PulseDynamicEndCardId;
                            }
                        }
                        else
                        {
                            option.NextPulseId = null;
                        }

                        switch (optionNumber)
                        {
                            case 1:
                                option1Id = optionId;
                                option1Content = option.Content;
                                option1NextCardId = option.NextPulseId;
                                break;
                            case 2:
                                option2Id = optionId;
                                option2Content = option.Content;
                                option2NextCardId = option.NextPulseId;
                                break;
                            case 3:
                                option3Id = optionId;
                                option3Content = option.Content;
                                option3NextCardId = option.NextPulseId;
                                break;
                            case 4:
                                option4Id = optionId;
                                option4Content = option.Content;
                                option4NextCardId = option.NextPulseId;
                                break;
                            case 5:
                                option5Id = optionId;
                                option5Content = option.Content;
                                option5NextCardId = option.NextPulseId;
                                break;
                            case 6:
                                option6Id = optionId;
                                option6Content = option.Content;
                                option6NextCardId = option.NextPulseId;
                                break;
                            case 7:
                                option7Id = optionId;
                                option7Content = option.Content;
                                option7NextCardId = option.NextPulseId;
                                break;
                            case 8:
                                option8Id = optionId;
                                option8Content = option.Content;
                                option8NextCardId = option.NextPulseId;
                                break;
                        }

                        if (!string.IsNullOrEmpty(option.NextPulseId) && !option.NextPulseId.Equals(DefaultResource.PulseDynamicEndCardId))
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("dynamic_option_to_logic",
                                                               new List<string> { "pulse_id", "option_id", "next_pulse_id" }));
                            updateBatch.Add(ps.Bind(cardId, optionId, option.NextPulseId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("dynamic_option_from_logic",
                                new List<string> { "pulse_id", "from_option_id", "from_pulse_id" }));
                            updateBatch.Add(ps.Bind(option.NextPulseId, optionId, cardId));
                        }

                        optionNumber++;
                    }
                }
                else
                {
                    if (newCardType == (int)DeckCardTypeEnum.NumberRange)
                    {
                        if (!Enum.IsDefined(typeof(CardRangeTypeEnum), newRangeType))
                        {
                            Log.Error(ErrorMessage.DeckInvalidRangeType);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.DeckInvalidRangeType);
                            response.ErrorMessage = ErrorMessage.DeckInvalidRangeType;
                        }

                        if (string.IsNullOrEmpty(newMinRangeLabel))
                        {
                            Log.Error(ErrorMessage.DeckMinRangeLabelIsEmpty);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.DeckMinRangeLabelIsEmpty);
                            response.ErrorMessage = ErrorMessage.DeckMinRangeLabelIsEmpty;
                        }

                        if (string.IsNullOrEmpty(newMaxRangeLabel))
                        {
                            Log.Error(ErrorMessage.DeckMaxRangeLabelIsEmpty);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.DeckMaxRangeLabelIsEmpty);
                            response.ErrorMessage = ErrorMessage.DeckMaxRangeLabelIsEmpty;
                        }
                    }
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" },
                    new List<string> {"card_type", "question_type", "title", "description", "image_url", "ordering", "logic_level", "number_of_options",
                        "option_1_id", "option_1_content", "option_1_next_card_id",
                        "option_2_id", "option_2_content", "option_2_next_card_id",
                        "option_3_id", "option_3_content", "option_3_next_card_id",
                        "option_4_id", "option_4_content", "option_4_next_card_id",
                        "option_5_id", "option_5_content", "option_5_next_card_id",
                        "option_6_id", "option_6_content", "option_6_next_card_id",
                        "option_7_id", "option_7_content", "option_7_next_card_id",
                        "option_8_id", "option_8_content", "option_8_next_card_id",
                        "range_type", "max_range_label", "min_range_label",
                        "start_timestamp", "end_timestamp", "duration_day", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newCardType, newQuestionType, newTitle, newDescription, newImageUrl, currentOrdering, currentLogicLevel, newNumberOfOptions,
                                           option1Id, option1Content, option1NextCardId,
                                           option2Id, option2Content, option2NextCardId,
                                           option3Id, option3Content, option3NextCardId,
                                           option4Id, option4Content, option4NextCardId,
                                           option5Id, option5Content, option5NextCardId,
                                           option6Id, option6Content, option6NextCardId,
                                           option7Id, option7Content, option7NextCardId,
                                           option8Id, option8Content, option8NextCardId,
                                           newRangeType, newMaxRangeLabel, newMinRangeLabel,
                                           newStartDate, deckEndDate, durationPerCard, adminUserId, currentTime, adminUserId, currentTime, deckId, cardId));

                // Sorting
                if (deckStatus == (int)PulseStatusEnum.Active)
                {
                    // Must be the base of the logic cards which is 1
                    if (currentLogicLevel <= 1 && currentStartDate != newStartDate)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                        deleteBatch.Add(ps.Bind(companyId, isPrioritized, (int)PulsePriorityEnum.Dynamic, currentStartDate, cardId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp",
                            new List<string> { "pulse_id", "company_id", "deck_id", "pulse_priority", "pulse_type", "is_prioritized", "start_timestamp" }));
                        updateBatch.Add(ps.Bind(cardId, companyId, deckId, (int)PulsePriorityEnum.Dynamic, (int)PulseTypeEnum.Dynamic, isPrioritized, newStartDate));
                    }
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                // Delete image from S3
                if (!string.IsNullOrEmpty(currentImageUrl) && !currentImageUrl.Equals(newImageUrl))
                {
                    DeleteFromS3(companyId, cardId, newImageUrl);
                }

                //response.Pulse = SelectCard(deckId, cardId, null, publishMethodType, false, false, null, false, false, false, null, session).Pulse;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse UpdateDynamicCardStartDate(string updateCardId,
                                                              string deckId,
                                                              string adminUserId,
                                                              string companyId,
                                                              DateTime newStartDate,
                                                              bool isApplyToAllLaterCards)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, session);
                if (deckRow == null)
                {
                    Log.Error("Pulse deck does not exists: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                Row pulseRow = vh.ValidatePulseDeckCard(deckId, updateCardId, session);
                if (pulseRow == null)
                {
                    Log.Error("Pulse deck card does not exists: " + updateCardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                // Date variable
                DateTime deckStartTimestamp = deckRow.GetValue<DateTime>("start_timestamp");
                DateTime? deckEndDate = deckRow.GetValue<DateTime?>("end_timestamp");

                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = pulseRow.GetValue<DateTime>("start_timestamp");
                DateTime? currentEndDate = null;

                // No changes at all
                if (currentStartDate == newStartDate)
                {
                    response.Success = true;
                    return response;
                }

                int deckStatus = deckRow.GetValue<int>("status");
                bool isPrioritized = deckRow.GetValue<bool>("is_prioritized");

                int durationPerCard = deckRow.GetValue<int>("duration_per_card");
                int publishMethodType = deckRow.GetValue<int>("publish_method_type");

                if (publishMethodType == (int)PublishMethodTypeEnum.Routine)
                {
                    int currentPerTimeFrameType = deckRow.GetValue<int>("per_time_frame_type");
                    int currentPeriodFrameType = deckRow.GetValue<int>("period_frame_type");
                    int currentNumberOfCards = deckRow.GetValue<int>("number_of_cards_per_time_frame");

                    currentEndDate = currentStartDate.AddDays(pulseRow.GetValue<int>("duration_day"));

                    int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);
                    if (progress != (int)ProgressStatusEnum.Upcoming)
                    {
                        Log.Error(ErrorMessage.PulseCannotBeEdited);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseCannotBeEdited);
                        response.ErrorMessage = ErrorMessage.PulseCannotBeEdited;
                        return response;
                    }

                    int numberOfCardsPerTimeFrame = deckRow.GetValue<int>("number_of_cards_per_time_frame");
                    int perTimeFrameType = deckRow.GetValue<int>("per_time_frame_type");

                    newStartDate = newStartDate.Date;
                    newStartDate = newStartDate.AddHours(deckStartTimestamp.Hour);
                    newStartDate = newStartDate.AddMinutes(deckStartTimestamp.Minute);

                    // Guidelines does not apply here
                    //ps = session.Prepare(CQLGenerator.CountStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "start_timestamp" }));
                    //int count = (int)session.Execute(ps.Bind(deckId, newStartDate)).FirstOrDefault().GetValue<long>("count");

                    //if (count + 1 > numberOfCardsPerTimeFrame)
                    //{
                    //    Log.Error(ErrorMessage.DeckInvalidRoutineDateForCard);
                    //    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalidRoutineDateForCard);
                    //    response.ErrorMessage = ErrorMessage.DeckInvalidRoutineDateForCard;
                    //    return response;
                    //}

                    PulseValidationResponse dateResponse = ValidateDate(newStartDate, deckEndDate, currentTime);
                    if (!dateResponse.Success)
                    {
                        Log.Error(dateResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                        response.ErrorMessage = dateResponse.ErrorMessage;
                        return response;
                    }

                    float ordering = pulseRow.GetValue<float>("ordering");
                    if (ordering.ToString().Contains("."))
                    {
                        Log.Error(ErrorMessage.DeckSubCardCannotChangeStartDate);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckSubCardCannotChangeStartDate);
                        response.ErrorMessage = ErrorMessage.DeckSubCardCannotChangeStartDate;
                        return response;
                    }

                    bool isCustomized = isApplyToAllLaterCards ? false : true;

                    List<PulseDynamic> allCards = SelectAllCards(deckId, adminUserId, companyId, null, true, false, false, session, deckRow).Pulses;
                    PulseDynamic currentCard = allCards.FirstOrDefault(c => c.PulseId.Equals(updateCardId));

                    if (currentCard != null)
                    {
                        // Set all updatedDate to startDate
                        foreach (PulseDynamic card in allCards)
                        {
                            card.UpdatedStartDate = card.StartDate;
                        }

                        int currentOrdering = StripAlphabetForNumber(currentCard.Ordering);
                        int currentIndex = allCards.FindIndex(c => c.PulseId.Equals(updateCardId));

                        // Only contains logic card without the first logic level card
                        List<PulseDynamic> currentCardLogicSet = new List<PulseDynamic>();

                        currentCard.UpdatedStartDate = newStartDate;

                        if (currentCard.LogicLevel == 1)
                        {
                            currentCardLogicSet = allCards.Where(c => c.FloatOrdering > currentOrdering && c.FloatOrdering < currentOrdering + 1).ToList();

                            foreach (PulseDynamic card in currentCardLogicSet)
                            {
                                card.UpdatedStartDate = newStartDate;
                            }
                        }

                        if (isApplyToAllLaterCards)
                        {
                            // Does not apply to current logic set
                            double timeOffset = DateHelper.SelectTimeOffsetForCompany(companyId, session);
                            for (int index = currentIndex + 1; index < allCards.Count; index++)
                            {
                                PulseDynamic nextCard = allCards[index];
                                if (currentOrdering != StripAlphabetForNumber(nextCard.Ordering) && !nextCard.IsCustomized)
                                {
                                    nextCard.UpdatedStartDate = GeneratePulseStartTimeForUpdateApplyToAll(currentPerTimeFrameType, currentPeriodFrameType, timeOffset, newStartDate, nextCard.StartDate, session);
                                }
                            }
                        }


                        allCards = allCards.OrderBy(c => c.UpdatedStartDate).ThenBy(c => c.FloatOrdering).ToList();
                        int newIndex = allCards.FindIndex(c => c.PulseId.Equals(updateCardId));

                        bool isAdd = true;
                        int fromIndex = 0;
                        int toIndex = 0;

                        if (currentIndex > newIndex)
                        {
                            fromIndex = newIndex;
                            toIndex = currentIndex + 1;
                        }
                        else if (newIndex > currentIndex)
                        {
                            isAdd = false;
                            fromIndex = currentIndex;
                            toIndex = newIndex + 1;
                        }

                        // There is change in ordering
                        if (currentIndex != newIndex)
                        {
                            // Need to add logic card set count to the index
                            toIndex += currentCardLogicSet.Count();

                            float startOrdering = 1.0f;
                            float parentOrdering = 1.0f;

                            if (fromIndex - 1 >= 0)
                            {
                                PulseDynamic beforeFirstUpdatingCard = allCards[fromIndex - 1];
                                startOrdering = StripAlphabetForNumber(beforeFirstUpdatingCard.Ordering);
                            }

                            // Include current card
                            for (int index = fromIndex; index < toIndex; index++)
                            {
                                PulseDynamic updatingCard = allCards[index];
                                string updatingCardId = updatingCard.PulseId;

                                float updatingCardOrdering = ConvertStringToOrdering(updatingCard.Ordering);

                                float newCardOrdering = 0.0f;

                                if (currentCardLogicSet.FirstOrDefault(c => c.PulseId.Equals(updatingCard.PulseId)) != null)
                                {
                                    string currentNumberString = StripAlphabetForNumber(updatingCard.Ordering).ToString();
                                    newCardOrdering = ConvertStringToOrdering(updatingCard.Ordering.Replace(currentNumberString, parentOrdering.ToString()));
                                }
                                else
                                {
                                    // Update current updated card
                                    if (updatingCardId.Equals(updateCardId))
                                    {
                                        newCardOrdering = startOrdering;
                                    }
                                    else
                                    {
                                        newCardOrdering = isAdd ? updatingCardOrdering + 1 : updatingCardOrdering - 1;
                                        if (newCardOrdering <= 0)
                                        {
                                            newCardOrdering += 1;
                                        }
                                    }
                                }

                                // Get on going ordering without logic
                                if (updatingCard.LogicLevel <= 1)
                                {
                                    if (updatingCard.LogicLevel == 1)
                                    {
                                        parentOrdering = startOrdering;
                                    }
                                    startOrdering++;
                                }

                                updatingCard.FloatOrdering = newCardOrdering;
                                updatingCard.Ordering = ConvertOrderingToString(newCardOrdering, updatingCard.LogicLevel);

                                ps = session.Prepare(CQLGenerator.DeleteStatement("pulse_dynamic_order", new List<string> { "deck_id", "ordering", "pulse_id" }));
                                deleteBatch.Add(ps.Bind(deckId, updatingCardOrdering, updatingCardId));

                                ps = session.Prepare(CQLGenerator.InsertStatement("pulse_dynamic_order", new List<string> { "deck_id", "ordering", "pulse_id" }));
                                updateBatch.Add(ps.Bind(deckId, newCardOrdering, updatingCardId));

                                ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" }, new List<string> { "start_timestamp", "ordering", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                                updateBatch.Add(ps.Bind(updatingCard.UpdatedStartDate, newCardOrdering, adminUserId, currentTime, deckId, updatingCardId));

                                if (updatingCard.LogicLevel <= 1 && deckStatus == (int)PulseStatusEnum.Active)
                                {
                                    // Update pulse_by_priority_with_timestamp
                                    ps = session.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                                    deleteBatch.Add(ps.Bind(companyId, isPrioritized, (int)PulsePriorityEnum.Dynamic, updatingCard.StartDate, updatingCardId));

                                    ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id", "deck_id", "pulse_type" }));
                                    updateBatch.Add(ps.Bind(companyId, isPrioritized, (int)PulsePriorityEnum.Dynamic, updatingCard.UpdatedStartDate, updatingCardId, deckId, updatingCard.PulseType));
                                }

                                updatingCard.StartDate = updatingCard.UpdatedStartDate;
                            }
                        }
                        else
                        {
                            // fromIndex = 0, toIndex = 0;
                            fromIndex = currentIndex;

                            if (isApplyToAllLaterCards)
                            {
                                toIndex = allCards.Count();
                            }
                            else
                            {
                                // Need to add logic card set count to the index
                                toIndex = currentIndex + 1 + currentCardLogicSet.Count();
                            }


                            // No change in ordering, just timestamp
                            for (int index = fromIndex; index < toIndex; index++)
                            {
                                PulseDynamic updatingCard = allCards[index];
                                string updatingCardId = updatingCard.PulseId;

                                ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" }, new List<string> { "start_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                                updateBatch.Add(ps.Bind(updatingCard.UpdatedStartDate, adminUserId, currentTime, deckId, updatingCardId));

                                if (updatingCard.LogicLevel <= 1 && deckStatus == (int)PulseStatusEnum.Active)
                                {
                                    // Update pulse_by_priority_with_timestamp
                                    ps = session.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                                    deleteBatch.Add(ps.Bind(companyId, isPrioritized, (int)PulsePriorityEnum.Dynamic, updatingCard.StartDate, updatingCardId));

                                    ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id", "deck_id", "pulse_type" }));
                                    updateBatch.Add(ps.Bind(companyId, isPrioritized, (int)PulsePriorityEnum.Dynamic, updatingCard.UpdatedStartDate, updatingCardId, deckId, updatingCard.PulseType));
                                }

                                updatingCard.StartDate = updatingCard.UpdatedStartDate;
                            }
                        }

                        ps = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" }, new List<string> { "is_start_date_customized" }, new List<string>()));
                        updateBatch.Add(ps.Bind(isCustomized, deckId, updateCardId));


                        List<PulseDynamic> updatedCards = allCards;

                        session.Execute(deleteBatch);
                        session.Execute(updateBatch);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseUpdateResponse RemoveChildOptionLogics(string deckId, string pulseId, ISession mainSession = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Going out
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("dynamic_option_to_logic", new List<string>(), new List<string> { "pulse_id" }));
                RowSet logicRowset = mainSession.Execute(ps.Bind(pulseId));

                foreach (Row logicRow in logicRowset)
                {
                    string fromOptionId = logicRow.GetValue<string>("option_id");
                    string toPulseId = logicRow.GetValue<string>("next_pulse_id");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("dynamic_option_from_logic", new List<string> { "pulse_id", "from_option_id", "from_pulse_id" }));
                    deleteBatch.Add(ps.Bind(toPulseId, fromOptionId, pulseId));
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("dynamic_option_to_logic", new List<string> { "pulse_id" }));
                deleteBatch.Add(ps.Bind(pulseId));

                mainSession.Execute(deleteBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseUpdateResponse RemoveParentOptionLogics(string deckId, string childPulseId, string adminUserId, DateTime currentTime, ISession mainSession = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Incoming (from parent)
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("dynamic_option_from_logic", new List<string>(), new List<string> { "pulse_id" }));
                RowSet logicRowset = mainSession.Execute(ps.Bind(childPulseId));

                foreach (Row logicRow in logicRowset)
                {
                    string parentOptionId = logicRow.GetValue<string>("from_option_id");
                    string parentPulseId = logicRow.GetValue<string>("from_pulse_id");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("dynamic_option_to_logic", new List<string> { "pulse_id", "option_id", "next_pulse_id" }));
                    deleteBatch.Add(ps.Bind(parentPulseId, parentOptionId, childPulseId));

                    int parentOptionNumber = Convert.ToInt16(parentOptionId.Replace(string.Format("{0}_", parentPulseId), ""));
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" }, new List<string> { string.Format("option_{0}_next_card_id", parentOptionNumber), "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(null, adminUserId, currentTime, deckId, parentPulseId));
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("dynamic_option_from_logic", new List<string> { "pulse_id" }));
                deleteBatch.Add(ps.Bind(childPulseId));

                mainSession.Execute(updateBatch);
                mainSession.Execute(deleteBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseUpdateResponse RemoveAllChildren(Row fromPulseRow, string deckId, string companyId, Row deckRow, ValidationHandler vh, ISession mainSession)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                int numberOfOption = fromPulseRow.GetValue<int>("number_of_options");
                for (int index = 1; index <= numberOfOption; index++)
                {
                    string nextCardId = fromPulseRow.GetValue<string>(string.Format("option_{0}_next_card_id", index));
                    if (!string.IsNullOrEmpty(nextCardId) && !nextCardId.Equals(DefaultResource.PulseDynamicEndCardId))
                    {
                        DeleteCard(null, companyId, deckId, nextCardId, deckRow, mainSession);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseValidationResponse ValidateOptionContent(int numberOfOptions, List<DeckCardOption> options, bool hasLogic = false)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                if (numberOfOptions < 2 || numberOfOptions > options.Count())
                {
                    Log.Error(ErrorMessage.DeckInvalidOptions);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DeckInvalidOptions);
                    response.ErrorMessage = ErrorMessage.DeckInvalidOptions;
                }

                foreach (DeckCardOption option in options)
                {
                    if (string.IsNullOrEmpty(option.Content))
                    {
                        Log.Error(ErrorMessage.DeckOptionIsEmpty);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.DeckOptionIsEmpty);
                        response.ErrorMessage = ErrorMessage.DeckOptionIsEmpty;
                        return response;
                    }

                    if (option.NextPulse == null)
                    {
                        option.NextPulse = new PulseDynamic();
                        option.NextPulse.PulseId = null;
                    }
                    else
                    {
                        if (hasLogic)
                        {
                            #region For Postman Creation
                            if (string.IsNullOrEmpty(option.NextPulse.PulseId))
                            {
                                option.NextPulse.PulseId = UUIDGenerator.GenerateUniqueIDForPulseDynamic();
                            }
                            #endregion
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PulseSelectDeckResponse SelectDeck(string deckId,
                                                  string companyId,
                                                  int query,
                                                  double timezoneOffset,
                                                  string containsText = null,
                                                  bool isCheckForLive = false,
                                                  bool isPrivacyRequired = false,
                                                  int progress = 0,
                                                  bool isOrderByAscending = true,
                                                  bool isCheckStartDate = false,
                                                  ISession mainSession = null,
                                                  Row deckRow = null)
        {
            PulseSelectDeckResponse response = new PulseSelectDeckResponse();
            response.Deck = null;
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;

                if (deckRow == null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                    if (deckRow == null)
                    {
                        Log.Error("Invalid deck: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                        response.ErrorMessage = ErrorMessage.DeckInvalid;
                        return response;
                    }
                }

                bool isForEveryone = false;
                bool isForDepartment = false;
                bool isForUser = false;

                List<Department> selectedDepartments = new List<Department>();
                List<User> selectedUsers = new List<User>();

                if (isPrivacyRequired)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("deck_privacy", new List<string>(), new List<string> { "deck_id" }));
                    Row privacyRow = mainSession.Execute(ps.Bind(deckId)).FirstOrDefault();

                    isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                    isForDepartment = privacyRow.GetValue<bool>("is_for_department");
                    isForUser = privacyRow.GetValue<bool>("is_for_user");

                    if (!isForEveryone)
                    {
                        if (isForDepartment)
                        {
                            Department departmentManager = new Department();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("deck_targeted_department", new List<string>(), new List<string> { "deck_id" }));
                            RowSet departmentRowset = mainSession.Execute(ps.Bind(deckId));
                            foreach (Row departmentRow in departmentRowset)
                            {
                                string departmentId = departmentRow.GetValue<string>("department_id");
                                Department department = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;
                                if (department != null)
                                {
                                    selectedDepartments.Add(department);
                                }
                            }
                        }

                        if (isForUser)
                        {
                            User userManager = new User();
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("deck_targeted_user", new List<string>(), new List<string> { "deck_id" }));
                            RowSet userRowset = mainSession.Execute(ps.Bind(deckId));
                            foreach (Row userRow in userRowset)
                            {
                                string userId = userRow.GetValue<string>("user_id");
                                User user = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                                if (user != null)
                                {
                                    selectedUsers.Add(user);
                                }
                            }
                        }
                    }
                }

                int status = deckRow.GetValue<int>("status");

                if (status == (int)PulseStatusEnum.Deleted)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                DateTime startDate = deckRow.GetValue<DateTime>("start_timestamp");
                DateTime? endDate = deckRow.GetValue<DateTime?>("end_timestamp");

                if (progress == 0)
                {
                    progress = SelectProgress(startDate, endDate, currentTime);
                }

                int numberOfCards = 0;
                string title = deckRow.GetValue<string>("title");
                bool isComplusory = deckRow.GetValue<bool>("is_complusory");
                bool isAnonymous = deckRow.GetValue<bool>("is_anonymous");
                int anonymityCount = 0;
                if (deckRow.IsNull("anonymity_count"))
                {
                    if (isAnonymous)
                    {
                        anonymityCount = 1;
                    }

                    #region overwrite the value of anonymity_count when anonymity_count is null.
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck", new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                    BatchStatement updateBatch = new BatchStatement();
                    updateBatch.Add(ps.Bind(anonymityCount, companyId, deckId));
                    mainSession.Execute(updateBatch);
                    #endregion
                }
                else
                {
                    anonymityCount = deckRow.GetValue<int>("anonymity_count");
                }

                bool isPrioritized = deckRow.GetValue<bool>("is_prioritized");
                int publishMethodType = deckRow.GetValue<int>("publish_method_type");
                int numberOfCardsPerTimeFrame = deckRow.GetValue<int>("number_of_cards_per_time_frame");
                int perTimeFrameType = deckRow.GetValue<int>("per_time_frame_type");
                int periodFrameType = deckRow.GetValue<int>("period_frame_type");
                int duration = deckRow.GetValue<int>("duration_per_card");

                List<PulseDynamic> pulses = new List<PulseDynamic>();
                if (query == (int)PulseQueryEnum.Basic)
                {
                    ps = mainSession.Prepare(CQLGenerator.CountStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id" }));
                    numberOfCards = (int)mainSession.Execute(ps.Bind(deckId)).FirstOrDefault().GetValue<long>("count");
                }
                else
                {
                    pulses = SelectAllCards(deckId, null, companyId, containsText, isOrderByAscending, isCheckStartDate, isCheckForLive, mainSession, deckRow).Pulses;
                    numberOfCards = pulses.Count();
                }

                PulseDeck deck = new PulseDeck
                {
                    Id = deckId,
                    Title = title,
                    NumberOfCards = numberOfCards,
                    DeckStartTimestamp = new DateTime(startDate.Ticks, DateTimeKind.Utc),
                    DeckEndTimestamp = endDate == null ? null : new DateTime?(new DateTime(endDate.Value.Ticks, DateTimeKind.Utc)),
                    IsCompulsory = isComplusory,
                    IsDeckAnonymous = isAnonymous,
                    AnonymityCount = anonymityCount,
                    IsPrioritized = isPrioritized,
                    PublishMethodType = publishMethodType,
                    NumberOfCardsPerTimeFrame = numberOfCardsPerTimeFrame,
                    PerTimeFrameType = perTimeFrameType,
                    PeriodFrameType = periodFrameType,
                    Status = status,
                    IsForEveryone = isForEveryone,
                    IsForDepartment = isForDepartment,
                    IsForUser = isForUser,
                    TargetedDepartments = selectedDepartments,
                    TargetedUsers = selectedUsers,
                    Progress = progress,
                    Pulses = pulses,
                    Duration = duration,
                    DeckStartTimestampString = startDate.AddHours(timezoneOffset).ToString("dd/MM/yyyy"),
                    DeckEndTimestampString = endDate == null ? string.Empty : endDate.Value.AddHours(timezoneOffset).ToString("dd/MM/yyyy")
                };

                response.Deck = deck;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectDeckResponse SelectFullDetailDeck(string adminUserId, string companyId, string deckId, bool isOrderedByAscending, string containsText = null, ISession session = null)
        {
            PulseSelectDeckResponse response = new PulseSelectDeckResponse();
            response.Deck = new PulseDeck();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                }

                response = SelectDeck(deckId, companyId, (int)PulseQueryEnum.FullDetail, DateHelper.SelectTimeOffsetForCompany(companyId, session), containsText, false, true, 0, isOrderedByAscending, false, session, null);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectDeckAllCardsResponse SelectAllCards(string deckId, string adminUserId, string companyId, string containsText = null, bool isOrderByAscending = true, bool isCheckForStartDate = false, bool isCheckForLive = false, ISession session = null, Row deckRow = null)
        {
            PulseSelectDeckAllCardsResponse response = new PulseSelectDeckAllCardsResponse();
            response.Pulses = new List<PulseDynamic>();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    deckRow = vh.ValidatePulseDeck(companyId, deckId, session);
                    if (deckRow == null)
                    {
                        Log.Error("Invalid deck: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                        response.ErrorMessage = ErrorMessage.DeckInvalid;
                        return response;
                    }
                }

                int deckPublishMethod = deckRow.GetValue<int>("publish_method_type");

                int anonymityCount = 0;
                if (deckRow.IsNull("anonymity_count"))
                {
                    if (deckRow.GetValue<bool>("is_anonymous"))
                    {
                        anonymityCount = 1;

                        #region Update null value of anonymity_count column.
                        PreparedStatement psUpdate = session.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                          new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                        BatchStatement bsUpdate = new BatchStatement();
                        bsUpdate.Add(psUpdate.Bind(anonymityCount, deckRow.GetValue<bool>("company_id"), deckRow.GetValue<bool>("id")));
                        session.Execute(bsUpdate);
                        #endregion
                    }
                }
                else
                {
                    anonymityCount = deckRow.GetValue<int>("anonymity_count");
                }

                // Select card details
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_order", new List<string>(), new List<string> { "deck_id" }));
                RowSet cardRowset = session.Execute(ps.Bind(deckId));

                PulseDynamic parentCard = null;
                PulseDynamic grandParentCard = null;

                foreach (Row cardRow in cardRowset)
                {
                    string cardId = cardRow.GetValue<string>("pulse_id");
                    float ordering = cardRow.GetValue<float>("ordering");

                    PulseDynamic selectedCard = SelectCard(deckId, cardId, null, deckPublishMethod, false, anonymityCount, isCheckForStartDate, isCheckForLive, false, null, session).Pulse;

                    if (selectedCard != null)
                    {
                        if (selectedCard.LogicLevel == 0)
                        {
                            parentCard = null;
                            grandParentCard = null;
                        }
                        else
                        {
                            if (selectedCard.LogicLevel == 1)
                            {
                                grandParentCard = selectedCard;
                            }
                            else if (selectedCard.LogicLevel == 2)
                            {
                                parentCard = selectedCard;
                            }
                        }

                        if (!string.IsNullOrEmpty(containsText))
                        {
                            if (selectedCard.ContainsText.FirstOrDefault(t => t.ToLower().Contains(containsText.ToLower())) != null)
                            {
                                if (grandParentCard != null && !response.Pulses.Contains(grandParentCard))
                                {
                                    response.Pulses.Add(grandParentCard);
                                }

                                if (parentCard != null && !response.Pulses.Contains(parentCard))
                                {
                                    response.Pulses.Add(parentCard);
                                }

                                if (!response.Pulses.Contains(selectedCard))
                                {
                                    response.Pulses.Add(selectedCard);
                                }

                            }
                        }
                        else
                        {
                            if (!response.Pulses.Contains(selectedCard))
                            {
                                response.Pulses.Add(selectedCard);
                            }
                        }

                    }
                }

                if (!isOrderByAscending)
                {
                    response.Pulses = response.Pulses.OrderByDescending(p => p.FloatOrdering).ToList();

                    List<PulseDynamic> logicPulses = new List<PulseDynamic>();
                    List<PulseDynamic> tempPulses = new List<PulseDynamic>();

                    foreach (PulseDynamic pulse in response.Pulses)
                    {
                        if (alphabetList.Any(s => pulse.Ordering.Contains(s)))
                        {
                            logicPulses.Add(pulse);
                        }
                        else
                        {
                            tempPulses.Add(pulse);
                            if (logicPulses.Count > 0)
                            {
                                logicPulses = logicPulses.OrderBy(p => p.Ordering).ToList();
                                tempPulses.AddRange(logicPulses);
                                logicPulses = new List<PulseDynamic>();
                            }
                        }
                    }

                    response.Pulses = tempPulses;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PulseDeckSelectCardResponse SelectCard(string deckId,
                                                     string pulseId,
                                                     DateTime? deckEndDate,
                                                     int deckPublishMethod,
                                                     bool isDeckComplusory = false,
                                                     int deckAnonymityCount = 0,
                                                     bool isCheckStartDate = false,
                                                     bool isCheckForLive = false,
                                                     bool isCheckCompleted = false,
                                                     string answeredByUserId = null,
                                                     ISession mainSession = null)
        {
            PulseDeckSelectCardResponse response = new PulseDeckSelectCardResponse();
            response.Pulse = null;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (mainSession == null)
                {
                    ISession session = cm.getMainSession();
                }

                PreparedStatement ps = null;
                int pulseType = (int)PulseTypeEnum.Dynamic;

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck", new List<string>(), new List<string> { "deck_id", "id" }));
                Row cardRow = mainSession.Execute(ps.Bind(deckId, pulseId)).FirstOrDefault();
                if (cardRow != null)
                {
                    List<string> containsText = new List<string>();

                    int cardType = cardRow.GetValue<int>("card_type");

                    if (isCheckCompleted)
                    {
                        ISession analyticSession = cm.getAnalyticSession();
                        if (new AnalyticPulse().CheckDynamicCardCompleted(answeredByUserId, pulseId, cardRow, analyticSession, mainSession, deckId))
                        {
                            return response;
                        }
                    }

                    int questionType = cardRow.GetValue<int>("question_type");
                    string title = cardRow.GetValue<string>("title");
                    containsText.Add(title);

                    string description = cardRow.GetValue<string>("description");
                    containsText.Add(description);

                    string imageUrl = cardRow.GetValue<string>("image_url") == null ? string.Empty : cardRow.GetValue<string>("image_url");
                    float ordering = (float)Convert.ToDouble(cardRow.GetValue<float>("ordering").ToString(string.Format("N{0}", DefaultResource.PulseDynamicMaxLogicLevel)));
                    DateTime startDate = new DateTime(cardRow.GetValue<DateTime>("start_timestamp").Ticks, DateTimeKind.Utc);
                    int durationPerCard = cardRow["duration_day"] == null ? 0 : cardRow.GetValue<int>("duration_day");
                    bool isCustomized = cardRow["is_start_date_customized"] == null ? false : cardRow.GetValue<bool>("is_start_date_customized");
                    DateTime? endDate = deckEndDate;

                    if (isCheckStartDate)
                    {
                        if (durationPerCard != -1)
                        {
                            if (DateTime.UtcNow < startDate.AddDays(durationPerCard))
                            {
                                return response;
                            }
                        }
                    }

                    if (isCheckForLive)
                    {
                        if (deckPublishMethod == (int)PublishMethodTypeEnum.Routine)
                        {
                            if (durationPerCard != -1)
                            {
                                endDate = startDate.AddDays(durationPerCard);
                            }
                            else
                            {
                                endDate = null;
                            }

                        }

                        int progress = SelectProgress(startDate, endDate, DateTime.UtcNow);

                        if (progress != (int)ProgressStatusEnum.Live)
                        {
                            return response;
                        }
                    }

                    int numberOfOptions = 0;

                    string option1Id = null;
                    string option2Id = null;
                    string option3Id = null;
                    string option4Id = null;
                    string option5Id = null;
                    string option6Id = null;
                    string option7Id = null;
                    string option8Id = null;

                    string option1Content = null;
                    string option2Content = null;
                    string option3Content = null;
                    string option4Content = null;
                    string option5Content = null;
                    string option6Content = null;
                    string option7Content = null;
                    string option8Content = null;

                    string option1NextCardId = null;
                    string option2NextCardId = null;
                    string option3NextCardId = null;
                    string option4NextCardId = null;
                    string option5NextCardId = null;
                    string option6NextCardId = null;
                    string option7NextCardId = null;
                    string option8NextCardId = null;

                    int rangeType = 0;
                    string minRangeLabel = string.Empty;
                    string maxRangeLabel = string.Empty;

                    List<string> nextCardIds = new List<string>();
                    bool hasIncomingLogic = false;
                    bool hasOutgoingLogic = false;

                    // Sort out ordering
                    int logicLevel = cardRow.GetValue<int>("logic_level");
                    string orderString = ConvertOrderingToString(ordering, logicLevel);

                    if (cardType == (int)DeckCardTypeEnum.SelectOne || cardType == (int)DeckCardTypeEnum.SelectOneWithLogic)
                    {
                        numberOfOptions = cardRow.GetValue<int>("number_of_options");
                        option1Id = cardRow.GetValue<string>("option_1_id");
                        option2Id = cardRow.GetValue<string>("option_2_id");
                        option3Id = cardRow.GetValue<string>("option_3_id");
                        option4Id = cardRow.GetValue<string>("option_4_id");
                        option5Id = cardRow.GetValue<string>("option_5_id");
                        option6Id = cardRow.GetValue<string>("option_6_id");
                        option7Id = cardRow.GetValue<string>("option_7_id");
                        option8Id = cardRow.GetValue<string>("option_8_id");

                        option1Content = cardRow.GetValue<string>("option_1_content");
                        containsText.Add(option1Content);
                        option2Content = cardRow.GetValue<string>("option_2_content");
                        containsText.Add(option2Content);
                        option3Content = cardRow.GetValue<string>("option_3_content");
                        containsText.Add(option3Content);
                        option4Content = cardRow.GetValue<string>("option_4_content");
                        containsText.Add(option4Content);
                        option5Content = cardRow.GetValue<string>("option_5_content");
                        containsText.Add(option5Content);
                        option6Content = cardRow.GetValue<string>("option_6_content");
                        containsText.Add(option6Content);
                        option7Content = cardRow.GetValue<string>("option_7_content");
                        containsText.Add(option7Content);
                        option8Content = cardRow.GetValue<string>("option_8_content");
                        containsText.Add(option8Content);

                        if (cardType == (int)DeckCardTypeEnum.SelectOne)
                        {
                            option1NextCardId = cardRow.GetValue<string>("option_1_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_1_next_card_id");
                            option2NextCardId = cardRow.GetValue<string>("option_2_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_2_next_card_id");
                            option3NextCardId = cardRow.GetValue<string>("option_3_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_3_next_card_id");
                            option4NextCardId = cardRow.GetValue<string>("option_4_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_4_next_card_id");
                            option5NextCardId = cardRow.GetValue<string>("option_5_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_5_next_card_id");
                            option6NextCardId = cardRow.GetValue<string>("option_6_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_6_next_card_id");
                            option7NextCardId = cardRow.GetValue<string>("option_7_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_7_next_card_id");
                            option8NextCardId = cardRow.GetValue<string>("option_8_next_card_id") == null ? string.Empty : cardRow.GetValue<string>("option_8_next_card_id");
                        }
                        else
                        {
                            option1NextCardId = cardRow.GetValue<string>("option_1_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_1_next_card_id");
                            option2NextCardId = cardRow.GetValue<string>("option_2_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_2_next_card_id");
                            option3NextCardId = cardRow.GetValue<string>("option_3_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_3_next_card_id");
                            option4NextCardId = cardRow.GetValue<string>("option_4_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_4_next_card_id");
                            option5NextCardId = cardRow.GetValue<string>("option_5_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_5_next_card_id");
                            option6NextCardId = cardRow.GetValue<string>("option_6_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_6_next_card_id");
                            option7NextCardId = cardRow.GetValue<string>("option_7_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_7_next_card_id");
                            option8NextCardId = cardRow.GetValue<string>("option_8_next_card_id") == null ? DefaultResource.PulseDynamicEndCardId : cardRow.GetValue<string>("option_8_next_card_id");

                            if (logicLevel != Convert.ToInt16(DefaultResource.PulseDynamicMaxLogicLevel))
                            {
                                nextCardIds = new List<string>
                                {
                                    option1NextCardId, option2NextCardId, option3NextCardId, option4NextCardId, option5NextCardId, option6NextCardId, option7NextCardId, option8NextCardId
                                };
                            }
                        }
                    }
                    else if (cardType == (int)DeckCardTypeEnum.NumberRange)
                    {
                        rangeType = cardRow.GetValue<int>("range_type");
                        minRangeLabel = cardRow.GetValue<string>("min_range_label");
                        maxRangeLabel = cardRow.GetValue<string>("max_range_label");
                    }

                    // Check incoming and outgoing logic
                    switch (logicLevel)
                    {
                        case 1:
                            if (logicLevel != Convert.ToInt16(DefaultResource.PulseDynamicMaxLogicLevel))
                            {
                                foreach (string nextCardId in nextCardIds)
                                {
                                    if (!nextCardId.Equals(DefaultResource.PulseDynamicEndCardId))
                                    {
                                        hasOutgoingLogic = true;
                                        break;
                                    }
                                }
                            }
                            break;
                        case 2:
                            hasIncomingLogic = true;
                            if (logicLevel != Convert.ToInt16(DefaultResource.PulseDynamicMaxLogicLevel))
                            {
                                foreach (string nextCardId in nextCardIds)
                                {
                                    if (!nextCardId.Equals(DefaultResource.PulseDynamicEndCardId))
                                    {
                                        hasOutgoingLogic = true;
                                        break;
                                    }
                                }
                            }
                            break;
                        case 3:
                            hasIncomingLogic = true;
                            hasOutgoingLogic = false;
                            break;
                    }

                    containsText.RemoveAll(text => string.IsNullOrEmpty(text));

                    #region Get AnonymityCount.

                    #endregion

                    response.Pulse = new PulseDynamic()
                    {
                        DeckId = deckId,
                        CardType = cardType,
                        PulseId = pulseId,
                        PulseType = pulseType,
                        QuestionType = questionType,
                        Title = title,
                        Description = description,
                        ImageUrl = imageUrl,
                        Ordering = orderString,
                        FloatOrdering = ordering,
                        LogicLevel = logicLevel,

                        NumberOfOptions = numberOfOptions,

                        Option1Id = option1Id,
                        Option1Content = option1Content,
                        Option1NextCardId = option1NextCardId,

                        Option2Id = option2Id,
                        Option2Content = option2Content,
                        Option2NextCardId = option2NextCardId,

                        Option3Id = option3Id,
                        Option3Content = option3Content,
                        Option3NextCardId = option3NextCardId,

                        Option4Id = option4Id,
                        Option4Content = option4Content,
                        Option4NextCardId = option4NextCardId,

                        Option5Id = option5Id,
                        Option5Content = option5Content,
                        Option5NextCardId = option5NextCardId,

                        Option6Id = option6Id,
                        Option6Content = option6Content,
                        Option6NextCardId = option6NextCardId,

                        Option7Id = option7Id,
                        Option7Content = option7Content,
                        Option7NextCardId = option7NextCardId,

                        Option8Id = option8Id,
                        Option8Content = option8Content,
                        Option8NextCardId = option8NextCardId,

                        RangeType = rangeType,
                        MinRangeLabel = minRangeLabel,
                        MaxRangeLabel = maxRangeLabel,

                        HasIncomingLogic = hasIncomingLogic,
                        HasOutgoingLogic = hasOutgoingLogic,

                        StartDate = startDate,
                        EndDate = endDate,
                        IsComplusory = isDeckComplusory,
                        IsAnonymous = (deckAnonymityCount > 0),
                        AnonymityCount = deckAnonymityCount,

                        IsCustomized = isCustomized,
                        Actions = SelectActionForPulse((int)PulseTypeEnum.Dynamic, isDeckComplusory),

                        SelectedRange = Convert.ToInt32(DefaultResource.DeckCardRangeNotSelected),

                        ContainsText = containsText
                    };
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PulseUpdateResponse UpdateDeckStatus(string adminUserId, string companyId, string deckId, int updatedStatus, Row deckRow = null, ISession mainSession = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                    if (deckRow == null)
                    {
                        Log.Error("Invalid deck: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                        response.ErrorMessage = ErrorMessage.DeckInvalid;
                        return response;
                    }
                }

                int currentStatus = deckRow.GetValue<int>("status");

                if (currentStatus == (int)PulseStatusEnum.Deleted)
                {
                    Log.Error("Pulse already been deleted");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.PulseAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.PulseAlreadyDeleted;
                    return response;
                }

                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                bool isPrioritized = deckRow.GetValue<bool>("is_prioritized");
                DateTime startTime = deckRow.GetValue<DateTime>("start_timestamp");
                int priority = SelectPulsePriority((int)PulseTypeEnum.Dynamic);

                if (updatedStatus == (int)PulseStatusEnum.Deleted)
                {
                    response = DeleteDeck(adminUserId, companyId, deckId, deckRow, mainSession);
                    if (!response.Success)
                    {
                        return response;
                    }
                }
                else
                {
                    if (updatedStatus == (int)PulseStatusEnum.Hidden)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck", new List<string>(), new List<string> { "deck_id" }));
                        RowSet cardRowSet = mainSession.Execute(ps.Bind(deckId));
                        foreach (Row cardRow in cardRowSet)
                        {
                            string pulseId = cardRow.GetValue<string>("id");
                            DateTimeOffset startDate = cardRow.GetValue<DateTimeOffset>("start_timestamp");

                            ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                            deleteBatch.Add(ps.Bind(companyId, isPrioritized, priority, startDate, pulseId));
                        }
                    }
                    else if (updatedStatus == (int)PulseStatusEnum.Active)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck", new List<string>(), new List<string> { "deck_id" }));
                        RowSet cardRowSet = mainSession.Execute(ps.Bind(deckId));
                        foreach (Row cardRow in cardRowSet)
                        {
                            string pulseId = cardRow.GetValue<string>("id");
                            DateTimeOffset startDate = cardRow.GetValue<DateTimeOffset>("start_timestamp");
                            int cardType = cardRow.GetValue<int>("card_type");

                            bool isStart = false;

                            if (cardType != (int)DeckCardTypeEnum.SelectOneWithLogic)
                            {
                                isStart = cardRow.GetValue<int>("logic_level") == 0 ? true : false;
                            }
                            else
                            {
                                isStart = cardRow.GetValue<int>("logic_level") == 1 ? true : false;
                            }

                            if (isStart)
                            {
                                ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp",
                                    new List<string> { "pulse_id", "company_id", "deck_id", "pulse_priority", "pulse_type", "is_prioritized", "start_timestamp" }));
                                updateBatch.Add(ps.Bind(pulseId, companyId, deckId, (int)PulsePriorityEnum.Dynamic, (int)PulseTypeEnum.Dynamic, isPrioritized, startDate));
                            }
                        }
                    }

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck", new List<string> { "company_id", "id" }, new List<string> { "status", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(updatedStatus, adminUserId, DateTime.UtcNow, companyId, deckId));
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse ImportDeck(string adminUserId,
                                              string companyId,
                                              string currentDeckId,
                                              List<string> deckIds)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row currentDeckRow = vh.ValidatePulseDeck(companyId, currentDeckId, mainSession);
                if (currentDeckRow == null)
                {
                    Log.Error("Invalid deck: " + currentDeckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                List<Row> importedDeckRows = new List<Row>();
                deckIds.Remove(currentDeckId);
                foreach (string deckId in deckIds)
                {
                    Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                    if (deckRow == null)
                    {
                        Log.Error("Invalid deck: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckImportedDeckIsInvalid);
                        response.ErrorMessage = ErrorMessage.DeckImportedDeckIsInvalid;
                        return response;
                    }

                    importedDeckRows.Add(deckRow);
                }

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                int currentDeckPublishMethod = currentDeckRow.GetValue<int>("publish_method_type");

                int currentPerTimeFrameType = 0;
                int currentPeriodFrameType = 0;
                int currentNumberOfCards = 0;

                DateTime startDeckDate = currentDeckRow.GetValue<DateTime>("start_timestamp");

                if (currentDeckPublishMethod == (int)PublishMethodTypeEnum.Routine)
                {
                    currentPerTimeFrameType = currentDeckRow.GetValue<int>("per_time_frame_type");
                    currentPeriodFrameType = currentDeckRow.GetValue<int>("period_frame_type");
                    currentNumberOfCards = currentDeckRow.GetValue<int>("number_of_cards_per_time_frame");
                }

                foreach (Row importedDeckRow in importedDeckRows)
                {
                    string importedDeckId = importedDeckRow.GetValue<string>("id");
                    PulseDeck fullDeck = SelectDeck(importedDeckId, companyId, (int)PulseQueryEnum.FullDetail, timezoneOffset, null, false, false, 0, true, false, mainSession, importedDeckRow).Deck;

                    // Parent card id
                    int startLogicNumber = 0;
                    string logicLevel1CardId = string.Empty;
                    List<DeckCardOption> logicLevel1Options = new List<DeckCardOption>();

                    string logicLevel2CardId = string.Empty;
                    List<DeckCardOption> logicLevel2Options = new List<DeckCardOption>();

                    List<DeckCardOption> currentOptions = new List<DeckCardOption>();

                    foreach (PulseDynamic pulse in fullDeck.Pulses)
                    {
                        DateTime startDate = new DateTime();
                        DateTime? parentDate = null;
                        string currentCardId = UUIDGenerator.GenerateUniqueIDForPulseDynamic();

                        if (currentDeckPublishMethod == (int)PublishMethodTypeEnum.Routine)
                        {
                            if (pulse.LogicLevel <= 1)
                            {
                                startDate = GeneratePulseStartTimeForImport(currentDeckId, companyId, currentPerTimeFrameType, currentPeriodFrameType, currentNumberOfCards, startDeckDate, mainSession, currentDeckRow);
                            }

                            if (pulse.LogicLevel > 1)
                            {
                                startDate = parentDate.Value;
                            }
                            else
                            {
                                if (pulse.LogicLevel == 1)
                                {
                                    parentDate = startDate;
                                }
                                else if (pulse.LogicLevel == 0)
                                {
                                    parentDate = null;
                                }
                            }

                        }
                        else
                        {
                            startDate = startDeckDate;
                        }

                        // Reset
                        if (startLogicNumber != 0 && StripAlphabetForNumber(pulse.Ordering) != startLogicNumber)
                        {
                            startLogicNumber = 0;
                            logicLevel1CardId = string.Empty;
                            logicLevel1Options = new List<DeckCardOption>();

                            logicLevel2CardId = string.Empty;
                            logicLevel2Options = new List<DeckCardOption>();
                        }

                        if (pulse.CardType == (int)DeckCardTypeEnum.SelectOneWithLogic || pulse.CardType == (int)DeckCardTypeEnum.SelectOne)
                        {
                            currentOptions = new List<DeckCardOption>() {
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option1Id,
                                    Content = pulse.Option1Content,
                                    NextPulseId = pulse.Option1NextCardId
                                },
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option2Id,
                                    Content = pulse.Option2Content,
                                    NextPulseId = pulse.Option2NextCardId
                                },
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option3Id,
                                    Content = pulse.Option3Content,
                                    NextPulseId = pulse.Option3NextCardId
                                },
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option4Id,
                                    Content = pulse.Option4Content,
                                    NextPulseId = pulse.Option4NextCardId
                                },
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option5Id,
                                    Content = pulse.Option5Content,
                                    NextPulseId = pulse.Option5NextCardId
                                },
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option6Id,
                                    Content = pulse.Option6Content,
                                    NextPulseId = pulse.Option6NextCardId
                                },
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option7Id,
                                    Content = pulse.Option7Content,
                                    NextPulseId = pulse.Option7NextCardId
                                },
                                new DeckCardOption
                                {
                                    OptionId = pulse.Option8Id,
                                    Content = pulse.Option8Content,
                                    NextPulseId = pulse.Option8NextCardId
                                }
                            };

                            currentOptions = currentOptions.Take(pulse.NumberOfOptions).ToList();

                            if (pulse.LogicLevel == 1)
                            {
                                startLogicNumber = StripAlphabetForNumber(pulse.Ordering);
                                logicLevel1CardId = currentCardId;
                                logicLevel1Options = currentOptions;
                            }
                            else if (pulse.LogicLevel == 2)
                            {
                                logicLevel2CardId = currentCardId;
                                logicLevel2Options = currentOptions;
                            }
                        }

                        string parentCardId = string.Empty;
                        int parentOptionNumber = 0;

                        // List of option ids
                        if (alphabetList.Any(s => pulse.Ordering.Contains(s)))
                        {
                            // Has logic which needs parent and option id
                            if (pulse.LogicLevel == 2)
                            {
                                // Level 2
                                // Requires level 1
                                parentCardId = logicLevel1CardId;
                                DeckCardOption selectedOption = logicLevel1Options.FirstOrDefault(o => o.NextPulseId == pulse.PulseId);
                                if (selectedOption != null)
                                {
                                    parentOptionNumber = Convert.ToInt16(selectedOption.OptionId.Split('_')[1]);
                                }
                            }
                            else if (pulse.LogicLevel == 3)
                            {
                                // Level 3
                                // Requires level 2
                                parentCardId = logicLevel2CardId;
                                DeckCardOption selectedOption = logicLevel2Options.FirstOrDefault(o => o.NextPulseId == pulse.PulseId);
                                if (selectedOption != null)
                                {
                                    parentOptionNumber = Convert.ToInt16(selectedOption.OptionId.Split('_')[1]);
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(parentCardId) || parentOptionNumber == 0)
                        {
                            parentCardId = string.Empty;
                            parentOptionNumber = 0;
                        }

                        CreateDynamicCard(currentCardId, currentDeckId, adminUserId, companyId, pulse.Title, pulse.Description, pulse.CardType, pulse.QuestionType, startDate, pulse.NumberOfOptions, pulse.RangeType, pulse.ImageUrl, pulse.MinRangeLabel, pulse.MaxRangeLabel, currentOptions, parentCardId, parentOptionNumber, 0, 0, mainSession, currentDeckRow);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse UpdateDeck(string deckId,
                                              string adminUserId,
                                              string companyId,
                                              string newTitle,
                                              bool newIsCompulsory,
                                              int newAnonymityCount,
                                              int newStatus,
                                              DateTime newStartDate,
                                              DateTime? newEndDate,
                                              bool newIsPrioritized,
                                              List<string> newTargetedDepartmentIds,
                                              List<string> newTargetedUserIds,
                                              int newDurationPerCard = 0)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                if (deckRow == null)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                int currentStatus = deckRow.GetValue<int>("status");
                int publishMethodType = deckRow.GetValue<int>("publish_method_type");
                int currentDurationPerCard = deckRow.GetValue<int>("duration_per_card");
                bool currentIsPrioritized = deckRow.GetValue<bool>("is_prioritized");

                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = deckRow.GetValue<DateTime>("start_timestamp");
                DateTime? currentEndDate = deckRow.GetValue<DateTime?>("end_timestamp");

                int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);

                newStartDate = newStartDate.AddSeconds(-newStartDate.Second);
                newStartDate = newStartDate.AddMilliseconds(-newStartDate.Millisecond);

                if (newEndDate.HasValue && newEndDate.Value == DateTime.MinValue)
                {
                    newEndDate = null;
                }

                PulseValidationResponse dateResponse = ValidateDate(newStartDate, newEndDate, currentTime);
                if (!dateResponse.Success)
                {
                    Log.Error(dateResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                    response.ErrorMessage = dateResponse.ErrorMessage;
                    return response;
                }

                PulseValidationResponse validateResponse = ValidateForUpdate(currentStartDate, currentEndDate, newStartDate, newEndDate, currentTime, currentStatus, progress, null, deckId, true);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Deck title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.DeckTitleIsEmpty;
                    return response;
                }

                PulseValidationResponse statusResponse = ValidateStatus(newStatus);
                if (!statusResponse.Success)
                {
                    Log.Error(statusResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(statusResponse.ErrorCode);
                    response.ErrorMessage = statusResponse.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                List<PulseDynamic> cards = SelectAllCards(deckId, adminUserId, companyId, null, true, false, false, mainSession, deckRow).Pulses;

                if (publishMethodType == (int)PublishMethodTypeEnum.Schedule)
                {
                    if (progress == (int)ProgressStatusEnum.Completed && currentStatus != (int)PulseStatusEnum.Unlisted)
                    {
                        Log.Error("Deck already completed. DeckId: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckProgressCompleted);
                        response.ErrorMessage = ErrorMessage.DeckProgressCompleted;
                        return response;
                    }
                    else
                    {
                        foreach (BoundStatement bs in UpdateTimestampForScheduleCards(adminUserId, deckId, cards, newStartDate, newEndDate, currentTime, mainSession))
                        {
                            updateBatch.Add(bs);
                        }
                    }

                    newDurationPerCard = 0;
                }
                else
                {
                    newEndDate = null;

                    if (currentDurationPerCard != newDurationPerCard)
                    {
                        PulseValidationResponse validateDurationPerCard = ValidateDurationPerCard(newDurationPerCard);
                        if (!validateDurationPerCard.Success)
                        {
                            response.ErrorCode = validateDurationPerCard.ErrorCode;
                            response.ErrorMessage = validateDurationPerCard.ErrorMessage;
                            return response;
                        }

                        // Update pulse duration
                        List<BoundStatement> updateCardDurationStatements = new List<BoundStatement>();
                        List<PulseDynamic> updatedCards = new List<PulseDynamic>();
                        foreach (PulseDynamic card in cards)
                        {
                            if (card.EndDate.HasValue)
                            {
                                if (card.EndDate.Value > currentTime)
                                {
                                    updatedCards.Add(card);
                                }
                            }
                        }

                        foreach (BoundStatement bs in UpdateDurationForRoutineCards(adminUserId, deckId, newDurationPerCard, updatedCards, currentTime, mainSession))
                        {
                            updateBatch.Add(bs);
                        }
                    }
                }

                if (currentStatus == (int)PulseStatusEnum.Active && (newStartDate != currentStartDate && publishMethodType == (int)PublishMethodTypeEnum.Schedule) || currentIsPrioritized != newIsPrioritized)
                {
                    UpdatePulsePriority(companyId, deckId, publishMethodType, cards, newIsPrioritized, currentIsPrioritized, mainSession);
                }

                if (newStatus != currentStatus)
                {
                    UpdateDeckStatus(adminUserId, companyId, deckId, newStatus, deckRow, mainSession);
                }

                // Hard code to true always
                bool isCompulsory = true;

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                    new List<string> { "company_id", "id" }, new List<string> { "is_complusory", "duration_per_card", "is_anonymous", "title", "status", "start_timestamp", "end_timestamp", "is_prioritized", "last_modified_by_admin_id", "last_modified_timestamp", "anonymity_count" }, new List<string>()));
                updateBatch.Add(ps.Bind(isCompulsory, newDurationPerCard, (newAnonymityCount > 0), newTitle, newStatus, newStartDate, newEndDate, newIsPrioritized, adminUserId, currentTime,
                    newAnonymityCount, companyId, deckId));

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck_by_company",
                    new List<string> { "company_id", "deck_id" }, new List<string> { "title", "status", "is_prioritized", "start_timestamp", "end_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, newStatus, newIsPrioritized, newStartDate, newEndDate, companyId, deckId));

                if (progress == (int)ProgressStatusEnum.Upcoming || newStatus == (int)PulseStatusEnum.Unlisted)
                {
                    List<BoundStatement> privacyStatement = UpdatePrivacy(deckId, newTargetedDepartmentIds, newTargetedUserIds, mainSession, false);
                    foreach (BoundStatement bs in privacyStatement)
                    {
                        updateBatch.Add(bs);
                    }
                }

                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse UpdateDeck(string deckId,
                                              string adminUserId,
                                              string companyId,
                                              string newTitle,
                                              bool newIsCompulsory,
                                              int newAnonymityCount,
                                              int newPublishMethodType,
                                              int newStatus,
                                              DateTime newStartDate,
                                              DateTime? newEndDate,
                                              bool newIsPrioritized,
                                              List<string> newTargetedDepartmentIds,
                                              List<string> newTargetedUserIds,
                                              int newNumberOfCardsPerTimeFrame = 0,
                                              int newPerTimeFrameType = 0,
                                              int newPeriodFrameType = 0)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                if (deckRow == null)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                int currentStatus = deckRow.GetValue<int>("status");

                DateTime currentTime = DateTime.UtcNow;
                DateTime currentStartDate = deckRow.GetValue<DateTime>("start_timestamp");
                DateTime? currentEndDate = deckRow.GetValue<DateTime?>("end_timestamp");

                int progress = SelectProgress(currentStartDate, currentEndDate, currentTime);

                PulseValidationResponse validateResponse = ValidateForUpdate(currentStartDate, currentEndDate, newStartDate, newEndDate, currentTime, currentStatus, progress, null, deckId, true);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Deck title is missing");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckTitleIsEmpty);
                    response.ErrorMessage = ErrorMessage.DeckTitleIsEmpty;
                    return response;
                }

                if (newPublishMethodType == (int)PublishMethodTypeEnum.Routine)
                {
                    // Do checking
                    PulseValidationResponse routineScheduleResponse = ValidateRoutineSchedule(newPerTimeFrameType, newPeriodFrameType, newNumberOfCardsPerTimeFrame);
                    if (!routineScheduleResponse.Success)
                    {
                        Log.Error(routineScheduleResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(routineScheduleResponse.ErrorCode);
                        response.ErrorMessage = routineScheduleResponse.ErrorMessage;
                        return response;
                    }
                }
                else
                {
                    newNumberOfCardsPerTimeFrame = 0;
                    newPeriodFrameType = 0;
                    newPerTimeFrameType = 0;
                }

                if (newEndDate.HasValue && newEndDate.Value == DateTime.MinValue)
                {
                    newEndDate = null;
                }

                newStartDate = newStartDate.AddSeconds(-newStartDate.Second);
                newStartDate = newStartDate.AddMilliseconds(-newStartDate.Millisecond);
                PulseValidationResponse dateResponse = ValidateDate(newStartDate, newEndDate, currentTime);
                if (!dateResponse.Success)
                {
                    Log.Error(dateResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(dateResponse.ErrorCode);
                    response.ErrorMessage = dateResponse.ErrorMessage;
                    return response;
                }

                PulseValidationResponse statusResponse = ValidateStatus(newStatus);
                if (!statusResponse.Success)
                {
                    Log.Error(statusResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(statusResponse.ErrorCode);
                    response.ErrorMessage = statusResponse.ErrorMessage;
                    return response;
                }

                if (newStatus != currentStatus)
                {
                    UpdateDeckStatus(adminUserId, companyId, deckId, newStatus, deckRow, mainSession);
                }

                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                // Hard code to true always
                bool isCompulsory = true;

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                    new List<string> { "company_id", "id" }, new List<string> { "is_complusory", "publish_method_type", "number_of_cards_per_time_frame", "per_time_frame_type", "period_frame_type", "is_anonymous", "title", "status", "start_timestamp", "end_timestamp", "is_prioritized", "last_modified_by_admin_id", "last_modified_timestamp", "anonymity_count" }, new List<string>()));
                batchStatement.Add(ps.Bind(isCompulsory, newPublishMethodType, newNumberOfCardsPerTimeFrame, newPerTimeFrameType, newPeriodFrameType, (newAnonymityCount > 0), newTitle, newStatus, newStartDate, newEndDate, newIsPrioritized, adminUserId, currentTime, newAnonymityCount, companyId, deckId));

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck_by_company",
                    new List<string> { "company_id", "deck_id" }, new List<string> { "publish_method_type", "title", "status", "is_prioritized", "start_timestamp", "end_timestamp" }, new List<string>()));
                batchStatement.Add(ps.Bind(newPublishMethodType, newTitle, newStatus, newIsPrioritized, newStartDate, newEndDate, companyId, deckId));

                List<BoundStatement> privacyStatement = UpdatePrivacy(deckId, newTargetedDepartmentIds, newTargetedUserIds, mainSession, false);
                foreach (BoundStatement bs in privacyStatement)
                {
                    batchStatement.Add(bs);
                }

                mainSession.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse DeleteDeck(string adminUserId, string companyId, string deckId, Row deckRow = null, ISession mainSession = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                    if (deckRow == null)
                    {
                        Log.Error("Invalid deck: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                        response.ErrorMessage = ErrorMessage.DeckInvalid;
                        return response;
                    }
                }


                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                bool isPrioritized = deckRow.GetValue<bool>("is_prioritized");
                int priority = SelectPulsePriority((int)PulseTypeEnum.Dynamic);

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_dynamic_deck", new List<string> { "company_id", "id" }));
                deleteBatch.Add(ps.Bind(companyId, deckId));

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_dynamic_order", new List<string> { "deck_id" }));
                deleteBatch.Add(ps.Bind(deckId));

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck", new List<string>(), new List<string> { "deck_id" }));
                RowSet cardRowSet = mainSession.Execute(ps.Bind(deckId));
                foreach (Row cardRow in cardRowSet)
                {
                    string pulseId = cardRow.GetValue<string>("id");

                    DeleteCard(adminUserId, companyId, deckId, pulseId, deckRow, mainSession);
                }

                mainSession.Execute(deleteBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse DeleteCard(string adminUserId, string companyId, string deckId, string pulseId, Row deckRow = null, ISession mainSession = null)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                    if (deckRow == null)
                    {
                        Log.Error("Invalid deck: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                        response.ErrorMessage = ErrorMessage.DeckInvalid;
                        return response;
                    }
                }

                Row cardRow = vh.ValidatePulseDeckCard(deckId, pulseId, mainSession);
                if (cardRow == null)
                {
                    Log.Error("Pulse already deleted");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                DateTime currentTime = DateTime.UtcNow;
                bool isPrioritized = deckRow.GetValue<bool>("is_prioritized");
                int priority = SelectPulsePriority((int)PulseTypeEnum.Dynamic);
                int status = deckRow.GetValue<int>("status");

                int cardType = cardRow.GetValue<int>("card_type");
                DateTimeOffset startDate = cardRow.GetValue<DateTimeOffset>("start_timestamp");
                float currentOrder = cardRow.GetValue<float>("ordering");

                int logicLevel = cardRow.GetValue<int>("logic_level");

                bool isUpdateAllCardsOrdering = true;
                if (logicLevel != 0)
                {
                    // Check logic level to remove child
                    // Recursive
                    // Get all options first
                    int numberOfOptions = cardRow.GetValue<int>("number_of_options");
                    for (int optionNumber = 1; optionNumber <= numberOfOptions; optionNumber++)
                    {
                        string nextCardId = cardRow.GetValue<string>(string.Format("option_{0}_next_card_id", optionNumber));
                        if (!string.IsNullOrEmpty(nextCardId) && !nextCardId.Equals(DefaultResource.PulseDynamicEndCardId))
                        {
                            DeleteCard(adminUserId, companyId, deckId, nextCardId, deckRow, mainSession);
                        }
                    }

                    RemoveChildOptionLogics(deckId, pulseId, mainSession);
                    RemoveParentOptionLogics(deckId, pulseId, adminUserId, currentTime, mainSession);

                    if (logicLevel != 1)
                    {
                        isUpdateAllCardsOrdering = false;
                    }
                    else
                    {
                        // Remove from ordering
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_dynamic_order", new List<string> { "deck_id", "ordering", "pulse_id" }));
                        deleteBatch.Add(ps.Bind(deckId, currentOrder, pulseId));
                    }
                }

                if (isUpdateAllCardsOrdering)
                {
                    // Reordering
                    ps = mainSession.Prepare(CQLGenerator.SelectStatementWithComparison("pulse_dynamic_order", new List<string>(), new List<string> { "deck_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                    RowSet orderRowSet = mainSession.Execute(ps.Bind(deckId, currentOrder));
                    foreach (Row orderRow in orderRowSet)
                    {
                        string rowPulseId = orderRow.GetValue<string>("pulse_id");
                        float ordering = orderRow.GetValue<float>("ordering");

                        float newOrdering = ordering - 1;
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_dynamic_order", new List<string> { "deck_id", "ordering", "pulse_id" }));
                        deleteBatch.Add(ps.Bind(deckId, ordering, rowPulseId));

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_dynamic_order", new List<string> { "deck_id", "ordering", "pulse_id" }));
                        updateBatch.Add(ps.Bind(deckId, newOrdering, rowPulseId));

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                        updateBatch.Add(ps.Bind(newOrdering, deckId, rowPulseId));
                    }
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_dynamic_order", new List<string> { "deck_id", "ordering", "pulse_id" }));
                deleteBatch.Add(ps.Bind(deckId, currentOrder, pulseId));

                DeleteFromS3(companyId, pulseId);

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_dynamic_card_by_deck", new List<string> { "deck_id", "id" }));
                updateBatch.Add(ps.Bind(deckId, pulseId));

                // Remove from priority table
                if (logicLevel <= 1)
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(companyId, isPrioritized, priority, startDate, pulseId));
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseAnswerCardResponse AnswerCard(string answeredByUserId, string companyId, string deckId, string pulseId, int selectedRange = -1, string selectedOptionId = null, string customAnswer = null)
        {
            PulseAnswerCardResponse response = new PulseAnswerCardResponse();
            response.NextPulse = new Pulse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                if (deckRow == null)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                Row cardRow = vh.ValidatePulseDeckCard(deckId, pulseId, mainSession);

                if (cardRow == null)
                {
                    Log.Error("Invalid pulseId: " + pulseId);
                    response.ErrorCode = Int16.Parse(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                int cardType = cardRow.GetValue<int>("card_type");
                PulseValidationResponse validateAnswer = ValidateCardAnswer(deckId, cardRow, selectedRange, selectedOptionId, customAnswer);
                if (!validateAnswer.Success)
                {
                    Log.Error(validateAnswer.ErrorMessage);
                    response.ErrorCode = validateAnswer.ErrorCode;
                    response.ErrorMessage = validateAnswer.ErrorMessage;
                    return response;
                }

                Thread updateThread = new Thread(() => new AnalyticPulse().AnswerCard(answeredByUserId, pulseId, cardType, DateTime.UtcNow, selectedRange, selectedOptionId, customAnswer, analyticSession));
                updateThread.Start();

                if (cardType == (int)DeckCardTypeEnum.SelectOneWithLogic)
                {
                    bool isComplusory = deckRow.GetValue<bool>("is_complusory");
                    bool isAnonymous = deckRow.GetValue<bool>("is_anonymous");
                    int publishMethod = deckRow.GetValue<int>("publish_method_type");

                    int anonymityCount = 0;
                    if (deckRow.IsNull("anonymity_count"))
                    {
                        if (deckRow.GetValue<bool>("is_anonymous"))
                        {
                            anonymityCount = 1;

                            #region Update null value of anonymity_count column.
                            PreparedStatement psUpdate = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                              new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                            BatchStatement bsUpdate = new BatchStatement();
                            bsUpdate.Add(psUpdate.Bind(anonymityCount, deckRow.GetValue<bool>("company_id"), deckRow.GetValue<bool>("id")));
                            mainSession.Execute(bsUpdate);
                            #endregion
                        }
                    }
                    else
                    {
                        anonymityCount = deckRow.GetValue<int>("anonymity_count");
                    }

                    string nextCardId = cardRow.GetValue<string>(string.Format("option_{0}_next_card_id", selectedOptionId.Replace(string.Format("{0}_", pulseId), "")));
                    if (!nextCardId.Equals(DefaultResource.PulseDynamicEndCardId))
                    {
                        response.NextPulse = SelectCard(deckId, nextCardId, null, publishMethod, isComplusory, anonymityCount, false, false, false, null, mainSession).Pulse;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseValidationResponse ValidateCardAnswer(string deckId, Row cardRow, int selectedRange = -1, string selectedOptionId = null, string customAnswer = null)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                PulseValidationResponse validateRangeResponse = new PulseValidationResponse();
                int cardType = cardRow.GetValue<int>("card_type");
                string pulseId = cardRow.GetValue<string>("id");
                switch (cardType)
                {
                    case (int)DeckCardTypeEnum.NumberRange:
                        int rangeType = cardRow.GetValue<int>("range_type");
                        validateRangeResponse = ValidateAnsweredRange(rangeType, selectedRange);
                        break;
                    case (int)DeckCardTypeEnum.SelectOne:
                    case (int)DeckCardTypeEnum.SelectOneWithLogic:
                        int numberOfOptions = cardRow.GetValue<int>("number_of_options");
                        validateRangeResponse = ValidateAnsweredOption(pulseId, numberOfOptions, selectedOptionId);
                        break;
                    case (int)DeckCardTypeEnum.Text:
                        validateRangeResponse = ValidateAnsweredCustomAnswer(customAnswer);
                        break;
                }

                if (!validateRangeResponse.Success)
                {
                    Log.Error(validateRangeResponse.ErrorMessage);
                    response.ErrorCode = validateRangeResponse.ErrorCode;
                    response.ErrorMessage = validateRangeResponse.ErrorMessage;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }


        private PulseValidationResponse ValidateAnsweredRange(int rangeType, int selectedRange)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                bool hasError = true;
                switch (rangeType)
                {
                    case (int)CardRangeTypeEnum.OneTo3:
                        if (selectedRange >= 1 && selectedRange <= 3)
                        {
                            hasError = false;
                        }
                        break;
                    case (int)CardRangeTypeEnum.OneTo5:
                        if (selectedRange >= 1 && selectedRange <= 5)
                        {
                            hasError = false;
                        }
                        break;
                    case (int)CardRangeTypeEnum.OneTo7:
                        if (selectedRange >= 1 && selectedRange <= 7)
                        {
                            hasError = false;
                        }
                        break;
                    case (int)CardRangeTypeEnum.OneTo10:
                        if (selectedRange >= 1 && selectedRange <= 10)
                        {
                            hasError = false;
                        }
                        break;
                    case (int)CardRangeTypeEnum.ZeroTo100Percent:
                        if ((selectedRange >= 0 && selectedRange <= 100) || selectedRange % 10 == 0)
                        {
                            hasError = false;
                        }
                        break;
                }

                if (hasError)
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.DeckCardInvalidRange);
                    response.ErrorMessage = ErrorMessage.DeckCardInvalidRange;
                }
                else
                {
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseValidationResponse ValidateDurationPerCard(int durationPerCard)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                bool hasError = false;
                if (durationPerCard < -1 || durationPerCard > 90)
                {
                    hasError = true;
                }

                if (hasError)
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.DeckInvalidDurationPerCard);
                    response.ErrorMessage = ErrorMessage.DeckInvalidDurationPerCard;
                }
                else
                {
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private PulseValidationResponse ValidateAnsweredOption(string pulseId, int numberOfOptions, string selectedOptionId)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                bool hasError = true;

                if (!string.IsNullOrEmpty(selectedOptionId))
                {
                    double optionSelected = Convert.ToDouble(selectedOptionId.Replace(pulseId + "_", ""));

                    if (optionSelected <= numberOfOptions || optionSelected <= 0)
                    {
                        hasError = false;
                    }
                }

                if (hasError)
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.DeckCardInvalidOption);
                    response.ErrorMessage = ErrorMessage.DeckCardInvalidOption;
                }
                else
                {
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.DeckCardInvalidOption);
                response.ErrorMessage = ErrorMessage.DeckCardInvalidOption;
            }

            return response;
        }

        private PulseValidationResponse ValidateAnsweredCustomAnswer(string customAnswer)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                bool hasError = true;

                if (!string.IsNullOrEmpty(customAnswer))
                {
                    hasError = false;
                }

                if (hasError)
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.DeckCardInvalidCustomAnswer);
                    response.ErrorMessage = ErrorMessage.DeckCardInvalidCustomAnswer;
                }
                else
                {
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectDeckAnalyticResponse SelectDeckAnalytic(string adminUserId, string companyId, string deckId, string searchId)
        {
            PulseSelectDeckAnalyticResponse response = new PulseSelectDeckAnalyticResponse();
            response.Deck = new PulseDeck();
            response.PulseStats = new AnalyticPulse.PulseResponsiveStats();
            response.SearchTerms = new List<AnalyticPulse.PulseResponsiveSearchTerm>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                if (deckRow == null)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                PulseDeck deck = SelectDeck(deckId, companyId, (int)PulseQueryEnum.FullDetail, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), null, false, true, 0, false, true, mainSession, deckRow).Deck;

                // Get privacy
                PulseSelectPrivacyResponse privacyResponse = SelectPulsePrivacy(deckId, mainSession, false);

                List<User> allTargetedAudience = new List<User>();
                List<User> targetedAudience = new List<User>();

                allTargetedAudience = SelectPulseTargetedAudience(deckId, companyId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, mainSession, false);

                if (!string.IsNullOrEmpty(searchId))
                {
                    targetedAudience = SelectPulseTargetedAudience(deckId, companyId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, mainSession, false, searchId);
                }
                else
                {
                    targetedAudience = allTargetedAudience;
                }

                int totalAudienceCount = targetedAudience.Count();

                // Get searchIds
                List<AnalyticPulse.PulseResponsiveSearchTerm> searchTerms = new List<AnalyticPulse.PulseResponsiveSearchTerm>();

                foreach (User user in allTargetedAudience)
                {
                    foreach (Department department in user.Departments)
                    {
                        if (!searchTerms.Any(s => s.Id.Equals(department.Id)))
                        {
                            searchTerms.Add(
                                new AnalyticPulse.PulseResponsiveSearchTerm
                                {
                                    Id = department.Id,
                                    Content = department.Title,
                                    IsSelected = department.Id.Equals(searchId) ? true : false
                                }
                            );
                        }
                    }
                }

                searchTerms = searchTerms.OrderByDescending(s => s.IsSelected).ThenBy(s => s.Content).ToList();

                AnalyticPulse.PulseResponsiveSearchTerm allDepartmentSearchTerm = new AnalyticPulse.PulseResponsiveSearchTerm
                {
                    Id = string.Empty,
                    Content = "All Departments",
                    IsSelected = string.IsNullOrEmpty(searchId) ? true : false
                };

                if (string.IsNullOrEmpty(searchId))
                {
                    searchTerms.Insert(0, allDepartmentSearchTerm);
                }
                else
                {
                    searchTerms.Insert(1, allDepartmentSearchTerm);
                }

                if (deck.Pulses.Count > 0)
                {
                    // Select respondents
                    new AnalyticPulse().SelectRespondentChartPerCard(deck, totalAudienceCount, companyId, searchId, analyticSession, mainSession);

                    PulseDynamic selectedPulse = (PulseDynamic)deck.Pulses[0];
                    PulseSelectDeckCardAnalyticResponse deckCardResponse = SelectDeckCardAnalytic(adminUserId, companyId, deckId, selectedPulse.PulseId, searchId, totalAudienceCount, selectedPulse, mainSession, analyticSession);
                    response.PulseStats = deckCardResponse.PulseStats;
                }

                response.Deck = deck;
                response.SearchTerms = searchTerms;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectDeckCardAnalyticResponse SelectDeckCardAnalytic(string adminUserId,
                                                                          string companyId,
                                                                          string deckId,
                                                                          string pulseId,
                                                                          string searchId,
                                                                          int totalAudienceCount = -1,
                                                                          PulseDynamic selectedPulse = null,
                                                                          ISession mainSession = null,
                                                                          ISession analyticSession = null)
        {
            PulseSelectDeckCardAnalyticResponse response = new PulseSelectDeckCardAnalyticResponse();
            response.PulseStats = new AnalyticPulse.PulseResponsiveStats();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                Row deckRow = null;
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                    if (deckRow == null)
                    {
                        Log.Error("Invalid deck: " + deckId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                        response.ErrorMessage = ErrorMessage.DeckInvalid;
                        return response;
                    }

                    Row cardRow = vh.ValidatePulseDeckCard(deckId, pulseId, mainSession);
                    if (cardRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }



                if (totalAudienceCount == -1)
                {
                    // Get privacy
                    PulseSelectPrivacyResponse privacyResponse = SelectPulsePrivacy(deckId, mainSession, false);

                    List<User> allTargetedAudience = new List<User>();
                    List<User> targetedAudience = new List<User>();

                    allTargetedAudience = SelectPulseTargetedAudience(deckId, companyId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, mainSession, false);

                    if (!string.IsNullOrEmpty(searchId))
                    {
                        targetedAudience = SelectPulseTargetedAudience(deckId, companyId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, mainSession, false, searchId);
                    }
                    else
                    {
                        targetedAudience = allTargetedAudience;
                    }

                    totalAudienceCount = targetedAudience.Count();

                    // Get searchIds
                    List<AnalyticPulse.PulseResponsiveSearchTerm> searchTerms = new List<AnalyticPulse.PulseResponsiveSearchTerm>();
                    foreach (User user in allTargetedAudience)
                    {
                        foreach (Department department in user.Departments)
                        {
                            if (!searchTerms.Any(s => s.Id.Equals(department.Id)))
                            {
                                searchTerms.Add(
                                    new AnalyticPulse.PulseResponsiveSearchTerm
                                    {
                                        Id = department.Id,
                                        Content = department.Title,
                                        IsSelected = department.Id.Equals(searchId) ? true : false
                                    }
                                );
                            }
                        }
                    }

                    searchTerms = searchTerms.OrderByDescending(s => s.IsSelected).ThenBy(s => s.Content).ToList();

                    AnalyticPulse.PulseResponsiveSearchTerm allDepartmentSearchTerm = new AnalyticPulse.PulseResponsiveSearchTerm
                    {
                        Id = string.Empty,
                        Content = "All Departments",
                        IsSelected = string.IsNullOrEmpty(searchId) ? true : false
                    };

                    if (string.IsNullOrEmpty(searchId))
                    {
                        searchTerms.Insert(0, allDepartmentSearchTerm);
                    }
                    else
                    {
                        searchTerms.Insert(1, allDepartmentSearchTerm);
                    }

                    response.SearchTerms = searchTerms;
                }

                if(deckRow== null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                }

                int anonymityCount = 0;
                if (deckRow.IsNull("anonymity_count"))
                {
                    if (deckRow.GetValue<bool>("is_anonymous"))
                    {
                        anonymityCount = 1;

                        #region Update null value of anonymity_count column.
                        PreparedStatement psUpdate = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                          new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                        BatchStatement bsUpdate = new BatchStatement();
                        bsUpdate.Add(psUpdate.Bind(anonymityCount, deckRow.GetValue<bool>("company_id"), deckRow.GetValue<bool>("id")));
                        mainSession.Execute(bsUpdate);
                        #endregion
                    }
                }
                else
                {
                    anonymityCount = deckRow.GetValue<int>("anonymity_count");
                }


                if (selectedPulse == null)
                {
                    selectedPulse = SelectCard(deckId, pulseId, null, 0, false, anonymityCount, true, false, false, null, mainSession).Pulse;
                }

                if (selectedPulse.CardType == (int)DeckCardTypeEnum.SelectOne || selectedPulse.CardType == (int)DeckCardTypeEnum.SelectOneWithLogic)
                {
                    List<DeckCardOption> options = new List<DeckCardOption>() {
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option1Id,
                                Content = selectedPulse.Option1Content
                            },
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option2Id,
                                Content = selectedPulse.Option2Content
                            },
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option3Id,
                                Content = selectedPulse.Option3Content
                            },
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option4Id,
                                Content = selectedPulse.Option4Content
                            },
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option5Id,
                                Content = selectedPulse.Option5Content
                            },
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option6Id,
                                Content = selectedPulse.Option6Content
                            },
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option7Id,
                                Content = selectedPulse.Option7Content
                            },
                            new DeckCardOption
                            {
                                OptionId = selectedPulse.Option8Id,
                                Content = selectedPulse.Option8Content
                            }
                        };

                    int numberOfOptions = selectedPulse.NumberOfOptions;
                    options = options.Take(numberOfOptions).ToList();
                    selectedPulse.Options = options;
                }

                // Get parent card and option
                if (selectedPulse.HasIncomingLogic)
                {
                    PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("dynamic_option_from_logic",
                        new List<string>(), new List<string> { "pulse_id" }));
                    Row logicRow = mainSession.Execute(ps.Bind(selectedPulse.PulseId)).FirstOrDefault();
                    if (logicRow != null)
                    {
                        string fromPulseId = logicRow.GetValue<string>("from_pulse_id");
                        string fromOptionId = logicRow.GetValue<string>("from_option_id");

                        PulseDynamic fromPulse = SelectCard(deckId, fromPulseId, null, 0, false, anonymityCount, true, false, false, null, mainSession).Pulse;
                        if (fromPulse != null)
                        {
                            int optionNumber = Convert.ToInt16(fromOptionId.Replace(fromPulseId + "_", ""));
                            DeckCardOption parentOption = new DeckCardOption();
                            switch (optionNumber)
                            {
                                case 1:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option1Id,
                                        Content = fromPulse.Option1Content
                                    };
                                    break;
                                case 2:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option2Id,
                                        Content = fromPulse.Option2Content
                                    };
                                    break;
                                case 3:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option3Id,
                                        Content = fromPulse.Option3Content
                                    };
                                    break;
                                case 4:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option4Id,
                                        Content = fromPulse.Option4Content
                                    };
                                    break;
                                case 5:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option5Id,
                                        Content = fromPulse.Option5Content
                                    };
                                    break;
                                case 6:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option6Id,
                                        Content = fromPulse.Option6Content
                                    };
                                    break;
                                case 7:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option7Id,
                                        Content = fromPulse.Option7Content
                                    };
                                    break;
                                case 8:
                                    parentOption = new DeckCardOption
                                    {
                                        OptionId = fromPulse.Option8Id,
                                        Content = fromPulse.Option8Content
                                    };
                                    break;
                            }

                            selectedPulse.ParentCard = fromPulse;
                            selectedPulse.ParentOption = parentOption;
                        }
                    }
                }

                response.PulseStats.Pulse = selectedPulse;
                new AnalyticPulse().SelectDynamicCardResult(response.PulseStats, companyId, searchId, totalAudienceCount, mainSession, analyticSession);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectDeckCardOptionAnalyticResponse SelectDeckCardOptionAnalytic(string adminUserId, string companyId, string deckId, string pulseId, string optionId)
        {
            PulseSelectDeckCardOptionAnalyticResponse response = new PulseSelectDeckCardOptionAnalyticResponse();
            response.Deck = new PulseDeck();
            response.Pulse = new PulseDynamic();
            response.Departments = new List<Department>();
            response.Responders = new List<PulseSelectDeckCardOptionAnalyticResponse.DeckCardOptionResponder>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                if (deckRow == null)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                Row cardRow = vh.ValidatePulseDeckCard(deckId, pulseId, mainSession);
                if (cardRow == null)
                {
                    Log.Error("Invalid pulse: " + pulseId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                DateTime? deckEndDate = deckRow.GetValue<DateTime?>("end_timestamp");

                // Get privacy
                PulseSelectPrivacyResponse privacyResponse = SelectPulsePrivacy(deckId, mainSession, false);
                List<User> targetedAudience = SelectPulseTargetedAudience(deckId, companyId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, mainSession, false);
                int totalAudienceCount = targetedAudience.Count();

                int anonymityCount = 0;
                if (deckRow.IsNull("anonymity_count"))
                {
                    if (deckRow.GetValue<bool>("is_anonymous"))
                    {
                        anonymityCount = 1;

                        #region Update null value of anonymity_count column.
                        PreparedStatement psUpdate = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                          new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                        BatchStatement bsUpdate = new BatchStatement();
                        bsUpdate.Add(psUpdate.Bind(anonymityCount, deckRow.GetValue<bool>("company_id"), deckRow.GetValue<bool>("id")));
                        mainSession.Execute(bsUpdate);
                        #endregion
                    }
                }
                else
                {
                    anonymityCount = deckRow.GetValue<int>("anonymity_count");
                }


                PulseDynamic selectedPulse = SelectCard(deckId, pulseId, deckEndDate, 0, false, anonymityCount, true, false, false, null, mainSession).Pulse;

                if (selectedPulse.CardType == (int)DeckCardTypeEnum.Text)
                {
                    Log.Error("Invalid method to call");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalidMethod);
                    response.ErrorMessage = ErrorMessage.DeckInvalidMethod;
                    return response;
                }

                if (selectedPulse.CardType == (int)DeckCardTypeEnum.SelectOne || selectedPulse.CardType == (int)DeckCardTypeEnum.SelectOneWithLogic)
                {
                    PulseValidationResponse validationResponse = ValidateAnsweredOption(selectedPulse.PulseId, selectedPulse.NumberOfOptions, optionId);
                    if (!validationResponse.Success)
                    {
                        response.ErrorCode = validationResponse.ErrorCode;
                        response.ErrorMessage = validationResponse.ErrorMessage;
                        return response;
                    }

                    List<DeckCardOption> options = new List<DeckCardOption>()
                    {
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option1Id,
                            Content = selectedPulse.Option1Content
                        },
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option2Id,
                            Content = selectedPulse.Option2Content
                        },
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option3Id,
                            Content = selectedPulse.Option3Content
                        },
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option4Id,
                            Content = selectedPulse.Option4Content
                        },
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option5Id,
                            Content = selectedPulse.Option5Content
                        },
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option6Id,
                            Content = selectedPulse.Option6Content
                        },
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option7Id,
                            Content = selectedPulse.Option7Content
                        },
                        new DeckCardOption
                        {
                            OptionId = selectedPulse.Option8Id,
                            Content = selectedPulse.Option8Content
                        }
                    };

                    int optionSelected = Convert.ToInt16(optionId.Replace(pulseId + "_", "")) - 1;

                    options = new List<DeckCardOption> { options[optionSelected] };
                    selectedPulse.Options = options;

                }
                else if (selectedPulse.CardType == (int)DeckCardTypeEnum.NumberRange)
                {
                    PulseValidationResponse validationResponse = ValidateAnsweredRange(selectedPulse.RangeType, Convert.ToInt16(optionId));
                    if (!validationResponse.Success)
                    {
                        response.ErrorCode = validationResponse.ErrorCode;
                        response.ErrorMessage = validationResponse.ErrorMessage;
                        return response;
                    }

                    List<DeckCardOption> options = new List<DeckCardOption>
                    {
                        new DeckCardOption
                        {
                            OptionId = optionId.ToString(),
                            Content = optionId.ToString(),
                        }
                    };

                    selectedPulse.Options = options;
                }

                double timeOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                response.Deck = SelectDeck(deckId, companyId, (int)PulseQueryEnum.Basic, timeOffset, null, false, false, 0, true, false, mainSession, deckRow).Deck;
                response.Pulse = selectedPulse;

                bool isAnonymous = deckRow.GetValue<bool>("is_anonymous");
                if (!isAnonymous)
                {
                    new AnalyticPulse().SelectDynamicCardOptionResult(companyId, pulseId, selectedPulse.CardType, optionId, timeOffset, response, analyticSession, mainSession);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectDeckCardCustomAnswerAnalyticResponse SelectDeckCardCustomAnswerAnalytic(string adminUserId, string companyId, string deckId, string pulseId)
        {
            PulseSelectDeckCardCustomAnswerAnalyticResponse response = new PulseSelectDeckCardCustomAnswerAnalyticResponse();
            response.Deck = new PulseDeck();
            response.Pulse = new PulseDynamic();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                if (deckRow == null)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                Row cardRow = vh.ValidatePulseDeckCard(deckId, pulseId, mainSession);
                if (cardRow == null)
                {
                    Log.Error("Invalid pulse: " + pulseId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                    response.ErrorMessage = ErrorMessage.PulseInvalid;
                    return response;
                }

                // Anonymous
                bool isAnonymous = deckRow.GetValue<bool>("is_anonymous");

                DateTime? deckEndDate = deckRow.GetValue<DateTime?>("end_timestamp");


                int anonymityCount = 0;
                if (deckRow.IsNull("anonymity_count"))
                {
                    if (deckRow.GetValue<bool>("is_anonymous"))
                    {
                        anonymityCount = 1;

                        #region Update null value of anonymity_count column.
                        PreparedStatement psUpdate = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                          new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                        BatchStatement bsUpdate = new BatchStatement();
                        bsUpdate.Add(psUpdate.Bind(anonymityCount, deckRow.GetValue<bool>("company_id"), deckRow.GetValue<bool>("id")));
                        mainSession.Execute(bsUpdate);
                        #endregion
                    }
                }
                else
                {
                    anonymityCount = deckRow.GetValue<int>("anonymity_count");
                }

                PulseDynamic selectedPulse = SelectCard(deckId, pulseId, deckEndDate, 0, false, anonymityCount, true, false, false, null, mainSession).Pulse;
                if (selectedPulse.CardType != (int)DeckCardTypeEnum.Text)
                {
                    Log.Error("Invalid method to call");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalidMethod);
                    response.ErrorMessage = ErrorMessage.DeckInvalidMethod;
                    return response;
                }

                // Fetch result
                new AnalyticPulse().SelectDeckCardCustomAnswersResult(selectedPulse, isAnonymous, companyId, analyticSession, mainSession);

                response.Deck = SelectDeck(deckId, companyId, (int)PulseQueryEnum.Basic, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), null, false, false, 0, true, false, mainSession, deckRow).Deck;
                response.Pulse = selectedPulse;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectDeckAnalyticByUserResponse SelectDeckAnalyticByUser(string adminUserId,
                                                                              string companyId,
                                                                              string deckId,
                                                                              string answeredByUserId)
        {
            PulseSelectDeckAnalyticByUserResponse response = new PulseSelectDeckAnalyticByUserResponse();
            response.LastUpdatedDateString = string.Empty;
            response.AnsweredByUser = new User();
            response.Deck = new PulseDeck();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                if (deckRow == null)
                {
                    Log.Error("Invalid deck: " + deckId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalid);
                    response.ErrorMessage = ErrorMessage.DeckInvalid;
                    return response;
                }

                Row userRow = vh.ValidateUser(answeredByUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid answeredUserId: " + answeredByUserId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                response.AnsweredByUser = new User().SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User;
                response.Deck = SelectDeck(deckId, companyId, (int)PulseQueryEnum.FullDetail, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), null, false, false, 0, true, true, mainSession, deckRow).Deck;

                new AnalyticPulse().SelectDeckAnalyticByUser(response, answeredByUserId, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<BoundStatement> UpdateDurationForRoutineCards(string adminUserId, string deckId, int newDuration, List<PulseDynamic> cards, DateTime currentTime, ISession mainSession = null)
        {
            List<BoundStatement> statements = new List<BoundStatement>();
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                foreach (PulseDynamic card in cards)
                {
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck",
                        new List<string> { "deck_id", "id" }, new List<string> { "duration_day", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    statements.Add(ps.Bind(newDuration, adminUserId, currentTime, deckId, card.PulseId));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return statements;
        }

        private List<BoundStatement> UpdateTimestampForScheduleCards(string adminUserId, string deckId, List<PulseDynamic> cards, DateTime newDeckStartTimestamp, DateTime? newDeckEndTimestamp, DateTime currentTime, ISession mainSession = null)
        {
            List<BoundStatement> statements = new List<BoundStatement>();
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                foreach (PulseDynamic card in cards)
                {
                    DateTime startTimestamp = newDeckStartTimestamp.AddMilliseconds(cards.Count - ((int)card.FloatOrdering - 1));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_card_by_deck",
                        new List<string> { "deck_id", "id" }, new List<string> { "start_timestamp", "end_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    statements.Add(ps.Bind(startTimestamp, newDeckEndTimestamp, adminUserId, currentTime, deckId, card.PulseId));

                    card.UpdatedStartDate = startTimestamp;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return statements;
        }

        private void UpdatePulsePriority(string companyId, string deckId, int publishMethod, List<PulseDynamic> cards, bool newDeckIsPrioritized, bool currentDeckIsPrioritized, ISession session = null)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                foreach (PulseDynamic card in cards)
                {
                    if (card.LogicLevel <= 1)
                    {
                        DateTime? newStartTime = null;
                        DateTime? currentStartTime = null;

                        // Changing deck start time only affects schedule cards
                        if (publishMethod == (int)PublishMethodTypeEnum.Routine)
                        {
                            newStartTime = card.StartDate;
                            currentStartTime = card.StartDate;
                        }
                        else if (publishMethod == (int)PublishMethodTypeEnum.Schedule)
                        {
                            newStartTime = card.UpdatedStartDate;
                            currentStartTime = card.StartDate;
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "company_id", "is_prioritized", "pulse_priority", "start_timestamp", "pulse_id" }));
                        deleteBatch.Add(ps.Bind(companyId, currentDeckIsPrioritized, (int)PulsePriorityEnum.Dynamic, currentStartTime, card.PulseId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp",
                            new List<string> { "pulse_id", "company_id", "deck_id", "pulse_priority", "pulse_type", "is_prioritized", "start_timestamp" }));
                        updateBatch.Add(ps.Bind(card.PulseId, companyId, deckId, (int)PulsePriorityEnum.Dynamic, (int)PulseTypeEnum.Dynamic, newDeckIsPrioritized, newStartTime));
                    }
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private string ConvertNumberToAlphabet(int number)
        {
            if (number - 1 < 0 || number - 1 >= alphabetList.Count)
            {
                return string.Empty;
            }

            string alphabet = alphabetList[number - 1];
            return alphabet;
        }

        private int ConvertAlphabetToNumber(string alphabet)
        {
            int number = 1;

            number = alphabetList.FindIndex(a => a.Equals(alphabet)) + 1;

            return number;
        }

        private float ConvertStringToOrdering(string ordering)
        {
            int delimiter = 10;
            int power = 1;
            float numberOrdering = StripAlphabetForNumber(ordering);
            string alphabets = ordering.Replace(numberOrdering.ToString(), "");
            for (int alphabetIndex = 0; alphabetIndex < alphabets.Length; alphabetIndex++)
            {
                delimiter = (int)Math.Pow(delimiter, power);
                string alphabet = alphabets.Substring(alphabetIndex, 1);

                int number = ConvertAlphabetToNumber(alphabet);

                numberOrdering += (float)number / delimiter;

                power++;
            }

            return numberOrdering;
        }

        private string ConvertOrderingToString(float ordering, int logicLevel)
        {
            string orderString = string.Empty;
            try
            {
                int indexOfSplit = ordering.ToString().IndexOf('.');
                orderString = ordering.ToString().Split('.')[0];
                if (logicLevel > 1)
                {
                    for (int index = indexOfSplit + 1; index < ordering.ToString().Length; index++)
                    {
                        int value = Convert.ToInt16(ordering.ToString().Substring(index, 1));
                        orderString += ConvertNumberToAlphabet(value);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return orderString;
        }

        private int StripAlphabetForNumber(string ordering)
        {
            foreach (string s in alphabetList)
            {
                ordering = ordering.Replace(s, "");
            }

            return Convert.ToInt32(ordering);
        }

        public DateTime GeneratePulseStartTimeForImport(string deckId, string companyId, int perFrameType, int periodType, int numberOfCardsPerTimeFrame, DateTime startDeckDate, ISession mainSession = null, Row deckRow = null)
        {
            DateTime startDate = new DateTime();

            if (mainSession == null)
            {
                ConnectionManager cm = new ConnectionManager();
                mainSession = cm.getMainSession();
            }

            double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

            List<PulseDynamic> deckCards = SelectAllCards(deckId, null, companyId, null, false, false, false, mainSession, deckRow).Pulses;

            if (deckCards.Count > 0)
            {
                int count = 0;
                // Get the last card which is not customized
                PulseDynamic lastCard = deckCards.Where(c => !c.IsCustomized).ToList()[0];
                startDate = lastCard.StartDate;

                if (lastCard.HasIncomingLogic)
                {
                    int ordering = StripAlphabetForNumber(lastCard.Ordering);
                    lastCard = deckCards.FirstOrDefault(c => c.Ordering.Equals(ordering.ToString()));

                    startDate = lastCard.StartDate;
                }

                // Always convert to local time first
                startDate = startDate.AddHours(timezoneOffset);
                switch (perFrameType)
                {
                    case (int)DeckPerTimeFrameTypeEnum.PerDay:
                        count = deckCards.Select(c => c.StartDate.AddHours(timezoneOffset) == startDate).ToList().Count();
                        if (count + 1 > numberOfCardsPerTimeFrame)
                        {
                            startDate = startDate.AddDays(1);
                        }
                        break;
                    case (int)DeckPerTimeFrameTypeEnum.PerWeek:
                        int lastCardDay = ((int)startDate.DayOfWeek == 0) ? 7 : (int)startDate.DayOfWeek;

                        // Must always set it to a later date
                        // Say selected day is Tue, but last card is modified to Wed
                        if (lastCardDay > periodType)
                        {
                            int dayDiff = lastCardDay - periodType + 7;
                            startDate = startDate.AddDays(dayDiff);

                            // Do one more round of check after adding 7 days to next week
                            lastCardDay = ((int)startDate.DayOfWeek == 0) ? 7 : (int)startDate.DayOfWeek;
                            if (lastCardDay > periodType)
                            {
                                dayDiff = lastCardDay - periodType;
                                startDate = startDate.AddDays(-dayDiff);
                            }
                            else if (lastCardDay < periodType)
                            {
                                dayDiff = periodType - lastCardDay;
                                startDate = startDate.AddDays(dayDiff);
                            }
                        }
                        // Say selected day is Tue, but last card is modified to Monday
                        else if (lastCardDay < periodType)
                        {
                            int dayDiff = periodType - lastCardDay;
                            startDate = startDate.AddDays(dayDiff);
                        }

                        count = deckCards.Select(c => c.StartDate.AddHours(timezoneOffset) == startDate).ToList().Count();
                        if (count + 1 > numberOfCardsPerTimeFrame)
                        {
                            int dayDiff = 0;
                            startDate = startDate.AddDays(7);

                            // Do one more round of check after adding 7 days to next week
                            lastCardDay = ((int)startDate.DayOfWeek == 0) ? 7 : (int)startDate.DayOfWeek;
                            if (lastCardDay > periodType)
                            {
                                dayDiff = lastCardDay - periodType;
                                startDate = startDate.AddDays(-dayDiff);
                            }
                            else if (lastCardDay < periodType)
                            {
                                dayDiff = periodType - lastCardDay;
                                startDate = startDate.AddDays(dayDiff);
                            }
                        }
                        break;
                    case (int)DeckPerTimeFrameTypeEnum.PerMonth:
                        count = deckCards.Select(c => c.StartDate.AddHours(timezoneOffset) == startDate).ToList().Count();
                        if (count + 1 > numberOfCardsPerTimeFrame)
                        {
                            int day = 0;
                            DateTime dateForNextMonth = startDate.AddMonths(1);
                            DateTime firstDayForNextMonth = new DateTime(dateForNextMonth.Year, dateForNextMonth.Month, 1);

                            switch (periodType)
                            {
                                case (int)PeriodFrameForMonthEnum.FirstWorkDay:
                                    day = ((int)firstDayForNextMonth.DayOfWeek == 0) ? 7 : (int)firstDayForNextMonth.DayOfWeek;

                                    // Need to add day to next working Monday
                                    if (day > 5)
                                    {
                                        int dayDiff = 8 - day;
                                        startDate = firstDayForNextMonth.AddDays(dayDiff);
                                    }

                                    break;
                                case (int)PeriodFrameForMonthEnum.LastWorkDay:
                                    DateTime lastDayForNextMonth = firstDayForNextMonth.AddMonths(1).AddDays(-1);
                                    day = ((int)lastDayForNextMonth.DayOfWeek == 0) ? 7 : (int)lastDayForNextMonth.DayOfWeek;

                                    // Need to substract day to current working day
                                    if (day > 5)
                                    {
                                        int dayDiff = day - 5;
                                        startDate = lastDayForNextMonth.AddDays(-dayDiff);
                                    }
                                    break;
                                case (int)PeriodFrameForMonthEnum.ParticularDay:
                                    try
                                    {
                                        startDate = new DateTime(dateForNextMonth.Year, dateForNextMonth.Month, startDeckDate.Day);
                                    }
                                    catch (Exception)
                                    {
                                        startDate = dateForNextMonth.AddMonths(1).AddDays(-1);
                                    }
                                    break;
                            }
                        }
                        break;
                }
                // Revert back to UTC time
                startDate = startDate.AddHours(-timezoneOffset);
            }
            else
            {
                startDate = startDeckDate;

                // Always convert to local time first
                startDate = startDate.AddHours(timezoneOffset);
                switch (perFrameType)
                {
                    case (int)DeckPerTimeFrameTypeEnum.PerWeek:
                        int lastCardDay = ((int)startDate.DayOfWeek == 0) ? 7 : (int)startDate.DayOfWeek;

                        // Must always set it to a later date
                        // Say selected day is Tue, but last card is modified to Wed
                        if (lastCardDay > periodType)
                        {
                            int dayDiff = lastCardDay - periodType + 7;
                            startDate = startDate.AddDays(dayDiff);

                            // Do one more round of check after adding 7 days to next week
                            lastCardDay = ((int)startDate.DayOfWeek == 0) ? 7 : (int)startDate.DayOfWeek;
                            if (lastCardDay > periodType)
                            {
                                dayDiff = lastCardDay - periodType;
                                startDate = startDate.AddDays(-dayDiff);
                            }
                            else if (lastCardDay < periodType)
                            {
                                dayDiff = periodType - lastCardDay;
                                startDate = startDate.AddDays(dayDiff);
                            }
                        }
                        // Say selected day is Tue, but last card is modified to Monday
                        else if (lastCardDay < periodType)
                        {
                            int dayDiff = periodType - lastCardDay;
                            startDate = startDate.AddDays(dayDiff);
                        }
                        break;
                    case (int)DeckPerTimeFrameTypeEnum.PerMonth:
                        int day = 0;
                        DateTime dateForCurrentMonth = startDate;
                        DateTime firstDayForCurrentMonth = new DateTime(dateForCurrentMonth.Year, dateForCurrentMonth.Month, 1);

                        switch (periodType)
                        {
                            case (int)PeriodFrameForMonthEnum.FirstWorkDay:
                                if (startDate > firstDayForCurrentMonth)
                                {
                                    dateForCurrentMonth = startDate.AddMonths(1);
                                    firstDayForCurrentMonth = new DateTime(dateForCurrentMonth.Year, dateForCurrentMonth.Month, 1);
                                }

                                day = ((int)firstDayForCurrentMonth.DayOfWeek == 0) ? 7 : (int)firstDayForCurrentMonth.DayOfWeek;

                                // Need to add day to next working Monday
                                if (day > 5)
                                {
                                    int dayDiff = 8 - day;
                                    startDate = firstDayForCurrentMonth.AddDays(dayDiff);
                                }

                                break;
                            case (int)PeriodFrameForMonthEnum.LastWorkDay:
                                DateTime lastDayForCurrentMonth = firstDayForCurrentMonth.AddMonths(1).AddDays(-1);
                                day = ((int)lastDayForCurrentMonth.DayOfWeek == 0) ? 7 : (int)lastDayForCurrentMonth.DayOfWeek;

                                // Need to substract day to current working day
                                if (day > 5)
                                {
                                    int dayDiff = day - 5;
                                    startDate = lastDayForCurrentMonth.AddDays(-dayDiff);
                                }

                                break;
                            case (int)PeriodFrameForMonthEnum.ParticularDay:
                                try
                                {
                                    startDate = new DateTime(dateForCurrentMonth.Year, dateForCurrentMonth.Month, startDeckDate.Day);
                                }
                                catch (Exception)
                                {
                                    startDate = dateForCurrentMonth.AddMonths(1).AddDays(-1);
                                }
                                break;
                        }
                        break;
                }
                // Revert back to UTC time
                startDate = startDate.AddHours(-timezoneOffset);
            }


            return startDate;
        }

        private DateTime GeneratePulseStartTimeForUpdateApplyToAll(int perFrameType, int periodType, double timezoneOffset, DateTime startDateFromUpdatedCard, DateTime originalDate, ISession mainSession)
        {
            DateTime newStartDate = originalDate;
            try
            {
                // Always convert to local time first
                startDateFromUpdatedCard = startDateFromUpdatedCard.AddHours(timezoneOffset);
                originalDate = originalDate.AddHours(timezoneOffset);
                switch (perFrameType)
                {
                    case (int)DeckPerTimeFrameTypeEnum.PerDay:
                        break;
                    case (int)DeckPerTimeFrameTypeEnum.PerWeek:
                        int updatedDay = ((int)startDateFromUpdatedCard.DayOfWeek == 0) ? 7 : (int)startDateFromUpdatedCard.DayOfWeek;
                        int originalDay = ((int)originalDate.DayOfWeek == 0) ? 7 : (int)originalDate.DayOfWeek;

                        if (updatedDay != originalDay)
                        {
                            int dayDiff = updatedDay - originalDay;
                            newStartDate = originalDate.AddDays(dayDiff);
                        }
                        break;
                    case (int)DeckPerTimeFrameTypeEnum.PerMonth:
                        DateTime dateForNextMonth = startDateFromUpdatedCard.AddMonths(1);
                        switch (periodType)
                        {
                            case (int)PeriodFrameForMonthEnum.FirstWorkDay:
                                break;
                            case (int)PeriodFrameForMonthEnum.LastWorkDay:
                                break;
                            case (int)PeriodFrameForMonthEnum.ParticularDay:
                                try
                                {
                                    newStartDate = new DateTime(dateForNextMonth.Year, dateForNextMonth.Month, newStartDate.Day);
                                }
                                catch (Exception)
                                {
                                    newStartDate = dateForNextMonth.AddMonths(1).AddDays(-1);
                                }
                                break;
                        }
                        break;
                }
                // Revert back to UTC time
                newStartDate = newStartDate.AddHours(-timezoneOffset);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return newStartDate;
        }

        private PulseCheckForCardCountOnDateResponse CheckForCardCount(string deckId, string cardId, int logicLevel, int numberOfCardsPerTimeFrame, DateTime startDate, ISession session)
        {
            PulseCheckForCardCountOnDateResponse response = new PulseCheckForCardCountOnDateResponse();
            response.Success = false;
            try
            {
                if (logicLevel <= 1)
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("pulse_dynamic_card_by_deck", new List<string>(), new List<string> { "deck_id", "start_timestamp" }));
                    RowSet countRowset = session.Execute(ps.Bind(deckId, startDate));

                    int count = 0;
                    foreach (Row countRow in countRowset)
                    {
                        // Does not include itself
                        if (countRow.GetValue<string>("id").Equals(cardId))
                        {
                            continue;
                        }

                        if (countRow.GetValue<int>("logic_level") <= 1)
                        {
                            count++;
                        }
                    }

                    if (count + 1 > numberOfCardsPerTimeFrame)
                    {
                        Log.Error(ErrorMessage.DeckInvalidRoutineDateForCard);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.DeckInvalidRoutineDateForCard);
                        response.ErrorMessage = ErrorMessage.DeckInvalidRoutineDateForCard;
                        return response;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }

    [DataContract]
    public class PulseDeck
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public bool IsCompulsory { get; set; }

        [DataMember]
        public int PublishMethodType { get; set; }

        [DataMember]
        public int NumberOfCards { get; set; }

        [DataMember]
        public int NumberOfCardsPerTimeFrame { get; set; }

        [DataMember]
        public int PerTimeFrameType { get; set; }

        [DataMember]
        public int PeriodFrameType { get; set; }

        [DataMember]
        public int Duration { get; set; }

        [DataMember]
        public bool IsDeckAnonymous { get; set; }

        [DataMember]
        public int AnonymityCount { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public bool IsPrioritized { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int Progress { get; set; }

        [DataMember]
        public DateTime DeckStartTimestamp { get; set; }

        [DataMember]
        public string DeckStartTimestampString { get; set; }

        [DataMember]
        public DateTime? DeckEndTimestamp { get; set; }

        [DataMember]
        public string DeckEndTimestampString { get; set; }

        [DataMember]
        public List<PulseDynamic> Pulses { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember]
        public bool IsForUser { get; set; }

        [DataMember]
        public List<User> TargetedUsers { get; set; }
    }

    [DataContract]
    public class DeckCardOption
    {
        [DataMember]
        public string OptionId { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public PulseDynamic NextPulse { get; set; }

        [DataMember]
        public int NumberOfSelection { get; set; }

        [DataMember]
        public int PercentageOfSelection { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NextPulseId { get; set; }
    }

    [DataContract]
    public class DeckTextAnswer
    {
        [DataMember]
        public string TextId { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public int NumberOfAlike { get; set; }

        [DataMember]
        public User AnsweredByUser { get; set; }

    }
}