﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLEducation
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        // ZX: Trying out the concept of storing max text length in model
        public const int TITLE_TEXT_MAX = 50;
        public const int INTRODUCTION_TEXT_MAX = 50;
        public const int INSTRUCTIONS_TEXT_MAX = 50;
        public const int CLOSINGWORD_TEXT_MAX = 50;
        public const int DESCRIPTION_TEXT_MAX = 26;

        public enum MLEducationQueryType
        {
            Basic = 1,
            WithoutPrivacy = 2,
            Full = 3
        }

        public enum MLEducationStatusEnum
        {
            Deleted = -1,
            Unlisted = 1,
            Active = 2,
            Hidden = 3
        }

        public enum ProgressStatusEnum
        {
            Upcoming = 1,
            Live = 2,
            Completed = 3
        }

        public enum DurationTypeEnum
        {
            Perpetual = 1,
            Schedule = 2
        }

        public enum DisplayAnswerTypeEnum
        {
            DoNotShowAnswer = 1,
            ShowAnswer = 2
        }

        public enum RequestUserProgressEnum
        {
            Absent = 1,
            Incomplete = 2,
            Completed = 3
        }

        [DataContract]
        [Serializable]
        public class MLEducationFeedback
        {
            [DataMember]
            public string Feedback { get; set; }
            [DataMember]
            public DateTime CreatedTimestamp { get; set; }
            [DataMember]
            public string CreatedTimestampString { get; set; }
        }

        [DataMember]
        public string EducationId { get; set; }

        [DataMember]
        public string IconUrl { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsAutoBreak { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int DurationType { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public String StartDateString { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public String EndDateString { get; set; }

        [DataMember]
        public int NumberOfCards { get; set; }

        [DataMember]
        public List<Chapter> Chapters { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember]
        public bool IsForUser { get; set; }

        [DataMember]
        public List<User> TargetedUsers { get; set; }

        [DataMember]
        public int ProgressStatus { get; set; }

        [DataMember]
        public MLEducationCategory Category { get; set; }

        [DataMember]
        public User Author { get; set; }

        [DataMember]
        public bool IsDisplayAuthorOnClient { get; set; }

        [DataMember]
        public int DisplayAnswerType { get; set; }

        [DataMember]
        public int LastPlayChapterIndex { get; set; }

        [DataMember]
        public int RequestUserProgress { get; set; }

        [DataMember]
        public string AnalyseDurationString { get; set; }

        [DataMember]
        public List<string> CompletedMessages { get; set; }

        /******************************/
        private MLEducation GetDetail(int queryType, int cardQueryType, MLEducationCategory category, string companyId, string educationId, string containsEducationTitle = null, string containsCardName = null, string answeredByUserId = null, ISession mainSession = null, ISession analyticSession = null)
        {
            MLEducation education = null;

            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = null;

                if (category == null)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_category_by_education",
                        new List<string> { "ml_category_id" }, new List<string> { "ml_education_id" }));
                    Row categoryByTopicRow = mainSession.Execute(ps.Bind(educationId)).FirstOrDefault();

                    if (categoryByTopicRow != null)
                    {
                        string categoryId = categoryByTopicRow.GetValue<string>("ml_category_id");
                        category = new MLEducationCategory().GetDetail(categoryId, companyId, mainSession).Category;
                    }
                    else
                    {
                        Log.Error("This ML-Education does not have a Category: EducationId: " + educationId);
                    }
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education", new List<string>(), new List<string> { "category_id", "id" }));
                Row educationRow = mainSession.Execute(ps.Bind(category.CategoryId, educationId)).FirstOrDefault();

                int status = educationRow.GetValue<int>("status");

                // Check status
                if (status == (int)MLEducationStatusEnum.Deleted)
                {
                    return education;
                }

                string title = educationRow.GetValue<string>("title");
                // Check for education title
                if (!string.IsNullOrEmpty(containsEducationTitle))
                {
                    if (!title.ToLower().Contains(containsEducationTitle.ToLower()))
                    {
                        return education;
                    }
                }

                // Check progress first
                int durationType = educationRow.GetValue<int>("duration_type");
                DateTime currentDate = DateTime.UtcNow;
                DateTime startDate = educationRow.GetValue<DateTime>("start_date");
                DateTime? endDate = educationRow.GetValue<DateTime?>("end_date");
                int progressStatus = SelectProgress(currentDate, startDate, endDate);

                string iconUrl = educationRow.GetValue<string>("icon_url");
                int numberOfCards = new MLEducationCard().SelectNumberOfCards(educationId, mainSession);
                bool isAutoBreak = educationRow.GetValue<bool>("is_auto_break");
                string description = educationRow.GetValue<string>("description");

                bool isForEveryone = false;
                bool isForDepartment = false;
                List<Department> selectedDepartments = new List<Department>();
                bool isForUser = false;
                List<User> selectedUsers = new List<User>();
                bool isDisplayAuthorOnClient = educationRow.GetValue<bool>("is_display_author_on_client");

                List<MLEducationCard> cards = new List<MLEducationCard>();

                int displayAnswerType = educationRow.GetValue<int>("display_answer_type");

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_privacy", new List<string>(), new List<string> { "ml_education_id" }));
                Row privacyRow = mainSession.Execute(ps.Bind(educationId)).FirstOrDefault();

                isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                isForDepartment = privacyRow.GetValue<bool>("is_for_department");
                isForUser = privacyRow.GetValue<bool>("is_for_user");

                User author = new User().SelectUserBasic(educationRow.GetValue<string>("created_by_admin_id"), companyId, false, mainSession, null, true).User;

                if (queryType > (int)MLEducationQueryType.Basic)
                {
                    cards = new MLEducationCard().GetList(educationId, category.CategoryId, null, companyId, cardQueryType, containsCardName, answeredByUserId, mainSession, analyticSession).Cards;

                    if (queryType > (int)MLEducationQueryType.WithoutPrivacy)
                    {
                        if (!isForEveryone)
                        {
                            if (isForDepartment)
                            {
                                Department departmentManager = new Department();
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_department", new List<string>(), new List<string> { "ml_education_id" }));
                                RowSet departmentRowset = mainSession.Execute(ps.Bind(educationId));
                                foreach (Row departmentRow in departmentRowset)
                                {
                                    string departmentId = departmentRow.GetValue<string>("department_id");
                                    Department department = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;
                                    if (department != null)
                                    {
                                        selectedDepartments.Add(department);
                                    }
                                }
                            }

                            if (isForUser)
                            {
                                User userManager = new User();
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_user", new List<string>(), new List<string> { "ml_education_id" }));
                                RowSet userRowset = mainSession.Execute(ps.Bind(educationId));
                                foreach (Row userRow in userRowset)
                                {
                                    string userId = userRow.GetValue<string>("user_id");
                                    User user = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                                    if (user != null)
                                    {
                                        selectedUsers.Add(user);
                                    }
                                }
                            }
                        }
                    }
                }

                #region Set Chapters
                List<Chapter> cps = new List<Chapter>();
                if (cards.Count > 0)
                {
                    if (isAutoBreak)
                    {
                        Chapter cp = new Chapter();
                        cp.Cards = new List<MLEducationCard>();
                        cp.Index = 0;

                        int index = 0;
                        for (int i = 0; i < cards.Count; i++)
                        {
                            if (cards[i].Type != (int)MLEducationCard.CardType.SelectOne && cards[i].Type != (int)MLEducationCard.CardType.MultiChoice)
                            {
                                if (i == 0)
                                {
                                    cards[i].Paging = index + 1;
                                    cp.Cards.Add(cards[i]);
                                }
                                else
                                {
                                    cps.Add(cp);

                                    // new chapter
                                    index++;
                                    cp = new Chapter();
                                    cp.Cards = new List<MLEducationCard>();
                                    cp.Index = index;
                                    cards[i].Paging = index + 1;
                                    cp.Cards.Add(cards[i]);
                                }
                            }
                            else
                            {
                                cards[i].Paging = index + 1;
                                cp.Cards.Add(cards[i]);
                            }
                        }

                        cps.Add(cp);
                    }
                    else
                    {
                        for (int i = 0; i < cards.Count; i++)
                        {
                            cards[i].Paging = 1;
                        }

                        Chapter cp = new Chapter();
                        cp.Index = 1;
                        cp.Cards = cards;
                        cps.Add(cp);
                    }
                }
                #endregion

                #region Get Last play chapter for the user.
                int userProgress = (int)RequestUserProgressEnum.Incomplete;
                int lastPlayChapterIndex = 0;
                MLEducationCard lastCard = null;
                if (!string.IsNullOrEmpty(answeredByUserId))
                {
                    if (cps.Count > 0) // Get last chapter index
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_incomplete", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                        Row row = analyticSession.Execute(ps.Bind(educationId, answeredByUserId)).FirstOrDefault();
                        if (row != null) // The user has not completed this education.
                        {
                            for (int i = 0; i < cps.Count; i++)
                            {
                                for (int j = 0; j < cps[i].Cards.Count; j++)
                                {
                                    if (cps[i].Cards[j].Attempt > 0)
                                    {
                                        lastCard = cps[i].Cards[j];
                                    }
                                }
                            }

                            if (lastCard != null)
                            {
                                lastPlayChapterIndex = lastCard.Paging - 1;
                                if (cps[lastCard.Paging - 1].Cards[cps[lastCard.Paging - 1].Cards.Count - 1].CardId == lastCard.CardId)
                                {
                                    if (lastPlayChapterIndex < cps.Count - 1)
                                    {
                                        lastPlayChapterIndex = lastPlayChapterIndex + 1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_completed", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                            row = analyticSession.Execute(ps.Bind(educationId, answeredByUserId)).FirstOrDefault();
                            if (row != null)// The user has completed this education.
                            {
                                userProgress = (int)RequestUserProgressEnum.Completed;
                            }
                            else
                            {
                                userProgress = (int)RequestUserProgressEnum.Absent;
                            }
                        }
                    }
                    else // only get user progress
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_incomplete", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                        Row row = analyticSession.Execute(ps.Bind(educationId, answeredByUserId)).FirstOrDefault();
                        if (row == null)
                        {

                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_completed", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                            row = analyticSession.Execute(ps.Bind(educationId, answeredByUserId)).FirstOrDefault();
                            if (row != null)// The user has completed this education.
                            {
                                userProgress = (int)RequestUserProgressEnum.Completed;
                            }
                            else
                            {
                                userProgress = (int)RequestUserProgressEnum.Absent;
                            }
                        }
                    }
                }
                #endregion

                education = new MLEducation
                {
                    EducationId = educationId,
                    Title = title,
                    IconUrl = iconUrl,
                    Description = description,
                    Category = category,
                    IsForEveryone = isForEveryone,
                    IsForDepartment = isForDepartment,
                    TargetedDepartments = selectedDepartments,
                    IsForUser = isForUser,
                    TargetedUsers = selectedUsers,
                    Status = status,
                    DurationType = durationType,
                    IsAutoBreak = isAutoBreak,
                    DisplayAnswerType = displayAnswerType,
                    Chapters = cps,
                    NumberOfCards = cards.Count,
                    StartDate = new DateTime(startDate.Ticks, DateTimeKind.Utc),
                    EndDate = endDate == null ? null : new DateTime?(new DateTime(endDate.Value.Ticks, DateTimeKind.Utc)),
                    ProgressStatus = progressStatus,
                    IsDisplayAuthorOnClient = isDisplayAuthorOnClient,
                    Author = author,
                    LastPlayChapterIndex = lastPlayChapterIndex,
                    RequestUserProgress = userProgress,
                    CompletedMessages = DefaultResource.EducationCompletedMessages.Split(';').ToList<string>()
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return education;
        }

        private MLValidationResponse ValidateEducationFields(string title, string iconUrl, string description)
        {
            MLValidationResponse response = new MLValidationResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(title))
                {
                    Log.Error(ErrorMessage.MLTopicTitleEmpty);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationTitleEmpty);
                    response.ErrorMessage = ErrorMessage.MLEducationTitleEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(iconUrl))
                {
                    Log.Error(ErrorMessage.MLTopicIconMissing);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationIconMissing);
                    response.ErrorMessage = ErrorMessage.MLEducationIconMissing;
                    return response;
                }


                //if (string.IsNullOrEmpty(description))
                //{
                //    Log.Error(ErrorMessage.MLTopicClosingWordsEmpty);
                //    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationDescriptionEmpty);
                //    response.ErrorMessage = ErrorMessage.MLEducationDescriptionEmpty;
                //    return response;
                //}

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private MLValidationResponse ValidateDate(int durationType, DateTime currentDate, DateTime? startDate, DateTime? endDate)
        {
            MLValidationResponse response = new MLValidationResponse();
            response.Success = false;
            try
            {
                if (startDate == null)
                {
                    Log.Error(ErrorMessage.MLDateIsNull);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                    response.ErrorMessage = ErrorMessage.MLDateIsNull;
                    return response;
                }

                // End date cannot be null for schedule
                if (durationType == (int)DurationTypeEnum.Schedule)
                {
                    if (endDate == null)
                    {
                        Log.Error(ErrorMessage.MLDateIsNull);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLDateIsNull);
                        response.ErrorMessage = ErrorMessage.MLDateIsNull;
                        return response;
                    }

                    if (endDate <= currentDate)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanToday);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanToday;
                        return response;
                    }

                    if (startDate >= endDate)
                    {
                        Log.Error(ErrorMessage.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.MLTopicEndDateEarlierThanStartDate;
                        return response;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private List<BoundStatement> CreatePrivacy(string educationId, List<string> targetedDepartmentIds, List<string> targetedUserIds, ISession session)
        {
            List<BoundStatement> boundStatements = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;
                bool isForDepartment = targetedDepartmentIds == null || targetedDepartmentIds.Count == 0 ? false : true;
                bool isForUser = targetedUserIds == null || targetedUserIds.Count == 0 ? false : true;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_privacy", new List<string> { "ml_education_id", "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group" }));
                boundStatements.Add(ps.Bind(educationId, isForEveryone, isForDepartment, isForUser, false));

                foreach (string departmentId in targetedDepartmentIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_department_targeted_education", new List<string> { "department_id", "ml_education_id" }));
                    boundStatements.Add(ps.Bind(departmentId, educationId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_targeted_department", new List<string> { "department_id", "ml_education_id" }));
                    boundStatements.Add(ps.Bind(departmentId, educationId));
                }

                foreach (string userId in targetedUserIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_targeted_user", new List<string> { "user_id", "ml_education_id" }));
                    boundStatements.Add(ps.Bind(userId, educationId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_user_targeted_education", new List<string> { "user_id", "ml_education_id" }));
                    boundStatements.Add(ps.Bind(userId, educationId));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatements;
        }

        public int SelectProgress(DateTime currentDate, DateTime startDate, DateTime? endDate)
        {
            int progress = (int)ProgressStatusEnum.Upcoming;
            try
            {
                if (startDate <= currentDate)
                {
                    progress = (int)ProgressStatusEnum.Live;

                    if (endDate.HasValue && endDate <= currentDate)
                    {
                        progress = (int)ProgressStatusEnum.Completed;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return progress;
        }

        public MLEducationDetailResponse GetDetail(string companyId, string adminUserId, string categoryId, string educationId, string containsCardName = null, int educationQueryType = (int)MLEducationQueryType.Full, int educationCardQueryType = (int)MLEducationCard.QueryType.Full, string answeredUserId = null, ISession mainSession = null)
        {
            MLEducationDetailResponse response = new MLEducationDetailResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                MLEducationCategory category = new MLEducationCategory().GetDetail(categoryId, companyId, mainSession).Category;
                response.Education = GetDetail(educationQueryType, educationCardQueryType, category, companyId, educationId, null, containsCardName, answeredUserId, mainSession, analyticSession);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public MLEducationListResponse GetList(string adminUserId, string companyId, string selectedCategoryId = null, string containsName = null)
        {
            MLEducationListResponse response = new MLEducationListResponse();
            response.Educations = new List<MLEducation>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = null;
                RowSet categoryRowset = null;

                if (string.IsNullOrEmpty(selectedCategoryId))
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_category", new List<string>(), new List<string> { "company_id", "is_valid" }));
                    categoryRowset = mainSession.Execute(ps.Bind(companyId, true));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_category", new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                    categoryRowset = mainSession.Execute(ps.Bind(companyId, selectedCategoryId, true));
                }

                foreach (Row categoryRow in categoryRowset)
                {
                    string categoryId = categoryRow.GetValue<string>("id");
                    string title = categoryRow.GetValue<string>("title");

                    MLEducationCategory category = new MLEducationCategory
                    {
                        CategoryId = categoryId,
                        Title = title
                    };

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_by_category", new List<string>(), new List<string> { "ml_category_id" }));
                    RowSet educationRowset = mainSession.Execute(ps.Bind(categoryId));

                    foreach (Row row in educationRowset)
                    {
                        string educationId = row.GetValue<string>("ml_education_id");
                        MLEducation selectedEducation = GetDetail((int)MLEducationQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, category, companyId, educationId, containsName, null, null, mainSession, analyticSession);

                        if (selectedEducation != null)
                        {
                            response.Educations.Add(selectedEducation);
                        }
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationListResponse GetListByUser(string requestUserId, string companyId, string containsName = null, string selectedCategoryId = null, ISession mainSession = null)
        {
            MLEducationListResponse response = new MLEducationListResponse();
            response.Educations = new List<MLEducation>();
            response.Success = false;

            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(requestUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = null;
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string>(), new List<string> { "user_id" }));
                RowSet rsDepartment = mainSession.Execute(ps.Bind(requestUserId));
                if (rsDepartment == null)
                {
                    Log.Error(ErrorMessage.UserMissDepartment);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.UserMissDepartment);
                    response.ErrorMessage = ErrorMessage.UserMissDepartment;
                    return response;
                }

                List<Department> departments = new List<Department>();
                foreach (Row rowDepartment in rsDepartment)
                {
                    departments.Add(new Department { Id = rowDepartment.GetValue<string>("department_id") });
                }

                User requestUser = new User { UserId = requestUserId, Departments = departments };

                RowSet rsCategory = null;

                if (string.IsNullOrEmpty(selectedCategoryId))
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_category", new List<string>(), new List<string> { "company_id", "is_valid" }));
                    rsCategory = mainSession.Execute(ps.Bind(companyId, true));
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_category", new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                    rsCategory = mainSession.Execute(ps.Bind(companyId, selectedCategoryId, true));
                }

                if (rsCategory != null)
                {
                    foreach (Row rowCategory in rsCategory)
                    {
                        string categoryId = rowCategory.GetValue<string>("id");
                        MLEducationCategory category = null;
                        MLEducationCategoryDetailResponse responseCategory = new MLEducationCategory().GetDetail(categoryId, companyId, mainSession);
                        if (responseCategory.Success)
                        {
                            category = responseCategory.Category;
                        }

                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_by_timestamp", new List<string>(), new List<string> { "ml_category_id" }));
                        RowSet rsEducation = mainSession.Execute(ps.Bind(categoryId));
                        if (rsEducation != null)
                        {
                            foreach (Row rowEducaation in rsEducation)
                            {
                                DateTime currentDateTime = DateTime.UtcNow;

                                #region Check privacy
                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_privacy", new List<string>(), new List<string> { "ml_education_id" }));
                                Row rowPrivacy = mainSession.Execute(ps.Bind(rowEducaation.GetValue<string>("ml_education_id"))).FirstOrDefault();
                                if (rowPrivacy != null)
                                {
                                    if (rowPrivacy.GetValue<bool>("is_for_everyone"))
                                    {
                                        MLEducation education = new MLEducation().GetDetail(
                                            (int)MLEducationQueryType.Basic,
                                            (int)MLEducationCard.QueryType.Basic,
                                            category,
                                            companyId,
                                            rowEducaation.GetValue<string>("ml_education_id"),
                                            containsName,
                                            null,
                                            requestUserId,
                                            mainSession,
                                            null
                                            );
                                        if (education != null && education.Status == (int)MLEducation.MLEducationStatusEnum.Active && education.StartDate < currentDateTime)
                                        {
                                            response.Educations.Add(education);
                                        }
                                    }
                                    else
                                    {
                                        bool hasFound = false;
                                        #region Is the user in department privacy.
                                        if (rowPrivacy.GetValue<bool>("is_for_department"))
                                        {
                                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_department", new List<string>(), new List<string> { "ml_education_id" }));
                                            RowSet rsPrivacyDepartment = mainSession.Execute(ps.Bind(rowEducaation.GetValue<string>(rowEducaation.GetValue<string>("ml_education_id"))));
                                            if (rsPrivacyDepartment != null)
                                            {
                                                foreach (Row rowPrivacyDepartment in rsPrivacyDepartment)
                                                {
                                                    foreach (Department department in requestUser.Departments)
                                                    {
                                                        if (department.Id == rowPrivacyDepartment.GetValue<string>("department_id"))
                                                        {
                                                            MLEducation education = new MLEducation().GetDetail(
                                                                (int)MLEducationQueryType.Basic,
                                                                (int)MLEducationCard.QueryType.Basic,
                                                                category,
                                                                companyId,
                                                                rowEducaation.GetValue<string>("ml_education_id"),
                                                                containsName,
                                                                null,
                                                                requestUserId,
                                                                mainSession,
                                                                null
                                                                    );

                                                            if (education != null && education.Status == (int)MLEducation.MLEducationStatusEnum.Active && education.StartDate < currentDateTime)
                                                            {
                                                                hasFound = true;
                                                                response.Educations.Add(education);
                                                            }
                                                            break;
                                                        }
                                                    }

                                                    if (hasFound)
                                                    {
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                        #region Is the user in privacy.
                                        if (!hasFound)
                                        {
                                            if (rowPrivacy.GetValue<bool>("is_for_user"))
                                            {
                                                ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_user", new List<string>(), new List<string> { "ml_education_id" }));
                                                RowSet rsPrivacyUser = mainSession.Execute(ps.Bind(rowEducaation.GetValue<string>("ml_education_id")));
                                                if (rsPrivacyUser != null)
                                                {
                                                    foreach (Row rowPrivacyUser in rsPrivacyUser)
                                                    {
                                                        if (rowPrivacyUser.GetValue<string>("user_id") == requestUserId)
                                                        {
                                                            MLEducation education = new MLEducation().GetDetail(
                                                                (int)MLEducationQueryType.Basic,
                                                                (int)MLEducationCard.QueryType.Basic,
                                                                category,
                                                                companyId,
                                                                rowEducaation.GetValue<string>("ml_education_id"),
                                                                containsName,
                                                                null,
                                                                requestUserId,
                                                                mainSession,
                                                                null
                                                                    );

                                                            if (education != null && education.Status == (int)MLEducation.MLEducationStatusEnum.Active && education.StartDate < currentDateTime)
                                                            {
                                                                response.Educations.Add(education);
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCreateResponse Create(string adminUserId,
                                           string companyId,
                                           string title,
                                           string description,
                                           string iconUrl,
                                           string categoryId,
                                           string categoryTitle,
                                           int durationType,
                                           List<string> targetedDepartmentIds,
                                           List<string> targetedUserIds,
                                           DateTime? startDate,
                                           DateTime? endDate,
                                           int displayAnswerType = 1,
                                           bool isDisplayAuthorOnClient = true,
                                           bool isAutoBreak = true)
        {
            MLEducationCreateResponse response = new MLEducationCreateResponse();
            response.Education = null;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                MLValidationResponse validateFieldResponse = ValidateEducationFields(title, iconUrl, description);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                DateTime currentDate = DateTime.UtcNow;
                MLValidationResponse validateDateResponse = ValidateDate(durationType, currentDate, startDate, endDate);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                // Generate IDs
                string educationId = UUIDGenerator.GenerateUniqueIDForMLEducation();
                MLEducationCategory categoryManager = new MLEducationCategory();
                if (string.IsNullOrEmpty(categoryId))
                {
                    categoryId = UUIDGenerator.GenerateUniqueIDForMLEducationCategory();
                    MLEducationCategoryCreateResponse categoryResponse = categoryManager.Create(companyId, categoryTitle, adminUserId, session, categoryId);

                    if (!categoryResponse.Success)
                    {
                        Log.Error(categoryResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(categoryResponse.ErrorCode);
                        response.ErrorMessage = categoryResponse.ErrorMessage;
                        return response;
                    }
                }
                else
                {
                    Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);
                    if (categoryRow == null)
                    {
                        Log.Error("Invalid MLEducationCategoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                        return response;
                    }
                }

                BatchStatement batch = new BatchStatement();
                List<BoundStatement> privacyStatments = CreatePrivacy(educationId, targetedDepartmentIds, targetedUserIds, session);

                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("ml_education",
                    new List<string> { "category_id", "id", "created_by_admin_id", "created_on_timestamp", "description", "display_answer_type", "duration_type", "end_date", "icon_url", "is_auto_break", "is_display_author_on_client", "last_modified_by_admin_id", "last_modified_timestamp", "start_date", "status", "title" }));
                batch.Add(ps.Bind(categoryId, educationId, adminUserId, currentDate, description, displayAnswerType, durationType, endDate, iconUrl, isAutoBreak, isDisplayAuthorOnClient, adminUserId, currentDate, startDate, (int)MLEducationStatusEnum.Unlisted, title));

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_by_category",
                    new List<string> { "ml_education_id", "ml_category_id" }));
                batch.Add(ps.Bind(educationId, categoryId));

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_category_by_education",
                    new List<string> { "ml_education_id", "ml_category_id" }));
                batch.Add(ps.Bind(educationId, categoryId));

                foreach (BoundStatement bs in privacyStatments)
                {
                    batch.Add(bs);
                }

                session.Execute(batch);

                response.Education = GetDetail((int)MLEducationQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, categoryManager.GetDetail(categoryId, companyId, session).Category, companyId, educationId, null, null, null, session);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationUpdateResponse UpdateStatus(string educationId, string categoryId, string adminUserId, string companyId, int updatedStatus)
        {
            MLEducationUpdateResponse response = new MLEducationUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);
                if (categoryRow == null)
                {
                    Log.Error("Invalid MLEducationCategoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                    return response;
                }
                MLEducationCategory category = new MLEducationCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLEducation(companyId, categoryId, educationId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid educationId: " + educationId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                #region Check card count.
                int cardCount = 0;
                ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card", new List<string>(), new List<string> { "ml_education_id" }));
                RowSet rsCards = session.Execute(ps.Bind(educationId));
                if (rsCards != null)
                {
                    foreach (Row rCard in rsCards)
                    {
                        cardCount++;
                    }
                }

                if (cardCount < 1 && (updatedStatus == (int)MLEducation.MLEducationStatusEnum.Active || updatedStatus == (int)MLEducation.MLEducationStatusEnum.Hidden))
                {
                    Log.Error(ErrorMessage.MLEducationCardCountMustMoreThenZero);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCardCountMustMoreThenZero);
                    response.ErrorMessage = string.Format(ErrorMessage.MLEducationCardCountMustMoreThenZero);
                    return response;
                }
                #endregion

                int currentStatus = topicRow.GetValue<int>("status");

                if (currentStatus == (int)MLEducationStatusEnum.Deleted)
                {
                    Log.Error("Education already been deleted");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.MLEducationAlreadyDeleted;
                    return response;
                }

                DateTime startTime = topicRow.GetValue<DateTime>("start_date");

                if (updatedStatus == (int)MLEducationStatusEnum.Deleted || updatedStatus == (int)MLEducationStatusEnum.Hidden)
                {
                    if (updatedStatus == (int)MLEducationStatusEnum.Deleted)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_by_category", new List<string> { "ml_category_id", "ml_education_id" }));
                        deleteBatch.Add(ps.Bind(categoryId, educationId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_category_by_education", new List<string> { "ml_category_id", "ml_education_id" }));
                        deleteBatch.Add(ps.Bind(categoryId, educationId));

                        // Delete all cards
                        new MLEducationCard().DeleteAllCards(educationId, companyId, session);
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(categoryId, startTime, educationId));
                }
                else if (updatedStatus == (int)MLEducationStatusEnum.Active)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_education_id" }));
                    updateBatch.Add(ps.Bind(categoryId, startTime, educationId));
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("ml_education", new List<string> { "category_id", "id" }, new List<string> { "status", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(updatedStatus, adminUserId, DateTime.UtcNow, categoryId, educationId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                if (updatedStatus == (int)MLEducationStatusEnum.Active || updatedStatus == (int)MLEducationStatusEnum.Hidden)
                {
                    response.Education = GetDetail((int)MLEducationQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, new MLEducationCategory().GetDetail(categoryId, companyId, session).Category, companyId, educationId, null, null, null, session);
                }
                else
                {
                    response.Education = null;
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationUpdateResponse Update(string educationId,
                                         string adminUserId,
                                         string companyId,
                                         string newTitle,
                                         string newDescription,
                                         string newIconUrl,
                                         string newCategoryId,
                                         string newCategoryTitle,
                                         int newDurationType,
                                         int newStatus,
                                         List<string> newTargetedDepartmentIds,
                                         List<string> newTargetedUserIds,
                                         DateTime newStartDate,
                                         DateTime? newEndDate,
                                         int newDisplayAnswerType = 2,
                                         bool newIsDisplayAuthorOnClient = true,
                                         bool newIsAutoBreak = true
                                         )
        {
            MLEducationUpdateResponse response = new MLEducationUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check category
                PreparedStatement ps = null;
                bool isUpdateCategory = false;
                string currentCategoryId = string.Empty;

                if (!string.IsNullOrEmpty(newCategoryId))
                {
                    Row rsCategoryRow = vh.ValidateMLEducationCategory(companyId, newCategoryId, session);
                    if (rsCategoryRow == null)
                    {
                        Log.Error("Invalid MLCategoryId: " + newCategoryId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                        return response;
                    }
                }

                // Fetch current categoryId
                ps = session.Prepare(CQLGenerator.SelectStatement("ml_category_by_education", new List<string>(), new List<string> { "ml_education_id" }));
                Row categoryRow = session.Execute(ps.Bind(educationId)).FirstOrDefault();

                if (categoryRow == null)
                {
                    Log.Error("Education already been deleted: " + educationId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.MLEducationAlreadyDeleted;
                    return response;
                }
                else
                {
                    currentCategoryId = categoryRow.GetValue<string>("ml_category_id");

                    if (!currentCategoryId.Equals(newCategoryId))
                    {
                        isUpdateCategory = true;
                    }
                }

                // Check education
                Row educationRow = vh.ValidateMLEducation(companyId, currentCategoryId, educationId, session);
                if (educationRow == null)
                {
                    Log.Error("Invalid educationId: " + educationId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                    return response;
                }

                // Check status first
                int currentStatus = educationRow.GetValue<int>("status");

                DateTime currentDate = DateTime.UtcNow;
                DateTime currentStartDate = educationRow.GetValue<DateTime>("start_date");
                DateTime? currentEndDate = educationRow.GetValue<DateTime?>("end_date");
                int progress = SelectProgress(currentDate, currentStartDate, currentEndDate);

                if (currentStatus != (int)MLEducationStatusEnum.Unlisted)
                {
                    // Status active or hidden
                    // Check progress
                    // Change of start date
                    if (currentStartDate != newStartDate)
                    {
                        if (progress != (int)ProgressStatusEnum.Upcoming)
                        {
                            Log.Error("Education in progress, start date cannot be changed: " + educationId);
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationStartDateCannotBeModified);
                            response.ErrorMessage = ErrorMessage.MLEducationStartDateCannotBeModified;
                            return response;
                        }
                    }
                }

                MLValidationResponse validateFieldResponse = ValidateEducationFields(newTitle, newIconUrl, newDescription);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                int durationType = educationRow.GetValue<int>("duration_type");
                MLValidationResponse validateDateResponse = ValidateDate(durationType, currentDate, newStartDate, newEndDate);
                if (!validateFieldResponse.Success)
                {
                    response.ErrorCode = validateFieldResponse.ErrorCode;
                    response.ErrorMessage = validateFieldResponse.ErrorMessage;
                    return response;
                }

                // if date validated
                MLEducationCategory categoryManager = new MLEducationCategory();
                if (string.IsNullOrEmpty(newCategoryId))
                {
                    newCategoryId = UUIDGenerator.GenerateUniqueIDForMLCategory();
                    MLEducationCategoryCreateResponse categoryResponse = categoryManager.Create(companyId, newCategoryTitle, adminUserId, session, newCategoryId);

                    if (!categoryResponse.Success)
                    {
                        Log.Error(categoryResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(categoryResponse.ErrorCode);
                        response.ErrorMessage = categoryResponse.ErrorMessage;
                        return response;
                    }
                }

                #region Check card count.
                int cardCount = 0;
                ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card", new List<string>(), new List<string> { "ml_education_id" }));
                RowSet rsCards = session.Execute(ps.Bind(educationId));
                if (rsCards != null)
                {
                    foreach (Row rCard in rsCards)
                    {
                        cardCount++;
                    }
                }

                if (cardCount < 1 && (newStatus == (int)MLEducation.MLEducationStatusEnum.Active || newStatus == (int)MLEducation.MLEducationStatusEnum.Hidden))
                {
                    Log.Error(ErrorMessage.MLEducationCardCountMustMoreThenZero);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCardCountMustMoreThenZero);
                    response.ErrorMessage = string.Format(ErrorMessage.MLEducationCardCountMustMoreThenZero);
                    return response;
                }
                #endregion

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                DateTime createdTimestamp = educationRow.GetValue<DateTime>("created_on_timestamp");
                string createdAdminId = educationRow.GetValue<string>("created_by_admin_id");

                if (isUpdateCategory)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education", new List<string> { "category_id", "id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, educationId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, currentStartDate, educationId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_by_category", new List<string> { "ml_category_id", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, educationId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_category_by_education", new List<string> { "ml_category_id", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, educationId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_by_category",
                        new List<string> { "ml_education_id", "ml_category_id" }));
                    updateBatch.Add(ps.Bind(educationId, newCategoryId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_category_by_education",
                        new List<string> { "ml_education_id", "ml_category_id" }));
                    updateBatch.Add(ps.Bind(educationId, newCategoryId));
                }

                // Active -> insert into timestamp
                if (newStatus == (int)MLEducationStatusEnum.Active)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_by_timestamp",
                        new List<string> { "ml_category_id", "start_on_timestamp", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(currentCategoryId, currentStartDate, educationId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_by_timestamp",
                        new List<string> { "ml_category_id", "ml_education_id", "start_on_timestamp" }));
                    updateBatch.Add(ps.Bind(newCategoryId, educationId, newStartDate));
                }
                else
                {
                    // Not active -> remove from timestamp
                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_by_timestamp", new List<string> { "ml_category_id", "start_on_timestamp", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(newCategoryId, currentStartDate, educationId));
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("ml_education",
                   new List<string> { "id", "category_id" }, new List<string> { "created_by_admin_id", "created_on_timestamp",  "description", "display_answer_type",
                       "duration_type", "end_date","icon_url","is_auto_break","is_display_author_on_client","last_modified_by_admin_id", "last_modified_timestamp",
                       "start_date","status","title"}, new List<string>()));
                updateBatch.Add(ps.Bind(createdAdminId, createdTimestamp, newDescription, newDisplayAnswerType, newDurationType, newEndDate,
                    newIconUrl, newIsAutoBreak, newIsDisplayAuthorOnClient, adminUserId, currentDate, newStartDate, newStatus, newTitle,



                       educationId, newCategoryId));

                if (progress == (int)ProgressStatusEnum.Upcoming || currentStatus == (int)MLEducationStatusEnum.Unlisted)
                {
                    List<BoundStatement> privacyStatement = UpdatePrivacy(educationId, newTargetedDepartmentIds, newTargetedUserIds, session);
                    foreach (BoundStatement bs in privacyStatement)
                    {
                        updateBatch.Add(bs);
                    }
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Education = GetDetail((int)MLEducationQueryType.Basic, (int)MLCard.MLCardQueryType.Basic, categoryManager.GetDetail(newCategoryId, companyId, session).Category, companyId, educationId, null, null, null, session);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<BoundStatement> UpdatePrivacy(string educationId, List<string> newTargetedDepartmentIds, List<string> newTargetedUserIds, ISession session)
        {
            try
            {
                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_department",
                    new List<string>(), new List<string> { "ml_education_id" }));
                RowSet departmentPrivacyRowset = session.Execute(ps.Bind(educationId));

                foreach (Row departmentPrivacyRow in departmentPrivacyRowset)
                {
                    string departmentId = departmentPrivacyRow.GetValue<string>("department_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_department_targeted_education", new List<string> { "department_id", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(departmentId, educationId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_targeted_department", new List<string> { "ml_education_id" }));
                deleteBatch.Add(ps.Bind(educationId));

                ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_targeted_user",
                    new List<string>(), new List<string> { "ml_education_id" }));
                RowSet userPrivacyRowset = session.Execute(ps.Bind(educationId));

                foreach (Row userPrivacyRow in userPrivacyRowset)
                {
                    string userId = userPrivacyRow.GetValue<string>("user_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_user_targeted_education", new List<string> { "user_id", "ml_education_id" }));
                    deleteBatch.Add(ps.Bind(userId, educationId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_targeted_user", new List<string> { "ml_education_id" }));
                deleteBatch.Add(ps.Bind(educationId));

                session.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return CreatePrivacy(educationId, newTargetedDepartmentIds, newTargetedUserIds, session);
        }

        public MLEducationDetailResponse GetDetailByUser(string requesterUserId, string companyId, string educationId, string categoryId)
        {
            MLEducationDetailResponse response = new MLEducationDetailResponse();
            response.Education = new MLEducation();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                    return response;
                }
                MLEducationCategory category = new MLEducationCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row educationRow = vh.ValidateMLEducation(companyId, categoryId, educationId, mainSession);

                if (educationRow == null)
                {
                    Log.Error("Invalid educationId: " + educationId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                    return response;
                }

                DateTime startDate = educationRow.GetValue<DateTime>("start_date");
                DateTime? endDate = educationRow.GetValue<DateTime?>("end_date");

                // Track progress
                DateTime currentTime = DateTime.UtcNow;
                if (endDate.HasValue && endDate.Value <= currentTime)
                {
                    Log.Error("MLearning has ended: " + educationId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicEnded);
                    response.ErrorMessage = ErrorMessage.MLTopicEnded;
                    return response;
                }

                response.Education = GetDetail((int)MLEducationQueryType.Full, (int)MLCard.MLCardQueryType.Full, category, companyId, educationId, null, null, requesterUserId, mainSession, analyticSession);

                // 如果 user 第一次 get 此 education。將 RequestUserProgress 改成 incomplete
                if (response.Education.RequestUserProgress == (int)RequestUserProgressEnum.Absent)
                {
                    new AnalyticMLEducation().StartActivity(requesterUserId, educationId, analyticSession);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }


        public MLEducationCompletedResponse CompletedEducation(string requesterUserId, string companyId, string educationId, string categoryId)
        {
            MLEducationCompletedResponse response = new MLEducationCompletedResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                #region Step 1. Check input data
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                    return response;
                }

                Row educationRow = vh.ValidateMLEducation(companyId, categoryId, educationId, mainSession);
                if (educationRow == null)
                {
                    Log.Error("Invalid educationId: " + educationId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                    return response;
                }
                #endregion

                #region Step 2. Call CompletedEducation method.
                response = new AnalyticMLEducation().CompletedEducation(requesterUserId, educationId, analyticSession);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

    }

    [Serializable]
    [DataContract]
    public class Chapter
    {
        [DataMember]
        public int Index { get; set; }

        [DataMember]
        public List<MLEducationCard> Cards { get; set; }

        [DataMember]
        public int Attempt { get; set; }
    }
}
