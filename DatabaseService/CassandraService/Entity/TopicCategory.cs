﻿using Cassandra;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;
using CassandraService.Entity;
using CassandraService.ServiceResponses;
using CassandraService.GlobalResources;
using CassandraService.CassandraUtilities;
using CassandraService.Utilities;
using CassandraService.Validation;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class TopicCategory
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public long TotalNumberOfTopics { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Topic> Topics { get; set; }

        public CategoryCreateResponse Create(String adminUserId, String companyId, String categoryTitle, ISession session = null)
        {
            CategoryCreateResponse response = new CategoryCreateResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    #region Step 1. Check data.
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    #region Step 1.1 Check Admin account's validation.
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                    #endregion
                }


                #region Step 1.2 Check Title of Category.
                if (string.IsNullOrEmpty(categoryTitle.Trim()))
                {
                    Log.Error(ErrorMessage.CategoryMissingTitle);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CategoryMissingTitle);
                    response.ErrorMessage = ErrorMessage.CategoryMissingTitle;
                    return response;
                }

                PreparedStatement ps_topic_category_by_company_title = session.Prepare(CQLGenerator.SelectStatement("topic_category_by_company_title", new List<string> { }, new List<string> { "company_id", "topic_category_title" }));
                BoundStatement bs_topic_category_by_company_title = ps_topic_category_by_company_title.Bind(companyId, categoryTitle);
                Row row_topic_category_by_company_title = session.Execute(bs_topic_category_by_company_title).FirstOrDefault();
                if (row_topic_category_by_company_title != null)
                {
                    Log.Error(ErrorMessage.CategoryNameHasBeenUsed);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CategoryNameHasBeenUsed);
                    response.ErrorMessage = ErrorMessage.CategoryNameHasBeenUsed;
                    return response;
                }
                #endregion
                    #endregion

                #region Step 2. Insert new category to databse.
                BatchStatement bs = new BatchStatement();
                String categoryId = UUIDGenerator.GenerateUniqueIDForTopicCategory();

                PreparedStatement psCategory = session.Prepare(CQLGenerator.InsertStatement("topic_category",
                    new List<string> { "id", "title", "company_id", "is_valid", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                bs.Add(psCategory.Bind(categoryId, categoryTitle, companyId, true, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));

                PreparedStatement psCategoryByTitle = session.Prepare(CQLGenerator.InsertStatement("topic_category_by_company_title",
                    new List<string> { "topic_category_id", "topic_category_title", "company_id" }));
                bs.Add(psCategoryByTitle.Bind(categoryId, categoryTitle, companyId));

                session.Execute(bs);
                response.Success = true;
                response.NewCategoryId = categoryId;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CategoryUpdateResponse Update(String adminUserId, String companyId, String categoryId, String categoryTitle)
        {
            CategoryUpdateResponse response = new CategoryUpdateResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check Title of Category.
                if (string.IsNullOrEmpty(categoryTitle.Trim()))
                {
                    Log.Error(ErrorMessage.CategoryMissingTitle);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CategoryMissingTitle);
                    response.ErrorMessage = ErrorMessage.CategoryMissingTitle;
                    return response;
                }

                PreparedStatement ps_topic_category_by_company_title = session.Prepare(CQLGenerator.SelectStatement("topic_category_by_company_title", new List<string> { }, new List<string> { "company_id", "topic_category_title" }));
                BoundStatement bs_topic_category_by_company_title = ps_topic_category_by_company_title.Bind(companyId, categoryTitle);
                Row row_topic_category_by_company_title = session.Execute(bs_topic_category_by_company_title).FirstOrDefault();
                if (row_topic_category_by_company_title != null)
                {
                    Log.Error(ErrorMessage.CategoryNameHasBeenUsed);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CategoryNameHasBeenUsed);
                    response.ErrorMessage = ErrorMessage.CategoryNameHasBeenUsed;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Update category.
                #region topic_category
                BatchStatement bs = new BatchStatement();
                PreparedStatement ps_topic_category = session.Prepare(CQLGenerator.UpdateStatement("topic_category", new List<string> { "company_id", "id" }, new List<string> { "title", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                bs.Add(ps_topic_category.Bind(categoryTitle, adminUserId, DateTime.UtcNow, companyId, categoryId));
                #endregion

                #region topic_category_by_company_title
                PreparedStatement ps_select_topic_category = session.Prepare(CQLGenerator.SelectStatement("topic_category", new List<string> { }, new List<string> { "company_id", "id" }));
                BoundStatement bs_select_topic_category = ps_select_topic_category.Bind(companyId, categoryId);
                Row row_select_topic_category = session.Execute(bs_select_topic_category).FirstOrDefault();
                if (row_select_topic_category == null)
                {
                    Log.Error(ErrorMessage.SystemError);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                    response.ErrorMessage = ErrorMessage.SystemError;
                    return response;
                }
                String oldTitle = row_select_topic_category.GetValue<String>("title");

                PreparedStatement ps_delete = session.Prepare(CQLGenerator.DeleteStatement("topic_category_by_company_title", new List<string> { "company_id", "topic_category_title", "topic_category_id" }));
                BoundStatement bs_delete = ps_delete.Bind(companyId, oldTitle, categoryId);
                bs.Add(bs_delete);

                PreparedStatement ps_insert = session.Prepare(CQLGenerator.InsertStatement("topic_category_by_company_title", new List<string> { "company_id", "topic_category_title", "topic_category_id" }));
                bs.Add(ps_insert.Bind(companyId, categoryTitle, categoryId));
                #endregion

                session.Execute(bs);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CategoryDeleteResponse Delete(String adminUserId, String companyId, String categoryId)
        {
            CategoryDeleteResponse response = new CategoryDeleteResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check topic count of category. If count > 0, cannot delete category.
                PreparedStatement ps_counter = session.Prepare(CQLGenerator.CountStatement("topic_by_category", new List<String> { "topic_category_id" }));
                BoundStatement bs_counter = ps_counter.Bind(categoryId);
                Row row_counter = session.Execute(bs_counter).FirstOrDefault();
                if (row_counter != null && row_counter.GetValue<long>("count") > 0)
                {
                    Log.Error(ErrorMessage.CategoryIsNotEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CategoryIsNotEmpty);
                    response.ErrorMessage = ErrorMessage.CategoryIsNotEmpty;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Delete category.
                BatchStatement batchStatement = new BatchStatement();

                #region Step 2.1 Delete topic_category table. (Update is_valid to false)
                PreparedStatement ps_topic_category = session.Prepare(CQLGenerator.UpdateStatement("topic_category", new List<string> { "company_id", "id" }, new List<string> { "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                BoundStatement bs_topic_category = ps_topic_category.Bind(false, adminUserId, DateTime.UtcNow, companyId, categoryId);
                batchStatement.Add(bs_topic_category);
                #endregion

                #region Step 2.2 Delete topic_category_by_company_title table.
                PreparedStatement ps_select_topic_category = session.Prepare(CQLGenerator.SelectStatement("topic_category", new List<string> { }, new List<string> { "company_id", "id" }));
                BoundStatement bs_select_topic_category = ps_select_topic_category.Bind(companyId, categoryId);
                Row row_select_topic_category = session.Execute(bs_select_topic_category).FirstOrDefault();
                if (row_select_topic_category == null)
                {
                    Log.Error(ErrorMessage.SystemError);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                    response.ErrorMessage = ErrorMessage.SystemError;
                    return response;
                }
                String oldTitle = row_select_topic_category.GetValue<String>("title");

                PreparedStatement ps_topic_category_by_company_title = session.Prepare(CQLGenerator.DeleteStatement("topic_category_by_company_title", new List<string> { "company_id", "topic_category_title", "topic_category_id" }));
                BoundStatement bs_department_by_company = ps_topic_category_by_company_title.Bind(companyId, oldTitle, categoryId);
                batchStatement.Add(bs_department_by_company);
                #endregion

                session.Execute(batchStatement);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CategorySelectAllResponse SelectAllCategoriesForDropdown(string adminUserId, string companyId, bool isCheckForValid = true, ISession session = null)
        {
            CategorySelectAllResponse response = new CategorySelectAllResponse();
            response.TopicCategories = new List<TopicCategory>();
            response.Success = false;

            try
            {
                PreparedStatement psCategory = null;
                BoundStatement bsCategory = null;

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                string categoryColumn = string.Empty;
                string titleColumn = string.Empty;

                if(!isCheckForValid)
                {
                    psCategory = session.Prepare(CQLGenerator.SelectStatement("topic_category",
                        new List<string>(), new List<string> { "company_id" }));
                    categoryColumn = "id";
                    titleColumn = "title";
                }
                else
                {
                    psCategory = session.Prepare(CQLGenerator.SelectStatement("topic_category_by_company_title",
                        new List<string>(), new List<string> { "company_id" }));
                    categoryColumn = "topic_category_id";
                    titleColumn = "topic_category_title";
                }

                bsCategory = psCategory.Bind(companyId);
                RowSet categoryByTitleRowset = session.Execute(bsCategory);

                foreach (Row categoryByTitleRow in categoryByTitleRowset)
                {
                    string categoryId = categoryByTitleRow.GetValue<string>(categoryColumn);
                    string categoryTitle = categoryByTitleRow.GetValue<string>(titleColumn);
                    response.TopicCategories.Add(new TopicCategory { Id = categoryId, Title = categoryTitle });
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CategorySelectAllResponse SelectAllCategories(string adminUserId, string companyId)
        {
            CategorySelectAllResponse response = new CategorySelectAllResponse();
            response.TopicCategories = new List<TopicCategory>();
            response.Success = false;

            try
            {
                PreparedStatement psCategory = null;
                BoundStatement bsCategory = null;

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                psCategory = session.Prepare(CQLGenerator.SelectStatement("topic_category_by_company_title",
                           new List<string> { "topic_category_id", "topic_category_title" }, new List<string> { "company_id" }));
                bsCategory = psCategory.Bind(companyId);
                RowSet categoryByTitleRowset = session.Execute(bsCategory);

                foreach (Row categoryByTitleRow in categoryByTitleRowset)
                {
                    string categoryId = categoryByTitleRow.GetValue<string>("topic_category_id");
                    string categoryTitle = categoryByTitleRow.GetValue<string>("topic_category_title");

                    psCategory = session.Prepare(CQLGenerator.CountStatement("topic_by_category",
                        new List<string> { "topic_category_id" }));
                    bsCategory = psCategory.Bind(categoryId);
                    Row categoryCountRow = session.Execute(bsCategory).FirstOrDefault();

                    int numberOfTopics = 0;

                    if (categoryCountRow != null)
                    {
                        numberOfTopics = (int)categoryCountRow.GetValue<long>("count");
                    }

                    response.TopicCategories.Add(new TopicCategory { Id = categoryId, Title = categoryTitle, TotalNumberOfTopics = numberOfTopics });
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CategorySelectResponse SelectCategory(string categoryId, string companyId, string adminUserId = null, ISession session = null)
        {
            CategorySelectResponse response = new CategorySelectResponse();
            response.TopicCategory = null;
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement psCategory = session.Prepare(CQLGenerator.SelectStatement("topic_category", new List<string> { "title" }, new List<string> { "company_id", "id", "is_valid" }));
                BoundStatement bsCategory = psCategory.Bind(companyId, categoryId, true);
                Row categoryByTitleRow = session.Execute(bsCategory).FirstOrDefault();

                if (categoryByTitleRow != null)
                {
                    string categoryTitle = categoryByTitleRow.GetValue<string>("title");

                    PreparedStatement ps_counter = session.Prepare(CQLGenerator.CountStatement("topic_by_category", new List<String> { "topic_category_id" }));
                    BoundStatement bs_counter = ps_counter.Bind(categoryId);
                    Row row_counter = session.Execute(bs_counter).FirstOrDefault();

                    response.TopicCategory = new TopicCategory
                    {
                        Id = categoryId,
                        Title = categoryTitle,
                        TotalNumberOfTopics = row_counter.GetValue<long>("count")

                    };
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}