﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class Assessment
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum AssessmentQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum AssessmentStatusEnum
        {
            Draft = 1,
            Live = 2
        }

        public enum AssessmentImageTypeEnum
        {
            AssessmentBanner = 1,
            TabulationImage = 2
        }

        public enum AssessmentAnswerConditionTypeEnum
        {
            TableWithHighestPoint = 1,
            TableWithLowestPoint = 2,
            DisplayAllTables = 3
        }

        [DataMember]
        public string AssessmentId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string IconUrl { get; set; }

        [DataMember]
        public bool HasBanner { get; set; }

        [DataMember]
        public List<AssessmentImage> Banners { get; set; }

        [DataMember]
        public string Introduction { get; set; }

        [DataMember]
        public string VideoUrl { get; set; }

        [DataMember]
        public string VideoThumbnailUrl { get; set; }

        [DataMember]
        public string VideoTitle { get; set; }

        [DataMember]
        public string VideoDescription { get; set; }

        [DataMember]
        public string ClosingWords { get; set; }

        [DataMember]
        public string CssArea { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public bool IsAnonymous { get; set; }

        [DataMember]
        public int NumberOfCards { get; set; }

        [DataMember]
        public List<AssessmentCard> Cards { get; set; }

        [DataMember]
        public Coach Coach { get; set; }

        [DataMember]
        public bool IsDisplayCoachName { get; set; }

        [DataMember]
        public int TotalNumberOfRetakesAvailable { get; set; }

        [DataMember]
        public int NumberOfRetakesLeft { get; set; }

        [DataMember]
        public List<AssessmentTabulation> Tabulations { get; set; }

        [DataMember]
        public List<AssessmentAnswer> Answers { get; set; }

        [DataMember]
        public bool IsCompleted { get; set; }

        [DataMember]
        public int CurrentAttempt { get; set; }

        [DataMember]
        public int NextAttempt { get; set; }


        public string GenerateAssessmentTabulationId()
        {
            return UUIDGenerator.GenerateUniqueIDForAssessmentTabulation();
        }

        public string GenerateAssessmentAnswerId()
        {
            return UUIDGenerator.GenerateUniqueIDForAssessmentAnswer();
        }

        public AssessmentCreateResponse CreateAssessment(string adminUserId,
                                                         string title,
                                                         string introduction,
                                                         string closingWords,
                                                         string iconUrl,
                                                         string coachId,
                                                         string coachName,
                                                         bool isDisplayCoachName,
                                                         bool isAnonymous,
                                                         List<AssessmentImage> imageBanners,
                                                         List<AssessmentTabulation> tabulations,
                                                         List<AssessmentAnswer> answers,
                                                         string videoUrl = null,
                                                         string videoTitle = null,
                                                         string videoDescription = null,
                                                         string videoThumbnailUrl = null,
                                                         string cssArea = null)
        {
            AssessmentCreateResponse response = new AssessmentCreateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                if (string.IsNullOrEmpty(iconUrl))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingIcon);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingIcon;
                    return response;
                }


                if (imageBanners == null || imageBanners.Count == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingBanners);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingBanners;
                    return response;
                }

                // Check coach
                Coach coachManager = new Coach();
                if (!string.IsNullOrEmpty(coachId))
                {
                    Row coachRow = vh.ValidateAssessmentCoach(coachId, session);
                    if (coachRow == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.CoachInvalid);
                        response.ErrorMessage = ErrorMessage.CoachInvalid;
                        return response;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(coachName))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingCoachName);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingCoachName;
                        return response;
                    }
                    coachId = coachManager.CreateCoach(coachName, adminUserId, false, session).CoachId;
                }

                InitField(title, introduction, closingWords, videoUrl, videoTitle, videoDescription, videoThumbnailUrl);

                string assessmentId = UUIDGenerator.GenerateUniqueIDForAssessment();

                bool hasBanner = true;
                DateTime currentTime = DateTime.UtcNow;

                AssessmentCreateStatementResponse statementResponse = null;
                BatchStatement batch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("assessment",
                    new List<string> { "id", "coach_id", "is_display_coach_name", "has_banner", "title", "introduction", "video_url", "video_title", "video_description", "video_thumbnail_url", "closing_words", "icon_url", "css_area", "status", "is_anonymous", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(assessmentId, coachId, isDisplayCoachName, hasBanner, title, introduction, videoUrl, videoTitle, videoDescription, videoThumbnailUrl, closingWords, iconUrl, cssArea, (int)AssessmentStatusEnum.Draft, isAnonymous, adminUserId, currentTime, adminUserId, currentTime));

                // Create link to coach
                foreach (BoundStatement bs in coachManager.LinkCoachToAssessment(assessmentId, coachId, session).UpdateStatements)
                {
                    batch.Add(bs);
                }

                if (hasBanner)
                {
                    statementResponse = CreateBanner(assessmentId, imageBanners, session);
                    if (!statementResponse.Success)
                    {
                        response.ErrorCode = statementResponse.ErrorCode;
                        response.ErrorMessage = statementResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in statementResponse.Statements)
                    {
                        batch.Add(bs);
                    }
                }

                statementResponse = CreateTabulation(assessmentId, tabulations, session, title.Equals(DefaultResource.AssessmentColoredBrainTitle));
                if (!statementResponse.Success)
                {
                    response.ErrorCode = statementResponse.ErrorCode;
                    response.ErrorMessage = statementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in statementResponse.Statements)
                {
                    batch.Add(bs);
                }

                statementResponse = CreateAnswer(assessmentId, answers, session);
                if (!statementResponse.Success)
                {
                    response.ErrorCode = statementResponse.ErrorCode;
                    response.ErrorMessage = statementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in statementResponse.Statements)
                {
                    batch.Add(bs);
                }

                session.Execute(batch);
                response.AssessmentId = assessmentId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentUpdateResponse UpdateAssessment(string assessmentId,
                                                         string adminUserId,
                                                         string newTitle,
                                                         string newIntroduction,
                                                         string newClosingWords,
                                                         string newIconUrl,
                                                         string coachId,
                                                         string coachName,
                                                         bool newIsDisplayCoachName,
                                                         bool newIsAnonymous,
                                                         int newStatus,
                                                         List<AssessmentImage> newImageBanners,
                                                         List<AssessmentTabulation> newTabulations,
                                                         List<AssessmentAnswer> newAnswers,
                                                         string newVideoUrl = null,
                                                         string newVideoTitle = null,
                                                         string newVideoDescription = null,
                                                         string newVideoThumbnailUrl = null,
                                                         string newCssArea = null)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin



                Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                if (assessmentRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(newIconUrl))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingIcon);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingIcon;
                    return response;
                }


                if (newImageBanners == null || newImageBanners.Count == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingBanners);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingBanners;
                    return response;
                }

                // Check coach
                Coach coachManager = new Coach();
                if (!string.IsNullOrEmpty(coachId))
                {
                    Row coachRow = vh.ValidateAssessmentCoach(coachId, session);
                    if (coachRow == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.CoachInvalid);
                        response.ErrorMessage = ErrorMessage.CoachInvalid;
                        return response;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(coachName))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingCoachName);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingCoachName;
                        return response;
                    }
                    coachId = coachManager.CreateCoach(coachName, adminUserId, false, session).CoachId;
                }

                InitField(newTitle, newIntroduction, newClosingWords, newVideoUrl, newVideoTitle, newVideoDescription, newVideoThumbnailUrl);

                Assessment currentAssessment = SelectAssessment(assessmentId, (int)AssessmentQueryType.FullDetail, (int)AssessmentCard.AssessmentCardQueryType.Basic, session, assessmentRow).Assessment;
                currentAssessment.Title = newTitle;
                currentAssessment.Introduction = newIntroduction;
                currentAssessment.ClosingWords = newClosingWords;

                List<string> uploadedLinks = new List<string>();
                uploadedLinks.Add(newVideoThumbnailUrl);
                uploadedLinks.Add(newVideoUrl);

                // Validate new tab and answers before continuing if live
                if (currentAssessment.Status == (int)AssessmentStatusEnum.Live)
                {
                    AssessmentValidationResponse validateResponse = ValidateAssessment(currentAssessment, newTabulations, newAnswers);
                    if (!validateResponse.Success)
                    {
                        response.ErrorCode = validateResponse.ErrorCode;
                        response.ErrorMessage = validateResponse.ErrorMessage;
                        return response;
                    }
                }

                bool hasBanner = true;
                DateTime currentTime = DateTime.UtcNow;

                AssessmentCreateStatementResponse statementResponse = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("assessment", new List<string> { "id" },
                    new List<string> { "coach_id", "is_display_coach_name", "has_banner", "title", "introduction", "video_url", "video_title", "video_description", "video_thumbnail_url", "closing_words", "icon_url", "css_area", "status", "is_anonymous", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(coachId, newIsDisplayCoachName, hasBanner, newTitle, newIntroduction, newVideoUrl, newVideoTitle, newVideoDescription, newVideoThumbnailUrl, newClosingWords, newIconUrl, newCssArea, (int)newStatus, newIsAnonymous, adminUserId, currentTime, assessmentId));

                // Update link to coach
                if (!currentAssessment.Coach.UserId.Equals(coachId))
                {
                    CoachUpdateWithStatementResponse linkResponse = coachManager.LinkCoachToAssessment(assessmentId, coachId, session, currentAssessment.Coach.UserId);
                    foreach (BoundStatement bs in linkResponse.DeleteStatements)
                    {
                        deleteBatch.Add(bs);
                    }
                    foreach (BoundStatement bs in linkResponse.UpdateStatements)
                    {
                        updateBatch.Add(bs);
                    }
                }

                // Remove all banners
                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_banner", new List<string> { "assessment_id" }));
                deleteBatch.Add(ps.Bind(assessmentId));

                // Create banners
                if (hasBanner)
                {
                    statementResponse = CreateBanner(assessmentId, newImageBanners, session);
                    if (!statementResponse.Success)
                    {
                        response.ErrorCode = statementResponse.ErrorCode;
                        response.ErrorMessage = statementResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in statementResponse.Statements)
                    {
                        updateBatch.Add(bs);
                    }

                    foreach (AssessmentImage image in newImageBanners)
                    {
                        uploadedLinks.Add(image.ImageUrl);
                    }
                }

                // Make comparison with current tabulations
                // Find tabulations to be deleted
                List<string> currentTabulationIds = currentAssessment.Tabulations.Select(tab => tab.TabulationId).ToList();
                List<string> newTabulationIds = newTabulations.Select(tab => tab.TabulationId).ToList();

                List<string> deletedTabulationIds = currentTabulationIds.Except(newTabulationIds).ToList();
                foreach (string deletedTabulationId in deletedTabulationIds)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("assessment_tabulation_allocation_by_tabulation", new List<string>(), new List<string> { "tabulation_id" }));
                    Row allocationRow = session.Execute(ps.Bind(deletedTabulationId)).FirstOrDefault();
                    if (allocationRow != null)
                    {
                        AssessmentTabulation tab = currentAssessment.Tabulations.FirstOrDefault(t => t.TabulationId.Equals(deletedTabulationId));
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentTabulationCannotBeDeleted);
                        response.ErrorMessage = string.Format(ErrorMessage.AssessmentTabulationCannotBeDeleted, tab.InfoTitle);
                        return response;
                    }
                }

                List<AssessmentImage> images = newTabulations.SelectMany(tab => tab.Images).ToList();
                uploadedLinks.AddRange(images.Select(i => i.ImageUrl).ToList());

                // Remove all tabulations
                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_tabulation", new List<string> { "assessment_id" }));
                deleteBatch.Add(ps.Bind(assessmentId));

                foreach (string currentTabulationId in currentTabulationIds)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("tabulation_image", new List<string> { "tabulation_id" }));
                    deleteBatch.Add(ps.Bind(currentTabulationId));
                }

                // Remove tabulation keywords
                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_keyword_tabulation_description", new List<string> { "assessment_keyword" }));
                deleteBatch.Add(ps.Bind(DefaultResource.AssessmentColoredBrainKey));

                // Create tabulations
                statementResponse = CreateTabulation(assessmentId, newTabulations, session, newTitle.Equals(DefaultResource.AssessmentColoredBrainTitle));
                if (!statementResponse.Success)
                {
                    response.ErrorCode = statementResponse.ErrorCode;
                    response.ErrorMessage = statementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in statementResponse.Statements)
                {
                    updateBatch.Add(bs);
                }

                // Remove all answers
                foreach (AssessmentAnswer answer in currentAssessment.Answers)
                {
                    foreach (AssessmentTabulation tabulation in answer.Tabulations)
                    {
                        if ((tabulation != null) && (answer != null))
                        {
                            ps = session.Prepare(CQLGenerator.DeleteStatement("answer_by_tabulation", new List<string> { "tabulation_id", "answer_id" }));
                            deleteBatch.Add(ps.Bind(tabulation.TabulationId, answer.AnswerId));
                        }
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("tabulation_by_answer", new List<string> { "answer_id" }));
                    deleteBatch.Add(ps.Bind(answer.AnswerId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_answer", new List<string> { "assessment_id" }));
                deleteBatch.Add(ps.Bind(assessmentId));

                // Create answers
                statementResponse = CreateAnswer(assessmentId, newAnswers, session);
                if (!statementResponse.Success)
                {
                    response.ErrorCode = statementResponse.ErrorCode;
                    response.ErrorMessage = statementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in statementResponse.Statements)
                {
                    updateBatch.Add(bs);
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                uploadedLinks = uploadedLinks.Where(link => !string.IsNullOrEmpty(link)).ToList();
                DeleteFromS3(assessmentId, uploadedLinks);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectAllResponse SelectAllAssessments(string adminUserId)
        {
            AssessmentSelectAllResponse response = new AssessmentSelectAllResponse();
            response.Assessments = new List<Assessment>();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment", new List<string>(), new List<string>()));
                RowSet allAssessmentsRowset = session.Execute(ps.Bind());

                foreach (Row assessmentRow in allAssessmentsRowset)
                {
                    string assessmentId = assessmentRow.GetValue<string>("id");
                    Assessment assessment = SelectAssessment(assessmentId, (int)AssessmentQueryType.Basic, (int)AssessmentCard.AssessmentCardQueryType.Basic, session, assessmentRow).Assessment;
                    if (assessment != null)
                    {
                        response.Assessments.Add(assessment);
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentSelectAllResponse SelectAllAssessmentByUser(string requesterUserId, string companyId, ISession mainSession, ISession analyticSession, bool isForColoredBrain = false)
        {
            AssessmentSelectAllResponse response = new AssessmentSelectAllResponse();
            response.Assessments = new List<Assessment>();
            response.Success = false;
            try
            {
                AnalyticAssessment analyticAssessment = new AnalyticAssessment();
                DateTime currentTime = DateTime.UtcNow;

                // Get assessment from subscription
                SubscriptionSelectAllResponse allSubscriptionResponse = new Subscription().SelectAllAssessmentSubscriptionByCompany(companyId, requesterUserId, mainSession);

                foreach (Subscription sub in allSubscriptionResponse.Subscriptions)
                {
                    Assessment assessment = sub.Assessment;
                    if (assessment != null)
                    {
                        // Check for colored brain
                        if (isForColoredBrain)
                        {
                            if (!assessment.Title.ToLower().Equals(DefaultResource.AssessmentColoredBrainTitle.ToLower()))
                            {
                                continue;
                            }
                        }

                        // Check for date
                        int validityDateAfterCompletion = sub.VisibleDaysAfterCompletion;
                        int durationType = sub.DurationType;

                        AssessmentSelectAttemptByUserResponse attemptResponse = analyticAssessment.SelectAttemptByUser(assessment.AssessmentId, requesterUserId, sub.NumberOfRetakes, analyticSession);
                        bool isLastAttemptCompleted = attemptResponse.IsCurrentAttemptCompleted;
                        if (attemptResponse.CurrentAttempt > 1)
                        {
                            isLastAttemptCompleted = true;
                        }

                        if (durationType == (int)Subscription.SubscriptionDurationTypeEnum.Schedule)
                        {
                            DateTime startTime = sub.StartDate.Value;
                            DateTime? endTime = sub.EndDate;

                            if (currentTime < startTime)
                            {
                                continue;
                            }

                            if (endTime.HasValue)
                            {
                                if (attemptResponse.IsCurrentAttemptCompleted)
                                {
                                    if (currentTime > attemptResponse.LastCompletedTimestamp.Value.AddDays(validityDateAfterCompletion))
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    if (currentTime > endTime.Value)
                                    {
                                        continue;
                                    }
                                }
                            }
                            else
                            {
                                if (attemptResponse.IsCurrentAttemptCompleted)
                                {
                                    if (currentTime > attemptResponse.LastCompletedTimestamp.Value.AddDays(validityDateAfterCompletion))
                                    {
                                        continue;
                                    }
                                }
                            }
                        }

                        assessment.IsCompleted = isLastAttemptCompleted;
                        assessment.TotalNumberOfRetakesAvailable = sub.NumberOfRetakes;
                        assessment.NumberOfRetakesLeft = attemptResponse.NumberOfRetakesLeft;
                        response.Assessments.Add(assessment);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentSelectResponse SelectAssessmentByUser(string requesterUserId, string companyId, string assessmentId, int queryType, Row subscriptionRow = null, Row assessmentRow = null,  ISession mainSession = null, ISession analyticSession = null)
        {
            AssessmentSelectResponse response = new AssessmentSelectResponse();
            response.Assessment = null;
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                if(mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    subscriptionRow = vh.ValidateAssessmentSubscription(assessmentId, companyId, mainSession);
                    if (subscriptionRow == null)
                    {
                        Log.Error("Invalid subscription");
                        response.ErrorCode = Convert.ToInt32(ErrorCode.SubscriptionInvalid);
                        response.ErrorMessage = ErrorMessage.SubscriptionInvalid;
                        return response;
                    }

                    assessmentRow = vh.ValidateAssessment(assessmentId, mainSession);
                    if (assessmentRow == null)
                    {
                        Log.Error("Invalid assessmentId: " + assessmentId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                        response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                        return response;
                    }

                }

                if(analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                AnalyticAssessment analyticAssessment = new AnalyticAssessment();

                // Get number of retake left
                int numberOfRetakesAvailable = subscriptionRow.GetValue<int>("number_of_retake");
                AssessmentSelectAttemptByUserResponse attemptResponse = analyticAssessment.SelectAttemptByUser(assessmentId, requesterUserId, numberOfRetakesAvailable, analyticSession);
                int nextAttempt = attemptResponse.NextAttempt;
                int currentAttempt = attemptResponse.CurrentAttempt;

                // Check for completion
                bool isUserAnswerRequired = false;
                bool isLastAttemptCompleted = attemptResponse.IsCurrentAttemptCompleted;
                if (!isLastAttemptCompleted)
                {
                    isUserAnswerRequired = true;
                }

                if(attemptResponse.CurrentAttempt > 1)
                {
                    isLastAttemptCompleted = true;
                }

                Assessment assessment = SelectAssessment(assessmentId, queryType, (int)AssessmentCard.AssessmentCardQueryType.FullDetail, mainSession, assessmentRow, nextAttempt, requesterUserId, isUserAnswerRequired, analyticSession).Assessment;
                if (assessment == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                assessment.IsCompleted = isLastAttemptCompleted;
                assessment.CurrentAttempt = attemptResponse.CurrentAttempt;
                assessment.NextAttempt = attemptResponse.NextAttempt;
                assessment.TotalNumberOfRetakesAvailable = numberOfRetakesAvailable;
                assessment.NumberOfRetakesLeft = attemptResponse.NumberOfRetakesLeft;
                response.Assessment = assessment;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentSelectResponse SelectFullDetailAssessment(string adminUserId, string assessmentId, bool isForPreview = false)
        {
            AssessmentSelectResponse response = new AssessmentSelectResponse();
            response.Assessment = new Assessment();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                if (assessmentRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                response.Assessment = SelectAssessment(assessmentId, (int)AssessmentQueryType.FullDetail, isForPreview ? (int)AssessmentCard.AssessmentCardQueryType.FullDetail : (int)AssessmentCard.AssessmentCardQueryType.Basic, session, assessmentRow).Assessment;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentSelectResponse SelectAssessment(string assessmentId, int assessmentQueryType, int cardQueryType, ISession mainSession, Row assessmentRow = null, int attempt = 0, string answeredByUserId = null, bool isUserAnswerRequired = false, ISession analyticSession = null)
        {
            AssessmentSelectResponse response = new AssessmentSelectResponse();
            response.Assessment = null;
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                if (assessmentRow == null)
                {
                    assessmentRow = vh.ValidateAssessment(assessmentId, mainSession);
                    if (assessmentRow == null)
                    {
                        Log.Error("Invalid assessmentId: " + assessmentId);
                        response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInvalid);
                        response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                        return response;
                    }
                }

                string iconUrl = assessmentRow.GetValue<string>("icon_url");
                string title = assessmentRow.GetValue<string>("title");
                string coachId = assessmentRow.GetValue<string>("coach_id");
                int status = assessmentRow.GetValue<int>("status");

                bool isDisplayCoachName = assessmentRow.GetValue<bool>("is_display_coach_name");
                bool hasBanner = true;

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_banner", new List<string>(), new List<string> { "assessment_id" }));
                RowSet bannerRowset = mainSession.Execute(ps.Bind(assessmentId));

                List<AssessmentImage> banners = new List<AssessmentImage>();
                foreach (Row bannerRow in bannerRowset)
                {
                    AssessmentImage banner = new AssessmentImage
                    {
                        ImageId = bannerRow.GetValue<string>("banner_id"),
                        ImageUrl = bannerRow.GetValue<string>("banner_url"),
                        Ordering = bannerRow.GetValue<int>("ordering"),
                    };

                    banners.Add(banner);
                }

                string introduction = assessmentRow.GetValue<string>("introduction");
                string videoUrl = assessmentRow.GetValue<string>("video_url") == null ? string.Empty : assessmentRow.GetValue<string>("video_url");
                string videoThumbnailUrl = assessmentRow.GetValue<string>("video_thumbnail_url") == null ? string.Empty : assessmentRow.GetValue<string>("video_thumbnail_url");
                string videoTitle = assessmentRow.GetValue<string>("video_title") == null ? string.Empty : assessmentRow.GetValue<string>("video_title");
                string videoDescription = assessmentRow.GetValue<string>("video_description") == null ? string.Empty : assessmentRow.GetValue<string>("video_description");
                string closingWords = assessmentRow.GetValue<string>("closing_words") == null ? string.Empty : assessmentRow.GetValue<string>("closing_words");
                string cssArea = assessmentRow.GetValue<string>("css_area") == null ? string.Empty : assessmentRow.GetValue<string>("css_area");
                bool isAnonymous = assessmentRow.GetValue<bool>("is_anonymous");


                List<AssessmentTabulation> tabulations = new List<AssessmentTabulation>();
                List<AssessmentAnswer> answers = new List<AssessmentAnswer>();
                List<AssessmentCard> cards = new List<AssessmentCard>();

                if (assessmentQueryType == (int)AssessmentQueryType.FullDetail)
                {
                    cards = new AssessmentCard().SelectAllCards(assessmentId, cardQueryType, mainSession, isUserAnswerRequired, attempt, answeredByUserId, analyticSession).Cards.OrderBy(r => r.Ordering).ToList();

                    tabulations = SelectAllTabulations(assessmentId, mainSession).OrderBy(r => r.Ordering).ToList();

                    answers = SelectAllAnswers(assessmentId, tabulations, mainSession).OrderBy(r => r.Ordering).ToList();
                }

                response.Assessment = new Assessment
                {
                    AssessmentId = assessmentId,
                    IconUrl = iconUrl,
                    Title = title,
                    HasBanner = hasBanner,
                    Banners = banners,
                    Introduction = introduction,
                    VideoUrl = videoUrl,
                    VideoThumbnailUrl = videoThumbnailUrl,
                    VideoTitle = videoTitle,
                    VideoDescription = videoDescription,
                    ClosingWords = closingWords,
                    CssArea = cssArea,
                    Status = status,
                    IsAnonymous = isAnonymous,
                    Coach = new Coach().SelectCoach(null, coachId, mainSession).Coach,
                    IsDisplayCoachName = isDisplayCoachName,
                    Tabulations = tabulations,
                    Answers = answers,
                    Cards = cards,
                    NumberOfCards = cards.Count
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public List<AssessmentTabulation> SelectAllTabulations(string assessmentId, ISession session)
        {
            List<AssessmentTabulation> tabulations = new List<AssessmentTabulation>();
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_tabulation", new List<string>(), new List<string> { "assessment_id" }));
                RowSet tabulationRowset = session.Execute(ps.Bind(assessmentId));

                foreach (Row tabulationRow in tabulationRowset)
                {
                    string tabulationId = tabulationRow.GetValue<string>("id");
                    bool hasImage = tabulationRow.GetValue<bool>("has_image");
                    string label = tabulationRow.GetValue<string>("label");
                    string infoTitle = tabulationRow.GetValue<string>("info_title");
                    string infoDescription = tabulationRow.GetValue<string>("info_description");
                    string infoHtmlDescription = tabulationRow.GetValue<string>("info_html_description");
                    int ordering = tabulationRow.GetValue<int>("ordering");

                    List<AssessmentImage> tabulationImages = new List<AssessmentImage>();

                    if (hasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("tabulation_image", new List<string>(), new List<string> { "tabulation_id" }));
                        RowSet imageRowset = session.Execute(ps.Bind(tabulationId));
                        foreach (Row imageRow in imageRowset)
                        {
                            AssessmentImage image = new AssessmentImage
                            {
                                ImageId = imageRow.GetValue<string>("image_id"),
                                ImageUrl = imageRow.GetValue<string>("image_url"),
                                Ordering = imageRow.GetValue<int>("ordering"),
                            };

                            tabulationImages.Add(image);
                        }
                    }

                    AssessmentTabulation tabulation = new AssessmentTabulation
                    {
                        TabulationId = tabulationId,
                        Label = label,
                        InfoTitle = infoTitle,
                        InfoDescription = infoDescription,
                        Ordering = ordering,
                        Images = tabulationImages,
                        HasImage = hasImage
                    };

                    tabulations.Add(tabulation);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return tabulations;
        }

        public List<AssessmentAnswer> SelectAllAnswers(string assessmentId, List<AssessmentTabulation> tabulations, ISession session)
        {
            List<AssessmentAnswer> answers = new List<AssessmentAnswer>();
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_answer", new List<string>(), new List<string> { "assessment_id" }));
                RowSet answerRowset = session.Execute(ps.Bind(assessmentId));

                foreach (Row answerRow in answerRowset)
                {
                    string answerId = answerRow.GetValue<string>("id");
                    string answerTitle = answerRow.GetValue<string>("title");
                    int ordering = answerRow.GetValue<int>("ordering");
                    int condition = answerRow.GetValue<int>("condition");
                    bool visibleInAssessment = answerRow.GetValue<bool>("visible_in_assessment");
                    bool visibleToSelf = answerRow.GetValue<bool>("visible_to_self_profile_page");
                    bool visibleToOthers = answerRow.GetValue<bool>("visible_to_others");
                    bool forCompletionUser = answerRow.GetValue<bool>("for_completed_user");
                    bool forIncompletionUser = answerRow.GetValue<bool>("for_incompleted_user");

                    List<AssessmentTabulation> answerTabulations = new List<AssessmentTabulation>();

                    ps = session.Prepare(CQLGenerator.SelectStatement("tabulation_by_answer", new List<string>(), new List<string> { "answer_id" }));
                    RowSet answerTabulationRowset = session.Execute(ps.Bind(answerId));
                    foreach (Row answerTabulationRow in answerTabulationRowset)
                    {
                        string tabulationId = answerTabulationRow.GetValue<string>("tabulation_id");

                        AssessmentTabulation selectedTabulation = tabulations.FirstOrDefault(t => t.TabulationId.Equals(tabulationId));
                        if (selectedTabulation != null)
                        {
                            answerTabulations.Add(selectedTabulation);
                        }
                    }

                    AssessmentAnswer answer = new AssessmentAnswer
                    {
                        AnswerId = answerId,
                        Title = answerTitle,
                        Ordering = ordering,
                        Condition = condition,
                        ShowInAssessment = visibleInAssessment,
                        VisibleToOthers = visibleToOthers,
                        VisibleToSelf = visibleToSelf,
                        VisibleToOthersForCompletedUser = forCompletionUser,
                        VisibleToOthersForIncompletedUser = forIncompletionUser,
                        Tabulations = answerTabulations
                    };

                    answers.Add(answer);
                }

                answers = answers.OrderBy(a => a.Ordering).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return answers;
        }

        public AssessmentTabulation SelectTabulation(string assessmentId, string tabulationId, ISession session)
        {
            AssessmentTabulation selectedTabulation = new AssessmentTabulation();
            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_tabulation", new List<string>(), new List<string> { "assessment_id", "id" }));
                Row tabulationRow = session.Execute(ps.Bind(assessmentId, tabulationId)).FirstOrDefault();

                if (tabulationRow != null)
                {
                    bool hasImage = tabulationRow.GetValue<bool>("has_image");
                    string label = tabulationRow.GetValue<string>("label");
                    string infoTitle = tabulationRow.GetValue<string>("info_title");
                    string infoDescription = tabulationRow.GetValue<string>("info_description");
                    int ordering = tabulationRow.GetValue<int>("ordering");

                    List<AssessmentImage> tabulationImages = new List<AssessmentImage>();

                    if (hasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("tabulation_image", new List<string>(), new List<string> { "tabulation_id" }));
                        RowSet imageRowset = session.Execute(ps.Bind(tabulationId));
                        foreach (Row imageRow in imageRowset)
                        {
                            AssessmentImage image = new AssessmentImage
                            {
                                ImageId = imageRow.GetValue<string>("image_id"),
                                ImageUrl = imageRow.GetValue<string>("image_url"),
                                Ordering = imageRow.GetValue<int>("ordering"),
                            };

                            tabulationImages.Add(image);
                        }
                    }

                    selectedTabulation = new AssessmentTabulation
                    {
                        TabulationId = tabulationId,
                        Label = label,
                        InfoTitle = infoTitle,
                        InfoDescription = infoDescription,
                        Ordering = ordering,
                        Images = tabulationImages,
                        HasImage = hasImage
                    };
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return selectedTabulation;
        }


        public AssessmentUpdateResponse UpdateAssessmentStatus(string adminUserId, string assessmentId, int newStatus, Assessment currentAssessment = null, ISession session = null)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    // Check admin

                    Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                    if (assessmentRow == null)
                    {
                        Log.Error("Invalid assessmentId: " + assessmentId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                        response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                        return response;
                    }

                    currentAssessment = SelectAssessment(assessmentId, (int)AssessmentQueryType.FullDetail, (int)AssessmentCard.AssessmentCardQueryType.Basic, session, assessmentRow).Assessment;
                }

                if (currentAssessment.Status != newStatus)
                {
                    if (newStatus == (int)AssessmentStatusEnum.Live)
                    {
                        AssessmentValidationResponse validateResponse = ValidateAssessment(currentAssessment);
                        if (!validateResponse.Success)
                        {
                            response.ErrorCode = validateResponse.ErrorCode;
                            response.ErrorMessage = validateResponse.ErrorMessage;
                            return response;
                        }
                    }
                    else
                    {
                        if (new Subscription().CheckSubscriptionForAssessment(assessmentId, session))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentHasOngoingSubscription);
                            response.ErrorMessage = ErrorMessage.AssessmentHasOngoingSubscription;
                            return response;
                        }
                    }

                    DateTime currentTime = DateTime.UtcNow;
                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("assessment", new List<string> { "id" }, new List<string> { "status", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                    session.Execute(ps.Bind(newStatus, adminUserId, currentTime, adminUserId, currentTime, assessmentId));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentUpdateResponse DeleteAssessment(string adminUserId, string companyId, string assessmentId)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                if (assessmentRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.AssessmentAlreadyDeleted;
                    return response;
                }

                if (new Subscription().CheckSubscriptionForAssessment(assessmentId, session))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentHasOngoingSubscription);
                    response.ErrorMessage = ErrorMessage.AssessmentHasOngoingSubscription;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                PreparedStatement ps = null;

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment", new List<string> { "id" }));
                batch.Add(ps.Bind(assessmentId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_banner", new List<string> { "assessment_id" }));
                batch.Add(ps.Bind(assessmentId));

                List<AssessmentTabulation> tabulations = SelectAllTabulations(assessmentId, session);
                foreach (AssessmentTabulation tabulation in tabulations)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("tabulation_image", new List<string> { "tabulation_id" }));
                    batch.Add(ps.Bind(tabulation.TabulationId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_tabulation", new List<string> { "assessment_id" }));
                batch.Add(ps.Bind(assessmentId));

                List<AssessmentAnswer> answers = SelectAllAnswers(assessmentId, tabulations, session);
                foreach (AssessmentAnswer answer in answers)
                {
                    foreach (AssessmentTabulation tabulation in answer.Tabulations)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("tabulation_by_answer", new List<string> { "answer_id", "tabulation_id" }));
                        batch.Add(ps.Bind(answer.AnswerId, tabulation.TabulationId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("answer_by_tabulation", new List<string> { "answer_id", "tabulation_id" }));
                        batch.Add(ps.Bind(answer.AnswerId, tabulation.TabulationId));
                    }
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_answer", new List<string> { "assessment_id" }));
                batch.Add(ps.Bind(assessmentId));

                new AssessmentCard().DeleteAllCards(assessmentId, session);

                // Remove link from coach
                batch.Add(new Coach().RemoveLinkToAssessment(assessmentId, assessmentRow.GetValue<string>("coach_id"), session));

                session.Execute(batch);

                DeleteFromS3(assessmentId);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentSelectResultByUserResponse SelectAssessmentResultByUser(string requesterUserId, string companyId, string assessmentId, ISession mainSession = null, ISession analyticSession = null)
        {
            AssessmentSelectResultByUserResponse response = new AssessmentSelectResultByUserResponse();
            response.Assessment = null;
            response.Results = new List<AssessmentAnswer>();
            response.NumberOfRetakesAvailable = 0;
            response.NumberOfRetakesLeft = 0;
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();

                if(mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                Row subscriptionRow = vh.ValidateAssessmentSubscription(assessmentId, companyId, mainSession);
                if (subscriptionRow == null)
                {
                    Log.Error("Invalid subscription");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.SubscriptionInvalid);
                    response.ErrorMessage = ErrorMessage.SubscriptionInvalid;
                    return response;
                }

                Row assessmentRow = vh.ValidateAssessment(assessmentId, mainSession);
                if (assessmentRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                AnalyticAssessment analyticManager = new AnalyticAssessment();

                // Check if all the questions are completed
                Assessment assessment = SelectAssessmentByUser(requesterUserId, companyId, assessmentId, (int)AssessmentQueryType.FullDetail, subscriptionRow, assessmentRow, mainSession, analyticSession).Assessment;

                int numberOfRetakesAvailable = assessment.TotalNumberOfRetakesAvailable;
                int currentAttempt = assessment.CurrentAttempt;
                int nextAttempt = assessment.NextAttempt;
                int numberOfRetakesLeft = assessment.NumberOfRetakesLeft;

                // Confim not completed since need to select result then set to completion (double check, set >= 1)
                if(currentAttempt >= 1)
                {
                    if (!analyticManager.IsAssessmentCompleted(assessment, currentAttempt, requesterUserId, analyticSession))
                    {
                        currentAttempt -= 1;

                        if(currentAttempt < 1)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentIncomplete);
                            response.ErrorMessage = ErrorMessage.AssessmentIncomplete;
                            return response;
                        }
                        else
                        {
                            if (!analyticManager.IsAssessmentCompleted(assessment, currentAttempt, requesterUserId, analyticSession))
                            {
                                response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentIncomplete);
                                response.ErrorMessage = ErrorMessage.AssessmentIncomplete;
                                return response;
                            }
                        }
                    }             
                }

                List<AssessmentTabulation> tabulations = SelectAllTabulations(assessmentId, mainSession);

                List<AssessmentAnswer> answers = SelectAllAnswers(assessmentId, tabulations, mainSession);
                analyticManager.SelectResultByUser(assessmentId, requesterUserId, currentAttempt, answers, analyticSession);

                if (assessment.Title.ToLower().Equals(DefaultResource.AssessmentColoredBrainTitle.ToLower()))
                {
                    foreach (AssessmentAnswer answer in answers)
                    {
                        foreach(AssessmentTabulation tabulation in answer.Tabulations)
                        {
                            // Get html description
                            PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_tabulation", new List<string>(), new List<string> { "assessment_id", "id" }));
                            string htmlDescription = mainSession.Execute(ps.Bind(assessmentId, tabulation.TabulationId)).FirstOrDefault().GetValue<string>("info_html_description");

                            string newDescription = RemoveKeywords(tabulation.InfoTitle, tabulation.InfoDescription);
                            string newHtmlDescription = RemoveKeywords(tabulation.InfoTitle, htmlDescription);
                            tabulation.InfoDescription = newDescription;
                            tabulation.InfoHtmlDescription = newHtmlDescription;
                        }
                    }
                }

                //response.Assessment = assessment;
                response.Results = answers;
                response.NumberOfRetakesAvailable = numberOfRetakesAvailable;
                response.NumberOfRetakesLeft = numberOfRetakesLeft;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AssessmentCreateStatementResponse CreateBanner(string assessmentId, List<AssessmentImage> imageBanners, ISession session)
        {
            AssessmentCreateStatementResponse response = new AssessmentCreateStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int bannerOrdering = 1;
                foreach (AssessmentImage banner in imageBanners)
                {
                    if (string.IsNullOrEmpty(banner.ImageUrl))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingImageUrl);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingImageUrl;
                        return response;
                    }

                    string bannerId = string.Format("{0}_banner_{1}", assessmentId, bannerOrdering);
                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_banner",
                        new List<string> { "assessment_id", "banner_id", "banner_url", "ordering" }));
                    response.Statements.Add(ps.Bind(assessmentId, bannerId, banner.ImageUrl, bannerOrdering));
                    bannerOrdering++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        private AssessmentCreateStatementResponse CreateTabulation(string assessmentId, List<AssessmentTabulation> tabulations, ISession session, bool isColoredBrain = false)
        {
            AssessmentCreateStatementResponse response = new AssessmentCreateStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int tabulationOrdering = 1;

                foreach (AssessmentTabulation tabulation in tabulations)
                {
                    AssessmentValidationResponse validateResponse = ValidateTabulation(tabulation);
                    if (!validateResponse.Success)
                    {
                        response.ErrorCode = validateResponse.ErrorCode;
                        response.ErrorMessage = validateResponse.ErrorMessage;
                        return response;
                    }

                    if (tabulation.Images != null && tabulation.Images.Count > 0)
                    {
                        AssessmentCreateStatementResponse createImageResponse = CreateTabulationImages(tabulation, session);
                        if (!createImageResponse.Success)
                        {
                            response.ErrorCode = createImageResponse.ErrorCode;
                            response.ErrorMessage = createImageResponse.ErrorMessage;
                            return response;
                        }

                        foreach (BoundStatement bs in createImageResponse.Statements)
                        {
                            response.Statements.Add(bs);
                        }
                    }

                    tabulation.Ordering = tabulationOrdering;

                    string formattedHtmlDescription = ReplaceWithHtml(tabulation.InfoTitle, tabulation.InfoDescription);

                    bool hasImage = tabulation.Images == null || tabulation.Images.Count > 0 ? true : false;
                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_tabulation",
                        new List<string> { "id", "assessment_id", "has_image", "label", "ordering", "info_title", "info_description", "info_html_description" }));
                    response.Statements.Add(ps.Bind(tabulation.TabulationId, assessmentId, hasImage, tabulation.Label, tabulation.Ordering, tabulation.InfoTitle, tabulation.InfoDescription, formattedHtmlDescription));

                    if (isColoredBrain)
                    {
                        Dictionary<string, string> keywords = ExtractKeywords(tabulation.InfoTitle, tabulation.InfoDescription);
                        foreach (string key in keywords.Keys)
                        {
                            if (key.Contains("TITLE"))
                            {
                                continue;
                            }

                            string description = keywords[key];
                            string title = keywords[key + "_TITLE"];

                            if (!string.IsNullOrEmpty(description))
                            {
                                string htmlDescription = ReplaceWithHtml(tabulation.InfoTitle, description);
                                ps = session.Prepare(CQLGenerator.InsertStatement("assessment_keyword_tabulation_description",
                                    new List<string> { "assessment_keyword", "description_keyword", "description", "html_description", "title" }));
                                response.Statements.Add(ps.Bind(DefaultResource.AssessmentColoredBrainKey, key, description, htmlDescription, title));
                            }
                        }
                    }

                    tabulationOrdering++;
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private Dictionary<string, string> ExtractKeywords(string label,  string description)
        {
            Dictionary<string, string> keywordDict = new Dictionary<string, string>();
            try
            {
                string[] keywordArray = null;

                if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutGreen.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingGreenKeywords.Split(',');
                }
                else if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutBlue.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingBlueKeywords.Split(',');
                }
                else if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutRed.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingRedKeywords.Split(',');
                }
                else if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutPurple.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingPurpleKeywords.Split(',');
                }

                if(keywordArray != null)
                {
                    foreach (string keyword in keywordArray)
                    {
                        string title = string.Empty;
                        string descriptionWithTitle = string.Empty;
                        string filterDescription = string.Empty;

                        MatchCollection matches = Regex.Matches(description, string.Format(@"\b(?<={0})\b(.|\n)*(?={1})", keyword, keyword));
                        if(matches.Count > 0)
                        {
                            descriptionWithTitle = matches[0].Groups.SyncRoot.ToString().Trim();

                            matches = Regex.Matches(descriptionWithTitle, string.Format(@"{0}(.|\n)*{1}", DefaultResource.AssessmentWorkingTitleKeyword, DefaultResource.AssessmentWorkingTitleKeyword));
                            if (matches.Count > 0)
                            {
                                string titleWithKey = matches[0].Groups.SyncRoot.ToString().Trim();
                                filterDescription = descriptionWithTitle.Replace(titleWithKey, "").Trim();

                                matches = Regex.Matches(titleWithKey, string.Format(@"\b(?<={0})\b(.|\n)*(?={1})", DefaultResource.AssessmentWorkingTitleKeyword, DefaultResource.AssessmentWorkingTitleKeyword));
                                title = matches[0].Groups.SyncRoot.ToString().Trim();
                            }
                            else
                            {
                                filterDescription = descriptionWithTitle;
                            }   
                        }

                        keywordDict.Add(keyword + "_TITLE", title);
                        keywordDict.Add(keyword, filterDescription);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return keywordDict;
        }

        private string RemoveKeywords(string label, string description)
        {
            Dictionary<string, string> keywordDict = new Dictionary<string, string>();
            try
            {
                List<string> keywordArray = null;

                if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutGreen.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingGreenKeywords.Split(',').ToList();
                }
                else if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutBlue.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingBlueKeywords.Split(',').ToList();
                }
                else if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutRed.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingRedKeywords.Split(',').ToList();
                }
                else if (label.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutPurple.ToLower()))
                {
                    keywordArray = DefaultResource.AssessmentWorkingPurpleKeywords.Split(',').ToList();
                }

                if (keywordArray != null)
                {
                    keywordArray.Add(DefaultResource.AssessmentWorkingTitleKeyword);

                    foreach (string keyword in keywordArray)
                    {
                        description = description.Replace(keyword + "\n", "");
                        description = description.Replace(keyword, "");
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return description;
        }

        private string ReplaceWithHtml(string label, string description)
        {
            string htmlDescription = string.Empty;
            try
            {
                htmlDescription = description;
                htmlDescription = RemoveKeywords(label, description);

                string replacementFrom = DefaultResource.AssessmentColorBrainUserReplacementKeywordsFrom;
                string replacementTo = DefaultResource.AssessmentColorBrainUserReplacementKeywordsTo;

                replacementFrom = replacementFrom.Replace("\\n", "\n");
                List<string> replacementFromList = replacementFrom.Split(',').ToList();
                List<string> replacementToList = replacementTo.Split(',').ToList();

                for(int index = 0; index < replacementFromList.Count; index++)
                {
                    htmlDescription = htmlDescription.Replace(replacementFromList[index], replacementToList[index]);
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex.ToString(), ex);
            }

            return htmlDescription;
        }

        private AssessmentCreateStatementResponse CreateTabulationImages(AssessmentTabulation tabulation, ISession session)
        {
            AssessmentCreateStatementResponse response = new AssessmentCreateStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                int imageOrdering = 1;
                foreach (AssessmentImage image in tabulation.Images)
                {
                    if (string.IsNullOrEmpty(image.ImageUrl))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingImageUrl);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingImageUrl;
                        return response;
                    }

                    string imageId = string.Format("{0}_image_{1}", tabulation.TabulationId, imageOrdering);

                    PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("tabulation_image",
                        new List<string> { "tabulation_id", "image_id", "image_url", "ordering" }));
                    response.Statements.Add(ps.Bind(tabulation.TabulationId, imageId, image.ImageUrl, imageOrdering));

                    imageOrdering++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private AssessmentValidationResponse ValidateTabulation(AssessmentTabulation tabulation)
        {
            AssessmentValidationResponse response = new AssessmentValidationResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(tabulation.TabulationId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentInvalidTabulationId);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalidTabulationId;
                    return response;
                }

                if (string.IsNullOrEmpty(tabulation.Label))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentMissingTabulationLabel);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingTabulationLabel;
                    return response;
                }

                if (string.IsNullOrEmpty(tabulation.InfoTitle))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentMissingTabulationInfoTitle);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingTabulationInfoTitle;
                    return response;
                }

                if (string.IsNullOrEmpty(tabulation.InfoDescription))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentMissingTabulationInfoDescription);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingTabulationInfoDescription;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private AssessmentUpdateWithStatementReponse UpdateTabulation(string assessmentId, AssessmentTabulation tabulation, ISession session)
        {
            AssessmentUpdateWithStatementReponse response = new AssessmentUpdateWithStatementReponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;

            try
            {
                AssessmentValidationResponse validateResponse = ValidateTabulation(tabulation);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                bool hasImage = tabulation.Images == null || tabulation.Images.Count > 0 ? true : false;
                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("assessment_tabulation",
                    new List<string> { "id", "assessment_id" }, new List<string> { "has_image", "label", "ordering", "info_title", "info_description" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(hasImage, tabulation.Label, tabulation.Ordering, tabulation.InfoTitle, tabulation.InfoDescription, tabulation.TabulationId, assessmentId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("tabulation_image", new List<string> { "tabulation_id" }));
                response.DeleteStatements.Add(ps.Bind(tabulation.TabulationId));

                AssessmentCreateStatementResponse createImageResponse = CreateTabulationImages(tabulation, session);
                if (!createImageResponse.Success)
                {
                    response.ErrorCode = createImageResponse.ErrorCode;
                    response.ErrorMessage = createImageResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in createImageResponse.Statements)
                {
                    response.UpdateStatements.Add(bs);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AssessmentCreateStatementResponse CreateAnswer(string assessmentId, List<AssessmentAnswer> answers, ISession session)
        {
            AssessmentCreateStatementResponse response = new AssessmentCreateStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int answerOrdering = 1;
                foreach (AssessmentAnswer answer in answers)
                {
                    AssessmentValidationResponse validateResponse = ValidateAnswer(answer);
                    if (!validateResponse.Success)
                    {
                        response.ErrorCode = validateResponse.ErrorCode;
                        response.ErrorMessage = validateResponse.ErrorMessage;
                        return response;
                    }

                    answer.Ordering = answerOrdering;

                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_answer",
                       new List<string> { "id", "assessment_id", "title", "ordering", "condition", "visible_in_assessment", "visible_to_self_profile_page", "visible_to_others", "for_completed_user", "for_incompleted_user" }));
                    response.Statements.Add(ps.Bind(answer.AnswerId, assessmentId, answer.Title, answer.Ordering, answer.Condition, answer.ShowInAssessment, answer.VisibleToSelf, answer.VisibleToOthers, answer.VisibleToOthersForCompletedUser, answer.VisibleToOthersForIncompletedUser));

                    foreach (AssessmentTabulation tabulation in answer.Tabulations)
                    {
                        if (tabulation != null)
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("tabulation_by_answer",
                            new List<string> { "answer_id", "tabulation_id" }));
                            response.Statements.Add(ps.Bind(answer.AnswerId, tabulation.TabulationId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("answer_by_tabulation",
                                new List<string> { "answer_id", "tabulation_id" }));
                            response.Statements.Add(ps.Bind(answer.AnswerId, tabulation.TabulationId));
                        }
                    }

                    answerOrdering++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AssessmentUpdateWithStatementReponse UpdateAnswer(string assessmentId, AssessmentAnswer answer, ISession session)
        {
            AssessmentUpdateWithStatementReponse response = new AssessmentUpdateWithStatementReponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;

            try
            {
                AssessmentValidationResponse validateResponse = ValidateAnswer(answer);
                if (!validateResponse.Success)
                {
                    response.ErrorCode = validateResponse.ErrorCode;
                    response.ErrorMessage = validateResponse.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("assessment_answer",
                    new List<string> { "id", "assessment_id" }, new List<string> { "title", "ordering", "condition", "visible_in_assessment", "visible_to_self_profile_page", "visible_to_others", "for_completed_user", "for_incompleted_user" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(answer.Title, answer.Ordering, answer.Condition, answer.ShowInAssessment, answer.VisibleToSelf, answer.VisibleToOthers, answer.VisibleToOthersForCompletedUser, answer.VisibleToOthersForIncompletedUser, answer.AnswerId, assessmentId));

                // Remove all links with tabulations
                ps = session.Prepare(CQLGenerator.SelectStatement("tabulation_by_answer", new List<string>(), new List<string> { "answer_id" }));
                RowSet linkRowset = session.Execute(ps.Bind(answer.AnswerId));

                foreach (Row linkRow in linkRowset)
                {
                    string tabulationId = linkRow.GetValue<string>("tabulation_id");
                    ps = session.Prepare(CQLGenerator.DeleteStatement("answer_by_tabulation", new List<string> { "tabulation_id", "answer_id" }));
                    response.DeleteStatements.Add(ps.Bind(tabulationId, answer.AnswerId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("tabulation_by_answer", new List<string> { "answer_id" }));
                response.DeleteStatements.Add(ps.Bind(answer.AnswerId));

                // Insert new links with tabulations
                foreach (AssessmentTabulation tabulation in answer.Tabulations)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("tabulation_by_answer",
                        new List<string> { "answer_id", "tabulation_id" }));
                    response.UpdateStatements.Add(ps.Bind(answer.AnswerId, tabulation.TabulationId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("answer_by_tabulation",
                        new List<string> { "answer_id", "tabulation_id" }));
                    response.UpdateStatements.Add(ps.Bind(answer.AnswerId, tabulation.TabulationId));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }


        private AssessmentValidationResponse ValidateAnswer(AssessmentAnswer answer)
        {
            AssessmentValidationResponse response = new AssessmentValidationResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(answer.AnswerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentInvalidAnswerId);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalidAnswerId;
                    return response;
                }

                if (string.IsNullOrEmpty(answer.Title))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentMissingAnswerTitle);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingAnswerTitle;
                    return response;
                }

                if (answer.Tabulations.Count == 0)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentMissingAnswerTables);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingAnswerTables;
                    return response;
                }

                if (answer.Condition < (int)AssessmentAnswerConditionTypeEnum.TableWithHighestPoint || answer.Condition > (int)AssessmentAnswerConditionTypeEnum.DisplayAllTables)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentAnswerConditionMismatch);
                    response.ErrorMessage = ErrorMessage.AssessmentAnswerConditionMismatch;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private AssessmentValidationResponse ValidateAssessment(Assessment assessment, List<AssessmentTabulation> newTabulations = null, List<AssessmentAnswer> newAnswers = null)
        {
            AssessmentValidationResponse response = new AssessmentValidationResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(assessment.Title))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingTitle);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingTitle;
                    return response;
                }

                if (string.IsNullOrEmpty(assessment.Introduction))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingIntroduction);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingIntroduction;
                    return response;
                }

                if (string.IsNullOrEmpty(assessment.ClosingWords))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingClosingWords);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingClosingWords;
                    return response;
                }

                if (newTabulations == null)
                {
                    if (assessment.Tabulations.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingTabulations);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingTabulations;
                        return response;
                    }
                }
                else
                {
                    if (newTabulations.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingTabulations);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingTabulations;
                        return response;
                    }
                }

                if (newAnswers == null)
                {
                    if (assessment.Answers.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingAnswers);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingAnswers;
                        return response;
                    }

                    foreach (AssessmentAnswer answer in assessment.Answers)
                    {
                        if (answer.Tabulations.Count == 0)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentAnswerMissingTabulation);
                            response.ErrorMessage = ErrorMessage.AssessmentAnswerMissingTabulation;
                            return response;
                        }
                    }
                }
                else
                {
                    if (newAnswers.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingAnswers);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingAnswers;
                        return response;
                    }

                    foreach (AssessmentAnswer answer in newAnswers)
                    {
                        if (answer.Tabulations.Count == 0)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentAnswerMissingTabulation);
                            response.ErrorMessage = ErrorMessage.AssessmentAnswerMissingTabulation;
                            return response;
                        }
                    }
                }


                if (assessment.Cards.Count == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentMissingCards);
                    response.ErrorMessage = ErrorMessage.AssessmentMissingCards;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private void InitField(string title, string introduction, string closingWords, string videoUrl, string videoTitle, string videoDescription, string videoThumbnailUrl)
        {
            try
            {
                if (string.IsNullOrEmpty(title))
                {
                    title = null;
                }

                if (string.IsNullOrEmpty(introduction))
                {
                    introduction = null;
                }

                if (string.IsNullOrEmpty(closingWords))
                {
                    closingWords = null;
                }

                if (string.IsNullOrEmpty(videoUrl))
                {
                    videoUrl = null;
                    videoTitle = null;
                    videoDescription = null;
                    videoThumbnailUrl = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void DeleteFromS3(string assessmentId, List<string> newUpdatedImageUrls = null)
        {
            try
            {
                bool isDeleteFolder = true;
                String bucketName = "cocadre-marketplace";

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;

                    listRequest.Prefix = assessmentId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        if (imageObject.Size <= 0)
                        {
                            continue;
                        }

                        if (newUpdatedImageUrls.Count > 0)
                        {
                            isDeleteFolder = false;
                            //pulses/PF7168c6ceedc34e709790cd63398c9668/U811e9a9236c140ad98a211a428b6dc84_20160527072241738_original.jpg 
                            string[] stringToken = imageObject.Key.Split('/');
                            string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                            string[] fileNames = fileNamePath.Split('_');
                            string fileName = fileNames[0] + "_" + fileNames[1];

                            if (newUpdatedImageUrls.FirstOrDefault(url => url.Contains(fileName)) != null)
                            {
                                continue;
                            }
                        }

                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }

                    if (isDeleteFolder)
                    {
                        // Delete folder
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = assessmentId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }

    [Serializable]
    [DataContract]
    public class AssessmentImage : ContentImage
    {
        [DataMember]
        public string ImageId { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public int Ordering { get; set; }
    }

    [Serializable]
    [DataContract]
    public partial class AssessmentTabulation
    {
        [DataMember]
        public string TabulationId { get; set; }

        [DataMember]
        public string Label { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public bool HasImage { get; set; }

        [DataMember]
        public List<AssessmentImage> Images { get; set; }

        [DataMember]
        public string InfoTitle { get; set; }

        [DataMember]
        public string InfoDescription { get; set; }

        [DataMember]
        public string InfoHtmlDescription { get; set; }

        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public List<ContentImage> ImagesUpload { get; set; }

        public AssessmentTabulation()
        {
            this.Images = new List<AssessmentImage>();
            this.ImagesUpload = new List<ContentImage>();
        }

    }

    [Serializable]
    [DataContract]
    public class ContentImage
    {
        [DataMember]
        public string base64 { get; set; }

        [DataMember]
        public string extension { get; set; }

        [DataMember]
        public int width { get; set; }

        [DataMember]
        public int height { get; set; }
    }

    [Serializable]
    [DataContract]
    public class Base64Content
    {
        [DataMember]
        public string base64 { get; set; }

        [DataMember]
        public string filename { get; set; }
    }


    [Serializable]
    [DataContract]
    public class AssessmentAnswer
    {
        [DataMember]
        public string AnswerId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int Condition { get; set; }

        [DataMember]
        public bool ShowInAssessment { get; set; }

        [DataMember]
        public bool VisibleToSelf { get; set; }

        [DataMember]
        public bool VisibleToOthers { get; set; }

        [DataMember]
        public bool VisibleToOthersForCompletedUser { get; set; }

        [DataMember]
        public bool VisibleToOthersForIncompletedUser { get; set; }

        [DataMember]
        public List<AssessmentTabulation> Tabulations { get; set; }

        [DataMember]
        public List<string> TabulationIdList { get; set; }

        public AssessmentAnswer()
        {
            this.TabulationIdList = new List<string>();
            this.Tabulations = new List<AssessmentTabulation>();
        }
    }



}