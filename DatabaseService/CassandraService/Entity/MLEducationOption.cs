﻿using Cassandra;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.GlobalResources;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using CassandraService.CassandraUtilities;
using Amazon.S3;
using Amazon;
using System.Web.Configuration;
using Amazon.S3.Model;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLEducationOption
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum QueryType
        {
            Basic = 1,
            Full = 2,
            FullWithAnalytics = 3
        }

        [DataMember]
        public string OptionId { get; set; }

        [DataMember]
        public bool HasUploadedContent { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public List<MLUploadedContent> UploadedContent { get; set; }


        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public bool IsIncompleteStateSelected { get; set; }

        [DataMember]
        public int NumberOfSelection { get; set; }

        [DataMember]
        public User AnsweredByUser { get; set; }

        [DataMember]
        public int Attempt { get; set; }

        public MLEducationOptionsCreateResponse CreateOptions(string adminUserId, string cardId, int currentCardOrdering, string educationId, List<MLEducationOption> options, ISession session)
        {
            MLEducationOptionsCreateResponse response = new MLEducationOptionsCreateResponse();
            response.BoundStatements = new List<BoundStatement>();
            response.UploadedUrls = new List<string>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                int ordering = 1;
                // Insert
                if (options != null || options.Count > 0)
                {
                    DateTime currentDate = DateTime.UtcNow;
                    foreach (MLEducationOption option in options)
                    {
                        bool hasUploadedContent = option.UploadedContent != null && option.UploadedContent.Count > 0 ? true : false;

                        if (string.IsNullOrEmpty(option.OptionId))
                        {
                            option.OptionId = UUIDGenerator.GenerateUniqueIDForMLEducationOption();
                        }

                        // Check if option is empty
                        if (string.IsNullOrEmpty(option.Content))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionContentEmpty);
                            response.ErrorMessage = ErrorMessage.MLOptionContentEmpty;
                            return response;
                        }

                        option.Ordering = ordering;

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_option",
                            new List<string> { "card_id", "id", "has_uploaded_content", "content", "ordering", "score", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                        response.BoundStatements.Add(ps.Bind(cardId, option.OptionId, hasUploadedContent, option.Content, option.Ordering, option.Score, adminUserId, currentDate, adminUserId, currentDate));

                        if (hasUploadedContent)
                        {
                            int uploadOrdering = 1;
                            foreach (MLUploadedContent content in option.UploadedContent)
                            {
                                string uploadId = UUIDGenerator.GenerateUniqueIDForMLUploadedContent();
                                int uploadType = content.UploadType;
                                string url = content.Url;

                                if (string.IsNullOrEmpty(url))
                                {
                                    Log.Error("Invalid option uploaded content url");
                                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionUploadContentUrlMissing);
                                    response.ErrorMessage = ErrorMessage.MLOptionUploadContentUrlMissing;
                                    return response;
                                }

                                content.Ordering = uploadOrdering;

                                ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_option_uploaded_content",
                                    new List<string> { "option_id", "upload_id", "upload_type", "url", "ordering" }));
                                response.BoundStatements.Add(ps.Bind(option.OptionId, uploadId, uploadType, url, content.Ordering));
                                response.UploadedUrls.Add(url);

                                uploadOrdering += 1;
                            }
                        }

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_option_order",
                            new List<string> { "card_id", "option_id", "ordering" }));
                        response.BoundStatements.Add(ps.Bind(cardId, option.OptionId, option.Ordering));

                        ordering++;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationOptionUpdateResponse UpdateOptions(string cardId, string educationId, int currentCardOrdering, List<MLEducationOption> options, string companyId, ISession session)
        {
            MLEducationOptionUpdateResponse response = new MLEducationOptionUpdateResponse();
            response.BoundStatements = new List<BoundStatement>();
            response.Success = false;

            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                foreach (MLEducationOption option in options)
                {
                    if (string.IsNullOrEmpty(option.OptionId))
                    {
                        continue;
                    }

                    if (string.IsNullOrEmpty(option.Content))
                    {
                        Log.Error("Option content is empty");
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionContentEmpty);
                        response.ErrorMessage = ErrorMessage.MLOptionContentEmpty;
                        return response;
                    }

                    // Delete from S3
                    // Check previously have files or not
                    // If have, check previous url if similar
                    bool hasUploadedContent = false;
                    ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_option", new List<string>(), new List<string> { "card_id", "id" }));
                    Row currentOptionRow = session.Execute(ps.Bind(cardId, option.OptionId)).FirstOrDefault();
                    if (currentOptionRow != null)
                    {
                        hasUploadedContent = currentOptionRow.GetValue<bool>("has_uploaded_content");
                    }

                    List<string> currentUploadedUrls = new List<string>();
                    if (hasUploadedContent)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_option_uploaded_content", new List<string>(), new List<string> { "option_id" }));
                        RowSet currentOptionImageRowset = session.Execute(ps.Bind(option.OptionId));

                        foreach (Row currentOptionImageRow in currentOptionImageRowset)
                        {
                            string uploadedUrl = currentOptionImageRow.GetValue<string>("url");
                            currentUploadedUrls.Add(uploadedUrl);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_option_uploaded_content", new List<string> { "option_id" }));
                        deleteBatch.Add(ps.Bind(option.OptionId));
                    }

                    if (option.UploadedContent.Count > 0)
                    {
                        int order = 1;
                        foreach (MLUploadedContent content in option.UploadedContent)
                        {
                            string uploadId = UUIDGenerator.GenerateUniqueIDForMLUploadedContent();
                            string url = content.Url;
                            int uploadType = content.UploadType;

                            if (string.IsNullOrEmpty(url))
                            {
                                Log.Error("Invalid uploaded content url");
                                response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionUploadContentUrlMissing);
                                response.ErrorMessage = ErrorMessage.MLOptionUploadContentUrlMissing;
                                return response;
                            }

                            ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_option_uploaded_content",
                                new List<string> { "option_id", "upload_id", "upload_type", "url", "ordering" }));
                            response.BoundStatements.Add(ps.Bind(option.OptionId, uploadId, uploadType, url, order));

                            order++;

                            if (currentUploadedUrls.Contains(url))
                            {
                                currentUploadedUrls.Remove(url);
                            }
                        }
                    }

                    if (currentUploadedUrls.Count > 0)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();
                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            // Remaining image url to be remove
                            foreach (string currentOptionImageUrl in currentUploadedUrls)
                            {
                                string path = currentOptionImageUrl.Replace("https://", "");
                                string[] splitPath = path.Split('/');
                                string optionId = splitPath[splitPath.Count() - 2];
                                string fileName = splitPath[splitPath.Count() - 1];

                                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "mlearnings/educations/" + educationId + "/" + cardId + "/" + optionId + "/" + fileName
                                };

                                s3Client.DeleteObject(deleteObjectRequest);
                            }
                        }
                    }


                    ps = session.Prepare(CQLGenerator.UpdateStatement("ml_education_card_option", new List<string> { "card_id", "id" }, new List<string> { "content", "has_uploaded_content" }, new List<string>()));
                    response.BoundStatements.Add(ps.Bind(option.Content, option.HasUploadedContent, cardId, option.OptionId));
                }

                session.Execute(deleteBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationOptionUpdateResponse DeleteAllOptions(string educationId, string cardId, string companyId, ISession session, List<string> newImageFileNames = null)
        {
            MLEducationOptionUpdateResponse response = new MLEducationOptionUpdateResponse();
            response.BoundStatements = new List<BoundStatement>();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_option", new List<string>(), new List<string> { "card_id" }));

                RowSet optionRowset = session.Execute(ps.Bind(cardId));

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    foreach (Row optionRow in optionRowset)
                    {
                        string optionId = optionRow.GetValue<string>("id");
                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_option_uploaded_content", new List<string> { "option_id" }));
                        response.BoundStatements.Add(ps.Bind(optionId));

                        // Remove from S3
                        String bucketName = "cocadre-" + companyId.ToLower();

                        ListObjectsRequest listRequest = new ListObjectsRequest();
                        listRequest.BucketName = bucketName;
                        listRequest.Prefix = "mlearnings/educations/" + educationId + "/" + cardId + "/" + optionId;

                        ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                        foreach (S3Object imageObject in listResponse.S3Objects)
                        {
                            if (imageObject.Size <= 0)
                            {
                                continue;
                            }

                            if (newImageFileNames != null && newImageFileNames.Count > 0)
                            {
                                //mlearnings/educations/MLT943cee78090948d086af5e30d051a250/MLCf60dd0247a12406ea824e7d59c5007cc/1_20160510101139774_original.jpeg
                                string[] stringToken = imageObject.Key.Split('/');
                                string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                                string[] fileNames = fileNamePath.Split('_');
                                string fileName = fileNames[0] + "_" + fileNames[1];

                                if (newImageFileNames.Any(newFileName => newFileName.Contains(fileName)))
                                {
                                    continue;
                                }
                            }

                            DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                            deleteRequest.BucketName = bucketName;
                            deleteRequest.Key = imageObject.Key;
                            s3Client.DeleteObject(deleteRequest);
                        }

                        // Delete folder if this is not update
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = "mlearnings/educations/" + educationId + "/" + cardId + "/" + optionId + "/";
                        s3Client.DeleteObject(deleteFolderRequest);
                    }

                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_option_order", new List<string> { "card_id" }));
                response.BoundStatements.Add(ps.Bind(cardId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_option", new List<string> { "card_id" }));
                response.BoundStatements.Add(ps.Bind(cardId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationOption GetDetail(string educationId, string cardId, string optionId, int queryType, ISession session)
        {
            MLEducationOption option = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_option", new List<string>(), new List<string> { "card_id", "id" }));
                Row answerRow = session.Execute(ps.Bind(cardId, optionId)).FirstOrDefault();

                if (answerRow == null)
                {
                    return option;
                }

                string content = answerRow.GetValue<string>("content");
                int score = answerRow.GetValue<int>("score");
                bool hasUploadedContent = false;
                List<MLUploadedContent> uploadContent = new List<MLUploadedContent>();

                if (queryType == (int)MLOption.MLOptionQueryType.Full)
                {
                    hasUploadedContent = answerRow.GetValue<bool>("has_uploaded_content");

                    if (hasUploadedContent)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_option_uploaded_content", new List<string>(), new List<string> { "option_id" }));
                        RowSet uploadRowset = session.Execute(ps.Bind(optionId));

                        foreach (Row uploadRow in uploadRowset)
                        {
                            string uploadId = uploadRow.GetValue<string>("upload_id");
                            string url = uploadRow.GetValue<string>("url");
                            int order = uploadRow.GetValue<int>("ordering");
                            int type = uploadRow.GetValue<int>("upload_type");

                            MLUploadedContent image = new MLUploadedContent
                            {
                                UploadId = uploadId,
                                Url = url,
                                Ordering = order,
                                UploadType = type,
                            };

                            uploadContent.Add(image);
                        }

                        if (uploadContent.Count > 1)
                        {
                            uploadContent = uploadContent.OrderBy(o => o.Ordering).ToList();
                        }
                    }
                }

                option = new MLEducationOption
                {
                    OptionId = optionId,
                    HasUploadedContent = hasUploadedContent,
                    Content = content,
                    UploadedContent = uploadContent,
                    Score = score
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return option;
        }

        public List<MLEducationOption> GetList(string educationId, string companyId, string cardId, ISession session, ISession analyticSession, string answeredByUserId = null, bool isOptionRandomized = false)
        {
            List<MLEducationOption> response = new List<MLEducationOption>();
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_option_order", new List<string>(), new List<string> { "card_id" }));
                RowSet answerOrderRowset = session.Execute(ps.Bind(cardId));

                AnalyticLearning analyticManager = new AnalyticLearning();

                foreach (Row answerOrderRow in answerOrderRowset)
                {
                    string optionId = answerOrderRow.GetValue<string>("option_id");
                    int ordering = answerOrderRow.GetValue<int>("ordering");

                    MLEducationOption option = GetDetail(educationId, cardId, optionId, (int)QueryType.Full, session);
                    if (option != null)
                    {
                        if (!string.IsNullOrEmpty(answeredByUserId))
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_answered_by_card", new List<string>(), new List<string> { "card_id", "option_id", "answered_by_user_id" }));
                            RowSet rsAnsweredLog = analyticSession.Execute(ps.Bind(cardId, optionId, answeredByUserId));
                            if (rsAnsweredLog != null)
                            {
                                option.Attempt = rsAnsweredLog.Count();
                            }
                            else
                            {
                                option.Attempt = 0;
                            }
                        }
                        option.Ordering = ordering;
                        response.Add(option);
                    }
                }

                if (response.Count > 1)
                {
                    if (isOptionRandomized)
                    {
                        response = response.OrderBy(o => Guid.NewGuid()).ToList();
                    }
                    else
                    {
                        response = response.OrderBy(o => o.Ordering).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }
    }
}
