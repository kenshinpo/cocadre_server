﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class RSTopicCategory
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum RSCategoryQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember(EmitDefaultValue = false)]
        public string CategoryId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NumberOfRSTopics { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsForEvent { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<RSTopic> Topics { get; set; }

        public RSCategoryCreateResponse CreateRSCategory(string companyId, string title, string adminUserId, ISession session = null, string categoryId = null, bool isForEvent = false)
        {
            RSCategoryCreateResponse response = new RSCategoryCreateResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error("Category title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryTitleMissing);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryTitleMissing;
                    return response;
                }

                if (string.IsNullOrEmpty(categoryId))
                {
                    categoryId = UUIDGenerator.GenerateUniqueIDForRSCategory();
                }

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.InsertStatement("rs_category", new List<string> { "id", "company_id", "title", "is_for_event", "is_valid", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batchStatement.Add(ps.Bind(categoryId, companyId, title, isForEvent, true, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));

                ps = session.Prepare(CQLGenerator.InsertStatement("rs_category_by_title", new List<string> { "rs_category_id", "company_id", "title", "is_for_event" }));
                batchStatement.Add(ps.Bind(categoryId, companyId, title, isForEvent));

                session.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCategorySelectResponse SelectRSCategory(string categoryId, string companyId, ISession session = null)
        {
            RSCategorySelectResponse response = new RSCategorySelectResponse();
            response.Category = null;
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (categoryRow != null)
                {
                    RSTopicCategory category = new RSTopicCategory
                    {
                        CategoryId = categoryRow.GetValue<string>("id"),
                        Title = categoryRow.GetValue<string>("title")
                    };

                    response.Category = category;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCategorySelectAllResponse SelectAllRSCategories(string adminUserId, string companyId, int queryType, ISession session = null)
        {
            RSCategorySelectAllResponse response = new RSCategorySelectAllResponse();
            response.Categories = new List<RSTopicCategory>();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_category_by_title", new List<string>(), new List<string> { "company_id" }));
                RowSet categoryRowset = session.Execute(ps.Bind(companyId));
                List<Row> categoryRowList = categoryRowset.ToList();

                if (categoryRowList.Count == 0)
                {
                    string eventCategoryId = UUIDGenerator.GenerateUniqueIDForRSCategory();
                    string title = "Event";
                    CreateRSCategory(companyId, title, adminUserId, session, eventCategoryId);
                    RSTopicCategory category = new RSTopicCategory
                    {
                        CategoryId = eventCategoryId,
                        Title = title,
                        NumberOfRSTopics = 0,
                        IsForEvent = false
                    };

                    response.Categories.Add(category);
                }
                else
                {
                    foreach (Row categoryRow in categoryRowList)
                    {
                        string categoryId = categoryRow.GetValue<string>("rs_category_id");
                        string title = categoryRow.GetValue<string>("title");
                        bool isForEvent = categoryRow.GetValue<bool>("is_for_event");

                        int numberOfTopics = 0;
                        if (queryType == (int)RSCategoryQueryType.FullDetail)
                        {
                            // Get number of topics
                            ps = session.Prepare(CQLGenerator.SelectStatement("rs_topic", new List<string>(), new List<string> { "category_id" }));
                            RowSet rs = session.Execute(ps.Bind(categoryId));
                            if (rs != null)
                            {
                                foreach (Row row in rs)
                                {
                                    if (row.GetValue<int>("status") > 0)
                                    {
                                        numberOfTopics++;
                                    }
                                }
                            }
                        }

                        RSTopicCategory category = new RSTopicCategory
                        {
                            CategoryId = categoryId,
                            Title = title,
                            NumberOfRSTopics = numberOfTopics,
                            IsForEvent = isForEvent
                        };

                        response.Categories.Add(category);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public RSCategoryUpdateResponse UpdateRSCategory(string adminUserId, string companyId, string categoryId, string newTitle)
        {
            RSCategoryUpdateResponse response = new RSCategoryUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row rsCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (rsCategoryRow == null)
                {
                    Log.Error("RSTopic category is invalid: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(newTitle))
                {
                    Log.Error("Category title is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryTitleMissing);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryTitleMissing;
                    return response;
                }

                bool isForEvent = rsCategoryRow.GetValue<bool>("is_for_event");

                if (isForEvent)
                {
                    Log.Error("Category is meant for event");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSEventTopicCategoryCannotBeDeleted);
                    response.ErrorMessage = ErrorMessage.RSEventTopicCategoryCannotBeDeleted;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                string currentTitle = rsCategoryRow.GetValue<string>("title");

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("rs_category",
                    new List<string> { "company_id", "id" }, new List<string> { "title", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newTitle, adminUserId, DateTime.UtcNow, companyId, categoryId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_category_by_title",
                    new List<string> { "company_id", "title", "rs_category_id" }));
                deleteBatch.Add(ps.Bind(companyId, currentTitle, categoryId));

                ps = session.Prepare(CQLGenerator.InsertStatement("rs_category_by_title",
                    new List<string> { "company_id", "title", "rs_category_id", "is_for_event" }));
                updateBatch.Add(ps.Bind(companyId, newTitle, categoryId, isForEvent));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCategoryUpdateResponse DeleteRSCategory(string adminUserId, string companyId, string categoryId)
        {
            RSCategoryUpdateResponse response = new RSCategoryUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row rsCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (rsCategoryRow == null)
                {
                    Log.Error("RSTopic category is invalid: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithLimit("rstopic_by_rscategory",
                    new List<string>(), new List<string> { "rs_topic_category_id" }, 1));
                Row categoryRelation = session.Execute(ps.Bind(categoryId)).FirstOrDefault();

                if (categoryRelation != null)
                {
                    Log.Error("Topics still exists in category: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryCannotBeDeleted);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryCannotBeDeleted;
                    return response;
                }

                bool isForEvent = rsCategoryRow.GetValue<bool>("is_for_event");

                if (isForEvent)
                {
                    Log.Error("Category is meant for event");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSEventTopicCategoryCannotBeDeleted);
                    response.ErrorMessage = ErrorMessage.RSEventTopicCategoryCannotBeDeleted;
                    return response;
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                string currentTitle = rsCategoryRow.GetValue<string>("title");

                ps = session.Prepare(CQLGenerator.DeleteStatement("rstopic_by_rscategory",
                    new List<string> { "rs_topic_category_id" }));
                deleteBatch.Add(ps.Bind(categoryId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_category_by_title",
                   new List<string> { "company_id", "title", "rs_category_id" }));
                deleteBatch.Add(ps.Bind(companyId, currentTitle, categoryId));

                ps = session.Prepare(CQLGenerator.UpdateStatement("rs_category",
                    new List<string> { "company_id", "id" }, new List<string> { "is_valid", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(false, adminUserId, DateTime.UtcNow, companyId, categoryId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
