﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    public class Chart
    {
        public enum ChartType
        {
            Bar = 1,
            Pie = 2,
            OneToOne = 3,
            Doughnut = 4,
            HorizontalHistogram = 5
        }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int MaxCount { get; set; }
        [DataMember]
        public string Disclaimer { get; set; }
        [DataMember]
        public bool IsLegendShown { get; set; }
        [DataMember]
        public List<string> Labels { get; set; }
        [DataMember]
        public List<Stat> Stats { get; set; }

        [DataContract]
        public class Stat
        {
            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public int Count { get; set; }
            [DataMember]
            public int Percentage { get; set; }
        }
    }
}
