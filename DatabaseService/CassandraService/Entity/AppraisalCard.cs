﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class AppraisalCard
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum AppraisalCardQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum AppraisalCardTypeEnum
        {
            NumberRange = 1,
            SelectOne = 2,
        }

        public enum AppraisalCardStatus
        {
            Deleted = -1,
            Active = 1,
            Hidden = 2
        }

        [DataMember]
        public string CardId { get; set; }

        [DataMember]
        public string ReferencedCardId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public bool IsCommentAllowed { get; set; }

        [DataMember]
        public List<AppraisalCardOption> Options { get; set; }

        [DataMember]
        public AppraisalCardOption OptionResult { get; set; }

        [DataMember]
        public List<AppraisalCardRange> NumberRanges { get; set; }

        [DataMember]
        public string CategoryId { get; set; }

        [DataMember]
        public string ReferencedCategoryId { get; set; }

        [DataMember]
        public string CategoryTitle { get; set; }

        [DataMember]
        public bool IsReferencedFromAdmin { get; set; }

        [DataMember]
        public List<AppraisalComment> Comments { get; set; }

        [DataMember]
        public double AverageRating { get; set; }

        [DataMember]
        public double SelfRating { get; set; }

        [DataMember(EmitDefaultValue = false)]
        Dictionary<string, List<Pulse.PulseAction>> PulseActions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public ComparisonFeedback ResultFeedback { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<ParticipantAnswerBreakdown> ResultBreakdown { get; set; }

        [Serializable]
        [DataContract]
        public class AppraisalCardType
        {
            [DataMember]
            public int Type { get; set; }

            [DataMember]
            public string Title { get; set; }

            [DataMember]
            public string Description { get; set; }
        }

        [Serializable]
        [DataContract]
        public class AppraisalComment
        {
            [DataMember]
            public string Content { get; set; }

            public string CommentedByUserId { get; set; }

            [DataMember]
            public User CommentedByUser { get; set; }

            [DataMember]
            public DateTime CreatedOnTimestamp { get; set; }

            [DataMember]
            public int Rated { get; set; }
        }

        [Serializable]
        [DataContract]
        public class ComparisonFeedback
        {
            [DataMember]
            public string Feedback { get; set; }
            [DataMember]
            public bool IsPositive { get; set; }
        }

        [Serializable]
        [DataContract]
        public class ParticipantAnswerBreakdown
        {
            [DataMember]
            public string ParticipantId { get; set; }

            [DataMember]
            public string CardId { get; set; }

            [DataMember]
            public string ReferencedCardId { get; set; }

            [DataMember]
            public bool IsReferencedFromAdmin { get; set; }

            [DataMember]
            public int SelectedScore { get; set; }

            [DataMember]
            public string Comment { get; set; }
        }

        public AppraisalCardCreateTemplateResponse CreateTemplateCardByAdmin(string adminUserId,
                                                                             string companyId,
                                                                             string appraisalId,
                                                                             string categoryId,
                                                                             string cardContent,
                                                                             int cardType,
                                                                             List<AppraisalCardOption> options)
        {
            AppraisalCardCreateTemplateResponse response = new AppraisalCardCreateTemplateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, appraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row categoryRow = vh.ValidateTemplateAppraisalCategory(categoryId, appraisalId, mainSession);
                if (categoryRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                AppraisalValidationResponse validationResponse = ValidateCreationCardFields(cardContent, false);
                if (!validationResponse.Success)
                {
                    response.ErrorCode = validationResponse.ErrorCode;
                    response.ErrorMessage = validationResponse.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalCardCreateStatementResponse createCardResponse = CreateTemplateCard(cardType, cardContent, categoryId, adminUserId, companyId, true, options, currentTime, mainSession);

                if (!createCardResponse.Success)
                {
                    response.ErrorCode = createCardResponse.ErrorCode;
                    response.ErrorMessage = createCardResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in createCardResponse.UpdateStatements)
                {
                    batch.Add(bs);
                }

                mainSession.Execute(batch);

                // Update counter for admin created card count
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.UpdateCounterStatement("admin_template_appraisal_360_card_count", new List<string> { "company_id", "appraisal_id" }, new List<string> { "card_count" }, new List<int> { 1 }));
                mainSession.Execute(ps.Bind(companyId, appraisalId));

                response.CardId = createCardResponse.NewCardId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCardCreateTemplateResponse CreateTemplateCardByUser(string creatorUserId, string companyId, string templateAppraisalId, string categoryId, string categoryTitle, string cardContent)
        {
            AppraisalCardCreateTemplateResponse response = new AppraisalCardCreateTemplateResponse();
            response.CardId = string.Empty;
            response.CategoryId = string.Empty;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(creatorUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                Row companySettingRow = new Appraisal().SelectAppraisalSetting(companyId, mainSession);
                if (companySettingRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCompanyNotOnboard);
                    response.ErrorMessage = ErrorMessage.AppraisalCompanyNotOnboard;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                int appraisalType = templateRow.GetValue<int>("type");
                if (appraisalType == (int)Appraisal.AppraisalTypeEnum.Bars)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalBarsCanOnlyBeCreatedByAdmin);
                    response.ErrorMessage = ErrorMessage.AppraisalBarsCanOnlyBeCreatedByAdmin;
                    return response;
                }

                AppraisalValidationResponse validationResponse = ValidateCreationCardFields(cardContent, true, categoryTitle);
                if (!validationResponse.Success)
                {
                    response.ErrorCode = validationResponse.ErrorCode;
                    response.ErrorMessage = validationResponse.ErrorMessage;
                    return response;
                }

                AppraisalCategory categoryManager = new AppraisalCategory();
                AppraisalCategory searchedCategory = categoryManager.SearchTemplateCategoryByUser(templateAppraisalId, categoryTitle, creatorUserId, mainSession);
                if (searchedCategory != null)
                {
                    if (!searchedCategory.CategoryId.Equals(categoryId))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                        return response;
                    }
                }
                else
                {
                    AppraisalCategoryCreateResponse createCategoryResponse = categoryManager.CreateTemplateCategoryByUser(templateAppraisalId, categoryTitle, creatorUserId, mainSession);
                    if (!createCategoryResponse.Success)
                    {
                        response.ErrorCode = createCategoryResponse.ErrorCode;
                        response.ErrorMessage = createCategoryResponse.ErrorMessage;
                        return response;
                    }
                    categoryId = createCategoryResponse.CategoryId;
                }

                DateTime currentTime = DateTime.UtcNow;
                AppraisalCardCreateStatementResponse createCardResponse = CreateTemplateCard(appraisalType, cardContent, categoryId, creatorUserId, companyId, false, null, currentTime, mainSession);

                if (!createCardResponse.Success)
                {
                    response.ErrorCode = createCardResponse.ErrorCode;
                    response.ErrorMessage = createCardResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in createCardResponse.UpdateStatements)
                {
                    batch.Add(bs);
                }

                mainSession.Execute(batch);
                response.CardId = createCardResponse.NewCardId;
                response.CategoryId = categoryId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCardCreateStatementResponse CreateCustomCard(string appraisalId,
                                                                     int type,
                                                                     string content,
                                                                     string cardId,
                                                                     string categoryId,
                                                                     string referencedTemplateId,
                                                                     string referencedCategoryId,
                                                                     string referencedCategoryTitle,
                                                                     string referencedCardId,
                                                                     bool isReferencedFromAdmin,
                                                                     bool isCommentAllowed,
                                                                     int ordering,
                                                                     List<AppraisalCardRange> numberRanges,
                                                                     List<AppraisalCardOption> options,
                                                                     ISession mainSession)
        {
            AppraisalCardCreateStatementResponse response = new AppraisalCardCreateStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card",
                    new List<string> { "appraisal_id", "referenced_template_id", "category_id", "category_title", "referenced_category_id", "is_referenced_from_admin", "card_id", "referenced_card_id", "type", "content", "is_comment_allowed", "ordering" }));
                response.UpdateStatements.Add(ps.Bind(appraisalId, referencedTemplateId, categoryId, referencedCategoryTitle, referencedCategoryId, isReferencedFromAdmin, cardId, referencedCardId, type, content, isCommentAllowed, ordering));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_by_ordering",
                    new List<string> { "appraisal_id", "referenced_template_id", "category_id", "category_title", "referenced_category_id", "is_referenced_from_admin", "card_id", "referenced_card_id", "type", "content", "is_comment_allowed", "ordering" }));
                response.UpdateStatements.Add(ps.Bind(appraisalId, referencedTemplateId, categoryId, referencedCategoryTitle, referencedCategoryId, isReferencedFromAdmin, cardId, referencedCardId, type, content, isCommentAllowed, ordering));

                if (type == (int)AppraisalCardTypeEnum.NumberRange)
                {
                    foreach (BoundStatement bs in new AppraisalCardRange().CreateCustomRange(cardId, numberRanges, mainSession).UpdateStatements)
                    {
                        response.UpdateStatements.Add(bs);
                    }
                }
                else if (type == (int)AppraisalCardTypeEnum.SelectOne)
                {
                    foreach (BoundStatement bs in new AppraisalCardOption().CreateCustomOptions(cardId, options, mainSession).UpdateStatements)
                    {
                        response.UpdateStatements.Add(bs);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCardDeleteStatementResponse DeleteCustomCard(string appraisalId,
                                                                     int type,
                                                                     int ordering,
                                                                     string cardId,
                                                                     ISession mainSession)
        {
            AppraisalCardDeleteStatementResponse response = new AppraisalCardDeleteStatementResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_card",
                    new List<string> { "appraisal_id", "card_id" }));
                response.DeleteStatements.Add(ps.Bind(appraisalId, cardId));

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_card_by_ordering",
                    new List<string> { "appraisal_id", "ordering", "card_id" }));
                response.DeleteStatements.Add(ps.Bind(appraisalId, ordering, cardId));

                if (type == (int)AppraisalCardTypeEnum.NumberRange)
                {
                    foreach (BoundStatement bs in new AppraisalCardRange().DeleteCustomRange(cardId, mainSession).DeleteStatements)
                    {
                        response.DeleteStatements.Add(bs);
                    }
                }
                else if (type == (int)AppraisalCardTypeEnum.SelectOne)
                {
                    foreach (BoundStatement bs in new AppraisalCardOption().DeleteCustomOptions(cardId, mainSession).DeleteStatements)
                    {
                        response.DeleteStatements.Add(bs);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCardSelectResponse SelectCardsFromCategory(string categoryId, string companyId, ISession mainSession, int cardQueryType = (int)AppraisalCardQueryType.Basic)
        {
            AppraisalCardSelectResponse response = new AppraisalCardSelectResponse();
            response.Cards = new List<AppraisalCard>();
            response.Success = false;
            try
            {
                Row companySettingRow = new Appraisal().SelectAppraisalSetting(companyId, mainSession);
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("template_appraisal_360_card_by_ordering", new List<string>(), new List<string> { "category_id" }));
                RowSet cardRowSet = mainSession.Execute(ps.Bind(categoryId));
                foreach (Row cardRow in cardRowSet)
                {
                    response.Cards.Add(SelectTemplateCard(cardRow, companySettingRow, mainSession, cardQueryType));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCard SelectTemplateCard(Row cardRow, Row companySettingRow, ISession mainSession, int cardQueryType = (int)AppraisalCardQueryType.Basic)
        {
            AppraisalCard card = null;

            try
            {
                string cardId = cardRow.GetValue<string>("card_id");
                int cardType = cardRow.GetValue<int>("type");

                card = new AppraisalCard
                {
                    CardId = cardId,
                    IsCommentAllowed = cardRow.GetValue<bool>("is_comment_allowed"),
                    Type = cardType,
                    Content = cardRow.GetValue<string>("content"),
                    Ordering = cardRow.GetValue<int>("ordering"),
                };

                if (cardQueryType == (int)AppraisalCardQueryType.FullDetail)
                {
                    List<AppraisalCardOption> options = new List<AppraisalCardOption>();
                    List<AppraisalCardRange> ranges = new List<AppraisalCardRange>();
                    if (cardType == (int)AppraisalCardTypeEnum.SelectOne)
                    {
                        options = new AppraisalCardOption().SelectTemplateOptions(cardId, mainSession).Options;
                    }
                    else if (cardType == (int)AppraisalCardTypeEnum.NumberRange)
                    {
                        ranges = new AppraisalCardRange().SelectLikert7(companySettingRow, mainSession).Ranges;
                    }

                    card.NumberRanges = ranges;
                    card.Options = options;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return card;
        }

        public AppraisalCardSelectResponse SelectCardsFromCustomAppraisal(string appraisalId, ISession mainSession, ISession analyticSession = null, string requesterUserId = null, bool isSelectUserAnswer = false)
        {
            AppraisalCardSelectResponse response = new AppraisalCardSelectResponse();
            response.Cards = new List<AppraisalCard>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_by_ordering", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet cardRowSet = mainSession.Execute(ps.Bind(appraisalId));
                foreach (Row cardRow in cardRowSet)
                {
                    AppraisalCard card = SelectCustomCard(cardRow, mainSession, analyticSession, requesterUserId, isSelectUserAnswer);
                    card.PulseActions = new Pulse().SelectActionForAnsweringAppraisal();
                    response.Cards.Add(card);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCardSelectResponse SelectCardsResultFromCustomAppraisal(string appraisalId, string creatorUserId, bool isSelfAssessmentNeeded, double comparisonDifference, List<string> completedParticipantsIds, ISession mainSession, ISession analyticSession, bool isBreakdownNeeded = false)
        {
            AppraisalCardSelectResponse response = new AppraisalCardSelectResponse();
            response.Cards = new List<AppraisalCard>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_by_ordering", new List<string>(), new List<string> { "appraisal_id" }));
                RowSet cardRowSet = mainSession.Execute(ps.Bind(appraisalId));
                foreach (Row cardRow in cardRowSet)
                {
                    response.Cards.Add(SelectCustomCardResult(cardRow, creatorUserId, isSelfAssessmentNeeded, comparisonDifference, completedParticipantsIds, mainSession, analyticSession, isBreakdownNeeded));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalCard SelectCustomCard(Row cardRow, ISession mainSession, ISession analyticSession = null, string answeredByUserId = null, bool isSelectUserAnswer = false)
        {
            AppraisalCard card = null;

            try
            {
                string cardId = cardRow.GetValue<string>("card_id");
                int cardType = cardRow.GetValue<int>("type");

                card = new AppraisalCard
                {
                    CardId = cardId,
                    ReferencedCardId = cardRow.GetValue<string>("referenced_card_id"),
                    IsCommentAllowed = cardRow.GetValue<bool>("is_comment_allowed"),
                    Type = cardRow.GetValue<int>("type"),
                    Content = cardRow.GetValue<string>("content"),
                    Ordering = cardRow.GetValue<int>("ordering"),
                    CategoryId = cardRow.GetValue<string>("category_id"),
                    ReferencedCategoryId = cardRow.GetValue<string>("referenced_category_id"),
                    CategoryTitle = cardRow.GetValue<string>("category_title"),
                    IsReferencedFromAdmin = cardRow.GetValue<bool>("is_referenced_from_admin"),
                };

                List<AppraisalCardOption> options = new List<AppraisalCardOption>();
                List<AppraisalCardRange> ranges = new List<AppraisalCardRange>();
                if (cardType == (int)AppraisalCardTypeEnum.SelectOne)
                {
                    options = new AppraisalCardOption().SelectCustomOptions(cardId, mainSession).Options;
                }
                else if (cardType == (int)AppraisalCardTypeEnum.NumberRange)
                {
                    ranges = new AppraisalCardRange().SelectCustomRange(cardId, mainSession).Ranges;
                }

                card.NumberRanges = ranges;
                card.Options = options;

                if (isSelectUserAnswer)
                {
                    new AnalyticAppraisal().GetSelectedAnswerByCard(card, answeredByUserId, analyticSession);
                    SelectComments(card, mainSession, answeredByUserId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return card;
        }

        public AppraisalCard SelectCustomCardResult(Row cardRow, string creatorUserId, bool isSelfAssessmentNeeded, double comparisonDifference, List<string> completedParticipantsIds, ISession mainSession, ISession analyticSession, bool isBreakdownNeeded = false)
        {
            AppraisalCard card = null;

            try
            {
                card = SelectCustomCard(cardRow, mainSession, analyticSession);
                SelectComments(card, mainSession);
                new AnalyticAppraisal().GetResultByCard(card, creatorUserId, isSelfAssessmentNeeded, comparisonDifference, completedParticipantsIds, analyticSession, isBreakdownNeeded);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return card;
        }

        public AppraisalAnswerCardResponse AnswerCard(string answeredByUserId, string companyId, string appraisalId, string cardId, int selectedRange, int selectedOption, string comment)
        {
            AppraisalAnswerCardResponse response = new AppraisalAnswerCardResponse();
            response.CompletionNotification = new Notification();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Appraisal appraisalManager = new Appraisal();
                Row appraisalRow = vh.ValidateCustomAppraisal(companyId, appraisalId, mainSession);
                if (appraisalRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                if (!appraisalManager.CheckAppraisalPrivacy(appraisalId, answeredByUserId, mainSession))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateCustomAppraisalCard(appraisalId, cardId, mainSession);
                if (cardRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCard);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCard;
                    return response;
                }

                AppraisalCard card = SelectCustomCard(cardRow, mainSession);
                if (card.Type == (int)AppraisalCardTypeEnum.NumberRange)
                {
                    AppraisalCardRange range = card.NumberRanges.FirstOrDefault(r => r.RangeNumber == selectedRange);
                    if (range == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidSelectedRange);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalidSelectedRange;
                        return response;
                    }
                }
                else if (card.Type == (int)AppraisalCardTypeEnum.SelectOne)
                {
                    AppraisalCardOption option = card.Options.FirstOrDefault(o => o.OptionNumber == selectedOption);
                    if (option == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidSelectedOption);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalidSelectedOption;
                        return response;
                    }
                }

                bool isCompleted = false;
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_by_ordering", new List<string>(), new List<string> { "appraisal_id", "ordering" }));
                Row nextCardRow = mainSession.Execute(ps.Bind(appraisalId, card.Ordering + 1)).FirstOrDefault();
                if (nextCardRow == null)
                {
                    isCompleted = true;
                }

                DateTime currentDate = DateTime.UtcNow;

                AnswerCardThread(appraisalId, answeredByUserId, card, selectedRange, selectedOption, comment, currentDate, isCompleted, mainSession, analyticSession);

                if (isCompleted)
                {
                    // Check if appraisal is completed and send notification to creator
                    if (appraisalManager.ValidateResultSet(appraisalRow, analyticSession).Success)
                    {
                        string createdByUserId = appraisalRow.GetValue<string>("created_by_user_id");
                        string title = appraisalRow.GetValue<string>("title");
                        response.CompletionNotification = new Notification().CreateCompletedAppraisalReportNotification(companyId, createdByUserId, appraisalId, title, currentDate, mainSession);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AppraisalUpdateCardResponse UpdateTemplateCardByUser(string requesterUserId, string companyId, string templateAppraisalId, string categoryId, string cardId, string newCardContent)
        {
            AppraisalUpdateCardResponse response = new AppraisalUpdateCardResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                int appraisalType = templateRow.GetValue<int>("type");
                if (appraisalType == (int)Appraisal.AppraisalTypeEnum.Bars)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalBarsCanOnlyBeCreatedByAdmin);
                    response.ErrorMessage = ErrorMessage.AppraisalBarsCanOnlyBeCreatedByAdmin;
                    return response;
                }

                Row categoryRow = vh.ValidateAppraisalUserCategory(templateAppraisalId, categoryId, requesterUserId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                Row cardRow = vh.ValidateTemplateAppraisalCard(categoryId, cardId, mainSession);
                if (cardRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCard);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCard;
                    return response;
                }
                int cardType = cardRow.GetValue<int>("type");

                AppraisalValidationResponse validationResponse = ValidateCreationCardFields(newCardContent, false);
                if (!validationResponse.Success)
                {
                    response.ErrorCode = validationResponse.ErrorCode;
                    response.ErrorMessage = validationResponse.ErrorMessage;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalUpdateCardStatementResponse updateStatementResponse = UpdateCard(requesterUserId, categoryId, cardId, cardType, newCardContent, null, cardRow, currentTime, mainSession);
                if (!updateStatementResponse.Success)
                {
                    response.ErrorCode = updateStatementResponse.ErrorCode;
                    response.ErrorMessage = updateStatementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updateStatementResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }
                foreach (BoundStatement bs in updateStatementResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalUpdateCardResponse RemoveTemplateCardByUser(string requesterUserId, string companyId, string templateAppraisalId, string categoryId, string cardId)
        {
            AppraisalUpdateCardResponse response = new AppraisalUpdateCardResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                int appraisalType = templateRow.GetValue<int>("type");
                if (appraisalType == (int)Appraisal.AppraisalTypeEnum.Bars)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalBarsCanOnlyBeCreatedByAdmin);
                    response.ErrorMessage = ErrorMessage.AppraisalBarsCanOnlyBeCreatedByAdmin;
                    return response;
                }

                Row categoryRow = vh.ValidateAppraisalUserCategory(templateAppraisalId, categoryId, requesterUserId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                Row cardRow = vh.ValidateTemplateAppraisalCard(categoryId, cardId, mainSession);
                if (cardRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCard);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCard;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalUpdateCardStatementResponse updateStatementResponse = RemoveCard(requesterUserId, categoryId, cardId, cardRow, currentTime, mainSession);
                if (!updateStatementResponse.Success)
                {
                    response.ErrorCode = updateStatementResponse.ErrorCode;
                    response.ErrorMessage = updateStatementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updateStatementResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }
                foreach (BoundStatement bs in updateStatementResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalCardCreateTemplateResponse UpdateTemplateCardByAdmin(string cardId,
                                                                           string adminUserId,
                                                                           string companyId,
                                                                           string appraisalId,
                                                                           string categoryId,
                                                                           string newCardContent,
                                                                           int newCardType,
                                                                           List<AppraisalCardOption> newOptions)
        {
            AppraisalCardCreateTemplateResponse response = new AppraisalCardCreateTemplateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row templateRow = vh.ValidateTemplateAppraisal(companyId, appraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row categoryRow = vh.ValidateTemplateAppraisalCategory(categoryId, appraisalId, mainSession);
                if (categoryRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                Row currentCardRow = vh.ValidateTemplateAppraisalCard(categoryId, cardId, mainSession);
                if (currentCardRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCard);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCard;
                    return response;
                }

                AppraisalValidationResponse validationResponse = ValidateCreationCardFields(newCardContent, false);
                if (!validationResponse.Success)
                {
                    response.ErrorCode = validationResponse.ErrorCode;
                    response.ErrorMessage = validationResponse.ErrorMessage;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalUpdateCardStatementResponse updateCardResponse = UpdateCard(adminUserId, categoryId, cardId, newCardType, newCardContent, newOptions, currentCardRow, currentTime, mainSession);

                if (!updateCardResponse.Success)
                {
                    response.ErrorCode = updateCardResponse.ErrorCode;
                    response.ErrorMessage = updateCardResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updateCardResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                foreach (BoundStatement bs in updateCardResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalUpdateCardResponse RemoveTemplateCardByAdmin(string adminUserId, string companyId, string templateAppraisalId, string categoryId, string cardId)
        {
            AppraisalUpdateCardResponse response = new AppraisalUpdateCardResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                BatchStatement batch = new BatchStatement();
                Row templateRow = vh.ValidateTemplateAppraisal(companyId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalid);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalid;
                    return response;
                }

                Row categoryRow = vh.ValidateTemplateAppraisalCategory(categoryId, templateAppraisalId, mainSession);
                if (templateRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCategory);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCategory;
                    return response;
                }

                Row cardRow = vh.ValidateTemplateAppraisalCard(categoryId, cardId, mainSession);
                if (cardRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCard);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCard;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                DateTime currentTime = DateTime.UtcNow;
                AppraisalUpdateCardStatementResponse updateStatementResponse = RemoveCard(adminUserId, categoryId, cardId, cardRow, currentTime, mainSession);
                if (!updateStatementResponse.Success)
                {
                    response.ErrorCode = updateStatementResponse.ErrorCode;
                    response.ErrorMessage = updateStatementResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in updateStatementResponse.DeleteStatements)
                {
                    deleteBatch.Add(bs);
                }
                foreach (BoundStatement bs in updateStatementResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                // Update counter for admin created card count
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.UpdateCounterStatement("admin_template_appraisal_360_card_count", new List<string> { "company_id", "appraisal_id" }, new List<string> { "card_count" }, new List<int> { -1 }));
                mainSession.Execute(ps.Bind(companyId, templateAppraisalId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public int SelectAdminCreatedCardCount(string companyId, string appraisalId, ISession mainSession)
        {
            int count = 0;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("admin_template_appraisal_360_card_count", new List<string>(), new List<string> { "company_id", "appraisal_id" }));
                Row countRow = mainSession.Execute(ps.Bind(companyId, appraisalId)).FirstOrDefault();

                if (countRow == null)
                {
                    List<AppraisalCategory> categories = new AppraisalCategory().SelectAdminDefinedCategoriesByAppraisal(companyId, appraisalId, mainSession, (int)AppraisalCardQueryType.Basic).Categories;
                    foreach (AppraisalCategory category in categories)
                    {
                        count += category.Cards.Count;
                    }

                    // Update counter for admin created card count
                    ps = mainSession.Prepare(CQLGenerator.UpdateCounterStatement("admin_template_appraisal_360_card_count", new List<string> { "company_id", "appraisal_id" }, new List<string> { "card_count" }, new List<int> { count }));
                    mainSession.Execute(ps.Bind(companyId, appraisalId));
                }
                else
                {
                    count = (int)countRow.GetValue<long>("card_count");
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return count;
        }

        private void AnswerCardThread(string appraisalId, string answeredByUserId, AppraisalCard card, int selectedRange, int selectedOption, string comment, DateTime currentDate, bool isCompleted, ISession mainSession, ISession analyticSession)
        {
            try
            {
                AnalyticAppraisal analyticManager = new AnalyticAppraisal();
                string cardId = card.CardId;
                int cardType = card.Type;
                analyticManager.AnswerCard(appraisalId, answeredByUserId, cardType, cardId, selectedRange, selectedOption, currentDate, analyticSession, isCompleted);

                if (string.IsNullOrEmpty(comment))
                {
                    comment = null;
                }
                else
                {
                    comment = comment.Trim();
                    if (string.IsNullOrEmpty(comment))
                    {
                        comment = null;
                    }
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                int rated = selectedRange;
                if (card.Type == (int)AppraisalCardTypeEnum.SelectOne)
                {
                    rated = selectedOption;
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_comment", new List<string>(), new List<string> { "card_id", "user_id" }));
                Row commentRow = mainSession.Execute(ps.Bind(cardId, answeredByUserId)).FirstOrDefault();
                if (commentRow != null)
                {
                    DateTime createdTimestamp = commentRow.GetValue<DateTime>("created_on_timestamp");
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_card_comment_by_timestamp", new List<string> { "card_id", "user_id", "created_on_timestamp" }));
                    deleteBatch.Add(ps.Bind(cardId, answeredByUserId, createdTimestamp));
                }

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_comment", new List<string> { "card_id", "rated", "user_id", "comment", "created_on_timestamp" }));
                updateBatch.Add(ps.Bind(cardId, rated, answeredByUserId, comment, currentDate));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_comment_by_timestamp", new List<string> { "card_id", "rated", "user_id", "comment", "created_on_timestamp" }));
                updateBatch.Add(ps.Bind(cardId, rated, answeredByUserId, comment, currentDate));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_comment_by_user", new List<string> { "card_id", "rated", "user_id", "comment", "created_on_timestamp" }));
                updateBatch.Add(ps.Bind(cardId, rated, answeredByUserId, comment, currentDate));

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private AppraisalValidationResponse ValidateCreationCardFields(string cardContent, bool isCheckForCategory, string categoryTitle = null)
        {
            AppraisalValidationResponse response = new AppraisalValidationResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(cardContent))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCardContent);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCardContent;
                    return response;
                }
                else
                {
                    cardContent = cardContent.Trim();
                    if (string.IsNullOrEmpty(cardContent))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCardContent);
                        response.ErrorMessage = ErrorMessage.AppraisalInvalidCardContent;
                        return response;
                    }
                }

                // User defined cards
                if (isCheckForCategory)
                {
                    if (string.IsNullOrEmpty(categoryTitle))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCategoryMissingTitle);
                        response.ErrorMessage = ErrorMessage.AppraisalCategoryMissingTitle;
                        return response;
                    }
                    else
                    {
                        categoryTitle = categoryTitle.Trim();
                        if (string.IsNullOrEmpty(categoryTitle))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalCategoryMissingTitle);
                            response.ErrorMessage = ErrorMessage.AppraisalCategoryMissingTitle;
                            return response;
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalCardCreateStatementResponse CreateTemplateCard(int type,
                                                                        string content,
                                                                        string categoryId,
                                                                        string creatorUserId,
                                                                        string companyId,
                                                                        bool isCreatedByAdmin,
                                                                        List<AppraisalCardOption> options,
                                                                        DateTime currentTime,
                                                                        ISession mainSession)
        {
            AppraisalCardCreateStatementResponse response = new AppraisalCardCreateStatementResponse();
            response.NewCardId = string.Empty;
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                // Check content
                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Invalid content");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalInvalidCardContent);
                    response.ErrorMessage = ErrorMessage.AppraisalInvalidCardContent;
                    return response;
                }

                bool isCommentAllowed = false;

                string cardId = UUIDGenerator.GenerateUniqueIDForAppraisalCard();

                if (type == (int)AppraisalCardTypeEnum.SelectOne)
                {
                    if (!isCreatedByAdmin)
                    {
                        Log.Error("BARS can only be created by admin");
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AppraisalBarsCanOnlyBeCreatedByAdmin);
                        response.ErrorMessage = ErrorMessage.AppraisalBarsCanOnlyBeCreatedByAdmin;
                        return response;
                    }
                }
                else
                {
                    isCommentAllowed = true;
                }

                if (options != null && options.Count > 0)
                {
                    AppraisalOptionCreateStatementResponse createOptionResponse = new AppraisalCardOption().CreateOptions(cardId, options, mainSession);
                    if (!createOptionResponse.Success)
                    {
                        response.ErrorCode = createOptionResponse.ErrorCode;
                        response.ErrorMessage = createOptionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in createOptionResponse.UpdateStatements)
                    {
                        response.UpdateStatements.Add(bs);
                    }
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.CountStatement("template_appraisal_360_card_by_ordering", new List<string> { "category_id" }));
                int ordering = (int)mainSession.Execute(ps.Bind(categoryId)).FirstOrDefault().GetValue<long>("count") + 1;

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360_card",
                  new List<string> { "card_id", "category_id", "ordering", "type", "content", "is_comment_allowed",
                        "is_created_by_admin", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                response.UpdateStatements.Add(ps.Bind(cardId, categoryId, ordering, type, content, isCommentAllowed, isCreatedByAdmin, creatorUserId, currentTime, creatorUserId, currentTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360_card_by_ordering",
                    new List<string> { "card_id", "category_id", "ordering", "type", "content", "is_comment_allowed",
                        "is_created_by_admin" }));
                response.UpdateStatements.Add(ps.Bind(cardId, categoryId, ordering, type, content, isCommentAllowed, isCreatedByAdmin));

                response.NewCardId = cardId;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private void SelectComments(AppraisalCard card, ISession mainSession, string commentedByUserId = null)
        {
            try
            {
                PreparedStatement ps = new PreparedStatement();
                card.Comments = new List<AppraisalComment>();
                if (!string.IsNullOrEmpty(commentedByUserId))
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_comment", new List<string>(), new List<string> { "card_id", "user_id" }));
                    Row commentRow = mainSession.Execute(ps.Bind(card.CardId, commentedByUserId)).FirstOrDefault();
                    if (commentRow != null)
                    {
                        string comment = commentRow.GetValue<string>("comment") == null ? string.Empty : commentRow.GetValue<string>("comment");
                        int rated = commentRow.GetValue<int>("rated");
                        card.Comments.Add(new AppraisalComment
                        {
                            Content = comment,
                            Rated = rated
                        });
                    }
                }
                else
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_comment", new List<string>(), new List<string> { "card_id" }));
                    RowSet commentRowSet = mainSession.Execute(ps.Bind(card.CardId));
                    foreach (Row commentRow in commentRowSet)
                    {
                        string comment = commentRow.GetValue<string>("comment") == null ? string.Empty : commentRow.GetValue<string>("comment");
                        string userId = commentRow.GetValue<string>("user_id");
                        DateTime createdOnTimestamp = commentRow.GetValue<DateTime>("created_on_timestamp");
                        int rated = commentRow.GetValue<int>("rated");
                        card.Comments.Add(new AppraisalComment
                        {
                            Content = comment,
                            CommentedByUserId = userId,
                            Rated = rated,
                            CreatedOnTimestamp = createdOnTimestamp
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private AppraisalUpdateCardStatementResponse UpdateCard(string updatedByUserId, string categoryId, string cardId, int updatedCardType, string updatedCardContent, List<AppraisalCardOption> updatedOptions, Row currentCardRow, DateTime currentTime, ISession mainSession)
        {
            AppraisalUpdateCardStatementResponse response = new AppraisalUpdateCardStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int cardOrdering = currentCardRow.GetValue<int>("ordering");
                int currentCardType = currentCardRow.GetValue<int>("type");
                bool isCommentAllowed = currentCardRow.GetValue<bool>("is_comment_allowed");
                bool isCreatedByAdmin = currentCardRow.GetValue<bool>("is_created_by_admin");

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360_card", new List<string> { "category_id", "card_id" }, new List<string> { "content", "type", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(updatedCardContent, updatedCardType, updatedByUserId, currentTime, categoryId, cardId));

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360_card_by_ordering", new List<string> { "category_id", "ordering", "card_id" }, new List<string> { "content", "type" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(updatedCardContent, currentCardType, categoryId, cardOrdering, cardId));

                if (isCreatedByAdmin)
                {
                    AppraisalCardOption optionManager = new AppraisalCardOption();
                    if (currentCardType == (int)AppraisalCardTypeEnum.SelectOne)
                    {
                        response.DeleteStatements.AddRange(optionManager.RemoveOptions(cardId, mainSession).DeleteStatements);
                    }

                    if (updatedCardType == (int)AppraisalCardTypeEnum.SelectOne)
                    {
                        response.DeleteStatements.AddRange(optionManager.CreateOptions(cardId, updatedOptions, mainSession).UpdateStatements);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AppraisalUpdateCardStatementResponse RemoveCard(string updatedByUserId, string categoryId, string cardId, Row currentCardRow, DateTime currentTime, ISession mainSession)
        {
            AppraisalUpdateCardStatementResponse response = new AppraisalUpdateCardStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int cardOrdering = currentCardRow.GetValue<int>("ordering");
                int cardType = currentCardRow.GetValue<int>("type");
                bool isCommentAllowed = currentCardRow.GetValue<bool>("is_comment_allowed");
                bool isCreatedByAdmin = currentCardRow.GetValue<bool>("is_created_by_admin");

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("template_appraisal_360_card", new List<string> { "category_id", "card_id" }));
                response.DeleteStatements.Add(ps.Bind(categoryId, cardId));

                ps = mainSession.Prepare(CQLGenerator.SelectStatementWithComparison("template_appraisal_360_card_by_ordering", new List<string>(), new List<string> { "category_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                RowSet orderingRowSet = mainSession.Execute(ps.Bind(categoryId, cardOrdering));
                foreach (Row orderingRow in orderingRowSet)
                {
                    int ordering = orderingRow.GetValue<int>("ordering");
                    string orderingCardId = orderingRow.GetValue<string>("card_id");
                    int orderingCardType = orderingRow.GetValue<int>("type");
                    string orderingContent = orderingRow.GetValue<string>("content");
                    bool orderingIsCommentAllowed = orderingRow.GetValue<bool>("is_comment_allowed");
                    bool orderingIsCreatedByAdmin = orderingRow.GetValue<bool>("is_created_by_admin");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("template_appraisal_360_card_by_ordering", new List<string> { "category_id", "ordering", "card_id" }));
                    response.DeleteStatements.Add(ps.Bind(categoryId, ordering, orderingCardId));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("template_appraisal_360_card_by_ordering", new List<string> { "card_id", "category_id", "ordering", "type", "content", "is_comment_allowed", "is_created_by_admin" }));
                    response.UpdateStatements.Add(ps.Bind(orderingCardId, categoryId, ordering - 1, orderingCardType, orderingContent, orderingIsCommentAllowed, orderingIsCreatedByAdmin));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("template_appraisal_360_card", new List<string> { "category_id", "card_id" }, new List<string> { "ordering", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    response.UpdateStatements.Add(ps.Bind(ordering - 1, updatedByUserId, currentTime, categoryId, orderingCardId));
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("template_appraisal_360_card_by_ordering", new List<string> { "category_id", "ordering", "card_id" }));
                response.DeleteStatements.Add(ps.Bind(categoryId, cardOrdering, cardId));

                if (cardType == (int)AppraisalCardTypeEnum.SelectOne && isCreatedByAdmin)
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("template_appraisal_360_card_option", new List<string> { "card_id" }));
                    response.DeleteStatements.Add(ps.Bind(cardId));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}
