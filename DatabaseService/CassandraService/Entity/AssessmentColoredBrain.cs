﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class AssessmentColoredBrain
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        private static Assessment assessmentManager = new Assessment();

        public AssessmentSelectColorBrainPersonalityResponse SelectColorBrainPersonalityByUser(string requesterUserId, string targetedUserId, string companyId, ISession mainSession, ISession analyticSession)
        {
            AssessmentSelectColorBrainPersonalityResponse response = new AssessmentSelectColorBrainPersonalityResponse();
            response.ColorBrainAssessment = null;
            response.ColorBrainPersonalityResult = null;
            response.ColoredBrainAboutUser = null;
            response.ColoredBrainFrameColor = null;
            response.ColoredBrainCode = DefaultResource.AssessmentNotCompleteCode;
            response.Success = false;
            try
            {
                AssessmentSelectAllResponse selectColorBrainResponse = assessmentManager.SelectAllAssessmentByUser(targetedUserId, companyId, mainSession, analyticSession, true);
                if (!selectColorBrainResponse.Success)
                {
                    response.ErrorCode = selectColorBrainResponse.ErrorCode;
                    response.ErrorMessage = selectColorBrainResponse.ErrorMessage;
                    return response;
                }

                if (selectColorBrainResponse.Assessments.Count >= 1)
                {
                    Assessment selectedColorBrainAssessment = selectColorBrainResponse.Assessments[0];

                    AssessmentSelectColorBrainPersonalityResponse targetedPersonality = SelectColorBrainPersonalityResult(targetedUserId, companyId, selectedColorBrainAssessment.AssessmentId, mainSession, analyticSession);

                    if (targetedPersonality.Success)
                    {
                        AssessmentAnswer colorBrainResult = targetedPersonality.ColorBrainPersonalityResult;
                        AssessmentTabulation colorBrainTab = colorBrainResult.Tabulations[0];
                        bool isShownResult = false;
                        if (requesterUserId.Equals(targetedUserId))
                        {
                            if (colorBrainResult.VisibleToSelf)
                            {
                                isShownResult = true;
                            }
                        }
                        else
                        {
                            if (colorBrainResult.VisibleToOthers)
                            {
                                if (!(colorBrainResult.VisibleToOthersForCompletedUser && colorBrainResult.VisibleToOthersForIncompletedUser))
                                {
                                    AssessmentSelectColorBrainPersonalityResponse requesterPersonality = SelectColorBrainPersonalityResult(requesterUserId, companyId, selectedColorBrainAssessment.AssessmentId, mainSession, analyticSession);
                                    if (colorBrainResult.VisibleToOthersForCompletedUser && !requesterPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentNotCompleteCode))
                                    {
                                        isShownResult = true;
                                    }
                                    else if (colorBrainResult.VisibleToOthersForIncompletedUser && requesterPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentNotCompleteCode))
                                    {
                                        isShownResult = true;
                                    }
                                }
                                else
                                {
                                    isShownResult = true;
                                }
                            }
                        }

                        if (isShownResult)
                        {
                            response.ColorBrainAssessment = selectedColorBrainAssessment;
                            response.ColorBrainPersonalityResult = colorBrainResult;
                            response.ColoredBrainAboutUser = targetedPersonality.ColoredBrainAboutUser;
                            response.ColoredBrainFrameColor = targetedPersonality.ColoredBrainFrameColor;
                            response.ColoredBrainCode = targetedPersonality.ColoredBrainCode;
                            response.Success = true;
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectResultByUserResponse SelectColorBrainResultByUser(string requesterUserId, string targetedUserId, string companyId, string assessmentId)
        {
            AssessmentSelectResultByUserResponse response = new AssessmentSelectResultByUserResponse();
            response.Results = new List<AssessmentAnswer>();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row subscriptionRow = vh.ValidateAssessmentSubscription(assessmentId, companyId, mainSession);
                if (subscriptionRow == null)
                {
                    Log.Error("Invalid subscription");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.SubscriptionInvalid);
                    response.ErrorMessage = ErrorMessage.SubscriptionInvalid;
                    return response;
                }

                Row assessmentRow = vh.ValidateAssessment(assessmentId, mainSession);
                if (assessmentRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }
                else
                {
                    if (!assessmentRow.GetValue<string>("title").ToLower().Equals(DefaultResource.AssessmentColoredBrainTitle.ToLower()))
                    {
                        Log.Error("Invalid assessmentId: " + assessmentId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                        response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                        return response;
                    }
                }

                AssessmentSelectResultByUserResponse assessmentResult = new Assessment().SelectAssessmentResultByUser(targetedUserId, companyId, assessmentId, mainSession, analyticSession);

                if (requesterUserId.Equals(targetedUserId))
                {
                    response = assessmentResult;
                }
                else
                {
                    AssessmentSelectColorBrainPersonalityResponse targetedPersonality = SelectColorBrainPersonalityResult(targetedUserId, companyId, assessmentId, mainSession, analyticSession);
                    AssessmentSelectColorBrainPersonalityResponse requesterPersonality = SelectColorBrainPersonalityResult(requesterUserId, companyId, assessmentId, mainSession, analyticSession);

                    if (targetedPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentNotCompleteCode))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentColorBrainResultNotApplicable);
                        response.ErrorMessage = ErrorMessage.AssessmentColorBrainResultNotApplicable;
                        return response;
                    }
                    else
                    {
                        User targetUser = new User().SelectUserBasic(targetedUserId, companyId, true, mainSession).User;
                        string targetFullName = string.Format("{0} {1}", targetUser.FirstName, targetUser.LastName);
                        response.Results = new List<AssessmentAnswer>();
                        List<AssessmentImage> images = assessmentResult.Results[0].Tabulations[0].Images;

                        string aboutTitle = string.Empty;
                        if (targetedPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentGreenBrainCode))
                        {
                            aboutTitle = string.Format("{0} is a Green Brain", targetFullName);
                        }
                        else if (targetedPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentBlueBrainCode))
                        {
                            aboutTitle = string.Format("{0} is a Blue Brain", targetFullName);
                        }
                        else if (targetedPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentRedBrainCode))
                        {
                            aboutTitle = string.Format("{0} is a Red Brain", targetFullName);
                        }
                        else if (targetedPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentPurpleBrainCode))
                        {
                            aboutTitle = string.Format("{0} is a Purple Brain", targetFullName);
                        }

                        string aboutTargetDescription = string.Empty;
                        string aboutTargetHtmlDescription = string.Empty;
                        PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_keyword_tabulation_description", new List<string>(), new List<string> { "assessment_keyword", "description_keyword" }));
                        Row aboutTargetDescriptionRow = mainSession.Execute(ps.Bind(DefaultResource.AssessmentColoredBrainKey, string.Format("#A{0}", targetedPersonality.ColoredBrainCode))).FirstOrDefault();
                        if (aboutTargetDescriptionRow != null)
                        {
                            aboutTargetDescription = aboutTargetDescriptionRow.GetValue<string>("description");
                            aboutTargetHtmlDescription = aboutTargetDescriptionRow.GetValue<string>("html_description");
                        }

                        AssessmentAnswer aboutTarget = new AssessmentAnswer
                        {
                            Title = aboutTitle,
                            Tabulations = new List<AssessmentTabulation> {
                                new AssessmentTabulation
                                {
                                    Images = images,
                                    InfoTitle = aboutTitle,
                                    InfoDescription = aboutTargetDescription,
                                    InfoHtmlDescription = aboutTargetHtmlDescription
                                }
                            }
                        };


                        string workingWithTargetTitle = string.Format("Working with {0}", targetFullName);
                        string workingWithTargetDescription = string.Empty;
                        string workingWithTargetHtmlDescription = string.Empty;
                        if (!requesterPersonality.ColoredBrainCode.Equals(DefaultResource.AssessmentNotCompleteCode))
                        {
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_keyword_tabulation_description", new List<string>(), new List<string> { "assessment_keyword", "description_keyword" }));
                            Row workingWithTargetRow = mainSession.Execute(ps.Bind(DefaultResource.AssessmentColoredBrainKey, string.Format("#{0}{1}", requesterPersonality.ColoredBrainCode, targetedPersonality.ColoredBrainCode))).FirstOrDefault();
                            if (workingWithTargetRow != null)
                            {
                                workingWithTargetDescription = workingWithTargetRow.GetValue<string>("description");
                                workingWithTargetHtmlDescription = workingWithTargetRow.GetValue<string>("html_description");
                            }
                        }
                        else
                        {
                            workingWithTargetDescription = string.Format(@"To know how you can work with {0}, please complete your 'Colored Brain' Assessment", targetFullName);
                            workingWithTargetHtmlDescription = workingWithTargetDescription;
                        }


                        AssessmentAnswer workingWithTarget = new AssessmentAnswer
                        {
                            Title = workingWithTargetTitle,
                            Tabulations = new List<AssessmentTabulation> {
                                new AssessmentTabulation
                                {
                                    InfoTitle = workingWithTargetTitle,
                                    InfoDescription = workingWithTargetDescription,
                                    InfoHtmlDescription = workingWithTargetHtmlDescription
                                }
                            }
                        };

                        response.Results.Add(aboutTarget);
                        response.Results.Add(workingWithTarget);
                        response.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private AssessmentSelectColorBrainPersonalityResponse SelectColorBrainPersonalityResult(string requesterUserId, string companyId, string assessmentId, ISession mainSession, ISession analyticSession)
        {
            AssessmentSelectColorBrainPersonalityResponse response = new AssessmentSelectColorBrainPersonalityResponse();
            response.ColorBrainPersonalityResult = null;
            response.ColoredBrainAboutUser = null;
            response.ColoredBrainFrameColor = null;
            response.ColoredBrainCode = DefaultResource.AssessmentNotCompleteCode;
            response.Success = false;
            try
            {
                AssessmentSelectResultByUserResponse resultResponse = assessmentManager.SelectAssessmentResultByUser(requesterUserId, companyId, assessmentId, mainSession, analyticSession);
                if (resultResponse.Success)
                {
                    AssessmentAnswer colorBrainResult = resultResponse.Results[0];
                    AssessmentTabulation colorBrainTab = colorBrainResult.Tabulations[0];

                    if (colorBrainTab.InfoTitle.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutGreen.ToLower()))
                    {
                        response.ColoredBrainFrameColor = DefaultResource.AssessmentColoredBrainTabGreenColor;
                        response.ColoredBrainCode = DefaultResource.AssessmentGreenBrainCode;
                    }
                    else if (colorBrainTab.InfoTitle.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutBlue.ToLower()))
                    {
                        response.ColoredBrainFrameColor = DefaultResource.AssessmentColoredBrainTabBlueColor;
                        response.ColoredBrainCode = DefaultResource.AssessmentBlueBrainCode;
                    }
                    else if (colorBrainTab.InfoTitle.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutRed.ToLower()))
                    {
                        response.ColoredBrainFrameColor = DefaultResource.AssessmentColoredBrainTabRedColor;
                        response.ColoredBrainCode = DefaultResource.AssessmentRedBrainCode;
                    }
                    else if (colorBrainTab.InfoTitle.ToLower().Contains(DefaultResource.AssessmentColoredBrainTabAboutPurple.ToLower()))
                    {
                        response.ColoredBrainFrameColor = DefaultResource.AssessmentColoredBrainTabPurpleColor;
                        response.ColoredBrainCode = DefaultResource.AssessmentPurpleBrainCode;
                    }

                    if (!response.ColoredBrainCode.Equals(DefaultResource.AssessmentNotCompleteCode))
                    {
                        PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_keyword_tabulation_description", new List<string>(), new List<string> { "assessment_keyword", "description_keyword" }));
                        Row aboutTargetDescriptionRow = mainSession.Execute(ps.Bind(DefaultResource.AssessmentColoredBrainKey, string.Format("#{0}PROFILE", response.ColoredBrainCode))).FirstOrDefault();
                        if (aboutTargetDescriptionRow != null)
                        {
                            response.ColoredBrainAboutUser = aboutTargetDescriptionRow.GetValue<string>("description");
                        }
                    }

                    if (response.ColoredBrainAboutUser == null || response.ColoredBrainFrameColor == null)
                    {
                        response.ColoredBrainAboutUser = null;
                        response.ColoredBrainFrameColor = null;
                        response.ColoredBrainCode = DefaultResource.AssessmentNotCompleteCode;
                    }

                    response.ColorBrainPersonalityResult = colorBrainResult;
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

    }

}