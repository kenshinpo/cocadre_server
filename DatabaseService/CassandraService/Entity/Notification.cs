﻿using Cassandra;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;
using CassandraService.Utilities;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Validation;
using System.Web.Configuration;
using static CassandraService.Entity.Gamification;
using System.Threading;
using static CassandraService.Entity.Pulse;

namespace CassandraService.Entity
{
    public class Notification
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedFeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedCommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedAchievementId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedPulseId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedAppraisalId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedReportId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NotificationText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Type { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SubType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User TaggedUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedUserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsSeen { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NumberOfNotificationForTargetedUser { get; set; }

        [Serializable]
        public enum NotificationType
        {
            [EnumMember]
            Feed = 1,

            [EnumMember]
            Challenge = 2,

            [EnumMember]
            Pulse = 3,

            [EnumMember]
            Report = 4,
        }

        [Serializable]
        public enum NotificationFeedSubType
        {
            [EnumMember]
            NewPostToUser = 1,

            [EnumMember]
            NewComment = 2,

            [EnumMember]
            NewReply = 3,

            [EnumMember]
            UpvotePost = 4,

            [EnumMember]
            UpvoteComment = 5,

            [EnumMember]
            UpvoteReply = 6,

            [EnumMember]
            MentionedInComment = 7,

            [EnumMember]
            MentionedInReply = 8,

            [EnumMember]
            MentionedInPost = 9,

            [EnumMember]
            NewPostToDepartment = 10,
        }

        [Serializable]
        public enum NotificationGameSubType
        {
            [EnumMember]
            NewChallenge = 1,

            [EnumMember]
            ChallengedUserCompletedChallenge = 2,

            [EnumMember]
            AchievementUnlocked = 3,
        }

        [Serializable]
        public enum NotificationPulseSubType
        {
            [EnumMember]
            Announcement = 1,

            [EnumMember]
            Dynamic = 2,

            [EnumMember]
            Live360 = 3
        }

        [Serializable]
        public enum NotificationReportSubType
        {
            [EnumMember]
            Live360End = 1,

            [EnumMember]
            Live360Completed = 2
        }

        [Serializable]
        public class Schedule
        {
            [DataMember]
            public string ScheduledId { get; set; }
            [DataMember]
            public string CompanyId { get; set; }
            [DataMember]
            public string GroupName { get; set; }
            [DataMember]
            public DateTime ScheduledOnTimestamp { get; set; }
        }

        public string CreateFeedNotification(string fromUserId,
                                             string companyId,
                                             string targetedUserId,
                                             int subType,
                                             int feedType,
                                             string feedId,
                                             string commentId,
                                             string replyId,
                                             DateTime createdDateTime,
                                             ISession mainSession)
        {
            string pushNotificationText = string.Empty;
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                Row notificationRow = null;

                string notificationId = string.Empty;

                if (string.IsNullOrEmpty(replyId))
                {
                    replyId = "NA";
                }

                if (string.IsNullOrEmpty(commentId))
                {
                    commentId = "NA";
                }

                int type = (int)NotificationType.Feed;

                // Grouping
                if (subType == (int)NotificationFeedSubType.NewComment ||
                    subType == (int)NotificationFeedSubType.NewReply ||
                    subType == (int)NotificationFeedSubType.UpvotePost ||
                    subType == (int)NotificationFeedSubType.UpvoteComment ||
                    subType == (int)NotificationFeedSubType.UpvoteReply)
                {

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_feed_grouping",
                        new List<string>(), new List<string> { "feed_id", "comment_id", "reply_id", "notification_subtype", "targeted_user_id" }));
                    notificationRow = mainSession.Execute(ps.Bind(feedId, commentId, replyId, subType, targetedUserId)).FirstOrDefault();

                    if (notificationRow != null)
                    {
                        DateTimeOffset createdOnTimestamp = notificationRow.GetValue<DateTimeOffset>("notification_created_on_timestamp");
                        notificationId = notificationRow.GetValue<string>("notification_id");

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_user",
                            new List<string> { "targeted_user_id", "notification_id" }, new List<string> { "notification_created_on_timestamp", "is_seen", "is_fetched" }, new List<string>()));
                        updateBatch.Add(ps.Bind(createdDateTime, false, false, targetedUserId, notificationId));

                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_feed_grouping",
                            new List<string> { "feed_id", "comment_id", "reply_id", "notification_subtype", "targeted_user_id", "notification_id" }, new List<string> { "notification_created_on_timestamp", "is_seen", "is_fetched" }, new List<string>()));
                        updateBatch.Add(ps.Bind(createdDateTime, false, false, feedId, commentId, replyId, subType, targetedUserId, notificationId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_sorted_by_user_timestamp",
                            new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                        deleteBatch.Add(ps.Bind(targetedUserId, createdOnTimestamp, notificationId));
                    }
                    else
                    {
                        if (subType == (int)NotificationFeedSubType.NewComment)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedNewCommentNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.NewReply)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedNewReplyNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.UpvotePost)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedUpvotePostNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.UpvoteComment)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedUpvoteCommentNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.UpvoteReply)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedUpvoteReplyNotification();
                        }

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        updateBatch.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, false, false, type, subType, createdDateTime));
                    }
                }
                else
                {
                    if (subType == (int)NotificationFeedSubType.MentionedInComment ||
                        subType == (int)NotificationFeedSubType.MentionedInReply)
                    {
                        if (subType == (int)NotificationFeedSubType.MentionedInComment)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedMentionedCommentNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.MentionedInReply)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedMentionedReplyNotification();
                        }
                    }
                    else if (subType == (int)NotificationFeedSubType.NewPostToUser ||
                             subType == (int)NotificationFeedSubType.NewPostToDepartment ||
                             subType == (int)NotificationFeedSubType.MentionedInPost)
                    {
                        notificationId = UUIDGenerator.GenerateUniqueIDForFeedNewPostNotification();
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                           new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    updateBatch.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, false, false, type, subType, createdDateTime));
                }

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_feed_grouping",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, false, false, type, subType, createdDateTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, false, false, type, subType, createdDateTime));

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                pushNotificationText = GenerateFeedPushNotificationText(fromUserId, companyId, feedId, feedType, subType, mainSession);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return pushNotificationText;
        }

        private string GenerateFeedPushNotificationText(string fromUserId, string companyId, string feedId, int feedType, int subType, ISession mainSession)
        {
            string pushNotificationText = string.Empty;
            try
            {
                User fromUser = new User().SelectUserBasic(fromUserId, companyId, false, mainSession).User;
                string fromUserFullName = string.Format("{0} {1}", fromUser.FirstName, fromUser.LastName);

                if (subType == (int)NotificationFeedSubType.NewPostToUser || subType == (int)NotificationFeedSubType.NewPostToDepartment || subType == (int)NotificationFeedSubType.MentionedInPost)
                {
                    pushNotificationText = GetNotificationTextForFeedCreation(feedId, companyId, feedType, subType, fromUserFullName, mainSession);
                }
                if (subType == (int)NotificationFeedSubType.NewComment)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationCommentOneOnMyPost, fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.NewReply)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationReplyOneOnMyComment, fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.UpvotePost)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationUpvotedPostNoTitle, fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.UpvoteComment)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationUpvotedCommentNoTitle, fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.UpvoteReply)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationUpvotedReplyNoTitle, fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.MentionedInComment)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationMentionedInComment, fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.MentionedInReply)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationMentionedInReply, fromUserFullName);
                }

                pushNotificationText = pushNotificationText.Replace("<b>", "");
                pushNotificationText = pushNotificationText.Replace("</b>", "");
            }
            catch (Exception ex)
            {

                Log.Error(ex.ToString());
            }

            return pushNotificationText;
        }

        public string CreateChallengeNotification(string fromUserId,
                                                  string targetedUserId,
                                                  string companyId,
                                                  int subType,
                                                  string challengeId,
                                                  string topicTitle,
                                                  DateTime createdDateTime,
                                                  ISession mainSession)
        {
            string pushNotificationText = string.Empty;
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();

                string notificationId = string.Empty;

                if (subType == (int)NotificationGameSubType.NewChallenge)
                {
                    notificationId = UUIDGenerator.GenerateUniqueIDForChallengeRequestNotification();
                }
                else if (subType == (int)NotificationGameSubType.ChallengedUserCompletedChallenge)
                {
                    notificationId = UUIDGenerator.GenerateUniqueIDForChallengeCompletedNotification();
                }

                int type = (int)NotificationType.Challenge;

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                       new List<string> { "notification_id", "targeted_user_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, challengeId, false, false, type, subType, createdDateTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_challenge",
                    new List<string> { "notification_id", "targeted_user_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, challengeId, false, false, type, subType, createdDateTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                       new List<string> { "notification_id", "targeted_user_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, challengeId, false, false, type, subType, createdDateTime));

                mainSession.Execute(updateBatch);

                pushNotificationText = GenerateChallengePushNotificationText(fromUserId, companyId, topicTitle, subType, mainSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return pushNotificationText;
        }

        public string GenerateChallengePushNotificationText(string fromUserId, string companyId, string topicTitle, int subType, ISession mainSession)
        {
            string pushNotificationText = string.Empty;
            try
            {
                User fromUser = new User().SelectUserBasic(fromUserId, companyId, false, mainSession).User;
                string fromUserFullName = string.Format("{0} {1}", fromUser.FirstName, fromUser.LastName);

                if (subType == (int)NotificationGameSubType.ChallengedUserCompletedChallenge)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationChallengeCompleted, fromUserFullName, topicTitle);
                }
                else if (subType == (int)NotificationGameSubType.NewChallenge)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationChallengeNew, fromUserFullName, topicTitle);
                }

                pushNotificationText = pushNotificationText.Replace("<b>", "");
                pushNotificationText = pushNotificationText.Replace("</b>", "");
            }
            catch (Exception ex)
            {

                Log.Error(ex.ToString());
            }

            return pushNotificationText;
        }

        public string CreateAchievementNotification(string targetedUserId,
                                                    string achievementId,
                                                    DateTime createdDateTime,
                                                    ISession mainSession)
        {
            string pushNotificationText = string.Empty;
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();

                string notificationId = UUIDGenerator.GenerateUniqueIDForAchievementUnlockedNotification();

                int type = (int)NotificationType.Challenge;
                int subType = (int)NotificationGameSubType.AchievementUnlocked;

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                       new List<string> { "notification_id", "targeted_user_id", "achievement_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, achievementId, false, false, type, subType, createdDateTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_achievement",
                    new List<string> { "notification_id", "targeted_user_id", "achievement_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, achievementId, false, false, type, subType, createdDateTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                       new List<string> { "notification_id", "targeted_user_id", "achievement_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, achievementId, false, false, type, subType, createdDateTime));

                mainSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return pushNotificationText;
        }

        public Notification CreateCompletedAppraisalReportNotification(string companyId,
                                                                       string targetedUserId,
                                                                       string appraisalId,
                                                                       string appraisalTitle,
                                                                       DateTime createdDateTime,
                                                                       ISession mainSession)
        {
            Notification notification = new Notification();
            try
            {
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                // Remove from scheduler
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("scheduled_notification_task_by_report", new List<string>(), new List<string> { "report_id", "company_id" }));
                Row scheduledRow = mainSession.Execute(ps.Bind(appraisalId, companyId)).FirstOrDefault();
                if(scheduledRow != null)
                {
                    string scheduledNotificationId = scheduledRow.GetValue<string>("notification_id");
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task_by_report", new List<string> { "report_id", "company_id", "notification_id" }));
                    deleteBatch.Add(ps.Bind(appraisalId, companyId, scheduledNotificationId));
                }

                string notificationId = UUIDGenerator.GenerateUniqueIDForAppraisalReportNotification();

                int type = (int)NotificationType.Report;
                int subType = (int)NotificationReportSubType.Live360Completed;

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                       new List<string> { "notification_id", "targeted_user_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, appraisalId, false, false, type, subType, createdDateTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_report",
                    new List<string> { "notification_id", "targeted_user_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, appraisalId, false, false, type, subType, createdDateTime));

                ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                       new List<string> { "notification_id", "targeted_user_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                updateBatch.Add(ps.Bind(notificationId, targetedUserId, appraisalId, false, false, type, subType, createdDateTime));

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);

                notification = GetReportNotification(companyId, appraisalId, appraisalTitle, targetedUserId, subType, mainSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return notification;
        }

        private string GenerateReportNotificationText(string reportTitle, int subType)
        {
            string pushNotificationText = string.Empty;
            try
            {
                if (subType == (int)NotificationReportSubType.Live360End)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationReportFor360End, reportTitle);
                }
                else if (subType == (int)NotificationReportSubType.Live360Completed)
                {
                    pushNotificationText = string.Format(NotificationMessage.NotificationReportFor360Completed, reportTitle);
                }

            }
            catch (Exception ex)
            {

                Log.Error(ex.ToString());
            }

            return pushNotificationText;
        }

        public NotificationUpdateSeenResponse UpdateSeenNotification(string targetedUserId,
                                                                     string companyId,
                                                                     string notificationId)
        {
            NotificationUpdateSeenResponse response = new NotificationUpdateSeenResponse();
            response.Success = false;

            try
            {

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(targetedUserId, companyId, session);

                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_user",
                    new List<string>(), new List<string> { "targeted_user_id", "notification_id" }));

                Row notificationRow = session.Execute(ps.Bind(targetedUserId, notificationId)).FirstOrDefault();

                if (notificationRow != null)
                {
                    BatchStatement batch = new BatchStatement();

                    ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_user",
                        new List<string> { "targeted_user_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                    batch.Add(ps.Bind(true, targetedUserId, notificationId));

                    int type = notificationRow.GetValue<int>("notification_type");
                    int subType = notificationRow.GetValue<int>("notification_subtype");
                    DateTime? createdTimestamp = notificationRow.GetValue<DateTime?>("notification_created_on_timestamp");

                    if (type == (int)NotificationType.Feed)
                    {
                        string feedId = notificationRow.GetValue<string>("feed_id");
                        string commentId = notificationRow.GetValue<string>("comment_id");
                        string replyId = notificationRow.GetValue<string>("reply_id");

                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_feed_grouping",
                            new List<string> { "feed_id", "comment_id", "reply_id", "notification_subtype", "targeted_user_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batch.Add(ps.Bind(true, feedId, commentId, replyId, subType, targetedUserId, notificationId));
                    }
                    else if (type == (int)NotificationType.Challenge)
                    {
                        string challengeId = notificationRow.GetValue<string>("challenge_id");
                        string achievementId = notificationRow.GetValue<string>("achievement_id");

                        if (subType == (int)NotificationGameSubType.NewChallenge || subType == (int)NotificationGameSubType.ChallengedUserCompletedChallenge)
                        {
                            ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_challenge",
                                new List<string> { "challenge_id", "targeted_user_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                            batch.Add(ps.Bind(true, challengeId, targetedUserId, notificationId));
                        }
                        else
                        {
                            ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_achievement",
                                new List<string> { "achievement_id", "targeted_user_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                            batch.Add(ps.Bind(true, achievementId, targetedUserId, notificationId));
                        }
                    }

                    if (createdTimestamp != null)
                    {
                        ps = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_sorted_by_user_timestamp",
                           new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batch.Add(ps.Bind(true, targetedUserId, createdTimestamp, notificationId));
                    }

                    session.Execute(batch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public NotificationSelectNumberResponse SelectNotificationNumberByUser(string targetedUserId,
                                                                               string companyId,
                                                                               ISession session = null)
        {
            NotificationSelectNumberResponse response = new NotificationSelectNumberResponse();
            response.Success = false;
            response.NumberOfNotification = 0;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.CountStatement("notification_privacy_sorted_by_user_timestamp",
                    new List<string> { "targeted_user_id", "is_fetched" }));
                long number = session.Execute(preparedStatement.Bind(targetedUserId, false)).FirstOrDefault().GetValue<long>("count");
                response.NumberOfNotification = (int)number;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public List<string> SelectTargetedUserIdForFeedNotification(string feedId,
                                                                    string commentId,
                                                                    string replyId,
                                                                    int notificationSubType,
                                                                    ISession session)
        {
            List<string> targetedUserIds = new List<string>();

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_feed_grouping",
                  new List<string>(), new List<string> { "feed_id", "comment_id", "reply_id", "notification_subtype" }));
                RowSet notificationRowset = session.Execute(preparedStatement.Bind(feedId, commentId, replyId, notificationSubType));

                foreach (Row notificationRow in notificationRowset)
                {
                    string userId = notificationRow.GetValue<string>("targeted_user_id");
                    targetedUserIds.Add(userId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return targetedUserIds;
        }

        public NotificationSelectAllResponse SelectNotificationsByUser(string targetedUserId,
                                                                   string companyId,
                                                                   int numberOfNotificationLoaded,
                                                                   DateTime? newestTimestamp,
                                                                   DateTime? oldestTimestamp)
        {
            NotificationSelectAllResponse response = new NotificationSelectAllResponse();
            response.Notifications = new List<Notification>();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["notification_limit"]);

                if (numberOfNotificationLoaded > 0)
                {
                    limit += numberOfNotificationLoaded;
                }


                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(targetedUserId, companyId, mainSession);

                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                PreparedStatement psNotification = null;
                BoundStatement bsNotification = null;

                bool isSelectCompleted = false;

                Gamification gamificationManager = new Gamification();
                PulseSingle pulseSingleManager = new PulseSingle();
                Appraisal appraisalManager = new Appraisal();
                Feed feedManager = new Feed();
                Department departmentManager = new Department();
                User userManager = new User();

                List<string> targetedUserDepartments = departmentManager.GetAllDepartmentByUserId(targetedUserId, companyId, mainSession).Departments.Select(d => d.Id).ToList();

                while (isSelectCompleted == false)
                {
                    if (oldestTimestamp != null)
                    {
                        DateTime currentDateTime = oldestTimestamp.Value;
                        psNotification = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("notification_privacy_sorted_by_user_timestamp",
                            null, new List<string> { "targeted_user_id" }, "notification_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        bsNotification = psNotification.Bind(targetedUserId, DateHelper.ConvertDateToLong(currentDateTime));
                    }
                    else
                    {
                        psNotification = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("notification_privacy_sorted_by_user_timestamp",
                            new List<string>(), new List<string> { "targeted_user_id" }, limit));
                        bsNotification = psNotification.Bind(targetedUserId);
                    }

                    RowSet allNotificationRowSet = mainSession.Execute(bsNotification);
                    List<Row> allNotificationRowList = new List<Row>();
                    allNotificationRowList = allNotificationRowSet.GetRows().ToList();

                    foreach (Row allNotificationRow in allNotificationRowList)
                    {
                        Notification notification = null;

                        string notificationId = allNotificationRow.GetValue<string>("notification_id");

                        // Check for duplicates
                        if (response.Notifications.FirstOrDefault(n => n.Id.Equals(notificationId)) != null)
                        {
                            Thread thread = new Thread(() => RemoveDuplicatedNotification(allNotificationRow, mainSession));
                            thread.Start();
                        }

                        string challengeId = allNotificationRow.GetValue<string>("challenge_id");
                        string achievementId = allNotificationRow.GetValue<string>("achievement_id");
                        string feedId = allNotificationRow.GetValue<string>("feed_id");
                        string commentId = allNotificationRow.GetValue<string>("comment_id");
                        string replyId = allNotificationRow.GetValue<string>("reply_id");
                        int? feedType = allNotificationRow.GetValue<int?>("feed_type");
                        int? pulseType = allNotificationRow.GetValue<int?>("pulse_type");
                        string pulseId = allNotificationRow.GetValue<string>("pulse_id");
                        string appraisalId = allNotificationRow.GetValue<string>("appraisal_id");
                        string reportId = allNotificationRow.GetValue<string>("report_id");
                        bool isSeen = allNotificationRow.GetValue<bool>("is_seen");
                        bool isFetched = allNotificationRow.GetValue<bool>("is_fetched");
                        int notificationSubType = allNotificationRow.GetValue<int>("notification_subtype");
                        int notificationType = allNotificationRow.GetValue<int>("notification_type");
                        DateTime createdOnTimestamp = allNotificationRow.GetValue<DateTime>("notification_created_on_timestamp");

                        string content = string.Empty;

                        oldestTimestamp = createdOnTimestamp;

                        // Update fetch
                        if (!isFetched)
                        {
                            Thread thread = new Thread(() => UpdateFetchedNotification(allNotificationRow, mainSession));
                            thread.Start();
                        }

                        if (notificationType == (int)NotificationType.Feed)
                        {
                            Row feedRow = vh.ValidateFeedPost(feedId, companyId, mainSession);

                            if (feedRow == null)
                            {
                                continue;
                            }

                            string ownerUserId = feedRow.GetValue<string>("owner_user_id");

                            User ownerUser = null;

                            bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                            bool isForUser = feedRow.GetValue<bool>("is_for_user");
                            bool isTaggedForUser = feedRow.GetValue<bool>("is_tagged_for_user");
                            bool isForGroup = feedRow.GetValue<bool>("is_for_group");
                            bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                            if (!ownerUserId.Equals(targetedUserId))
                            {
                                if (!feedManager.CheckPostForCurrentUser(targetedUserId, feedId, ownerUserId, isForEveryone, isForDepartment, isForUser, isTaggedForUser, isForGroup, targetedUserDepartments, mainSession))
                                {
                                    continue;
                                }
                            }

                            if (notificationSubType == (int)NotificationFeedSubType.NewPostToUser || notificationSubType == (int)NotificationFeedSubType.NewPostToDepartment || notificationSubType == (int)NotificationFeedSubType.MentionedInPost)
                            {
                                ownerUser = userManager.SelectUserBasic(ownerUserId, companyId, false, mainSession).User;
                                string ownerFullName = string.Format("{0} {1}", ownerUser.FirstName, ownerUser.LastName);

                                content = GetNotificationTextForFeedCreation(feedId, companyId, feedType.Value, notificationSubType, ownerFullName, mainSession);
                            }
                            else if (notificationSubType == (int)NotificationFeedSubType.NewComment || notificationSubType == (int)NotificationFeedSubType.NewReply)
                            {
                                PreparedStatement psCommentByTimestamp = null;
                                RowSet commentByTimestampRowSet = null;

                                if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                {
                                    psCommentByTimestamp = mainSession.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc",
                                        new List<string>(), new List<string> { "feed_id", "is_comment_valid" }));
                                    commentByTimestampRowSet = mainSession.Execute(psCommentByTimestamp.Bind(feedId, true));
                                }
                                else
                                {
                                    psCommentByTimestamp = mainSession.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc",
                                        new List<string>(), new List<string> { "comment_id", "is_reply_valid" }));
                                    commentByTimestampRowSet = mainSession.Execute(psCommentByTimestamp.Bind(commentId, true));
                                }


                                int numberOfCommentors = 0;
                                string lastCommentorUserId = string.Empty;

                                List<string> commentorUserIds = new List<string>();
                                foreach (Row commentByTimestampRow in commentByTimestampRowSet)
                                {
                                    string currentUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                                    if (!currentUserId.Equals(ownerUserId))
                                    {
                                        lastCommentorUserId = currentUserId;

                                        if (!commentorUserIds.Contains(currentUserId))
                                        {
                                            commentorUserIds.Add(currentUserId);
                                            numberOfCommentors++;
                                        }

                                    }
                                }

                                numberOfCommentors--;

                                if (numberOfCommentors < 0)
                                {
                                    continue;
                                }

                                ownerUser = userManager.SelectUserBasic(lastCommentorUserId, companyId, false, mainSession).User;
                                string creatorFullName = string.Format("{0} {1}", ownerUser.FirstName, ownerUser.LastName);

                                if (isPostedByAdmin)
                                {
                                    if (numberOfCommentors >= 1)
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentManyOnAdminPost, creatorFullName, numberOfCommentors);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyManyOnAdminComment, creatorFullName, numberOfCommentors);
                                        }

                                    }
                                    else
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentOneOnAdminPost, creatorFullName);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyOneOnAdminComment, creatorFullName);
                                        }

                                    }
                                }
                                else
                                {
                                    if (numberOfCommentors >= 1)
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentManyOnMyPost, creatorFullName, numberOfCommentors);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyManyOnMyComment, creatorFullName, numberOfCommentors);
                                        }

                                    }
                                    else
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentOneOnMyPost, creatorFullName);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyOneOnMyComment, creatorFullName);
                                        }

                                    }
                                }

                            }
                            else if (notificationSubType == (int)NotificationFeedSubType.MentionedInComment || notificationSubType == (int)NotificationFeedSubType.MentionedInReply)
                            {
                                PreparedStatement psCommentByTimestamp = null;
                                Row mentionRow = null;

                                if (notificationSubType == (int)NotificationFeedSubType.MentionedInComment)
                                {
                                    psCommentByTimestamp = mainSession.Prepare(CQLGenerator.SelectStatement("comment_by_feed",
                                        new List<string>(), new List<string> { "feed_id", "comment_id", "is_comment_valid" }));
                                    mentionRow = mainSession.Execute(psCommentByTimestamp.Bind(feedId, commentId, true)).FirstOrDefault();
                                }
                                else
                                {
                                    psCommentByTimestamp = mainSession.Prepare(CQLGenerator.SelectStatement("reply_by_comment",
                                        new List<string>(), new List<string> { "comment_id", "reply_id", "is_reply_valid" }));
                                    mentionRow = mainSession.Execute(psCommentByTimestamp.Bind(commentId, replyId, true)).FirstOrDefault();
                                }

                                if (mentionRow == null)
                                {
                                    continue;
                                }

                                string commentorUserId = mentionRow.GetValue<string>("commentor_user_id");
                                ownerUser = userManager.SelectUserBasic(commentorUserId, companyId, false, mainSession).User;
                                string creatorFullName = string.Format("{0} {1}", ownerUser.FirstName, ownerUser.LastName);

                                if (notificationSubType == (int)NotificationFeedSubType.MentionedInComment)
                                {
                                    content = string.Format(NotificationMessage.NotificationMentionedInComment, creatorFullName);
                                }
                                else
                                {
                                    content = string.Format(NotificationMessage.NotificationMentionedInReply, creatorFullName);
                                }
                            }
                            else if (notificationSubType == (int)NotificationFeedSubType.UpvotePost || notificationSubType == (int)NotificationFeedSubType.UpvoteComment || notificationSubType == (int)NotificationFeedSubType.UpvoteReply)
                            {
                                Dictionary<string, object> pointDict = new Dictionary<string, object>();
                                int positivePoint = 0;

                                if (notificationSubType == (int)NotificationFeedSubType.UpvotePost)
                                {
                                    string feedText = string.Empty;
                                    string textVariable = string.Empty;
                                    string tableName = string.Empty;

                                    pointDict = feedManager.GetPointsForFeed(feedId, companyId, mainSession);

                                    positivePoint = (int)pointDict["positivePoint"];

                                    if (positivePoint <= 0)
                                    {
                                        continue;
                                    }

                                    if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                                    {
                                        textVariable = "content";
                                        tableName = "feed_text";
                                    }
                                    else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                                    {
                                        textVariable = "caption";
                                        tableName = "feed_image";
                                    }
                                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                                    {
                                        textVariable = "caption";
                                        tableName = "feed_video";
                                    }
                                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                                    {
                                        textVariable = "caption";
                                        tableName = "feed_shared_url";
                                    }

                                    PreparedStatement psFeedPost = mainSession.Prepare(CQLGenerator.SelectStatement(tableName,
                                        new List<string>(), new List<string> { "id", "is_valid" }));
                                    Row feedPostRow = mainSession.Execute(psFeedPost.Bind(feedId, true)).FirstOrDefault();

                                    if (feedPostRow == null)
                                    {
                                        continue;
                                    }

                                    feedText = feedPostRow[textVariable] != null ? ReplaceUserIdWithName(feedPostRow.GetValue<string>(textVariable), companyId, mainSession) : string.Empty;

                                    if (!string.IsNullOrEmpty(feedText))
                                    {
                                        content = string.Format(NotificationMessage.NotificationUpvotedPostWithTitle, positivePoint, feedText);
                                    }
                                    else
                                    {
                                        content = string.Format(NotificationMessage.NotificationUpvotedPostNoTitle, positivePoint);
                                    }

                                }
                                else if (notificationSubType == (int)NotificationFeedSubType.UpvoteComment)
                                {
                                    pointDict = feedManager.GetPointsForComment(feedId, commentId, mainSession);

                                    positivePoint = (int)pointDict["positivePoint"];

                                    if (positivePoint <= 0)
                                    {
                                        continue;
                                    }

                                    PreparedStatement psCommentText = mainSession.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                                        new List<string>(), new List<string> { "feed_id", "id", "is_valid" }));
                                    Row commentTextRow = mainSession.Execute(psCommentText.Bind(feedId, commentId, true)).FirstOrDefault();

                                    if (commentTextRow == null)
                                    {
                                        continue;
                                    }

                                    string commentText = commentTextRow["content"] != null ? ReplaceUserIdWithName(commentTextRow.GetValue<string>("content"), companyId, mainSession) : string.Empty;

                                    content = string.Format(NotificationMessage.NotificationUpvotedComment, positivePoint, commentText);
                                }
                                else if (notificationSubType == (int)NotificationFeedSubType.UpvoteReply)
                                {
                                    pointDict = feedManager.GetPointsForReply(commentId, replyId, mainSession);

                                    positivePoint = (int)pointDict["positivePoint"];

                                    if (positivePoint <= 0)
                                    {
                                        continue;
                                    }

                                    PreparedStatement psReplyText = mainSession.Prepare(CQLGenerator.SelectStatement("feed_reply_text",
                                       new List<string>(), new List<string> { "comment_id", "id", "is_valid" }));
                                    Row replyTextRow = mainSession.Execute(psReplyText.Bind(commentId, replyId, true)).FirstOrDefault();

                                    if (replyTextRow == null)
                                    {
                                        continue;
                                    }

                                    string replyText = replyTextRow["content"] != null ? ReplaceUserIdWithName(replyTextRow.GetValue<string>("content"), companyId, mainSession) : string.Empty;

                                    content = string.Format(NotificationMessage.NotificationUpvotedReply, positivePoint, replyText);
                                }
                            }

                            notification = new Notification
                            {
                                Id = notificationId,
                                TaggedFeedId = feedId,
                                TaggedCommentId = commentId,
                                TaggedReplyId = replyId,
                                TaggedUser = ownerUser,
                                Type = notificationType,
                                SubType = notificationSubType,
                                NotificationText = content,
                                IsSeen = isSeen,
                                CreatedOnTimestamp = createdOnTimestamp
                            };
                        }
                        // Game
                        else if (notificationType == (int)NotificationType.Challenge)
                        {
                            User player = null;

                            if (notificationSubType != (int)NotificationGameSubType.AchievementUnlocked)
                            {
                                Row challengeRow = vh.ValidateChallenge(challengeId, companyId, mainSession);

                                if (challengeRow == null)
                                {
                                    DeleteNotification(allNotificationRow, mainSession);
                                    continue;
                                }

                                List<string> playerIds = challengeRow.GetValue<List<string>>("players_ids");
                                string initiatorUserId = playerIds[0];
                                string challengedUserId = playerIds[1];

                                string topicId = challengeRow.GetValue<string>("topic_id");
                                string topicCategoryId = challengeRow.GetValue<string>("topic_category_id");

                                Topic topic = new Topic().SelectTopicBasic(topicId, null, companyId, topicCategoryId, null, mainSession).Topic;

                                if (topic != null)
                                {
                                    if (notificationSubType == (int)NotificationGameSubType.NewChallenge)
                                    {
                                        if (challengedUserId.Equals(targetedUserId))
                                        {
                                            player = userManager.SelectUserBasic(initiatorUserId, companyId, false, mainSession).User;
                                            string playerFullName = string.Format("{0} {1}", player.FirstName, player.LastName);
                                            content = string.Format(NotificationMessage.NotificationChallengeNew, playerFullName, topic.TopicTitle);
                                        }
                                    }
                                    else if (notificationSubType == (int)NotificationGameSubType.ChallengedUserCompletedChallenge)
                                    {
                                        if (initiatorUserId.Equals(targetedUserId))
                                        {
                                            player = userManager.SelectUserBasic(challengedUserId, companyId, false, mainSession).User;
                                            string playerFullName = string.Format("{0} {1}", player.FirstName, player.LastName);
                                            content = string.Format(NotificationMessage.NotificationChallengeCompleted, playerFullName, topic.TopicTitle);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Achievement selectedAchievement = gamificationManager.SelectAchievementByUser(targetedUserId, companyId, achievementId, mainSession, analyticSession).Achievement;
                                if (selectedAchievement == null || !selectedAchievement.IsUnlocked)
                                {
                                    Log.Debug("Remove achievement unlocked notification");

                                    DeleteNotification(allNotificationRow, mainSession);
                                    continue;
                                }

                                content = string.Format(NotificationMessage.NotificationAchievementUnlocked, selectedAchievement.Title);
                            }

                            notification = new Notification
                            {
                                Id = notificationId,
                                TaggedChallengeId = challengeId,
                                TaggedUser = player,
                                Type = notificationType,
                                SubType = notificationSubType,
                                NotificationText = content,
                                IsSeen = isSeen,
                                CreatedOnTimestamp = createdOnTimestamp
                            };

                        }
                        // Pulse
                        else if (notificationType == (int)NotificationType.Pulse)
                        {
                            User createdByUser = null;

                            if (notificationSubType == (int)NotificationPulseSubType.Announcement)
                            {
                                PulseSelectPrivacyResponse privacyResponse = pulseSingleManager.SelectPulsePrivacy(pulseId, mainSession);
                                if (!pulseSingleManager.CheckPulseForCurrentUser(targetedUserId, pulseId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, targetedUserDepartments, mainSession, true))
                                {
                                    continue;
                                }
                                Pulse pulse = pulseSingleManager.SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, 0, true, false, pulseType.Value, 0, mainSession).Pulse;
                                if (pulse == null)
                                {
                                    continue;
                                }
                                content = string.Format(NotificationMessage.NotificationAnnouncement, pulse.Title);
                            }
                            else if (notificationSubType == (int)NotificationPulseSubType.Live360)
                            {
                                Pulse appraisalPulse = appraisalManager.MapToPulse(pulseId, targetedUserId, companyId, null, mainSession, analyticSession, false).Pulse;
                                if (appraisalPulse == null)
                                {
                                    continue;
                                }

                                string createdByUserId = appraisalPulse.Appraisal.CreatedByUser.UserId;
                                createdByUser = userManager.SelectUserBasic(createdByUserId, companyId, false, mainSession).User;
                                if (createdByUserId.Equals(targetedUserId))
                                {
                                    content = string.Format(NotificationMessage.Notification360ForSelfAssessment, appraisalPulse.Appraisal.Title);
                                }
                                else
                                {
                                    string creatorName = string.Format("{0} {1}", createdByUser.FirstName, createdByUser.LastName);
                                    content = string.Format(NotificationMessage.Notification360ForColleagues, creatorName, appraisalPulse.Appraisal.Title);
                                }
                            }

                            notification = new Notification
                            {
                                Id = notificationId,
                                TaggedPulseId = pulseId,
                                TaggedAppraisalId = appraisalId,
                                TaggedUser = createdByUser,
                                Type = notificationType,
                                SubType = notificationSubType,
                                NotificationText = content,
                                IsSeen = isSeen,
                                CreatedOnTimestamp = createdOnTimestamp
                            };
                        }
                        // Report
                        else if (notificationType == (int)NotificationType.Report)
                        {
                            User createdByUser = null;

                            if (notificationSubType == (int)NotificationReportSubType.Live360End || notificationSubType == (int)NotificationReportSubType.Live360Completed)
                            {
                                Row appraisalRow = vh.ValidateCustomAppraisal(companyId, reportId, mainSession);
                                if (appraisalRow == null)
                                {
                                    continue;
                                }

                                string createdByUserId = appraisalRow.GetValue<string>("created_by_user_id");
                                if (!targetedUserId.Equals(createdByUserId))
                                {
                                    continue;
                                }

                                AppraisalValidationResponse validateResultResponse = appraisalManager.ValidateResultSet(appraisalRow, analyticSession);
                                if (!validateResultResponse.Success)
                                {
                                    continue;
                                }

                                createdByUser = userManager.SelectUserBasic(createdByUserId, companyId, false, mainSession).User;
                                content = GenerateReportNotificationText(appraisalRow.GetValue<string>("title"), notificationSubType);
                            }

                            notification = new Notification
                            {
                                Id = notificationId,
                                TaggedReportId = reportId,
                                TaggedUser = createdByUser,
                                Type = notificationType,
                                SubType = notificationSubType,
                                NotificationText = content,
                                IsSeen = isSeen,
                                CreatedOnTimestamp = createdOnTimestamp
                            };
                        }

                        if (notification != null)
                        {
                            response.Notifications.Add(notification);

                            if (response.Notifications.Count() == limit)
                            {
                                isSelectCompleted = true;
                                break;
                            }

                        }
                    }// for

                    if (allNotificationRowList.Count == 0)
                    {
                        isSelectCompleted = true;
                    }

                }// while

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void DeleteFeedNotificationByUser(string targetedUserId, int subType, string feedId, string commentId, string replyId, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_feed_grouping", new List<string>(), new List<string> { "targeted_user_id", "notification_subtype", "feed_id", "comment_id", "reply_id" }));
                Row notificationRow = mainSession.Execute(ps.Bind(targetedUserId, subType, feedId, commentId, replyId)).FirstOrDefault();

                if (notificationRow == null)
                {
                    Log.Error("Feed Notification not found for delete");
                }
                else
                {
                    DeleteNotification(notificationRow, mainSession);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void CreateScheduledAnnouncementNotification(string companyId, List<string> targetedDepartmentsIds, List<string> targetedUserIds, string pulseId, DateTime scheduledTimestamp, ISession mainSession)
        {
            try
            {
                Thread thread = new Thread(() => CreateScheduledNotification(companyId, targetedDepartmentsIds, targetedUserIds, (int)NotificationType.Pulse, (int)NotificationPulseSubType.Announcement, scheduledTimestamp, mainSession, pulseId, null, (int)Pulse.PulseTypeEnum.Announcement));
                thread.Start();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateScheduledAnnouncementNotification(string companyId, List<string> newTargetedDepartmentsIds, List<string> newTargetedUserIds, string pulseId, DateTime newScheduledTimestamp, ISession mainSession)
        {
            try
            {
                Thread thread = new Thread(() => UpdateScheduledNotification(companyId, newTargetedDepartmentsIds, newTargetedUserIds, (int)NotificationType.Pulse, (int)NotificationPulseSubType.Announcement, newScheduledTimestamp, mainSession, pulseId, null, (int)Pulse.PulseTypeEnum.Announcement));
                thread.Start();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void CreateScheduledAppraisalNotification(string companyId, string creatorUserId, List<string> targetedUserIds, string pulseId, string appraisalId, DateTime scheduledStartTimestamp, DateTime scheduledEndTimestamp, ISession mainSession)
        {
            try
            {
                // Start notification
                Thread startThread = new Thread(() => CreateScheduledNotification(companyId, null, targetedUserIds, (int)NotificationType.Pulse, (int)NotificationPulseSubType.Live360, scheduledStartTimestamp, mainSession, pulseId, appraisalId, (int)Pulse.PulseTypeEnum.Live360));
                startThread.Start();

                // End notification
                Thread endThread = new Thread(() => CreateScheduledNotification(companyId, null, new List<string> { creatorUserId }, (int)NotificationType.Report, (int)NotificationReportSubType.Live360End, scheduledEndTimestamp, mainSession, null, appraisalId, 0));
                endThread.Start();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateScheduledAppraisalNotification(string companyId, string creatorUserId, List<string> newTargetedUserIds, string pulseId, string appraisalId, DateTime newScheduledStartTimestamp, DateTime newScheduledEndTimestamp, ISession mainSession)
        {
            try
            {
                // Start notification
                Thread startThread = new Thread(() => UpdateScheduledNotification(companyId, null, newTargetedUserIds, (int)NotificationType.Pulse, (int)NotificationPulseSubType.Live360, newScheduledStartTimestamp, mainSession, pulseId, appraisalId, (int)Pulse.PulseTypeEnum.Live360));
                startThread.Start();

                // End notification
                Thread endThread = new Thread(() => UpdateScheduledNotification(companyId, null, new List<string> { creatorUserId }, (int)NotificationType.Report, (int)NotificationReportSubType.Live360End, newScheduledEndTimestamp, mainSession, null, appraisalId, 0));
                endThread.Start();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public TriggerScheduledNotificationResponse TriggerScheduledNotification(string scheduledId, string companyId)
        {
            TriggerScheduledNotificationResponse response = new TriggerScheduledNotificationResponse();
            response.PushNotifications = new List<Notification>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                Log.Debug($"Scheduled notification for {scheduledId}");

                response.PushNotifications = PushScheduledNotification(scheduledId, companyId, mainSession, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public HttpRequestGetScheduleResponse PullSchedules()
        {
            HttpRequestGetScheduleResponse response = new HttpRequestGetScheduleResponse();
            response.Schedules = new List<Schedule>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                Log.Debug("PullSchedules");

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("scheduled_notification_task", new List<string>(), new List<string>()));
                RowSet scheduleRowSet = mainSession.Execute(ps.Bind());

                foreach (Row scheduleRow in scheduleRowSet)
                {
                    string scheduleId = scheduleRow.GetValue<string>("scheduled_id");
                    string companyId = scheduleRow.GetValue<string>("company_id");

                    Schedule schedule = response.Schedules.FirstOrDefault(s => s.ScheduledId.Equals(scheduleId));
                    if(schedule != null)
                    {
                        continue;
                    }

                    int notificationType = scheduleRow.GetValue<int>("notification_type");
                    int notificationSubType = scheduleRow.GetValue<int>("notification_subtype");

                    string groupKey = string.Empty;
                    if (notificationType == (int)NotificationType.Pulse)
                    {
                        if(notificationSubType == (int)NotificationPulseSubType.Announcement)
                        {
                            groupKey = PrefixId.ScheduledAnnouncementKey;
                        }
                        else if(notificationSubType == (int)NotificationPulseSubType.Live360)
                        {
                            groupKey = PrefixId.ScheduledAppraisalKey;
                        }
                    }
                    else if(notificationType == (int)NotificationType.Report)
                    {
                        groupKey = PrefixId.ScheduledReportKey;
                    }

                    DateTime scheduledTimestamp = scheduleRow.GetValue<DateTime>("scheduled_on_timestamp");
                    response.Schedules.Add(new Schedule
                    {
                        CompanyId = companyId,
                        ScheduledId = scheduleId,
                        GroupName = groupKey,
                        ScheduledOnTimestamp = scheduledTimestamp
                    });
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private List<Notification> PushScheduledNotification(string scheduledId, string companyId, ISession mainSession, ISession analyticSession)
        {
            List<Notification> notifications = new List<Notification>();
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                ps = mainSession.Prepare(CQLGenerator.SelectStatement("scheduled_notification_task", new List<string>(), new List<string> { "scheduled_id", "company_id" }));
                RowSet scheduleRowSet = mainSession.Execute(ps.Bind(scheduledId, companyId));

                string targetedPulseId = null;
                User appraisalCreatorUser = null;

                DateTime currentTime = DateTime.UtcNow;

                PulseSingle pulseSingleManager = new PulseSingle();
                Appraisal appraisalManager = new Appraisal();
                User userManager = new User();
                Department departmentManager = new Department();

                string titleHeader = string.Empty;

                foreach (Row scheduleRow in scheduleRowSet)
                {
                    string targetedUserId = scheduleRow.GetValue<string>("targeted_user_id");
                    string notificationId = scheduleRow.GetValue<string>("notification_id");
                    string challengeId = scheduleRow.GetValue<string>("challenge_id");
                    string achievementId = scheduleRow.GetValue<string>("achievement_id");
                    string feedId = scheduleRow.GetValue<string>("feed_id");
                    string commentId = scheduleRow.GetValue<string>("comment_id");
                    string replyId = scheduleRow.GetValue<string>("reply_id");
                    int? feedType = scheduleRow.GetValue<int?>("feed_type");
                    int? pulseType = scheduleRow.GetValue<int?>("pulse_type");
                    string pulseId = scheduleRow.GetValue<string>("pulse_id");
                    string appraisalId = scheduleRow.GetValue<string>("appraisal_id");
                    string reportId = scheduleRow.GetValue<string>("report_id");
                    bool isSeen = scheduleRow.GetValue<bool>("is_seen");
                    bool isFetched = scheduleRow.GetValue<bool>("is_fetched");
                    int notificationSubType = scheduleRow.GetValue<int>("notification_subtype");
                    int notificationType = scheduleRow.GetValue<int>("notification_type");

                    // Double checking for duplicates
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_user", new List<string>(), new List<string> { "notification_id", "targeted_user_id" }));
                    Row notificationRow = mainSession.Execute(ps.Bind(notificationId, targetedUserId)).FirstOrDefault();
                    if(notificationRow != null)
                    {
                        continue;
                    }

                    targetedPulseId = pulseId;

                    List<string> targetedUserDepartments = departmentManager.GetAllDepartmentByUserId(targetedUserId, companyId, mainSession).Departments.Select(d => d.Id).ToList();

                    if (notificationType == (int)NotificationType.Pulse)
                    {
                        if (notificationSubType == (int)NotificationPulseSubType.Live360)
                        {
                            // Check for privacy
                            Pulse appraisalPulse = appraisalManager.MapToPulse(pulseId, targetedUserId, companyId, currentTime, mainSession, analyticSession, false).Pulse;
                            if (appraisalPulse == null)
                            {
                                break;
                            }

                            titleHeader = appraisalPulse.Appraisal.Title;
                            appraisalCreatorUser = appraisalPulse.Appraisal.CreatedByUser;

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_appraisal",
                                new List<string> { "notification_id", "targeted_user_id", "pulse_type", "pulse_id", "appraisal_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, pulseType, pulseId, appraisalId, isSeen, isFetched, notificationType, notificationSubType, currentTime));

                            notifications.Add(GetPushAppraisalNotification(companyId, titleHeader, appraisalCreatorUser, targetedUserId, pulseId, appraisalId, mainSession));
                        }
                        else if (notificationSubType == (int)NotificationPulseSubType.Announcement)
                        {
                            PulseSelectPrivacyResponse privacyResponse = pulseSingleManager.SelectPulsePrivacy(pulseId, mainSession);

                            if (!pulseSingleManager.CheckPulseForCurrentUser(targetedUserId, pulseId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, targetedUserDepartments, mainSession, true))
                            {
                                Log.Error("Pulse not for current user");
                                continue;
                            }

                            if (string.IsNullOrEmpty(titleHeader))
                            {
                                // Check pulse validity
                                Pulse announcementPulse = pulseSingleManager.SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, 0, false, false, (int)PulseTypeEnum.Announcement, 0, mainSession).Pulse;
                                if (announcementPulse == null)
                                {
                                    Log.Error("Announcement pulse does not exists!");
                                    break;
                                }
                                titleHeader = announcementPulse.Title;
                            }

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_pulse",
                                new List<string> { "notification_id", "targeted_user_id", "pulse_type", "pulse_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, pulseType, pulseId, isSeen, isFetched, notificationType, notificationSubType, currentTime));

                            notifications.Add(GetPushAnnouncementNotification(companyId, titleHeader, targetedUserId, pulseId, mainSession));
                        }
                    }
                    else if(notificationType == (int)NotificationType.Report)
                    {
                        if (notificationSubType == (int)NotificationReportSubType.Live360End)
                        {
                            // Check for privacy
                            AppraisalSelectResponse reportResponse = appraisalManager.SelectFullCustomAppraisalByUser(targetedUserId, companyId, reportId, true);
                            if (!reportResponse.Success)
                            {
                                Log.Debug(reportResponse.ErrorMessage);
                                break;
                            }

                            ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_report",
                                new List<string> { "notification_id", "targeted_user_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(notificationId, targetedUserId, reportId, false, false, notificationType, notificationSubType, currentTime));

                            titleHeader = reportResponse.Appraisal.Title;
                            notifications.Add(GetReportNotification(companyId, reportId, titleHeader, targetedUserId, notificationSubType, mainSession));
                        }

                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_user",
                        new List<string> { "notification_id", "targeted_user_id", "pulse_type", "pulse_id", "appraisal_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    updateBatch.Add(ps.Bind(notificationId, targetedUserId, pulseType, pulseId, appraisalId, reportId, isSeen, isFetched, notificationType, notificationSubType, currentTime));

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("notification_privacy_sorted_by_user_timestamp",
                              new List<string> { "notification_id", "targeted_user_id", "pulse_type", "pulse_id", "appraisal_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    updateBatch.Add(ps.Bind(notificationId, targetedUserId, pulseType, pulseId, appraisalId, reportId, isSeen, isFetched, notificationType, notificationSubType, currentTime));

                    mainSession.Execute(updateBatch);
                    updateBatch = new BatchStatement();
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task",
                        new List<string> { "scheduled_id", "company_id" }));
                deleteBatch.Add(ps.Bind(scheduledId, companyId));

                if (!string.IsNullOrEmpty(targetedPulseId))
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task_by_pulse",
                        new List<string> { "pulse_id", "company_id" }));
                    deleteBatch.Add(ps.Bind(targetedPulseId, companyId));
                }

                mainSession.Execute(deleteBatch);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return notifications;
        }

        private Notification GetReportNotification(string companyId, string reportId, string reportTitle, string notifiedUserId, int notificationSubType, ISession mainSession)
        {
            string title = GenerateReportNotificationText(reportTitle, notificationSubType);
            title = title.Replace("<b>", "");
            title = title.Replace("</b>", "");

            return new Notification
            {
                NotificationText = title,
                TaggedUserId = notifiedUserId,
                TaggedReportId = reportId,
                SubType = notificationSubType,
                Type = (int)NotificationType.Report,
                NumberOfNotificationForTargetedUser = SelectNotificationNumberByUser(notifiedUserId, companyId, mainSession).NumberOfNotification
            };
        }

        private Notification GetPushAnnouncementNotification(string companyId, string title, string notifiedUserId, string pulseId, ISession mainSession)
        {
            string notificationText = string.Format(NotificationMessage.NotificationAnnouncement, title);
            notificationText = notificationText.Replace("<b>", "");
            notificationText = notificationText.Replace("</b>", "");

            return new Notification
            {
                TaggedUserId = notifiedUserId,
                NotificationText = notificationText,
                NumberOfNotificationForTargetedUser = SelectNotificationNumberByUser(notifiedUserId, companyId, mainSession).NumberOfNotification,
                Type = (int)NotificationType.Pulse,
                SubType = (int)NotificationPulseSubType.Announcement,
                TaggedPulseId = pulseId
            };
        }

        private Notification GetPushAppraisalNotification(string companyId, string title, User creatorUser, string notifiedUserId, string pulseId, string appraisalId, ISession mainSession)
        {
            string notificationText = string.Empty;

            if (creatorUser.UserId.Equals(notifiedUserId))
            {
                notificationText = string.Format(NotificationMessage.Notification360ForSelfAssessment, title);
            }
            else
            {
                string fullName = $"{creatorUser.FirstName} {creatorUser.LastName}";
                notificationText = string.Format(NotificationMessage.Notification360ForColleagues, fullName, title);
            }

            notificationText = notificationText.Replace("<b>", "");
            notificationText = notificationText.Replace("</b>", "");

            return new Notification
            {
                TaggedUserId = notifiedUserId,
                NotificationText = notificationText,
                NumberOfNotificationForTargetedUser = SelectNotificationNumberByUser(notifiedUserId, companyId, mainSession).NumberOfNotification,
                Type = (int)NotificationType.Pulse,
                SubType = (int)NotificationPulseSubType.Live360,
                TaggedPulseId = pulseId,
                TaggedAppraisalId = appraisalId
            };
        }

        private void UpdateScheduledNotification(string companyId,
                                                 List<string> targetedDepartmentsIds,
                                                 List<string> targetedUserIds,
                                                 int notificationType,
                                                 int notificationSubType,
                                                 DateTime scheduledTimestamp,
                                                 ISession mainSession,
                                                 string pulseId,
                                                 string appraisalId,
                                                 int pulseType)
        {
            try
            {
                Log.Debug("UpdateScheduledNotification");

                PreparedStatement ps = null;
                BoundStatement bs = null;
                if (notificationType == (int)NotificationType.Pulse)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("scheduled_notification_task_by_pulse", new List<string>(), new List<string> { "pulse_id", "company_id" }, 1));
                    bs = ps.Bind(pulseId, companyId);
                }
                else if(notificationType == (int)NotificationType.Report)
                {
                    string reportId = string.Empty;

                    if(notificationSubType == (int)NotificationReportSubType.Live360End)
                    {
                        reportId = appraisalId;
                    }

                    ps = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("scheduled_notification_task_by_report", new List<string>(), new List<string> { "report_id", "company_id" }, 1));
                    bs = ps.Bind(reportId, companyId);
                }

                string scheduledTaskId = null;
                Row scheduledRow = mainSession.Execute(bs).FirstOrDefault();
                if (scheduledRow != null)
                {
                    scheduledTaskId = scheduledRow.GetValue<string>("scheduled_id");
                    CreateScheduledNotification(companyId, targetedDepartmentsIds, targetedUserIds, notificationType, notificationSubType, scheduledTimestamp, mainSession, pulseId, appraisalId, pulseType, scheduledTaskId);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void CreateScheduledNotification(string companyId,
                                                 List<string> targetedDepartmentIds,
                                                 List<string> targetedUserIds,
                                                 int notificationType,
                                                 int notificationSubType,
                                                 DateTime scheduledTimestamp,
                                                 ISession mainSession,
                                                 string pulseId,
                                                 string appraisalId,
                                                 int pulseType,
                                                 string scheduledTaskId = null)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                string reportId = null;
                if (!string.IsNullOrEmpty(scheduledTaskId))
                {
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task",
                        new List<string> { "scheduled_id", "company_id" }));
                    deleteBatch.Add(ps.Bind(scheduledTaskId, companyId));

                    if (notificationType == (int)NotificationType.Pulse)
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task_by_pulse",
                            new List<string> { "pulse_id", "company_id" }));
                        deleteBatch.Add(ps.Bind(pulseId, companyId));
                    }
                    else if(notificationType == (int)NotificationType.Report)
                    {
                        if(notificationSubType == (int)NotificationReportSubType.Live360End)
                        {
                            reportId = appraisalId;
                        }

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task_by_report",
                           new List<string> { "report_id", "company_id" }));
                        deleteBatch.Add(ps.Bind(reportId, companyId));
                    }

                    mainSession.Execute(deleteBatch);
                }
                else
                {
                    scheduledTaskId = UUIDGenerator.GenerateUniqueIDForScheduledNotification();
                }

                string groupKey = string.Empty;
                List<string> notifiedUserIds = GetAllNotifiedUsers(companyId, targetedDepartmentIds, targetedUserIds, mainSession);

                foreach (string notifiedUserId in notifiedUserIds)
                {
                    string notificationId = string.Empty;

                    if (notificationType == (int)NotificationType.Pulse)
                    {
                        if (notificationSubType == (int)NotificationPulseSubType.Announcement)
                        {
                            groupKey = PrefixId.ScheduledAnnouncementKey;
                            notificationId = UUIDGenerator.GenerateUniqueIDForAnnouncementNotification();
                        }
                        else if (notificationSubType == (int)NotificationPulseSubType.Live360)
                        {
                            groupKey = PrefixId.ScheduledAppraisalKey;
                            notificationId = UUIDGenerator.GenerateUniqueIDForAppraisalNotification();
                        }

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("scheduled_notification_task_by_pulse",
                            new List<string> { "scheduled_id", "company_id", "notification_id", "targeted_user_id", "pulse_type", "pulse_id", "appraisal_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "scheduled_on_timestamp" }));
                        updateBatch.Add(ps.Bind(scheduledTaskId, companyId, notificationId, notifiedUserId, pulseType, pulseId, appraisalId, false, false, notificationType, notificationSubType, scheduledTimestamp));
                    }
                    else if(notificationType == (int)NotificationType.Report)
                    {
                        groupKey = PrefixId.ScheduledReportKey;

                        if (notificationSubType == (int)NotificationReportSubType.Live360End)
                        {
                            reportId = appraisalId;
                            notificationId = UUIDGenerator.GenerateUniqueIDForAppraisalReportNotification();
                            appraisalId = null;
                        }

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("scheduled_notification_task_by_report",
                            new List<string> { "scheduled_id", "company_id", "notification_id", "targeted_user_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "scheduled_on_timestamp" }));
                        updateBatch.Add(ps.Bind(scheduledTaskId, companyId, notificationId, notifiedUserId, reportId, false, false, notificationType, notificationSubType, scheduledTimestamp));
                    }

                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("scheduled_notification_task",
                        new List<string> { "scheduled_id", "company_id", "notification_id", "targeted_user_id", "pulse_type", "pulse_id", "appraisal_id", "report_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "scheduled_on_timestamp" }));
                    updateBatch.Add(ps.Bind(scheduledTaskId, companyId, notificationId, notifiedUserId, pulseType, pulseId, appraisalId, reportId, false, false, notificationType, notificationSubType, scheduledTimestamp));
                }

                mainSession.Execute(updateBatch);

                // Make HTTP request to TaskScheduler API
                new HttpRequestHandler().MakeTaskScheduleRequest(scheduledTaskId, companyId, groupKey, scheduledTimestamp);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void DeleteNotification(Row notificationRow, ISession mainSession)
        {
            try
            {
                string notificationId = notificationRow.GetValue<string>("notification_id");
                string targetedUserId = notificationRow.GetValue<string>("targeted_user_id");
                DateTime createdOnTimestamp = notificationRow.GetValue<DateTime>("notification_created_on_timestamp");
                int type = notificationRow.GetValue<int>("notification_type");
                int subType = notificationRow.GetValue<int>("notification_subtype");

                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_user",
                    new List<string> { "targeted_user_id", "notification_id" }));
                deleteBatch.Add(ps.Bind(targetedUserId, notificationId));

                if (type == (int)NotificationType.Feed)
                {
                    string feedId = notificationRow.GetValue<string>("feed_id");
                    string commentId = notificationRow.GetValue<string>("comment_id");
                    string replyId = notificationRow.GetValue<string>("reply_id");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_feed_grouping",
                        new List<string> { "feed_id", "comment_id", "reply_id", "notification_subtype", "targeted_user_id", "notification_id" }));
                    deleteBatch.Add(ps.Bind(feedId, commentId, replyId, subType, targetedUserId, notificationId));
                }
                else if (type == (int)NotificationType.Challenge)
                {
                    string challengeId = notificationRow.GetValue<string>("challenge_id");
                    string achievementId = notificationRow.GetValue<string>("achievement_id");

                    if (!string.IsNullOrEmpty(challengeId))
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_challenge",
                            new List<string> { "challenge_id", "targeted_user_id", "notification_id" }));
                        deleteBatch.Add(ps.Bind(challengeId, targetedUserId, notificationId));
                    }
                    else
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_achievement",
                            new List<string> { "achievement_id", "targeted_user_id", "notification_id" }));
                        deleteBatch.Add(ps.Bind(achievementId, targetedUserId, notificationId));
                    }
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_sorted_by_user_timestamp",
                    new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                deleteBatch.Add(ps.Bind(targetedUserId, createdOnTimestamp, notificationId));

                mainSession.Execute(deleteBatch);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void UpdateFetchedNotification(Row notificationRow, ISession mainSession)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                string notificationId = notificationRow.GetValue<string>("notification_id");
                string targetedUserId = notificationRow.GetValue<string>("targeted_user_id");
                int type = notificationRow.GetValue<int>("notification_type");
                int subType = notificationRow.GetValue<int>("notification_subtype");
                DateTime? createdTimestamp = notificationRow.GetValue<DateTime?>("notification_created_on_timestamp");

                if (type == (int)NotificationType.Feed)
                {
                    string feedId = notificationRow.GetValue<string>("feed_id");
                    string commentId = notificationRow.GetValue<string>("comment_id");
                    string replyId = notificationRow.GetValue<string>("reply_id");

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_feed_grouping",
                        new List<string> { "feed_id", "comment_id", "reply_id", "notification_subtype", "targeted_user_id", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                    batch.Add(ps.Bind(true, feedId, commentId, replyId, subType, targetedUserId, notificationId));
                }
                else if (type == (int)NotificationType.Challenge)
                {
                    string challengeId = notificationRow.GetValue<string>("challenge_id");
                    string achievementId = notificationRow.GetValue<string>("achievement_id");

                    if (subType == (int)NotificationGameSubType.NewChallenge || subType == (int)NotificationGameSubType.ChallengedUserCompletedChallenge)
                    {
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_challenge",
                            new List<string> { "challenge_id", "targeted_user_id", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batch.Add(ps.Bind(true, challengeId, targetedUserId, notificationId));
                    }
                    else
                    {
                        ps = mainSession.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_achievement",
                            new List<string> { "achievement_id", "targeted_user_id", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                        batch.Add(ps.Bind(true, achievementId, targetedUserId, notificationId));
                    }
                }

                if (createdTimestamp != null)
                {
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("notification_privacy_sorted_by_user_timestamp",
                       new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                    batch.Add(ps.Bind(true, targetedUserId, createdTimestamp, notificationId));
                }

                mainSession.Execute(batch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void RemoveDuplicatedNotification(Row notificationRow, ISession mainSession)
        {
            try
            {
                string notificationId = notificationRow.GetValue<string>("notification_id");
                string targetedUserId = notificationRow.GetValue<string>("targeted_user_id");
                DateTime createdTimestamp = notificationRow.GetValue<DateTime>("notification_created_on_timestamp");

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_sorted_by_user_timestamp",
                    new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                mainSession.Execute(ps.Bind(targetedUserId, createdTimestamp, notificationId));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private string GetNotificationTextForFeedCreation(string feedId, string companyId, int feedType, int feedSubType, string ownerFullName, ISession session)
        {
            string content = string.Empty;

            try
            {
                if (feedType == Convert.ToInt16(FeedTypeCode.TextPost))
                {
                    switch (feedSubType)
                    {
                        case (int)NotificationFeedSubType.NewPostToUser:
                            content = string.Format(NotificationMessage.NotificationTextToUser, ownerFullName);
                            break;
                        case (int)NotificationFeedSubType.NewPostToDepartment:
                            content = string.Format(NotificationMessage.NotificationTextToDepartment, ownerFullName);
                            break;
                        case (int)NotificationFeedSubType.MentionedInPost:
                            content = string.Format(NotificationMessage.NotificationTextToMentioned, ownerFullName);
                            break;
                    }
                }
                else if (feedType == Convert.ToInt16(FeedTypeCode.ImagePost))
                {
                    PreparedStatement psFeedImageUrl = session.Prepare(CQLGenerator.CountStatement("feed_image_url", new List<string> { "feed_id", "is_valid" }));
                    int countOfImages = (int)session.Execute(psFeedImageUrl.Bind(feedId, true)).FirstOrDefault().GetValue<long>("count");

                    switch (feedSubType)
                    {
                        case (int)NotificationFeedSubType.NewPostToUser:
                            if (countOfImages > 1)
                            {
                                content = string.Format(NotificationMessage.NotificationImageManyToUser, ownerFullName, countOfImages);
                            }
                            else
                            {
                                content = string.Format(NotificationMessage.NotificationImageOneToUser, ownerFullName, countOfImages);
                            }
                            break;
                        case (int)NotificationFeedSubType.NewPostToDepartment:
                            if (countOfImages > 1)
                            {
                                content = string.Format(NotificationMessage.NotificationImageManyToDepartment, ownerFullName, countOfImages);
                            }
                            else
                            {
                                content = string.Format(NotificationMessage.NotificationImageOneToDepartment, ownerFullName, countOfImages);
                            }
                            break;
                        case (int)NotificationFeedSubType.MentionedInPost:
                            if (countOfImages > 1)
                            {
                                content = string.Format(NotificationMessage.NotificationImageManyToMentioned, ownerFullName, countOfImages);
                            }
                            else
                            {
                                content = string.Format(NotificationMessage.NotificationImageOneToMentioned, ownerFullName, countOfImages);
                            }
                            break;
                    }
                }
                else if (feedType == Convert.ToInt16(FeedTypeCode.SharedUrlPost))
                {
                    //PreparedStatement psFeedSharedUrl = session.Prepare(CQLGenerator.SelectStatement("feed_shared_url",
                    //    new List<string> { "caption" }, new List<string> { "id", "is_valid" }));
                    //Row feedSharedUrlRow = session.Execute(psFeedSharedUrl.Bind(feedId, true)).FirstOrDefault();
                    //string title = feedSharedUrlRow["caption"] == null ? string.Empty : feedSharedUrlRow.GetValue<string>("caption");

                    //title = ReplaceUserIdWithName(title, companyId, session);

                    switch (feedSubType)
                    {
                        case (int)NotificationFeedSubType.NewPostToUser:
                            content = string.Format(NotificationMessage.NotificationSharedUrlWithNoTitleToUser, ownerFullName);
                            break;
                        case (int)NotificationFeedSubType.NewPostToDepartment:
                            content = string.Format(NotificationMessage.NotificationSharedUrlWithNoTitleToDepartment, ownerFullName);
                            break;
                        case (int)NotificationFeedSubType.MentionedInPost:
                            content = string.Format(NotificationMessage.NotificationSharedUrlWithNoTitleToMentioned, ownerFullName);
                            break;
                    }
                }
                else if (feedType == Convert.ToInt16(FeedTypeCode.VideoPost))
                {
                    switch (feedSubType)
                    {
                        case (int)NotificationFeedSubType.NewPostToUser:
                            content = string.Format(NotificationMessage.NotificationVideoToUser, ownerFullName);
                            break;
                        case (int)NotificationFeedSubType.NewPostToDepartment:
                            content = string.Format(NotificationMessage.NotificationVideoToDepartment, ownerFullName);
                            break;
                        case (int)NotificationFeedSubType.MentionedInPost:
                            content = string.Format(NotificationMessage.NotificationVideoToMentioned, ownerFullName);
                            break;
                    }
                }

                //Log.Debug("Content: " + content);
            }
            catch (Exception ex)
            {

                Log.Debug(ex.ToString());
            }

            return content;
        }

        private string ReplaceUserIdWithName(string text, string companyId, ISession session)
        {
            string replacement = string.Empty;

            Feed feedManager = new Feed();
            replacement = feedManager.ReplaceContentWithUserNames(text, companyId, session);

            return replacement;
        }

        private List<string> GetAllNotifiedUsers(string companyId, List<string> targetedDepartmentIds, List<string> targetedUserIds, ISession mainSession)
        {
            List<string> notifiedUserIds = new List<string>();
            try
            {
                User userManager = new User();
                if (targetedDepartmentIds == null && targetedUserIds == null)
                {
                    notifiedUserIds = userManager.GetAllUserForAdmin(null, companyId, null, 0, (int)User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession).Users.Select(u => u.UserId).ToList();
                }
                else
                {
                    if (targetedDepartmentIds != null)
                    {
                        notifiedUserIds = userManager.SelectAllUsersByDepartmentIds(targetedDepartmentIds, null, companyId, mainSession).Users.Select(u => u.UserId).ToList();
                    }

                    if (targetedUserIds != null)
                    {
                        notifiedUserIds.AddRange(targetedUserIds);
                    }

                    notifiedUserIds = notifiedUserIds.Distinct().ToList<string>();

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return notifiedUserIds;
        }
    }
}