﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using Cassandra.Mapping;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    public class UserComparer : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
        {
            return x.UserId.Equals(y.UserId, StringComparison.InvariantCultureIgnoreCase);
        }

        public int GetHashCode(User obj)
        {
            return obj.UserId.GetHashCode();
        }
    }

    [Serializable]
    [DataContract]
    public class User //: IEquatable<User>
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");
        private static string DefaultProfileImageUrl = "https://s3-ap-southeast-1.amazonaws.com/cocadre/profile/default_profile_photo_original.png";

        [DataMember(EmitDefaultValue = false)]
        public string UserToken { get; set; }

        [DataMember]
        public string UserId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Email { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProfileImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? Gender { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset? Birthday { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset LastActiveTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset LastModifiedProfileTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset LastModifiedStatusTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Address { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressPostalCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressCountryAbb { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressCountryName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Phone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PhoneCountryCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PhoneCountryAbb { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Company Company { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Department> Departments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public AccountStatus Status { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public AccountType Type { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Position { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Module> AccessModules { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Module> AccessSystemModules { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime? ModeratorExpiredDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsEmailSent { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool HasLogin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsTaggedForFeed { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BirthdayString { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Group> Groups { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime LastLoginDateTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool NeedResetPassword { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public Company.CompanyJob Job { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String AgeBand { get; set; }

        #region Exp
        [DataMember]
        public int Level { get; set; }
        [DataMember]
        public int Experience { get; set; }
        #endregion

        #region Quiz
        [DataMember]
        public int TotalNumberOfUniqueCorrects { get; set; }
        #endregion

        #region MLEducation
        [DataMember(EmitDefaultValue = false)]
        public MLEducationAnalytic EducationAnalytic { get; set; }

        [Serializable]
        [DataContract]
        public class MLEducationAnalytic
        {
            [DataMember]
            public int ProgressStatus { get; set; }

            [DataMember]
            public DateTimeOffset? LastProgressDateTime { get; set; }

            [DataMember]
            public string LastProgressDateTimeString { get; set; }
        }
        #endregion

        [Serializable]
        [DataContract]
        public class AccountType
        {
            public const int CODE_ALL_USER = 0;
            public const int CODE_NORMAL_USER = 1;
            public const int CODE_MODERATER = 2;
            public const int CODE_ADMIN = 3;
            public const int CODE_COCADRE_ADMIN = 800;
            public const int CODE_SUPER_ADMIN = 999;

            [DataMember(EmitDefaultValue = false)]
            public int Code { get; private set; }

            [DataMember(EmitDefaultValue = false)]
            public String Title { get; private set; }

            public AccountType(int code)
            {
                switch (code)
                {
                    case CODE_NORMAL_USER:
                        Code = CODE_NORMAL_USER;
                        Title = "Normal User";
                        break;

                    case CODE_MODERATER:
                        Code = CODE_MODERATER;
                        Title = "Moderater";
                        break;

                    case CODE_ADMIN:
                        Code = CODE_ADMIN;
                        Title = "Admin";
                        break;

                    case CODE_COCADRE_ADMIN:
                        Code = CODE_COCADRE_ADMIN;
                        Title = "Cocadre Admin";
                        break;

                    case CODE_SUPER_ADMIN:
                        Code = CODE_SUPER_ADMIN;
                        Title = "Super Admin";
                        break;

                    default:
                        break;
                }
            }
        }

        [Serializable]
        [DataContract]
        public class AccountStatus
        {
            public const int CODE_ACTIVE = 1;
            public const int CODE_SUSPENEDED = -1;
            public const int CODE_DELETING = -2;
            public const int CODE_DELETED = -999;

            [DataMember(EmitDefaultValue = false)]
            public int Code { get; private set; }

            [DataMember(EmitDefaultValue = false)]
            public String Title { get; private set; }

            public AccountStatus(int code)
            {
                switch (code)
                {
                    case CODE_ACTIVE:
                        Code = CODE_ACTIVE;
                        Title = "Active";
                        break;
                    case CODE_SUSPENEDED:
                        Code = CODE_SUSPENEDED;
                        Title = "Suspended";
                        break;
                    case CODE_DELETING:
                        Code = CODE_DELETING;
                        Title = "Deleting";
                        break;
                    case CODE_DELETED:
                        Code = CODE_DELETED;
                        Title = "Deleted";
                        break;
                    default:
                        break;
                }
            }
        }

        [Serializable]
        public enum DeviceTokenType
        {
            [EnumMember]
            iOS = 1,

            [EnumMember]
            Android = 2
        }

        public UserSelectBasicResponse SelectUserBasic(string userId,
                                                       string companyId,
                                                       bool checkForValid,
                                                       ISession session = null,
                                                       string containsName = null,
                                                       bool isIncludeDepartment = false)
        {
            UserSelectBasicResponse response = new UserSelectBasicResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager conn_manager = new ConnectionManager();
                    session = conn_manager.getMainSession();
                }
                PreparedStatement psUser = null;
                BoundStatement bsUser = null;

                bool isUserAdmin = false;
                AccountStatus status = new AccountStatus(User.AccountStatus.CODE_ACTIVE);

                if (!checkForValid)
                {
                    if (userId.Equals(companyId))
                    {
                        psUser = session.Prepare(CQLGenerator.SelectStatement("company",
                            new List<string>(), new List<string> { "id" }));
                        bsUser = psUser.Bind(userId);

                        isUserAdmin = true;
                    }
                    else
                    {
                        PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                            new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsAuthentication = psAuthentication.Bind(userId);
                        Row authenticationRow = session.Execute(bsAuthentication).FirstOrDefault();

                        if (authenticationRow != null)
                        {
                            status = new AccountStatus(authenticationRow.GetValue<int>("account_status"));
                        }

                        psUser = session.Prepare(CQLGenerator.SelectStatement("user_basic",
                        new List<string>(), new List<string> { "id" }));
                        bsUser = psUser.Bind(userId);
                    }

                }
                else
                {
                    PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                        new List<string>(), new List<string> { "user_id", "account_status" }));
                    BoundStatement bsAuthentication = psAuthentication.Bind(userId, User.AccountStatus.CODE_ACTIVE);
                    Row authenticationRow = session.Execute(bsAuthentication).FirstOrDefault();

                    if (authenticationRow != null)
                    {
                        psUser = session.Prepare(CQLGenerator.SelectStatement("user_basic",
                            new List<string>(), new List<string> { "id" }));
                        bsUser = psUser.Bind(userId);
                    }

                }

                Row userRow = null;

                if (bsUser != null)
                {
                    userRow = session.Execute(bsUser).FirstOrDefault();
                }


                if (userRow != null)
                {
                    if (!isUserAdmin)
                    {
                        string firstName = userRow["first_name"].ToString();
                        string lastName = userRow["last_name"].ToString();
                        string fullName = firstName + " " + lastName;

                        bool isFound = true;

                        if (!string.IsNullOrEmpty(containsName))
                        {
                            isFound = fullName.ToLower().Contains(containsName.ToLower());
                        }

                        if (isFound)
                        {
                            string profileImageUrl = userRow["profile_image_url"].ToString();

                            List<Department> departments = new List<Department>();
                            if (isIncludeDepartment)
                            {
                                Department departmentManager = new Department();
                                departments = departmentManager.GetAllDepartmentByUserId(userId, companyId, session).Departments;
                            }

                            DateTime? birthday = null;
                            try
                            {
                                if (userRow["date_of_birth"] != null)
                                {
                                    birthday = userRow.GetValue<DateTime>("date_of_birth");
                                }

                                if (birthday < new DateTime(1700, 1, 1))
                                {
                                    birthday = null;
                                }
                            }
                            catch (Exception)
                            {
                                birthday = null;
                            }

                            response.User = new User
                            {
                                UserId = userId,
                                FirstName = firstName,
                                LastName = lastName,
                                ProfileImageUrl = profileImageUrl,
                                Email = userRow["email"].ToString(),
                                Departments = departments,
                                Status = status,
                                Job = new Company().SelectUserJob(companyId, userId, session, userRow.GetValue<string>("job_level_id")),
                                AgeBand = GetAgeBand(birthday)
                            };
                        }
                    }
                    else
                    {
                        string firstName = DefaultResource.AdminProfileName;
                        string profileImageUrl = userRow["admin_profile_image_url"].ToString();

                        response.User = new User
                        {
                            UserId = userId,
                            FirstName = firstName,
                            ProfileImageUrl = profileImageUrl
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private string GetAgeBand(DateTime? birthday)
        {
            if (birthday == null)
            {
                return "NA";
            }
            else
            {
                int age = DateTime.UtcNow.Year - birthday.Value.Year;
                if (birthday > DateTime.UtcNow.AddYears(-age))
                {
                    age--;
                }

                if (age < 21)
                {
                    return "< 21";
                }
                else if (age > 20 && age < 26)
                {
                    return "21 - 25";
                }
                else if (age > 25 && age < 31)
                {
                    return "26 - 30";
                }
                else if (age > 30 && age < 36)
                {
                    return "31 - 35";
                }
                else if (age > 35 && age < 41)
                {
                    return "36 - 40";
                }
                else if (age > 40 && age < 46)
                {
                    return "41 - 45";
                }
                else if (age > 45 && age < 51)
                {
                    return "46 - 50";
                }
                else if (age > 50 && age < 56)
                {
                    return "51 - 55";
                }
                else if (age > 55 && age < 61)
                {
                    return "56 - 60";
                }
                else if (age > 60 && age < 66)
                {
                    return "61 - 65";
                }
                else if (age > 65 && age < 71)
                {
                    return "66 - 70";
                }
                else
                {
                    return "> 70";
                }
            }
        }

#warning Deprecated code in SelectUserWithTokenAndCompany, replaced by SelectUserWithCompany
        public UserSelectTokenWithCompanyResponse SelectUserWithTokenAndCompany(String userId, String companyId)
        {
            UserSelectTokenWithCompanyResponse response = new UserSelectTokenWithCompanyResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow != null)
                {
                    Row userRow = vh.ValidateUser(userId, companyId, session);

                    if (userRow != null)
                    {
                        //Authenticator authenticator = new Authenticator();
                        //authenticator.CreateAuthenticationToken(companyId, userId, session);

                        string userToken = DateHelper.ConvertDateToLong(DateTime.UtcNow).ToString();

                        if (!string.IsNullOrEmpty(userToken))
                        {
                            if (userRow["joined_on_timestamp"] == null)
                            {
                                PreparedStatement psUser = session.Prepare(CQLGenerator.UpdateStatement("user_basic",
                                    new List<string> { "id" }, new List<string> { "joined_on_timestamp" }, new List<string>()));
                                session.Execute(psUser.Bind(DateTime.UtcNow, userId));
                            }

                            string companyTitle = companyRow.GetValue<string>("title");
                            string companyLogoUrl = companyRow["logo_url"] == null ? DefaultResource.CompanyLogoUrl : companyRow.GetValue<string>("logo_url");
                            string companyBannerUrl = companyRow["client_banner_url"] == null ? DefaultResource.CompanyClientBannerUrl : companyRow.GetValue<string>("client_banner_url");
                            string matchUpBannerUrl = companyRow["client_matchup_banner_url"] == null ? DefaultResource.CompanyMatchupBannerUrl : companyRow.GetValue<string>("client_matchup_banner_url");
                            string profilePopupBannerUrl = companyRow["client_profile_popup_banner_url"] == null ? DefaultResource.CompanyProfilePopupUrl : companyRow.GetValue<string>("client_profile_popup_banner_url");

                            Company company = new Company
                            {
                                CompanyId = companyId,
                                CompanyTitle = companyTitle,
                                CompanyLogoUrl = companyLogoUrl,
                                CompanyBannerUrl = companyBannerUrl,
                                MatchupBannerUrl = matchUpBannerUrl,
                                ProfilePopupBannerUrl = profilePopupBannerUrl
                            };

                            string firstName = userRow.GetValue<string>("first_name");
                            string lastName = userRow.GetValue<string>("last_name");
                            string email = userRow.GetValue<string>("email");
                            string profileImageUrl = userRow.GetValue<string>("profile_image_url");

                            List<Department> departments = new Department().GetAllDepartmentByUserId(userId, companyId, session).Departments;

                            User user = new User
                            {
                                UserId = userId,
                                FirstName = firstName,
                                LastName = lastName,
                                Email = email,
                                ProfileImageUrl = profileImageUrl,
                                Company = company,
                                UserToken = userToken,
                                Departments = departments
                            };

                            response.User = user;
                            response.Success = true;

                            AnalyticUserActivity analytic = new AnalyticUserActivity();
                            analytic.UpdateUserLogin(userId, companyId, true);
                        }

                    }
                    else
                    {
                        Log.Error("Invalid userId: " + userId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectWithCompanyResponse SelectUserWithCompany(string userId, string companyId)
        {
            UserSelectWithCompanyResponse response = new UserSelectWithCompanyResponse();
            response.User = new User();
            response.UnlockedModules = new List<Module>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow != null)
                {
                    Row userRow = vh.ValidateUser(userId, companyId, session);

                    if (userRow != null)
                    {
                        if (userRow["joined_on_timestamp"] == null)
                        {
                            PreparedStatement psUser = session.Prepare(CQLGenerator.UpdateStatement("user_basic",
                                new List<string> { "id" }, new List<string> { "joined_on_timestamp" }, new List<string>()));
                            session.Execute(psUser.Bind(DateTime.UtcNow, userId));
                        }

                        string companyTitle = companyRow.GetValue<string>("title");
                        string companyLogoUrl = companyRow["logo_url"] == null ? DefaultResource.CompanyLogoUrl : companyRow.GetValue<string>("logo_url");
                        string companyBannerUrl = companyRow["client_banner_url"] == null ? DefaultResource.CompanyClientBannerUrl : companyRow.GetValue<string>("client_banner_url");
                        string matchUpBannerUrl = companyRow["client_matchup_banner_url"] == null ? DefaultResource.CompanyMatchupBannerUrl : companyRow.GetValue<string>("client_matchup_banner_url");
                        string profilePopupBannerUrl = companyRow["client_profile_popup_banner_url"] == null ? DefaultResource.CompanyProfilePopupUrl : companyRow.GetValue<string>("client_profile_popup_banner_url");
                        string pullRefreshBannerUrl = companyRow["client_pull_refresh_banner_url"] == null ? DefaultResource.CompanyClientPullRefreshImageUrl : companyRow.GetValue<string>("client_pull_refresh_banner_url");
                        string email = userRow.GetValue<string>("email");

                        bool isPasswordDefault = false;

                        // Check if password is default
                        PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                            new List<string>(), new List<string> { "email", "user_id", "company_id" }));
                        Row authenticationRow = session.Execute(ps.Bind(email, userId, companyId)).FirstOrDefault();
                        if (authenticationRow != null)
                        {
                            isPasswordDefault = authenticationRow.GetValue<bool>("is_default");
                        }

                        Company company = new Company
                        {
                            CompanyId = companyId,
                            CompanyTitle = companyTitle,
                            CompanyLogoUrl = companyLogoUrl,
                            CompanyBannerUrl = companyBannerUrl,
                            MatchupBannerUrl = matchUpBannerUrl,
                            ProfilePopupBannerUrl = profilePopupBannerUrl,
                            CompanyPullRefreshBannerUrl = pullRefreshBannerUrl,
                            IsPasswordDefault = isPasswordDefault
                        };

                        string firstName = userRow.GetValue<string>("first_name");
                        string lastName = userRow.GetValue<string>("last_name");
                        string profileImageUrl = userRow.GetValue<string>("profile_image_url");
                        int? gender;
                        if (userRow["gender"] == null)
                        {
                            gender = null;
                        }
                        else
                        {
                            gender = userRow.GetValue<int>("gender");
                        }

                        DateTimeOffset? birthday;
                        string bdString = null;
                        if (userRow["date_of_birth"] == null)
                        {
                            birthday = null;
                            bdString = null;
                        }
                        else
                        {
                            birthday = userRow.GetValue<DateTimeOffset>("date_of_birth");
                            DateTimeOffset bd = userRow.GetValue<DateTimeOffset>("date_of_birth");
                            JsonSerializerSettings jss = new JsonSerializerSettings
                            {
                                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                            };
                            bdString = JsonConvert.SerializeObject(bd, jss).Replace("\\", "").Replace("\"", "");
                        }


                        List<Department> departments = new Department().GetAllDepartmentByUserId(userId, companyId, session).Departments;

                        User user = new User
                        {
                            UserId = userId,
                            FirstName = firstName,
                            LastName = lastName,
                            Email = email,
                            ProfileImageUrl = profileImageUrl,
                            Company = company,
                            Departments = departments,
                            Gender = gender,
                            Birthday = birthday,
                            BirthdayString = bdString,
                            Groups = new Group().SelectAllGroupsByUser(userId, companyId, session).Groups
                        };

                        #region Get user contact
                        ps = session.Prepare(CQLGenerator.SelectStatement("user_contact", new List<string> { }, new List<string> { "id" }));
                        Row contactRow = session.Execute(ps.Bind(userId)).FirstOrDefault();
                        if (contactRow != null)
                        {
                            user.Address = contactRow.GetValue<String>("address");
                            user.AddressCountryName = contactRow.GetValue<String>("address_country_name");
                            user.AddressPostalCode = contactRow.GetValue<String>("address_postal_code");
                            user.Phone = contactRow.GetValue<String>("phone");
                            user.PhoneCountryCode = contactRow.GetValue<String>("phone_country_code");
                            user.PhoneCountryAbb = contactRow.GetValue<String>("phone_country_name");
                        }
                        #endregion

                        response.UnlockedModules = new Module().GetAllModules(companyId, session);
                        response.User = user;
                        response.Success = true;

                        AnalyticUserActivity analytic = new AnalyticUserActivity();
                        analytic.UpdateUserLogin(userId, companyId, true);

                    }
                    else
                    {
                        Log.Error("Invalid userId: " + userId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }
                }
                else
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectUpdateResponse SelectUpdatedDetail(string requesterUserId, string companyId)
        {
            UserSelectUpdateResponse response = new UserSelectUpdateResponse();
            response.User = new User();
            response.Company = new Company();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow == null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }


                string companyTitle = companyRow.GetValue<string>("title");
                string companyLogoUrl = companyRow.GetValue<string>("logo_url");
                string companyBannerUrl = companyRow.GetValue<string>("client_banner_url");
                string matchUpBannerUrl = companyRow.GetValue<string>("client_matchup_banner_url");
                string profilePopupBannerUrl = companyRow.GetValue<string>("client_profile_popup_banner_url");

                Company company = new Company
                {
                    CompanyId = companyId,
                    CompanyTitle = companyTitle,
                    CompanyLogoUrl = companyLogoUrl,
                    CompanyBannerUrl = companyBannerUrl,
                    MatchupBannerUrl = matchUpBannerUrl,
                    ProfilePopupBannerUrl = profilePopupBannerUrl
                };

                string firstName = userRow.GetValue<string>("first_name");
                string lastName = userRow.GetValue<string>("last_name");
                string email = userRow.GetValue<string>("email");
                string profileImageUrl = userRow.GetValue<string>("profile_image_url");

                List<Department> departments = new Department().GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;

                User user = new User
                {
                    UserId = requesterUserId,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email,
                    ProfileImageUrl = profileImageUrl,
                    Company = company,
                    Departments = departments
                };

                response.User = user;
                response.Company = company;
                response.Success = true;


            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectAllByDepartmentResponse SelectAllUsersSortedByDepartment(string requesterUserId, string companyId, ISession session = null)
        {
            UserSelectAllByDepartmentResponse response = new UserSelectAllByDepartmentResponse();
            response.Departments = new List<Department>();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                response = CacheHelper.CheckCacheForColleagues(companyId);

                if (response == null)
                {
                    Log.Debug("Cache for colleague not hit, need search thru DB");
                    response = new UserSelectAllByDepartmentResponse();
                    response.Departments = new List<Department>();
                    response.Success = false;

                    List<Department> departments = new Department().GetAllDepartment(null, companyId, Department.QUERY_TYPE_BASIC, session).Departments;

                    foreach (Department department in departments)
                    {
                        department.Users = new List<User>();

                        PreparedStatement psColleagues = session.Prepare(CQLGenerator.SelectStatement("user_by_department",
                            new List<string> { "user_id", "position" }, new List<string> { "department_id" }));
                        BoundStatement bsColleagues = psColleagues.Bind(department.Id);
                        RowSet colleagueRowset = session.Execute(bsColleagues);

                        foreach (Row colleagueRow in colleagueRowset)
                        {
                            string userId = colleagueRow.GetValue<string>("user_id");
                            User colleague = SelectUserBasic(userId, companyId, true, session, null, true).User;

                            if (colleague != null)
                            {
                                colleague.Position = colleagueRow.GetValue<string>("position");
                                department.Users.Add(colleague);
                            }
                        }
                        response.Departments.Add(department);
                    }

                    // update cache with new data
                    CacheHelper.UpdateCacheForColleagues(companyId, response);
                }
                else
                {
                    //Log.Debug("Cache for colleague hit");
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public List<string> SelectAllUserIdsByDepartmentIds(List<string> departmentIds, string requesterUserId, string companyId, ISession session, string startsWithName = null)
        {
            List<string> userIds = new List<string>();

            try
            {
                foreach (string departmentId in departmentIds)
                {
                    PreparedStatement psColleagues = session.Prepare(CQLGenerator.SelectStatement("user_by_department",
                        new List<string> { "user_id", "position" }, new List<string> { "department_id" }));
                    BoundStatement bsColleagues = psColleagues.Bind(departmentId);
                    RowSet colleagueRowset = session.Execute(bsColleagues);

                    foreach (Row colleagueRow in colleagueRowset)
                    {
                        string userId = colleagueRow.GetValue<string>("user_id");
                        string position = colleagueRow.GetValue<string>("position");
                        if (!userIds.Contains(userId) && !userId.Equals(requesterUserId))
                        {
                            userIds.Add(userId);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return userIds;
        }

        public UserSelectAllBasicResponse SelectAllUsersByDepartmentIds(List<string> departmentIds, string requesterUserId, string companyId, ISession session, string containsName = null, bool isExcludeRequesterUser = true, bool isIncludeDepartment = true)
        {
            UserSelectAllBasicResponse response = new UserSelectAllBasicResponse();
            response.Users = new List<User>();
            response.Success = false;
            try
            {
                foreach (string departmentId in departmentIds)
                {
                    PreparedStatement psColleagues = session.Prepare(CQLGenerator.SelectStatement("user_by_department",
                        new List<string> { "user_id", "position" }, new List<string> { "department_id" }));
                    BoundStatement bsColleagues = psColleagues.Bind(departmentId);
                    RowSet colleagueRowset = session.Execute(bsColleagues);

                    foreach (Row colleagueRow in colleagueRowset)
                    {
                        string userId = colleagueRow.GetValue<string>("user_id");
                        string position = colleagueRow.GetValue<string>("position");
                        if (!response.Users.Any(x => x.UserId == userId))
                        {
                            if (isExcludeRequesterUser)
                            {
                                if (!userId.Equals(requesterUserId))
                                {
                                    User colleague = SelectUserBasic(userId, companyId, true, session, containsName, isIncludeDepartment).User;
                                    if (colleague != null)
                                    {
                                        colleague.Position = position;
                                        response.Users.Add(colleague);
                                    }
                                }
                            }
                            else
                            {
                                User colleague = SelectUserBasic(userId, companyId, true, session, containsName, isIncludeDepartment).User;
                                if (colleague != null)
                                {
                                    colleague.Position = position;
                                    response.Users.Add(colleague);
                                }
                            }
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectAllBasicResponse SelectAllUsersByDepartments(List<Department> departments, string requesterUserId, string companyId, ISession session, string containsName = null, bool isExcludeRequesterUser = true)
        {
            UserSelectAllBasicResponse response = new UserSelectAllBasicResponse();
            response.Users = new List<User>();
            response.Success = false;
            try
            {
                foreach (Department department in departments)
                {
                    PreparedStatement psColleagues = session.Prepare(CQLGenerator.SelectStatement("user_by_department",
                        new List<string> { "user_id", "position" }, new List<string> { "department_id" }));
                    BoundStatement bsColleagues = psColleagues.Bind(department.Id);
                    RowSet colleagueRowset = session.Execute(bsColleagues);

                    foreach (Row colleagueRow in colleagueRowset)
                    {
                        string userId = colleagueRow.GetValue<string>("user_id");
                        string position = colleagueRow.GetValue<string>("position");
                        if (!response.Users.Any(x => x.UserId == userId))
                        {
                            if (isExcludeRequesterUser)
                            {
                                if (!userId.Equals(requesterUserId))
                                {
                                    User colleague = SelectUserBasic(userId, companyId, true, session, containsName).User;
                                    if (colleague != null)
                                    {
                                        colleague.Position = position;
                                        response.Users.Add(colleague);
                                    }
                                }
                            }
                            else
                            {
                                User colleague = SelectUserBasic(userId, companyId, true, session, containsName).User;
                                if (colleague != null)
                                {
                                    colleague.Position = position;
                                    response.Users.Add(colleague);
                                }
                            }
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserListResponse GetAllUserForAdmin(String adminUserId, String companyId, String departmentId, int userTypeCode, int userStatusCode, List<String> searchKeys, bool isPendingInvite = false, bool isPendingLogin = false, DateTime? createdTimestamp = null, ISession session = null)
        {
            UserListResponse response = new UserListResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    #region Step 1. Check data.
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                    #region Step 1.1 Check Admin account's validation.
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                    #endregion
                    #endregion
                }


                #region Step 2. Read database.
                List<User> queryUsers = new List<User>();

                #region Step 2.1 Filter user by department, account type and account status.
                PreparedStatement ps_department_by_company;
                BoundStatement bs_department_by_company;
                if (String.IsNullOrEmpty(departmentId))
                {
                    ps_department_by_company = session.Prepare(CQLGenerator.SelectStatement("department_by_company", new List<string> { }, new List<string> { "company_id" }));
                    bs_department_by_company = ps_department_by_company.Bind(companyId);

                }
                else
                {
                    ps_department_by_company = session.Prepare(CQLGenerator.SelectStatement("department_by_company", new List<string> { }, new List<string> { "company_id", "department_id" }));
                    bs_department_by_company = ps_department_by_company.Bind(companyId, departmentId);
                }
                RowSet rowsDepartment = session.Execute(bs_department_by_company);
                if (rowsDepartment != null)
                {
                    foreach (Row rowDepartment in rowsDepartment.GetRows())
                    {
                        PreparedStatement ps_user_by_department = session.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id" })); ;
                        BoundStatement bs_user_by_department = ps_user_by_department.Bind(rowDepartment.GetValue<String>("department_id"));
                        RowSet rowsUser = session.Execute(bs_user_by_department);
                        if (rowsUser != null)
                        {
                            foreach (Row rowUser in rowsUser)
                            {
                                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" })); ;
                                BoundStatement bs_user_account_type = ps_user_account_type.Bind(rowUser.GetValue<String>("user_id"));
                                Row row = session.Execute(bs_user_account_type).FirstOrDefault();
                                if (row != null)
                                {
                                    User user = new User();
                                    user.Company = new Company { CompanyId = companyId };
                                    user.Departments = new List<Department>();
                                    user.Departments.Add(new Department { Id = rowUser.GetValue<String>("department_id"), Position = rowUser.GetValue<String>("position") });
                                    user.UserId = row.GetValue<String>("user_id");
                                    user.Type = new AccountType(row.GetValue<int>("account_type"));
                                    user.Status = new AccountStatus(row.GetValue<int>("account_status"));
                                    user.LastModifiedStatusTimestamp = row.GetValue<DateTimeOffset>("last_modified_timestamp");

                                    if (userTypeCode != 0 && userStatusCode != 0)
                                    {
                                        if (row.GetValue<int>("account_type") == userTypeCode && row.GetValue<int>("account_status") == userStatusCode)
                                        {
                                            if (!queryUsers.Contains(user))
                                            {
                                                queryUsers.Add(user);
                                            }
                                        }
                                    }
                                    else if (userTypeCode != 0 && userStatusCode == 0)
                                    {
                                        if (row.GetValue<int>("account_type") == userTypeCode)
                                        {
                                            if (!queryUsers.Contains(user))
                                            {
                                                queryUsers.Add(user);
                                            }
                                        }
                                    }
                                    else if (userTypeCode == 0 && userStatusCode != 0)
                                    {
                                        if (row.GetValue<int>("account_status") == userStatusCode)
                                        {
                                            if (!queryUsers.Contains(user))
                                            {
                                                queryUsers.Add(user);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!queryUsers.Contains(user))
                                        {
                                            queryUsers.Add(user);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region Step 2.2 Filter user searchKeys.
                String searchKey = String.Empty;
                List<int> removeIndex = new List<int>();
                if (searchKeys != null && searchKeys.Count > 0)
                {
                    for (int i = 0; i < searchKeys.Count; i++)
                    {
                        if (i == 0)
                        {
                            searchKey = searchKeys[i];
                        }
                        else
                        {
                            searchKey = searchKey + " " + searchKeys[i];
                        }
                    }
                }
                #endregion

                #region Step 2.3 Get data of users.
                List<Company.CompanyJob> jobs = new Company().SelectJobs(companyId, adminUserId, (int)Company.CompanyJobQueryType.Basic, session).Jobs;
                for (int i = 0; i < queryUsers.Count; i++)
                {
                    PreparedStatement ps_user_basic = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" })); ;
                    BoundStatement bs_user_basic = ps_user_basic.Bind(queryUsers[i].UserId);
                    Row row = session.Execute(bs_user_basic).FirstOrDefault();
                    if (row != null)
                    {
                        // Check for those who have not received invitation
                        if (isPendingInvite)
                        {
                            if (row.GetValue<bool>("is_email_sent"))
                            {
                                queryUsers.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                        // Check for those who have not login
                        if (isPendingLogin)
                        {
                            if (row["joined_on_timestamp"] != null)
                            {
                                queryUsers.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                        // Check for created date
                        if(createdTimestamp != null)
                        {
                            DateTime userCreatedOn = row.GetValue<DateTime>("created_on_timestamp");
                            if (userCreatedOn > createdTimestamp)
                            {
                                queryUsers.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }

                        if (String.IsNullOrEmpty(searchKey))
                        {
                            queryUsers[i].FirstName = row.GetValue<String>("first_name");
                            queryUsers[i].LastName = row.GetValue<String>("last_name");
                            queryUsers[i].ProfileImageUrl = row.GetValue<String>("profile_image_url");
                            queryUsers[i].Email = row.GetValue<String>("email");
                            queryUsers[i].LastModifiedProfileTimestamp = row.GetValue<DateTimeOffset>("last_modified_timestamp");

                            queryUsers[i].IsEmailSent = row.GetValue<bool>("is_email_sent");
                            queryUsers[i].HasLogin = row["joined_on_timestamp"] == null ? false : true;

                            string jobId = row.GetValue<string>("job_level_id");
                            queryUsers[i].Job = jobs.FirstOrDefault(j => j.JobId.Equals(jobId));

                            for (int j = 0; j < queryUsers[i].Departments.Count; j++)
                            {
                                PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" })); ;
                                BoundStatement bs_department = ps_department.Bind(companyId, queryUsers[i].Departments[j].Id, true);
                                Row row_department = session.Execute(bs_department).FirstOrDefault();
                                if (row_department != null)
                                {
                                    queryUsers[i].Departments[j].Title = row_department.GetValue<String>("title");
                                }
                            }
                        }
                        else
                        {
                            String fName_lName = row.GetValue<String>("first_name").ToLower() + " " + row.GetValue<String>("last_name").ToLower();
                            String lName_fName = row.GetValue<String>("last_name").ToLower() + " " + row.GetValue<String>("first_name").ToLower();
                            String email = row.GetValue<String>("email").ToLower();
                            if (fName_lName.Contains(searchKey.ToLower()) || lName_fName.Contains(searchKey.ToLower()) || email.Contains(searchKey.ToLower()))
                            {
                                queryUsers[i].FirstName = row.GetValue<String>("first_name");
                                queryUsers[i].LastName = row.GetValue<String>("last_name");
                                queryUsers[i].ProfileImageUrl = row.GetValue<String>("profile_image_url");
                                queryUsers[i].Email = row.GetValue<String>("email");
                                queryUsers[i].LastModifiedProfileTimestamp = row.GetValue<DateTimeOffset>("last_modified_timestamp");

                                queryUsers[i].IsEmailSent = row.GetValue<bool>("is_email_sent");
                                queryUsers[i].HasLogin = row["joined_on_timestamp"] == null ? false : true;

                                string jobId = row.GetValue<string>("job_level_id");
                                queryUsers[i].Job = jobs.FirstOrDefault(j => j.JobId.Equals(jobId));

                                for (int j = 0; j < queryUsers[i].Departments.Count; j++)
                                {
                                    PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" })); ;
                                    BoundStatement bs_department = ps_department.Bind(companyId, queryUsers[i].Departments[j].Id, true);
                                    Row row_department = session.Execute(bs_department).FirstOrDefault();
                                    if (row_department != null)
                                    {
                                        queryUsers[i].Departments[j].Title = row_department.GetValue<String>("title");
                                    }
                                }
                            }
                            else
                            {
                                queryUsers.RemoveAt(i);
                                i--;
                            }
                        }
                    }
                }

                response.Users = queryUsers;
                response.Success = true;
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserListResponse GetAdmin(String adminUserId, String companyId)
        {
            UserListResponse response = new UserListResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.





                RowSet rowSet = session.Execute(@"
    SELECT user_id FROM department_by_user WHERE department_id IN 
        (SELECT department_id FROM department_by_company WHERE company_id ='" + companyId + @"')
    ;");

                //                RowSet rowSet = session.Execute(@"
                //SELECT * FROM user_account_type WHERE account_type = 3 AND user_id IN 
                //    (SELECT user_id FROM department_by_user WHERE department_id IN 
                //        (SELECT department_id FROM department_by_company WHERE company_id ='" + companyId + @"')
                //    );");
                if (rowSet != null)
                {
                    List<User> users = new List<User>();
                    foreach (Row row in rowSet)
                    {
                        users.Add(new User { UserId = row.GetValue<String>("user_id") });
                    }
                    response.Users = users;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.UserNoAdmin);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserNoAdmin);
                    response.ErrorMessage = ErrorMessage.UserNoAdmin;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserDetailResponse GetUserDetail(String adminUserId, String companyId, String userId, ISession mainSession = null)
        {
            UserDetailResponse response = new UserDetailResponse();
            response.Success = false;
            try
            {
                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                #region Step 1. Check data.
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.
                User user = new User();

                #region company information of user
                PreparedStatement ps_company = mainSession.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_company = ps_company.Bind(companyId);
                Row row_company = mainSession.Execute(bs_company).FirstOrDefault();
                if (row_company != null)
                {
                    Company company = new Company();
                    company.CompanyId = companyId;
                    company.CompanyTitle = row_company.GetValue<String>("title");
                    company.CompanyLogoUrl = row_company.GetValue<String>("logo_url");
                    user.Company = company;
                }
                else
                {
                    Log.Error(ErrorMessage.CompanyInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }
                #endregion

                #region departments of user
                List<Department> departments = new List<Department>();
                PreparedStatement ps_department_by_user = mainSession.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" })); ;
                BoundStatement bs_department_by_user = ps_department_by_user.Bind(userId);
                RowSet rowSet = mainSession.Execute(bs_department_by_user);
                foreach (Row r in rowSet)
                {
                    Department department = new Department();
                    PreparedStatement ps_department = mainSession.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" })); ;
                    BoundStatement bs_department = ps_department.Bind(companyId, r.GetValue<String>("department_id"), true);
                    Row row_department = mainSession.Execute(bs_department).FirstOrDefault();
                    if (row_department == null)
                    {
                        Log.Error(ErrorMessage.DepartmentIsInvalid);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentIsInvalid);
                        response.ErrorMessage = ErrorMessage.DepartmentIsInvalid;
                        return response;
                    }
                    else
                    {
                        department.Id = row_department.GetValue<String>("id");
                        department.Title = row_department.GetValue<String>("title");
                    }

                    PreparedStatement ps_user_by_department = mainSession.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id", "user_id" })); ;
                    BoundStatement bs_user_by_department = ps_user_by_department.Bind(r.GetValue<String>("department_id"), userId);
                    Row row_user_by_department = mainSession.Execute(bs_user_by_department).FirstOrDefault();
                    if (row_user_by_department != null)
                    {
                        department.Position = row_user_by_department.GetValue<String>("position");
                    }
                    departments.Add(department);
                }
                user.Departments = departments;
                #endregion

                #region account type of user
                PreparedStatement ps_user_account_type = mainSession.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs_user_account_type = ps_user_account_type.Bind(userId);
                Row row_user_account_type = mainSession.Execute(bs_user_account_type).FirstOrDefault();
                if (row_user_account_type != null)
                {
                    user.Type = new AccountType(row_user_account_type.GetValue<int>("account_type"));
                    user.Status = new AccountStatus(row_user_account_type.GetValue<int>("account_status"));
                    user.LastModifiedStatusTimestamp = row_user_account_type.GetValue<DateTimeOffset>("last_modified_timestamp");

                    // Get access rights
                    if (row_user_account_type.GetValue<int>("account_type") == AccountType.CODE_MODERATER)
                    {
                        PreparedStatement ps_moderator_access_rights = mainSession.Prepare(CQLGenerator.SelectStatement("moderator_access_rights", new List<string> { }, new List<string> { "user_id" }));
                        BoundStatement bs_moderator_access_rights = ps_moderator_access_rights.Bind(userId);
                        RowSet rowSet_moderator_access_rights = mainSession.Execute(bs_moderator_access_rights);
                        if (rowSet_moderator_access_rights != null)
                        {
                            List<Module> accessModules = new List<Module>();
                            foreach (Row rowRights in rowSet_moderator_access_rights)
                            {
                                accessModules.Add(new Module(rowRights.GetValue<int>("access_rights_key")));
                            }
                            user.AccessModules = accessModules;
                        }

                        // If user is a moderator. Get the expired date.
                        PreparedStatement ps_moderator_access_management = mainSession.Prepare(CQLGenerator.SelectStatement("moderator_access_management", new List<string> { }, new List<string> { "user_id" }));
                        Row row = mainSession.Execute(ps_moderator_access_management.Bind(userId)).FirstOrDefault();
                        if (row != null)
                        {
                            if (row["expired_timestamp"] == null)
                            {
                                user.ModeratorExpiredDate = null;
                            }
                            else
                            {
                                user.ModeratorExpiredDate = DateHelper.ConvertDateToTimezoneSpecific(row.GetValue<DateTime>("expired_timestamp"), companyId, mainSession);
                            }
                        }
                        else
                        {
                            Log.Warn("Cannot get data at moderator_access_management table. The userId is " + userId);
                        }
                    }
                    else if (row_user_account_type.GetValue<int>("account_type") == AccountType.CODE_ADMIN)
                    {
                        user.AccessModules = Module.GetModules(AccountType.CODE_ADMIN);
                    }
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region basic information of user
                PreparedStatement ps_user_basic = mainSession.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_basic = ps_user_basic.Bind(userId);
                Row row_user_basic = mainSession.Execute(bs_user_basic).FirstOrDefault();
                if (row_user_basic != null)
                {
                    user.UserId = userId;
                    user.Email = row_user_basic.GetValue<String>("email");
                    user.FirstName = row_user_basic.GetValue<String>("first_name");
                    user.LastName = row_user_basic.GetValue<String>("last_name");
                    if (row_user_basic["gender"] == null)
                    {
                        user.Gender = null;
                    }
                    else
                    {
                        user.Gender = row_user_basic.GetValue<int>("gender");
                    }

                    if (row_user_basic["date_of_birth"] == null)
                    {
                        user.Birthday = null;
                    }
                    else
                    {
                        user.Birthday = row_user_basic.GetValue<DateTimeOffset>("date_of_birth");
                    }

                    user.ProfileImageUrl = row_user_basic.GetValue<String>("profile_image_url");
                    user.LastModifiedProfileTimestamp = row_user_basic.GetValue<DateTimeOffset>("last_modified_timestamp");

                    user.Job = new Company().SelectUserJob(companyId, userId, mainSession, row_user_basic.GetValue<string>("job_level_id"));
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region contact information of user
                PreparedStatement ps_user_contact = mainSession.Prepare(CQLGenerator.SelectStatement("user_contact", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_contact = ps_user_contact.Bind(userId);
                Row row_user_contact = mainSession.Execute(bs_user_contact).FirstOrDefault();
                if (row_user_contact != null)
                {
                    user.Address = row_user_contact.GetValue<String>("address");
                    user.AddressCountryName = row_user_contact.GetValue<String>("address_country_name");
                    user.AddressPostalCode = row_user_contact.GetValue<String>("address_postal_code");
                    user.Phone = row_user_contact.GetValue<String>("phone");
                    user.PhoneCountryCode = row_user_contact.GetValue<String>("phone_country_code");
                    user.PhoneCountryAbb = row_user_contact.GetValue<String>("phone_country_name");
                }
                #endregion

                response.User = user;
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectAllBasicResponse SelectAllRecentlyPlayedUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId)
        {
            UserSelectAllBasicResponse response = new UserSelectAllBasicResponse();
            response.Users = new List<User>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                Topic topicManager = new Topic();
                if (!topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, session))
                {
                    Log.Error("Topic privacy has been updated: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicPrivacyNotAllowed);
                    response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowed;
                    return response;
                }

                // Get all recently played opponents
                List<User> recentlyPlayedOpponents = new List<User>();
                List<string> recentlyPlayedOpponentIds = new List<string>();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("challenge_history_by_user_desc",
                       new List<string>(), new List<string> { "user_id" }));
                RowSet recentOpponentRowset = session.Execute(ps.Bind(requesterUserId));

                int opponentForTopicLimit = Convert.ToInt16(WebConfigurationManager.AppSettings["opponent_for_topic_limit"]);

                foreach (Row recentOpponentRow in recentOpponentRowset)
                {
                    string userId = recentOpponentRow.GetValue<string>("opponent_user_id");

                    if (topicManager.CheckPrivacyForTopic(userId, companyId, topicId, session))
                    {
                        if (recentlyPlayedOpponents.FindIndex(user => user.UserId.Equals(userId)) < 0 && recentlyPlayedOpponents.Count < opponentForTopicLimit)
                        {
                            User user = new User().SelectUserBasic(userId, companyId, false, session).User;
                            if (user != null)
                            {
                                recentlyPlayedOpponents.Add(user);
                                recentlyPlayedOpponentIds.Add(user.UserId);
                            }
                        }
                    }

                }

                //// Fill up the blanks
                //if(recentlyPlayedOpponents.Count < opponentForTopicLimit)
                //{
                //    List<User> randomUsers = new List<User>();
                //    int remainingCount = opponentForTopicLimit - recentlyPlayedOpponents.Count;
                //    ps = session.Prepare(CQLGenerator.SelectStatement("topic_privacy", new List<string> { }, new List<string> { "company_id", "topic_id" }));
                //    Row topicPrivacy = session.Execute(ps.Bind(companyId, topicId)).FirstOrDefault();

                //    if (topicPrivacy != null)
                //    {
                //        bool isForEveryone = topicPrivacy.GetValue<bool>("is_for_everyone");
                //        bool isForDepartment = topicPrivacy.GetValue<bool>("is_for_department");
                //        bool isForUser = topicPrivacy.GetValue<bool>("is_for_user");

                //        if (isForEveryone)
                //        {
                //            Department departmentManager = new Department();
                //            List<Department> departments = departmentManager.GetAllDepartment(null, companyId, Department.QUERY_TYPE_BASIC, session).Departments;
                //            if (departments != null)
                //            {
                //                randomUsers = SelectAllUsersByDepartments(departments, requesterUserId, companyId, session, null).Users;
                //                randomUsers = randomUsers.Where(i => !recentlyPlayedOpponentIds.Contains(i.UserId)).ToList(); 
                //            }
                //        }
                //        else
                //        {
                //            if (isForDepartment)
                //            {
                //                List<Department> targetedDepartments = new Department().GetAllDepartmentByTopicId(topicId, companyId, session).Departments;
                //                randomUsers = SelectAllUsersByDepartments(targetedDepartments, requesterUserId, companyId, session, null).Users;
                //                randomUsers = randomUsers.Where(i => !recentlyPlayedOpponentIds.Contains(i.UserId)).ToList(); 
                //            }
                //            else if (isForUser)
                //            {
                //                ps = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_user", new List<string> { }, new List<string> { "topic_id" }));
                //                RowSet userByTopicRowset = session.Execute(ps.Bind(topicId));

                //                foreach (Row userByTopicRow in userByTopicRowset)
                //                {
                //                    string userId = userByTopicRow.GetValue<string>("user_id");

                //                    User selectedUser = SelectUserBasic(userId, companyId, true, session).User;

                //                    if (selectedUser != null && recentlyPlayedOpponents.FindIndex(user => user.UserId.Equals(selectedUser.UserId)) < 0)
                //                    {
                //                        randomUsers.Add(selectedUser);
                //                    }
                //                }

                //                randomUsers = randomUsers.Where(i => !recentlyPlayedOpponentIds.Contains(i.UserId)).ToList(); 
                //            }
                //        }
                //    }

                //    // So that the take can work with the count limit
                //    if(randomUsers.Count < remainingCount)
                //    {
                //        remainingCount = randomUsers.Count();
                //    }

                //    if(randomUsers.Count >= remainingCount)
                //    {
                //        randomUsers = randomUsers.OrderBy(a => Guid.NewGuid()).ToList();
                //        randomUsers = randomUsers.Take(remainingCount).ToList();
                //        recentlyPlayedOpponents.AddRange(randomUsers);
                //    }
                //}

                response.Users = recentlyPlayedOpponents;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectBasicResponse SelectRandomOpponentForTopicId(string topicId, string categoryId, string requesterUserId, string companyId)
        {
            UserSelectBasicResponse response = new UserSelectBasicResponse();
            response.Success = false;
            response.User = new User();

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                List<User> allOpponents = SelectAllUsersByTopicId(topicId, categoryId, requesterUserId, companyId, null, session).Users;
                allOpponents.RemoveAll(o => o.UserId.Equals(requesterUserId));

                if (allOpponents.Count > 0)
                {
                    int random = new Random().Next(0, allOpponents.Count);
                    response.User = allOpponents[random];
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectAllBasicResponse SelectAllUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId, string containsName = null, ISession session = null)
        {
            UserSelectAllBasicResponse response = new UserSelectAllBasicResponse();
            response.Users = new List<User>();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    // Check topic category
                    Row topicCategoryRow = vh.ValidateTopicCategory(companyId, categoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }

                    // Check topic row
                    Row topicRow = vh.ValidateTopic(companyId, categoryId, topicId, session);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                        response.ErrorMessage = ErrorMessage.TopicInvalid;
                        return response;
                    }

                }

                Topic topicManager = new Topic();

                // Try to get from cache
                List<User> cachedUsers = CacheHelper.RetrieveUsersByCompany(companyId);

                if (cachedUsers == null)
                {
                    if (!topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, session))
                    {
                        Log.Error("Topic privacy has been updated: " + topicId);
                        response.ErrorCode = Int16.Parse(ErrorCode.TopicPrivacyNotAllowed);
                        response.ErrorMessage = ErrorMessage.TopicPrivacyNotAllowed;
                        return response;
                    }

                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("topic_privacy", new List<string> { }, new List<string> { "company_id", "topic_id" }));
                    BoundStatement bs = ps.Bind(companyId, topicId);
                    Row topicPrivacy = session.Execute(bs).FirstOrDefault();

                    if (topicPrivacy != null)
                    {
                        bool isForEveryone = topicPrivacy.GetValue<bool>("is_for_everyone");
                        bool isForDepartment = topicPrivacy.GetValue<bool>("is_for_department");
                        bool isForUser = topicPrivacy.GetValue<bool>("is_for_user");

                        if (isForEveryone)
                        {
                            Department departmentManager = new Department();
                            List<Department> departments = departmentManager.GetAllDepartment(null, companyId, Department.QUERY_TYPE_BASIC, session).Departments;
                            if (departments != null)
                            {
                                response.Users = SelectAllUsersByDepartments(departments, requesterUserId, companyId, session, containsName).Users;
                            }
                        }
                        else
                        {
                            if (isForDepartment)
                            {
                                List<Department> targetedDepartments = new Department().GetAllDepartmentByTopicId(topicId, companyId, session).Departments;
                                response.Users = SelectAllUsersByDepartments(targetedDepartments, requesterUserId, companyId, session, containsName).Users;
                            }
                            else if (isForUser)
                            {
                                ps = session.Prepare(CQLGenerator.SelectStatement("topic_targeted_user", new List<string> { }, new List<string> { "topic_id" }));
                                RowSet userByTopicRowset = session.Execute(ps.Bind(topicId));

                                foreach (Row userByTopicRow in userByTopicRowset)
                                {
                                    string userId = userByTopicRow.GetValue<string>("user_id");

                                    User selectedUser = SelectUserBasic(userId, companyId, true, session, containsName).User;

                                    if (selectedUser != null)
                                    {
                                        response.Users.Add(selectedUser);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (User cachedUser in cachedUsers)
                    {
                        if (!topicManager.CheckPrivacyForTopic(cachedUser.UserId, companyId, topicId, session))
                        {
                            continue;
                        }

                        if (!string.IsNullOrEmpty(containsName))
                        {
                            string firstName = cachedUser.FirstName;
                            string lastName = cachedUser.LastName;
                            string fullName = firstName + " " + lastName;

                            if (fullName.ToLower().Contains(containsName.ToLower()))
                            {
                                response.Users.Add(cachedUser);
                            }
                        }
                        else
                        {
                            response.Users.Add(cachedUser);
                        }
                    }
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode)
        {
            UserCreateResponse response = new UserCreateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                DateTime currentTime = DateTime.UtcNow;
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();

                if (string.IsNullOrEmpty(email))
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.UserMissingEmail);
                    response.ErrorMessage = ErrorMessage.UserMissingEmail;
                    return response;
                }
                #endregion

                if (string.IsNullOrEmpty(adminUserId))
                {
                    adminUserId = UUIDGenerator.GenerateUniqueIDForUser();
                }

                #region Step 2. Write data to database.

                #region Company

                if (string.IsNullOrEmpty(companyId))
                {
                    companyId = UUIDGenerator.GenerateUniqueIDForCompany();
                }

                string supportMessage = string.Format(DefaultResource.CompanySupportMessage, email);
                string countryTimezone = DefaultResource.CompanyCountryTimeZone;
                string zoneName = DefaultResource.CompanyZoneName;
                string adminProfile = DefaultResource.CompanyAdminPhotoUrl;
                double timezone = double.Parse(DefaultResource.CompanyTimezone);
                string clientBanner = DefaultResource.CompanyClientBannerUrl;
                string matchupBanner = DefaultResource.CompanyMatchupBannerUrl;
                string profilePopup = DefaultResource.CompanyProfilePopupUrl;

                string emailSquareImageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                string emailPersonnelInvitationTitle = DefaultResource.CompanyEmailPersonnelInvitationTitle;
                string emailPersonnelInvitationDescription = string.Format(DefaultResource.CompanyEmailPersonnelInvitationDescription, companyTitle);
                string emailPersonnelInvitationSupportInfo = DefaultResource.CompanyEmailPersonnelInvitationSupportInfo;

                string emailAdminInvitationTitle = DefaultResource.CompanyEmailAdminInvitationTitle;
                string emailAdminInvitationDescription = string.Format(DefaultResource.CompanyEmailAdminInvitationDescription, companyTitle);
                string emailAdminInvitationSupportInfo = DefaultResource.CompanyEmailAdminInvitationSupportInfo;

                string emailForgotPasswordTitle = DefaultResource.CompanyEmailForgotPasswordTitle;
                string emailForgotPasswordDescription = DefaultResource.CompanyEmailForgotPasswordDescription;
                string emailForgotPasswordSupportInfo = DefaultResource.CompanyEmailForgotPasswordSupportInfo;

                string emailResetPasswordTitle = DefaultResource.CompanyEmailResetPasswordTitle;
                string emailResetPasswordDescription = DefaultResource.CompanyEmailResetPasswordDescription;
                string emailResetPasswordSupportInfo = DefaultResource.CompanyEmailResetPasswordSupportInfo;

                string headerImageUrl = DefaultResource.CompanyHeaderImageUrl;
                string pullRefreshImageUrl = DefaultResource.CompanyClientPullRefreshImageUrl;

                if (string.IsNullOrEmpty(companyLogoUrl))
                {
                    companyLogoUrl = DefaultResource.CompanyLogoUrl;
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("company",
                    new List<string> {
                        "id", "title", "primary_admin_user_id", "secondary_email", "support_message", "country_timezone", "zone_name", "timezone_offset", "logo_url",
                        "admin_profile_image_url",

                        "email_square_logo_url", "email_personnel_invitation_title", "email_personnel_invitation_description", "email_personnel_invitation_support_info",
                        "email_admin_invitation_title", "email_admin_invitation_description", "email_admin_invitation_support_info",

                        "email_forgot_password_title", "email_forgot_password_description", "email_forgot_password_support_info",

                        "email_reset_password_title", "email_reset_password_description", "email_reset_password_support_info",

                        "admin_website_header_logo_url",

                        "client_banner_url", "client_pull_refresh_banner_url", "client_matchup_banner_url", "client_profile_popup_banner_url",

                        "created_by_user_id", "last_modified_by_user_id", "created_on_timestamp", "last_modified_timestamp", "is_valid"}));
                updateBatch.Add(
                    ps.Bind(
                        companyId, companyTitle, adminUserId, null, supportMessage, countryTimezone, zoneName, timezone, companyLogoUrl,
                        adminProfile,
                        emailSquareImageUrl, emailPersonnelInvitationTitle, emailPersonnelInvitationDescription, emailAdminInvitationSupportInfo,
                        emailAdminInvitationTitle, emailAdminInvitationDescription, emailAdminInvitationSupportInfo,
                        emailForgotPasswordTitle, emailForgotPasswordDescription, emailForgotPasswordSupportInfo,
                        emailResetPasswordTitle, emailResetPasswordDescription, emailResetPasswordSupportInfo,
                        headerImageUrl,
                        clientBanner, pullRefreshImageUrl, matchupBanner, profilePopup,
                        adminUserId, adminUserId, currentTime, currentTime, true));

                Company companyManager = new Company();
                CompanyCreateDefaultJobStatementResponse defaultJobResponse = companyManager.CreateDefaultJobsStatement(companyId, adminUserId, currentTime, session);
                foreach (BoundStatement bs in defaultJobResponse.Statements)
                {
                    updateBatch.Add(bs);
                }

                CompanyUpdateUserJobStatementResponse userJobResponse = companyManager.UpdateUserJobStatement(companyId, adminUserId, defaultJobResponse.JobId, session);
                foreach (BoundStatement bs in userJobResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }

                #endregion

                #region user_authentication.
                if (string.IsNullOrEmpty(plainPassword))
                {
                    plainPassword = UUIDGenerator.GenerateNumberPasswordForUser();
                }

                String salt = Crypto.GenerateRandomSalt(plainPassword.Length);
                String hashedPassword = Crypto.EncryptTextWithSalt(plainPassword, salt);

                ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication", new List<string> { "email", "user_id", "company_id", "created_by_user_id", "created_on_timestamp", "hashed_password", "last_modified_by_user_id", "last_modified_timestamp", "salt", "is_default" }));
                updateBatch.Add(ps.Bind(email, adminUserId, companyId, adminUserId, currentTime, hashedPassword, adminUserId, currentTime, salt, true));
                #endregion

                #region department, department_by_company
                string departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();
                string departmentTitle = WebConfigurationManager.AppSettings["default_department_title"].ToString();
                ps = session.Prepare(CQLGenerator.InsertStatement("department", new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));
                updateBatch.Add(ps.Bind(companyId, departmentId, adminUserId, currentTime, true, adminUserId, currentTime, departmentTitle));

                ps = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                updateBatch.Add(ps.Bind(companyId, departmentId));
                #endregion

                #region department_by_user, user_by_department
                ps = session.Prepare(CQLGenerator.InsertStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                updateBatch.Add(ps.Bind(adminUserId, departmentId));

                ps = session.Prepare(CQLGenerator.InsertStatement("user_by_department", new List<string> { "department_id", "user_id", "position", "user_status" }));
                updateBatch.Add(ps.Bind(departmentId, adminUserId, position, User.AccountStatus.CODE_ACTIVE));
                #endregion

                #region user_basic
                if (String.IsNullOrEmpty(profileImageUrl))
                {
                    profileImageUrl = DefaultResource.UserProfileUrl;
                }
                ps = session.Prepare(CQLGenerator.InsertStatement("user_basic", new List<string> { "id", "created_by_user_id", "created_on_timestamp", "email", "first_name", "job_level_id", "last_modified_by_user_id", "last_modified_timestamp", "last_name", "profile_image_url", "invited_on_timestamp", "is_email_sent" }));
                updateBatch.Add(ps.Bind(adminUserId, adminUserId, currentTime, email, firstName, defaultJobResponse.JobId, adminUserId, currentTime, lastName, profileImageUrl, currentTime, true));
                #endregion

                #region user_contact
                ps = session.Prepare(CQLGenerator.InsertStatement("user_contact", new List<string> { "id", "address", "address_country_name", "address_postal_code", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp", "phone", "phone_country_code", "phone_country_name" }));
                updateBatch.Add(ps.Bind(adminUserId, address, addressCountryName, postalCode, adminUserId, currentTime, adminUserId, currentTime, phoneNumber, phoneCountryCode, phoneCountryName));
                #endregion

                #region user_account_type
                ps = session.Prepare(CQLGenerator.InsertStatement("user_account_type", new List<string> { "user_id", "account_status", "account_type", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                updateBatch.Add(ps.Bind(adminUserId, User.AccountStatus.CODE_ACTIVE, User.AccountType.CODE_ADMIN, adminUserId, currentTime, adminUserId, currentTime));
                #endregion

                #region Setting
                Setting setting = new Setting();
                setting.CreateDefaultSettingPermisson(companyId, session);
                #endregion

                session.Execute(updateBatch);
                #endregion

                #region Step 3. Create bucket on AWS
                try
                {
                    String bucketName = "cocadre-" + companyId.ToLower();
                    String snsTopic = "arn:aws:sns:ap-southeast-1:433645821931:CocadreS3UploadEvent";
                    String snsTopicAdmin = "arn:aws:sns:ap-southeast-1:433645821931:CocadreS3UploadEventAdmin";
                    String topicEvent = "s3:ObjectCreated:*";
                    String id = "CocadreSNSS3PutEvent";

                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        PutBucketRequest putRequest = new PutBucketRequest();
                        putRequest.BucketName = bucketName;

                        PutBucketResponse putResponse = s3Client.PutBucket(putRequest);

                        List<TopicConfiguration> topicConfigurations = new List<TopicConfiguration>();

                        // feeds/ original.jpg
                        topicConfigurations.Add(new TopicConfiguration()
                        {
                            Id = id,
                            Events = new List<EventType> { topicEvent },
                            Topic = snsTopic,
                            Filter = new Filter
                            {
                                S3KeyFilter = new S3KeyFilter
                                {
                                    FilterRules = new List<FilterRule>
                                    {
                                        new FilterRule { Name = "Suffix", Value = "original.jpg" },
                                        new FilterRule { Name = "Prefix", Value = "feeds/" }
                                    }
                                }
                            }
                        });

                        // users/ original.jpg
                        topicConfigurations.Add(new TopicConfiguration()
                        {
                            Id = "CocadreSNSS3PutEventUser",
                            Events = new List<EventType> { topicEvent },
                            Topic = snsTopic,
                            Filter = new Filter
                            {
                                S3KeyFilter = new S3KeyFilter
                                {
                                    FilterRules = new List<FilterRule>
                                    {
                                        new FilterRule { Name = "Suffix", Value = "original.jpg" },
                                        new FilterRule { Name = "Prefix", Value = "users/" }
                                    }
                                }
                            }
                        });

                        // company/ original.jpg
                        topicConfigurations.Add(new TopicConfiguration()
                        {
                            Id = "CocadreSNSS3PutEventCompanyJpg",
                            Events = new List<EventType> { topicEvent },
                            Topic = snsTopicAdmin,
                            Filter = new Filter
                            {
                                S3KeyFilter = new S3KeyFilter
                                {
                                    FilterRules = new List<FilterRule>
                                    {
                                        new FilterRule { Name = "Suffix", Value = "original.jpg" },
                                        new FilterRule { Name = "Prefix", Value = "company/" }
                                    }
                                }
                            }
                        });

                        // company/ original.jpeg
                        topicConfigurations.Add(new TopicConfiguration()
                        {
                            Id = "CocadreSNSS3PutEventCompanyJpeg",
                            Events = new List<EventType> { topicEvent },
                            Topic = snsTopicAdmin,
                            Filter = new Filter
                            {
                                S3KeyFilter = new S3KeyFilter
                                {
                                    FilterRules = new List<FilterRule>
                                    {
                                        new FilterRule { Name = "Suffix", Value = "original.jpeg" },
                                        new FilterRule { Name = "Prefix", Value = "company/" }
                                    }
                                }
                            }
                        });

                        // company/ original.png
                        topicConfigurations.Add(new TopicConfiguration()
                        {
                            Id = "CocadreSNSS3PutEventCompanyPng",
                            Events = new List<EventType> { topicEvent },
                            Topic = snsTopicAdmin,
                            Filter = new Filter
                            {
                                S3KeyFilter = new S3KeyFilter
                                {
                                    FilterRules = new List<FilterRule>
                                    {
                                        new FilterRule { Name = "Suffix", Value = "original.png" },
                                        new FilterRule { Name = "Prefix", Value = "company/" }
                                    }
                                }
                            }
                        });

                        // company/ original.bmp
                        topicConfigurations.Add(new TopicConfiguration()
                        {
                            Id = "CocadreSNSS3PutEventCompanyBmp",
                            Events = new List<EventType> { topicEvent },
                            Topic = snsTopicAdmin,
                            Filter = new Filter
                            {
                                S3KeyFilter = new S3KeyFilter
                                {
                                    FilterRules = new List<FilterRule>
                                    {
                                        new FilterRule { Name = "Suffix", Value = "original.bmp" },
                                        new FilterRule { Name = "Prefix", Value = "company/" }
                                    }
                                }
                            }
                        });

                        PutBucketNotificationRequest request = new PutBucketNotificationRequest
                        {
                            BucketName = bucketName,
                            TopicConfigurations = topicConfigurations,
                        };

                        s3Client.PutBucketNotification(request);

                        #region Create CORS Configuration to a Bucket
                        CORSConfiguration configCORS = new CORSConfiguration
                        {
                            Rules = new System.Collections.Generic.List<CORSRule>
                            {
                                new CORSRule
                                {
                                    Id = "CORSDefaultRule",
                                    AllowedMethods = new List<string> {"GET"},
                                    AllowedOrigins = new List<string> {"*"},
                                    MaxAgeSeconds = 3000,
                                    AllowedHeaders = new List<string> { "Authorization" }
                                }
                            }
                        };

                        PutCORSConfigurationRequest requestCORS = new PutCORSConfigurationRequest
                        {
                            BucketName = bucketName,
                            Configuration = configCORS
                        };

                        s3Client.PutCORSConfiguration(requestCORS);
                        #endregion



                    }
                }
                catch (AmazonS3Exception amazonS3Exception)
                {
                    if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                    {
                        Log.Error("Check the provided AWS Credentials.");
                    }
                    else
                    {
                        Log.Error(string.Format("Error occurred. Message:'{0}' when enabling notifications.", amazonS3Exception.Message));
                    }
                }

                #endregion

                #region Step 3. Send email to user. Using AWS SES.
                EmailSendResponse sendResponse = EmailHandler.SendNewAdminEmail(email, companyId, DefaultResource.CompanyEmailCreatedAdminTitle, string.Format(DefaultResource.CompanyEmailCreatedAdminDescription, companyTitle), DefaultResource.CompanyEmailCreatedAdminSupportInfo, DefaultResource.CompanyEmailSquareImageUrl, plainPassword, session);
                if (!sendResponse.Success)
                {
                    #region Step 1. Delete data on database.
                    BatchStatement bs = new BatchStatement();

                    ps = session.Prepare(CQLGenerator.DeleteStatement("company", new List<string> { "id" }));
                    bs.Add(ps.Bind(companyId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                    bs.Add(ps.Bind(email, adminUserId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("department", new List<string> { "company_id", "id" }));
                    bs.Add(ps.Bind(companyId, departmentId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                    bs.Add(ps.Bind(companyId, departmentId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                    bs.Add(ps.Bind(adminUserId, departmentId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                    bs.Add(ps.Bind(departmentId, adminUserId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_basic", new List<string> { "id" }));
                    bs.Add(ps.Bind(adminUserId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_contact", new List<string> { "id" }));
                    bs.Add(ps.Bind(adminUserId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_account_type", new List<string> { "user_id" }));
                    bs.Add(ps.Bind(adminUserId));

                    session.Execute(bs);
                    #endregion

                    #region Step 2. Delete folder on S3.
                    String bucketName = "cocadre-" + companyId.ToLower();

                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }
                    #endregion

                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;
                    return response;
                }
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserCreateResponse Create(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, String jobTitle, int gender, DateTime? birthday, bool isSendEmail = true, String plainPassword = null)
        {
            UserCreateResponse response = new UserCreateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                PreparedStatement ps = null;

                BatchStatement updateBatch = new BatchStatement();

                if (string.IsNullOrEmpty(email))
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.UserMissingEmail);
                    response.ErrorMessage = ErrorMessage.UserMissingEmail;
                    return response;
                }

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }
                #endregion

                DateTime currentTime = DateTime.UtcNow;

                #region Step 1.2 Check Email
                email = email.ToLower().Trim();

                PreparedStatement ps_user_authentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "company_id" }));
                BoundStatement bs_user_authentication = ps_user_authentication.Bind(email, companyId);
                Row row_user_authentication = session.Execute(bs_user_authentication).FirstOrDefault();
                if (row_user_authentication != null)
                {
                    Log.Error(ErrorMessage.UserDuplicatedEmail);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserDuplicatedEmail);
                    response.ErrorMessage = ErrorMessage.UserDuplicatedEmail;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Write data to database.
                #region user_authentication.
                if (string.IsNullOrEmpty(plainPassword))
                {
                    plainPassword = UUIDGenerator.GenerateNumberPasswordForUser();
                }

                String salt = Crypto.GenerateRandomSalt(plainPassword.Length);
                String hashedPassword = Crypto.EncryptTextWithSalt(plainPassword, salt);

                ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication", new List<string> { "email", "user_id", "company_id", "created_by_user_id", "created_on_timestamp", "hashed_password", "last_modified_by_user_id", "last_modified_timestamp", "salt", "is_default" }));
                updateBatch.Add(ps.Bind(email, userId, companyId, adminUserId, currentTime, hashedPassword, adminUserId, currentTime, salt, true));
                #endregion

                #region department, department_by_company
                bool isNewDepartment = true;
                String departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();

                ps = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "is_valid" }));
                RowSet departmentRowSet = session.Execute(ps.Bind(companyId, true));
                foreach (Row row in departmentRowSet)
                {
                    if (row.GetValue<string>("title").ToLower().Trim().Equals(departmentTitle.ToLower().Trim()))
                    {
                        departmentId = row.GetValue<string>("id");
                        isNewDepartment = false;
                        break;
                    }
                }

                if (isNewDepartment)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("department", new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));
                    updateBatch.Add(ps.Bind(companyId, departmentId, adminUserId, currentTime, true, adminUserId, currentTime, departmentTitle));

                    ps = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                    updateBatch.Add(ps.Bind(companyId, departmentId));
                }

                Company companyManager = new Company();
                CompanyCreateJobStatementResponse jobResponse = companyManager.CreateJobStatement(companyId, adminUserId, jobTitle, currentTime, session);
                string jobId = jobResponse.JobId;
                foreach (BoundStatement bs in jobResponse.Statements)
                {
                    updateBatch.Add(bs);
                }

                CompanyUpdateUserJobStatementResponse userJobResponse = companyManager.UpdateUserJobStatement(companyId, userId, jobResponse.JobId, session);
                foreach (BoundStatement bs in userJobResponse.UpdateStatements)
                {
                    updateBatch.Add(bs);
                }
                #endregion

                #region department_by_user, user_by_department
                ps = session.Prepare(CQLGenerator.InsertStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                updateBatch.Add(ps.Bind(userId, departmentId));

                ps = session.Prepare(CQLGenerator.InsertStatement("user_by_department", new List<string> { "department_id", "user_id", "position", "user_status" }));
                updateBatch.Add(ps.Bind(departmentId, userId, position, User.AccountStatus.CODE_ACTIVE));
                #endregion

                #region user_basic
                bool isDefaultProfileImage = false;

                if (String.IsNullOrEmpty(profileImageUrl))
                {
                    isDefaultProfileImage = true;
                    profileImageUrl = DefaultProfileImageUrl;
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("user_basic", new List<string> { "id", "created_by_user_id", "created_on_timestamp", "date_of_birth", "email", "first_name", "gender", "last_modified_by_user_id", "last_modified_timestamp", "last_name", "profile_image_url", "invited_on_timestamp", "is_email_sent", "job_level_id" }));
                updateBatch.Add(ps.Bind(userId, adminUserId, currentTime, birthday, email, firstName, gender, adminUserId, currentTime, lastName, profileImageUrl, currentTime, isSendEmail, jobId));
                #endregion

                #region user_contact
                ps = session.Prepare(CQLGenerator.InsertStatement("user_contact", new List<string> { "id", "address", "address_country_name", "address_postal_code", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp", "phone", "phone_country_code", "phone_country_name" }));
                updateBatch.Add(ps.Bind(userId, address, addressCountryName, postalCode, adminUserId, currentTime, adminUserId, currentTime, phoneNumber, phoneCountryCode, phoneCountryName));
                #endregion

                #region user_account_type
                ps = session.Prepare(CQLGenerator.InsertStatement("user_account_type", new List<string> { "user_id", "account_status", "account_type", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                updateBatch.Add(ps.Bind(userId, User.AccountStatus.CODE_ACTIVE, User.AccountType.CODE_NORMAL_USER, adminUserId, currentTime, adminUserId, currentTime));
                #endregion

                session.Execute(updateBatch);
                #endregion

                #region Step 3. Update Cache
                CacheHelper.UpdateCacheForColleague(companyId, departmentId, departmentTitle, userId, email, firstName, lastName, profileImageUrl, position);
                #endregion

                #region Step 4. Send email to user. Using AWS SES.
                if (isSendEmail)
                {
                    CompanySelectEmailDetailResponse emailDetail = new Company().SelectEmailTemplate(companyId, (int)Company.CompanyEmailTemplate.PersonnelInvite, session, companyRow);

                    EmailSendResponse sendResponse = EmailHandler.SendNewUserEmail(email, companyId, emailDetail.EmailTitle, emailDetail.EmailDescription, emailDetail.EmailSupportInfo, emailDetail.EmailLogoUrl, plainPassword, session);
                    if (!sendResponse.Success)
                    {
                        #region Step 1. Delete data on database.
                        BatchStatement bs = new BatchStatement();

                        ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                        bs.Add(ps.Bind(email, userId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("department", new List<string> { "company_id", "id" }));
                        bs.Add(ps.Bind(companyId, departmentId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                        bs.Add(ps.Bind(companyId, departmentId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                        bs.Add(ps.Bind(userId, departmentId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                        bs.Add(ps.Bind(departmentId, userId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("user_basic", new List<string> { "id" }));
                        bs.Add(ps.Bind(userId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("user_contact", new List<string> { "id" }));
                        bs.Add(ps.Bind(userId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("user_account_type", new List<string> { "user_id" }));
                        bs.Add(ps.Bind(userId));

                        session.Execute(bs);
                        #endregion

                        #region Step 2. Delete file on S3.
                        if (!isDefaultProfileImage)
                        {
                            String bucketName = "cocadre-" + companyId.ToLower() + "/users/" + userId;
                            String[] array = profileImageUrl.Split('/');
                            profileImageUrl = array[array.Length - 1];

                            List<String> keys = new List<string>();
                            keys.Add(profileImageUrl);
                            keys.Add(profileImageUrl.Replace("_original", "_large"));
                            keys.Add(profileImageUrl.Replace("_original", "_medium"));
                            keys.Add(profileImageUrl.Replace("_original", "_small"));

                            using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                            {
                                for (int i = 0; i < keys.Count; i++)
                                {
                                    DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                    {
                                        BucketName = bucketName,
                                        Key = keys[i]
                                    };
                                    try
                                    {
                                        s3Client.DeleteObject(deleteObjectRequest);
                                        Log.Debug("Delete file: " + bucketName + "/" + keys[i]);
                                    }
                                    catch (AmazonS3Exception s3Exception)
                                    {
                                        Log.Error(s3Exception.ToString(), s3Exception);
                                    }
                                }
                            }
                        }
                        #endregion

                        response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                        response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;
                        return response;
                    }
                }
                #endregion

                // Add profile image to album
                if (!isDefaultProfileImage)
                {
                    UploadProfileImage(userId, adminUserId, companyId, profileImageUrl, (int)Dashboard.ProfileApprovalState.Approved, session);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectDeviceTokenResponse SelectUserDeviceToken(string userId)
        {
            UserSelectDeviceTokenResponse response = new UserSelectDeviceTokenResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psDeviceToken = session.Prepare(CQLGenerator.SelectStatement("user_device_token",
                           new List<string> { "device_token", "device_type", "device_type_name" }, new List<string> { "user_id" }));
                BoundStatement bsDeviceToken = psDeviceToken.Bind(userId);
                Row deviceRow = session.Execute(bsDeviceToken).FirstOrDefault();

                if (deviceRow != null)
                {
                    response.DeviceToken = deviceRow.GetValue<string>("device_token");
                    response.DeviceType = deviceRow.GetValue<int>("device_type");
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserUpdateDeviceTokenResponse UpdateUserDeviceToken(string userId, string companyId, string deviceToken, int deviceType)
        {
            UserUpdateDeviceTokenResponse response = new UserUpdateDeviceTokenResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psDeviceToken = null;
                BoundStatement bsDeviceToken = null;

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow != null)
                {
                    Row userRow = vh.ValidateUser(userId, companyId, session);

                    if (userRow != null)
                    {
                        string deviceTypeName = "iOS";

                        if (deviceType == (int)DeviceTokenType.Android)
                        {
                            deviceTypeName = "Android";
                        }

                        psDeviceToken = session.Prepare(CQLGenerator.SelectStatement("user_device_token",
                            new List<string> { "device_token", "device_type", "device_type_name" }, new List<string> { "user_id" }));
                        bsDeviceToken = psDeviceToken.Bind(userId);
                        Row deviceRow = session.Execute(bsDeviceToken).FirstOrDefault();

                        // Device token already recorded before
                        if (deviceRow != null)
                        {
                            psDeviceToken = session.Prepare(CQLGenerator.UpdateStatement("user_device_token",
                                new List<string> { "user_id" }, new List<string> { "device_token", "device_type", "device_type_name" }, new List<string>()));
                            bsDeviceToken = psDeviceToken.Bind(deviceToken, deviceType, deviceTypeName, userId);
                            session.Execute(bsDeviceToken);

                            response.OutdatedDeviceToken = deviceRow.GetValue<string>("device_token");
                            response.OutdatedDeviceType = deviceRow.GetValue<int>("device_type");
                        }
                        // Device token not recorded
                        else
                        {
                            psDeviceToken = session.Prepare(CQLGenerator.InsertStatement("user_device_token",
                                new List<string> { "user_id", "device_token", "device_type", "device_type_name" }));
                            bsDeviceToken = psDeviceToken.Bind(userId, deviceToken, deviceType, deviceTypeName);
                            session.Execute(bsDeviceToken);
                        }


                        response.Success = true;
                    }
                    else
                    {
                        Log.Error("Invalid userId: " + userId);
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }

                }
                else
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                }
            }

            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserDeleteResponse Delete(string adminUserId, string companyId, string userId, ISession session = null)
        {
            UserDeleteResponse response = new UserDeleteResponse();
            response.Success = true;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (session == null)
                {
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }


                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement psUser = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string> { "id" }));
                Row userRow = session.Execute(psUser.Bind(userId)).FirstOrDefault();

                string email = userRow.GetValue<string>("email");

                PreparedStatement psDepartmentByUser = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string>(), new List<string> { "user_id" }));
                RowSet departmentByUserRowset = session.Execute(psDepartmentByUser.Bind(userId));

                string mainDepartmentId = string.Empty;
                foreach (Row departmentByUserRow in departmentByUserRowset)
                {
                    string departmentId = departmentByUserRow.GetValue<string>("department_id");

                    if (string.IsNullOrEmpty(mainDepartmentId))
                    {
                        mainDepartmentId = departmentId;
                    }

                    PreparedStatement psUserByDepartment = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "user_id", "department_id" }));
                    deleteBatch = deleteBatch.Add(psUserByDepartment.Bind(userId, departmentId));
                }

                psDepartmentByUser = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id" }));
                deleteBatch = deleteBatch.Add(psDepartmentByUser.Bind(userId));

                // Deleted
                PreparedStatement psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_authentication_token_by_user", new List<string>(), new List<string> { "user_id" }));
                RowSet rs = session.Execute(psUserToken.Bind(userId));
                foreach (Row row in rs)
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication_token", new List<string> { "user_token", "user_id" }));
                    deleteBatch.Add(ps.Bind(row.GetValue<string>("user_token"), userId));
                }

                psUserToken = session.Prepare(CQLGenerator.DeleteStatement("user_authentication_token_by_user", new List<string> { "user_id" }));
                deleteBatch.Add(psUserToken.Bind(userId));

                PreparedStatement psResetToken = session.Prepare(CQLGenerator.SelectStatement("reset_email_token_by_user", new List<string>(), new List<string> { "user_id" }));
                rs = session.Execute(psResetToken.Bind(userId));
                foreach (Row row in rs)
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.DeleteStatement("reset_email_token", new List<string> { "reset_token" }));
                    deleteBatch.Add(ps.Bind(row.GetValue<string>("reset_token")));
                }

                psResetToken = session.Prepare(CQLGenerator.DeleteStatement("reset_email_token_by_user", new List<string> { "user_id" }));
                deleteBatch.Add(psResetToken.Bind(userId));

                PreparedStatement psUserDeviceToken = session.Prepare(CQLGenerator.DeleteStatement("user_device_token", new List<string> { "user_id" }));
                deleteBatch = deleteBatch.Add(psUserDeviceToken.Bind(userId));

                PreparedStatement psUserAuthentication = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "user_id", "email" }));
                deleteBatch = deleteBatch.Add(psUserAuthentication.Bind(userId, email));

                PreparedStatement psModeratorAccessRight = session.Prepare(CQLGenerator.DeleteStatement("moderator_access_rights", new List<string> { "user_id" }));
                deleteBatch = deleteBatch.Add(psModeratorAccessRight.Bind(userId));

                PreparedStatement psModeratorAccessTimespan = session.Prepare(CQLGenerator.DeleteStatement("moderator_access_management", new List<string> { "user_id" }));
                deleteBatch = deleteBatch.Add(psModeratorAccessRight.Bind(userId));

                PreparedStatement psPermissionFeedUserSuspension = session.Prepare(CQLGenerator.DeleteStatement("permission_feed_user_suspension", new List<string> { "company_id", "user_id" }));
                deleteBatch = deleteBatch.Add(psPermissionFeedUserSuspension.Bind(companyId, userId));

                string jobId = userRow.GetValue<string>("job_level_id");
                PreparedStatement psCompanyJobUser = session.Prepare(CQLGenerator.DeleteStatement("company_job_user", new List<string> { "company_id", "user_id", "job_id" }));
                deleteBatch = deleteBatch.Add(psCompanyJobUser.Bind(companyId, userId, jobId));

                psCompanyJobUser = session.Prepare(CQLGenerator.DeleteStatement("company_job_user_by_job", new List<string> { "company_id", "user_id", "job_id" }));
                deleteBatch = deleteBatch.Add(psCompanyJobUser.Bind(companyId, userId, jobId));

                // Update
                PreparedStatement psAccountType = session.Prepare(CQLGenerator.UpdateStatement("user_account_type",
                    new List<string> { "user_id" }, new List<string> { "account_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch = updateBatch.Add(psAccountType.Bind(User.AccountStatus.CODE_DELETED, adminUserId, DateTime.UtcNow, userId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                // Analytic
                AnalyticQuiz analytic = new AnalyticQuiz();
                ISession analyticSession = cm.getAnalyticSession();
                analytic.RemoveFromLeaderboard(true, false, companyId, analyticSession, userId, mainDepartmentId, null);
                analytic.RemoveFromEventLeaderboard(adminUserId, companyId, userId, session, analyticSession);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUpdateResponse Update(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, string jobTitle, int gender, DateTime? birthday, ISession session = null)
        {
            UserUpdateResponse response = new UserUpdateResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                ConnectionManager cm = new ConnectionManager();

                int origStatus = 0;

                if (string.IsNullOrEmpty(email))
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.UserMissingEmail);
                    response.ErrorMessage = ErrorMessage.UserMissingEmail;
                    return response;
                }

                if (session == null)
                {
                    #region Step 1.1 Check Admin account's validation.

                    session = cm.getMainSession();
                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                    #endregion
                }

                #region Step 1.2 Check user status. (user_account_type)
                ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string>(), new List<string> { "user_id" }));
                Row accountTypeRow = session.Execute(ps.Bind(userId)).FirstOrDefault();
                if (accountTypeRow == null || accountTypeRow.GetValue<int>("account_status") == AccountStatus.CODE_DELETED)
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                else
                {
                    origStatus = accountTypeRow.GetValue<int>("account_status");
                }
                #endregion

                DateTime currentTime = DateTime.UtcNow;

                #region Step 1.3 Check Email
                email = email.ToLower().Trim();
                String origEmail = String.Empty;
                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string> { "id" }));
                Row userRow = session.Execute(ps.Bind(userId)).FirstOrDefault();
                if (userRow == null)
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                origEmail = userRow.GetValue<String>("email");

                if (!origEmail.Equals(email))
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "company_id" }));
                    Row authenticationRow = session.Execute(ps.Bind(email, companyId)).FirstOrDefault();
                    if (authenticationRow != null && !authenticationRow.GetValue<String>("user_id").Equals(userId))
                    {
                        Log.Error(ErrorMessage.UserDuplicatedEmail);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.UserDuplicatedEmail);
                        response.ErrorMessage = ErrorMessage.UserDuplicatedEmail;
                        return response;
                    }
                    else if (authenticationRow == null)
                    {

                        ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "company_id" }));
                        authenticationRow = session.Execute(ps.Bind(origEmail, companyId)).FirstOrDefault();

                        #region user_authentication. Delete > Insert
                        ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                        updateBatch.Add(ps.Bind(origEmail, userId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("user_authentication", new List<string> { "email", "user_id", "company_id", "created_by_user_id", "created_on_timestamp", "hashed_password", "is_default", "last_modified_by_user_id", "last_modified_timestamp", "salt" }));
                        updateBatch.Add(ps.Bind(email, userId, companyId, authenticationRow.GetValue<String>("created_by_user_id"), authenticationRow.GetValue<DateTimeOffset>("created_on_timestamp"), authenticationRow.GetValue<String>("hashed_password"), authenticationRow.GetValue<Boolean>("is_default"), adminUserId, currentTime, authenticationRow.GetValue<String>("salt")));
                        #endregion
                    }
                }
                #endregion
                #endregion

                #region Step 2. Write data to database.
                #region Department
                bool isNewDepartment = true;
                String departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();

                ps = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "is_valid" }));
                RowSet departmentRowSet = session.Execute(ps.Bind(companyId, true));
                foreach (Row row in departmentRowSet)
                {
                    if (row.GetValue<string>("title").ToLower().Trim().Equals(departmentTitle.ToLower().Trim()))
                    {
                        departmentId = row.GetValue<string>("id");
                        isNewDepartment = false;
                        break;
                    }
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" }));
                RowSet departmentByUserRowSet = session.Execute(ps.Bind(userId));
                List<String> origDepartments = new List<String>();
                foreach (Row row in departmentByUserRowSet)
                {
                    origDepartments.Add(row.GetValue<String>("department_id"));
                }


                if (!origDepartments[0].Equals(departmentId)) // Department has changed
                {
                    #region department, department_by_company
                    if (isNewDepartment)
                    {
                        ps = session.Prepare(CQLGenerator.InsertStatement("department", new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));
                        updateBatch.Add(ps.Bind(companyId, departmentId, adminUserId, currentTime, true, adminUserId, currentTime, departmentTitle));

                        ps = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                        updateBatch.Add(ps.Bind(companyId, departmentId));
                    }
                    #endregion

                    #region department_by_user, user_by_department. Delete > Insert
                    ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id" }));
                    deleteBatch.Add(ps.Bind(userId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                    updateBatch.Add(ps.Bind(userId, departmentId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                    deleteBatch.Add(ps.Bind(origDepartments[0], userId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("user_by_department", new List<string> { "department_id", "user_id", "position", "user_status" }));
                    updateBatch.Add(ps.Bind(departmentId, userId, position, origStatus));
                    #endregion

                    #region analytic
                    AnalyticQuiz analytic = new AnalyticQuiz();
                    ISession analyticSession = cm.getAnalyticSession();
                    analytic.SwitchLeaderboardForDepartment(userId, companyId, origDepartments[0], departmentId, analyticSession);
                    analytic.SwitchEventLeaderboardForDepartment(userId, companyId, origDepartments[0], departmentId, session, analyticSession);
                    #endregion
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.UpdateStatement("user_by_department", new List<string> { "department_id", "user_id" }, new List<string> { "position" }, new List<string>()));
                    updateBatch.Add(ps.Bind(position, departmentId, userId));
                }
                #endregion

                #region user_basic
                Company companyManager = new Company();

                ps = session.Prepare(CQLGenerator.SelectStatement("company_job_user", new List<string> { }, new List<string> { "company_id", "user_id" }));
                Row jobRow = session.Execute(ps.Bind(companyId, userId)).FirstOrDefault();
                string currentJobId = null;
                if (jobRow != null)
                {
                    currentJobId = jobRow.GetValue<string>("job_id");
                }

                CompanyCreateJobStatementResponse jobResponse = companyManager.CreateJobStatement(companyId, adminUserId, jobTitle, currentTime, session);
                string jobId = jobResponse.JobId;
                foreach (BoundStatement bs in jobResponse.Statements)
                {
                    updateBatch.Add(bs);
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "date_of_birth", "email", "first_name", "gender", "last_modified_by_user_id", "last_modified_timestamp", "last_name" }, new List<string>()));
                updateBatch.Add(ps.Bind(birthday, email, firstName, gender, adminUserId, currentTime, lastName, userId));

                if (String.IsNullOrEmpty(profileImageUrl))
                {
                    // Get the current image url
                    profileImageUrl = userRow.GetValue<String>("profile_image_url");
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "profile_image_url" }, new List<string>()));
                    updateBatch.Add(ps.Bind(profileImageUrl, userId));
                }

                if (!String.IsNullOrEmpty(jobId))
                {
                    ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "job_level_id" }, new List<string>()));
                    updateBatch.Add(ps.Bind(jobId, userId));

                    if (!string.IsNullOrEmpty(currentJobId) && !currentJobId.Equals(jobId))
                    {
                        CompanyUpdateUserJobStatementResponse userJobResponse = companyManager.UpdateUserJobStatement(companyId, userId, jobResponse.JobId, session, currentJobId);
                        foreach (BoundStatement bs in userJobResponse.UpdateStatements)
                        {
                            updateBatch.Add(bs);
                        }

                        foreach (BoundStatement bs in userJobResponse.DeleteStatements)
                        {
                            deleteBatch.Add(bs);
                        }
                    }
                }

                #endregion

                #region user_contact
                ps = session.Prepare(CQLGenerator.UpdateStatement("user_contact", new List<string> { "id" }, new List<string> { "address", "address_country_name", "address_postal_code", "last_modified_by_user_id", "last_modified_timestamp", "phone", "phone_country_code", "phone_country_name" }, new List<string>()));
                updateBatch.Add(ps.Bind(address, addressCountryName, postalCode, adminUserId, currentTime, phoneNumber, phoneCountryCode, phoneCountryName, userId));
                #endregion

                //#region user_account_type
                //user_account_type_ps = session.Prepare(CQLGenerator.InsertStatement("user_account_type", new List<string> { "user_id", "account_status", "account_type", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                //batch_statement.Add(user_account_type_ps.Bind(userId, User.AccountStatus.CODE_ACTIVE, User.AccountType.CODE_NORMAL_USER, adminUserId, currentTime, adminUserId, currentTime));
                //#endregion

                session.Execute(deleteBatch);
                session.Execute(updateBatch);
                #endregion

                #region Step 3. Update Cache
                if (isNewDepartment)
                {
                    CacheHelper.UpdateCacheForCreateDepartment(companyId, departmentId, departmentTitle);
                }
                string originalDepartmentId = origDepartments[0];
                CacheHelper.UpdateCacheForColleague(companyId, departmentId, departmentTitle, userId, email, firstName, lastName, profileImageUrl, position, originalDepartmentId);
                #endregion

                #region Step 4. Send email to user. Using AWS SES.
                //try
                //{
                //    EmailTemplate emailTemplate = new EmailTemplate(EmailTemplate.Type.CREATED_USER, "密碼");
                //    Destination destination = new Destination(new List<String> { email });
                //    Message message = new Message();
                //    message.Subject = new Content(emailTemplate.Tilte);
                //    Body mBody = new Body();
                //    if (emailTemplate.IsHtmlFormat)
                //    {
                //        mBody.Html = new Content(emailTemplate.Body);
                //    }
                //    else
                //    {
                //        mBody.Text = new Content(emailTemplate.Body);
                //    }
                //    message.Body = mBody;
                //    SendEmailRequest request = new SendEmailRequest("support@cocadre.com", destination, message);
                //    AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient("AKIAIRFRL6Z2OYHO4R3Q", "HfL9OECxbpWqEH7KjHtl8T5/dJM6UiVQ66ywHXzY", RegionEndpoint.USWest2);
                //    client.SendEmail(request);
                //    Log.Debug("Sent to " + email + " done.");
                //}
                //catch (Exception ex)
                //{
                //    Log.Error(ex.ToString(), ex);
                //    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                //    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;

                //    #region Step 1. Delete data on database.
                //    BatchStatement bs = new BatchStatement();

                //    user_authentication_ps = session.Prepare(CQLGenerator.DeleteStatement("user_authentication", new List<string> { "email", "user_id" }));
                //    bs.Add(user_authentication_ps.Bind(email, userId));

                //    department_ps = session.Prepare(CQLGenerator.DeleteStatement("department", new List<string> { "company_id", "id" }));
                //    bs.Add(department_ps.Bind(companyId, departmentId));

                //    department_by_company_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                //    bs.Add(department_by_company_ps.Bind(companyId, departmentId));

                //    department_by_user_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                //    bs.Add(department_by_user_ps.Bind(userId, departmentId));

                //    user_by_department_ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                //    bs.Add(user_by_department_ps.Bind(departmentId, userId));

                //    user_basic_ps = session.Prepare(CQLGenerator.DeleteStatement("user_basic", new List<string> { "id" }));
                //    bs.Add(user_basic_ps.Bind(userId));

                //    user_contact_ps = session.Prepare(CQLGenerator.DeleteStatement("user_contact", new List<string> { "id" }));
                //    bs.Add(user_contact_ps.Bind(userId));

                //    user_account_type_ps = session.Prepare(CQLGenerator.DeleteStatement("user_account_type", new List<string> { "user_id" }));
                //    bs.Add(user_account_type_ps.Bind(userId));

                //    session.Execute(bs);
                //    #endregion

                //    #region Step 2. Delete file on S3.

                //    String bucketName = "cocadre-" + companyId.ToLower() + "/users/" + userId;
                //    String fileName = profileImageUrl.Substring(profileImageUrl.LastIndexOf("/"), profileImageUrl.Length - profileImageUrl.LastIndexOf("/")).Replace("/", "");
                //    String format = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - fileName.LastIndexOf("."));

                //    List<String> keys = new List<string>();
                //    keys.Add(fileName.Replace(format, "") + "_original" + format);
                //    keys.Add(fileName.Replace(format, "") + "_large" + format);
                //    keys.Add(fileName.Replace(format, "") + "_medium" + format);
                //    keys.Add(fileName.Replace(format, "") + "_small" + format);

                //    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client( WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                //    {
                //        for (int i = 0; i < keys.Count; i++)
                //        {
                //            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                //            {
                //                BucketName = bucketName,
                //                Key = keys[i]
                //            };
                //            try
                //            {
                //                s3Client.DeleteObject(deleteObjectRequest);
                //                Log.Debug("Delete file: " + bucketName + "/" + keys[i]);
                //            }
                //            catch (AmazonS3Exception s3Exception)
                //            {
                //                Log.Error(s3Exception.ToString(), s3Exception);
                //            }
                //        }
                //    }
                //    #endregion

                //    return response;
                //}
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectProfileResponse UpdateForClient(String companyId, String userId, String firstName, String lastName, String email, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, int? gender, DateTime? birthday)
        {
            UserSelectProfileResponse response = new UserSelectProfileResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check input data.
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                BatchStatement batch_statement = new BatchStatement();
                BatchStatement bsDelete = new BatchStatement();
                BoundStatement bs_department_by_user = new BoundStatement();

                PreparedStatement department_ps = null;
                PreparedStatement department_by_company_ps = null;
                PreparedStatement department_by_user_ps = null;
                PreparedStatement user_by_department_ps = null;
                PreparedStatement user_basic_ps = null;
                PreparedStatement user_contact_ps = null;
                PreparedStatement ps_department = null;
                PreparedStatement ps_department_by_user = null;

                #region Step 1.1 Verify user.
                ErrorStatus es = vh.isValidatedAsUser(userId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);

                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }
                #endregion

                #region Step 1.2 Check user status. (user_account_type)
                int origStatus = AccountStatus.CODE_ACTIVE;
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string>(), new List<string> { "user_id" }));
                BoundStatement bs_user_account_type = ps_user_account_type.Bind(userId);
                Row row_user_account_type = session.Execute(bs_user_account_type).FirstOrDefault();
                if (row_user_account_type == null || row_user_account_type.GetValue<int>("account_status") == AccountStatus.CODE_DELETED)
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                else
                {
                    origStatus = row_user_account_type.GetValue<int>("account_status");
                }
                #endregion

                #endregion

                #region Step 2. Access database

                #region Not use. Change to call User.Update();
                /*
                #region Step 2.1 Department (department, department_by_company, department_by_user, user_by_department)
                bool isNewDepartment = true;
                String departmentId = UUIDGenerator.GenerateUniqueIDForDepartment();

                PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "is_valid" }));
                RowSet rs_department = session.Execute(ps_department.Bind(companyId, true));
                foreach (Row row in rs_department)
                {
                    if (row.GetValue<string>("title").ToLower().Trim().Equals(departmentTitle.ToLower().Trim()))
                    {
                        departmentId = row.GetValue<string>("id");
                        isNewDepartment = false;
                        break;
                    }
                }

                PreparedStatement ps_department_by_user = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs_department_by_user = ps_department_by_user.Bind(userId);
                RowSet rowSet_department_by_user = session.Execute(bs_department_by_user);
                List<String> origDepartments = new List<String>();
                foreach (Row row in rowSet_department_by_user)
                {
                    origDepartments.Add(row.GetValue<String>("department_id"));
                }


                if (!origDepartments[0].Equals(departmentId)) // Department has changed
                {
                    #region department, department_by_company
                    if (isNewDepartment)
                    {
                        department_ps = session.Prepare(CQLGenerator.InsertStatement("department", new List<string> { "company_id", "id", "created_by_user_id", "created_on_timestamp", "is_valid", "last_modified_by_user_id", "last_modified_timestamp", "title" }));
                        batch_statement.Add(department_ps.Bind(companyId, departmentId, null, DateTime.UtcNow, true, null, DateTime.UtcNow, departmentTitle.Trim()));

                        department_by_company_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_company", new List<string> { "company_id", "department_id" }));
                        batch_statement.Add(department_by_company_ps.Bind(companyId, departmentId));
                    }
                    #endregion

                    #region department_by_user, user_by_department. Delete > Insert
                    department_by_user_ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_user", new List<string> { "user_id" }));
                    bsDelete.Add(department_by_user_ps.Bind(userId));

                    department_by_user_ps = session.Prepare(CQLGenerator.InsertStatement("department_by_user", new List<string> { "user_id", "department_id" }));
                    batch_statement.Add(department_by_user_ps.Bind(userId, departmentId));

                    user_by_department_ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_department", new List<string> { "department_id", "user_id" }));
                    batch_statement.Add(user_by_department_ps.Bind(origDepartments[0], userId));

                    user_by_department_ps = session.Prepare(CQLGenerator.InsertStatement("user_by_department", new List<string> { "department_id", "user_id", "position", "user_status" }));
                    batch_statement.Add(user_by_department_ps.Bind(departmentId, userId, position, origStatus));
                    #endregion

                    #region analytic
                    AnalyticQuiz analytic = new AnalyticQuiz();
                    ISession analyticSession = cm.getAnalyticSession();
                    analytic.SwitchLeaderboardForDepartment(userId, companyId, origDepartments[0], departmentId, analyticSession);
                    analytic.SwitchEventLeaderboardForDepartment(userId, companyId, origDepartments[0], departmentId, session, analyticSession);
                    #endregion
                }
                else
                {
                    user_by_department_ps = session.Prepare(CQLGenerator.UpdateStatement("user_by_department", new List<string> { "department_id", "user_id" }, new List<string> { "position" }, new List<string>()));
                    batch_statement.Add(user_by_department_ps.Bind(position, departmentId, userId));
                }
                #endregion

                #region Step 2.2. user_basic
                user_basic_ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "date_of_birth", "first_name", "gender", "last_modified_by_user_id", "last_modified_timestamp", "last_name" }, new List<string>()));
                batch_statement.Add(user_basic_ps.Bind(birthday, firstName, gender, userId, DateTime.UtcNow, lastName, userId));
                #endregion

                #region Step 2.3 user_contact
                user_contact_ps = session.Prepare(CQLGenerator.UpdateStatement("user_contact", new List<string> { "id" }, new List<string> { "address", "address_country_name", "address_postal_code", "last_modified_by_user_id", "last_modified_timestamp", "phone", "phone_country_code", "phone_country_name" }, new List<string>()));
                batch_statement.Add(user_contact_ps.Bind(address, addressCountryName, postalCode, userId, DateTime.UtcNow, phoneNumber, phoneCountryCode, phoneCountryName, userId));
                #endregion

                session.Execute(bsDelete);
                session.Execute(batch_statement);
                */
                #endregion

                UserUpdateResponse updateResponse = Update(null, companyId, userId, firstName, lastName, email, null, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentTitle, null, gender.Value, birthday.Value, session);
                if (!updateResponse.Success)
                {
                    response.Success = updateResponse.Success;
                    response.ErrorCode = updateResponse.ErrorCode;
                    response.ErrorMessage = updateResponse.ErrorMessage;
                    return response;
                }

                User user = new User();

                #region Step 2.4. company information of user
                PreparedStatement ps_company = session.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_company = ps_company.Bind(companyId);
                Row row_company = session.Execute(bs_company).FirstOrDefault();
                if (row_company != null)
                {
                    Company company = new Company();
                    company.CompanyId = companyId;
                    company.CompanyTitle = row_company.GetValue<String>("title");
                    company.CompanyLogoUrl = row_company.GetValue<String>("logo_url");
                    company.CompanyBannerUrl = row_company.GetValue<String>("client_banner_url");
                    company.MatchupBannerUrl = row_company.GetValue<String>("client_matchup_banner_url");
                    company.ProfilePopupBannerUrl = row_company.GetValue<String>("client_profile_popup_banner_url");
                    company.CompanyPullRefreshBannerUrl = row_company.GetValue<String>("client_pull_refresh_banner_url");
                    user.Company = company;
                }
                else
                {
                    Log.Error(ErrorMessage.CompanyInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }
                #endregion

                #region Step 2.5. departments of user
                List<Department> departments = new List<Department>();
                ps_department_by_user = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" })); ;
                bs_department_by_user = ps_department_by_user.Bind(userId);
                RowSet rowSet = session.Execute(bs_department_by_user);
                foreach (Row r in rowSet)
                {
                    Department department = new Department();
                    ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" })); ;
                    BoundStatement bs_department = ps_department.Bind(companyId, r.GetValue<String>("department_id"), true);
                    Row row_department = session.Execute(bs_department).FirstOrDefault();
                    if (row_department == null)
                    {
                        Log.Error(ErrorMessage.DepartmentIsInvalid);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentIsInvalid);
                        response.ErrorMessage = ErrorMessage.DepartmentIsInvalid;
                        return response;
                    }
                    else
                    {
                        department.Id = row_department.GetValue<String>("id");
                        department.Title = row_department.GetValue<String>("title");
                    }

                    PreparedStatement ps_user_by_department = session.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id", "user_id" })); ;
                    BoundStatement bs_user_by_department = ps_user_by_department.Bind(r.GetValue<String>("department_id"), userId);
                    Row row_user_by_department = session.Execute(bs_user_by_department).FirstOrDefault();
                    if (row_user_by_department != null)
                    {
                        department.Position = row_user_by_department.GetValue<String>("position");
                    }
                    departments.Add(department);
                }
                user.Departments = departments;
                #endregion

                #region Step 2.6. basic information of user
                PreparedStatement ps_user_basic = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_basic = ps_user_basic.Bind(userId);
                Row row_user_basic = session.Execute(bs_user_basic).FirstOrDefault();
                if (row_user_basic != null)
                {
                    user.UserId = userId;
                    user.Email = row_user_basic.GetValue<String>("email");
                    user.FirstName = row_user_basic.GetValue<String>("first_name");
                    user.LastName = row_user_basic.GetValue<String>("last_name");
                    if (row_user_basic["gender"] == null)
                    {
                        user.Gender = null;
                    }
                    else
                    {
                        user.Gender = row_user_basic.GetValue<int>("gender");
                    }


                    if (row_user_basic["date_of_birth"] == null)
                    {
                        user.Birthday = null;
                        user.BirthdayString = null;
                    }
                    else
                    {
                        user.Birthday = row_user_basic.GetValue<DateTimeOffset>("date_of_birth");
                        DateTimeOffset bd = row_user_basic.GetValue<DateTimeOffset>("date_of_birth");
                        JsonSerializerSettings jss = new JsonSerializerSettings
                        {
                            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                        };
                        user.BirthdayString = JsonConvert.SerializeObject(bd, jss).Replace("\\", "").Replace("\"", "");
                    }

                    user.ProfileImageUrl = row_user_basic.GetValue<String>("profile_image_url");
                    user.LastModifiedProfileTimestamp = row_user_basic.GetValue<DateTimeOffset>("last_modified_timestamp");
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region Step 2.7. contact information of user
                PreparedStatement ps_user_contact = session.Prepare(CQLGenerator.SelectStatement("user_contact", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_contact = ps_user_contact.Bind(userId);
                Row row_user_contact = session.Execute(bs_user_contact).FirstOrDefault();
                if (row_user_contact != null)
                {
                    user.Address = row_user_contact.GetValue<String>("address");
                    user.AddressCountryName = row_user_contact.GetValue<String>("address_country_name");
                    user.AddressPostalCode = row_user_contact.GetValue<String>("address_postal_code");
                    user.Phone = row_user_contact.GetValue<String>("phone");
                    user.PhoneCountryCode = row_user_contact.GetValue<String>("phone_country_code");
                    user.PhoneCountryAbb = row_user_contact.GetValue<String>("phone_country_name");
                }
                #endregion

                response.UserProfile = user;

                #endregion

                //#region Step 3. Update Cache
                //if (isNewDepartment)
                //{
                //    CacheHelper.UpdateCacheForCreateDepartment(companyId, departmentId, departmentTitle);
                //}
                //string originalDepartmentId = origDepartments[0];
                //CacheHelper.UpdateCacheForColleague(user.Company.CompanyId, user.Departments[0].Id, user.Departments[0].Title, user.UserId, user.Email, user.FirstName, user.LastName, user.ProfileImageUrl, user.Departments[0].Position, originalDepartmentId);
                //#endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserUpdateStatusResponse UpdateStatus(String adminUserId, String companyId, String userId, int statusCode)
        {
            UserUpdateStatusResponse response = new UserUpdateStatusResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                BatchStatement bs = new BatchStatement();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check UserId.
                if (string.IsNullOrEmpty(userId.Trim()))
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region Step 1.3 Check user's status.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                Row r = session.Execute(ps.Bind(userId)).FirstOrDefault();
                if (r.GetValue<int>("account_status") == statusCode)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserStatusIsIdentical);
                    response.ErrorMessage = ErrorMessage.UserStatusIsIdentical;
                    return response;
                }
                #endregion

                #endregion

                #region Step 2. Update user account status.
                #region user_account_type
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.UpdateStatement("user_account_type", new List<string> { "user_id" }, new List<string> { "account_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                bs.Add(ps_user_account_type.Bind(statusCode, adminUserId, DateTime.UtcNow, userId));
                #endregion

                #region user_by_department
                PreparedStatement ps_department_by_user = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs_department_by_user = ps_department_by_user.Bind(userId);
                RowSet rowsUser = session.Execute(bs_department_by_user);
                foreach (Row row in rowsUser)
                {
                    PreparedStatement ps_user_by_department = session.Prepare(CQLGenerator.UpdateStatement("user_by_department", new List<string> { "department_id", "user_id" }, new List<string> { "user_status" }, new List<string>()));
                    bs.Add(ps_user_by_department.Bind(statusCode, row.GetValue<String>("department_id"), userId));
                }
                #endregion

                session.Execute(bs);
                response.Success = true;

                #region Analytics
                AnalyticQuiz analytic = new AnalyticQuiz();
                ISession analyticSession = cm.getAnalyticSession();

                string mainDepartmentId = new Department().GetAllDepartmentByUserId(userId, companyId, session).Departments[0].Id;

                if (statusCode == AccountStatus.CODE_ACTIVE)
                {
                    analytic.UnhideFromLeaderboard(true, false, companyId, analyticSession, userId, mainDepartmentId, null);
                    analytic.UnhideFromEventLeaderboard(adminUserId, companyId, userId, session, analyticSession);
                }
                else if (statusCode == AccountStatus.CODE_DELETED || statusCode == AccountStatus.CODE_DELETING || statusCode == AccountStatus.CODE_SUSPENEDED)
                {
                    analytic.HideFromLeaderboard(true, false, companyId, analyticSession, userId, mainDepartmentId, null);
                    analytic.HideFromEventLeaderboard(adminUserId, companyId, userId, session, analyticSession);
                }
                #endregion

                #endregion

                #region Step 3. Update Cache
                CacheHelper.UpdateCacheForUserStatus(companyId, mainDepartmentId, userId, statusCode, session);
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserUpdateTypeResponse UpdateType(String adminUserId, String companyId, String userId, int typeCode, List<int> accessModulesKey, DateTime? expiryDate)
        {
            UserUpdateTypeResponse response = new UserUpdateTypeResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                BatchStatement bs = new BatchStatement();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check UserId.
                if (string.IsNullOrEmpty(userId.Trim()))
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Update user account status.
                #region user_account_type
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.UpdateStatement("user_account_type", new List<string> { "user_id" }, new List<string> { "account_type", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                bs.Add(ps_user_account_type.Bind(typeCode, adminUserId, DateTime.UtcNow, userId));
                #endregion

                #region moderator_access_rights
                PreparedStatement ps;
                if (typeCode == User.AccountType.CODE_NORMAL_USER || typeCode == User.AccountType.CODE_ADMIN)
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("moderator_access_rights", new List<string> { "user_id" }));
                    bs.Add(ps.Bind(userId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("moderator_access_management", new List<string> { "user_id" }));
                    bs.Add(ps.Bind(userId));
                }
                else if (typeCode == User.AccountType.CODE_MODERATER)
                {
                    for (int i = 0; i < accessModulesKey.Count; i++)
                    {
                        ps = session.Prepare(CQLGenerator.InsertStatement("moderator_access_rights", new List<string> { "user_id", "access_rights_key" }));
                        bs.Add(ps.Bind(userId, accessModulesKey[i]));
                    }

                    ps = session.Prepare(CQLGenerator.InsertStatement("moderator_access_management",
                       new List<string> { "user_id", "created_by_admin_id", "expired_timestamp", "last_modified_by_admin_id", "last_updated_timestamp" }));
                    bs.Add(ps.Bind(userId, adminUserId, expiryDate, adminUserId, DateTime.UtcNow));
                }
                #endregion
                session.Execute(bs);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PostingSuspendedUserListResponse GetAllPostingSuspendedUser(String adminUserId, String companyId)
        {
            PostingSuspendedUserListResponse response = new PostingSuspendedUserListResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_user_suspension", new List<string> { }, new List<string> { "company_id" }));
                BoundStatement bs = ps.Bind(companyId);
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    List<User> users = new List<User>();
                    foreach (Row row in rowSet)
                    {
                        #region Get account type of user
                        ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                        bs = ps.Bind(row.GetValue<string>("user_id"));
                        Row row_account_type = session.Execute(bs).FirstOrDefault();
                        if (row_account_type != null && row_account_type.GetValue<int>("account_status") != AccountStatus.CODE_DELETED)
                        {
                            User user = new User();
                            user.Type = new AccountType(row_account_type.GetValue<int>("account_type"));

                            #region Get departments of user
                            ps = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" }));
                            bs = ps.Bind(row.GetValue<string>("user_id"));
                            RowSet rowSet_department_by_user = session.Execute(bs);
                            List<Department> departments = new List<Department>();
                            if (rowSet_department_by_user != null)
                            {
                                foreach (Row row_department_by_user in rowSet_department_by_user)
                                {
                                    ps = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id" }));
                                    bs = ps.Bind(companyId, row_department_by_user.GetValue<string>("department_id"));
                                    Row row_department = session.Execute(bs).FirstOrDefault();
                                    if (row_department != null)
                                    {
                                        Department department = new Department();
                                        department.Title = row_department.GetValue<string>("title");
                                        department.Id = row_department.GetValue<string>("id");
                                        departments.Add(department);
                                    }
                                }
                            }
                            if (departments.Count > 0)
                            {
                                user.Departments = departments;
                            }
                            #endregion

                            #region Get basic info of user
                            if (departments.Count > 0)
                            {
                                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<string>("user_id"));
                                Row row_user_basic = session.Execute(bs).FirstOrDefault();
                                if (row_user_basic != null)
                                {
                                    user.UserId = row.GetValue<string>("user_id");
                                    user.FirstName = row_user_basic.GetValue<string>("first_name");
                                    user.LastName = row_user_basic.GetValue<string>("last_name");
                                    user.ProfileImageUrl = row_user_basic.GetValue<string>("profile_image_url");
                                    users.Add(user);
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    response.Users = users;
                    response.Success = true;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserListResponse SearchUser(String adminUserId, String companyId, String searchKey)
        {
            UserListResponse response = new UserListResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database
                #region Get all departments of company
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("department_by_company", new List<string> { }, new List<string> { "company_id" }));
                BoundStatement bs = ps.Bind(companyId);
                RowSet rowSet_department_by_company = session.Execute(bs);
                #endregion
                if (rowSet_department_by_company != null)
                {
                    List<User> users = new List<User>();
                    foreach (Row row_department_by_company in rowSet_department_by_company)
                    {
                        #region Get all users of department
                        ps = session.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id", "user_status" }));
                        bs = ps.Bind(row_department_by_company.GetValue<string>("department_id"), AccountStatus.CODE_ACTIVE);
                        RowSet rowSet_user_by_department = session.Execute(bs);
                        if (rowSet_user_by_department != null)
                        {
                            foreach (Row row_user_by_department in rowSet_user_by_department)
                            {
                                User user = new User();
                                user.Position = row_user_by_department.GetValue<string>("position");
                                user.UserId = row_user_by_department.GetValue<string>("user_id");
                                #region Get all basic info of user
                                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row_user_by_department.GetValue<string>("user_id"));
                                Row row_user_basic = session.Execute(bs).FirstOrDefault();
                                if (row_user_basic != null)
                                {
                                    user.FirstName = row_user_basic.GetValue<string>("first_name");
                                    user.LastName = row_user_basic.GetValue<string>("last_name");
                                    user.Email = row_user_basic.GetValue<string>("email");
                                    user.ProfileImageUrl = row_user_basic.GetValue<string>("profile_image_url");
                                    users.Add(user);
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }

                    #region search user
                    List<User> searchedUser = new List<User>();
                    if (!String.IsNullOrEmpty(searchKey))
                    {
                        for (int i = 0; i < users.Count; i++)
                        {
                            string fullName = users[i].FirstName.ToLower() + " " + users[i].LastName.ToLower();
                            string reverseFullName = users[i].LastName.ToLower() + " " + users[i].FirstName.ToLower();

                            if (users[i].Email.ToLower().Contains(searchKey.ToLower()) || fullName.Contains(searchKey.ToLower()) || reverseFullName.Contains(searchKey.ToLower()))
                            {
                                searchedUser.Add(users[i]);
                            }
                        }
                    }
                    else
                    {
                        searchedUser = users;
                    }
                    #endregion
                    response.Users = searchedUser;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DepartmentDataEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentDataEmpty);
                    response.ErrorMessage = ErrorMessage.DepartmentDataEmpty;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserListResponse SearchUserForPostingSuspend(String adminUserId, String companyId, String searchKey)
        {
            UserListResponse response = new UserListResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database
                #region Get all departments of company
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("department_by_company", new List<string> { }, new List<string> { "company_id" }));
                BoundStatement bs = ps.Bind(companyId);
                RowSet rowSet_department_by_company = session.Execute(bs);
                #endregion
                if (rowSet_department_by_company != null)
                {
                    List<User> users = new List<User>();
                    foreach (Row row_department_by_company in rowSet_department_by_company)
                    {
                        #region Get all users of department
                        ps = session.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id", "user_status" }));
                        bs = ps.Bind(row_department_by_company.GetValue<string>("department_id"), AccountStatus.CODE_ACTIVE);
                        RowSet rowSet_user_by_department = session.Execute(bs);
                        if (rowSet_user_by_department != null)
                        {
                            foreach (Row row_user_by_department in rowSet_user_by_department)
                            {
                                // filter users. 1. admin 2. suspended users.
                                ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_user_suspension", new List<string> { }, new List<string> { "company_id", "user_id" }));
                                bs = ps.Bind(companyId, row_user_by_department.GetValue<string>("user_id"));
                                Row row_permission_feed_user_suspension = session.Execute(bs).FirstOrDefault();

                                if (!row_user_by_department.GetValue<string>("user_id").Equals(adminUserId) && row_permission_feed_user_suspension == null)
                                {
                                    User user = new User();
                                    user.Position = row_user_by_department.GetValue<string>("position");
                                    user.UserId = row_user_by_department.GetValue<string>("user_id");
                                    #region Get all basic info of user
                                    ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                                    bs = ps.Bind(row_user_by_department.GetValue<string>("user_id"));
                                    Row row_user_basic = session.Execute(bs).FirstOrDefault();
                                    if (row_user_basic != null)
                                    {
                                        user.FirstName = row_user_basic.GetValue<string>("first_name");
                                        user.LastName = row_user_basic.GetValue<string>("last_name");
                                        user.Email = row_user_basic.GetValue<string>("email");
                                        user.ProfileImageUrl = row_user_basic.GetValue<string>("profile_image_url");
                                        users.Add(user);
                                    }
                                    #endregion
                                }

                            }
                        }
                        #endregion
                    }

                    #region search user
                    List<User> searchedUser = new List<User>();
                    if (!String.IsNullOrEmpty(searchKey))
                    {
                        for (int i = 0; i < users.Count; i++)
                        {
                            string fullName = users[i].FirstName.ToLower() + " " + users[i].LastName.ToLower();
                            string reverseFullName = users[i].LastName.ToLower() + " " + users[i].FirstName.ToLower();
                            if (users[i].Email.ToLower().Contains(searchKey.ToLower()) || fullName.Contains(searchKey.ToLower()) || reverseFullName.Contains(searchKey.ToLower()))
                            {
                                searchedUser.Add(users[i]);
                            }
                        }
                    }
                    else
                    {
                        searchedUser = users;
                    }
                    #endregion
                    response.Users = searchedUser;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DepartmentDataEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentDataEmpty);
                    response.ErrorMessage = ErrorMessage.DepartmentDataEmpty;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PostingPermissionSuspendUserResponse PostingPermissionSuspendUsers(String adminUserId, String companyId, List<String> userIds)
        {
            PostingPermissionSuspendUserResponse response = new PostingPermissionSuspendUserResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. write to database.
                BatchStatement batchStatement = new BatchStatement();

                PreparedStatement ps;
                for (int i = 0; i < userIds.Count; i++)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("permission_feed_user_suspension", new List<string> { "company_id", "user_id", "last_assigned_by_user_id", "last_assigned_on_timestamp" }));
                    batchStatement.Add(ps.Bind(companyId, userIds[i], adminUserId, DateTime.UtcNow));
                }

                session.Execute(batchStatement);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PostingPermissionSuspendUserResponse PostingPermissionSuspendUser(String adminUserId, String companyId, String userId)
        {
            PostingPermissionSuspendUserResponse response = new PostingPermissionSuspendUserResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 1.2 Check feed permisson of user.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_user_suspension", new List<string> { }, new List<string> { "company_id", "user_id" }));
                Row r = session.Execute(ps.Bind(companyId, userId)).FirstOrDefault();
                if (r != null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserStatusIsIdentical);
                    response.ErrorMessage = ErrorMessage.UserStatusIsIdentical;
                    return response;
                }
                #endregion

                #endregion

                #region Step 2. write to database.
                BatchStatement batchStatement = new BatchStatement();
                ps = session.Prepare(CQLGenerator.InsertStatement("permission_feed_user_suspension", new List<string> { "company_id", "user_id", "last_assigned_by_user_id", "last_assigned_on_timestamp" }));
                batchStatement.Add(ps.Bind(companyId, userId, adminUserId, DateTime.UtcNow));

                session.Execute(batchStatement);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PostingPermissionRestoreUserResponse PostingPermissionRestoreUser(String adminUserId, String companyId, String userId)
        {
            PostingPermissionRestoreUserResponse response = new PostingPermissionRestoreUserResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Write database. Delete on permission_feed_user_suspension table
                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.DeleteStatement("permission_feed_user_suspension", new List<string> { "company_id", "user_id" }));
                BoundStatement bs = ps.Bind(companyId, userId);
                batchStatement.Add(bs);
                session.Execute(batchStatement);
                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public ModeratorChangeRightsResponse ModeratorChangeRights(String adminUserId, String companyId, String userId, List<int> accessModulesKey, DateTime? expiryDate)
        {
            ModeratorChangeRightsResponse response = new ModeratorChangeRightsResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                if (string.IsNullOrEmpty(userId))
                {
                    Log.Error("UserId is empty");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                #region Step 1.2 Check user account type
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs = ps.Bind(userId);
                Row rowUserAccountType = session.Execute(bs).FirstOrDefault();
                if (rowUserAccountType == null || rowUserAccountType.GetValue<int>("account_type") != AccountType.CODE_MODERATER)
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Write database. (Delete > Insert moderator_access_rights)
                BatchStatement batchStatement = new BatchStatement();
                ps = session.Prepare(CQLGenerator.DeleteStatement("moderator_access_rights", new List<string> { "user_id" }));
                bs = ps.Bind(userId);
                batchStatement.Add(bs);
                session.Execute(batchStatement);

                batchStatement = new BatchStatement();
                for (int i = 0; i < accessModulesKey.Count; i++)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("moderator_access_rights", new List<string> { "user_id", "access_rights_key" }));
                    batchStatement.Add(ps.Bind(userId, accessModulesKey[i]));
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("moderator_access_management",
                    new List<string>(), new List<string> { "user_id" }));
                Row moderatorRow = session.Execute(ps.Bind(userId)).FirstOrDefault();

                // Just assigned moderator rights
                if (moderatorRow == null)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("moderator_access_management",
                        new List<string> { "user_id", "expired_timestamp", "created_by_admin_id", "last_modified_by_admin_id", "last_updated_timestamp" }));
                    batchStatement.Add(ps.Bind(userId, expiryDate, adminUserId, adminUserId, DateTime.UtcNow));
                }
                // Update moderator rights
                else
                {
                    ps = session.Prepare(CQLGenerator.UpdateStatement("moderator_access_management",
                        new List<string> { "user_id" }, new List<string> { "expired_timestamp", "last_modified_by_admin_id", "last_updated_timestamp" }, new List<string>()));
                    batchStatement.Add(ps.Bind(expiryDate, adminUserId, DateTime.UtcNow, userId));
                }

                session.Execute(batchStatement);

                response.Success = true;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserImageProgressResponse SelectUserImageProgress(string userId, string imageName, ISession session = null)
        {
            UserImageProgressResponse userImageProgressResponse = new UserImageProgressResponse();
            userImageProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                IMapper mapper = new Mapper(session);
                UserImageProgress userImageProgress = mapper.FirstOrDefault<UserImageProgress>(CQLGenerator.SelectStatement("user_image_progress", new List<string> { }, new List<string> { "user_id", "image_name" }), userId, imageName);

                userImageProgressResponse.userImageProgress = userImageProgress;
                userImageProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                userImageProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                userImageProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return userImageProgressResponse;
        }

        public UserImageProgressResponse UpdateUserImageProgress(UserImageProgress userImageProgress,
            ISession session = null)
        {
            UserImageProgressResponse userImageProgressResponse = new UserImageProgressResponse();
            userImageProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                IMapper mapper = new Mapper(session);
                mapper.Update<UserImageProgress>(userImageProgress);

                userImageProgressResponse.userImageProgress = userImageProgress;
                userImageProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                userImageProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                userImageProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }

            return userImageProgressResponse;
        }

        public UserImageProgressResponse CreateUserImageProgress(
                                          string userId,
                                          string imageName,
                                          double progress,
                                          bool completed,
                                          bool uploaded,
                                          string environment,
                                          string companyId,
                                          ISession session = null)
        {
            UserImageProgressResponse userImageProgressResponse = new UserImageProgressResponse();
            userImageProgressResponse.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                var newUserImageProgress = new UserImageProgress()
                {
                    ImageName = imageName,
                    UserId = userId,
                    Progress = progress,
                    Completed = completed,
                    Uploaded = uploaded,
                    Environment = environment,
                    CompanyId = companyId
                };

                IMapper mapper = new Mapper(session);
                mapper.Insert(newUserImageProgress);

                userImageProgressResponse.userImageProgress = newUserImageProgress;
                userImageProgressResponse.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                userImageProgressResponse.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                userImageProgressResponse.ErrorMessage = ErrorMessage.SystemError;
            }
            return userImageProgressResponse;
        }

        public UserUploadProfileImageResponse UploadProfileImage(string uploaderUserId, string adminUserId, string companyId, string uploadedImageUrl, int approvalState, ISession session = null)
        {
            UserUploadProfileImageResponse response = new UserUploadProfileImageResponse();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    Row companyRow = vh.ValidateCompany(companyId, session);

                    if (companyRow == null)
                    {
                        Log.Error("Company is invalid: " + companyId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                        response.ErrorMessage = ErrorMessage.CompanyInvalid;
                        return response;
                    }

                    Row userRow = vh.ValidateUser(uploaderUserId, companyId, session);

                    if (userRow == null)
                    {
                        Log.Error("User is invalid: " + uploaderUserId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }
                }

                #region Get AutoApprovalProfilePhoto setting
                IMapper mapper = new Mapper(session);
                Setting autoApprovalSetting = mapper.FirstOrDefault<Setting>("WHERE company_id = ? AND setting_key = ?",
                   companyId,
                   SettingKey.AutoApproval);
                if (autoApprovalSetting == null) // There is no auto approval setting at Setting table.
                {
                    mapper.Insert<Setting>(new Setting { CompanyId = companyId, Key = SettingKey.AutoApproval, Value = "false" });
                }
                else
                {
                    if (Convert.ToBoolean(autoApprovalSetting.Value))
                    {
                        approvalState = (int)Dashboard.ProfileApprovalState.Approved;
                    }
                }
                #endregion


                string approvalId = UUIDGenerator.GenerateUniqueIDForProfileApproval();
                DateTimeOffset currentTime = DateTime.UtcNow;

                CreateUserProfileImageInAlbum(uploaderUserId, adminUserId, companyId, uploadedImageUrl, approvalId, approvalState, currentTime, session);

                if (approvalState == (int)Dashboard.ProfileApprovalState.Pending)
                {
                    Dashboard dashboard = new Dashboard();
                    dashboard.CreateApprovalForProfileImage(approvalId, uploaderUserId, adminUserId, uploadedImageUrl, companyId, currentTime, approvalState, session);
                }
                else if (approvalState == (int)Dashboard.ProfileApprovalState.Approved) // Update user_basic table
                {
                    BatchStatement bs = new BatchStatement();
                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "last_modified_by_user_id", "last_modified_timestamp", "profile_image_url" }, new List<string>()));
                    bs.Add(ps.Bind(uploaderUserId, DateTime.UtcNow, uploadedImageUrl, uploaderUserId));
                    session.Execute(bs);
                }

                #region Get user detail
                User user = new User();
                #region company information of user
                PreparedStatement ps_company = session.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_company = ps_company.Bind(companyId);
                Row row_company = session.Execute(bs_company).FirstOrDefault();
                if (row_company != null)
                {
                    Company company = new Company();
                    company.CompanyId = companyId;
                    company.CompanyTitle = row_company.GetValue<String>("title");
                    company.CompanyLogoUrl = row_company.GetValue<String>("logo_url");
                    company.CompanyBannerUrl = row_company.GetValue<String>("client_banner_url");
                    company.MatchupBannerUrl = row_company.GetValue<String>("client_matchup_banner_url");
                    company.ProfilePopupBannerUrl = row_company.GetValue<String>("client_profile_popup_banner_url");
                    company.CompanyPullRefreshBannerUrl = row_company.GetValue<String>("client_pull_refresh_banner_url");
                    user.Company = company;
                }
                else
                {
                    Log.Error(ErrorMessage.CompanyInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }
                #endregion
                #region departments of user
                List<Department> departments = new List<Department>();
                PreparedStatement ps_department_by_user = session.Prepare(CQLGenerator.SelectStatement("department_by_user", new List<string> { }, new List<string> { "user_id" })); ;
                BoundStatement bs_department_by_user = ps_department_by_user.Bind(uploaderUserId);
                RowSet rowSet = session.Execute(bs_department_by_user);
                foreach (Row r in rowSet)
                {
                    Department department = new Department();
                    PreparedStatement ps_department = session.Prepare(CQLGenerator.SelectStatement("department", new List<string> { }, new List<string> { "company_id", "id", "is_valid" })); ;
                    BoundStatement bs_department = ps_department.Bind(companyId, r.GetValue<String>("department_id"), true);
                    Row row_department = session.Execute(bs_department).FirstOrDefault();
                    if (row_department == null)
                    {
                        Log.Error(ErrorMessage.DepartmentIsInvalid);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.DepartmentIsInvalid);
                        response.ErrorMessage = ErrorMessage.DepartmentIsInvalid;
                        return response;
                    }
                    else
                    {
                        department.Id = row_department.GetValue<String>("id");
                        department.Title = row_department.GetValue<String>("title");
                    }

                    PreparedStatement ps_user_by_department = session.Prepare(CQLGenerator.SelectStatement("user_by_department", new List<string> { }, new List<string> { "department_id", "user_id" })); ;
                    BoundStatement bs_user_by_department = ps_user_by_department.Bind(r.GetValue<String>("department_id"), uploaderUserId);
                    Row row_user_by_department = session.Execute(bs_user_by_department).FirstOrDefault();
                    if (row_user_by_department != null)
                    {
                        department.Position = row_user_by_department.GetValue<String>("position");
                    }
                    departments.Add(department);
                }
                user.Departments = departments;
                #endregion
                #region account type of user
                PreparedStatement ps_user_account_type = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs_user_account_type = ps_user_account_type.Bind(uploaderUserId);
                Row row_user_account_type = session.Execute(bs_user_account_type).FirstOrDefault();
                if (row_user_account_type != null)
                {
                    user.Type = new AccountType(row_user_account_type.GetValue<int>("account_type"));
                    user.Status = new AccountStatus(row_user_account_type.GetValue<int>("account_status"));
                    user.LastModifiedStatusTimestamp = row_user_account_type.GetValue<DateTimeOffset>("last_modified_timestamp");
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion
                #region basic information of user
                PreparedStatement ps_user_basic = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_basic = ps_user_basic.Bind(uploaderUserId);
                Row row_user_basic = session.Execute(bs_user_basic).FirstOrDefault();
                if (row_user_basic != null)
                {
                    user.UserId = uploaderUserId;
                    user.Email = row_user_basic.GetValue<String>("email");
                    user.FirstName = row_user_basic.GetValue<String>("first_name");
                    user.LastName = row_user_basic.GetValue<String>("last_name");
                    if (row_user_basic["gender"] == null)
                    {
                        user.Gender = null;
                    }
                    else
                    {
                        user.Gender = row_user_basic.GetValue<int>("gender");
                    }

                    if (row_user_basic["date_of_birth"] == null)
                    {
                        user.Birthday = null;
                        user.BirthdayString = null;
                    }
                    else
                    {
                        user.Birthday = row_user_basic.GetValue<DateTimeOffset>("date_of_birth");
                        DateTimeOffset bd = row_user_basic.GetValue<DateTimeOffset>("date_of_birth");
                        JsonSerializerSettings jss = new JsonSerializerSettings
                        {
                            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                        };
                        user.BirthdayString = JsonConvert.SerializeObject(bd, jss).Replace("\\", "").Replace("\"", "");
                    }

                    user.ProfileImageUrl = row_user_basic.GetValue<String>("profile_image_url");
                    user.LastModifiedProfileTimestamp = row_user_basic.GetValue<DateTimeOffset>("last_modified_timestamp");
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion
                #region contact information of user
                PreparedStatement ps_user_contact = session.Prepare(CQLGenerator.SelectStatement("user_contact", new List<string> { }, new List<string> { "id" }));
                BoundStatement bs_user_contact = ps_user_contact.Bind(uploaderUserId);
                Row row_user_contact = session.Execute(bs_user_contact).FirstOrDefault();
                if (row_user_contact != null)
                {
                    user.Address = row_user_contact.GetValue<String>("address");
                    user.AddressCountryName = row_user_contact.GetValue<String>("address_country_name");
                    user.AddressPostalCode = row_user_contact.GetValue<String>("address_postal_code");
                    user.Phone = row_user_contact.GetValue<String>("phone");
                    user.PhoneCountryCode = row_user_contact.GetValue<String>("phone_country_code");
                    user.PhoneCountryAbb = row_user_contact.GetValue<String>("phone_country_name");
                }
                #endregion
                #endregion

                response.ProfileImageStatus = approvalState;
                response.UserDetail = user;

                if (approvalState == (int)Dashboard.ProfileApprovalState.Approved)
                {
                    CacheHelper.UpdateCacheForColleague(user.Company.CompanyId, user.Departments[0].Id, user.Departments[0].Title, user.UserId, user.Email, user.FirstName, user.LastName, user.ProfileImageUrl, user.Departments[0].Position, user.Departments[0].Id);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        #region User Profile Album
        public bool CreateUserProfileImageInAlbum(string userId, string adminUserId, string companyId, string uploadedImageUrl, string approvalId, int approvalState, DateTimeOffset currentTimestamp, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("profile_image_approval_by_user_timestamp_desc",
                      new List<string> { "approval_id", "company_id", "user_id", "profile_image_url", "approval_state", "seek_approval_on_timestamp", "updated_by_admin_user_id", "updated_timestamp" }));
                session.Execute(preparedStatement.Bind(approvalId, companyId, userId, uploadedImageUrl, approvalState, currentTimestamp, adminUserId, currentTimestamp));

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }

        public bool UpdateUserProfileImageApprovalInAlbum(string userId, string adminUserId, string approvalId, int newApprovalState, DateTimeOffset currentTimestamp, DateTimeOffset seekApprovalTimestamp, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("profile_image_approval_by_user_timestamp_desc",
                    new List<string> { "user_id", "seek_approval_on_timestamp", "approval_id" }, new List<string> { "approval_state", "updated_by_admin_user_id", "updated_timestamp" }, new List<string>()));
                session.Execute(preparedStatement.Bind(newApprovalState, adminUserId, currentTimestamp, userId, seekApprovalTimestamp, approvalId));

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }
        #endregion

        public UserIdResponse GetUserId(String userId, String companyId)
        {

            UserIdResponse response = new UserIdResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                //ValidationHandler vh = new ValidationHandler();
                //ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                //if (es != null)
                //{
                //    Log.Error(es.ErrorMessage);
                //    response.ErrorCode = es.ErrorCode;
                //    response.ErrorMessage = es.ErrorMessage;
                //    return response;
                //}
                #endregion
                #endregion

                #region Step 2. Read database.
                //PreparedStatement ps_moderator_access_rights = session.Prepare(CQLGenerator.SelectStatement("moderator_access_rights", new List<string> { }, new List<string> { "user_id" }));
                //BoundStatement bs_moderator_access_rights = ps_moderator_access_rights.Bind(userId);
                //RowSet rowSet_moderator_access_rights = session.Execute(bs_moderator_access_rights);

                RowSet rowSet = session.Execute(string.Format(
                    "SELECT * FROM user_authentication WHERE email = '{0}' AND company_id = '{1}';", userId, companyId));

                if (rowSet != null)
                {
                    Row row = rowSet.First();
                    response.UserId = row.GetValue<String>("user_id");
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    response.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public BasicUserInfoResponse GetBasicUserInfo(String userId)
        {

            BasicUserInfoResponse response = new BasicUserInfoResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                //ValidationHandler vh = new ValidationHandler();
                //ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                //if (es != null)
                //{
                //    Log.Error(es.ErrorMessage);
                //    response.ErrorCode = es.ErrorCode;
                //    response.ErrorMessage = es.ErrorMessage;
                //    return response;
                //}
                #endregion
                #endregion

                #region Step 2. Read database.
                RowSet rowSet = session.Execute(string.Format(
                    "SELECT * FROM user_basic WHERE id = '{0}';", userId));

                if (rowSet != null)
                {
                    Row row = rowSet.First();
                    response.UserId = row.GetValue<String>("id");
                    response.Email = row.GetValue<String>("email");
                    response.FirstName = row.GetValue<String>("first_name");
                    response.LastName = row.GetValue<String>("last_name");
                    response.ProfileImageUrl = row.GetValue<String>("profile_image_url");

                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    response.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserAccountTypeResponse GetUserAccountType(String userId)
        {

            UserAccountTypeResponse response = new UserAccountTypeResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                //ValidationHandler vh = new ValidationHandler();
                //ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                //if (es != null)
                //{
                //    Log.Error(es.ErrorMessage);
                //    response.ErrorCode = es.ErrorCode;
                //    response.ErrorMessage = es.ErrorMessage;
                //    return response;
                //}
                #endregion
                #endregion

                #region Step 2. Read database.
                RowSet rowSet = session.Execute(string.Format(
                    "SELECT * FROM user_account_type WHERE user_id = '{0}';", userId));

                if (rowSet != null)
                {
                    Row row = rowSet.First();

                    response.AccountType = new AccountType(row.GetValue<int>("account_type"));
                    response.AccountStatus = row.GetValue<int>("account_status");
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    response.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public ModeratorAccessRightsResponse GetModeratorAccessRights(String userId)
        {

            ModeratorAccessRightsResponse response = new ModeratorAccessRightsResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                //ValidationHandler vh = new ValidationHandler();
                //ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                //if (es != null)
                //{
                //    Log.Error(es.ErrorMessage);
                //    response.ErrorCode = es.ErrorCode;
                //    response.ErrorMessage = es.ErrorMessage;
                //    return response;
                //}
                #endregion
                #endregion

                #region Step 2. Read database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("moderator_access_rights", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs = ps.Bind(userId);
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    DateTime? accessExpiryDate = null;
                    List<Module> accessModules = new List<Module>();
                    foreach (Row rowRights in rowSet)
                    {
                        accessModules.Add(new Module(rowRights.GetValue<int>("access_rights_key")));
                    }

                    // Get expiry date
                    ps = session.Prepare(CQLGenerator.SelectStatement("moderator_access_management", new List<string> { }, new List<string> { "user_id" }));
                    Row row = session.Execute(ps.Bind(userId)).FirstOrDefault();
                    if (rowSet != null)
                    {
                        accessExpiryDate = row.GetValue<DateTime?>("expired_timestamp");
                    }

                    response.Modules = accessModules;
                    response.AccessExpiryDate = accessExpiryDate;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    response.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public ErrorUser CheckUserInputData(String companyId, String managerId, String firstName, String lastName, String email, String departmentName, String designation, String gender, String birthday, String phoneCountryCode, String phoneNumber)
        {
            ErrorUser errorUser = new ErrorUser();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region First name
                if (String.IsNullOrEmpty(firstName))
                {
                    //errorUser.FirstName = firstName;
                    errorUser.ErrorReason.Add(ErrorMessage.UserFirstNameIsEmpty);
                }
                #endregion

                #region Last name
                if (String.IsNullOrEmpty(lastName))
                {
                    //errorUser.LastName = lastName;
                    errorUser.ErrorReason.Add(ErrorMessage.UserLastNameIsEmpty);
                }
                #endregion

                #region Email
                if (String.IsNullOrEmpty(email))
                {
                    errorUser.ErrorReason.Add(ErrorMessage.UserEmailIsEmpty);
                }
                else
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string>(), new List<string> { "email", "company_id" }));
                    Row row = session.Execute(ps.Bind(email, companyId)).FirstOrDefault();
                    if (row != null)
                    {
                        errorUser.ErrorReason.Add(ErrorMessage.UserDuplicatedEmail);
                    }
                }
                #endregion

                #region Department
                if (String.IsNullOrEmpty(departmentName))
                {
                    errorUser.ErrorReason.Add(ErrorMessage.DepartmentNameCantBeEmpty);
                }
                #endregion

                #region Designation /* Dont check */
                #endregion

                #region Gender
                if (!String.IsNullOrEmpty(gender))
                {
                    try
                    {
                        int iGender = Convert.ToInt16(gender);
                        if (iGender != 1 || iGender != 2)
                        {
                            errorUser.ErrorReason.Add(ErrorMessage.UserGenderIsInvalid);
                        }
                    }
                    catch (Exception)
                    {
                        errorUser.ErrorReason.Add(ErrorMessage.UserGenderIsInvalid);
                    }
                }
                #endregion

                #region Birthday
                if (!String.IsNullOrEmpty(birthday))
                {
                    try
                    {
                        DateTime bd = Convert.ToDateTime(birthday);
                    }
                    catch (Exception)
                    {
                        errorUser.ErrorReason.Add(ErrorMessage.UserBirthdayIsInvalid);
                    }
                }

                #endregion

                #region PhoneCountryCode
                if (!String.IsNullOrEmpty(phoneCountryCode))
                {
                    try
                    {
                        int countryCode = Convert.ToInt16(gender);
                    }
                    catch (Exception)
                    {
                        errorUser.ErrorReason.Add(ErrorMessage.UserBirthdayIsInvalid);
                    }
                }
                #endregion

                #region PhoneNumber /* Dont check */
                #endregion

                if (errorUser.ErrorReason == null || errorUser.ErrorReason.Count < 1)
                {
                    errorUser = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return errorUser;
        }

        public UserDetailResponse GetUserLastLoginDateTime(string companyId, string userId)
        {
            UserDetailResponse response = new UserDetailResponse();
            response.Success = false;
            response.User = null;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("user_login_log", new List<string>(), new List<string> { "company_id" }));
                RowSet rowSet = analyticSession.Execute(ps.Bind(companyId));
                if (rowSet != null)
                {
                    foreach (Row r in rowSet)
                    {
                        if (r.GetValue<string>("user_id").Equals(userId))
                        {
                            response.User = new User
                            {
                                UserId = userId,
                                LastLoginDateTime = r.GetValue<DateTime>("login_date_timestamp")
                            };
                            break;
                        }
                    }


                    if (response.User != null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string>(), new List<string> { "id" }));
                        Row r = mainSession.Execute(ps.Bind(response.User.UserId)).FirstOrDefault();
                        if (r != null)
                        {
                            response.User.FirstName = r.GetValue<string>("first_name");
                            response.User.LastName = r.GetValue<string>("last_name");
                            response.User.Email = r.GetValue<string>("email");

                            response.Success = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public UserCleanUpResponse RemoveRecentlyDeletedUsers()
        {
            UserCleanUpResponse response = new UserCleanUpResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string>(), new List<string> { "account_status" }));
                RowSet rowSet = session.Execute(ps.Bind(AccountStatus.CODE_DELETING));

                DateTime currentTime = DateTime.UtcNow;
                int maxDays = Convert.ToInt16(WebConfigurationManager.AppSettings["deleting_limit_days"]);
                foreach (Row row in rowSet)
                {
                    DateTime lastModifiedTimestamp = row.GetValue<DateTime>("last_modified_timestamp");
                    if (currentTime >= lastModifiedTimestamp.AddDays(maxDays))
                    {
                        string userId = row.GetValue<string>("user_id");
                        ps = session.Prepare(CQLGenerator.SelectStatementWithAllowFiltering("user_authentication", new List<string>(), new List<string> { "user_id" }));
                        Row userRow = session.Execute(ps.Bind(userId)).FirstOrDefault();
                        if (userRow != null)
                        {
                            string companyId = userRow.GetValue<string>("company_id");
                            UserDeleteResponse deleteResponse = Delete(companyId, companyId, userId, session);

                            if (!deleteResponse.Success)
                            {
                                response.ErrorCode = deleteResponse.ErrorCode;
                                response.ErrorMessage = deleteResponse.ErrorMessage;
                                return response;
                            }
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public UserSelectProfileResponse SelectProfile(string requesterUserId, string targetedUserId, string companyId)
        {
            UserSelectProfileResponse response = new UserSelectProfileResponse();
            response.ColoredBrainAssessment = null;
            response.UserProfile = new User();
            response.NumberOfUnlockedAchievements = 0;
            response.Badge = new Gamification.Achievement();
            response.ColoredBrainAboutUser = null;
            response.ColoredBrainFrameColor = null;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                Row requesterUserRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (requesterUserRow == null)
                {
                    Log.Error("Invalid requester user id: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                response.UserProfile = SelectUserBasic(targetedUserId, companyId, true, mainSession, null, true).User;

                if (response.UserProfile == null)
                {
                    Log.Error("Invalid targeted user id: " + targetedUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserTargetInvalid);
                    response.ErrorMessage = ErrorMessage.UserTargetInvalid;
                    return response;
                }

                Gamification gamificationManager = new Gamification();
                AssessmentColoredBrain colorBrainManager = new AssessmentColoredBrain();

                List<Gamification.Achievement> unlockedAchievements = gamificationManager.SelectAllAchievementsByUser(targetedUserId, companyId, (int)Gamification.GamificationQueryType.Basic, mainSession, analyticSession).Achievements.Where(a => a.IsUnlocked).ToList();
                response.NumberOfUnlockedAchievements = unlockedAchievements.Count;
                response.Badge = unlockedAchievements.Where(a => a.IsSetAsBadge).FirstOrDefault();

                AssessmentSelectColorBrainPersonalityResponse coloredBrainResult = colorBrainManager.SelectColorBrainPersonalityByUser(requesterUserId, targetedUserId, companyId, mainSession, analyticSession);
                if (coloredBrainResult.Success)
                {
                    response.ColoredBrainAssessment = coloredBrainResult.ColorBrainAssessment;
                    response.ColoredBrainAboutUser = coloredBrainResult.ColoredBrainAboutUser;
                    response.ColoredBrainFrameColor = coloredBrainResult.ColoredBrainFrameColor;
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        #region Implement IEquatable<User>
        //public bool Equals(User other)
        //{
        //    if (other == null)
        //        return false;

        //    if (Id == other.UserId)
        //        return true;
        //    else
        //        return false;
        //}

        //public override bool Equals(object obj)
        //{
        //    if (Object.ReferenceEquals(null, obj))
        //    {
        //        return false;
        //    }

        //    // Is the same object?
        //    if (Object.ReferenceEquals(this, obj))
        //    {
        //        return true;
        //    }

        //    // Is the same type?
        //    if (obj.GetType() != this.GetType())
        //    {
        //        return false;
        //    }

        //    return Equals(obj as User);
        //}

        //public override int GetHashCode()
        //{
        //    return UserId.GetHashCode();
        //}
        #endregion
    }

    [Serializable]
    [DataContract]
    public class ErrorUser
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public int CSVNo { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Email { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Department { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> ErrorReason { get; set; }

    }

    public class UserImageProgress
    {
        // IMapper class
        public string UserId { get; set; }
        public string ImageName { get; set; }
        public double Progress { get; set; }
        public bool Completed { get; set; }
        public bool Uploaded { get; set; }
        public string Environment { get; set; }
        public string CompanyId { get; set; }
    }
}