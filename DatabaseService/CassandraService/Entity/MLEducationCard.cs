﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLEducationCard
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum QueryType
        {
            Basic = 1,
            Full = 2,
            FullWithAnalytics = 3
        }

        public enum CardType
        {
            SelectOne = 1,
            MultiChoice = 2,
            Instructions = 3,
            Article = 4,
            Video = 5,
            Slides = 6
        }

        public enum MLCardStatus
        {
            Deleted = -1,
            Active = 1,
            Hidden = 2
        }

        public enum MLUploadTypeEnum
        {
            Image = 1,
            VideoLandscape = 2,
            PdfPortrait = 3,
            PdfLandscape = 4,
            SlideShare = 5,
            VideoPortrait = 6
        }

        [DataMember]
        public string CardId { get; set; }

        [DataMember]
        public int Type { get; set; }


        [DataMember]
        public bool HasUploadedContent { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public List<MLUploadedContent> UploadedContent { get; set; }


        [DataMember]
        public List<MLPageContent> PagesContent { get; set; }

        [DataMember]
        public bool IsOptionRandomized { get; set; }

        [DataMember]
        public int MinOptionToSelect { get; set; }

        [DataMember]
        public int MaxOptionToSelect { get; set; }

        [DataMember]
        public int Paging { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int BackgroundType { get; set; }

        [DataMember]
        public List<MLEducationOption> Options { get; set; }

        [DataMember]
        public int Attempt { get; set; }

        public string GenerateCardID()
        {
            return UUIDGenerator.GenerateUniqueIDForMLEducationCard();
        }

        public MLEducationCardCreateResponse Create(string cardId,
                                                int type,
                                                bool isOptionRandomized,
                                                int backgoundType,
                                                string categoryId,
                                                string educationId,
                                                string adminUserId,
                                                string companyId,
                                                string content,
                                                int minOptionToSelect = 0,
                                                int maxOptionToSelect = 0,
                                                string description = null,
                                                List<MLUploadedContent> uploadedContent = null,
                                                List<MLEducationOption> options = null,
                                                List<MLPageContent> pagesContent = null,
                                                int ordering = -1)
        {
            MLEducationCardCreateResponse response = new MLEducationCardCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);

                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int32.Parse(ErrorCode.MLEducationCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                    return response;
                }

                // Check education row
                Row educationRow = vh.ValidateMLEducation(companyId, categoryId, educationId, session);

                if (educationRow == null)
                {
                    Log.Error("Invalid educationId: " + educationId);
                    response.ErrorCode = Int32.Parse(ErrorCode.MLEducationInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(cardId))
                {
                    cardId = UUIDGenerator.GenerateUniqueIDForMLEducationCard();
                }

                List<string> uploadedUrls = new List<string>();
                MLEducationOption optionManagement = new MLEducationOption();

                MLEducationOptionsCreateResponse createOptionsResponse = new MLEducationOptionsCreateResponse();
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                // Setting to default
                switch (type)
                {
                    case (int)CardType.SelectOne:
                        minOptionToSelect = 1;
                        maxOptionToSelect = 1;
                        break;
                    case (int)CardType.MultiChoice:
                        break;
                    case (int)CardType.Instructions:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                    case (int)CardType.Article:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                    case (int)CardType.Video:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                    case (int)CardType.Slides:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                }

                if (type != (int)CardType.Article)
                {
                    // Check content
                    if (string.IsNullOrEmpty(content))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardContentEmpty);
                        response.ErrorMessage = ErrorMessage.MLCardContentEmpty;
                        return response;
                    }
                }

                if (type == (int)CardType.SelectOne || type == (int)CardType.MultiChoice)
                {
                    if (type == (int)CardType.MultiChoice)
                    {
                        if (maxOptionToSelect < minOptionToSelect)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionSelectionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.MLOptionSelectionRangeIsInvalid;
                            return response;
                        }
                    }

                    createOptionsResponse = optionManagement.CreateOptions(adminUserId, cardId, ordering, educationId, options, session);

                    if (!createOptionsResponse.Success)
                    {
                        Log.Error(createOptionsResponse.ErrorMessage);
                        response.ErrorCode = createOptionsResponse.ErrorCode;
                        response.ErrorMessage = createOptionsResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in createOptionsResponse.BoundStatements)
                    {
                        batch.Add(bs);
                    }

                    foreach (string url in createOptionsResponse.UploadedUrls)
                    {
                        uploadedUrls.Add(url);
                    }
                }
                else if (type == (int)CardType.Article)
                {

                    if (pagesContent.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingPageContent);
                        response.ErrorMessage = ErrorMessage.MLMissingPageContent;
                        return response;
                    }

                    int paging = 1;
                    foreach (MLPageContent page in pagesContent)
                    {
                        if (string.IsNullOrEmpty(page.Content))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingPageContent);
                            response.ErrorMessage = ErrorMessage.MLMissingPageContent;
                            return response;
                        }

                        if (string.IsNullOrEmpty(page.Title))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingPageTitle);
                            response.ErrorMessage = ErrorMessage.MLMissingPageTitle;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_page_content",
                            new List<string> { "card_id", "title", "content", "paging" }));
                        batch.Add(ps.Bind(cardId, page.Title, page.Content, paging));

                        paging++;
                    }
                }
                else if (type == (int)CardType.Video)
                {
                    if (uploadedContent.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingVideo);
                        response.ErrorMessage = ErrorMessage.MLMissingVideo;
                        return response;
                    }

                    MLUploadedContent upContent = uploadedContent[0];
                    //upContent.UploadType = (int)MLUploadTypeEnum.Video;
                    uploadedContent = new List<MLUploadedContent>();
                    uploadedContent.Add(upContent);
                }
                else if (type == (int)CardType.Slides)
                {
                    if (uploadedContent.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingDocument);
                        response.ErrorMessage = ErrorMessage.MLMissingDocument;
                        return response;
                    }
                }

                ps = session.Prepare(CQLGenerator.CountStatement("ml_education_card_order", new List<string> { "ml_education_id" }));

                bool isUpdate = true;
                if (ordering == -1)
                {
                    isUpdate = false;
                    ordering = (int)session.Execute(ps.Bind(educationId)).FirstOrDefault().GetValue<long>("count") + 1;
                }

                backgoundType = backgoundType <= 0 ? 1 : backgoundType;

                bool hasUploadedContent = uploadedContent != null && uploadedContent.Count > 0 ? true : false;
                DateTime currentDate = DateTime.UtcNow;
                ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card",
                    new List<string> { "ml_education_id", "id", "background_image_type", "content", "created_by_admin_id", "created_on_timestamp", "description", "has_uploaded_content", "is_option_randomized", "last_modified_by_admin_id", "last_modified_timestamp", "max_option_to_select", "min_option_to_select", "ordering", "status", "type" }));
                batch.Add(ps.Bind(educationId, cardId, backgoundType, content, adminUserId, currentDate, description, hasUploadedContent, isOptionRandomized, adminUserId, currentDate, maxOptionToSelect, minOptionToSelect, ordering, (int)MLCardStatus.Active, type));

                if (hasUploadedContent)
                {
                    int uploadOrdering = 1;
                    foreach (MLUploadedContent upContent in uploadedContent)
                    {
                        string uploadId = UUIDGenerator.GenerateUniqueIDForMLUploadedContent();
                        string url = upContent.Url;
                        int uploadType = upContent.UploadType;

                        if (string.IsNullOrEmpty(url))
                        {
                            Log.Error(ErrorMessage.MLCardUploadContentUrlMissing);
                            response.ErrorCode = Int32.Parse(ErrorCode.MLCardUploadContentUrlMissing);
                            response.ErrorMessage = ErrorMessage.MLCardUploadContentUrlMissing;
                            return response;
                        }

                        upContent.Ordering = uploadOrdering;

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_uploaded_content",
                            new List<string> { "card_id", "upload_id", "upload_type", "url", "ordering" }));
                        batch.Add(ps.Bind(cardId, uploadId, uploadType, url, upContent.Ordering));

                        uploadedUrls.Add(url);
                        uploadOrdering++;
                    }
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_order",
                    new List<string> { "ml_education_id", "card_id", "ordering" }));
                batch.Add(ps.Bind(educationId, cardId, ordering));

                if (isUpdate)
                {
                    Delete(educationId, categoryId, cardId, adminUserId, companyId, false, session, uploadedUrls);
                }

                session.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCardUpdateResponse Delete(string educationId, string categoryId, string cardId, string adminUserId, string companyId, bool isReorderingNeeded = true, ISession session = null, List<string> newImageFileNames = null)
        {
            MLEducationCardUpdateResponse response = new MLEducationCardUpdateResponse();
            response.Success = false;

            try
            {
                int currentOrder = 0;

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    // Check education category
                    Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);

                    if (categoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Int32.Parse(ErrorCode.MLEducationCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                        return response;
                    }

                    // Check education row
                    Row topicRow = vh.ValidateMLEducation(companyId, categoryId, educationId, session);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid educationId: " + educationId);
                        response.ErrorCode = Int32.Parse(ErrorCode.MLEducationInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                        return response;
                    }

                    // Check card row
                    Row cardRow = vh.ValidateMLEducationCard(cardId, educationId, session);

                    if (cardRow == null)
                    {
                        Log.Error("Card already been deleted: " + cardId);
                        response.ErrorCode = Int32.Parse(ErrorCode.MLEducationCardAlreadyBeenDeleted);
                        response.ErrorMessage = ErrorMessage.MLEducationCardAlreadyBeenDeleted;
                        return response;
                    }

                    currentOrder = cardRow.GetValue<int>("ordering");
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Remove from S3
                String bucketName = "cocadre-" + companyId.ToLower();

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;

                    //cocadre-{CompanyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{FileName}.{FileFormaat}
                    listRequest.Prefix = "mlearnings/educations/" + educationId + "/" + cardId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        if (imageObject.Size <= 0)
                        {
                            continue;
                        }

                        if (newImageFileNames != null && newImageFileNames.Count > 0)
                        {
                            //mlearnings/educations/MLT943cee78090948d086af5e30d051a250/MLCf60dd0247a12406ea824e7d59c5007cc/1_20160510101139774_original.jpeg
                            string[] stringToken = imageObject.Key.Split('/');
                            string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                            string[] fileNames = fileNamePath.Split('_');
                            string fileName = fileNames[0] + "_" + fileNames[1];

                            if (newImageFileNames.Any(newFileName => newFileName.Contains(fileName)))
                            {
                                continue;
                            }
                        }

                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }

                    if (isReorderingNeeded)
                    {
                        // Delete folder if this is not update
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = "mlearnings/educations/" + educationId + "/" + cardId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_uploaded_content", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_page_content", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                MLEducationOptionUpdateResponse optionResponse = new MLEducationOption().DeleteAllOptions(educationId, cardId, companyId, session, newImageFileNames);
                if (!optionResponse.Success)
                {
                    Log.Error(optionResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                    response.ErrorMessage = optionResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in optionResponse.BoundStatements)
                {
                    deleteBatch.Add(bs);
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card", new List<string> { "ml_education_id", "id" }));
                deleteBatch.Add(ps.Bind(educationId, cardId));

                // Reorder card
                if (isReorderingNeeded)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("ml_education_card_order", new List<string>(), new List<string> { "ml_education_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                    RowSet orderRowset = session.Execute(ps.Bind(educationId, currentOrder));

                    foreach (Row orderRow in orderRowset)
                    {
                        int order = orderRow.GetValue<int>("ordering");
                        string nextCardId = orderRow.GetValue<string>("card_id");

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_order", new List<string> { "ml_education_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(educationId, order, nextCardId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("ml_education_card", new List<string> { "ml_education_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                        updateBatch.Add(ps.Bind(order - 1, educationId, nextCardId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_order", new List<string> { "ml_education_id", "ordering", "card_id" }));
                        updateBatch.Add(ps.Bind(educationId, order - 1, nextCardId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_order", new List<string> { "ml_education_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(educationId, currentOrder, cardId));
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public int SelectNumberOfCards(string topicId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psCard = session.Prepare(CQLGenerator.CountStatement("ml_education_card",
                    new List<string> { "ml_education_id", "status" }));
                Row cardRow = session.Execute(psCard.Bind(topicId, (int)MLCardStatus.Active)).FirstOrDefault();

                if (cardRow != null)
                {
                    number = (int)cardRow.GetValue<long>("count");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        public MLEducationCardDetailResponse GetDetail(string adminUserId, string companyId, string cardId, string educationId, int queryType, string answeredByUserId = null, ISession mainSession = null, ISession analyticSession = null)
        {
            MLEducationCardDetailResponse response = new MLEducationCardDetailResponse();
            response.Card = null;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_card", new List<string>(), new List<string> { "ml_education_id", "id", "status" }));
                Row cardRow = mainSession.Execute(ps.Bind(educationId, cardId, (int)MLCardStatus.Active)).FirstOrDefault();

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCardInvalid;
                    return response;
                }

                // Get Basic Details
                int ordering = cardRow.GetValue<int>("ordering");
                string content = cardRow.GetValue<string>("content");
                string description = cardRow.GetValue<string>("description");
                bool hasUploadedContent = cardRow.GetValue<bool>("has_uploaded_content");
                int type = cardRow.GetValue<int>("type");
                List<MLPageContent> pagesContent = new List<MLPageContent>();
                List<MLUploadedContent> uploadedContent = new List<MLUploadedContent>();
                List<MLEducationOption> options = new List<MLEducationOption>();
                int minOptionToSelect = cardRow["min_option_to_select"] == null ? 0 : cardRow.GetValue<int>("min_option_to_select");
                int maxOptionToSelect = cardRow["max_option_to_select"] == null ? 0 : cardRow.GetValue<int>("max_option_to_select");

                bool isOptionRandomized = false;
                int backgroundType = 1;
                int attempt = 0;

                if (hasUploadedContent)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_card_uploaded_content", new List<string>(), new List<string> { "card_id" }));
                    RowSet uploadContentRowset = mainSession.Execute(ps.Bind(cardId));

                    foreach (Row uploadContentRow in uploadContentRowset)
                    {
                        string uploadId = uploadContentRow.GetValue<string>("upload_id");
                        string url = uploadContentRow.GetValue<string>("url");
                        int order = uploadContentRow.GetValue<int>("ordering");
                        int uploadType = uploadContentRow.GetValue<int>("upload_type");

                        if (answeredByUserId != null && answeredByUserId != string.Empty)
                        {
                            url = url.Replace("mp4", "m3u8");
                        }

                        MLUploadedContent upContent = new MLUploadedContent
                        {
                            UploadId = uploadId,
                            Url = url,
                            Ordering = order,
                            UploadType = uploadType
                        };

                        uploadedContent.Add(upContent);
                    }

                    uploadedContent = uploadedContent.OrderBy(o => o.Ordering).ToList();
                }

                MLEducationOption optionManager = new MLEducationOption();

                // Get the first page as the content for the basic card
                if (type == (int)CardType.Article)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_card_page_content", new List<string>(), new List<string> { "card_id", "paging" }));
                    Row pageContentRow = mainSession.Execute(ps.Bind(cardId, 1)).FirstOrDefault();

                    if (pageContentRow != null)
                    {
                        string pageContent = pageContentRow.GetValue<string>("content");
                        string pageTitle = pageContentRow.GetValue<string>("title");
                        int paging = pageContentRow.GetValue<int>("paging");

                        MLPageContent pContent = new MLPageContent
                        {
                            Title = pageTitle,
                            Content = pageContent,
                            Paging = paging
                        };

                        pagesContent.Add(pContent);
                    }
                }

                // Get Full Details
                if (queryType >= (int)QueryType.Full)
                {
                    isOptionRandomized = cardRow.GetValue<bool>("is_option_randomized");
                    if (!string.IsNullOrEmpty(adminUserId))
                    {
                        isOptionRandomized = false;
                    }

                    backgroundType = cardRow.GetValue<int>("background_image_type");

                    if (type == (int)CardType.SelectOne || type == (int)CardType.MultiChoice)
                    {
                        options = optionManager.GetList(educationId, companyId, cardId, mainSession, analyticSession, answeredByUserId);
                    }
                    else if (type == (int)CardType.Article)
                    {
                        pagesContent = new List<MLPageContent>();

                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_card_page_content", new List<string>(), new List<string> { "card_id" }));
                        RowSet pagesContentRowset = mainSession.Execute(ps.Bind(cardId));

                        foreach (Row pageContentRow in pagesContentRowset)
                        {
                            string pageContent = pageContentRow.GetValue<string>("content");
                            string pageTitle = pageContentRow.GetValue<string>("title");
                            int paging = pageContentRow.GetValue<int>("paging");

                            MLPageContent pContent = new MLPageContent
                            {
                                Title = pageTitle,
                                Content = pageContent,
                                Paging = paging
                            };

                            pagesContent.Add(pContent);
                        }

                        pagesContent = pagesContent.OrderBy(p => p.Paging).ToList();
                    }


                    if (analyticSession != null && !string.IsNullOrEmpty(answeredByUserId))
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("ml_education_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }, 1));
                        Row rowAnsweredLog = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();
                        if (rowAnsweredLog != null)
                        {
                            attempt = rowAnsweredLog.GetValue<int>("attempt");
                        }
                    }
                }

                response.Card = new MLEducationCard
                {
                    CardId = cardId,
                    Type = type,
                    HasUploadedContent = hasUploadedContent,
                    Content = content,
                    Description = description,
                    UploadedContent = uploadedContent,
                    Options = options,
                    IsOptionRandomized = isOptionRandomized,
                    Ordering = ordering,
                    BackgroundType = backgroundType,
                    PagesContent = pagesContent,
                    MinOptionToSelect = minOptionToSelect,
                    MaxOptionToSelect = maxOptionToSelect,
                    Attempt = attempt
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }


        public MLEducationCardListResponse GetList(string educationId, string categoryId, string adminUserId, string companyId, int queryType, string containsName = null, string answeredByUserId = null, ISession mainSession = null, ISession analyticSession = null)
        {
            MLEducationCardListResponse response = new MLEducationCardListResponse();
            response.Cards = new List<MLEducationCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    Row categoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, mainSession);
                    if (categoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                        return response;
                    }

                    Row topicRow = vh.ValidateMLEducation(companyId, categoryId, educationId, mainSession);
                    if (topicRow == null)
                    {
                        Log.Error("Invalid educationId: " + educationId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                        return response;
                    }
                }

                if (analyticSession == null && (queryType == (int)QueryType.FullWithAnalytics || !string.IsNullOrEmpty(answeredByUserId)))
                {
                    analyticSession = cm.getAnalyticSession();
                }


                if (!string.IsNullOrEmpty(containsName))
                {
                    containsName = containsName.Trim();
                }

                // Select card details using ml_education_card_order
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_education_card_order", new List<string>(), new List<string> { "ml_education_id" }));
                RowSet cardRowset = mainSession.Execute(ps.Bind(educationId));

                foreach (Row cardRow in cardRowset)
                {
                    string cardId = cardRow.GetValue<string>("card_id");
                    int ordering = cardRow.GetValue<int>("ordering");

                    MLEducationCard selectedCard = GetDetail(adminUserId, companyId, cardId, educationId, queryType, answeredByUserId, mainSession, analyticSession).Card;

                    if (selectedCard != null)
                    {
                        selectedCard.Ordering = ordering;

                        if (!string.IsNullOrEmpty(containsName))
                        {
                            if (selectedCard.Type == (int)CardType.Article)
                            {
                                foreach (MLPageContent pageContent in selectedCard.PagesContent)
                                {
                                    if (pageContent.Title.ToLower().Contains(containsName.ToLower()) || pageContent.Content.ToLower().Contains(containsName.ToLower()))
                                    {
                                        response.Cards.Add(selectedCard);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(selectedCard.Content) && selectedCard.Content.ToLower().Contains(containsName.ToLower()))
                                {
                                    response.Cards.Add(selectedCard);
                                }
                                else if (!string.IsNullOrEmpty(selectedCard.Description) && selectedCard.Description.ToLower().Contains(containsName.ToLower()))
                                {
                                    response.Cards.Add(selectedCard);
                                }
                                else
                                {
                                    foreach (MLEducationOption option in selectedCard.Options)
                                    {
                                        if (option.Content.ToLower().Contains(containsName.ToLower()))
                                        {
                                            response.Cards.Add(selectedCard);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            response.Cards.Add(selectedCard);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCardUpdateResponse Update(string cardId,
                                       int newType,
                                       bool newIsOptionRandomized,
                                       int newBackgoundType,
                                       string categoryId,
                                       string educationId,
                                       string adminUserId,
                                       string companyId,
                                       string newContent,
                                       int newMinOptionToSelect = 0,
                                       int newMaxOptionToSelect = 0,
                                       string newDescription = null,
                                       List<MLUploadedContent> newUploadedContent = null,
                                       List<MLEducationOption> newOptions = null,
                                       List<MLPageContent> newPagesContent = null)
        {
            MLEducationCardUpdateResponse response = new MLEducationCardUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateMLEducationCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int32.Parse(ErrorCode.MLEducationCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCategoryInvalid;
                    return response;
                }

                // Check education row
                Row educationRow = vh.ValidateMLEducation(companyId, categoryId, educationId, session);

                if (educationRow == null)
                {
                    Log.Error("Invalid educationId: " + educationId);
                    response.ErrorCode = Int32.Parse(ErrorCode.MLEducationInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                    return response;
                }

                int educationStatus = educationRow.GetValue<int>("status");
                DateTime startDate = educationRow.GetValue<DateTime>("start_date");
                DateTime? endDate = educationRow.GetValue<DateTime?>("end_date");
                DateTime currentDate = DateTime.UtcNow;
                int progress = new MLEducation().SelectProgress(currentDate, startDate, endDate);

                // Check card row
                Row cardRow = vh.ValidateMLEducationCard(cardId, educationId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int32.Parse(ErrorCode.MLEducationCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCardInvalid;
                    return response;
                }

                if (newType != (int)CardType.Article)
                {
                    // Check content
                    if (string.IsNullOrEmpty(newContent))
                    {
                        Log.Error("Invalid content");
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardContentEmpty);
                        response.ErrorMessage = ErrorMessage.MLCardContentEmpty;
                        return response;
                    }
                }

                if (educationStatus == (int)MLEducation.MLEducationStatusEnum.Unlisted || progress == (int)MLEducation.ProgressStatusEnum.Upcoming)
                {
                    // Check card for error
                    int currentOrder = cardRow.GetValue<int>("ordering");
                    MLEducationCardCreateResponse createResponse = Create(cardId, newType, newIsOptionRandomized, newBackgoundType, categoryId, educationId, adminUserId, companyId, newContent, newMinOptionToSelect, newMaxOptionToSelect,
                        newDescription, newUploadedContent, newOptions, newPagesContent, currentOrder);

                    response.Success = createResponse.Success;
                    response.ErrorCode = createResponse.ErrorCode;
                    response.ErrorMessage = createResponse.ErrorMessage;
                }
                else
                {
                    bool hasUploadedContent = newUploadedContent.Count > 0 ? true : false;

                    BatchStatement deleteBatch = new BatchStatement();
                    BatchStatement updateBatch = new BatchStatement();

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("ml_education_card", new List<string> { "ml_educations_id", "id" }, new List<string> {
                        "backgound_image_type",
                        "content",
                        "description",
                        "has_uploaded_content",
                        "is_option_randomized",
                        "last_modified_by_admin_id",
                        "last_modified_timestamp",
                        "max_option_to_select",
                        "min_option_to_select"
                    }, new List<string>()));
                    updateBatch.Add(ps.Bind(
                        newBackgoundType,
                        newContent,
                        newDescription,
                        hasUploadedContent,
                        newIsOptionRandomized,
                        adminUserId,
                        currentDate,
                        newMaxOptionToSelect,
                        newMinOptionToSelect,
                        educationId, cardId));

                    // Update option
                    int currentCardOrdering = cardRow.GetValue<int>("ordering");
                    MLEducationOptionUpdateResponse optionResponse = new MLEducationOption().UpdateOptions(cardId, educationId, currentCardOrdering, newOptions, companyId, session);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.BoundStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    // Delete from S3
                    bool currentHasImage = cardRow.GetValue<bool>("has_uploaded_content");
                    List<string> currentImageUrls = new List<string>();
                    if (currentHasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card_uploaded_content", new List<string>(), new List<string> { "card_id" }));
                        RowSet currentImageRowset = session.Execute(ps.Bind(cardId));

                        foreach (Row currentImageRow in currentImageRowset)
                        {
                            string url = currentImageRow.GetValue<string>("url");
                            currentImageUrls.Add(url);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_educaation_card_uploaded_content", new List<string> { "card_id" }));
                        deleteBatch.Add(ps.Bind(cardId));
                    }

                    if (newUploadedContent.Count > 0)
                    {
                        int order = 1;
                        foreach (MLUploadedContent upContent in newUploadedContent)
                        {
                            string uploadId = UUIDGenerator.GenerateUniqueIDForMLUploadedContent();
                            string url = upContent.Url;
                            int uploadType = upContent.UploadType;

                            if (string.IsNullOrEmpty(url))
                            {
                                Log.Error(ErrorMessage.MLCardUploadContentUrlMissing);
                                response.ErrorCode = Int32.Parse(ErrorCode.MLCardUploadContentUrlMissing);
                                response.ErrorMessage = ErrorMessage.MLCardUploadContentUrlMissing;
                                return response;
                            }

                            ps = session.Prepare(CQLGenerator.InsertStatement("ml_education_card_uploaded_content",
                                new List<string> { "card_id", "upload_id", "upload_type", "url", "ordering" }));
                            updateBatch.Add(ps.Bind(cardId, uploadId, uploadType, url, order));

                            order++;

                            if (currentImageUrls.Contains(url))
                            {
                                currentImageUrls.Remove(url);
                            }
                        }
                    }

                    if (currentImageUrls.Count > 0)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();
                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            // Remaining image url to be remove
                            foreach (string currentImageUrl in currentImageUrls)
                            {
                                string path = currentImageUrl.Replace("https://", "");
                                string[] splitPath = path.Split('/');
                                string imageName = splitPath[splitPath.Count() - 1];

                                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "mlearnings/educations/" + educationId + "/" + cardId + "/" + imageName
                                };

                                s3Client.DeleteObject(deleteObjectRequest);
                            }
                        }
                    }

                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCardUpdateResponse DeleteAllCards(string educationId, string companyId, ISession session)
        {
            MLEducationCardUpdateResponse response = new MLEducationCardUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_education_card", new List<string>(), new List<string> { "ml_education_id" }));
                RowSet cardRowset = session.Execute(ps.Bind(educationId));

                String bucketName = "cocadre-" + companyId.ToLower();
                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    foreach (Row cardRow in cardRowset)
                    {
                        deleteBatch = new BatchStatement();
                        string cardId = cardRow.GetValue<string>("id");
                        bool hasUploadedContent = cardRow.GetValue<bool>("has_uploaded_content");
                        int order = cardRow.GetValue<int>("ordering");
                        if (hasUploadedContent)
                        {
                            // Remove from S3
                            ListObjectsRequest listRequest = new ListObjectsRequest();
                            listRequest.BucketName = bucketName;
                            listRequest.Prefix = "mlearnings/educations/" + educationId + "/" + cardId;

                            ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                            foreach (S3Object imageObject in listResponse.S3Objects)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = imageObject.Key;
                                s3Client.DeleteObject(deleteRequest);
                            }

                            DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                            deleteFolderRequest.BucketName = bucketName;
                            deleteFolderRequest.Key = "mlearnings/educations/" + educationId + "/" + cardId;
                            s3Client.DeleteObject(deleteFolderRequest);

                            ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_uploaded_content", new List<string> { "card_id" }));
                            deleteBatch.Add(ps.Bind(cardId));
                        }

                        MLEducationOptionUpdateResponse optionResponse = new MLEducationOption().DeleteAllOptions(educationId, cardId, companyId, session, null);
                        if (!optionResponse.Success)
                        {
                            Log.Error(optionResponse.ErrorMessage);
                            response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                            response.ErrorMessage = optionResponse.ErrorMessage;
                            return response;
                        }

                        foreach (BoundStatement bs in optionResponse.BoundStatements)
                        {
                            deleteBatch.Add(bs);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card", new List<string> { "ml_education_id", "id" }));
                        deleteBatch.Add(ps.Bind(educationId, cardId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_education_card_order", new List<string> { "ml_education_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(educationId, order, cardId));

                        session.Execute(deleteBatch);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationCardAnswerResponse AnswerCard(string answeredByUserId, string companyId, string educationId, string categoryId, string cardId, List<string> optionIds)
        {
            MLEducationCardAnswerResponse response = new MLEducationCardAnswerResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row educationRow = vh.ValidateMLEducation(companyId, categoryId, educationId, mainSession);

                if (educationRow == null || educationRow != null && educationRow.GetValue<int>("status") == (int)MLEducation.MLEducationStatusEnum.Hidden)
                {
                    Log.Error("Invalid educationId: " + educationId);
                    response.ErrorCode = Int32.Parse(ErrorCode.MLEducationInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateMLEducationCard(cardId, educationId, mainSession);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int32.Parse(ErrorCode.MLEducationCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLEducationCardInvalid;
                    return response;
                }

                foreach (string optionId in optionIds)
                {
                    Row optionRow = vh.ValidateMLEducationCardOption(cardId, optionId, mainSession);

                    if (optionRow == null)
                    {
                        Log.Error("Invalid optionId: " + optionId);
                        response.ErrorCode = Int32.Parse(ErrorCode.MLEducationOptionInvalid);
                        response.ErrorMessage = ErrorMessage.MLEducationOptionInvalid;
                        return response;
                    }
                }

                AnalyticMLEducation analyticManager = new AnalyticMLEducation();
                Thread updateThread = new Thread(() => analyticManager.AnswerCard(answeredByUserId, educationId, cardId, optionIds, analyticSession));
                updateThread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        //public int FindPageForCard(string educationId, int cardOrder, ISession session)
        //{
        //    int page = 1;

        //    try
        //    {
        //        PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("ml_education_card_order", new List<string>(), new List<string> { "ml_education_id" }, "ordering", CQLGenerator.Comparison.LessThan, 0));
        //        RowSet orderRowset = session.Execute(ps.Bind(educationId, cardOrder));

        //        foreach (Row orderRow in orderRowset)
        //        {
        //            string cardId = orderRow.GetValue<string>("card_id");
        //            MLCard card = SelectCard(null, null, cardId, educationId, (int)MLCardQueryType.Basic, null, null, session, null).Card;

        //            if (card != null && card.HasPageBreak)
        //            {
        //                page++;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.ToString(), ex);
        //    }

        //    return page;
        //}
    }
}
