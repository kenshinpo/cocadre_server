﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [DataContract]
    public class SurveyQuestionAnswer
    {
        public string Id { get; set; }
        public string Text { get; set; }
    }

    public class SurveyQuestionCustomAction
    {
        public string Condition { get; set; }
        public string RedirectQuestion { get; set; }
    }

    [DataContract]
    public class SurveyCategory
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public DateTime ListedDateTime { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int SurveyCount { get; set; }
    }

    [DataContract]
    public class SurveyQuestion
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember]
        public string PageNo { get; set; }

        [DataMember]
        public string QuestionNo { get; set; }

        [DataMember]
        public string QuestionId { get; set; }

        [DataMember]
        public Survey.RSQuestionType QuestionType { get; set; }

        [DataMember]
        public string QuestionText { get; set; }

        [DataMember]
        public List<SurveyQuestionAnswer> AnswerList { get; set; }

        ////////////////////////////////////////
        // Data items on the 3rd column of the card
        ////////////////////////////////////////

        [DataMember]
        public string QuestionStatus { get; set; }

        [DataMember]
        public bool AllowQuestionToBeSkipped{ get; set; }

        [DataMember]
        public bool AllowOthers { get; set; }

        [DataMember]
        public bool RandomizeOptionOrder { get; set; }

        [DataMember]
        public int AnswerDisplay { get; set; }

        [DataMember]
        public bool PageBreak { get; set; }

        [DataMember]
        public List<SurveyQuestionCustomAction> CustomActionList { get; set; }

        public SurveyQuestion()
        {
            this.AnswerList = new List<SurveyQuestionAnswer>();
            this.CustomActionList = new List<SurveyQuestionCustomAction>();
        }
    }

    [DataContract]
    public class Survey
    {
        #region enumerations

        [Serializable]
        public enum RSQuestionType
        {
            SelectOne = 1,
            MultiChoice = 2,
            Text = 3,
            NumberRange = 4,
            DropList = 5,
            Instructions = 6
        }

        [Serializable]
        public enum RSQuestionStatus
        {
            Active = 1,
            Hidden = 2,
            Deleted = -1
        }

        [Serializable]
        public enum RSTopicStatus
        {
            Unlisted = 1,
            Active = 2,
            Hidden = 3,
            Deleted = -1
        }

        #endregion enumerations

        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public DateTime ListedDateTime { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SurveyLogoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SurveyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SurveyTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SurveyIntroduction{ get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SurveyClosing { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SurveyCategory { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalNumberOfQuestions { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public RSTopicStatus SurveyStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Participants { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int TotalUsers { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CompletedUsers { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<SurveyQuestion> SurveyQuestionList { get; set; }

        ////////////////////////////////////////
        // Obsolete data members
        ////////////////////////////////////////

        [Obsolete]
        [DataMember(EmitDefaultValue = false)]
        public string SurveyTopic { get; set; }

        public Survey()
        {
            this.SurveyQuestionList = new List<SurveyQuestion>();
        }
            

    }
}
