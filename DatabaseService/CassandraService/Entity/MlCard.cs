﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class MLCard
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum MLCardQueryType
        {
            Basic = 1,
            Full = 2,
            FullWithAnalytics = 3
        }

        public enum MLCardType
        {
            SelectOne = 1,
            MultiChoice = 2,
            Instructional = 3,
            ReadAndAnswer = 4,
            WatchAndAnswer = 5,
            Slides = 6
        }

        public enum MLCardStatus
        {
            Deleted = -1,
            Active = 1,
            Hidden = 2
        }

        public enum MLUploadTypeEnum
        {
            Image = 1,
            VideoLandscape = 2,
            PdfPortrait = 3,
            PdfLandscape = 4,
            SlideShare = 5,
            VideoPortrait = 6
        }

        public enum MLScoringCalculationType
        {
            OnlyRightAnswerHasScore = 1,
            AllRightAnswersThenHaveScore = 2
        }

        [DataMember]
        public string CardId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public bool HasUploadedContent { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public List<MLUploadedContent> UploadedContent { get; set; }

        [DataMember]
        public List<MLPageContent> PagesContent { get; set; }

        [DataMember]
        public bool IsOptionRandomized { get; set; }

        [DataMember]
        public int MinOptionToSelect { get; set; }

        [DataMember]
        public int MaxOptionToSelect { get; set; }

        [DataMember]
        public bool HasPageBreak { get; set; }

        [DataMember]
        public int Paging { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int BackgroundType { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public List<MLOption> Options { get; set; }

        [DataMember]
        public int ScoringCalculationType { get; set; }

        [DataMember]
        public int CorrectAttempt { get; set; }

        [DataMember]
        public int IncorrectAttempt { get; set; }

        [DataMember]
        public int UserCardScore { get; set; }

        [DataMember]
        public bool HasAttemptedByUser { get; set; }


        public string GenerateCardID()
        {
            return UUIDGenerator.GenerateUniqueIDForMLCard();
        }

        public MLCardCreateResponse CreateMLCard(string cardId,
                                                 int type,
                                                 bool isOptionRandomized,
                                                 bool hasPageBreak,
                                                 int backgoundType,
                                                 string note,
                                                 string categoryId,
                                                 string topicId,
                                                 string adminUserId,
                                                 string companyId,
                                                 string content,
                                                 int scoringCalculationType = 1,
                                                 int minOptionToSelect = 0,
                                                 int maxOptionToSelect = 0,
                                                 string description = null,
                                                 List<MLUploadedContent> uploadedContent = null,
                                                 List<MLOption> options = null,
                                                 List<MLPageContent> pagesContent = null,
                                                 int ordering = -1)
        {
            MLCardCreateResponse response = new MLCardCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(cardId))
                {
                    cardId = UUIDGenerator.GenerateUniqueIDForMLCard();
                }

                List<string> uploadedUrls = new List<string>();
                MLOption optionManagement = new MLOption();
                MLOptionCreateResponse optionResponse = new MLOptionCreateResponse();
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                // Setting to default
                switch (type)
                {
                    case (int)MLCardType.SelectOne:
                        minOptionToSelect = 1;
                        maxOptionToSelect = 1;
                        break;
                    case (int)MLCardType.MultiChoice:
                        break;
                    case (int)MLCardType.Instructional:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                    case (int)MLCardType.ReadAndAnswer:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                    case (int)MLCardType.WatchAndAnswer:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                    case (int)MLCardType.Slides:
                        minOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        isOptionRandomized = false;
                        break;
                }

                scoringCalculationType = (int)MLScoringCalculationType.OnlyRightAnswerHasScore;

                if (type != (int)MLCardType.ReadAndAnswer)
                {
                    // Check content
                    if (string.IsNullOrEmpty(content))
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardContentEmpty);
                        response.ErrorMessage = ErrorMessage.MLCardContentEmpty;
                        return response;
                    }
                }

                if (type == (int)MLCardType.SelectOne || type == (int)MLCardType.MultiChoice)
                {
                    if (type == (int)MLCardType.MultiChoice)
                    {
                        if (maxOptionToSelect < minOptionToSelect)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionSelectionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.MLOptionSelectionRangeIsInvalid;
                            return response;
                        }

                        if(maxOptionToSelect > options.Count)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionSelectionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.MLOptionSelectionRangeIsInvalid;
                            return response;
                        }
                    }

                    optionResponse = optionManagement.CreateOptions(adminUserId, cardId, ordering, topicId, options, minOptionToSelect, maxOptionToSelect, session);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.BoundStatements)
                    {
                        batch.Add(bs);
                    }

                    foreach (string url in optionResponse.UploadedUrls)
                    {
                        uploadedUrls.Add(url);
                    }
                }
                else if (type == (int)MLCardType.ReadAndAnswer)
                {

                    if (pagesContent.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingPageContent);
                        response.ErrorMessage = ErrorMessage.MLMissingPageContent;
                        return response;
                    }

                    int paging = 1;
                    foreach (MLPageContent page in pagesContent)
                    {
                        if (string.IsNullOrEmpty(page.Content))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingPageContent);
                            response.ErrorMessage = ErrorMessage.MLMissingPageContent;
                            return response;
                        }

                        if (string.IsNullOrEmpty(page.Title))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingPageTitle);
                            response.ErrorMessage = ErrorMessage.MLMissingPageTitle;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_page_content",
                            new List<string> { "card_id", "title", "content", "paging" }));
                        batch.Add(ps.Bind(cardId, page.Title, page.Content, paging));

                        paging++;
                    }
                }
                else if (type == (int)MLCardType.WatchAndAnswer)
                {
                    if (uploadedContent.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingVideo);
                        response.ErrorMessage = ErrorMessage.MLMissingVideo;
                        return response;
                    }

                    MLUploadedContent upContent = uploadedContent[0];
                    //upContent.UploadType = (int)MLUploadTypeEnum.Video;
                    uploadedContent = new List<MLUploadedContent>();
                    uploadedContent.Add(upContent);
                }
                else if (type == (int)MLCardType.Slides)
                {
                    if (uploadedContent.Count == 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLMissingDocument);
                        response.ErrorMessage = ErrorMessage.MLMissingDocument;
                        return response;
                    }
                }

                ps = session.Prepare(CQLGenerator.CountStatement("ml_card_order", new List<string> { "ml_topic_id" }));

                bool isUpdate = true;
                if (ordering == -1)
                {
                    isUpdate = false;
                    ordering = (int)session.Execute(ps.Bind(topicId)).FirstOrDefault().GetValue<long>("count") + 1;
                }

                backgoundType = backgoundType <= 0 ? 1 : backgoundType;

                bool hasUploadedContent = uploadedContent != null && uploadedContent.Count > 0 ? true : false;
                DateTime currentDate = DateTime.UtcNow;
                ps = session.Prepare(CQLGenerator.InsertStatement("ml_card",
                    new List<string> { "ml_topic_id", "id", "type", "has_uploaded_content", "content", "description", "is_option_randomized", "has_page_break", "ordering", "background_image_type", "note", "min_option_to_select", "max_option_to_select", "scoring_calculation_type", "status", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(topicId, cardId, type, hasUploadedContent, content, description, isOptionRandomized, hasPageBreak, ordering, backgoundType, note, minOptionToSelect, maxOptionToSelect, scoringCalculationType, (int)MLCardStatus.Active, adminUserId, currentDate, adminUserId, currentDate));

                if (hasUploadedContent)
                {
                    int uploadOrdering = 1;
                    foreach (MLUploadedContent upContent in uploadedContent)
                    {
                        string uploadId = UUIDGenerator.GenerateUniqueIDForMLUploadedContent();
                        string url = upContent.Url;
                        int uploadType = upContent.UploadType;

                        if (string.IsNullOrEmpty(url))
                        {
                            Log.Error(ErrorMessage.MLCardUploadContentUrlMissing);
                            response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardUploadContentUrlMissing);
                            response.ErrorMessage = ErrorMessage.MLCardUploadContentUrlMissing;
                            return response;
                        }

                        upContent.Ordering = uploadOrdering;

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_uploaded_content",
                            new List<string> { "card_id", "upload_id", "upload_type", "url", "ordering" }));
                        batch.Add(ps.Bind(cardId, uploadId, uploadType, url, upContent.Ordering));

                        uploadedUrls.Add(url);
                        uploadOrdering++;
                    }
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_order",
                    new List<string> { "ml_topic_id", "card_id", "ordering" }));
                batch.Add(ps.Bind(topicId, cardId, ordering));

                if (isUpdate)
                {
                    DeleteCard(topicId, categoryId, cardId, adminUserId, companyId, false, session, uploadedUrls);
                }

                session.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLCardUpdateResponse UpdateCard(string cardId,
                                               int newType,
                                               bool newIsOptionRandomized,
                                               bool newHasPageBreak,
                                               int newBackgoundType,
                                               string newNote,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               string newContent,
                                               int newScoringCalculationType = 1,
                                               int newMinOptionToSelect = 0,
                                               int newMaxOptionToSelect = 0,
                                               string newDescription = null,
                                               List<MLUploadedContent> newUploadedContent = null,
                                               List<MLOption> newOptions = null,
                                               List<MLPageContent> newPagesContent = null)
        {
            MLCardUpdateResponse response = new MLCardUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                int topicStatus = topicRow.GetValue<int>("status");
                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");
                DateTime currentDate = DateTime.UtcNow;
                int progress = new MLTopic().SelectProgress(currentDate, startDate, endDate);

                // Check card row
                Row cardRow = vh.ValidateMLTopicCard(cardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLCardInvalid;
                    return response;
                }

                if (newType != (int)MLCardType.ReadAndAnswer)
                {
                    // Check content
                    if (string.IsNullOrEmpty(newContent))
                    {
                        Log.Error("Invalid content");
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardContentEmpty);
                        response.ErrorMessage = ErrorMessage.MLCardContentEmpty;
                        return response;
                    }
                }


                if (topicStatus == (int)MLTopic.MLTopicStatusEnum.Unlisted || progress == (int)MLTopic.ProgressStatusEnum.Upcoming)
                {
                    // Check card for error
                    int currentOrder = cardRow.GetValue<int>("ordering");
                    MLCardCreateResponse createResponse = CreateMLCard(cardId, newType, newIsOptionRandomized, newHasPageBreak, newBackgoundType, newNote, categoryId, topicId, adminUserId, companyId, newContent, newScoringCalculationType, newMinOptionToSelect, newMaxOptionToSelect, newDescription, newUploadedContent, newOptions, newPagesContent, currentOrder);

                    response.Success = createResponse.Success;
                    response.ErrorCode = createResponse.ErrorCode;
                    response.ErrorMessage = createResponse.ErrorMessage;
                }
                else
                {
                    bool hasUploadedContent = newUploadedContent.Count > 0 ? true : false;
                    newScoringCalculationType = (int)MLScoringCalculationType.OnlyRightAnswerHasScore;

                    BatchStatement deleteBatch = new BatchStatement();
                    BatchStatement updateBatch = new BatchStatement();

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("ml_card", new List<string> { "ml_topic_id", "id" }, new List<string> {
                        "backgound_image_type",
                        "content",
                        "description",
                        "has_page_break",
                        "has_uploaded_content",
                        "is_option_randomized",
                        "last_modified_by_admin_id",
                        "last_modified_timestamp",
                        "max_option_to_select",
                        "min_option_to_select",
                        "note",
                        "scoring_calculation_type"

                    }, new List<string>()));
                    updateBatch.Add(ps.Bind(
                        newBackgoundType,
                        newContent,
                        newDescription,
                        newHasPageBreak,
                        hasUploadedContent,
                        newIsOptionRandomized,
                        adminUserId,
                        currentDate,
                        newMaxOptionToSelect,
                        newMinOptionToSelect,
                        newNote,
                        newScoringCalculationType,
                        topicId,
                        cardId));

                    // Update option
                    int currentCardOrdering = cardRow.GetValue<int>("ordering");
                    MLOptionUpdateResponse optionResponse = new MLOption().UpdateOptions(cardId, topicId, currentCardOrdering, newOptions, companyId, session);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.BoundStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    // Delete from S3
                    bool currentHasImage = cardRow.GetValue<bool>("has_uploaded_content");
                    List<string> currentImageUrls = new List<string>();
                    if (currentHasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("ml_card_uploaded_content", new List<string>(), new List<string> { "card_id" }));
                        RowSet currentImageRowset = session.Execute(ps.Bind(cardId));

                        foreach (Row currentImageRow in currentImageRowset)
                        {
                            string url = currentImageRow.GetValue<string>("url");
                            currentImageUrls.Add(url);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_uploaded_content", new List<string> { "card_id" }));
                        deleteBatch.Add(ps.Bind(cardId));
                    }

                    if (newUploadedContent.Count > 0)
                    {
                        int order = 1;
                        foreach (MLUploadedContent upContent in newUploadedContent)
                        {
                            string uploadId = UUIDGenerator.GenerateUniqueIDForMLUploadedContent();
                            string url = upContent.Url;
                            int uploadType = upContent.UploadType;

                            if (string.IsNullOrEmpty(url))
                            {
                                Log.Error(ErrorMessage.MLCardUploadContentUrlMissing);
                                response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardUploadContentUrlMissing);
                                response.ErrorMessage = ErrorMessage.MLCardUploadContentUrlMissing;
                                return response;
                            }

                            ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_uploaded_content",
                                new List<string> { "card_id", "upload_id", "upload_type", "url", "ordering" }));
                            updateBatch.Add(ps.Bind(cardId, uploadId, uploadType, url, order));

                            order++;

                            if (currentImageUrls.Contains(url))
                            {
                                currentImageUrls.Remove(url);
                            }
                        }
                    }

                    if (currentImageUrls.Count > 0)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();
                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            // Remaining image url to be remove
                            foreach (string currentImageUrl in currentImageUrls)
                            {
                                string path = currentImageUrl.Replace("https://", "");
                                string[] splitPath = path.Split('/');
                                string imageName = splitPath[splitPath.Count() - 1];

                                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "mlearnings/" + topicId + "/" + cardId + "/" + imageName
                                };

                                s3Client.DeleteObject(deleteObjectRequest);
                            }
                        }
                    }

                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private MLCardCheckErrorResponse CheckCardForError(string topicId,
                                                           int currentCardOrdering,
                                                           int type,
                                                           List<MLOption> options,
                                                           ISession session)
        {
            MLCardCheckErrorResponse response = new MLCardCheckErrorResponse();
            response.Success = false;
            try
            {
                if (type == (int)MLCardType.SelectOne || type == (int)MLCardType.MultiChoice)
                {
                    if (options != null || options.Count > 0)
                    {
                        MLOption optionManager = new MLOption();
                        foreach (MLOption option in options)
                        {
                            bool optionHasImage = option.HasUploadedContent;

                            // Check if option is empty
                            if (string.IsNullOrEmpty(option.Content))
                            {
                                Log.Error("Invalid option content");
                                response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionContentEmpty);
                                response.ErrorMessage = ErrorMessage.MLOptionContentEmpty;
                                return response;
                            }

                            if (optionHasImage)
                            {
                                foreach (MLUploadedContent upContent in option.UploadedContent)
                                {
                                    string url = upContent.Url;

                                    if (string.IsNullOrEmpty(url))
                                    {
                                        Log.Error(ErrorMessage.MLCardUploadContentUrlMissing);
                                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardUploadContentUrlMissing);
                                        response.ErrorMessage = ErrorMessage.MLCardUploadContentUrlMissing;
                                        return response;
                                    }
                                }
                            }
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;

        }

        public MLCardUpdateResponse DeleteAllCards(string topicId, string companyId, ISession session)
        {
            MLCardUpdateResponse response = new MLCardUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("ml_card", new List<string>(), new List<string> { "ml_topic_id" }));
                RowSet cardRowset = session.Execute(ps.Bind(topicId));

                String bucketName = "cocadre-" + companyId.ToLower();
                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    foreach (Row cardRow in cardRowset)
                    {
                        deleteBatch = new BatchStatement();
                        string cardId = cardRow.GetValue<string>("id");
                        bool hasUploadedContent = cardRow.GetValue<bool>("has_uploaded_content");
                        int order = cardRow.GetValue<int>("ordering");
                        if (hasUploadedContent)
                        {
                            // Remove from S3
                            ListObjectsRequest listRequest = new ListObjectsRequest();
                            listRequest.BucketName = bucketName;
                            listRequest.Prefix = "mlearnings/evaluations/" + topicId + "/" + cardId;

                            ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                            foreach (S3Object imageObject in listResponse.S3Objects)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = imageObject.Key;
                                s3Client.DeleteObject(deleteRequest);
                            }

                            DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                            deleteFolderRequest.BucketName = bucketName;
                            deleteFolderRequest.Key = "mlearnings/" + topicId + "/" + cardId;
                            s3Client.DeleteObject(deleteFolderRequest);

                            ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_uploaded_content", new List<string> { "card_id" }));
                            deleteBatch.Add(ps.Bind(cardId));
                        }

                        MLOptionUpdateResponse optionResponse = new MLOption().DeleteAllOptions(topicId, cardId, companyId, session, null);
                        if (!optionResponse.Success)
                        {
                            Log.Error(optionResponse.ErrorMessage);
                            response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                            response.ErrorMessage = optionResponse.ErrorMessage;
                            return response;
                        }

                        foreach (BoundStatement bs in optionResponse.BoundStatements)
                        {
                            deleteBatch.Add(bs);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card", new List<string> { "ml_topic_id", "id" }));
                        deleteBatch.Add(ps.Bind(topicId, cardId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(topicId, order, cardId));

                        session.Execute(deleteBatch);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLCardUpdateResponse DeleteCard(string topicId, string categoryId, string cardId, string adminUserId, string companyId, bool isReorderingNeeded = true, ISession session = null, List<string> newImageFileNames = null)
        {
            MLCardUpdateResponse response = new MLCardUpdateResponse();
            response.Success = false;

            try
            {
                int currentOrder = 0;

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    // Check topic category
                    Row topicCategoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, session);

                    if (topicCategoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.CategoryInvalid);
                        response.ErrorMessage = ErrorMessage.CategoryInvalid;
                        return response;
                    }

                    // Check topic row
                    Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, session);

                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.TopicInvalid);
                        response.ErrorMessage = ErrorMessage.TopicInvalid;
                        return response;
                    }

                    // Check card row
                    Row cardRow = vh.ValidateMLTopicCard(cardId, topicId, session);

                    if (cardRow == null)
                    {
                        Log.Error("Card already been deleted: " + cardId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardAlreadyBeenDeleted);
                        response.ErrorMessage = ErrorMessage.MLCardAlreadyBeenDeleted;
                        return response;
                    }

                    currentOrder = cardRow.GetValue<int>("ordering");
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Remove from S3
                String bucketName = "cocadre-" + companyId.ToLower();

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;

                    //cocadre-{CompanyId}/mlearnings/evaluations/{MLTopicId}/{MLCardId}/{FileName}.{FileFormaat}
                    listRequest.Prefix = "mlearnings/evaluations/" + topicId + "/" + cardId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        if (imageObject.Size <= 0)
                        {
                            continue;
                        }

                        if (newImageFileNames != null && newImageFileNames.Count > 0)
                        {
                            //mlearnings/MLT943cee78090948d086af5e30d051a250/MLCf60dd0247a12406ea824e7d59c5007cc/1_20160510101139774_original.jpeg
                            string[] stringToken = imageObject.Key.Split('/');
                            string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                            string[] fileNames = fileNamePath.Split('_');
                            string fileName = fileNames[0] + "_" + fileNames[1];

                            if (newImageFileNames.Any(newFileName => newFileName.Contains(fileName)))
                            {
                                continue;
                            }
                        }

                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }

                    if (isReorderingNeeded)
                    {
                        // Delete folder if this is not update
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = "mlearnings/evaluations/" + topicId + "/" + cardId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_uploaded_content", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_page_content", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                MLOptionUpdateResponse optionResponse = new MLOption().DeleteAllOptions(topicId, cardId, companyId, session, newImageFileNames);
                if (!optionResponse.Success)
                {
                    Log.Error(optionResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt32(optionResponse.ErrorCode);
                    response.ErrorMessage = optionResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in optionResponse.BoundStatements)
                {
                    deleteBatch.Add(bs);
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card", new List<string> { "ml_topic_id", "id" }));
                deleteBatch.Add(ps.Bind(topicId, cardId));

                // Reorder card
                if (isReorderingNeeded)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("ml_card_order", new List<string>(), new List<string> { "ml_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                    RowSet orderRowset = session.Execute(ps.Bind(topicId, currentOrder));

                    foreach (Row orderRow in orderRowset)
                    {
                        int order = orderRow.GetValue<int>("ordering");
                        string nextCardId = orderRow.GetValue<string>("card_id");

                        ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(topicId, order, nextCardId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("ml_card", new List<string> { "ml_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                        updateBatch.Add(ps.Bind(order - 1, topicId, nextCardId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                        updateBatch.Add(ps.Bind(topicId, order - 1, nextCardId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(topicId, currentOrder, cardId));
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public int SelectNumberOfCards(string topicId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psCard = session.Prepare(CQLGenerator.CountStatement("ml_card",
                    new List<string> { "ml_topic_id", "status" }));
                Row cardRow = session.Execute(psCard.Bind(topicId, (int)MLCardStatus.Active)).FirstOrDefault();

                if (cardRow != null)
                {
                    number = (int)cardRow.GetValue<long>("count");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        public MLCardSelectAllResponse SelectAllCards(string topicId, string categoryId, string adminUserId, string companyId, int queryType, string containsName = null, AnalyticLearning.MLTopicAttempt attempt = null, string answeredByUserId = null, ISession mainSession = null, ISession analyticSession = null)
        {
            MLCardSelectAllResponse response = new MLCardSelectAllResponse();
            response.Cards = new List<MLCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                    if (categoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                        return response;
                    }

                    Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);
                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                        response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                        return response;
                    }
                }

                if (analyticSession == null && (queryType == (int)MLCardQueryType.FullWithAnalytics || !string.IsNullOrEmpty(answeredByUserId)))
                {
                    analyticSession = cm.getAnalyticSession();
                }


                if (!string.IsNullOrEmpty(containsName))
                {
                    containsName = containsName.Trim();
                }

                // Select card details using ml_card_order
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_card_order", new List<string>(), new List<string> { "ml_topic_id" }));
                RowSet cardRowset = mainSession.Execute(ps.Bind(topicId));

                int page = 1;
                foreach (Row cardRow in cardRowset)
                {
                    string cardId = cardRow.GetValue<string>("card_id");
                    int ordering = cardRow.GetValue<int>("ordering");

                    MLCard selectedCard = SelectCard(adminUserId, companyId, cardId, topicId, queryType, attempt, answeredByUserId, mainSession, analyticSession).Card;

                    if (selectedCard != null)
                    {
                        selectedCard.Ordering = ordering;
                        selectedCard.Paging = page;

                        if (selectedCard.HasPageBreak)
                        {
                            page++;
                        }

                        if (!string.IsNullOrEmpty(containsName))
                        {
                            if (selectedCard.Type == (int)MLCardType.ReadAndAnswer)
                            {
                                foreach (MLPageContent pageContent in selectedCard.PagesContent)
                                {
                                    if (pageContent.Title.ToLower().Contains(containsName.ToLower()) || pageContent.Content.ToLower().Contains(containsName.ToLower()))
                                    {
                                        response.Cards.Add(selectedCard);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(selectedCard.Content) && selectedCard.Content.ToLower().Contains(containsName.ToLower()))
                                {
                                    response.Cards.Add(selectedCard);
                                }
                                else if (!string.IsNullOrEmpty(selectedCard.Description) && selectedCard.Description.ToLower().Contains(containsName.ToLower()))
                                {
                                    response.Cards.Add(selectedCard);
                                }
                                else
                                {
                                    foreach (MLOption option in selectedCard.Options)
                                    {
                                        if (option.Content.ToLower().Contains(containsName.ToLower()))
                                        {
                                            response.Cards.Add(selectedCard);
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            response.Cards.Add(selectedCard);
                        }

                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLCardSelectResponse SelectCard(string adminUserId, string companyId, string cardId, string topicId, int queryType, AnalyticLearning.MLTopicAttempt attempt = null, string answeredByUserId = null, ISession mainSession = null, ISession analyticSession = null)
        {
            MLCardSelectResponse response = new MLCardSelectResponse();
            response.Card = null;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_card", new List<string>(), new List<string> { "ml_topic_id", "id", "status" }));
                Row cardRow = mainSession.Execute(ps.Bind(topicId, cardId, (int)MLCardStatus.Active)).FirstOrDefault();

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLCardInvalid;
                    return response;
                }

                // Get Basic Details
                bool hasAttemptedByUser = false;
                int ordering = cardRow.GetValue<int>("ordering");
                string content = cardRow.GetValue<string>("content");
                string description = cardRow.GetValue<string>("description");
                bool hasUploadedContent = cardRow.GetValue<bool>("has_uploaded_content");
                int type = cardRow.GetValue<int>("type");
                bool hasPageBreak = cardRow.GetValue<bool>("has_page_break");
                int scoringCalculationType = cardRow.GetValue<int>("scoring_calculation_type");
                List<MLPageContent> pagesContent = new List<MLPageContent>();
                List<MLUploadedContent> uploadedContent = new List<MLUploadedContent>();
                List<MLOption> options = new List<MLOption>();
                int minOptionToSelect = cardRow["min_option_to_select"] == null ? 0 : cardRow.GetValue<int>("min_option_to_select");
                int maxOptionToSelect = cardRow["max_option_to_select"] == null ? 0 : cardRow.GetValue<int>("max_option_to_select");

                bool isOptionRandomized = false;
                int backgroundType = 1;
                string note = string.Empty;
                int page = 1;

                if (hasUploadedContent)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_card_uploaded_content", new List<string>(), new List<string> { "card_id" }));
                    RowSet uploadContentRowset = mainSession.Execute(ps.Bind(cardId));

                    foreach (Row uploadContentRow in uploadContentRowset)
                    {
                        string uploadId = uploadContentRow.GetValue<string>("upload_id");
                        string url = uploadContentRow.GetValue<string>("url");
                        int order = uploadContentRow.GetValue<int>("ordering");
                        int uploadType = uploadContentRow.GetValue<int>("upload_type");

                        MLUploadedContent upContent = new MLUploadedContent
                        {
                            UploadId = uploadId,
                            Url = url,
                            Ordering = order,
                            UploadType = uploadType
                        };

                        uploadedContent.Add(upContent);
                    }

                    uploadedContent = uploadedContent.OrderBy(o => o.Ordering).ToList();
                }

                MLOption optionManager = new MLOption();

                // Get the first page as the content for the basic card
                if (type == (int)MLCardType.ReadAndAnswer)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_card_page_content", new List<string>(), new List<string> { "card_id", "paging" }));
                    Row pageContentRow = mainSession.Execute(ps.Bind(cardId, 1)).FirstOrDefault();

                    if (pageContentRow != null)
                    {
                        string pageContent = pageContentRow.GetValue<string>("content");
                        string pageTitle = pageContentRow.GetValue<string>("title");
                        int paging = pageContentRow.GetValue<int>("paging");

                        MLPageContent pContent = new MLPageContent
                        {
                            Title = pageTitle,
                            Content = pageContent,
                            Paging = paging
                        };

                        pagesContent.Add(pContent);
                    }
                }

                // Get Full Details
                if (queryType >= (int)MLCardQueryType.Full)
                {
                    isOptionRandomized = cardRow.GetValue<bool>("is_option_randomized");
                    backgroundType = cardRow.GetValue<int>("background_image_type");
                    note = cardRow.GetValue<string>("note");
                    page = FindPageForCard(topicId, ordering, mainSession);

                    if (type == (int)MLCardType.SelectOne || type == (int)MLCardType.MultiChoice)
                    {
                        options = optionManager.SelectOptions(topicId, companyId, cardId, mainSession, analyticSession, attempt, answeredByUserId);
                        if (options.FirstOrDefault(o => o.IsIncompleteStateSelected) != null)
                        {
                            hasAttemptedByUser = true;
                        }
                    }
                    else if (type == (int)MLCardType.ReadAndAnswer)
                    {
                        pagesContent = new List<MLPageContent>();

                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("ml_card_page_content", new List<string>(), new List<string> { "card_id" }));
                        RowSet pagesContentRowset = mainSession.Execute(ps.Bind(cardId));

                        foreach (Row pageContentRow in pagesContentRowset)
                        {
                            string pageContent = pageContentRow.GetValue<string>("content");
                            string pageTitle = pageContentRow.GetValue<string>("title");
                            int paging = pageContentRow.GetValue<int>("paging");

                            MLPageContent pContent = new MLPageContent
                            {
                                Title = pageTitle,
                                Content = pageContent,
                                Paging = paging
                            };

                            pagesContent.Add(pContent);
                        }

                        pagesContent = pagesContent.OrderBy(p => p.Paging).ToList();
                    }
                }

                response.Card = new MLCard
                {
                    CardId = cardId,
                    Type = type,
                    HasUploadedContent = hasUploadedContent,
                    Content = content,
                    Description = description,
                    UploadedContent = uploadedContent,
                    Options = options,
                    IsOptionRandomized = isOptionRandomized,
                    HasPageBreak = hasPageBreak,
                    Ordering = ordering,
                    Paging = page,
                    BackgroundType = backgroundType,
                    Note = note,
                    PagesContent = pagesContent,
                    ScoringCalculationType = scoringCalculationType,
                    MinOptionToSelect = minOptionToSelect,
                    MaxOptionToSelect = maxOptionToSelect,
                    HasAttemptedByUser = hasAttemptedByUser
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLCardAnswerResponse AnswerCard(string answeredByUserId, string companyId, string topicId, string topicCategoryId, string cardId, List<string> optionIds)
        {
            MLCardAnswerResponse response = new MLCardAnswerResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateMLTopic(companyId, topicCategoryId, topicId, mainSession);

                if (topicRow == null || topicRow != null && topicRow.GetValue<int>("status") == (int)MLTopic.MLTopicStatusEnum.Hidden)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateMLTopicCard(cardId, topicId, mainSession);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLCardInvalid;
                    return response;
                }

                //Log.Debug("Selected cardId: " + cardId);
                //Log.Debug("Selected content: " + cardRow.GetValue<string>("content"));

                foreach (string optionId in optionIds)
                {
                    Row optionRow = vh.ValidateMLTopicCardOption(cardId, optionId, mainSession);

                    if (optionRow == null)
                    {
                        Log.Error("Invalid optionId: " + optionId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.MLOptionInvalid);
                        response.ErrorMessage = ErrorMessage.MLOptionInvalid;
                        return response;
                    }

                    Log.Debug("Selected optionId: " + optionId);
                }

                AnalyticLearning analyticManager = new AnalyticLearning();
                int attempt = analyticManager.SelectCurrentAttemptByUser(analyticSession, answeredByUserId, topicId);
                if (attempt == 0)
                {
                    Log.Error(ErrorMessage.MLProgressNotCaptured);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLProgressNotCaptured);
                    response.ErrorMessage = ErrorMessage.MLProgressNotCaptured;
                    return response;
                }

                //Log.Debug("Attempt: " + attempt);

                //analyticManager.AnswerRSCard(answeredByUserId, topicId, cardId, attempt, optionIds, DateTime.UtcNow, analyticSession);

                //Log.Debug("Already save to db");

                Thread updateThread = new Thread(() => analyticManager.AnswerRSCard(answeredByUserId, topicId, cardId, attempt, optionIds, DateTime.UtcNow, analyticSession));
                updateThread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                Log.Error("CardId: " + cardId);
                Log.Error("OptionIds: " + optionIds.ToString());
                Log.Error("TopicId: " + topicId);
                Log.Error("CategoryId: " + topicCategoryId);

                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLCardReorderResponse ReorderCard(string cardId, string topicId, string categoryId, string adminUserId, string companyId, int insertAtOrder)
        {
            MLCardReorderResponse response = new MLCardReorderResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                int topicStatus = topicRow.GetValue<int>("status");
                DateTime startDate = topicRow.GetValue<DateTime>("start_date");
                DateTime? endDate = topicRow.GetValue<DateTime?>("end_date");
                DateTime currentTime = DateTime.UtcNow;
                int progress = new MLTopic().SelectProgress(currentTime, startDate, endDate);

                if (topicStatus != (int)MLTopic.MLTopicStatusEnum.Unlisted && progress != (int)MLTopic.ProgressStatusEnum.Upcoming)
                {
                    Log.Error("Topic is not unlisted and not upcoming: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardReordering);
                    response.ErrorMessage = ErrorMessage.MLCardReordering;
                    return response;
                }

                Row cardRow = vh.ValidateMLTopicCard(cardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLCardInvalid;
                    return response;
                }

                bool currentHasPageBreak = cardRow.GetValue<bool>("has_page_break");
                int currentOrder = cardRow.GetValue<int>("ordering");

                if (currentOrder != insertAtOrder)
                {
                    BatchStatement updateBatch = new BatchStatement();
                    BatchStatement deleteBatch = new BatchStatement();

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("ml_card", new List<string> { "ml_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                    updateBatch.Add(ps.Bind(insertAtOrder, topicId, cardId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(topicId, currentOrder, cardId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                    updateBatch.Add(ps.Bind(topicId, insertAtOrder, cardId));

                    // Ignore the top range
                    if (currentOrder > insertAtOrder)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatementWithRangeComparison("ml_card_order",
                            new List<string>(), new List<string> { "ml_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThanOrEquals, "ordering", CQLGenerator.Comparison.LessThan, 0));
                        RowSet orderRowset = session.Execute(ps.Bind(topicId, insertAtOrder, currentOrder));

                        foreach (Row orderRow in orderRowset)
                        {
                            string updateCardId = orderRow.GetValue<string>("card_id");
                            int updateCardOrder = orderRow.GetValue<int>("ordering");

                            ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                            deleteBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));

                            updateCardOrder++;

                            ps = session.Prepare(CQLGenerator.UpdateStatement("ml_card", new List<string> { "ml_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                            updateBatch.Add(ps.Bind(updateCardOrder, topicId, updateCardId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                            updateBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));
                        }
                    }
                    // Ignore the bottom range
                    else
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatementWithRangeComparison("ml_card_order",
                            new List<string>(), new List<string> { "ml_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, "ordering", CQLGenerator.Comparison.LessThanOrEquals, 0));
                        RowSet orderRowset = session.Execute(ps.Bind(topicId, currentOrder, insertAtOrder));

                        foreach (Row orderRow in orderRowset)
                        {
                            string updateCardId = orderRow.GetValue<string>("card_id");
                            int updateCardOrder = orderRow.GetValue<int>("ordering");

                            ps = session.Prepare(CQLGenerator.DeleteStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                            deleteBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));

                            updateCardOrder--;

                            ps = session.Prepare(CQLGenerator.UpdateStatement("ml_card", new List<string> { "ml_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                            updateBatch.Add(ps.Bind(updateCardOrder, topicId, updateCardId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("ml_card_order", new List<string> { "ml_topic_id", "ordering", "card_id" }));
                            updateBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));
                        }
                    }

                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public int FindPageForCard(string topicId, int cardOrder, ISession session)
        {
            int page = 1;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("ml_card_order", new List<string>(), new List<string> { "ml_topic_id" }, "ordering", CQLGenerator.Comparison.LessThan, 0));
                RowSet orderRowset = session.Execute(ps.Bind(topicId, cardOrder));

                foreach (Row orderRow in orderRowset)
                {
                    string cardId = orderRow.GetValue<string>("card_id");
                    MLCard card = SelectCard(null, null, cardId, topicId, (int)MLCardQueryType.Basic, null, null, session, null).Card;

                    if (card != null && card.HasPageBreak)
                    {
                        page++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return page;
        }

        public MLSelectCardQuestionResultResponse SelectCardQuestionResult(string adminUserId, string companyId, string topicId, string categoryId, string cardId, string searchId)
        {
            MLSelectCardQuestionResultResponse response = new MLSelectCardQuestionResultResponse();
            response.PreviewCard = null;
            response.SearchTerms = new List<AnalyticLearning.MLFilteredDepartmentSearchTerm>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                MLTopicCategory category = new MLTopicCategory
                {
                    CategoryId = categoryRow.GetValue<string>("id"),
                    Title = categoryRow.GetValue<string>("title")
                };

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateMLTopicCard(cardId, topicId, mainSession);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLCardInvalid);
                    response.ErrorMessage = ErrorMessage.MLCardInvalid;
                    return response;
                }

                MLTopic topicManager = new MLTopic();
                // Get targeted audience
                List<User> allTargetedAudience = topicManager.SelectTargetedAudience(topicId, companyId, null, mainSession);

                List<User> searchedAudience = !string.IsNullOrEmpty(searchId) ? topicManager.SelectTargetedAudience(topicId, companyId, searchId, mainSession) : allTargetedAudience;

                // Get searchIds
                List<AnalyticLearning.MLFilteredDepartmentSearchTerm> searchTerms = new List<AnalyticLearning.MLFilteredDepartmentSearchTerm>();

                foreach (User user in allTargetedAudience)
                {
                    foreach (Department department in user.Departments)
                    {
                        if (!searchTerms.Any(s => s.Id.Equals(department.Id)))
                        {
                            searchTerms.Add(
                                new AnalyticLearning.MLFilteredDepartmentSearchTerm
                                {
                                    Id = department.Id,
                                    Content = department.Title,
                                    IsSelected = department.Id.Equals(searchId) ? true : false
                                }
                            );
                        }
                    }
                }

                searchTerms = searchTerms.OrderByDescending(s => s.IsSelected).ThenBy(s => s.Content).ToList();

                AnalyticLearning.MLFilteredDepartmentSearchTerm allDepartmentSearchTerm = new AnalyticLearning.MLFilteredDepartmentSearchTerm
                {
                    Id = string.Empty,
                    Content = "All Departments",
                    IsSelected = string.IsNullOrEmpty(searchId) ? true : false
                };

                if (string.IsNullOrEmpty(searchId))
                {
                    searchTerms.Insert(0, allDepartmentSearchTerm);
                }
                else
                {
                    searchTerms.Insert(1, allDepartmentSearchTerm);
                }

                response.SearchTerms = searchTerms;

                response.PreviewCard = new MLCard().SelectCard(adminUserId, companyId, cardId, topicId, (int)MLCard.MLCardQueryType.Full, null, null, mainSession, analyticSession).Card;

                new AnalyticLearning().SelectPreviewCardResult(topicId, response.PreviewCard, searchedAudience, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLSelectSortedCardAttemptHistoryResponse SelectSortedCardHistoryResult(string adminUserId, string companyId, string answeredByUserId, string topicId, string categoryId, string questionToggleType, string containsName = null)
        {
            MLSelectSortedCardAttemptHistoryResponse response = new MLSelectSortedCardAttemptHistoryResponse();
            response.Cards = new List<MLCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateMLTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateMLTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLTopicInvalid);
                    response.ErrorMessage = ErrorMessage.MLTopicInvalid;
                    return response;
                }

                Row userRow = vh.ValidateUser(answeredByUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid userId: " + answeredByUserId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                double timeoffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);
                AnalyticLearning.MLTopicAttempt topicAttempt = new AnalyticLearning().SelectMLAttempt(topicId, answeredByUserId, timeoffset, analyticSession);
                if (topicAttempt.Attempt.Equals("NA"))
                {
                    response.Success = true;
                    return response;
                }

                response.Cards = SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)MLCardQueryType.FullWithAnalytics, containsName, topicAttempt, answeredByUserId, mainSession, analyticSession).Cards;

                response.CardToggleType = new List<AnalyticLearning.MLCardToggleType>
                {
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "All questions",
                        IsSelected = questionToggleType.Equals(((int)AnalyticLearning.MLCardToggleTypeEnum.AllQuestions).ToString()) ? true : false,
                        Id = ((int)AnalyticLearning.MLCardToggleTypeEnum.AllQuestions).ToString()
                    },
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "Hardest questions",
                        IsSelected = questionToggleType.Equals(((int)AnalyticLearning.MLCardToggleTypeEnum.HardestQuestions).ToString()) ? true : false,
                        Id = ((int)AnalyticLearning.MLCardToggleTypeEnum.HardestQuestions).ToString()
                    },
                    new AnalyticLearning.MLCardToggleType
                    {
                        Content = "Easiest questions",
                        IsSelected = questionToggleType.Equals(((int)AnalyticLearning.MLCardToggleTypeEnum.EasiestQuestions).ToString()) ? true : false,
                        Id = ((int)AnalyticLearning.MLCardToggleTypeEnum.EasiestQuestions).ToString()
                    }
                };

                new AnalyticLearning().SelectSortedUserCardHistory(answeredByUserId, response.Cards, Convert.ToInt32(questionToggleType), analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }

    [Serializable]
    [DataContract]
    public class MLUploadedContent
    {
        [DataMember]
        public string UploadId { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public int UploadType { get; set; }

        [DataMember]
        public int Ordering { get; set; }
    }

    [Serializable]
    [DataContract]
    public class MLPageContent
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public int Paging { get; set; }
    }
}
