﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class AppraisalCardRange
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember]
        public int RangeNumber { get; set; }

        [DataMember]
        public string Label { get; set; }

        [DataMember]
        public string BackgroundColor { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NumberOfSelection { get; set; }

        public AppraisalRangeCreateStatementResponse CreateCustomRange(string cardId, List<AppraisalCardRange> ranges, ISession mainSession)
        {
            AppraisalRangeCreateStatementResponse response = new AppraisalRangeCreateStatementResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int number = 1;
                foreach (AppraisalCardRange range in ranges)
                {
                    ps = mainSession.Prepare(CQLGenerator.InsertStatement("user_appraisal_360_card_range",
                                            new List<string> { "card_id", "label", "number" }));
                    response.UpdateStatements.Add(ps.Bind(cardId, range.Label, number));
                    number++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalRangeDeleteStatementResponse DeleteCustomRange(string cardId, ISession mainSession)
        {
            AppraisalRangeDeleteStatementResponse response = new AppraisalRangeDeleteStatementResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.DeleteStatement("user_appraisal_360_card_range", new List<string> { "card_id" }));
                response.DeleteStatements.Add(ps.Bind(cardId));
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalRangeSelectResponse SelectLikert7(Row companySettingRow, ISession mainSession)
        {
            AppraisalRangeSelectResponse response = new AppraisalRangeSelectResponse();
            response.Ranges = new List<AppraisalCardRange>();
            response.Success = false;
            try
            {
                int rangeNumber = 7;
                for (int index = 1; index <= rangeNumber; index++)
                {
                    response.Ranges.Add(new AppraisalCardRange
                    {
                        RangeNumber = index,
                        Label = companySettingRow.GetValue<string>(string.Format("likert_7_range_{0}_label", index)),
                        BackgroundColor = companySettingRow.GetValue<string>(string.Format("likert_7_range_{0}_bgcolor", index)),
                    });
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AppraisalRangeSelectResponse SelectCustomRange(string cardId, ISession mainSession)
        {
            AppraisalRangeSelectResponse response = new AppraisalRangeSelectResponse();
            response.Ranges = new List<AppraisalCardRange>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_appraisal_360_card_range", new List<string>(), new List<string> { "card_id" }));
                RowSet rangeRowSet = mainSession.Execute(ps.Bind(cardId));
                foreach(Row rangeRow in rangeRowSet)
                {
                    response.Ranges.Add(new AppraisalCardRange
                    {
                        RangeNumber = rangeRow.GetValue<int>("number"),
                        Label = rangeRow.GetValue<string>("label")
                    });
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
