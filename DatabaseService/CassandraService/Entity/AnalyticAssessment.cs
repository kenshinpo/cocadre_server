﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CassandraService.Entity
{
    public class AnalyticAssessment
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        private class AnalyticAllocation
        {
            public string TabulationId { get; set; }
            public int CurrentScore { get; set; }
        }

        public AssessmentSelectAttemptByUserResponse SelectAttemptByUser(string assessmentId, string userId, int numberOfRetakesAvailable, ISession analyticSession)
        {
            AssessmentSelectAttemptByUserResponse response = new AssessmentSelectAttemptByUserResponse();
            response.CurrentAttempt = 1;
            response.NextAttempt = 1;
            response.NumberOfRetakesLeft = numberOfRetakesAvailable;
            response.IsCurrentAttemptCompleted = false;
            response.LastCompletedTimestamp = null;
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_progress", new List<string>(), new List<string> { "assessment_id", "user_id" }));
                List<Row> progressRowset = analyticSession.Execute(ps.Bind(assessmentId, userId)).ToList();

                if (progressRowset.Count == 0)
                {
                    return response;
                }

                progressRowset = progressRowset.OrderByDescending(r => r.GetValue<int>("attempt")).ToList();
                Row lastProgressRow = progressRowset[0];

                response.CurrentAttempt = lastProgressRow.GetValue<int>("attempt");
                if (lastProgressRow.GetValue<DateTime?>("completed_on_timestamp") == null)
                {
                    response.NextAttempt = response.CurrentAttempt;
                    if(response.CurrentAttempt > 1)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_progress", new List<string>(), new List<string> { "assessment_id", "user_id", "attempt" }));
                        Row lastCompletedRow = analyticSession.Execute(ps.Bind(assessmentId, userId, response.CurrentAttempt - 1)).FirstOrDefault();
                        if (lastCompletedRow != null)
                        {
                            response.LastCompletedTimestamp = lastCompletedRow.GetValue<DateTime?>("completed_on_timestamp");
                        }
                    }
                }
                else
                {
                    response.LastCompletedTimestamp = lastProgressRow.GetValue<DateTime?>("completed_on_timestamp");
                    response.IsCurrentAttemptCompleted = true;
                    response.NextAttempt = response.CurrentAttempt + 1;
                }

                response.NumberOfRetakesLeft = numberOfRetakesAvailable + 1 - response.CurrentAttempt;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return response;
        }

        public void UpdateProgress(string assessmentId, string userId, int attempt, DateTime currentTime, ISession analyticSession, bool isCompleted = false)
        {
            try
            {
                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_progress", new List<string>(), new List<string> { "assessment_id", "user_id", "attempt" }));
                Row progress = analyticSession.Execute(ps.Bind(assessmentId, userId, attempt)).FirstOrDefault();

                if (progress != null)
                {
                    if (!isCompleted)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("assessment_progress", new List<string> { "assessment_id", "user_id", "attempt" }, new List<string> { "last_updated_timestamp" }, new List<string>()));
                        analyticSession.Execute(ps.Bind(currentTime, assessmentId, userId, attempt));
                    }
                    else
                    {
                        DateTime? completedTimestamp = progress.GetValue<DateTime?>("completed_on_timestamp");

                        if(completedTimestamp == null)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("assessment_progress", new List<string> { "assessment_id", "user_id", "attempt" }, new List<string> { "last_updated_timestamp", "completed_on_timestamp" }, new List<string>()));
                            analyticSession.Execute(ps.Bind(currentTime, currentTime, assessmentId, userId, attempt));
                        }
                    }
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("assessment_progress", new List<string> { "assessment_id", "user_id", "attempt" }, new List<string> { "start_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                    analyticSession.Execute(ps.Bind(currentTime, currentTime, assessmentId, userId, attempt));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void UpdateOptionAnswered(int attempt, string assessmentId, string answeredByUserId, string cardId, int cardType, List<AssessmentCardOption> options, List<OptionTabulationAllocation> allocations, ISession analyticSession)
        {
            try
            {
                DateTime currentTime = DateTime.UtcNow;

                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                List<AnalyticAllocation> scoreAllocations = new List<AnalyticAllocation>();
                foreach (AssessmentCardOption option in options)
                {
                    string optionId = option.OptionId;
                    int pointAllocated = option.PointAllocated;

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("assessment_option_answered_by_user",
                        new List<string> { "answered_by_user_id", "attempt", "card_id", "option_id", "point_allocated", "answered_on_timestamp" }));
                    batch.Add(ps.Bind(answeredByUserId, attempt, cardId, optionId, pointAllocated, currentTime));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("assessment_option_answered_by_card",
                        new List<string> { "answered_by_user_id", "attempt", "card_id", "option_id", "point_allocated", "answered_on_timestamp" }));
                    batch.Add(ps.Bind(answeredByUserId, attempt, cardId, optionId, pointAllocated, currentTime));

                    List<OptionTabulationAllocation> optionAllocations = allocations.Where(a => a.OptionId.Equals(optionId)).ToList();
                    foreach (OptionTabulationAllocation optionAllocation in optionAllocations)
                    {
                        string tabulationId = optionAllocation.TabulationId;
                        int valueOperator = optionAllocation.ValueOperator;
                        int value = optionAllocation.Value;
                        int tabulationOperator = optionAllocation.TabulationOperator;

                        int currentScore = 0;

                        if (scoreAllocations.FirstOrDefault(s => s.TabulationId.Equals(tabulationId)) == null)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_score_by_user",
                                new List<string>(), new List<string> { "scored_by_user_id", "attempt", "tabulation_id" }));

                            Row tabulationScoreRow = analyticSession.Execute(ps.Bind(answeredByUserId, attempt, tabulationId)).FirstOrDefault();
                            if (tabulationScoreRow != null)
                            {
                                currentScore = tabulationScoreRow.GetValue<int>("score");
                            }

                            scoreAllocations.Add(new AnalyticAllocation
                            {
                                TabulationId = tabulationId,
                                CurrentScore = currentScore
                            });
                        }

                        AnalyticAllocation scoreAllocation = scoreAllocations.FirstOrDefault(s => s.TabulationId.Equals(tabulationId));
                        int addedScore = 0;
                        switch (valueOperator)
                        {
                            case (int)AssessmentCard.AssessmentCardOperator.Multiplication:
                                if (cardType == (int)AssessmentCard.AssessmentCardType.SelectOne || cardType == (int)AssessmentCard.AssessmentCardType.MultiChoice)
                                {
                                    pointAllocated = 1;
                                }
                                addedScore = pointAllocated * value;
                                break;
                        }

                        switch (tabulationOperator)
                        {
                            case (int)AssessmentCard.AssessmentCardOperator.Addition:
                                scoreAllocation.CurrentScore += addedScore;
                                break;
                        }
                    }
                }

                foreach (AnalyticAllocation scoreAllocation in scoreAllocations)
                {
                    string tabulationId = scoreAllocation.TabulationId;
                    int updatedScore = scoreAllocation.CurrentScore <= 0 ? 0 : scoreAllocation.CurrentScore;

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("assessment_score_by_user", new List<string> { "scored_by_user_id", "attempt", "tabulation_id" }, new List<string> { "score" }, new List<string>()));
                    batch.Add(ps.Bind(updatedScore, answeredByUserId, attempt, tabulationId));
                }

                analyticSession.Execute(batch);

                UpdateProgress(assessmentId, answeredByUserId, attempt, currentTime, analyticSession);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void SelectResultByUser(string assessmentId, string userId, int attempt, List<AssessmentAnswer> answers, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;
                foreach (AssessmentAnswer answer in answers)
                {
                    int condition = answer.Condition;
                    List<AssessmentTabulation> tabulations = answer.Tabulations;

                    foreach (AssessmentTabulation tabulation in tabulations)
                    {
                        string tabulationId = tabulation.TabulationId;
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_score_by_user", new List<string>(), new List<string> { "scored_by_user_id", "attempt", "tabulation_id" }));

                        int score = 0;
                        Row scoreRow = analyticSession.Execute(ps.Bind(userId, attempt, tabulationId)).FirstOrDefault();
                        if (scoreRow != null)
                        {
                            score = scoreRow.GetValue<int>("score");
                        }

                        tabulation.Score = score;
                    }

                    List<AssessmentTabulation> resultTabulations = new List<AssessmentTabulation>();

                    switch (condition)
                    {
                        case (int)Assessment.AssessmentAnswerConditionTypeEnum.TableWithLowestPoint:
                            tabulations = tabulations.OrderBy(t => t.Score).ToList();
                            foreach (AssessmentTabulation tabulation in tabulations)
                            {
                                if (tabulation.Score == tabulations[0].Score)
                                {
                                    resultTabulations.Add(tabulation);
                                }
                            }
                            break;
                        case (int)Assessment.AssessmentAnswerConditionTypeEnum.TableWithHighestPoint:
                            tabulations = tabulations.OrderByDescending(t => t.Score).ToList();
                            foreach (AssessmentTabulation tabulation in tabulations)
                            {
                                if (tabulation.Score == tabulations[0].Score)
                                {
                                    resultTabulations.Add(tabulation);
                                }
                            }
                            break;
                    }

                    answer.Tabulations = resultTabulations;
                    answer.Tabulations = answer.Tabulations.OrderBy(t => t.Ordering).ToList();
                }

                answers = answers.OrderBy(a => a.Ordering).ToList();

                UpdateProgress(assessmentId, userId, attempt, DateTime.UtcNow, analyticSession, true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public bool IsAssessmentCompleted(Assessment assessment, int attempt, string answeredByUserId, ISession analyticSession)
        {
            bool isCompleted = true;

            try
            {
                PreparedStatement ps = null;
                Row answerRow = null;
                RowSet answerRowset = null;
                foreach (AssessmentCard card in assessment.Cards)
                {
                    if(card.Type == (int)AssessmentCard.AssessmentCardType.SelectOne)
                    {
                        if(!card.ToBeSkipped)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "attempt", "card_id" }));
                            answerRow = analyticSession.Execute(ps.Bind(answeredByUserId, attempt, card.CardId)).FirstOrDefault();
                            if(answerRow == null)
                            {
                                isCompleted = false;
                                break;
                            }
                        }
                    }
                    else if (card.Type == (int)AssessmentCard.AssessmentCardType.Accumulation)
                    {
                        int totalPoints = card.TotalPointsAllocated;

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "attempt", "card_id" }));
                        answerRowset = analyticSession.Execute(ps.Bind(answeredByUserId, attempt, card.CardId));

                        foreach(Row row in answerRowset)
                        {
                            totalPoints -= row.GetValue<int>("point_allocated");
                        }

                        if(totalPoints != 0)
                        {
                            isCompleted = false;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return isCompleted;
        }

        public AssessmentSelectCardOptionAnsweredByUserResponse SelectCardOptionAnswer(string answeredByUserId, string cardId, string optionId, int attempt, ISession analyticSession)
        {
            AssessmentSelectCardOptionAnsweredByUserResponse response = new AssessmentSelectCardOptionAnsweredByUserResponse();
            response.IsSelected = false;
            response.PointAllocated = 0;
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("assessment_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "attempt", "card_id", "option_id" }));
                Row resultRow = analyticSession.Execute(ps.Bind(answeredByUserId, attempt, cardId, optionId)).FirstOrDefault();

                if(resultRow != null)
                {
                    response.IsSelected = true;
                    response.PointAllocated = resultRow.GetValue<int>("point_allocated");
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
