﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class AssessmentCardOption
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");
   
        public enum AssessmentCardOptionQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember]
        public string OptionId { get; set; }

        [DataMember]
        public bool HasImage { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public List<AssessmentImage> Images { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public int PointAllocated { get; set; }


        [DataMember]
        public List<OptionTabulationAllocation> Allocations { get; set; }

        public AssessmentCreateOptionResponse CreateOptions(string adminUserId, string assessmentId, string cardId, List<AssessmentCardOption> options, DateTime currentTime, ISession session)
        {
            AssessmentCreateOptionResponse response = new AssessmentCreateOptionResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                PreparedStatement ps = null;

                // Insert
                if (options != null || options.Count > 0)
                {
                    foreach (AssessmentCardOption option in options)
                    {
                        bool hasImage = ((option.Images == null) || (option.Images.Count <= 0)) ? false : true;
                        option.HasImage = hasImage;

                        // Check if optionId is null
                        if (string.IsNullOrEmpty(option.OptionId))
                        {
                            option.OptionId = UUIDGenerator.GenerateUniqueIDForAssessmentCardOption();
                        }

                        // Check if option is empty
                        if (string.IsNullOrEmpty(option.Content))
                        {
                            Log.Error("Invalid option content");
                            response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentCardOptionMissingContent);
                            response.ErrorMessage = ErrorMessage.AssessmentCardOptionMissingContent;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.InsertStatement("assessment_card_option",
                            new List<string> { "card_id", "id", "has_image", "content", "ordering", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                        response.Statements.Add(ps.Bind(cardId, option.OptionId, option.HasImage, option.Content, option.Ordering, adminUserId, currentTime, adminUserId, currentTime));

                        if (hasImage)
                        {
                            AssessmentCreateStatementResponse statementResponse = CreateImage(option.OptionId, option.Images, session);
                            if (!statementResponse.Success)
                            {
                                response.ErrorCode = statementResponse.ErrorCode;
                                response.ErrorMessage = statementResponse.ErrorMessage;
                                return response;
                            }

                            foreach (BoundStatement bs in statementResponse.Statements)
                            {
                                response.Statements.Add(bs);
                            }
                        }

                        int allocationOrdering = 1;
                        foreach(OptionTabulationAllocation allocation in option.Allocations)
                        {
                            string tabulationId = allocation.TabulationId;
                            if (string.IsNullOrWhiteSpace(tabulationId))
                            {
                                tabulationId = allocation.Tabulation.TabulationId;
                            }
                            Row tabulationRow = vh.ValidateAssessmentTabulation(assessmentId, tabulationId, session);
                            if(tabulationRow == null)
                            {
                                response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentInvalidTabulation);
                                response.ErrorMessage = ErrorMessage.AssessmentInvalidTabulation;
                                return response;
                            }

                            allocation.ValueOperator = (int)AssessmentCard.AssessmentCardOperator.Multiplication;
                            allocation.TabulationOperator = (int)AssessmentCard.AssessmentCardOperator.Addition;

                            ps = session.Prepare(CQLGenerator.InsertStatement("assessment_tabulation_allocation",
                                new List<string> { "card_id", "option_id", "tabulation_id", "ordering", "value_operator", "tabulation_operator", "value"}));
                            response.Statements.Add(ps.Bind(cardId, option.OptionId, tabulationId, allocationOrdering, allocation.ValueOperator, allocation.TabulationOperator, allocation.Value));

                            ps = session.Prepare(CQLGenerator.InsertStatement("assessment_tabulation_allocation_by_tabulation",
                                new List<string> { "card_id", "option_id", "tabulation_id", "ordering", "value_operator", "tabulation_operator", "value" }));
                            response.Statements.Add(ps.Bind(cardId, option.OptionId, tabulationId, allocationOrdering, allocation.ValueOperator, allocation.TabulationOperator, allocation.Value));

                            allocationOrdering++;
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentSelectAllOptionResponse SelectAllOptions(string assessmentId, string cardId, ISession session, bool isTabulationNeeded = false)
        {
            AssessmentSelectAllOptionResponse response = new AssessmentSelectAllOptionResponse();
            response.Options = new List<AssessmentCardOption>();
            response.Success = false;
            try
            {
                Assessment assessmentManager = new Assessment();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_card_option", new List<string>(), new List<string> { "card_id" }));
                RowSet optionRowset = session.Execute(ps.Bind(cardId));
                foreach(Row optionRow in optionRowset)
                {
                    string optionId = optionRow.GetValue<string>("id");
                    bool hasImage = optionRow.GetValue<bool>("has_image");

                    List<AssessmentImage> images = new List<AssessmentImage>();
                    if(hasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("assessment_option_image", new List<string>(), new List<string> { "option_id" }));
                        RowSet optionImageRowset = session.Execute(ps.Bind(optionId));
                        foreach (Row optionImageRow in optionImageRowset)
                        {
                            AssessmentImage image = new AssessmentImage
                            {
                                ImageId = optionImageRow.GetValue<string>("image_id"),
                                ImageUrl = optionImageRow.GetValue<string>("image_url"),
                                Ordering = optionImageRow.GetValue<int>("ordering"),
                            };

                            images.Add(image);
                        }

                        images = images.OrderBy(c => c.Ordering).ToList();
                    }

                    List<OptionTabulationAllocation> allocations = new List<OptionTabulationAllocation>();
                    if(isTabulationNeeded)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("assessment_tabulation_allocation", new List<string>(), new List<string> { "card_id", "option_id" }));
                        RowSet allocationRowset = session.Execute(ps.Bind(cardId, optionId));
                        foreach (Row allocationRow in allocationRowset)
                        {
                            string tabulationId = allocationRow.GetValue<string>("tabulation_id");
                            OptionTabulationAllocation allocation = new OptionTabulationAllocation
                            {
                                Tabulation = assessmentManager.SelectTabulation(assessmentId, tabulationId, session),
                                ValueOperator = allocationRow.GetValue<int>("value_operator"),
                                TabulationOperator = allocationRow.GetValue<int>("tabulation_operator"),
                                Ordering = allocationRow.GetValue<int>("ordering"),
                                Value = allocationRow.GetValue<int>("value"),
                                CardId = cardId,
                                OptionId = optionId,
                                TabulationId = tabulationId
                            };

                            allocations.Add(allocation);
                        }
                    }

                    string content = optionRow.GetValue<string>("content");
                    int ordering = optionRow.GetValue<int>("ordering");

                    AssessmentCardOption option = new AssessmentCardOption
                    {
                        OptionId = optionId,
                        HasImage = hasImage,
                        Images = images,
                        Content = content,
                        Ordering = ordering,
                        Allocations = allocations
                    };
                    response.Options.Add(option);
                }

                response.Options = response.Options.OrderBy(o => o.Ordering).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public AssessmentUpdateResponse DeleteAllOptions(string assessmentId, string cardId, ISession session)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                List<AssessmentCardOption> options = SelectAllOptions(assessmentId, cardId, session, true).Options;
                foreach(AssessmentCardOption option in options)
                {
                    foreach(OptionTabulationAllocation tabulation in option.Allocations)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_tabulation_allocation_by_tabulation", new List<string> { "tabulation_id" }));
                        deleteBatch.Add(ps.Bind(tabulation.TabulationId));
                    }

                    List<string> toBeDeletedImageUrls = new List<string>();
                    if(option.HasImage)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_option_image", new List<string> { "option_id" }));
                        deleteBatch.Add(ps.Bind(option.OptionId));

                        foreach(AssessmentImage image in option.Images)
                        {
                            toBeDeletedImageUrls.Add(image.ImageUrl);
                        }

                        DeleteFromS3(assessmentId, cardId, toBeDeletedImageUrls);
                    }

                }
                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_tabulation_allocation", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_card_option", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                session.Execute(deleteBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private AssessmentCreateStatementResponse CreateImage(string optionId, List<AssessmentImage> optionImages, ISession session)
        {
            AssessmentCreateStatementResponse response = new AssessmentCreateStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int imageOrdering = 1;
                foreach (AssessmentImage image in optionImages)
                {
                    string imageId = string.Format("{0}_image_{1}", optionId, imageOrdering);
                    string imageUrl = image.ImageUrl;

                    if (string.IsNullOrEmpty(imageUrl))
                    {
                        Log.Error("Invalid option image url");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentMissingImageUrl);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingImageUrl;
                        return response;
                    }

                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_option_image",
                        new List<string> { "option_id", "image_id", "image_url", "ordering" }));
                    response.Statements.Add(ps.Bind(optionId, imageId, imageUrl, imageOrdering));

                    imageOrdering++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        private void DeleteFromS3(string assessmentId, string cardId, List<string> deletedImageUrls = null)
        {
            try
            {
                String bucketName = "cocadre-marketplace";

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;

                    listRequest.Prefix = assessmentId + "/" + cardId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        if (imageObject.Size <= 0)
                        {
                            continue;
                        }

                        if (deletedImageUrls.Count > 0)
                        {
                            //pulses/PF7168c6ceedc34e709790cd63398c9668/U811e9a9236c140ad98a211a428b6dc84_20160527072241738_original.jpg 
                            string[] stringToken = imageObject.Key.Split('/');
                            string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                            string[] fileNames = fileNamePath.Split('_');
                            string fileName = fileNames[0] + "_" + fileNames[1];

                            if (deletedImageUrls.FirstOrDefault(url => url.Contains(fileName)) == null)
                            {
                                continue;
                            }
                        }

                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }

    [Serializable]
    [DataContract]
    public class OptionTabulationAllocation
    {
        [DataMember]
        public AssessmentTabulation Tabulation { get; set; }

        [DataMember]
        public string TabulationId { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int ValueOperator { get; set; }

        [DataMember]
        public int TabulationOperator { get; set; }

        [DataMember]
        public int Value { get; set; }

        public string OptionId { get; set; }
        public string CardId { get; set; }
    }
}
