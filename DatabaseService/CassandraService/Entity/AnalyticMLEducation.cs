﻿using Cassandra;
using CassandraService.ServiceResponses;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.GlobalResources;
using CassandraService.CassandraUtilities;
using System.Runtime.Serialization;
using CassandraService.Validation;
using CassandraService.Utilities;

namespace CassandraService.Entity
{
    public class AnalyticMLEducation
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum UserProgressEnum
        {
            Absent = 1,
            Incomplete = 2,
            Completed = 3
        }

        [DataContract]
        [Serializable]
        public class Attendance
        {
            [DataMember]
            public int Progress { get; set; }

            [DataMember]
            public string Name { get; set; }

            [DataMember]
            public int Count { get; set; }

            [DataMember]
            public int Percentage { get; set; }
        }

        public void StartActivity(string requesterUserId, string educationId, ISession analyticSession)
        {
            try
            {
                #region Step 1. Check ml_education_user_incomplete table.
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_incomplete", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                Row rowIncomplete = analyticSession.Execute(ps.Bind(educationId, requesterUserId)).FirstOrDefault();
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_completed", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                Row rowCompleted = analyticSession.Execute(ps.Bind(educationId, requesterUserId)).FirstOrDefault();
                #endregion

                #region Step 2. If user is not in ml_education_user_incomplete and ml_education_user_completed tables, insert a data into ml_education_user_incomplete table.
                if (rowIncomplete == null && rowCompleted == null)
                {
                    DateTime currentTime = DateTime.UtcNow;
                    BatchStatement updateBatch = new BatchStatement();

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_education_user_incomplete", new List<string> { "ml_education_id", "user_id", "incomplete_on_timestamp" }));
                    updateBatch.Add(ps.Bind(educationId, requesterUserId, currentTime));
                    analyticSession.Execute(updateBatch);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }


        public MLEducationAnswerResponse AnswerCard(string answeredByUserId, string educationId, string cardId, List<string> optionIds, ISession analyticSession)
        {
            MLEducationAnswerResponse response = new MLEducationAnswerResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check ml_education_user_incomplete and ml_education_user_completed table.
                StartActivity(answeredByUserId, educationId, analyticSession);
                #endregion


                #region Step 2. Get attempt
                int attempt = 0;
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("ml_education_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }, 1));
                Row row = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();
                if (row != null)
                {
                    attempt = row.GetValue<int>("attempt");
                }

                attempt++;
                #endregion

                #region Step 2. Insert to table.
                DateTime currentTime = DateTime.UtcNow;
                BatchStatement updateBatch = new BatchStatement();

                for (int i = 0; i < optionIds.Count; i++)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_education_answered_by_user", new List<string> { "answered_by_user_id", "card_id", "attempt", "option_id", "answered_on_timestamp" }));
                    updateBatch.Add(ps.Bind(answeredByUserId, cardId, attempt, optionIds[i], currentTime));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_education_answered_by_card", new List<string> { "answered_by_user_id", "card_id", "attempt", "option_id", "answered_on_timestamp" }));
                    updateBatch.Add(ps.Bind(answeredByUserId, cardId, attempt, optionIds[i], currentTime));
                }

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_education_user_incomplete", new List<string> { "ml_education_id", "user_id" }, new List<string> { "incomplete_on_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(currentTime, educationId, answeredByUserId));


                analyticSession.Execute(updateBatch);
                response.Success = true;
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationCompletedResponse CompletedEducation(string userId, string educationId, ISession analyticSession)
        {
            MLEducationCompletedResponse response = new MLEducationCompletedResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check ml_education_user_incomplete table.
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_incomplete", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                Row row = analyticSession.Execute(ps.Bind(educationId, userId)).FirstOrDefault();
                if (row == null)
                {
                    response.Success = true;
                    Log.Error(ErrorMessage.MLEducationAnalyticProgressError);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.MLEducationAnalyticProgressError);
                    response.ErrorMessage = ErrorMessage.MLEducationAnalyticProgressError;
                    return response;
                }
                #endregion

                #region Step 2. Delete ml_education_user_incomplete table.
                BatchStatement deleteBatch = new BatchStatement();
                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_education_user_incomplete", new List<string> { "ml_education_id", "user_id" }));
                deleteBatch.Add(ps.Bind(educationId, userId));
                analyticSession.Execute(deleteBatch);
                #endregion

                #region Step 3. Insert to ml_education_user_completed table
                DateTime currentTime = DateTime.UtcNow;
                BatchStatement updateBatch = new BatchStatement();
                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_education_user_completed", new List<string> { "ml_education_id", "user_id", "completed_on_timestamp" }));
                updateBatch.Add(ps.Bind(educationId, userId, currentTime));
                analyticSession.Execute(updateBatch);
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationAnalticResponse GetAnalyticAttendance(string companyId, string managerUserId, string categoryId, string educationId, string deparmtnetId = null)
        {
            MLEducationAnalticResponse response = new MLEducationAnalticResponse();
            response.Success = false;
            response.Users = new List<User>();

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                #region Step 1. Get company timezone.
                double timeZone = 0.0;
                CompanyResponse companyResponse = new Company().GetCompany(companyId, mainSession);
                if (companyResponse.Success)
                {
                    timeZone = companyResponse.Company.TimeZoneOffset;
                }
                else
                {
                    response.ErrorCode = companyResponse.ErrorCode;
                    response.ErrorMessage = companyResponse.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 2. Get Education detail
                MLEducationDetailResponse educationResponse = new MLEducation().GetDetail(companyId, managerUserId, categoryId, educationId, null, (int)MLEducation.MLEducationQueryType.Basic, (int)MLEducationCard.QueryType.Basic);
                if (educationResponse.Success)
                {
                    response.Education = educationResponse.Education;
                    response.Education.StartDate = response.Education.StartDate.AddHours(timeZone);
                    response.Education.StartDateString = response.Education.StartDate.ToString("dd/MM/yyyy");

                    DateTime currentDateTime = DateTime.UtcNow.AddHours(timeZone);
                    response.Education.AnalyseDurationString = response.Education.StartDate.ToString("dd/MM/yyyy") + " - " + currentDateTime.ToString("dd/MM/yyyy");
                    if (response.Education.EndDate.HasValue)
                    {
                        response.Education.EndDate = response.Education.EndDate.Value.AddHours(timeZone);
                        response.Education.EndDateString = response.Education.EndDate.Value.ToString("dd/MM/yyyy");
                        if (response.Education.EndDate < currentDateTime)
                        {
                            response.Education.AnalyseDurationString = response.Education.StartDate.ToString("dd/MM/yyyy") + " - " + response.Education.EndDate.Value.ToString("dd/MM/yyyy");
                        }
                    }
                }
                else
                {
                    response.ErrorCode = educationResponse.ErrorCode;
                    response.ErrorMessage = educationResponse.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 3. Get all user in the company.
                if (string.IsNullOrEmpty(deparmtnetId))
                {
                    deparmtnetId = null;
                }
                UserListResponse activeUsersResponse = new User().GetAllUserForAdmin(managerUserId, companyId, deparmtnetId, 0, User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession);
                if (activeUsersResponse.Success)
                {
                    response.Users.AddRange(activeUsersResponse.Users);
                }

                //// Including recently deleted users
                //UserListResponse recentluDeletedUsersResponse = new User().GetAllUserForAdmin(managerUserId, companyId, null, 0, User.AccountStatus.CODE_DELETING, null, false, false, mainSession);
                //if (recentluDeletedUsersResponse.Success)
                //{
                //    response.Users.AddRange(recentluDeletedUsersResponse.Users);
                //}

                //// Including suspended users
                //UserListResponse suspendedUsersResponse = new User().GetAllUserForAdmin(managerUserId, companyId, null, 0, User.AccountStatus.CODE_SUSPENEDED, null, false, false, mainSession);
                //if (suspendedUsersResponse.Success)
                //{
                //    response.Users.AddRange(suspendedUsersResponse.Users);
                //}
                #endregion

                #region Step 4. Calculate users.
                int totalUserCount = response.Users.Count;
                int incompleteCount = 0;
                int completedCount = 0;

                PreparedStatement ps;
                for (int i = 0; i < response.Users.Count; i++)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_incomplete", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                    Row row = analyticSession.Execute(ps.Bind(educationId, response.Users[i].UserId)).FirstOrDefault();
                    if (row != null)
                    {
                        incompleteCount++;
                        response.Users[i].EducationAnalytic = new User.MLEducationAnalytic
                        {
                            LastProgressDateTime = row.GetValue<DateTime>("incomplete_on_timestamp").AddHours(timeZone),
                            LastProgressDateTimeString = row.GetValue<DateTime>("incomplete_on_timestamp").AddHours(timeZone).ToString("dd/MM/yyyy"),
                            ProgressStatus = (int)UserProgressEnum.Incomplete
                        };
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_completed", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                        row = analyticSession.Execute(ps.Bind(educationId, response.Users[i].UserId)).FirstOrDefault();
                        if (row != null)
                        {
                            completedCount++;
                            response.Users[i].EducationAnalytic = new User.MLEducationAnalytic
                            {
                                LastProgressDateTime = row.GetValue<DateTime>("completed_on_timestamp").AddHours(timeZone),
                                LastProgressDateTimeString = row.GetValue<DateTime>("completed_on_timestamp").AddHours(timeZone).ToString("dd/MM/yyyy"),
                                ProgressStatus = (int)UserProgressEnum.Completed
                            };
                        }
                        else
                        {
                            response.Users[i].EducationAnalytic = new User.MLEducationAnalytic
                            {
                                LastProgressDateTime = null,
                                LastProgressDateTimeString = null,
                                ProgressStatus = (int)UserProgressEnum.Absent
                            };
                        }
                    }
                }

                double incPer = (double)incompleteCount / (double)totalUserCount * 100;
                double comPer = (double)completedCount / (double)totalUserCount * 100;

                response.Attendants = new List<Attendance>();
                response.Attendants.Add(new Attendance
                {
                    Progress = (int)UserProgressEnum.Completed,
                    Name = "Completed",
                    Count = completedCount,
                    Percentage = Convert.ToInt16(Math.Round(comPer))
                });

                response.Attendants.Add(new Attendance
                {
                    Progress = (int)UserProgressEnum.Incomplete,
                    Name = "Incomplete",
                    Count = incompleteCount,
                    Percentage = Convert.ToInt16(Math.Round(incPer))
                });

                response.Attendants.Add(new Attendance
                {
                    Progress = (int)UserProgressEnum.Absent,
                    Name = "Absent",
                    Count = totalUserCount - incompleteCount - completedCount,
                    Percentage = 100 - (Convert.ToInt16(Math.Round(incPer))) - (Convert.ToInt16(Math.Round(comPer)))
                });
                #endregion

                #region Step 5. Get all deaprtments
                DepartmentListResponse departmentResponse = new Department().GetAllDepartment(managerUserId, companyId, Department.QUERY_TYPE_BASIC, mainSession);
                if (departmentResponse.Success)
                {
                    response.Departments = departmentResponse.Departments;
                }
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public MLEducationAnalticResponse GetAnalyticUsers(string companyId, string managerUserId, string educationId, string deparmtnetId = null)
        {
            MLEducationAnalticResponse response = new MLEducationAnalticResponse();
            response.Success = false;
            response.Users = new List<User>();

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                #region Step 1. Get company timezone.
                double timeZone = 0.0;
                CompanyResponse companyResponse = new Company().GetCompany(companyId, mainSession);
                if (companyResponse.Success)
                {
                    timeZone = companyResponse.Company.TimeZoneOffset;
                }
                else
                {
                    response.ErrorCode = companyResponse.ErrorCode;
                    response.ErrorMessage = companyResponse.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 2. Get all user in the company.
                if (string.IsNullOrEmpty(deparmtnetId))
                {
                    deparmtnetId = null;
                }
                UserListResponse activeUsersResponse = new User().GetAllUserForAdmin(managerUserId, companyId, deparmtnetId, 0, User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession);
                if (activeUsersResponse.Success)
                {
                    response.Users.AddRange(activeUsersResponse.Users);
                }

                //// Including recently deleted users
                //UserListResponse recentluDeletedUsersResponse = new User().GetAllUserForAdmin(managerUserId, companyId, null, 0, User.AccountStatus.CODE_DELETING, null, false, false, mainSession);
                //if (recentluDeletedUsersResponse.Success)
                //{
                //    response.Users.AddRange(recentluDeletedUsersResponse.Users);
                //}

                //// Including suspended users
                //UserListResponse suspendedUsersResponse = new User().GetAllUserForAdmin(managerUserId, companyId, null, 0, User.AccountStatus.CODE_SUSPENEDED, null, false, false, mainSession);
                //if (suspendedUsersResponse.Success)
                //{
                //    response.Users.AddRange(suspendedUsersResponse.Users);
                //}
                #endregion

                #region Step 3. Calculate users.
                int totalUserCount = response.Users.Count;
                int incompleteCount = 0;
                int completedCount = 0;

                PreparedStatement ps;
                for (int i = 0; i < response.Users.Count; i++)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_incomplete", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                    Row row = analyticSession.Execute(ps.Bind(educationId, response.Users[i].UserId)).FirstOrDefault();
                    if (row != null)
                    {
                        incompleteCount++;
                        response.Users[i].EducationAnalytic = new User.MLEducationAnalytic
                        {
                            LastProgressDateTime = row.GetValue<DateTime>("incomplete_on_timestamp").AddHours(timeZone),
                            LastProgressDateTimeString = row.GetValue<DateTime>("incomplete_on_timestamp").AddHours(timeZone).ToString("dd/MM/yyyy"),
                            ProgressStatus = (int)UserProgressEnum.Incomplete
                        };
                    }
                    else
                    {
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_completed", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                        row = analyticSession.Execute(ps.Bind(educationId, response.Users[i].UserId)).FirstOrDefault();
                        if (row != null)
                        {
                            completedCount++;
                            response.Users[i].EducationAnalytic = new User.MLEducationAnalytic
                            {
                                LastProgressDateTime = row.GetValue<DateTime>("completed_on_timestamp").AddHours(timeZone),
                                LastProgressDateTimeString = row.GetValue<DateTime>("completed_on_timestamp").AddHours(timeZone).ToString("dd/MM/yyyy"),
                                ProgressStatus = (int)UserProgressEnum.Completed
                            };
                        }
                        else
                        {
                            response.Users[i].EducationAnalytic = new User.MLEducationAnalytic
                            {
                                LastProgressDateTime = null,
                                LastProgressDateTimeString = null,
                                ProgressStatus = (int)UserProgressEnum.Absent
                            };
                        }
                    }
                }

                double incPer = (double)(incompleteCount / totalUserCount) * 100;
                double comPer = (double)(completedCount / totalUserCount) * 100;


                response.Attendants = new List<Attendance>();
                response.Attendants.Add(new Attendance
                {
                    Progress = (int)UserProgressEnum.Completed,
                    Name = "Completed",
                    Count = completedCount,
                    Percentage = Convert.ToInt16(Math.Round(comPer))
                });

                response.Attendants.Add(new Attendance
                {
                    Progress = (int)UserProgressEnum.Incomplete,
                    Name = "Incomplete",
                    Count = incompleteCount,
                    Percentage = Convert.ToInt16(Math.Round(incPer))
                });

                response.Attendants.Add(new Attendance
                {
                    Progress = (int)UserProgressEnum.Absent,
                    Name = "Absent",
                    Count = totalUserCount - incompleteCount - completedCount,
                    Percentage = 100 - (Convert.ToInt16(Math.Round(incPer))) - (Convert.ToInt16(Math.Round(comPer)))
                });
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public MLEducationAnalticResponse GetAnalyticUser(string companyId, string managerUserId, string categoryId, string educationId, string answeredUserId)
        {
            MLEducationAnalticResponse response = new MLEducationAnalticResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                #region Step 1. Get company timezone.
                double timeZone = 0.0;
                CompanyResponse companyResponse = new Company().GetCompany(companyId, mainSession);
                if (companyResponse.Success)
                {
                    timeZone = companyResponse.Company.TimeZoneOffset;
                }
                else
                {
                    response.ErrorCode = companyResponse.ErrorCode;
                    response.ErrorMessage = companyResponse.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 2. Get Education detail
                MLEducationDetailResponse educationResponse = new MLEducation().GetDetail(companyId, managerUserId, categoryId, educationId, null, (int)MLEducation.MLEducationQueryType.Full, (int)MLEducationCard.QueryType.Full, answeredUserId);
                if (educationResponse.Success)
                {
                    response.Education = educationResponse.Education;
                    response.Education.StartDate = response.Education.StartDate.AddHours(timeZone);
                    response.Education.StartDateString = response.Education.StartDate.ToString("dd/MM/yyyy");

                    DateTime currentDateTime = DateTime.UtcNow.AddHours(timeZone);
                    response.Education.AnalyseDurationString = response.Education.StartDate.ToString("dd/MM/yyyy") + " - " + currentDateTime.ToString("dd/MM/yyyy");
                    if (response.Education.EndDate.HasValue)
                    {
                        response.Education.EndDate = response.Education.EndDate.Value.AddHours(timeZone);
                        response.Education.EndDateString = response.Education.EndDate.Value.ToString("dd/MM/yyyy");
                        if (response.Education.EndDate < currentDateTime)
                        {
                            response.Education.AnalyseDurationString = response.Education.StartDate.ToString("dd/MM/yyyy") + " - " + response.Education.EndDate.Value.ToString("dd/MM/yyyy");
                        }
                    }
                }
                else
                {
                    response.ErrorCode = educationResponse.ErrorCode;
                    response.ErrorMessage = educationResponse.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 3. Get answered user detail.
                UserDetailResponse userDetailResponse = new User().GetUserDetail(managerUserId, companyId, answeredUserId);
                if (userDetailResponse.Success)
                {
                    response.AnsweredUser = userDetailResponse.User;

                    PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_education_user_completed", new List<string>(), new List<string> { "ml_education_id", "user_id" }));
                    Row row = analyticSession.Execute(ps.Bind(educationId, answeredUserId)).FirstOrDefault();
                    if (row != null)
                    {
                        response.AnsweredUser.EducationAnalytic = new User.MLEducationAnalytic
                        {
                            LastProgressDateTime = row.GetValue<DateTime>("completed_on_timestamp").AddHours(timeZone),
                            LastProgressDateTimeString = row.GetValue<DateTime>("completed_on_timestamp").AddHours(timeZone).ToString("dd/MM/yyyy"),
                            ProgressStatus = (int)UserProgressEnum.Completed
                        };
                    }
                    else
                    {
                        response.AnsweredUser.EducationAnalytic = new User.MLEducationAnalytic
                        {
                            LastProgressDateTime = null,
                            LastProgressDateTimeString = null,
                            ProgressStatus = (int)UserProgressEnum.Absent
                        };
                    }
                }
                else
                {
                    response.ErrorCode = userDetailResponse.ErrorCode;
                    response.ErrorMessage = userDetailResponse.ErrorMessage;
                    return response;
                }
                #endregion

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
