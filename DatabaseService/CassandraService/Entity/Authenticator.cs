﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using Amazon.SimpleEmail.Model;
using Amazon.SimpleEmail;
using Amazon;
using System.Web.Configuration;
using System.Threading;

namespace CassandraService.Entity
{
    public class Authenticator
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public LoginByManagerResponse LoginByManager(String email, String password)
        {
            LoginByManagerResponse response = new LoginByManagerResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check input data.
                if (String.IsNullOrEmpty(email))
                {
                    Log.Error(ErrorMessage.UserMismatchEmailOrPassword);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                    return response;
                }

                if (String.IsNullOrEmpty(password))
                {
                    Log.Error(ErrorMessage.UserMismatchEmailOrPassword);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                    return response;
                }
                #endregion

                #region Step 2. Read database
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication", new List<string> { }, new List<string> { "email" }));
                BoundStatement bs = ps.Bind(email);
                RowSet rowSet = session.Execute(bs);

                if (rowSet != null)
                {
                    List<Company> companies = new List<Company>();
                    foreach (Row row in rowSet.GetRows())
                    {
                        String hashedPassword = Crypto.EncryptTextWithSalt(password, row.GetValue<String>("salt"));
                        if (hashedPassword.Equals(row.GetValue<String>("hashed_password")))
                        {
                            ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                            bs = ps.Bind(row.GetValue<String>("user_id"));
                            Row rowUser = session.Execute(bs).FirstOrDefault();

                            if (rowUser != null && rowUser.GetValue<int>("account_status") == User.AccountStatus.CODE_ACTIVE && rowUser.GetValue<int>("account_type") == User.AccountType.CODE_SUPER_ADMIN) // Super admin
                            {
                                companies.Clear();

                                // Get user information
                                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<String>("user_id"));
                                Row rowAdminOrModerator = session.Execute(bs).FirstOrDefault();

                                User user = new User
                                {
                                    UserId = rowAdminOrModerator.GetValue<String>("id"),
                                    FirstName = rowAdminOrModerator.GetValue<String>("first_name"),
                                    LastName = rowAdminOrModerator.GetValue<String>("last_name"),
                                    Email = rowAdminOrModerator.GetValue<String>("email"),
                                    ProfileImageUrl = rowAdminOrModerator.GetValue<String>("profile_image_url"),
                                    Type = new User.AccountType(rowUser.GetValue<int>("account_type")),
                                    AccessModules = Module.GetModules(User.AccountType.CODE_SUPER_ADMIN),
                                    AccessSystemModules = Module.GetSystemModules(User.AccountType.CODE_SUPER_ADMIN),
                                    NeedResetPassword = row.GetValue<bool>("is_default")
                                };

                                // Get all company information
                                ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "is_valid" }));
                                RowSet rsCompany = session.Execute(ps.Bind(true));
                                foreach (Row rCompany in rsCompany)
                                {
                                    Company company = new Company
                                    {
                                        CompanyId = rCompany.GetValue<String>("id"),
                                        CompanyLogoUrl = rCompany.GetValue<String>("logo_url"),
                                        EmailLogoUrl = rCompany.GetValue<String>("email_square_logo_url"),
                                        CompanyTitle = rCompany.GetValue<String>("title"),
                                        Manager = user,
                                        AdminImageUrl = rCompany.GetValue<String>("admin_profile_image_url"),
                                        PrimaryManagerId = rCompany.GetValue<String>("primary_admin_user_id"),
                                        TimeZoneOffset = rCompany["timezone_offset"] == null ? 8.0 : rCompany.GetValue<double>("timezone_offset"),
                                        CompanyBannerUrl = rCompany.GetValue<string>("admin_website_header_logo_url")
                                    };

                                    if (rCompany.GetValue<String>("id").Equals(row.GetValue<String>("company_id")))
                                    {
                                        user.Company = company;
                                        response.Admin = user;
                                    }

                                    companies.Add(company);
                                }
                                break;
                            }
                            else if (rowUser != null && rowUser.GetValue<int>("account_status") == User.AccountStatus.CODE_ACTIVE && rowUser.GetValue<int>("account_type") == User.AccountType.CODE_COCADRE_ADMIN) // Cocadre admin
                            {
                                // Get user information
                                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<String>("user_id"));
                                Row rowCocadreAdmin = session.Execute(bs).FirstOrDefault();

                                User user = new User
                                {
                                    UserId = rowCocadreAdmin.GetValue<String>("id"),
                                    FirstName = rowCocadreAdmin.GetValue<String>("first_name"),
                                    LastName = rowCocadreAdmin.GetValue<String>("last_name"),
                                    Email = rowCocadreAdmin.GetValue<String>("email"),
                                    ProfileImageUrl = rowCocadreAdmin.GetValue<String>("profile_image_url"),
                                    Type = new User.AccountType(rowUser.GetValue<int>("account_type")),
                                    AccessModules = Module.GetModules(User.AccountType.CODE_COCADRE_ADMIN),
                                    AccessSystemModules = Module.GetSystemModules(User.AccountType.CODE_COCADRE_ADMIN),
                                    NeedResetPassword = row.GetValue<bool>("is_default")
                                };

                                // Get company information
                                ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<String>("company_id"));
                                Row rowCompany = session.Execute(bs).FirstOrDefault();

                                Company company = new Company
                                {
                                    CompanyId = row.GetValue<String>("company_id"),
                                    CompanyLogoUrl = rowCompany.GetValue<String>("logo_url"),
                                    CompanyTitle = rowCompany.GetValue<String>("title"),
                                    EmailLogoUrl = rowCompany.GetValue<String>("email_square_logo_url"),
                                    Manager = user,
                                    AdminImageUrl = rowCompany.GetValue<String>("admin_profile_image_url"),
                                    PrimaryManagerId = rowCompany.GetValue<String>("primary_admin_user_id"),
                                    TimeZoneOffset = rowCompany.GetValue<double>("timezone_offset"),
                                    CompanyBannerUrl = rowCompany.GetValue<string>("admin_website_header_logo_url")
                                };

                                user.Company = company;
                                response.Admin = user;

                                companies.Add(company);
                            }
                            else if (rowUser != null && rowUser.GetValue<int>("account_status") == User.AccountStatus.CODE_ACTIVE && rowUser.GetValue<int>("account_type") > User.AccountType.CODE_NORMAL_USER && rowUser.GetValue<int>("account_type") != User.AccountType.CODE_SUPER_ADMIN) //Company Admin & Moderater
                            {
                                // Get user information
                                ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<String>("user_id"));
                                Row rowAdminOrModerator = session.Execute(bs).FirstOrDefault();

                                // Get access module
                                List<Module> accessModules = new List<Module>();
                                if (rowUser.GetValue<int>("account_type") == User.AccountType.CODE_ADMIN)
                                {
                                    accessModules = Module.GetModules(User.AccountType.CODE_ADMIN, row.GetValue<String>("company_id"));
                                }
                                else if (rowUser.GetValue<int>("account_type") == User.AccountType.CODE_MODERATER)
                                {
                                    ps = session.Prepare(CQLGenerator.SelectStatement("moderator_access_rights", new List<string> { }, new List<string> { "user_id" }));
                                    bs = ps.Bind(row.GetValue<String>("user_id"));
                                    RowSet rowSetRights = session.Execute(bs);
                                    foreach (Row item in rowSetRights)
                                    {
                                        accessModules.Add(new Module(item.GetValue<int>("access_rights_key")));
                                    }

                                    ps = session.Prepare(CQLGenerator.SelectStatement("moderator_access_management", new List<string> { }, new List<string> { "user_id" }));
                                    bs = ps.Bind(row.GetValue<String>("user_id"));
                                    Row row_moderator_access_management = session.Execute(bs).FirstOrDefault();
                                    if (row_moderator_access_management != null)
                                    {
                                        DateTime dt = row_moderator_access_management.GetValue<DateTime?>("expired_timestamp") ?? DateTime.Now;
                                        if (dt < DateTime.UtcNow)
                                        {
                                            continue;
                                        }
                                    }
                                }

                                User user = new User
                                {
                                    UserId = rowAdminOrModerator.GetValue<String>("id"),
                                    FirstName = rowAdminOrModerator.GetValue<String>("first_name"),
                                    LastName = rowAdminOrModerator.GetValue<String>("last_name"),
                                    Email = rowAdminOrModerator.GetValue<String>("email"),
                                    ProfileImageUrl = rowAdminOrModerator.GetValue<String>("profile_image_url"),
                                    Type = new User.AccountType(rowUser.GetValue<int>("account_type")),
                                    AccessModules = accessModules,
                                    NeedResetPassword = row.GetValue<bool>("is_default")
                                };

                                // Get company information
                                ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<string> { }, new List<string> { "id" }));
                                bs = ps.Bind(row.GetValue<String>("company_id"));
                                Row rowCompany = session.Execute(bs).FirstOrDefault();

                                companies.Add(new Company
                                {
                                    CompanyId = row.GetValue<String>("company_id"),
                                    CompanyLogoUrl = rowCompany.GetValue<String>("logo_url"),
                                    CompanyTitle = rowCompany.GetValue<String>("title"),
                                    EmailLogoUrl = rowCompany.GetValue<String>("email_square_logo_url"),
                                    Manager = user,
                                    AdminImageUrl = rowCompany.GetValue<String>("admin_profile_image_url"),
                                    PrimaryManagerId = rowCompany.GetValue<String>("primary_admin_user_id"),
                                    TimeZoneOffset = rowCompany.GetValue<double>("timezone_offset"),
                                    CompanyBannerUrl = rowCompany.GetValue<string>("admin_website_header_logo_url")
                                }
                                );
                            }
                        }
                    }

                    if (companies.Count > 0)
                    {
                        response.Companies = companies;
                        response.Success = true;
                    }
                    else
                    {
                        Log.Error(ErrorMessage.UserInvalid);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                    }
                }
                else
                {
                    Log.Error(ErrorMessage.UserInvalid);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GetManagerInfoResponse GetManagerInfo(String managerId, String companyId)
        {
            GetManagerInfoResponse response = new GetManagerInfoResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check manager account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(managerId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("company", new List<String> { }, new List<String> { "id", "is_valid" }));
                BoundStatement bs = ps.Bind(companyId, true);
                Row row_Company = session.Execute(bs).FirstOrDefault();
                if (row_Company != null)
                {
                    // Get company information
                    response.CompanyId = companyId;
                    response.CompanyName = row_Company.GetValue<String>("title");
                    response.CompanyAdminUrl = row_Company.GetValue<String>("admin_profile_image_url");
                    response.CompanyLogoUrl = row_Company.GetValue<String>("logo_url");
                    response.CompanyTimeZone = row_Company.GetValue<double>("timezone_offset");
                    response.PrimaryManagerId = row_Company.GetValue<String>("primary_admin_user_id");
                    response.CompanyWebsiteHeaderUrl = row_Company.GetValue<String>("admin_website_header_logo_url");

                    ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<String> { }, new List<String> { "user_id" }));
                    bs = ps.Bind(managerId);
                    Row row_UserAccountType = session.Execute(bs).FirstOrDefault();
                    if (row_UserAccountType != null)
                    {
                        if (row_UserAccountType.GetValue<int>("account_status") == User.AccountStatus.CODE_ACTIVE)
                        {
                            // Get access module
                            if (row_UserAccountType.GetValue<int>("account_type") == User.AccountType.CODE_SUPER_ADMIN)
                            {
                                response.ManagerAccountType = new User.AccountType(User.AccountType.CODE_SUPER_ADMIN);
                                response.ManagerAccessModules = Module.GetModules(User.AccountType.CODE_SUPER_ADMIN);
                                response.ManagerAccessSystemModules = Module.GetSystemModules(User.AccountType.CODE_SUPER_ADMIN);
                            }
                            else if (row_UserAccountType.GetValue<int>("account_type") == User.AccountType.CODE_COCADRE_ADMIN)
                            {
                                response.ManagerAccountType = new User.AccountType(User.AccountType.CODE_COCADRE_ADMIN);
                                response.ManagerAccessModules = Module.GetModules(User.AccountType.CODE_COCADRE_ADMIN);
                                response.ManagerAccessSystemModules = Module.GetSystemModules(User.AccountType.CODE_COCADRE_ADMIN);
                            }
                            else if (row_UserAccountType.GetValue<int>("account_type") == User.AccountType.CODE_ADMIN)
                            {
                                response.ManagerAccountType = new User.AccountType(User.AccountType.CODE_ADMIN);
                                response.ManagerAccessModules = Module.GetModules(User.AccountType.CODE_ADMIN, companyId);
                            }
                            else if (row_UserAccountType.GetValue<int>("account_type") == User.AccountType.CODE_MODERATER)
                            {
                                response.ManagerAccountType = new User.AccountType(User.AccountType.CODE_MODERATER);
                                List<Module> accessModules = new List<Module>();
                                ps = session.Prepare(CQLGenerator.SelectStatement("moderator_access_rights", new List<String> { }, new List<String> { "user_id" }));
                                bs = ps.Bind(managerId);
                                RowSet rowSetRights = session.Execute(bs);
                                foreach (Row item in rowSetRights)
                                {
                                    accessModules.Add(new Module(item.GetValue<int>("access_rights_key")));
                                }
                                response.ManagerAccessModules = accessModules;
                            }
                            else
                            {
                                // user is normal user.
                                return response;
                            }

                            // Get user information
                            ps = session.Prepare(CQLGenerator.SelectStatement("user_basic", new List<String> { }, new List<String> { "id" }));
                            bs = ps.Bind(managerId);
                            Row rowUserBasicr = session.Execute(bs).FirstOrDefault();
                            if (rowUserBasicr != null)
                            {
                                response.ManagerUserId = managerId;
                                response.ManagerEmail = rowUserBasicr.GetValue<String>("email");
                                response.ManagerFirstName = rowUserBasicr.GetValue<String>("first_name");
                                response.ManagerLastName = rowUserBasicr.GetValue<String>("last_name");
                                response.ManagerProfileImageUrl = rowUserBasicr.GetValue<String>("profile_image_url");
                                response.Success = true;
                            }
                            else
                            {
                                // Cannot find data at user_basic table.
                                return response;
                            }
                        }
                        else
                        {
                            // user is not on active status.
                            return response;
                        }
                    }
                    else
                    {
                        // Cannot find data at user_account_type table.
                        return response;
                    }
                }
                else
                {
                    // Cannot find data at company table.
                    return response;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;

            }
            return response;
        }

        public Row CheckAuthenticationExists(string email,
                                             ISession session)
        {
            Row authenticationRow = null;
            try
            {
                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string>(), new List<string> { "email" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email);
                authenticationRow = session.Execute(bsAuthentication).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return authenticationRow;
        }

        public AuthenticationCheckEmailForCompanyResponse CheckEmailExistsForCompany(string adminUserId, string companyId, string email)
        {
            AuthenticationCheckEmailForCompanyResponse response = new AuthenticationCheckEmailForCompanyResponse();
            response.IsEmailExists = false;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string>(), new List<string> { "email", "company_id" }));
                Row authenticationRow = session.Execute(ps.Bind(email, companyId)).FirstOrDefault();

                if (authenticationRow != null)
                {
                    string userId = authenticationRow.GetValue<string>("user_id");

                    ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                        new List<string>(), new List<string> { "user_id", "account_status" }));
                    Row deletedUserRow = session.Execute(ps.Bind(userId, User.AccountStatus.CODE_DELETED)).FirstOrDefault();

                    if (deletedUserRow == null)
                    {
                        response.IsEmailExists = true;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

#warning Deprecated code in SelectAuthenticatedUser, replaced by SelectAuthenticatedEmailWithAllCompanies
        public AuthenticationSelectUserResponse SelectAuthenticatedUser(string email,
                                                                        string encryptedPassword)
        {
            AuthenticationSelectUserResponse response = new AuthenticationSelectUserResponse();
            response.UserToken = null;
            response.Companies = new List<Company>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string> { "user_id", "salt", "company_id", "hashed_password", "is_default" }, new List<string> { "email" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email);
                RowSet authenticationRowSet = session.Execute(bsAuthentication);

                List<Dictionary<string, string>> authenticatedList = new List<Dictionary<string, string>>();

                foreach (Row authenticationRow in authenticationRowSet)
                {
                    string salt = authenticationRow.GetValue<string>("salt");
                    string currentHashedPassword = Crypto.EncryptTextWithSalt(encryptedPassword, salt);
                    string dbHashedPassword = authenticationRow.GetValue<string>("hashed_password");

                    if (currentHashedPassword.Equals(dbHashedPassword))
                    {
                        Dictionary<string, string> authenticatedUserDict = new Dictionary<string, string>();

                        string userId = authenticationRow.GetValue<string>("user_id");
                        string companyId = authenticationRow.GetValue<string>("company_id");

                        Row companyRow = vh.ValidateCompany(companyId, session);
                        if (companyRow == null)
                        {
                            Log.Error("Company invalid for login: " + companyId);
                            continue;
                        }

                        PreparedStatement psUserAccountType = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                            new List<string> { }, new List<string> { "user_id", "account_status" }));
                        BoundStatement bsUserAccountType = psUserAccountType.Bind(userId, Int32.Parse(UserStatus.Active));
                        Row accountRow = session.Execute(bsUserAccountType).FirstOrDefault();
                        if (accountRow == null)
                        {
                            Log.Error("Account has been deleted: " + email);
                            continue;
                        }

                        string companyTitle = companyRow.GetValue<string>("title");
                        string companyLogoUrl = companyRow.GetValue<string>("logo_url");
                        bool isPasswordDefault = authenticationRow["is_default"] != null ? authenticationRow.GetValue<bool>("is_default") : true;

                        authenticatedUserDict.Add("userId", userId);
                        authenticatedUserDict.Add("companyId", companyId);
                        authenticatedUserDict.Add("email", email);

                        authenticatedList.Add(authenticatedUserDict);

                        Company company = new Company
                        {
                            CompanyId = companyId,
                            CompanyTitle = companyTitle,
                            CompanyLogoUrl = companyLogoUrl,
                            UserId = userId,
                            IsPasswordDefault = isPasswordDefault,
                        };
                        response.Companies.Add(company);
                    }
                }

                if (response.Companies.Count == 0)
                {
                    Log.Debug("Mismatch email or password");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                }
                else
                {
                    Authenticator authenticator = new Authenticator();
                    string userToken = authenticator.CreateAuthenticationToken(authenticatedList, null, session);

                    response.UserToken = userToken;

                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationSelectUserResponse SelectAuthenticatedEmailWithAllCompanies(string email,
                                                                                         string encryptedPassword)
        {
            AuthenticationSelectUserResponse response = new AuthenticationSelectUserResponse();
            response.UserToken = null;
            response.Companies = new List<Company>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                if (string.IsNullOrEmpty(email))
                {
                    Log.Debug("Empty email");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserEmptyEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserEmptyEmailOrPassword;
                    return response;
                }

                if (string.IsNullOrEmpty(encryptedPassword))
                {
                    Log.Debug("Empty password");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserEmptyEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserEmptyEmailOrPassword;
                    return response;
                }

                email = email.ToLower().Trim();

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string>(), new List<string> { "email" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email);
                RowSet authenticationRowset = session.Execute(bsAuthentication);

                bool isAuthenticated = false;

                List<Dictionary<string, string>> authenticatedList = new List<Dictionary<string, string>>();

                foreach (Row authenticationRow in authenticationRowset)
                {
                    string salt = authenticationRow.GetValue<string>("salt");
                    string currentHashedPassword = Crypto.EncryptTextWithSalt(encryptedPassword, salt);
                    string dbHashedPassword = authenticationRow.GetValue<string>("hashed_password");

                    if (currentHashedPassword.Equals(dbHashedPassword))
                    {
                        Dictionary<string, string> authenticatedUserDict = new Dictionary<string, string>();

                        string userId = authenticationRow.GetValue<string>("user_id");
                        string companyId = authenticationRow.GetValue<string>("company_id");

                        //CHECK
                        authenticatedUserDict.Add("userId", userId);
                        authenticatedUserDict.Add("companyId", companyId);
                        authenticatedUserDict.Add("email", email);

                        authenticatedList.Add(authenticatedUserDict);

                        if (!isAuthenticated)
                        {
                            // So long as one is correct
                            psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                                new List<string>(), new List<string> { "email" }));
                            RowSet emailRowset = session.Execute(psAuthentication.Bind(email));

                            foreach (Row emailRow in emailRowset)
                            {
                                userId = emailRow.GetValue<string>("user_id");
                                companyId = emailRow.GetValue<string>("company_id");

                                Row companyRow = vh.ValidateCompany(companyId, session);
                                if (companyRow == null)
                                {
                                    Log.Error("Company invalid for login: " + companyId);
                                    continue;
                                }

                                PreparedStatement psUserAccountType = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                                    new List<string> { }, new List<string> { "user_id", "account_status" }));
                                BoundStatement bsUserAccountType = psUserAccountType.Bind(userId, Int32.Parse(UserStatus.Active));
                                Row accountRow = session.Execute(bsUserAccountType).FirstOrDefault();
                                if (accountRow == null)
                                {
                                    Log.Error("Account has been deleted: " + email);
                                    continue;
                                }

                                string companyTitle = companyRow.GetValue<string>("title");
                                string companyLogoUrl = companyRow.GetValue<string>("logo_url");
                                bool isPasswordDefault = emailRow["is_default"] != null ? emailRow.GetValue<bool>("is_default") : true;

                                Company company = new Company
                                {
                                    CompanyId = companyId,
                                    CompanyTitle = companyTitle,
                                    CompanyLogoUrl = companyLogoUrl,
                                    UserId = userId,
                                    IsPasswordDefault = isPasswordDefault,
                                    NumberOfNotifications = new Notification().SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification
                                };
                                response.Companies.Add(company);
                            }

                            isAuthenticated = true;
                        }
                    }
                }

                if (response.Companies.Count == 0)
                {
                    Log.Debug("Mismatch email or password");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                }
                else
                {
                    Authenticator authenticator = new Authenticator();
                    string userToken = authenticator.CreateAuthenticationToken(authenticatedList, null, session);

                    response.UserToken = userToken;

                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationSelectUserByCompanyResponse AuthenticateLoginByCompany(string loginCompanyId,
                                                                                    string loginUserId,
                                                                                    string currentAuthenticationToken,
                                                                                    string encryptedPassword)
        {
            AuthenticationSelectUserByCompanyResponse response = new AuthenticationSelectUserByCompanyResponse();
            response.UserToken = string.Empty;
            response.IsPasswordDefault = false;
            response.UnlockedModules = new List<Module>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                // Check for intended email with userId
                Row userRow = vh.ValidateUser(loginUserId, loginCompanyId, session);
                if (userRow == null)
                {
                    Log.Error("Invalid userId: " + loginUserId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                string email = userRow.GetValue<string>("email");

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string>(), new List<string> { "email", "user_id" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email, loginUserId);
                Row authenticationRow = session.Execute(bsAuthentication).FirstOrDefault();

                string salt = authenticationRow.GetValue<string>("salt");
                string currentHashedPassword = Crypto.EncryptTextWithSalt(encryptedPassword, salt);
                string dbHashedPassword = authenticationRow.GetValue<string>("hashed_password");

                if (!currentHashedPassword.Equals(dbHashedPassword))
                {
                    Log.Debug("Mismatch email or password");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserMismatchEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserMismatchEmailOrPassword;
                    return response;
                }

                bool isDefault = authenticationRow.GetValue<bool>("is_default");
                response.IsPasswordDefault = isDefault;

                // Check for all available userIds with the same password
                psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                    new List<string>(), new List<string> { "email" }));
                bsAuthentication = psAuthentication.Bind(email);
                RowSet authenticationRowset = session.Execute(bsAuthentication);

                List<Dictionary<string, string>> authenticatedList = new List<Dictionary<string, string>>();

                foreach (Row allAuthenticationRow in authenticationRowset)
                {
                    salt = allAuthenticationRow.GetValue<string>("salt");
                    currentHashedPassword = Crypto.EncryptTextWithSalt(encryptedPassword, salt);
                    dbHashedPassword = allAuthenticationRow.GetValue<string>("hashed_password");

                    if (currentHashedPassword.Equals(dbHashedPassword))
                    {
                        Dictionary<string, string> authenticatedUserDict = new Dictionary<string, string>();

                        string userId = authenticationRow.GetValue<string>("user_id");
                        string companyId = authenticationRow.GetValue<string>("company_id");

                        //CHECK
                        authenticatedUserDict.Add("userId", userId);
                        authenticatedUserDict.Add("companyId", companyId);
                        authenticatedUserDict.Add("email", email);

                        authenticatedList.Add(authenticatedUserDict);
                    }
                }

                Authenticator authenticator = new Authenticator();
                string userToken = authenticator.CreateAuthenticationToken(authenticatedList, currentAuthenticationToken, session);
                response.UserToken = userToken;

                User userManager = new User();
                UserSelectWithCompanyResponse userResponse = userManager.SelectUserWithCompany(loginUserId, loginCompanyId);
                userResponse.User.Company.IsPasswordDefault = isDefault;
                response.User = userResponse.User;

                response.UnlockedModules = new Module().GetAllModules(loginCompanyId, session);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationSelectCompanyResponse SwitchCompanyByUser(string requesterUserId, string fromCompanyId)
        {
            AuthenticationSelectCompanyResponse response = new AuthenticationSelectCompanyResponse();
            response.Companies = new List<Company>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, fromCompanyId, session);
                if (userRow == null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                string email = userRow.GetValue<string>("email");

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                                new List<string>(), new List<string> { "email" }));
                RowSet authenticationRowset = session.Execute(psAuthentication.Bind(email));

                foreach (Row authenticationRow in authenticationRowset)
                {
                    string userId = authenticationRow.GetValue<string>("user_id");
                    string companyId = authenticationRow.GetValue<string>("company_id");

                    Row companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow == null)
                    {
                        Log.Error("Company invalid for login: " + companyId);
                        continue;
                    }

                    PreparedStatement psUserAccountType = session.Prepare(CQLGenerator.SelectStatement("user_account_type",
                        new List<string> { }, new List<string> { "user_id", "account_status" }));
                    BoundStatement bsUserAccountType = psUserAccountType.Bind(userId, Int32.Parse(UserStatus.Active));
                    Row accountRow = session.Execute(bsUserAccountType).FirstOrDefault();
                    if (accountRow == null)
                    {
                        Log.Error("Account has been deleted: " + email);
                        continue;
                    }

                    string companyTitle = companyRow.GetValue<string>("title");
                    string companyLogoUrl = companyRow.GetValue<string>("logo_url");
                    bool isPasswordDefault = authenticationRow["is_default"] != null ? authenticationRow.GetValue<bool>("is_default") : true;

                    Company company = new Company
                    {
                        CompanyId = companyId,
                        CompanyTitle = companyTitle,
                        CompanyLogoUrl = companyLogoUrl,
                        UserId = userId,
                        IsPasswordDefault = isPasswordDefault,
                        NumberOfNotifications = new Notification().SelectNotificationNumberByUser(userId, companyId, session).NumberOfNotification
                    };
                    response.Companies.Add(company);
                }

                if (response.Companies.Count == 0)
                {
                    Log.Debug("Invalid userId for switch company: " + requesterUserId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }
                else
                {
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool CreateAuthenticatedUser(string companyId,
                                            string newUserId,
                                            string adminUserId,
                                            string email,
                                            string plainPassword,
                                            ISession session)
        {
            bool response = false;

            try
            {
                string randomSalt = Crypto.GenerateRandomSalt(plainPassword.Length);
                string hashedPassword = Crypto.EncryptTextWithSalt(plainPassword, randomSalt);

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.InsertStatement("user_authentication",
                    new List<string> { "user_id", "email", "hashed_password", "salt", "company_id", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(newUserId, email, hashedPassword, randomSalt, companyId, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow);
                session.Execute(bsAuthentication);

                response = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public bool UpdateCompanyForAuthenticatedUser(string email,
                                                      string userId,
                                                      string updatedCompanyId,
                                                      ISession session)
        {
            bool response = false;

            try
            {
                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("user_authentication",
                    new List<string> { "email", "user_id" }, new List<string> { "company_id" }, new List<string>()));
                BoundStatement bsAuthentication = psAuthentication.Bind(updatedCompanyId, email, userId);
                session.Execute(bsAuthentication);

                response = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public bool DeleteAuthenticatedUser(string email,
                                            string userId,
                                            ISession session)
        {
            bool response = false;

            try
            {
                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.DeleteStatement("user_authentication",
                    new List<string> { "email", "user_id" }));
                BoundStatement bsAuthentication = psAuthentication.Bind(email, userId);
                session.Execute(bsAuthentication);

                response = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public string SelectAuthenticationToken(string userId,
                                                ISession session)
        {
            string userToken = string.Empty;

            try
            {
                PreparedStatement psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_token",
                    new List<string> { "token" }, new List<string> { "user_id" }));
                BoundStatement bsUserToken = psUserToken.Bind(userId);
                Row userTokenRow = session.Execute(bsUserToken).FirstOrDefault();

                if (userTokenRow != null)
                {
                    string token = userTokenRow.GetValue<string>("token");
                    userToken = token;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return userToken;
        }

        public string CreateAuthenticationToken(List<Dictionary<string, string>> authenticatedList,
                                                string currentAuthenticationToken,
                                                ISession session)
        {
            string userToken = string.Empty;

            try
            {
                DateTime currentTime = DateTime.UtcNow;
                BatchStatement batch = new BatchStatement();

                string token = UUIDGenerator.GenerateUniqueTokenForUser();

                PreparedStatement psUserToken = null;

                // Invalidate current existing token
                if (!string.IsNullOrEmpty(currentAuthenticationToken))
                {
                    psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_authentication_token",
                        new List<string>(), new List<string> { "user_token", "is_token_valid" }));
                    RowSet userTokenRowset = session.Execute(psUserToken.Bind(currentAuthenticationToken, true));

                    foreach (Row userTokenRow in userTokenRowset)
                    {
                        string invalidatedUserId = userTokenRow.GetValue<string>("user_id");
                        psUserToken = session.Prepare(CQLGenerator.UpdateStatement("user_authentication_token",
                            new List<string> { "user_token", "user_id" }, new List<string> { "is_token_valid", "last_modified_timestamp" }, new List<string>()));
                        batch.Add(psUserToken.Bind(false, currentTime, currentAuthenticationToken, invalidatedUserId));

                        psUserToken = session.Prepare(CQLGenerator.UpdateStatement("user_authentication_token_by_user",
                            new List<string> { "user_token", "user_id" }, new List<string> { "is_token_valid", "last_modified_timestamp" }, new List<string>()));
                        batch.Add(psUserToken.Bind(false, currentTime, currentAuthenticationToken, invalidatedUserId));
                    }
                }

                // Assign new tokens and if current existing userId has a token, invalidate it
                foreach (Dictionary<string, string> authenticatedUser in authenticatedList)
                {
                    string email = authenticatedUser["email"];
                    string companyId = authenticatedUser["companyId"];
                    string userId = authenticatedUser["userId"];

                    psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_authentication_token_by_user",
                    new List<string>(), new List<string> { "user_id", "is_token_valid" }));

                    Row userTokenRow = session.Execute(psUserToken.Bind(userId, true)).FirstOrDefault();

                    if (userTokenRow != null)
                    {
                        string oldToken = userTokenRow.GetValue<string>("user_token");
                        psUserToken = session.Prepare(CQLGenerator.UpdateStatement("user_authentication_token",
                            new List<string> { "user_token", "user_id" }, new List<string> { "is_token_valid", "last_modified_timestamp" }, new List<string>()));
                        batch.Add(psUserToken.Bind(false, currentTime, oldToken, userId));

                        psUserToken = session.Prepare(CQLGenerator.UpdateStatement("user_authentication_token_by_user",
                            new List<string> { "user_token", "user_id" }, new List<string> { "is_token_valid", "last_modified_timestamp" }, new List<string>()));
                        batch.Add(psUserToken.Bind(false, currentTime, oldToken, userId));
                    }

                    psUserToken = session.Prepare(CQLGenerator.InsertStatement("user_authentication_token",
                        new List<string> { "email", "user_token", "user_id", "company_id", "is_token_valid", "last_modified_timestamp" }));
                    batch.Add(psUserToken.Bind(email, token, userId, companyId, true, currentTime));

                    psUserToken = session.Prepare(CQLGenerator.InsertStatement("user_authentication_token_by_user",
                        new List<string> { "email", "user_token", "user_id", "company_id", "is_token_valid", "last_modified_timestamp" }));
                    batch.Add(psUserToken.Bind(email, token, userId, companyId, true, currentTime));
                }

                session.Execute(batch);

                userToken = token;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return userToken;
        }

#warning Deprecated code in CheckUserTokenValidity, now check with userToken and userId
        public UserTokenValidityResponse CheckUserTokenValidity(string userToken)
        {
            UserTokenValidityResponse response = new UserTokenValidityResponse();
            response.isTokenExists = false;
            response.isTokenValid = false;
            response.Success = false;
            try
            {
                // Check for invalid and does not exists
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_authentication_token",
                    new List<string>(), new List<string> { "user_token" }));
                BoundStatement bsUserToken = psUserToken.Bind(userToken);
                Row userTokenRow = session.Execute(bsUserToken).FirstOrDefault();

                if (userTokenRow != null)
                {
                    bool isValid = userTokenRow.GetValue<bool>("is_token_valid");
                    response.isTokenExists = true;
                    response.isTokenValid = isValid;
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public UserTokenValidityResponse CheckUserTokenValidity(string userToken, string userId)
        {
            UserTokenValidityResponse response = new UserTokenValidityResponse();
            response.isTokenExists = false;
            response.isTokenValid = false;
            response.Success = false;
            try
            {
                // Check for invalid and does not exists
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                PreparedStatement psUserToken = session.Prepare(CQLGenerator.SelectStatement("user_authentication_token",
                    new List<string>(), new List<string> { "user_token", "user_id" }));
                BoundStatement bsUserToken = psUserToken.Bind(userToken, userId);
                Row userTokenRow = session.Execute(bsUserToken).FirstOrDefault();

                if (userTokenRow != null)
                {
                    bool isValid = userTokenRow.GetValue<bool>("is_token_valid");
                    response.isTokenExists = true;
                    response.isTokenValid = isValid;
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error("UserToken: " + userToken);
                Log.Error("UserId: " + userId);
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public AuthenticationUpdateResponse UpdatePasswordForUser(string requesterUserId, string email, string companyId, string newPassword, string currentPassword = null, int accountType = User.AccountType.CODE_NORMAL_USER)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;

            ISession session = null;
            PreparedStatement psAuthentication = null;
            BoundStatement bsAuthentication = null;

            string currentHashedPassword = string.Empty;
            string currentRandomSalt = string.Empty;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                session = cm.getMainSession();
                ValidationHandler vh = new ValidationHandler();

                if (!string.IsNullOrEmpty(companyId)) // user is normal admin, modereter or user.
                {
                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                  new List<string>(), new List<string> { "email", "user_id" }));
                bsAuthentication = psAuthentication.Bind(email, requesterUserId);
                Row authenticationRow = session.Execute(bsAuthentication).FirstOrDefault();

                if (authenticationRow == null)
                {
                    Log.Error("Invalid authentication for email: " + email);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalidEmail);
                    response.ErrorMessage = ErrorMessage.UserInvalidEmail;
                    return response;
                }

                if (string.IsNullOrEmpty(companyId)) // User is Cocadre admin or super admin.
                {
                    companyId = authenticationRow.GetValue<string>("company_id");
                }

                #region need check current password!
                if (!string.IsNullOrEmpty(currentPassword))
                {
                    currentHashedPassword = Crypto.EncryptTextWithSalt(currentPassword, authenticationRow.GetValue<String>("salt"));
                    if (!currentHashedPassword.Equals(authenticationRow.GetValue<String>("hashed_password")))
                    {
                        Log.Error(ErrorMessage.UserPasswordMismatch);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.UserPasswordMismatch);
                        response.ErrorMessage = ErrorMessage.UserPasswordMismatch;
                        return response;
                    }
                }
                #endregion

                if (string.IsNullOrEmpty(newPassword))
                {
                    Log.Error("Password missing");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserEmptyEmailOrPassword);
                    response.ErrorMessage = ErrorMessage.UserEmptyEmailOrPassword;
                    return response;
                }

                string randomSalt = Crypto.GenerateRandomSalt(newPassword.Length);
                string hashedPassword = Crypto.EncryptTextWithSalt(newPassword, randomSalt);

                psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("user_authentication",
                    new List<string> { "email", "user_id" }, new List<string> { "hashed_password", "salt", "is_default", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                bsAuthentication = psAuthentication.Bind(hashedPassword, randomSalt, false, requesterUserId, DateTime.UtcNow, email, requesterUserId);

                session.Execute(bsAuthentication);

                #region Logout normal modereter, normal admin, cocadre admin and super admin on all devices.
                if (accountType > User.AccountType.CODE_NORMAL_USER)
                {
                    LogoutUser(requesterUserId, companyId);
                }
                #endregion

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }

        public AuthenticationUpdateResponse ReinviteUsers(string adminUserId, string companyId)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = true;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.Success = false;
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    response.Success = false;
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                // Get all userIds with pending invite and pending login
                User userManager = new User();
                List<User> pendingInviteUsers = userManager.GetAllUserForAdmin(adminUserId, companyId, null, 0, User.AccountStatus.CODE_ACTIVE, null, true, false, null, session).Users;
                List<User> pendingLoginUsers = userManager.GetAllUserForAdmin(adminUserId, companyId, null, 0, User.AccountStatus.CODE_ACTIVE, null, false, true, null, session).Users;

                IEqualityComparer<User> userComparer = new PropertyComparer<User>("UserId");
                List<User> massUsers = pendingInviteUsers.Union(pendingLoginUsers).Distinct(userComparer).ToList();

                // Spawn a new thread
                Thread thread = new Thread(() => MassReinviteUsersThread(adminUserId, massUsers, companyRow, DateTime.UtcNow, session));
                thread.Start();

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void MassReinviteUsersThread(string adminUserId, List<User> pendingInviteUsers, Row companyRow, DateTime currentTime, ISession session)
        {
            BatchStatement updateBatch = new BatchStatement();
            PreparedStatement ps = null;

            string companyId = companyRow.GetValue<string>("id");
            for (int index = 1; index <= pendingInviteUsers.Count; index++)
            {
                string userId = pendingInviteUsers[index - 1].UserId;
                string email = pendingInviteUsers[index - 1].Email;
                if (index % 10 == 0)
                {
                    Thread.Sleep(1000);
                }

                string plainPassword = "12345";
                string salt = Crypto.GenerateRandomSalt(plainPassword.Length);
                string hashedPassword = Crypto.EncryptTextWithSalt(plainPassword, salt);

                CompanySelectEmailDetailResponse emailDetail = new Company().SelectEmailTemplate(companyId, (int)Company.CompanyEmailTemplate.PersonnelInvite, session, companyRow);

                EmailSendResponse sendResponse = EmailHandler.SendNewUserEmail(email, companyId, emailDetail.EmailTitle, emailDetail.EmailDescription, emailDetail.EmailSupportInfo, emailDetail.EmailLogoUrl, plainPassword, session);
                if (!sendResponse.Success)
                {
                    Log.Error(sendResponse.ErrorMessage);
                    continue;
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("user_authentication", new List<string> { "email", "user_id" }, new List<string> { "hashed_password", "salt", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(hashedPassword, salt, adminUserId, currentTime, email, userId));

                ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "is_email_sent" }, new List<string>()));
                updateBatch.Add(ps.Bind(true, userId));

                session.Execute(updateBatch);
                updateBatch = new BatchStatement();
            }
        }

        public AuthenticationUpdateResponse ReinviteUser(string adminUserId, string companyId, string userId, Row companyRow = null, ISession session = null)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow == null)
                    {
                        response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                        response.ErrorMessage = ErrorMessage.CompanyInvalid;
                        return response;
                    }
                }


                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;
                DateTime currentTime = DateTime.UtcNow;

                Row userRow = vh.ValidateUser(userId, companyId, session);
                if (userRow != null)
                {
                    string email = userRow.GetValue<string>("email");
                    try
                    {
                        string plainPassword = UUIDGenerator.GenerateNumberPasswordForUser();
                        string salt = Crypto.GenerateRandomSalt(plainPassword.Length);
                        string hashedPassword = Crypto.EncryptTextWithSalt(plainPassword, salt);

                        CompanySelectEmailDetailResponse emailDetail = new Company().SelectEmailTemplate(companyId, (int)Company.CompanyEmailTemplate.PersonnelInvite, session, companyRow);

                        EmailSendResponse sendResponse = EmailHandler.SendNewUserEmail(email, companyId, emailDetail.EmailTitle, emailDetail.EmailDescription, emailDetail.EmailSupportInfo, emailDetail.EmailLogoUrl, plainPassword, session);
                        if (!sendResponse.Success)
                        {
                            response.ErrorCode = sendResponse.ErrorCode;
                            response.ErrorMessage = sendResponse.ErrorMessage;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.UpdateStatement("user_authentication", new List<string> { "email", "user_id" }, new List<string> { "hashed_password", "salt", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                        updateBatch.Add(ps.Bind(hashedPassword, salt, adminUserId, currentTime, email, userId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("user_basic", new List<string> { "id" }, new List<string> { "is_email_sent" }, new List<string>()));
                        updateBatch.Add(ps.Bind(true, userId));

                        session.Execute(updateBatch);
                        response.Success = true;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex.ToString(), ex);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.UserEmailNotSent);
                        response.ErrorMessage = ErrorMessage.UserEmailNotSent;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationUpdateResponse ResetPasswordForUser(string adminUserId, string companyId, string userId)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                Row userRow = vh.ValidateUser(userId, companyId, session);
                if (userRow == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                // Check if user has already login to Cocadre
                if (userRow["joined_on_timestamp"] == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserHaveNotLogin);
                    response.ErrorMessage = ErrorMessage.UserHaveNotLogin;
                    return response;
                }

                string email = userRow.GetValue<string>("email");
                string resetToken = GenerateResetPasswordToken(userId, companyId, email, session);

                string companyTitle = companyRow.GetValue<string>("title");
                string companyLogoUrl = companyRow.GetValue<string>("logo_url");

                Company company = new Company
                {
                    CompanyId = companyId,
                    CompanyTitle = companyTitle,
                    CompanyLogoUrl = companyLogoUrl,
                    ResetPasswordUrl = string.Format("{0}{1}{2}", WebConfigurationManager.AppSettings["base_api_url"], DefaultResource.EmailResetApiUrl, resetToken)
                };

                CompanySelectEmailDetailResponse emailDetail = new Company().SelectEmailTemplate(companyId, (int)Company.CompanyEmailTemplate.ForgotPassword, session, null);

                EmailSendResponse sendResponse = EmailHandler.SendForgetPasswordEmail(email, emailDetail.EmailTitle, emailDetail.EmailDescription, emailDetail.EmailSupportInfo, emailDetail.EmailLogoUrl, new List<Company> { company }, session);
                if (!sendResponse.Success)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationUpdateResponse ResetPasswordForEmail(string email)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                PreparedStatement psAuthentication = null;
                BatchStatement updateBatch = new BatchStatement();

                psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                  new List<string>(), new List<string> { "email" }));
                RowSet authenticationRowSet = session.Execute(psAuthentication.Bind(email));

                List<Company> companiesForReset = new List<Company>();

                foreach (Row authenticationRow in authenticationRowSet)
                {
                    string userId = authenticationRow.GetValue<string>("user_id");
                    string companyId = authenticationRow.GetValue<string>("company_id");

                    Row companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow != null)
                    {
                        Row userRow = vh.ValidateUser(userId, companyId, session);
                        if (userRow != null)
                        {
                            string resetToken = GenerateResetPasswordToken(userId, companyId, email, session);
                            string companyTitle = companyRow.GetValue<string>("title");
                            string companyLogoUrl = companyRow.GetValue<string>("logo_url");

                            Company company = new Company
                            {
                                CompanyId = companyId,
                                CompanyTitle = companyTitle,
                                CompanyLogoUrl = companyLogoUrl,
                                ResetPasswordUrl = string.Format("{0}{1}{2}", WebConfigurationManager.AppSettings["base_api_url"], DefaultResource.EmailResetApiUrl, resetToken)
                            };

                            companiesForReset.Add(company);
                        }
                    }

                }

                if (companiesForReset.Count == 0)
                {
                    Log.Error("Invalid email: " + email);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalidEmail);
                    response.ErrorMessage = ErrorMessage.UserInvalidEmail;
                    return response;
                }

                string title = DefaultResource.CompanyEmailForgotPasswordTitle;
                string logoUrl = DefaultResource.CompanyEmailSquareImageUrl;
                string description = DefaultResource.CompanyEmailForgotPasswordDescription;
                string supportInfo = DefaultResource.CompanyEmailForgotPasswordSupportInfo;

                if (companiesForReset.Count == 1)
                {
                    CompanySelectEmailDetailResponse emailDetail = new Company().SelectEmailTemplate(companiesForReset[0].CompanyId, (int)Company.CompanyEmailTemplate.ForgotPassword, session, null);

                    title = emailDetail.EmailLogoUrl;
                    logoUrl = emailDetail.EmailTitle;
                    description = emailDetail.EmailDescription;
                    supportInfo = emailDetail.EmailSupportInfo;
                }

                EmailSendResponse sendResponse = EmailHandler.SendForgetPasswordEmail(email, title, description, supportInfo, logoUrl, companiesForReset, session);
                if (!sendResponse.Success)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;
                    return response;
                }
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;

        }

        private string GenerateResetPasswordToken(string userId, string companyId, string email, ISession session)
        {
            string resetToken = string.Empty;

            try
            {
                DateTime currentTime = DateTime.UtcNow;

                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement psAuthentication = session.Prepare(CQLGenerator.SelectStatement("reset_email_token_by_user",
                    new List<string>(), new List<string> { "user_id", "is_valid" }));
                Row resetTokenRow = session.Execute(psAuthentication.Bind(userId, true)).FirstOrDefault();

                bool needNewToken = true;

                if (resetTokenRow != null)
                {
                    DateTimeOffset resetTimestamp = resetTokenRow.GetValue<DateTimeOffset>("reset_timestamp").ToUniversalTime();
                    string currentResetToken = resetTokenRow.GetValue<string>("reset_token");
                    if (currentTime < resetTimestamp.AddHours(Convert.ToInt16(WebConfigurationManager.AppSettings["reset_password_expired_hours"])))
                    {
                        resetToken = currentResetToken;
                        needNewToken = false;
                    }
                    else
                    {
                        // Token expired
                        psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("reset_email_token",
                            new List<string> { "reset_token", "reset_timestamp" }, new List<string> { "is_valid" }, new List<string>()));
                        updateBatch.Add(psAuthentication.Bind(false, currentResetToken, resetTimestamp));

                        psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("reset_email_token_by_user",
                            new List<string> { "user_id", "reset_timestamp" }, new List<string> { "is_valid" }, new List<string>()));
                        updateBatch.Add(psAuthentication.Bind(false, userId, resetTimestamp));
                    }
                }

                if (needNewToken)
                {
                    resetToken = UUIDGenerator.GenerateResetPasswordTokenForUser();

                    psAuthentication = session.Prepare(CQLGenerator.InsertStatement("reset_email_token",
                        new List<string> { "reset_token", "user_id", "company_id", "email", "reset_timestamp", "is_valid" }));
                    updateBatch.Add(psAuthentication.Bind(resetToken, userId, companyId, email, currentTime, true));

                    psAuthentication = session.Prepare(CQLGenerator.InsertStatement("reset_email_token_by_user",
                        new List<string> { "reset_token", "user_id", "company_id", "email", "reset_timestamp", "is_valid" }));
                    updateBatch.Add(psAuthentication.Bind(resetToken, userId, companyId, email, currentTime, true));
                }

                session.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }


            return resetToken;
        }

        public AuthenticationUpdateResponse ResetPasswordWithToken(string token)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();
            response.Success = false;

            ISession session = null;
            PreparedStatement psAuthentication = null;
            BatchStatement updateBatch = new BatchStatement();

            Dictionary<string, object> authenticationDict = new Dictionary<string, object>();
            try
            {
                ConnectionManager cm = new ConnectionManager();
                session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                psAuthentication = session.Prepare(CQLGenerator.SelectStatement("reset_email_token",
                               new List<string>(), new List<string> { "reset_token", "is_valid" }));
                Row resetTokenRow = session.Execute(psAuthentication.Bind(token, true)).FirstOrDefault();

                if (resetTokenRow == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserResetPasswordTokenInvalid);
                    response.ErrorMessage = ErrorMessage.UserResetPasswordTokenInvalid;
                    return response;
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                DateTimeOffset resetTimestamp = resetTokenRow.GetValue<DateTimeOffset>("reset_timestamp").ToUniversalTime();

                if (currentTime > resetTimestamp.AddHours(Convert.ToInt16(WebConfigurationManager.AppSettings["reset_password_expired_hours"])))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserResetPasswordTokenExpired);
                    response.ErrorMessage = ErrorMessage.UserResetPasswordTokenExpired;
                    return response;
                }

                string userId = resetTokenRow.GetValue<string>("user_id");
                string email = resetTokenRow.GetValue<string>("email");
                string companyId = resetTokenRow.GetValue<string>("company_id");

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                Row userRow = vh.ValidateUser(userId, companyId, session);
                if (userRow == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                psAuthentication = session.Prepare(CQLGenerator.SelectStatement("user_authentication",
                  new List<string>(), new List<string> { "email", "user_id" }));
                Row authenticationRow = session.Execute(psAuthentication.Bind(email, userId)).FirstOrDefault();

                if (authenticationRow == null)
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                string currentHashedPassword = authenticationRow.GetValue<string>("hashed_password");
                string currentRandomSalt = authenticationRow.GetValue<string>("salt");
                bool isDefault = authenticationRow.GetValue<bool>("is_default");

                authenticationDict.Add("hashedPassword", currentHashedPassword);
                authenticationDict.Add("randomSalt", currentRandomSalt);
                authenticationDict.Add("isDefault", isDefault);
                authenticationDict.Add("userId", userId);
                authenticationDict.Add("companyId", companyId);

                string newPassword = UUIDGenerator.GenerateNumberPasswordForUser();
                string randomSalt = Crypto.GenerateRandomSalt(newPassword.Length);
                string hashedPassword = Crypto.EncryptTextWithSalt(newPassword, randomSalt);

                psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("user_authentication",
                    new List<string> { "email", "user_id" }, new List<string> { "hashed_password", "salt", "is_default", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(psAuthentication.Bind(hashedPassword, randomSalt, true, userId, DateTime.UtcNow, email, userId));

                CompanySelectEmailDetailResponse emailDetail = new Company().SelectEmailTemplate(companyId, (int)Company.CompanyEmailTemplate.ResettedPassword, session, companyRow);

                EmailSendResponse sendResponse = EmailHandler.SendNewPasswordEmail(email, companyId, emailDetail.EmailTitle, emailDetail.EmailDescription, emailDetail.EmailSupportInfo, emailDetail.EmailLogoUrl, newPassword, session);
                if (!sendResponse.Success)
                {
                    currentHashedPassword = (string)authenticationDict["hashedPassword"];
                    currentRandomSalt = (string)authenticationDict["randomSalt"];
                    isDefault = (bool)authenticationDict["isDefault"];
                    userId = (string)authenticationDict["userId"];
                    companyId = (string)authenticationDict["companyId"];

                    // Reverting to previous password
                    psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("user_authentication",
                        new List<string> { "email", "user_id" }, new List<string> { "hashed_password", "salt", "is_default" }, new List<string>()));
                    session.Execute(psAuthentication.Bind(currentHashedPassword, currentRandomSalt, isDefault, email, userId));

                    response.ErrorCode = Convert.ToInt16(ErrorCode.SystemErrorSendEmailFailed);
                    response.ErrorMessage = ErrorMessage.SystemErrorSendEmailFailed;
                    return response;
                }

                psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("reset_email_token",
                   new List<string> { "reset_token", "reset_timestamp" }, new List<string> { "is_valid" }, new List<string>()));
                updateBatch.Add(psAuthentication.Bind(false, token, resetTimestamp));

                psAuthentication = session.Prepare(CQLGenerator.UpdateStatement("reset_email_token_by_user",
                    new List<string> { "user_id", "reset_timestamp" }, new List<string> { "is_valid" }, new List<string>()));
                updateBatch.Add(psAuthentication.Bind(false, userId, resetTimestamp));

                session.Execute(updateBatch);
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AuthenticationLogoutUserResponse LogoutUser(string userId, string companyId)
        {
            AuthenticationLogoutUserResponse response = new AuthenticationLogoutUserResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("user_authentication_token_by_user", new List<string>(), new List<string> { "user_id", "is_token_valid" }));
                RowSet authenticateRowSet = mainSession.Execute(ps.Bind(userId, true));

                DateTime currentTime = DateTime.UtcNow;
                foreach (Row authenticateRow in authenticateRowSet)
                {
                    string userToken = authenticateRow.GetValue<string>("user_token");
                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_authentication_token",
                           new List<string> { "user_token", "user_id" }, new List<string> { "is_token_valid", "last_modified_timestamp" }, new List<string>()));
                    batch.Add(ps.Bind(false, currentTime, userToken, userId));

                    ps = mainSession.Prepare(CQLGenerator.UpdateStatement("user_authentication_token_by_user",
                        new List<string> { "user_token", "user_id" }, new List<string> { "is_token_valid", "last_modified_timestamp" }, new List<string>()));
                    batch.Add(ps.Bind(false, currentTime, userToken, userId));
                }

                mainSession.Execute(batch);

                new AnalyticUserActivity().UpdateUserLogin(userId, companyId, false, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

    }
}