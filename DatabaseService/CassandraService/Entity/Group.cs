﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class Group
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum GroupQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember]
        public string GroupId { get; set; }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public List<User> Members { get; set; }

        public GroupCreateResponse CreateGroup(string requesterUserId, string companyId, string groupName, List<string> groupMemberIds)
        {
            GroupCreateResponse response = new GroupCreateResponse();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                if(string.IsNullOrEmpty(groupName))
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.GroupMissingTitle);
                    response.ErrorMessage = ErrorMessage.GroupMissingTitle;
                    return response;
                }

                if(groupMemberIds.Count == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.GroupMissingMember);
                    response.ErrorMessage = ErrorMessage.GroupMissingMember;
                    return response;
                }

                User userManager = new User();
                groupMemberIds.RemoveAll(m => m.Equals(requesterUserId));

                int count = 0;
                string groupId = UUIDGenerator.GenerateUniqueIDForGroup();
                foreach (string memberId in groupMemberIds)
                {
                    if(userManager.SelectUserBasic(memberId, companyId, true, mainSession).User != null)
                    {
                        count++;

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("group_member", new List<string> { "group_id", "user_id" }));
                        batch.Add(ps.Bind(groupId, memberId));
                    }
                }

                if(count == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.GroupMissingMember);
                    response.ErrorMessage = ErrorMessage.GroupMissingMember;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;
                ps = mainSession.Prepare(CQLGenerator.InsertStatement("group", new List<string> { "user_id", "id", "name", "created_on_timestamp", "last_modified_timestamp" }));
                batch.Add(ps.Bind(requesterUserId, groupId, groupName, currentTime, currentTime));

                mainSession.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupSelectResponse SelectGroup(string requesterUserId, string companyId, string groupId, int queryType, ISession mainSession = null, Row groupRow = null)
        {
            GroupSelectResponse response = new GroupSelectResponse();
            response.Group = null;
            response.Success = false;

            try
            {
                PreparedStatement ps = null;

                ValidationHandler vh = new ValidationHandler();

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();

                    Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                    if (userRow == null)
                    {
                        Log.Error("Invalid user");
                        response.ErrorCode = Convert.ToInt32(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }

                }

                if(groupRow == null)
                {
                    groupRow = vh.ValidateGroup(groupId, requesterUserId, mainSession);
                    if (groupRow == null)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.GroupInvalid);
                        response.ErrorMessage = ErrorMessage.GroupInvalid;
                        return response;
                    }
                }


                string groupName = groupRow.GetValue<string>("name");
                DateTime createdDate = groupRow.GetValue<DateTime>("created_on_timestamp");

                User userManager = new User();
                List<User> members = new List<User>();
                if (queryType == (int)GroupQueryType.FullDetail)
                {
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("group_member", new List<string>(), new List<string> { "group_id" }));
                    RowSet memberRowset = mainSession.Execute(ps.Bind(groupId));

                    foreach (Row memberRow in memberRowset)
                    {
                        string userId = memberRow.GetValue<string>("user_id");
                        User userFound = userManager.SelectUserBasic(userId, companyId, false, mainSession, null, true).User;
                        if (userFound != null)
                        {
                            members.Add(userFound);
                        }
                    }

                    members = members.OrderBy(m => m.FirstName).ThenBy(m => m.LastName).ToList();
                }

                Group group = new Group
                {
                    GroupId = groupId,
                    GroupName = groupName,
                    Members = members,
                    CreatedDate = createdDate
                };

                response.Group = group;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupSelectAllResponse SelectAllGroupsByUser(string requesterUserId, string companyId, ISession mainSession = null)
        {
            GroupSelectAllResponse response = new GroupSelectAllResponse();
            response.Groups = new List<Group>();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;

                ValidationHandler vh = new ValidationHandler();

                if(mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("group", new List<string>(), new List<string> { "user_id" }));
                RowSet groupRowset = mainSession.Execute(ps.Bind(requesterUserId));

                foreach(Row groupRow in groupRowset)
                {
                    string groupId = groupRow.GetValue<string>("id");
                    Group selectedGroup = SelectGroup(requesterUserId, companyId, groupId, (int)GroupQueryType.Basic, mainSession, groupRow).Group;
                    if(selectedGroup != null)
                    {
                        response.Groups.Add(selectedGroup);
                    }
                }

                response.Groups = response.Groups.OrderByDescending(g => g.CreatedDate).ToList();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupUpdateResponse DeleteGroup(string requesterUserId, string companyId, string groupId)
        {
            GroupUpdateResponse response = new GroupUpdateResponse();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                Row groupRow = vh.ValidateGroup(groupId, requesterUserId, mainSession);
                if (groupRow == null)
                {
                    Log.Error("Group already been deleted");
                    response.Success = true;
                    response.ErrorCode = Convert.ToInt16(ErrorCode.GroupAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.GroupAlreadyDeleted;
                    return response;
                }

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("group", new List<string> { "user_id", "id" }));
                batch.Add(ps.Bind(requesterUserId, groupId));

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("group_member", new List<string> { "group_id" }));
                batch.Add(ps.Bind(groupId));

                mainSession.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public GroupUpdateResponse UpdateGroup(string requesterUserId, string companyId, string groupId, string newGroupName, List<string> newGroupMemberIds)
        {
            GroupUpdateResponse response = new GroupUpdateResponse();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                Row groupRow = vh.ValidateGroup(groupId, requesterUserId, mainSession);
                if (groupRow == null)
                {
                    Log.Error("Group already been deleted");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.GroupInvalid);
                    response.ErrorMessage = ErrorMessage.GroupInvalid;
                    return response;
                }

                if (newGroupMemberIds.Count == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.GroupMissingMember);
                    response.ErrorMessage = ErrorMessage.GroupMissingMember;
                    return response;
                }

                User userManager = new User();
                newGroupMemberIds.RemoveAll(m => m.Equals(requesterUserId));

                int count = 0;
                foreach (string memberId in newGroupMemberIds)
                {
                    if (userManager.SelectUserBasic(memberId, companyId, true, mainSession).User != null)
                    {
                        count++;

                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("group_member", new List<string> { "group_id", "user_id" }));
                        updateBatch.Add(ps.Bind(groupId, memberId));
                    }
                }

                if (count == 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.GroupMissingMember);
                    response.ErrorMessage = ErrorMessage.GroupMissingMember;
                    return response;
                }

                DateTime currentTime = DateTime.UtcNow;

                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("group_member", new List<string> { "group_id" }));
                deleteBatch.Add(ps.Bind(groupId));

                ps = mainSession.Prepare(CQLGenerator.UpdateStatement("group", new List<string> { "user_id", "id" }, new List<string> { "name", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(newGroupName, currentTime, requesterUserId, groupId));

                mainSession.Execute(deleteBatch);
                mainSession.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
