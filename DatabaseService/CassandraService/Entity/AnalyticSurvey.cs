﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    public class AnalyticSurvey
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataContract]
        [Serializable]
        public class RSAnalyticOverview
        {
            [DataMember]
            public RSTopic Topic { get; set; }

            [DataMember]
            public RSSummary Summary { get; set; }

            [DataMember]
            public RSPersonnelCompletion PersonnelCompletion { get; set; }

            [DataMember]
            public RSResponderReport Report { get; set; }
        }


        [DataContract]
        [Serializable]
        public class RSSummary
        {
            [DataMember]
            public int TotalPages;

            [DataMember]
            public int TotalCards { get; set; }

            [DataMember]
            public int TotalDays { get; set; }

            [DataMember]
            public int AverageTimeCompletionInSecond { get; set; }


            [DataMember]
            public string AverageTimeCompletionString { get; set; }
        }

        [DataContract]
        [Serializable]
        public class RSPersonnelCompletion
        {
            [DataMember]
            public int CompletionNumber;

            [DataMember]
            public int CompletionPercentage { get; set; }

            [DataMember]
            public int IncompletionNumber;

            [DataMember]
            public int IncompletionPercentage { get; set; }

            [DataMember]
            public int AbsentNumber;

            [DataMember]
            public int AbsentPercentage { get; set; }

            [DataMember]
            public int TotalTimeTakenInSecond { get; set; }
        }


        [DataContract]
        [Serializable]
        public class RSResponderReport
        {
            [DataMember]
            public List<RSReportPerDay> ReportList;

            [DataMember]
            public int TotalCompletionPercentage;

            [DataMember]
            public int TotalCompletion;

            [DataMember]
            public int TotalBounces;

            [DataMember]
            public int TotalTargetedAudience;
        }


        [DataContract]
        [Serializable]
        public class RSReportPerDay
        {
            [DataMember]
            public DateTime Datestamp;

            [DataMember]
            public string DatestampString;

            [DataMember]
            public int CompletionNumber { get; set; }

            [DataMember]
            public int BounceNumber;
        }


        [DataContract]
        [Serializable]
        public class RSReportPerUser
        {
            [DataMember]
            public User User;

            [DataMember]
            public DateTime? LastUpdateTimestamp;

            [DataMember]
            public string LastUpdateTimestampString;

            [DataMember]
            public bool IsCompleted;
        }

        public int SelectCompletedNumberOfUsersForRSTopic(string topicId, ISession analyticSession)
        {
            int numberOfUsers = 0;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_completion_by_topic_timestamp", new List<string> { "topic_id" }));
                numberOfUsers = (int)analyticSession.Execute(ps.Bind(topicId)).FirstOrDefault().GetValue<long>("count");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return numberOfUsers;
        }

        public bool CheckRSCompleted(string topicId, string requesterUserId, ISession analyticSession)
        {
            bool isCompleted = false;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_completion_by_user_timestamp", new List<string>(), new List<string> { "completed_by_user_id", "topic_id" }));
                Row completedRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId)).FirstOrDefault();

                if (completedRow != null)
                {
                    isCompleted = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isCompleted;
        }

        public void AnswerRSCard(string answeredByUserId,
                                 string topicId,
                                 string cardId,
                                 int cardType,
                                 bool hasCustomAnswer,
                                 bool hasTextAnswer,
                                 List<string> optionIds,
                                 RSOption newCustomAnswer,
                                 RSOption previousCustomAnswer,
                                 int rangeSelected,
                                 DateTime currentTime,
                                 bool isSkipped,
                                 ISession analyticSession)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Check if number range
                if (cardType == (int)RSCard.RSCardType.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_range_answered_by_user",
                       new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                    Row rangeRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                    if (rangeRow != null)
                    {
                        int previousRangeSelected = rangeRow.GetValue<int>("range_value");

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_range_answered_by_user", new List<string> { "card_id", "range_value", "answered_by_user_id" }));
                        deleteBatch.Add(ps.Bind(cardId, previousRangeSelected, answeredByUserId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_range_answered_by_card", new List<string> { "card_id", "range_value", "answered_by_user_id" }));
                        deleteBatch.Add(ps.Bind(cardId, previousRangeSelected, answeredByUserId));
                    }

                    if (!isSkipped)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_range_answered_by_user", new List<string> { "card_id", "range_value", "answered_by_user_id", "answered_on_timestamp" }));
                        updateBatch.Add(ps.Bind(cardId, rangeSelected, answeredByUserId, currentTime));
                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_range_answered_by_card", new List<string> { "card_id", "range_value", "answered_by_user_id", "answered_on_timestamp" }));
                        updateBatch.Add(ps.Bind(cardId, rangeSelected, answeredByUserId, currentTime));
                    }
                }
                else
                {
                    // Text, Single/Mutiple option, Single option with custom answer

                    // isCustom = text, Single option with custom answer
                    if (hasTextAnswer)
                    {
                        if (hasCustomAnswer)
                        {
                            // Only Single option has custom answer
                            // Remove previously answered options
                            RSOptionRemovePreviouslyOptionAnswerResponse optionAnswerResponse = RemoveOptionAnswer(analyticSession, answeredByUserId, cardId);
                            if (optionAnswerResponse.Success)
                            {
                                foreach (BoundStatement bs in optionAnswerResponse.deleteStatements)
                                {
                                    deleteBatch.Add(bs);
                                }

                                foreach (BoundStatement bs in optionAnswerResponse.updateStatements)
                                {
                                    updateBatch.Add(bs);
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(previousCustomAnswer.OptionId))
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_card",
                                new List<string>(), new List<string> { "card_id", "option_id", "created_by_user_id" }));
                            Row previousCustomRow = analyticSession.Execute(ps.Bind(cardId, previousCustomAnswer.OptionId, answeredByUserId)).FirstOrDefault();

                            if (previousCustomRow != null)
                            {
                                // Remove previously answer custom answer
                                RemoveCustomAnswer(analyticSession, answeredByUserId, cardId, previousCustomAnswer);

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_user", new List<string> { "created_by_user_id", "card_id", "option_id" }));
                                deleteBatch.Add(ps.Bind(answeredByUserId, cardId, previousCustomAnswer.OptionId));

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_card", new List<string> { "created_by_user_id", "card_id", "option_id" }));
                                deleteBatch.Add(ps.Bind(answeredByUserId, cardId, previousCustomAnswer.OptionId));

                            }
                        }


                        if (!isSkipped)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_created_by_user", new List<string> { "card_id", "option_id", "created_by_user_id", "created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, newCustomAnswer.OptionId, answeredByUserId, currentTime));
                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_created_by_card", new List<string> { "card_id", "option_id", "created_by_user_id", "created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, newCustomAnswer.OptionId, answeredByUserId, currentTime));

                            string lowerCustomAnswer = newCustomAnswer.Content.ToLower().Trim();
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer", new List<string>(), new List<string> { "custom_answer", "card_id" }));
                            Row similarRow = analyticSession.Execute(ps.Bind(lowerCustomAnswer, cardId)).FirstOrDefault();

                            string similarId = string.Empty;
                            int number = 0;
                            if (similarRow != null)
                            {
                                similarId = similarRow.GetValue<string>("similar_id");
                                DateTime lastUpdatedTime = similarRow.GetValue<DateTime>("created_on_timestamp");
                                number = similarRow.GetValue<int>("number");

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                                deleteBatch.Add(ps.Bind(cardId, number, lastUpdatedTime, lowerCustomAnswer));

                                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_custom_option_by_answer", new List<string> { "custom_answer", "card_id" }, new List<string> { "number", "created_on_timestamp" }, new List<string>()));
                                updateBatch.Add(ps.Bind(number + 1, currentTime, lowerCustomAnswer, cardId));
                            }
                            else
                            {
                                similarId = UUIDGenerator.GenerateUniqueIDForRSSimilar();

                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_by_answer", new List<string> { "similar_id", "card_id", "custom_answer", "number", "created_on_timestamp" }));
                                updateBatch.Add(ps.Bind(similarId, cardId, lowerCustomAnswer, number + 1, currentTime));
                            }

                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                            updateBatch.Add(ps.Bind(cardId, number + 1, currentTime, lowerCustomAnswer));

                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_by_similar", new List<string> { "similar_id", "option_id", "created_by_user_id", "created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(similarId, newCustomAnswer.OptionId, answeredByUserId, currentTime));
                        }

                    }

                    //Single/Mutiple option
                    else
                    {
                        // Delete previously selected options
                        RSOptionRemovePreviouslyOptionAnswerResponse optionAnswerResponse = RemoveOptionAnswer(analyticSession, answeredByUserId, cardId);
                        if (optionAnswerResponse.Success)
                        {
                            foreach (BoundStatement bs in optionAnswerResponse.deleteStatements)
                            {
                                deleteBatch.Add(bs);
                            }

                            foreach (BoundStatement bs in optionAnswerResponse.updateStatements)
                            {
                                updateBatch.Add(bs);
                            }
                        }

                        // Delete previously custom answer
                        if (!string.IsNullOrEmpty(previousCustomAnswer.Content))
                        {
                            RSOptionRemovePreviouslyCustomAnswerResponse customAnswerResponse = RemoveCustomAnswer(analyticSession, answeredByUserId, cardId, previousCustomAnswer);
                            if (customAnswerResponse.Success)
                            {
                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_user", new List<string> { "created_by_user_id", "card_id" }));
                                deleteBatch.Add(ps.Bind(answeredByUserId, cardId));

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_card", new List<string> { "created_by_user_id", "card_id", "option_id" }));
                                deleteBatch.Add(ps.Bind(answeredByUserId, cardId, previousCustomAnswer.OptionId));
                            }
                        }

                        if (!isSkipped)
                        {
                            foreach (string optionId in optionIds)
                            {
                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_option_answered_by_user", new List<string> { "card_id", "option_id", "answered_by_user_id", "answered_on_timestamp" }));
                                updateBatch.Add(ps.Bind(cardId, optionId, answeredByUserId, currentTime));
                                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_option_answered_by_card", new List<string> { "card_id", "option_id", "answered_by_user_id", "answered_on_timestamp" }));
                                updateBatch.Add(ps.Bind(cardId, optionId, answeredByUserId, currentTime));
                            }
                        }
                    }
                }

                // Update last updated time
                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id" }, new List<string> { "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(currentTime, answeredByUserId, topicId));
                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id" }, new List<string> { "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(currentTime, answeredByUserId, topicId));

                analyticSession.Execute(deleteBatch);
                analyticSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public RSOptionRemovePreviouslyOptionAnswerResponse RemoveOptionAnswer(ISession analyticSession, string answeredByUserId, string cardId)
        {
            RSOptionRemovePreviouslyOptionAnswerResponse response = new RSOptionRemovePreviouslyOptionAnswerResponse();
            response.deleteStatements = new List<BoundStatement>();
            response.updateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                // Delete previously selected options
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                RowSet optionAnsweredRowset = analyticSession.Execute(ps.Bind(answeredByUserId, cardId));

                foreach (Row optionAnsweredRow in optionAnsweredRowset)
                {
                    string optionId = optionAnsweredRow.GetValue<string>("option_id");
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_option_answered_by_card", new List<string> { "answered_by_user_id", "card_id", "option_id" }));
                    response.deleteStatements.Add(ps.Bind(answeredByUserId, cardId, optionId));
                }

                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_option_answered_by_user", new List<string> { "answered_by_user_id", "card_id" }));
                response.deleteStatements.Add(ps.Bind(answeredByUserId, cardId));
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public RSOptionRemovePreviouslyCustomAnswerResponse RemoveCustomAnswer(ISession analyticSession, string answeredByUserId, string cardId, RSOption previousCustomAnswer)
        {
            RSOptionRemovePreviouslyCustomAnswerResponse response = new RSOptionRemovePreviouslyCustomAnswerResponse();
            response.deleteStatements = new List<BoundStatement>();
            response.updateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                // Delete custom answer
                string previousCustomAnswerContent = previousCustomAnswer.Content.ToLower().Trim();
                DateTime previousCustomAnswerCreatedTimestamp = previousCustomAnswer.CreatedOnTimestamp;

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer",
                    new List<string>(), new List<string> { "custom_answer", "card_id" }));
                Row answerRow = analyticSession.Execute(ps.Bind(previousCustomAnswerContent, cardId)).FirstOrDefault();

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                if (answerRow != null)
                {
                    string similiarId = answerRow.GetValue<string>("similar_id");
                    int numberOfSimiliar = answerRow.GetValue<int>("number");
                    DateTime createdDate = answerRow.GetValue<DateTime>("created_on_timestamp");

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                    deleteBatch.Add(ps.Bind(cardId, numberOfSimiliar, createdDate, previousCustomAnswerContent));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_by_similar", new List<string> { "similar_id", "created_on_timestamp", "created_by_user_id" }));
                    deleteBatch.Add(ps.Bind(similiarId, previousCustomAnswerCreatedTimestamp, answeredByUserId));

                    if (numberOfSimiliar > 1)
                    {
                        numberOfSimiliar--;
                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_custom_option_by_answer",
                            new List<string> { "custom_answer", "card_id" }, new List<string> { "number", "created_on_timestamp" }, new List<string>()));
                        updateBatch.Add(ps.Bind(numberOfSimiliar, createdDate, previousCustomAnswerContent, cardId));

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                        updateBatch.Add(ps.Bind(cardId, numberOfSimiliar, createdDate, previousCustomAnswerContent));
                    }
                    else if (numberOfSimiliar == 1)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_by_answer", new List<string> { "card_id", "custom_answer" }));
                        deleteBatch.Add(ps.Bind(cardId, previousCustomAnswerContent));
                    }

                    // Need to execute delete first
                    analyticSession.Execute(deleteBatch);
                    analyticSession.Execute(updateBatch);

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateRSBounceActivity(string requesterUserId, string topicId, ISession analyticSession)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                DateTime bouncedDatestamp = DateTime.UtcNow.Date;

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_bounced_by_user_datestamp", new List<string>(), new List<string> { "bounced_by_user_id", "topic_id", "bounced_on_datestamp" }));
                Row bounceRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId, bouncedDatestamp)).FirstOrDefault();

                if (bounceRow == null)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_bounced_by_user_datestamp", new List<string> { "bounced_by_user_id", "topic_id", "bounced_on_datestamp" }));
                    analyticSession.Execute(ps.Bind(requesterUserId, topicId, bouncedDatestamp));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_bounced_by_topic_datestamp", new List<string> { "bounced_by_user_id", "topic_id", "bounced_on_datestamp" }));
                    analyticSession.Execute(ps.Bind(requesterUserId, topicId, bouncedDatestamp));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateRSCompletionActivity(string requesterUserId, string topicId, List<RSCard> cards, ISession analyticSession)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                DateTime currentTime = DateTime.UtcNow;
                BatchStatement updateBatch = new BatchStatement();
                // Last resort delete and update to see if duplicate completion really happens
                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_completion_by_user_timestamp", new List<string>(), new List<string> { "completed_by_user_id", "topic_id" }));
                Row completionRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId)).FirstOrDefault();

                bool hasCompleted = true;
                if (completionRow == null)
                {
                    // Check if user really completed the survey
                    // Use logic based algorithm
                    for (int index = 0; index < cards.Count; index++)
                    {
                        RSCard card = cards[index];
                        string cardId = card.CardId;
                        int cardType = card.Type;
                        if (cardType != (int)RSCard.RSCardType.Instructional)
                        {
                            if (cardType == (int)RSCard.RSCardType.MultiChoice || cardType == (int)RSCard.RSCardType.SelectOne || cardType == (int)RSCard.RSCardType.DropList)
                            {
                                bool hasCustom = card.HasCustomAnswer;

                                // Check option
                                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                                Row optionRow = analyticSession.Execute(ps.Bind(requesterUserId, cardId)).FirstOrDefault();

                                if (optionRow == null)
                                {
                                    if (hasCustom)
                                    {
                                        // Check custom
                                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "card_id" }));
                                        optionRow = analyticSession.Execute(ps.Bind(requesterUserId, cardId)).FirstOrDefault();

                                        if (optionRow == null)
                                        {
                                            if (card.ToBeSkipped)
                                            {
                                                continue;
                                            }
                                            hasCompleted = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (card.ToBeSkipped)
                                        {
                                            continue;
                                        }
                                        hasCompleted = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    string optionId = optionRow.GetValue<string>("option_id");

                                    // Check for logic
                                    RSOption selectedOption = card.Options.FirstOrDefault(option => option.OptionId == optionId);
                                    if (selectedOption != null)
                                    {
                                        if (!string.IsNullOrEmpty(selectedOption.NextCardId))
                                        {
                                            if (selectedOption.NextCardId == DefaultResource.RSEndCardId)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                index = selectedOption.NextCard.Ordering - 2;
                                            }

                                        }
                                    }
                                }
                            }
                            else if (cardType == (int)RSCard.RSCardType.NumberRange)
                            {
                                // Check range
                                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_range_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                                Row rangeRow = analyticSession.Execute(ps.Bind(requesterUserId, cardId)).FirstOrDefault();

                                if (rangeRow == null)
                                {
                                    if (card.ToBeSkipped)
                                    {
                                        continue;
                                    }
                                    hasCompleted = false;
                                    break;
                                }
                            }
                            else if (cardType == (int)RSCard.RSCardType.Text)
                            {
                                // Check custom
                                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "card_id" }));
                                Row customOptionRow = analyticSession.Execute(ps.Bind(requesterUserId, cardId)).FirstOrDefault();

                                if (customOptionRow == null)
                                {
                                    if (card.ToBeSkipped)
                                    {
                                        continue;
                                    }
                                    hasCompleted = false;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (hasCompleted)
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id" }, new List<string> { "completed_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, currentTime, requesterUserId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id" }, new List<string> { "completed_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, currentTime, requesterUserId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_completion_by_topic_timestamp", new List<string> { "topic_id", "completed_on_timestamp", "completed_by_user_id", "completed_on_datestamp" }));
                    updateBatch.Add(ps.Bind(topicId, currentTime, requesterUserId, currentTime.Date));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_completion_by_user_timestamp", new List<string> { "completed_by_user_id", "completed_on_timestamp", "topic_id", "completed_on_datestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, currentTime, topicId, currentTime.Date));

                    analyticSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void UpdateRSStartProgressByUser(string requesterUserId, string topicId, ISession analyticSession)
        {
            try
            {
                BatchStatement batch = new BatchStatement();

                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_progress_by_user", new List<string>(), new List<string> { "user_id", "topic_id" }));
                Row completionRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId)).FirstOrDefault();

                if (completionRow != null)
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id" }, new List<string> { "is_omitted" }, new List<string>()));
                    batch.Add(ps.Bind(true, requesterUserId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id" }, new List<string> { "is_omitted" }, new List<string>()));
                    batch.Add(ps.Bind(true, requesterUserId, topicId));
                }
                else
                {
                    DateTime currentTime = DateTime.UtcNow;
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id", "start_on_timestamp", "is_omitted" }));
                    batch.Add(ps.Bind(requesterUserId, topicId, currentTime, false));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id", "start_on_timestamp", "is_omitted" }));
                    batch.Add(ps.Bind(requesterUserId, topicId, currentTime, false));
                }

                analyticSession.Execute(batch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public int SelectRangeFromCard(string cardId, string answeredByUserId, ISession analyticSession)
        {
            int selectedRange = Convert.ToInt32(DefaultResource.RSCardRangeNotSelected);

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_range_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                Row rangeRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                if (rangeRow != null)
                {
                    selectedRange = rangeRow.GetValue<int>("range_value");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return selectedRange;
        }

        public RSOption SelectCustomAnswerFromCardByUser(string topicId, string cardId, string answeredByUserId, ISession mainSession, ISession analyticSession)
        {
            RSOption customAnswer = new RSOption();

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "card_id" }));
                Row customRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                if (customRow != null)
                {
                    string optionId = customRow.GetValue<string>("option_id");
                    customAnswer = new RSOption().SelectCustomOption(topicId, cardId, optionId, mainSession);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return customAnswer;
        }

        public bool CheckForSelectedOption(string cardId, string optionId, string answeredByUserId, ISession analyticSession)
        {
            bool isSelected = false;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id", "option_id" }));
                Row selectedRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId, optionId)).FirstOrDefault();

                if (selectedRow != null)
                {
                    isSelected = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isSelected;
        }

        public RSAnalyticOverview SelectRSResultOverview(string topicId, int totalPages, int totalCards, int totalTargetedAudience, double timezoneOffset, ISession analyticSession)
        {
            RSAnalyticOverview overview = new RSAnalyticOverview();
            overview.Summary = new RSSummary();
            overview.PersonnelCompletion = new RSPersonnelCompletion();
            overview.Report = new RSResponderReport();
            overview.Report.ReportList = new List<RSReportPerDay>();

            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = null;
                int limit = 14;
                DateTime baseTimestamp = DateTime.UtcNow.AddDays(-(limit - 1)).Date;

                // Get number of completion, bounce per day
                for (int day = 0; day < limit; day++)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("rs_completion_by_topic_timestamp", new List<string>(), new List<string> { "topic_id" }, "completed_on_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "completed_on_timestamp", CQLGenerator.Comparison.LessThan, 0));
                    RowSet completionRowset = analyticSession.Execute(ps.Bind(topicId, baseTimestamp, baseTimestamp.AddDays(1)));

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("rs_bounced_by_topic_datestamp", new List<string>(), new List<string> { "topic_id" }, "bounced_on_datestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "bounced_on_datestamp", CQLGenerator.Comparison.LessThan, 0));
                    RowSet bounceRowset = analyticSession.Execute(ps.Bind(topicId, baseTimestamp, baseTimestamp.AddDays(1)));

                    int bounceNumber = bounceRowset.Count();
                    int completionNumber = completionRowset.Count();

                    RSReportPerDay report = new RSReportPerDay
                    {
                        BounceNumber = bounceNumber,
                        CompletionNumber = completionNumber,
                        Datestamp = baseTimestamp.AddHours(timezoneOffset),
                        DatestampString = baseTimestamp.AddHours(timezoneOffset).ToString("dd MMM yyyy")
                    };

                    overview.Report.ReportList.Add(report);
                    overview.Report.TotalTargetedAudience = totalTargetedAudience;
                    baseTimestamp = baseTimestamp.AddDays(1);
                }

                // Get total bounces
                ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_bounced_by_topic_datestamp",
                           new List<string> { "topic_id" }));
                Row countRow = analyticSession.Execute(ps.Bind(topicId)).FirstOrDefault();
                int count = (int)countRow.GetValue<long>("count");
                overview.Report.TotalBounces = count;

                // Get Personnel completion
                overview.PersonnelCompletion = SelectPersonnelCompletion(topicId, totalTargetedAudience, analyticSession);

                // Get data for summary
                int totalSeconds = overview.PersonnelCompletion.CompletionNumber == 0 ? 0 : overview.PersonnelCompletion.TotalTimeTakenInSecond / overview.PersonnelCompletion.CompletionNumber;
                int hour = totalSeconds / (60 * 60);
                int min = (totalSeconds - (hour * (60 * 60))) / 60;
                int sec = totalSeconds - (hour * (60 * 60)) - (min * 60);
                string totalSecondString = string.Empty;

                if (hour > 0)
                {
                    totalSecondString += hour + " hr ";
                    min = totalSeconds - (hour * (60 * 60));
                }
                if (min > 0)
                {
                    totalSecondString += min + " min ";
                    sec = totalSeconds - (min * 60) - (hour * (60 * 60));
                }

                totalSecondString += sec + " sec ";

                overview.Summary = new RSSummary
                {
                    TotalCards = totalCards,
                    TotalPages = totalPages,
                    AverageTimeCompletionInSecond = totalSeconds,
                    AverageTimeCompletionString = totalSecondString
                };

                overview.Report.TotalCompletion = overview.PersonnelCompletion.CompletionNumber;
                overview.Report.TotalCompletionPercentage = overview.PersonnelCompletion.CompletionPercentage;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return overview;
        }

        private RSPersonnelCompletion SelectPersonnelCompletion(string topicId, int totalTargetedAudience, ISession analyticSession)
        {
            RSPersonnelCompletion personnelCompletion = new RSPersonnelCompletion();

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_completion_by_topic_timestamp", new List<string> { "topic_id" }));
                Row totalCompletionRow = analyticSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_progress_by_topic", new List<string>(), new List<string> { "topic_id" }));
                RowSet progressRowset = analyticSession.Execute(ps.Bind(topicId));

                int totalIncompletion = 0;
                int totalTimeTaken = 0;

                foreach (Row progressRow in progressRowset)
                {
                    if (progressRow["completed_on_timestamp"] == null)
                    {
                        totalIncompletion++;
                    }
                    else
                    {
                        bool isOmitted = progressRow.GetValue<bool>("is_omitted");

                        if (!isOmitted)
                        {
                            DateTimeOffset completedTimestamp = progressRow.GetValue<DateTimeOffset>("completed_on_timestamp");
                            DateTimeOffset startTimestamp = progressRow.GetValue<DateTimeOffset>("start_on_timestamp");

                            totalTimeTaken += Math.Abs((int)completedTimestamp.Subtract(startTimestamp).TotalSeconds);
                        }

                    }
                }

                int totalCompletion = (int)totalCompletionRow.GetValue<long>("count");
                int totalAbsent = totalTargetedAudience - totalCompletion - totalIncompletion;

                int percentageCompletion = (int)((double)totalCompletion / totalTargetedAudience * 100);
                int percentageIncompletion = (int)((double)totalIncompletion / totalTargetedAudience * 100);
                int percentageAbsent = 100 - percentageCompletion - percentageIncompletion;

                personnelCompletion = new RSPersonnelCompletion
                {
                    CompletionNumber = totalCompletion,
                    IncompletionNumber = totalIncompletion,
                    AbsentNumber = totalAbsent,

                    CompletionPercentage = percentageCompletion,
                    IncompletionPercentage = percentageIncompletion,
                    AbsentPercentage = percentageAbsent,

                    TotalTimeTakenInSecond = totalTimeTaken
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return personnelCompletion;
        }

        public void SelectRSCardResult(RSCard card, ISession analyticSession, ISession mainSession)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;

                int totalResponses = 0;

                if (card.Type == (int)RSCard.RSCardType.NumberRange)
                {
                    List<RSOption> rangeOptions = new List<RSOption>();

                    // Get total responses
                    ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_range_answered_by_card",
                            new List<string> { "card_id" }));
                    Row totalCountRow = analyticSession.Execute(ps.Bind(card.CardId)).FirstOrDefault();
                    totalResponses = (int)totalCountRow.GetValue<long>("count");

                    // Convert range to option
                    // Get count per range
                    for (int index = card.MaxRange; index >= card.MinRange; index--)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_range_answered_by_card",
                            new List<string> { "card_id", "range_value" }));
                        Row countRow = analyticSession.Execute(ps.Bind(card.CardId, index)).FirstOrDefault();
                        int count = (int)countRow.GetValue<long>("count");
                        RSOption option = new RSOption
                        {
                            OptionId = index.ToString(),
                            Content = index.ToString(),
                            NumberOfSelection = count,
                            PercentageOfSelection = totalResponses == 0 ? 0 : (int)((double)count / totalResponses * 100)
                        };

                        rangeOptions.Add(option);
                    }

                    card.TotalResponses = totalResponses;
                    card.Options = rangeOptions;
                }
                else
                {
                    if (card.Type == (int)RSCard.RSCardType.MultiChoice || card.Type == (int)RSCard.RSCardType.SelectOne || card.Type == (int)RSCard.RSCardType.DropList)
                    {
                        // Total responders (must be unique)
                        // Get total responses
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_option_answered_by_card",
                                new List<string> { "card_id" }));
                        Row totalCountRow = analyticSession.Execute(ps.Bind(card.CardId)).FirstOrDefault();
                        totalResponses = (int)totalCountRow.GetValue<long>("count");

                        // Get count per option
                        List<string> answeredByUserIds = new List<string>();

                        foreach (RSOption option in card.Options)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_card",
                                new List<string>(), new List<string> { "card_id", "option_id" }));
                            RowSet answerRowset = analyticSession.Execute(ps.Bind(card.CardId, option.OptionId));

                            int count = 0;
                            foreach (Row answerRow in answerRowset)
                            {
                                string answerUserId = answerRow.GetValue<string>("answered_by_user_id");
                                if (!answeredByUserIds.Contains(answerUserId))
                                {
                                    answeredByUserIds.Add(answerUserId);
                                }
                                count++;
                            }

                            option.NumberOfSelection = count;
                            option.PercentageOfSelection = totalResponses == 0 ? 0 : (int)((double)count / totalResponses * 100);
                        }

                        //foreach (RSOption option in card.Options)
                        //{
                        //    ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_option_answered_by_card",
                        //        new List<string> { "card_id", "option_id" }));
                        //    Row countRow = analyticSession.Execute(ps.Bind(card.CardId, option.OptionId)).FirstOrDefault();
                        //    int count = (int)countRow.GetValue<long>("count");
                        //    option.NumberOfSelection = count;
                        //    option.PercentageOfSelection = totalResponses == 0 ? 0 : (int)((double)count / totalResponses * 100);
                        //}

                        card.TotalResponses = answeredByUserIds.Count();
                        card.Options = card.Options.OrderByDescending(option => option.NumberOfSelection).ToList();

                        if (card.HasCustomAnswer)
                        {
                            // Get count of custom answers
                            ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_custom_option_created_by_card", new List<string> { "card_id" }));
                            card.TotalResponses += (int)mainSession.Execute(ps.Bind(card.CardId)).FirstOrDefault().GetValue<long>("count");

                            // Take TOP 6 smiliar and then by timestamp
                            SelectSortedCustomAnswer(card, analyticSession);
                        }
                    }
                    else if (card.Type == (int)RSCard.RSCardType.Text)
                    {
                        // Get count of custom answers
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_custom_option_created_by_card", new List<string> { "card_id" }));
                        card.TotalResponses = (int)mainSession.Execute(ps.Bind(card.CardId)).FirstOrDefault().GetValue<long>("count");

                        // Take TOP 6 smiliar and then by timestamp
                        SelectSortedCustomAnswer(card, analyticSession);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectRSOptionResult(string companyId, string cardId, int cardType, string optionId, AnalyticSelectRSOptionResultResponse response, ISession analyticSession, ISession mainSession)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;
                RowSet optionResultRowset = null;
                if (cardType != (int)RSCard.RSCardType.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_card", new List<string>(), new List<string> { "card_id", "option_id" }));
                    optionResultRowset = analyticSession.Execute(ps.Bind(cardId, optionId));
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_range_answered_by_card", new List<string>(), new List<string> { "card_id", "range_value" }));
                    optionResultRowset = analyticSession.Execute(ps.Bind(cardId, Convert.ToInt32(optionId)));
                }

                User userManager = new User();
                foreach (Row optionResultRow in optionResultRowset)
                {
                    string answeredByUserId = optionResultRow.GetValue<string>("answered_by_user_id");
                    User answeredUser = userManager.SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User;

                    if (answeredUser != null)
                    {
                        Department deletedDepartment = new Department { Title = "Deleted", Id = "DeletedDepartmentId" };

                        if (answeredUser.Departments == null || answeredUser.Departments.Count == 0)
                        {
                            deletedDepartment.CountOfUsers++;
                            answeredUser.Departments.Add(deletedDepartment);

                            if (!response.Departments.Any(d => d.Id == deletedDepartment.Id))
                            {
                                response.Departments.Add(deletedDepartment);
                            }
                        }
                        else
                        {
                            if (response.Departments.Any(d => d.Id == answeredUser.Departments[0].Id))
                            {
                                Department department = response.Departments.FirstOrDefault(d => d.Id == answeredUser.Departments[0].Id);
                                department.CountOfUsers++;
                            }
                            else
                            {
                                Department department = new Department
                                {
                                    Id = answeredUser.Departments[0].Id,
                                    CountOfUsers = 1,
                                    Title = answeredUser.Departments[0].Title
                                };
                                response.Departments.Add(department);
                            }
                        }

                        response.Responders.Add(answeredUser);
                    }

                }

                response.Departments = response.Departments.OrderByDescending(department => department.CountOfUsers).ToList();
                response.Responders = response.Responders.OrderBy(user => user.FirstName).ToList();
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectRSCardResultForExport(string companyId, bool isAnonymous, RSCard card, ISession analyticSession, ISession mainSession)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;

                int totalResponses = 0;

                AnalyticSelectRSOptionResultResponse optionResponse = new AnalyticSelectRSOptionResultResponse();
                optionResponse.Responders = new List<User>();
                optionResponse.Departments = new List<Department>();

                if (card.Type == (int)RSCard.RSCardType.NumberRange)
                {
                    List<RSOption> rangeOptions = new List<RSOption>();

                    // Get total responses
                    ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_range_answered_by_card",
                            new List<string> { "card_id" }));
                    Row totalCountRow = analyticSession.Execute(ps.Bind(card.CardId)).FirstOrDefault();
                    totalResponses = (int)totalCountRow.GetValue<long>("count");

                    // Convert range to option
                    // Get count per range
                    for (int index = card.MaxRange; index >= card.MinRange; index--)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_range_answered_by_card",
                            new List<string> { "card_id", "range_value" }));
                        Row countRow = analyticSession.Execute(ps.Bind(card.CardId, index)).FirstOrDefault();
                        int count = (int)countRow.GetValue<long>("count");

                        SelectRSOptionResult(companyId, card.CardId, card.Type, index.ToString(), optionResponse, analyticSession, mainSession);

                        RSOption option = new RSOption
                        {
                            OptionId = index.ToString(),
                            Content = index.ToString(),
                            NumberOfSelection = count,
                            PercentageOfSelection = totalResponses == 0 ? 0 : (int)((double)count / totalResponses * 100),
                            UserResponders = optionResponse.Responders,
                            DepartmentResponders = optionResponse.Departments
                        };

                        rangeOptions.Add(option);
                    }

                    card.TotalResponses = totalResponses;
                    card.Options = rangeOptions;
                }
                else
                {
                    if (card.Type == (int)RSCard.RSCardType.MultiChoice || card.Type == (int)RSCard.RSCardType.SelectOne || card.Type == (int)RSCard.RSCardType.DropList)
                    {
                        // Total responders (must be unique)
                        // Get total responses
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_option_answered_by_card",
                                new List<string> { "card_id" }));
                        Row totalCountRow = analyticSession.Execute(ps.Bind(card.CardId)).FirstOrDefault();
                        totalResponses = (int)totalCountRow.GetValue<long>("count");

                        // Get count per option
                        List<string> answeredByUserIds = new List<string>();

                        foreach (RSOption option in card.Options)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_card",
                                new List<string>(), new List<string> { "card_id", "option_id" }));
                            RowSet answerRowset = analyticSession.Execute(ps.Bind(card.CardId, option.OptionId));

                            int count = 0;
                            foreach (Row answerRow in answerRowset)
                            {
                                string answerUserId = answerRow.GetValue<string>("answered_by_user_id");
                                if (!answeredByUserIds.Contains(answerUserId))
                                {
                                    answeredByUserIds.Add(answerUserId);
                                }
                                count++;
                            }

                            option.NumberOfSelection = count;
                            option.PercentageOfSelection = totalResponses == 0 ? 0 : (int)((double)count / totalResponses * 100);

                            SelectRSOptionResult(companyId, card.CardId, card.Type, option.OptionId, optionResponse, analyticSession, mainSession);
                            option.DepartmentResponders = optionResponse.Departments;
                            option.UserResponders = optionResponse.Responders;
                        }

                        card.TotalResponses = answeredByUserIds.Count();
                        card.Options = card.Options.OrderByDescending(option => option.NumberOfSelection).ToList();

                        if (card.HasCustomAnswer)
                        {
                            // Get count of custom answers
                            ps = mainSession.Prepare(CQLGenerator.CountStatement("rs_custom_option",
                                new List<string> { "card_id" }));
                            card.TotalResponses += (int)mainSession.Execute(ps.Bind(card.CardId)).FirstOrDefault().GetValue<long>("count");

                            SelectRSCustomAnswersResultFromCard(card, isAnonymous, companyId, analyticSession, mainSession);
                        }
                    }
                    else if (card.Type == (int)RSCard.RSCardType.Text)
                    {
                        // Get count of custom answers
                        ps = mainSession.Prepare(CQLGenerator.CountStatement("rs_custom_option",
                                new List<string> { "card_id" }));
                        card.TotalResponses = (int)mainSession.Execute(ps.Bind(card.CardId)).FirstOrDefault().GetValue<long>("count");

                        SelectRSCustomAnswersResultFromCard(card, isAnonymous, companyId, analyticSession, mainSession);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void SelectSortedCustomAnswer(RSCard card, ISession analyticSession)
        {
            try
            {
                int limit = 6;

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("rs_custom_option_sort_by_number_timestamp",
                            new List<string>(), new List<string> { "card_id" }, limit));
                RowSet customOptionSortRowset = analyticSession.Execute(ps.Bind(card.CardId));

                card.CustomAnswers = new List<RSOption>();

                int index = 1;
                foreach (Row customOptionSortRow in customOptionSortRowset)
                {
                    int numberOfSelection = customOptionSortRow.GetValue<int>("number");
                    string customAnswer = customOptionSortRow.GetValue<string>("custom_answer");
                    customAnswer = char.ToUpper(customAnswer[0]) + customAnswer.Substring(1);

                    RSOption option = new RSOption
                    {
                        OptionId = index.ToString(),
                        Content = customAnswer,
                        NumberOfSelection = numberOfSelection
                    };

                    card.CustomAnswers.Add(option);
                    index++;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectRSCustomAnswersResultFromCard(RSCard card, bool isAnonymous, string companyId, ISession analyticSession, ISession mainSession)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_sort_by_number_timestamp",
                            new List<string>(), new List<string> { "card_id" }));
                RowSet customOptionSortRowset = analyticSession.Execute(ps.Bind(card.CardId));

                card.CustomAnswers = new List<RSOption>();

                User userManager = new User();

                int numberOfResponses = 0;
                foreach (Row customOptionSortRow in customOptionSortRowset)
                {
                    string lowerCustomAnswer = customOptionSortRow.GetValue<string>("custom_answer");

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer",
                            new List<string>(), new List<string> { "custom_answer", "card_id" }));
                    Row similarRow = analyticSession.Execute(ps.Bind(lowerCustomAnswer, card.CardId)).FirstOrDefault();

                    if (similarRow != null)
                    {
                        string similarId = similarRow.GetValue<string>("similar_id");
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_similar",
                           new List<string>(), new List<string> { "similar_id" }));
                        RowSet similarOptionRowset = analyticSession.Execute(ps.Bind(similarId));

                        foreach (Row similarOptionRow in similarOptionRowset)
                        {
                            string optionId = similarOptionRow.GetValue<string>("option_id");
                            string answeredByUserId = similarOptionRow.GetValue<string>("created_by_user_id");

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option",
                                new List<string>(), new List<string> { "card_id", "id" }));
                            Row customOptionRow = mainSession.Execute(ps.Bind(card.CardId, optionId)).FirstOrDefault();

                            if (customOptionRow != null)
                            {
                                string customAnswer = customOptionRow.IsNull("content") ? string.Empty : customOptionRow.GetValue<string>("content");

                                RSOption option = new RSOption
                                {
                                    OptionId = optionId,
                                    Content = customAnswer,
                                    NumberOfSelection = 0,
                                    AnsweredByUser = isAnonymous ? null : userManager.SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User
                                };

                                card.CustomAnswers.Add(option);
                                numberOfResponses++;
                            }
                        }
                    }
                }

                card.TotalResponses = numberOfResponses;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AnalyticSelectRSResponderReportResponse SelectRSRespondersReport(string topicId, bool isAnonymous, List<User> targetedAudience, double timezoneOffset, ISession mainSession, ISession analyticSession)
        {
            AnalyticSelectRSResponderReportResponse response = new AnalyticSelectRSResponderReportResponse();
            response.PersonnelCompletion = new RSPersonnelCompletion();
            response.Reports = new List<RSReportPerUser>();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;

                response.PersonnelCompletion = SelectPersonnelCompletion(topicId, targetedAudience.Count(), analyticSession);

                foreach (User targetedUser in targetedAudience)
                {
                    bool isCompleted = false;

                    string userId = targetedUser.UserId;

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_progress_by_user",
                          new List<string>(), new List<string> { "user_id", "topic_id" }));
                    Row progressRow = analyticSession.Execute(ps.Bind(userId, topicId)).FirstOrDefault();

                    RSReportPerUser report = new RSReportPerUser();
                    report.User = isAnonymous ? null : targetedUser;

                    if (progressRow != null)
                    {
                        DateTime? lastUpdatedTimestamp = progressRow.GetValue<DateTime?>("last_updated_timestamp");
                        string lastUpdatedTimestampString = string.Empty;

                        if (lastUpdatedTimestamp != null)
                        {
                            lastUpdatedTimestamp = lastUpdatedTimestamp.Value.AddHours(timezoneOffset);
                            lastUpdatedTimestampString = lastUpdatedTimestamp.Value.ToString("dd MMM yyyy");
                        }

                        if (progressRow["completed_on_timestamp"] != null)
                        {
                            isCompleted = true;
                        }

                        report = new RSReportPerUser
                        {
                            IsCompleted = isCompleted,
                            LastUpdateTimestamp = lastUpdatedTimestamp,
                            LastUpdateTimestampString = lastUpdatedTimestampString,
                            User = isAnonymous ? null : targetedUser
                        };
                    }

                    response.Reports.Add(report);
                }


                response.Reports = response.Reports.OrderByDescending(report => report.IsCompleted).ThenBy(report => report.LastUpdateTimestamp).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

    }
}
