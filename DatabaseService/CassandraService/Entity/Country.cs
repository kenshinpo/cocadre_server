﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using System.Linq;
using Cassandra;

using log4net;
using System.Web.Hosting;
using System.IO;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Validation;
using CassandraService.Utilities;
using Microsoft.VisualBasic.FileIO;
using System.Text;

namespace CassandraService.Entity
{
    [Serializable]
    public class Country
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public string Abb { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Code { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Display { get; set; }

        public void AddCountryToTable()
        {
            PreparedStatement ps = null;
            BatchStatement bs = new BatchStatement();

            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();

            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(Files.Country);
                Stream stream = new MemoryStream(byteArray);
                TextFieldParser parser = new TextFieldParser(stream);
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();
                string[] fields;
                List<Country> country_list = new List<Country>();
                while (!parser.EndOfData)
                {
                    fields = parser.ReadFields();
                    string countryAbb = fields[0].Trim();
                    string countryName = fields[1].Trim();
                    string countryCodeString = fields[3];
                    if (!countryCodeString.Equals("NULL"))
                    {
                        int countryCode = Int16.Parse(countryCodeString);

                        ps = session.Prepare(CQLGenerator.InsertStatement("country",
                            new List<string> { "country_abb", "country_code", "country_name" }));
                        bs = bs.Add(ps.Bind(countryAbb, countryCode, countryName));

                        ps = session.Prepare(CQLGenerator.InsertStatement("country_by_code",
                           new List<string> { "country_pk", "country_abb", "country_code", "country_name" }));
                        bs = bs.Add(ps.Bind("country", countryAbb, countryCode, countryName));
                    }
                }
                session.Execute(bs);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void AddCountryIpToTable()
        {
            PreparedStatement ps = null;

            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();

            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(Files.Country_IP);
                Stream stream = new MemoryStream(byteArray);
                TextFieldParser parser = new TextFieldParser(stream);
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();
                string[] fields;
                while (!parser.EndOfData)
                {
                    fields = parser.ReadFields();

                    string fromIp = fields[0];
                    string toIp = fields[1];
                    Int64 fromIpNum = Int64.Parse(fields[2]);
                    Int64 toIpNum = Int64.Parse(fields[3]);
                    string countryAbb = fields[4].Trim();
                    string countryName = fields[5].Trim();
                    ps = session.Prepare(CQLGenerator.InsertStatement("country_ip",
                                               new List<string> { "ip_pk", "from_ip", "to_ip", "from_ip_num", "to_ip_num", "country_abb", "country_name" }));
                    BoundStatement bound = ps.Bind("ip", fromIp, toIp, fromIpNum, toIpNum, countryAbb, countryName);
                    session.Execute(bound);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void AddTimezoneToTable()
        {
            PreparedStatement ps = null;
            BatchStatement bs = new BatchStatement();

            ConnectionManager cm = new ConnectionManager();
            ISession session = cm.getMainSession();

            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(Files.Timezone);
                Stream stream = new MemoryStream(byteArray);
                TextFieldParser parser = new TextFieldParser(stream);
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();
                string[] fields;
                while (!parser.EndOfData)
                {
                    fields = parser.ReadFields();
                    double offset = double.Parse(fields[0]);
                    string title = fields[1].Trim();
                    string zoneName = fields[2].Trim();

                    ps = session.Prepare(CQLGenerator.InsertStatement("timezone",
                        new List<string> { "zone_pk", "offset", "title", "zone_name" }));
                    bs = bs.Add(ps.Bind("timezone", offset, title, zoneName));
                }

                session.Execute(bs);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public CountryListResponse GetAllCountry(string adminUserId, string companyId)
        {
            CountryListResponse response = new CountryListResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1. Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion

                #region Step 2. Read data from database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("country", new List<string> { }, new List<string> { }));
                BoundStatement bs = ps.Bind();
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    List<Country> countries = new List<Country>();
                    foreach (Row row in rowSet.GetRows())
                    {
                        countries.Add(new Country { Abb = row.GetValue<String>("country_abb"), Name = row.GetValue<String>("country_name"), Code = Convert.ToString(row.GetValue<int>("country_code")), Display = "+" + row.GetValue<int>("country_code") + " - (" + row.GetValue<string>("country_abb") + ")" });
                    }
                    countries = countries.OrderBy(c => c.Code).ThenBy(c => c.Abb).ToList();
                    response.Countries = countries;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseCountryDataEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseCountryDataEmpty);
                    response.ErrorMessage = ErrorMessage.DatabaseCountryDataEmpty;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CountryListResponse UpdateCountries()
        {
            CountryListResponse response = new CountryListResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Get all countries information
                List<Country> countries = new List<Country>();
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("country", new List<string> { }, new List<string> { }));
                BoundStatement bs = ps.Bind();
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    foreach (Row row in rowSet.GetRows())
                    {
                        countries.Add(new Country { Abb = row.GetValue<string>("country_abb"), Name = row.GetValue<string>("country_name"), Code = row.GetValue<string>("country_code"), Display = "+" + row.GetValue<int>("country_code") + "(" + row.GetValue<string>("country_abb") + ")" });
                    }

                    //countries = countries.OrderBy(c => c.Code).ThenBy(c => c.Abb).ToList();
                }
                #endregion

                #region Read all user_contact
                BatchStatement batch_statement = new BatchStatement();
                ps = session.Prepare(CQLGenerator.SelectStatement("user_contact", new List<string> { }, new List<string> { }));
                bs = ps.Bind();
                rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    foreach (Row row in rowSet.GetRows())
                    {
                        if (row.GetValue<string>("phone_country_name") == null)
                        {
                            if (row["phone_country_code"] != null)
                            {
                                PreparedStatement ps_user_contact;

                                if (row.GetValue<string>("phone_country_code").Equals(""))
                                {
                                    ps_user_contact = session.Prepare(CQLGenerator.UpdateStatement("user_contact", new List<string> { "id" }, new List<string> { "phone_country_code", "phone_country_name" }, new List<string>()));
                                    batch_statement.Add(ps_user_contact.Bind(null, null, row.GetValue<string>("id")));
                                }
                                else
                                {
                                    ps_user_contact = session.Prepare(CQLGenerator.UpdateStatement("user_contact", new List<string> { "id" }, new List<string> { "phone_country_name" }, new List<string>()));
                                    Country c = countries.Where(cc => cc.Code.Equals(row.GetValue<string>("phone_country_code"))).FirstOrDefault();
                                    string countryAbb;
                                    if (c != null)
                                    {
                                        countryAbb = c.Abb;
                                    }
                                    else
                                    {
                                        countryAbb = null;
                                    }
                                    batch_statement.Add(ps_user_contact.Bind(countryAbb, row.GetValue<string>("id")));
                                }
                            }
                        }
                    }
                }
                #endregion

                #region execute 
                session.Execute(batch_statement);
                #endregion

                response.Countries = countries;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CountryListResponse GetCountriesForPhone()
        {
            CountryListResponse response = new CountryListResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("country", new List<string> { }, new List<string> { }));
                BoundStatement bs = ps.Bind();
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    List<Country> countries = new List<Country>();
                    foreach (Row row in rowSet.GetRows())
                    {
                        countries.Add(new Country { Abb = row.GetValue<string>("country_abb"), Name = row.GetValue<string>("country_name"), Code = Convert.ToString(row.GetValue<int>("country_code")), Display = "+" + row.GetValue<int>("country_code") + "(" + row.GetValue<string>("country_abb") + ")" });
                    }

                    countries = countries.OrderBy(c => c.Code).ThenBy(c => c.Abb).ToList();
                    response.Countries = countries;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseCountryDataEmpty);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseCountryDataEmpty);
                    response.ErrorMessage = ErrorMessage.DatabaseCountryDataEmpty;
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}