﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class Subscription
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum SubscriptionQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum SubscriptionDurationTypeEnum
        {
            Perpetual = 1,
            Schedule = 2
        }

        public enum SubscriptionStatusEnum
        {
            Active = 1,
            Hidden = 2
        }

        [DataMember]
        public Assessment Assessment { get; set; }

        [DataMember]
        public Company Company { get; set; }

        [DataMember]
        public int DurationType { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember]
        public bool IsForUser { get; set; }

        [DataMember]
        public List<User> TargetedUsers { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int VisibleDaysAfterCompletion { get; set; }

        [DataMember]
        public int NumberOfRetakes { get; set; }

        [DataMember]
        public int NumberOfLicences { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }

        public SubscriptionCreateResponse CreateAssessmentSubscription(string adminUserId,
                                                                       string companyId,
                                                                       string assessmentId,
                                                                       int numberOfLicences,
                                                                       int durationType,
                                                                       int visibleDaysAfterCompletion,
                                                                       int numberOfRetake,
                                                                       int status,
                                                                       DateTime? startDate,
                                                                       DateTime? endDate,
                                                                       List<string> targetedDepartmentIds,
                                                                       List<string> targetedUserIds)
        {
            SubscriptionCreateResponse response = new SubscriptionCreateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                if (assessmentRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                if(assessmentRow.GetValue<int>("status") == (int)Assessment.AssessmentStatusEnum.Draft)
                {
                    response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInDraft);
                    response.ErrorMessage = ErrorMessage.AssessmentInDraft;
                    return response;
                }

                // Check if exists
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_subscription", new List<string>(),
                   new List<string> { "assessment_id", "company_id" }));
                Row subRow = session.Execute(ps.Bind(assessmentId, companyId)).FirstOrDefault();
                if(subRow != null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.SubscriptionAlreadyExists);
                    response.ErrorMessage = ErrorMessage.SubscriptionAlreadyExists;
                    return response;
                }

                if(durationType != (int)SubscriptionDurationTypeEnum.Perpetual && durationType != (int)SubscriptionDurationTypeEnum.Schedule)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.SubscriptionInvalidDurationType);
                    response.ErrorMessage = ErrorMessage.SubscriptionInvalidDurationType;
                    return response;
                }

                if (numberOfRetake <= 0)
                {
                    numberOfRetake = 1;
                }

                if(visibleDaysAfterCompletion <= 0)
                {
                    visibleDaysAfterCompletion = 1;
                }

                DateTime currentTime = DateTime.UtcNow;
                if (durationType == (int)SubscriptionDurationTypeEnum.Perpetual)
                {
                    startDate = currentTime;
                    endDate = null;
                }
                else
                {
                    SubscriptionValidationResponse validateDateResponse = ValidateDate(startDate.Value, endDate, currentTime);
                    if (!validateDateResponse.Success)
                    {
                        response.ErrorCode = validateDateResponse.ErrorCode;
                        response.ErrorMessage = validateDateResponse.ErrorMessage;
                        return response;
                    }
                }

                bool isForDepartment = targetedDepartmentIds == null || targetedDepartmentIds.Count == 0 ? false : true;
                bool isForUser = targetedUserIds == null || targetedUserIds.Count == 0 ? false : true;
                bool isForEveryone = isForDepartment || isForUser ? false : true;

                BatchStatement batch = new BatchStatement();
                ps = session.Prepare(CQLGenerator.InsertStatement("assessment_subscription", 
                    new List<string> { "assessment_id", "company_id", "status", "number_of_licenses", "duration_type", "visible_days_after_completion", "number_of_retake", "start_timestamp", "end_timestamp", "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batch.Add(ps.Bind(assessmentId, companyId, status, numberOfLicences, durationType, visibleDaysAfterCompletion, numberOfRetake, startDate, endDate, isForEveryone, isForDepartment, isForUser, false, adminUserId, currentTime, adminUserId, currentTime));

                foreach(BoundStatement bs in CreatePrivacy(assessmentId, targetedDepartmentIds, targetedUserIds, session))
                {
                    batch.Add(bs);
                }

                if(status == (int)SubscriptionStatusEnum.Active)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_subscription_sorted_by_timestamp",
                    new List<string> { "assessment_id", "company_id", "start_timestamp"}));
                    batch.Add(ps.Bind(assessmentId, companyId, startDate));
                }

                session.Execute(batch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public SubscriptionSelectAllResponse SelectAllAssessmentSubscription(string adminUserId)
        {
            SubscriptionSelectAllResponse response = new SubscriptionSelectAllResponse();
            response.Subscriptions = new List<Subscription>();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_subscription", new List<string>(), new List<string>()));
                RowSet subRowset = session.Execute(ps.Bind());

                foreach(Row subRow in subRowset)
                {
                    string companyId = subRow.GetValue<string>("company_id");
                    string assessmentId = subRow.GetValue<string>("assessment_id");

                    Row companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow == null)
                    {
                        continue;
                    }

                    Row assesementRow = vh.ValidateAssessment(assessmentId, session);
                    if (assesementRow == null)
                    {
                        continue;
                    }

                    Subscription subscription = SelectAssessmentSubscription(companyId, assessmentId, (int)SubscriptionQueryType.Basic, session, subRow).Subscription;
                    if(subscription != null)
                    {
                        response.Subscriptions.Add(subscription);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public SubscriptionSelectAllResponse SelectAllAssessmentSubscriptionByCompany(string companyId, string userId, ISession session)
        {
            SubscriptionSelectAllResponse response = new SubscriptionSelectAllResponse();
            response.Subscriptions = new List<Subscription>();
            response.Success = false;
            try
            {
                Department departmentManager = new Department();
                List<Department> departments = departmentManager.GetAllDepartmentByUserId(userId, companyId, session).Departments;

                List<string> departmentIds = new List<string>();
                foreach(Department department in departments)
                {
                    departmentIds.Add(department.Id);
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_subscription_sorted_by_timestamp", new List<string>(), new List<string> { "company_id" }));
                RowSet subRowset = session.Execute(ps.Bind(companyId));

                foreach (Row subRow in subRowset)
                {
                    string assessmentId = subRow.GetValue<string>("assessment_id");
                    ps = session.Prepare(CQLGenerator.SelectStatement("assessment_subscription", new List<string>(), new List<string> { "assessment_id", "company_id" }));
                    Row assessSubRow = session.Execute(ps.Bind(assessmentId, companyId)).FirstOrDefault();

                    bool isForEveryone = assessSubRow.GetValue<bool>("is_for_everyone");
                    bool isForDepartment = assessSubRow.GetValue<bool>("is_for_department");
                    bool isForUser = assessSubRow.GetValue<bool>("is_for_user");

                    if(CheckSubscriptionForCurrentUser(userId, assessmentId, isForEveryone, isForDepartment, isForUser, departmentIds, session))
                    {
                        Subscription sub = SelectAssessmentSubscription(companyId, assessmentId, (int)SubscriptionQueryType.Basic, session, assessSubRow).Subscription;
                        if(sub != null)
                        {
                            response.Subscriptions.Add(sub);
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        public SubscriptionSelectResponse SelectFullDetailAssessmentSubscription(string adminUserId, string companyId, string assessmentId)
        {
            SubscriptionSelectResponse response = new SubscriptionSelectResponse();
            response.Subscription = null;
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                Row companyRow = vh.ValidateCompany(companyId, session);
                if(companyRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                Row assesementRow = vh.ValidateAssessment(assessmentId, session);
                if (assesementRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                Subscription subscription = SelectAssessmentSubscription(companyId, assessmentId, (int)SubscriptionQueryType.FullDetail, session).Subscription;
                if (subscription != null)
                {
                    response.Subscription = subscription;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public SubscriptionSelectResponse SelectAssessmentSubscription(string companyId, string assessmentId, int queryType, ISession session, Row subRow = null)
        {
            SubscriptionSelectResponse response = new SubscriptionSelectResponse();
            response.Subscription = null;
            response.Success = false;
            try
            {
                if(subRow == null)
                {
                    PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("assessment_subscription", new List<string>(), new List<string> { "company_id", "assessment_id" }));
                    subRow = session.Execute(ps.Bind(companyId, assessmentId)).FirstOrDefault();
                }

                if (subRow != null)
                {
                    bool isForEveryone = false;
                    bool isForDepartment = false;
                    List<Department> departments = new List<Department>();
                    bool isForUser = false;
                    List<User> users = new List<User>();
                    int numberOfLicences = 0;
                    int durationType = subRow.GetValue<int>("duration_type");
                    int visibleDaysAfterCompletion = subRow.GetValue<int>("visible_days_after_completion");
                    int numberOfRetakes = subRow.GetValue<int>("number_of_retake");
                    DateTime? startDate = subRow.GetValue<DateTime?>("start_timestamp");
                    DateTime? endDate = subRow.GetValue<DateTime?>("end_timestamp");
                    int status = 0;

                    List<Department> targetedDepartments = new List<Department>();
                    List<User> targetedUsers = new List<User>();

                    Company company = new Company().GetCompany(companyId, session).Company;
                    Assessment assessment = new Assessment().SelectAssessment(assessmentId, (int)Assessment.AssessmentQueryType.Basic, (int)AssessmentCard.AssessmentCardQueryType.Basic, session).Assessment;

                    if(queryType == (int)SubscriptionQueryType.FullDetail)
                    {
                        isForEveryone = subRow.GetValue<bool>("is_for_everyone");
                        isForDepartment = subRow.GetValue<bool>("is_for_department");
                        isForUser = subRow.GetValue<bool>("is_for_user");
                        status = subRow.GetValue<int>("status");
                        numberOfLicences = subRow.GetValue<int>("number_of_licenses");

                        if(isForDepartment)
                        {
                            targetedDepartments = SelectTargetedDepartments(assessmentId, companyId, session);
                        }

                        if(isForUser)
                        {
                            targetedUsers = SelectTargetedUsers(assessmentId, companyId, session);
                        }
                    }

                    Subscription subscription = new Subscription
                    {
                        Assessment = assessment,
                        Company = company,
                        Status = status,
                        NumberOfLicences = numberOfLicences,
                        DurationType = durationType,
                        VisibleDaysAfterCompletion = visibleDaysAfterCompletion,
                        NumberOfRetakes = numberOfRetakes,
                        StartDate = startDate,
                        EndDate = endDate,
                        IsForEveryone = isForEveryone,
                        IsForDepartment = isForDepartment,
                        TargetedDepartments = targetedDepartments,
                        IsForUser = IsForUser,
                        TargetedUsers = targetedUsers
                    };

                    response.Subscription = subscription;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public bool CheckSubscriptionForAssessment(string assessmentId, ISession session = null)
        {
            bool isSubscriptionExists = false;
            try
            {
                if(session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.CountStatement("assessment_subscription", new List<string> { "assessment_id" }));
                int count = (int)session.Execute(ps.Bind(assessmentId)).FirstOrDefault().GetValue<long>("count");

                if(count > 0)
                {
                    isSubscriptionExists = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isSubscriptionExists;
        }

        public SubscriptionDeleteResponse DeleteSubscription(string adminUserId, string companyId, string assessmentId)
        {
            SubscriptionDeleteResponse response = new SubscriptionDeleteResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                Row subscriptionRow = vh.ValidateAssessmentSubscription(assessmentId, companyId, session);
                if (subscriptionRow == null)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.SubscriptionInvalid);
                    response.ErrorMessage = ErrorMessage.SubscriptionInvalid;
                    return response;
                }

                BatchStatement batch = new BatchStatement();

                DateTime startTimestamp = subscriptionRow.GetValue<DateTime>("timestamp");

                PreparedStatement ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_subscription", new List<string> { "assessment_id", "company_id" }));
                batch.Add(ps.Bind(assessmentId, companyId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_subscription_sorted_by_timestamp", new List<string> { "company_id", "start_timestamp", "assessment_id" }));
                batch.Add(ps.Bind(companyId, startTimestamp, assessmentId));

                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_targeted_department", new List<string>(), new List<string> { "assessment_id" }));
                RowSet departmentRowSet = session.Execute(ps.Bind(assessmentId));
                foreach(Row departmentRow in departmentRowSet)
                {
                    string departmentId = departmentRow.GetValue<string>("department_id");
                    ps = session.Prepare(CQLGenerator.DeleteStatement("department_targeted_assessment", new List<string> { "department_id", "assessment_id" }));
                    batch.Add(ps.Bind(departmentId, assessmentId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_targeted_department", new List<string> { "assessment_id" }));
                batch.Add(ps.Bind(assessmentId));

                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_targeted_user", new List<string>(), new List<string> { "assessment_id" }));
                RowSet userRowSet = session.Execute(ps.Bind(assessmentId));
                foreach (Row userRow in userRowSet)
                {
                    string userId = userRow.GetValue<string>("user_id");
                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_targeted_assessment", new List<string> { "user_id", "assessment_id" }));
                    batch.Add(ps.Bind(userId, assessmentId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_targeted_user", new List<string> { "assessment_id" }));
                batch.Add(ps.Bind(assessmentId));

                session.Execute(batch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        private SubscriptionValidationResponse ValidateDate(DateTime startDate, DateTime? endDate, DateTime currentTime)
        {
            SubscriptionValidationResponse response = new SubscriptionValidationResponse();
            response.Success = false;
            try
            {
                if (endDate.HasValue && endDate.Value != DateTime.MinValue)
                {
                    if (endDate <= currentTime)
                    {
                        Log.Error("Subscription must end after current date");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.SubscriptionEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.SubscriptionEndDateEarlierThanToday;
                        return response;
                    }

                    if (startDate >= endDate)
                    {
                        Log.Error("Subscription must start before end date");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.SubscriptionEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.SubscriptionEndDateEarlierThanStartDate;
                        return response;
                    }
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<BoundStatement> CreatePrivacy(string assessmentId, List<string> targetedDepartmentIds, List<string> targetedUserIds, ISession session)
        {
            List<BoundStatement> statements = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;

                foreach (string departmentId in targetedDepartmentIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("department_targeted_assessment", new List<string> { "assessment_id", "department_id" }));
                    statements.Add(ps.Bind(departmentId, assessmentId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_targeted_department", new List<string> { "assessment_id", "department_id" }));
                    statements.Add(ps.Bind(departmentId, assessmentId));
                }

                foreach (string userId in targetedUserIds)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_targeted_user", new List<string> { "user_id", "assessment_id" }));
                    statements.Add(ps.Bind(userId, assessmentId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("user_targeted_assessment", new List<string> { "user_id", "assessment_id" }));
                    statements.Add(ps.Bind(userId, assessmentId));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return statements;
        }

        private List<BoundStatement> UpdatePrivacy(string assessmentId, List<string> newTargetedDepartmentIds, List<string> newTargetedUserIds, ISession session, bool isForSingle = true)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_targeted_department",
                    new List<string>(), new List<string> { "assessment_id" }));
                RowSet departmentPrivacyRowset = session.Execute(ps.Bind(assessmentId));

                foreach (Row departmentPrivacyRow in departmentPrivacyRowset)
                {
                    string departmentId = departmentPrivacyRow.GetValue<string>("department_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("department_targeted_assessment", new List<string> { "department_id", "assessment_id" }));
                    deleteBatch.Add(ps.Bind(departmentId, assessmentId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_targeted_department", new List<string> { "assessment_id" }));
                deleteBatch.Add(ps.Bind(assessmentId));

                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_targeted_user",
                    new List<string>(), new List<string> { "assessment_id" }));
                RowSet userPrivacyRowset = session.Execute(ps.Bind(assessmentId));

                foreach (Row userPrivacyRow in userPrivacyRowset)
                {
                    string userId = userPrivacyRow.GetValue<string>("user_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_targeted_assessment", new List<string> { "user_id", "assessment_id" }));
                    deleteBatch.Add(ps.Bind(userId, assessmentId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_targeted_user", new List<string> { "assessment_id" }));
                deleteBatch.Add(ps.Bind(assessmentId));

                session.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return CreatePrivacy(assessmentId, newTargetedDepartmentIds, newTargetedUserIds, session);
        }

        private List<Department> SelectTargetedDepartments(string assessmentId, string companyId, ISession mainSession)
        {
            List<Department> targetedDepartments = new List<Department>();
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_targeted_department", new List<string>(), new List<string> { "assessment_id" }));
                RowSet departmentRowset = mainSession.Execute(ps.Bind(assessmentId));

                Department departmentManager = new Department();
                foreach (Row departmentRow in departmentRowset)
                {
                    string departmentId = departmentRow.GetValue<string>("department_id");
                    if (targetedDepartments.FirstOrDefault(d => d.Id == departmentId) == null)
                    {
                        Department department = departmentManager.GetDepartmentDetail(null, companyId, departmentId, (int)Department.QUERY_TYPE_BASIC, mainSession).Department;
                        if (department != null)
                        {
                            targetedDepartments.Add(department);
                        }
                    }
                       
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return targetedDepartments;
        }

        private List<User> SelectTargetedUsers(string assessmentId, string companyId, ISession mainSession)
        {
            List<User> targetedUsers = new List<User>();

            try
            {
                User userManager = new User();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_targeted_user", new List<string>(), new List<string> { "assessment_id" }));
                RowSet userRowset = mainSession.Execute(ps.Bind(assessmentId));

                foreach (Row userRow in userRowset)
                {
                    string userId = userRow.GetValue<string>("user_id");
                    if (targetedUsers.FirstOrDefault(u => u.UserId == userId) == null)
                    {
                        User user = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                        if (user != null)
                        {
                            targetedUsers.Add(user);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return targetedUsers;
        }

        private bool CheckSubscriptionForCurrentUser(string userId, string assessmentId, bool isForEveryone, bool isForDepartment, bool isForUser, List<string> departmentIds, ISession session)
        {
            bool isPulseForCurrentUser = false;

            if (isForEveryone)
            {
                isPulseForCurrentUser = true;
            }
            else
            {
                if (isForDepartment)
                {
                    foreach (string departmentId in departmentIds)
                    {
                        PreparedStatement psFeedTargetedDepartment = session.Prepare(CQLGenerator.SelectStatement("department_targeted_assessment", new List<string>(), new List<string> { "assessment_id", "department_id" }));
                        BoundStatement bsFeedTargetedDepartment = psFeedTargetedDepartment.Bind(assessmentId, departmentId);

                        Row departmentPrivacyRow = session.Execute(bsFeedTargetedDepartment).FirstOrDefault();

                        if (departmentPrivacyRow != null)
                        {
                            isPulseForCurrentUser = true;
                            break;
                        }
                    }
                }

                if (!isPulseForCurrentUser && isForUser)
                {
                    PreparedStatement psFeedTargetedUser = session.Prepare(CQLGenerator.SelectStatement("assessment_targeted_user", new List<string>(), new List<string> { "assessment_id", "user_id" }));
                    BoundStatement bsFeedTargetedUser = psFeedTargetedUser.Bind(assessmentId, userId);

                    Row userPrivacyRow = session.Execute(bsFeedTargetedUser).FirstOrDefault();

                    if (userPrivacyRow != null)
                    {
                        isPulseForCurrentUser = true;
                    }
                }
            }

            return isPulseForCurrentUser;
        }
    }
}
