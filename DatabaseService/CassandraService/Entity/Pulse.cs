﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [DataContract]
    public class Pulse
    {
        public static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember]
        public string PulseId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public string StartDateString { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public string EndDateString { get; set; }

        [DataMember]
        public bool IsPrioritized { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int PulseType { get; set; }

        [DataMember]
        public int ProgressStatus { get; set; }

        [DataMember]
        public Dictionary<string, List<PulseAction>> Actions { get; set; }

        [DataMember]
        public bool IsForEveryone { get; set; }

        [DataMember]
        public bool IsForDepartment { get; set; }

        [DataMember]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember]
        public bool IsForUser { get; set; }

        [DataMember]
        public List<User> TargetedUsers { get; set; }

        [DataMember]
        public int ActionTaken { get; set; }

        [DataMember]
        public DateTime SavedDate { get; set; }

        [DataMember]
        public DateTime DeletedDate { get; set; }

        // Live 360
        [DataMember(EmitDefaultValue = false)]
        public Appraisal Appraisal { get; set; }

        public enum PulseTypeEnum
        {
            Announcement = 1,
            Banner = 2,
            Dynamic = 3,
            Live360 = 4,

            Topic = 5,
            Survey = 6,
            Event = 7,
        }

        public enum PulsePriorityEnum
        {
            Announcement = 1,
            Dynamic = 2,
            Live360 = 3,

            Survey = 4,
            Event = 5,
            Topic = 6,
            Challenge = 7
        }

        public enum PulseStatusEnum
        {
            Deleted = -1,
            Unlisted = 1,
            Active = 2,
            Hidden = 3
        }

        public enum PulseQueryEnum
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum ProgressStatusEnum
        {
            Upcoming = 1,
            Live = 2,
            Completed = 3
        }

        public enum PulseActionEnum
        {
            DoNothing = 0,
            Next = 1,
            Skip = 2,
            SaveToInbox = 3,
            Delete = 4,
            Submit = 5,
            Acknowledge = 6,
            PlayTopic = 7,
            StartEvent = 8,
            StartSurvey = 9,
            AcceptChallenge = 10,
        }

        public enum PulseActionTypeEnum
        {
            Select = 1,
            Swipe = 2
        }

        public enum PulseSwipeActionDirectionEnum
        {
            NoDirection = 0,
            Right = 1,
            Left = 2
        }

        [DataContract]
        public class PulseAction
        {
            [DataMember]
            public int Code { get; set; }
            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public int Direction { get; set; }

            [DataMember]
            public int Type { get; set; }
        }

        public Dictionary<string, List<PulseAction>> SelectActionForPulse(int pulseType, bool isDeckComplusory = false)
        {
            Dictionary<string, List<PulseAction>> dictionary = new Dictionary<string, List<PulseAction>>();
            List<PulseAction> actions = new List<PulseAction>();
            try
            {
                switch (pulseType)
                {
                    case (int)PulseTypeEnum.Announcement:
                        actions = new List<PulseAction>
                            {
                                 new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Select,
                                    Code =  (int)PulseActionEnum.Acknowledge,
                                    Name = DefaultResource.PulseActionAcknowledge,
                                    Direction = (int)PulseSwipeActionDirectionEnum.NoDirection
                                },

                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Skip,
                                    Name = DefaultResource.PulseActionSkipCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Left
                                },

                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Skip,
                                    Name = DefaultResource.PulseActionSkipCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Right
                                },
                            };

                        dictionary.Add(DefaultResource.PulseActionBeforeKey, actions);

                        actions = new List<PulseAction>
                            {
                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Delete,
                                    Name = DefaultResource.PulseActionDelete,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Left
                                },
                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.SaveToInbox,
                                    Name = DefaultResource.PulseActionSave,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Right
                                }
                            };

                        dictionary.Add(DefaultResource.PulseActionAfterKey, actions);
                        break;
                    case (int)PulseTypeEnum.Banner:
                        actions = new List<PulseAction>
                            {
                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Next,
                                    Name = DefaultResource.PulseActionNextCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Right
                                },
                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Next,
                                    Name = DefaultResource.PulseActionNextCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Left
                                }
                            };

                        dictionary.Add(DefaultResource.PulseActionBeforeKey, actions);
                        break;
                    case (int)PulseTypeEnum.Dynamic:
                        if (isDeckComplusory)
                        {
                            actions = new List<PulseAction>
                            {
                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.DoNothing,
                                    Name = DefaultResource.PulseActionDoNothing,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Left
                                },

                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.DoNothing,
                                    Name = DefaultResource.PulseActionDoNothing,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Right
                                }
                            };
                        }
                        else
                        {
                            actions = new List<PulseAction>
                            {
                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Next,
                                    Name = DefaultResource.PulseActionNextCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Left
                                },

                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Next,
                                    Name = DefaultResource.PulseActionNextCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Right
                                }
                            };
                        }

                        dictionary.Add(DefaultResource.PulseActionBeforeKey, actions);

                        actions = new List<PulseAction>
                        {
                            new PulseAction {
                                Type = (int)PulseActionTypeEnum.Swipe,
                                Code =  (int)PulseActionEnum.Submit,
                                Name = DefaultResource.PulseActionSubmit,
                                Direction = (int)PulseSwipeActionDirectionEnum.Left
                            },

                            new PulseAction {
                                Type = (int)PulseActionTypeEnum.Swipe,
                                Code =  (int)PulseActionEnum.Submit,
                                Name = DefaultResource.PulseActionSubmit,
                                Direction = (int)PulseSwipeActionDirectionEnum.Right
                            }
                        };

                        dictionary.Add(DefaultResource.PulseActionAfterKey, actions);
                        break;
                    case (int)PulseTypeEnum.Live360:
                        actions = new List<PulseAction>
                            {
                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Next,
                                    Name = DefaultResource.PulseActionNextCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Left
                                },

                                new PulseAction {
                                    Type = (int)PulseActionTypeEnum.Swipe,
                                    Code =  (int)PulseActionEnum.Next,
                                    Name = DefaultResource.PulseActionNextCard,
                                    Direction = (int)PulseSwipeActionDirectionEnum.Right
                                }
                            };

                        dictionary.Add(DefaultResource.PulseActionBeforeKey, actions);

                        actions = new List<PulseAction>
                        {
                            new PulseAction {
                                Type = (int)PulseActionTypeEnum.Swipe,
                                Code =  (int)PulseActionEnum.Submit,
                                Name = DefaultResource.PulseActionSubmit,
                                Direction = (int)PulseSwipeActionDirectionEnum.Left
                            },

                            new PulseAction {
                                Type = (int)PulseActionTypeEnum.Swipe,
                                Code =  (int)PulseActionEnum.Submit,
                                Name = DefaultResource.PulseActionSubmit,
                                Direction = (int)PulseSwipeActionDirectionEnum.Right
                            }
                        };

                        dictionary.Add(DefaultResource.PulseActionAfterKey, actions);
                        break;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return dictionary;
        }

        public Dictionary<string, List<PulseAction>> SelectActionForAnsweringAppraisal()
        {
            Dictionary<string, List<PulseAction>> dictionary = new Dictionary<string, List<PulseAction>>();
            List<PulseAction> actions = new List<PulseAction>();
            try
            {
                actions = new List<PulseAction>
                {
                    new PulseAction {
                        Type = (int)PulseActionTypeEnum.Swipe,
                        Code =  (int)PulseActionEnum.DoNothing,
                        Name = DefaultResource.PulseActionDoNothing,
                        Direction = (int)PulseSwipeActionDirectionEnum.Left
                    },

                    new PulseAction {
                        Type = (int)PulseActionTypeEnum.Swipe,
                        Code =  (int)PulseActionEnum.DoNothing,
                        Name = DefaultResource.PulseActionDoNothing,
                        Direction = (int)PulseSwipeActionDirectionEnum.Right
                    }
                };

                dictionary.Add(DefaultResource.PulseActionBeforeKey, actions);

                actions = new List<PulseAction>
                {
                    new PulseAction {
                        Type = (int)PulseActionTypeEnum.Swipe,
                        Code =  (int)PulseActionEnum.Submit,
                        Name = DefaultResource.PulseActionSubmit,
                        Direction = (int)PulseSwipeActionDirectionEnum.Left
                    },

                    new PulseAction {
                        Type = (int)PulseActionTypeEnum.Swipe,
                        Code =  (int)PulseActionEnum.Submit,
                        Name = DefaultResource.PulseActionSubmit,
                        Direction = (int)PulseSwipeActionDirectionEnum.Right
                    }
                };

                dictionary.Add(DefaultResource.PulseActionAfterKey, actions);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return dictionary;
        }

        public PulseSelectAllResponse SelectAllByUser(string requesterUserId, string companyId)
        {
            PulseSelectAllResponse response = new PulseSelectAllResponse();
            response.Pulses = new List<Pulse>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                PulseSingle singleManager = new PulseSingle();
                PulseDynamic dynamicManager = new PulseDynamic();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                int limit = Int16.Parse(WebConfigurationManager.AppSettings["pulse_fetch_limit"]);

                // Check priority
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_by_priority_with_timestamp", new List<string>(), new List<string> { "company_id" }));
                RowSet pulseRowSet = mainSession.Execute(ps.Bind(companyId));

                AnalyticPulse analyticPulse = new AnalyticPulse();
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                Department departmentManager = new Department();
                List<Department> departmentsOfUser = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;
                List<string> departmentIds = new List<string>();
                foreach (Department department in departmentsOfUser)
                {
                    departmentIds.Add(department.Id);
                }

                PulseSelectPrivacyResponse privacyResponse = new PulseSelectPrivacyResponse();

                DateTime currentTime = DateTime.UtcNow;
                foreach (Row pulseRow in pulseRowSet)
                {
                    int pulseType = pulseRow.GetValue<int>("pulse_type");
                    string pulseId = pulseRow.GetValue<string>("pulse_id");
                    DateTime startTimestamp = pulseRow.GetValue<DateTime>("start_timestamp");

                    if(startTimestamp > currentTime)
                    {
                        continue;
                    }

                    Pulse pulse = null;
                    switch (pulseType)
                    {
                        case (int)PulseTypeEnum.Announcement:
                            privacyResponse = SelectPulsePrivacy(pulseId, mainSession);
                            if (!CheckPulseForCurrentUser(requesterUserId, pulseId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, departmentIds, mainSession, true))
                            {
                                continue;
                            }
                            // Check if completed
                            int actionTaken = analyticPulse.SelectSingleActionTakenByUser(pulseId, pulseType, requesterUserId, analyticSession);

                            if (actionTaken != (int)PulseActionEnum.DoNothing)
                            {
                                continue;
                            }

                            pulse = singleManager.SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, timezoneOffset, true, false, pulseType, 0, mainSession).Pulse;
                            break;
                        case (int)PulseTypeEnum.Dynamic:
                            string deckId = pulseRow.GetValue<string>("deck_id");

                            Row deckRow = vh.ValidatePulseDeck(companyId, deckId, mainSession);
                            if (deckRow == null)
                            {
                                Log.Error("Invalid deck: " + deckId);
                                continue;
                            }

                            privacyResponse = SelectPulsePrivacy(deckId, mainSession, false);
                            if (!CheckPulseForCurrentUser(requesterUserId, deckId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, departmentIds, mainSession, false))
                            {
                                continue;
                            }

                            bool isDeckCompulsory = deckRow.GetValue<bool>("is_complusory");
                            bool isDeckAnonymous = deckRow.GetValue<bool>("is_anonymous");
                            int deckPublishMethod = deckRow.GetValue<int>("publish_method_type");
                            DateTime? deckEndTimestamp = deckRow.GetValue<DateTime?>("end_timestamp");

                            int anonymityCount = 0;
                            if (deckRow.IsNull("anonymity_count"))
                            {
                                if (deckRow.GetValue<bool>("is_anonymous"))
                                {
                                    anonymityCount = 1;
                                    #region Update null value of anonymity_count column.
                                    PreparedStatement psUpdate = mainSession.Prepare(CQLGenerator.UpdateStatement("pulse_dynamic_deck",
                                      new List<string> { "company_id", "id" }, new List<string> { "anonymity_count" }, new List<string>()));
                                    BatchStatement bsUpdate = new BatchStatement();
                                    bsUpdate.Add(psUpdate.Bind(anonymityCount, deckRow.GetValue<bool>("company_id"), deckRow.GetValue<bool>("id")));
                                    mainSession.Execute(bsUpdate);
                                    #endregion
                                }
                            }
                            else
                            {
                                anonymityCount = deckRow.GetValue<int>("anonymity_count");
                            }

                            pulse = dynamicManager.SelectCard(deckId, pulseId, deckEndTimestamp, deckPublishMethod, isDeckCompulsory, anonymityCount, false, true, true, requesterUserId, mainSession).Pulse;
                            break;
                        case (int)PulseTypeEnum.Live360:
                            pulse = new Appraisal().MapToPulse(pulseId, requesterUserId, companyId, currentTime, mainSession, analyticSession).Pulse;
                            break;
                    }

                    if (pulse != null)
                    {
                        if (response.Pulses.Count < limit)
                        {
                            response.Pulses.Add(pulse);
                        }
                    }
                }

                if (response.Pulses.Count < limit)
                {
                    // Fetch from banner
                    limit = limit - response.Pulses.Count();
                    List<Pulse> bannerPulses = singleManager.SelectBannerPulses(requesterUserId, companyId, departmentIds, timezoneOffset, limit, mainSession);
                    response.Pulses.AddRange(bannerPulses);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectResponse SelectPulse(string requesterUserId, string companyId, string pulseId, int pulseType)
        {
            PulseSelectResponse response = new PulseSelectResponse();
            response.Pulse = new Pulse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                PulseSingle singleManager = new PulseSingle();
                PulseDynamic dynamicManager = new PulseDynamic();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                AnalyticPulse analyticPulse = new AnalyticPulse();
                DateTime currentTime = DateTime.UtcNow;
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                Department departmentManager = new Department();
                List<Department> departmentsOfUser = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;
                List<string> departmentIds = new List<string>();
                foreach (Department department in departmentsOfUser)
                {
                    departmentIds.Add(department.Id);
                }

                PulseSelectPrivacyResponse privacyResponse = new PulseSelectPrivacyResponse();

                switch (pulseType)
                {
                    case (int)PulseTypeEnum.Announcement:
                        privacyResponse = SelectPulsePrivacy(pulseId, mainSession);
                        if (!CheckPulseForCurrentUser(requesterUserId, pulseId, privacyResponse.IsForEveryone, privacyResponse.IsForDepartment, privacyResponse.IsForUser, departmentIds, mainSession, true))
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                            response.ErrorMessage = ErrorMessage.PulseInvalid;
                            return response;
                        }

                        // Check if completed
                        int actionTaken = analyticPulse.SelectSingleActionTakenByUser(pulseId, pulseType, requesterUserId, analyticSession);

                        if (actionTaken != (int)PulseActionEnum.DoNothing)
                        {
                            response.ErrorCode = Convert.ToInt32(ErrorCode.PulseAcknowledged);
                            response.ErrorMessage = ErrorMessage.PulseAcknowledged;
                            return response;                     
                        }

                        PulseSelectResponse singlePulseResponse = singleManager.SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, timezoneOffset, true, false, pulseType, 0, mainSession);
                        if(!singlePulseResponse.Success)
                        {
                            response.ErrorCode = singlePulseResponse.ErrorCode;
                            response.ErrorMessage = singlePulseResponse.ErrorMessage;
                            return response;
                        }
                        response.Pulse = singlePulseResponse.Pulse;
                        break;
                    case (int)PulseTypeEnum.Live360:
                        PulseMapResponse mapAppraisalResponse = new Appraisal().MapToPulse(pulseId, requesterUserId, companyId, currentTime, mainSession, analyticSession);
                        if(!mapAppraisalResponse.Success)
                        {
                            response.ErrorCode = mapAppraisalResponse.ErrorCode;
                            response.ErrorMessage = mapAppraisalResponse.ErrorMessage;
                            return response;
                        }
                        response.Pulse = mapAppraisalResponse.Pulse;
                        break;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectAllResponse SelectFromInbox(string requesterUserId, string companyId, DateTime? lastDrawnTimestamp)
        {
            PulseSelectAllResponse response = new PulseSelectAllResponse();
            response.Pulses = new List<Pulse>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                bool isCompleted = false;
                DateTime? savedDate = lastDrawnTimestamp;
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["pulse_inbox_fetch_limit"]);
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                PulseSingle feedManager = new PulseSingle();
                AnalyticPulse analyticPulse = new AnalyticPulse();
                Department departmentManager = new Department();
                List<Department> departments = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;
                List<string> departmentIds = new List<string>();
                foreach (Department department in departments)
                {
                    departmentIds.Add(department.Id);
                }

                while (!isCompleted)
                {
                    PreparedStatement ps = null;
                    RowSet pulseRowSet = null;
                    if (savedDate == null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("pulse_inbox_by_save_timestamp", new List<string>(), new List<string> { "user_id" }, limit));
                        pulseRowSet = mainSession.Execute(ps.Bind(requesterUserId));
                    }
                    else
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("pulse_inbox_by_save_timestamp", new List<string>(), new List<string> { "user_id" }, "saved_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        pulseRowSet = mainSession.Execute(ps.Bind(requesterUserId, savedDate));
                    }

                    List<Row> inboxRowSetList = new List<Row>();

                    if (pulseRowSet != null)
                    {
                        inboxRowSetList = pulseRowSet.ToList();

                        foreach (Row inboxRow in inboxRowSetList)
                        {
                            string pulseId = inboxRow.GetValue<string>("pulse_id");
                            int pulseType = inboxRow.GetValue<int>("pulse_type");

                            DateTime savedTimestamp = inboxRow.GetValue<DateTime>("saved_on_timestamp");
                            savedDate = savedTimestamp;

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_privacy", new List<string>(), new List<string> { "pulse_id" }));
                            Row privacyRow = mainSession.Execute(ps.Bind(pulseId)).FirstOrDefault();

                            if (privacyRow == null)
                            {
                                continue;
                            }

                            bool isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = privacyRow.GetValue<bool>("is_for_department");
                            bool isForUser = privacyRow.GetValue<bool>("is_for_user");

                            if (CheckPulseForCurrentUser(requesterUserId, pulseId, isForEveryone, isForDepartment, isForUser, departmentIds, mainSession))
                            {
                                if (response.Pulses.FindIndex(pulse => pulse.PulseId.Equals(pulseId)) < 0 && response.Pulses.Count < limit)
                                {
                                    Pulse pulse = null;
                                    if (pulseType != (int)PulseTypeEnum.Dynamic)
                                    {
                                        pulse = feedManager.SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, timezoneOffset, false, false, 0, 0, mainSession).Pulse;
                                        if (pulse != null)
                                        {
                                            int actionTaken = analyticPulse.SelectSingleActionTakenByUser(pulseId, pulseType, requesterUserId, analyticSession);
                                            pulse.ActionTaken = actionTaken;
                                        }
                                    }

                                    if (pulse != null)
                                    {
                                        pulse.SavedDate = savedTimestamp;
                                        response.Pulses.Add(pulse);
                                        if (response.Pulses.Count == limit)
                                        {
                                            isCompleted = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                    if (inboxRowSetList.Count == 0)
                    {
                        isCompleted = true;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseSelectAllResponse SelectFromTrashbin(string requesterUserId, string companyId, DateTime? lastDrawnTimestamp)
        {
            PulseSelectAllResponse response = new PulseSelectAllResponse();
            response.Pulses = new List<Pulse>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                PulseSingle feedManager = new PulseSingle();
                AnalyticPulse analyticPulse = new AnalyticPulse();
                bool isCompleted = false;
                DateTime? deleteDate = lastDrawnTimestamp;
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["pulse_inbox_fetch_limit"]);
                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                Department departmentManager = new Department();
                List<Department> departments = departmentManager.GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;
                List<string> departmentIds = new List<string>();
                foreach (Department department in departments)
                {
                    departmentIds.Add(department.Id);
                }

                while (!isCompleted)
                {
                    PreparedStatement ps = null;
                    RowSet pulseRowSet = null;
                    if (deleteDate == null)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatementWithLimit("pulse_trashbin_by_delete_timestamp", new List<string>(), new List<string> { "user_id" }, limit));
                        pulseRowSet = mainSession.Execute(ps.Bind(requesterUserId));
                    }
                    else
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("pulse_trashbin_by_delete_timestamp", new List<string>(), new List<string> { "user_id" }, "deleted_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        pulseRowSet = mainSession.Execute(ps.Bind(requesterUserId, deleteDate));
                    }

                    List<Row> trashbinRowSetList = new List<Row>();

                    if (pulseRowSet != null)
                    {
                        trashbinRowSetList = pulseRowSet.ToList();

                        foreach (Row binRow in trashbinRowSetList)
                        {
                            string pulseId = binRow.GetValue<string>("pulse_id");
                            int pulseType = binRow.GetValue<int>("pulse_type");

                            DateTime deletedTimestamp = binRow.GetValue<DateTime>("deleted_on_timestamp");
                            deleteDate = deletedTimestamp;

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_privacy", new List<string>(), new List<string> { "pulse_id" }));
                            Row privacyRow = mainSession.Execute(ps.Bind(pulseId)).FirstOrDefault();

                            if (privacyRow == null)
                            {
                                continue;
                            }

                            bool isForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = privacyRow.GetValue<bool>("is_for_department");
                            bool isForUser = privacyRow.GetValue<bool>("is_for_user");

                            if (CheckPulseForCurrentUser(requesterUserId, pulseId, isForEveryone, isForDepartment, isForUser, departmentIds, mainSession))
                            {
                                if (response.Pulses.FindIndex(pulse => pulse.PulseId.Equals(pulseId)) < 0 && response.Pulses.Count < limit)
                                {
                                    Pulse pulse = null;
                                    if (pulseType != (int)PulseTypeEnum.Dynamic)
                                    {
                                        pulse = feedManager.SelectSinglePulse(pulseId, companyId, (int)PulseQueryEnum.FullDetail, timezoneOffset, false, false, 0, 0, mainSession).Pulse;
                                        if (pulse != null)
                                        {
                                            int actionTaken = analyticPulse.SelectSingleActionTakenByUser(pulseId, pulseType, requesterUserId, analyticSession);
                                            pulse.ActionTaken = actionTaken;
                                        }
                                    }

                                    if (pulse != null)
                                    {
                                        pulse.DeletedDate = deletedTimestamp;
                                        response.Pulses.Add(pulse);
                                        if (response.Pulses.Count == limit)
                                        {
                                            isCompleted = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                    if (trashbinRowSetList.Count == 0)
                    {
                        isCompleted = true;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateImpressionResponse UpdateImpression(string requesterUserId, string companyId, string pulseId)
        {
            PulseUpdateImpressionResponse response = new PulseUpdateImpressionResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }
                else
                {
                    Thread updateThread = new Thread(() => UpdateImpressionThread(requesterUserId, companyId, pulseId, vh, mainSession, analyticSession));
                    updateThread.Start();
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void UpdateImpressionThread(string requesterUserId, string companyId, string pulseId, ValidationHandler vh, ISession mainSession, ISession analyticSession)
        {
            try
            {
                Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                if (pulseRow != null)
                {
                    new AnalyticPulse().UpdateImpression(pulseId, requesterUserId, analyticSession);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public PulseUpdateResponse MoveToTrashBin(string pulseId, int pulseType, string requesterUserId, string companyId)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                if (pulseType != (int)PulseTypeEnum.Dynamic)
                {
                    Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                    if (pulseRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }
                }
                else
                {

                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_inbox_by_user", new List<string>(), new List<string> { "user_id", "pulse_id" }));
                Row inboxRow = mainSession.Execute(ps.Bind(requesterUserId, pulseId)).FirstOrDefault();

                if (inboxRow != null)
                {
                    DateTime savedTimestamp = inboxRow.GetValue<DateTime>("saved_on_timestamp");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_inbox_by_user", new List<string> { "user_id", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId));
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_inbox_by_pulse", new List<string> { "user_id", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId));
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_inbox_by_save_timestamp", new List<string> { "user_id", "pulse_id", "saved_on_timestamp" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId, savedTimestamp));

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_trashbin_by_user", new List<string>(), new List<string> { "user_id", "pulse_id" }));
                    Row trashRow = mainSession.Execute(ps.Bind(requesterUserId, pulseId)).FirstOrDefault();
                    if (trashRow == null)
                    {
                        DateTime currentTime = DateTime.UtcNow;
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_trashbin_by_user", new List<string> { "user_id", "pulse_id", "pulse_type", "deleted_on_timestamp" }));
                        updateBatch.Add(ps.Bind(requesterUserId, pulseId, pulseType, currentTime));
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_trashbin_by_pulse", new List<string> { "user_id", "pulse_id", "pulse_type", "deleted_on_timestamp" }));
                        updateBatch.Add(ps.Bind(requesterUserId, pulseId, pulseType, currentTime));
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_trashbin_by_delete_timestamp", new List<string> { "user_id", "pulse_id", "pulse_type", "deleted_on_timestamp" }));
                        updateBatch.Add(ps.Bind(requesterUserId, pulseId, pulseType, currentTime));
                    }

                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse RestoreToInbox(string pulseId, int pulseType, string requesterUserId, string companyId)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                if (pulseType != (int)PulseTypeEnum.Dynamic)
                {
                    Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                    if (pulseRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }
                }
                else
                {

                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_trashbin_by_user", new List<string>(), new List<string> { "user_id", "pulse_id" }));
                Row trashRow = mainSession.Execute(ps.Bind(requesterUserId, pulseId)).FirstOrDefault();

                if (trashRow != null)
                {
                    DateTime deletedTimestamp = trashRow.GetValue<DateTime>("deleted_on_timestamp");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_user", new List<string> { "user_id", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId));
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_pulse", new List<string> { "user_id", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId));
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_delete_timestamp", new List<string> { "user_id", "pulse_id", "deleted_on_timestamp" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId, deletedTimestamp));

                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_inbox_by_user", new List<string>(), new List<string> { "user_id", "pulse_id" }));
                    Row inboxRow = mainSession.Execute(ps.Bind(requesterUserId, pulseId)).FirstOrDefault();
                    if (inboxRow == null)
                    {
                        DateTime currentTime = DateTime.UtcNow;
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_inbox_by_user", new List<string> { "user_id", "pulse_id", "pulse_type", "saved_on_timestamp" }));
                        updateBatch.Add(ps.Bind(requesterUserId, pulseId, pulseType, currentTime));
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_inbox_by_pulse", new List<string> { "user_id", "pulse_id", "pulse_type", "saved_on_timestamp" }));
                        updateBatch.Add(ps.Bind(requesterUserId, pulseId, pulseType, currentTime));
                        ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_inbox_by_save_timestamp", new List<string> { "user_id", "pulse_id", "pulse_type", "saved_on_timestamp" }));
                        updateBatch.Add(ps.Bind(requesterUserId, pulseId, pulseType, currentTime));
                    }

                    mainSession.Execute(deleteBatch);
                    mainSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseUpdateResponse RemoveFromTrashBin(string pulseId, int pulseType, string requesterUserId, string companyId)
        {
            PulseUpdateResponse response = new PulseUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, mainSession);
                if (userRow == null)
                {
                    Log.Error("Invalid user: " + requesterUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                }

                if (pulseType != (int)PulseTypeEnum.Dynamic)
                {
                    Row pulseRow = vh.ValidatePulseSingle(companyId, pulseId, mainSession);
                    if (pulseRow == null)
                    {
                        Log.Error("Invalid pulse: " + pulseId);
                        response.ErrorCode = Convert.ToInt32(ErrorCode.PulseInvalid);
                        response.ErrorMessage = ErrorMessage.PulseInvalid;
                        return response;
                    }
                }
                else
                {

                }

                BatchStatement deleteBatch = new BatchStatement();

                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_trashbin_by_user", new List<string>(), new List<string> { "user_id", "pulse_id" }));
                Row trashRow = mainSession.Execute(ps.Bind(requesterUserId, pulseId)).FirstOrDefault();

                if (trashRow != null)
                {
                    DateTime deletedTimestamp = trashRow.GetValue<DateTime>("deleted_on_timestamp");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_user", new List<string> { "user_id", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId));
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_pulse", new List<string> { "user_id", "pulse_id" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId));
                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_trashbin_by_delete_timestamp", new List<string> { "user_id", "pulse_id", "deleted_on_timestamp" }));
                    deleteBatch.Add(ps.Bind(requesterUserId, pulseId, deletedTimestamp));

                    mainSession.Execute(deleteBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseValidationResponse ValidateDate(DateTime startDate, DateTime? endDate, DateTime currentTime)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                if (endDate.HasValue && endDate.Value != DateTime.MinValue)
                {
                    if (endDate <= currentTime)
                    {
                        Log.Error("Pulse must end after current date");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.PulseEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.PulseEndDateEarlierThanToday;
                        return response;
                    }

                    if (startDate >= endDate)
                    {
                        Log.Error("Pulse must start before end date");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.PulseEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.PulseEndDateEarlierThanStartDate;
                        return response;
                    }
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseValidationResponse ValidateForUpdate(DateTime currentStartDate, DateTime? currentEndDate, DateTime newStartDate, DateTime? newEndDate, DateTime currentTime, int currentStatus, int progress, string pulseId = null, string deckId = null, bool isValidateDeck = false)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                //currentStartDate = currentStartDate.ToUniversalTime();
                if (currentEndDate.HasValue)
                {
                    currentEndDate = currentEndDate.Value;//.ToUniversalTime();
                }

                if (currentStatus != (int)PulseStatusEnum.Unlisted)
                {
                    // Status active or hidden
                    // Check progress
                    // Change of start date
                    if (currentStartDate != newStartDate)
                    {
                        if (progress != (int)ProgressStatusEnum.Upcoming)
                        {
                            if (!isValidateDeck)
                            {
                                Log.Error("Pulse in progress, start date cannot be changed: " + pulseId);
                                response.ErrorCode = Convert.ToInt16(ErrorCode.PulseStartDateCannotBeModified);
                                response.ErrorMessage = ErrorMessage.PulseStartDateCannotBeModified;
                                return response;
                            }
                            else
                            {
                                Log.Error("Deck in progress, start date cannot be changed: " + deckId);
                                response.ErrorCode = Convert.ToInt16(ErrorCode.DeckStartDateCannotBeModified);
                                response.ErrorMessage = ErrorMessage.DeckStartDateCannotBeModified;
                                return response;
                            }

                        }
                    }
                }

                // Change of end date
                if (currentEndDate != newEndDate)
                {
                    if (newEndDate.HasValue)
                    {
                        if (newEndDate <= currentTime)
                        {
                            if (!isValidateDeck)
                            {
                                Log.Error("Pulse must end after current date");
                                response.ErrorCode = Convert.ToInt16(ErrorCode.PulseEndDateEarlierThanToday);
                                response.ErrorMessage = ErrorMessage.PulseEndDateEarlierThanToday;
                                return response;
                            }
                            else
                            {
                                Log.Error("Deck must end after current date");
                                response.ErrorCode = Convert.ToInt16(ErrorCode.DeckEndDateEarlierThanToday);
                                response.ErrorMessage = ErrorMessage.DeckEndDateEarlierThanToday;
                                return response;
                            }

                        }

                        if (newStartDate >= newEndDate)
                        {
                            if (!isValidateDeck)
                            {
                                Log.Error("Pulse must start before end date");
                                response.ErrorCode = Convert.ToInt16(ErrorCode.PulseEndDateEarlierThanStartDate);
                                response.ErrorMessage = ErrorMessage.PulseEndDateEarlierThanStartDate;
                                return response;
                            }
                            else
                            {
                                Log.Error("Deck must start before end date");
                                response.ErrorCode = Convert.ToInt16(ErrorCode.DeckEndDateEarlierThanStartDate);
                                response.ErrorMessage = ErrorMessage.DeckEndDateEarlierThanStartDate;
                                return response;

                            }

                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseValidationResponse ValidateStatus(int status)
        {
            PulseValidationResponse response = new PulseValidationResponse();
            response.Success = false;
            try
            {
                if (status != (int)PulseStatusEnum.Unlisted && status != (int)PulseStatusEnum.Active && status != (int)PulseStatusEnum.Hidden && status != (int)PulseStatusEnum.Deleted)
                {
                    Log.Error("Invalid status");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.PulseInvalidStatus);
                    response.ErrorMessage = ErrorMessage.PulseInvalidStatus;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool CheckForSingleCorrectAction(string pulseId, int type, int action, ISession mainSession)
        {
            bool isCorrect = false;

            try
            {
                Dictionary<string, List<PulseAction>> dict = SelectActionForPulse(type);

                List<PulseAction> actions = new List<PulseAction>();
                actions.AddRange(dict[DefaultResource.PulseActionBeforeKey]);
                if (dict.ContainsKey(DefaultResource.PulseActionAfterKey))
                {
                    actions.AddRange(dict[DefaultResource.PulseActionAfterKey]);
                }

                if (actions.FirstOrDefault(a => a.Code == action) != null)
                {
                    isCorrect = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isCorrect;
        }

        public int SelectProgress(DateTime startDate, DateTime? endDate, DateTime currentTime)
        {
            int progress = (int)ProgressStatusEnum.Upcoming;
            try
            {
                startDate = startDate.AddSeconds(-startDate.Second);
                startDate = startDate.AddMilliseconds(-startDate.Millisecond);

                if (startDate <= currentTime)
                {
                    progress = (int)ProgressStatusEnum.Live;

                    if (endDate.HasValue)
                    {
                        endDate = endDate.Value.AddSeconds(-endDate.Value.Second);
                        endDate = endDate.Value.AddMilliseconds(-endDate.Value.Millisecond);

                        if (endDate <= currentTime)
                        {
                            progress = (int)ProgressStatusEnum.Completed;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return progress;
        }

        public List<BoundStatement> CreatePrivacy(string id, List<string> targetedDepartmentIds, List<string> targetedUserIds, ISession session, bool isForSingle = true)
        {
            List<BoundStatement> boundStatements = new List<BoundStatement>();

            try
            {
                PreparedStatement ps = null;
                bool isForDepartment = targetedDepartmentIds == null || targetedDepartmentIds.Count == 0 ? false : true;
                bool isForUser = targetedUserIds == null || targetedUserIds.Count == 0 ? false : true;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                string idColumn = isForSingle ? "pulse_id" : "deck_id";
                string privacyTable = isForSingle ? "pulse_privacy" : "deck_privacy";
                string departmentPulseTable = isForSingle ? "department_targeted_pulse" : "department_targeted_deck";
                string pulseDepartmentTable = isForSingle ? "pulse_targeted_department" : "deck_targeted_department";
                string userPulseTable = isForSingle ? "user_targeted_pulse" : "user_targeted_deck";
                string pulseUserTable = isForSingle ? "pulse_targeted_user" : "deck_targeted_user";

                ps = session.Prepare(CQLGenerator.InsertStatement(privacyTable, new List<string> { idColumn, "is_for_everyone", "is_for_department", "is_for_user", "is_for_custom_group" }));
                boundStatements.Add(ps.Bind(id, isForEveryone, isForDepartment, isForUser, false));

                if (isForDepartment)
                {
                    foreach (string departmentId in targetedDepartmentIds)
                    {
                        ps = session.Prepare(CQLGenerator.InsertStatement(departmentPulseTable, new List<string> { "department_id", idColumn }));
                        boundStatements.Add(ps.Bind(departmentId, id));

                        ps = session.Prepare(CQLGenerator.InsertStatement(pulseDepartmentTable, new List<string> { "department_id", idColumn }));
                        boundStatements.Add(ps.Bind(departmentId, id));
                    }
                }

                if (isForUser)
                {
                    foreach (string userId in targetedUserIds)
                    {
                        ps = session.Prepare(CQLGenerator.InsertStatement(pulseUserTable, new List<string> { "user_id", idColumn }));
                        boundStatements.Add(ps.Bind(userId, id));

                        ps = session.Prepare(CQLGenerator.InsertStatement(userPulseTable, new List<string> { "user_id", idColumn }));
                        boundStatements.Add(ps.Bind(userId, id));
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return boundStatements;
        }

        public bool CheckPulseForCurrentUser(string userId, string id, bool isForEveryone, bool isForDepartment, bool isForUser, List<string> departmentIds, ISession session, bool isForSingle = true)
        {

            bool isPulseForCurrentUser = false;

            string idColumn = isForSingle ? "pulse_id" : "deck_id";
            string privacyTable = isForSingle ? "pulse_privacy" : "deck_privacy";
            string departmentPulseTable = isForSingle ? "department_targeted_pulse" : "department_targeted_deck";
            string pulseDepartmentTable = isForSingle ? "pulse_targeted_department" : "deck_targeted_department";
            string userPulseTable = isForSingle ? "user_targeted_pulse" : "user_targeted_deck";
            string pulseUserTable = isForSingle ? "pulse_targeted_user" : "deck_targeted_user";

            if (isForEveryone)
            {
                isPulseForCurrentUser = true;
            }
            else
            {
                if (isForDepartment)
                {
                    foreach (string departmentId in departmentIds)
                    {
                        PreparedStatement psPulseDepartment = session.Prepare(CQLGenerator.SelectStatement(pulseDepartmentTable, new List<string>(), new List<string> { idColumn, "department_id" }));
                        BoundStatement bsPulseDepartment = psPulseDepartment.Bind(id, departmentId);

                        Row departmentPrivacyRow = session.Execute(bsPulseDepartment).FirstOrDefault();

                        if (departmentPrivacyRow != null)
                        {
                            isPulseForCurrentUser = true;
                            break;
                        }
                    }
                }

                if (!isPulseForCurrentUser && isForUser)
                {
                    PreparedStatement psPulseUser = session.Prepare(CQLGenerator.SelectStatement(pulseUserTable, new List<string>(), new List<string> { idColumn, "user_id" }));
                    BoundStatement bsPulseUser = psPulseUser.Bind(id, userId);

                    Row userPrivacyRow = session.Execute(bsPulseUser).FirstOrDefault();

                    if (userPrivacyRow != null)
                    {
                        isPulseForCurrentUser = true;
                    }
                }
            }

            return isPulseForCurrentUser;
        }

        public int SelectPulsePriority(int pulseType)
        {
            int priority = 0;

            switch (pulseType)
            {
                case (int)PulseTypeEnum.Announcement:
                    priority = (int)PulsePriorityEnum.Announcement;
                    break;
                case (int)PulseTypeEnum.Event:
                    priority = (int)PulsePriorityEnum.Event;
                    break;
                case (int)PulseTypeEnum.Survey:
                    priority = (int)PulsePriorityEnum.Survey;
                    break;
                case (int)PulseTypeEnum.Topic:
                    priority = (int)PulsePriorityEnum.Topic;
                    break;
                case (int)PulseTypeEnum.Dynamic:
                    priority = (int)PulsePriorityEnum.Dynamic;
                    break;
            }

            return priority;
        }

        public List<BoundStatement> UpdatePrivacy(string id, List<string> newTargetedDepartmentIds, List<string> newTargetedUserIds, ISession session, bool isForSingle = true)
        {
            try
            {
                PreparedStatement ps = null;
                bool isForDepartment = newTargetedDepartmentIds == null || newTargetedDepartmentIds.Count == 0 ? false : true;
                bool isForUser = newTargetedUserIds == null || newTargetedUserIds.Count == 0 ? false : true;
                bool isForEveryone = !isForDepartment && !isForUser ? true : false;

                string idColumn = isForSingle ? "pulse_id" : "deck_id";
                string privacyTable = isForSingle ? "pulse_privacy" : "deck_privacy";
                string departmentPulseTable = isForSingle ? "department_targeted_pulse" : "department_targeted_deck";
                string pulseDepartmentTable = isForSingle ? "pulse_targeted_department" : "deck_targeted_department";
                string userPulseTable = isForSingle ? "user_targeted_pulse" : "user_targeted_deck";
                string pulseUserTable = isForSingle ? "pulse_targeted_user" : "deck_targeted_user";

                BatchStatement deleteBatch = new BatchStatement();

                ps = session.Prepare(CQLGenerator.SelectStatement(pulseDepartmentTable,
                    new List<string>(), new List<string> { idColumn }));
                RowSet departmentPrivacyRowset = session.Execute(ps.Bind(id));

                foreach (Row departmentPrivacyRow in departmentPrivacyRowset)
                {
                    string departmentId = departmentPrivacyRow.GetValue<string>("department_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement(departmentPulseTable, new List<string> { "department_id", idColumn }));
                    deleteBatch.Add(ps.Bind(departmentId, id));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement(pulseDepartmentTable, new List<string> { idColumn }));
                deleteBatch.Add(ps.Bind(id));

                ps = session.Prepare(CQLGenerator.SelectStatement(pulseUserTable,
                    new List<string>(), new List<string> { idColumn }));
                RowSet userPrivacyRowset = session.Execute(ps.Bind(id));

                foreach (Row userPrivacyRow in userPrivacyRowset)
                {
                    string userId = userPrivacyRow.GetValue<string>("user_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement(userPulseTable, new List<string> { "user_id", idColumn }));
                    deleteBatch.Add(ps.Bind(userId, id));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement(pulseUserTable, new List<string> { idColumn }));
                deleteBatch.Add(ps.Bind(id));

                session.Execute(deleteBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return CreatePrivacy(id, newTargetedDepartmentIds, newTargetedUserIds, session, isForSingle);
        }

        public List<User> SelectPulseTargetedAudience(string id, string companyId, bool isForEveryone, bool isForDepartment, bool isForUser, ISession mainSession, bool isForSingle = true, string searchId = null)
        {
            List<User> audience = new List<User>();

            try
            {
                string idColumn = isForSingle ? "pulse_id" : "deck_id";
                string privacyTable = isForSingle ? "pulse_privacy" : "deck_privacy";
                string departmentPulseTable = isForSingle ? "department_targeted_pulse" : "department_targeted_deck";
                string pulseDepartmentTable = isForSingle ? "pulse_targeted_department" : "deck_targeted_department";
                string userPulseTable = isForSingle ? "user_targeted_pulse" : "user_targeted_deck";
                string pulseUserTable = isForSingle ? "pulse_targeted_user" : "deck_targeted_user";

                User userManager = new User();

                if (isForEveryone)
                {
                    if (string.IsNullOrEmpty(searchId))
                    {
                        audience = userManager.GetAllUserForAdmin(null, companyId, null, 0, (int)User.AccountStatus.CODE_ACTIVE, null, false, false, null, mainSession).Users;
                    }
                    else
                    {
                        audience = userManager.SelectAllUsersByDepartmentIds(new List<string> { searchId }, null, companyId, mainSession).Users;
                    }

                }
                else
                {
                    PreparedStatement ps = null;
                    if (isForDepartment)
                    {
                        List<string> departmentIds = new List<string>();
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement(pulseDepartmentTable, new List<string>(), new List<string> { idColumn }));
                        RowSet departmentRowset = mainSession.Execute(ps.Bind(id));

                        foreach (Row departmentRow in departmentRowset)
                        {
                            string departmentId = departmentRow.GetValue<string>("department_id");
                            if (string.IsNullOrEmpty(searchId))
                            {
                                departmentIds.Add(departmentId);
                            }
                            else
                            {
                                if (departmentId.Equals(searchId))
                                {
                                    departmentIds.Add(departmentId);
                                }
                            }

                        }

                        audience = userManager.SelectAllUsersByDepartmentIds(departmentIds, null, companyId, mainSession).Users;
                    }

                    if (isForUser)
                    {
                        ps = mainSession.Prepare(CQLGenerator.SelectStatement(pulseUserTable, new List<string>(), new List<string> { idColumn }));
                        RowSet userRowset = mainSession.Execute(ps.Bind(id));

                        foreach (Row userRow in userRowset)
                        {
                            string userId = userRow.GetValue<string>("user_id");
                            if (audience.FirstOrDefault(u => u.UserId == userId) == null)
                            {
                                User user = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                                if (user != null)
                                {
                                    if (string.IsNullOrEmpty(searchId))
                                    {
                                        audience.Add(user);
                                    }
                                    else
                                    {
                                        if (user.Departments.FirstOrDefault(d => d.Id.Equals(searchId)) != null)
                                        {
                                            audience.Add(user);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return audience;
        }

        public void DeleteFromS3(string companyId, string pulseId, string newUpdatedImageUrl = null)
        {
            try
            {
                bool isDeleteFolder = true;
                String bucketName = "cocadre-" + companyId.ToLower();

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;
                    listRequest.Prefix = "pulses/" + pulseId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        if (imageObject.Size <= 0)
                        {
                            continue;
                        }

                        if (!string.IsNullOrEmpty(newUpdatedImageUrl))
                        {
                            isDeleteFolder = false;
                            //pulses/PF7168c6ceedc34e709790cd63398c9668/U811e9a9236c140ad98a211a428b6dc84_20160527072241738_original.jpg 
                            string[] stringToken = imageObject.Key.Split('/');
                            string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                            string[] fileNames = fileNamePath.Split('_');
                            string fileName = fileNames[0] + "_" + fileNames[1];

                            if (newUpdatedImageUrl.Contains(fileName))
                            {
                                continue;
                            }
                        }

                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }

                    if (isDeleteFolder)
                    {
                        // Delete folder
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = "pulses/" + pulseId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public PulseSelectPrivacyResponse SelectPulsePrivacy(string id, ISession session, bool isForSingle = true)
        {
            PulseSelectPrivacyResponse response = new PulseSelectPrivacyResponse();
            response.Success = false;
            try
            {
                string idColumn = isForSingle ? "pulse_id" : "deck_id";
                string table = isForSingle ? "pulse_privacy" : "deck_privacy";

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement(table, new List<string>(), new List<string> { idColumn }));
                Row privacyRow = session.Execute(ps.Bind(id)).FirstOrDefault();

                if (privacyRow != null)
                {
                    response.IsForEveryone = privacyRow.GetValue<bool>("is_for_everyone");
                    response.IsForDepartment = privacyRow.GetValue<bool>("is_for_department");
                    response.IsForUser = privacyRow.GetValue<bool>("is_for_user");

                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public PulseMappingStatementResponse MapToPulse(string pulseId, string companyId, int pulsePriority, int pulseType, bool isPrioritized, DateTime startDate, ISession mainSession)
        {
            PulseMappingStatementResponse response = new PulseMappingStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.InsertStatement("pulse_by_priority_with_timestamp",
                    new List<string> { "pulse_id", "company_id", "pulse_priority", "pulse_type", "is_prioritized", "start_timestamp" }));
                response.Statements.Add(ps.Bind(pulseId, companyId, pulsePriority, pulseType, isPrioritized, startDate));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public PulseMappingStatementResponse DeleteMapFromPulse(string pulseId, string companyId, int pulsePriority, int pulseType, bool isPrioritized, DateTime startDate, ISession mainSession)
        {
            PulseMappingStatementResponse response = new PulseMappingStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp",
                    new List<string> { "pulse_id", "company_id", "pulse_priority", "is_prioritized", "start_timestamp" }));
                response.Statements.Add(ps.Bind(pulseId, companyId, pulsePriority, isPrioritized, startDate));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }
}
