﻿namespace CassandraService.Entity
{
    public class ChallengeChoice
    {
        public string Id { get; set; }

        public string Content { get; set; }

        public string ContentImageUrl { get; set; }

        public string ContentImageMd5 { get; set; }
    }
}