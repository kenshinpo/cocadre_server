﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    public class AnalyticSearch
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public void UpdateSearchPersonnelAnalytics(string companyId, string searchedByUserId, string searchedUserId, DateTime searchedTimestamp, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;

                string searchId = UUIDGenerator.GenerateUniqueIDForSearch();

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("search_personnel_history",
                    new List<string> { "search_id", "company_id", "searched_by_user_id", "searched_user_id", "searched_on_timestamp" }));
                analyticSession.Execute(ps.Bind(searchId, companyId, searchedByUserId, searchedUserId, searchedTimestamp));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

        }

        public void UpdateSearchHashTagAnalytics(string companyId, string searchedByUserId, string searchedHashTag, DateTime searchedTimestamp, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;

                string searchId = UUIDGenerator.GenerateUniqueIDForSearch();

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("search_hashtag_history",
                    new List<string> { "search_id", "company_id", "searched_by_user_id", "searched_small_cap_tag", "searched_on_timestamp" }));
                analyticSession.Execute(ps.Bind(searchId, companyId, searchedByUserId, searchedHashTag, searchedTimestamp));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        public void UpdateSearchContentAnalytics(string companyId, string searchedByUserId, string content, DateTime searchedTimestamp, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;

                string searchId = UUIDGenerator.GenerateUniqueIDForSearch();

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("search_content_history",
                    new List<string> { "search_id", "company_id", "searched_by_user_id", "content", "searched_on_timestamp" }));
                analyticSession.Execute(ps.Bind(searchId, companyId, searchedByUserId, content, searchedTimestamp));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}
