﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class AssessmentCard
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum AssessmentCardQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum AssessmentCardType
        {
            SelectOne = 1,
            MultiChoice = 2,
            Text = 3,
            NumberRange = 4,
            DropList = 5,
            Instructional = 6,
            Accumulation = 7
        }

        public enum AssessmentCardOperator
        {
            Addition = 1,
            Substraction = 2,
            Multiplication = 3,
            Division = 4
        }

        [DataMember]
        public string CardId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public bool HasImage { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public List<AssessmentImage> CardImages { get; set; }

        [DataMember]
        public bool ToBeSkipped { get; set; }

        [DataMember]
        public bool IsOptionRandomized { get; set; }

        [DataMember]
        public bool HasPageBreak { get; set; }

        [DataMember]
        public int Paging { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int BackgroundType { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public int TotalPointsAllocated { get; set; }

        [DataMember]
        public List<AssessmentCardOption> Options { get; set; }

        public AssessmentCreateCardResponse CreateAssessmentCard(string cardId,
                                                                int type,
                                                                string content,
                                                                List<AssessmentImage> images,
                                                                bool toBeSkipped,
                                                                bool isOptionRandomized,
                                                                bool hasPageBreak,
                                                                int backgoundType,
                                                                string note,
                                                                string assessmentId,
                                                                string adminUserId,
                                                                List<AssessmentCardOption> options,
                                                                int totalPointsAllocation = 0,
                                                                int miniOptionToSelect = 0,
                                                                int maxOptionToSelect = 0,
                                                                int startRangePosition = 0,
                                                                int maxRange = 0,
                                                                string maxRangeLabel = null,
                                                                int midRange = 0,
                                                                string midRangeLabel = null,
                                                                int minRange = 0,
                                                                string minRangeLabel = null,
                                                                ISession session = null,
                                                                int cardOrdering = -1)
        {
            AssessmentCreateCardResponse response = new AssessmentCreateCardResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                    if (assessmentRow == null)
                    {
                        Log.Error("Invalid assessmentId: " + assessmentId);
                        response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInvalid);
                        response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                        return response;
                    }
                }

                // Check content
                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Invalid content");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.AssessmentCardMissingContent);
                    response.ErrorMessage = ErrorMessage.AssessmentCardMissingContent;
                    return response;
                }

                if (string.IsNullOrEmpty(cardId))
                {
                    cardId = UUIDGenerator.GenerateUniqueIDForRSCard();
                }

                DateTime currentTime = DateTime.UtcNow;

                AssessmentCardOption optionManagement = new AssessmentCardOption();
                AssessmentCreateOptionResponse optionResponse = new AssessmentCreateOptionResponse();
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                // Setting to default
                switch (type)
                {
                    case (int)AssessmentCardType.SelectOne:
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)AssessmentCardType.MultiChoice:
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        break;
                    case (int)AssessmentCardType.Text:
                        isOptionRandomized = false;
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)AssessmentCardType.NumberRange:
                        isOptionRandomized = false;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)AssessmentCardType.DropList:
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)AssessmentCardType.Instructional:
                        toBeSkipped = false;
                        isOptionRandomized = false;
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)AssessmentCardType.Accumulation:
                        startRangePosition = 0;
                        minRange = 0;
                        minRangeLabel = null;
                        midRange = 0;
                        midRangeLabel = null;
                        maxRange = 0;
                        maxRangeLabel = null;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                }

                // Check points allocation
                if (!(type == (int)AssessmentCardType.Text || type == (int)AssessmentCardType.Instructional))
                {

                    if ((type == (int)AssessmentCardType.Accumulation) && (totalPointsAllocation <= 0))
                    {
                        response.ErrorCode = Int16.Parse(ErrorCode.AssessmentCardOptionInvalidPointsAllocation);
                        response.ErrorMessage = ErrorMessage.AssessmentCardOptionInvalidPointsAllocation;
                        return response;
                    }

                    if (options == null || options.Count == 0)
                    {
                        response.ErrorCode = Int16.Parse(ErrorCode.AssessmentCardNeedAtLeastOneOption);
                        response.ErrorMessage = ErrorMessage.AssessmentCardNeedAtLeastOneOption;
                        return response;
                    }

                    // Create options
                    if (type == (int)AssessmentCardType.MultiChoice)
                    {
                        if (miniOptionToSelect <= 0 || maxOptionToSelect == 0 || maxOptionToSelect <= miniOptionToSelect || miniOptionToSelect > options.Count || maxOptionToSelect > options.Count)
                        {
                            Log.Error("Mismatch in option selection values");
                            response.ErrorCode = Int16.Parse(ErrorCode.AssessmentCardOptionInvalidSelectedOptionNumber);
                            response.ErrorMessage = ErrorMessage.AssessmentCardOptionInvalidSelectedOptionNumber;
                            return response;
                        }
                    }
                    optionResponse = optionManagement.CreateOptions(adminUserId, assessmentId, cardId, options, currentTime, session);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.Statements)
                    {
                        batch.Add(bs);
                    }
                }

                ps = session.Prepare(CQLGenerator.CountStatement("assessment_card_order", new List<string> { "assessment_id" }));
                if (cardOrdering == -1)
                {
                    cardOrdering = (int)session.Execute(ps.Bind(assessmentId)).FirstOrDefault().GetValue<long>("count") + 1;
                }

                bool cardHasImage = images.Count > 0 ? true : false;
                if (cardHasImage)
                {
                    AssessmentCreateStatementResponse statementResponse = CreateImage(cardId, images, session);
                    if (!statementResponse.Success)
                    {
                        response.ErrorCode = statementResponse.ErrorCode;
                        response.ErrorMessage = statementResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in statementResponse.Statements)
                    {
                        batch.Add(bs);
                    }
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("assessment_card",
                    new List<string> { "assessment_id", "id", "type", "has_image", "content", "to_be_skipped", "is_option_randomized", "mini_option_to_select", "max_option_to_select", "has_page_break", "ordering", "background_image_type", "note", "max_range", "min_range", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp", "range_start_position", "max_range_label", "mid_range", "mid_range_label", "min_range_label", "total_points" }));
                batch.Add(ps.Bind(assessmentId, cardId, type, cardHasImage, content, toBeSkipped, isOptionRandomized, miniOptionToSelect, maxOptionToSelect, hasPageBreak, cardOrdering, backgoundType, note, maxRange, minRange, adminUserId, currentTime, adminUserId, currentTime, startRangePosition, maxRangeLabel, midRange, midRangeLabel, minRangeLabel, totalPointsAllocation));


                ps = session.Prepare(CQLGenerator.InsertStatement("assessment_card_order",
                    new List<string> { "assessment_id", "card_id", "ordering" }));
                batch.Add(ps.Bind(assessmentId, cardId, cardOrdering));

                session.Execute(batch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentUpdateResponse UpdateAssessmentCard(string cardId,
                                                             int newType,
                                                             string newContent,
                                                             List<AssessmentImage> newImages,
                                                             bool newToBeSkipped,
                                                             bool newIsOptionRandomized,
                                                             bool newHasPageBreak,
                                                             int newBackgoundType,
                                                             string newNote,
                                                             string assessmentId,
                                                             string adminUserId,
                                                             List<AssessmentCardOption> newOptions,
                                                             int newTotalPointsAllocation = 0,
                                                             int newMiniOptionToSelect = 0,
                                                             int newMaxOptionToSelect = 0,
                                                             int newStartRangePosition = 0,
                                                             int newMaxRange = 0,
                                                             string newMaxRangeLabel = null,
                                                             int newMidRange = 0,
                                                             string newMidRangeLabel = null,
                                                             int newMinRange = 0,
                                                             string newMinRangeLabel = null)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                if (assessmentRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateAssessmentCard(assessmentId, cardId, session);
                if (cardRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                List<string> newImageUrls = newImages.Select(i => i.ImageUrl).ToList();

                int ordering = cardRow.GetValue<int>("ordering");

                DeleteCard(adminUserId, assessmentId, cardId, false, cardRow, newImageUrls, session);
                AssessmentCreateCardResponse createResponse =  CreateAssessmentCard(cardId, newType, newContent, newImages, newToBeSkipped, newIsOptionRandomized, newHasPageBreak, newBackgoundType, newNote, assessmentId, adminUserId, newOptions, newTotalPointsAllocation, newMiniOptionToSelect, newMaxOptionToSelect, newStartRangePosition, newMaxRange, newMaxRangeLabel, newMidRange, newMidRangeLabel, newMinRange, newMinRangeLabel, session, ordering);

                response.Success = createResponse.Success;
                response.ErrorCode = createResponse.ErrorCode;
                response.ErrorMessage = createResponse.ErrorMessage;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectAllCardsResponse SelectAllCards(string assessmentId, int queryType, ISession mainSession, bool isUserAnswerRequired = false, int attempt = 0, string answeredByUserId = null, ISession analyticSession = null)
        {
            AssessmentSelectAllCardsResponse response = new AssessmentSelectAllCardsResponse();
            response.Cards = new List<AssessmentCard>();
            response.Success = false;
            try
            {
                AnalyticAssessment analyticAssessment = new AnalyticAssessment();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_card_order", new List<string>(), new List<string> { "assessment_id" }));
                RowSet allCardRowset = mainSession.Execute(ps.Bind(assessmentId));

                int paging = 1;
                foreach (Row cardRow in allCardRowset)
                {
                    string cardId = cardRow.GetValue<string>("card_id");
                    AssessmentCard card = SelectCard(assessmentId, cardId, queryType, mainSession).Card;
                    if (card != null)
                    {
                        card.Paging = paging;

                        if (card.HasPageBreak)
                        {
                            paging++;
                        }

                        if (isUserAnswerRequired)
                        {
                            foreach (AssessmentCardOption option in card.Options)
                            {
                                AssessmentSelectCardOptionAnsweredByUserResponse answerResponse = analyticAssessment.SelectCardOptionAnswer(answeredByUserId, cardId, option.OptionId, attempt, analyticSession);
                                option.IsSelected = answerResponse.IsSelected;
                                option.PointAllocated = answerResponse.PointAllocated;
                            }
                        }
                        response.Cards.Add(card);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentSelectCardResponse SelectFullDetailAssessmentCard(string assessmentId, string cardId, string adminUserId)
        {
            AssessmentSelectCardResponse response = new AssessmentSelectCardResponse();
            response.Card = null;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                // Check admin

                response = SelectCard(assessmentId, cardId, (int)AssessmentCardQueryType.FullDetail, session);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentUpdateResponse DeleteAllCards(string assessmentId, ISession session)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                List<AssessmentCard> cards = SelectAllCards(assessmentId, (int)AssessmentCardQueryType.Basic, session).Cards;

                foreach (AssessmentCard card in cards)
                {
                    // Remove all options
                    new AssessmentCardOption().DeleteAllOptions(assessmentId, card.CardId, session);

                    if (card.HasImage)
                    {
                        ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_card_image", new List<string> { "card_id" }));
                        deleteBatch.Add(ps.Bind(card.CardId));

                        DeleteFromS3(assessmentId, card.CardId);
                    }
                }

                session.Execute(deleteBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentUpdateResponse DeleteCard(string adminUserId, string assessmentId, string cardId, bool isUpdateOrdering = true, Row cardRow = null, List<string> updatedCardImageUrls = null, ISession session = null)
        {
            AssessmentUpdateResponse response = new AssessmentUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                if (session == null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    // Check Admin

                    Row assessmentRow = vh.ValidateAssessment(assessmentId, session);
                    if (assessmentRow == null)
                    {
                        Log.Error("Invalid assessmentId: " + assessmentId);
                        response.ErrorCode = Int16.Parse(ErrorCode.AssessmentInvalid);
                        response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                        return response;
                    }

                    cardRow = vh.ValidateAssessmentCard(assessmentId, cardId, session);
                    if (cardRow == null)
                    {
                        Log.Error("Invalid cardId: " + cardId);
                        response.ErrorCode = Int16.Parse(ErrorCode.AssessmentCardAlreadyDeleted);
                        response.ErrorMessage = ErrorMessage.AssessmentCardAlreadyDeleted;
                        return response;
                    }
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_card", new List<string> { "assessment_id", "id" }));
                deleteBatch.Add(ps.Bind(assessmentId, cardId));

                if (isUpdateOrdering)
                {
                    int currentOrdering = cardRow.GetValue<int>("ordering");

                    // Update ordering
                    ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("assessment_card_order", new List<string>(), new List<string> { "assessment_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                    RowSet orderRowset = session.Execute(ps.Bind(assessmentId, currentOrdering));

                    foreach (Row orderRow in orderRowset)
                    {
                        int ordering = orderRow.GetValue<int>("ordering");
                        string updatedCardId = orderRow.GetValue<string>("card_id");

                        ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_card_order", new List<string> { "assessment_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(assessmentId, ordering, updatedCardId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("assessment_card_order", new List<string> { "assessment_id", "ordering", "card_id" }));
                        updateBatch.Add(ps.Bind(assessmentId, ordering - 1, updatedCardId));

                        ps = session.Prepare(CQLGenerator.UpdateStatement("assessment_card", new List<string> { "assessment_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                        updateBatch.Add(ps.Bind(ordering - 1, assessmentId, updatedCardId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_card_order", new List<string> { "assessment_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(assessmentId, currentOrdering, cardId));
                }

                // Remove all options
                new AssessmentCardOption().DeleteAllOptions(assessmentId, cardId, session);

                // Delete images
                if (cardRow.GetValue<bool>("has_image"))
                {
                    ps = session.Prepare(CQLGenerator.DeleteStatement("assessment_card_image", new List<string> { "card_id" }));
                    deleteBatch.Add(ps.Bind(cardId));

                    DeleteFromS3(assessmentId, cardId, updatedCardImageUrls);
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AssessmentAnswerCardResponse AnswerAssessmentCard(string answeredByUserId, string companyId, string assessmentId, string cardId, List<AssessmentCardOption> options)
        {
            AssessmentAnswerCardResponse response = new AssessmentAnswerCardResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row subscriptionRow = vh.ValidateAssessmentSubscription(assessmentId, companyId, mainSession);
                if (subscriptionRow == null)
                {
                    Log.Error("Invalid subscription");
                    response.ErrorCode = Convert.ToInt32(ErrorCode.SubscriptionInvalid);
                    response.ErrorMessage = ErrorMessage.SubscriptionInvalid;
                    return response;
                }

                Row assessmentRow = vh.ValidateAssessment(assessmentId, mainSession);
                if (assessmentRow == null)
                {
                    Log.Error("Invalid assessmentId: " + assessmentId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalid);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateAssessmentCard(assessmentId, cardId, mainSession);
                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentInvalidCard);
                    response.ErrorMessage = ErrorMessage.AssessmentInvalidCard;
                    return response;
                }

                int cardType = cardRow.GetValue<int>("type");
                if (cardType == (int)AssessmentCardType.Instructional)
                {
                    response.Success = true;
                    return response;
                }
                else if (cardType == (int)AssessmentCardType.Accumulation)
                {
                    int totalPoints = cardRow.GetValue<int>("total_points");

                    foreach (AssessmentCardOption option in options)
                    {
                        totalPoints -= option.PointAllocated;
                    }

                    if (totalPoints != 0)
                    {
                        response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentPointsDoesNotMatch);
                        response.ErrorMessage = ErrorMessage.AssessmentPointsDoesNotMatch;
                        return response;

                    }
                }
                else if (cardType == (int)AssessmentCardType.SelectOne || cardType == (int)AssessmentCardType.MultiChoice || cardType == (int)AssessmentCardType.DropList)
                {
                    foreach (AssessmentCardOption option in options)
                    {
                        option.PointAllocated = 0;
                    }
                }


                // Get number of retake left
                AnalyticAssessment analyticManager = new AnalyticAssessment();
                int numberOfRetakesAvailable = subscriptionRow.GetValue<int>("number_of_retake");

                AssessmentSelectAttemptByUserResponse attemptResponse = analyticManager.SelectAttemptByUser(assessmentId, answeredByUserId, numberOfRetakesAvailable, analyticSession);
                int currentAttempt = attemptResponse.CurrentAttempt;
                if (attemptResponse.IsCurrentAttemptCompleted)
                {
                    currentAttempt = attemptResponse.NextAttempt;
                }

                if(attemptResponse.NumberOfRetakesLeft < 0)
                {
                    response.ErrorCode = Convert.ToInt32(ErrorCode.AssessmentNoTriesLeft);
                    response.ErrorMessage = ErrorMessage.AssessmentNoTriesLeft;
                    return response;
                }

                // Set up tabulations
                List<OptionTabulationAllocation> allocations = SelectAllAllocationsByCard(cardId, mainSession);

                analyticManager.UpdateOptionAnswered(currentAttempt, assessmentId, answeredByUserId, cardId, cardType, options, allocations, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<OptionTabulationAllocation> SelectAllAllocationsByCard(string cardId, ISession mainSession)
        {
            List<OptionTabulationAllocation> allocations = new List<OptionTabulationAllocation>();
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("assessment_tabulation_allocation", new List<string>(), new List<string> { "card_id" }));
                RowSet allocationRowset = mainSession.Execute(ps.Bind(cardId));
                foreach (Row allocationRow in allocationRowset)
                {
                    OptionTabulationAllocation allocation = new OptionTabulationAllocation
                    {
                        TabulationId = allocationRow.GetValue<string>("tabulation_id"),
                        ValueOperator = allocationRow.GetValue<int>("value_operator"),
                        TabulationOperator = allocationRow.GetValue<int>("tabulation_operator"),
                        Ordering = allocationRow.GetValue<int>("ordering"),
                        Value = allocationRow.GetValue<int>("value"),
                        CardId = cardId,
                        OptionId = allocationRow.GetValue<string>("option_id")
                    };

                    allocations.Add(allocation);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return allocations;
        }

        private AssessmentSelectCardResponse SelectCard(string assessmentId, string cardId, int queryType, ISession session)
        {
            AssessmentSelectCardResponse response = new AssessmentSelectCardResponse();
            response.Card = null;
            response.Success = false;
            try
            {
                PreparedStatement ps = new PreparedStatement();
                ps = session.Prepare(CQLGenerator.SelectStatement("assessment_card", new List<string>(), new List<string> { "assessment_id", "id" }));
                Row cardRow = session.Execute(ps.Bind(assessmentId, cardId)).FirstOrDefault();

                if (cardRow == null)
                {
                    return response;
                }

                int type = cardRow.GetValue<int>("type");
                string content = cardRow.GetValue<string>("content");
                int ordering = cardRow.GetValue<int>("ordering");
                bool hasPageBreak = cardRow.GetValue<bool>("has_page_break");

                List<AssessmentImage> cardImages = new List<AssessmentImage>();
                bool hasImage = cardRow.GetValue<bool>("has_image");

                if (hasImage)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("assessment_card_image", new List<string>(), new List<string> { "card_id" }));
                    RowSet cardImageRowset = session.Execute(ps.Bind(cardId));
                    foreach (Row cardImageRow in cardImageRowset)
                    {
                        AssessmentImage image = new AssessmentImage
                        {
                            ImageId = cardImageRow.GetValue<string>("image_id"),
                            ImageUrl = cardImageRow.GetValue<string>("image_url"),
                            Ordering = cardImageRow.GetValue<int>("ordering"),
                        };

                        cardImages.Add(image);
                    }

                    cardImages = cardImages.OrderBy(c => c.Ordering).ToList();
                }

                bool isToBeSkipped = false;
                bool isOptionRandomized = false;
                int miniOptionToSelect = 0;
                int maxOptionToSelect = 0;

                int rangeStartPosition = 0;
                int maxRange = 0;
                string maxRangeLabel = string.Empty;
                int midRange = 0;
                string midRangeLabel = string.Empty;
                int minRange = 0;
                string minRangeLabel = string.Empty;

                int backgroundImageType = 0;
                string note = string.Empty;

                int totalPoints = 0;

                List<AssessmentCardOption> options = new List<AssessmentCardOption>();
                if (queryType == (int)AssessmentCardQueryType.FullDetail)
                {
                    isToBeSkipped = cardRow.GetValue<bool>("to_be_skipped");
                    isOptionRandomized = cardRow.GetValue<bool>("is_option_randomized");
                    miniOptionToSelect = cardRow.GetValue<int>("mini_option_to_select");
                    maxOptionToSelect = cardRow.GetValue<int>("max_option_to_select");

                    rangeStartPosition = cardRow.GetValue<int>("range_start_position");
                    maxRange = cardRow.GetValue<int>("max_range");
                    maxRangeLabel = cardRow.GetValue<string>("max_range_label");
                    midRange = cardRow.GetValue<int>("mid_range");
                    midRangeLabel = cardRow.GetValue<string>("mid_range_label");
                    minRange = cardRow.GetValue<int>("min_range");
                    minRangeLabel = cardRow.GetValue<string>("min_range_label");

                    backgroundImageType = cardRow.GetValue<int>("background_image_type");
                    note = cardRow.GetValue<string>("note");

                    totalPoints = cardRow.GetValue<int>("total_points");

                    options = new AssessmentCardOption().SelectAllOptions(assessmentId, cardId, session, true).Options.OrderBy(r => r.Ordering).ToList();
                }

                response.Card = new AssessmentCard
                {
                    CardId = cardId,
                    Type = type,
                    HasImage = hasImage,
                    CardImages = cardImages,
                    Content = content,
                    ToBeSkipped = isToBeSkipped,
                    IsOptionRandomized = isOptionRandomized,
                    HasPageBreak = hasPageBreak,
                    BackgroundType = backgroundImageType,
                    Note = note,
                    TotalPointsAllocated = totalPoints,
                    Options = options,
                    Ordering = ordering
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private AssessmentCreateStatementResponse CreateImage(string cardId, List<AssessmentImage> cardImages, ISession session)
        {
            AssessmentCreateStatementResponse response = new AssessmentCreateStatementResponse();
            response.Statements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                int imageOrdering = 1;
                foreach (AssessmentImage image in cardImages)
                {
                    string imageId = string.Format("{0}_image_{1}", cardId, imageOrdering);
                    string imageUrl = image.ImageUrl;

                    if (string.IsNullOrEmpty(imageUrl))
                    {
                        Log.Error("Missing card image url");
                        response.ErrorCode = Int16.Parse(ErrorCode.AssessmentMissingImageUrl);
                        response.ErrorMessage = ErrorMessage.AssessmentMissingImageUrl;
                        return response;
                    }

                    ps = session.Prepare(CQLGenerator.InsertStatement("assessment_card_image",
                        new List<string> { "card_id", "image_id", "image_url", "ordering" }));
                    response.Statements.Add(ps.Bind(cardId, imageId, imageUrl, imageOrdering));

                    imageOrdering++;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        private void DeleteFromS3(string assessmentId, string cardId, List<string> newUpdatedImageUrls = null)
        {
            try
            {
                bool isDeleteFolder = true;
                String bucketName = "cocadre-marketplace";

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;

                    listRequest.Prefix = assessmentId + "/" + cardId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        if (imageObject.Size <= 0)
                        {
                            continue;
                        }

                        if (newUpdatedImageUrls.Count > 0)
                        {
                            isDeleteFolder = false;
                            //pulses/PF7168c6ceedc34e709790cd63398c9668/U811e9a9236c140ad98a211a428b6dc84_20160527072241738_original.jpg 
                            string[] stringToken = imageObject.Key.Split('/');
                            string fileNamePath = stringToken[stringToken.Count() - 1].Split('.')[0];
                            string[] fileNames = fileNamePath.Split('_');
                            string fileName = fileNames[0] + "_" + fileNames[1];

                            if (newUpdatedImageUrls.FirstOrDefault(url => url.Contains(fileName)) != null)
                            {
                                continue;
                            }
                        }

                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }

                    if (isDeleteFolder)
                    {
                        // Delete folder
                        DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                        deleteFolderRequest.BucketName = bucketName;
                        deleteFolderRequest.Key = assessmentId + "/" + cardId;
                        s3Client.DeleteObject(deleteFolderRequest);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

    }


}
