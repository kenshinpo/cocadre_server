﻿using Cassandra;
using CassandraService.CassandraUtilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace CassandraService.Entity
{
    public class AnalyticGamification
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataContract]
        public class GamificationSearchTerm
        {
            [DataMember]
            public string Content { get; set; }
            [DataMember]
            public string Id { get; set; }
            [DataMember]
            public bool IsSelected { get; set; }
        }

        [DataContract]
        public class Achiever
        {
            [DataMember]
            public User User { get; set; }
            [DataMember]
            public DateTime UnlockedOnTimestamp { get; set; }
            [DataMember]
            public string UnlockedOnTimestampString { get; set; }
        }

        public int SelectAchieverCountForAchievement(string achievementId, ISession analyticSession)
        {
            int count = 0;
            try
            {
                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.CountStatement("user_achievement_unlocked_by_achievement", new List<string> { "achievement_id" }));
                count = (int)analyticSession.Execute(ps.Bind(achievementId)).FirstOrDefault().GetValue<long>("count");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
            return count;
        }

        public Dictionary<string, object> SelectAchieversForAchievement(string achievementId, string companyId, string searchId, double timeoffset, ISession mainSession, ISession analyticSession)
        {
            User userManager = new User();
            Dictionary<string, object> result = new Dictionary<string, object>();
            List<Achiever> achievers = new List<Achiever>();
            List<GamificationSearchTerm> searchTerms = new List<GamificationSearchTerm>();

            PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("user_achievement_unlocked_by_achievement", new List<string>(), new List<string> { "achievement_id" }));
            RowSet achievementRowset = analyticSession.Execute(ps.Bind(achievementId));
            foreach(Row achievementRow in achievementRowset)
            {
                string userId = achievementRow.GetValue<string>("user_id");
                DateTime unlockedTimestamp = achievementRow.GetValue<DateTime>("unlocked_on_timestamp");
                User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession, null, true).User;
                if(selectedUser != null)
                {
                    bool isToAdd = false;
                    foreach (Department department in selectedUser.Departments)
                    {
                        if (searchTerms.FirstOrDefault(s => s.Id.Equals(department.Id)) == null)
                        {
                            searchTerms.Add(new GamificationSearchTerm
                            {
                                Content = department.Title,
                                Id = department.Id,
                                IsSelected = department.Id.Equals(searchId) ? true : false
                            });
                        }

                        if (!string.IsNullOrEmpty(searchId))
                        {
                            if (department.Id.Equals(searchId))
                            {
                                isToAdd = true;
                            }
                        }
                        else
                        {
                            isToAdd = true;
                        }
                    }

                    if (isToAdd)
                    {
                        achievers.Add(new Achiever
                        {
                            User = selectedUser,
                            UnlockedOnTimestamp = unlockedTimestamp.AddHours(timeoffset),
                            UnlockedOnTimestampString = unlockedTimestamp.AddHours(timeoffset).ToString("dd/MM/yyyy")
                        });
                    }
                }
            }

            achievers = achievers.OrderByDescending(a => a.UnlockedOnTimestamp).ToList();
            result.Add("Achievers", achievers);
            result.Add("SearchTerms", searchTerms);
            return result;
        }

        public Dictionary<string, object> SelectUnlockedDetailsForAchievementByUser(string achievementId, string userId, ISession analyticSession)
        {
            Dictionary<string, object> details = new Dictionary<string, object>();
            DateTime? unlockedOnTimestamp = null;
            DateTime? isSeenTimestamp = null;
            bool isSetAsBadge = false;
            try
            {
                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("achievement_unlocked_by_user", new List<string>(), new List<string> { "achievement_id", "user_id" }));
                Row achievementUnlockedRow = analyticSession.Execute(ps.Bind(achievementId, userId)).FirstOrDefault();
                if(achievementUnlockedRow != null)
                {
                    unlockedOnTimestamp = achievementUnlockedRow.GetValue<DateTime?>("unlocked_on_timestamp");
                    isSeenTimestamp = achievementUnlockedRow.GetValue<DateTime?>("is_seen_on_timestamp");
                    isSetAsBadge = achievementUnlockedRow.GetValue<bool>("is_set_badge");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            details.Add("UnlockedTimestamp", unlockedOnTimestamp);
            details.Add("SeenTimestamp", isSeenTimestamp);
            details.Add("IsSetBadge", isSetAsBadge);

            return details;
        }

        public List<Gamification.Achievement> SelectUnlockedAchievementsByUser(string achieverId, string companyId, double timeoffset, ISession mainSession, ISession analyticSession)
        {
            Gamification gamificationManager = new Gamification();
            List<Gamification.Achievement> unlockedAchievements = new List<Gamification.Achievement>();

            PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("achievement_unlocked_by_user", new List<string>(), new List<string> { "user_id" }));
            RowSet achievementRowset = analyticSession.Execute(ps.Bind(achieverId));
            foreach(Row achievementRow in achievementRowset)
            {
                string achievementId = achievementRow.GetValue<string>("achievement_id");
                Gamification.Achievement achievement = gamificationManager.SelectAchievement(null, companyId, achievementId, mainSession, analyticSession, (int)Gamification.GamificationQueryType.Basic).Achievement;
                if(achievement != null)
                {
                    DateTime unlockedOnTimestamp = achievementRow.GetValue<DateTime>("unlocked_on_timestamp").AddHours(timeoffset);
                    achievement.UnlockedOnTimestamp = unlockedOnTimestamp;
                    achievement.UnlockedOnTimestampString = unlockedOnTimestamp.ToString("dd/MM/yyyy");
                    unlockedAchievements.Add(achievement);
                }
            }

            unlockedAchievements = unlockedAchievements.OrderBy(a => a.UnlockedOnTimestamp).ToList();
            return unlockedAchievements;
        }

        public void UpdateStreak(string userId, string achievementId, string topicId, string challengeId, DateTime currentTime, int result, bool isUpdateLosingStreak, ISession analyticSession)
        {
            bool isReset = false;
            string sortedTimestampTableName = string.Empty;
            string sortedTopicTableName = string.Empty;
            string baseTableName = string.Empty;
            string resetTimestampColumnName = string.Empty;

            // Update losing streak
            if (isUpdateLosingStreak)
            {
                baseTableName = "achievement_losing_streak";
                sortedTopicTableName = "achievement_losing_streak_by_topic";
                sortedTimestampTableName = "achievement_losing_streak_by_user_timestamp";
                resetTimestampColumnName = "last_reset_losing_timestamp";

                if (result != (int)Gamification.GamificationResultTypeEnum.Lost)
                {
                    isReset = true;
                }
            }
            // Update winning streak
            else
            {
                baseTableName = "achievement_winning_streak";
                sortedTopicTableName = "achievement_winning_streak_by_topic";
                sortedTimestampTableName = "achievement_winning_streak_by_user_timestamp";
                resetTimestampColumnName = "last_reset_winning_timestamp";

                if (result != (int)Gamification.GamificationResultTypeEnum.Win)
                {
                    isReset = true;
                }
            }

            BatchStatement batch = new BatchStatement();
            PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit(sortedTimestampTableName, new List<string>(), new List<string> { "user_id" }, 1));
            Row lastStreakRow = analyticSession.Execute(ps.Bind(userId)).FirstOrDefault();

            // Update
            DateTime resetTimestamp = currentTime;
            if (lastStreakRow != null)
            {
                int lastResult = lastStreakRow.GetValue<int>("result");
                if (!isReset)
                {
                    if((isUpdateLosingStreak && lastResult == (int)Gamification.GamificationResultTypeEnum.Lost) || (!isUpdateLosingStreak && lastResult == (int)Gamification.GamificationResultTypeEnum.Win))
                    {
                        resetTimestamp = lastStreakRow.GetValue<DateTime>(resetTimestampColumnName);
                    }
                }
            }

            ps = analyticSession.Prepare(CQLGenerator.InsertStatement(baseTableName, new List<string> { "achievement_id", "user_id", "topic_id", "challenge_id", "result", resetTimestampColumnName, "recorded_on_timestamp" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, challengeId, result, resetTimestamp, currentTime));

            ps = analyticSession.Prepare(CQLGenerator.InsertStatement(sortedTopicTableName, new List<string> { "achievement_id", "user_id", "topic_id", "challenge_id", "result", resetTimestampColumnName, "recorded_on_timestamp" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, challengeId, result, resetTimestamp, currentTime));

            ps = analyticSession.Prepare(CQLGenerator.InsertStatement(sortedTimestampTableName, new List<string> { "achievement_id", "user_id", "topic_id", "challenge_id", "result", resetTimestampColumnName, "recorded_on_timestamp" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, challengeId, result, resetTimestamp, currentTime));

            analyticSession.Execute(batch);
        }


        public void UpdatePerfectAndGet(string userId, string achievementId, string topicId, string challengeId, string questionId, ISession analyticSession)
        {
            BatchStatement batch = new BatchStatement();
            PreparedStatement ps = analyticSession.Prepare(CQLGenerator.InsertStatement("achievement_correct_questions", new List<string> { "achievement_id", "user_id", "topic_id", "question_id" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, questionId));

            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("achievement_correct_questions_by_user", new List<string> { "achievement_id", "user_id", "topic_id", "question_id" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, questionId));

            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("achievement_correct_questions_by_topic", new List<string> { "achievement_id", "user_id", "topic_id", "question_id" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, questionId));

            analyticSession.Execute(batch);
        }

        public void UpdatePlayAndGet(string userId, string achievementId, string topicId, string challengeId, DateTime currentTime, ISession analyticSession)
        {
            BatchStatement batch = new BatchStatement();
            PreparedStatement ps = analyticSession.Prepare(CQLGenerator.InsertStatement("achievement_play_count", new List<string> { "achievement_id", "user_id", "topic_id", "challenge_id", "play_on_timestamp" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, challengeId, currentTime));

            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("achievement_play_count_by_topic", new List<string> { "achievement_id", "user_id", "topic_id", "challenge_id", "play_on_timestamp" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, challengeId, currentTime));

            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("achievement_play_count_by_user", new List<string> { "achievement_id", "user_id", "topic_id", "challenge_id", "play_on_timestamp" }));
            batch.Add(ps.Bind(achievementId, userId, topicId, challengeId, currentTime));

            analyticSession.Execute(batch);
        }

        public int SelectStreakCount(string achievementId, string userId, bool isLosingStreak, ISession analyticSession)
        {
            int streakCount = 0;

            try
            {
                int result = 0;
                string sortedTimestampTableName = string.Empty;
                string resetTimestampColumnName = string.Empty;

                // Update losing streak
                if (isLosingStreak)
                {
                    sortedTimestampTableName = "achievement_losing_streak_by_user_timestamp";
                    resetTimestampColumnName = "last_reset_losing_timestamp";
                    result = (int)Gamification.GamificationResultTypeEnum.Lost;
                }
                // Update winning streak
                else
                {
                    sortedTimestampTableName = "achievement_winning_streak_by_user_timestamp";
                    resetTimestampColumnName = "last_reset_winning_timestamp";
                    result = (int)Gamification.GamificationResultTypeEnum.Win;
                }

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit(sortedTimestampTableName, new List<string>(), new List<string> { "user_id" }, 1));
                Row lastStreakRow = analyticSession.Execute(ps.Bind(userId)).FirstOrDefault();

                if (lastStreakRow != null)
                {
                    CQLGenerator.Comparison comparison = CQLGenerator.Comparison.GreaterThanOrEquals;
                    int lastResult = lastStreakRow.GetValue<int>("result");
                    if (isLosingStreak)
                    {
                        if(lastResult != (int)Gamification.GamificationResultTypeEnum.Lost)
                        {
                            comparison = CQLGenerator.Comparison.GreaterThan;
                        }
                    }
                    else
                    {
                        if (lastResult != (int)Gamification.GamificationResultTypeEnum.Win)
                        {
                            comparison = CQLGenerator.Comparison.GreaterThan;
                        }
                    }

                    DateTime resetTimestamp = lastStreakRow.GetValue<DateTime>(resetTimestampColumnName);
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison(sortedTimestampTableName, new List<string>(), new List<string> { "user_id", "achievement_id", "result" }, "recorded_on_timestamp", comparison, 0));
                    RowSet streakRowset = analyticSession.Execute(ps.Bind(userId, achievementId, result, resetTimestamp));
                    streakCount = streakRowset.Count();
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return streakCount;
        }

        public int SelectCorrectCountByTopic(string achievementId, string userId, string topicId, ISession analyticSession)
        {
            int count = 0;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.CountStatement("achievement_correct_questions_by_user", new List<string> { "user_id", "achievement_id", "topic_id" }));
                count = (int)analyticSession.Execute(ps.Bind(userId, achievementId, topicId)).FirstOrDefault().GetValue<long>("count");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return count;
        }

        public int SelectPlayCountByTopic(string achievementId, string userId, string topicId, ISession analyticSession)
        {
            int count = 0;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.CountStatement("achievement_play_count_by_user", new List<string> { "user_id", "achievement_id", "topic_id" }));
                count = (int)analyticSession.Execute(ps.Bind(userId, achievementId, topicId)).FirstOrDefault().GetValue<long>("count");

                Log.Debug(string.Format("Getting play count for topic {0}, achievement {1}, user {2}: count {3}", topicId, achievementId, userId, count));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return count;
        }

        public int SelectMaxPlayCountByAchievement(string achievementId, string userId, ISession analyticSession)
        {
            int count = 0;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("achievement_play_count_by_user", new List<string>(), new List<string> { "user_id", "achievement_id" }));
                List<Row> rows = analyticSession.Execute(ps.Bind(userId, achievementId)).OrderBy(r => r.GetValue<string>("topic_id")).ToList();
                if(rows.Count > 0)
                {
                    count = rows.GroupBy(r => r.GetValue<string>("topic_id")).Max(g => g.Count());
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return count;
        }
    }

}
