﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Threading;

namespace CassandraService.Entity
{
    public class FixBugScript
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public FixBugResponse FixSurveyCompletion()
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();

                PreparedStatement ps = null;

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_completion_by_user_timestamp", new List<string>(), new List<string>()));
                RowSet rowSet = analyticSession.Execute(ps.Bind());

                Thread thread = new Thread(() => FixSurveyCompletionThread(rowSet, analyticSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void FixSurveyCompletionThread(RowSet rowSet, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;
                string lastUserId = string.Empty;
                string lastTopicId = string.Empty;
                DateTime? lastCompletedOnTimestamp = null;

                BatchStatement deleteBatch = new BatchStatement();

                foreach (Row row in rowSet)
                {
                    string userId = row.GetValue<string>("completed_by_user_id");
                    string topicId = row.GetValue<string>("topic_id");
                    DateTime completedOnTimestamp = row.GetValue<DateTime>("completed_on_timestamp");

                    if (string.IsNullOrEmpty(lastUserId))
                    {
                        lastUserId = userId;
                        lastTopicId = topicId;
                        lastCompletedOnTimestamp = completedOnTimestamp;
                    }
                    else
                    {
                        if (lastUserId.Equals(userId) && lastTopicId.Equals(topicId))
                        {
                            ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_completion_by_user_timestamp", new List<string> { "completed_by_user_id", "topic_id", "completed_on_timestamp" }));
                            deleteBatch.Add(ps.Bind(lastUserId, lastTopicId, lastCompletedOnTimestamp.Value));

                            ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_completion_by_topic_timestamp", new List<string> { "completed_by_user_id", "topic_id", "completed_on_timestamp" }));
                            deleteBatch.Add(ps.Bind(lastUserId, lastTopicId, lastCompletedOnTimestamp.Value));

                            analyticSession.Execute(deleteBatch);
                            deleteBatch = new BatchStatement();
                        }

                        lastUserId = userId;
                        lastTopicId = topicId;
                        lastCompletedOnTimestamp = completedOnTimestamp;
                    }
                }

                Log.Debug("Fix survey completion completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public FixBugResponse FixSurveyCustomAnswerGrouping(string cardId)
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                Thread thread = new Thread(() => FixSurveyCustomAnswerGroupingThread(cardId, mainSession, analyticSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void FixSurveyCustomAnswerGroupingThread(string cardId, ISession mainSession, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;
                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_sort_by_number_timestamp", new List<string>(), new List<string> { "card_id" }));
                RowSet sortedRows = analyticSession.Execute(ps.Bind(cardId));
                foreach (Row sortedRow in sortedRows)
                {
                    string customAnswer = sortedRow.GetValue<string>("custom_answer");
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer", new List<string>(), new List<string> { "custom_answer", "card_id" }));
                    RowSet customAnswerRows = analyticSession.Execute(ps.Bind(customAnswer, cardId));

                    foreach (Row customAnswerRow in customAnswerRows)
                    {
                        string similarId = customAnswerRow.GetValue<string>("similar_id");
                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_by_similar", new List<string> { "similar_id" }));
                        deleteBatch.Add(ps.Bind(similarId));
                    }

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_by_answer", new List<string> { "custom_answer", "card_id" }));
                    deleteBatch.Add(ps.Bind(customAnswer, cardId));
                }

                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                analyticSession.Execute(deleteBatch);
                deleteBatch = new BatchStatement();

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_card", new List<string>(), new List<string> { "card_id" }));
                List<Row> rowSet = analyticSession.Execute(ps.Bind(cardId)).ToList();
                rowSet = rowSet.OrderBy(r => r.GetValue<DateTime>("created_on_timestamp")).ToList();

                foreach (Row row in rowSet)
                {
                    string optionId = row.GetValue<string>("option_id");
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option", new List<string>(), new List<string> { "card_id", "id" }));
                    Row optionRow = mainSession.Execute(ps.Bind(cardId, optionId)).FirstOrDefault();

                    if (optionRow != null)
                    {
                        string answeredByUserId = optionRow.GetValue<string>("created_by_user_id");
                        string content = optionRow.GetValue<string>("content");
                        DateTime currentTime = optionRow.GetValue<DateTime>("created_on_timestamp");

                        string lowerCustomAnswer = content.ToLower().Trim();
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer", new List<string>(), new List<string> { "custom_answer", "card_id" }));
                        Row similarRow = analyticSession.Execute(ps.Bind(lowerCustomAnswer, cardId)).FirstOrDefault();

                        string similarId = string.Empty;
                        int number = 0;
                        if (similarRow != null)
                        {
                            similarId = similarRow.GetValue<string>("similar_id");
                            DateTime lastUpdatedTime = similarRow.GetValue<DateTime>("created_on_timestamp");
                            number = similarRow.GetValue<int>("number");

                            ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                            deleteBatch.Add(ps.Bind(cardId, number, lastUpdatedTime, lowerCustomAnswer));

                            ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_custom_option_by_answer", new List<string> { "custom_answer", "card_id" }, new List<string> { "number", "created_on_timestamp" }, new List<string>()));
                            updateBatch.Add(ps.Bind(number + 1, currentTime, lowerCustomAnswer, cardId));
                        }
                        else
                        {
                            similarId = UUIDGenerator.GenerateUniqueIDForRSSimilar();

                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_by_answer", new List<string> { "similar_id", "card_id", "custom_answer", "number", "created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(similarId, cardId, lowerCustomAnswer, number + 1, currentTime));
                        }

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                        updateBatch.Add(ps.Bind(cardId, number + 1, currentTime, lowerCustomAnswer));

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_by_similar", new List<string> { "similar_id", "option_id", "created_by_user_id", "created_on_timestamp" }));
                        updateBatch.Add(ps.Bind(similarId, optionId, answeredByUserId, currentTime));
                    }
                    else
                    {
                        string createdByUserId = row.GetValue<string>("created_by_user_id");
                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_card", new List<string> { "card_id", "option_id", "created_by_user_id" }));
                        deleteBatch.Add(ps.Bind(cardId, optionId, createdByUserId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_user", new List<string> { "card_id", "option_id", "created_by_user_id" }));
                        deleteBatch.Add(ps.Bind(cardId, optionId, createdByUserId));
                    }

                    analyticSession.Execute(deleteBatch);
                    analyticSession.Execute(updateBatch);
                    deleteBatch = new BatchStatement();
                    updateBatch = new BatchStatement();
                }
                Log.Debug("Fix survey custom answer completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public FixBugResponse FixImpressionTally()
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                Thread thread = new Thread(() => FixImpressionTallyThread(mainSession, analyticSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void FixImpressionTallyThread(ISession mainSession, ISession analyticSession)
        {
            try
            {
                AnalyticFeed analyticManager = new AnalyticFeed();
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                  new List<string>(), new List<string> { "is_feed_valid" }));
                RowSet feedRowSet = mainSession.Execute(ps.Bind(true));

                foreach (Row feedRow in feedRowSet)
                {
                    List<Dictionary<string, object>> seenByUsers = new List<Dictionary<string, object>>();

                    string feedId = feedRow.GetValue<string>("feed_id");

                    // Get from feed points
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_point",
                       new List<string>(), new List<string> { "feed_id" }));
                    RowSet feedPointRowSet = mainSession.Execute(ps.Bind(feedId));
                    foreach (Row feedPointRow in feedPointRowSet)
                    {
                        string userId = feedPointRow.GetValue<string>("user_id");
                        DateTime votedTimestamp = feedPointRow.GetValue<DateTime>("voted_timestamp");

                        Dictionary<string, object> dict = new Dictionary<string, object>();
                        dict.Add("UserId", userId);
                        dict.Add("Timestamp", votedTimestamp);

                        seenByUsers.Add(dict);
                    }

                    // Get from comments and replies
                    ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                        new List<string>(), new List<string> { "feed_id", "is_valid" }));
                    RowSet feedCommentRowSet = mainSession.Execute(ps.Bind(feedId, true));
                    foreach (Row feedCommentRow in feedCommentRowSet)
                    {
                        string commentUserId = feedCommentRow.GetValue<string>("user_id");
                        string commentId = feedCommentRow.GetValue<string>("id");
                        DateTime commentCreatedTimestamp = feedCommentRow.GetValue<DateTime>("created_on_timestamp");

                        Dictionary<string, object> foundDict = seenByUsers.FirstOrDefault(d => d["UserId"].Equals(commentUserId));
                        if (foundDict == null)
                        {
                            Dictionary<string, object> dict = new Dictionary<string, object>();
                            dict.Add("UserId", commentUserId);
                            dict.Add("Timestamp", commentCreatedTimestamp);
                            seenByUsers.Add(dict);
                        }
                        else
                        {
                            if ((DateTime)foundDict["Timestamp"] > commentCreatedTimestamp)
                            {
                                foundDict["Timestamp"] = commentCreatedTimestamp;
                            }
                        }

                        ps = mainSession.Prepare(CQLGenerator.SelectStatement("feed_reply_text",
                            new List<string>(), new List<string> { "comment_id", "is_valid" }));
                        RowSet feedReplyRowset = mainSession.Execute(ps.Bind(commentId, true));
                        foreach (Row feedReplyRow in feedReplyRowset)
                        {
                            string replyUserId = feedReplyRow.GetValue<string>("user_id");
                            DateTime replyCreatedTimestamp = feedReplyRow.GetValue<DateTime>("created_on_timestamp");

                            foundDict = seenByUsers.FirstOrDefault(d => d["UserId"].Equals(replyUserId));
                            if (foundDict == null)
                            {
                                Dictionary<string, object> dict = new Dictionary<string, object>();
                                dict.Add("UserId", replyUserId);
                                dict.Add("Timestamp", replyCreatedTimestamp);
                                seenByUsers.Add(dict);
                            }
                            else
                            {
                                if ((DateTime)foundDict["Timestamp"] > replyCreatedTimestamp)
                                {
                                    foundDict["Timestamp"] = replyCreatedTimestamp;
                                }
                            }
                        }
                    }

                    // Get unique impression count
                    int impression = 0;
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("feed_unique_impression_counter",
                        new List<string>(), new List<string> { "feed_id" }));
                    Row impressionRow = analyticSession.Execute(ps.Bind(feedId)).FirstOrDefault();

                    if (impressionRow != null)
                    {
                        impression = (int)impressionRow.GetValue<long>("impression");
                    }

                    if (impression < seenByUsers.Count)
                    {
                        // Update from seenByUsers
                        foreach (Dictionary<string, object> user in seenByUsers)
                        {
                            string userId = (string)user["UserId"];
                            DateTime timestamp = (DateTime)user["Timestamp"];

                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("feed_unique_impression_stats",
                               new List<string>(), new List<string> { "feed_id", "seen_by_user_id" }));
                            Row addedImpressionRow = analyticSession.Execute(ps.Bind(feedId, userId)).FirstOrDefault();
                            if (addedImpressionRow == null)
                            {
                                //Log.Debug("Added user to impression");
                                analyticManager.UpdateImpression(feedId, userId, analyticSession, timestamp);
                            }
                        }
                    }
                }

                Log.Debug("Fix impression tally completed");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public FixBugResponse RemoveAppraisalPulses()
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                Thread thread = new Thread(() => RemoveAppraisalPulsesThread(mainSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void RemoveAppraisalPulsesThread(ISession mainSession)
        {
            try
            {
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("pulse_by_priority_with_timestamp", new List<string>(), new List<string>()));
                RowSet pulseRowSet = mainSession.Execute(ps.Bind());

                foreach (Row pulseRow in pulseRowSet)
                {
                    int pulseType = pulseRow.GetValue<int>("pulse_type");
                    if(pulseType == (int)Pulse.PulseTypeEnum.Live360)
                    {
                        string companyId = pulseRow.GetValue<string>("company_id");
                        bool isPrioritized = pulseRow.GetValue<bool>("is_prioritized");
                        int priority = pulseRow.GetValue<int>("pulse_priority");
                        DateTime startTimestamp = pulseRow.GetValue<DateTime>("start_timestamp");
                        string pulseId = pulseRow.GetValue<string>("pulse_id");

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("pulse_by_priority_with_timestamp", new List<string> { "pulse_id", "company_id", "is_prioritized", "pulse_priority", "start_timestamp" }));
                        mainSession.Execute(ps.Bind(pulseId, companyId, isPrioritized, priority, startTimestamp));
                    }
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_appraisal", new List<string>(), new List<string>()));
                RowSet notificationRowSet = mainSession.Execute(ps.Bind());

                BatchStatement deleteBatch = new BatchStatement();
                foreach (Row notificationRow in notificationRowSet)
                {
                    string pulseId = notificationRow.GetValue<string>("pulse_id");
                    DateTime createdTimestamp = notificationRow.GetValue<DateTime>("notification_created_on_timestamp");
                    string targetedUserId = notificationRow.GetValue<string>("targeted_user_id");
                    string notificationId = notificationRow.GetValue<string>("notification_id");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_appraisal", new List<string> { "pulse_id", "targeted_user_id", "notification_id" }));
                    deleteBatch.Add(ps.Bind(pulseId, targetedUserId, notificationId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_user", new List<string> { "targeted_user_id", "notification_id" }));
                    deleteBatch.Add(ps.Bind(targetedUserId, notificationId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_sorted_by_user_timestamp", new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                    deleteBatch.Add(ps.Bind(targetedUserId, createdTimestamp, notificationId));

                    mainSession.Execute(deleteBatch);
                    deleteBatch = new BatchStatement();
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_report", new List<string>(), new List<string>()));
                RowSet notificationReportRowSet = mainSession.Execute(ps.Bind());

                deleteBatch = new BatchStatement();
                foreach (Row notificationReportRow in notificationReportRowSet)
                {
                    string reportId = notificationReportRow.GetValue<string>("report_id");
                    DateTime createdTimestamp = notificationReportRow.GetValue<DateTime>("notification_created_on_timestamp");
                    string targetedUserId = notificationReportRow.GetValue<string>("targeted_user_id");
                    string notificationId = notificationReportRow.GetValue<string>("notification_id");

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_report", new List<string> { "report_id", "targeted_user_id", "notification_id" }));
                    deleteBatch.Add(ps.Bind(reportId, targetedUserId, notificationId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_by_user", new List<string> { "targeted_user_id", "notification_id" }));
                    deleteBatch.Add(ps.Bind(targetedUserId, notificationId));

                    ps = mainSession.Prepare(CQLGenerator.DeleteStatement("notification_privacy_sorted_by_user_timestamp", new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                    deleteBatch.Add(ps.Bind(targetedUserId, createdTimestamp, notificationId));

                    mainSession.Execute(deleteBatch);
                    deleteBatch = new BatchStatement();
                }

                ps = mainSession.Prepare(CQLGenerator.SelectStatement("scheduled_notification_task", new List<string>(), new List<string>()));
                RowSet scheduleNotificationRowSet = mainSession.Execute(ps.Bind());

                deleteBatch = new BatchStatement();
                foreach (Row scheduleNotificationRow in scheduleNotificationRowSet)
                {
                    int notificationType = scheduleNotificationRow.GetValue<int>("notification_type");
                    int notificationSubType = scheduleNotificationRow.GetValue<int>("notification_subtype");
                    string scheduleId = scheduleNotificationRow.GetValue<string>("scheduled_id");
                    string companyId = scheduleNotificationRow.GetValue<string>("company_id");
                    string notificationId = scheduleNotificationRow.GetValue<string>("notification_id");
                    string pulseId = scheduleNotificationRow.GetValue<string>("pulse_id");
                    string reportId = scheduleNotificationRow.GetValue<string>("report_id");

                    if (notificationType == (int)Notification.NotificationType.Pulse && notificationSubType == (int)Notification.NotificationPulseSubType.Live360)
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task", new List<string> { "scheduled_id", "company_id", "notification_id" }));
                        deleteBatch.Add(ps.Bind(scheduleId, companyId, notificationId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task_by_pulse", new List<string> { "pulse_id", "company_id", "notification_id" }));
                        deleteBatch.Add(ps.Bind(pulseId, companyId, notificationId));

                        mainSession.Execute(deleteBatch);
                        deleteBatch = new BatchStatement();
                    }
                    else if(notificationType == (int)Notification.NotificationType.Report && (notificationSubType == (int)Notification.NotificationReportSubType.Live360Completed || notificationSubType == (int)Notification.NotificationReportSubType.Live360End))
                    {
                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task", new List<string> { "scheduled_id", "company_id", "notification_id" }));
                        deleteBatch.Add(ps.Bind(scheduleId, companyId, notificationId));

                        ps = mainSession.Prepare(CQLGenerator.DeleteStatement("scheduled_notification_task_by_report", new List<string> { "report_id", "company_id", "notification_id" }));
                        deleteBatch.Add(ps.Bind(reportId, companyId, notificationId));

                        mainSession.Execute(deleteBatch);
                        deleteBatch = new BatchStatement();
                    }
                }


                Log.Debug("Remove all appraisal pulse");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public FixBugResponse UpdateMissingDau(DateTime date)
        {
            FixBugResponse response = new FixBugResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();

                Thread thread = new Thread(() => UpdateMissingDauThread(date, mainSession));
                thread.Start();

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void UpdateMissingDauThread(DateTime date, ISession mainSession)
        {
            try
            {
                new AnalyticDau().UpdateDailyActiveUser(date);
                Log.Debug("Update dau complete");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }
    }
}