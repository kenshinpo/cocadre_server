﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    public class AnalyticLearning
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum MLTopicResultTypeEnum
        {
            Absent = -3,
            Incomplete = -2,
            Failed = -1,
            Passed = 1
        }

        public enum MLCardToggleTypeEnum
        {
            AllQuestions = 1,
            HardestQuestions = 2,
            EasiestQuestions = 3,
            CorrectAnswers = 4,
            WrongAnswers = 5
        }

        [DataContract]
        [Serializable]
        public class MLTopicAttempt
        {
            [DataMember]
            public int ResultCode { get; set; }
            [DataMember]
            public string ResultMessage { get; set; }
            [DataMember]
            public string Attempt { get; set; }
            [DataMember]
            public string Grade { get; set; }
            [DataMember]
            public DateTime? CompletedDate { get; set; }
            [DataMember]
            public string CompletedDateString { get; set; }
            [DataMember]
            public MLTopicAttempt CurrentIncompleteAttempt { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MLTopicCandidateResult
        {
            [DataMember]
            public Chart Attendance { get; set; }
            [DataMember]
            public Chart PassingRate { get; set; }
            [DataMember]
            public List<MLTopicCandidate> Candidates { get; set; }
        }

        [DataContract]
        [Serializable]
        public class MLTopicCandidate
        {
            [DataMember]
            public User Candidate { get; set; }
            [DataMember]
            public MLTopicAttempt Attempt { get; set; }

        }

        [DataContract]
        public class MLFilteredDepartmentSearchTerm
        {
            [DataMember]
            public string Content { get; set; }
            [DataMember]
            public string Id { get; set; }
            [DataMember]
            public bool IsSelected { get; set; }
        }

        [DataContract]
        public class MLCardToggleType
        {
            [DataMember]
            public string Content { get; set; }
            [DataMember]
            public string Id { get; set; }
            [DataMember]
            public bool IsSelected { get; set; }
        }

        public MLTopicAttempt SelectMLAttempt(string topicId, string requesterUserId, double timeoffset, ISession analyticSession)
        {
            // If first time
            MLTopicAttempt result = new MLTopicAttempt();
            result.ResultCode = (int)MLTopicResultTypeEnum.Absent;
            result.ResultMessage = DefaultResource.MLAbsent;
            result.CompletedDate = null;
            result.CompletedDateString = "NA";
            result.Attempt = "NA";
            result.Grade = "NA";

            result.CurrentIncompleteAttempt = new MLTopicAttempt
            {
                ResultCode = (int)MLTopicResultTypeEnum.Absent,
                ResultMessage = DefaultResource.MLAbsent,
                CompletedDate = null,
                CompletedDateString = "NA",
                Attempt = "NA",
                Grade = "NA"
            };

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("ml_progress", new List<string>(), new List<string> { "ml_topic_id", "user_id" }, 1));
                Row progressRow = analyticSession.Execute(ps.Bind(topicId, requesterUserId)).FirstOrDefault();

                // Tried to attempt at least once
                if (progressRow != null)
                {
                    int resultCode = progressRow.GetValue<int>("result");
                    int currentAttempt = progressRow.GetValue<int>("attempt");
                    string currentAttemptString = currentAttempt.ToString();
                    string message = DefaultResource.MLIncomplete;
                    DateTime? completedDate = null;
                    string completedDateString = string.Empty;
                    string grade = "NA";
                    MLTopicAttempt currentIncompletedAttempt = new MLTopicAttempt
                    {
                        ResultCode = (int)MLTopicResultTypeEnum.Absent,
                        ResultMessage = DefaultResource.MLAbsent,
                        CompletedDate = null,
                        CompletedDateString = "NA",
                        Attempt = "NA",
                        Grade = "NA"
                    }; ;

                    int totalScore = progressRow.GetValue<int>("total_score");
                    int userScore = progressRow.GetValue<int>("user_score");

                    decimal percentage = totalScore > 0 ? Math.Round(((decimal)userScore / totalScore) * 100, 0) : 0;
                    grade = percentage + "%";

                    completedDate = progressRow.GetValue<DateTime?>("completed_on_timestamp");
                    completedDateString = completedDate != null ? completedDate.Value.AddHours(timeoffset).ToString("dd/MM/yyyy") : "NA";

                    switch (resultCode)
                    {
                        case (int)MLTopicResultTypeEnum.Incomplete:
                            message = DefaultResource.MLIncomplete;

                            currentIncompletedAttempt = new MLTopicAttempt
                            {
                                Attempt = currentAttempt.ToString(),
                                Grade = grade,
                                CompletedDate = completedDate,
                                CompletedDateString = completedDateString,
                                ResultCode = resultCode,
                                ResultMessage = message
                            };

                            if (currentAttempt > 1)
                            {
                                currentAttempt -= 1;
                                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("ml_progress", new List<string>(), new List<string> { "ml_topic_id", "user_id", "attempt" }, 1));
                                progressRow = analyticSession.Execute(ps.Bind(topicId, requesterUserId, currentAttempt)).FirstOrDefault();

                                totalScore = progressRow.GetValue<int>("total_score");
                                userScore = progressRow.GetValue<int>("user_score");

                                percentage = totalScore > 0 ? Math.Round(((decimal)userScore / totalScore) * 100, 0) : 0;
                                grade = percentage + "%";

                                completedDate = progressRow.GetValue<DateTime?>("completed_on_timestamp");
                                completedDateString = completedDate.Value.AddHours(timeoffset).ToString("dd/MM/yyyy");

                                resultCode = progressRow.GetValue<int>("result");
                                switch (resultCode)
                                {
                                    case (int)MLTopicResultTypeEnum.Failed:
                                        message = DefaultResource.MLFailed;
                                        break;
                                    case (int)MLTopicResultTypeEnum.Passed:
                                        message = DefaultResource.MLPassed;
                                        break;
                                }
                                currentAttemptString = currentAttempt.ToString();
                            }
                            else
                            {
                                currentAttemptString = "NA";
                            }
                            break;
                        case (int)MLTopicResultTypeEnum.Failed:
                            message = DefaultResource.MLFailed;
                            break;
                        case (int)MLTopicResultTypeEnum.Passed:
                            message = DefaultResource.MLPassed;
                            break;
                    }

                    result = new MLTopicAttempt
                    {
                        ResultCode = resultCode,
                        ResultMessage = message,
                        Attempt = currentAttemptString,
                        Grade = grade,
                        CompletedDate = completedDate,
                        CompletedDateString = completedDateString,
                        CurrentIncompleteAttempt = currentIncompletedAttempt
                    };
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return result;
        }

        public int SelectProgressByUser(string topicId, string requesterUserId, ISession analyticSession)
        {
            int progress = (int)MLTopicResultTypeEnum.Incomplete;
            MLTopicAttempt attempt = SelectMLAttempt(topicId, requesterUserId, 0, analyticSession);
            if (attempt.CompletedDate != null)
            {
                progress = attempt.ResultCode;
            }
            return progress;
        }

        public int SelectCurrentAttemptByUser(ISession analyticSession, string answeredByUserId, string topicId, bool returnErrorIfNotFound = true)
        {
            int currentAttempt = 0;
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("ml_progress", new List<string>(), new List<string> { "ml_topic_id", "user_id" }, 1));
                Row progressRow = analyticSession.Execute(ps.Bind(topicId, answeredByUserId)).FirstOrDefault();

                if (progressRow != null)
                {
                    currentAttempt = progressRow.GetValue<int>("attempt");
                    if (progressRow.GetValue<DateTime?>("completed_on_timestamp") != null && progressRow.GetValue<int>("total_score") != 0)
                    {
                        currentAttempt += 1;
                    }
                }
                else
                {
                    if (!returnErrorIfNotFound)
                    {
                        currentAttempt = 1;
                    }
                }

            }
            catch (Exception ex)
            {

                Log.Error(ex.ToString(), ex);
            }

            return currentAttempt;
        }

        public MLAnswerResponse AnswerRSCard(string answeredByUserId,
                                             string topicId,
                                             string cardId,
                                             int attempt,
                                             List<string> optionIds,
                                             DateTime currentTime,
                                             ISession analyticSession)
        {
            MLAnswerResponse response = new MLAnswerResponse();
            response.Success = false;
            try
            {
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Update option selection
                MLOptionUpdateAnswerResponse optionAnswerResponse = UpdateOptionAnswer(analyticSession, attempt, answeredByUserId, cardId, optionIds, currentTime);
                if (optionAnswerResponse.Success)
                {
                    foreach (BoundStatement bs in optionAnswerResponse.DeleteStatements)
                    {
                        deleteBatch.Add(bs);
                    }

                    foreach (BoundStatement bs in optionAnswerResponse.UpdateStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    // Update last updated time
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_progress", new List<string> { "user_id", "ml_topic_id", "attempt" }, new List<string> { "last_updated_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, answeredByUserId, topicId, attempt));
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_progress_by_user", new List<string> { "user_id", "ml_topic_id", "attempt" }, new List<string> { "last_updated_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, answeredByUserId, topicId, attempt));

                    analyticSession.Execute(deleteBatch);
                    analyticSession.Execute(updateBatch);

                    Log.Debug("Confirm saved to db");
                }
                else
                {
                    Log.Error(optionAnswerResponse.ErrorMessage);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        private MLOptionUpdateAnswerResponse UpdateOptionAnswer(ISession analyticSession, int attempt, string answeredByUserId, string cardId, List<string> newOptionIds, DateTime currentTime)
        {
            MLOptionUpdateAnswerResponse response = new MLOptionUpdateAnswerResponse();
            response.DeleteStatements = new List<BoundStatement>();
            response.UpdateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                // Delete previously selected options
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "attempt", "card_id" }));
                RowSet optionAnsweredRowset = analyticSession.Execute(ps.Bind(answeredByUserId, attempt, cardId));

                foreach (Row optionAnsweredRow in optionAnsweredRowset)
                {
                    string optionId = optionAnsweredRow.GetValue<string>("option_id");
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_option_answered_by_card", new List<string> { "answered_by_user_id", "card_id", "option_id", "attempt" }));
                    response.DeleteStatements.Add(ps.Bind(answeredByUserId, cardId, optionId, attempt));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_option_answered_by_user", new List<string> { "answered_by_user_id", "attempt", "card_id", "option_id" }));
                    response.DeleteStatements.Add(ps.Bind(answeredByUserId, attempt, cardId, optionId));
                }

                foreach (string optionId in newOptionIds)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_option_answered_by_user", new List<string> { "card_id", "option_id", "attempt", "answered_by_user_id", "answered_on_timestamp" }));
                    response.UpdateStatements.Add(ps.Bind(cardId, optionId, attempt, answeredByUserId, currentTime));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_option_answered_by_card", new List<string> { "card_id", "option_id", "attempt", "answered_by_user_id", "answered_on_timestamp" }));
                    response.UpdateStatements.Add(ps.Bind(cardId, optionId, attempt, answeredByUserId, currentTime));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorMessage = ex.ToString();
            }

            return response;
        }

        public MLUpdateActivityResponse StartMLActivity(string requesterUserId, string topicId, ISession analyticSession)
        {
            MLUpdateActivityResponse response = new MLUpdateActivityResponse();
            response.Success = false;
            try
            {
                DateTime currentTime = DateTime.UtcNow;
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();

                int currentAttempt = SelectCurrentAttemptByUser(analyticSession, requesterUserId, topicId, false);

                if (currentAttempt == 1)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_incomplete_first_attempt", new List<string> { "ml_topic_id", "user_id" }));
                    updateBatch.Add(ps.Bind(topicId, requesterUserId));
                }

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_progress", new List<string> { "ml_topic_id", "user_id", "attempt" }, new List<string> { "user_score", "total_score", "result", "start_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(0, 0, (int)MLTopicResultTypeEnum.Incomplete, currentTime, currentTime, topicId, requesterUserId, currentAttempt));

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_progress_by_user", new List<string> { "ml_topic_id", "user_id", "attempt" }, new List<string> { "user_score", "total_score", "result", "start_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(0, 0, (int)MLTopicResultTypeEnum.Incomplete, currentTime, currentTime, topicId, requesterUserId, currentAttempt));

                analyticSession.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool CheckForSelectedOption(int attempt, string cardId, string optionId, string answeredByUserId, ISession analyticSession)
        {
            bool isSelected = false;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "attempt", "card_id", "option_id" }));
                Row selectedRow = analyticSession.Execute(ps.Bind(answeredByUserId, attempt, cardId, optionId)).FirstOrDefault();

                if (selectedRow != null)
                {
                    isSelected = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isSelected;
        }

        public MLSelectUserResultResponse SelectUserResult(string requesterUserId, string companyId, int currentAttempt, string topicId, string passingGrade, List<MLCard> cards, ISession analyticSession)
        {
            MLSelectUserResultResponse response = new MLSelectUserResultResponse();
            response.TotalScore = 0;
            response.UserScore = 0;
            response.IsPassed = false;
            response.Success = false;
            try
            {
                DateTime currentTime = DateTime.UtcNow;
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();

                // Check if user really completed the learning
                // Use logic based algorithm
                // Get scoring
                int totalScore = 0;
                int userScore = 0;
                List<Dictionary<string, object>> optionScoring = new List<Dictionary<string, object>>();
                foreach (MLCard card in cards)
                {
                    string cardId = card.CardId;
                    int cardType = card.Type;
                    if (cardType != (int)MLCard.MLCardType.Instructional)
                    {
                        if (cardType == (int)MLCard.MLCardType.MultiChoice || cardType == (int)MLCard.MLCardType.SelectOne)
                        {
                            // Check option
                            Dictionary<string, object> optionScoringDict = new Dictionary<string, object>();
                            bool isAnsweredByUser = false;
                            int totalOptionScore = 0;
                            int optionScore = 0;

                            card.Options = card.Options.OrderByDescending(o => o.Score).ToList();

                            int optionIndex = 0;
                            int maxSelectedcount = card.MaxOptionToSelect;

                            foreach (MLOption option in card.Options)
                            {
                                if (cardType == (int)MLCard.MLCardType.MultiChoice)
                                {
                                    if (optionIndex < maxSelectedcount)
                                    {
                                        totalOptionScore += option.Score;
                                    }
                                    optionIndex++;
                                }
                                else
                                {
                                    totalOptionScore += option.Score;
                                }

                                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "attempt", "card_id", "option_id" }));
                                Row progressRow = analyticSession.Execute(ps.Bind(requesterUserId, currentAttempt, cardId, option.OptionId)).FirstOrDefault();

                                if (progressRow != null)
                                {
                                    optionScore += option.Score;

                                    if (!isAnsweredByUser)
                                    {
                                        isAnsweredByUser = true;
                                    }
                                }
                            }

                            optionScoringDict.Add("OptionScore", totalOptionScore);
                            optionScoringDict.Add("UserScore", optionScore);
                            optionScoringDict.Add("CardId", cardId);
                            optionScoring.Add(optionScoringDict);

                            if (!isAnsweredByUser)
                            {
                                Log.Error("Attempt: " + currentAttempt);
                                Log.Error(string.Format("Card number: {0}, Content: {1}", card.Ordering, card.Content));
                                Log.Error(ErrorMessage.MLIncompleted);
                                response.ErrorCode = Convert.ToInt32(ErrorCode.MLIncompleted);
                                response.ErrorMessage = ErrorMessage.MLIncompleted;
                                return response;
                            }

                            totalScore += totalOptionScore;

                            switch (card.ScoringCalculationType)
                            {
                                case (int)MLCard.MLScoringCalculationType.OnlyRightAnswerHasScore:
                                    userScore += optionScore;
                                    break;
                                case (int)MLCard.MLScoringCalculationType.AllRightAnswersThenHaveScore:
                                    if (optionScore == totalOptionScore)
                                    {
                                        userScore += optionScore;
                                    }
                                    break;
                            }
                        }
                    }
                }

                foreach (Dictionary<string, object> dict in optionScoring)
                {
                    int totalOptionScore = (int)dict["OptionScore"];
                    int optionScore = (int)dict["UserScore"];
                    string cardId = (string)dict["CardId"];

                    // Update attempt
                    MLUpdateAttemptResponse topicAttemptResponse = UpdateTopicAttempt(companyId, topicId, totalOptionScore, optionScore, analyticSession);
                    foreach (BoundStatement bs in topicAttemptResponse.DeleteStatements)
                    {
                        deleteBatch.Add(bs);
                    }

                    foreach (BoundStatement bs in topicAttemptResponse.UpdateStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    MLUpdateAttemptResponse topicCardAttemptResponse = UpdateTopicCardAttempt(topicId, cardId, totalOptionScore, optionScore, analyticSession);
                    foreach (BoundStatement bs in topicCardAttemptResponse.DeleteStatements)
                    {
                        deleteBatch.Add(bs);
                    }

                    foreach (BoundStatement bs in topicCardAttemptResponse.UpdateStatements)
                    {
                        updateBatch.Add(bs);
                    }

                    analyticSession.Execute(deleteBatch);
                    analyticSession.Execute(updateBatch);

                    updateBatch = new BatchStatement();
                    deleteBatch = new BatchStatement();
                }

                int result = (int)MLTopicResultTypeEnum.Failed;
                if (passingGrade.Contains("%"))
                {
                    if (((float)userScore / totalScore) * 100 >= Convert.ToInt32(passingGrade.Replace("%", "")))
                    {
                        result = (int)MLTopicResultTypeEnum.Passed;
                    }
                }
                else
                {
                    if (userScore >= Convert.ToInt32(passingGrade))
                    {
                        result = (int)MLTopicResultTypeEnum.Passed;
                    }
                }

                if (result == (int)MLTopicResultTypeEnum.Passed)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_passes", new List<string> { "ml_topic_id", "user_id", "attempt", "passes_on_timestamp" }));
                    updateBatch.Add(ps.Bind(topicId, requesterUserId, currentAttempt, currentTime));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_failure", new List<string> { "ml_topic_id", "user_id" }));
                    deleteBatch.Add(ps.Bind(topicId, requesterUserId));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_pass_ranking", new List<string> { "user_id", "ml_topic_id", "attempt", "user_score", "total_score", "result", "completed_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, topicId, currentAttempt, userScore, totalScore, result, currentTime));
                }
                else
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_failure", new List<string> { "ml_topic_id", "user_id" }, new List<string> { "last_failed_timestamp", "attempt" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, currentAttempt, topicId, requesterUserId));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_fail_ranking", new List<string> { "user_id", "ml_topic_id", "attempt", "user_score", "total_score", "result", "completed_on_timestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, topicId, currentAttempt, userScore, totalScore, result, currentTime));
                }

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_progress", new List<string> { "ml_topic_id", "user_id", "attempt" }, new List<string> { "user_score", "total_score", "result", "completed_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(userScore, totalScore, result, currentTime, currentTime, topicId, requesterUserId, currentAttempt));

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_progress_by_user", new List<string> { "ml_topic_id", "user_id", "attempt" }, new List<string> { "user_score", "total_score", "result", "completed_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(userScore, totalScore, result, currentTime, currentTime, topicId, requesterUserId, currentAttempt));


                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_incomplete_first_attempt", new List<string> { "ml_topic_id", "user_id" }));
                deleteBatch.Add(ps.Bind(topicId, requesterUserId));

                analyticSession.Execute(deleteBatch);
                analyticSession.Execute(updateBatch);

                #region For solving situation of 0/0.
                if (totalScore == 0 && userScore == 0)
                {
                    totalScore = 100;
                    userScore = 100;
                }
                #endregion

                response.TotalScore = totalScore;
                response.UserScore = userScore;
                response.IsPassed = result == (int)MLTopicResultTypeEnum.Passed ? true : false;
                response.IsPassingGradeUsingPercentage = passingGrade.Contains("%");
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void SelectCandidateResult(List<User> targetedAudience, MLSelectCandidateResultResponse response, double timeoffset, ISession analyticSession)
        {
            try
            {
                response.CandidateResult = new MLTopicCandidateResult();
                response.CandidateResult.Attendance = new Chart();
                response.CandidateResult.PassingRate = new Chart();
                response.CandidateResult.Candidates = new List<MLTopicCandidate>();

                int totalCount = targetedAudience.Count();
                int totalPasses = 0;
                int totalFailures = 0;
                int totalIncompleteFirstAttempt = 0;
                int totalAbsent = 0;

                foreach (User user in targetedAudience)
                {
                    MLTopicAttempt attempt = SelectMLAttempt(response.Topic.TopicId, user.UserId, timeoffset, analyticSession);

                    switch (attempt.ResultCode)
                    {
                        case (int)MLTopicResultTypeEnum.Absent:
                            totalAbsent++;
                            break;
                        case (int)MLTopicResultTypeEnum.Failed:
                            totalFailures++;
                            break;
                        case (int)MLTopicResultTypeEnum.Incomplete:
                            totalIncompleteFirstAttempt++;
                            break;
                        case (int)MLTopicResultTypeEnum.Passed:
                            totalPasses++;
                            break;
                    }

                    MLTopicCandidate candidate = new MLTopicCandidate
                    {
                        Candidate = user,
                        Attempt = attempt
                    };

                    response.CandidateResult.Candidates.Add(candidate);
                }

                response.CandidateResult.Candidates = response.CandidateResult.Candidates.OrderByDescending(c => c.Attempt.ResultCode).ThenByDescending(c => c.Attempt.CompletedDateString).ThenBy(c => c.Candidate.FirstName).ThenBy(c => c.Candidate.LastName).ToList();

                int active = totalFailures + totalIncompleteFirstAttempt + totalPasses;
                int activePercent = (int)Math.Round((decimal)active / totalCount * 100, 1);

                int totalPassesPercent = (int)Math.Round((decimal)totalPasses / totalCount * 100, 1);
                int totalFailurePercent = (int)Math.Round((decimal)totalFailures / totalCount * 100, 1);
                int totalIncompleteFirstAttemptPercent = (int)Math.Round((decimal)totalIncompleteFirstAttempt / totalCount * 100, 1);
                int totalAbsentPercent = 100 - totalPassesPercent - totalFailurePercent - totalIncompleteFirstAttemptPercent;

                response.CandidateResult.Attendance = new Chart
                {
                    Title = "Attendance",
                    IsLegendShown = true,
                    Disclaimer = string.Format("% of Active Participant: {0}% ({1}/{2})", activePercent, active, totalCount),
                    MaxCount = totalCount,
                    Type = (int)Chart.ChartType.Bar,
                    Stats = new List<Chart.Stat>
                    {
                        new Chart.Stat
                        {
                            Count = totalPasses,
                            Name = "Passed",
                            Percentage = totalPassesPercent
                        },
                        new Chart.Stat
                        {
                            Count = totalFailures,
                            Name = "Failed",
                            Percentage = totalFailurePercent
                        },
                        new Chart.Stat
                        {
                            Count = totalIncompleteFirstAttempt,
                            Name = "Incomplete",
                            Percentage = totalIncompleteFirstAttemptPercent
                        },
                        new Chart.Stat
                        {
                            Count = totalAbsent,
                            Name = "Absent",
                            Percentage = totalAbsentPercent
                        }
                    }
                };

                int totalOverallPassingBase = totalPasses + totalFailures;
                int totalOverallPassingPercent = totalOverallPassingBase > 0 ? (int)Math.Round((decimal)totalPasses / totalOverallPassingBase * 100, 0) : 0;

                response.CandidateResult.PassingRate = new Chart
                {
                    Title = "Overall Passing Rate",
                    IsLegendShown = false,
                    Disclaimer = string.Format("% of unique individuals who passed the test"),
                    MaxCount = totalOverallPassingBase,
                    Type = (int)Chart.ChartType.Doughnut,
                    Labels = new List<string>
                    {
                        string.Format("{0} Active Participants", totalOverallPassingBase)
                    },
                    Stats = new List<Chart.Stat>
                    {
                        new Chart.Stat
                        {
                            Count = totalPasses,
                            Name = "Pass",
                            Percentage = totalOverallPassingPercent
                        }
                    }
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
        }

        public List<MLCard> SelectQuestionResult(List<User> targetedAudience, int questionToggleType, MLSelectQuestionResultResponse response, ISession analyticSession)
        {
            List<MLCard> sortedCards = new List<MLCard>();
            try
            {
                PreparedStatement ps = null;

                switch (questionToggleType)
                {
                    case (int)MLCardToggleTypeEnum.AllQuestions:
                        foreach (MLCard card in response.Topic.Cards)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_card_attempt_log", new List<string>(), new List<string> { "ml_topic_id", "card_id" }));
                            Row sortedRow = analyticSession.Execute(ps.Bind(response.Topic.TopicId, card.CardId)).FirstOrDefault();
                            if (sortedRow != null)
                            {
                                card.CorrectAttempt = sortedRow.GetValue<int>("correct_attempt");
                                card.IncorrectAttempt = sortedRow.GetValue<int>("incorrect_attempt");
                            }
                        }
                        break;
                    case (int)MLCardToggleTypeEnum.HardestQuestions:
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_card_attempt_log_by_incorrect", new List<string>(), new List<string> { "ml_topic_id" }));
                        RowSet sortedIncorrectRowset = analyticSession.Execute(ps.Bind(response.Topic.TopicId));
                        foreach (Row sortedIncorrectRow in sortedIncorrectRowset)
                        {
                            string cardId = sortedIncorrectRow.GetValue<string>("card_id");
                            int correctAttempt = sortedIncorrectRow.GetValue<int>("correct_attempt");
                            int incorrectAttempt = sortedIncorrectRow.GetValue<int>("incorrect_attempt");

                            MLCard selectedCard = response.Topic.Cards.FirstOrDefault(c => c.CardId.Equals(cardId));
                            selectedCard.CorrectAttempt = correctAttempt;
                            selectedCard.IncorrectAttempt = incorrectAttempt;
                            sortedCards.Add(selectedCard);
                            response.Topic.Cards.RemoveAll(c => c.CardId.Equals(cardId));
                        }

                        sortedCards.AddRange(response.Topic.Cards);
                        response.Topic.Cards = sortedCards;
                        break;
                    case (int)MLCardToggleTypeEnum.EasiestQuestions:
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_card_attempt_log_by_correct", new List<string>(), new List<string> { "ml_topic_id" }));
                        RowSet sortedCorrectRowset = analyticSession.Execute(ps.Bind(response.Topic.TopicId));
                        foreach (Row sortedCorrectRow in sortedCorrectRowset)
                        {
                            string cardId = sortedCorrectRow.GetValue<string>("card_id");
                            int correctAttempt = sortedCorrectRow.GetValue<int>("correct_attempt");
                            int incorrectAttempt = sortedCorrectRow.GetValue<int>("incorrect_attempt");

                            MLCard selectedCard = response.Topic.Cards.FirstOrDefault(c => c.CardId.Equals(cardId));
                            selectedCard.CorrectAttempt = correctAttempt;
                            selectedCard.IncorrectAttempt = incorrectAttempt;
                            sortedCards.Add(selectedCard);
                            response.Topic.Cards.RemoveAll(c => c.CardId.Equals(cardId));
                        }

                        sortedCards.AddRange(response.Topic.Cards);
                        response.Topic.Cards = sortedCards;
                        break;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return sortedCards;
        }

        public void SelectPreviewCardResult(string topicId, MLCard selectedCard, List<User> targetedAudience, ISession analyticSession)
        {
            try
            {
                PreparedStatement ps = null;

                if (selectedCard != null)
                {
                    int totalSelection = 0;
                    // Get total selection per option
                    foreach (MLOption option in selectedCard.Options)
                    {
                        foreach (User user in targetedAudience)
                        {
                            string currentAttemptString = SelectMLAttempt(topicId, user.UserId, 0, analyticSession).Attempt;

                            int currentAttempt = 1;
                            if (!currentAttemptString.Equals("NA"))
                            {
                                currentAttempt = Convert.ToInt16(currentAttemptString);
                            }

                            ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "attempt", "card_id", "option_id" }));
                            Row selectionRow = analyticSession.Execute(ps.Bind(user.UserId, currentAttempt, selectedCard.CardId, option.OptionId)).FirstOrDefault();

                            if (selectionRow != null)
                            {
                                option.NumberOfSelection++;
                                totalSelection++;
                            }

                        }
                    }

                    foreach (MLOption option in selectedCard.Options)
                    {
                        option.PercentageOfSelection = totalSelection == 0 ? 0 : (int)Math.Round((decimal)option.NumberOfSelection / totalSelection * 100, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectUserAttemptHistoryResult(List<User> targetedAudience, User answeredByUser, int questionToggleType, MLSelectUserAttemptHistoryResponse response, double timeoffset, ISession analyticSession)
        {
            response.RankChart = new Chart();
            response.ScoreChart = new Chart();
            try
            {
                PreparedStatement ps = null;
                MLTopicAttempt currentAttempt = SelectMLAttempt(response.Topic.TopicId, answeredByUser.UserId, timeoffset, analyticSession);
                response.Attempt = currentAttempt;

                if (currentAttempt.ResultCode == (int)MLTopicResultTypeEnum.Absent || currentAttempt.ResultCode == (int)MLTopicResultTypeEnum.Incomplete)
                {
                    return;
                }

                int attemptInt = Convert.ToInt16(currentAttempt.Attempt);
                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_progress_by_user", new List<string>(), new List<string> { "user_id", "attempt", "ml_topic_id" }));
                Row progressRow = analyticSession.Execute(ps.Bind(answeredByUser.UserId, attemptInt, response.Topic.TopicId)).FirstOrDefault();

                int totalScore = progressRow.GetValue<int>("total_score");
                int userScore = progressRow.GetValue<int>("user_score");
                response.ScoreChart = new Chart
                {
                    Title = "Score",
                    Type = (int)Chart.ChartType.Doughnut,
                    Labels = new List<string>
                        {
                            string.Format("{0} {1}/{2}", currentAttempt.ResultCode == (int)MLTopicResultTypeEnum.Passed ? "Passed" : "Failed", userScore, totalScore)
                        },
                    MaxCount = totalScore,
                    IsLegendShown = false,
                    Stats = new List<Chart.Stat>
                        {
                            new Chart.Stat
                            {
                                Name = string.Empty,
                                Percentage = (int)Math.Round((decimal)userScore / totalScore * 100, 0),
                                Count = userScore
                            }
                        }
                };

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_pass_ranking", new List<string>(), new List<string> { "ml_topic_id" }));
                List<Row> rankingList = analyticSession.Execute(ps.Bind(response.Topic.TopicId)).ToList();

                int ranking = 0;
                bool isFoundAtPass = false;
                List<string> alreadyInRanking = new List<string>();
                foreach (Row rankingRow in rankingList)
                {
                    string rankedUserId = rankingRow.GetValue<string>("user_id");
                    int rankedAttempt = rankingRow.GetValue<int>("attempt");
                    if (!rankedUserId.Equals(answeredByUser.UserId))
                    {
                        if (!alreadyInRanking.Contains(rankedUserId))
                        {
                            ranking++;
                            alreadyInRanking.Add(rankedUserId);
                        }
                    }
                    else
                    {
                        if (rankedAttempt == attemptInt)
                        {
                            ranking++;
                            isFoundAtPass = true;
                            break;
                        }
                    }
                }

                if (!isFoundAtPass)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_fail_ranking", new List<string>(), new List<string> { "ml_topic_id" }));
                    rankingList = analyticSession.Execute(ps.Bind(response.Topic.TopicId)).ToList();
                    foreach (Row rankingRow in rankingList)
                    {
                        string rankedUserId = rankingRow.GetValue<string>("user_id");
                        int rankedAttempt = rankingRow.GetValue<int>("attempt");
                        if (!rankedUserId.Equals(answeredByUser.UserId))
                        {
                            if (!alreadyInRanking.Contains(rankedUserId))
                            {
                                ranking++;
                                alreadyInRanking.Add(rankedUserId);
                            }
                        }
                        else
                        {
                            if (rankedAttempt == attemptInt)
                            {
                                ranking++;
                                break;
                            }
                        }
                    }
                }

                if (ranking >= 0)
                {
                    response.RankChart = new Chart
                    {
                        Title = "Rank",
                        Type = (int)Chart.ChartType.OneToOne,
                        Labels = new List<string>
                            {
                                string.Format("/{0} Candidates", targetedAudience.Count)
                            },
                        Disclaimer = "Ranking is sorted by attempt, then by score then followed by time",
                        MaxCount = targetedAudience.Count,
                        IsLegendShown = false,
                        Stats = new List<Chart.Stat>
                            {
                                new Chart.Stat
                                {
                                    Name = string.Empty,
                                    Percentage = 0,
                                    Count = ranking
                                }
                            }
                    };
                }

                SelectSortedUserCardHistory(answeredByUser.UserId, response.Topic.Cards, questionToggleType, analyticSession);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
        }

        public void SelectSortedUserCardHistory(string answeredUserId, List<MLCard> cards, int questionToggleType, ISession analyticSession)
        {
            try
            {
                List<MLCard> sortedCards = new List<MLCard>();

                for (int index = 0; index < cards.Count; index++)
                {
                    MLCard card = cards[index];
                    List<string> selectedOptionContent = new List<string>();
                    int totalOptionScore = 0;
                    int totalUserScore = 0;
                    foreach (MLOption option in card.Options)
                    {
                        totalOptionScore += option.Score;
                        if (option.IsSelected)
                        {
                            totalUserScore += option.Score;
                        }
                    }

                    bool isCorrect = totalOptionScore == totalUserScore ? true : false;

                    switch (questionToggleType)
                    {
                        case (int)MLCardToggleTypeEnum.AllQuestions:
                            sortedCards.Add(card);
                            break;
                        case (int)MLCardToggleTypeEnum.CorrectAnswers:
                            if (isCorrect && totalOptionScore != 0)
                            {
                                sortedCards.Add(card);
                            }
                            break;
                        case (int)MLCardToggleTypeEnum.WrongAnswers:
                            if (!isCorrect && totalOptionScore != 0)
                            {
                                sortedCards.Add(card);
                            }
                            break;
                    }

                    card.UserCardScore = totalUserScore;
                }

                cards = new List<MLCard>();
                cards = sortedCards;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private MLUpdateAttemptResponse UpdateTopicAttempt(string companyId, string topicId, int totalOptionScore, int userScore, ISession analyticSession)
        {
            MLUpdateAttemptResponse response = new MLUpdateAttemptResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_topic_attempt_log", new List<string>(), new List<string> { "company_id", "ml_topic_id" }));
                Row attemptRow = analyticSession.Execute(ps.Bind(companyId, topicId)).FirstOrDefault();

                float currentCorrectRatio = 0.0f;
                float currentIncorrectRatio = 0.0f;
                int currentCorrectAttempt = 0;
                int currentIncorrectAttempt = 0;

                if (attemptRow != null)
                {
                    currentCorrectRatio = attemptRow.GetValue<float>("correct_ratio");
                    currentIncorrectRatio = attemptRow.GetValue<float>("incorrect_ratio");
                    currentCorrectAttempt = attemptRow.GetValue<int>("correct_attempt");
                    currentIncorrectAttempt = attemptRow.GetValue<int>("incorrect_attempt");
                }

                int newCorrectAttempt = 0;
                int newIncorrectAttempt = 0;
                float newCorrectRatio = 0.0f;
                float newIncorrectRatio = 0.0f;

                if (totalOptionScore != userScore)
                {
                    newCorrectAttempt = currentCorrectAttempt;
                    newIncorrectAttempt = currentIncorrectAttempt + 1;
                }
                else
                {
                    newIncorrectAttempt = currentIncorrectAttempt;
                    newCorrectAttempt = currentCorrectAttempt + 1;
                }

                newCorrectRatio = (float)newCorrectAttempt / (newCorrectAttempt + newIncorrectAttempt);
                newIncorrectRatio = (float)newIncorrectAttempt / (newCorrectAttempt + newIncorrectAttempt);

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_topic_attempt_log", new List<string> { "company_id", "ml_topic_id" }, new List<string> { "correct_ratio", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(newCorrectRatio, newIncorrectRatio, newCorrectAttempt, newIncorrectAttempt, companyId, topicId));

                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_topic_attempt_log_by_correct", new List<string> { "company_id", "correct_ratio", "ml_topic_id" }));
                response.DeleteStatements.Add(ps.Bind(companyId, currentCorrectRatio, topicId));

                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_topic_attempt_log_by_incorrect", new List<string> { "company_id", "incorrect_ratio", "ml_topic_id" }));
                response.DeleteStatements.Add(ps.Bind(companyId, currentIncorrectRatio, topicId));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_topic_attempt_log_by_correct", new List<string> { "company_id", "correct_ratio", "ml_topic_id", "correct_attempt", "incorrect_attempt" }));
                response.UpdateStatements.Add(ps.Bind(companyId, newCorrectRatio, topicId, newCorrectAttempt, newIncorrectAttempt));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_topic_attempt_log_by_incorrect", new List<string> { "company_id", "incorrect_ratio", "ml_topic_id", "correct_attempt", "incorrect_attempt" }));
                response.UpdateStatements.Add(ps.Bind(companyId, newIncorrectRatio, topicId, newCorrectAttempt, newIncorrectAttempt));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private MLUpdateAttemptResponse UpdateTopicCardAttempt(string topicId, string cardId, int totalOptionScore, int userScore, ISession analyticSession)
        {
            MLUpdateAttemptResponse response = new MLUpdateAttemptResponse();
            response.UpdateStatements = new List<BoundStatement>();
            response.DeleteStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("ml_card_attempt_log", new List<string>(), new List<string> { "ml_topic_id", "card_id" }));
                Row attemptRow = analyticSession.Execute(ps.Bind(topicId, cardId)).FirstOrDefault();

                float currentCorrectRatio = 0.0f;
                float currentIncorrectRatio = 0.0f;
                int currentCorrectAttempt = 0;
                int currentIncorrectAttempt = 0;

                if (attemptRow != null)
                {
                    currentCorrectRatio = attemptRow.GetValue<float>("correct_ratio");
                    currentIncorrectRatio = attemptRow.GetValue<float>("incorrect_ratio");
                    currentCorrectAttempt = attemptRow.GetValue<int>("correct_attempt");
                    currentIncorrectAttempt = attemptRow.GetValue<int>("incorrect_attempt");
                }

                int newCorrectAttempt = 0;
                int newIncorrectAttempt = 0;
                float newCorrectRatio = 0.0f;
                float newIncorrectRatio = 0.0f;

                if (totalOptionScore != userScore)
                {
                    newCorrectAttempt = currentCorrectAttempt;
                    newIncorrectAttempt = currentIncorrectAttempt + 1;
                }
                else
                {
                    newIncorrectAttempt = currentIncorrectAttempt;
                    newCorrectAttempt = currentCorrectAttempt + 1;
                }

                newCorrectRatio = (float)newCorrectAttempt / (newCorrectAttempt + newIncorrectAttempt);
                newIncorrectRatio = (float)newIncorrectAttempt / (newCorrectAttempt + newIncorrectAttempt);

                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("ml_card_attempt_log", new List<string> { "ml_topic_id", "card_id" }, new List<string> { "correct_ratio", "incorrect_ratio", "correct_attempt", "incorrect_attempt" }, new List<string>()));
                response.UpdateStatements.Add(ps.Bind(newCorrectRatio, newIncorrectRatio, newCorrectAttempt, newIncorrectAttempt, topicId, cardId));

                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_card_attempt_log_by_correct", new List<string> { "ml_topic_id", "correct_ratio", "card_id" }));
                response.DeleteStatements.Add(ps.Bind(topicId, currentCorrectRatio, cardId));

                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("ml_card_attempt_log_by_incorrect", new List<string> { "ml_topic_id", "incorrect_ratio", "card_id" }));
                response.DeleteStatements.Add(ps.Bind(topicId, currentIncorrectRatio, cardId));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_card_attempt_log_by_correct", new List<string> { "ml_topic_id", "correct_ratio", "card_id", "correct_attempt", "incorrect_attempt" }));
                response.UpdateStatements.Add(ps.Bind(topicId, newCorrectRatio, cardId, newCorrectAttempt, newIncorrectAttempt));

                ps = analyticSession.Prepare(CQLGenerator.InsertStatement("ml_card_attempt_log_by_incorrect", new List<string> { "ml_topic_id", "incorrect_ratio", "card_id", "correct_attempt", "incorrect_attempt" }));
                response.UpdateStatements.Add(ps.Bind(topicId, newIncorrectRatio, cardId, newCorrectAttempt, newIncorrectAttempt));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt32(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
