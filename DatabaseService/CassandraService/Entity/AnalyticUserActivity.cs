﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Web.Configuration;
using System.Web.Hosting;

namespace CassandraService.Entity
{
    public class AnalyticUserActivity
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataContract]
        public class MAUReport
        {
            [DataMember]
            public double TotalTimeInSeconds { get; set; }
            [DataMember]
            public double AvgTimeInSecondsPerUser { get; set; }
            [DataMember]
            public double TotalTimeInHour { get; set; }
            [DataMember]
            public double AvgTimeInHourPerUser { get; set; }
            [DataMember]
            public int TotalUserCount { get; set; }
            [DataMember]
            public string Month { get; set; }
        }

        [DataContract]
        public class ActivityReport
        {
            [DataMember]
            public User User { get; set; }
            [DataMember]
            public double TotalTimeInSeconds { get; set; }
            [DataMember]
            public double TotalTimeInHour { get; set; }
            [DataMember]
            public string TotalDurationString { get; set; }
        }

        [DataContract]
        public class ActivityBreakdownReport
        {
            [DataMember]
            public User User { get; set; }
            [DataMember]
            public string StartTimestamp { get; set; }
            [DataMember]
            public string EndTimestamp { get; set; }
            [DataMember]
            public double TimeInSeconds { get; set; }
            [DataMember]
            public double TimeInHour { get; set; }
            [DataMember]
            public string DurationString { get; set; }
        }

        public void UpdateUserActivity(string userId,
                                       string companyId,
                                       bool isActive)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                string logId = UUIDGenerator.GenerateUniqueIDForLog();
                DateTime currentDatetime = DateTime.UtcNow;

                // The date will have some problem due to zone time 
                PreparedStatement psUserActivityLog = session.Prepare(CQLGenerator.InsertStatement("user_activity_log",
                    new List<string> { "id", "company_id", "user_id", "is_active", "activity_datestamp", "activity_date_timestamp" }));
                BoundStatement bsUserActivityLog = psUserActivityLog.Bind(logId, companyId, userId, isActive, DateHelper.ConvertDateToLong(currentDatetime.Date), DateHelper.ConvertDateToLong(currentDatetime));

                session.Execute(bsUserActivityLog);

                //Log.Debug("Updated activity successfully");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public UserUpdateLoginResponse UpdateUserLogin(string userId,
                                                       string companyId,
                                                       bool isLogin,
                                                       ISession analyticSession = null)
        {
            UserUpdateLoginResponse response = new UserUpdateLoginResponse();
            response.Success = false;

            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                string logId = UUIDGenerator.GenerateUniqueIDForLog();
                DateTime currentDateTime = DateTime.UtcNow;

                bool isToUpdate = true;
                PreparedStatement ps = null;
                BatchStatement updateBatch = new BatchStatement();

                // Check if previously login
                if (isLogin)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("user_login_log_by_user", new List<string>(), new List<string> { "company_id", "user_id" }, "login_date_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 1));
                    Row latestLoginRow = analyticSession.Execute(ps.Bind(companyId, userId, currentDateTime)).FirstOrDefault();
                    if (latestLoginRow != null)
                    {
                        if (latestLoginRow.GetValue<bool>("is_login"))
                        {
                            Log.Debug("No need update login table!");
                            isToUpdate = false;
                        }
                    }
                }

                if (isToUpdate)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("user_login_log",
                        new List<string> { "id", "company_id", "user_id", "is_login", "login_datestamp", "login_date_timestamp" }));
                    updateBatch.Add(ps.Bind(logId, companyId, userId, isLogin, currentDateTime.Date, currentDateTime));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("user_login_log_by_user",
                       new List<string> { "id", "company_id", "user_id", "is_login", "login_datestamp", "login_date_timestamp" }));
                    updateBatch.Add(ps.Bind(logId, companyId, userId, isLogin, currentDateTime.Date, currentDateTime));

                    analyticSession.Execute(updateBatch);
                }

                if (!isLogin)
                {
                    UserSelectDeviceTokenResponse userResponse = new User().SelectUserDeviceToken(userId);
                    response.DeviceToken = userResponse.DeviceToken;
                    response.DeviceType = userResponse.DeviceType;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticMonthlyActiveUserResponse GetMonthlyActiveUsers(string adminUserId, string companyId, DateTime startDate, DateTime endDate)
        {
            AnalyticMonthlyActiveUserResponse response = new AnalyticMonthlyActiveUserResponse();
            response.Reports = new List<MAUReport>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                // Strip off all hours, minute, second and millisecond
                startDate = startDate.AddMilliseconds(-startDate.Millisecond).AddHours(timezoneOffset);

                endDate = endDate.AddMilliseconds(-endDate.Millisecond).AddHours(timezoneOffset);

                int monthsApart = 12 * (endDate.Year - startDate.Year) + endDate.Month - startDate.Month + 1;
                if (monthsApart > 0)
                {
                    DateTime loopStartDate = new DateTime();
                    DateTime loopEndDate = new DateTime();
                    for (int startMonth = 0; startMonth < monthsApart; startMonth++)
                    {
                        loopStartDate = new DateTime(startDate.Year, startDate.Month, 1).AddMonths(startMonth);
                        loopEndDate = new DateTime(startDate.Year, startDate.Month, 1).AddMonths(startMonth + 1);

                        List<User> uniqueUsers = new List<User>();

                        List<Dictionary<string, object>> timeFrameList = new List<Dictionary<string, object>>();
                        double totalTimeInSeconds = 0;

                        PreparedStatement ps = null;
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("user_activity_log", new List<string>(), new List<string> { "company_id" }, "activity_date_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "activity_date_timestamp", CQLGenerator.Comparison.LessThan, 0));

                        User userManager = new User();
                        List<Row> activityRowset = analyticSession.Execute(ps.Bind(companyId, loopStartDate.AddHours(-timezoneOffset), loopEndDate.AddHours(-timezoneOffset))).ToList();

                        for (int index = 0; index < activityRowset.Count; index++)
                        {
                            Row activityRow = activityRowset[index];
                            bool isActive = activityRow.GetValue<bool>("is_active");

                            if (index == 0 && !isActive)
                            {
                                continue;
                            }

                            string userId = activityRow.GetValue<string>("user_id");
                            DateTime activityDate = activityRow.GetValue<DateTime>("activity_date_timestamp");

                            Dictionary<string, object> timeFrame = timeFrameList.FirstOrDefault(t => t["UserId"].Equals(userId));
                            if (timeFrame == null)
                            {
                                if (uniqueUsers.FirstOrDefault(u => u.UserId.Equals(userId)) == null)
                                {
                                    User selectedUser = userManager.SelectUserBasic(userId, companyId, false, mainSession).User;
                                    if (selectedUser != null)
                                    {
                                        uniqueUsers.Add(selectedUser);
                                    }
                                }

                                // Need the first time to be active
                                if (!isActive)
                                {
                                    continue;
                                }

                                timeFrame = new Dictionary<string, object>();
                                timeFrame["UserId"] = userId;
                                timeFrame["IsActive"] = isActive;
                                timeFrame["LastUpdatedTimestamp"] = activityDate;
                                timeFrameList.Add(timeFrame);
                            }
                            else
                            {
                                bool previouslyIsActive = (bool)timeFrame["IsActive"];
                                // Replace
                                if (previouslyIsActive && isActive)
                                {
                                    timeFrame["IsActive"] = isActive;
                                    timeFrame["LastUpdatedTimestamp"] = activityDate;
                                }
                                else
                                {
                                    DateTime previouslyUpdatedTimestamp = (DateTime)timeFrame["LastUpdatedTimestamp"];
                                    totalTimeInSeconds += Math.Abs((activityDate - previouslyUpdatedTimestamp).TotalSeconds);
                                    timeFrameList.Remove(timeFrame);
                                }
                            }
                        }

                        double totalTimeInHour = totalTimeInSeconds / (60 * 60);
                        MAUReport report = new MAUReport
                        {
                            TotalTimeInSeconds = totalTimeInSeconds,
                            TotalTimeInHour = totalTimeInHour,
                            TotalUserCount = uniqueUsers.Count,
                            AvgTimeInSecondsPerUser = uniqueUsers.Count == 0 ? 0 : totalTimeInSeconds / uniqueUsers.Count,
                            AvgTimeInHourPerUser = uniqueUsers.Count == 0 ? 0 : totalTimeInHour / uniqueUsers.Count,
                            Month = loopStartDate.AddHours(timezoneOffset).Month.ToString()
                        };

                        response.Reports.Add(report);
                        response.Success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticActivityResponse GetActivity(string adminUserId, string companyId, DateTime startDate, DateTime endDate, bool isGroupedByUser = true)
        {
            AnalyticActivityResponse response = new AnalyticActivityResponse();
            response.Reports = new List<ActivityReport>();
            response.BreakdownReports = new List<ActivityBreakdownReport>();
            response.TotalTimeTaken = 0;
            response.AverageTimeTakenPerUser = 0;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                double timezoneOffset = DateHelper.SelectTimeOffsetForCompany(companyId, mainSession);

                // Strip off all hours, minute, second and millisecond
                startDate = startDate.AddMilliseconds(-startDate.Millisecond).AddHours(timezoneOffset);

                endDate = endDate.AddMilliseconds(-endDate.Millisecond).AddHours(timezoneOffset);

                PreparedStatement ps = null;
                ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("user_activity_log", new List<string>(), new List<string> { "company_id" }, "activity_date_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "activity_date_timestamp", CQLGenerator.Comparison.LessThan, 0));

                List<User> uniqueUsers = new List<User>();
                User userManager = new User();
                List<Row> activityRowset = analyticSession.Execute(ps.Bind(companyId, startDate.AddHours(-timezoneOffset), endDate.AddHours(-timezoneOffset))).ToList();

                List<Dictionary<string, object>> timeFrameList = new List<Dictionary<string, object>>();

                for (int index = 0; index < activityRowset.Count; index++)
                {
                    Row activityRow = activityRowset[index];
                    bool isActive = activityRow.GetValue<bool>("is_active");

                    if (index == 0 && !isActive)
                    {
                        continue;
                    }

                    string userId = activityRow.GetValue<string>("user_id");
                    DateTime activityDate = activityRow.GetValue<DateTime>("activity_date_timestamp");

                    Dictionary<string, object> timeFrame = timeFrameList.FirstOrDefault(t => t["UserId"].Equals(userId));
                    if (timeFrame == null)
                    {
                        if (uniqueUsers.FirstOrDefault(u => u.UserId.Equals(userId)) == null)
                        {
                            User selectedUser = userManager.SelectUserBasic(userId, companyId, false, mainSession, null, true).User;
                            if (selectedUser != null)
                            {
                                if (selectedUser.Departments.Count == 0)
                                {
                                    selectedUser.Departments.Add(new Department
                                    {
                                        Title = "Deleted"
                                    });
                                }
                                uniqueUsers.Add(selectedUser);
                            }
                        }

                        // Need the first time to be active
                        if (!isActive)
                        {
                            continue;
                        }

                        timeFrame = new Dictionary<string, object>();
                        timeFrame["UserId"] = userId;
                        timeFrame["IsActive"] = isActive;
                        timeFrame["LastUpdatedTimestamp"] = activityDate;
                        timeFrameList.Add(timeFrame);
                    }
                    else
                    {
                        bool previouslyIsActive = (bool)timeFrame["IsActive"];
                        // Replace
                        if (previouslyIsActive && isActive)
                        {
                            timeFrame["IsActive"] = isActive;
                            timeFrame["LastUpdatedTimestamp"] = activityDate;
                        }
                        else
                        {
                            DateTime previouslyUpdatedTimestamp = (DateTime)timeFrame["LastUpdatedTimestamp"];
                            double totalTimeInSeconds = Math.Abs((activityDate - previouslyUpdatedTimestamp).TotalSeconds);
                            timeFrameList.Remove(timeFrame);

                            response.BreakdownReports.Add(new ActivityBreakdownReport
                            {
                                User = uniqueUsers.FirstOrDefault(u => u.UserId.Equals(timeFrame["UserId"])),
                                StartTimestamp = previouslyUpdatedTimestamp.AddHours(timezoneOffset).ToString("HH:mm:ss"),
                                EndTimestamp = activityDate.AddHours(timezoneOffset).ToString("HH:mm:ss"),
                                TimeInSeconds = totalTimeInSeconds,
                                TimeInHour = totalTimeInSeconds / (60 * 60),
                                DurationString = GenerateTimeTakenString(totalTimeInSeconds)
                            });

                            ActivityReport report = response.Reports.FirstOrDefault(r => r.User.UserId.Equals(timeFrame["UserId"]));
                            if (report == null)
                            {
                                response.Reports.Add(new ActivityReport
                                {
                                    User = uniqueUsers.FirstOrDefault(u => u.UserId.Equals(timeFrame["UserId"])),
                                    TotalTimeInSeconds = totalTimeInSeconds,
                                    TotalTimeInHour = totalTimeInSeconds / (60 * 60)
                                });
                            }
                            else
                            {
                                report.TotalTimeInSeconds += totalTimeInSeconds;
                                report.TotalTimeInHour = report.TotalTimeInSeconds / (60 * 60);
                            }

                            response.TotalTimeTaken += totalTimeInSeconds;
                        }
                    }
                }

                if (isGroupedByUser)
                {
                    response.Reports = response.Reports.OrderByDescending(r => r.TotalTimeInSeconds).ToList();
                    List<ActivityBreakdownReport> newSortedBreakdownReports = new List<ActivityBreakdownReport>();
                    foreach (ActivityReport report in response.Reports)
                    {
                        string userId = report.User.UserId;
                        List<ActivityBreakdownReport> reportByUser = response.BreakdownReports.Where(r => r.User.UserId.Equals(userId)).ToList();
                        reportByUser = reportByUser.OrderByDescending(r => r.StartTimestamp).ToList();
                        newSortedBreakdownReports.AddRange(reportByUser);
                        report.TotalDurationString = GenerateTimeTakenString(report.TotalTimeInSeconds);
                    }
                    response.BreakdownReports = newSortedBreakdownReports;
                }
                else
                {
                    response.BreakdownReports = response.BreakdownReports.OrderBy(b => b.StartTimestamp).ToList();
                }

                response.TotalTimeTakenString = GenerateTimeTakenString(response.TotalTimeTaken);
                response.AverageTimeTakenPerUser = response.Reports.Count == 0 ? 0 : response.TotalTimeTaken / response.Reports.Count;
                response.AverageTimeTakenPerUserString = GenerateTimeTakenString(response.AverageTimeTakenPerUser);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private string GenerateTimeTakenString(double time)
        {
            string timeTaken = string.Empty;
            try
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(time);

                int days = timeSpan.Days;
                int hours = timeSpan.Hours;
                int minutes = timeSpan.Minutes;
                int seconds = timeSpan.Seconds;

                if (days > 0)
                {
                    timeTaken += $"{days} days ";
                }
                if (hours > 0)
                {
                    timeTaken += $"{hours} hours ";
                }
                if (minutes > 0)
                {
                    timeTaken += $"{minutes} mins ";
                }
                if (seconds >= 0)
                {
                    timeTaken += $"{seconds} secs ";
                }

                timeTaken = timeTaken.Trim();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return timeTaken;
        }

    }
}
