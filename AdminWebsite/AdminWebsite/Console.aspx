﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Console.aspx.cs" Inherits="AdminWebsite.Console1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">
            <asp:Literal ID="ltlAccountType" runat="server" /><span> console</span>
        </div>
        <div class="appbar__action">
            <%--<a aria-disabled="true"><i class="fa fa-ellipsis-v"></i></a>--%>
            <%--<a aria-disabled="true"><i class="fa fa-question-circle"></i></a>--%>
            <!-- <a class="data-sidebar-toggle" href="#"><i class="fa fa-filter"></i></a> -->
        </div>
    </div>
    <!-- / App Bar -->

    <div class="adminconsole">
        <div class="adminconsole__main">
            <asp:Repeater ID="rtModule" runat="server" OnItemDataBound="rtModule_ItemDataBound">
                <HeaderTemplate>
                    <div class="adminconsole__main__container">
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="hlModule" CssClass="sectionlink" runat="server">
                        <span class="sectionlink__image">
                            <asp:Image ID="imgModule" runat="server" />
                        </span>
                        <asp:Label ID="lblTitle" runat="server" CssClass="sectionlink__title" />
                        <asp:Label ID="lbldescription" runat="server" CssClass="sectionlink__description" />
                    </asp:HyperLink>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <aside class="adminconsole__side" style="display: none">
            <div class="unit">
                <h3 class="unit__title">Activities in last 7 days</h3>
                <ul class="activity">
                    <li class="activity__item">3 Active Users
                    <span class="activity__item__percentage">10%</span>
                    </li>
                    <li class="activity__item">1 Topic created
                    <span class="activity__item__percentage">100%</span>
                    </li>
                    <li class="activity__item">3 Posts on Feed
                    </li>
                </ul>
                <a class="unit__link">View Reports</a>
            </div>
            <div class="unit">
                <h3 class="unit__title">Notifications</h3>
                <ul class="notifications">
                    <li class="notifications__item">
                        <img src="img/image-01.jpg" alt="" class="notifications__item__image">
                        <p class="notifications__item__body"><b>Alan Lee</b> reported <b>Will Tan</b>'s post as inappropiate.</p>
                    </li>
                    <li class="notifications__item">
                        <img src="img/image-01.jpg" alt="" class="notifications__item__image">
                        <p class="notifications__item__body"><b>Lily Wee</b> reported reported <b>Company 101</b>as faulty.</p>
                    </li>
                    <li class="notifications__item">
                        <img src="img/image-01.jpg" alt="" class="notifications__item__image">
                        <p class="notifications__item__body"><b>Poon Jun Jin</b> submitted a display photo for approval.</p>
                    </li>
                </ul>
                <a class="unit__link">View Dashboard</a>
            </div>
        </aside>
    </div>
    <!-- / Main Content -->
</asp:Content>
