﻿using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CassandraService.Entity.Company;

namespace AdminWebsite
{
    public class CompanyAjaxRequest
    {
        public string adminUserId { get; set; }
        public string companyId { get; set; }
        public string title { get; set; }
        public bool isFilterApplied { get; set; }
        public string textColor { get; set; }
        public string backgroundUrl { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public bool isPrioritized { get; set; }
        public List<string> targetedDepartmentIds { get; set; }
        public List<string> targetedUserIds { get; set; }
    }

    public class GetCompanyDetailRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
    }

    public class GetCompanyDetailResponse : ServiceResponse
    {
        public string CompanyId { get; set; }
        public string CompanyTitle { get; set; }
        public string AdminProfileImageUrl { get; set; }
        public double Timezone { get; set; }
        public string EmailInvitationDescription { get; set; }
        public string EmailInvitationSupportInfo { get; set; }
        public string EmailInvitationTitle { get; set; }
    }


    #region UploadCompanyImage
    public class UploadCompanyImageRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
        public string PresentImageId { get; set; }
        public int CompanyImageType { get; set; }
        public string ImageBase64 { get; set; }
        public string ImageExtension { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
    }
    #endregion

    #region GetCompanyImages
    public class GetCompanyImagesRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
        public int CompanyImageType { get; set; }
        public int FetchCount { get; set; }
        public DateTime LastFetchTimestamp { get; set; }
        public bool IsNextPage { get; set; }
    }
    #endregion

    #region GetCompanyImageDetail
    public class GetCompanyImageDetailRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
        public int CompanyImageType { get; set; }
        public string ImageId { get; set; }
       
    }
    #endregion

    #region SetInUseCompanyImage
    public class SetInUseCompanyImageRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
        public string CompanyImageId { get; set; }
        public int CompanyImageType { get; set; }
    }
    #endregion

    #region DeleteCompanyImage
    public class DeleteCompanyImageRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
        public string CompanyImageId { get; set; }
        public int CompanyImageType { get; set; }
    }
    #endregion

    public class GetCompanyAlbumResponse : ServiceResponse
    {
        public class CompaanyImage
        {
            public string ImageId { get; set; }
            public string ImageUrl { get; set; }
        }

        public List<CompaanyImage> Images { get; set; }
        public int ImagesType { get; set; }
    }

    [RoutePrefix("Company")]
    public class CompanyController : ApiController
    {
        private AdminService asc = new AdminService();

        [AcceptVerbs("GET", "POST")]
        [Route("UploadCompanyImage")]
        public CompanyImageResponse UploadCompanyImage(UploadCompanyImageRequest request)
        {
            CompanyImageResponse response = new CompanyImageResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check input
                if (string.IsNullOrEmpty(request.CompanyId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ManagerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ImageBase64) || string.IsNullOrEmpty(request.ImageExtension) || request.ImageWidth == 0 || request.ImageHeight == 0 || request.CompanyImageType == 0)
                {
                    response.ErrorCode = -99999;
                    response.ErrorMessage = "Input of image is not valid.";
                    return response;
                }
                #endregion

                #region Step 2. Upload image to S3 (/cocadre-{CompanyId}/company/{CompanyImageType}/{timestamp}_original.{extension})
                string imgBase64String = string.Empty, imgFormat = string.Empty, folderName = "company/", imageUrl = string.Empty;
                string bucketName = "cocadre-" + request.CompanyId.ToLower();

                switch (request.CompanyImageType)
                {
                    case (int)CompanyImageType.AdminProfile:
                        folderName += "admin_photo";
                        break;

                    case (int)CompanyImageType.EmailSquare:
                        folderName += "email_square";
                        break;

                    case (int)CompanyImageType.WebsiteHeader:
                        folderName += "website_header";
                        break;

                    case (int)CompanyImageType.LoginSelection:
                        folderName += "login_selection";
                        break;

                    case (int)CompanyImageType.ClientBanner:
                        folderName += "main_banner";
                        break;

                    case (int)CompanyImageType.ClientPullRefresh:
                        folderName += "animation_logo";
                        break;

                    case (int)CompanyImageType.ClientMatchUp:
                        folderName += "matchup_banner";
                        break;

                    case (int)CompanyImageType.ClientPopUp:
                        folderName += "popup_photo";
                        break;

                    default:
                        // do nothing
                        break;
                }


                String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                IAmazonS3 s3Client = S3Utility.GetIAmazonS3(request.CompanyId, request.ManagerId);
                S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                using (s3Client)
                {
                    imgBase64String = request.ImageBase64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                    imgFormat = request.ImageExtension.Replace("image/", "");
                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    metadatas.Add("Width", Convert.ToString(request.ImageWidth));
                    metadatas.Add("Height", Convert.ToString(request.ImageHeight));
                    byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                    String fileName = PROCESS_TIMESTAMP + "_original." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                }
                #endregion

                #region  Step 3. Access to database.
                // Get In use image
                //CompanyImagesponse inUseCompanyImage = asc.GetInUseCompanyImage(request.CompanyId, request.ManagerId, request.CompanyImageType);
                response = asc.CreateCompanyImage(request.CompanyId, imageUrl, request.CompanyImageType, request.ManagerId);
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("GET", "POST")]
        [Route("GetCompanyImages")]
        public CompanyImagesResponse GetCompanyImages(GetCompanyImagesRequest request)
        {
            CompanyImagesResponse response = new CompanyImagesResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check input
                if (string.IsNullOrEmpty(request.CompanyId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ManagerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region  Step 2. Access to database.
                response = asc.GetCompanyImagesDesc(request.CompanyId, request.ManagerId, request.CompanyImageType, request.LastFetchTimestamp, request.FetchCount, request.IsNextPage);
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SetInUseCompanyImage")]
        public CompanyImageResponse SetInUseCompanyImage(SetInUseCompanyImageRequest request)
        {
            CompanyImageResponse response = new CompanyImageResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check input
                if (string.IsNullOrEmpty(request.CompanyId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ManagerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.CompanyImageId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.CompanyMissingImage);
                    response.ErrorMessage = ErrorMessage.CompanyMissingImage;
                    return response;
                }
                #endregion

                #region  Step 2. Access to database.
                response = asc.SetSelectedCompanyImage(request.CompanyId, request.CompanyImageId, request.ManagerId, request.CompanyImageType);
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("GET", "POST")]
        [Route("DeleteCompanyImage")]
        public CompanyImageResponse DeleteCompanyImage(DeleteCompanyImageRequest request)
        {
            CompanyImageResponse response = new CompanyImageResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check input
                if (string.IsNullOrEmpty(request.CompanyId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ManagerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.CompanyImageId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.CompanyMissingImage);
                    response.ErrorMessage = ErrorMessage.CompanyMissingImage;
                    return response;
                }
                #endregion

                #region  Step 2. Access to database.
                response = asc.DeleteCompanyImage(request.CompanyId, request.CompanyImageId, request.ManagerId, request.CompanyImageType);
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("GET", "POST")]
        [Route("GetCompanyImageDetail")]
        public GetCompanyImageDetailResponse GetCompanyImageDetail(GetCompanyImageDetailRequest request)
        {
            GetCompanyImageDetailResponse response = new GetCompanyImageDetailResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check input
                if (string.IsNullOrEmpty(request.CompanyId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ManagerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorMessage.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region  Step 2. Access to database.
                response = asc.GetCompanyImageDetail(request.CompanyId, request.ManagerId, request.ImageId, request.CompanyImageType);
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }


        // GET api/<controller>
        public DepartmentListResponse Get(string userId, string companyId)
        {
            //DepartmentListResponse response = asc.GetAllDepartment(userId, companyId);
            //if (response.Success)
            //{
            //    return response.Departments;
            //}

            //return new string[] { "card1", "card2" };

            return asc.GetAllDepartment(userId, companyId);
        }

        // GET api/<controller>/5
        public string Get([FromBody]PulseBannerCardAjaxRequest req)
        {
            return "value";
        }

        // POST api/<controller>
        //public CassandraService.ServiceResponses.RSCardSelectResponse Post([FromBody]PulseBannerCardAjaxRequest req)
        //{

        //    return null;
        //}

        // PUT api/<controller>/5
        //public CassandraService.ServiceResponses.RSCardUpdateResponse Put(int id, [FromBody]PulseAnnouncementCardAjaxRequest req)
        public CassandraService.ServiceResponses.PulseCreateResponse Put([FromBody]PulseBannerCardAjaxRequest req)
        {
            //CassandraService.ServiceResponses.PulseCreateResponse response = asc.CreateBanner(req.adminUserId,
            //    req.companyId,
            //    req.title,
            //    req.isFilterApplied,
            //    req.textColor,
            //    req.backgroundUrl,
            //    req.description,
            //    req.status,
            //    req.startDate,
            //    req.endDate,
            //    req.isPrioritized,
            //    req.targetedDepartmentIds,
            //    req.targetedUserIds);

            //return response;
            return null;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {

        }
    }
}