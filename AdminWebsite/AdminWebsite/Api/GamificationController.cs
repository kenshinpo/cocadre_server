﻿using AdminWebsite.App_Code.ServiceRequests;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CassandraService.Entity.Gamification;

namespace AdminWebsite.Api
{
    public class BadgeFileContent
    {
        public string Url { get; set; }
        public bool IsNewFile { get; set; }
        public string Base64 { get; set; }
        public string Extension { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class AchievementRequest : BasicRequest
    {
        public string AchievementId { get; set; }
        public string Title { get; set; }
        public int BadgeStyle { get; set; }
        public int Type { get; set; }
        public int Rule { get; set; }
        public bool IsIconCustomized { get; set; }
        public BadgeFileContent UploadFile { get; set; }
        public int BadgeColor { get; set; }
        public bool IsForEach { get; set; }
        public List<string> TargetTopicIds { get; set; }
        public int PlayTimes { get; set; }
        public int FailTimes { get; set; }
        public string AchieverId { get; set; }
        public string SearchId { get; set; }

        public List<string> AchievementIds { get; set; }
    }

    [RoutePrefix("Api/Gamification")]
    public class GamificationController : ApiController
    {
        private AdminService asc = new AdminService();

        private string[] COLOR_CODE_ARRAY = { "#FFFFFF", "#FF9600", "#FFBB00", "#FF2851", "#C1272D", "#AC00FF", "#0095FF", "#54C7FC", "#95D000", "#5EAA00", "#808080" };

        [AcceptVerbs("POST")]
        [Route("CreateAchievement")]
        public GamificationCreateAchievementResponse CreateAchievement(AchievementRequest request)
        {
            GamificationCreateAchievementResponse response = new GamificationCreateAchievementResponse();

            try
            {
                #region Step 1. Upload Image of Badge to S3.
                request.AchievementId = UUIDGenerator.GenerateUniqueIDForAchievement();
                // S3 Path: cocadre-{CompanyId}/gamifications/achievements/badges/{achievementId}/{fileName}.{extensionName}
                String bucketName = "cocadre-" + request.CompanyId.ToLowerInvariant();
                string folderName = "gamifications/achievements/badges/" + request.AchievementId;
                string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                #endregion

                #region Step 2. Convert data
                int ruleType = -1;
                int playTimes = 0;
                int failTimes = 0;

                switch (request.Rule)
                {
                    case 1:  // PLAY_TO_GET_ALL_TOPICS
                        ruleType = (int)AchievementRuleTypeEnum.AllTopics;
                        playTimes = request.PlayTimes;
                        break;

                    case 2:  // PLAY_TO_GET_SPECIFIC_TOPIC
                        ruleType = (int)AchievementRuleTypeEnum.SpecificTopic;
                        playTimes = request.PlayTimes;
                        break;

                    case 3:  // PLAY_TO_GET_MULTIPLE_TOPICS
                        if (request.IsForEach)
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEach;
                        }
                        else
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEither;
                        }
                        playTimes = request.PlayTimes;
                        break;

                    case 4:  // FAIL_BUT_GET_ALL_TOPICS
                        ruleType = (int)AchievementRuleTypeEnum.LosingStreak;
                        failTimes = request.FailTimes;
                        break;

                    case 5:  // PERFECT_AND_GET_SPECIFIC_TOPIC
                        ruleType = (int)AchievementRuleTypeEnum.SpecificTopic;
                        break;

                    case 6:  // PERFECT_AND_GET_MULTIPLE_TOPICS
                        if (request.IsForEach)
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEach;
                        }
                        else
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEither;
                        }
                        break;

                    default:
                        ruleType = (int)AchievementRuleTypeEnum.AllTopics;
                        break;
                }

                #endregion

                #region Step 3. Call service
                response = asc.CreateAchievement(request.ManagerId,
                request.CompanyId,
                request.AchievementId,
                request.Title,
                request.BadgeStyle,
                request.IsIconCustomized,
                fileUrl,
                COLOR_CODE_ARRAY[request.BadgeColor],
                request.Type,
                ruleType,
                request.TargetTopicIds,
                playTimes,
                failTimes
                );

                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetAchievementDetail")]
        public GamificationSelectAchievementResponse GetAchievementDetail(AchievementRequest request)
        {
            return asc.SelectAchievement(request.ManagerId, request.CompanyId, request.AchievementId);
        }

        [AcceptVerbs("POST")]
        [Route("EditAchievement")]
        public GamificationUpdateAchievementResponse EditAchievement(AchievementRequest request)
        {
            GamificationUpdateAchievementResponse response = new GamificationUpdateAchievementResponse();

            try
            {
                #region Step 1. Upload Image of Badge to S3.
                string fileUrl = string.Empty;
                if (request.UploadFile.IsNewFile)
                {
                    // S3 Path: cocadre-{CompanyId}/gamifications/achievements/badges/{achievementId}/{fileName}.{extensionName}
                    String bucketName = "cocadre-" + request.CompanyId.ToLowerInvariant();
                    string folderName = "gamifications/achievements/badges/" + request.AchievementId;
                    fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                }
                else
                {
                    fileUrl = request.UploadFile.Url;
                }
                #endregion

                #region Step 2. Convert data
                int ruleType = -1;
                int playTimes = 0;
                int failTimes = 0;

                switch (request.Rule)
                {
                    case 1:  // PLAY_TO_GET_ALL_TOPICS
                        ruleType = (int)AchievementRuleTypeEnum.AllTopics;
                        playTimes = request.PlayTimes;
                        break;

                    case 2:  // PLAY_TO_GET_SPECIFIC_TOPIC
                        ruleType = (int)AchievementRuleTypeEnum.SpecificTopic;
                        playTimes = request.PlayTimes;
                        break;

                    case 3:  // PLAY_TO_GET_MULTIPLE_TOPICS
                        if (request.IsForEach)
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEach;
                        }
                        else
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEither;
                        }
                        playTimes = request.PlayTimes;
                        break;

                    case 4:  // FAIL_BUT_GET_ALL_TOPICS
                        ruleType = (int)AchievementRuleTypeEnum.LosingStreak;
                        failTimes = request.FailTimes;
                        break;

                    case 5:  // PERFECT_AND_GET_SPECIFIC_TOPIC
                        ruleType = (int)AchievementRuleTypeEnum.SpecificTopic;
                        break;

                    case 6:  // PERFECT_AND_GET_MULTIPLE_TOPICS
                        if (request.IsForEach)
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEach;
                        }
                        else
                        {
                            ruleType = (int)AchievementRuleTypeEnum.MultipleTopicsByEither;
                        }
                        break;

                    default:
                        ruleType = (int)AchievementRuleTypeEnum.AllTopics;
                        break;
                }

                #endregion

                #region Step 3. Call service
                response = asc.UpdateAchievement(
                    request.ManagerId,
                    request.CompanyId,
                    request.AchievementId,
                    request.Title,
                    request.BadgeStyle,
                    request.IsIconCustomized,
                    fileUrl,
                    COLOR_CODE_ARRAY[request.BadgeColor],
                    request.Type,
                    ruleType,
                    request.TargetTopicIds,
                    playTimes,
                    failTimes
                );

                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("SelectUserAchievementResult")]
        public GamificationSelectUserAchievementResultResponse SelectUserAchievementResult(AchievementRequest request)
        {
            return asc.SelectUserAchievementResult(request.ManagerId, request.CompanyId, request.AchieverId);
        }

        [AcceptVerbs("POST")]
        [Route("SelectAchievementResult")]
        public GamificationSelectAchievementResultResponse SelectAchievementResult(AchievementRequest request)
        {
            return asc.SelectAchievementResult(request.ManagerId, request.CompanyId, request.AchievementId, request.SearchId);
        }

        [AcceptVerbs("POST")]
        [Route("ReorderingAchievements")]
        public GamificationSelectAllAchievementResponse ReorderingAchievements(AchievementRequest request)
        {
            return asc.ReorderingAchievements(request.ManagerId, request.CompanyId, request.AchievementIds);
        }

        private string AddToS3(string companyId, string managerUserId, string bucketName, string folderName, BadgeFileContent fileContent, string processTimestamp = null)
        {
            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(companyId, managerUserId);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String fileBase64String = String.Empty, fileFormat = String.Empty, fileUrl = String.Empty;
            int fileWidth = 0, fileHeight = 0;

            using (s3Client)
            {
                if (string.IsNullOrEmpty(processTimestamp))
                {
                    processTimestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                }


                fileWidth = Convert.ToInt16(fileContent.Width);
                fileHeight = Convert.ToInt16(fileContent.Height);
                fileBase64String = fileContent.Base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                fileFormat = fileContent.Extension.Replace("image/", "");

                #region Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] imgBytes = Convert.FromBase64String(fileBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                String fileName = processTimestamp + "_original" + "." + fileFormat.ToLower();
                metadatas.Clear();
                metadatas.Add("Width", Convert.ToString(fileWidth));
                metadatas.Add("Height", Convert.ToString(fileHeight));
                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                #region Upload large image (450 * 450)
                System.Drawing.Image newImage;
                if (fileWidth > 450 || fileHeight > 450)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(450, 450), ImageUtility.GetImageFormat(originImage));
                    fileName = processTimestamp + "_large" + "." + fileFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = processTimestamp + "_large" + "." + fileFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion

                #region Upload medium image (180 * 180)
                if (fileWidth > 180 || fileHeight > 180)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(180, 180), ImageUtility.GetImageFormat(originImage));
                    fileName = processTimestamp + "_medium" + "." + fileFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = processTimestamp + "_medium" + "." + fileFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion

                #region Upload small image(150 * 150)
                if (fileWidth > 150 || fileHeight > 150)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(150, 150), ImageUtility.GetImageFormat(originImage));
                    fileName = processTimestamp + "_small" + "." + fileFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = processTimestamp + "_small" + "." + fileFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion

                fileUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + processTimestamp + "_original." + fileFormat.ToLower();


                return fileUrl;
            }

        }
    }
}
