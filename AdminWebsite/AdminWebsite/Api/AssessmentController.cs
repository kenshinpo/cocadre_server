﻿using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class AssessmentAjaxRequest
    {
        public string adminUserId { get; set; }
        public string companyId { get; set; }
        public string title { get; set; }
        public string introduction { get; set; }
        public string closingWords { get; set; }
        public string iconUrl { get; set; }
        public string coachId { get; set; }
        public string coachName { get; set; }
        public bool isDisplayCoachName { get; set; }
        public bool isAnonymous { get; set; }
        public List <AssessmentImage> imageBanners { get; set; }
        public List<AssessmentTabulation> tabulation_list { get; set; }
        public List <AssessmentAnswer> result_list { get; set; }
        public string videoUrl { get; set; }
        public string videoTitle { get; set; }
        public string videoDescription { get; set; }
        public string videoThumbnailUrl { get; set; }
        public string cssArea { get; set; }

        public List<CassandraService.Entity.ContentImage> imageBannersUpload { get; set; }

        public CassandraService.Entity.Base64Content videoUpload { get; set; }

        public CassandraService.Entity.Base64Content videoThumbnailUpload { get; set; }

        public AssessmentAjaxRequest()
        {
            this.imageBanners = new List<AssessmentImage>();
            this.tabulation_list = new List<AssessmentTabulation>();
            this.result_list = new List<AssessmentAnswer>();

            this.imageBannersUpload = new List<CassandraService.Entity.ContentImage>();
        }
    }


    public class AssessmentController : ApiController
    {
        public class GetAssessmentAjaxRequest
        {
            public string userId { get; set; }
            public string assessmentId { get; set; }
        }

        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET: api/Assessment
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}
        public AssessmentSelectResponse Get([FromUri]GetAssessmentAjaxRequest req)
        {
            AssessmentSelectResponse response = asc.SelectFullDetailAssessment(req.userId, req.assessmentId);

            return response;
        }

        // GET: api/Assessment/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Assessment
        public void Post([FromBody]string value)
        {
        }

        #region S3 Helper functions
        private string AddToS3(string company_id, string manager_user_id, CassandraService.Entity.ContentImage cardImage, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            // for S3
            String bucketName = "cocadre-marketplace";
            String folderName = string.Format("assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
            int imgWidth = 0, imgHeight = 0;
            string filenamePrefix = string.Empty;

            using (s3Client)
            {
                // Grab image width and height

                imgWidth = Convert.ToInt16(cardImage.width);
                imgHeight = Convert.ToInt16(cardImage.height);
                imgBase64String = cardImage.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "").Replace("data:image/gif;base64,","");
                imgFormat = cardImage.extension.Replace("image/", "");
                filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                String fileName = filenamePrefix + "_original" + "." + imgFormat.ToLower();
                metadatas.Clear();
                metadatas.Add("Width", Convert.ToString(imgWidth));
                metadatas.Add("Height", Convert.ToString(imgHeight));
                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                #region Upload large (if original image is 1920 * 1920)
                // Upload large (if original image is 1920 * 1920)
                System.Drawing.Image newImage;
                if (imgWidth > 1920 || imgHeight > 1920)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload large (if original image is 1920 * 1920)

                #region Upload medium (if original image is 1080 * 1080)
                // Upload medium (if original image is 1080 * 1080)
                if (imgWidth > 1080 || imgHeight > 1080)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload medium (if original image is 1080 * 1080)

                #region Upload small (if original image is 540 * 540)
                // Upload small (if original image is 540 * 540)
                if (imgWidth > 540 || imgHeight > 540)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload small (if original image is 540 * 540)

                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + imgFormat.ToLower();

                return imageUrl;

            }

        }


        private string DumpToS3(string company_id, string manager_user_id, CassandraService.Entity.Base64Content content, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            // for S3
            String bucketName = "cocadre-marketplace";
            String folderName = string.Format("assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            using (s3Client)
            {
                string imgBase64String = content.base64.Remove(0, content.base64.IndexOf(",") + 1);
                string filename = System.IO.Path.GetFileName(content.filename);
                string extension = System.IO.Path.GetExtension(content.filename).Remove(0, 1);
                string filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] contentBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                String fileName = filenamePrefix + "_original" + "." + extension.ToLower();
                S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                string s3url = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + extension.ToLower();

                return s3url;

            }
        }


        private string DumpToTranscoderS3(string company_id, string manager_user_id, CassandraService.Entity.Base64Content content, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            // cocadre-elastic-transcoder-input/dev/cocadre-marketplace/assessment/{AssessmentID}/{video}.mp4

            // for S3
            String bucketName = "cocadre-elastic-transcoder-input";
            String folderName = string.Format("dev/cocadre-marketplace/assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            using (s3Client)
            {
                string imgBase64String = content.base64.Remove(0, content.base64.IndexOf(",") + 1);
                string filename = System.IO.Path.GetFileName(content.filename);
                string extension = System.IO.Path.GetExtension(content.filename).Remove(0, 1);
                string filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] contentBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                String fileName = filenamePrefix + "_original" + "." + extension.ToLower();
                S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                string s3url = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + extension.ToLower();

                return s3url;

            }
        }
        #endregion S3 Helper functions

        // PUT: api/Assessment/5
        public CassandraService.ServiceResponses.AssessmentCreateResponse Put([FromBody]AssessmentAjaxRequest req)
        {
            string assessmentId = UUIDGenerator.GenerateUniqueIDForAssessment();

            //if (req.useCustomImage)
            //{
            //    req.backgroundUrl = AddToS3(req.companyId, req.adminUserId, req.customImageDataUrl, pulseId);
            //}
            //CassandraService.ServiceResponses.AssessmentCreateResponse response = null;

            string oldTabulationId = "";

            Dictionary<string, AssessmentTabulation> tabulationMap = new Dictionary<string, AssessmentTabulation>();

            // Save image banners into S3
            foreach (CassandraService.Entity.ContentImage img in req.imageBannersUpload)
            {
                int imageBannerOrder = 1;
                string bannerUrl = AddToS3(req.companyId, req.adminUserId, img, assessmentId);
                req.imageBanners.Add(new AssessmentImage
                {
                    ImageUrl = bannerUrl,
                    Ordering = imageBannerOrder++
                });
            }

            // Save video into S3
            if (req.videoUpload.filename != null)
            {
                req.videoUrl = DumpToS3(req.companyId, req.adminUserId, req.videoUpload, assessmentId);
                DumpToTranscoderS3(req.companyId, req.adminUserId, req.videoUpload, assessmentId);
            }

            // Save video thumbnail into S3
            if (req.videoThumbnailUpload.filename != null)
            {
                req.videoThumbnailUrl = DumpToS3(req.companyId, req.adminUserId, req.videoThumbnailUpload, assessmentId);
            }

            // Save tabulation images to S3

            foreach (AssessmentTabulation tab in req.tabulation_list)
            {
                oldTabulationId = tab.TabulationId;
                tab.TabulationId = UUIDGenerator.GenerateUniqueIDForAssessmentTabulation();
                tabulationMap.Add(oldTabulationId, tab);

                foreach (CassandraService.Entity.ContentImage img in tab.ImagesUpload)
                {
                    string bannerUrl = AddToS3(req.companyId, req.adminUserId, img, assessmentId);
                    tab.Images.Add(new AssessmentImage
                    {
                        ImageId = UUIDGenerator.GenerateUniqueIDForRSImage(),
                        ImageUrl = bannerUrl
                         
                    });
                }
            }

            foreach (AssessmentAnswer ans in req.result_list)
            {
                ans.AnswerId = UUIDGenerator.GenerateUniqueIDForAssessmentAnswer();
                foreach (string tabulationId in ans.TabulationIdList)
                {
                    ans.Tabulations.Add(tabulationMap[tabulationId]);
                }
            }

            CassandraService.ServiceResponses.AssessmentCreateResponse response = asc.CreateAssessment(
                req.adminUserId,
                req.title,
                req.introduction,
                req.closingWords,
                req.iconUrl,
                req.coachId,
                req.coachName,
                req.isDisplayCoachName,
                req.isAnonymous,
                req.imageBanners,
                req.tabulation_list,
                req.result_list,
                req.videoUrl,
                req.videoTitle,
                req.videoDescription,
                req.videoThumbnailUrl,
                req.cssArea);

            return response;
        }

        // DELETE: api/Assessment/5
        public void Delete(int id)
        {
        }
    }
}
