﻿using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class PulseRequest
    {
        public string CompanyId { get; set; }
        public string AdminUserId { get; set; }
        public string RequesterUserId { get; set; }
        public string PulseId { get; set; }
        public string Title { get; set; }
        public int Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsPrioritized { get; set; }
        public List<string> TargetedDepartmentIds { get; set; }
        public List<string> TargetedUserIds { get; set; }

        public bool IsFilterApplied { get; set; }
        public string TextColor { get; set; }
        public string BackgroundUrl { get; set; }
        public string Description { get; set; }
    }

    public class DeckRequest
    {
        public string CompanyId { get; set; }
        public string AdminUserId { get; set; }
        public string RequesterUserId { get; set; }
        public string Title { get; set; }
        public int DeckType { get; set; }
        public bool IsCompulsory { get; set; }
        public bool IsAnonymous { get; set; }
        public int PublishMethodType { get; set; }
        public int Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsPrioritized { get; set; }
        public List<string> TargetedDepartmentIds { get; set; }
        public List<string> TargetedUserIds { get; set; }
        public int NumberOfCardsPerTimeFrame { get; set; }
        public int PerTimeFrameType { get; set; }

        public string PulseId { get; set; }
        public string DeckId { get; set; }
        public string Description { get; set; }
        public int CardType { get; set; }
        public int QuestionType { get; set; }
        public int NumberOfOptions { get; set; }
        public int RangeType { get; set; }
        public List<DeckCardOption> Options { get; set; }

        public string MinRangeLabel { get; set; }
        public string MaxRangeLabel { get; set; }

        public int UpdatedStatus { get; set; }

        public string AnsweredByUserId { get; set; }
        public int SelectedRange { get; set; }
        public string SelectedOptionId { get; set; }
        public string CustomAnswer { get; set; }

        public string SearchId { get; set; }

        public List<string> DeckIds { get; set; }
    }

    [RoutePrefix("Api/PulseAnalytics")]
    public class PulseAnalyticsController : ApiController
    {
        private AdminService service = new AdminService();

        [AcceptVerbs("GET", "POST")]
        [Route("SelectFeedAnalytic")]
        public PulseSelectSingleAnalyticResponse SelectFeedAnalytic(PulseRequest request)
        {
            return service.SelectAnalyticForFeedPulse(request.AdminUserId, request.CompanyId, request.PulseId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectDeckAnalytic")]
        public PulseSelectDeckAnalyticResponse SelectDeckAnalytic(DeckRequest request)
        {
            return service.SelectDeckAnalytic(request.AdminUserId, request.CompanyId, request.DeckId, request.SearchId);
        }


        [AcceptVerbs("GET", "POST")]
        [Route("SelectDeckCardAnalytic")]
        public PulseSelectDeckCardAnalyticResponse SelectDeckCardAnalytic(DeckRequest request)
        {
            return service.SelectDeckCardAnalytic(request.AdminUserId, request.CompanyId, request.DeckId, request.PulseId, request.SearchId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectDeckCardOptionAnalytic")]
        public PulseSelectDeckCardOptionAnalyticResponse SelectDeckCardOptionAnalytic(DeckRequest request)
        {
            return service.SelectDeckCardOptionAnalytic(request.AdminUserId, request.CompanyId, request.DeckId, request.PulseId, request.SelectedOptionId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectDeckCardCustomAnswerAnalytic")]
        public PulseSelectDeckCardCustomAnswerAnalyticResponse SelectDeckCardCustomAnswerAnalytic(DeckRequest request)
        {
            return service.SelectDeckCardCustomAnswerAnalytic(request.AdminUserId, request.CompanyId, request.DeckId, request.PulseId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectDeckAnalyticByUser")]
        public PulseSelectDeckAnalyticByUserResponse SelectDeckAnalyticByUser(DeckRequest request)
        {
            return service.SelectDeckAnalyticByUser(request.AdminUserId, request.CompanyId, request.DeckId, request.AnsweredByUserId);
        }
    }
}
