﻿using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CassandraService.Entity;
using CassandraService.ServiceInterface;

namespace CocadreMVCWebAPI.Controllers.Service
{
    // Requests
    public class SelectAllRSCategoriesRequest
    {
        public string CompanyId { get; set; }
        public string AdminUserId { get; set; }
        public int QueryType { get; set; }
    }

    public class CreateRSTopicRequest
    {
        public string CompanyId { get; set; }
        public string AdminUserId { get; set; }
        public string Title { get; set; }
        public string Introduction { get; set; }
        public string ClosingWords { get; set; }
        public string CategoryId { get; set; }
        public string CategoryTitle { get; set; }
        public string TopicIconUrl { get; set; }
        public int Status { get; set; }
        public List<string> TargetedDepartmentIds { get; set; }
        public List<string> TargetedUserIds { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int AnonymityCount { get; set; }

        //public bool IsRandomizedAllQuestion { get; set; }
        //public bool IsShowProgressBar { get; set; }
        //public bool IsAllowReturnPrevious { get; set; }
        //public bool IsAllowFeedback { get; set; }
        //public bool IsAllowViewHistory { get; set; }
    }

    public class SelectAllRSTopicsRequest
    {
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }
        public string SelectedCategoryId { get; set; }
        public string ContainsName { get; set; }
    }

    public class CreateCardRequest
    {
        public string CardId { get; set; }
        public int Type { get; set; }
        public string Content { get; set; }
        public string Caption { get; set; }
        public bool HasImage { get; set; }
        public List<RSImage> Images { get; set; }
        public bool ToBeSkipped { get; set; }
        public bool HasCustomAnswer { get; set; }
        public string CustomAnswerInstruction { get; set; }
        public bool IsOptionRandomized { get; set; }
        public bool AllowMultipleLines { get; set; }
        public bool HasPageBreak { get; set; }
        public int Ordering { get; set; }
        public int BackgroundImageType { get; set; }
        public string Note { get; set; }
        public int MaxRange { get; set; }
        public int MinRange { get; set; }

        public string CategoryId { get; set; }
        public string TopicId { get; set; }
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }
        public List<RSOption> Options { get; set; }
        public int MiniOptionToSelect { get; set; }
        public int MaxOptionToSelect { get; set; }
    }

    public class SelectRSTopicRequest
    {
        public string TopicId { get; set; }
        public string CategoryId { get; set; }
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }
    }

    public class SelectRSCardRequest
    {
        public string TopicId { get; set; }
        public string CardId { get; set; }
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }
    }

    public class PreviewRequest
    {
        public string TopicId { get; set; }
        public string CategoryId { get; set; }
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }

        public string CardId { get; set; }
        public string OptionId { get; set; }
        public string AnsweredByUserId { get; set; }
    }

    public class UpdateCardRequest
    {
        public string CardId { get; set; }
        public int Type { get; set; }
        public string TopicId { get; set; }
        public string CategoryId { get; set; }
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }
        public List<RSImage> Images { get; set; }
        public List<RSOption> Options { get; set; }
        public string Note { get; set; }
        public bool HasPageBreak { get; set; }
        public string Content { get; set; }
        public bool HasImage { get; set; }
        public int MiniOptionToSelect { get; set; }
        public int MaxOptionToSelect { get; set; }

        public bool ToBeSkipped { get; set; }
        public bool HasCustomAnswer { get; set; }
        public string CustomAnswerInstruction { get; set; }
        public bool IsOptionRandomized { get; set; }
        public bool AllowMultipleLines { get; set; }
        public int BackgroundType { get; set; }

        //int maxRange,
        //string maxRangeLabel,
        //int midRange,
        //string midRangeLabel,
        //int minRange,
        //string minRangeLabel

        public int MaxRange { get; set; }
        public string MaxRangeLabel { get; set; }

        public int MidRange { get; set; }
        public string MidRangeLabel { get; set; }

        public int MinRange { get; set; }
        public string MinRangeLabel { get; set; }

        public int StartRangePosition { get; set; }
    }


    [RoutePrefix("AdminRS")]
    public class AdminRSController : ApiController
    {
        private AdminService client = new AdminService();

        [AcceptVerbs("GET", "POST")]
        [Route("GetRSCategories")]
        public RSCategorySelectAllResponse GetRSCategories(SelectAllRSCategoriesRequest request)
        {
            return client.SelectAllRSCategories(request.AdminUserId, request.CompanyId, request.QueryType);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("CreateRSTopic")]
        public RSTopicCreateResponse CreateRSTopic(CreateRSTopicRequest request)
        {
            return client.CreateRSTopic(request.AdminUserId,
                                                request.CompanyId,
                                                request.Title,
                                                request.Introduction,
                                                request.ClosingWords,
                                                request.CategoryId,
                                                request.CategoryTitle,
                                                request.TopicIconUrl,
                                                request.Status,
                                                request.TargetedDepartmentIds,
                                                request.TargetedUserIds,
                                                request.StartDate,
                                                request.EndDate.Value,
                                                request.AnonymityCount);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectAllRSTopics")]
        public RSTopicSelectAllBasicResponse SelectAllRSTopics(SelectAllRSTopicsRequest request)
        {
            return client.SelectAllRSTopicByCategory(request.AdminUserId,
                                                             request.CompanyId,
                                                             request.SelectedCategoryId,
                                                             request.ContainsName);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("CreateCard")]
        public RSCardCreateResponse CreateCard(CreateCardRequest request)
        {
            //return client.CreateCard(request.CardId,
            //                                request.Type,
            //                                request.Content,
            //                                request.HasImage,
            //                                request.Images,
            //                                request.ToBeSkipped,
            //                                request.HasCustomAnswer,
            //                                request.CustomAnswerInstruction,
            //                                request.IsOptionRandomized,
            //                                request.AllowMultipleLines,
            //                                request.HasPageBreak,
            //                                request.BackgroundImageType,
            //                                request.Note,
            //                                request.CategoryId,
            //                                request.TopicId,
            //                                request.AdminUserId,
            //                                request.CompanyId,
            //                                request.Options,
            //                                request.MiniOptionToSelect,
            //                                request.MaxOptionToSelect,
            //                                request.MaxRange,
            //                                request.MinRange);
            return null;
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectRSTopic")]
        public RSTopicSelectResponse SelectRSTopic(SelectRSTopicRequest request)
        {
            return client.SelectFullDetailRSTopic(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectRSCard")]
        public RSCardSelectResponse SelectRSCard(SelectRSCardRequest request)
        {
            return client.SelectFullDetailCard(request.AdminUserId, request.CompanyId, request.CardId, request.TopicId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("Preview")]
        public RSCardSelectAllResponse Preview(PreviewRequest request)
        {
            return client.Preview(request.TopicId, request.CategoryId, request.AdminUserId, request.CompanyId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateCard")]
        public RSCardUpdateResponse UpdateCard(UpdateCardRequest request)
        {
            return client.UpdateCard(
                request.CardId,
                request.Type,
                request.Content,
                request.HasImage, 
                request.Images,
                request.ToBeSkipped,
                request.HasCustomAnswer,
                request.CustomAnswerInstruction,
                request.IsOptionRandomized,
                request.AllowMultipleLines,
                request.HasPageBreak,
                request.BackgroundType,
                request.Note,
                request.CategoryId,
                request.TopicId, 
                request.AdminUserId, 
                request.CompanyId, 
                request.Options, 
                request.MiniOptionToSelect, 
                request.MaxOptionToSelect,
                request.StartRangePosition,
                request.MaxRange,
                request.MaxRangeLabel,
                request.MidRange,
                request.MidRangeLabel,
                request.MinRange,
                request.MinRangeLabel);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectResultOverview")]
        public AnalyticSelectRSResultOverviewResponse SelectResultOverview(PreviewRequest request)
        {
            return client.SelectResultOverview(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectCardResult")]
        public AnalyticSelectRSCardResultResponse SelectCardResult(PreviewRequest request)
        {
            return client.SelectCardResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectCardResultByUser")]
        public AnalyticSelectRSCardResultByUserResponse SelectCardResultByUser(PreviewRequest request)
        {
            return client.SelectCardResultByUser(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.AnsweredByUserId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectCustomAnswersResult")]
        public AnalyticSelectRSCustomAnswersResponse SelectCustomAnswersResult(PreviewRequest request)
        {
            return client.SelectCustomAnswersFromCard(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectRespondersReport")]
        public AnalyticSelectRSResponderReportResponse SelectRespondersReport(PreviewRequest request)
        {
            return client.SelectRespondersReport(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectOptionResult")]
        public AnalyticSelectRSOptionResultResponse SelectOptionResult(PreviewRequest request)
        {
            return client.SelectOptionResult(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId, request.OptionId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectFeedback")]
        public RSTopicSelectFeedbackResponse SelectFeedback(PreviewRequest request)
        {
            return client.SelectFeedback(request.AdminUserId, request.CompanyId, request.TopicId, request.CategoryId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("UpdateCountries")]
        public CountryListResponse UpdateCountries()
        {
            return client.UpdateCountries();
        }
    }
}