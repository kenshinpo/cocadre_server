﻿using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    #region Get auto approval profile photo
    public class GetAutoApprovalProfilePhotoRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
    }
    #endregion

    #region Upload auto approval profile photo
    public class UpdateAutoApprovalProfilePhotoRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
        public bool IsAutoApproval { get; set; }
    }
    #endregion

    [RoutePrefix("Setting")]
    public class SettingController : ApiController
    {
        private AdminService asc = new AdminService();

        [AcceptVerbs("POST")]
        [Route("UpdateProfilePhotoSetting")]
        public AutoApprovalProfilePhotoResponse UpdateAutoApprovalProfilePhoto(UpdateAutoApprovalProfilePhotoRequest request)
        {
            AutoApprovalProfilePhotoResponse response = new AutoApprovalProfilePhotoResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check input
                if (string.IsNullOrEmpty(request.CompanyId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ManagerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region Step 2. Call service
                response = asc.UpdateAutoApprovalProfilePhoto(request.CompanyId, request.ManagerId, request.IsAutoApproval);
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetProfilePhotoSetting")]
        public AutoApprovalProfilePhotoResponse GetProfilePhotoSetting(GetAutoApprovalProfilePhotoRequest request)
        {
            AutoApprovalProfilePhotoResponse response = new AutoApprovalProfilePhotoResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check input
                if (string.IsNullOrEmpty(request.CompanyId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(request.ManagerId))
                {
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }
                #endregion

                #region Step 2. Call service
                response = asc.GetAutoApprovalProfilePhoto(request.CompanyId, request.ManagerId);
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }
    }
}
