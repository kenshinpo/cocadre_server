﻿using AdminWebsite.App_Code.ServiceRequests;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CassandraService.Entity.MLCard;

namespace AdminWebsite.Api
{
    public class UploadFileContent
    {
        public const int FILE_TYPE_IMAGE = 1;
        public const int FILE_TYPE_VIDEO_LANDSCAPE = 2;
        public const int FILE_TYPE_PDF_PORTRAIT = 3;
        public const int FILE_TYPE_PDF_LANDSCAPE = 4;
        public const int FILE_TYPE_SLIDESHARE = 5;
        public const int FILE_TYPE_VIDEO_PORTRAIT = 6;

        public int UploadType { get; set; }
        public string Url { get; set; }
        public bool IsNewFile { get; set; }
        public string Base64 { get; set; }
        public string Extension { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public class Option
    {
        public string OptionId { get; set; }
        public string Content { get; set; }
        public int Score { get; set; }
        public UploadFileContent UploadedContent { get; set; }
    }

    //public class BasicRequest
    //{
    //    public string CompanyId { get; set; }
    //    public string ManagerId { get; set; }
    //}

    public class SearchCardsRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string TopicId { get; set; }
        public string KeyWord { get; set; }
    }

    public class GetCardDetailRequest : BasicRequest
    {
        public string CardId { get; set; }
        public string TopicId { get; set; }
    }

    public class DeleteCardRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string TopicId { get; set; }
        public string CardId { get; set; }
    }

    public class AnalyticsRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string TopicId { get; set; }
        public string SearchId { get; set; }
        public string CardId { get; set; }
        public string QuestionToggleType { get; set; }
        public string AnsweredUserId { get; set; }

    }

    public class CreateUpdateCardRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string ExamId { get; set; }
        public string CardId { get; set; }
        public int CardType { get; set; }
        public List<MLPageContent> ArticlePages { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public UploadFileContent UploadFile { get; set; }
        public bool IsUsingSlideShare { get; set; }
        public string SlideShareUrl { get; set; }
        public List<Option> Options { get; set; }
        public bool IsOptionRandomized { get; set; }
        public int MinOptionToSelect { get; set; }
        public int MaxOptionToSelect { get; set; }
        public int BackgroundType { get; set; }
        public string Note { get; set; }
        public bool HasPageBreak { get; set; }
    }

    [RoutePrefix("MLEvaluation")]
    public class MLEvaluationController : ApiController
    {
        private AdminService asc = new AdminService();

        /// <summary>
        /// 前端的 TYPE 與後端的 TYPE 定義不同。利用陣列取得後端的定義值。
        /// </summary>
        private int[] CARD_TYPE_REVERT = new int[] {0,
            (int)CassandraService.Entity.MLCard.MLCardType.ReadAndAnswer,
            (int)CassandraService.Entity.MLCard.MLCardType.WatchAndAnswer,
            (int)CassandraService.Entity.MLCard.MLCardType.Instructional,
            (int)CassandraService.Entity.MLCard.MLCardType.SelectOne,
            (int)CassandraService.Entity.MLCard.MLCardType.MultiChoice,
            (int)CassandraService.Entity.MLCard.MLCardType.Slides
            };

        [AcceptVerbs("POST")]
        [Route("CreateCard")]
        public MLCardCreateResponse CreateCard(CreateUpdateCardRequest request)
        {
            MLCardCreateResponse response = new MLCardCreateResponse();

            try
            {
                #region Step 1. Upload Image/Video/PDF to S3.
                String bucketName = "cocadre-" + request.CompanyId.ToLowerInvariant();
                request.CardId = UUIDGenerator.GenerateUniqueIDForMLCard();
                List<MLUploadedContent> cardUploadFiles = new List<MLUploadedContent>();
                if (request.UploadFile != null && request.UploadFile.IsNewFile)
                {
                    // S3 Path: cocadre-{CompanyId}/mlearnings/evaluations/{MLTopicId}/{MLCardId}/{FileName}.{FileFormaat}
                    string folderName = "mlearnings/evaluations/" + request.ExamId + "/" + request.CardId;

                    if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_IMAGE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT || request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                    {
                        // Must save to 2 place.
                        // 1. Path: cocadre-{CompanyId}/mlearnings/{MLTopicId}/{MLCardId}/{FileName}.{FileFormaat}
                        // 2. Path: cocadre-elastic-transcoder-input/{environment}/{companyId}/mlearnings/{MLTopicId}/{MLCardId}/{FileName}.{FileFormaat}

                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);

                        bucketName = "cocadre-elastic-transcoder-input";
                        folderName = System.Web.Configuration.WebConfigurationManager.AppSettings["ServerEnvironment"].ToString() + "/" + request.CompanyId.ToLowerInvariant() + "/" + folderName;
                        AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);

                        if(request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoPortrait, Url = fileUrl });
                        }
                        else if(request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoLandscape, Url = fileUrl });
                        }
                        else
                        {
                            // do nothing
                        }
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_PORTRAIT)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfPortrait, Url = fileUrl });

                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_LANDSCAPE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfLandscape, Url = fileUrl });

                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_SLIDESHARE)
                    {
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.SlideShare, Url = request.UploadFile.Url });
                    }
                    else
                    {
                        // do nothing
                    }
                }
                #endregion

                #region Step 2. Upload Image of Options to S3.
                List<MLOption> options = new List<MLOption>();
                // S3 Path: cocadre-{CompanyId}/mlearnings/evaluations/{MLTopicId}/{MLCardId}/{OptionId}
                if (request.CardType == CARD_TYPE_REVERT[(int)CassandraService.Entity.MLCard.MLCardType.SelectOne] || request.CardType == CARD_TYPE_REVERT[(int)CassandraService.Entity.MLCard.MLCardType.MultiChoice])
                {
                    for (int i = 0; i < request.Options.Count; i++)
                    {
                        request.Options[i].OptionId = UUIDGenerator.GenerateUniqueIDForMLOption();
                        MLOption mlOption = new MLOption { Content = request.Options[i].Content, Score = request.Options[i].Score };

                        if (request.Options[i].UploadedContent != null && request.Options[i].UploadedContent.IsNewFile)
                        {
                            string folderName = "mlearnings/evaluations/" + request.ExamId + "/" + request.CardId + "/" + request.Options[i].OptionId;
                            string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.Options[i].UploadedContent);

                            mlOption.HasUploadedContent = true;
                            mlOption.UploadedContent = new List<MLUploadedContent>();
                            mlOption.UploadedContent.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                        }

                        options.Add(mlOption);
                    }
                }
                #endregion

                #region Step 3. Article Pages
                List<MLPageContent> pages = new List<MLPageContent>();
                if (request.CardType == CARD_TYPE_REVERT[(int)CassandraService.Entity.MLCard.MLCardType.ReadAndAnswer])
                {
                    for (int i = 0; i < request.ArticlePages.Count; i++)
                    {
                        pages.Add(new MLPageContent { Title = request.ArticlePages[i].Title, Content = request.ArticlePages[i].Content });
                    }
                }
                #endregion

                request.CardType = CARD_TYPE_REVERT[request.CardType];

                #region Step 4. Call service
                response = asc.CreateMLCard(request.CardId,
                    request.CardType,
                    request.IsOptionRandomized,
                    request.HasPageBreak,
                    request.BackgroundType,
                    request.Note,
                    request.CategoryId,
                    request.ExamId,
                    request.ManagerId,
                    request.CompanyId,
                    request.Content,
                    1,
                    request.MinOptionToSelect,
                    request.MaxOptionToSelect,
                    request.Description,
                    cardUploadFiles,
                    options,
                    pages);

                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("DeleteCard")]
        public MLCardUpdateResponse DeleteCard(DeleteCardRequest request)
        {
            MLCardUpdateResponse response = new MLCardUpdateResponse();
            try
            {
                response = asc.DeleteMLCard(request.TopicId, request.CategoryId, request.CardId, request.ManagerId, request.CompanyId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetCardDetail")]
        public MLCardSelectResponse GetCardDetail(GetCardDetailRequest request)
        {
            MLCardSelectResponse response = new MLCardSelectResponse();
            try
            {
                response = asc.SelectFullDetailMLCard(request.ManagerId, request.CompanyId, request.CardId, request.TopicId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("SearchCards")]
        public MLTopicSelectResponse SearchCards(SearchCardsRequest request)
        {
            MLTopicSelectResponse response = new MLTopicSelectResponse();
            try
            {
                response = asc.SelectFullDetailMLTopic(request.ManagerId, request.CompanyId, request.TopicId, request.CategoryId, request.KeyWord);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("AnalyticCandidate")]
        public MLSelectCandidateResultResponse AnalyticCandidate(AnalyticsRequest request)
        {
            MLSelectCandidateResultResponse response = new MLSelectCandidateResultResponse();
            try
            {
                response = asc.SelectMLCandidateResult(request.ManagerId, request.CompanyId, request.TopicId, request.CategoryId, request.SearchId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("AnalyticQuestion")]
        public MLSelectQuestionResultResponse AnalyticQuestion(AnalyticsRequest request)
        {
            MLSelectQuestionResultResponse response = new MLSelectQuestionResultResponse();
            try
            {
                response = asc.SelectMLQuestionResult(request.ManagerId, request.CompanyId, request.TopicId, request.CategoryId, request.SearchId, request.QuestionToggleType);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("AnalyticPreviewQuestion")]
        public MLSelectCardQuestionResultResponse AnalyticPreviewQuestion(AnalyticsRequest request)
        {
            MLSelectCardQuestionResultResponse response = new MLSelectCardQuestionResultResponse();
            try
            {
                response = asc.SelectMLCardQuestionResult(request.ManagerId, request.CompanyId, request.TopicId, request.CategoryId, request.CardId, request.SearchId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("AnalytictUserAttempt")]
        public MLSelectUserAttemptHistoryResponse AnalytictUserAttempt(AnalyticsRequest request)
        {
            MLSelectUserAttemptHistoryResponse response = new MLSelectUserAttemptHistoryResponse();
            try
            {
                response = asc.SelectMLUserAttemptHistoryResult(request.ManagerId, request.CompanyId, request.AnsweredUserId, request.TopicId, request.CategoryId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("UpdateCard")]
        public MLCardUpdateResponse UpdateCard(CreateUpdateCardRequest request)
        {
            MLCardUpdateResponse response = new MLCardUpdateResponse();
            try
            {
                #region Step 1. Upload Image/Video/PDF to S3.
                String bucketName = "cocadre-" + request.CompanyId.ToLowerInvariant();
                List<MLUploadedContent> cardUploadFiles = new List<MLUploadedContent>();
                if (request.UploadFile != null && request.UploadFile.IsNewFile)
                {
                    // S3 Path: cocadre-{CompanyId}/mlearnings/evaluations/{MLTopicId}/{MLCardId}/{FileName}.{FileFormaat}
                    string folderName = "mlearnings/evaluations/" + request.ExamId + "/" + request.CardId;

                    if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_IMAGE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT || request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                    {
                        // Must save to 2 place.
                        // 1. Path: cocadre-{CompanyId}/mlearnings/evaluations/{MLTopicId}/{MLCardId}/{FileName}.{FileFormaat}
                        // 2. Path: cocadre-elastic-transcoder-input/{environment}/{companyId}/mlearnings/{MLTopicId}/{MLCardId}/{FileName}.{FileFormaat}

                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);

                        bucketName = "cocadre-elastic-transcoder-input";
                        folderName = System.Web.Configuration.WebConfigurationManager.AppSettings["ServerEnvironment"].ToString() + "/" + request.CompanyId.ToLowerInvariant() + "/" + folderName;
                        AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);

                        if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoPortrait, Url = fileUrl });
                        }
                        else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoLandscape, Url = fileUrl });
                        }
                        else
                        {
                            // do nothing
                        }
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_PORTRAIT)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfPortrait, Url = fileUrl });

                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_LANDSCAPE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfLandscape, Url = fileUrl });

                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_SLIDESHARE)
                    {
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.SlideShare, Url = request.UploadFile.Url });
                    }
                    else
                    {
                        // do nothing
                    }
                }
                else if (request.UploadFile != null)
                {
                    cardUploadFiles.Add(new MLUploadedContent { UploadType = request.UploadFile.UploadType, Url = request.UploadFile.Url });
                }
                #endregion

                #region Step 2. Upload Image of Options to S3.
                List<MLOption> options = new List<MLOption>();
                // S3 Path: cocadre-{CompanyId}/mlearnings/evaluations/{MLTopicId}/{MLCardId}/{OptionId}
                if (request.CardType == CARD_TYPE_REVERT[(int)CassandraService.Entity.MLCard.MLCardType.SelectOne] || request.CardType == CARD_TYPE_REVERT[(int)CassandraService.Entity.MLCard.MLCardType.MultiChoice])
                {
                    for (int i = 0; i < request.Options.Count; i++)
                    {
                        MLOption mlOption = new MLOption { Content = request.Options[i].Content, Score = request.Options[i].Score };

                        if (request.Options[i].UploadedContent != null && request.Options[i].UploadedContent.IsNewFile)
                        {
                            if (string.IsNullOrEmpty(request.Options[i].OptionId))
                            {
                                request.Options[i].OptionId = UUIDGenerator.GenerateUniqueIDForMLOption();
                            }
                            string folderName = "mlearnings/evaluations/" + request.ExamId + "/" + request.CardId + "/" + request.Options[i].OptionId;
                            string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.Options[i].UploadedContent);

                            mlOption.HasUploadedContent = true;
                            mlOption.UploadedContent = new List<MLUploadedContent>();
                            mlOption.UploadedContent.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                        }
                        else if (request.Options[i].UploadedContent != null)
                        {
                            mlOption.HasUploadedContent = false;
                            mlOption.UploadedContent = new List<MLUploadedContent>();
                            mlOption.UploadedContent.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = request.Options[i].UploadedContent.Url });
                        }

                        options.Add(mlOption);
                    }
                }
                #endregion

                #region Step 3. Article Pages
                List<MLPageContent> pages = new List<MLPageContent>();
                if (request.CardType == CARD_TYPE_REVERT[(int)CassandraService.Entity.MLCard.MLCardType.ReadAndAnswer])
                {
                    for (int i = 0; i < request.ArticlePages.Count; i++)
                    {
                        pages.Add(new MLPageContent { Title = request.ArticlePages[i].Title, Content = request.ArticlePages[i].Content });
                    }
                }
                #endregion

                request.CardType = CARD_TYPE_REVERT[request.CardType];

                #region Step 4. Call API
                response = asc.UpdateMLCard(request.CardId,
                    request.CardType,
                    request.IsOptionRandomized,
                    request.HasPageBreak,
                    request.BackgroundType,
                    request.Note,
                    request.CategoryId,
                    request.ExamId,
                    request.ManagerId,
                    request.CompanyId,
                    request.Content,
                    1,
                    request.MinOptionToSelect,
                    request.MaxOptionToSelect,
                    request.Description,
                    cardUploadFiles,
                    options,
                    pages
                    );
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        #region S3 Helper functions
        private string AddToS3(string companyId, string managerUserId, string bucketName, string folderName, UploadFileContent fileContent)
        {
            //String bucketName = "cocadre-" + companyId.ToLowerInvariant();
            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(companyId, managerUserId);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String fileBase64String = String.Empty, fileFormat = String.Empty, fileUrl = String.Empty;
            int fileWidth = 0, fileHeight = 0;

            using (s3Client)
            {
                String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                if (fileContent.UploadType == UploadFileContent.FILE_TYPE_IMAGE)
                {
                    fileWidth = Convert.ToInt16(fileContent.Width);
                    fileHeight = Convert.ToInt16(fileContent.Height);
                    fileBase64String = fileContent.Base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                    fileFormat = fileContent.Extension.Replace("image/", "");

                    #region Upload original image
                    // Upload original image
                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    byte[] imgBytes = Convert.FromBase64String(fileBase64String);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                    System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                    String fileName = "1_" + PROCESS_TIMESTAMP + "_original" + "." + fileFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(fileWidth));
                    metadatas.Add("Height", Convert.ToString(fileHeight));
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    #endregion Upload original image

                    #region Upload large (if original image is 1920 * 1920)
                    // Upload large (if original image is 1920 * 1920)
                    System.Drawing.Image newImage;
                    if (fileWidth > 1920 || fileHeight > 1920)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                        fileName = "1_" + PROCESS_TIMESTAMP + "_large" + "." + fileFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = "1_" + PROCESS_TIMESTAMP + "_large" + "." + fileFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion Upload large (if original image is 1920 * 1920)

                    #region Upload medium (if original image is 1080 * 1080)
                    // Upload medium (if original image is 1080 * 1080)
                    if (fileWidth > 1080 || fileHeight > 1080)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                        fileName = "1_" + PROCESS_TIMESTAMP + "_medium" + "." + fileFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = "1_" + PROCESS_TIMESTAMP + "_medium" + "." + fileFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion Upload medium (if original image is 1080 * 1080)

                    #region Upload small (if original image is 540 * 540)
                    // Upload small (if original image is 540 * 540)
                    if (fileWidth > 540 || fileHeight > 540)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                        fileName = "1_" + PROCESS_TIMESTAMP + "_small" + "." + fileFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = "1_" + PROCESS_TIMESTAMP + "_small" + "." + fileFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion Upload small (if original image is 540 * 540)

                    fileUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/1_" + PROCESS_TIMESTAMP + "_original." + fileFormat.ToLower();
                }
                else if (fileContent.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT || fileContent.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                {
                    fileBase64String = fileContent.Base64.Remove(0, fileContent.Base64.IndexOf(",") + 1);
                    fileFormat = fileContent.Extension.Replace("video/", "");

                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    byte[] contentBytes = Convert.FromBase64String(fileBase64String);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                    String fileName = "1_" + PROCESS_TIMESTAMP + "_original" + "." + fileFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);

                    fileUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/1_" + PROCESS_TIMESTAMP + "_original." + fileFormat.ToLower();
                }
                else if (fileContent.UploadType == UploadFileContent.FILE_TYPE_PDF_PORTRAIT || fileContent.UploadType == UploadFileContent.FILE_TYPE_PDF_LANDSCAPE)
                {
                    fileBase64String = fileContent.Base64.Remove(0, fileContent.Base64.IndexOf(",") + 1);
                    fileFormat = fileContent.Extension.Replace("application/", "");
                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    byte[] contentBytes = Convert.FromBase64String(fileBase64String);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                    String fileName = "1_" + PROCESS_TIMESTAMP + "_original" + "." + fileFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);

                    fileUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/1_" + PROCESS_TIMESTAMP + "_original." + fileFormat.ToLower();
                }
                else
                {
                    // do nothing
                }

                return fileUrl;
            }

        }

        private string AddVideoToS3(string companyId, string managerUserId, string folderName, UploadFileContent fileContent)
        {
            String bucketName = "cocadre-" + companyId;
            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(companyId, managerUserId);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            using (s3Client)
            {
                string fileBase64String = fileContent.Base64.Remove(0, fileContent.Base64.IndexOf(",") + 1);
                String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] contentBytes = Convert.FromBase64String(fileBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                String fileName = PROCESS_TIMESTAMP + "_original" + "." + fileContent.Extension.ToLower();
                S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);

                string s3url = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;

                return s3url;


            }
        }
        #endregion S3 Helper functions
    }
}
