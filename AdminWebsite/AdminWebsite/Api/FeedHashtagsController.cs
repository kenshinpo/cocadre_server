﻿using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class BasicHashTagRequest
    {
        public string CompanyId { get; set; }
        public string AdminUserId { get; set; }
        public string Hashtag { get; set; }
    }

    public class RenameHashtagRequest : BasicHashTagRequest
    {
        public string OldHashtag { get; set; }
        public string NewHashtag { get; set; }
    }

    public class ReorderingHashtagRequest : BasicHashTagRequest
    {
        public List<string> Hashtags { get; set; }
    }


    [RoutePrefix("FeedHashtags")]
    public class FeedHashtagsController : ApiController
    {
        private AdminService service = new AdminService();

        [AcceptVerbs("POST")]
        [Route("GetHashtagList")]
        public FeedSelectAllHashTagResponse GetHashtagList(BasicHashTagRequest request)
        {
            return service.SelectAllFeedRecommendedHashTags(request.AdminUserId, request.CompanyId);
        }

        [AcceptVerbs("POST")]
        [Route("AddHashtag")]
        public FeedCreateHashTagResponse AddHashtag(BasicHashTagRequest request)
        {
            return service.CreateFeedRecommendedHashTag(request.AdminUserId, request.CompanyId, request.Hashtag);
        }

        [AcceptVerbs("POST")]
        [Route("DelHashtag")]
        public FeedUpdateHashTagResponse DelHashtag(BasicHashTagRequest request)
        {
            return service.DeleteFeedRecommendedHashTag(request.AdminUserId, request.CompanyId, request.Hashtag);
        }

        [AcceptVerbs("POST")]
        [Route("RenameHashtag")]
        public FeedUpdateHashTagResponse RenameHashtag(RenameHashtagRequest request)
        {
            return service.UpdateFeedRecommendedHashTag(request.AdminUserId, request.CompanyId, request.OldHashtag, request.NewHashtag);
        }

        [AcceptVerbs("POST")]
        [Route("ReorderingHashtag")]
        public FeedUpdateHashTagResponse ReorderingHashtag(ReorderingHashtagRequest request)
        {
            return service.UpdateFeedRecommendedHashTagOrdering(request.AdminUserId, request.CompanyId, request.Hashtags);
        }
    }
}
