﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite
{
    public class RSCardDeleteController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "card1", "card2" };
        }

        // GET api/<controller>/5
        public string Get([FromBody]RSCardAjaxRequest req)
        {
            return "value";
        }

        // POST api/<controller>
        public CassandraService.ServiceResponses.RSCardUpdateResponse Post([FromBody]RSCardAjaxRequest req)
        {
            CassandraService.ServiceResponses.RSCardUpdateResponse response = new CassandraService.ServiceResponses.RSCardUpdateResponse();
            response.Success = false;
            
            try
            {
                return asc.DeleteCard(
                    req.TopicId,
                    req.CategoryId,
                    req.CardId,
                    req.AdminUserId,
                    req.CompanyId);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -1;
                response.ErrorMessage = "Cannot update card." + ex.Message;
            }

            return response;
        }

        // PUT api/<controller>/5
        public CassandraService.ServiceResponses.RSCardUpdateResponse Put(int id, [FromBody]RSCardAjaxRequest req)
        {
            //string cardId,
            //string content,
            //bool hasImage,
            //List< RSImage > images,
            //string note,
            //bool hasPageBreak,
            //string categoryId,
            //string topicId,
            //string adminUserId,
            //string companyId,
            //List< RSOption > options)

            string x = string.Empty;

            //CassandraService.ServiceResponses.RSCardUpdateResponse response = asc.UpdateCard(
            //    req.AdminUserId,
            //    req.CompanyId,
            //    req.CardId,
            //    req.TopicId);

            return null;
            //return response;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}