﻿using AdminWebsite.App_Code.ServiceRequests;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class Live360Request : BasicRequest
    {
        /// <summary>
        /// 暫時 for Likert7。未來統一使用 AppraisalId
        /// </summary>
        public string Likert7Id { get; set; }
        /*************************************/
        public string AppraisalId { get; set; }
        public int AppraisalType { get; set; }
        public string CategoryId { get; set; }
        public string CardId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsPrioritized { get; set; }
        public int Anonymity { get; set; }
        public bool IsCustomizeQuestion { get; set; }
        public List<string> TargetedDepartmentIds { get; set; }
        public List<string> TargetedUserIds { get; set; }
        public int PublishMethod { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CardContent { get; set; }
        public List<AppraisalCardOption> Options { get; set; }
        public int Status { get; set; }
        public int TeamType { get; set; }
        public int QuestionLimit { get; set; }

    }

    [RoutePrefix("Api/Live360")]
    public class Live360Controller : ApiController
    {
        private AdminService asc = new AdminService();

        [AcceptVerbs("POST")]
        [Route("GetDepartments")]
        public DepartmentListResponse GetDepartments(Live360Request request)
        {
            DepartmentListResponse response = new DepartmentListResponse();
            try
            {
                response = asc.GetAllDepartment(request.ManagerId, request.CompanyId);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        /// <summary>
        /// 暫時 for Likert7。未來統一使用 CreateAppraisal
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [Route("CreateLikert7")]
        public AppraisalCreateTemplateResponse CreateLikert7(Live360Request request)
        {
            AppraisalCreateTemplateResponse response = new AppraisalCreateTemplateResponse();
            try
            {
                response = asc.CreateAppraisalTemplate(request.ManagerId,
                                                        request.CompanyId,
                                                        (int)Appraisal.AppraisalTypeEnum.Likert7,
                                                        request.Title,
                                                        request.Description,
                                                        request.IsPrioritized,
                                                        request.Anonymity,
                                                        request.IsCustomizeQuestion,
                                                        1,
                                                        request.QuestionLimit,
                                                        request.TargetedDepartmentIds,
                                                        request.TargetedUserIds,
                                                        request.PublishMethod,
                                                        request.StartDate,
                                                        request.EndDate);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("CreateAppraisal")]
        public AppraisalCreateTemplateResponse CreateAppraisal(Live360Request request)
        {
            AppraisalCreateTemplateResponse response = new AppraisalCreateTemplateResponse();
            try
            {
                response = asc.CreateAppraisalTemplate(request.ManagerId,
                                                        request.CompanyId,
                                                        request.AppraisalType,
                                                        request.Title,
                                                        request.Description,
                                                        request.IsPrioritized,
                                                        request.Anonymity,
                                                        request.IsCustomizeQuestion,
                                                        request.TeamType,
                                                        request.QuestionLimit,
                                                        request.TargetedDepartmentIds,
                                                        request.TargetedUserIds,
                                                        request.PublishMethod,
                                                        request.StartDate,
                                                        request.EndDate);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        /// <summary>
        /// 暫時 for Likert7。未來統一使用 GetAppraisalDetail
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [Route("GetLikert7Detail")]
        public AppraisalSelectTemplateResponse GetLikert7Detail(Live360Request request)
        {
            AppraisalSelectTemplateResponse response = new AppraisalSelectTemplateResponse();
            try
            {
                response = asc.SelectAppraisalFullTemplateByAdmin(request.ManagerId,
                                                        request.CompanyId,
                                                        request.Likert7Id);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetAppraisalDetail")]
        public AppraisalSelectTemplateResponse GetAppraisalDetail(Live360Request request)
        {
            AppraisalSelectTemplateResponse response = new AppraisalSelectTemplateResponse();
            try
            {
                response = asc.SelectAppraisalFullTemplateByAdmin(request.ManagerId,
                                                        request.CompanyId,
                                                        request.AppraisalId);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        /// <summary>
        /// 暫時 for Likert7。未來統一使用 UpdateAppraisal
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [Route("UpdateLikert7")]
        public AppraisalUpdateResponse UpdateLikert7(Live360Request request)
        {
            AppraisalUpdateResponse response = new AppraisalUpdateResponse();
            response.Success = true;
            try
            {  
                response = asc.UpdateAppraisalTemplate(request.ManagerId,
                                                        request.CompanyId,
                                                        request.Likert7Id,
                                                        request.Title,
                                                        request.Description,
                                                        request.IsPrioritized,
                                                        request.Anonymity,
                                                        request.IsCustomizeQuestion,
                                                        request.TeamType,
                                                        request.QuestionLimit,
                                                        request.TargetedDepartmentIds,
                                                        request.TargetedUserIds,
                                                        request.PublishMethod,
                                                        request.Status,
                                                        request.StartDate,
                                                        request.EndDate);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("UpdateAppraisal")]
        public AppraisalUpdateResponse UpdateAppraisal(Live360Request request)
        {
            AppraisalUpdateResponse response = new AppraisalUpdateResponse();
            response.Success = true;
            try
            {
                response = asc.UpdateAppraisalTemplate(request.ManagerId,
                                                        request.CompanyId,
                                                        request.AppraisalId,
                                                        request.Title,
                                                        request.Description,
                                                        request.IsPrioritized,
                                                        request.Anonymity,
                                                        request.IsCustomizeQuestion,
                                                        request.TeamType,
                                                        request.QuestionLimit,
                                                        request.TargetedDepartmentIds,
                                                        request.TargetedUserIds,
                                                        request.PublishMethod,
                                                        request.Status,
                                                        request.StartDate,
                                                        request.EndDate);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        /// <summary>
        /// 暫時 for Likert7。未來統一使用 CreateAppraisalCategory
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [Route("CreateLikert7Category")]
        public AppraisalCategoryCreateResponse CreateLikert7Category(Live360Request request)
        {
            AppraisalCategoryCreateResponse response = new AppraisalCategoryCreateResponse();
            try
            {
                response = asc.CreateAppraisalTemplateCategory(request.ManagerId,
                                                        request.CompanyId,
                                                        request.Likert7Id,
                                                        request.Title
                                                       );
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("CreateAppraisalCategory")]
        public AppraisalCategoryCreateResponse CreateAppraisalCategory(Live360Request request)
        {
            AppraisalCategoryCreateResponse response = new AppraisalCategoryCreateResponse();
            try
            {
                response = asc.CreateAppraisalTemplateCategory(request.ManagerId,
                                                        request.CompanyId,
                                                        request.AppraisalId,
                                                        request.Title
                                                       );
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        /// <summary>
        /// 暫時 for Likert7。未來統一使用 UpdateAppraisalCategory
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [Route("UpdateLikert7Category")]
        public AppraisalUpdateCategoryResponse UpdateLikert7Category(Live360Request request)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = true;
            try
            {
                response = asc.UpdateTemplateAppraisalCategoryTitle(request.CompanyId,
                    request.ManagerId,
                    request.Likert7Id,
                    request.CategoryId,
                    request.Title);

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("UpdateAppraisalCategory")]
        public AppraisalUpdateCategoryResponse UpdateAppraisalCategory(Live360Request request)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = true;
            try
            {
                response = asc.UpdateTemplateAppraisalCategoryTitle(request.CompanyId,
                    request.ManagerId,
                    request.AppraisalId,
                    request.CategoryId,
                    request.Title);

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        /// <summary>
        /// 暫時 for Likert7。未來統一使用 DeleteAppraisalCategory
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AcceptVerbs("POST")]
        [Route("DeleteLikert7Category")]
        public AppraisalUpdateCategoryResponse DeleteLikert7Category(Live360Request request)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = true;
            try
            {
                response = asc.RemoveAppraisalCategory(request.CompanyId,
                                                        request.ManagerId,
                                                        request.Likert7Id,
                                                        request.CategoryId
                                                       );
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("DeleteAppraisalCategory")]
        public AppraisalUpdateCategoryResponse DeleteAppraisalCategory(Live360Request request)
        {
            AppraisalUpdateCategoryResponse response = new AppraisalUpdateCategoryResponse();
            response.Success = true;
            try
            {
                response = asc.RemoveAppraisalCategory(request.CompanyId,
                                                        request.ManagerId,
                                                        request.AppraisalId,
                                                        request.CategoryId
                                                       );
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("CreateLikert7Card")]
        public AppraisalCardCreateTemplateResponse CreateLikert7Card(Live360Request request)
        {
            AppraisalCardCreateTemplateResponse response = new AppraisalCardCreateTemplateResponse();
            try
            {
                response = asc.CreateAppraisalTemplateCard(request.ManagerId,
                                                        request.CompanyId,
                                                        request.Likert7Id,
                                                        request.CategoryId,
                                                        request.CardContent,
                                                        (int)AppraisalCard.AppraisalCardTypeEnum.NumberRange,
                                                        request.Options
                                                       );

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("UpdateLikert7Card")]
        public AppraisalCardCreateTemplateResponse UpdateLikert7Card(Live360Request request)
        {
            AppraisalCardCreateTemplateResponse response = new AppraisalCardCreateTemplateResponse();
            try
            {
                response = asc.UpdateTemplateAppraisalCard(request.CardId,
                                                        request.ManagerId,
                                                        request.CompanyId,
                                                        request.Likert7Id,
                                                        request.CategoryId,
                                                        request.CardContent,
                                                        (int)AppraisalCard.AppraisalCardTypeEnum.NumberRange,
                                                        request.Options
                                                       );

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("DeleteLikert7Card")]
        public AppraisalUpdateCardResponse DeleteLikert7Card(Live360Request request)
        {
            AppraisalUpdateCardResponse response = new AppraisalUpdateCardResponse();
            response.Success = true;
            try
            {
                response = asc.RemoveTemplateAppraisalCard(request.ManagerId,
                                                        request.CompanyId,
                                                        request.Likert7Id,
                                                        request.CategoryId,
                                                        request.CardId);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetGroupTeamType")]
        public AppraisalSelectGroupTeamTypeResponse GetGroupTeamType(Live360Request request)
        {
            AppraisalSelectGroupTeamTypeResponse response = new AppraisalSelectGroupTeamTypeResponse();
            response.Success = true;
            try
            {
                response = asc.SelectGroupTeamTypes(request.ManagerId, request.CompanyId);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }
    }

}
