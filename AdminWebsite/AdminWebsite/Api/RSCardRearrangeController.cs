﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite
{
    public class RSCardRearrangeController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "card1", "card2" };
        }

        // GET api/<controller>/5
        public string Get([FromBody]RSCardAjaxRequest req)
        {
            return "value";
        }

        // POST api/<controller>
        public CassandraService.ServiceResponses.RSCardReorderResponse Post([FromBody]RSCardAjaxRequest req)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            CassandraService.ServiceResponses.RSCardReorderResponse response = new CassandraService.ServiceResponses.RSCardReorderResponse();

            if (req.UserData.Length > 0)
            {
                try
                {
                    response = asc.ReorderCard(
                        req.CardId, 
                        req.TopicId,
                        req.CategoryId,
                        req.AdminUserId,
                        req.CompanyId,
                        int.Parse(req.UserData)
                    );

                    return response;
                }
                catch (Exception ex)
                {
                    // do nothing 
                    response.Success = false;
                    response.ErrorCode = -1;
                    response.ErrorMessage = "Cannot update card." + ex.Message;
                }
            }

            return response;
            //CassandraService.ServiceResponses.RSCardUpdateResponse response = asc.UpdateCard();

            //CassandraService.ServiceResponses.RSCardSelectResponse response = asc.SelectFullDetailCard(
            //    req.AdminUserId,
            //    req.CompanyId,
            //    req.CardId,
            //    req.TopicId);

            //return response;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]RSCardAjaxRequest req)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}