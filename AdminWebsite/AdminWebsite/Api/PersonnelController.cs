﻿using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class PersonnelAjaxRequest
    {
        public string adminUserId { get; set; }
        public string companyId { get; set; }
        public string title { get; set; }
        public bool isFilterApplied { get; set; }
        public string textColor { get; set; }
        public string backgroundUrl { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public bool isPrioritized { get; set; }
        public List<string> targetedDepartmentIds { get; set; }
        public List<string> targetedUserIds { get; set; }
    }

    public class AutoCompleteEntry
    {
        public string label { get; set; }
        public string value { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }

        public AutoCompleteEntry()
        {
        }

        public AutoCompleteEntry(string label, string value, string first_name, string last_name, string email)
        {
            this.label = label;
            this.value = value;
            this.first_name = first_name;
            this.last_name = last_name;
            this.email = email;
        }
    }

    public class PersonnelController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        public string[] Get(string term)
        {
            string[] result = new string[] {
                "asd",
                "zxc"
            };

            return result;
        }

        public static Dictionary<string, List<AutoCompleteEntry>> termResults = new Dictionary<string, List<AutoCompleteEntry>>();

        // GET api/<controller>
        public List<AutoCompleteEntry> Get(string userId, string companyId, string term)
        {
            #region comment out
            //DepartmentListResponse response = asc.GetAllDepartment(userId, companyId);
            //if (response.Success)
            //{
            //    return response.Departments;
            //}

            //return new string[] { "card1", "card2" };
            //return [];
            // return asc.GetAllDepartment(userId, companyId);
            #endregion comment out

            List<AutoCompleteEntry> autoCompleteEntryList = new List<AutoCompleteEntry>();

            //if (termResults.ContainsKey(term))
            //{
            //    return termResults[term];
            //}

            UserListResponse response = asc.SearchUser(userId, companyId, term);
            if (response.Success)
            {
                autoCompleteEntryList = response.Users.Select(r => new AutoCompleteEntry
                {
                    label = string.Format("{0} {1} ({2})", r.FirstName, r.LastName, r.Email),
                    value = r.UserId,
                    first_name = r.FirstName,
                    last_name = r.LastName,
                    email = r.Email
                }).ToList();
            }

            //termResults[term] = autoCompleteEntryList;

            //return termResults[term];
            return autoCompleteEntryList;
        }

        // GET api/<controller>/5
        public string Get([FromBody]PulseBannerCardAjaxRequest req)
        {
            return "value";
        }

        // POST api/<controller>
        public CassandraService.ServiceResponses.RSCardSelectResponse Post([FromBody]PulseBannerCardAjaxRequest req)
        {

            return null;
        }

        // PUT api/<controller>/5
        //public CassandraService.ServiceResponses.RSCardUpdateResponse Put(int id, [FromBody]PulseAnnouncementCardAjaxRequest req)
        public CassandraService.ServiceResponses.PulseCreateResponse Put([FromBody]PulseBannerCardAjaxRequest req)
        {
            //CassandraService.ServiceResponses.PulseCreateResponse response = asc.CreateBanner(req.adminUserId,
            //    req.companyId,
            //    req.title,
            //    req.isFilterApplied,
            //    req.textColor,
            //    req.backgroundUrl,
            //    req.description,
            //    req.status,
            //    req.startDate,
            //    req.endDate,
            //    req.isPrioritized,
            //    req.targetedDepartmentIds,
            //    req.targetedUserIds);

            //return response;
            return null;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {

        }
    }
}