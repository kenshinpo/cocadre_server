﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite
{
    public class RSTopicAjaxRequest
    {
        //'TopicId': "RSTe80be03be86642028ef09d86094bcbc2",
        //'CategoryId': "RSCce652b65592444c28d9a6ef6bd78a926",
        //'AdminUserId': "U811e9a9236c140ad98a211a428b6dc84",
        //'CompanyId': "C8b28091502514318bdcf48bec7c69129"
        public string TopicId { get; set; }
        public string CategoryId { get; set; }
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }
        public string CardName { get; set; }
    }

    public class RSTopicController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "topic1", "topic2" };
        }

        // GET api/<controller>/5
        public string Get(string id)
        {
            return "value" + id;
        }

        // POST api/<controller>
        //public CassandraService.Entity.RSTopic Post([FromBody]RSTopicAjaxRequest req)
        public CassandraService.ServiceResponses.RSTopicSelectResponse Post([FromBody]RSTopicAjaxRequest req)
        {
            return asc.SelectFullDetailRSTopic(
                req.AdminUserId,
                req.CompanyId,
                req.TopicId,
                req.CategoryId,
                req.CardName);
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}