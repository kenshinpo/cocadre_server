﻿using AdminWebsite.App_Code.ServiceRequests;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class MatchupRequest
    {
        public string CompanyId { get; set; }
        public string AdminUserId { get; set; }
        public string TopicId { get; set; }
        public string QuestionId { get; set; }

        public int Metric { get; set; }
        public int Limit { get; set; }

        public string Datestamp { get; set; }
    }

    public class AnalyticRequest : BasicRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    [RoutePrefix("Api/Analytics")]
    public class AnalyticsController : ApiController
    {
        private AdminService service = new AdminService();

        [AcceptVerbs("GET", "POST")]
        [Route("GetDau")]
        public AnalyticsSelectDetailDauResponse GetDau(AnalyticRequest request)
        {
            return service.SelectDau(request.ManagerId, request.CompanyId, request.StartDate, request.EndDate);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("GetUserActivityPerDay")]
        public AnalyticActivityResponse GetUserActivityPerDay(AnalyticRequest request)
        {
            return service.GetActivity(request.ManagerId, request.CompanyId, request.StartDate.Date, request.StartDate.Date.AddDays(1), false);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectTopicAttempts")]
        public AnalyticSelectTopicAttemptResponse SelectTopicAttempts(MatchupRequest request)
        {
            return service.SelectTopicAttempts(request.AdminUserId, request.CompanyId, request.Metric, request.Limit);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectMatchUpOverall")]
        public AnalyticSelectMatchUpOverallResponse SelectMatchUpOverall(MatchupRequest request)
        {
            return service.SelectMatchUpOverall(request.AdminUserId, request.CompanyId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectDailyTopicActivities")]
        public AnalyticSelectTopicAttemptResponse SelectDailyTopicActivities(MatchupRequest request)
        {
            return service.SelectDailyTopicActivities(request.AdminUserId, request.CompanyId, request.Datestamp);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectQuestionAttempts")]
        public AnalyticSelectQuestionAttemptResponse SelectQuestionAttempts(MatchupRequest request)
        {
            return service.SelectQuestionAttempts(request.AdminUserId, request.CompanyId, request.TopicId, request.Metric, request.Limit);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectOptionAttempts")]
        public AnalyticSelectOptionAttemptResponse SelectOptionAttempts(MatchupRequest request)
        {
            return service.SelectOptionAttempts(request.AdminUserId, request.CompanyId, request.TopicId, request.QuestionId);
        }

        [AcceptVerbs("GET", "POST")]
        [Route("SelectTopicOverall")]
        public AnalyticSelectTopicOverallResponse SelectTopicOverall(MatchupRequest request)
        {
            return service.SelectTopicOverall(request.AdminUserId, request.CompanyId, request.TopicId);
        }
    }
}
