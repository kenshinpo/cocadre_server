﻿using AdminWebsite.App_Code.ServiceRequests;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http;
using static CassandraService.Entity.MLCard;

namespace AdminWebsite.Api
{
    public class CreateUpdateEducationCardRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string EducationId { get; set; }
        public string CardId { get; set; }
        public int CardType { get; set; }
        public List<MLPageContent> ArticlePages { get; set; }
        public string Content { get; set; }
        public string Description { get; set; }
        public UploadFileContent UploadFile { get; set; }
        public bool IsUsingSlideShare { get; set; }
        public string SlideShareUrl { get; set; }
        public List<Option> Options { get; set; }
        public bool IsOptionRandomized { get; set; }
        public int MinOptionToSelect { get; set; }
        public int MaxOptionToSelect { get; set; }
        public int BackgroundType { get; set; }
        public string Note { get; set; }
        public bool HasPageBreak { get; set; }
    }

    public class GetEducationCardDetailRequest : BasicRequest
    {
        public string CardId { get; set; }
        public string EducationId { get; set; }
    }

    public class DeleteEducationCardRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string EducationId { get; set; }
        public string CardId { get; set; }
    }

    public class SearchEducationCardsRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string EducationId { get; set; }
        public string KeyWord { get; set; }
    }

    public class EducationAnalyticsRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string EducationId { get; set; }
        public string DepartmentId { get; set; }
        public string AnsweredUserId { get; set; }
    }

    public class EducationRequest : BasicRequest
    {
        public string CategoryId { get; set; }
        public string ContainsName { get; set; }
    }

    [Serializable]
    [DataContract]
    public class GetAutoCompleteEducationListResponse
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ErrorCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ErrorMessage { get; set; }

        [DataMember]
        public List<AutoCompleteEducationEntry> Educations { get; set; }
    }

    [Serializable]
    [DataContract]
    public class AutoCompleteEducationEntry
    {
        [DataMember]
        public string label { get; set; }
        [DataMember]
        public string value { get; set; }
        [DataMember]
        public string education_id { get; set; }
        [DataMember]
        public string education_title { get; set; }
    }

    [RoutePrefix("Api/MLEducation")]
    public class MLEducationController : ApiController
    {
        private AdminService asc = new AdminService();

        [AcceptVerbs("POST")]
        [Route("CreateCard")]
        public MLEducationCardCreateResponse CreateCard(CreateUpdateEducationCardRequest request)
        {
            MLEducationCardCreateResponse response = new MLEducationCardCreateResponse();

            try
            {
                #region Step 1. Upload Image/Video/PDF to S3.
                String bucketName = "cocadre-" + request.CompanyId.ToLowerInvariant();
                request.CardId = UUIDGenerator.GenerateUniqueIDForMLCard();
                List<MLUploadedContent> cardUploadFiles = new List<MLUploadedContent>();
                if (request.UploadFile != null && request.UploadFile.IsNewFile)
                {
                    // S3 Path: cocadre-{CompanyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{FileName}.{FileFormaat}
                    string folderName = "mlearnings/educations/" + request.EducationId + "/" + request.CardId;

                    if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_IMAGE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT || request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                    {
                        // Must save to 2 place.
                        // 1. Path: cocadre-{CompanyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{FileName}.{FileFormaat}
                        // 2. Path: cocadre-elastic-transcoder-input/{environment}/{companyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{FileName}.{FileFormaat}

                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);

                        bucketName = "cocadre-elastic-transcoder-input";
                        folderName = System.Web.Configuration.WebConfigurationManager.AppSettings["ServerEnvironment"].ToString() + "/" + request.CompanyId.ToLowerInvariant() + "/" + folderName;
                        AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);

                        if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoPortrait, Url = fileUrl });
                        }
                        else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoLandscape, Url = fileUrl });
                        }
                        else
                        {
                            // do nothing
                        }
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_PORTRAIT)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfPortrait, Url = fileUrl });
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_LANDSCAPE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfLandscape, Url = fileUrl });
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_SLIDESHARE)
                    {
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.SlideShare, Url = request.UploadFile.Url });
                    }
                    else
                    {
                        // do nothing
                    }
                }
                #endregion

                #region Step 2. Upload Image of Options to S3.
                List<MLEducationOption> options = new List<MLEducationOption>();
                // S3 Path: cocadre-{CompanyId}/mlearnings/educations/{MLTopicId}/{MLCardId}/{OptionId}
                if (request.CardType == (int)CassandraService.Entity.MLCard.MLCardType.SelectOne || request.CardType == (int)CassandraService.Entity.MLCard.MLCardType.MultiChoice)
                {
                    for (int i = 0; i < request.Options.Count; i++)
                    {
                        request.Options[i].OptionId = UUIDGenerator.GenerateUniqueIDForMLOption();
                        // MLEducationOption mlOption = new MLEducationOption { Content = request.Options[i].Content, Score = request.Options[i].Score };

                        MLEducationOption mlOption = new MLEducationOption { Content = request.Options[i].Content, Score = ((i == 0) ? 1 : 0) };

                        if (request.Options[i].UploadedContent != null && request.Options[i].UploadedContent.IsNewFile)
                        {
                            string folderName = "mlearnings/educations/" + request.EducationId + "/" + request.CardId + "/" + request.Options[i].OptionId;
                            string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.Options[i].UploadedContent);

                            mlOption.HasUploadedContent = true;
                            mlOption.UploadedContent = new List<MLUploadedContent>();
                            mlOption.UploadedContent.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                        }

                        options.Add(mlOption);
                    }
                }
                #endregion

                #region Step 3. Article Pages
                List<MLPageContent> pages = new List<MLPageContent>();
                if (request.CardType == (int)CassandraService.Entity.MLCard.MLCardType.ReadAndAnswer)
                {
                    for (int i = 0; i < request.ArticlePages.Count; i++)
                    {
                        pages.Add(new MLPageContent { Title = request.ArticlePages[i].Title, Content = request.ArticlePages[i].Content });
                    }
                }
                #endregion

                #region Step 4. Call service
                response = asc.CreateMLEducationCard(request.CardId,
                    request.CardType,
                    request.IsOptionRandomized,
                    request.BackgroundType,
                    request.CategoryId,
                    request.EducationId,
                    request.ManagerId,
                    request.CompanyId,
                    request.Content,
                    request.MinOptionToSelect,
                    request.MaxOptionToSelect,
                    request.Description,
                    cardUploadFiles,
                    options,
                    pages);

                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("DeleteCard")]
        public MLEducationCardUpdateResponse DeleteCard(DeleteEducationCardRequest request)
        {
            MLEducationCardUpdateResponse response = new MLEducationCardUpdateResponse();
            try
            {
                response = asc.DeleteMLEducationCard(request.EducationId, request.CategoryId, request.CardId, request.ManagerId, request.CompanyId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetCardDetail")]
        public MLEducationCardDetailResponse GetCardDetail(GetEducationCardDetailRequest request)
        {
            MLEducationCardDetailResponse response = new MLEducationCardDetailResponse();
            try
            {
                response = asc.SelectFullDetailMLEducationCard(request.ManagerId, request.CompanyId, request.CardId, request.EducationId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("SearchCards")]
        public MLEducationDetailResponse SearchCards(SearchEducationCardsRequest request)
        {
            MLEducationDetailResponse response = new MLEducationDetailResponse();
            try
            {
                response = asc.SelectFullDetailMLEducation(request.CompanyId, request.ManagerId, request.CategoryId, request.EducationId, request.KeyWord);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("AnalyticAttendance")]
        public MLEducationAnalticResponse AnalyticAttendance(EducationAnalyticsRequest request)
        {
            MLEducationAnalticResponse response = new MLEducationAnalticResponse();
            try
            {
                response = asc.SelectEducationAnalyticAttendance(request.CompanyId, request.ManagerId, request.CategoryId, request.EducationId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("AnalyticUsers")]
        public MLEducationAnalticResponse AnalyticUsers(EducationAnalyticsRequest request)
        {
            MLEducationAnalticResponse response = new MLEducationAnalticResponse();
            try
            {
                response = asc.SelectEducationAnalyticUsers(request.CompanyId, request.ManagerId, request.EducationId, request.DepartmentId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("AnalyticUser")]
        public MLEducationAnalticResponse AnalyticUser(EducationAnalyticsRequest request)
        {
            MLEducationAnalticResponse response = new MLEducationAnalticResponse();
            try
            {
                response = asc.SelectEducationAnalyticUser(request.CompanyId, request.ManagerId, request.CategoryId, request.EducationId, request.AnsweredUserId);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("UpdateCard")]
        public MLEducationCardUpdateResponse UpdateCard(CreateUpdateEducationCardRequest request)
        {
            MLEducationCardUpdateResponse response = new MLEducationCardUpdateResponse();
            try
            {
                #region Step 1. Upload Image/Video/PDF to S3.
                String bucketName = "cocadre-" + request.CompanyId.ToLowerInvariant();
                List<MLUploadedContent> cardUploadFiles = new List<MLUploadedContent>();
                if (request.UploadFile != null && request.UploadFile.IsNewFile)
                {
                    // S3 Path: cocadre-{CompanyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{FileName}.{FileFormaat}
                    string folderName = "mlearnings/educations/" + request.EducationId + "/" + request.CardId;

                    if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_IMAGE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT || request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                    {
                        // Must save to 2 place.
                        // 1. Path: cocadre-{CompanyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{FileName}.{FileFormaat}
                        // 2. Path: cocadre-elastic-transcoder-input/{environment}/{companyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{FileName}.{FileFormaat}

                        string processTimestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile, processTimestamp);

                        bucketName = "cocadre-elastic-transcoder-input";
                        folderName = System.Web.Configuration.WebConfigurationManager.AppSettings["ServerEnvironment"].ToString() + "/" + request.CompanyId.ToLowerInvariant() + "/" + folderName;
                        AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile, processTimestamp);

                        if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoPortrait, Url = fileUrl });
                        }
                        else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                        {
                            cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.VideoLandscape, Url = fileUrl });
                        }
                        else
                        {
                            // do nothing
                        }
                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_PORTRAIT)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfPortrait, Url = fileUrl });

                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_PDF_LANDSCAPE)
                    {
                        string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.UploadFile);
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.PdfLandscape, Url = fileUrl });

                    }
                    else if (request.UploadFile.UploadType == UploadFileContent.FILE_TYPE_SLIDESHARE)
                    {
                        cardUploadFiles.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.SlideShare, Url = request.UploadFile.Url });
                    }
                    else
                    {
                        // do nothing
                    }
                }
                else if (request.UploadFile != null)
                {
                    cardUploadFiles.Add(new MLUploadedContent { UploadType = request.UploadFile.UploadType, Url = request.UploadFile.Url });
                }
                #endregion

                #region Step 2. Upload Image of Options to S3.
                List<MLEducationOption> options = new List<MLEducationOption>();
                // S3 Path: cocadre-{CompanyId}/mlearnings/educations/{MLEducationId}/{MLCardId}/{OptionId}
                if (request.CardType == (int)CassandraService.Entity.MLCard.MLCardType.SelectOne || request.CardType == (int)CassandraService.Entity.MLCard.MLCardType.MultiChoice)
                {
                    for (int i = 0; i < request.Options.Count; i++)
                    {
                        MLEducationOption mlOption = new MLEducationOption { Content = request.Options[i].Content, Score = ((i == 0) ? 1 : 0) };

                        if (request.Options[i].UploadedContent != null && request.Options[i].UploadedContent.IsNewFile)
                        {
                            if (string.IsNullOrEmpty(request.Options[i].OptionId))
                            {
                                request.Options[i].OptionId = UUIDGenerator.GenerateUniqueIDForMLOption();
                            }
                            string folderName = "mlearnings/educations/" + request.EducationId + "/" + request.CardId + "/" + request.Options[i].OptionId;
                            string fileUrl = AddToS3(request.CompanyId, request.ManagerId, bucketName, folderName, request.Options[i].UploadedContent);

                            mlOption.HasUploadedContent = true;
                            mlOption.UploadedContent = new List<MLUploadedContent>();
                            mlOption.UploadedContent.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = fileUrl });
                        }
                        else if (request.Options[i].UploadedContent != null)
                        {
                            mlOption.HasUploadedContent = false;
                            mlOption.UploadedContent = new List<MLUploadedContent>();
                            mlOption.UploadedContent.Add(new MLUploadedContent { UploadType = (int)MLUploadTypeEnum.Image, Url = request.Options[i].UploadedContent.Url });
                        }

                        options.Add(mlOption);
                    }
                }
                #endregion

                #region Step 3. Article Pages
                List<MLPageContent> pages = new List<MLPageContent>();
                if (request.CardType == (int)CassandraService.Entity.MLCard.MLCardType.ReadAndAnswer)
                {
                    for (int i = 0; i < request.ArticlePages.Count; i++)
                    {
                        pages.Add(new MLPageContent { Title = request.ArticlePages[i].Title, Content = request.ArticlePages[i].Content });
                    }
                }
                #endregion

                request.CardType = request.CardType;

                #region Step 4. Call API
                response = asc.UpdateMLEducationCard(request.CardId,
                    request.CardType,
                    request.IsOptionRandomized,
                    request.BackgroundType,
                    request.CategoryId,
                    request.EducationId,
                    request.ManagerId,
                    request.CompanyId,
                    request.Content,
                    request.MinOptionToSelect,
                    request.MaxOptionToSelect,
                    request.Description,
                    cardUploadFiles,
                    options,
                    pages
                    );
                #endregion
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetAutoCompleteList")]
        public GetAutoCompleteEducationListResponse GetEducationList(EducationRequest request)
        {
            GetAutoCompleteEducationListResponse response = new GetAutoCompleteEducationListResponse();
            response.Success = false;

            try
            {
                MLEducationListResponse educationListResponse = asc.SelectAllMLEducaation(request.ManagerId, request.CompanyId, request.CategoryId, request.ContainsName);
                if (educationListResponse.Success)
                {
                    response.Educations = new List<AutoCompleteEducationEntry>();
                    for (int i = 0; i < educationListResponse.Educations.Count; i++)
                    {
                        response.Educations.Add(new AutoCompleteEducationEntry
                        {
                            label = educationListResponse.Educations[i].Title,
                            value = educationListResponse.Educations[i].EducationId,
                            education_id = educationListResponse.Educations[i].EducationId,
                            education_title = educationListResponse.Educations[i].Title
                            //is_for_everyone = educationListResponse.Educations[i].IsForEveryone,
                            //is_for_department = educationListResponse.Educations[i].IsForDepartment,
                            //is_for_user = educationListResponse.Educations[0].IsForEveryone,
                            //targeted_departments = educationListResponse.Educations[0].TargetedDepartments,
                            //targeted_users = educationListResponse.Educations[0].TargetedUsers
                        });
                    }
                    response.Success = true;
                }
                else
                {
                    response.ErrorCode = educationListResponse.ErrorCode;
                    response.ErrorMessage = educationListResponse.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        #region S3 Helper functions
        private string AddToS3(string companyId, string managerUserId, string bucketName, string folderName, UploadFileContent fileContent, string processTimestamp = null)
        {
            //String bucketName = "cocadre-" + companyId.ToLowerInvariant();
            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(companyId, managerUserId);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String fileBase64String = String.Empty, fileFormat = String.Empty, fileUrl = String.Empty;
            int fileWidth = 0, fileHeight = 0;

            using (s3Client)
            {
                if (string.IsNullOrEmpty(processTimestamp))
                {
                    processTimestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                }

                if (fileContent.UploadType == UploadFileContent.FILE_TYPE_IMAGE)
                {
                    fileWidth = Convert.ToInt16(fileContent.Width);
                    fileHeight = Convert.ToInt16(fileContent.Height);
                    fileBase64String = fileContent.Base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                    fileFormat = fileContent.Extension.Replace("image/", "");

                    #region Upload original image
                    // Upload original image
                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    byte[] imgBytes = Convert.FromBase64String(fileBase64String);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                    System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                    String fileName = "1_" + processTimestamp + "_original" + "." + fileFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(fileWidth));
                    metadatas.Add("Height", Convert.ToString(fileHeight));
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    #endregion Upload original image

                    #region Upload large (if original image is 1920 * 1920)
                    // Upload large (if original image is 1920 * 1920)
                    System.Drawing.Image newImage;
                    if (fileWidth > 1920 || fileHeight > 1920)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                        fileName = "1_" + processTimestamp + "_large" + "." + fileFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = "1_" + processTimestamp + "_large" + "." + fileFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion Upload large (if original image is 1920 * 1920)

                    #region Upload medium (if original image is 1080 * 1080)
                    // Upload medium (if original image is 1080 * 1080)
                    if (fileWidth > 1080 || fileHeight > 1080)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                        fileName = "1_" + processTimestamp + "_medium" + "." + fileFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = "1_" + processTimestamp + "_medium" + "." + fileFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion Upload medium (if original image is 1080 * 1080)

                    #region Upload small (if original image is 540 * 540)
                    // Upload small (if original image is 540 * 540)
                    if (fileWidth > 540 || fileHeight > 540)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                        fileName = "1_" + processTimestamp + "_small" + "." + fileFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = "1_" + processTimestamp + "_small" + "." + fileFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion Upload small (if original image is 540 * 540)

                    fileUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/1_" + processTimestamp + "_original." + fileFormat.ToLower();
                }
                else if (fileContent.UploadType == UploadFileContent.FILE_TYPE_VIDEO_PORTRAIT || fileContent.UploadType == UploadFileContent.FILE_TYPE_VIDEO_LANDSCAPE)
                {
                    fileBase64String = fileContent.Base64.Remove(0, fileContent.Base64.IndexOf(",") + 1);
                    fileFormat = fileContent.Extension.Replace("video/", "");

                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    byte[] contentBytes = Convert.FromBase64String(fileBase64String);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                    String fileName = "1_" + processTimestamp + "_original" + "." + fileFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);

                    fileUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/1_" + processTimestamp + "_original." + fileFormat.ToLower();
                }
                else if (fileContent.UploadType == UploadFileContent.FILE_TYPE_PDF_PORTRAIT || fileContent.UploadType == UploadFileContent.FILE_TYPE_PDF_LANDSCAPE)
                {
                    fileBase64String = fileContent.Base64.Remove(0, fileContent.Base64.IndexOf(",") + 1);
                    fileFormat = fileContent.Extension.Replace("application/", "");
                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    byte[] contentBytes = Convert.FromBase64String(fileBase64String);
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                    String fileName = "1_" + processTimestamp + "_original" + "." + fileFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);

                    fileUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/1_" + processTimestamp + "_original." + fileFormat.ToLower();
                }
                else
                {
                    // do nothing
                }

                return fileUrl;
            }

        }

        private string AddVideoToS3(string companyId, string managerUserId, string folderName, UploadFileContent fileContent)
        {
            String bucketName = "cocadre-" + companyId;
            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(companyId, managerUserId);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            using (s3Client)
            {
                string fileBase64String = fileContent.Base64.Remove(0, fileContent.Base64.IndexOf(",") + 1);
                String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] contentBytes = Convert.FromBase64String(fileBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                String fileName = PROCESS_TIMESTAMP + "_original" + "." + fileContent.Extension.ToLower();
                S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);

                string s3url = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;

                return s3url;
            }
        }
        #endregion S3 Helper functions
    }
}
