﻿using AdminWebsite.App_Code.ServiceRequests;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Web.Http;

namespace AdminWebsite.Api
{
    [RoutePrefix("Api/MatchUp")]
    public class MatchUpController : ApiController
    {
        private AdminService asc = new AdminService();

        [Serializable]
        [DataContract]
        public class GetListResponse
        {
            [DataMember]
            public bool Success { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int ErrorCode { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string ErrorMessage { get; set; }

            [DataMember]
            public List<AutoCompleteEntry> Topics { get; set; }
        }

        [Serializable]
        [DataContract]
        public class AutoCompleteEntry
        {
            [DataMember]
            public string label { get; set; }

            [DataMember]
            public string value { get; set; }

            [DataMember]
            public string topic_id { get; set; }

            [DataMember]
            public string topic_title { get; set; }
        }

        [AcceptVerbs("POST")]
        [Route("GetList")]
        public GetListResponse GetList(MatchUpGetListRequest request)
        {
            GetListResponse response = new GetListResponse();
            response.Success = false;

            try
            {
                GamificationSelectTopicResponse gamificationResponse = asc.SelectTopicsForAchievement(request.ManagerId, request.CompanyId, request.ContainsName);
                if (gamificationResponse.Success)
                {
                    response.Topics = new List<AutoCompleteEntry>();
                    for (int i = 0; i < gamificationResponse.Topics.Count; i++)
                    {
                        response.Topics.Add(new AutoCompleteEntry
                        {
                            label = gamificationResponse.Topics[i].TopicTitle,
                            value = gamificationResponse.Topics[i].TopicId,
                            topic_id = gamificationResponse.Topics[i].TopicId,
                            topic_title = gamificationResponse.Topics[i].TopicTitle
                        });
                    }
                    response.Success = true;
                }
                else
                {
                    response.ErrorCode = gamificationResponse.ErrorCode;
                    response.ErrorMessage = gamificationResponse.ErrorMessage;
                }

            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("Get")]
        [Route("Export")]
        public HttpResponseMessage Export(string ManagerId, string CompanyId, string TopicId)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            try
            {
                #region Step 1. Get FileStream 
                TopicExportResponse responseExport = new TopicExportResponse();
                responseExport = asc.ExportTopic(ManagerId, CompanyId, TopicId);
                #endregion

                #region Step 2.  HttpResponseMessage
                if (responseExport.Success)
                {
                    response.Content = responseExport.ZipFileStream;
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = responseExport.FileFullName
                    };
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response = new HttpResponseMessage(HttpStatusCode.NoContent);
                }
                #endregion
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("ImportQuestions")]
        public MatchUpImportQuestionsResponse ImportQuestions(MatchUpImportQuestionsRequest request)
        {
            MatchUpImportQuestionsResponse response = new MatchUpImportQuestionsResponse();
            try
            {
                response = asc.ImportQuestions(request.ManagerId, request.CompanyId, request.TopicId, request.CategoryId, request.ZipFileBase64String);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }
    }
}
