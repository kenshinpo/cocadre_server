﻿using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite
{
    public class PulseResponsiveDeckAjaxRequest
    {
        public string deckId { get; set; }
        public string adminUserId { get; set; }
        public string companyId { get; set; }
        public string title { get; set; }
        public int deckType { get; set; }
        public bool isCompulsory { get; set; }
        public bool isAnonymous { get; set; }
        public int publishMethodType { get; set; }
        public int status { get; set; }
        public DateTime startDate { get; set; }
        public DateTime? endDate { get; set; }
        public bool isPrioritized { get; set; }
        public List<string> targetedDepartmentIds { get; set; }
        public List<string> targetedUserIds { get; set; }
        public int numberOfCardsPerTimeFrame { get; set; }
        public int perTimeFrameType { get; set; }
        public int periodRule { get; set; }
        public int action { get; set; }
        public int duration { get; set; }
        public List<string> importDeckIds { get; set; }
        public int anonymityCount { get; set; }

    }

    public class PulseResponsiveDeckController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "card1", "card2" };
        //}

        //public CassandraService.Entity.PulseDynamic Get()
        //{
        //    CassandraService.Entity.PulseDynamic pd = new CassandraService.Entity.PulseDynamic();

        //    return pd;
        //}

        // GET api/<controller>
        public PulseSelectAllDecksResponse Get([FromUri]PulseResponsiveDeckAjaxRequest req)
        {
            return asc.SelectAllDeckBasic(req.adminUserId, req.companyId);
        }

        // POST api/<controller>
        public CassandraService.ServiceResponses.PulseUpdateResponse Post([FromBody]PulseResponsiveDeckAjaxRequest req)
        {
            if (req.action == 1) // update all
            {
                CassandraService.ServiceResponses.PulseUpdateResponse response = asc.UpdateDeck(
                    req.deckId,
                    req.adminUserId,
                    req.companyId,
                    req.title,
                    req.isCompulsory,
                    req.anonymityCount,
                    req.status,
                    req.startDate,
                    req.endDate,
                    req.isPrioritized,
                    req.targetedDepartmentIds,
                    req.targetedUserIds,
                    req.duration
                    );
                return response;
                //return new CassandraService.ServiceResponses.PulseUpdateResponse() { Success = false, ErrorCode = -1, ErrorMessage = "Test API" };
            }
            else if(req.action == 2) // just update status
            {
                CassandraService.ServiceResponses.PulseUpdateResponse response = asc.UpdateDeckStatus(
                    req.adminUserId,
                    req.companyId,
                    req.deckId,
                    req.status
                    );
                return response;
            }
            else // import deck
            {
                return asc.ImportDeck(req.adminUserId, req.companyId, req.deckId, req.importDeckIds);
            }

        }


        // PUT api/<controller>/5
        public CassandraService.ServiceResponses.PulseDeckCreateResponse Put([FromBody]PulseResponsiveDeckAjaxRequest req)
        {
            if (string.IsNullOrWhiteSpace(req.deckId))
            {
                req.deckId = UUIDGenerator.GenerateUniqueIDForPulseDeck();
            }

            CassandraService.ServiceResponses.PulseDeckCreateResponse response = asc.CreateDeck(
                req.deckId,
                req.adminUserId,
                req.companyId,
                req.title,
                req.isCompulsory,
                req.anonymityCount,
                req.publishMethodType,
                req.status,
                req.startDate,
                req.endDate,
                req.isPrioritized,
                req.targetedDepartmentIds,
                req.targetedUserIds,
                req.numberOfCardsPerTimeFrame,
                req.perTimeFrameType,
                req.periodRule,
                req.duration
                );

            return response;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {

        }

        #region S3 Helper functions
        private string AddToS3(string company_id, string manager_user_id, AdminWebsite.ContentImage cardImage, string pulseId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            // for S3
            String bucketName = "cocadre-" + company_id.ToLowerInvariant();
            String folderName = string.Format("pulses/{0}", pulseId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
            int imgWidth = 0, imgHeight = 0;
            string filenamePrefix = string.Empty;

            using (s3Client)
            {
                // Grab image width and height

                imgWidth = Convert.ToInt16(cardImage.width);
                imgHeight = Convert.ToInt16(cardImage.height);
                imgBase64String = cardImage.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                imgFormat = cardImage.extension.Replace("image/", "");
                filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                String fileName = filenamePrefix + "_original" + "." + imgFormat.ToLower();
                metadatas.Clear();
                metadatas.Add("Width", Convert.ToString(imgWidth));
                metadatas.Add("Height", Convert.ToString(imgHeight));
                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                #region Upload large (if original image is 1920 * 1920)
                // Upload large (if original image is 1920 * 1920)
                System.Drawing.Image newImage;
                if (imgWidth > 1920 || imgHeight > 1920)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload large (if original image is 1920 * 1920)

                #region Upload medium (if original image is 1080 * 1080)
                // Upload medium (if original image is 1080 * 1080)
                if (imgWidth > 1080 || imgHeight > 1080)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload medium (if original image is 1080 * 1080)

                #region Upload small (if original image is 540 * 540)
                // Upload small (if original image is 540 * 540)
                if (imgWidth > 540 || imgHeight > 540)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload small (if original image is 540 * 540)

                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + imgFormat.ToLower();

                return imageUrl;

            }

        }
        #endregion S3 Helper functions
    }
}