﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite
{
    public class RSLogicController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "card1", "card2" };
        }

        // GET api/<controller>/5
        public string Get([FromBody]RSCardAjaxRequest req)
        {
            return "value";
        }

        // POST api/<controller>
        public CassandraService.ServiceResponses.RSOptionSelectLogicResponse Post([FromBody]RSCardAjaxRequest req)
        {
            CassandraService.ServiceResponses.RSOptionSelectLogicResponse response = asc.SelectLogicToNextCard(
                req.TopicId,
                req.CategoryId,
                req.CardId,
                req.AdminUserId,
                req.CompanyId);

            return response;
        }

        // PUT api/<controller>/5
        public void Put(int id)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}