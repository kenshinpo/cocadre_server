﻿using AdminWebsite.App_Code.ServiceRequests;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class AssignmentRequest : BasicRequest
    {
        public string AssignmentId { get; set; }
        public List<string> TargetEducationIds { get; set; }
        public string TargetEducationId { get; set; }
        public int DruationType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ParticipantsType { get; set; }
        public int FrequencyDays { get; set; }
        public List<string> TargetedDepartmentIds { get; set; }
        public List<string> TargetedUserIds { get; set; }
    }

    [RoutePrefix("Api/MLAssignment")]
    public class MLAssignmentController : ApiController
    {
        private AdminService asc = new AdminService();

        [AcceptVerbs("POST")]
        [Route("GetDepartmentPrivacy")]
        public MLAssignmentPrivicyResponse GetDepartmentPrivacy(AssignmentRequest request)
        {
            MLAssignmentPrivicyResponse response = new MLAssignmentPrivicyResponse();
            try
            {
                response = asc.SelectAssignmentDepartmentPrivacy(request.ManagerId, request.CompanyId, request.TargetEducationIds);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetPrivacy")]
        public MLAssignmentPrivicyResponse GetPrivacy(AssignmentRequest request)
        {
            MLAssignmentPrivicyResponse response = new MLAssignmentPrivicyResponse();
            try
            {
                response = asc.SelectAssignmentPrivacy(request.ManagerId, request.CompanyId, request.TargetEducationIds);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("CreateAssignments")]
        public MLAssignmentListResponse CreateAssignments(AssignmentRequest request)
        {
            MLAssignmentListResponse response = new MLAssignmentListResponse();
            try
            {

                response = asc.CreateAssignments(request.ManagerId,
                    request.CompanyId,
                    request.TargetEducationIds,
                    request.DruationType,
                    request.StartDate,
                    request.EndDate,
                    request.ParticipantsType,
                    request.FrequencyDays,
                    request.TargetedDepartmentIds,
                    request.TargetedUserIds
                    );
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("GetAssignmentDetail")]
        public MLAssignmentDetailResponse GetAssignmentDetail(AssignmentRequest request)
        {
            MLAssignmentDetailResponse response = new MLAssignmentDetailResponse();
            try
            {

                response = asc.SelectAssignmentDetail(request.ManagerId,
                    request.CompanyId,
                    request.AssignmentId
                    );
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("UpdateAssignment")]
        public MLAssignmentUpdateResponse UpdateAssignment(AssignmentRequest request)
        {
            MLAssignmentUpdateResponse response = new MLAssignmentUpdateResponse();
            try
            {

                response = asc.UpdateMLAssignment(request.ManagerId,
                    request.CompanyId,
                    request.AssignmentId,
                    request.TargetEducationId,
                    request.DruationType,
                    request.StartDate,
                    request.EndDate,
                    request.ParticipantsType,
                    request.FrequencyDays,
                    request.TargetedDepartmentIds,
                    request.TargetedUserIds
                    );
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }
    }
}
