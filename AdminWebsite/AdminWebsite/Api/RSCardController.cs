﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite
{
    public class RSCardAjaxRequest
    {
        //'TopicId': "RSTe80be03be86642028ef09d86094bcbc2",
        //'CategoryId': "RSCce652b65592444c28d9a6ef6bd78a926",
        //'AdminUserId': "U811e9a9236c140ad98a211a428b6dc84",
        //'CompanyId': "C8b28091502514318bdcf48bec7c69129"

        public string Action { get; set; }
        public string TopicId { get; set; }
        public string CategoryId { get; set; }
        public string CardId { get; set; }
        public string AdminUserId { get; set; }
        public string CompanyId { get; set; }
        public string UserData { get; set; }
        
        //string cardId, 
        //string topicId
        //string adminUserId,
        //string companyId,
    }

    public class RSCardController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "card1", "card2" };
        }

        // GET api/<controller>/5
        public string Get([FromBody]RSCardAjaxRequest req)
        {
            return "value";
        }

        // POST api/<controller>
        public CassandraService.ServiceResponses.RSCardSelectResponse Post([FromBody]RSCardAjaxRequest req)
        {
            if (req.Action.Equals("get", StringComparison.InvariantCultureIgnoreCase))
            {

            }
            CassandraService.ServiceResponses.RSCardSelectResponse response = asc.SelectFullDetailCard(
                req.AdminUserId,
                req.CompanyId,
                req.CardId,
                req.TopicId);

            //response.Card.Note = "sample note";
            //response.Card.CustomAnswerInstruction = "insstr";
            //response.Card.ToBeSkipped = true;
            //response.Card.HasCustomAnswer = true;
            //response.Card.IsOptionRandomized = true;

            return response;

    //        return asc.SelectFullDetailRSTopic(
    //req.AdminUserId,
    //req.CompanyId,
    //req.TopicId,
    //req.CategoryId);

        }

        // PUT api/<controller>/5
        public CassandraService.ServiceResponses.RSCardUpdateResponse Put(int id, [FromBody]RSCardAjaxRequest req)
        {
            //string cardId,
            //string content,
            //bool hasImage,
            //List< RSImage > images,
            //string note,
            //bool hasPageBreak,
            //string categoryId,
            //string topicId,
            //string adminUserId,
            //string companyId,
            //List< RSOption > options)

            string x = string.Empty;

            //CassandraService.ServiceResponses.RSCardUpdateResponse response = asc.UpdateCard(
            //    req.AdminUserId,
            //    req.CompanyId,
            //    req.CardId,
            //    req.TopicId);

            return null;
            //return response;
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}