﻿using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite
{
    public class RSCardUpdateController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "card1", "card2" };
        }

        // GET api/<controller>/5
        public string Get([FromBody]RSCardAjaxRequest req)
        {
            return "value";
        }

        private string AddToS3(string company_id, string survey_id, string manager_user_id, string card_id, int ordering, AdminWebsite.App_Code.Entity.SurveyCard.ContentImage cardImage, string option_id = null)
        {
            if (cardImage == null)
                return string.Empty;

            // for S3
            String bucketName = "cocadre-" + company_id.ToLowerInvariant();
            String folderName;
            if (option_id == null)
            {
                folderName = "surveys/" + survey_id + "/" + card_id;
            }
            else
            {
                folderName = "surveys/" + survey_id + "/" + card_id + "/" + option_id;
            }
            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
            int imgWidth = 0, imgHeight = 0;
            string filenamePrefix = string.Empty;

            using (s3Client)
            {
                // Grab image width and height

                imgWidth = Convert.ToInt16(cardImage.width);
                imgHeight = Convert.ToInt16(cardImage.height);
                imgBase64String = cardImage.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                imgFormat = cardImage.extension.Replace("image/", "");
                filenamePrefix = ordering.ToString() + "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");


                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                String fileName = filenamePrefix + "_original" + "." + imgFormat.ToLower();
                metadatas.Clear();
                metadatas.Add("Width", Convert.ToString(imgWidth));
                metadatas.Add("Height", Convert.ToString(imgHeight));
                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                #region Upload large (if original image is 1920 * 1920)
                // Upload large (if original image is 1920 * 1920)
                System.Drawing.Image newImage;
                if (imgWidth > 1920 || imgHeight > 1920)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload large (if original image is 1920 * 1920)

                #region Upload medium (if original image is 1080 * 1080)
                // Upload medium (if original image is 1080 * 1080)
                if (imgWidth > 1080 || imgHeight > 1080)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload medium (if original image is 1080 * 1080)

                #region Upload small (if original image is 540 * 540)
                // Upload small (if original image is 540 * 540)
                if (imgWidth > 540 || imgHeight > 540)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload small (if original image is 540 * 540)

                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + imgFormat.ToLower();

                return imageUrl;

            }

        }

        // POST api/<controller>
        public CassandraService.ServiceResponses.RSCardUpdateResponse Post([FromBody]RSCardAjaxRequest req)
        {
            CassandraService.ServiceResponses.RSCardUpdateResponse response = new CassandraService.ServiceResponses.RSCardUpdateResponse();
            response.Success = false;

            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            AdminWebsite.App_Code.Entity.SurveyCard.ContentImage cardImage = null;
            string s3ImageUrl = string.Empty;

            if (req.UserData.Length > 0)
            {
                try
                {
                    CassandraService.Entity.RSCard card = javascriptSerializer.Deserialize<CassandraService.Entity.RSCard>(req.UserData);

                    // Special handling for images

                    #region Handling of card (question) images
                    if ((card.HasImage) && (card.CardImages.Count > 0))
                    {
                        // Get card image information
                        cardImage = javascriptSerializer.Deserialize<AdminWebsite.App_Code.Entity.SurveyCard.ContentImage>(card.CardImages[0].ImageUrl);

                        // Processs card image depending on card image information
                        if (cardImage.extension != null)
                        {
                            s3ImageUrl = AddToS3(req.CompanyId, req.TopicId, req.AdminUserId, req.CardId, 1, cardImage);
                            card.CardImages[0].ImageUrl = s3ImageUrl;
                        }
                        else
                        {
                            card.CardImages[0].ImageUrl = cardImage.base64;
                        }
                    }
                    #endregion Handling of card (question) images

                    #region Handling of card option images
                    if (card.Options.Count > 0)
                    {
                        foreach (CassandraService.Entity.RSOption opt in card.Options)
                        {
                            cardImage = null;

                            if (opt.HasImage)
                            {
                                try
                                {
                                    cardImage = javascriptSerializer.Deserialize<AdminWebsite.App_Code.Entity.SurveyCard.ContentImage>(opt.Images[0].ImageUrl);
                                }
                                catch (Exception)
                                {
                                }

                                if (cardImage != null)
                                {
                                    if (cardImage.extension != null)
                                    {
                                        s3ImageUrl = AddToS3(req.CompanyId, req.TopicId, req.AdminUserId, req.CardId, 1, cardImage, opt.OptionId);
                                        opt.Images[0].ImageUrl = s3ImageUrl;
                                    }
                                    else
                                    {
                                        opt.Images[0].ImageUrl = cardImage.base64;
                                    }
                                }

                            }
                        }
                    }
                    #endregion Handling of card option images

                    #region comment out
                    //if (card.HasImage &&
                    //    (card.CardImages.Count > 0) &&
                    //    (card.CardImages[0].ImageUrl.StartsWith("{"))
                    //    )
                    //{
                    //    // Add cardImage to S3
                    //    if (cardImage != null)
                    //    {
                    //        if (!cardImage.base64.StartsWith("http"))
                    //        {
                    //            s3ImageUrl = AddToS3(req.CompanyId, req.TopicId, req.AdminUserId, req.CardId, 1, cardImage);
                    //            card.CardImages[0].ImageUrl = s3ImageUrl;
                    //        }
                    //        else
                    //        {
                    //            card.CardImages[0].ImageUrl = cardImage.base64;
                    //        }
                    //    }
                    //}
                    #endregion comment out

                    #region comment out
                    // Handling of card option images
                    //if (card.Options.Count > 0)
                    //{
                    //    foreach (CassandraService.Entity.RSOption opt in card.Options)
                    //    {
                    //        if (opt.HasImage)
                    //        {
                    //            cardImage = javascriptSerializer.Deserialize<AdminWebsite.App_Code.Entity.SurveyCard.ContentImage>(opt.Images[0].ImageUrl);

                    //            if ((cardImage != null) && (!cardImage.base64.StartsWith("http")))
                    //            {
                    //                s3ImageUrl = AddToS3(req.CompanyId, req.TopicId, req.AdminUserId, req.CardId, 1, cardImage);
                    //                opt.Images[0].ImageUrl = s3ImageUrl;
                    //                //card.CardImages[0].ImageUrl = s3ImageUrl;
                    //            }
                    //            else
                    //            {
                    //                card.CardImages[0].ImageUrl = cardImage.base64;
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion comment out

                    return asc.UpdateCard(
                        card.CardId,
                        card.Type,
                        card.Content,
                        card.HasImage,
                        card.CardImages,
                        card.ToBeSkipped,
                        card.HasCustomAnswer,
                        card.CustomAnswerInstruction,
                        card.IsOptionRandomized,
                        card.AllowMultipleLines,
                        card.HasPageBreak,
                        card.BackgroundType,
                        card.Note,
                        req.CategoryId,
                        req.TopicId,
                        req.AdminUserId,
                        req.CompanyId,
                        card.Options,
                        card.MiniOptionToSelect,
                        card.MaxOptionToSelect,
                        card.StartRangePosition,
                        card.MaxRange,
                        card.MaxRangeLabel,
                        card.MidRange,
                        card.MidRangeLabel,
                        card.MinRange,
                        card.MinRangeLabel);

                    //int maxRange,
                    //string maxRangeLabel,
                    //int midRange,
                    //string midRangeLabel,
                    //int minRange,
                    //string minRangeLabel

                }
                catch (Exception ex)
                {
                    // do nothing 
                    response.Success = false;
                    response.ErrorCode = -1;
                    response.ErrorMessage = "Cannot update card." + ex.Message;
                }
            }

            return response;
            //CassandraService.ServiceResponses.RSCardUpdateResponse response = asc.UpdateCard();

            //CassandraService.ServiceResponses.RSCardSelectResponse response = asc.SelectFullDetailCard(
            //    req.AdminUserId,
            //    req.CompanyId,
            //    req.CardId,
            //    req.TopicId);

            //return response;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]RSCardAjaxRequest req)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}