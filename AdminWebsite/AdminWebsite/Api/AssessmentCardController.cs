﻿using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class AssessmentCardAjaxRequest
    {
        public string cardId { get; set; }
        public int type { get; set; }
        public string content { get; set; }
        public List<AssessmentImage> images { get; set; }
        public bool toBeSkipped { get; set; }
        public bool isOptionRandomized { get; set; }
        public bool hasPageBreak { get; set; }
        public int backgoundType { get; set; }
        public string note { get; set; }
        public string assessmentId { get; set; }
        public string adminUserId { get; set; }
        public List<AssessmentCardOption> options { get; set; }
        public int totalPointsAllocation { get; set; }
        public int miniOptionToSelect { get; set; }
        public int maxOptionToSelect { get; set; }
        public int startRangePosition { get; set; }
        public int maxRange { get; set; }
        public string maxRangeLabel { get; set; }
        public int midRange { get; set; }
        public string midRangeLabel { get; set; }
        public int minRange { get; set; }
        public string minRangeLabel { get; set; }
        //public List<AssessmentCardOption> Options { get; set; }

        public AssessmentCardAjaxRequest()
        {
            this.images = new List<AssessmentImage>();
            this.options = new List<AssessmentCardOption>();
        }
    }

    public class AssessmentCardAjaxRequest2
    {
        public string cardId { get; set; }
        public int type { get; set; }
        public string content { get; set; }
        public List<AssessmentImage> images { get; set; }
        public bool toBeSkipped { get; set; }
        public bool isOptionRandomized { get; set; }
        public bool hasPageBreak { get; set; }
        public int backgoundType { get; set; }
        public string note { get; set; }
        public string assessmentId { get; set; }
        public string adminUserId { get; set; }
        public List<AssessmentCardOption> Options { get; set; }
        public int totalPointsAllocated { get; set; }
        public int miniOptionToSelect { get; set; }
        public int maxOptionToSelect { get; set; }
        public int startRangePosition { get; set; }
        public int maxRange { get; set; }
        public string maxRangeLabel { get; set; }
        public int midRange { get; set; }
        public string midRangeLabel { get; set; }
        public int minRange { get; set; }
        public string minRangeLabel { get; set; }

        public List<CassandraService.Entity.ContentImage> CardImages { get; set; }

        public AssessmentCardAjaxRequest2()
        {
            this.images = new List<AssessmentImage>();
            this.Options = new List<AssessmentCardOption>();
            this.CardImages = new List<CassandraService.Entity.ContentImage>();
        }
    }

    //'Action': 'UPDATE',
    //'TopicId': $("#main_content_hfTopicId").val(),
    //'CategoryId': $("#main_content_hfCategoryId").val(),
    //'CardId': cardId,
    //'AdminUserId': $("#main_content_hfAdminUserId").val(),
    //'CompanyId': $("#main_content_hfCompanyId").val()
    public class AssessmentCardDeleteAjaxRequest
    {
        public string adminUserId { get; set; }
        public string assessmentId { get; set; }
        public string cardId { get; set; }
    }

    public class AssessmentCardController : ApiController
    {
        CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();

        // POST api/<controller>
        public CassandraService.ServiceResponses.AssessmentSelectCardResponse GET([FromUri]AssessmentCardAjaxRequest req)
        {
            CassandraService.ServiceResponses.AssessmentSelectCardResponse response = asc.SelectFullDetailAssessmentCard(
                req.assessmentId,
                req.cardId,
                req.adminUserId);

            return response;
        }


        // POST api/<controller>
        public CassandraService.ServiceResponses.AssessmentUpdateResponse Post([FromBody]AssessmentCardAjaxRequest2 req)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer =
                new System.Web.Script.Serialization.JavaScriptSerializer();
            javascriptSerializer.MaxJsonLength = int.MaxValue;

            string imageUrl = string.Empty;
            CassandraService.ServiceResponses.AssessmentUpdateResponse response = null;

            #region Save card images to S3
            if (req.CardImages.Count > 0) 
            {
                if (req.CardImages[0].base64.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                {
                    req.images.Add(new AssessmentImage
                    {
                        ImageId = UUIDGenerator.GenerateUniqueIDForRSImage(),
                        ImageUrl = req.CardImages[0].base64
                    });
                }
                else
                {
                    // Write to S3
                    // Update req.images
                    imageUrl = AddToS3(
                        "COCADRE",
                        req.adminUserId,
                        req.CardImages[0],
                        req.assessmentId
                    );

                    req.images.Add(new AssessmentImage
                    {
                        ImageId = UUIDGenerator.GenerateUniqueIDForRSImage(),
                        ImageUrl = imageUrl
                    });
                }
            }
            #endregion Save card images to S3

            // Save option images to S3
            int optionOrder = 1;
            foreach (AssessmentCardOption option in req.Options)
            {
                if (option.OptionId.StartsWith("undefined_", StringComparison.InvariantCultureIgnoreCase))
                {
                    option.OptionId = UUIDGenerator.GenerateUniqueIDForAssessmentCardOption();
                }
                option.Ordering = optionOrder++;

                if (option.HasImage)
                {
                    foreach (AssessmentImage image in option.Images)
                    {
                        if (image.ImageId == null)
                        {
                            CassandraService.Entity.ContentImage contentImage = javascriptSerializer.Deserialize<CassandraService.Entity.ContentImage>(image.ImageUrl);
                            if (contentImage.base64.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                            {
                                image.ImageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                                image.ImageUrl = contentImage.base64;
                            }
                            else
                            {
                                imageUrl = AddToS3(
                                    "COCADRE",
                                    req.adminUserId,
                                    contentImage,
                                    req.assessmentId
                                );

                                image.ImageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                                image.ImageUrl = imageUrl;
                            }
                            
                        }
                    }
                }
            }

            try
            {
                response = asc.UpdateAssessmentCard(
                    req.cardId,
                    req.type,
                    req.content,
                    req.images,
                    req.toBeSkipped,
                    req.isOptionRandomized,
                    req.hasPageBreak,
                    req.backgoundType,
                    req.note,
                    req.assessmentId,
                    req.adminUserId,
                    req.Options,
                    req.totalPointsAllocated,
                    req.miniOptionToSelect,
                    req.maxOptionToSelect,
                    req.startRangePosition,
                    req.maxRange,
                    req.maxRangeLabel,
                    req.midRange,
                    req.midRangeLabel,
                    req.minRange,
                    req.minRangeLabel
                    );
            }
            catch (Exception ex)
            {

            }


            return response;
        }

        // PUT: api/Assessment/5
        public CassandraService.ServiceResponses.AssessmentCreateCardResponse Put([FromBody]AssessmentCardAjaxRequest req)
        {
            string assessmentCardId = UUIDGenerator.GenerateUniqueIDForAssessmentCard();
            string oldTabulationId = "";
            Dictionary<string, AssessmentTabulation> tabulationMap = new Dictionary<string, AssessmentTabulation>();

            //if (req.useCustomImage)
            //{
            //    req.backgroundUrl = AddToS3(req.companyId, req.adminUserId, req.customImageDataUrl, pulseId);
            //}
            //CassandraService.ServiceResponses.AssessmentCreateResponse response = null;


            #region Save image banners into S3
            //foreach (CassandraService.Entity.ContentImage img in req.imageBannersUpload)
            //{
            //    string bannerUrl = AddToS3(req.companyId, req.adminUserId, img, assessmentId);
            //    req.imageBanners.Add(new AssessmentImage
            //    {
            //        ImageUrl = bannerUrl
            //    });
            //}
            #endregion Save image banners into S3

            #region Save video into S3
            //if (req.videoUpload.filename != null)
            //{
            //    req.videoUrl = DumpToS3(req.companyId, req.adminUserId, req.videoUpload, assessmentId);
            //}
            #endregion Save video into S3

            #region Save video thumbnail into S3
            //if (req.videoThumbnailUpload.filename != null)
            //{
            //    req.videoThumbnailUrl = DumpToS3(req.companyId, req.adminUserId, req.videoThumbnailUpload, assessmentId);
            //}
            #endregion Save video thumbnail into S3

            #region Save tabulation images to S3

            //foreach (AssessmentTabulation tab in req.tabulation_list)
            //{
            //    oldTabulationId = tab.TabulationId;
            //    tab.TabulationId = UUIDGenerator.GenerateUniqueIDForAssessmentTabulation();
            //    tabulationMap.Add(oldTabulationId, tab);

            //    foreach (CassandraService.Entity.ContentImage img in tab.ImagesUpload)
            //    {
            //        string bannerUrl = AddToS3(req.companyId, req.adminUserId, img, assessmentId);
            //        tab.Images.Add(new AssessmentImage
            //        {
            //            ImageId = UUIDGenerator.GenerateUniqueIDForRSImage(),
            //            ImageUrl = bannerUrl
            //        });
            //    }
            //}
            #endregion Save tabulation images to S3

            #region save AssessmentAnswer
            //foreach (AssessmentAnswer ans in req.result_list)
            //{
            //    ans.AnswerId = UUIDGenerator.GenerateUniqueIDForAssessmentAnswer();
            //    foreach (string tabulationId in ans.TabulationIdList)
            //    {
            //        ans.Tabulations.Add(tabulationMap[tabulationId]);
            //    }
            //}
            #endregion save AssessmentAnswer

            // Save AssessmentImage

            // Save AssessmentCardOption

            CassandraService.ServiceResponses.AssessmentCreateCardResponse response = asc.CreateAssessmentCard(
                assessmentCardId,
                req.type,
                req.content,
                req.images,
                req.toBeSkipped,
                req.isOptionRandomized,
                req.hasPageBreak,
                req.backgoundType,
                req.note,
                req.assessmentId,
                req.adminUserId,
                req.options,
                req.totalPointsAllocation,
                req.miniOptionToSelect,
                req.maxOptionToSelect,
                req.startRangePosition,
                req.maxRange, 
                req.maxRangeLabel,
                req.midRange,
                req.midRangeLabel,
                req.minRange,
                req.minRangeLabel);

            return response;
        }

        // DELETE: api/Dummy/5
        public CassandraService.ServiceResponses.AssessmentUpdateResponse Delete([FromUri]AssessmentCardDeleteAjaxRequest req)
        {
            CassandraService.ServiceResponses.AssessmentUpdateResponse response = null;

            try
            {
                response = asc.DeleteAssessmentCard(req.adminUserId, req.assessmentId, req.cardId);
            }
            catch (Exception)
            {
                
            }

            return response;
        }

        #region S3 Helper functions
        private string AddToS3(string company_id, string manager_user_id, CassandraService.Entity.ContentImage cardImage, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            // for S3
            String bucketName = "cocadre-marketplace";
            String folderName = string.Format("assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
            int imgWidth = 0, imgHeight = 0;
            string filenamePrefix = string.Empty;

            using (s3Client)
            {
                // Grab image width and height

                imgWidth = Convert.ToInt16(cardImage.width);
                imgHeight = Convert.ToInt16(cardImage.height);
                imgBase64String = cardImage.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "").Replace("data:image/gif;base64,", "");
                imgFormat = cardImage.extension.Replace("image/", "");
                filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                String fileName = filenamePrefix + "_original" + "." + imgFormat.ToLower();
                metadatas.Clear();
                metadatas.Add("Width", Convert.ToString(imgWidth));
                metadatas.Add("Height", Convert.ToString(imgHeight));
                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                #region Upload large (if original image is 1920 * 1920)
                // Upload large (if original image is 1920 * 1920)
                System.Drawing.Image newImage;
                if (imgWidth > 1920 || imgHeight > 1920)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload large (if original image is 1920 * 1920)

                #region Upload medium (if original image is 1080 * 1080)
                // Upload medium (if original image is 1080 * 1080)
                if (imgWidth > 1080 || imgHeight > 1080)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload medium (if original image is 1080 * 1080)

                #region Upload small (if original image is 540 * 540)
                // Upload small (if original image is 540 * 540)
                if (imgWidth > 540 || imgHeight > 540)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload small (if original image is 540 * 540)

                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + imgFormat.ToLower();

                return imageUrl;

            }

        }

        private string DumpToS3(string company_id, string manager_user_id, CassandraService.Entity.Base64Content content, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            // for S3
            String bucketName = "cocadre-marketplace";
            String folderName = string.Format("assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            using (s3Client)
            {
                string imgBase64String = content.base64.Remove(0, content.base64.IndexOf(",") + 1);
                string filename = System.IO.Path.GetFileName(content.filename);
                string extension = System.IO.Path.GetExtension(content.filename).Remove(0, 1);
                string filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] contentBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                String fileName = filenamePrefix + "_original" + "." + extension.ToLower();
                S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                string s3url = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + extension.ToLower();

                return s3url;

            }
        }
        #endregion S3 Helper functions
    }
}
