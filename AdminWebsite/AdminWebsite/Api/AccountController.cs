﻿using AdminWebsite.App_Code.ServiceRequests;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdminWebsite.Api
{
    public class AccountRequest : BasicRequest
    {
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string CurrentPassword { get; set; }
        public int AccountType { get; set; }
    }

    [RoutePrefix("Api/Account")]
    public class AccountController : ApiController
    {
        private AdminService asc = new AdminService();

        [AcceptVerbs("POST")]
        [Route("ForgotPassword")]
        public AuthenticationUpdateResponse ForgotPassword(AccountRequest request)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();

            try
            {
                return asc.ForgotPassword(request.Email);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }

        [AcceptVerbs("POST")]
        [Route("ChangePassword")]
        public AuthenticationUpdateResponse ChangePassword(AccountRequest request)
        {
            AuthenticationUpdateResponse response = new AuthenticationUpdateResponse();

            try
            {
                return asc.UpdatePasswordForManager(request.ManagerId, request.Email, request.CompanyId, request.NewPassword, request.CurrentPassword, request.AccountType);
            }
            catch (Exception ex)
            {
                response.ErrorCode = -99999;
                response.ErrorMessage = ex.ToString();
            }
            return response;
        }
    }
}
