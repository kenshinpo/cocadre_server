﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="WebsiteHeader.aspx.cs" Inherits="AdminWebsite.CompanyProfile.WebsiteHeader" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script>
        function PreviewImage(fileInput, maxWidth, maxHeight, maxSize)
        {
            var imgNewImage = document.getElementById('<%=imgNewImage.ClientID%>');
            var hfNewBase64 = document.getElementById('<%=hfNewBase64.ClientID%>');
            var hfNewExt = document.getElementById('<%=hfNewExt.ClientID%>');
            var hfNewWidth = document.getElementById('<%=hfNewWidth.ClientID%>');
            var hfNewHeight = document.getElementById('<%=hfNewHeight.ClientID%>');
            var plNewImage = document.getElementById('<%=plNewImage.ClientID%>');
            var rbNewImage = document.getElementById('<%=rbNewImage.ClientID%>');

            if (fileInput.files && fileInput.files[0]) {
                var type = fileInput.files[0].type;
                var size = fileInput.files[0].size / (1024 * 1024);
                if (type.toLowerCase() == "image/png" || type.toLowerCase() == "image/jpeg" || type.toLowerCase() == "image/jpg") {
                    if (size > maxSize) {
                        toastr.error('Uploaded photo exceeded ' + maxSize + 'MB.');
                    }
                    else {
                        var fileReader = new FileReader();
                        var image = new Image();
                        fileReader.onload = function (e)
                        {
                            image.src = e.target.result;
                            image.onload = function ()
                            {
                                if ((this.width != maxWidth || this.height != maxHeight)) {
                                    toastr.error('Photo should be ' + maxWidth + ' x ' + maxHeight + '.');
                                }
                                else {
                                    hfNewBase64.setAttribute('value', this.src);
                                    hfNewExt.setAttribute('value', type);
                                    hfNewWidth.setAttribute('value', this.width);
                                    hfNewHeight.setAttribute('value', this.height);
                                    imgNewImage.src = this.src;
                                    rbNewImage.checked = true;
                                    plNewImage.style.visibility = 'visible';
                                    fileInput.value = "";

                                    changeToApplyChangeButton();
                                }
                            };
                        }
                        fileReader.readAsDataURL(fileInput.files[0]);
                    }
                }
                else {
                    toastr.error('File extension is invalid.');
                }
            }
        }

        function changeToApplyChangeButton()
        {
            $(".btn.white").removeClass("white").prop("disabled", false).val('Apply Changes').click(ShowProgressBar);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField ID="hfNewBase64" runat="server" />
    <asp:HiddenField ID="hfNewExt" runat="server" />
    <asp:HiddenField ID="hfNewWidth" runat="server" />
    <asp:HiddenField ID="hfNewHeight" runat="server" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Company <span>profile</span></div>
        <div class="appbar__meta">Website header</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/Profile">Profile</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AdminPhoto">Admin photo</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/Email">Email</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Website header</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/CompanyBanner">Login company selection</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/PhoneBanner">Phone main banner</a>
                </li>--%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AnimationLogo">Refresh animation logo</a>
                </li>
                <%--
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/MatchupBanner">Matchup main banner</a>
                </li>
                --%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/CoverPhoto">Profile cover photo</a>
                </li>
            </ul>
        </aside>

        <div class="data__content" style="padding-left: 40px;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="profile-section">
                        <div>
                            <div class="profile-header">Website header</div>
                            <p>
                                Your custom logo will be displayed for mentioned services at
                            <asp:Literal ID="ltlCompanyName1" runat="server" />.<br />
                                The image file should be in PNG or JPG format. Do not include any copyrighted materials.
                            </p>
                            <p>
                                Uploading a new logo will replace your current one across all products and for all users at
                            <asp:Literal ID="ltlCompanyName2" runat="server" />.
                            </p>
                        </div>
                        <div style="float: right; margin-left: 20px;">
                            <asp:Button ID="btnApplyChange" CssClass="btn white" runat="server" Text="Save" Enabled="false" OnClick="btnApplyChange_Click" />
                        </div>
                    </div>
                    <div>
                        <div class="selection">
                            <label>
                                <asp:RadioButton CssClass="profile-radiobutton" ID="rbDefaultImage" GroupName="Image" runat="server" Text="Default image" onclick="changeToApplyChangeButton();" />
                                <asp:Image CssClass="profile-radiobutton-image" ID="imgDefaultImage" runat="server" />
                            </label>
                        </div>
                        <asp:Panel ID="plNewImage" runat="server" CssClass="selection">
                            <label>
                                <asp:RadioButton CssClass="profile-radiobutton" ID="rbNewImage" GroupName="Image" runat="server" Text="Uploaded image" onclick="changeToApplyChangeButton();" />
                                <asp:Image CssClass="profile-radiobutton-image" ID="imgNewImage" runat="server" />
                            </label>
                        </asp:Panel>
                        <p class="profile-margin">Image should be 252 x 70 and no bigger than 1MB</p>

                        <label for="fuNewImage" style="cursor: pointer; margin-left: 25px;" class="btn uppercase">
                            <input type="file" id="fuNewImage" accept="image/*" title="Add image" style="display: none" onchange="PreviewImage(this, 252, 70, 1);" />
                            <i class="fa fa-plus"></i>Upload Image
                        </label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>