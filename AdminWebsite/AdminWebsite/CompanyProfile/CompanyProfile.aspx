﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="CompanyProfile.aspx.cs" Inherits="AdminWebsite.CompanyProfile.CompanyProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script>
        function passwordGrade()
        {
            var textBox = $get("<%=TextBox1.ClientID%>");
            var pwd = textBox.value;
            console.debug("pwd: " + pwd);
            var score = 0;
            var regexArr = ['[0-9]', '[a-z]', '[A-Z]', '[\\W_]'];
            var repeatCount = 0;
            var prevChar = '';
            //check length 
            var len = pwd.length;
            score += len > 18 ? 18 : len;
            //check type 
            for (var i = 0, num = regexArr.length; i < num; i++) { if (eval('/' + regexArr[i] + '/').test(pwd)) score += 4; }
            //bonus point 
            for (var i = 0, num = regexArr.length; i < num; i++) {
                if (pwd.match(eval('/' + regexArr[i] + '/g')) && pwd.match(eval('/' + regexArr[i] + '/g')).length >= 2) score += 2;
                if (pwd.match(eval('/' + regexArr[i] + '/g')) && pwd.match(eval('/' + regexArr[i] + '/g')).length >= 5) score += 2;
            }
            //deduction 
            for (var i = 0, num = pwd.length; i < num; i++) {
                if (pwd.charAt(i) == prevChar) repeatCount++;
                else prevChar = pwd.charAt(i);
            }
            score -= repeatCount * 1;

            var PW = $get("PW");

            if (pwd.length == 0) {
                PW.innerHTML = "";
            }
            else if (pwd.length < 5) {
                PW.innerHTML = "Too short";
            }
            else {
                if (score < 11) {
                    PW.innerHTML = "Weak";
                }
                else if (score > 10 && score < 21) {
                    PW.innerHTML = "Fair";
                }
                else if (score > 20 && score < 31) {
                    PW.innerHTML = "Good";
                }
                    //else if (score > 30 && score < 41) {
                    //    PW.innerHTML = "Strong";
                    //}
                else {
                    PW.innerHTML = "Strong";
                }
            }

            //return score;
        }

        function ResetPersonalizationController(img, hfImgBase64, hfImgExt, hfImgWidth, hfImgHeight, radioButtonDefault, radioButtonNew, divNewPhoto)
        {
            img.src = null;;
            hfImgBase64.removeAttribute('value');
            hfImgExt.removeAttribute('value');;
            hfImgWidth.removeAttribute('value');;
            hfImgHeight.removeAttribute('value');;
            radioButtonDefault.checked = true;;
        }


        function PreviewImage(input, maxWidth, maxHeight, mode)
        {
            var img, hfImgBase64, hfImgExt, hfImgWidth, hfImgHeight, radioButtonDefault, radioButtonNew, divNewPhoto;

            if (mode == 1) { // Admin Photo
                img = document.getElementById('<%=imgNewAdminPhoto.ClientID%>');
                hfImgBase64 = document.getElementById('<%=hfNewAdminPhotoBase64.ClientID%>');
                hfImgExt = document.getElementById('<%=hfNewAdminPhotoExt.ClientID%>');
                hfImgWidth = document.getElementById('<%=hfNewAdminPhotoWidth.ClientID%>');
                hfImgHeight = document.getElementById('<%=hfNewAdminPhotoHeight.ClientID%>');
                radioButtonDefault = document.getElementById('<%=rbDefaultAdminPhoto.ClientID%>');
                radioButtonNew = document.getElementById('<%=rbNewAdminPhoto.ClientID%>');
                divNewPhoto = document.getElementById('<%=plAdminPhoto.ClientID%>');
            }
            else if (mode == 2) { // Company Logo
                img = document.getElementById('<%=imgNewCompanyLogo.ClientID%>');
                hfImgBase64 = document.getElementById('<%=hfNewCompanyLogoBase64.ClientID%>');
                hfImgExt = document.getElementById('<%=hfNewCompanyLogoExt.ClientID%>');
                hfImgWidth = document.getElementById('<%=hfNewCompanyLogoWidth.ClientID%>');
                hfImgHeight = document.getElementById('<%=hfNewCompanyLogoHeight.ClientID%>');
                radioButtonDefault = document.getElementById('<%=rbDefaultCompanyLogo.ClientID%>');
                radioButtonNew = document.getElementById('<%=rbNewCompanyLogo.ClientID%>');
                divNewPhoto = document.getElementById('<%=plCompanyLogo.ClientID%>');
            }
            else if (mode == 3) { // Phone Banner
                img = document.getElementById('<%=imgNewPhoneBanner.ClientID%>');
                hfImgBase64 = document.getElementById('<%=hfNewPhoneBannerBase64.ClientID%>');
                hfImgExt = document.getElementById('<%=hfNewPhoneBannerExt.ClientID%>');
                hfImgWidth = document.getElementById('<%=hfNewPhoneBannerWidth.ClientID%>');
                hfImgHeight = document.getElementById('<%=hfNewPhoneBannerHeight.ClientID%>');
                radioButtonDefault = document.getElementById('<%=rbDefaultPhoneBanner.ClientID%>');
                radioButtonNew = document.getElementById('<%=rbNewPhoneBanner.ClientID%>');
                divNewPhoto = document.getElementById('<%=plPhoneBanner.ClientID%>');
            }
            else if (mode == 4) { // Matchup Banner
                img = document.getElementById('<%=imgNewMatchupBanner.ClientID%>');
                hfImgBase64 = document.getElementById('<%=hfNewMatchupBannerBase64.ClientID%>');
                hfImgExt = document.getElementById('<%=hfNewMatchupBannerExt.ClientID%>');
                hfImgWidth = document.getElementById('<%=hfNewMatchupBannerWidth.ClientID%>');
                hfImgHeight = document.getElementById('<%=hfNewMatchupBannerHeight.ClientID%>');
                radioButtonDefault = document.getElementById('<%=rbDefaultMatchupBanner.ClientID%>');
                radioButtonNew = document.getElementById('<%=rbNewMatchupBanner.ClientID%>');
                divNewPhoto = document.getElementById('<%=plMatchupBanner.ClientID%>');
            }
            else if (mode == 5) { // Profile Cover 
                img = document.getElementById('<%=imgNewProfileCover.ClientID%>');
                hfImgBase64 = document.getElementById('<%=hfNewProfileCoverBase64.ClientID%>');
                hfImgExt = document.getElementById('<%=hfNewProfileCoverExt.ClientID%>');
                hfImgWidth = document.getElementById('<%=hfNewProfileCoverWidth.ClientID%>');
                hfImgHeight = document.getElementById('<%=hfNewProfileCoverHeight.ClientID%>');
                radioButtonDefault = document.getElementById('<%=rbDefaultProfileCover.ClientID%>');
                radioButtonNew = document.getElementById('<%=rbNewProfileCover.ClientID%>');
                divNewPhoto = document.getElementById('<%=plProfileCover.ClientID%>');
            }
            else {

            }

    if (input.files && input.files[0]) {
        var t = input.files[0].type;
        var s = input.files[0].size / (1024 * 1024);
        if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
            if (s > 2) {
                toastr.error('Uploaded photo exceeded 2MB.');
            } else {
                var fileReader = new FileReader();
                var image = new Image();
                fileReader.onload = function (e)
                {
                    image.src = e.target.result;
                    image.onload = function ()
                    {
                        var w = this.width,
                            h = this.height;
                        if ((w != maxWidth || h != maxHeight)) {
                            toastr.error('Uploaded photo exceeded ' + maxWidth + ' x ' + maxHeight + '.');
                        }
                        else {
                            hfImgBase64.setAttribute('value', this.src);
                            hfImgExt.setAttribute('value', t);
                            hfImgWidth.setAttribute('value', w);
                            hfImgHeight.setAttribute('value', h);
                            img.src = this.src;

                            radioButtonNew.checked = true;
                            divNewPhoto.style.visibility = 'visible';
                        }
                    };
                }
            }
        }
        else {
            toastr.error('File extension is invalid.');
        }
        fileReader.readAsDataURL(input.files[0]);
    }
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <!-- New Admin Photo-->
    <asp:HiddenField ID="hfNewAdminPhotoBase64" runat="server" />
    <asp:HiddenField ID="hfNewAdminPhotoExt" runat="server" />
    <asp:HiddenField ID="hfNewAdminPhotoWidth" runat="server" />
    <asp:HiddenField ID="hfNewAdminPhotoHeight" runat="server" />
    <!-- New Company Logo-->
    <asp:HiddenField ID="hfNewCompanyLogoBase64" runat="server" />
    <asp:HiddenField ID="hfNewCompanyLogoExt" runat="server" />
    <asp:HiddenField ID="hfNewCompanyLogoWidth" runat="server" />
    <asp:HiddenField ID="hfNewCompanyLogoHeight" runat="server" />
    <!-- New Phone Banner-->
    <asp:HiddenField ID="hfNewPhoneBannerBase64" runat="server" />
    <asp:HiddenField ID="hfNewPhoneBannerExt" runat="server" />
    <asp:HiddenField ID="hfNewPhoneBannerWidth" runat="server" />
    <asp:HiddenField ID="hfNewPhoneBannerHeight" runat="server" />
    <!-- New Matchup Banner-->
    <asp:HiddenField ID="hfNewMatchupBannerBase64" runat="server" />
    <asp:HiddenField ID="hfNewMatchupBannerExt" runat="server" />
    <asp:HiddenField ID="hfNewMatchupBannerWidth" runat="server" />
    <asp:HiddenField ID="hfNewMatchupBannerHeight" runat="server" />
    <!-- New Profile Cover -->
    <asp:HiddenField ID="hfNewProfileCoverBase64" runat="server" />
    <asp:HiddenField ID="hfNewProfileCoverExt" runat="server" />
    <asp:HiddenField ID="hfNewProfileCoverWidth" runat="server" />
    <asp:HiddenField ID="hfNewProfileCoverHeight" runat="server" />

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfPrimaryAdminId" runat="server" />
            <div id="popup_reassign" class="popup" width="100%" style="display: none;">
                <h1 class="popup__title">Re-assign Primary Admin</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                Select the next admin to be the Primary admin account for
                                <asp:Literal ID="ltlReassignCompanyName" runat="server"></asp:Literal>.
                            </p>
                            <div class="main">
                                <div class="form__row">
                                    <p class="label">Assign the next Primary admin</p>
                                    <div class="mdl-selectfield">
                                        <asp:DropDownList ID="ddlAdmins" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbReassignAssign" runat="server" Text="Assign" CssClass="popup__action__item popup__action__item--cta" OnClick="lbReassignAssign_Click" />
                    <asp:LinkButton ID="lbReassignCancel" runat="server" Text="Cancel" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbReassignCancel_Click" />
                </div>
            </div>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_reassign"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Company Profile <span>Console</span></div>
        <div class="appbar__action">
        </div>
    </div>
    <!-- / App Bar -->

    <div class="data dark">
        <div class="data__content">

            <div class="companyprofile">
                <div class="f-accordion f-accordion--completeview">

                    <div class="f-accordion__head Media">
                        <img class="Media-figure" src="../Img/icon-companyprofile_big.png" alt="" />
                        <div class="Media-body">
                            <h1 class="f-accordion__head__title">Profile</h1>
                            <h2 class="f-accordion__head__subtitle">
                                <asp:Literal ID="ltlCompanyName" runat="server" />
                            </h2>
                        </div>
                    </div>

                    <%--<div class="f-accordion__body">--%>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <ajaxToolkit:Accordion
                                ID="MyAccordion"
                                runat="Server"
                                SelectedIndex="-1"
                                AutoSize="None"
                                FadeTransitions="true"
                                RequireOpenedPane="false"
                                SuppressHeaderPostbacks="true">

                                <Panes>
                                    <ajaxToolkit:AccordionPane ID="apProfile" runat="server">
                                        <Header>
                                            <div class="f-accordion__body">
                                                <div class="f-accordion__section">
                                                    <a class="f-accordion__section__link">
                                                        <h3 class="f-accordion__section__title">Profile</h3>
                                                        <h4 class="f-accordion__section__subtitle">Change your company name and time zone. Set organization-wide settings.</h4>
                                                    </a>
                                                </div>
                                            </div>
                                        </Header>
                                        <Content>
                                            <div class="f-accordion__section f-accordion__section--active">
                                                <div class="f-accordion__section__title" style="padding: 0px;"></div>
                                                <table class="settings-table">
                                                    <tbody>
                                                        <tr>
                                                            <td class="settings-table__title">Organization
                                                            </td>
                                                            <td class="settings-table__content">
                                                                <asp:TextBox ID="tbCompanyTitle" runat="server" placeholder="Organization Name" MaxLength="40" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="settings-table__title">Contact Information
                                                            </td>
                                                            <td class="settings-table__content">
                                                                <p>
                                                                    Designate contact email addresses for service communications, payment notifications, and any email subscriptions.
                                                                </p>
                                                                <div class="label">
                                                                    Primary administrator account
                                                                </div>
                                                                <p>
                                                                    <asp:Literal ID="ltlNwePrimaryAdminName" runat="server" />
                                                                    <asp:Literal ID="ltlOldPrimaryAdminName" runat="server" />
                                                                    <asp:HiddenField ID="hfNewPrimaryAdminId" runat="server" />
                                                                </p>
                                                                <asp:Panel ID="plPrimaryAdmin" runat="server">
                                                                    <asp:LinkButton ID="lbRessignPrimaryAdamin" runat="server" Text="Re-assign Primary admin" OnClick="lbRessignPrimaryAdamin_Click" />
                                                                    <div class="gray">Please ask the mentioned personnel to assign the rights to another admin if you have decided to delete his/her account</div>

                                                                    <%--
                                                                            <div class="label label--far">Secondary emaill address (<i>Optional</i>)</div>
                                                                            <div class="emailtextbox">
                                                                                <input type="text" placeholder="username" />
                                                                                @
                                                                                <input type="text" placeholder="domainname.com" />
                                                                            </div>
                                                                            <div class="gray">Email address needed for recovery. Please make sure it's a valid email</div>
                                                                    --%>
                                                                    <%--<a>Change password</a>--%>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="settings-table__title">Support Message
                                                            </td>
                                                            <td class="settings-table__content">
                                                                <p>
                                                                    The information you want to give your personnel when they need help.
                                                                        <br />
                                                                    Example:
                                                                </p>
                                                                <asp:TextBox ID="tbSupportMsg" runat="server" TextMode="MultiLine" placeholder="Please contact admin at +65 12345678" Style="width: 80%; height: 80px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="settings-table__title">Time Zone
                                                            </td>
                                                            <td class="settings-table__content">
                                                                <p>
                                                                    Time zone for
                                                                        <asp:Literal ID="ltlCompanyName2" runat="server" />
                                                                </p>
                                                                <div class="mdl-selectfield" style="width: 80%;">
                                                                    <asp:DropDownList ID="ddlTimeZone" runat="server" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <%--<tr>
                                                            <td class="settings-table__title">Security and Privacy Additional Terms
                                                            </td>
                                                            <td class="settings-table__content">
                                                                <p>Review and agree to the amendment(s) below if applicable to your compliance needs.</p>
                                                                <p><a href="#">Terms &amp; Services</a></p>
                                                            </td>
                                                        </tr>--%>
                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="popup__action">
                                                                    <asp:LinkButton ID="lbUpadateProfile" runat="server" Text="APPLY CHANGES" CssClass="popup__action__item popup__action__item--cta" OnClick="lbUpadateProfile_Click" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </Content>
                                    </ajaxToolkit:AccordionPane>

                                    <ajaxToolkit:AccordionPane ID="apPersonalization" runat="server" ContentCssClass="f-accordion__section f-accordion__section--active">
                                        <Header>
                                            <%--<div class="f-accordion__body">--%>
                                            <div class="f-accordion__section">
                                                <a class="f-accordion__section__link">
                                                    <h3 class="f-accordion__section__title">Personalization</h3>
                                                    <h4 class="f-accordion__section__subtitle">Upload your company logo</h4>
                                                </a>
                                            </div>
                                            <%--  </div>--%>
                                        </Header>
                                        <Content>
                                            <div class="f-accordion__section f-accordion__section--active">
                                                <div class="f-accordion__section__title" style="padding: 0px;"></div>
                                                <table class="settings-table">
                                                    <tbody>
                                                        <tr>
                                                            <td class="settings-table__title">Admin Photo
                                                            </td>
                                                            <td class="settings-table__content">
                                                                <div class="label">Upload custom image <%--<a href="#">(Preview)</a>--%></div>
                                                                <p>
                                                                    Your custom photo will be displayed as the Admin photo on Admin announcement post and reply. The image file should be in PNG or GIF format. Do not include any copyrighted materials.
                                                                </p>
                                                                <div class="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbDefaultAdminPhoto" GroupName="AdminPhoto" runat="server" Text="Default photo" />
                                                                        <asp:Image ID="imgDefaultAdminPhoto" runat="server" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/company/admin_photo.jpg" />
                                                                    </label>
                                                                </div>
                                                                <asp:Panel ID="plAdminPhoto" runat="server" CssClass="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbNewAdminPhoto" GroupName="AdminPhoto" runat="server" Text="Uploaded photo" />
                                                                        <asp:Image ID="imgNewAdminPhoto" runat="server" />
                                                                    </label>
                                                                </asp:Panel>
                                                                <p>Uploaded photo up to 150x150 and no bigger than 60KB</p>

                                                                <label for="fuNewAdminPhoto" style="cursor: pointer;" class="btn uppercase">
                                                                    <input type="file" id="fuNewAdminPhoto" accept="image/*" title="Add photo" style="display: none" onchange="PreviewImage(this, 150, 150, 1);" />
                                                                    <i class="fa fa-plus"></i>Upload Photo
                                                                </label>
                                                                <%--<asp:FileUpload ID="fuNewAdminPhoto" runat="server" Width="250px" onchange="PreviewImage(this, 150, 150, 1);" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="settings-table__title">Logos
                                                            </td>
                                                            <td class="settings-table__content">
                                                                <div class="label">Upload custom image</div>
                                                                <p>
                                                                    Your custom logo will be displayed for mentioned services at Monetary Authority of Singapore. The image file should be in PNG or GIF format. Do not include any copyrighted materials.
                                                                </p>
                                                                <p>
                                                                    Uploading a new logo will replace your current one across all products and for all users at Monetary Authority of Singapore.
                                                                </p>
                                                                <div class="label">Website Header &amp; Company selection <%--<a href="#">(Preview)</a>--%></div>
                                                                <div class="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbDefaultCompanyLogo" GroupName="CompanyLogo" runat="server" Checked="true" Text="Default photo" />
                                                                        <asp:Image ID="imgDefaultCompanyLogo" runat="server" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/company/company_logo.jpg" />
                                                                    </label>
                                                                </div>
                                                                <asp:Panel ID="plCompanyLogo" runat="server" CssClass="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbNewCompanyLogo" GroupName="CompanyLogo" runat="server" Text="Uploaded photo" />
                                                                        <asp:Image ID="imgNewCompanyLogo" runat="server" />
                                                                    </label>
                                                                </asp:Panel>
                                                                <p>Uploaded photo up to 818 x 168 and no bigger than 60KB</p>

                                                                <label for="fuNewCompanyLogo" style="cursor: pointer;" class="btn uppercase">
                                                                    <input type="file" id="fuNewCompanyLogo" accept="image/*" title="Add photo" style="display: none" onchange="PreviewImage(this, 818, 168, 2);" />
                                                                    <i class="fa fa-plus"></i>Upload Photo
                                                                </label>

                                                                <%--<asp:FileUpload ID="fuNewCompanyLogo" runat="server" onchange="PreviewImage(this, 700, 170, 2);" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <hr />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="settings-table__title"></td>
                                                            <td class="settings-table__content">
                                                                <div class="label">Phone main banner</div>
                                                                <div class="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbDefaultPhoneBanner" GroupName="PhoneBanner" runat="server" Checked="true" Text="Default photo" />
                                                                        <asp:Image ID="imgDefaultPhoneBanner" runat="server" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/company/phone_banner.jpg" />
                                                                    </label>
                                                                </div>
                                                                <asp:Panel ID="plPhoneBanner" runat="server" CssClass="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbNewPhoneBanner" GroupName="PhoneBanner" runat="server" Text="Uploaded photo" />
                                                                        <asp:Image ID="imgNewPhoneBanner" runat="server" />
                                                                    </label>
                                                                </asp:Panel>
                                                                <p>Uploaded photo up to 1242 x 165 and no bigger than 60KB</p>

                                                                <label for="fuNewPhoneBanner" style="cursor: pointer;" class="btn uppercase">
                                                                    <input type="file" id="fuNewPhoneBanner" accept="image/*" title="Add photo" style="display: none" onchange="PreviewImage(this, 1242, 165, 3);" />
                                                                    <i class="fa fa-plus"></i>Upload Photo
                                                                </label>

                                                                <%--<asp:FileUpload ID="fuNewPhoneBanner" runat="server" onchange="PreviewImage(this, 750, 100, 3);" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="settings-table__title"></td>
                                                            <td class="settings-table__content">
                                                                <div class="label">Matchup main banner <%--<a href="#">(Preview)</a>--%></div>
                                                                <div class="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbDefaultMatchupBanner" GroupName="MatchupBanner" runat="server" Checked="true" Text="Default photo" />
                                                                        <asp:Image ID="imgDefaultMatchupBanner" runat="server" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/company/matchup_banner.jpg" />
                                                                    </label>
                                                                </div>
                                                                <asp:Panel ID="plMatchupBanner" runat="server" CssClass="selection">
                                                                    <%--<div class="selection">--%>
                                                                    <label>
                                                                        <%--<div id="div_newMatchupBanner" style="visibility: hidden">--%>
                                                                        <asp:RadioButton ID="rbNewMatchupBanner" GroupName="MatchupBanner" runat="server" Text="Uploaded photo" />
                                                                        <asp:Image ID="imgNewMatchupBanner" runat="server" />
                                                                        <%--</div>--%>
                                                                    </label>
                                                                    <%--</div>--%>
                                                                </asp:Panel>
                                                                <p>Uploaded photo up to 1242 x 580 and no bigger than 120KB</p>

                                                                <label for="fuNewMatchupBanner" style="cursor: pointer;" class="btn uppercase">
                                                                    <input type="file" id="fuNewMatchupBanner" accept="image/*" title="Add photo" style="display: none" onchange="PreviewImage(this, 1242, 580, 4);" />
                                                                    <i class="fa fa-plus"></i>Upload Photo
                                                                </label>

                                                                <%--<asp:FileUpload ID="fuNewMatchupBanner" runat="server" onchange="PreviewImage(this, 1500, 700, 4);" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <hr>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="settings-table__title"></td>
                                                            <td class="settings-table__content">
                                                                <div class="label">Profile popup cover photo <%--<a href="#">(Preview)</a>--%></div>
                                                                <div class="selection">
                                                                    <label>
                                                                        <asp:RadioButton ID="rbDefaultProfileCover" GroupName="ProfileCover" runat="server" Checked="true" Text="Default photo" />
                                                                        <asp:Image ID="imgDefaultProfileCover" runat="server" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/company/profile_cover.jpg" />
                                                                    </label>
                                                                </div>
                                                                <asp:Panel ID="plProfileCover" runat="server" CssClass="selection">
                                                                    <%--<div class="selection">--%>
                                                                    <label>
                                                                        <%--<div id="div_newProfileCover" style="visibility: hidden">--%>
                                                                        <asp:RadioButton ID="rbNewProfileCover" GroupName="ProfileCover" runat="server" Text="Uploaded photo" />
                                                                        <asp:Image ID="imgNewProfileCover" runat="server" />
                                                                        <%--</div>--%>
                                                                    </label>
                                                                    <%--</div>--%>
                                                                </asp:Panel>
                                                                <p>Uploaded photo up to 840 x 510 and no bigger than 60KB</p>

                                                                <label for="fuNewProfileCover" style="cursor: pointer;" class="btn uppercase">
                                                                    <input type="file" id="fuNewProfileCover" accept="image/*" title="Add photo" style="display: none" onchange="PreviewImage(this, 840, 510, 5);" />
                                                                    <i class="fa fa-plus"></i>Upload Photo
                                                                </label>

                                                                <%--<asp:FileUpload ID="fuNewProfileCover" runat="server" onchange="PreviewImage(this, 800, 500, 5);" />--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <div class="popup__action">
                                                                    <asp:LinkButton ID="lbUpadatePersonalization" runat="server" Text="APPLY CHANGES" CssClass="popup__action__item popup__action__item--cta" OnClick="lbUpadatePersonalization_Click" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </Content>
                                    </ajaxToolkit:AccordionPane>

                                   <%-- <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server" HeaderCssClass="f-accordion__section" ContentCssClass="f-accordion__section f-accordion__section--active">
                                        <Header>
                                            <div class="f-accordion__section__link">
                                                <h3 class="f-accordion__section__title">Legal &amp; compliance</h3>
                                            </div>
                                        </Header>
                                        <Content>
                                            <div class="f-accordion__section__pad">
                                                <p>
                                                    For more info contact us at
                                                    <br>
                                                    aporigin@gmail.com
                                                </p>
                                            </div>
                                        </Content>
                                    </ajaxToolkit:AccordionPane>--%>
                                </Panes>
                            </ajaxToolkit:Accordion>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%--</div>--%>
                </div>
            </div>
        </div>
    </div>

    <hr />
    <asp:TextBox ID="TextBox1" runat="server" onkeyup="passwordGrade()" />
    <span id="PW"></span>
</asp:Content>
