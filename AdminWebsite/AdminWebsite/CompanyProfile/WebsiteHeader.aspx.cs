﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.CompanyProfile
{
    public partial class WebsiteHeader : System.Web.UI.Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        private void GetCompanyPersonalization()
        {
            CompanySelectImageResponse response = asc.SelectImage(adminInfo.UserId, adminInfo.CompanyId, (int)Company.CompanyImageType.WebsiteHeader);
            if (response.Success)
            {
                if (response.ImageUrl == null || response.ImageUrl.Equals(DefaultResource.CompanyHeaderImageUrl))
                {
                    plNewImage.Style.Add("visibility", "hidden");
                    rbDefaultImage.Checked = true;
                }
                else
                {
                    plNewImage.Style.Add("visibility", "visible");
                    rbNewImage.Checked = true;
                    imgNewImage.ImageUrl = response.ImageUrl;
                }
            }
            else
            {
                Log.Error("CompanySelectImageResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    GetCompanyPersonalization();
                    imgDefaultImage.ImageUrl = DefaultResource.CompanyHeaderImageUrl;
                    ltlCompanyName1.Text = adminInfo.CompanyName;
                    ltlCompanyName2.Text = adminInfo.CompanyName;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                String imageUrl = string.Empty;
                if (rbNewImage.Checked)
                {
                    imageUrl = imgNewImage.ImageUrl;
                }
                else
                {
                    imageUrl = DefaultResource.CompanyHeaderImageUrl;
                }
                #endregion

                #region Step 2. Upload new image to S3.
                String imgBase64String = String.Empty, imgFormat = String.Empty;
                String bucketName = "cocadre-" + adminInfo.CompanyId.ToLower();
                String folderName = "company";
                IAmazonS3 s3Client = S3Utility.GetIAmazonS3(adminInfo.CompanyId, adminInfo.UserId);
                S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                using (s3Client)
                {
                    if (rbNewImage.Checked && !String.IsNullOrEmpty(hfNewBase64.Value))
                    {
                        String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                        imgBase64String = ImageUtility.FilterBase64String(hfNewBase64.Value);
                        imgFormat = hfNewExt.Value.Replace("image/", "");
                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", hfNewWidth.Value);
                        metadatas.Add("Height", hfNewHeight.Value);
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = PrefixId.CompanyProfile_WebsiteHeader + PROCESS_TIMESTAMP + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                        Log.Debug("WebsiteHeaderUrl: " + imageUrl);
                    }
                }
                #endregion

                #region Step 3. Call API
                CompanyUpdateProfileResponse response = asc.UpdateImage(adminInfo.UserId, adminInfo.CompanyId, imageUrl, (int)Company.CompanyImageType.WebsiteHeader);
                if (response.Success)
                {
                    GetCompanyPersonalization();
                    MessageUtility.ShowToast(this.Page, "Website header changes applied.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("CompanyUpdateProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    MessageUtility.ShowToast(this.Page, "Website header failed to update.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}