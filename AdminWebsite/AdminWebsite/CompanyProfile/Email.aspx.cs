﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.CompanyProfile
{
    public partial class Email : System.Web.UI.Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        private const int COUNT_MAX_TITLE = 30;
        private const int COUNT_MAX_DESCRIPTION = 200;
        private const int COUNT_MAX_SUPPORT_INFO = 200;

        private void GetCompanyPersonalization()
        {
            CompanySelectImageResponse response = asc.SelectImage(adminInfo.UserId, adminInfo.CompanyId, (int)Company.CompanyImageType.EmailSquare);
            if (response.Success)
            {
                if (response.ImageUrl == null || response.ImageUrl.Equals(DefaultResource.CompanyEmailSquareImageUrl))
                {
                    plNewImage.Style.Add("visibility", "hidden");
                    rbDefaultImage.Checked = true;
                }
                else
                {
                    plNewImage.Style.Add("visibility", "visible");
                    rbNewImage.Checked = true;
                    imgNewImage.ImageUrl = response.ImageUrl;
                }
            }
            else
            {
                Log.Error("CompanySelectImageResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }

            CompanySelectEmailTemplateResponse responseEmail = asc.SelectEmailTemplate(adminInfo.UserId, adminInfo.CompanyId);
            if (responseEmail.Success)
            {
                string jsCommand = string.Empty;
                #region Personnel Invitation
                tbPersonnelInvitationEmailTitle.Text = responseEmail.EmailPersonnelInvitationTitle;
                tbPersonnelInvitationEmailTitle.MaxLength = COUNT_MAX_TITLE;
                tbPersonnelInvitationEmailTitle.Attributes.Add("onkeyup", "textBoxCounter('" + tbPersonnelInvitationEmailTitle.ClientID + "', 'lblPersonnelInvitationEmailTitleCount', " + COUNT_MAX_TITLE + ");changeToApplyChangeButton('.btn.white.personnel.invitation');");
                jsCommand += "textBoxCounter('" + tbPersonnelInvitationEmailTitle.ClientID + "', 'lblPersonnelInvitationEmailTitleCount', " + COUNT_MAX_TITLE + ");";

                tbPersonnelInvitationDescription.Text = responseEmail.EmailPersonnelInvitationDescription;
                tbPersonnelInvitationDescription.Attributes.Add("onkeyup", "textBoxCounter('" + tbPersonnelInvitationDescription.ClientID + "', 'lblPersonnelInvitationDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");changeToApplyChangeButton('.btn.white.personnel.invitation');");
                jsCommand += "textBoxCounter('" + tbPersonnelInvitationDescription.ClientID + "', 'lblPersonnelInvitationDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");";

                tbPersonnelInvitationSupportInfo.Text = responseEmail.EmailPersonnelInvitationSupportInfo;
                tbPersonnelInvitationSupportInfo.Attributes.Add("onkeyup", "textBoxCounter('" + tbPersonnelInvitationSupportInfo.ClientID + "', 'lblPersonnelInvitationSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");changeToApplyChangeButton('.btn.white.personnel.invitation');");
                jsCommand += "textBoxCounter('" + tbPersonnelInvitationSupportInfo.ClientID + "', 'lblPersonnelInvitationSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");";
                #endregion

                #region Admin Invitation
                tbAdminInvitationEmailTitle.Text = responseEmail.EmailAdminInvitationTitle;
                tbAdminInvitationEmailTitle.MaxLength = COUNT_MAX_TITLE;
                tbAdminInvitationEmailTitle.Attributes.Add("onkeyup", "textBoxCounter('" + tbAdminInvitationEmailTitle.ClientID + "', 'lblAdminInvitationEmailTitleCount', " + COUNT_MAX_TITLE + ");changeToApplyChangeButton('.btn.white.admin.invitation');");
                jsCommand += "textBoxCounter('" + tbAdminInvitationEmailTitle.ClientID + "', 'lblAdminInvitationEmailTitleCount', " + COUNT_MAX_TITLE + ");";

                tbAdminInvitationDescription.Text = responseEmail.EmailAdminInvitationDescription;
                tbAdminInvitationDescription.Attributes.Add("onkeyup", "textBoxCounter('" + tbAdminInvitationDescription.ClientID + "', 'lblAdminInvitationDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");changeToApplyChangeButton('.btn.white.admin.invitation');");
                jsCommand += "textBoxCounter('" + tbAdminInvitationDescription.ClientID + "', 'lblAdminInvitationDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");";

                tbAdminInvitationSupportInfo.Text = responseEmail.EmailAdminInvitationSupportInfo;
                tbAdminInvitationSupportInfo.Attributes.Add("onkeyup", "textBoxCounter('" + tbAdminInvitationSupportInfo.ClientID + "', 'lblAdminInvitationSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");changeToApplyChangeButton('.btn.white.admin.invitation');");
                jsCommand += "textBoxCounter('" + tbAdminInvitationSupportInfo.ClientID + "', 'lblAdminInvitationSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");";
                #endregion

                #region Forgot Password
                tbForgotPasswordEmailTitle.Text = responseEmail.EmailForgotPasswordTitle;
                tbForgotPasswordEmailTitle.MaxLength = COUNT_MAX_TITLE;
                tbForgotPasswordEmailTitle.Attributes.Add("onkeyup", "textBoxCounter('" + tbForgotPasswordEmailTitle.ClientID + "', 'lblForgotPasswordEmailTitleCount', " + COUNT_MAX_TITLE + ");changeToApplyChangeButton('.btn.white.forgot.password');");
                jsCommand += "textBoxCounter('" + tbForgotPasswordEmailTitle.ClientID + "', 'lblForgotPasswordEmailTitleCount', " + COUNT_MAX_TITLE + ");";

                tbForgotPasswordDescription.Text = responseEmail.EmailForgotPasswordDescription;
                tbForgotPasswordDescription.Attributes.Add("onkeyup", "textBoxCounter('" + tbForgotPasswordDescription.ClientID + "', 'lblForgotPasswordDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");changeToApplyChangeButton('.btn.white.forgot.password');");
                jsCommand += "textBoxCounter('" + tbForgotPasswordDescription.ClientID + "', 'lblForgotPasswordDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");";

                tbForgotPasswordSupportInfo.Text = responseEmail.EmailForgotPasswordSupportInfo;
                tbForgotPasswordSupportInfo.Attributes.Add("onkeyup", "textBoxCounter('" + tbForgotPasswordSupportInfo.ClientID + "', 'lblForgotPasswordSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");changeToApplyChangeButton('.btn.white.forgot.password');");
                jsCommand += "textBoxCounter('" + tbForgotPasswordSupportInfo.ClientID + "', 'lblForgotPasswordSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");";
                #endregion

                #region Reset Password
                tbResetPasswordEmailTitle.Text = responseEmail.EmailResetPasswordTitle;
                tbResetPasswordEmailTitle.MaxLength = COUNT_MAX_TITLE;
                tbResetPasswordEmailTitle.Attributes.Add("onkeyup", "textBoxCounter('" + tbResetPasswordEmailTitle.ClientID + "', 'lblResetPasswordEmailTitleCount', " + COUNT_MAX_TITLE + ");changeToApplyChangeButton('.btn.white.reset.password');");
                jsCommand += "textBoxCounter('" + tbResetPasswordEmailTitle.ClientID + "', 'lblResetPasswordEmailTitleCount', " + COUNT_MAX_TITLE + ");";

                tbResetPasswordDescription.Text = responseEmail.EmailResetPasswordDescription;
                tbResetPasswordDescription.Attributes.Add("onkeyup", "textBoxCounter('" + tbResetPasswordDescription.ClientID + "', 'lblResetPasswordDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");changeToApplyChangeButton('.btn.white.reset.password');");
                jsCommand += "textBoxCounter('" + tbResetPasswordDescription.ClientID + "', 'lblResetPasswordDescriptionCount', " + COUNT_MAX_DESCRIPTION + ");";

                tbResetPasswordSupportInfo.Text = responseEmail.EmailResetPasswordSupportInfo;
                tbResetPasswordSupportInfo.Attributes.Add("onkeyup", "textBoxCounter('" + tbResetPasswordSupportInfo.ClientID + "', 'lblResetPasswordSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");changeToApplyChangeButton('.btn.white.reset.password');");
                jsCommand += "textBoxCounter('" + tbResetPasswordSupportInfo.ClientID + "', 'lblResetPasswordSupportInfoCount', " + COUNT_MAX_SUPPORT_INFO + ");";
                #endregion


                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "textBoxCounter", jsCommand, true);
            }
            else
            {
                Log.Error("CompanySelectEmailTemplateResponse.Success is false. ErrorMessage: " + responseEmail.ErrorMessage);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    GetCompanyPersonalization();
                    imgDefaultImage.ImageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                    ltlCompanyName1.Text = adminInfo.CompanyName;
                    ltlCompanyName2.Text = adminInfo.CompanyName;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                String imageUrl = string.Empty;
                if (rbNewImage.Checked)
                {
                    imageUrl = imgNewImage.ImageUrl;
                }
                else
                {
                    imageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                }
                #endregion

                #region Step 2. Upload new image to S3.
                String imgBase64String = String.Empty, imgFormat = String.Empty;
                String bucketName = "cocadre-" + adminInfo.CompanyId.ToLower();
                String folderName = "company";
                IAmazonS3 s3Client = S3Utility.GetIAmazonS3(adminInfo.CompanyId, adminInfo.UserId);
                S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                using (s3Client)
                {
                    if (rbNewImage.Checked && !String.IsNullOrEmpty(hfNewBase64.Value))
                    {
                        String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                        imgBase64String = ImageUtility.FilterBase64String(hfNewBase64.Value);
                        imgFormat = hfNewExt.Value.Replace("image/", "");
                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", hfNewWidth.Value);
                        metadatas.Add("Height", hfNewHeight.Value);
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = PrefixId.CompanyProfile_SquareLogo + PROCESS_TIMESTAMP + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                        Log.Debug("SquareLogoUrl: " + imageUrl);
                    }
                }
                #endregion

                #region Step 3. Call API
                CompanyUpdateProfileResponse response = asc.UpdateImage(adminInfo.UserId, adminInfo.CompanyId, imageUrl, (int)Company.CompanyImageType.EmailSquare);
                if (response.Success)
                {
                    GetCompanyPersonalization();
                    MessageUtility.ShowToast(this.Page, "Company square logo changes applied.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("CompanyUpdateProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    MessageUtility.ShowToast(this.Page, "Company square logo failed to update.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnPersonnelInvitationApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                if (string.IsNullOrEmpty(tbPersonnelInvitationEmailTitle.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter email title.", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbPersonnelInvitationEmailTitle.Text.Length > COUNT_MAX_TITLE)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the email title should not exceed " + COUNT_MAX_TITLE, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbPersonnelInvitationDescription.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter description", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbPersonnelInvitationDescription.Text.Length > COUNT_MAX_DESCRIPTION)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the description should not exceed " + COUNT_MAX_DESCRIPTION, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbPersonnelInvitationSupportInfo.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter support info", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbPersonnelInvitationSupportInfo.Text.Length > COUNT_MAX_SUPPORT_INFO)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the support info should not exceed " + COUNT_MAX_SUPPORT_INFO, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }
                #endregion

                #region Step 2. Call API
                CompanyUpdateProfileResponse response = asc.UpdateEmailTemplate(adminInfo.UserId, adminInfo.CompanyId, (int)Company.CompanyEmailTemplate.PersonnelInvite, tbPersonnelInvitationEmailTitle.Text.Trim(), tbPersonnelInvitationDescription.Text.Trim(), tbPersonnelInvitationSupportInfo.Text.Trim());
                if (response.Success)
                {
                    GetCompanyPersonalization();
                    MessageUtility.ShowToast(this.Page, "Changes applied.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("CompanyUpdateProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    MessageUtility.ShowToast(this.Page, "Failed to update", MessageUtility.TOAST_TYPE_INFO);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnAdminInvitationApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                if (string.IsNullOrEmpty(tbAdminInvitationEmailTitle.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter email title.", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbAdminInvitationEmailTitle.Text.Length > COUNT_MAX_TITLE)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the email title should not exceed " + COUNT_MAX_TITLE, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbAdminInvitationDescription.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter description", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbAdminInvitationDescription.Text.Length > COUNT_MAX_DESCRIPTION)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the description should not exceed " + COUNT_MAX_DESCRIPTION, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbAdminInvitationSupportInfo.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter support info", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbAdminInvitationSupportInfo.Text.Length > COUNT_MAX_SUPPORT_INFO)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the support info should not exceed " + COUNT_MAX_SUPPORT_INFO, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }
                #endregion

                #region Step 2. Call API
                CompanyUpdateProfileResponse response = asc.UpdateEmailTemplate(adminInfo.UserId, adminInfo.CompanyId, (int)Company.CompanyEmailTemplate.AdminInvite, tbAdminInvitationEmailTitle.Text.Trim(), tbAdminInvitationDescription.Text.Trim(), tbAdminInvitationSupportInfo.Text.Trim());
                if (response.Success)
                {
                    GetCompanyPersonalization();
                    MessageUtility.ShowToast(this.Page, "Changes applied.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("CompanyUpdateProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    MessageUtility.ShowToast(this.Page, "Failed to update", MessageUtility.TOAST_TYPE_INFO);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnForgotPasswordApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                if (string.IsNullOrEmpty( tbForgotPasswordEmailTitle.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter email title.", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbForgotPasswordEmailTitle.Text.Length > COUNT_MAX_TITLE)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the email title should not exceed " + COUNT_MAX_TITLE, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbForgotPasswordDescription.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter description", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbForgotPasswordDescription.Text.Length > COUNT_MAX_DESCRIPTION)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the description should not exceed " + COUNT_MAX_DESCRIPTION, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbForgotPasswordSupportInfo.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter support info", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbForgotPasswordSupportInfo.Text.Length > COUNT_MAX_SUPPORT_INFO)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the support info should not exceed " + COUNT_MAX_SUPPORT_INFO, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }
                #endregion

                #region Step 2. Call API
                CompanyUpdateProfileResponse response = asc.UpdateEmailTemplate(adminInfo.UserId, adminInfo.CompanyId, (int)Company.CompanyEmailTemplate.ForgotPassword, tbForgotPasswordEmailTitle.Text.Trim(), tbForgotPasswordDescription.Text.Trim(), tbForgotPasswordSupportInfo.Text.Trim());
                if (response.Success)
                {
                    GetCompanyPersonalization();
                    MessageUtility.ShowToast(this.Page, "Changes applied.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("CompanyUpdateProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    MessageUtility.ShowToast(this.Page, "Failed to update", MessageUtility.TOAST_TYPE_INFO);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnResetPasswordApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                if (string.IsNullOrEmpty(tbResetPasswordEmailTitle.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter email title.", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbResetPasswordEmailTitle.Text.Length > COUNT_MAX_TITLE)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the email title should not exceed " + COUNT_MAX_TITLE, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbResetPasswordDescription.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter description", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbResetPasswordDescription.Text.Length > COUNT_MAX_DESCRIPTION)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the description should not exceed " + COUNT_MAX_DESCRIPTION, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (string.IsNullOrEmpty(tbResetPasswordSupportInfo.Text))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter support info", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }

                if (tbResetPasswordSupportInfo.Text.Length > COUNT_MAX_SUPPORT_INFO)
                {
                    MessageUtility.ShowToast(this.Page, "The length of the support info should not exceed " + COUNT_MAX_SUPPORT_INFO, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }
                #endregion

                #region Step 2. Call API
                CompanyUpdateProfileResponse response = asc.UpdateEmailTemplate(adminInfo.UserId, adminInfo.CompanyId, (int)Company.CompanyEmailTemplate.ResettedPassword, tbResetPasswordEmailTitle.Text.Trim(), tbResetPasswordDescription.Text.Trim(), tbResetPasswordSupportInfo.Text.Trim());
                if (response.Success)
                {
                    GetCompanyPersonalization();
                    MessageUtility.ShowToast(this.Page, "Changes applied.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("CompanyUpdateProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    MessageUtility.ShowToast(this.Page, "Failed to update", MessageUtility.TOAST_TYPE_INFO);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}