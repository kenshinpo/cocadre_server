﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="AdminWebsite.CompanyProfile.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script>
        function changeToApplyChangeButton() {
            $(".btn.white").removeClass("white").prop("disabled", false).val('Apply Changes').click(ShowProgressBar);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfPrimaryAdminId" runat="server" />
            <div id="popup_reassign" class="popup" style="display: none; width: 100%;">
                <h1 class="popup__title">Re-assign Primary Admin</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                Select the next admin to be the Primary admin account for
                                <asp:Literal ID="ltlReassignCompanyName" runat="server"></asp:Literal>.
                            </p>
                            <div class="main">
                                <div class="form__row">
                                    <p class="label" style="font-weight: bold;">Assign the next Primary admin</p>
                                    <div class="mdl-selectfield">
                                        <asp:DropDownList ID="ddlAdmins" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbReassignAssign" runat="server" Text="Assign" CssClass="popup__action__item popup__action__item--cta" OnClick="lbReassignAssign_Click" />
                    <asp:LinkButton ID="lbReassignCancel" runat="server" Text="Cancel" CssClass="popup__action__item popup__action__item--cancel" />
                </div>
            </div>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_reassign"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Company <span>profile</span></div>
        <div class="appbar__meta">Profile</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Profile</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumAdminPhoto">Admin photo</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumEmail">Email</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumWebsiteHeader">Website header</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumCompanySelection">Login company selection</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumPhoneBanner">Phone main banner</a>
                </li>--%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumAnimationLogo">Refresh animation logo</a>
                </li>
                <%--
                    <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumMatchupBanner">Matchup main banner</a>
                </li>
                --%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumCoverPhoto">Profile cover photo</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">
            <div class="f-accordion__section f-accordion__section--active">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table class="settings-table">
                            <tbody>
                                <tr>
                                    <td class="settings-table__title">Organization
                                    </td>
                                    <td class="settings-table__content">
                                        <asp:TextBox ID="tbCompanyTitle" runat="server" placeholder="Organization Name" MaxLength="40" Style="float: left; width: 250px; margin-right: 100px;" onkeyup="changeToApplyChangeButton();" />
                                        <div>
                                            <asp:Button ID="btnApplyChange" CssClass="btn white" runat="server" Text="Save" Enabled="false" OnClick="btnApplyChange_Click" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="settings-table__title">Contact Information
                                    </td>
                                    <td class="settings-table__content">
                                        <p>
                                            Designate contact email addresses for service communications, payment notifications, and any email subscriptions.
                                        </p>
                                        <div class="label" style="font-weight: bold;">
                                            Primary administrator account
                                        </div>
                                        <p>
                                            <asp:Literal ID="ltlNwePrimaryAdminName" runat="server" />
                                            <asp:Literal ID="ltlOldPrimaryAdminName" runat="server" />
                                            <asp:HiddenField ID="hfNewPrimaryAdminId" runat="server" />
                                        </p>
                                        <asp:Panel ID="plPrimaryAdmin" runat="server">

                                            <asp:LinkButton ID="lbRessignPrimaryAdamin" runat="server" Text="Re-assign Primary admin" OnClick="lbRessignPrimaryAdamin_Click" />

                                            <div class="gray">Please ask the mentioned personnel to assign the rights to another admin if you have decided to delete his/her account</div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="settings-table__title">Support Message
                                    </td>
                                    <td class="settings-table__content">
                                        <p>
                                            The information you want to give your personnel when they need help.
                                                                        <br />
                                            Example:
                                        </p>
                                        <asp:TextBox ID="tbSupportMsg" runat="server" TextMode="MultiLine" placeholder="Please contact admin at +65 12345678" Style="width: 80%; height: 80px; border-width: 1px; border-color: #ddd;" onkeyup="changeToApplyChangeButton();" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="settings-table__title">Time Zone
                                    </td>
                                    <td class="settings-table__content">
                                        <p>
                                            Time zone for
                                                                        <asp:Literal ID="ltlCompanyName2" runat="server" />
                                        </p>
                                        <div class="mdl-selectfield" style="width: 80%;">
                                            <asp:DropDownList ID="ddlTimeZone" runat="server" onchange="changeToApplyChangeButton();" />
                                        </div>
                                    </td>
                                </tr>
                                <%-- <tr>
                                    <td colspan="2">
                                        <div class="popup__action">
                                            <asp:LinkButton ID="lbUpadateProfile" runat="server" Text="APPLY CHANGES" CssClass="popup__action__item popup__action__item--cta" OnClick="lbUpadateProfile_Click" OnClientClick="ShowProgressBar();" />
                                        </div>
                                    </td>
                                </tr>--%>
                            </tbody>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
