﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.CompanyProfile
{
    public partial class CompanyProfile : Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        private const String DEFAULT_ADMIN_PHOTO_URL = "https://s3-ap-southeast-1.amazonaws.com/cocadre/company/admin_photo.jpg";
        private const String DEFAULT_COMPANY_LOGO_URL = "https://s3-ap-southeast-1.amazonaws.com/cocadre/company/company_logo.jpg";
        private const String DEFAULT_PHONE_BANNER_URL = "https://s3-ap-southeast-1.amazonaws.com/cocadre/company/phone_banner.jpg";
        private const String DEFAULT_MATCHUP_BANNER_URL = "https://s3-ap-southeast-1.amazonaws.com/cocadre/company/matchup_banner.jpg";
        private const String DEFAULT_PROFILE_COVER_URL = "https://s3-ap-southeast-1.amazonaws.com/cocadre/company/profile_cover.jpg";

        private void GetCompanyProfile()
        {
            //SelectCompanyProfileResponse response = asc.SelectProfile(adminInfo.UserId, adminInfo.CompanyId);
            //if (response.Success)
            //{
            //    tbCompanyTitle.Text = response.CompanyProfile.Title;
            //    ViewState["CurrentPrimaryAdminName"] = response.CompanyProfile.PrimaryAdminUser.FirstName + " " + response.CompanyProfile.PrimaryAdminUser.LastName;
            //    ltlNwePrimaryAdminName.Text = response.CompanyProfile.PrimaryAdminUser.FirstName + " " + response.CompanyProfile.PrimaryAdminUser.LastName + " (" + response.CompanyProfile.PrimaryAdminUser.Email + ")";
            //    hfPrimaryAdminId.Value = response.CompanyProfile.PrimaryAdminUser.UserId;
            //    hfNewPrimaryAdminId.Value = response.CompanyProfile.PrimaryAdminUser.UserId;
            //    if (response.CompanyProfile.IsPrimaryAdmin)
            //    {

            //        plPrimaryAdmin.Visible = true;
            //    }
            //    else
            //    {
            //        plPrimaryAdmin.Visible = false;
            //    }

            //    tbSupportMsg.Text = response.CompanyProfile.SupportMessage;
            //    ltlCompanyName.Text = response.CompanyProfile.Title;
            //    ltlCompanyName2.Text = response.CompanyProfile.Title;

            //    ddlTimeZone.Items.Clear();
            //    for (int i = 0; i < response.Timezones.Count; i++)
            //    {
            //        String timeZoneCityName = response.Timezones[i].Title.Substring(response.Timezones[i].Title.IndexOf(')') + 2);
            //        ddlTimeZone.Items.Add(new ListItem(response.Timezones[i].Title, timeZoneCityName + "|" + response.Timezones[i].Offset));
            //        if (response.Timezones[i].IsSelected)
            //        {
            //            ddlTimeZone.Items[i].Selected = true;
            //        }
            //    }
            //}
            //else
            //{
            //    Log.Error("SelectCompanyProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            //}
        }

        private void GetCompanyPersonalization()
        {
            //SelectCompanyPersonalizationResponse response = asc.SelectPersonalization(adminInfo.UserId, adminInfo.CompanyId);
            //if (response.Success)
            //{
            //    // Admin Photo
            //    if (response.CompanyPersonalization.AdminImageUrl == null || response.CompanyPersonalization.AdminImageUrl.Equals(DEFAULT_ADMIN_PHOTO_URL))
            //    {
            //        plAdminPhoto.Style.Add("visibility", "hidden");
            //        rbDefaultAdminPhoto.Checked = true;
            //    }
            //    else
            //    {
            //        plAdminPhoto.Style.Add("visibility", "visible");
            //        rbNewAdminPhoto.Checked = true;
            //        imgNewAdminPhoto.ImageUrl = response.CompanyPersonalization.AdminImageUrl;
            //    }

            //    // Company Logo
            //    if (response.CompanyPersonalization.CompanyLogoUrl == null || response.CompanyPersonalization.CompanyLogoUrl.Equals(DEFAULT_COMPANY_LOGO_URL))
            //    {
            //        plCompanyLogo.Style.Add("visibility", "hidden");
            //        rbDefaultCompanyLogo.Checked = true;
            //    }
            //    else
            //    {
            //        plCompanyLogo.Style.Add("visibility", "visible");
            //        rbNewCompanyLogo.Checked = true;
            //        imgNewCompanyLogo.ImageUrl = response.CompanyPersonalization.CompanyLogoUrl;
            //    }

            //    // Phone Banner
            //    if (response.CompanyPersonalization.ClientBannerUrl == null || response.CompanyPersonalization.ClientBannerUrl.Equals(DEFAULT_PHONE_BANNER_URL))
            //    {
            //        plPhoneBanner.Style.Add("visibility", "hidden");
            //        rbDefaultPhoneBanner.Checked = true;
            //    }
            //    else
            //    {
            //        plPhoneBanner.Style.Add("visibility", "visible");
            //        rbNewPhoneBanner.Checked = true;
            //        imgNewPhoneBanner.ImageUrl = response.CompanyPersonalization.ClientBannerUrl;
            //    }

            //    // Matchup Banner
            //    if (response.CompanyPersonalization.MatchupBannerUrl == null || response.CompanyPersonalization.MatchupBannerUrl.Equals(DEFAULT_MATCHUP_BANNER_URL))
            //    {
            //        plMatchupBanner.Style.Add("visibility", "hidden");
            //        rbDefaultMatchupBanner.Checked = true;
            //    }
            //    else
            //    {
            //        plMatchupBanner.Style.Add("visibility", "visible");
            //        rbNewMatchupBanner.Checked = true;
            //        imgNewMatchupBanner.ImageUrl = response.CompanyPersonalization.MatchupBannerUrl;
            //    }

            //    // Profile Cover
            //    if (response.CompanyPersonalization.ProfilePopupBannerUrl == null || response.CompanyPersonalization.ProfilePopupBannerUrl.Equals(DEFAULT_PROFILE_COVER_URL))
            //    {
            //        plProfileCover.Style.Add("visibility", "hidden");
            //        rbDefaultProfileCover.Checked = true;
            //    }
            //    else
            //    {
            //        plProfileCover.Style.Add("visibility", "visible");
            //        rbNewProfileCover.Checked = true;
            //        imgNewProfileCover.ImageUrl = response.CompanyPersonalization.ProfilePopupBannerUrl;
            //    }
            //}
            //else
            //{
            //    Log.Error("SelectCompanyProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            //}
        }

        private void GetAllAdmin()
        {
            try
            {
                UserListResponse response = asc.GetAllUser(adminInfo.UserId, adminInfo.CompanyId, null, CassandraService.Entity.User.AccountType.CODE_ADMIN, CassandraService.Entity.User.AccountStatus.CODE_ACTIVE, null, false, false);
                if (response.Success)
                {
                    ddlAdmins.Items.Clear();
                    for (int i = 0; i < response.Users.Count; i++)
                    {
                        ddlAdmins.Items.Add(new ListItem(response.Users[i].FirstName + " " + response.Users[i].LastName + " (" + response.Users[i].Email + ")", response.Users[i].UserId));
                        if (hfPrimaryAdminId.Value.Equals(response.Users[i].UserId))
                        {
                            ddlAdmins.Items[i].Selected = true;
                        }
                    }
                }
                else
                {
                    Log.Error("UserListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    GetCompanyProfile();
                    GetCompanyPersonalization();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbRessignPrimaryAdamin_Click(object sender, EventArgs e)
        {
            ltlReassignCompanyName.Text = adminInfo.CompanyName;
            GetAllAdmin();
            mpePop.Show();
        }

        protected void lbReassignCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        protected void lbUpadateProfile_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data.
                if (String.IsNullOrEmpty(tbCompanyTitle.Text.Trim()))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter organization.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (String.IsNullOrEmpty(tbSupportMsg.Text.Trim()))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter support message.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                String timeZoneCityName = ddlTimeZone.SelectedItem.Value.Split('|')[0];
                double timeZoneOffset = Convert.ToDouble(ddlTimeZone.SelectedItem.Value.Split('|')[1]);
                //UpdateCompanyProfileResponse response = asc.UpdateProfile(adminInfo.UserId, adminInfo.CompanyId, tbCompanyTitle.Text.Trim(), hfNewPrimaryAdminId.Value, String.Empty, tbSupportMsg.Text, timeZoneCityName, timeZoneOffset);
                //if (response.Success)
                //{
                //    ltlNwePrimaryAdminName.Text = String.Empty;
                //    hfNewPrimaryAdminId.Value = String.Empty;
                //    GetCompanyProfile();

                //    #region Update admin_info session
                //    adminInfo.CompanyName = tbCompanyTitle.Text.Trim();
                //    Session["admin_info"] = adminInfo;
                //    #endregion
                //    MessageUtility.ShowToast(this.Page, "Company profile has been updated.", MessageUtility.TOAST_TYPE_INFO);
                //}
                //else
                //{
                //    String toastMsg;

                //    Log.Error("UpdateCompanyProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);

                //    if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserNoAdmin))
                //    {
                //        toastMsg = "User is not eligible to be an admin.";
                //    }
                //    else
                //    {
                //        toastMsg = "Company profile has not been updated.";
                //    }
                //    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                //}
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbReassignAssign_Click(object sender, EventArgs e)
        {
            try
            {
                ltlNwePrimaryAdminName.Text = ddlAdmins.SelectedItem.Text;
                hfNewPrimaryAdminId.Value = ddlAdmins.SelectedItem.Value;
                if (!adminInfo.PrimaryManagerId.Equals(ddlAdmins.SelectedItem.Value))
                {
                    ltlOldPrimaryAdminName.Text = "<font color ='red'> (Previously " + ViewState["CurrentPrimaryAdminName"].ToString() + ")</font>";
                }
                else
                {
                    ltlOldPrimaryAdminName.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument.Equals("AdminPhoto"))
            {
                rbNewAdminPhoto.Checked = true;
            }
        }

        protected void lbUpadatePersonalization_Click(object sender, EventArgs e)
        {
            try
            {
                String adminPhotoUrl = String.Empty, companyLogoUrl = String.Empty, phoneBannerUrl = String.Empty, matchupBannerUrl = String.Empty, profileCoverUrl = String.Empty;

                #region Step 1. Check data.

                #region Step 1.1 Admin Photo
                if (rbNewAdminPhoto.Checked)
                {
                    //if (hfNewAdminPhotoBase64.Value == String.Empty || hfNewAdminPhotoExt.Value == String.Empty || hfNewAdminPhotoHeight.Value == String.Empty || hfNewAdminPhotoWidth.Value == String.Empty)
                    //{
                    //    MessageUtility.ShowToast(this.Page, "Please select a image for Admin Photo.", MessageUtility.TOAST_TYPE_ERROR);
                    //    return;
                    //}

                    adminPhotoUrl = imgNewAdminPhoto.ImageUrl;
                }
                else
                {
                    adminPhotoUrl = DEFAULT_ADMIN_PHOTO_URL;
                }
                #endregion

                #region Step 1.2 Company Logo
                if (rbNewCompanyLogo.Checked)
                {
                    //if (hfNewCompanyLogoBase64.Value == String.Empty || hfNewCompanyLogoExt.Value == String.Empty || hfNewCompanyLogoHeight.Value == String.Empty || hfNewCompanyLogoWidth.Value == String.Empty)
                    //{
                    //    MessageUtility.ShowToast(this.Page, "Please select a image for Company Logo.", MessageUtility.TOAST_TYPE_ERROR);
                    //    return;
                    //}

                    companyLogoUrl = imgNewCompanyLogo.ImageUrl;
                }
                else
                {
                    companyLogoUrl = DEFAULT_COMPANY_LOGO_URL;
                }
                #endregion

                #region Step 1.3 Phone Banner
                if (rbNewPhoneBanner.Checked)
                {
                    //if (hfNewPhoneBannerBase64.Value == String.Empty || hfNewPhoneBannerExt.Value == String.Empty || hfNewPhoneBannerHeight.Value == String.Empty || hfNewPhoneBannerWidth.Value == String.Empty)
                    //{
                    //    MessageUtility.ShowToast(this.Page, "Please select a image for Phone Banner.", MessageUtility.TOAST_TYPE_ERROR);
                    //    return;
                    //}

                    phoneBannerUrl = imgNewPhoneBanner.ImageUrl;
                }
                else
                {
                    phoneBannerUrl = DEFAULT_PHONE_BANNER_URL;
                }
                #endregion


                #region Step 1.4 Matchup Banner
                if (rbNewMatchupBanner.Checked)
                {
                    //if (hfNewMatchupBannerBase64.Value == String.Empty || hfNewMatchupBannerExt.Value == String.Empty || hfNewMatchupBannerHeight.Value == String.Empty || hfNewMatchupBannerWidth.Value == String.Empty)
                    //{
                    //    MessageUtility.ShowToast(this.Page, "Please select a image for Matchup Banner.", MessageUtility.TOAST_TYPE_ERROR);
                    //    return;
                    //}

                    matchupBannerUrl = imgNewMatchupBanner.ImageUrl;
                }
                else
                {
                    matchupBannerUrl = DEFAULT_MATCHUP_BANNER_URL;
                }
                #endregion

                #region Step 1.5 Profile Cover
                if (rbNewProfileCover.Checked)
                {
                    //if (hfNewProfileCoverBase64.Value == String.Empty || hfNewProfileCoverExt.Value == String.Empty || hfNewProfileCoverHeight.Value == String.Empty || hfNewProfileCoverWidth.Value == String.Empty)
                    //{
                    //    MessageUtility.ShowToast(this.Page, "Please select a image for Profile Cover.", MessageUtility.TOAST_TYPE_ERROR);
                    //    return;
                    //}

                    profileCoverUrl = imgNewProfileCover.ImageUrl;
                }
                else
                {
                    profileCoverUrl = DEFAULT_PROFILE_COVER_URL;
                }
                #endregion

                #endregion

                #region Step 2. Upload new images to S3.
                String imgBase64String = String.Empty, imgFormat = String.Empty;
                String bucketName = "cocadre-" + adminInfo.CompanyId.ToLower();
                String folderName = "company";
                IAmazonS3 s3Client = S3Utility.GetIAmazonS3(adminInfo.CompanyId, adminInfo.UserId);
                S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                using (s3Client)
                {
                    String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                    #region Step 2.1 Admin Photo (/cocadre-{CompanyId}/company/admin_photo_{yyyyMMddHHmmssfff}.png)
                    if (rbNewAdminPhoto.Checked && !String.IsNullOrEmpty(hfNewAdminPhotoBase64.Value))
                    {
                        imgBase64String = ImageUtility.FilterBase64String(hfNewAdminPhotoBase64.Value);
                        imgFormat = hfNewAdminPhotoExt.Value.Replace("image/", "");

                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", hfNewAdminPhotoWidth.Value);
                        metadatas.Add("Height", hfNewAdminPhotoHeight.Value);
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = "admin_photo_" + PROCESS_TIMESTAMP + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        adminPhotoUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                        Log.Debug("adminPhotoUrl: " + adminPhotoUrl);
                    }
                    #endregion

                    #region Step 2.2 Company Logo (/cocadre-{CompanyId}/company/company_logo_{yyyyMMddHHmmssfff}.png)
                    if (rbNewCompanyLogo.Checked && !String.IsNullOrEmpty(hfNewCompanyLogoBase64.Value))
                    {
                        imgBase64String = ImageUtility.FilterBase64String(hfNewCompanyLogoBase64.Value);
                        imgFormat = hfNewCompanyLogoExt.Value.Replace("image/", "");

                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", hfNewCompanyLogoWidth.Value);
                        metadatas.Add("Height", hfNewCompanyLogoHeight.Value);
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = "company_logo_" + PROCESS_TIMESTAMP + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        companyLogoUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                        Log.Debug("companyLogoUrl: " + companyLogoUrl);
                    }
                    #endregion

                    #region Step 2.3 Phone Banner (/cocadre-{CompanyId}/company/phone_banner_{yyyyMMddHHmmssfff}.png)
                    if (rbNewPhoneBanner.Checked && !String.IsNullOrEmpty(hfNewPhoneBannerBase64.Value))
                    {
                        imgBase64String = ImageUtility.FilterBase64String(hfNewPhoneBannerBase64.Value);
                        imgFormat = hfNewPhoneBannerExt.Value.Replace("image/", "");

                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", hfNewPhoneBannerWidth.Value);
                        metadatas.Add("Height", hfNewPhoneBannerHeight.Value);
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = "phone_banner_" + PROCESS_TIMESTAMP + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        phoneBannerUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                        Log.Debug("phoneBannerUrl: " + phoneBannerUrl);
                    }
                    #endregion

                    #region Step 2.4 Matchup Banner (/cocadre-{CompanyId}/company/matchup_banner_{yyyyMMddHHmmssfff}.png)
                    if (rbNewMatchupBanner.Checked && !String.IsNullOrEmpty(hfNewMatchupBannerBase64.Value))
                    {
                        imgBase64String = ImageUtility.FilterBase64String(hfNewMatchupBannerBase64.Value);
                        imgFormat = hfNewMatchupBannerExt.Value.Replace("image/", "");

                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", hfNewMatchupBannerWidth.Value);
                        metadatas.Add("Height", hfNewMatchupBannerHeight.Value);
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = "matchup_banner_" + PROCESS_TIMESTAMP + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        matchupBannerUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                        Log.Debug("matchupBannerUrl: " + matchupBannerUrl);
                    }
                    #endregion

                    #region Step 2.5 Profile Cover (/cocadre-{CompanyId}/company/profile_cover_{yyyyMMddHHmmssfff}.png)
                    if (rbNewProfileCover.Checked && !String.IsNullOrEmpty(hfNewProfileCoverBase64.Value))
                    {
                        imgBase64String = ImageUtility.FilterBase64String(hfNewProfileCoverBase64.Value);
                        imgFormat = hfNewProfileCoverExt.Value.Replace("image/", "");

                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", hfNewProfileCoverWidth.Value);
                        metadatas.Add("Height", hfNewProfileCoverHeight.Value);
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = "profile_cover_" + PROCESS_TIMESTAMP + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        profileCoverUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                        Log.Debug("profileCoverUrl: " + profileCoverUrl);
                    }
                    #endregion
                }
                #endregion

                #region Step 3. Call API
                //UpdateCompanyPersonalizationResponse response = asc.UpdatePersonalization(adminInfo.UserId, adminInfo.CompanyId, adminPhotoUrl, companyLogoUrl, phoneBannerUrl, matchupBannerUrl, profileCoverUrl);
                //if (response.Success)
                //{
                //    GetCompanyPersonalization();
                //    MessageUtility.ShowToast(this.Page, "Company personalization has been updated.", MessageUtility.TOAST_TYPE_INFO);
                //}
                //else
                //{
                //    Log.Error("UpdateCompanyPersonalizationResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                //    MessageUtility.ShowToast(this.Page, "Company personalization has not been updated.", MessageUtility.TOAST_TYPE_ERROR);
                //}
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}