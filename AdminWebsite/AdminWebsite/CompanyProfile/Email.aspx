﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Email.aspx.cs" Inherits="AdminWebsite.CompanyProfile.Email" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script>
        function PreviewImage(fileInput, maxWidth, maxHeight, maxSize) {
            var imgNewImage = document.getElementById('<%=imgNewImage.ClientID%>');
            var hfNewBase64 = document.getElementById('<%=hfNewBase64.ClientID%>');
            var hfNewExt = document.getElementById('<%=hfNewExt.ClientID%>');
            var hfNewWidth = document.getElementById('<%=hfNewWidth.ClientID%>');
            var hfNewHeight = document.getElementById('<%=hfNewHeight.ClientID%>');
            var plNewImage = document.getElementById('<%=plNewImage.ClientID%>');
            var rbNewImage = document.getElementById('<%=rbNewImage.ClientID%>');

            if (fileInput.files && fileInput.files[0]) {
                var type = fileInput.files[0].type;
                var size = fileInput.files[0].size / (1024 * 1024);
                if (type.toLowerCase() == "image/png" || type.toLowerCase() == "image/jpeg" || type.toLowerCase() == "image/jpg") {
                    if (size > maxSize) {
                        toastr.error('Uploaded photo exceeded ' + maxSize + 'MB.');
                    }
                    else {
                        var fileReader = new FileReader();
                        var image = new Image();
                        fileReader.onload = function (e) {
                            image.src = e.target.result;
                            image.onload = function () {
                                if ((this.width != maxWidth || this.height != maxHeight)) {
                                    toastr.error('Photo should be ' + maxWidth + ' x ' + maxHeight + '.');
                                }
                                else {
                                    hfNewBase64.setAttribute('value', this.src);
                                    hfNewExt.setAttribute('value', type);
                                    hfNewWidth.setAttribute('value', this.width);
                                    hfNewHeight.setAttribute('value', this.height);
                                    imgNewImage.src = this.src;
                                    rbNewImage.checked = true;
                                    plNewImage.style.visibility = 'visible';
                                    fileInput.value = "";

                                    changeToApplyChangeButton('.btn.white.apply.change');
                                }
                            };
                        }
                        fileReader.readAsDataURL(fileInput.files[0]);
                    }
                }
                else {
                    toastr.error('File extension is invalid.');
                }
            }
        }

        function changeToApplyChangeButton(className) {
            $(className).removeClass("white").prop("disabled", false).val('Apply Changes').click(ShowProgressBar);
        }

        function textBoxCounter(textBoxId, labelId, maxCount) {
            var textbox = document.getElementById(textBoxId);
            var label = document.getElementById(labelId);
            var countLength = maxCount - textbox.value.length;
            if (countLength < 0) {
                textbox.style.borderColor = "red";
            }
            else {
                textbox.style.borderColor = "";
            }
            label.innerHTML = countLength;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField ID="hfNewBase64" runat="server" />
    <asp:HiddenField ID="hfNewExt" runat="server" />
    <asp:HiddenField ID="hfNewWidth" runat="server" />
    <asp:HiddenField ID="hfNewHeight" runat="server" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Company <span>profile</span></div>
        <div class="appbar__meta">Email</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/Profile">Profile</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AdminPhoto">Admin photo</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Email</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/WebsiteHeader">Website header</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/CompanyBanner">Login company selection</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/PhoneBanner">Phone main banner</a>
                </li>--%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AnimationLogo">Refresh animation logo</a>
                </li>
                <%--
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/MatchupBanner">Matchup main banner</a>
                </li>
                --%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/CoverPhoto">Profile cover photo</a>
                </li>

            </ul>
        </aside>

        <div class="data__content" style="padding-left: 40px;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="profile-section">
                        <div>
                            <div class="profile-header">Company square logo for email</div>
                            <p>
                                Your custom logo will be displayed for mentioned services at
                            <asp:Literal ID="ltlCompanyName1" runat="server" />.<br />
                                The image file should be in PNG or JPG format. Do not include any copyrighted materials.
                            </p>
                            <p>
                                Uploading a new logo will replace your current one across all products and for all users at
                            <asp:Literal ID="ltlCompanyName2" runat="server" />.
                            </p>
                        </div>
                        <div style="float: right; margin-left: 20px;">
                            <asp:Button ID="btnApplyChange" CssClass="btn white apply change" runat="server" Text="Save" Enabled="false" OnClick="btnApplyChange_Click" />
                        </div>
                    </div>
                    <div>
                        <div class="selection">
                            <label>
                                <asp:RadioButton CssClass="profile-radiobutton" ID="rbDefaultImage" GroupName="Image" runat="server" Text="Default image" onclick="changeToApplyChangeButton('.btn.white.apply.change');" />
                                <asp:Image CssClass="profile-radiobutton-image" ID="imgDefaultImage" runat="server" />
                            </label>
                        </div>
                        <asp:Panel ID="plNewImage" runat="server" CssClass="selection">
                            <label>
                                <asp:RadioButton CssClass="profile-radiobutton" ID="rbNewImage" GroupName="Image" runat="server" Text="Uploaded image" onclick="changeToApplyChangeButton('.btn.white.apply.change');" />
                                <asp:Image CssClass="profile-radiobutton-image" ID="imgNewImage" runat="server" />
                            </label>
                        </asp:Panel>
                        <p class="profile-margin">Image should be 150 x 150 and no bigger than 1MB</p>

                        <label for="fuNewImage" style="cursor: pointer; margin-left: 25px;" class="btn uppercase">
                            <input type="file" id="fuNewImage" accept="image/*" title="Add image" style="display: none" onchange="PreviewImage(this, 150, 150, 1);" />
                            <i class="fa fa-plus"></i>Upload Image
                        </label>
                    </div>
                    <hr style="color: rgba(203, 203, 203, 1); height: 5px;" />
                    <div>
                        <div style="display: inline-flex; margin-bottom: 30px;">
                            <div class="profile-header">
                                New Personnel Invitation
                            </div>
                            <div style="float: right; margin-left: 20px;">
                                <asp:Button ID="btnPersonnelInvitationApplyChange" CssClass="btn white personnel invitation" runat="server" Text="Save" Enabled="false" OnClick="btnPersonnelInvitationApplyChange_Click" />
                            </div>
                        </div>
                        <div class="profile-email-section">
                            Email Title
                            <div>
                                <asp:TextBox ID="tbPersonnelInvitationEmailTitle" runat="server" CssClass="profile-detail" onkeydown="return (event.keyCode!=13);" />
                                <span class="profile-lettercount" id="lblPersonnelInvitationEmailTitleCount">30</span>
                            </div>
                        </div>
                        <div style="display: inline-flex;">
                            <div class="profile-email-section" style="float: left; margin-right: 50px;">
                                Description
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbPersonnelInvitationDescription" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblPersonnelInvitationDescriptionCount">200</span>
                                </div>
                            </div>
                            <div class="profile-email-section">
                                Support info
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbPersonnelInvitationSupportInfo" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblPersonnelInvitationSupportInfoCount">200</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear-float"></div>
                    </div>
                    <hr style="color: rgba(203, 203, 203, 1); height: 5px;" />
                    <div>
                        <div style="display: inline-flex; margin-bottom: 30px;">
                            <div class="profile-header">
                                Admin Invitation
                            </div>
                            <div style="float: right; margin-left: 20px;">
                                <asp:Button ID="btnAdminInvitationApplyChange" CssClass="btn white admin invitation" runat="server" Text="Save" Enabled="false" OnClick="btnAdminInvitationApplyChange_Click" />
                            </div>
                        </div>
                        <div class="profile-email-section">
                            Email Title
                            <div>
                                <asp:TextBox ID="tbAdminInvitationEmailTitle" runat="server" CssClass="profile-detail" onkeydown="return (event.keyCode!=13);" />
                                <span class="profile-lettercount" id="lblAdminInvitationEmailTitleCount">30</span>
                            </div>
                        </div>
                        <div style="display: inline-flex;">
                            <div class="profile-email-section" style="float: left; margin-right: 50px;">
                                Description
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbAdminInvitationDescription" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblAdminInvitationDescriptionCount">200</span>
                                </div>
                            </div>
                            <div class="profile-email-section">
                                Support info
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbAdminInvitationSupportInfo" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblAdminInvitationSupportInfoCount">200</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear-float"></div>
                    </div>
                    <hr style="color: rgba(203, 203, 203, 1); height: 5px;" />
                    <div>
                        <div style="display: inline-flex; margin-bottom: 30px;">
                            <div class="profile-header">
                                Forgot Password
                            </div>
                            <div style="float: right; margin-left: 20px;">
                                <asp:Button ID="btnForgotPasswordApplyChange" CssClass="btn white forgot password" runat="server" Text="Save" Enabled="false" OnClick="btnForgotPasswordApplyChange_Click" />
                            </div>
                        </div>
                        <div class="profile-email-section">
                            Email Title
                            <div>
                                <asp:TextBox ID="tbForgotPasswordEmailTitle" runat="server" CssClass="profile-detail" onkeydown="return (event.keyCode!=13);" />
                                <span class="profile-lettercount" id="lblForgotPasswordEmailTitleCount">30</span>
                            </div>
                        </div>
                        <div style="display: inline-flex;">
                            <div class="profile-email-section" style="float: left; margin-right: 50px;">
                                Description
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbForgotPasswordDescription" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblForgotPasswordDescriptionCount">200</span>
                                </div>
                            </div>
                            <div class="profile-email-section">
                                Support info
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbForgotPasswordSupportInfo" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblForgotPasswordSupportInfoCount">200</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear-float"></div>
                    </div>
                    <hr style="color: rgba(203, 203, 203, 1); height: 5px;" />
                    <div>
                        <div style="display: inline-flex; margin-bottom: 30px;">
                            <div class="profile-header">
                                Reset Password
                            </div>
                            <div style="float: right; margin-left: 20px;">
                                <asp:Button ID="btnResetPasswordApplyChange" CssClass="btn white reset password" runat="server" Text="Save" Enabled="false" OnClick="btnResetPasswordApplyChange_Click" />
                            </div>
                        </div>
                        <div class="profile-email-section">
                            Email Title
                            <div>
                                <asp:TextBox ID="tbResetPasswordEmailTitle" runat="server" CssClass="profile-detail" onkeydown="return (event.keyCode!=13);" />
                                <span class="profile-lettercount" id="lblResetPasswordEmailTitleCount">30</span>
                            </div>
                        </div>
                        <div style="display: inline-flex;">
                            <div class="profile-email-section" style="float: left; margin-right: 50px;">
                                Description
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbResetPasswordDescription" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblResetPasswordDescriptionCount">200</span>
                                </div>
                            </div>
                            <div class="profile-email-section">
                                Support info
                                <div class="email-textbox">
                                    <asp:TextBox ID="tbResetPasswordSupportInfo" TextMode="MultiLine" runat="server" CssClass="profile-tb-detail" />
                                    <span class="profile-tb-lettercount" id="lblResetPasswordSupportInfoCount">200</span>
                                </div>
                            </div>
                        </div>
                        <div class="clear-float"></div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
