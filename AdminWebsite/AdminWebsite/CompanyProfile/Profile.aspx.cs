﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.CompanyProfile
{
    public partial class Profile : System.Web.UI.Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        private void GetCompanyProfile()
        {
            CompanySelectProfileResponse response = asc.SelectProfile(adminInfo.UserId, adminInfo.CompanyId);
            if (response.Success)
            {
                tbCompanyTitle.Text = response.CompanyProfile.Title;
                ViewState["CurrentPrimaryAdminName"] = response.CompanyProfile.PrimaryAdminUser.FirstName + " " + response.CompanyProfile.PrimaryAdminUser.LastName;
                ltlNwePrimaryAdminName.Text = response.CompanyProfile.PrimaryAdminUser.FirstName + " " + response.CompanyProfile.PrimaryAdminUser.LastName + " (" + response.CompanyProfile.PrimaryAdminUser.Email + ")";
                hfPrimaryAdminId.Value = response.CompanyProfile.PrimaryAdminUser.UserId;
                hfNewPrimaryAdminId.Value = response.CompanyProfile.PrimaryAdminUser.UserId;
                if (response.CompanyProfile.IsPrimaryAdmin)
                {

                    plPrimaryAdmin.Visible = true;
                }
                else
                {
                    plPrimaryAdmin.Visible = false;
                }

                tbSupportMsg.Text = response.CompanyProfile.SupportMessage;
                ltlCompanyName2.Text = response.CompanyProfile.Title;

                ddlTimeZone.Items.Clear();
                for (int i = 0; i < response.Timezones.Count; i++)
                {
                    String timeZoneCityName = response.Timezones[i].Title.Substring(response.Timezones[i].Title.IndexOf(')') + 2);
                    ddlTimeZone.Items.Add(new ListItem(response.Timezones[i].Title, timeZoneCityName + "|" + response.Timezones[i].Offset));
                    if (response.Timezones[i].IsSelected)
                    {
                        ddlTimeZone.Items[i].Selected = true;
                    }
                }
            }
            else
            {
                Log.Error("SelectCompanyProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        private void GetAllAdmin()
        {
            try
            {
                UserListResponse response = asc.GetAllUser(adminInfo.UserId, adminInfo.CompanyId, null, CassandraService.Entity.User.AccountType.CODE_ADMIN, CassandraService.Entity.User.AccountStatus.CODE_ACTIVE, null, false, false);
                if (response.Success)
                {
                    ddlAdmins.Items.Clear();
                    for (int i = 0; i < response.Users.Count; i++)
                    {
                        ddlAdmins.Items.Add(new ListItem(response.Users[i].FirstName + " " + response.Users[i].LastName + " (" + response.Users[i].Email + ")", response.Users[i].UserId));
                        if (hfPrimaryAdminId.Value.Equals(response.Users[i].UserId))
                        {
                            ddlAdmins.Items[i].Selected = true;
                        }
                    }
                }
                else
                {
                    Log.Error("UserListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    GetCompanyProfile();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbRessignPrimaryAdamin_Click(object sender, EventArgs e)
        {
            ltlReassignCompanyName.Text = adminInfo.CompanyName;
            GetAllAdmin();
            mpePop.Show();
        }

        protected void lbReassignAssign_Click(object sender, EventArgs e)
        {
            try
            {
                ltlNwePrimaryAdminName.Text = ddlAdmins.SelectedItem.Text;
                hfNewPrimaryAdminId.Value = ddlAdmins.SelectedItem.Value;
                if (!adminInfo.PrimaryManagerId.Equals(ddlAdmins.SelectedItem.Value))
                {
                    ltlOldPrimaryAdminName.Text = "<font color ='red'> (Previously " + ViewState["CurrentPrimaryAdminName"].ToString() + ")</font>";
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "changeToApplyChangeButton", "changeToApplyChangeButton();", true);
                }
                else
                {
                    ltlOldPrimaryAdminName.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnApplyChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data.
                if (String.IsNullOrEmpty(tbCompanyTitle.Text.Trim()))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter organization.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (String.IsNullOrEmpty(tbSupportMsg.Text.Trim()))
                {
                    MessageUtility.ShowToast(this.Page, "Please enter support message.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                String timeZoneCityName = ddlTimeZone.SelectedItem.Value.Split('|')[0];
                double timeZoneOffset = Convert.ToDouble(ddlTimeZone.SelectedItem.Value.Split('|')[1]);
                CompanyUpdateProfileResponse response = asc.UpdateProfile(adminInfo.UserId, adminInfo.CompanyId, tbCompanyTitle.Text.Trim(), hfNewPrimaryAdminId.Value, String.Empty, tbSupportMsg.Text, timeZoneCityName, timeZoneOffset);
                if (response.Success)
                {
                    ltlNwePrimaryAdminName.Text = String.Empty;
                    ltlOldPrimaryAdminName.Text = String.Empty;
                    hfNewPrimaryAdminId.Value = String.Empty;
                    GetCompanyProfile();

                    #region Update admin_info session
                    adminInfo.CompanyName = tbCompanyTitle.Text.Trim();
                    Session["admin_info"] = adminInfo;
                    #endregion
                    MessageUtility.ShowToast(this.Page, "Company profile has been updated.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    String toastMsg;

                    Log.Error("UpdateCompanyProfileResponse.Success is false. ErrorMessage: " + response.ErrorMessage);

                    if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserNoAdmin))
                    {
                        toastMsg = "User is not eligible to be an admin.";
                    }
                    else
                    {
                        toastMsg = "Company profile has not been updated.";
                    }
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}