﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AlbumAnimationLogo.aspx.cs" Inherits="AdminWebsite.CompanyProfile.AlbumAnimationLogo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <style>
        .hidden-menu {
            position: absolute;
            right: -115px;
            z-index: 20;
            top: 20px;
            display: none;
            width: 150px;
            height: 80px;
        }

            .hidden-menu ul {
                background: #ccc;
                color: #000;
                height: 80px;
                display: block;
            }

                .hidden-menu ul li {
                    padding: 10px 20px;
                    display: block;
                    height: 40px;
                    border: 1px solid #333;
                }

                    .hidden-menu ul li:last-child {
                        border-top: 0px;
                    }

        .editBtn {
            display: none;
        }

        .popup .editBtn {
            cursor: pointer;
            display: block;
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="../JS/cropbox.js"></script>
    <script>
        var COMPANY_IMAGE_TYPE = 6;
        var IMAGE_MAX_SIZE = 5; // unit: MB
        var DEFAULT_IMAGE_URL = "https://s3-ap-southeast-1.amazonaws.com/cocadre/company/pull_refresh.png";
        var FETCH_DATA_COUNT = 999999;
        var IMAGE_WIDTH = 273;
        var IMAGE_HEIGHT = 56;
        var imageBase64, imageExtension;
        var showImageId;

        $(function () {
            popupCropImage();
            ShowProgressBar();
            getImages(null);
        });

        function popupCropImage() {
            var options =
            {
                thumbBox: '.crop-imagebox__imageBox__thumbBox',
                spinner: '.crop-imagebox__imageBox__spinner',
                imgSrc: 'avatar.png'
            }
            var cropper;

            $('#ifAddImage').on('change', function () {
                if (this.files && this.files[0]) {
                    var type = this.files[0].type;
                    imageExtension = type;
                    var size = this.files[0].size / (1024 * 1024);

                    if (type.toLowerCase() == "image/png" || type.toLowerCase() == "image/jpeg" || type.toLowerCase() == "image/jpg") {
                        if (size > IMAGE_MAX_SIZE) {
                            ShowToast('Uploaded image exceeded ' + IMAGE_MAX_SIZE + 'MB.', 2);
                        }
                        else {
                            var thumbBox = $('.crop-imagebox__imageBox__thumbBox');
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                options.imgSrc = e.target.result;
                                cropper = $('.crop-imagebox__imageBox').cropbox(options);
                            }
                            reader.readAsDataURL(this.files[0]);
                            showPopWindow('popup_crop_image');
                        }
                    }
                    else {
                        ShowToast('File extension is invalid.', 2);
                    }
                }
            });

            $('#btnCrop').on('click', function () {
                imageBase64 = cropper.getBase64ForCompanyImage(IMAGE_WIDTH, IMAGE_HEIGHT, 0.5);
                ShowProgressBar();
                uploadImage();
            })

            $('#btnZoomIn').on('click', function () {
                cropper.zoomIn();
            })

            $('#btnZoomOut').on('click', function () {
                cropper.zoomOut();
            })

            $('#btnCancelCrop').on('click', function () {
                $('#ifAddImage').val('');
                hidePopWindow('popup_crop_image');
            })
        };

        function uploadImage() {
            $.ajax({
                type: "POST",
                url: '/Company/UploadCompanyImage',
                data: {
                    "CompanyId": CompanyId,
                    "ManagerId": ManagerId,
                    "PresentImageId": "",
                    "CompanyImageType": COMPANY_IMAGE_TYPE,
                    "ImageBase64": imageBase64,
                    "ImageExtension": "jpg",
                    "ImageWidth": IMAGE_WIDTH * 3,
                    "ImageHeight": IMAGE_HEIGHT * 3
                },
                crossDomain: true,
                dataType: 'json',
                success: function (res) {
                    HideProgressBar();

                    if (res.Success) {
                        ShowToast('Photo has been updated', 1);
                        $('#ifAddImage').val('');
                        $('#popup_masker').css('display', 'none');
                        $('#popup_crop_image').css('display', 'none');
                        getImages(null);
                    }
                    else {
                        ShowToast('Uploading failed, please check your connection', 2);
                    }
                }
            });
        };

        function getImages(timestamp) {
            $.ajax({
                type: "POST",
                url: '/Company/GetCompanyImages',
                data: {
                    "CompanyId": CompanyId,
                    "ManagerId": ManagerId,
                    "CompanyImageType": COMPANY_IMAGE_TYPE,
                    "FetchCount": FETCH_DATA_COUNT,
                    "LastFetchTimestamp": timestamp,
                    "IsNextPage": true
                },
                crossDomain: true,
                dataType: 'json',
                success: function (res) {
                    if (res.Success) {
                        var CompanyImages = res.CompanyImages;
                        var html = "";
                        var inUseImageUrl = "";
                        if (CompanyImages != null && CompanyImages.length > 0) {
                            inUseImageUrl = CompanyImages[0].Url.replace("_original", "_original");
                            for (var i = 0; i < CompanyImages.length; i++) {
                                if (i == 0) {
                                    html += "<div class=\"imageBox\" data-imageId=\"" + CompanyImages[i].CompanyImageId + "\" data-url=\"" + CompanyImages[i].Url.replace("_original", "_original") + "\"  data-timestamp=\"" + CompanyImages[i].CreatedOnTimestamp + "\" style=\"width: 150px; position:relative; cursor: pointer; height: 150px; float: left; margin: 5px; background-image: url('" + CompanyImages[i].Url.replace("_original", "_original") + "'); background-position-x: center; background-position-y: center; background-repeat: no-repeat; border: 2px #3CDE10 solid; \">  ";
                                    html += "<div style=\"border-radius: 6px; width: 85px; text-align: center; margin-top: 115px; margin-left: 5px; background-color: rgba(139, 197, 63, 1);\"><span style=\"padding: 3px; color: rgb(255, 255, 255); font-size: 1.2em;\"> ✓ In use</span></div>";
                                }
                                else {
                                    html += "<div class=\"imageBox\" data-imageId=\"" + CompanyImages[i].CompanyImageId + "\" data-url=\"" + CompanyImages[i].Url.replace("_original", "_original") + "\"  data-timestamp=\"" + CompanyImages[i].CreatedOnTimestamp + "\" style=\"width: 150px; position:relative; cursor: pointer;  height: 150px; float: left; margin: 5px; background-image: url('" + CompanyImages[i].Url.replace("_original", "_original") + "'); background-position-x: center; background-position-y: center; background-repeat: no-repeat; border: 2px #CCC solid;\">  ";
                                }
                                html += "<div class=\"hidden-menu\"><ul style=\"background:#ccc; color:#fff; text-align:center;\"><li class=\"setImage\" data-imageid='" + CompanyImages[i].CompanyImageId + "' >Set Profile Image</li><li class=\"delImage\" data-imageid='" + CompanyImages[i].CompanyImageId + "' >Delete Image</li></ul></div>";

                                html += "   <div class=\"editBtn\" style=\"border-radius: 99em; width: 30px; height: 30px; text-align: center; background-color: rgb(204, 204, 204);  position:absolute; right: 10px; top: 10px; z-index: 3;\"> ";
                                html += "       <span style=\"color: rgba(127, 127, 127, 1); font-weight: 900;\">...</span> ";
                                html += "   </div>";
                                html += "</div>";
                            }
                        }
                        else {
                            inUseImageUrl = DEFAULT_IMAGE_URL;

                            html += "<div class=\"defaultImageBox\" style=\"width: 150px; cursor: pointer; position:relative; height: 150px; float: left; margin: 5px; background-image: url('" + DEFAULT_IMAGE_URL + "'); background-position-x: center; background-position-y: center; border: 2px #3CDE10 solid; background-repeat: no-repeat; \"> ";
                            html += "<div style=\"border-radius: 6px; width: 85px; text-align: center; margin-top: 115px; margin-left: 5px; background-color: rgba(139, 197, 63, 1);\"><span style=\"padding: 3px; color: rgb(255, 255, 255); font-size: 1.2em;\"> ✓ In use</span></div>";
                            html += "</div>";
                        }

                        $('#imgPreivew').attr('src', inUseImageUrl);
                        $('#divContent').html(html);
                    }
                    else {
                        ShowToast('Photo is not available', 2);
                    }
                    HideProgressBar();
                }
            });
        }

        function setInUseImage(imageId) {
            ShowProgressBar();
            $.ajax({
                type: "POST",
                url: '/Company/SetInUseCompanyImage',
                data: {
                    "CompanyId": CompanyId,
                    "ManagerId": ManagerId,
                    "CompanyImageId": imageId,
                    "CompanyImageType": COMPANY_IMAGE_TYPE
                },
                crossDomain: true,
                dataType: 'json',
                success: function (res) {
                    if (res.Success) {
                        getImages(null);
                        ShowToast('Photo have been updated', 1);
                    }
                    else {
                        ShowToast('Failed, please check your connection', 2);
                    }
                    HideProgressBar();
                }
            });
        }

        function delImage(imageId) {
            ShowProgressBar();
            $.ajax({
                type: "POST",
                url: '/Company/DeleteCompanyImage',
                data: {
                    "CompanyId": CompanyId,
                    "ManagerId": ManagerId,
                    "CompanyImageId": imageId,
                    "CompanyImageType": COMPANY_IMAGE_TYPE
                },
                crossDomain: true,
                dataType: 'json',
                success: function (res) {
                    if (res.Success) {
                        getImages(null);
                        ShowToast('Selected photo has been deleted', 1);
                    }
                    else {
                        ShowToast('Failed to delete photo, please check again', 2);
                    }

                    getImages(null);
                    $('#popup_delete_image_masker').css('display', 'none');
                    $('#popup_delete_image').css('display', 'none');
                    HideProgressBar();
                }
            });
        }

        function nextImage(imgId) {
            showImageId = imgId;
            showImageViewer();
        }

        function prveImage(imgId) {
            showImageId = imgId;
            showImageViewer();
        }

        function showImageViewer() {
            if (showImageId == null || showImageId == "") // default image
            {
                $('#aPrevImg').removeAttr("onclick");
                $('#aPrevImg').css("display", "none");
                $('#imgViewerInUse').css("display", "block");

                $('#aNextImg').removeAttr("onclick");
                $('#aNextImg').css("display", "none");

                $('.hidden-menu').css('display', 'none');
                $('.editBtn').css('display', 'none');
                $('.editBtn').css('visibility', 'hidden');
                $('#imgViewer').attr("src", DEFAULT_IMAGE_URL);
                $('#popupProgress').hide();
                showPopWindow('popup_image_viewer');
            }
            else // User upload images
            {
                $('#popupProgress').show();
                $.ajax({
                    type: "POST",
                    url: '/Company/GetCompanyImageDetail',
                    data: {
                        "CompanyId": CompanyId,
                        "ManagerId": ManagerId,
                        "CompanyImageType": COMPANY_IMAGE_TYPE,
                        "ImageId": showImageId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        if (res.Success) {

                            if (res.PreviousImage == null)// Prev img
                            {
                                $('#aPrevImg').removeAttr("onclick");
                                $('#aPrevImg').css("display", "none");
                                $('#imgViewerInUse').css("display", "block");
                            }
                            else {
                                $('#aPrevImg').attr("onclick", "prveImage('" + res.PreviousImage.CompanyImageId + "')");
                                $('#aPrevImg').css("display", "inline");
                                $('#imgViewerInUse').css("display", "none");
                            }

                            if (res.NextImage == null)// Next img
                            {
                                $('#aNextImg').removeAttr("onclick");
                                $('#aNextImg').css("display", "none");
                            }
                            else {
                                $('#aNextImg').attr("onclick", "prveImage('" + res.NextImage.CompanyImageId + "')");
                                $('#aNextImg').css("display", "inline");
                            }

                            $('.setImage.imgviewer').data('imageid', res.CurrentImage.CompanyImageId);
                            $('.delImage.imgviewer').data('imageid', res.CurrentImage.CompanyImageId);

                            $('.hidden-menu').css('display', 'none');
                            $('.editBtn').css('display', 'block');
                            $('.editBtn').css('visibility', '');
                            $('#imgViewer').attr("src", res.CurrentImage.Url);
                            $(this).find('.hidden-menu').css('display', 'none');
                            showPopWindow('popup_image_viewer');
                        }
                        else {
                            ShowToast('Photo is not available', 2);
                        }
                        $('#popupProgress').hide();
                    }
                });
            }
        }

        function showPopWindow(eleId) {
            $('#popup_masker').css('display', 'inline-block');
            $('#' + eleId).css('display', 'block');
        }

        function hidePopWindow(eleId) {
            $('#popup_masker').css('display', 'none');
            $('#' + eleId).css('display', 'none');
        }

        function showPreview() {
            $('#divPreview').css('display', 'block');
        }

        function hidePreview() {
            $('#divPreview').css('display', 'none');
        }

        $(function () {
            $(document).on('click', '.imageBox', function (e) {
                e.preventDefault();
                e.stopPropagation();
                showImageId = $(this).data('imageid');
                showImageViewer();
            });

            $(document).on('mouseover', '.imageBox', function (e) {
                $(this).find('.editBtn').css('display', 'block');
            });

            $(document).on('mouseout', '.imageBox', function (e) {
                if (!$(this).find('.editBtn').hasClass('active')) {
                    $(this).find('.editBtn').css('display', 'none');
                }
            });

            $(document).on('click', '.defaultImageBox', function (e) {
                e.preventDefault();
                e.stopPropagation();
                showImageId = null;
                showImageViewer();
            });

            $(document).on('click', '.editBtn', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $('.hidden-menu, .editBtn').css('display', 'none');
                $('.editBtn').removeClass('active');
                $(this).css('display', 'block');
                $(this).addClass('active');
                $(this).parent().find('.hidden-menu').css('display', 'block');
            });

            $(window).on('click', function () {
                $('.hidden-menu, .editBtn').css('display', 'none');
                $('.editBtn').removeClass('active');
                $('.popup .editBtn').css('display', 'block');
            });

            $(document).on('click', '.setImage', function (e) {
                e.preventDefault();
                e.stopPropagation();
                setInUseImage($(this).data('imageid'));
                hidePopWindow('popup_image_viewer');
            })

            $(document).on('click', '.delImage', function (e) {
                e.preventDefault();
                e.stopPropagation();
                $('#btnConfirmDelete').data("imageid", $(this).data('imageid'));
                $('#popup_delete_image_masker').css('display', 'inline');
                $('#popup_delete_image').css('display', 'inline');
            })

            $('#btnCancelDelete').on('click', function () {
                $('#ifAddImage').val('');
                $('#popup_delete_image_masker').css('display', 'none');
                $('#popup_delete_image').css('display', 'none');
            })

            $('#btnConfirmDelete').on('click', function () {
                $('#ifAddImage').val('');
                delImage($(this).data('imageid'));
                hidePopWindow('popup_image_viewer');
            })
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <div id="popup_masker" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none; overflow: hidden;"></div>
    <!-- Crop Image -->
    <div id="popup_crop_image" class="popup" style="left: 50%; top: 50%; height: auto; overflow: auto; position: absolute; z-index: 9001; margin: 0 auto; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: none;">
        <h1 class="popup__title">Crop image</h1>
        <div class="popup__content">
            <div class="container" style="width: 1150px;">
                <div class="photocrop">
                    <div class="crop-imagebox__imageBox" style="width: 1094px; height: 226px;">
                        <div class="crop-imagebox__imageBox__thumbBox" style="background-size: cover; width: 548px; height: 114px; margin: 0 auto; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%);"></div>
                        <div class="crop-imagebox__imageBox__spinner" style="display: none">Loading...</div>
                    </div>
                    <div class="crop-imagebox__action" style="width: auto;">
                        <input type="button" id="btnZoomIn" value="+" style="margin: 2px; float: right;" />
                        <input type="button" id="btnZoomOut" value="-" style="margin: 2px; float: right;" />
                    </div>
                    <div class="cropped">
                    </div>
                </div>
            </div>
            <div class="label">
            </div>
            <p class="error">
            </p>
        </div>
        <div class="popup__action">
            <div id="btnCrop" class="popup__action__item popup__action__item--cta">CROP & SAVE</div>
            <div id="btnCancelCrop" class="popup__action__item popup__action__item--others">Cancel</div>
        </div>
    </div>
    <!-- / Crop Image -->

    <!-- Image Viewer -->
    <div id="popup_image_viewer" class="popup" style="display: none; position: absolute; height: auto; left: 50%; top: 50%; margin: 0 auto; background: #000; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: none; width: 75%; height: 75%">
        <div id="popupProgress" style="text-align: center; z-index: 15; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%);">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
            <div id="ProgressText" style="font-size: 15px; color: #1B3563;"></div>
        </div>
        <div style="top: 20px; right: 20px; font-size: 1.5em; font-weight: bold; position: absolute; cursor: pointer; z-index: 3;" onclick="hidePopWindow('popup_image_viewer');">
            <span style="color: white; cursor: pointer;"><i class="fa fa-close"></i></span>
        </div>
        <a id="aPrevImg" style="color: #fff; cursor: pointer; position: absolute; left: 25px; top: 50%;" onclick="prveImage();"><i class="fa fa-2x fa-chevron-left"></i></a>
        <a id="aNextImg" style="color: #fff; cursor: pointer; position: absolute; right: 25px; top: 50%;" onclick="nextImage();"><i class="fa fa-2x fa-chevron-right"></i></a>
        <div class="img-wrapper" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%);">
            <div style="margin: 0 auto; position: relative; width: 100%;">
                <div class="hidden-menu">
                    <ul style="background: #ccc; color: #fff; text-align: center;">
                        <li class="setImage imgviewer" style="cursor: pointer;">Set Profile Image</li>
                        <li class="delImage imgviewer" style="cursor: pointer;">Delete Image</li>
                    </ul>
                </div>
                <div class="editBtn" style="border-radius: 99em; width: 30px; height: 30px; text-align: center; background-color: rgb(204, 204, 204); position: absolute; right: 10px; top: 10px; z-index: 3;">
                    <span style="color: rgba(127, 127, 127, 1); font-weight: 900;">...</span>
                </div>
                <img id="imgViewer" style="height: auto; background: white; width: 100%;" />
                <div id="imgViewerInUse" style="border-radius: 6px; left: 20px; width: 85px; text-align: center; bottom: 20px; display: block; position: absolute; z-index: 3; background-color: rgba(139, 197, 63, 1);"><span style="padding: 3px; color: rgb(255, 255, 255); font-size: 1.2em;">✓ In use</span></div>
            </div>
        </div>
    </div>
    <!-- / Image Viewer -->

    <!-- Confirm Delete -->
    <div id="popup_delete_image_masker" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none; z-index: 15000 !important;"></div>
    <div id="popup_delete_image" class="popup" style="left: 50%; top: 50%; width: 400px; height: 165px; overflow: auto; position: absolute; z-index: 15001 !important; max-width: none; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%);">
        <h1 class="popup__title">Delete Image</h1>
        <div class="popup__content" style="min-height: auto;">
            <span>Are you sure you want to delete this photo?</span>
        </div>
        <div class="popup__action">
            <div id="btnConfirmDelete" class="popup__action__item popup__action__item--cta">Confirm</div>
            <div id="btnCancelDelete" class="popup__action__item popup__action__item--others" style="float: right;">Cancel</div>
        </div>
    </div>
    <!-- / Confirm Delete -->

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Company <span>profile</span></div>
        <div class="appbar__meta">Refresh animation logo</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/Profile">Profile</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumAdminPhoto">Admin photo</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumEmail">Email</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumWebsiteHeader">Website header</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumCompanySelection">Login company selection</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumPhoneBanner">Phone main banner</a>
                </li>--%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Refresh animation logo</a>
                </li>
                <%--
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumMatchupBanner">Matchup main banner</a>
                </li>
                --%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/CompanyProfile/AlbumCoverPhoto">Profile cover photo</a>
                </li>
            </ul>
        </aside>

        <div class="data__content" style="padding-left: 40px;">
            <div style="margin-top: 30px; margin-bottom: 50px;">
                <label style="font-weight: 700; font-size: 1.2em;">Refresh animation logo <span style="cursor: pointer; color: rgba(51, 144, 254, 1);" onmouseover="showPreview();" onmouseout="hidePreview();">(Preview)</span></label>
                <p>
                    Your custom logo will be displayed for mentioned services.
                    <br />
                    The image file should be in PNG or JPG format. Do not include any copyrighted materials.
                </p>
                <p>
                    Uploading a new image will replace your current one across all products and for all users.
                </p>
            </div>

            <div id="divPreview" style="left: 610px; top: 200px; width: 266px; height: 460px; position: absolute; z-index: 5; background-image: url('/img/company_image_animation_logo_preview.png'); background-size: contain; display: none; box-shadow: 4px 4px 12px -2px rgba(51,51,102,0.5);">
                <img id="imgPreivew" style="left: 60px; top: 76px; width: 138px; position: absolute;" />
            </div>

            <div style="width: 650px">
                <div style="width: 150px; position: relative; cursor: pointer; height: 150px; border: 2px dashed #CCC; float: left; margin: 5px; align-self: center; display: flex; display: -moz-box; display: -ms-flexbox; justify-content: center; align-items: center;">
                    <label for="ifAddImage" style="cursor: pointer; width: 100%; height: 100%; display: flex; display: -moz-box; display: -ms-flexbox; justify-content: center; align-items: center;">
                        <span style="color: #CCC;">UPLOAD IMAGE</span>
                        <input type="file" id="ifAddImage" accept="image/png, image/jpeg" title="UPLOAD IMAGE" style="display: none" />
                    </label>
                </div>
                <div id="divContent">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
