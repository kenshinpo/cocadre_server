﻿using AdminWebsite.App_Code.Entity;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Data;

namespace AdminWebsite
{
    public partial class SAnalytics : System.Web.UI.Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }

            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                ltlCompanyId.Text = adminInfo.CompanyId;
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }

        }

        protected void btnLastLogin_Click(object sender, EventArgs e)
        {
            try
            {
                UserDetailResponse response = asc.GetUserLastLoginDateTime(adminInfo.CompanyId, tbUserId.Text);
                if (response.Success)
                {
                    if (response.User != null)
                    {
                        ltlLastLogin.Text = response.User.LastLoginDateTime.ToString("yyyy/MM/dd HH:mm:ss");
                    }
                    else
                    {
                        ltlLastLogin.Text = "無此 User Login 資料";
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnMauSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime startDateTime = new DateTime(Convert.ToInt16(tbStartDateYY.Text), Convert.ToInt16(tbStartDateMM.Text), Convert.ToInt16(tbStartDateDD.Text), Convert.ToInt16(tbStartDateHH.Text), Convert.ToInt16(tbStartDateMMM.Text), Convert.ToInt16(tbStartDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                DateTime endDateTime = new DateTime(Convert.ToInt16(tbEndDateYY.Text), Convert.ToInt16(tbEndDateMM.Text), Convert.ToInt16(tbEndDateDD.Text), Convert.ToInt16(tbEndDateHH.Text), Convert.ToInt16(tbEndDateMMM.Text), Convert.ToInt16(tbEndDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                AnalyticMonthlyActiveUserResponse response = asc.SelectMau(adminInfo.UserId, adminInfo.CompanyId, startDateTime, endDateTime);
                if (response.Success)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Month", typeof(string));
                    dt.Columns.Add("Number of active users", typeof(int));
                    dt.Columns.Add("Total time spent (Hours)", typeof(double));
                    dt.Columns.Add("Avg time spend per hour (Hours)", typeof(double));

                    foreach (CassandraService.Entity.AnalyticUserActivity.MAUReport report in response.Reports)
                    {
                        dt.Rows.Add(report.Month, report.TotalUserCount, report.TotalTimeInHour, report.AvgTimeInHourPerUser);
                    }

                    gvMauTableView.DataSource = dt;
                    gvMauTableView.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnMatchupSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                AnalyticMatchUpBreakdownPersonnelReport response = asc.SelectMatchupTopPersonnelOverview(adminInfo.UserId, adminInfo.CompanyId);
                if (response.Success)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("UserId", typeof(string));
                    dt.Columns.Add("User", typeof(string));
                    dt.Columns.Add("Topic", typeof(string));
                    dt.Columns.Add("Category", typeof(string));
                    dt.Columns.Add("Total played times", typeof(int));

                    foreach (AnalyticQuiz.MatchUpPersonnelBreakdownReport report in response.Reports)
                    {
                        string userName = string.Format("{0} {1}", report.User.FirstName, report.User.LastName);
                        if (report.User.Status.Code != CassandraService.Entity.User.AccountStatus.CODE_ACTIVE)
                        {
                            userName += " (" + report.User.Status.Title + ")";
                        }

                        dt.Rows.Add(report.User.UserId, userName, report.Topic.TopicTitle, report.Topic.TopicCategory.Title, report.PlayedTimes);
                    }

                    gvMatchupTop.DataSource = dt;
                    gvMatchupTop.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnMatchupHide_Click(object sender, EventArgs e)
        {
            gvMatchupTop.DataSource = new DataTable();
            gvMatchupTop.DataBind();
        }

        protected void btnTopPersonnelSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                AnalyticsSelectFullLeaderboardByCompanyResponse response = asc.SelectFullLeaderboardByCompany(adminInfo.UserId, adminInfo.CompanyId);
                if (response.Success)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Rank", typeof(int));
                    dt.Columns.Add("UserId", typeof(string));
                    dt.Columns.Add("User", typeof(string));
                    dt.Columns.Add("Number of question answered correctly", typeof(int));

                    int rank = 1;
                    foreach (AnalyticQuiz.TopPersonnelScore report in response.ScoreChart)
                    {
                        string userName = string.Format("{0} {1}", report.User.FirstName, report.User.LastName);
                        if (report.User.Status.Code != CassandraService.Entity.User.AccountStatus.CODE_ACTIVE)
                        {
                            userName += " (" + report.User.Status.Title + ")";
                        }

                        dt.Rows.Add(rank++, report.User.UserId, userName, report.NumberOfQuestionAnsweredCorrectly);
                    }

                    gvTopPersonnel.DataSource = dt;
                    gvTopPersonnel.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnTopPersonnelHide_Click(object sender, EventArgs e)
        {
            gvTopPersonnel.DataSource = new DataTable();
            gvTopPersonnel.DataBind();
        }

        protected void btnMatchGroupSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                AnalyticMatchUpGroupPersonnelReport response = asc.SelectMatchUpGroupPersonnelReport(adminInfo.UserId, adminInfo.CompanyId);
                if (response.Success)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("UserId", typeof(string));
                    dt.Columns.Add("User", typeof(string));
                    dt.Columns.Add("Total played times", typeof(int));

                    foreach (AnalyticQuiz.MatchUpPersonnelGroupByReport report in response.Reports)
                    {
                        string userName = string.Format("{0} {1}", report.User.FirstName, report.User.LastName);
                        if(report.User.Status.Code != CassandraService.Entity.User.AccountStatus.CODE_ACTIVE)
                        {
                            userName += " (" + report.User.Status.Title + ")";
                        }
                        dt.Rows.Add(report.User.UserId, userName, report.PlayedTimes);
                    }

                    gvMatchGroup.DataSource = dt;
                    gvMatchGroup.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnMatchGroupHide_Click(object sender, EventArgs e)
        {
            gvMatchGroup.DataSource = new DataTable();
            gvMatchGroup.DataBind();
        }

        protected void btnLoginSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                gvLoginTableView.DataSource = null;

                DateTime startDateTime = new DateTime(Convert.ToInt16(tbLoginStartDateYY.Text), Convert.ToInt16(tbLoginStartDateMM.Text), Convert.ToInt16(tbLoginStartDateDD.Text), Convert.ToInt16(tbLoginStartDateHH.Text), Convert.ToInt16(tbLoginStartDateMMM.Text), Convert.ToInt16(tbLoginStartDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                DateTime endDateTime = new DateTime(Convert.ToInt16(tbLoginEndDateYY.Text), Convert.ToInt16(tbLoginEndDateMM.Text), Convert.ToInt16(tbLoginEndDateDD.Text), Convert.ToInt16(tbLoginEndDateHH.Text), Convert.ToInt16(tbLoginEndDateMMM.Text), Convert.ToInt16(tbLoginEndDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                AnalyticLoginDetailResponse response = asc.SelectLoginDetail(adminInfo.UserId, adminInfo.CompanyId, startDateTime, endDateTime);
                if (response.Success)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Count", typeof(int));
                    dt.Columns.Add("User", typeof(string));
                    dt.Columns.Add("Department", typeof(string));
                    dt.Columns.Add("Email", typeof(string));
                    dt.Columns.Add("Is Login", typeof(bool));
                    dt.Columns.Add("Last Login Date", typeof(string));

                    int index = 1;
                    foreach (CassandraService.Entity.AnalyticDau.LoginDetail report in response.LoginDetails)
                    {
                        string userName = string.Format("{0} {1}", report.User.FirstName, report.User.LastName);
                        if (report.User.Status.Code != CassandraService.Entity.User.AccountStatus.CODE_ACTIVE)
                        {
                            userName += " (" + report.User.Status.Title + ")";
                        }
                        dt.Rows.Add(index, userName, report.User.Departments[0].Title, report.User.Email, report.IsLogin, report.LastLoginDateString);
                        index++;
                    }

                    gvLoginTableView.DataSource = dt;
                    gvLoginTableView.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnActivitySubmit_Click(object sender, EventArgs e)
        {
            try
            {
                gvActivityTableView.DataSource = null;

                DateTime startDateTime = new DateTime(Convert.ToInt16(tbActivityStartDateYY.Text), Convert.ToInt16(tbActivityStartDateMM.Text), Convert.ToInt16(tbActivityStartDateDD.Text), Convert.ToInt16(tbActivityStartDateHH.Text), Convert.ToInt16(tbActivityStartDateMMM.Text), Convert.ToInt16(tbActivityStartDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                DateTime endDateTime = new DateTime(Convert.ToInt16(tbActivityEndDateYY.Text), Convert.ToInt16(tbActivityEndDateMM.Text), Convert.ToInt16(tbActivityEndDateDD.Text), Convert.ToInt16(tbActivityEndDateHH.Text), Convert.ToInt16(tbActivityEndDateMMM.Text), Convert.ToInt16(tbActivityEndDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                AnalyticActivityResponse response = asc.GetActivity(adminInfo.UserId, adminInfo.CompanyId, startDateTime, endDateTime);
                if (response.Success)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Count", typeof(int));
                    dt.Columns.Add("User", typeof(string));
                    dt.Columns.Add("Department", typeof(string));
                    dt.Columns.Add("Email", typeof(string));
                    dt.Columns.Add("Total Time Spend in Hour", typeof(double));

                    int index = 1;
                    foreach (CassandraService.Entity.AnalyticUserActivity.ActivityReport report in response.Reports)
                    {
                        string userName = string.Format("{0} {1}", report.User.FirstName, report.User.LastName);
                        if (report.User.Status.Code != CassandraService.Entity.User.AccountStatus.CODE_ACTIVE)
                        {
                            userName += " (" + report.User.Status.Title + ")";
                        }
                        dt.Rows.Add(index, userName, report.User.Departments[0].Title, report.User.Email, report.TotalTimeInHour);
                        index++;
                    }

                    gvActivityTableView.DataSource = dt;
                    gvActivityTableView.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }

        protected void btnActivityBreakdownSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                gvActivityBreakdownTableView.DataSource = null;

                DateTime startDateTime = new DateTime(Convert.ToInt16(tbActivityBreakdownStartDateYY.Text), Convert.ToInt16(tbActivityBreakdownStartDateMM.Text), Convert.ToInt16(tbActivityBreakdownStartDateDD.Text), Convert.ToInt16(tbActivityBreakdownStartDateHH.Text), Convert.ToInt16(tbActivityBreakdownStartDateMMM.Text), Convert.ToInt16(tbActivityBreakdownStartDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                DateTime endDateTime = new DateTime(Convert.ToInt16(tbActivityBreakdownEndDateYY.Text), Convert.ToInt16(tbActivityBreakdownEndDateMM.Text), Convert.ToInt16(tbActivityBreakdownEndDateDD.Text), Convert.ToInt16(tbActivityBreakdownEndDateHH.Text), Convert.ToInt16(tbActivityBreakdownEndDateMMM.Text), Convert.ToInt16(tbActivityBreakdownEndDateSS.Text), DateTimeKind.Utc).AddHours(-adminInfo.TimeZone);
                AnalyticActivityResponse response = asc.GetActivity(adminInfo.UserId, adminInfo.CompanyId, startDateTime, endDateTime);
                if (response.Success)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Count", typeof(int));
                    dt.Columns.Add("User", typeof(string));
                    dt.Columns.Add("Department", typeof(string));
                    dt.Columns.Add("Email", typeof(string));
                    dt.Columns.Add("Start Time", typeof(string));
                    dt.Columns.Add("End Time", typeof(string));
                    dt.Columns.Add("Total Time Spent in Second", typeof(double));
                    dt.Columns.Add("Total Time Spent in Hour", typeof(double));

                    int index = 1;
                    foreach (CassandraService.Entity.AnalyticUserActivity.ActivityBreakdownReport report in response.BreakdownReports)
                    {
                        string userName = string.Format("{0} {1}", report.User.FirstName, report.User.LastName);
                        if (report.User.Status.Code != CassandraService.Entity.User.AccountStatus.CODE_ACTIVE)
                        {
                            userName += " (" + report.User.Status.Title + ")";
                        }
                        dt.Rows.Add(index, userName, report.User.Departments[0].Title, report.User.Email, report.StartTimestamp, report.EndTimestamp, report.TimeInSeconds, report.TimeInHour);
                        index++;
                    }

                    gvActivityBreakdownTableView.DataSource = dt;
                    gvActivityBreakdownTableView.DataBind();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
        }
    }
}