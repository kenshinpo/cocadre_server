﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SAnalytics.aspx.cs" Inherits="AdminWebsite.SAnalytics" MasterPageFile="~/Navi.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <style type="text/css">
        .main {
            height: auto !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            for (i = 1; i <= 8; i++) {
                $("#panel-" + i).slideUp();
            }
        });

        function toggle(value, counts) {
            $(document).ready(function () {
                //for expand clicked row
                $("#panel-" + value).slideToggle();
                for (i = 1; i <= counts; i++) {
                    if (i != value) {
                        // for all other to hide
                        $("#panel-" + i).slideUp();
                    }
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server" style="height: auto !important;">
    <div style="margin: 10px;">
        <div>
            <div style="width: 100px; display: inline-block;">Company Id:</div>
            <asp:Literal ID="ltlCompanyId" runat="server" /><br />
            <%--            <div style="width: 100px; display: inline-block;">Count:</div>
            <asp:Literal ID="ltlCount" runat="server" />--%>
        </div>
        <hr />
        <div>
            <div onclick="toggle(1, 8)">
                <h3>Matchup Breakdown by User</h3>
            </div>
            <div id="panel-1">
                <div>
                    <asp:GridView ID="gvMatchupTop" runat="server"></asp:GridView>
                </div>
                <div>
                    <asp:Button ID="btnMatchupSubmit" runat="server" Text="Go" OnClick="btnMatchupSubmit_Click" />
                </div>
            </div>
        </div>
        <hr />
        <div>
            <div onclick="toggle(2, 8)">
                <h3>Matchup Group By User</h3>
            </div>
            <div id="panel-2">
                <div>
                    <asp:GridView ID="gvMatchGroup" runat="server"></asp:GridView>
                </div>
                <div>
                    <asp:Button ID="btnMatchGroupSubmit" runat="server" Text="Go" OnClick="btnMatchGroupSubmit_Click" />
                </div>
            </div>
        </div>
        <hr />
        <div>
            <div onclick="toggle(3, 8)">
                <h3>Top Personnel Score (Number of question answered correctly)</h3>
            </div>
            <div id="panel-3">
                <div>
                    <asp:GridView ID="gvTopPersonnel" runat="server"></asp:GridView>
                </div>
                <div>
                    <asp:Button ID="btnTopPersonnelSubmit" runat="server" Text="Go" OnClick="btnTopPersonnelSubmit_Click" />
                </div>
            </div>
        </div>
        <hr />
        <div>
            <div onclick="toggle(4, 8)">
                <h3>Monthly Active Users Listing</h3>
            </div>
            <div id="panel-4">
                <div style="width: 100px; display: inline-block;">Start Date:</div>
                <asp:TextBox ID="tbStartDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbStartDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbStartDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="01" />
                <asp:TextBox ID="tbStartDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbStartDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbStartDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="00" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <div style="width: 100px; display: inline-block;">End Date:</div>
                <asp:TextBox ID="tbEndDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbEndDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbEndDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="30" />
                <asp:TextBox ID="tbEndDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="23" />:<asp:TextBox ID="tbEndDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="59" />:<asp:TextBox ID="tbEndDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="59" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <asp:Button ID="btnMauSubmit" runat="server" Text="Go" OnClick="btnMauSubmit_Click" /><br />

                <div>
                    <asp:GridView ID="gvMauTableView" runat="server"></asp:GridView>
                </div>
            </div>
        </div>
        <hr />
        <div>
            <div onclick="toggle(5, 8)">
                <h3>Login Users Listing</h3>
            </div>
            <div id="panel-5">
                <div style="width: 100px; display: inline-block;">Start Date:</div>
                <asp:TextBox ID="tbLoginStartDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbLoginStartDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbLoginStartDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="01" />
                <asp:TextBox ID="tbLoginStartDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbLoginStartDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbLoginStartDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="00" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <div style="width: 100px; display: inline-block;">End Date:</div>
                <asp:TextBox ID="tbLoginEndDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbLoginEndDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbLoginEndDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="30" />
                <asp:TextBox ID="tbLoginEndDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="23" />:<asp:TextBox ID="tbLoginEndDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="59" />:<asp:TextBox ID="tbLoginEndDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="59" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <asp:Button ID="btnLoginSubmit" runat="server" Text="Go" OnClick="btnLoginSubmit_Click" /><br />

                <div>
                    <asp:GridView ID="gvLoginTableView" runat="server"></asp:GridView>
                </div>
            </div>
        </div>
        <hr />
        <div>
            <div onclick="toggle(6, 8)">
                <h3>Activity Listing by User</h3>
            </div>
            <div id="panel-6">
                <div style="width: 100px; display: inline-block;">Start Date:</div>
                <asp:TextBox ID="tbActivityStartDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbActivityStartDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbActivityStartDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="01" />
                <asp:TextBox ID="tbActivityStartDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbActivityStartDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbActivityStartDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="00" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <div style="width: 100px; display: inline-block;">End Date:</div>
                <asp:TextBox ID="tbActivityEndDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbActivityEndDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbActivityEndDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="30" />
                <asp:TextBox ID="tbActivityEndDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="23" />:<asp:TextBox ID="tbActivityEndDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="59" />:<asp:TextBox ID="tbActivityEndDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="59" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <asp:Button ID="btnActivitySubmit" runat="server" Text="Go" OnClick="btnActivitySubmit_Click" /><br />

                <div>
                    <asp:GridView ID="gvActivityTableView" runat="server"></asp:GridView>
                </div>
            </div>
        </div>
        <hr />
        <div>
            <div onclick="toggle(7, 8)">
                <h3>Activity Breakdown Listing by User</h3>
            </div>
            <div id="panel-7">
                <div style="width: 100px; display: inline-block;">Start Date:</div>
                <asp:TextBox ID="tbActivityBreakdownStartDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbActivityBreakdownStartDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbActivityBreakdownStartDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="01" />
                <asp:TextBox ID="tbActivityBreakdownStartDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbActivityBreakdownStartDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="00" />:<asp:TextBox ID="tbActivityBreakdownStartDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="00" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <div style="width: 100px; display: inline-block;">End Date:</div>
                <asp:TextBox ID="tbActivityBreakdownEndDateYY" runat="server" Style="width: 100px; display: inline-block;" Text="2016" />/<asp:TextBox ID="tbActivityBreakdownEndDateMM" runat="server" Style="width: 100px; display: inline-block;" Text="06" />/<asp:TextBox ID="tbActivityBreakdownEndDateDD" runat="server" Style="width: 100px; display: inline-block;" Text="30" />
                <asp:TextBox ID="tbActivityBreakdownEndDateHH" runat="server" Style="width: 100px; display: inline-block;" Text="23" />:<asp:TextBox ID="tbActivityBreakdownEndDateMMM" runat="server" Style="width: 100px; display: inline-block;" Text="59" />:<asp:TextBox ID="tbActivityBreakdownEndDateSS" runat="server" Style="width: 100px; display: inline-block;" Text="59" />
                (yyyy/MM/dd HH:mm:ss)<br />
                <asp:Button ID="btnActivityBreakdownSubmit" runat="server" Text="Go" OnClick="btnActivityBreakdownSubmit_Click" /><br />

                <div>
                    <asp:GridView ID="gvActivityBreakdownTableView" runat="server"></asp:GridView>
                </div>
            </div>
        </div>
        <hr />
        <div>
            <div onclick="toggle(8, 8)">
                <h3>User Last Login Date Time</h3>
            </div>
            <div id="panel-8">
                <div style="width: 100px; display: inline-block;">User Id:</div>
                <asp:TextBox ID="tbUserId" runat="server" Style="width: 100px; display: inline-block;" />
                <br />
                <asp:Literal ID="ltlLastLogin" runat="server" />
                <div>
                    <asp:Button ID="btnLastLogin" runat="server" Text="Go" OnClick="btnLastLogin_Click" />
                </div>
            </div>
        </div>
        <hr />
    </div>
</asp:Content>
