﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
namespace AdminWebsite
{
    public partial class index : Page
    {
        private static ILog Log = LogManager.GetLogger("AdminWebsiteLog");

        private AdminService asc = new AdminService();

        private List<Dictionary<String, String>> accountInfo = new List<Dictionary<String, String>>();

        private List<CassandraService.Entity.Module> listModule = null;

        protected void Page_Load(object sender, EventArgs e)
        {

            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");

            if (!IsPostBack)
            {
                if (this.User.Identity.IsAuthenticated)
                {
                    FormsAuthentication.RedirectFromLoginPage(username.Text.Trim().ToLower(), false);
                }
                else
                {
                    string appPath = HttpContext.Current.Request.Url.Authority;
                    if (appPath.Contains("localhost"))
                    {
                        username.Text = "apo@aporigin.com";
                        pwd.Text = "123456";
                    }
                }
            }
        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            if (IsValid)
            {
                try
                {
                    LoginByManagerResponse response = asc.LoginByManager(username.Text.Trim().ToLower(), pwd.Text.Trim());

                    if (response.Success)
                    {
                        // ZX: Issue authentication first
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, username.Text.Trim().ToLower(), DateTime.Now, DateTime.Now.AddMinutes(30), true, string.Empty);
                        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
                        Response.Cookies.Add(authCookie);

                        ViewState["CompanyList"] = response.Companies;
                        ManagerInfo managerInfo = new ManagerInfo();

                        if (response.Admin != null) // SuperAdmin or CocadreAdmin
                        {
                            if (response.Admin.NeedResetPassword)
                            {  
                                managerInfo.CompanyId = response.Admin.Company.CompanyId;
                                managerInfo.UserId = response.Admin.UserId;
                                managerInfo.Email = response.Admin.Email;
                                managerInfo.IsLogined = true;
                                Session["admin_info"] = managerInfo;
                                Response.Redirect("/ResetPassword", false);
                            }
                            else
                            {
                                login_pnl.SetActiveView(viewSystemManager);
                                lvCompaniesForSystem.DataSource = response.Companies;
                                lvCompaniesForSystem.DataBind();

                                listModule = response.Admin.AccessSystemModules;
                                rtModule.DataSource = response.Admin.AccessSystemModules;
                                rtModule.DataBind();
                                
                                managerInfo.CompanyId = response.Admin.Company.CompanyId;
                                managerInfo.CompanyLogoUrl = response.Admin.Company.CompanyLogoUrl;
                                managerInfo.CompnayWebsiteHeader = response.Admin.Company.CompanyBannerUrl;
                                managerInfo.CompanyName = response.Admin.Company.CompanyTitle;
                                managerInfo.Email = response.Admin.Company.Manager.Email;
                                managerInfo.FirstName = response.Admin.Company.Manager.FirstName;
                                managerInfo.LastName = response.Admin.Company.Manager.LastName;
                                managerInfo.ProfileImageUrl = response.Admin.Company.Manager.ProfileImageUrl;
                                managerInfo.TimeZone = response.Admin.Company.TimeZoneOffset;
                                managerInfo.AccountType = response.Admin.Company.Manager.Type;
                                managerInfo.UserId = response.Admin.Company.Manager.UserId;
                                managerInfo.AccessModules = response.Admin.Company.Manager.AccessModules;
                                managerInfo.AccessSystemModules = response.Admin.Company.Manager.AccessSystemModules;
                                managerInfo.CompanyAdminImageUrl = response.Admin.Company.AdminImageUrl;
                                managerInfo.PrimaryManagerId = response.Admin.Company.PrimaryManagerId;
                                managerInfo.Companies = response.Companies;
                                managerInfo.IsLogined = true;
                                Session["admin_info"] = managerInfo;
                            }
                        }
                        else // CompanyAdmin or CompanyModerater
                        {
                            login_pnl.SetActiveView(viewCompanyManager);
                            lvCompanies.DataSource = response.Companies;
                            lvCompanies.DataBind();

                            // 2017/03/20 - A-Po
                            // For Chrome issue. 
                            // Chrome 在 lvCompanies_ItemDataBound 後會觸發 Navi.Master.Page_Init。導致 session 為 null，系統 logout，無法 login。
                            // Fix: 在 lvCompanies_ItemDataBound 之前，先給系統非 null 的 session。
                            managerInfo.IsLogined = false;
                            Session["admin_info"] = managerInfo;
                        }
                    }
                    else
                    {
                        cv.EnableClientScript = false;
                        cv.IsValid = false;
                        cv.ErrorMessage = "Invalid Login ID / Password!";
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString());
                    cv.EnableClientScript = false;
                    cv.IsValid = false;
                    cv.ErrorMessage = "Error connecting to server!";
                }
            }
        }

        protected void lvCompanies_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (ViewState["CompanyList"] != null)
                {
                    List<Company> companyList = ViewState["CompanyList"] as List<Company>;
                    if (e.Item.ItemType == ListViewItemType.DataItem)
                    {
                        LinkButton lbCompany = e.Item.FindControl("lbCompany") as LinkButton;
                        System.Web.UI.WebControls.Image imgCompany = e.Item.FindControl("imgCompany") as System.Web.UI.WebControls.Image;
                        Label lblCompanyName = e.Item.FindControl("lblCompanyName") as Label;
                        lbCompany.CommandArgument = companyList[e.Item.DataItemIndex].CompanyId;
                        lblCompanyName.Text = companyList[e.Item.DataItemIndex].CompanyTitle;
                        imgCompany.ImageUrl = companyList[e.Item.DataItemIndex].CompanyLogoUrl;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvCompanies_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Logout.aspx", true);
            }

            try
            {
                if (ViewState["CompanyList"] != null)
                {
                    List<Company> companyList = ViewState["CompanyList"] as List<Company>;
                    if (e.CommandName.Equals("Click"))
                    {
                        Company company = companyList.Where(c => c.CompanyId == Convert.ToString(e.CommandArgument)).FirstOrDefault();
                        if (company != null)
                        {
                            if (company.Manager.NeedResetPassword)
                            {
                                ManagerInfo managerInfo = new ManagerInfo();
                                managerInfo.CompanyId = company.CompanyId;
                                managerInfo.UserId = company.Manager.UserId;
                                managerInfo.Email = company.Manager.Email;
                                managerInfo.AccountType = company.Manager.Type;
                                managerInfo.IsLogined = true;
                                Session["admin_info"] = managerInfo;
                                Response.Redirect("/ResetPassword", false);
                            }
                            else
                            {
                                String role = String.Empty;
                                role = company.Manager.Type.Title;
                                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, company.Manager.Email, DateTime.Now, DateTime.Now.AddMinutes(30), true, role);
                                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
                                Response.Cookies.Add(authCookie);
                                try
                                {
                                    ManagerInfo managerInfo = new ManagerInfo();
                                    managerInfo.CompanyId = company.CompanyId;
                                    managerInfo.CompanyLogoUrl = company.CompanyLogoUrl;
                                    managerInfo.CompnayWebsiteHeader = company.CompanyBannerUrl;
                                    managerInfo.CompanyName = company.CompanyTitle;
                                    managerInfo.Email = company.Manager.Email;
                                    managerInfo.FirstName = company.Manager.FirstName;
                                    managerInfo.LastName = company.Manager.LastName;
                                    managerInfo.ProfileImageUrl = company.Manager.ProfileImageUrl;
                                    managerInfo.TimeZone = company.TimeZoneOffset;
                                    managerInfo.AccountType = company.Manager.Type;
                                    managerInfo.UserId = company.Manager.UserId;
                                    managerInfo.AccessModules = company.Manager.AccessModules;
                                    managerInfo.AccessSystemModules = company.Manager.AccessSystemModules;
                                    managerInfo.CompanyAdminImageUrl = company.AdminImageUrl;
                                    managerInfo.PrimaryManagerId = company.PrimaryManagerId;
                                    managerInfo.Companies = companyList;
                                    managerInfo.IsLogined = true;
                                    Session["admin_info"] = managerInfo;


                                    ListView lv = sender as ListView;
                                    if (lv.ID.Equals("lvCompaniesForSystem"))
                                    {
                                        string js = @"window.open('/home','_blank');";
                                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "windowOpen", js, true);
                                    }
                                    else
                                    {
                                        FormsAuthentication.RedirectFromLoginPage(company.Manager.Email, false);
                                    }
                                }
                                catch (Exception)
                                {
                                    cv.EnableClientScript = false;
                                    cv.IsValid = false;
                                    cv.ErrorMessage = "Connection Error. Please retry.";
                                }
                            }
                        }
                        else
                        {
                            Log.Error("Company data is null.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbMasterConsole_Click(object sender, EventArgs e)
        {
            try
            {
                //ManagerInfo managerInfo = new ManagerInfo();
                //managerInfo.CompanyId = company.CompanyId;
                //managerInfo.CompanyLogoUrl = company.CompanyLogoUrl;
                //managerInfo.CompnayWebsiteHeader = company.CompanyBannerUrl;
                //managerInfo.CompanyName = company.CompanyTitle;
                //managerInfo.Email = company.Manager.Email;
                //managerInfo.FirstName = company.Manager.FirstName;
                //managerInfo.LastName = company.Manager.LastName;
                //managerInfo.ProfileImageUrl = company.Manager.ProfileImageUrl;
                //managerInfo.TimeZone = company.TimeZoneOffset;
                //managerInfo.AccountType = company.Manager.Type;
                //managerInfo.UserId = company.Manager.UserId;
                //managerInfo.AccessModules = company.Manager.AccessModules;
                //managerInfo.CompanyAdminImageUrl = company.AdminImageUrl;
                //managerInfo.PrimaryManagerId = company.PrimaryManagerId;
                //managerInfo.Companies = companyList;
                //Session["admin_info"] = managerInfo;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtModule_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HyperLink hlModule = e.Item.FindControl("hlModule") as HyperLink;
                    System.Web.UI.WebControls.Image imgModule = e.Item.FindControl("imgModule") as System.Web.UI.WebControls.Image;
                    Label lblTitle = e.Item.FindControl("lblTitle") as Label;
                    Label lbldescription = e.Item.FindControl("lbldescription") as Label;

                    hlModule.Enabled = listModule[e.Item.ItemIndex].IsUsing;
                    switch (listModule[e.Item.ItemIndex].Key)
                    {
                        case CassandraService.Entity.Module.MODULE_ASSESSMENT:
                            hlModule.NavigateUrl = "/Assessment/List";
                            break;
                        case CassandraService.Entity.Module.MODULE_MODULE:
                            hlModule.NavigateUrl = "/Module/Companies";
                            break;
                        default:
                            hlModule.NavigateUrl = "";
                            break;
                    }
                    if (listModule[e.Item.ItemIndex].IsUsing)
                    {
                        imgModule.ImageUrl = "/img/" + listModule[e.Item.ItemIndex].IconUrl;
                    }
                    else
                    {
                        imgModule.ImageUrl = "/img/" + listModule[e.Item.ItemIndex].IconUrl.Replace(".png", "_invalid.png");
                    }
                    lblTitle.Text = listModule[e.Item.ItemIndex].Title;
                    lbldescription.Text = listModule[e.Item.ItemIndex].Description;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}