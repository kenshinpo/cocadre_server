﻿<%@ Page Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Department.aspx.cs" Inherits="AdminWebsite.Personnels.DepartmentPage" %>

<asp:Content ID="cttMain" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfDepartmentId" runat="server" />
            <asp:HiddenField ID="hfDepartmentTitle" runat="server" />
            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <asp:LinkButton ID="lbOpenAddDepartment" runat="server" CssClass="mfb-component__button--main" data-mfb-label="Add Department" OnClick="lbOpenAddDepartment_Click">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-briefcase"></i>
                    </asp:LinkButton>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Add Department -->
            <asp:Panel ID="popup_adddepartment" runat="server" CssClass="popup popup--adddepartment" Width="100%" Style="display: none;">
                <h1 class="popup__title">Add a Department</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Department</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbAddName" runat="server" placeholder="Title of your department" required="required" MaxLength="30" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDone" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbDone_Click" OnClientClick="ShowProgressBar();" Text="Done" />
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Add Department -->

            <!-- Rename Department -->
            <asp:Panel ID="popup_renamedepartment" runat="server" CssClass="popup popup--renamedepartment" Width="100%" Style="display: none;">
                <h1 class="popup__title">Rename Department</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Department</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbEditName" runat="server" placeholder="Title of your department" MaxLength="30" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbRename" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" OnClick="lbRename_Click" OnClientClick="ShowProgressBar();" Text="Save" />
                    <asp:LinkButton ID="lbRenameCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Rename Department -->

            <!-- Delete Department -->
            <asp:Panel ID="popup_deletedepartment" runat="server" CssClass="popup popup--deletedepartment" Width="100%" Style="display: none;">
                <h1 class="popup__title">Delete Department</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <asp:Literal ID="ltlDeleteMsg" runat="server" />
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbDelete_Click" Text="Delete" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbDelCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Delete Department -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_adddepartment"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Personnel <span>console</span></div>
        <div class="appbar__meta">Department</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link " href="/Personnel/Personnel">User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Department</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link " href="/Personnel/JobLevel">Job Level</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upDepartmentList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvDepartment" runat="server" OnItemDataBound="lvDepartment_ItemDataBound" OnItemCommand="lvDepartment_ItemCommand" OnItemCreated="lvDepartment_ItemCreated">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th>
                                            <asp:LinkButton ID="lbSortByName" runat="server" OnClick="lbSortByName_Click" Text="Department" />
                                        </th>
                                        <th>No. of Personnel</th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lbName" runat="server" ClientIDMode="AutoID" CommandName="Rename" Style="color: #999;" />
                                </td>
                                <td>
                                    <asp:HyperLink ID="hlNoOfPersonnel" runat="server" />
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbRename" ClientIDMode="AutoID" runat="server" CommandName="Rename" Text="Rename Department" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Remove" Text="Delete Department" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>