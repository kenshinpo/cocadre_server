﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.ServiceRequests;
using AdminWebsite.App_Code.ServiceResponses;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using Newtonsoft.Json;
using System.Configuration;
using Microsoft.VisualBasic.FileIO;

namespace AdminWebsite.Personnels
{
    public partial class Personnel : AutoCompleteSearchUser
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        private const int SORT_BY_NAME = 1;
        private const int SORT_BY_DEPARTMENT = 2;
        private const int SORT_BY_TYPE = 3;
        private const String DEFAULT_PROFILE_PHOTO = "https://s3-ap-southeast-1.amazonaws.com/cocadre/profile/default_profile_photo_original.png";

        private List<User> userList = null;
        private List<Department> departmentList = null;
        private List<Company.CompanyJob> jobLevelList = null;
        private List<CassandraService.Entity.Module> moduleList;

        public void ResetAddInput()
        {
            try
            {
                tbAddFirstName.Text = String.Empty;
                tbAddLastName.Text = String.Empty;
                tbAddEmail.Text = String.Empty;
                ddlAddGender.ClearSelection();
                tbAddBDYear.Text = String.Empty;
                ddlAddBDMonth.ClearSelection();
                tbAddBDDay.Text = String.Empty;
                imgAddUserPhoto.ImageUrl = DEFAULT_PROFILE_PHOTO;
                hfImgBase64.Value = String.Empty;
                tbAddPosition.Text = String.Empty;
                tbAddPhone.Text = String.Empty;
                tbAddAddress.Text = String.Empty;
                tbAddAddressPostalCode.Text = String.Empty;
                #region Reset cbAddDepartment
                GetDepartmentList();
                cbAddDepartment.Items.Clear();
                for (int i = 0; i < departmentList.Count; i++)
                {
                    cbAddDepartment.Items.Add(new ListItem(departmentList[i].Title, Convert.ToString(departmentList[i].Id)));
                }
                if (cbAddDepartment.Items.Count > 0)
                {
                    cbAddDepartment.Items[0].Selected = true;
                }
                #endregion

                #region Reset cbJobLevel
                GetJobLevelList();
                cbJobLevel.Items.Clear();
                for (int i = 0; i < jobLevelList.Count; i++)
                {
                    cbJobLevel.Items.Add(new ListItem(jobLevelList[i].Title, Convert.ToString(jobLevelList[i].JobId)));
                }
                if (cbJobLevel.Items.Count > 0)
                {
                    cbJobLevel.Items[0].Selected = true;
                }
                #endregion

                #region Reset ddlAddPhoneCountryCode
                HttpRequest request = base.Request;
                RegionInfo ri = CommonUtility.GetCountry();
                ddlAddPhoneCountryCode.Items.Clear();
                List<Country> countryList = GetCountryList();
                countryList.Sort((x, y) => { return x.Code.CompareTo(y.Code); });
                for (int i = 0; i < countryList.Count; i++)
                {
                    ddlAddPhoneCountryCode.Items.Add(new ListItem(countryList[i].Display, countryList[i].Abb));

                    if (countryList[i].Name.ToLower().Equals(ri.EnglishName.ToLower()))
                    {
                        ddlAddPhoneCountryCode.Items[i].Selected = true;
                    }
                }
                #endregion

                #region Reset ddlAddAddressCountryCode
                ddlAddAddressCountryCode.Items.Clear();
                countryList.Sort((x, y) => { return x.Name.CompareTo(y.Name); });
                for (int i = 0; i < countryList.Count; i++)
                {
                    ddlAddAddressCountryCode.Items.Add(new ListItem(countryList[i].Name, Convert.ToString(countryList[i].Abb)));

                    if (countryList[i].Name.ToLower().Equals(ri.EnglishName.ToLower()))
                    {
                        ddlAddAddressCountryCode.Items[i].Selected = true;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void HideAllAddUserPanel()
        {
            try
            {
                plAddUserBasicInfo.CssClass = "wrap_AddUserBasicInfo hide";
                plAddUserExtraInfo.CssClass = "wrap_AddUserExtraInfo hide";
                plAddUserPhotoType.CssClass = "wrap_AddUserPhotoType hide";
                plAddUserPhotoUrl.CssClass = "wrap_AddUserPhotoUrl hide";
                plAddUserPhotoUpload.CssClass = "wrap_AddUserPhotoUpload hide";
                plAddUserSuccess.CssClass = "wrap_AddUserSuccess hide";
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void HideAllUpdateUserPanel()
        {
            try
            {
                plUpdateUserBasicInfo.CssClass = "wrap_UpdateUserBasicInfo hide";
                plUpdateUserExtraInfo.CssClass = "wrap_UpdateUserExtraInfo hide";
                plUpdateUserPhotoType.CssClass = "wrap_UpdateUserPhotoType hide";
                plUpdateUserPhotoUrl.CssClass = "wrap_UpdateUserPhotoUrl hide";
                plUpdateUserPhotoUpload.CssClass = "wrap_UpdateUserPhotoUpload hide";
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void GetUserList(string departmentId, int accountType, int accountStatus, bool isPendingInvite, bool isPendingLogin)
        {
            try
            {
                List<String> keys = null;

                if (!String.IsNullOrEmpty(tbFilterUser.Text.Trim()))
                {
                    keys = tbFilterUser.Text.Trim().Split(' ').ToList<String>();
                }

                UserListResponse response = asc.GetAllUser(adminInfo.UserId, adminInfo.CompanyId, departmentId, accountType, accountStatus, keys, isPendingInvite, isPendingLogin);
                if (response.Success)
                {
                    userList = response.Users;
                    int sortBy = Convert.ToInt16(ViewState["SortBy"]);
                    bool isSortAcsend = true;
                    switch (sortBy)
                    {
                        case SORT_BY_NAME:
                            userList = userList.OrderBy(o => o.FirstName).ThenBy(o => o.LastName).ToList();
                            isSortAcsend = Convert.ToBoolean(ViewState["IsSortByNameAcsend"]);
                            break;
                        case SORT_BY_DEPARTMENT:
                            userList = userList.OrderBy(o => o.Departments[0].Title).ToList();
                            isSortAcsend = Convert.ToBoolean(ViewState["IsSortByDepartmentAcsend"]);
                            break;
                        case SORT_BY_TYPE:
                            userList = userList.OrderBy(o => o.Type.Title).ToList();
                            isSortAcsend = Convert.ToBoolean(ViewState["IsSortByTypeAcsend"]);
                            break;
                        default:
                            userList = userList.OrderBy(o => o.FirstName).ThenBy(o => o.LastName).ToList();
                            isSortAcsend = Convert.ToBoolean(ViewState["IsSortByNameAcsend"]);
                            break;
                    }

                    if (!isSortAcsend)
                    {
                        userList.Reverse();
                    }
                }
                else
                {
                    Log.Error("GetAllUser request is failed. result error msg: " + response.ErrorMessage);
                    userList = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                userList = null;
            }
        }

        public void GetAllAccountKind()
        {
            try
            {
                if (ViewState["accountKindList"] == null)
                {
                    AccountKindListResponse response = asc.GetAllAccountKind(adminInfo.UserId, adminInfo.CompanyId);
                    if (response.Success)
                    {
                        List<AccountKind> accountKindList = response.AccountKinds;
                        accountKindList = accountKindList.OrderBy(o => o.Order).ToList();
                        ViewState["accountKindList"] = accountKindList;
                    }
                    else
                    {
                        Log.Error("GetAllAccountKind request is failed. result error msg: " + response.ErrorMessage);
                        ViewState["accountKindList"] = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                ViewState["accountKindList"] = null;
            }
        }

        public void GetDepartmentList()
        {
            try
            {
                DepartmentListResponse response = asc.GetAllDepartment(adminInfo.UserId, adminInfo.CompanyId);
                if (response.Success)
                {
                    List<Department> list = response.Departments;
                    list.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
                    departmentList = list;
                }
                else
                {
                    Log.Error("DepartmentListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    departmentList = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                departmentList = null;
            }
        }

        public void GetJobLevelList()
        {
            try
            {
                CompanySelectAllJobResponse response = asc.SelectJobs(adminInfo.CompanyId, adminInfo.UserId);
                if (response.Success)
                {
                    List<Company.CompanyJob> list = response.Jobs;
                    list.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
                    jobLevelList = list;
                }
                else
                {
                    Log.Error("CompanySelectAllJobResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    jobLevelList = null;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                jobLevelList = null;
            }
        }

        public List<Country> GetCountryList()
        {
            try
            {
                if (Session["Couuntries"] == null)
                {
                    CountryListResponse response = asc.GetAllCountry(adminInfo.UserId, adminInfo.CompanyId);
                    if (response.Success)
                    {
                        Session["Couuntries"] = response.Countries;
                    }
                    else
                    {
                        Log.Error("CountrySelectResponse request is failed. Result error msg: " + response.ErrorMessage);
                        return null;
                    }
                }

                List<Country> list = (List<Country>)Session["Couuntries"];
                list = list.OrderBy(c => c.Code).ThenBy(c => c.Abb).ToList();
                return list;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                return null;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    ViewState["IsSortByNameAcsend"] = true;
                    ViewState["IsSortByDepartmentAcsend"] = true;
                    ViewState["IsSortByTypeAcsend"] = true;
                    ViewState["SortBy"] = 1;

                    String departmentId = String.Empty;
                    if (Page.RouteData.Values["DepartmentId"] != null)
                    {
                        departmentId = Page.RouteData.Values["DepartmentId"].ToString();
                    }

                    List<AccountKind> accountKindList = null;

                    #region Get account type and databind to ddlFilterUserType
                    ddlFilterUserType.Items.Clear();
                    GetAllAccountKind();
                    if (ViewState["accountKindList"] != null)
                    {
                        accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                        for (int i = 0; i < accountKindList.Count; i++)
                        {
                            ddlFilterUserType.Items.Add(new ListItem(accountKindList[i].Title, Convert.ToString(accountKindList[i].Order)));
                        }
                    }
                    #endregion

                    #region Get departments and databind to ddlFilterDepartment
                    ddlFilterDepartment.Items.Clear();
                    ddlFilterDepartment.Items.Add(new ListItem("All", String.Empty));
                    GetDepartmentList();
                    if (departmentList != null)
                    {
                        for (int i = 0; i < departmentList.Count; i++)
                        {
                            ddlFilterDepartment.Items.Add(new ListItem(departmentList[i].Title, departmentList[i].Id));
                            if (departmentId == departmentList[i].Id)
                            {
                                ddlFilterDepartment.Items[i + 1].Selected = true;
                            }
                        }
                    }
                    #endregion

                    #region Get users and databind to lvPerson
                    ViewState["SortBy"] = SORT_BY_NAME;
                    if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                    }
                    else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                    }
                    else
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                    }

                    lvPerson.DataSource = userList;
                    lvPerson.DataBind();
                    #endregion

                    #region Get moderator rights
                    GetModeratorModulesResponse response = asc.GetModeratorModules();
                    moduleList = response.Modules;
                    for (int i = 0; i < moduleList.Count; i++)
                    {
                        String text = "<img src='/img/" + moduleList[i].IconUrl + "' alt='" + moduleList[i].Title + "' title='" + moduleList[i].Title + "' height='25px' /><div><strong>" + moduleList[i].Title + "</strong><br />" + moduleList[i].RightsDescription + "</div>";
                        cblChangeTypeModule.Items.Add(new ListItem(text, Convert.ToString(moduleList[i].Key)));
                        cblChangeRightsModule.Items.Add(new ListItem(text, Convert.ToString(moduleList[i].Key)));
                    }
                    #endregion

                    #region
                    HideAllAddUserPanel();
                    HideAllUpdateUserPanel();
                    #endregion
                }

                tbFilterUser.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterUser.ClientID + "').focus();return true;}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvPerson_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (userList != null && userList.Count > 0)
                {
                    if (e.Item.ItemType == ListViewItemType.DataItem)
                    {
                        System.Web.UI.WebControls.Image imgPhoto = e.Item.FindControl("imgPhoto") as System.Web.UI.WebControls.Image;
                        LinkButton lbName = e.Item.FindControl("lbName") as LinkButton;
                        Label lblDelete = e.Item.FindControl("lblDelete") as Label;
                        Label lblSuspended = e.Item.FindControl("lblSuspended") as Label;

                        //Label lblDepartment = e.Item.FindControl("lblDepartment") as Label;
                        //Label lblUserType = e.Item.FindControl("lblUserType") as Label;
                        LinkButton lbDepartment = e.Item.FindControl("lbDepartment") as LinkButton;
                        LinkButton lbJobLevel = e.Item.FindControl("lbJobLevel") as LinkButton;
                        LinkButton lbUserType = e.Item.FindControl("lbUserType") as LinkButton;
                        LinkButton lbPhoto = e.Item.FindControl("lbPhoto") as LinkButton;

                        LinkButton lbUpdate = e.Item.FindControl("lbUpdate") as LinkButton;
                        LinkButton lbChangeRights = e.Item.FindControl("lbChangeRights") as LinkButton;
                        LinkButton lbChangeType = e.Item.FindControl("lbChangeType") as LinkButton;
                        LinkButton lbSuspend = e.Item.FindControl("lbSuspend") as LinkButton;
                        LinkButton lbRestoreSus = e.Item.FindControl("lbRestoreSus") as LinkButton;
                        LinkButton lbDelete = e.Item.FindControl("lbDelete") as LinkButton;
                        LinkButton lbRestoreDel = e.Item.FindControl("lbRestoreDel") as LinkButton;
                        LinkButton lbConfirmDelete = e.Item.FindControl("lbConfirmDelete") as LinkButton;

                        LinkButton lbResendInvitation = e.Item.FindControl("lbResendInvitation") as LinkButton;
                        LinkButton lbResendInvitationAll = e.Item.FindControl("lbResendInvitationAll") as LinkButton;
                        LinkButton lbResentPassword = e.Item.FindControl("lbResentPassword") as LinkButton;

                        #region Acoount status and type
                        // ACTIVE
                        if (userList[e.Item.DataItemIndex].Status.Code == CassandraService.Entity.User.AccountStatus.CODE_ACTIVE)
                        {
                            lbUpdate.Visible = true;
                            lbChangeType.Visible = true;
                            lbSuspend.Visible = true;
                            lbDelete.Visible = true;
                        }
                        //SUSPENEDED
                        else if (userList[e.Item.DataItemIndex].Status.Code == CassandraService.Entity.User.AccountStatus.CODE_SUSPENEDED)
                        {
                            lbUpdate.Visible = true;
                            lbChangeType.Visible = true;
                            lbRestoreSus.Visible = true;
                            lblSuspended.Visible = true;
                        }
                        //DELETING
                        else if (userList[e.Item.DataItemIndex].Status.Code == CassandraService.Entity.User.AccountStatus.CODE_DELETING)
                        {
                            lbRestoreDel.Visible = true;
                            lbConfirmDelete.Visible = true;

                            String msg = "Permanently deleted in ";
                            DateTimeOffset dtoMod = userList[e.Item.DataItemIndex].LastModifiedStatusTimestamp.AddDays(8);
                            DateTimeOffset dtoNow = DateTimeOffset.UtcNow;
                            if ((dtoMod - dtoNow).Days < 2)
                            {
                                msg = msg + "1 day";
                                lblDelete.Text = msg;
                            }
                            else
                            {
                                msg = msg + (dtoMod - dtoNow).Days + " days";
                                lblDelete.Text = msg;
                            }
                            lblDelete.Visible = true;
                        }

                        if (userList[e.Item.DataItemIndex].Type.Code == CassandraService.Entity.User.AccountType.CODE_SUPER_ADMIN) // SuperAdmin
                        {
                            lbChangeType.Visible = false;
                            lbDelete.Visible = false;
                            lbSuspend.Visible = false;
                        }
                        else if (userList[e.Item.DataItemIndex].Type.Code == CassandraService.Entity.User.AccountType.CODE_ADMIN) // Admin
                        {
                            if (userList[e.Item.DataItemIndex].UserId == adminInfo.PrimaryManagerId)
                            {
                                lbChangeType.Visible = false;
                                lbDelete.Visible = false;
                                lbSuspend.Visible = false;
                            }
                        }
                        else if (userList[e.Item.DataItemIndex].Type.Code == CassandraService.Entity.User.AccountType.CODE_MODERATER) // Moderater
                        {
                            lbChangeRights.Visible = true;
                        }

                        if (!userList[e.Item.DataItemIndex].IsEmailSent) // 尚未登入
                        {
                            lbResendInvitation.Visible = true;
                            lbResendInvitationAll.Visible = true;
                        }
                        else
                        {
                            if (userList[e.Item.DataItemIndex].HasLogin) // 登入過
                            {
                                lbResentPassword.Visible = true;
                            }
                            else
                            {
                                lbResendInvitation.Visible = true;
                                lbResendInvitationAll.Visible = true;
                            }
                        }
                        #endregion

                        imgPhoto.ImageUrl = userList[e.Item.DataItemIndex].ProfileImageUrl;//.Replace("_original", "_small");
                        lbName.Text = userList[e.Item.DataItemIndex].FirstName + " " + userList[e.Item.DataItemIndex].LastName;
                        lbName.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbDepartment.Text = userList[e.Item.DataItemIndex].Departments[0].Title;
                        lbDepartment.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbJobLevel.Text = userList[e.Item.DataItemIndex].Job == null ? "" : userList[e.Item.DataItemIndex].Job.Title;
                        lbJobLevel.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbUserType.Text = userList[e.Item.DataItemIndex].Type.Title;
                        lbUserType.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbPhoto.CommandArgument = userList[e.Item.DataItemIndex].UserId;

                        lbUpdate.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbChangeRights.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbChangeType.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbSuspend.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbRestoreSus.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbDelete.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbRestoreDel.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbConfirmDelete.CommandArgument = userList[e.Item.DataItemIndex].UserId;

                        lbResendInvitation.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                        lbResentPassword.CommandArgument = userList[e.Item.DataItemIndex].UserId;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAllAccountKind();
                List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                }
                else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                }
                else
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                }
                lvPerson.DataSource = userList;
                lvPerson.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType, bool isShowPop)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            if (isShowPop)
            {
                mpePop.Show();
            }
            else
            {
                mpePop.Hide();
            }
        }

        protected void lbAddUserCreate_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                String toastMsg = String.Empty;

                // First Name
                if (String.IsNullOrEmpty(tbAddFirstName.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Please enter first name.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                if (tbAddFirstName.Text.Length > 25)
                {
                    ShowToastMsgAndHideProgressBar("First name cannot be more than 25 characters.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Last Name
                if (String.IsNullOrEmpty(tbAddLastName.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Please enter last name.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                if (tbAddLastName.Text.Length > 25)
                {
                    ShowToastMsgAndHideProgressBar("Last name cannot be more than 25 characters.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Email
                if (String.IsNullOrEmpty(tbAddEmail.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Please enter email address.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                RegexUtility ru = new RegexUtility();
                if (!ru.IsValidEmail(tbAddEmail.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Email address is not valid.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Department
                String departmentTitle = String.Empty;
                TextBox tb = cbAddDepartment.FindControl("cbAddDepartment_TextBox") as TextBox;
                if (tb != null)
                {
                    if (String.IsNullOrEmpty(tb.Text.Trim()))
                    {
                        ShowToastMsgAndHideProgressBar("Please select department.", MessageUtility.TOAST_TYPE_ERROR, true);
                        return;
                    }
                    else
                    {
                        departmentTitle = tb.Text.Trim();
                    }
                }

                // Job Level
                String jobLevelTitle = String.Empty;
                TextBox jobLevel_tb = cbJobLevel.FindControl("cbJobLevel_TextBox") as TextBox;
                if (jobLevel_tb != null)
                {
                    if (String.IsNullOrEmpty(jobLevel_tb.Text.Trim()))
                    {
                        ShowToastMsgAndHideProgressBar("Please select job level.", MessageUtility.TOAST_TYPE_ERROR, true);
                        return;
                    }
                    else
                    {
                        jobLevelTitle = jobLevel_tb.Text.Trim();
                    }
                }

                // Birthday
                DateTime? dtUserBirthday = null;
                try
                {
                    if (!String.IsNullOrEmpty(tbAddBDYear.Text.Trim()) && !String.IsNullOrEmpty(tbAddBDDay.Text.Trim()))
                    {
                        dtUserBirthday = new DateTime(Convert.ToInt16(tbAddBDYear.Text.Trim()), Convert.ToInt16(ddlAddBDMonth.SelectedValue), Convert.ToInt16(tbAddBDDay.Text.Trim()));
                        DateTime dtMax = DateTime.UtcNow;
                        DateTime dtMin = new DateTime(1900, 1, 1);

                        if (DateTime.Compare(dtUserBirthday.Value, dtMax) > 0 || DateTime.Compare(dtUserBirthday.Value, dtMin) < 0)
                        {
                            ShowToastMsgAndHideProgressBar("Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR, true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString(), ex);
                    ShowToastMsgAndHideProgressBar("Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Country
                string countryCode = string.Empty;
                if (ddlAddPhoneCountryCode.SelectedItem != null)
                {
                    List<Country> countries = GetCountryList();
                    countryCode = Convert.ToString(countries.Where(c => c.Abb.Equals(ddlAddPhoneCountryCode.SelectedItem.Value)).FirstOrDefault().Code);
                }
                #endregion

                #region Step 2. Upload profile image to S3.
                String userId = "U" + Guid.NewGuid().ToString().Replace("-", "");
                String profileImageUrl = DEFAULT_PROFILE_PHOTO;

                if (!String.IsNullOrEmpty(hfImgBase64.Value))
                {
                    String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                    String imgBase64String = ImageUtility.FilterBase64String(hfImgBase64.Value), imgFormat = "jpg";
                    IAmazonS3 s3Client = S3Utility.GetIAmazonS3(adminInfo.CompanyId, adminInfo.UserId);
                    using (s3Client)
                    {
                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", "1080");
                        metadatas.Add("Height", "1080");
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = PROCESS_TIMESTAMP + "_original." + imgFormat.ToLower();

                        // Upload to original and resize bucket.
                        #region Original path: cocarde-{companyId}/users/{userId}.{format}
                        String bucketName = "cocadre-" + adminInfo.CompanyId.ToLower();
                        String folderName = "users/" + userId;
                        S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);

                        profileImageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;

                        #endregion

                        #region Resize path: cocarde-images-resize-input/{environment}/{companyId}/users/{userId}.{format}
                        bucketName = "cocadre-images-resize-input";
                        folderName = ConfigurationManager.AppSettings["ServerEnvironment"].ToString() + "/" + adminInfo.CompanyId.ToLower() + "/users/" + userId;
                        S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        #endregion
                    }
                }
                #endregion



                //#region Step 2. Upload profile image to S3. (original, large, medium and small size) (/cocadre-images-resize-input/dev/{company_id}/users/{user_id})


                //if (!String.IsNullOrEmpty(hfImgBase64.Value))
                //{
                //    String imgBase64String = String.Empty, imgFormat = String.Empty;
                //    String bucketName = "cocadre-images-resize-input";
                //    String folderName = ConfigurationManager.AppSettings["ServerEnvironment"].ToString() + "/" + adminInfo.CompanyId + "/users/" + userId;
                //    String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                //    IAmazonS3 s3Client = S3Utility.GetIAmazonS3(adminInfo.CompanyId, adminInfo.UserId);
                //    S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                //    using (s3Client)
                //    {
                //        imgBase64String = ImageUtility.FilterBase64String(hfImgBase64.Value);
                //        imgFormat = "jpg";

                //        #region Step 2.1. Upload original image. (1080 * 1080)
                //        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                //        metadatas.Add("Width", "1080");
                //        metadatas.Add("Height", "1080");
                //        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                //        String fileName = PROCESS_TIMESTAMP + "_original." + imgFormat.ToLower();
                //        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                //        profileImageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;
                //        Log.Debug("profileImageUrl: " + profileImageUrl);
                //        #endregion
                //    }
                //}
                //#endregion

                #region Step 3. Call webapi
                UserCreateResponse userCreateResponse = asc.CreateUser(adminInfo.UserId,
                                                                       adminInfo.CompanyId,
                                                                       userId,
                                                                       tbAddFirstName.Text.Trim(),
                                                                       tbAddLastName.Text.Trim(),
                                                                       tbAddEmail.Text.Trim().ToLower(),
                                                                       profileImageUrl,
                                                                       tbAddPosition.Text.Trim(),
                                                                       tbAddPhone.Text.Trim(),
                                                                       countryCode,
                                                                       (ddlAddPhoneCountryCode.SelectedItem == null) ? String.Empty : ddlAddPhoneCountryCode.SelectedItem.Value,
                                                                       tbAddAddress.Text.Trim(),
                                                                       (ddlAddAddressCountryCode.SelectedItem == null) ? String.Empty : ddlAddAddressCountryCode.SelectedItem.Text,
                                                                       tbAddAddressPostalCode.Text.Trim(),
                                                                       departmentTitle,
                                                                       jobLevelTitle,
                                                                       Convert.ToInt16(ddlAddGender.SelectedValue),
                                                                       dtUserBirthday
                                                                       );
                #endregion

                #region Step 4. Get response.
                hfImgBase64.Value = String.Empty;
                if (userCreateResponse.Success)
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    ltlAdduserSuccessEmail.Text = tbAddEmail.Text;
                    HideAllAddUserPanel();
                    plAddUserSuccess.CssClass = "wrap_AddUserSuccess";
                    upPop.Update();
                    mpePop.Show();
                }
                else
                {
                    Log.Error("Created failed. " + userCreateResponse.ErrorMessage);
                    ShowToastMsgAndHideProgressBar("User failed to create." + userCreateResponse.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR, true);
                    upPop.Update();
                    mpePop.Show();

                    #region Delete file on S3

                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                ShowToastMsgAndHideProgressBar("User failed to create.", MessageUtility.TOAST_TYPE_ERROR, false);
            }
        }

        protected void lbAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.PopupControlID = "popup_adduser";
                mpePop.Show();
                HideAllAddUserPanel();
                plAddUserBasicInfo.CssClass = "wrap_AddUserBasicInfo";
                ResetAddInput();
                upPop.Update();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                hfImgBase64.Value = string.Empty;
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAddUserSuccess_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
                List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                }
                else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                }
                else
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                }
                lvPerson.DataSource = userList;
                lvPerson.DataBind();
                ShowToastMsgAndHideProgressBar(tbAddFirstName.Text + " " + tbAddLastName.Text + " has been created as a normal user", MessageUtility.TOAST_TYPE_INFO, false);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvPerson_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("UpdateUser"))
                {
                    UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    #region data bind
                    if (response.Success)
                    {
                        ltlUpdateUserPhotoError.Text = "";
                        hfUserId.Value = response.User.UserId;
                        tbUpdateAddress.Text = response.User.Address;
                        tbUpdateAddressPostalCode.Text = response.User.AddressPostalCode;
                        tbUpdateEmail.Text = response.User.Email;
                        ddlUpdateGender.ClearSelection();
                        if (response.User.Gender == null)
                        {
                            ddlUpdateGender.Items[0].Selected = true;
                        }
                        else
                        {
                            for (int i = 0; i < ddlUpdateGender.Items.Count; i++)
                            {
                                if (Convert.ToInt16(ddlUpdateGender.Items[i].Value) == response.User.Gender)
                                {
                                    ddlUpdateGender.Items[i].Selected = true;
                                    break;
                                }
                            }
                        }
                        if (response.User.Birthday == null)
                        {
                            tbUpdateBDYear.Text = String.Empty;
                            ddlUpdateBDMonth.ClearSelection();
                            ddlUpdateBDMonth.Items[0].Selected = true;
                            tbUpdateBDDay.Text = String.Empty;
                        }
                        else
                        {
                            tbUpdateBDYear.Text = response.User.Birthday.Value.Year.ToString();
                            ddlUpdateBDMonth.ClearSelection();
                            for (int i = 0; i < ddlUpdateBDMonth.Items.Count; i++)
                            {
                                if (Convert.ToInt16(ddlUpdateBDMonth.Items[i].Value) == response.User.Birthday.Value.Month)
                                {
                                    ddlUpdateBDMonth.Items[i].Selected = true;
                                    break;
                                }
                            }
                            tbUpdateBDDay.Text = response.User.Birthday.Value.Day.ToString();
                        }
                        tbUpdateFirstName.Text = response.User.FirstName;
                        tbUpdateLastName.Text = response.User.LastName;
                        tbUpdatePhone.Text = response.User.Phone;

                        if (response.User.Departments.Count > 0)
                        {
                            tbUpdatePosition.Text = response.User.Departments[0].Position;
                        }
                        else
                        {
                            Log.Error("User does not have department at table department_by_user");
                            Log.Error("UserId: " + response.User.UserId);
                            tbUpdatePosition.Text = string.Empty;
                        }

                        GetDepartmentList();
                        cbUpdateDepartment.Items.Clear();
                        for (int j = 0; j < departmentList.Count; j++)
                        {
                            cbUpdateDepartment.Items.Add(new ListItem(departmentList[j].Title, Convert.ToString(departmentList[j].Id)));
                            if (departmentList[j].Id.Equals(response.User.Departments[0].Id))
                            {
                                cbUpdateDepartment.Items[j].Selected = true;
                            }
                        }

                        // Update job level
                        GetJobLevelList();
                        cbUpdateJobLevel.Items.Clear();
                        for (int j = 0; j < jobLevelList.Count; j++)
                        {
                            cbUpdateJobLevel.Items.Add(new ListItem(jobLevelList[j].Title, Convert.ToString(jobLevelList[j].JobId)));
                            if (jobLevelList[j].JobId.Equals(response.User.Job.JobId))
                            {
                                cbUpdateJobLevel.Items[j].Selected = true;
                            }
                        }

                        if (!String.IsNullOrEmpty(response.User.ProfileImageUrl))
                        {
                            imgUpdateUserPhoto.ImageUrl = response.User.ProfileImageUrl;
                        }
                        else
                        {
                            imgUpdateUserPhoto.ImageUrl = "";
                        }
                        hfImgBase64.Value = String.Empty;

                        #region Get country code(Phone)
                        HttpRequest request = base.Request;
                        ddlUpdatePhoneCountryCode.Items.Clear();
                        List<Country> countryList = GetCountryList();
                        //countryList.Sort((x, y) => { return x.Code.CompareTo(y.Code); });
                        for (int i = 0; i < countryList.Count; i++)
                        {
                            ddlUpdatePhoneCountryCode.Items.Add(new ListItem(countryList[i].Display, countryList[i].Abb));
                            if (!String.IsNullOrEmpty(response.User.PhoneCountryAbb) && countryList[i].Abb.ToLower().Equals(response.User.PhoneCountryAbb.ToLower()))
                            {
                                ddlUpdatePhoneCountryCode.Items[i].Selected = true;
                            }
                        }
                        #endregion

                        #region Get country code(Address)
                        ddlUpdateAddressCountryCode.Items.Clear();
                        countryList.Sort((x, y) => { return x.Name.CompareTo(y.Name); });
                        for (int i = 0; i < countryList.Count; i++)
                        {
                            ddlUpdateAddressCountryCode.Items.Add(new ListItem(countryList[i].Name, Convert.ToString(countryList[i].Abb)));
                            if (!String.IsNullOrEmpty(response.User.AddressCountryName) && countryList[i].Name.ToLower().Equals(response.User.AddressCountryName.ToLower()))
                            {
                                ddlUpdateAddressCountryCode.Items[i].Selected = true;
                            }
                        }
                        #endregion

                        HideAllUpdateUserPanel();
                        plUpdateUserBasicInfo.CssClass = "wrap_UpdateUserBasicInfo";
                        mpePop.PopupControlID = "popup_updateuser";
                        mpePop.Show();
                        upPop.Update();
                        //mpeUpdateUser.Show();
                        //upUpdateUser.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "AddUserSuccess", "ReloadToast();toastr.info('Load user data failed.');", true);
                    }
                    #endregion
                }
                else if (e.CommandName.Equals("ChangeType"))
                {
                    UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        ltlChangeTypeUserId.Text = e.CommandArgument.ToString();
                        ltlChangeTypeUserName1.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlChangeTypeUserName2.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlChangeTypeTitle.Text = response.User.Type.Title;
                        ddlChangeType.Items.Clear();
                        switch (response.User.Type.Code)
                        {
                            // NORMAL_USER = 1
                            case 1:
                                ddlChangeType.Items.Add(new ListItem("Admin", "3"));
                                ddlChangeType.Items.Add(new ListItem("Moderator", "2"));
                                plChangeTypeModule.Visible = false;
                                plChangeTypeAdminMsg.Visible = true;
                                break;
                            // MODERATER = 2
                            case 2:
                                ddlChangeType.Items.Add(new ListItem("Admin", "3"));
                                ddlChangeType.Items.Add(new ListItem("Normal User", "1"));
                                plChangeTypeModule.Visible = false;
                                plChangeTypeAdminMsg.Visible = true;
                                break;
                            // ADMIN = 3
                            case 3:
                                ddlChangeType.Items.Add(new ListItem("Moderator", "2"));
                                ddlChangeType.Items.Add(new ListItem("Normal User", "1"));
                                plChangeTypeModule.Visible = true;
                                plChangeTypeAdminMsg.Visible = false;
                                break;
                            default:
                                break;
                        }
                        mpePop.PopupControlID = "popup_changeaccessrights";
                        mpePop.Show();
                        upPop.Update();
                        upChangeType.Update();
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Load user data failed.", MessageUtility.TOAST_TYPE_ERROR, false);
                    }
                }
                else if (e.CommandName.Equals("ChangeRights"))
                {
                    cblChangeRightsModule.ClearSelection();
                    UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        for (int i = 0; i < response.User.AccessModules.Count; i++)
                        {
                            for (int j = 0; j < cblChangeRightsModule.Items.Count; j++)
                            {
                                if (Convert.ToInt16(cblChangeRightsModule.Items[j].Value) == response.User.AccessModules[i].Key)
                                {
                                    cblChangeRightsModule.Items[j].Selected = true;
                                }
                            }
                        }

                        // If user is a moderator. Get the expired date.
                        if (response.User.ModeratorExpiredDate.HasValue)
                        {
                            rbChangeRightsNoExpiryDate.Checked = false;
                            rbChangeRightsExpiredDate.Checked = true;
                            DateTime dt = response.User.ModeratorExpiredDate ?? DateTime.Now;
                            tbChangeRightsExpiredDate.Text = dt.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            rbChangeRightsNoExpiryDate.Checked = true;
                            rbChangeRightsExpiredDate.Checked = false;
                            tbChangeRightsExpiredDate.Text = String.Empty;
                        }

                        hfChangeRightUserId.Value = e.CommandArgument.ToString();
                        mpePop.PopupControlID = "popup_assignaccessrights";
                        mpePop.Show();
                        upPop.Update();
                        upChangeRights.Update();
                    }
                    else
                    {
                        MessageUtility.ShowToast(this.Page, "Load user data failed.", MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
                else if (e.CommandName.Equals("SuspendUser"))
                {
                    UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        ltlSusUserId.Text = e.CommandArgument.ToString();
                        ltlSusUserName1.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlSusUserName2.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlSusCompanyName.Text = response.User.Company.CompanyTitle;
                        if (!String.IsNullOrEmpty(response.User.ProfileImageUrl))
                        {
                            imgSusUser.ImageUrl = response.User.ProfileImageUrl;
                        }
                        else
                        {
                            imgSusUser.ImageUrl = "";
                        }
                        mpePop.PopupControlID = "popup_suspenduser";
                        mpePop.Show();
                        upPop.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "AddUserSuccess", "ReloadToast();toastr.info('Load user data failed.');", true);
                    }
                }
                else if (e.CommandName.Equals("DeleteUser"))
                {
                    UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        ltlDelUserId.Text = e.CommandArgument.ToString();
                        ltlDelUserName.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlDelUserName2.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlDelUserName3.Text = response.User.FirstName + " " + response.User.LastName;
                        if (!String.IsNullOrEmpty(response.User.ProfileImageUrl))
                        {
                            imgDelUser.ImageUrl = response.User.ProfileImageUrl;
                        }
                        else
                        {
                            imgDelUser.ImageUrl = "";
                        }
                        mpePop.PopupControlID = "popup_deleteuser";
                        mpePop.Show();
                        upPop.Update();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "AddUserSuccess", "ReloadToast();toastr.info('Load user data failed.');", true);
                    }
                }
                else if (e.CommandName.Equals("RestoreDeleted") || e.CommandName.Equals("RestoreSuspend"))
                {
                    UserUpdateStatusResponse response = asc.UpdateUserStatus(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString(), 1);
                    if (response.Success)
                    {
                        List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                        if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                        {
                            GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                        }
                        else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                        {
                            GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                        }
                        else
                        {
                            GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                        }
                        lvPerson.DataSource = userList;
                        lvPerson.DataBind();

                        ShowToastMsgAndHideProgressBar("User has been restored.", MessageUtility.TOAST_TYPE_INFO, false);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Restored user failed.", MessageUtility.TOAST_TYPE_ERROR, false);
                    }
                }
                else if (e.CommandName.Equals("ConfirmDelete"))
                {
                    UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        ltlConfirmDelUserId.Text = e.CommandArgument.ToString();
                        ltlConfirmDelUserName.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlConfirmDelUserName2.Text = response.User.FirstName + " " + response.User.LastName;
                        ltlConfirmDelUserName3.Text = response.User.FirstName + " " + response.User.LastName;
                        if (!String.IsNullOrEmpty(response.User.ProfileImageUrl))
                        {
                            imgConfirmDelUser.ImageUrl = response.User.ProfileImageUrl;
                        }
                        else
                        {
                            imgConfirmDelUser.ImageUrl = "";
                        }
                        mpePop.PopupControlID = "popup_confirmdeleteuser";
                        mpePop.Show();
                        upPop.Update();
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Load user data failed.", MessageUtility.TOAST_TYPE_ERROR, false);
                    }
                }
                else if (e.CommandName.Equals("ResendInvitation"))
                {
                    AuthenticationUpdateResponse response = asc.ReinviteUser(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        ShowToastMsgAndHideProgressBar("Invitation sent", MessageUtility.TOAST_TYPE_INFO, false);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Invitation fail to send", MessageUtility.TOAST_TYPE_ERROR, false);
                    }
                }
                else if (e.CommandName.Equals("ResendInvitationAll"))
                {
                    AuthenticationUpdateResponse response = asc.ReinviteUsers(adminInfo.UserId, adminInfo.CompanyId);
                    if (response.Success)
                    {
                        ShowToastMsgAndHideProgressBar("Awaiting for email to be send out", MessageUtility.TOAST_TYPE_INFO, false);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Email failed to send out", MessageUtility.TOAST_TYPE_ERROR, false);
                    }
                }
                else if (e.CommandName.Equals("ResentPassword"))
                {
                    AuthenticationUpdateResponse response = asc.ResetPasswordForUser(adminInfo.UserId, adminInfo.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        ShowToastMsgAndHideProgressBar("New interim password sent to user", MessageUtility.TOAST_TYPE_INFO, false);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Reset password failed", MessageUtility.TOAST_TYPE_ERROR, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbDelUserDelete_Click(object sender, EventArgs e)
        {
            try
            {
                UserUpdateStatusResponse response = asc.UpdateUserStatus(adminInfo.UserId, adminInfo.CompanyId, ltlDelUserId.Text, -2);
                if (response.Success)
                {
                    mpePop.Hide();
                    upPop.Update();
                    List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                    if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                    }
                    else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                    }
                    else
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                    }
                    lvPerson.DataSource = userList;
                    lvPerson.DataBind();
                    ShowToastMsgAndHideProgressBar(ltlDelUserName.Text + " has been deleted.", MessageUtility.TOAST_TYPE_INFO, false);
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Deleted user failed.", MessageUtility.TOAST_TYPE_ERROR, true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbSusUserSuspend_Click(object sender, EventArgs e)
        {
            try
            {
                UserUpdateStatusResponse response = asc.UpdateUserStatus(adminInfo.UserId, adminInfo.CompanyId, ltlSusUserId.Text, -1);
                if (response.Success)
                {
                    ShowToastMsgAndHideProgressBar(ltlSusUserName1.Text + " has been suspended.", MessageUtility.TOAST_TYPE_INFO, false);
                    mpePop.Hide();
                    upPop.Update();
                    List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                    if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                    }
                    else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                    }
                    else
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                    }
                    lvPerson.DataSource = userList;
                    lvPerson.DataBind();
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Suspended user failed.", MessageUtility.TOAST_TYPE_ERROR, true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ddlChangeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt16(ddlChangeType.SelectedValue) == CassandraService.Entity.User.AccountType.CODE_ADMIN)
                {
                    plChangeTypeModule.Visible = false;
                    plChangeTypeAdminMsg.Visible = true;
                }
                else if (Convert.ToInt16(ddlChangeType.SelectedValue) == CassandraService.Entity.User.AccountType.CODE_MODERATER)
                {
                    plChangeTypeModule.Visible = true;
                    plChangeTypeAdminMsg.Visible = false;
                }
                else
                {
                    plChangeTypeModule.Visible = false;
                    plChangeTypeAdminMsg.Visible = false;
                }

                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "resizeChangeType", " resizePopup($('.changeType'));", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbChangeTypeChange_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data.
                List<int> accessModulesKey = new List<int>();
                if (Convert.ToInt16(ddlChangeType.SelectedValue) == CassandraService.Entity.User.AccountType.CODE_MODERATER)
                {
                    // Get rights for moderater.
                    for (int i = 0; i < cblChangeTypeModule.Items.Count; i++)
                    {
                        if (cblChangeTypeModule.Items[i].Selected)
                        {
                            accessModulesKey.Add(Convert.ToInt16(cblChangeTypeModule.Items[i].Value));
                        }
                    }

                    if (accessModulesKey.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please select a rights for moderator.", MessageUtility.TOAST_TYPE_ERROR, true);
                        upChangeType.Update();
                        return;
                    }
                }

                #region Expired date
                DateTime expiredDate = new DateTime();
                if (rbChangeTypeExpiredDate.Checked)
                {
                    try
                    {
                        expiredDate = DateTime.ParseExact(tbChangeTypeExpiredDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToUniversalTime();
                    }
                    catch (Exception)
                    {
                        ShowToastMsgAndHideProgressBar("Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR, true);
                        upChangeType.Update();
                        return;
                    }
                }
                #endregion
                #endregion

                #region Step 2. Call api.
                UserUpdateTypeResponse response;
                if (rbChangeTypeNoExpiryDate.Checked)
                {
                    response = asc.UpdateUserType(adminInfo.UserId, adminInfo.CompanyId, ltlChangeTypeUserId.Text, Convert.ToInt16(ddlChangeType.SelectedValue), accessModulesKey, null);
                }
                else
                {
                    response = asc.UpdateUserType(adminInfo.UserId, adminInfo.CompanyId, ltlChangeTypeUserId.Text, Convert.ToInt16(ddlChangeType.SelectedValue), accessModulesKey, expiredDate);
                }

                if (response.Success)
                {
                    List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                    if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                    }
                    else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                    }
                    else
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                    }
                    lvPerson.DataSource = userList;
                    lvPerson.DataBind();
                    ShowToastMsgAndHideProgressBar(ltlChangeTypeUserName1.Text + @"'s user type has been changed.", MessageUtility.TOAST_TYPE_INFO, false);
                    upChangeType.Update();
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Changed user failed.", MessageUtility.TOAST_TYPE_ERROR, true);
                    upChangeType.Update();
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbConfirmDelUserDelete_Click(object sender, EventArgs e)
        {
            try
            {
                UserDeleteResponse response = asc.Delete(adminInfo.UserId, adminInfo.CompanyId, ltlConfirmDelUserId.Text);
                if (response.Success)
                {
                    mpePop.Hide();
                    upPop.Update();
                    List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                    if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                    }
                    else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                    }
                    else
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                    }
                    lvPerson.DataSource = userList;
                    lvPerson.DataBind();

                    ShowToastMsgAndHideProgressBar(ltlConfirmDelUserName.Text + " has been deleted.", MessageUtility.TOAST_TYPE_INFO, false);
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Deleted user failed.", MessageUtility.TOAST_TYPE_ERROR, true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                ShowToastMsgAndHideProgressBar("Deleted user failed", MessageUtility.TOAST_TYPE_ERROR, true);
            }
        }

        protected void lbSortByName_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isSortAcsend = Convert.ToBoolean(ViewState["IsSortByNameAcsend"]);
                ViewState["IsSortByNameAcsend"] = !isSortAcsend;
                ViewState["SortBy"] = SORT_BY_NAME;
                List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                }
                else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                }
                else
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                }
                lvPerson.DataSource = userList;
                lvPerson.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbSortByDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isSortAcsend = Convert.ToBoolean(ViewState["IsSortByDepartmentAcsend"]);
                ViewState["IsSortByDepartmentAcsend"] = !isSortAcsend;
                ViewState["SortBy"] = SORT_BY_DEPARTMENT;
                List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                }
                else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                }
                else
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                }
                lvPerson.DataSource = userList;
                lvPerson.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbSortByType_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isSortAcsend = Convert.ToBoolean(ViewState["IsSortByTypeAcsend"]);
                ViewState["IsSortByTypeAcsend"] = !isSortAcsend;
                ViewState["SortBy"] = SORT_BY_TYPE;
                List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                }
                else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                }
                else
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                }
                lvPerson.DataSource = userList;
                lvPerson.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbUpdateUser_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                //HideAllUpdateUserPanel();
                //plUpdateUserBasicInfo.CssClass = "wrap_AddUserBasicInfo";

                String toastMsg = String.Empty;

                // First Name
                if (String.IsNullOrEmpty(tbUpdateFirstName.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Please enter first name.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                if (tbUpdateFirstName.Text.Length > 25)
                {
                    ShowToastMsgAndHideProgressBar("First name cannot be more than 25 characters.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Last Name
                if (String.IsNullOrEmpty(tbUpdateLastName.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Please enter last name.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                if (tbUpdateLastName.Text.Length > 25)
                {
                    ShowToastMsgAndHideProgressBar("Last name cannot be more than 25 characters.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Email
                if (String.IsNullOrEmpty(tbUpdateEmail.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Please enter email address.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                RegexUtility ru = new RegexUtility();
                if (!ru.IsValidEmail(tbUpdateEmail.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("Email address is not valid.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Department
                String departmentTitle = String.Empty;
                TextBox tb = cbUpdateDepartment.FindControl("cbUpdateDepartment_TextBox") as TextBox;
                if (tb != null)
                {
                    if (String.IsNullOrEmpty(tb.Text.Trim()))
                    {
                        ShowToastMsgAndHideProgressBar("Please select department.", MessageUtility.TOAST_TYPE_ERROR, true);
                        return;
                    }
                    else
                    {
                        departmentTitle = tb.Text.Trim();
                    }
                }


                // Job Level
                String jobLevelTitle = String.Empty;
                TextBox cbUpdateJobLevel_tb = cbUpdateJobLevel.FindControl("cbUpdateJobLevel_TextBox") as TextBox;
                if (cbUpdateJobLevel_tb != null)
                {
                    if (String.IsNullOrEmpty(cbUpdateJobLevel_tb.Text.Trim()))
                    {
                        ShowToastMsgAndHideProgressBar("Please select job level.", MessageUtility.TOAST_TYPE_ERROR, true);
                        return;
                    }
                    else
                    {
                        jobLevelTitle = cbUpdateJobLevel_tb.Text.Trim();
                    }
                }

                // Birthday
                DateTime? dtUserBirthday = null;
                try
                {
                    if (!String.IsNullOrEmpty(tbUpdateBDYear.Text.Trim()) && !String.IsNullOrEmpty(tbUpdateBDDay.Text.Trim()))
                    {
                        dtUserBirthday = new DateTime(Convert.ToInt16(tbUpdateBDYear.Text.Trim()), Convert.ToInt16(ddlUpdateBDMonth.SelectedValue), Convert.ToInt16(tbUpdateBDDay.Text.Trim()));
                        DateTime dtMax = DateTime.UtcNow;
                        DateTime dtMin = new DateTime(1900, 1, 1);

                        if (DateTime.Compare(dtUserBirthday.Value, dtMax) > 0 || DateTime.Compare(dtUserBirthday.Value, dtMin) < 0)
                        {
                            ShowToastMsgAndHideProgressBar("Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR, true);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString(), ex);
                    ShowToastMsgAndHideProgressBar("Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }

                // Country
                string countryCode = string.Empty;
                if (ddlUpdatePhoneCountryCode.SelectedItem != null)
                {
                    List<Country> countries = GetCountryList();
                    countryCode = Convert.ToString(countries.Where(c => c.Abb.Equals(ddlUpdatePhoneCountryCode.SelectedItem.Value)).FirstOrDefault().Code);
                }

                #endregion

                #region Step 2. Upload profile image to S3.
                String userId = hfUserId.Value;
                String profileImageUrl = imgUpdateUserPhoto.ImageUrl;

                if (!String.IsNullOrEmpty(hfImgBase64.Value))
                {
                    String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                    String imgBase64String = ImageUtility.FilterBase64String(hfImgBase64.Value), imgFormat = "jpg";
                    IAmazonS3 s3Client = S3Utility.GetIAmazonS3(adminInfo.CompanyId, adminInfo.UserId);
                    using (s3Client)
                    {
                        Dictionary<String, String> metadatas = new Dictionary<string, string>();
                        metadatas.Add("Width", "1080");
                        metadatas.Add("Height", "1080");
                        byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                        String fileName = PROCESS_TIMESTAMP + "_original." + imgFormat.ToLower();

                        // Upload to original and resize bucket.
                        #region Original path: cocarde-{companyId}/users/{userId}.{format}
                        String bucketName = "cocadre-" + adminInfo.CompanyId.ToLower();
                        String folderName = "users/" + userId;
                        S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);

                        profileImageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + fileName;

                        #endregion

                        #region Resize path: cocarde-images-resize-input/{environment}/{companyId}/users/{userId}.{format}
                        bucketName = "cocadre-images-resize-input";
                        folderName = ConfigurationManager.AppSettings["ServerEnvironment"].ToString() + "/" + adminInfo.CompanyId.ToLower() + "/users/" + userId;
                        S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                        #endregion
                    }
                }
                #endregion

                #region Step 3. Call webapi
                UserUpdateResponse userUpdateResponse = asc.UpdateUser(adminInfo.UserId,
                                                                       adminInfo.CompanyId,
                                                                       hfUserId.Value,
                                                                       tbUpdateFirstName.Text.Trim(),
                                                                       tbUpdateLastName.Text.Trim(),
                                                                       tbUpdateEmail.Text.Trim().ToLower(),
                                                                       profileImageUrl,
                                                                       tbUpdatePosition.Text.Trim(),
                                                                       tbUpdatePhone.Text,
                                                                       countryCode,
                                                                       (ddlUpdatePhoneCountryCode.SelectedItem == null) ? String.Empty : ddlUpdatePhoneCountryCode.SelectedItem.Value,
                                                                       tbUpdateAddress.Text.Trim(),
                                                                       (ddlUpdateAddressCountryCode.SelectedItem == null) ? String.Empty : ddlUpdateAddressCountryCode.SelectedItem.Text,
                                                                       tbUpdateAddressPostalCode.Text.Trim(),
                                                                       departmentTitle,
                                                                       jobLevelTitle,
                                                                       Convert.ToInt16(ddlUpdateGender.SelectedValue),
                                                                       dtUserBirthday
                                                                       );
                #endregion

                #region Step 4. Get response.
                hfImgBase64.Value = String.Empty;
                if (userUpdateResponse.Success)
                {
                    ShowToastMsgAndHideProgressBar("User has been updated.", MessageUtility.TOAST_TYPE_INFO, false);
                    List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                    if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                    }
                    else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                    }
                    else
                    {
                        GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                    }
                    lvPerson.DataSource = userList;
                    lvPerson.DataBind();
                    upPop.Update();
                    mpePop.Hide();
                }
                else
                {
                    Log.Error("Created failed. " + userUpdateResponse.ErrorMessage);
                    ShowToastMsgAndHideProgressBar("User failed to update." + userUpdateResponse.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR, true);
                    upPop.Update();
                    mpePop.Show();
                    #region Delete file on S3
                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                ShowToastMsgAndHideProgressBar("User failed to update.", MessageUtility.TOAST_TYPE_ERROR, false);
            }
        }

        protected void lbChangeRightsSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                List<int> accessModulesKey = new List<int>();
                for (int i = 0; i < cblChangeRightsModule.Items.Count; i++)
                {
                    if (cblChangeRightsModule.Items[i].Selected)
                    {
                        accessModulesKey.Add(Convert.ToInt16(cblChangeRightsModule.Items[i].Value));
                    }
                }

                if (accessModulesKey.Count == 0)
                {
                    ShowToastMsgAndHideProgressBar("Please select a rights for moderator.", MessageUtility.TOAST_TYPE_ERROR, true);
                    upChangeRights.Update();
                    upPop.Update();
                    return;
                }

                #region Expired date
                DateTime expiredDate = new DateTime();
                if (rbChangeRightsExpiredDate.Checked)
                {
                    try
                    {
                        expiredDate = DateTime.ParseExact(tbChangeRightsExpiredDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToUniversalTime();
                    }
                    catch (Exception)
                    {
                        ShowToastMsgAndHideProgressBar("Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR, true);
                        upChangeRights.Update();
                        upPop.Update();
                        return;
                    }
                }
                #endregion
                #endregion

                #region Call method
                ModeratorChangeRightsResponse response;
                if (rbChangeRightsNoExpiryDate.Checked)
                {
                    response = asc.ModeratorChangeRights(adminInfo.UserId, adminInfo.CompanyId, hfChangeRightUserId.Value, accessModulesKey, null);
                }
                else
                {
                    response = asc.ModeratorChangeRights(adminInfo.UserId, adminInfo.CompanyId, hfChangeRightUserId.Value, accessModulesKey, expiredDate);
                }

                //ModeratorChangeRightsResponse response = asc.ModeratorChangeRights(adminInfo.UserId, adminInfo.CompanyId, hfChangeRightUserId.Value, accessModulesKey, null);
                if (response.Success)
                {
                    mpePop.Hide();
                    upChangeRights.Update();
                    ShowToastMsgAndHideProgressBar("Moderator rights has been changed.", MessageUtility.TOAST_TYPE_INFO, false);
                    upPop.Update();
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Change rights failed.", MessageUtility.TOAST_TYPE_ERROR, true);
                }

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvPerson_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            try
            {
                // dpPerson.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);

                List<AccountKind> accountKindList = null;
                GetAllAccountKind();

                if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                }
                else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                }
                else
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                }
                lvPerson.DataSource = userList;
                lvPerson.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void btnFilterUser_Click(object sender, EventArgs e)
        {
            try
            {
                Log.Debug("Button1_Click: " + tbFilterUser.Text);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ibFilterUser_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                GetAllAccountKind();
                List<AccountKind> accountKindList = ViewState["accountKindList"] as List<AccountKind>;
                if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 6)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, true, false);
                }
                else if (Convert.ToInt16(ddlFilterUserType.SelectedValue) == 7)
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, 0, 1, false, true);
                }
                else
                {
                    GetUserList(ddlFilterDepartment.SelectedValue, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].TypeCode, accountKindList[Convert.ToInt16(ddlFilterUserType.SelectedValue)].StatusCode, false, false);
                }
                lvPerson.DataSource = userList;
                lvPerson.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvPerson_DataBound(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvPerson_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (userList == null || userList.Count == 0)
                {
                    if (e.Item.ItemType == ListViewItemType.EmptyItem)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        if (ddlFilterDepartment.SelectedIndex > 0 || ddlFilterUserType.SelectedIndex > 0 || !String.IsNullOrEmpty(tbFilterUser.Text.Trim()))
                        {
                            if (String.IsNullOrEmpty(tbFilterUser.Text.Trim()))
                            {
                                ltlEmptyMsg.Text = @"Filter """ + ddlFilterUserType.SelectedItem.Text + @""" + """ + ddlFilterDepartment.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                            }
                            else
                            {
                                ltlEmptyMsg.Text = @"Filter """ + tbFilterUser.Text.Trim() + @""" + """ + ddlFilterUserType.SelectedItem.Text + @""" + """ + ddlFilterDepartment.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                            }
                        }
                        else
                        {
                            ltlEmptyMsg.Text = @"Welcome to CoCadre.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding a user by clicking on the green button.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddMultiUsers_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.PopupControlID = "popup_addmultipleusers";
                mpePop.Show();
                upPop.Update();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnAddMultiUsersUpload_Click(object sender, EventArgs e)
        {
            #region temp
            try
            {
                List<ImportedUser> importUsers = new List<ImportedUser>();
                bool isAllCorrectData = true;
                #region Step 1. Check CSV

                if (ufMultiUsersCsv.HasFile)
                {
                    using (TextFieldParser parser = new TextFieldParser(new StreamReader(ufMultiUsersCsv.PostedFile.InputStream)))
                    {
                        bool checkHeader = true;
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters(",");

                        string[] header = parser.ReadFields();
                        string[] titleFields = { "First Name", "Last Name", "Email", "Department", "Designation", "Job Level", "Gender", "Birthday(YYYY/MM/DD)", "Address Country Name", "Address", "Address Postal Code", "Phone Country Code", "Phone Country Name", "Phone Number", "Password" };
                        if (checkHeader)
                        {
                            #region Check CSV version
                            /*
                             *  The lastest version data format(2017/05/03): 
                                00: First Name
                                01: Last Name
                                02: Email
                                03: Department
                                04: Designation
                                05: Job Level
                                06: Gender
                                07: Birthday(YYYY/MM/DD)
                                08: Address Country Name
                                09: Address
                                10: Address Postal Code
                                11: Phone Country Code
                                12: Phone Country Name
                                13: Phone Number
                                14: Password
                             *
                             */

                            if (header.Length != titleFields.Length)
                            {
                                // The csv file is not latest version
                                ShowToastMsgAndHideProgressBar("Please use the latest sample csv file", MessageUtility.TOAST_TYPE_ERROR, false);
                                return;
                            }
                            else
                            {
                                bool isLatestVersion = true;
                                // Check every title
                                for (int i = 0; i < header.Length; i++)
                                {
                                    if (!header[i].Equals(titleFields[i]))
                                    {
                                        isLatestVersion = false;
                                        break;
                                    }
                                }

                                if (!isLatestVersion)
                                {
                                    // The csv file is not latest version
                                    ShowToastMsgAndHideProgressBar("Please use the latest sample csv file", MessageUtility.TOAST_TYPE_ERROR, false);
                                    return;
                                }
                                else
                                {
                                    checkHeader = false;
                                }
                            }
                            #endregion
                        }

                        int lineNumber = 1;

                        while (!parser.EndOfData)
                        {
                            string[] lineFields = parser.ReadFields();

                            if(lineFields.Length != titleFields.Length)
                            {
                                ShowToastMsgAndHideProgressBar("Please use the latest sample csv file", MessageUtility.TOAST_TYPE_ERROR, false);
                                return;
                            }

                            ImportedUser iu = new ImportedUser();
                            iu.DataErrors = new List<string>();
                            iu.No = lineNumber;

                            #region 0: First Name (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[0]))
                            {
                                iu.DataErrors.Add("No first name entry");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.FirstName = lineFields[0];
                            }
                            #endregion

                            #region 1: Last Name (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[1]))
                            {
                                iu.DataErrors.Add("No last name entry");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.LastName = lineFields[1];
                            }
                            #endregion

                            #region 2: Email
                            // (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[2]))
                            {
                                iu.DataErrors.Add("Email required");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.Email = lineFields[2];
                                // Check Email (Cannot be duplicate in csv file)
                                if (importUsers.Exists(x => x.Email == lineFields[2]))
                                {
                                    iu.DataErrors.Add("Duplicate Email");
                                    isAllCorrectData = false;
                                }
                                else
                                {
                                    // Check Email (Cannot be duplicate in database)
                                    AuthenticationCheckEmailForCompanyResponse response = asc.CheckEmailExistsForCompany(adminInfo.UserId, adminInfo.CompanyId, lineFields[2]);
                                    if (response.Success && response.IsEmailExists)
                                    {
                                        iu.DataErrors.Add("Email exists in system");
                                        isAllCorrectData = false;
                                    }
                                }
                            }
                            #endregion

                            #region 3: Department (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[3]))
                            {
                                iu.DataErrors.Add("Department required");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.Department = lineFields[3];
                            }
                            #endregion

                            #region 4: Designation
                            if (!string.IsNullOrEmpty(lineFields[4]))
                            {
                                iu.Designation = lineFields[4];
                            }
                            #endregion

                            #region 5: Job Level
                            if (!string.IsNullOrEmpty(lineFields[5]))
                            {
                                iu.JobLevel = lineFields[5];
                            }
                            #endregion

                            #region 6: Gender
                            try
                            {
                                if (lineFields[6].ToLower().Equals("male"))
                                {
                                    iu.Gender = 1;
                                }
                                else if (lineFields[6].ToLower().Equals("famale"))
                                {
                                    iu.Gender = 2;
                                }
                                else
                                {
                                    iu.Gender = 0;
                                }
                            }
                            catch (Exception)
                            {
                                iu.Gender = 0;
                            }
                            #endregion

                            #region 7: Birthday(YYYY/MM/DD)
                            if (!string.IsNullOrEmpty(lineFields[7]))
                            {
                                try
                                {
                                    iu.Birthday = DateTime.ParseExact(lineFields[7], "yyyy/MM/dd", CultureInfo.InvariantCulture);
                                }
                                catch (Exception)
                                {
                                    iu.DataErrors.Add("Birthday is invalid");
                                    iu.Birthday = new DateTime(1980, 1, 1);
                                    isAllCorrectData = false;
                                }
                            }
                            #endregion

                            #region 8: Address Country Name
                            if (!string.IsNullOrEmpty(lineFields[8]))
                            {
                                iu.AddressCountryName = lineFields[8];
                            }
                            #endregion

                            #region 9: Address
                            if (!string.IsNullOrEmpty(lineFields[9]))
                            {
                                iu.Address = lineFields[9];
                            }
                            #endregion

                            #region 10: Address Postal Code
                            if (!string.IsNullOrEmpty(lineFields[10]))
                            {
                                iu.AddressPostalCode = lineFields[10];
                            }
                            #endregion

                            #region 11: Phone Country Code
                            if (!string.IsNullOrEmpty(lineFields[11]))
                            {
                                iu.PhoneCountryCode = lineFields[11];
                            }
                            #endregion

                            #region 12: Phone Country Name
                            if (!string.IsNullOrEmpty(lineFields[12]))
                            {
                                iu.PhoneCountryName = lineFields[12];
                            }
                            #endregion

                            #region 13: Phone Number
                            if (!string.IsNullOrEmpty(lineFields[13]))
                            {
                                iu.PhoneNumber = lineFields[13];
                            }
                            #endregion

                            #region 14: Password
                            iu.Password = null;
                            if (!string.IsNullOrEmpty(lineFields[14]))
                            {
                                iu.Password = lineFields[14];
                            }
                            #endregion

                            importUsers.Add(iu);
                            lineNumber++;

                        }
                    }

                    if (importUsers.Count > 0)
                    {
                        Session["MultipleUsersToAdd"] = importUsers;
                        Session["IsMultipleUsersDataCorrect"] = isAllCorrectData;
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Personnel/AddMultiUsersReport/', 0);", true);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("This is an empty csv file", MessageUtility.TOAST_TYPE_ERROR, false);
                    }
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Please select a csv file", MessageUtility.TOAST_TYPE_ERROR, false);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
                ShowToastMsgAndHideProgressBar("Please use the latest sample csv file", MessageUtility.TOAST_TYPE_ERROR, false);
            }
            #endregion

            #region By Zhixian
            //bool checkHeader = true;
            //string current_line = string.Empty;
            //List<string[]> toAddList = new List<string[]>();
            //List<ErrorEntry> errorList = new List<ErrorEntry>();
            //string[] lineFields;
            //int recordNumber = 1;

            //try
            //{
            //    if (ufMultiUsersCsv.HasFile)
            //    {
            //        lblMultiUserCsvPath.Text = ufMultiUsersCsv.FileName;

            //        using (System.IO.StreamReader sr = new StreamReader(ufMultiUsersCsv.PostedFile.InputStream))
            //        {
            //            while (!sr.EndOfStream)
            //            {
            //                #region Step 1. Check csv file.
            //                // First row is suppose to field headers;
            //                // Check to make sure field headers are in order
            //                if (checkHeader)
            //                {
            //                    current_line = sr.ReadLine();
            //                    checkHeader = false;
            //                    continue;
            //                }
            //                #endregion

            //                #region Step 2. Upload csv file. Save at server: "TempFolder\MutliUsers\{yyyyMMddHHmmsss}.csv".

            //                // Read line
            //                current_line = sr.ReadLine();

            //                // Parse to fields
            //                lineFields = current_line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            //                // Check validity of data
            //                // SKIP FOR NOW

            //                // Check if exists
            //                errorList.Add(new ErrorEntry
            //                {
            //                    CsvNo = recordNumber,
            //                    Name = string.Format("{0} {1}", lineFields[0], lineFields[1]),
            //                    Email = lineFields[2],
            //                    Department = string.Empty,
            //                    ErrorsHtml = "some errors<br/>some error 2<br/>some error 3"
            //                });

            //                // if exists add to toAddList
            //                // else add to errorList
            //                toAddList.Add(lineFields);

            //                #endregion

            //                recordNumber++;
            //            }
            //        }

            //        // Check if have error entries
            //        if (errorList.Count > 0)
            //        {
            //            lvErrorUpload.DataSource = errorList;
            //            lvErrorUpload.DataBind();

            //            // Display error message
            //            UpdatePanel3.Visible = false;
            //            FailedUserUploadPanel.Visible = true;
            //            mpePop.Hide();
            //            upPop.Update();

            //        }
            //        else
            //        {
            //            // ZX: There is no errors. Store the toAddList into Session
            //            Session["MultipleUsersToAdd"] = toAddList;

            //            // Change to add multiple users ready modal dialog
            //            mpePop.PopupControlID = "popup_addmultipleusersready";
            //            mpePop.Show();
            //            upPop.Update();
            //        }

            //        #region Step 3. Read cvs file and write to db row by row.
            //        // IGNORE; We will do this part in the "ready to go" step.
            //        #endregion

            //        #region Step 4. Delete the csv file.
            //        // IGNORE; No CSV file is stored on server. 
            //        // HOWEVER We DO need to take note to free up the session variable after its contents is processed.
            //        #endregion

            //    } // end if (ufMultiUsersCsv.HasFile)
            //    else
            //    {
            //        lblMultiUserCsvPath.Text = "Please choose a file";
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Log.Error(ex.ToString(), ex, this.Page);
            //}
            #endregion
        }

        protected void btnReadyToAddMultiUsersUpload_Click(object sender, EventArgs e)
        {
            // Do some processing here to store uploaded information into database
            UpdatePanel3.Visible = true;
            FailedUserUploadPanel.Visible = false;

            ShowToastMsgAndHideProgressBar("1382 Personnel has been created.", MessageUtility.TOAST_TYPE_INFO, false);

            upPop.Update();

        }
    } // end public partial class Personnel : AutoCompleteSearchUser

    public class ErrorEntry
    {
        public int CsvNo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string ErrorsHtml { get; set; }

        //public ErrorEntry()
        //{
        //    this.Errors = new List<string>();
        //}
    }
}
