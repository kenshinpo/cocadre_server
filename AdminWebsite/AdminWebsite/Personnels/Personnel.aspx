﻿<%@ Page Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Personnel.aspx.cs" Inherits="AdminWebsite.Personnels.Personnel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <%--    <link rel="stylesheet" href="/css/Gamification/croppie.css" />
    <script src="/js/Gamification/croppie.js"></script>--%>
</asp:Content>
<asp:Content ID="personnel_content" ContentPlaceHolderID="main_content" runat="server">
    <%--<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>--%>
    <script src="/JS/cropbox.js"></script>
    <script type="text/javascript">

        $(function () {
            initJsFunction();
            initChips();
        });

        function initJsFunction() {
            ReloadAddUserPopup(); // @ apo-min.js
            ReloadUpdateUserPopup(); // @ apo-min.js

            // for AddUser crop image
            var options =
            {
                thumbBox: '.crop-imagebox__imageBox__thumbBox',
                spinner: '.crop-imagebox__imageBox__spinner',
                imgSrc: 'avatar.png'
            }
            var cropper;

            $('#ufAddUserPhoto').on('change', function () {
                ReloadErrorToast();
                if (this.files && this.files[0]) {
                    var type = this.files[0].type;
                    var size = this.files[0].size / (1024 * 1024);

                    if (type.toLowerCase() == "image/png" || type.toLowerCase() == "image/jpeg" || type.toLowerCase() == "image/jpg") {
                        if (size > 5) {
                            toastr.error('Uploaded photo exceeded 5MB.');
                        }
                        else {
                            //var thumbBox = $('.crop-imagebox__imageBox__thumbBox');
                            //thumbBox.removeAttr("style");
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                //options.imgSrc = e.target.result;
                                //cropper = $('.crop-imagebox__imageBox').cropbox(options);


                                var crop = $('#AddUserUploadCrop');
                                crop.html("");
                                crop.croppie({
                                    enableExif: true,
                                    viewport: {
                                        width: 216,
                                        height: 216
                                    },
                                    boundary: {
                                        width: 350,
                                        height: 350
                                    },
                                    url: e.target.result
                                });
                                $('.cr-slider').css("display", "inline-block");
                            }
                            reader.readAsDataURL(this.files[0]);

                            $('.wrap_AddUserPhotoUpload').show();
                            $('.wrap_AddUserPhotoUrl').hide();
                            $('.wrap_AddUserBasicInfo').hide();
                            $('.wrap_AddUserExtraInfo').hide();
                            $('.wrap_AddUserPhotoType').hide();
                            $('.wrap_AddUserSuccess').hide();

                            resizePopup($('.wrap_AddUserPhotoUpload'));

                            $('#ufAddUserPhoto').val("");
                        }
                    }
                    else {
                        toastr.error('File extension is invalid.');
                    }
                }
            })

            $('#btnAddUserCrop').on('click', function () {
                var crop = $('#AddUserUploadCrop');
                crop.croppie('result', {
                    type: 'canvas',
                    format: 'viewport'
                }).then(function (resp) {

                    var imgAddUserPhoto = $get("<%=imgAddUserPhoto.ClientID%>");
                    imgAddUserPhoto.src = resp;
                    $get("<%=hfImgBase64.ClientID%>").setAttribute('value', resp);

                    $('.wrap_AddUserBasicInfo').show();
                    $('.wrap_AddUserExtraInfo').hide();
                    $('.wrap_AddUserPhotoType').hide();
                    $('.wrap_AddUserPhotoUrl').hide();
                    $('.wrap_AddUserPhotoUpload').hide();
                    $('.wrap_AddUserSuccess').hide();

                    resizePopup($('.wrap_AddUserBasicInfo'));
                });
            })
            // for AddUser crop image

            // for UpdateUser crop image
            $('#ufUpdateUserPhoto').on('change', function () {
                ReloadErrorToast();
                if (this.files != null && this.files[0] != null) {
                    var type = this.files[0].type;
                    var size = this.files[0].size / (1024 * 1024);

                    if (type.toLowerCase() == "image/png" || type.toLowerCase() == "image/jpeg" || type.toLowerCase() == "image/jpg") {
                        if (size > 5) {
                            toastr.error('Uploaded photo exceeded 5MB.');
                        }
                        else {
                            //var thumbBox = $('.crop-imagebox__imageBox__thumbBox');
                            //thumbBox.removeAttr("style");
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                //options.imgSrc = e.target.result;
                                //cropper = $('.crop-imagebox__imageBox').cropbox(options);

                                var crop = $('#UpdateUserUploadCrop');
                                crop.html("");
                                crop.croppie({
                                    enableExif: true,
                                    viewport: {
                                        width: 216,
                                        height: 216
                                    },
                                    boundary: {
                                        width: 350,
                                        height: 350
                                    },
                                    url: e.target.result
                                });
                                $('.cr-slider').css("display", "inline-block");
                            }
                            reader.readAsDataURL(this.files[0]);

                            $('.wrap_UpdateUserPhotoUpload').show();
                            $('.wrap_UpdateUserPhotoUrl').hide();
                            $('.wrap_UpdateUserBasicInfo').hide();
                            $('.wrap_UpdateUserExtraInfo').hide();
                            $('.wrap_UpdateUserPhotoType').hide();
                            resizePopup($('.wrap_UpdateUserPhotoUpload'));

                            $('#ufUpdateUserPhoto').val("");
                        }
                    }
                    else {
                        toastr.error('File extension is invalid.');
                    }
                }
            })

            $('#btnUpdateUserCrop').on('click', function () {

                var crop = $('#UpdateUserUploadCrop');
                crop.croppie('result', {
                    type: 'canvas',
                    format: 'viewport'
                }).then(function (resp) {

                    var imgUpdateUserPhoto = $get("<%=imgUpdateUserPhoto.ClientID%>");
                    imgUpdateUserPhoto.src = resp;
                    $get("<%=hfImgBase64.ClientID%>").setAttribute('value', resp);

                    $('.wrap_UpdateUserBasicInfo').show();
                    $('.wrap_UpdateUserPhotoUpload').hide();
                    $('.wrap_UpdateUserPhotoUrl').hide();
                    $('.wrap_UpdateUserExtraInfo').hide();
                    $('.wrap_UpdateUserPhotoType').hide();
                    resizePopup($('.wrap_UpdateUserBasicInfo'));
                });

                <%--var img = cropper.getDataURL();
                var imgUpdateUserPhoto = $get("<%=imgUpdateUserPhoto.ClientID%>");
                imgUpdateUserPhoto.src = img;

                $get("<%=hfImgBase64.ClientID%>").setAttribute('value', img);--%>
            })

            //$('#btnUpdateUserZoomIn').on('click', function () {
            //    cropper.zoomIn();
            //})

            //$('#btnUpdateUserZoomOut').on('click', function () {
            //    cropper.zoomOut();
            //})
            // for AddUser crop image

            //ufMultiUsersCsv
            $('#<%=ufMultiUsersCsv.ClientID%>').on('change', function () {
                $('#<%=lblMultiUserCsvPath.ClientID%>').text("Selected File: " + this.value);
            });
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initJsFunction();
            initChips();
        })
    </script>


    <asp:UpdatePanel ID="upPop" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>

            <asp:HiddenField ID="hfImgBase64" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <asp:LinkButton ID="lbAddUser" runat="server" CssClass="mfb-component__button--main" data-mfb-label="Add User" OnClick="lbAddUser_Click">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-user-plus"></i>
                    </asp:LinkButton>
                    <ul class="mfb-component__list">
                        <li>
                            <asp:LinkButton ID="lbAddMultiUsers" runat="server" data-mfb-label="Add Multiple Users" CssClass="mfb-component__button--child orange" OnClick="lbAddMultiUsers_Click">
                                <i class="mfb-component__child-icon fa fa-users"></i>
                            </asp:LinkButton>
                        </li>

                        <%--
                        <li>
                            <a href="#popup_addadmin" data-mfb-label="Add Admin" class="mfb-component__button--child popup-with-form orange">
                                <i class="mfb-component__child-icon fa fa-user-secret"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#popup_addmoderator" data-mfb-label="Add Moderator" class="mfb-component__button--child popup-with-form blue">
                                <i class="mfb-component__child-icon fa fa-user-md"></i>
                            </a>
                        </li>
                        --%>
                    </ul>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Add User Form -->
            <asp:Panel ID="popup_adduser" runat="server" CssClass="popup popup--adduser" Width="100%" Style="display: none;">
                <asp:Panel ID="plAddUserBasicInfo" runat="server" CssClass="wrap_AddUserBasicInfo">
                    <h1 class="popup__title">Create a new user</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <div class="main">
                                    <div class="form__row">
                                        <asp:TextBox ID="tbAddFirstName" runat="server" placeholder="First Name" MaxLength="25" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbAddLastName" runat="server" placeholder="Last Name" MaxLength="25" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbAddEmail" runat="server" placeholder="Email Address" TextMode="Email" MaxLength="100" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="label">Department</div>
                                    <div class="form__row">
                                        <ajaxToolkit:ComboBox ID="cbAddDepartment" runat="server" AutoCompleteMode="SuggestAppend" CssClass="combos" MaxLength="30" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbAddPosition" runat="server" placeholder="Designation" MaxLength="25" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="label">Job Level</div>
                                    <div class="form__row">
                                        <ajaxToolkit:ComboBox ID="cbJobLevel" runat="server" AutoCompleteMode="SuggestAppend" CssClass="combos" MaxLength="30" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                </div>
                                <div class="side">
                                    <div class="upload">
                                        <label for="ufAddUserPhoto" style="cursor: pointer;">
                                            <div class="constrained" style="border-radius: 0%;">
                                                <asp:Image ID="imgAddUserPhoto" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/profile/default_profile_photo_original.png" runat="server" Style="border-radius: 0%;" />
                                            </div>
                                            <span class="upload__text" style="color: #4683ea;">
                                                <input type="file" id="ufAddUserPhoto" accept="image/*" title="Add photo" style="display: none" />
                                                Add photo
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <p class="clear"><small>Temporary password will be assigned</small></p>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <%--<a class="popup__action__item popup__action__item--others open-AddUserExtraInfo">Additional Info</a>
                        <asp:LinkButton ID="lbAddUserCreate" runat="server" OnClick="lbAddUserCreate_Click" CssClass="popup__action__item popup__action__item--cta open-AddUserBasicInfo" OnClientClick="ShowProgressBar();" Text="Create" />--%>

                        <a class="popup__action__item popup__action__item--cta open-AddUserExtraInfo">Next</a>
                        <asp:LinkButton ID="lbAddUserCancelBasic" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="plAddUserExtraInfo" runat="server" CssClass="wrap_AddUserExtraInfo hide">
                    <h1 class="popup__title">Create a new user</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <div class="main">

                                    <!-- Add Gender and Birthday -->
                                    <div class="label">Gender</div>
                                    <div class="form__row">
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlAddGender" runat="server">
                                                <asp:ListItem Text="Male" Value="1" Selected="True" />
                                                <asp:ListItem Text="Female" Value="2" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="label">Birthday</div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbAddBDDay" runat="server" class="day" placeholder="Day" MaxLength="2" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlAddBDMonth" runat="server">
                                                <asp:ListItem Text="1" Value="1" Selected="True" />
                                                <asp:ListItem Text="2" Value="2" />
                                                <asp:ListItem Text="3" Value="3" />
                                                <asp:ListItem Text="4" Value="4" />
                                                <asp:ListItem Text="5" Value="5" />
                                                <asp:ListItem Text="6" Value="6" />
                                                <asp:ListItem Text="7" Value="7" />
                                                <asp:ListItem Text="8" Value="8" />
                                                <asp:ListItem Text="9" Value="9" />
                                                <asp:ListItem Text="10" Value="10" />
                                                <asp:ListItem Text="11" Value="11" />
                                                <asp:ListItem Text="12" Value="12" />
                                            </asp:DropDownList>
                                        </div>
                                        <asp:TextBox ID="tbAddBDYear" runat="server" class="year" placeholder="Year" MaxLength="4" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <!-- /Add Gender and Birthday -->

                                    <div class="label">Additional Info</div>
                                    <div class="label-small">Phone</div>
                                    <div class="form__row">
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlAddPhoneCountryCode" runat="server" CssClass="areacode" Style="width: auto;" />
                                        </div>
                                        <asp:TextBox ID="tbAddPhone" runat="server" placeholder="Phone" MaxLength="15" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row mdl-selectfield" style="display: none;">
                                        <asp:DropDownList ID="ddlAddAddressCountryCode" runat="server" />
                                    </div>
                                    <div class="form__row" style="display: none;">
                                        <asp:TextBox ID="tbAddAddress" runat="server" placeholder="Street Address" MaxLength="100" onkeydown="return (event.keyCode!=13);" />
                                        <asp:TextBox ID="tbAddAddressPostalCode" runat="server" placeholder="Postal Code" MaxLength="10" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <asp:LinkButton ID="lbAddUserCreate" runat="server" OnClick="lbAddUserCreate_Click" CssClass="popup__action__item popup__action__item--cta open-AddUserBasicInfo" OnClientClick="ShowProgressBar();" Text="Create" />
                        <asp:LinkButton ID="lbAddUserCancelExtra" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                        <a class="popup__action__item popup__action__item--cta open-AddUserBasicInfo">Back</a>
                    </div>
                </asp:Panel>
                <asp:Panel ID="plAddUserPhotoType" runat="server" CssClass="wrap_AddUserPhotoType hide">
                    <h1 class="popup__title">Add Photo</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <a class="btn popup--addphoto__local open-AddUserPhotoUpload"><i class="fa fa-plus"></i>Upload Photo</a>
                            </div>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <a class="popup__action__item open-AddUserBasicInfo">Back to Previous</a>
                    </div>
                </asp:Panel>
                <asp:Panel ID="plAddUserPhotoUrl" runat="server" CssClass="wrap_AddUserPhotoUrl hide">
                    <h1 class="popup__title">Add Photo</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <a class="btn secondary popup--addphoto__web"><i class="fa fa-globe"></i>Add Photo from Web</a>
                                <div class="form__row popup--addphoto__web__url">
                                    <div class="main">
                                        <input id="a10" type="url" placeholder="Paste a URL" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <asp:LinkButton ID="lbAddUserPhotoUrlSave" runat="server" CssClass="popup__action__item popup__action__item--cta">Save</asp:LinkButton>
                        <a class="popup__action__item popup__action__item--others open-AddUserBasicInfo">Back to Previous</a>
                    </div>
                </asp:Panel>


                <asp:Panel ID="plAddUserPhotoUpload" runat="server" CssClass="wrap_AddUserPhotoUpload hide" Height="700">
                    <h1 class="popup__title">Crop Photo</h1>
                    <div class="popup__content">
                        <div class="container">
                            <div class="photocrop">
                                <div id="AddUserUploadCrop"></div>
                                <!--<div class="crop-imagebox__imageBox">
                                    <div class="crop-imagebox__imageBox__thumbBox" style="background-image: url(https://s3-ap-southeast-1.amazonaws.com/cocadre/profile/default_profile_photo_original.png); background-size: cover;"></div>
                                    <div class="crop-imagebox__imageBox__spinner" style="display: none">Loading...</div>
                                </div>
                                <div class="crop-imagebox__action">
                                    <input type="button" id="btnAddUserZoomIn" value="+" style="margin: 2px; float: right;">
                                    <input type="button" id="btnAddUserZoomOut" value="-" style="margin: 2px; float: right;">
                                </div>
                                <div class="cropped">
                                </div>-->
                            </div>
                        </div>
                        <div class="label">
                            For faster upload, reduce the file size to 1MB and lower.<br />
                            Uploaded photo should not be bigger than 5MB.
                        </div>

                        <p class="error">
                            <asp:Literal ID="ltlAddUserPhotoError" runat="server" />
                        </p>
                    </div>
                    <div class="popup__action" style="position: absolute; bottom: 0px; width: 100%;">
                        <a id="btnAddUserCrop" class="popup__action__item popup__action__item--cta" style="right: 0px;">CROP & SAVE</a>
                        <a class="popup__action__item popup__action__item--others open-AddUserBasicInfo">Back to Previous</a>
                    </div>
                </asp:Panel>


                <asp:Panel ID="plAddUserSuccess" runat="server" CssClass="wrap_AddUserSuccess hide">
                    <h1 class="popup__title">Getting Started Instructions</h1>
                    <div class="popup__content">
                        <div class="container">
                            <p>
                                The personnel's login information has been sent to
                            <asp:Literal ID="ltlAdduserSuccessEmail" runat="server" Text="example@company.com" />
                            </p>
                        </div>
                    </div>
                    <div class="popup__action">
                        <asp:LinkButton ID="lbAddUserSuccess" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" OnClick="lbAddUserSuccess_Click" Text="Done" />
                    </div>
                </asp:Panel>
            </asp:Panel>
            <!-- / Add User Form -->

            <!-- Update User Form -->
            <asp:Panel ID="popup_updateuser" runat="server" CssClass="popup popup--updateuser" Width="100%" Style="display: none;">
                <asp:Panel ID="plUpdateUserBasicInfo" runat="server" CssClass="wrap_UpdateUserBasicInfo">
                    <asp:HiddenField ID="hfUserId" runat="server" />
                    <h1 class="popup__title">Update User</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <div class="main">
                                    <div class="form__row">
                                        <asp:TextBox ID="tbUpdateFirstName" runat="server" placeholder="First Name" MaxLength="25" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbUpdateLastName" runat="server" placeholder="Last Name" MaxLength="25" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbUpdateEmail" runat="server" placeholder="Email Address" TextMode="Email" MaxLength="100" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="label">Department</div>
                                    <div class="form__row">
                                        <ajaxToolkit:ComboBox ID="cbUpdateDepartment" runat="server" AutoCompleteMode="SuggestAppend" CssClass="combos" MaxLength="30" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbUpdatePosition" runat="server" placeholder="Designation" MaxLength="25" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="label">Job Level</div>
                                    <div class="form__row">
                                        <ajaxToolkit:ComboBox ID="cbUpdateJobLevel" runat="server" AutoCompleteMode="SuggestAppend" CssClass="combos" MaxLength="30" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                </div>
                                <div class="side">
                                    <div class="upload">
                                        <label for="ufUpdateUserPhoto" style="cursor: pointer;">
                                            <div class="constrained" style="border-radius: 0%;">
                                                <asp:Image ID="imgUpdateUserPhoto" runat="server" Style="border-radius: 0%;" />
                                            </div>
                                            <span class="upload__text" style="color: #4683ea;">
                                                <input type="file" id="ufUpdateUserPhoto" accept="image/*" title="Add photo" style="display: none" />
                                                Add photo
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <%-- <a class="popup__action__item popup__action__item--others  open-UpdateUserExtraInfo">Additional Info</a>
                        <asp:LinkButton ID="lbUpdateUser" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" OnClick="lbUpdateUser_Click" OnClientClick="ShowProgressBar();" Text="Update" />--%>
                        <a class="popup__action__item popup__action__item--cta  open-UpdateUserExtraInfo">Next</a>
                        <asp:LinkButton ID="lbUpdateUserCancelBasic" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="plUpdateUserExtraInfo" runat="server" CssClass="wrap_UpdateUserExtraInfo hide">
                    <h1 class="popup__title">Update User</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <div class="main">

                                    <!-- Add Gender and Birthday -->
                                    <div class="label">Gender</div>
                                    <div class="form__row">
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlUpdateGender" runat="server">
                                                <asp:ListItem Text="Male" Value="1" />
                                                <asp:ListItem Text="Female" Value="2" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="label">Birthday</div>
                                    <div class="form__row">
                                        <asp:TextBox ID="tbUpdateBDDay" class="day" runat="server" placeholder="Day" MaxLength="2" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlUpdateBDMonth" runat="server">
                                                <asp:ListItem Text="1" Value="1" />
                                                <asp:ListItem Text="2" Value="2" />
                                                <asp:ListItem Text="3" Value="3" />
                                                <asp:ListItem Text="4" Value="4" />
                                                <asp:ListItem Text="5" Value="5" />
                                                <asp:ListItem Text="6" Value="6" />
                                                <asp:ListItem Text="7" Value="7" />
                                                <asp:ListItem Text="8" Value="8" />
                                                <asp:ListItem Text="9" Value="9" />
                                                <asp:ListItem Text="10" Value="10" />
                                                <asp:ListItem Text="11" Value="11" />
                                                <asp:ListItem Text="12" Value="12" />
                                            </asp:DropDownList>
                                        </div>
                                        <asp:TextBox ID="tbUpdateBDYear" class="year" runat="server" placeholder="Year" MaxLength="4" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <!-- /Add Gender and Birthday -->

                                    <div class="label">Additional Info</div>
                                    <div class="label-small">Phone</div>
                                    <div class="form__row">
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlUpdatePhoneCountryCode" runat="server" Style="width: auto;" />
                                        </div>
                                        <asp:TextBox ID="tbUpdatePhone" runat="server" placeholder="Phone" MaxLength="15" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                    <div class="form__row mdl-selectfield" style="display: none;">
                                        <asp:DropDownList ID="ddlUpdateAddressCountryCode" runat="server" />
                                    </div>
                                    <div class="form__row" style="display: none;">
                                        <asp:TextBox ID="tbUpdateAddress" runat="server" placeholder="Street Address" MaxLength="100" onkeydown="return (event.keyCode!=13);" />
                                        <asp:TextBox ID="tbUpdateAddressPostalCode" runat="server" placeholder="Postal Code" MaxLength="10" onkeypress="return allowOnlyNumber(event);" onkeydown="return (event.keyCode!=13);" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <asp:LinkButton ID="lbUpdateUser" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" OnClick="lbUpdateUser_Click" OnClientClick="ShowProgressBar();" Text="Update" />
                        <asp:LinkButton ID="lbUpdateUserCancelExtra" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                        <a class="popup__action__item popup__action__item--cta open-UpdateUserBasicInfo">Back</a>
                    </div>
                </asp:Panel>
                <asp:Panel ID="plUpdateUserPhotoType" runat="server" CssClass="wrap_UpdateUserPhotoType hide">
                    <h1 class="popup__title">Add Photo</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <a class="btn popup--addphoto__local open-UpdateUserPhotoUpload"><i class="fa fa-plus"></i>Upload Photo</a>
                            </div>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <a class="popup__action__item open-UpdateUserBasicInfo">Back to Previous</a>
                    </div>
                </asp:Panel>
                <asp:Panel ID="plUpdateUserPhotoUrl" runat="server" CssClass="wrap_UpdateUserPhotoUrl hide">
                    <h1 class="popup__title">Add Photo</h1>
                    <div class="popup__content">
                        <fieldset class="form">
                            <div class="container">
                                <a class="btn secondary popup--addphoto__web"><i class="fa fa-globe"></i>Add Photo from Web</a>
                                <div class="form__row popup--addphoto__web__url">
                                    <div class="main">
                                        <input id="a11" type="url" placeholder="Paste a URL" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="popup__action">
                        <asp:LinkButton ID="lbUpdateUserPhotoUrlSave" runat="server" CssClass="popup__action__item popup__action__item--cta" Text="Save" />
                        <a class="popup__action__item popup__action__item--others open-UpdateUserBasicInfo">Back to Previous</a>
                    </div>
                </asp:Panel>
                <asp:Panel ID="plUpdateUserPhotoUpload" runat="server" CssClass="wrap_UpdateUserPhotoUpload hide" Height="700">
                    <h1 class="popup__title">Crop Photo</h1>
                    <div class="popup__content">
                        <div class="container">
                            <div class="photocrop">
                                <div id="UpdateUserUploadCrop"></div>
                                <!--<div class="crop-imagebox__imageBox">
                                    <div class="crop-imagebox__imageBox__thumbBox" style="background-image: url(https://s3-ap-southeast-1.amazonaws.com/cocadre/profile/default_profile_photo_original.png); background-size: cover;"></div>
                                    <div class="crop-imagebox__imageBox__spinner" style="display: none">Loading...</div>
                                </div>
                                <div class="crop-imagebox__action">
                                    <input type="button" id="btnUpdateUserZoomIn" value="+" style="margin: 2px; float: right;">
                                    <input type="button" id="btnUpdateUserZoomOut" value="-" style="margin: 2px; float: right;">
                                </div>-->
                            </div>
                        </div>
                        <div class="label">
                            For faster upload, reduce the file size to 1MB and lower.<br />
                            Uploaded photo should not be bigger than 5MB.
                        </div>

                        <p class="error">
                            <asp:Literal ID="ltlUpdateUserPhotoError" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div class="popup__action" style="position: absolute; bottom: 0px; width: 100%;">
                        <a id="btnUpdateUserCrop" class="popup__action__item popup__action__item--cta" style="right: 0px;">CROP & SAVE</a>
                        <a class="popup__action__item popup__action__item--others open-UpdateUserBasicInfo">Back to Previous</a>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <!-- / Update User Form -->

            <!-- Delete User Form -->
            <asp:Panel ID="popup_deleteuser" runat="server" CssClass="popup popup--deleteuser" Width="100%" Style="display: none;">
                <asp:Literal ID="ltlDelUserId" runat="server" Visible="false" />
                <h1 class="popup__title">Delete
                    <asp:Literal ID="ltlDelUserName" runat="server" />
                </h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                Confirm deletion of <strong>
                                    <asp:Literal ID="ltlDelUserName2" runat="server" /></strong>?
                            </p>
                            <div class="Media">
                                <div class="image-holder">
                                    <asp:Image ID="imgDelUser" CssClass="Media-figure" runat="server" />
                                </div>
                                <div class="Media-body">
                                    <asp:Literal ID="ltlDelUserName3" runat="server" />
                                </div>
                            </div>
                            <p class="clear">
                                <i class="fa fa-info-circle color-blue"></i>
                                All data associated with this account will be automatically deleted after 7 days.
                            </p>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDelUserDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbDelUserDelete_Click" Text="DELETE" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbDelUserCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="CANCEL" />
                </div>
            </asp:Panel>
            <!-- / Delete User Form -->

            <!-- Confirm Delete User Form -->
            <asp:Panel ID="popup_confirmdeleteuser" runat="server" CssClass="popup popup--deleteuser" Width="100%" Style="display: none;">
                <asp:Literal ID="ltlConfirmDelUserId" runat="server" Visible="false" />
                <h1 class="popup__title">Delete
                    <asp:Literal ID="ltlConfirmDelUserName" runat="server" /></h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                Confirm deletion of <strong>
                                    <asp:Literal ID="ltlConfirmDelUserName2" runat="server" /></strong>?
                            </p>
                            <div class="Media">
                                <div class="image-holder">
                                    <asp:Image ID="imgConfirmDelUser" CssClass="Media-figure" runat="server" />
                                </div>
                                <div class="Media-body">
                                    <asp:Literal ID="ltlConfirmDelUserName3" runat="server" />
                                </div>
                            </div>
                            <p class="clear">
                                <i class="fa fa-info-circle color-blue"></i>
                                All data associated with this account will be deleted and can never be restored.
                            </p>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbConfirmDelUserDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbConfirmDelUserDelete_Click" Text="DELETE" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbConfirmDelUserCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="CANCEL" />
                </div>
            </asp:Panel>
            <!-- / Confirm Delete User Form -->

            <!-- Suspend User Form -->
            <asp:Panel ID="popup_suspenduser" runat="server" CssClass="popup popup--suspenduser" Width="100%" Style="display: none;">
                <asp:Literal ID="ltlSusUserId" runat="server" Visible="false" />
                <h1 class="popup__title">Suspend
                    <asp:Literal ID="ltlSusUserName1" runat="server" /></h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <p>This user will not be able to:</p>
                            <ul class="normal suspension-reasons">
                                <li>Login to Cocadre
                                    <asp:Literal ID="ltlSusCompanyName" runat="server" />
                                    account</li>
                            </ul>
                            <div class="Media">
                                <div class="image-holder">
                                    <asp:Image ID="imgSusUser" CssClass="Media-figure" runat="server" />
                                </div>
                                <div class="Media-body">
                                    <asp:Literal ID="ltlSusUserName2" runat="server" />
                                </div>
                            </div>
                            <p>You will still be able to activate this user later.</p>
                            <p class="error">User license fee still apply to suspended users.</p>

                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbSusUserSuspend" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" OnClick="lbSusUserSuspend_Click" Text="SUSPEND" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbSusUserCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="CANCEL" />
                </div>
            </asp:Panel>
            <!-- / Suspend User Form -->

            <!-- Change User Type Form -->
            <asp:Panel ID="popup_changeaccessrights" runat="server" CssClass="popup popup--changeaccessrights" Width="100%" Style="display: none;">
                <asp:UpdatePanel ID="upChangeType" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" class="changeType">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlChangeType" />
                    </Triggers>
                    <ContentTemplate>
                        <asp:Literal ID="ltlChangeTypeUserId" runat="server" Visible="false" />
                        <h1 class="popup__title">Change User Type</h1>
                        <div class="popup__content">
                            <fieldset class="form">
                                <div class="container">
                                    <p>
                                        <asp:Literal ID="ltlChangeTypeUserName1" runat="server" />
                                        is currently a
                                <asp:Literal ID="ltlChangeTypeTitle" runat="server" />.
                                    </p>

                                    <div class="main">
                                        <div class="form__row">
                                            <div class="label"><b>Assign User</b></div>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlChangeType" runat="server" OnSelectedIndexChanged="ddlChangeType_SelectedIndexChanged" AutoPostBack="true" />
                                            </div>
                                        </div>
                                    </div>
                                    <asp:Panel ID="plChangeTypeAdminMsg" runat="server" Visible="false">
                                        <p class="clear">
                                            <asp:Literal ID="ltlChangeTypeUserName2" runat="server" />
                                            can manage all aspects of the CoCadre Admin Console including but not limited to removing another Admin, add/modify/delete Department or Users, subscribe items in Marketplace and all rights of what a moderator can do.
                                        </p>
                                    </asp:Panel>
                                    <asp:Panel ID="plChangeTypeModule" runat="server" Visible="false">
                                        <p class="clear">Expired date</p>
                                        <div class="accessrights">
                                            <asp:RadioButton ID="rbChangeTypeNoExpiryDate" runat="server" GroupName="ChangeTypeExpiredDate" Text="No expiry date" Checked="true" />
                                            <asp:RadioButton ID="rbChangeTypeExpiredDate" runat="server" GroupName="ChangeTypeExpiredDate" />
                                            <asp:TextBox ID="tbChangeTypeExpiredDate" runat="server" />
                                            <ajaxToolkit:CalendarExtender runat="server" TargetControlID="tbChangeTypeExpiredDate" ID="ceChangeTypeExpiredDate" CssClass="ajax__calendar" Format="dd/MM/yyyy" />
                                        </div>
                                        <br />
                                        <p class="clear">Assign rights for the moderator</p>
                                        <div class="accessrights">
                                            <asp:CheckBoxList ID="cblChangeTypeModule" runat="server" RepeatLayout="Flow" />
                                        </div>
                                    </asp:Panel>
                                </div>
                            </fieldset>
                        </div>
                        <div class="popup__action">
                            <asp:LinkButton ID="lbChangeTypeChange" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" OnClick="lbChangeTypeChange_Click" Text="Save Changes" OnClientClick="ShowProgressBar();" />
                            <asp:LinkButton ID="lbChangeTypeCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="CANCEL" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <!-- / Change User Type Form -->

            <!-- Assign Access Rights Form -->
            <asp:Panel ID="popup_assignaccessrights" runat="server" CssClass="popup popup--assignaccessrights" Width="100%" Style="display: none;">
                <asp:UpdatePanel ID="upChangeRights" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hfChangeRightUserId" runat="server" />
                        <h1 class="popup__title">Moderator Access Rights</h1>
                        <div class="popup__content">
                            <fieldset class="form">
                                <div class="container">
                                    <p>Expired date</p>
                                    <div class="accessrights">
                                        <asp:RadioButton ID="rbChangeRightsNoExpiryDate" runat="server" GroupName="ChangeRightsExpiredDate" Text="No expiry date" />
                                        <asp:RadioButton ID="rbChangeRightsExpiredDate" runat="server" GroupName="ChangeRightsExpiredDate" />
                                        <asp:TextBox ID="tbChangeRightsExpiredDate" runat="server" />
                                        <ajaxToolkit:CalendarExtender runat="server" TargetControlID="tbChangeRightsExpiredDate" ID="ceChangeRightsExpiredDate" CssClass="ajax__calendar" Format="dd/MM/yyyy" />
                                    </div>
                                    <br />
                                    <p>Assign rights for the moderator</p>
                                    <div class="accessrights">
                                        <asp:CheckBoxList ID="cblChangeRightsModule" runat="server" RepeatLayout="Flow">
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="popup__action">
                            <asp:LinkButton ID="lbChangeRightsSave" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" OnClick="lbChangeRightsSave_Click" Text="Save Changes" OnClientClick="ShowProgressBar();" />
                            <asp:LinkButton ID="lbChangeRightsCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="CANCEL" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <!-- / Assign Access Rights Form  -->

            <!-- Add Multiple User Form -->
            <asp:Panel ID="popup_addmultipleusers" runat="server" CssClass="popup addmultipleusers-form" Width="100%" Style="display: none;">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAddMultiUsersUpload" />
                    </Triggers>
                    <ContentTemplate>
                        <h1 class="popup__title">Add Multiple Users</h1>
                        <div class="popup__content">

                            <h2 class="step">Upload the .CSV file</h2>
                            <p>Open a sample spreadsheet. The first 4 columns: First Name, Last Name, Email, Department are mandatory.</p>
                            <p>
                                <asp:Label ID="lblMultiUserCsvPath" runat="server" />
                                <br />
                                <label for="<%=ufMultiUsersCsv.ClientID %>" style="cursor: pointer;" class="btn secondary uppercase">
                                    <span>
                                        <i class="fa fa-download"></i>
                                        Attach File
                                    </span>
                                    <asp:FileUpload ID="ufMultiUsersCsv" CssClass="multiUsersCsv" runat="server" accept=".csv" title="Choose csv file" Style="display: none;" />
                                </label>
                            </p>
                            <h2 class="step">Or download our <a href="/CSVs/sample.csv">sample.CSV</a></h2>
                        </div>
                        <div class="popup__action">
                            <asp:LinkButton ID="btnAddMultiUsersUpload" runat="server" CssClass="popup__action__item popup__action__item--cta" OnClick="btnAddMultiUsersUpload_Click" Text="UPLOAD" OnClientClick="ShowProgressBar();" />
                            <asp:LinkButton ID="lbAddMultiUsersCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="CANCEL" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <!-- Add Multiple User Form -->

            <!-- Add Multiple User Ready Form -->
            <asp:Panel ID="popup_addmultipleusersready" runat="server" CssClass="popup addmultipleusers-form" Width="100%" Style="display: none;">
                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="ReadyToAddUploadButton" />
                    </Triggers>
                    <ContentTemplate>

                        <div class="right">
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="X" />
                        </div>

                        <p class="center clear">
                            <i class="fa fa-check-circle fa-5x" style="color: green"></i>
                        </p>

                        <h1 class="popup__title center"><strong>You're ready to go</strong></h1>

                        <div class="">

                            <p class="center">1352 Personnel has been verified.</p>

                            <p class="center">Proceed to update?</p>

                        </div>
                        <div class="popup__action">
                            <asp:LinkButton ID="ReadyToAddUploadButton" runat="server" CssClass="popup__action__item popup__action__item--cta" OnClick="btnReadyToAddMultiUsersUpload_Click" Text="UPLOAD" />
                            <asp:LinkButton ID="ReadyToAddCancelLinkButton" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbPopCancel_Click" Text="CANCEL" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <!-- Add Multiple User Ready Form  End -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_adduser"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:LinkButton ID="lbEmpty" runat="server" />


    <%--***************************--%>

    <!-- Add Admin Form -->
    <div id="popup_addadmin" class="mfp-hide popup popup--addadmin">
        <h1 class="popup__title">Add Admin</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <p>Admin can manage all aspects of the CoCadre Admin Console including but not limited to removing another Admin, add/modify/delete Department or Users, subscribe items in Marketplace and all rights of what a moderator can do.</p>
                    <div class="main">
                        <div class="form__row">
                            <div class="label">
                                <label for=""><b>Personnel Name</b></label>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <%-- <asp:TextBox ID="tbAddAdminUserName" runat="server" />
                                    <ajaxToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="1" TargetControlID="tbAddAdminUserName" ServiceMethod="GetDepartmentList" CompletionSetCount="15"
                                        runat="server" />--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--<input id="a11" type="text" placeholder="First Name"  />--%>
                            <div class="suggestions" style="display: ;">
                                <ul class="suggestions__list">
                                    <li class="suggestions__list__item">
                                        <a class="suggestions__link" href="#">
                                            <span class="suggestions__name">Addison Kang</span>
                                            <span class="suggestions__email">addison@gmail.com</span>
                                        </a>
                                    </li>
                                    <li class="suggestions__list__item">
                                        <a class="suggestions__link" href="#">
                                            <span class="suggestions__name">Addison Kang</span>
                                            <span class="suggestions__email">addison@gmail.com</span>
                                        </a>
                                    </li>
                                    <li class="suggestions__list__item">
                                        <a class="suggestions__link" href="#">
                                            <span class="suggestions__name">Addison Kang</span>
                                            <span class="suggestions__email">addison@gmail.com</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="form__row">
                            <div class="label">
                                <label for=""><b>Personnel Name</b></label>
                            </div>
                            <div class="Media Media--boxed">
                                <img class="Media-figure" alt="" />
                                <div class="Media-body">
                                    <span class="suggestions__name">Addison Kang</span>
                                    <span class="suggestions__email">addison@gmail.com</span>
                                </div>
                            </div>
                            <p>User_name is currently a Normal User.</p>
                            <p class="error">User_name is already an Admin.</p>
                        </div>
                    </div>
                    <p class="clear">
                        <i class="fa fa-info-circle color-blue"></i>
                        <small>If you're adding a new admin, please understand that they'll have the same control as you.</small>
                    </p>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a href="#" class="popup__action__item popup__action__item--cancel popup__action__item--cta" onclick="toastr.info('Someone is now an Admin')">Add</a>
            <a href="#" class="popup__action__item popup__action__item--cancel">Cancel</a>
        </div>
    </div>
    <!-- / Add Admin Form -->

    <!-- Remove Admin Form -->
    <div id="popup_removeadmin" class="mfp-hide popup popup--removeadmin">
        <h1 class="popup__title">Remove Admin Rights</h1>
        <div class="popup__content">
            <div class="container">
                <p>Conform removal of <strong>Company HR Management Name</strong> as an admin?</p>
                <div class="Media">
                    <img class="Media-figure" alt="" />
                    <div class="Media-body">
                        <span class="suggestions__name">Company HR Management Name</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup__action">
            <a href="#" class="popup__action__item popup__action__item--cancel popup__action__item--cancel popup__action__item--confirm" onclick="toastr.info('Company HR Management Name administrator rights has been removed')">Remove</a>
            <a href="#" class="popup__action__item popup__action__item--cancel">Cancel</a>
        </div>
    </div>
    <!-- / Remove Admin Form -->

    <!-- Add Moderator Form -->
    <div id="popup_addmoderator" class="mfp-hide popup popup--addmoderator">
        <h1 class="popup__title">Add Moderator</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="main">
                        <div class="form__row">
                            <div class="label">
                                <label for=""><b>Personnel Name</b></label>
                            </div>
                            <input id="a12" type="text" placeholder="First Name" />
                            <div class="suggestions" style="display: none;">
                                <ul class="suggestions__list">
                                    <li class="suggestions__list__item">
                                        <a class="suggestions__link" href="#">
                                            <span class="suggestions__name">Addison Kang</span>
                                            <span class="suggestions__email">addison@gmail.com</span>
                                        </a>
                                    </li>
                                    <li class="suggestions__list__item">
                                        <a class="suggestions__link" href="#">
                                            <span class="suggestions__name">Addison Kang</span>
                                            <span class="suggestions__email">addison@gmail.com</span>
                                        </a>
                                    </li>
                                    <li class="suggestions__list__item">
                                        <a class="suggestions__link" href="#">
                                            <span class="suggestions__name">Addison Kang</span>
                                            <span class="suggestions__email">addison@gmail.com</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="form__row">
                            <div class="label">
                                <label for=""><b>Personnel Name</b></label>
                            </div>
                            <div class="Media Media--boxed">
                                <img class="Media-figure" alt="" />
                                <div class="Media-body">
                                    <span class="suggestions__name">Addison Kang</span>
                                    <span class="suggestions__email">addison@gmail.com</span>
                                </div>
                            </div>
                            <p>User_name is currently a Normal User.</p>
                            <p class="error">User_name is already an Admin or Moderator.</p>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a href="#popup_assignaccessrights" class="popup__action__item popup__action__item--cta popup-with-form">Next</a>
            <a href="#" class="popup__action__item popup__action__item--cancel">Cancel</a>
        </div>
    </div>
    <!-- / Add Moderator Form -->

    <!-- Remove Moderator Form -->
    <div id="popup_removemoderator" class="mfp-hide popup popup--removemoderator">
        <h1 class="popup__title">Remove Moderator Rights</h1>
        <div class="popup__content">
            <div class="container">
                <p>Conform removal of <strong>Vivian Wong</strong> as a moderator?</p>
                <div class="Media">
                    <img class="Media-figure" alt="" />
                    <div class="Media-body">
                        <span class="suggestions__name">Vivian Wong</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="popup__action">
            <a href="#" class="popup__action__item popup__action__item--cancel popup__action__item--cancel popup__action__item--confirm" onclick="toastr.info('Vivian Wong moderator rights have been removed')">Remove</a>
            <a href="#" class="popup__action__item popup__action__item--cancel">Cancel</a>
        </div>
    </div>
    <!-- / Remove Moderator Form -->

    <!-- Specify Moderator Rights Form -->
    <div id="popup_specifyaccessrights" class="mfp-hide popup popup--specifyaccessrights">
        <h1 class="popup__title">Moderator Access Rights</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <p>Assign rights for the moderator</p>
                    <div class="accessrights">
                        <label class="accessrights__item" for="access01">
                            <div class="accessrights__checkbox">
                                <input class="" type="checkbox" id="access01" checked="checked" />
                            </div>
                            <i class="fa fa-list-alt fa-2x accessrights__icon"></i>
                            <div class="accessrights__description">View the alert section of the "Reported Post" (including private post), respond to and delete comments or the entire Post on the Feed as moderator</div>
                        </label>
                        <label class="accessrights__item" for="access02">
                            <div class="accessrights__checkbox">
                                <input class="" type="checkbox" id="access02" checked="checked" />
                            </div>
                            <i class="fa fa-lightbulb-o fa-2x accessrights__icon"></i>
                            <div class="accessrights__description">Add, modify and delete Quiz category, topic and discussion</div>
                        </label>
                        <label class="accessrights__item" for="access03">
                            <div class="accessrights__checkbox">
                                <input class="" type="checkbox" id="access03" />
                            </div>
                            <i class="fa fa-calendar fa-2x accessrights__icon"></i>
                            <div class="accessrights__description">Create Quiz event and view ongoing result before the end of the event</div>
                        </label>
                        <label class="accessrights__item" for="access04">
                            <div class="accessrights__checkbox">
                                <input class="" type="checkbox" id="access04" />
                            </div>
                            <i class="fa fa-line-chart fa-2x accessrights__icon"></i>
                            <div class="accessrights__description">Access to Analytics Report</div>
                        </label>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a href="#" class="popup__action__item popup__action__item--cancel popup__action__item--cta popup-with-form" onclick="toastr.info('Vivian Wong\'s Moderator rights have been changed')">Save Changes</a>
            <a href="#" class="popup__action__item popup__action__item--cancel">Cancel</a>
        </div>
    </div>
    <!-- / Specify Moderator Rights Form  -->

    <%--***************************--%>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Personnel <span>console</span></div>
        <div class="appbar__meta">Personnel</div>
        <div class="appbar__action">
            <%--<a><i class="fa fa-question-circle"></i></a>--%>
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- / App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Personnel/Department">Department</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Personnel/JobLevel">Job Level</a>
                </li>
            </ul>
            <div class="data__sidebar__title">Filters</div>

            <div class="pad">
                <label>Search Personnel</label>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="tbFilterUser" runat="server" placeholder="Type the personnel's name" MaxLength="30" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterUser" runat="server" OnClick="ibFilterUser_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="pad">
                <label>By Type</label>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="mdl-selectfield">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlFilterUserType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" onchange="ShowProgressBar('Filtering');" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="pad">
                <label>By Department</label>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" class="mdl-selectfield">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlFilterDepartment" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" onchange="ShowProgressBar('Filtering');" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <%--OnPagePropertiesChanging="lvPerson_PagePropertiesChanging"--%>
                    <asp:ListView ID="lvPerson" runat="server" OnItemDataBound="lvPerson_ItemDataBound" OnItemCommand="lvPerson_ItemCommand" DataKeyNames="UserId" OnDataBound="lvPerson_DataBound" OnItemCreated="lvPerson_ItemCreated">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 40px;"></th>
                                        <th>
                                            <asp:LinkButton ID="lbSortByName" runat="server" OnClick="lbSortByName_Click" Text="Name" />
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lbSortByDepartment" runat="server" OnClick="lbSortByDepartment_Click" Text="Department" />
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lbSortByJobLevel" runat="server" Text="Job Level" />
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lbSortByType" runat="server" OnClick="lbSortByType_Click" Text="User Type" />
                                        </th>
                                        <th class="no-sort"></th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <div class="constrained">
                                        <asp:LinkButton ID="lbPhoto" runat="server" ClientIDMode="AutoID" CommandName="UpdateUser" Style="color: #999;">
                                            <asp:Image ID="imgPhoto" runat="server" />
                                        </asp:LinkButton>
                                    </div>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbName" runat="server" ClientIDMode="AutoID" CommandName="UpdateUser" Style="color: #999; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;" />
                                    <asp:Label ID="lblDelete" runat="server" CssClass="deleted" Font-Italic="true" Visible="false" />
                                    <asp:Label ID="lblSuspended" Text="Suspended" CssClass="suspended" Font-Italic="true" Visible="false" runat="server" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbDepartment" runat="server" ClientIDMode="AutoID" CommandName="UpdateUser" Style="color: #999; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;" />
                                    <%--<asp:Label ID="lblDepartment" runat="server" />--%>
                                </td>
                                                                <td>
                                    <asp:LinkButton ID="lbJobLevel" runat="server" ClientIDMode="AutoID" CommandName="UpdateUser" Style="color: #999; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;" />
                                    <%--<asp:Label ID="lblDepartment" runat="server" />--%>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbUserType" runat="server" ClientIDMode="AutoID" CommandName="UpdateUser" Style="color: #999;" />
                                    <%--<asp:Label ID="lblUserType" runat="server" />--%>
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbUpdate" runat="server" ClientIDMode="AutoID" CommandName="UpdateUser" Visible="false" Text="Edit User Info" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbResendInvitation" runat="server" ClientIDMode="AutoID" CommandName="ResendInvitation" Visible="false" Text="Resend Invitation" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbResendInvitationAll" runat="server" ClientIDMode="AutoID" CommandName="ResendInvitationAll" Visible="false" Text="Mass Resend Invitation" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbResentPassword" runat="server" ClientIDMode="AutoID" CommandName="ResentPassword" Visible="false" Text="Resend Password" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbChangeRights" runat="server" ClientIDMode="AutoID" CommandName="ChangeRights" Visible="false" Text="Specify Moderator Rights" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbChangeType" runat="server" ClientIDMode="AutoID" CommandName="ChangeType" Visible="false" Text="Change User Type" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbSuspend" runat="server" ClientIDMode="AutoID" CommandName="SuspendUser" Visible="false" Text="Suspend User" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbRestoreSus" runat="server" ClientIDMode="AutoID" CommandName="RestoreSuspend" Visible="false" Text="Restore User" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" runat="server" ClientIDMode="AutoID" CommandName="DeleteUser" Visible="false" Text="Delete User" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbRestoreDel" runat="server" ClientIDMode="AutoID" CommandName="RestoreDeleted" Visible="false" Text="Restore User" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbConfirmDelete" runat="server" ClientIDMode="AutoID" CommandName="ConfirmDelete" Visible="false" Text="Delete User" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    <%--
                    <asp:DataPager ID="dpPerson" runat="server" PageSize="5" PagedControlID="lvPerson" QueryStringField="p" ClientIDMode="AutoID">
                        <Fields>
                            <asp:NextPreviousPagerField FirstPageText="<<" PreviousPageText="<" ButtonType="Link" ShowFirstPageButton="true" ShowPreviousPageButton="true" ShowNextPageButton="false" />
                            <asp:NumericPagerField ButtonCount="5" />
                            <asp:NextPreviousPagerField LastPageText=">>" NextPageText=">" ButtonType="Link" ShowLastPageButton="true" ShowNextPageButton="true" ShowPreviousPageButton="false" />
                        </Fields>
                    </asp:DataPager>
                    --%>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="FailedUserUploadPanel" runat="server" Visible="false">
                <ContentTemplate>

                    <div id="upload_personnel_failed">
                        <div class="left red" style="margin: 0em 1em;">
                            <i class="fa fa-times-circle fa-5x"></i>
                        </div>
                        <div class="right" style="margin: 1em;">
                            <asp:Button ID="Button1" runat="server" Text="RE-UPLOAD .CSV" CssClass="popup__action__item popup__action__item--cta" OnClick="lbAddMultiUsers_Click" />
                        </div>
                        <div style="margin: 1em;">
                            <h2 style="margin-bottom: 0px">Unsuccessful!</h2>
                            <p>
                                The file is not uploaded into our database.<br />
                                Please amend the X Personnel with errors on your .CSV before re-loading again.
                            </p>
                        </div>
                    </div>

                    <asp:ListView ID="lvErrorUpload" runat="server">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="" style="table-layout: auto">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 80px; white-space: nowrap; vertical-align: top;">
                                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="lbSortByDepartment_Click" Text="CSV No." />
                                        </th>
                                        <th class="no-sort" style="width: 200px">
                                            <asp:LinkButton ID="lbSortByName" runat="server" OnClick="lbSortByDepartment_Click" Text="Name" />
                                        </th>
                                        <th class="no-sort" style="width: 200px">
                                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="lbSortByDepartment_Click" Text="Email" />
                                        </th>
                                        <th class="no-sort" style="width: 200px">
                                            <asp:LinkButton ID="lbSortByDepartment" runat="server" OnClick="lbSortByDepartment_Click" Text="Department" />
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lbSortByType" runat="server" OnClick="lbSortByDepartment_Click" Text="Errors" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lbCSVNo" runat="server" Text='<%# Eval("CsvNo") %>' />
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lbName" runat="server" Text='<%# Eval("Name") %>' />
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lbEmail" runat="server" Text='<%# Eval("Email") %>' />
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("Department") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lbErrors" runat="server" Text='<%# Eval("ErrorsHtml") %>' CssClass="red" />
                                </td>

                            </tr>
                        </ItemTemplate>
                    </asp:ListView>

                    <div style="margin-left: auto; margin-right: auto;">
                        <center>
                        <table style="width:inherit;border-radius: 4px;border: 2px solid #ccc;border-collapse: initial;">
                            <tbody>
                                <tr>
                                    <td style="padding:10px;margin:3px;border:0px #E7E7E7 solid;background-color:#F5F5F5;border-top-left-radius:5px;border-bottom-left-radius:5px;"><a href="#">&laquo;</a></td>
                                    <td style="padding:10px;margin:3px;border:1px #E7E7E7 solid;background-color:#F5F5F5;"><a href="#">1</a></td>
                                    <td style="padding:10px;margin:3px;border:1px #E7E7E7 solid;"><a href="#">2</a></td>
                                    <td style="padding:10px;margin:3px;border:1px #E7E7E7 solid;"><a href="#">3</a></td>
                                    <td style="padding:10px;margin:3px;border:1px #E7E7E7 solid;"><a href="#">4</a></td>
                                    <td style="padding:10px;margin:3px;border:1px #E7E7E7 solid;"><a href="#">5</a></td>
                                    <td style="padding:10px;margin:3px;border:0px #E7E7E7 solid;background-color:#F5F5F5;border-top-right-radius:5px;border-bottom-right-radius:5px;"><a href="#">&raquo;</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
