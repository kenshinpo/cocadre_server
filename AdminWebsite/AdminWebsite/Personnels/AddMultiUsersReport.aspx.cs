﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using Microsoft.VisualBasic.FileIO;

namespace AdminWebsite.Personnels
{
    public partial class AddMultiUsersReport : System.Web.UI.Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        private List<ImportedUser> allUsers;
        private List<ImportedUser> errorUsers;

        private void GetErrorDataUsers()
        {
            errorUsers = new List<ImportedUser>();
            for (int i = 0; i < allUsers.Count; i++)
            {
                if (allUsers[i].DataErrors.Count > 0)
                {
                    errorUsers.Add(allUsers[i]);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (Session["MultipleUsersToAdd"] == null || Session["IsMultipleUsersDataCorrect"] == null)
                {
                    MessageUtility.ShowToast(this.Page, "Redirect to user page. Please wait...", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Personnel/Personnel/', 300);", true);
                }

                if (!IsPostBack)
                {
                    bool isCorrect = Convert.ToBoolean(Session["IsMultipleUsersDataCorrect"]);
                    allUsers = Session["MultipleUsersToAdd"] as List<ImportedUser>;

                    if (isCorrect) // Ready to go
                    {
                        plContent.Visible = false;
                        ltlImportUserCount.Text = allUsers.Count.ToString();
                        mpePop.Show();
                    }
                    else // Error users list
                    {
                        FailedUserUploadPanel.Visible = true;
                        GetErrorDataUsers();
                        ltlErrorUsersCount.Text = Convert.ToString(errorUsers.Count);
                        lvErrorUpload.DataSource = errorUsers;
                        lvErrorUpload.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void ReadyToAddCancelLinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                Session["MultipleUsersToAdd"] = null;
                Session["IsMultipleUsersDataCorrect"] = null;

                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Personnel/Personnel/', 0);", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                allUsers = new List<ImportedUser>();
                List<ImportedUser> importedUsers = Session["MultipleUsersToAdd"] as List<ImportedUser>;
                for (int i = 0; i < importedUsers.Count; i++)
                {
                    String userId = "U" + Guid.NewGuid().ToString().Replace("-", "");
                    UserCreateResponse response = asc.CreateUser(adminInfo.UserId,
                                                                     adminInfo.CompanyId,
                                                                     userId,
                                                                     importedUsers[i].FirstName,
                                                                     importedUsers[i].LastName,
                                                                     importedUsers[i].Email,
                                                                     null,
                                                                     importedUsers[i].Designation,
                                                                     importedUsers[i].PhoneNumber,
                                                                     (importedUsers[i].PhoneCountryCode == null) ? null : importedUsers[i].PhoneCountryCode.Replace("+", ""),
                                                                     (importedUsers[i].PhoneCountryName == null) ? null : importedUsers[i].PhoneCountryName,
                                                                     importedUsers[i].Address,
                                                                     (importedUsers[i].AddressCountryName == null) ? null : importedUsers[i].AddressCountryName,
                                                                     importedUsers[i].AddressPostalCode,
                                                                     importedUsers[i].Department,
                                                                     importedUsers[i].JobLevel,
                                                                     importedUsers[i].Gender,
                                                                     importedUsers[i].Birthday,
                                                                     false,
                                                                     importedUsers[i].Password
                                                                     );
                    if (!response.Success)
                    {
                        if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserDuplicatedEmail))
                        {
                            importedUsers[i].DataErrors.Add("Email exists in system");
                        }
                        else
                        {
                            importedUsers[i].DataErrors.Add("Create user failed");
                        }
                        allUsers.Add(importedUsers[i]);
                    }
                }

                if (allUsers.Count == 0)
                {
                    MessageUtility.ShowToast(this.Page, importedUsers.Count + " entries succesfully added.", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Personnel/Personnel/',300);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Personnel/Personnel/',300);", true);
                    ltlErrorUsersCount.Text = Convert.ToString(errorUsers.Count);
                    lvErrorUpload.DataSource = allUsers;
                    lvErrorUpload.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lvErrorUpload_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    Label lblErrors = e.Item.FindControl("lblErrors") as Label;

                    string errorMsg = string.Empty;
                    for (int i = 0; i < errorUsers[e.Item.DataItemIndex].DataErrors.Count; i++)
                    {
                        if (i == (errorUsers[e.Item.DataItemIndex].DataErrors.Count - 1))
                        {
                            errorMsg += errorUsers[e.Item.DataItemIndex].DataErrors[i];
                        }
                        else
                        {
                            errorMsg += errorUsers[e.Item.DataItemIndex].DataErrors[i] + ", ";
                        }
                    }
                    lblErrors.Text = errorMsg;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void btnReupload_Click(object sender, EventArgs e)
        {
            try
            {
                List<ImportedUser> importUsers = new List<ImportedUser>();
                bool isAllCorrectData = true;
                #region Step 1. Check CSV

                if (ufMultiUsersCsv.HasFile)
                {
                    using (TextFieldParser parser = new TextFieldParser(new StreamReader(ufMultiUsersCsv.PostedFile.InputStream)))
                    {
                        bool checkHeader = true;
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters(",");

                        string[] header = parser.ReadFields();
                        if (checkHeader)
                        {
                            #region Check CSV version
                            /*
                             *  The lastest version data format(2017/05/03): 
                                00: First Name
                                01: Last Name
                                02: Email
                                03: Department
                                04: Designation
                                05: Job Level
                                06: Gender
                                07: Birthday(YYYY/MM/DD)
                                08: Address Country Name
                                09: Address
                                10: Address Postal Code
                                11: Phone Country Code
                                12: Phone Country Name
                                13: Phone Number
                                14: Password
                             *
                             */

                            string[] titleFields = { "First Name", "Last Name", "Email", "Department", "Designation", "Job Level", "Gender", "Birthday(YYYY/MM/DD)", "Address Country Name", "Address", "Address Postal Code", "Phone Country Code", "Phone Country Name", "Phone Number", "Password" };

                            if (header.Length != titleFields.Length)
                            {
                                // The csv file is not latest version
                                MessageUtility.ShowToast(this.Page, "Please use the latest sample csv file", MessageUtility.TOAST_TYPE_ERROR);
                                return;
                            }
                            else
                            {
                                bool isLatestVersion = true;
                                // Check every title
                                for (int i = 0; i < header.Length; i++)
                                {
                                    if (!header[i].Equals(titleFields[i]))
                                    {
                                        isLatestVersion = false;
                                        break;
                                    }
                                }

                                if (!isLatestVersion)
                                {
                                    // The csv file is not latest version
                                    MessageUtility.ShowToast(this.Page, "Please use the latest sample csv file", MessageUtility.TOAST_TYPE_ERROR);
                                    return;
                                }
                                else
                                {
                                    checkHeader = false;
                                }
                            }
                            #endregion
                        }

                        int lineNumber = 1;

                        while (!parser.EndOfData)
                        {
                            string[] lineFields = parser.ReadFields();
                            ImportedUser iu = new ImportedUser();
                            iu.DataErrors = new List<string>();
                            iu.No = lineNumber;

                            #region 0: First Name (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[0]))
                            {
                                iu.DataErrors.Add("No first name entry");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.FirstName = lineFields[0];
                            }
                            #endregion

                            #region 1: Last Name (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[1]))
                            {
                                iu.DataErrors.Add("No last name entry");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.LastName = lineFields[1];
                            }
                            #endregion

                            #region 2: Email
                            // (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[2]))
                            {
                                iu.DataErrors.Add("Email required");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.Email = lineFields[2];
                                // Check Email (Cannot be duplicate in csv file)
                                if (importUsers.Exists(x => x.Email == lineFields[2]))
                                {
                                    iu.DataErrors.Add("Duplicate Email");
                                    isAllCorrectData = false;
                                }
                                else
                                {
                                    // Check Email (Cannot be duplicate in database)
                                    AuthenticationCheckEmailForCompanyResponse response = asc.CheckEmailExistsForCompany(adminInfo.UserId, adminInfo.CompanyId, lineFields[2]);
                                    if (response.Success && response.IsEmailExists)
                                    {
                                        iu.DataErrors.Add("Email exists in system");
                                        isAllCorrectData = false;
                                    }
                                }
                            }
                            #endregion

                            #region 3: Department (Cannot be empty)
                            if (string.IsNullOrEmpty(lineFields[3]))
                            {
                                iu.DataErrors.Add("Department required");
                                isAllCorrectData = false;
                            }
                            else
                            {
                                iu.Department = lineFields[3];
                            }
                            #endregion

                            #region 4: Designation
                            if (!string.IsNullOrEmpty(lineFields[4]))
                            {
                                iu.Designation = lineFields[4];
                            }
                            #endregion

                            #region 5: Job Level
                            if (!string.IsNullOrEmpty(lineFields[5]))
                            {
                                iu.JobLevel = lineFields[5];
                            }
                            #endregion

                            #region 6: Gender
                            try
                            {
                                if (lineFields[6].ToLower().Equals("male"))
                                {
                                    iu.Gender = 1;
                                }
                                else if (lineFields[6].ToLower().Equals("famale"))
                                {
                                    iu.Gender = 2;
                                }
                                else
                                {
                                    iu.Gender = 0;
                                }
                            }
                            catch (Exception)
                            {
                                iu.Gender = 0;
                            }
                            #endregion

                            #region 7: Birthday(YYYY/MM/DD)
                            if (!string.IsNullOrEmpty(lineFields[7]))
                            {
                                try
                                {
                                    iu.Birthday = DateTime.ParseExact(lineFields[7], "yyyy/MM/dd", CultureInfo.InvariantCulture);
                                }
                                catch (Exception)
                                {
                                    iu.DataErrors.Add("Birthday is invalid");
                                    iu.Birthday = new DateTime(1980, 1, 1);
                                    isAllCorrectData = false;
                                }
                            }
                            #endregion

                            #region 8: Address Country Name
                            if (!string.IsNullOrEmpty(lineFields[8]))
                            {
                                iu.AddressCountryName = lineFields[8];
                            }
                            #endregion

                            #region 9: Address
                            if (!string.IsNullOrEmpty(lineFields[9]))
                            {
                                iu.Address = lineFields[9];
                            }
                            #endregion

                            #region 10: Address Postal Code
                            if (!string.IsNullOrEmpty(lineFields[10]))
                            {
                                iu.AddressPostalCode = lineFields[10];
                            }
                            #endregion

                            #region 11: Phone Country Code
                            if (!string.IsNullOrEmpty(lineFields[11]))
                            {
                                iu.PhoneCountryCode = lineFields[11];
                            }
                            #endregion

                            #region 12: Phone Country Name
                            if (!string.IsNullOrEmpty(lineFields[12]))
                            {
                                iu.PhoneCountryName = lineFields[12];
                            }
                            #endregion

                            #region 13: Phone Number
                            if (!string.IsNullOrEmpty(lineFields[13]))
                            {
                                iu.PhoneNumber = lineFields[13];
                            }
                            #endregion

                            #region 14: Password
                            iu.Password = null;
                            if (!string.IsNullOrEmpty(lineFields[14]))
                            {
                                iu.Password = lineFields[14];
                            }
                            #endregion

                            importUsers.Add(iu);
                            lineNumber++;

                        }
                    }

                    if (importUsers.Count > 0)
                    {
                        Session["MultipleUsersToAdd"] = importUsers;
                        Session["IsMultipleUsersDataCorrect"] = isAllCorrectData;
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Personnel/AddMultiUsersReport/', 0);", true);
                    }
                    else
                    {
                        MessageUtility.ShowToast(this.Page, "This is an empty csv file", MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
                else
                {
                    MessageUtility.ShowToast(this.Page, "Please select a csv file", MessageUtility.TOAST_TYPE_ERROR);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}