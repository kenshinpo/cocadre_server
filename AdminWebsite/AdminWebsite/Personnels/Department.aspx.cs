﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.Personnels
{
    public partial class DepartmentPage : Page
    {
        private ManagerInfo adminInfor;
        private AdminService asc = new AdminService();

        private List<Department> departmentList;

        public void GetDepartmentList()
        {
            try
            {
                DepartmentListResponse response = asc.GetAllDepartment(adminInfor.UserId, adminInfor.CompanyId);
                if (response.Success)
                {
                    List<Department> list = response.Departments;
                    list.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
                    if (!Convert.ToBoolean(ViewState["IsSortByNameAcsend"]))
                    {
                        list.Reverse();
                    }
                    departmentList = list;
                }
                else
                {
                    Log.Error("GetAllDepartment.Success is false. ErrorMessage: " + response.ErrorMessage, this.Page);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout", false);
                return;
            }
            adminInfor = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    ViewState["IsSortByNameAcsend"] = true;
                    GetDepartmentList();
                    lvDepartment.DataSource = departmentList;
                    lvDepartment.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvDepartment_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (departmentList != null && departmentList.Count > 0)
                {
                    if (e.Item.ItemType == ListViewItemType.DataItem)
                    {
                        LinkButton lbName = e.Item.FindControl("lbName") as LinkButton;
                        HyperLink hlNoOfPersonnel = e.Item.FindControl("hlNoOfPersonnel") as HyperLink;
                        LinkButton lbRename = e.Item.FindControl("lbRename") as LinkButton;
                        LinkButton lbDelete = e.Item.FindControl("lbDelete") as LinkButton;

                        lbName.Text = departmentList[e.Item.DataItemIndex].Title;
                        hlNoOfPersonnel.Text = departmentList[e.Item.DataItemIndex].CountOfUsers + " Personnel";
                        hlNoOfPersonnel.NavigateUrl = "/Personnel/Personnel/" + departmentList[e.Item.DataItemIndex].Id;
                        lbName.CommandArgument = departmentList[e.Item.DataItemIndex].Id;
                        lbRename.CommandArgument = departmentList[e.Item.DataItemIndex].Id;
                        lbDelete.CommandArgument = departmentList[e.Item.DataItemIndex].Id;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbDone_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check input data
                if (String.IsNullOrEmpty(tbAddName.Text.Trim()))
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, ErrorMessage.DepartmentNameCantBeEmpty, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }
                #endregion

                #region Step 2. Call Api
                DepartmentCreateResponse response = asc.CreateDepartment(adminInfor.UserId, adminInfor.CompanyId, tbAddName.Text);
                if (response.Success)
                {
                    mpePop.Hide();
                    GetDepartmentList();
                    lvDepartment.DataSource = departmentList;
                    lvDepartment.DataBind();
                    MessageUtility.ShowToast(this.Page, tbAddName.Text + " has been created.", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvDepartment_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Remove") || e.CommandName.Equals("Rename"))
                {
                    DepartmentDetailResponse response = asc.GetDepartmentDetail(adminInfor.UserId, adminInfor.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        hfDepartmentId.Value = response.Department.Id;
                        hfDepartmentTitle.Value = response.Department.Title;

                        if (e.CommandName.Equals("Remove"))
                        {
                            if (response.Department.CountOfUsers == 0)
                            {
                                ltlDeleteMsg.Text = "<p>Are you sure you want to delete this department : <b>" + response.Department.Title + "</b> ?</p>";
                                lbDelete.Visible = true;
                                lbDelCancel.Text = "Cancel";
                            }
                            else
                            {
                                ltlDeleteMsg.Text = @"
                            <p class='error'>There are still personnel(s) left in the department!<br />For safety reasons we can only allow you to delete empty department.
                            <p><img src='/img/tips_icon.png' width='16px' height='20px' /> You can move the personnel to other departments in the <a href='/Personnel/Personnel/" + response.Department.Id + "'>user console</a> manager.</p>";
                                lbDelete.Visible = false;
                                lbDelCancel.Text = "Exit";
                            }

                            mpePop.PopupControlID = "popup_deletedepartment";
                            mpePop.Show();
                        }
                        else if (e.CommandName.Equals("Rename"))
                        {
                            tbEditName.Text = response.Department.Title;
                            mpePop.PopupControlID = "popup_renamedepartment";
                            mpePop.Show();
                        }
                        else
                        {
                            Log.Info("There is no this command name in the lvDepartment.");
                        }
                    }
                    else
                    {
                        Log.Error("DepartmentDetailResponse.Success is false. ErrorMessage : " + response.ErrorMessage, this.Page);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DepartmentDeleteResponse response = asc.DeleteDepartment(adminInfor.UserId, adminInfor.CompanyId, hfDepartmentId.Value);
                if (response.Success)
                {
                    mpePop.Hide();
                    GetDepartmentList();
                    lvDepartment.DataSource = departmentList;
                    lvDepartment.DataBind();
                    MessageUtility.ShowToast(this.Page, hfDepartmentTitle.Value + " has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, "Delete department failed.", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbRename_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check input data
                if (String.IsNullOrEmpty(tbEditName.Text.Trim()))
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, ErrorMessage.DepartmentNameCantBeEmpty, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }
                #endregion

                #region Step 2. Call API
                if (hfDepartmentTitle.Value.Equals(tbEditName.Text))
                {
                    mpePop.Hide();
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                else
                {
                    DepartmentUpdateResponse response = asc.UpdateDepartment(adminInfor.UserId, adminInfor.CompanyId, hfDepartmentId.Value, tbEditName.Text);
                    String toastMsg = String.Empty;
                    if (response.Success)
                    {
                        mpePop.Hide();
                        GetDepartmentList();
                        lvDepartment.DataSource = departmentList;
                        lvDepartment.DataBind();
                        MessageUtility.ShowToast(this.Page, hfDepartmentTitle.Value + " has been updated to " + tbEditName.Text, MessageUtility.TOAST_TYPE_INFO);
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    }
                    else
                    {
                        Log.Error("DepartmentUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        mpePop.Show();
                        MessageUtility.ShowToast(this.Page, response.ErrorMessage.Replace("name", "name " + tbEditName.Text.Trim()), MessageUtility.TOAST_TYPE_ERROR);
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbOpenAddDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                tbAddName.Text = String.Empty;
                mpePop.PopupControlID = "popup_adddepartment";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSortByName_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isSortByNameAcsend = Convert.ToBoolean(ViewState["IsSortByNameAcsend"]);
                ViewState["IsSortByNameAcsend"] = !isSortByNameAcsend;
                GetDepartmentList();
                lvDepartment.DataSource = departmentList;
                lvDepartment.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvDepartment_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (departmentList == null || departmentList.Count == 0)
                {
                    if (e.Item.ItemType == ListViewItemType.EmptyItem)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        ltlEmptyMsg.Text = @"Department is empty.<br /><img src='/img/tips_icon.png' width='16' height='20' /> You can add department with the add button at the bottom right corner of this page.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}