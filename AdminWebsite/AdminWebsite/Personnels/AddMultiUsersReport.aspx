﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AddMultiUsersReport.aspx.cs" Inherits="AdminWebsite.Personnels.AddMultiUsersReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script>
        function displayPath(inputFile)
        {
            $('#lblMultiUserCsvPath').text("Selected File: " + inputFile.value);
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <asp:UpdatePanel ID="upPop" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Ready to go -->
            <asp:Panel ID="popup_addmultipleusersready" runat="server" CssClass="popup addmultipleusers-form" Width="100%" Style="display: none; max-height: 261px !important;">

                <p class="center clear">
                    <i class="fa fa-check-circle fa-5x" style="color: green; margin-top: 10px;"></i>
                </p>

                <h1 class="popup__title center"><strong>You're ready to go</strong></h1>

                <div class="">

                    <p class="center">
                        <asp:Literal ID="ltlImportUserCount" runat="server" />
                        Personnel has been verified.
                    </p>

                    <p class="center">Proceed to update?</p>

                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="btnImport" runat="server" CssClass="popup__action__item popup__action__item--cta" Text="UPDATE" OnClick="btnImport_Click" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="ReadyToAddCancelLinkButton" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="ReadyToAddCancelLinkButton_Click" Text="CANCEL" />
                </div>
            </asp:Panel>
            <!-- / Ready to go -->
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_addmultipleusersready"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Personnel <span>console</span></div>
        <div class="appbar__meta">Add multiple users</div>
        <div class="appbar__action">
            <%--<a><i class="fa fa-question-circle"></i></a>--%>
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- / App Bar -->
    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Personnel/Personnel">User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Personnel/Department">Department</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Add multiple users</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">
            <asp:Panel ID="plContent" runat="server" CssClass="report-header">
                <div class="left red" style="margin: 0 3em;">
                    <i class="fa fa-times-circle fa-5x" style="font-size: 8em;"></i>
                </div>
                <div class="right" style="margin: 1em;">
                    <label for="<%=ufMultiUsersCsv.ClientID %>" style="cursor: pointer;" class="btn secondary uppercase">
                        <span>
                            <i class="fa fa-download"></i>
                            Attach File
                        </span>
                        <asp:FileUpload ID="ufMultiUsersCsv" CssClass="multiUsersCsv" runat="server" accept=".csv" title="Choose csv file" Style="display: none;" onchange="displayPath(this);" />
                    </label>
                    <asp:Button ID="btnReupload" runat="server" Text="RE-UPLOAD .CSV" CssClass="report-upload" OnClick="btnReupload_Click"  OnClientClick="ShowProgressBar();" />
                </div>
                <div class="report-error-message">
                    <h2 style="margin-top: 1em;">Unsuccessful</h2>
                    <p>
                        The file is not uploaded into our database.<br />
                        Please amend the
                        <span class="report-errorcount">
                            <asp:Literal ID="ltlErrorUsersCount" runat="server" />
                            Personnel</span>
                        with errors on your .CSV before re-loading again.
                    </p>
                    <span id="lblMultiUserCsvPath"></span>
                </div>
                <div class="clear-float"></div>
            </asp:Panel>

            <asp:UpdatePanel ID="FailedUserUploadPanel" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvErrorUpload" runat="server" OnItemDataBound="lvErrorUpload_ItemDataBound">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="" style="table-layout: auto">
                                <thead>
                                    <tr runat="server" style="border-top-color: #ddd; border-top-width: 1px; border-top-style: solid;">
                                        <th class="no-sort" style="width: 80px; white-space: nowrap; vertical-align: top;">
                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="CSV No." />
                                        </th>
                                        <th class="no-sort" style="width: 200px">
                                            <asp:LinkButton ID="lbSortByName" runat="server" Text="Name" />
                                        </th>
                                        <th class="no-sort" style="width: 200px">
                                            <asp:LinkButton ID="LinkButton3" runat="server" Text="Email" />
                                        </th>
                                        <th class="no-sort" style="width: 200px">
                                            <asp:LinkButton ID="lbSortByDepartment" runat="server" Text="Department" />
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="lbSortByType" runat="server" Text="Errors" />
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lblCSVNo" runat="server" Text='<%# Eval("No") %>' />
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("FirstName") +" "+ Eval("LastName") %>' />
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                                </td>
                                <td style="vertical-align: top;">
                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("Department") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblErrors" runat="server" CssClass="red" />
                                </td>

                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
