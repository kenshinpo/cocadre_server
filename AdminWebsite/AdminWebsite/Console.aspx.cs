﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using log4net;

namespace AdminWebsite
{
    public partial class Console1 : System.Web.UI.Page
    {
        private ManagerInfo adminInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            ltlAccountType.Text = adminInfo.AccountType.Title;
            rtModule.DataSource = adminInfo.AccessModules;
            rtModule.DataBind();
        }

        protected void rtModule_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HyperLink hlModule = e.Item.FindControl("hlModule") as HyperLink;
                    System.Web.UI.WebControls.Image imgModule = e.Item.FindControl("imgModule") as System.Web.UI.WebControls.Image;
                    Label lblTitle = e.Item.FindControl("lblTitle") as Label;
                    Label lbldescription = e.Item.FindControl("lbldescription") as Label;

                    hlModule.Enabled = adminInfo.AccessModules[e.Item.ItemIndex].IsUsing;
                    // Set Module first page
                    switch (adminInfo.AccessModules[e.Item.ItemIndex].Key)
                    {
                        case CassandraService.Entity.Module.MODULE_PERSONNEL:
                            hlModule.NavigateUrl = "/Personnel/Personnel";
                            break;
                        case CassandraService.Entity.Module.MODULE_MATCHUP:
                            hlModule.NavigateUrl = "/MatchUp/TopicList";
                            break;
                        case CassandraService.Entity.Module.MODULE_FEED:
                            hlModule.NavigateUrl = "/Feed/Permission";
                            break;
                        case CassandraService.Entity.Module.MODULE_DASHBOARD:
                            hlModule.NavigateUrl = "/Dashboard/AlertReport";
                            break;
                        case CassandraService.Entity.Module.MODULE_ANALYTICS:
                            hlModule.NavigateUrl = "/Analytics/Summary";
                            break;
                        case CassandraService.Entity.Module.MODULE_COMPANY_PROFILE:
                            hlModule.NavigateUrl = "/CompanyProfile/Profile";
                            break;
                        case CassandraService.Entity.Module.MODULE_EVENT:
                            hlModule.NavigateUrl = "/Event/Event";
                            break;
                        case CassandraService.Entity.Module.MODULE_SURVEY:
                            hlModule.NavigateUrl = "/Survey/List";
                            break;
                        case CassandraService.Entity.Module.MODULE_DYNAMIC_PULSE:
                            hlModule.NavigateUrl = "/DynamicPulse/AnnouncementPulseFeedList";
                            break;
                        case CassandraService.Entity.Module.MODULE_SETTING:
                            hlModule.NavigateUrl = "/Setting/ProfilePhoto";
                            break;
                        case CassandraService.Entity.Module.MODULE_ASSESSMENT:
                            hlModule.NavigateUrl = "/Assessment/List";
                            break;
                        case CassandraService.Entity.Module.MODULE_MLEARNING:
                            hlModule.NavigateUrl = "/MLearning/EvaluationList";
                            break;
                        case CassandraService.Entity.Module.MODULE_GAMIFICATION:
                            hlModule.NavigateUrl = "/Gamification/AchievementList";
                            break;
                        case CassandraService.Entity.Module.MODULE_ANNOUNCEMENT:
                            hlModule.NavigateUrl = "/Announcement/PostNotification";
                            break;
                        case CassandraService.Entity.Module.MODULE_LIVE360:
                            hlModule.NavigateUrl = "/Live360/Likert7List";
                            break;
                        default:
                            hlModule.NavigateUrl = "";
                            break;
                    }
                    if (adminInfo.AccessModules[e.Item.ItemIndex].IsUsing)
                    {
                        imgModule.ImageUrl = "/img/" + adminInfo.AccessModules[e.Item.ItemIndex].IconUrl;
                    }
                    else
                    {
                        imgModule.ImageUrl = "/img/" + adminInfo.AccessModules[e.Item.ItemIndex].IconUrl.Replace(".png", "_invalid.png");
                    }
                    lblTitle.Text = adminInfo.AccessModules[e.Item.ItemIndex].Title;
                    lbldescription.Text = adminInfo.AccessModules[e.Item.ItemIndex].Description;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }
    }
}