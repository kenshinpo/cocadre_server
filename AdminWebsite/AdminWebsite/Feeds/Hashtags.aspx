﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Hashtags.aspx.cs" Inherits="AdminWebsite.Feeds.Hashtags" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <style>
        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(215, 215, 215, 1) !important;
        }

        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(215, 215, 215, 1) !important;
            opacity: 1;
        }

        ::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(215, 215, 215, 1) !important;
            opacity: 1;
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(215, 215, 215, 1) !important;
        }
    </style>
    <script src="/js/Feeds/Hashtags.js"></script>
    <script src="http://rubaxa.github.io/Sortable/Sortable.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <div id="popupBackground" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

    <div id="popupDel" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Delete Hashtag</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <label style="display: inline-block; width: auto;">The hashtag  </label>
                        <span id="spDelHashtag" style="font-weight: 900; display: inline-block;"></span>
                        <label style="width: auto; display: inline-block;">are going to be deleted.</label>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="lbDelConfirm" class="popup__action__item popup__action__item--cta" style="color: rgba(254, 30, 38, 1);" href="javascript:deleteCard()">DELETE</a>
            <a id="lbDelCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDelPopup()">CANCEL</a>
        </div>
    </div>

    <div id="popupRename" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Rename Hashtag</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <label style="width: auto; color: rgba(154, 154, 154, 1); font-weight: 900; display: inline-block;">#</label>
                        <input id="iptRenameHashtag" style="border-color: rgba(154, 154, 154, 1); width: 55%; display: inline-block;" type="text" maxlength="50" placeholder="Enter your hashtag" />
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="lbRenameConfirm" class="popup__action__item popup__action__item--cta" style="color: rgba(254, 30, 38, 1);">Save</a>
            <a id="lbRenameCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideRenamePopup()">CANCEL</a>
        </div>
    </div>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Feed <span>console</span></div>
        <div class="appbar__meta">Hashtags</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- / App Bar -->

    <div class="data">
        <aside class="data__sidebar data__sidebar--size1 filter" style="padding-top: 18px;">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Feed/Permission">Feed Posting Permission</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Hashtags</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Feed/SuspendedUser">Suspend Users from Feed</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">

            <div id="main_content_UpdatePanel1">


                <div class="content feed-suspend">
                    <div class="content__pad">
                        <h1 class="content__title" style="font-size: 1.6em;">Create default #Hashtags</h1>
                        <label style="margin: 0px; color: rgba(165, 165, 165, 1); font-size: 1.1em;">Hashtags serve as a categories function. Suggest a few for your employees.</label>
                        <label style="color: rgba(165, 165, 165, 1); font-size: 1.1em;">The list will be sorted as below.</label>
                        <br />
                        <div class="feed-suspend__search">
                            <div class="feed-suspend__search__input">
                                <div class="suspendedsearch">
                                    <label style="color: rgba(154, 154, 154, 1); font-weight: 900; display: inline-block;">#</label>
                                    <input id="iptHashtag" type="text" maxlength="50" placeholder="Enter your hashtag" style="border-color: rgba(154, 154, 154, 1); width: 55%; display: inline-block;" onkeyup="changeSubmitStyle();" />
                                    <a class="btn disabled" id="btnSubmit" style="margin-left: 50px; background: rgba(203, 203, 203, 1); color: rgba(253, 254, 254, 1); font-size: 1.2em; border-radius: 5px; padding: 13px 45px; margin-bottom: 20px;" disabled="disabled" href="javascript:saveHashTag();">Submit</a>
                                </div>
                                <label style="color: rgba(165, 165, 165, 1);">
                                    *Other than “_”, all other special signs are not accepted.(e.g. !@#$%^&*;:./?)<br />
                                    *Capital and small letters will be registered as the same tag.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="data__content">
                <table class="dataTable hover">
                    <tbody class="hashtags_body" id="sortTbody">
                        <tr>
                            <th style="width: 10%; font-weight: 900; color: black;">No </th>
                            <th style="width: 45%; font-weight: 900; color: black;">Hashtag</th>
                            <th style="width: 15%; font-weight: 900; color: black;">Date Added</th>
                            <th class="no-sort"></th>
                        </tr>
                    </tbody>
                </table>
                <label id="spNoData" style="color: rgba(220, 220, 220, 1); text-align: center; margin-top:20px; font-size:1.2em;">Currently no #hashtags are created</label>
            </div>
        </div>
    </div>
</asp:Content>
