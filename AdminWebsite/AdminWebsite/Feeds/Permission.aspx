﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Permission.aspx.cs" Inherits="AdminWebsite.Feeds.Permission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Feed <span>console</span></div>
        <div class="appbar__meta">Feed Posting Permission</div>
        <div class="appbar__action">
            <%--<a href=""><i class="fa fa-ellipsis-v"></i></a>
            <a href=""><i class="fa fa-question-circle"></i></a>--%>
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- / App Bar -->

    <div class="data">

        <aside class="data__sidebar data__sidebar--size1 filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Feed Posting Permission</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Feed/Hashtags">Hashtags</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Feed/SuspendedUser">Suspend User from Feed</a>
                </li>

            </ul>
        </aside>

        <div class="data__content">
            <div class="content feed-permission">
                <div class="content__pad">
                    <h1 class="content__title">Who can post on Feed?</h1>
                    <p class="gray">Please select one only</p>
                    <div class="feed-permission__management">
                        <asp:UpdatePanel ID="upPremission" runat="server">
                            <ContentTemplate>
                                <asp:RadioButtonList ID="rblPremission" runat="server" OnSelectedIndexChanged="rblPremission_SelectedIndexChanged" RepeatColumns="3" RepeatLayout="Flow" RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Text="Admin Only" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Admin &amp; Moderators" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="All Personnel" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Selected Department only" Value="4"></asp:ListItem>
                                </asp:RadioButtonList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="feed-action">
                        <asp:Button ID="btnUpdate" runat="server" Text="Apply Change" OnClick="btnUpdate_Click" />
                    </div>
                </div>
                <hr />
                <div class="content__pad departments-list--asp">
                    <asp:UpdatePanel ID="upDepartment" runat="server">
                        <ContentTemplate>
                            <asp:CheckBox ID="cbSelectAll" runat="server" OnCheckedChanged="cbSelectAll_CheckedChanged" Text="Select all" AutoPostBack="true" /><br />
                            <asp:CheckBoxList ID="cblDepartment" runat="server" RepeatLayout="Flow" RepeatColumns="4" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="cblDepartment_SelectedIndexChanged" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
