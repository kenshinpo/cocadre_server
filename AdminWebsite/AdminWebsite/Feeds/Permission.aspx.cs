﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;

namespace AdminWebsite.Feeds
{
    public partial class Permission : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo;
        private List<Department> departmentList = null;
        private Company company = null;

        public void GetDepartmentList()
        {
            try
            {
                if (departmentList == null)
                {
                    DepartmentListResponse response = asc.GetAllDepartment(adminInfo.UserId, adminInfo.CompanyId);
                    if (response.Success)
                    {
                        List<Department> list = response.Departments;
                        list.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
                        departmentList = list;
                    }
                    else
                    {
                        Log.Error("GetAllDepartment.Success is false. ErrorMessage: " + response.ErrorMessage);
                        departmentList = null;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
                departmentList = null;
            }
        }

        public void GetCompanyPremission(string companyId, string adminUserId)
        {
            try
            {
                GetCompanyPermissionResponse response = asc.GetCompanyPermission(companyId, adminUserId);
                if (response.Success)
                {
                    company = response.Company;
                }
                else
                {
                    Log.Error("GetCompanyPremission is false. ErrorMessage: " + response.ErrorMessage);
                    company = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }

            try
            {
                adminInfo = Session["admin_info"] as ManagerInfo;
                if (!IsPostBack)
                {
                    #region Get Permission
                    //GetCompanyPremission
                    GetCompanyPremission(adminInfo.CompanyId, adminInfo.UserId);
                    rblPremission.ClearSelection();
                    if (company != null)
                    {
                        for (int k = 0; k < rblPremission.Items.Count; k++)
                        {
                            if (Convert.ToInt16(rblPremission.Items[k].Value) == company.CompanyPermissionType.Code)
                            {
                                rblPremission.Items[k].Selected = true;
                                if (company.CompanyPermissionType.Code == 4)
                                {
                                    cblDepartment.Enabled = true;
                                }
                                else
                                {
                                    cbSelectAll.Enabled = false;
                                    cblDepartment.Enabled = false;
                                }
                                break;
                            }
                        }

                        #region Get department.
                        cblDepartment.Items.Clear();
                        GetDepartmentList();
                        
                        if (departmentList != null)
                        {
                            for (int i = 0; i < departmentList.Count; i++)
                            {
                                cblDepartment.Items.Add(new ListItem(departmentList[i].Title, departmentList[i].Id));
                                if (company.CompanyPermissionDepartments != null && company.CompanyPermissionDepartments.Count > 0)
                                {
                                    for (int j = 0; j < company.CompanyPermissionDepartments.Count; j++)
                                    {
                                        if (departmentList[i].Id.Equals(company.CompanyPermissionDepartments[j]))
                                        {
                                            cblDepartment.Items[i].Selected = true;
                                        }
                                    }
                                }
                            }
                        }


                        bool isSelectAll = true;
                        for (int i = 0; i < cblDepartment.Items.Count; i++)
                        {
                            if (!cblDepartment.Items[i].Selected)
                            {
                                isSelectAll = false;
                                break;
                            }
                        }

                        if (isSelectAll)
                        {
                            cbSelectAll.Checked = true;
                        }
                        else
                        {
                            cbSelectAll.Checked = false;
                        }
                        #endregion
                    }
                    else
                    {
                        // system error.
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }

        }

        protected void rblPremission_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt16(rblPremission.SelectedValue) != 4)
                {
                    cbSelectAll.Checked = false;
                    cblDepartment.ClearSelection();
                    cbSelectAll.Enabled = false;
                    cblDepartment.Enabled = false;
                }
                else
                {
                    cbSelectAll.Enabled = true;
                    cblDepartment.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                List<string> allowDepartmentId = new List<string>();
                if (Convert.ToInt16(rblPremission.SelectedValue) == Company.PermissionType.CODE_DEPARTMENT)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        if (cblDepartment.Items[i].Selected)
                        {
                            allowDepartmentId.Add(cblDepartment.Items[i].Value);
                        }
                    }

                    if (allowDepartmentId.Count < 1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "toast", "ReloadToast();toastr.info('Please select a department');", true);
                        return;
                    }
                }
                #endregion


                CompanyUpdatePermissionResponse response = asc.UpdateCompanyPermisson(adminInfo.CompanyId, adminInfo.UserId, Convert.ToInt16(rblPremission.SelectedValue), allowDepartmentId);
                if (response.Success)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "toast", "ReloadToast();toastr.info('Permission has been updated.');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "toast", "ReloadToast();toastr.info('Permission update failed.');", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void cbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbSelectAll.Checked)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        cblDepartment.Items[i].Selected = true;
                    }
                }
                else
                {
                    cblDepartment.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void cblDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool isSelectAll = true;
            for (int i = 0; i < cblDepartment.Items.Count; i++)
            {
                if (!cblDepartment.Items[i].Selected)
                {
                    isSelectAll = false;
                    break;
                }
            }
            if (isSelectAll)
            {
                cbSelectAll.Checked = true;
            }
            else
            {
                cbSelectAll.Checked = false;
            }
        }
    }
}