﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SuspendedUser.aspx.cs" Inherits="AdminWebsite.Feeds.SuspendedUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script type="text/javascript">
        var delayTimer;
        function RefreshUpdatePanel(event) {
            if (event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 91 || event.keyCode == 93 || event.keyCode == 144 || (event.keyCode > 15 && event.keyCode < 21) || (event.keyCode > 32 && event.keyCode < 41) || (event.keyCode > 111 && event.keyCode < 124)) {
                return false;
            }
            clearTimeout(delayTimer);
            delayTimer = setTimeout(function () {

                __doPostBack('<%= tbSearchKey.ClientID %>', '');
            }, 1000);

            var progress = $('#imgLoading');
            progress.fadeToggle();
        }

        $(document).on("keydown", function (e) {
            if (e.which === 8 && !$(e.target).is("input, textarea")) {
                e.preventDefault();
            }
        });

        function showLoading() {
            var progress = $('#imgLoading');

            progress.fadeToggle();
        }

        function SetFocus() {
            var textBox = document.getElementById('<%= tbSearchKey.ClientID %>');
            var elemLen = textBox.value.length;
            if (document.selection) {
                // Set focus
                // Use IE Ranges
                var oSel = document.selection.createRange();
                // Reset position to 0 & then set at end
                oSel.moveStart('character', -elemLen);
                oSel.moveStart('character', elemLen);
                oSel.moveEnd('character', 0);
                oSel.select();
            }
            else if (textBox.selectionStart || textBox.selectionStart == '0') {
                // Firefox/Chrome
                textBox.selectionStart = elemLen;
                textBox.selectionEnd = elemLen;
            }
            textBox.focus();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>
    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Feed <span>console</span></div>
        <div class="appbar__meta">Suspend User from Feed</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- / App Bar -->

    <div class="data">
        <aside class="data__sidebar data__sidebar--size1 filter" style="padding-top: 18px;">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Feed/Permission">Feed Posting Permission</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Feed/Hashtags">Hashtags</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Suspend Users from Feed</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <div class="content feed-suspend">
                        <div class="content__pad">
                            <h1 class="content__title">Personnel to be suspended</h1>
                            <div class="feed-suspend__search">
                                <div class="feed-suspend__search__input">
                                    <asp:Repeater ID="rtTags" runat="server" OnItemDataBound="rtTags_ItemDataBound" OnItemCommand="rtTags_ItemCommand">
                                        <HeaderTemplate>
                                            <div class="tags">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="tag">
                                                <asp:Label ID="lblTagUserName" runat="server" CssClass="tag__label" />
                                                <asp:LinkButton ID="lbTagCancel" runat="server" CssClass="tag__icon" Text="x" CommandName="Remove" />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <div class="suspendedsearch">
                                        <asp:TextBox ID="tbSearchKey" runat="server" placeholder="Search Personnel" onkeydown="return (event.keyCode!=13);showLoading();" onkeyup="RefreshUpdatePanel(event);" MaxLength="50" OnTextChanged="tbSearchKey_TextChanged" autocomplete="off" />
                                        <img id="imgLoading" src="../img/circle_loading.gif" style="display: none;" />
                                        <div class="suggestions">
                                            <asp:Repeater ID="rtSearchResult" runat="server" OnItemCommand="rtSearchResult_ItemCommand" OnItemDataBound="rtSearchResult_ItemDataBound">
                                                <HeaderTemplate>
                                                    <ul class="suggestions__list">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li class="suggestions__list__item">
                                                        <asp:LinkButton ID="lbAddUser" runat="server" CssClass="suggestions__link" ClientIDMode="AutoID" CommandName="AddUser" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId")+ "," + DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName") %>'>
                                                            <span class="suggestions__name">
                                                                <asp:Literal ID="ltlUserName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName")  %>' />
                                                            </span>
                                                            <span class="suggestions__email"><%# DataBinder.Eval(Container.DataItem, "Email") %></span>
                                                        </asp:LinkButton></li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                                <div class="feed-suspend__search__submit">
                                    <asp:LinkButton ID="lbSuspend" runat="server" CssClass="btn disabled" Text="Suspend" OnClick="lbSuspend_Click" Enabled="false" OnClientClick="ShowProgressBar();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="upSuspension" runat="server" class="data__content">
                <ContentTemplate>
                    <asp:ListView ID="lvSuspension" runat="server" OnItemDataBound="lvSuspension_ItemDataBound" OnItemCommand="lvSuspension_ItemCommand" OnItemCreated="lvSuspension_ItemCreated">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 40px;"></th>
                                        <th>Name </th>
                                        <th>Department</th>
                                        <th>User Type</th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <div class="constrained">
                                        <asp:Image ID="imgPhoto" runat="server" />
                                    </div>
                                </td>
                                <td>
                                    <asp:Label ID="lblName" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblDepartment" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblUserType" runat="server" />
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbRestore" runat="server" CommandName="Restore" Text="Restore" OnClientClick="ShowProgressBar();" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
