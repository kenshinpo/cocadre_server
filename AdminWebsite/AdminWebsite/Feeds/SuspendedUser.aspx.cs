﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.Feeds
{
    public partial class SuspendedUser : System.Web.UI.Page
    {
        private ManagerInfo adminInfor;
        private AdminService asc = new AdminService();
        private List<User> suspendedUserList = null;
        private List<Dictionary<String, String>> selectedList = null;
        private List<User> searchUserList = null;

        public void GetSuspenedUser()
        {
            try
            {
                PostingSuspendedUserListResponse response = asc.GetAllPostingSuspendedUser(adminInfor.UserId, adminInfor.CompanyId);
                if (response.Success)
                {
                    suspendedUserList = response.Users;
                    suspendedUserList = suspendedUserList.OrderBy(o => o.FirstName).ToList();
                }
                else
                {
                    Log.Error("PostingSuspendedUserListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        public void GetSearchUser()
        {
            try
            {
                UserListResponse response;
                if (String.IsNullOrEmpty(tbSearchKey.Text.Trim()))
                {
                    searchUserList = new List<User>();
                }
                else
                {
                    response = asc.SearchUserForPostingSuspend(adminInfor.UserId, adminInfor.CompanyId, tbSearchKey.Text.Trim());
                    if (response.Success)
                    {
                        selectedList = ViewState["SelectedUser"] as List<Dictionary<String, String>>;
                        searchUserList = new List<User>();

                        for (int i = 0; i < selectedList.Count; i++)
                        {
                            User user = (from a in response.Users where a.UserId == selectedList[i]["UserId"] select a).FirstOrDefault();
                            if (user != null)
                            {
                                response.Users.Remove(user);
                            }
                        }
                        searchUserList = response.Users;
                    }
                    else
                    {
                        Log.Error("SearchUserForPostingSuspend.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["admin_info"] == null)
                {
                    Response.Redirect("/Logout");
                    return;
                }
                adminInfor = Session["admin_info"] as ManagerInfo;


                if (!IsPostBack)
                {
                    ViewState["SelectedUser"] = new List<Dictionary<String, String>>();

                    GetSuspenedUser();
                    lvSuspension.DataSource = suspendedUserList;
                    lvSuspension.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvSuspension_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (suspendedUserList != null && suspendedUserList.Count > 0)
                {
                    if (e.Item.ItemType == ListViewItemType.DataItem)
                    {
                        System.Web.UI.WebControls.Image imgPhoto = e.Item.FindControl("imgPhoto") as System.Web.UI.WebControls.Image;
                        Label lblName = e.Item.FindControl("lblName") as Label;
                        Label lblDepartment = e.Item.FindControl("lblDepartment") as Label;
                        Label lblUserType = e.Item.FindControl("lblUserType") as Label;
                        LinkButton lbRestore = e.Item.FindControl("lbRestore") as LinkButton;

                        imgPhoto.ImageUrl = suspendedUserList[e.Item.DataItemIndex].ProfileImageUrl.Replace("_original", "_large");
                        lblName.Text = suspendedUserList[e.Item.DataItemIndex].FirstName + " " + suspendedUserList[e.Item.DataItemIndex].LastName;
                        lblDepartment.Text = suspendedUserList[e.Item.DataItemIndex].Departments[0].Title;
                        lblUserType.Text = suspendedUserList[e.Item.DataItemIndex].Type.Title;
                        lbRestore.CommandArgument = suspendedUserList[e.Item.DataItemIndex].UserId;
                    }
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetFocus", "SetFocus();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvSuspension_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                Panel chipsPnl = e.Item.FindControl("chipsPnl") as Panel;

                if (e.CommandName.Equals("Restore"))
                {
                    PostingPermissionRestoreUserResponse response = asc.PostingPermissionResotreUser(adminInfor.UserId, adminInfor.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        //chipsPnl.Visible = false;
                        GetSuspenedUser();
                        lvSuspension.DataSource = suspendedUserList;
                        lvSuspension.DataBind();
                        MessageUtility.ShowToast(this.Page, "User has been restored.", MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        MessageUtility.ShowToast(this.Page, "Restored user failed", MessageUtility.TOAST_TYPE_ERROR);
                    }
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void tbSearchKey_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetSearchUser();
                rtSearchResult.DataSource = searchUserList;
                rtSearchResult.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddUser"))
                {
                    selectedList = ViewState["SelectedUser"] as List<Dictionary<String, String>>;
                    String[] value = Convert.ToString(e.CommandArgument).Split(',');
                    Dictionary<String, String> dic = new Dictionary<String, String>();
                    dic.Add("UserId", value[0]);
                    dic.Add("UserName", value[1]);
                    selectedList.Add(dic);
                    rtTags.DataSource = selectedList;
                    rtTags.DataBind();
                    ViewState["SelectedUserId"] = selectedList;
                    tbSearchKey.Text = String.Empty;
                    GetSearchUser();
                    rtSearchResult.DataSource = searchUserList;
                    rtSearchResult.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtTags_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label lblTagUserName = e.Item.FindControl("lblTagUserName") as Label;
                    LinkButton lbTagCancel = e.Item.FindControl("lbTagCancel") as LinkButton;
                    lblTagUserName.Text = selectedList[e.Item.ItemIndex]["UserName"];
                    lbTagCancel.CommandArgument = selectedList[e.Item.ItemIndex]["UserId"];
                }

                if (selectedList == null || selectedList.Count == 0)
                {
                    lbSuspend.CssClass = "btn disabled";
                    lbSuspend.Enabled = false;
                }
                else
                {
                    lbSuspend.CssClass = "btn";
                    lbSuspend.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetFocus", "SetFocus();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtTags_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                selectedList = ViewState["SelectedUser"] as List<Dictionary<String, String>>;
                if (selectedList == null || selectedList.Count == 0)
                {
                    Log.Error(@"ViewState[""SelectedUser""] is null.", this.Page);
                }
                else
                {
                    for (int i = 0; i < selectedList.Count; i++)
                    {
                        if (selectedList[i]["UserId"] == Convert.ToString(e.CommandArgument))
                        {
                            selectedList.RemoveAt(i);
                            break;
                        }
                    }

                    ViewState["SelectedUser"] = selectedList;

                    if (selectedList == null || selectedList.Count == 0)
                    {
                        lbSuspend.CssClass = "btn disabled";
                        lbSuspend.Enabled = false;
                    }
                    else
                    {
                        lbSuspend.CssClass = "btn";
                        lbSuspend.Enabled = true;
                    }

                    rtTags.DataSource = selectedList;
                    rtTags.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSuspend_Click(object sender, EventArgs e)
        {
            try
            {
                selectedList = ViewState["SelectedUser"] as List<Dictionary<String, String>>;
                List<String> selectIds = new List<string>();
                for (int i = 0; i < selectedList.Count; i++)
                {
                    selectIds.Add(selectedList[i]["UserId"]);
                }

                PostingPermissionSuspendUserResponse response = asc.PostingPermissionSuspendUsers(adminInfor.UserId, adminInfor.CompanyId, selectIds);
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    if (selectIds.Count == 1)
                    {
                        toastMsg = "User has been suspended";
                    }
                    else
                    {
                        toastMsg = "Users have been suspended";
                    }
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    GetSuspenedUser();
                    lvSuspension.DataSource = suspendedUserList;
                    lvSuspension.DataBind();
                    selectedList = new List<Dictionary<string, string>>();
                    ViewState["SelectedUser"] = selectedList;
                    rtTags.DataSource = selectedList;
                    rtTags.DataBind();
                    lbSuspend.CssClass = "btn disabled";
                    lbSuspend.Enabled = false;
                }
                else
                {
                    toastMsg = "Failed to suspended user(s)";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    Log.Error("PostingPermissionSuspendUserResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvSuspension_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (selectedList == null || selectedList.Count == 0)
                {
                    if (e.Item.ItemType == ListViewItemType.EmptyItem)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        ltlEmptyMsg.Text = "There are no users suspended from posting feed at the moment.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}