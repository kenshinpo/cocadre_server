﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Feeds
{
    public partial class Hashtags : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                ClientScript.RegisterHiddenField("hfCompanyId", managerInfo.CompanyId);
                ClientScript.RegisterHiddenField("hfManagerId", managerInfo.UserId);
                ClientScript.RegisterHiddenField("hfTimezone", Convert.ToString(managerInfo.TimeZone));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}