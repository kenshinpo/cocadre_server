﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Linq;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using System.Web.Security;

namespace AdminWebsite
{
    public partial class Navi : System.Web.UI.MasterPage
    {
        private ManagerInfo managerInfo;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Log.Error("Session Timeout! Redirect to Logout.");
                Response.Redirect("/Logout");
            }
            else
            {
                managerInfo = Session["admin_info"] as ManagerInfo;
                if(managerInfo.IsLogined)
                {
                    #region Reload admin_info from db
                    AdminService asc = new AdminService();
                    GetManagerInfoResponse response = asc.GetManagerInfo(managerInfo.UserId, managerInfo.CompanyId);
                    if (response.Success)
                    {
                        managerInfo.AccessModules = response.ManagerAccessModules;
                        managerInfo.AccessSystemModules = response.ManagerAccessSystemModules;
                        managerInfo.AccountType = response.ManagerAccountType;
                        managerInfo.CompanyAdminImageUrl = response.CompanyAdminUrl;
                        managerInfo.CompanyId = response.CompanyId;
                        managerInfo.CompanyLogoUrl = response.CompanyLogoUrl;
                        managerInfo.CompanyName = response.CompanyName;
                        managerInfo.TimeZone = response.CompanyTimeZone;
                        managerInfo.Email = response.ManagerEmail;
                        managerInfo.FirstName = response.ManagerFirstName;
                        managerInfo.LastName = response.ManagerLastName;
                        managerInfo.ProfileImageUrl = response.ManagerProfileImageUrl;
                        managerInfo.UserId = response.ManagerUserId;
                        managerInfo.PrimaryManagerId = response.PrimaryManagerId;
                        managerInfo.CompnayWebsiteHeader = response.CompanyWebsiteHeaderUrl;
                        Session["admin_info"] = managerInfo;
                    }
                    else
                    {
                        Log.Error("User is invalid! Redirect to Logout.");
                        Response.Redirect("/Logout");
                    }
                    #endregion
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            managerInfo = Session["admin_info"] as ManagerInfo;
            user_email.Text = managerInfo.Email + "<i class='fa fa-caret-down'></i>";
            user_email.Attributes["onmouseover"] = "this.style.cursor='pointer'";
            ltlInforCompanyName1.Text = managerInfo.CompanyName;
            ltlInforCompanyName2.Text = managerInfo.CompanyName;
            imgUserPhoto.ImageUrl = managerInfo.ProfileImageUrl;
            ltlInforUserName.Text = managerInfo.FirstName + " " + managerInfo.LastName;
            ltlInforUserEmail.Text = managerInfo.Email;
            imgCompanyLogo.ImageUrl = managerInfo.CompnayWebsiteHeader;
            rtOtherAccounts.DataSource = managerInfo.Companies.Where(c => c.CompanyId != managerInfo.CompanyId).ToList();
            rtOtherAccounts.DataBind();

            string path_info = Request.ServerVariables["Path_Info"];
            string page = path_info.StartsWith("/") ? path_info.Substring(1, path_info.Length - 1) : path_info;
            int index = page.IndexOf("/");
            if (index > 0)
            {
                page = page.Substring(0, index);
            }
            List<CassandraService.Entity.Module> accessModule = managerInfo.AccessModules;
            bool isCorrectRights = false;

            // Add "Home" first.
            HtmlGenericControl ul = new HtmlGenericControl("ul");
            HtmlGenericControl li = new HtmlGenericControl("li");
            HyperLink link = new HyperLink();
            link.Text = "Home";
            link.NavigateUrl = "/Home";
            if (page.Contains("Home"))
            {
                li.Attributes.Add("class", "active");
                isCorrectRights = true;
            }
            li.Controls.Add(link);
            ul.Controls.Add(li);

            // Add access module.
            for (int i = 0; i < accessModule.Count; i++)
            {
                li = new HtmlGenericControl("li");
                link = new HyperLink();
                link.Text = accessModule[i].Title;

                // Set Module first page
                if (accessModule[i].IsUsing)
                {
                    switch (accessModule[i].Key)
                    {
                        case CassandraService.Entity.Module.MODULE_PERSONNEL:
                            link.NavigateUrl = "/Personnel/Personnel";
                            break;
                        case CassandraService.Entity.Module.MODULE_MATCHUP:
                            link.NavigateUrl = "/MatchUp/TopicList";
                            break;
                        case CassandraService.Entity.Module.MODULE_FEED:
                            link.NavigateUrl = "/Feed/Permission";
                            break;
                        case CassandraService.Entity.Module.MODULE_DASHBOARD:
                            link.NavigateUrl = "/Dashboard/AlertReport";
                            break;
                        case CassandraService.Entity.Module.MODULE_ANALYTICS:
                            link.NavigateUrl = "/Analytics/Summary";
                            break;
                        case CassandraService.Entity.Module.MODULE_COMPANY_PROFILE:
                            link.NavigateUrl = "/CompanyProfile/Profile";
                            break;
                        case CassandraService.Entity.Module.MODULE_EVENT:
                            link.NavigateUrl = "/Event/Event";
                            break;
                        case CassandraService.Entity.Module.MODULE_SURVEY:
                            link.NavigateUrl = "/Survey/List";
                            break;
                        case CassandraService.Entity.Module.MODULE_DYNAMIC_PULSE:
                            link.NavigateUrl = "/DynamicPulse/AnnouncementPulseFeedList";
                            break;
                        case CassandraService.Entity.Module.MODULE_SETTING:
                            link.NavigateUrl = "/Setting/ProfilePhoto";
                            break;
                        case CassandraService.Entity.Module.MODULE_MLEARNING:
                            link.NavigateUrl = "/MLearning/EvaluationList";
                            break;
                        case CassandraService.Entity.Module.MODULE_GAMIFICATION:
                            link.NavigateUrl = "/Gamification/AchievementList";
                            break;
                        case CassandraService.Entity.Module.MODULE_ANNOUNCEMENT:
                            link.NavigateUrl = "/Announcement/PostNotification";
                            break;
                        case CassandraService.Entity.Module.MODULE_LIVE360:
                            link.NavigateUrl = "/Live360/Likert7List";
                            break;
                        default:
                            link.NavigateUrl = "";
                            link.Style.Add("color", "#9D9D9D");
                            link.Enabled = false;
                            break;
                    }
                }
                else
                {
                    link.NavigateUrl = "";
                    link.Style.Add("color", "#9D9D9D");
                    link.Enabled = false;
                }

                if (page.ToLower().Contains(accessModule[i].RootPath.ToLower().Replace(" ", "").Replace("-", "")))
                {
                    li.Attributes.Add("class", "active");
                    isCorrectRights = true;
                }

                li.Controls.Add(link);
                ul.Controls.Add(li);
            }

            navigation.Controls.Add(ul);

            #region Check system modules
            if (managerInfo.AccessSystemModules != null)
            {
                for (int i = 0; i < managerInfo.AccessSystemModules.Count; i++)
                {
                    if (page.ToLower().Contains(managerInfo.AccessSystemModules[i].RootPath.ToLower().Replace(" ", "")))
                    {
                        isCorrectRights = true;
                    }
                }
            }
            #endregion

            #region Check test modules
            if (page.ToLower().Contains("test"))
            {
                isCorrectRights = true;
            }
            #endregion

            if (!isCorrectRights)
            {
                Response.Redirect("/Home");
            }
        }

        protected void user_email_Click(object sender, EventArgs e)
        {
            Response.Redirect("Logout");
        }

        protected void rtOtherAccounts_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Switch")) // Switch account
                {
                    managerInfo = Session["admin_info"] as ManagerInfo;
                    managerInfo.CompanyId = e.CommandArgument.ToString().Split(',')[0];
                    managerInfo.UserId = e.CommandArgument.ToString().Split(',')[1];
                    Session["admin_info"] = managerInfo;

                    FormsAuthentication.RedirectFromLoginPage(managerInfo.Email, false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                Response.Redirect("Logout");
            }
        }
    }
}