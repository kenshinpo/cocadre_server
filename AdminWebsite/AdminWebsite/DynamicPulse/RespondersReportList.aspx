﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="RespondersReportList.aspx.cs" Inherits="AdminWebsite.DynamicPulse.RespondersReportList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .appbar__meta a {
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfDeckId" />
    <asp:HiddenField runat="server" ID="hfPulseId" />
    <asp:HiddenField runat="server" ID="hfOptionId" />
    <asp:HiddenField runat="server" ID="hfDepartmentName" />

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><a href="/Survey/List" style="color: #000;">Dynamic <span>pulse</span></a></div>
        <div class="appbar__meta"><a href="" id="survey-root-link">Responsive Pulse</a></div>
        <div class="appbar__meta"><a href="" class="pulse-title"></a></div>
        <div class="appbar__meta"><a href="" class="question"></a></div>
        <div class="appbar__meta"><a href="" class="answer"></a></div>
        <div class="appbar__meta breadcrumb-participant"></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="content-wrapper" class="data" style="overflow-y: scroll;">

        <div id="responsive-pulse" class="container" style="width: 100%;">

            <div style="width: 100%; margin: 20px auto;">

                <div class="title-section" style="width: 100%; margin: 0 auto;">

                    <div class="full-row">
                        <div class="card-7-col no-padding">
                            <h1 class="pulse-title no-margin ellipsis-title"></h1>
                        </div>
                        <div class="card-3-col align-right no-padding">
                        </div>
                    </div>

                    <div class="full-row">
                        <div class="card-7-col no-padding">
                            <p class="secondary-label question no-margin"></p>
                        </div>
                        <div class="pulse-status card-3-col align-right no-padding">
                        </div>
                    </div>

                    <div class="full-row">
                        <div class="card-7-col no-padding">
                            <p class="align-left">Showing data from <span class="start-date"></span><span class="end-date"></span></p>
                        </div>
                        <div class="card-3-col align-right no-padding">
                            <p style="margin-top: 10px;">End date: <span class="end-date"></span></p>
                        </div>
                    </div>

                </div>

                <div id="tab" class="tabs tabs--styled">
                    <ul class="tabs__list">
                        <li class="tabs__list__item">
                            <a class="tabs__link" href="#responders-report">Responders report</a>
                        </li>
                    </ul>
                    <div class="tabs__panels">
                        <div class="tabs__panels__item responders-report" id="responders-report" style="width: 100%;">

                            <div class="full-row align-center animated fadeInUp">
                                <div class="input-group" style="position: relative; margin: 0 auto;">
                                    <i class="fa fa-search"></i>
                                    <input name="hidden_name" id="hidden_name" class="search-input" type="text" placeholder="Search Responders" value="" />
                                </div>
                            </div>

                            <div id="personnel_list" class="card animated fadeInUp no-padding">
                                <div class="card-header" style="padding: 20px 20px 0px 20px;">
                                    <h1 class="title secondary-label " style="font-size: 14px; float: left;">Answer: <span class="answer" style="color: #000;"></span></h1>
                                    <p style="float: right; font-weight: 700;" class="total_respondent"></p>
                                </div>
                                <div class="table-content">

                                    <table id="responder-table">
                                        <thead>
                                            <th data-dynatable-column="no" style="width: 10%">No</th>
                                            <th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width: 40%">Name</th>
                                            <th class="hidden" data-dynatable-column="hidden_name">Hidden name</th>
                                            <th data-dynatable-column="date" data-dynatable-sorts="date" style="width: 20%">Date</th>
                                            <th data-dynatable-column="department" data-dynatable-sorts="department" style="width: 30%">Department</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="/js/vendor/moment-with-locales.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script>
        $(function () {
            var apiUrl = '/Api/PulseAnalytics/';
            var companyId = $('#main_content_hfCompanyId').val();
            var adminUserId = $('#main_content_hfAdminUserId').val();
            var deckId = $('#main_content_hfDeckId').val();
            var selectedOptionId = $('#main_content_hfOptionId').val();
            var pulseId = $('#main_content_hfPulseId').val();
            var table_data = [];
            var table = $('#responder-table');
            var isFirstPageLoad = true;

            function setStatus(statusCode) {

                var status = ""

                if (statusCode == 1) {
                    status = "Upcoming";
                } else if (statusCode == 2) {
                    status = "Live";
                } else if (statusCode == 3) {
                    status = "Completed";
                }

                var status_html = "";
                status_html += '<div class="legend-item" style="float:right;">';
                status_html += '<div class="legend-icon">';
                status_html += '<div class="legend-color' + statusCode + '" style="border-radius:50%;"></div>';
                status_html += '</div>';
                status_html += '<label>' + status + '</label>';
                status_html += '</div>';

                $('.pulse-status').empty();
                $('.pulse-status').html(status_html);
            }

            function fetchData() {
                $('#responsive-pulse').addClass('hidden');
                $('#responsive-pulse').removeClass('animated fadeInUp');

                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: apiUrl + 'SelectDeckCardOptionAnalytic',
                    data: {
                        "CompanyId": companyId,
                        "AdminUserId": adminUserId,
                        "DeckId": deckId,
                        "PulseId": pulseId,
                        "SelectedOptionId": selectedOptionId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {
                            $('#responsive-pulse').removeClass('hidden');
                            $('#responsive-pulse').addClass('animated fadeInUp');

                            var deck = res.Deck;
                            var pulse = res.Pulse;
                            var responders = res.Responders;
                            setStatus(deck.Status);

                            $('.pulse-title').html(deck.Title);
                            $('#pulse-stat .stat-title').html('RP' + pulse.Ordering);
                            $('.question').html('RP' + pulse.Ordering + ": " + pulse.Title);
                            $('.start-date').html(deck.DeckStartTimestampString);

                            if (deck.DeckEndTimestampString) {
                                $('.end-date').html('to ' + deck.DeckEndTimestampString);
                            } else {
                                $('.end-date').html('until now');
                            }

                            if (pulse.CardType == 3) {
                                $('.answer').html(pulse.TextAnswers[0].Content);
                            } else {
                                $('.answer').html(pulse.Options[0].Content);
                            }

                            $('.total_respondent').html(pulse.TotalRespondents + ' Responders');

                            table_data = [];

                            if (responders.length > 0) {
                                var isDeleted = false;
                                $.each(responders, function (key, value) {
                                    isDeleted = value.Responder.Status.Code === -999 ? true : false;
                                    table_data.push({
                                        "no": key + 1,
                                        "responder_name": '<a href="/DynamicPulse/ResponderReportDetail/' + deckId + '/' + value.Responder.UserId + '" class="responder-name" data-userid="' + value.Responder.UserId + '" target="_blank">' + value.Responder.FirstName + " " + value.Responder.LastName + '</a>' + (isDeleted === true ? " (Deleted)" : ""),
                                        "hidden_name": value.Responder.FirstName + " " + value.Responder.LastName,
                                        "date": value.AnsweredOnTimestampString != undefined ? value.AnsweredOnTimestampString : " ",
                                        "department": value.Responder.Departments[0].Title,
                                    });
                                });

                                table.dynatable({
                                    features: {
                                        paginate: false,
                                        search: false,
                                        sorting: true,
                                        recordCount: false,
                                        pushState: false,
                                    },
                                    dataset: {
                                        records: table_data,
                                        sorts: { 'no': 1 }
                                    },
                                    inputs: {
                                        queries: $('#hidden_name')
                                    }
                                })

                                var dynatable = table.data('dynatable');

                                if (typeof dynatable.records !== "undefined") {
                                    dynatable.records.updateFromJson({ records: table_data });
                                    dynatable.records.init();
                                }
                                dynatable.paginationPerPage.set(15);
                                dynatable.process();
                            }

                        } else {
                            $('#content-wrapper').prepend('<p class="align-center" style="margin-top:70px; width:100%;">No analytic for the selected pulse. </p>');
                        }
                    }
                });
            }

            table.bind('dynatable:afterProcess', function (e, dynatable) {
                if ($('#main_content_hfDepartmentName').val() != "" && isFirstPageLoad) {
                    $('#hidden_name').val($('#main_content_hfDepartmentName').val());
                    var e = $.Event("keypress", { which: 13 });
                    $('#hidden_name').trigger(e);
                    isFirstPageLoad = false;
                }
            });

            table.bind('dynatable:afterUpdate', function (e, dynatable) {
                setTimeout(function () {
                    table.css('display', 'table');
                    table.addClass('animated fadeIn');
                }, 500);
            });

            // CUSTOM TABLE SEARCH FILTER FUNCTION
            table.bind('dynatable:init', function (e, dynatable) {
                dynatable.queries.functions['hidden_name'] = function (record, queryValue) {
                    return record.department.toLowerCase().indexOf(queryValue.toLowerCase()) > -1 || record.hidden_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                };
            });
            fetchData();
        });
    </script>
</asp:Content>
