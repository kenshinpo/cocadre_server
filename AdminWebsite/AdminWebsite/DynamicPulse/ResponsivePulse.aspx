﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ResponsivePulse.aspx.cs" Inherits="AdminWebsite.DynamicPulse.ResponsivePulse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
	<link href="/css/ion.rangeSlider/ion.rangeSlider.css" rel="stylesheet" />
	<link href="/css/ion.rangeSlider/ion.rangeSlider.skinNice.css" rel="stylesheet" />
	<style type="text/css">
		.ui-autocomplete-loading {
			background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
		}

		.invalid-field {
			border-bottom-color: #ff0000 !important;
		}

		/* CSS for on-off switch */
		.onoffswitch {
			position: relative;
			width: 76px;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
		}

		.onoffswitch-checkbox {
			display: none;
		}

		.onoffswitch-label {
			display: block;
			overflow: hidden;
			cursor: pointer;
			border-radius: 20px;
		}

		.onoffswitch-inner {
			display: block;
			width: 200%;
			margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}

			.onoffswitch-inner:before, .onoffswitch-inner:after {
				display: block;
				float: left;
				width: 50%;
				height: 30px;
				padding: 0;
				line-height: 30px;
				font-size: 13px;
				color: white;
				font-weight: bold;
				box-sizing: border-box;
			}

			.onoffswitch-inner:before {
				content: "YES";
				padding-left: 10px;
				background-color: #709EEB;
				color: #F2F2F2;
			}

			.onoffswitch-inner:after {
				content: "NO";
				padding-right: 10px;
				background-color: #4d4d4d;
				color: #F2F2F2;
				text-align: right;
			}

		.onoffswitch-switch {
			display: block;
			width: 26px;
			height: 26px;
			margin: 2px;
			background: #F2F2F2;
			position: absolute;
			bottom: 0px;
			right: 46px;
			border-radius: 20px;
			transition: all 0.3s ease-in 0s;
		}

		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}

		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px;
		}

		.input-field {
			background-color: transparent !important;
			border: none !important;
			border-bottom: 1px solid #ddd !important;
		}

		.duration_tip, .anonymous_tip, .customized_tip {
			position: absolute;
			/*top: 0px;*/
			opacity: 0;
			display: block !important;
			background: #ffffff;
			width: 275px;
			border: 1px solid #ccc;
			padding: 10px;
			z-index: -1000;
			box-shadow: 0px 0px 10px #cccccc;
			transition: all 0.3s ease;
			-webkit-transition: all 0.3s ease;
			-ms-transition: all 0.3s ease;
			-moz-transition: all 0.3s ease;
		}

		.survey-bar__search__input {
			padding-left: 23px !important;
		}

		.customized_tip {
			margin-top: 11px;
		}

		.duration_tip, .anonymous_tip {
			right: 0px;
		}

		.form__row.duration__tip:hover .duration_tip {
			opacity: 1;
		}

		.tooltip-icon:hover + .anonymous_tip {
			opacity: 1;
			z-index: 1000;
		}

		.tooltip-icon:hover + .customized_tip {
			opacity: 1;
			z-index: 1000;
			left: 0px;
		}

		/*

		*/
		button.pulse-type {
			background-color: white;
			color: #ccc;
		}

			button.pulse-type.active {
				background-color: #3f75d2;
				color: #fff;
			}

		span.card-publish-date input, span.card-publish-date select {
			margin: 0;
			padding: 0;
			border: none;
			text-align: center;
			color: #333;
			background-color: #F4F4F4;
			/*border-bottom:1px solid #ccc;*/
		}

		.mini-card, .full-card {
			margin: 0 auto 1.5em;
		}

		button.to-mini-card, button.duplicate-card, button.cascade-card {
			background: none;
			color: #4683ea;
			border: none;
			padding: 0 0 0 10px;
		}

		.card-level-2 {
			width: 98%;
			margin-left: 2% !important;
		}

		.card-level-3 {
			width: 96%;
			margin-left: 4% !important;
		}

		.card-date {
			opacity: 1;
			transition: all 0.3s ease;
			-webkit-transition: all 0.3s ease;
			-ms-transition: all 0.3s ease;
			-moz-transition: all 0.3s ease;
			-o-transition: all 0.3s ease;
		}

		.mini-card-holder {
			/*min-height: 120px;*/
			float: right;
			margin-bottom: 1.5em;
			transition: all 0.3s ease;
			-webkit-transition: all 0.3s ease;
			-ms-transition: all 0.3s ease;
			-moz-transition: all 0.3s ease;
			-o-transition: all 0.3s ease;
		}

		.mini-card, .full-card {
			margin: 0px;
			/*margin-bottom: 15px;*/
		}

		.grid__span--3 {
			width: 12%;
		}

		.grid__span.grid__span--9.grid__span--last {
			width: 85.64%;
		}

		.grid__span--6.pulse-card-container {
			width: 50%;
		}

		.level1-indicator {
			position: absolute;
			top: 50%;
			left: -25px;
			width: 10px;
			height: 10px;
			background: #fff;
			box-shadow: 1px 1px 2px #ccc;
			border: 1px solid #ccc;
			border-radius: 50%;
		}

		.level2-indicator {
			width: 20px;
			height: 40px;
			border-left: 1px solid #ccc;
			border-bottom: 1px solid #ccc;
			position: absolute;
			top: 5px;
			left: -20px;
		}

		.card-holder {
			clear: both;
		}

		.fake-layer1 {
			position: absolute;
			width: 100%;
			height: 5px;
			bottom: -5px;
			left: 0px;
			background: white;
			border-radius: 2px;
			border: 1px solid rgba(0, 0, 0, 0.2);
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
		}

		.fake-layer2 {
			position: absolute;
			width: 98%;
			height: 5px;
			bottom: -10px;
			left: 2%;
			background: white;
			border-radius: 2px;
			border: 1px solid rgba(0, 0, 0, 0.2);
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
		}

		.fake-layer {
			display: none;
		}
	</style>
	<script src="/js/DynamicPulse/ResponsivePulse.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

	<div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

	<div id="plSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
		<h1 class="popup__title">Select Department</h1>
		<div class="popup__content">
			<fieldset class="form">
				<div class="container">
					<div class="accessrights">
						<input id="selectAllDepartment" type="checkbox" onclick="selectAllDepartments(this);" />
						<label for="selectAllDepartment">Select All</label>
					</div>
					<hr />
					<div class="accessrights departments">
						<span id="department_checkboxlist">
							<label>
								<input type="checkbox" value="D04461505c52d44d8be0ff0d80c6a2408" />Human Resource</label><br />
						</span>
					</div>
				</div>
			</fieldset>
		</div>
		<div class="popup__action">
			<a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:saveDepartmentSelection()">Select</a>
			<a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDepartmentSelector()">Cancel</a>
		</div>
	</div>

	<div id="plSelectDecks" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
		<h1 class="popup__title">Import deck</h1>
		<div class="popup__content">
			<fieldset class="form">
				<div class="container">
					<span>Please select the deck you want to import the pulses from</span>
					<p></p>
					<div class="accessrights">
						<span id="deck_checkboxlist"></span>
					</div>
				</div>
			</fieldset>
		</div>
		<div class="popup__action">
			<a id="lbSelectDeckSelect" class="popup__action__item popup__action__item--cta" href="javascript:saveDeckSelection()">Select</a>
			<a id="lbSelectDeckCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDeckSelector()">Cancel</a>
		</div>
	</div>

	<div id="plDelPulse" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
		<h1 class="popup__title">Delete Pulse</h1>
		<div class="popup__content">
			<fieldset class="form">
				<div class="container">
					<div class="accessrights">
						<label style="display: inline-block; width: auto;">The pulse of </label>
						<span id="spDelPulseTime" style="font-weight: 900; display: inline-block;"></span>
						<label>are going to be deleted.</label>
					</div>
				</div>
			</fieldset>
		</div>
		<div class="popup__action">
			<a id="lbDelPulseConfirm" class="popup__action__item popup__action__item--cta" style="color: rgba(254, 30, 38, 1);" href="javascript:deleteCard()">DELETE</a>
			<a id="lbDelPulseCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDelPulse()">CANCEL</a>
		</div>
	</div>

	<div id="plEditPulseDateTime" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
		<h1 class="popup__title">Data changes</h1>
		<div class="popup__content">
			<fieldset class="form">
				<div class="container">
					<div class="accessrights">
						<label style="display: inline-block; width: auto;">The following pulse's date has been changed</label>
						<br />
						<div>
							<label style="width: 8%; float: left;">from </label>
							<span id="spDateTimeFrom" style="font-weight: bold;"></span>
						</div>
						<br />
						<div>
							<label style="width: 8%; float: left;">to </label>
							<span id="spDateTimeTo" style="font-weight: bold;"></span>
						</div>
						<br />
					</div>
				</div>
			</fieldset>
		</div>
		<div class="popup__action" style="padding-left: 1.5rem; font-weight: bold; width: 100%; display: flex; justify-content: space-between;">
			<a id="lbEditCancel" style="color: rgba(0, 0, 0, 1); text-transform: uppercase;">Cancel</a>
			<a id="lbEditDateTimeAll" class="" style="color: rgba(0, 118, 255, 1); text-transform: uppercase;">Save for future pulses</a>
			<a id="lbEditDateTimeOnly" class="" style="color: rgba(0, 118, 255, 1); text-transform: uppercase;">Save for this pulse only</a>
		</div>
	</div>

	<asp:HiddenField runat="server" ID="hfAdminUserId" />
	<asp:HiddenField runat="server" ID="hfCompanyId" />
	<asp:HiddenField runat="server" ID="hfTimezone" />
	<asp:HiddenField runat="server" ID="hfDeckId" />
	<asp:HiddenField runat="server" ID="hfDeck" />
	<asp:HiddenField runat="server" ID="hfPulseList" />

	<div class="data__content" style="background: rgba(244, 244, 244, 1);">

		<!-- App Bar -->
		<div class="appbar">
			<div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
			<div class="appbar__title"><a href="/DynamicPulse/ResponsivePulseFeedList" style="color: #000;">Dynamic <span>Pulse</span></a></div>
			<div class="appbar__meta">
				<asp:Literal ID="ltlActionName" runat="server" Text="Add 'Responsive' Pulse" />
			</div>
			<div class="appbar__action">
			</div>
		</div>
		<!-- / App Bar -->

		<div class="responsive-pulse-root">
			<div class="container">

				<div class="card card-header grid" style="border: none; box-shadow: none; margin: 1.5em 10px 0px 10px; padding: 0px; background: rgba(244, 244, 244, 1);">
					<div class="grid__inner" style="background-color: transparent;">
						<!-- PULSE LOGO START -->
						<div class="grid__span--2">
							<div style="text-align: right;">
								<img id="main_content_imgTopic" style="width: 100px; height: 100px;" src="/Img/icon_responsive_pulse.png" />
							</div>
						</div>
						<!-- PULSE LOGO END -->

						<div class="grid__span grid__span--10 grid__span--last" style="background-color: transparent;">
							<!-- CARD HEADER (ROW HEADER) START -->
							<div class="grid__inner" style="background-color: transparent;">

								<div style="float: right; width: 50%;">
									<div class="grid__span--11" style="width: 100%;">
										<button id="lbApplyChanges" type="button" class="survey-bar__search__button">Apply Changes</button>
										<a class="survey-bar__search__button" href="/DynamicPulse/ResponsivePulseFeedList">Cancel</a>
										<a class="survey-bar__search__button analytics" style="margin: 0px 10px 0px 0px;">Analytics</a>
									</div>
								</div>

								<div class="tabs tabs--styled">
									<ul class="tabs__list" style="text-align: left;">
										<li class="tabs__list__item">
											<a class="tabs__link" href="#pulse-deck-info">Deck  Info</a>
										</li>
										<li class="tabs__list__item">
											<a class="tabs__link" href="#pulse-deck-privacy">Privacy</a>
										</li>
									</ul>
									<div class="tabs__panels">
										<!-- PULSE DECK INFO (CONTENT) START -->
										<div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-info">
											<div class="grid__inner" style="background-color: transparent;">
												<div class="grid__span--8" style="width: 50%; text-align: right; margin: 0px;">
													<div>
														<div class="form__row" style="margin: 0px;">
															<div id="div_deckTitle" style="border-width: 0px 0px 1px; border-style: solid; border-color: rgb(221, 221, 221); border-image: none; width: 100%; vertical-align: middle; display: inline-block; position: relative;">
																<input id="deckTitle" class="text-placeholder unfixed" style="border: currentColor; border-image: none; width: 97%; margin: 0px; background: rgba(244, 244, 244, 1); padding: 0.4em; font-size: 1.3em; font-weight: bold;" onkeyup="textCounter(this, 'deckTitleCounter', DECK_TITLE_COUNT_MAX);" type="text" placeholder="Title of deck" />
																<span id="deckTitleCounter" class="unfixed" style="right: 5px; bottom: 10px; color: #DDD; position: absolute;">50</span>
																<label id="lbDeckTitle" class="text-placeholder fixed" style="border: currentColor; border-image: none; width: 97%; margin: 0px; background: rgba(244, 244, 244, 1); padding: 0.4em; font-size: 1.3em; font-weight: bold; text-align: left;"></label>
															</div>
														</div>

														<div class="form__row" style="text-align: right;">
															<div class="grid">
																<div class="grid__inner" style="background-color: transparent;">
																	<div class="form__row" style="margin: 0px 0px 0.5em; display: none;">
																		<i class="fa fa-info-circle tooltip-icon" style="color: rgb(230, 230, 230); font-size: 1.4em; margin-left: 5px;"></i>
																		<div class="anonymous_tip" style="display: none; top: 20px; text-align: left;">
																			<label style="font-weight: 700;">Compulsory</label>
																			<span>Card must be answered and acknowledged to submit. If not, it will be shifted to the back of the pile of Pulse cards.</span>
																		</div>
																		<label style="font-weight: 700; display: inline-block;"><span style="color: rgba(152, 152, 152, 1);">Compulsory: </span></label>
																		<div id="dvCompulsory" class="mdl-selectfield unfixed" style="display: inline-block; width: 150px; margin-left: 10px;">
																			<select id="select_compulsory" style="display: inline-block; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold;">
																				<option value="false" selected="selected">No, default</option>
																				<option value="true">Yes</option>
																			</select>
																		</div>
																		<label id="lbCompulsory" class="fixed" style="display: inline-block; width: 150px; padding-bottom: 4px; margin: 10px 0 -5px 10px; font-weight: bold; text-align: left; padding-left: 11px;"></label>
																	</div>

																	<div class="form__row" style="margin: 1em 0px 0.5em 0px;">
																		<label style="font-weight: 700; display: inline-block;"><span style="color: rgba(152, 152, 152, 1);">Status: </span></label>
																		<div id="dvStatus" class="mdl-selectfield unfixed" style="display: inline-block; width: 150px; margin-left: 10px;">
																			<select id="select_status" style="display: inline-block; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold;">
																				<option value="1" selected="selected">Unlisted</option>
																				<option value="2">Active</option>
																				<option value="3">Hidden</option>
																			</select>
																		</div>
																		<label id="lbStatus" class="fixed" style="display: inline-block; width: 150px; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold; text-align: left; margin-left: 10px; padding-left: 11px;"></label>
																	</div>

																	<div class="form__row" style="margin: 0px 0px 0.5em;">
																		<label style="font-weight: 700; display: inline-block;"><i class="fa fa-star" style="color: #FFC700"></i><span style="color: rgba(152, 152, 152, 1);">Priority Overrule: </span></label>
																		<div id="dvPriority" class="mdl-selectfield unfixed" style="display: inline-block; width: 150px; margin-left: 10px;">
																			<select id="select_priority" style="display: inline-block; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold;">
																				<option value="false" selected="selected">No</option>
																				<option value="true">Yes</option>
																			</select>
																		</div>
																		<label id="lbPriority" class="fixed" style="display: inline-block; width: 150px; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold; text-align: left; margin-left: 10px; padding-left: 11px;"></label>
																	</div>

																	<div class="form__row" style="margin: 0px 0px 0.5em;">
																		<i class="fa fa-info-circle tooltip-icon" style="color: rgb(230, 230, 230); font-size: 1.4em; margin-left: 5px;"></i>
																		<div class="anonymous_tip" style="display: none; top: 20px; text-align: left;">
																			<img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />
																			<label style="font-weight: 700;">Anonymity</label>
																			<span>Participant's detail will not be disclosed in the Final Report.</span>
																		</div>
																		<label style="font-weight: 700; display: inline-block;"><span style="color: rgba(152, 152, 152, 1);">Anonymous Pulse: </span></label>
																		<div id="dvAnonymous" class="mdl-selectfield unfixed" style="display: inline-block; width: 150px; margin-left: 10px;">

																			<select id="anonymity_count" style="display: inline-block; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold;">
																				<option value="0" selected="selected">OFF</option>
																				<option value="1">Anonymous 1</option>
																				<option value="3">Anonymous 3</option>
																				<option value="5">Anonymous 5</option>
																				<option value="7">Anonymous 7</option>
																				<option value="9">Anonymous 9</option>
																				<option value="11">Anonymous 11</option>
																			</select>


																			<select id="select_anonymous" style="display: inline-block; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold; display: none;">
																				<option value="false" selected="selected">No</option>
																				<option value="true">Yes</option>
																			</select>
																		</div>
																		<label id="lbAnonymous" class="fixed" style="display: inline-block; width: 150px; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold; text-align: left; margin-left: 10px; padding-left: 11px;"></label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="grid__span--4" style="width: 50%; text-align: right;">
													<div>
														<div style="margin: 0px 0px 0.5em;">
															<label style="font-weight: 700; display: inline-block;"><span style="color: rgba(152, 152, 152, 1);">Publish method: </span></label>
															<div style="display: inline-block; width: 350px; margin-left: 10px; text-align: left;">
																<span id="spPublishMethod" style="font-weight: bold; margin-left: 10px;"></span>
															</div>
														</div>

														<div style="margin: 0px 0px 0.5em;">
															<label style="font-weight: 700; display: inline-block;"><span style="color: rgba(152, 152, 152, 1);">Start date: </span></label>
															<div id="dvDeckStartDate" class="mdl-selectfield unfixed" style="display: inline-block; width: 350px; margin-left: 10px; font-weight: bold; text-align: left;">
																<input type="text" id="deckStartDate" style="width: 102px; display: inline; margin-bottom: 0px; padding: 0.5em;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" class="input-field" />
																<label style="width: 25px; display: inline;">at</label>
																<input type="text" id="deckStartHH" maxlength="2" style="width: 30px; display: inline; margin-bottom: 0px; padding: 0.5em;" placeholder="hh" onchange="validateHour(this);" class="input-field" />
																<label style="width: 25px; display: inline;">:</label>
																<input type="text" id="deckStartMM" maxlength="2" style="width: 30px; display: inline; margin-bottom: 0px; padding: 0.5em;" placeholder="mm" onchange="validateMinute(this);" class="input-field" />
																<select id="deckStartMer" style="width: 150px; display: inline; border: none; border-bottom: 1px solid #ddd; background: rgba(243, 243, 243, 1); padding: 0.5em;">
																	<option value="am">am</option>
																	<option value="pm">pm</option>
																</select>
															</div>
															<div id="dvDeckStartDateTime" class="fixed" style="display: inline-block; width: 350px; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold; text-align: left; margin-left: 10px;"></div>
														</div>

														<div style="margin: 0px 0px 0.5em; display: none;" id="dvEndDate">
															<label style="font-weight: 700; display: inline-block;"><span style="color: rgba(152, 152, 152, 1);">End date: </span></label>
															<div id="dvDeckEndDate" class="mdl-selectfield unfixed" style="display: inline-block; width: 350px; margin-left: 10px; font-weight: bold; text-align: left;">
																<input type="text" id="deckEndDate" style="width: 102px; display: inline; margin-bottom: 0px; padding: 0.5em;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" class="input-field" />
																<label style="width: 25px; display: inline;">at</label>
																<input type="text" id="deckEndHH" maxlength="2" style="width: 30px; display: inline; margin-bottom: 0px; padding: 0.5em;" placeholder="hh" onchange="validateHour(this);" class="input-field" />
																<label style="width: 25px; display: inline;">:</label>
																<input type="text" id="deckEndMM" maxlength="2" style="width: 30px; display: inline; margin-bottom: 0px; padding: 0.5em;" placeholder="mm" onchange="validateMinute(this);" class="input-field" />
																<select id="deckEndMer" style="width: 150px; display: inline; border: none; border-bottom: 1px solid #ddd; background: rgba(243, 243, 243, 1); padding: 0.5em;">
																	<option value="am">am</option>
																	<option value="pm">pm</option>
																</select>
															</div>
															<div id="dvDeckEndDateTime" class="fixed" style="display: inline-block; width: 350px; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold; text-align: left; margin-left: 10px;"></div>
														</div>

														<div class="form__row" style="margin: 0px 0px 0.5em; display: none;" id="dvDuration">
															<label style="font-weight: 700; display: inline-block;"><span style="color: rgba(152, 152, 152, 1);">Duration: </span></label>
															<div id="dvRoutineDuration" class="mdl-selectfield unfixed" style="display: inline-block; width: 350px; margin-left: 10px;">
																<select id="routine_duration" style="display: inline-block; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold;">
																	<option value="-1" selected="selected">Infinite</option>
																	<option value="1">1 day</option>
																	<option value="2">2 days</option>
																	<option value="3">3 days</option>
																	<option value="4">4 days</option>
																	<option value="5">5 days</option>
																	<option value="6">6 days</option>
																	<option value="7">7 days</option>
																	<option value="8">8 days</option>
																	<option value="9">9 days</option>
																	<option value="10">10 days</option>
																	<option value="11">11 days</option>
																	<option value="12">12 days</option>
																	<option value="13">13 days</option>
																	<option value="14">14 days</option>
																	<option value="15">15 days</option>
																	<option value="16">16 days</option>
																	<option value="17">17 days</option>
																	<option value="18">18 days</option>
																	<option value="19">19 days</option>
																	<option value="20">20 days</option>
																	<option value="21">21 days</option>
																	<option value="22">22 days</option>
																	<option value="23">23 days</option>
																	<option value="24">24 days</option>
																	<option value="25">25 days</option>
																	<option value="26">26 days</option>
																	<option value="27">27 days</option>
																	<option value="28">28 days</option>
																	<option value="29">29 days</option>
																	<option value="30">30 days</option>
																	<option value="60">2 months</option>
																	<option value="90">3 months</option>
																</select>
															</div>
															<label id="lbDuration" class="fixed" style="display: inline-block; width: 150px; padding-bottom: 4px; margin-bottom: 0px; font-weight: bold; text-align: left; margin-left: 10px; padding-left: 11px;"></label>
														</div>
														<button class="import-decks survey-bar__search__button" type="button" style="margin: 50px 0 0 0">Import Pulse</button>
													</div>
												</div>
											</div>

										</div>
										<!-- PULSE DECK INFO (CONTENT) END -->

										<!-- PULSE DECK PRIVACY (CONTENT) START -->
										<div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-privacy">
											<div id="tab-privacy">
												&nbsp;
										
												<p>
													<label for="checkbox_privacy">
														<input type="checkbox" id="checkbox_privacy" onchange="checkbox_selected_everyone();" checked="checked" />
														<i class="fa fa-users" style="color: #999999"></i>Everyone
											
													</label>
												</p>
												<hr />
												<p>
													<label for="checkbox_selected_departments">
														<input type="checkbox" id="checkbox_selected_departments" onchange="checkbox_selected_departments_change();" />
														<i class="fa fa-briefcase" style="color: #999999"></i>Selected departments
												
													</label>
													<div class="department tags"></div>
													(<span id="selectDepartmentLinkButton" style="color: #C1C1C1; cursor: default;">+ Add more department</span>)
											
												</p>

												<p>
													<label for="checkbox_selected_personnel">
														<input type="checkbox" id="checkbox_selected_personnel" onchange="checkbox_selected_personnel_change();" />
														<i class="fa fa-user" style="color: #999999"></i>Selected personnel
												
													</label>
													<div class="personnel tags"></div>

													<input type="text" id="text_selected_personnel" style="width: 300px; border: none; border-bottom: 1px solid #ccc; background: rgba(243, 243, 243, 1);" onkeydown="return (event.keyCode!=13);" />
												</p>

											</div>
										</div>
										<!-- PULSE DECK PRIVACY (CONTENT) END -->
									</div>
								</div>
							</div>
							<!-- CARD HEADER (ROW HEADER) END -->
						</div>

					</div>
				</div>

				<!-- SEARCH PULSE START -->
				<asp:Panel ID="plSearchQuestion" runat="server" Visible="true">
					<div id="sticky_anchor"></div>
					<div class="survey-bar sticky">
						<div class="container" style="padding: 10px 0px 10px 0px;">
							<div class="survey-bar__search">

								<div class="grid__inner" style="background-color: transparent;">
									<div style="width: 50%; display: inline-block; float: left; position: relative;">
										<i class="fa fa-search" style="position: absolute; left: 2px; top: 14px; color: #aeaeae;"></i>
										<asp:TextBox ID="tbSearch" runat="server" placeholder="Search question" CssClass="survey-bar__search__input input-field" onkeydown="searchPulses(this, event);" />
									</div>
									<div style="width: 50%; display: inline-block">
										<strong>Logic</strong>
										<img src="/Img/icon_logic_redirect.png" title="Source" />
										Redirect
										<img src="/Img/icon_logic_destination.png" title="Destination" />
										Destination &nbsp;
										<button type="button" class="add-pulse survey-bar__search__button" style="margin: 0px;">+ Add a pulse</button>
										<asp:LinkButton Style="visibility: hidden;" ID="lbSearch" runat="server" CssClass="survey-bar__search__button" OnClick="lbSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
									</div>
								</div>

							</div>
						</div>
					</div>
				</asp:Panel>
				<!-- SEARCH PULSE END -->
				<div class="card-add"></div>
				<div class="card-list"></div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="/Js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
	<script type="text/javascript">

		// General model representing a responsive pulse survey
		function DynamicPulse(deckId) {
			this.ActionTaken = 0;
			this.Actions = null;
			this.CardType = 0;
			this.DeckId = deckId;
			this.DeletedDate = "0001-01-01T00=00=00";
			this.Description = null;
			this.EndDate = null;
			this.EndDateString = null;
			this.HasIncomingLogic = false;
			this.HasOutgoingLogic = false;
			this.ImageUrl = null;
			this.IsAnonymous = false;
			this.IsComplusory = false;
			this.IsForDepartment = false;
			this.IsForEveryone = false;
			this.IsForUser = false;
			this.IsPrioritized = false;
			this.LogicLevel = 1;
			this.MaxRangeLabel = null;
			this.MinRangeLabel = null;
			this.NumberOfOptions = 0;
			this.Option1Content = null;
			this.Option1Id = null;
			this.Option1NextCardId = null;
			this.Option2Content = null;
			this.Option2Id = null;
			this.Option2NextCardId = null;
			this.Option3Content = null;
			this.Option3Id = null;
			this.Option3NextCardId = null;
			this.Option4Content = null;
			this.Option4Id = null;
			this.Option4NextCardId = null;
			this.Option5Content = null;
			this.Option5Id = null;
			this.Option5NextCardId = null;
			this.Option6Content = null;
			this.Option6Id = null;
			this.Option6NextCardId = null;
			this.Option7Content = null;
			this.Option7Id = null;
			this.Option7NextCardId = null;
			this.Option8Content = null;
			this.Option8Id = null;
			this.Option8NextCardId = null;
			this.Options = null;
			this.Ordering = null;
			this.ParentCard = null;
			this.ParentOption = null;
			this.ProgressStatus = 0;
			this.PulseId = null;
			this.PulseType = 0;
			this.QuestionType = 0;
			this.RangeType = 0;
			this.RespondentChart = null;
			this.SavedDate = "0001-01-01T00=00=00";
			this.SelectedOption = null;
			this.SelectedRange = 0;
			this.StartDate = moment().utc();
			this.StartDateString = null;
			this.Status = 0;

			this.TargetedDepartments = null;
			this.TargetedUsers = null;
			this.TextAnswer = null;
			this.TextAnswers = null;
			this.Title = null;
			this.TotalRespondents = 0;

			// Custom update
			this.Options = [];
			this.UseCustomImage = false;
			this.CustomImageDataUrl = "";

			this.PulseId = "undefined" + (new Date()).toISOString().replace(/-|T|:|\.|Z/g, "");
		}

		function PulseDeck() {
			this.deckId = $("#main_content_hfDeckId").val();
			this.adminUserId = $("#main_content_hfAdminUserId").val();
			this.companyId = $("#main_content_hfCompanyId").val();
			this.title = $("#deckTitle").val();
			this.deckType = "";
			this.isCompulsory = ($("#select_compulsory").val() === "true");
			this.isAnonymous = $("#select_anonymous").prop("checked");
			this.anonymityCount = $("#anonymity_count").val();
			this.publishMethodType = $('input[name=publish_method]:checked').val(); //$(".publish_method:checked").val();
			this.status = parseInt($("#select_status").val());
			this.startDate = $("#deckStartDate").val() + " " + $("#deckStartHH").val() + ":" + $("#deckStartMM").val() + " " + $("#deckStartMer").val();
			this.endDate = $("#deckEndDate").val() + " " + $("#deckEndHH").val() + ":" + $("#deckEndMM").val() + " " + $("#deckEndMer").val();
			this.isPrioritized = false;
			this.targetedDepartmentIds = [];
			this.targetedUserIds = [];
			this.numberOfCardsPerTimeFrame = 0;
			this.perTimeFrameType = 0;
			this.privacy = new Privacy();
			this.periodRule = 0;
			this.Pulses = [];
			this.action = 1;
			this.duration = -1;
			this.importDeckIds = [];
			this.editMode = -1;
		}

		function DeckCardOption() {
			this.OptionId = null;
			this.Content = null;
			this.NextPulse = null;
			this.NumberOfSelection = 0;
			this.PercentageOfSelection = 0;
		}

		function Privacy() {
			this.everyone = true;
			this.selectd_department = false;
			this.selected_personnel = false;
		}

		const EDIT_MODE_UPCOMING_OR_UNLISTED = 1;
		const EDIT_MODE_LIVE = 2;
		const EDIT_MODE_COMPLETED = 3;

		var model = {
			deck: new PulseDeck()
		};

		var DECK_TITLE_COUNT_MAX = 50;
		var startDateString, endDateString;
		var departmentList;
		var deckList;
		var timezone;
		var lastNonCustomizedCard;

		////////////////////////////////////////////////////////////////////////////////

		// Privacy related javascript functions -- START

		function checkbox_selected_everyone() {
			// Update UI
			$("#checkbox_selected_departments").prop("checked", false);
			$("#checkbox_selected_personnel").prop("checked", false);
		}

		function checkbox_selected_departments_change() {
			if ($("#checkbox_selected_departments").is(":checked")) // Departments is included in Privacy.
			{
				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selectd_department = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
				$("#selectDepartmentLinkButton").css("color", "blue");
				$("#selectDepartmentLinkButton").css("cursor", "pointer");
				$("#selectDepartmentLinkButton").click(displayDepartmentSelector);
			}
			else // Departments is not included in Privacy.
			{
				// Update Model
				model.deck.privacy.selectd_department = false;

				// Update UI
				$("#selectDepartmentLinkButton").css("color", "#C1C1C1");
				$("#selectDepartmentLinkButton").css("cursor", "default");
				$("#selectDepartmentLinkButton").prop('onclick', null).off('click');
			}
		}

		function displayDepartmentSelector() {
			var companyId, adminUserId, ajaxPromise;

			companyId = $("#main_content_hfCompanyId").val();
			adminUserId = $("#main_content_hfAdminUserId").val();

			// Ajax call to fetch 
			ajaxPromise = jQuery.ajax({
				type: "GET",
				url: "/api/Company?userid=" + adminUserId + "&companyid=" + companyId,
				contentType: "application/json; charset=utf-8",
				data: {},
				dataType: "json",
				beforeSend: function (xhr, settings) {
					ShowProgressBar();
				},
				success: function (d, status, xhr) {
					if (d.Success) {
						departmentList = d.Departments;
					}
				},
				error: function (xhr, status, error) {
				},
				complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
					// Render department

					var idx, departmentObj, html, checked, isAllChecked = true;
					$("#department_checkboxlist").children().remove();
					for (idx = 0; idx < departmentList.length; idx++) {
						checked = "";
						if (model.deck.targetedDepartmentIds.indexOf(departmentList[idx].Id) >= 0) {
							checked = "checked=\"checked\"";
						}
						else {
							isAllChecked = false;
						}

						html = "<label><input class=\"cbDepartment " + departmentList[idx].Id + "\" type=\"checkbox\" " + checked + " value=\"" + departmentList[idx].Id + "\" onclick=\"checkbox_selectd_department_click('" + departmentList[idx].Id + "');\" />" + departmentList[idx].Title + "</label>";

						$("#department_checkboxlist").append(html);

						if (isAllChecked) {
							$("#selectAllDepartment").attr("checked", true);
						}
						else {
							$("#selectAllDepartment").removeAttr("checked");;
						}
					}

					HideProgressBar();
					$("#mpe_backgroundElement").show();
					$("#plSelectDepartment").show();
				}
			});
		}

		function checkbox_selectd_department_click(departmentId) {
			if ($(".cbDepartment." + departmentId).is(':checked')) {
				var cbDepartments = $("#department_checkboxlist").children();
				var isAllDepartmentChecked = true;
				for (var i = 0; i < cbDepartments.length; i++) {
					console.debug($(".cbDepartment." + $($(cbDepartments[i]).children()[0]).val()).is(':checked'));
					if (!$(".cbDepartment." + $($(cbDepartments[i]).children()[0]).val()).is(':checked')) {
						isAllDepartmentChecked = false;
						break;
					}
				}
				$("#selectAllDepartment").prop("checked", isAllDepartmentChecked);;
			}
			else {
				$("#selectAllDepartment").prop("checked", false);;
			}
		}

		function checkbox_selected_personnel_change() {
			if ($("#checkbox_selected_personnel").is(":checked")) // Personnels is included in Privacy.
			{
				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selected_personnel = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
			}
			else  // Personnels is not included in Privacy.
			{
				// Update Model
				model.deck.privacy.selected_personnel = false;
			}
		}

		function hideDepartmentSelector() {
			$("#mpe_backgroundElement").hide();
			$("#plSelectDepartment").hide();
		}

		function saveDepartmentSelection() {
			var idx, $checkedItems = [], html;

			// Clear targetedDepartmentIds 
			model.deck.targetedDepartmentIds = [];
			$(".department.tags").children().remove();

			// Re-populate targetedDepartmentIds with selected department id
			$checkedItems = $("#plSelectDepartment .departments :checked");
			for (idx = 0; idx < $checkedItems.length; idx++) {
				model.deck.targetedDepartmentIds.push($checkedItems[idx].value);
				html = "";
				html = html + "<div class=\"tag " + $checkedItems[idx].value + "\">";
				html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + $($checkedItems[idx]).parent().text() + "</span>";
				html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + $checkedItems[idx].value + "');\">x</a>";
				html = html + "</div>";
				$(".department.tags").append(html);
			}

			hideDepartmentSelector();

			$("#checkbox_selected_departments").prop("checked", model.deck.targetedDepartmentIds.length > 0);
			$("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
		}

		function hideDeckSelector() {
			$("#mpe_backgroundElement").hide();
			$("#plSelectDecks").hide();
		}

		function hideDelPulse() {
			$("#mpe_backgroundElement").hide();
			$("#plDelPulse").hide();
		}


		function hideEditDateTime(pulseId) {
			var pulse = getPulse(pulseId);
			var dateTime = moment.utc(pulse.StartDate).add(timezone, 'hours');
			$(".card-holder." + pulseId + " .cardStartDate").val(dateTime.format("DD MMM YYYY"));
			$(".card-holder." + pulseId + " .mini-card-day").html(dateTime.format("dddd"));
			$(".card-holder." + pulseId + " .mini-card-hour").html(dateTime.format("hh"));
			$(".card-holder." + pulseId + " .mini-card-min").html(dateTime.format("mm"));
			$(".card-holder." + pulseId + " .mini-card-period").html(dateTime.format("A"));

			$("#mpe_backgroundElement").hide();
			$("#plEditPulseDateTime").hide();
		}

		function saveDeckSelection() {
			var idx, $checkedItems = [], html;
			var listDeckIds = new Array();

			$checkedItems = $("#plSelectDecks :checked");
			for (idx = 0; idx < $checkedItems.length; idx++) {
				listDeckIds.push($checkedItems[idx].value);
			}

			model.deck.action = 3;
			model.deck.deckId = $("#main_content_hfDeckId").val();
			model.deck.companyId = $("#main_content_hfCompanyId").val();
			model.deck.adminUserId = $("#main_content_hfAdminUserId").val();
			model.deck.importDeckIds = listDeckIds;

			jQuery.ajax({
				type: "POST",
				url: "/api/PulseResponsiveDeck",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(model.deck),
				dataType: "json",
				beforeSend: function (xhr, settings) {
					ShowProgressBar();
				},
				success: function (d, status, xhr) {
					if (d.Success) {
						hideDeckSelector();
						ShowToast('Decks has been imported', 1);
						RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
					} else {
						ShowToast(d.ErrorMessage, 2);
					}
				},
				error: function (xhr, status, error) {
					ShowToast(error, 2);
				},
				complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
					HideProgressBar();
				}
			});

			hideDepartmentSelector();
		}

		function removeDepartment(departmentId) {
			var foundIndex = model.deck.targetedDepartmentIds.indexOf(departmentId);

			if (foundIndex >= 0) {
				model.deck.targetedDepartmentIds.splice(foundIndex, 1);

				// Update UI
				$(".department.tags .tag." + departmentId).remove();
			}

			$("#checkbox_selected_departments").prop("checked", model.deck.targetedDepartmentIds.length > 0);
			$("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
		}

		function removePersonnel(personnelId) {
			var foundIndex = model.deck.targetedUserIds.indexOf(personnelId);

			if (foundIndex >= 0) {
				model.deck.targetedUserIds.splice(foundIndex, 1);

				// Update UI
				$(".personnel.tags .tag." + personnelId).remove();
			}

			$("#checkbox_selected_personnel").prop("checked", model.deck.targetedUserIds.length > 0);
		}

		function setPriorityOverrule(input) {
			model.deck.isPrioritized = input.checked;
		}

		// Privacy related javascript functions -- END

		////////////////////////////////////////////////////////////////////////////////

		// Validate publish date time -- START

		function validateDate(input) {
			input.className = input.className.replace("invalid-field", "");
			if (!moment(input.value, "DD/MM/YYYY", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function validateHour(input) {
			input.className = input.className.replace("invalid-field", "");

			if (!moment(input.value, "h", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function validateMinute(input) {
			input.className = input.className.replace("invalid-field", "");

			if (!moment(input.value, "m", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function getAddCardHtml(cardHtml, pulse) {
			mStartDate = moment(pulse.StartDate).add(timezone, 'hours')
			startDateString = mStartDate.format("DD MMM YYYY") + " " + mStartDate.format("dddd") + ",";
			startTimeHH = mStartDate.format("HH");
			startTimeMM = mStartDate.format("mm");

			holderHtml = "";
			holderHtml = holderHtml + "<div class=\"card-holder row-" + ($(".card-list .card-holder").length + 1).toString() + "\">";
			holderHtml = holderHtml + "    <div class=\"grid__span--2 card-date\" style=\"margin-right:1%; width:14%;\">";
			holderHtml = holderHtml + "        <span style=\"display: inline;\" class=\"card-publish-date\">";
			holderHtml = holderHtml + "            <input type=\"text\" class=\"input-field cardStartDate\" style=\"display: inline;\" placeholder=\"dd/mm/yyyy\" onchange=\"validateDate(this);\" value=\"" + startDateString + "\" /><br />";
			holderHtml = holderHtml + "            <input class=\"cardStartHH\" style=\"width: 2em; display: inline;\" onchange=\"validateHour(this);\" type=\"text\" maxlength=\"2\" placeholder=\"hh\" value=\"" + startTimeHH + "\" />";
			holderHtml = holderHtml + "            <span>:</span>";
			holderHtml = holderHtml + "            <input type=\"text\" class=\"cardStartMM\" maxlength=\"2\" style=\"width: 2em; display: inline;\" placeholder=\"mm\" onchange=\"validateMinute(this);\" value=\"" + startTimeMM + "\" />";
			holderHtml = holderHtml + "            <select class=\"cardStartMer\" style=\"width: 4em; display: inline;\">";
			holderHtml = holderHtml + "                <option value=\"am\">am</option>";
			holderHtml = holderHtml + "                <option value=\"pm\">pm</option>";
			holderHtml = holderHtml + "            </select>";
			holderHtml = holderHtml + "        </span>";
			holderHtml = holderHtml + "    </div>";
			holderHtml = holderHtml + "    <div class=\"grid__span--10 grid__span--last mini-card-holder\" style=\"width:85%;\">";
			holderHtml = holderHtml + cardHtml;
			holderHtml = holderHtml + "    </div>";
			holderHtml = holderHtml + "</div>";

			return holderHtml;
		}

		function saveToModelDeck() {
			// Save UI component data to model
			// Title
			model.deck.title = $("#deckTitle").val();
			// Compulsory
			model.deck.isCompulsory = ($("#select_compulsory").val() === "true");
			// Anonymous 
			model.deck.isAnonymous = ($("#select_anonymous").val() === "true");
			model.deck.anonymityCount = parseInt($("#anonymity_count").val());

			// Status
			model.deck.status = parseInt($("#select_status").val());
			// Priority Overrule
			model.deck.isPrioritized = ($("#select_priority").val() === "true");

			// Start Date
			var timezone = parseFloat($("#main_content_hfTimezone").val());
			model.deck.startDate = $("#deckStartDate").val() + " " + $("#deckStartHH").val() + ":" + $("#deckStartMM").val() + " " + $("#deckStartMer").val();
			if (moment(model.deck.startDate, "DD/MM/YYYY h:m a", true).isValid()) {
				model.deck.startDate = moment(model.deck.startDate, "DD/MM/YYYY HH:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
			} else {
				model.deck.startDate = null;
			}

			// Publish method
			if (model.deck.publishMethodType == 1)  // Publish method is "Schedute".
			{
				// End Date
				model.deck.endDate = $("#deckEndDate").val() + " " + $("#deckEndHH").val() + ":" + $("#deckEndMM").val() + " " + $("#deckEndMer").val();
				if (moment(model.deck.endDate, "DD/MM/YYYY h:m a", true).isValid()) {
					model.deck.endDate = moment(model.deck.endDate, "DD/MM/YYYY HH:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
				}
				else {
					model.deck.endDate = null;
				}
			}
			else // Publish method is "Routine".
			{
				//// numberOfCardsPerTimeFrame
				//model.deck.numberOfCardsPerTimeFrame = $("#routine_card").val();
				//// perTimeFrameType
				//model.deck.perTimeFrameType = $("#routine_period").val();
				//// Duration
				//model.deck.duration = $("#routine_duration").val();

				//// periodRule
				//if (model.deck.perTimeFrameType == 1) // Per day
				//{
				//	model.deck.periodRule = 0;
				//}
				//else if (model.deck.perTimeFrameType == 2) // Per week
				//{
				//	model.deck.periodRule = $("#routine_interval_for_week").val();
				//}
				//else if (model.deck.perTimeFrameType == 3) // Per month
				//{
				//	model.deck.periodRule = $("#routine_interval_for_moonth").val();
				//}
				//else {
				//	// do nothing
				//}
			}

			// Privacy
			if ($("#checkbox_privacy").is(":checked")) // Everyone for Privacy
			{
				model.deck.privacy.everyone = true;
				model.deck.privacy.selectd_department = false;
				model.deck.targetedDepartmentIds = [];
				model.deck.privacy.selected_personnel = false;
				model.deck.targetedUserIds = [];
			}
			else {
				model.deck.privacy.everyone = false;
				model.deck.privacy.selectd_department = $("#checkbox_selected_departments").is(":checked");
				model.deck.privacy.selected_personnel = $("#checkbox_selected_personnel").is(":checked");
			}
		}

		function loadDeck() {
			var pj;

			if ($("#main_content_hfDeck").val().trim().length <= 0) {
				$("#lbApplyChanges").text("Create");
				$('#select_status option[value=2]').remove();
				$('#select_status option[value=3]').remove();
				return;
			}

			pj = JSON.parse($("#main_content_hfDeck").val().trim());
			if (!pj) {
				return;
			}

			if (pj.Status == 1) // unlisted
			{
				model.deck.editMode = EDIT_MODE_UPCOMING_OR_UNLISTED;
			}
			else {
				var nowTimeUTC = moment.utc();

				if (moment.utc(new Date(parseInt(pj.DeckStartTimestamp.replace("/Date(", "").replace(")/", ""), 10))).isAfter(nowTimeUTC)) // upcoming
				{
					model.deck.editMode = EDIT_MODE_UPCOMING_OR_UNLISTED;
				}
				else {
					if (pj.DeckEndTimestamp == null || moment.utc(new Date(parseInt(pj.DeckEndTimestamp.replace("/Date(", "").replace(")/", ""), 10))).isAfter(nowTimeUTC)) // live
					{
						model.deck.editMode = EDIT_MODE_LIVE;
					}
					else // completed
					{
						model.deck.editMode = EDIT_MODE_COMPLETED;
					}
				}
			}

			// Set title
			$("#deckTitle").val(pj.Title);
			$("#deckTitleCounter").html(DECK_TITLE_COUNT_MAX - pj.Title.length);
			$("#lbDeckTitle").html(pj.Title);

			// Set compulsory
			$("#select_compulsory").val(pj.IsCompulsory.toString());
			if (pj.IsCompulsory) {
				$("#lbCompulsory").html("Yes");
			}
			else {
				$("#lbCompulsory").html("No, Default");
			}

			// Set status
			if (pj.Status == 1) // unlisted
			{
				$('#select_status option[value=3]').remove();
				$('#lbStatus').html("Unlisted");

			}
			else if (pj.Status == 2) // active
			{
				$('#select_status option[value=1]').remove();
				$('#lbStatus').html("Active");
			}
			else if (pj.Status == 3) // hidden
			{
				$('#select_status option[value=1]').remove();
				$('#lbStatus').html("Hidden");
			}
			else {
				// do noting
			}
			$("#select_status").val(pj.Status);

			// Set priority overrule
			$("#select_priority").val(pj.IsPrioritized.toString());
			if (pj.IsPrioritized) {
				$("#lbPriority").html("Yes");
			}
			else {
				$("#lbPriority").html("No");
			}

			// Set anonymous pulse
			//$("#select_anonymous").val(pj.IsDeckAnonymous.toString());
			//if (pj.IsDeckAnonymous) {
			//	$("#lbAnonymous").html("Yes");
			//}
			//else {
			//	$("#lbAnonymous").html("No");
			//}

			$("#anonymity_count").val(pj.AnonymityCount.toString());
			if (pj.AnonymityCount == 0) {
				$("#lbAnonymous").html("OFF");
			}
			else {
				$("#lbAnonymous").html("Anonymity " + pj.AnonymityCount);
			}

			// Set publish method
			var publishMethod = "";

			model.deck.publishMethodType = pj.PublishMethodType;
			if (pj.PublishMethodType === 1) // Publish method is "Schedule"
			{
				publishMethod += "Schedule";

				$("#dvEndDate").show(); // Display end date controls

				// Set end date
				var timezone = parseFloat($("#main_content_hfTimezone").val());
				if (pj.DeckEndTimestamp != null) {
					pj.DeckEndTimestamp = moment.utc(new Date(parseInt(pj.DeckEndTimestamp.replace("/Date(", "").replace(")/", ""), 10))).add(timezone, 'hours');
					$("#deckEndDate").val(pj.DeckEndTimestamp.format("DD/MM/YYYY"));
					$("#deckEndHH").val(pj.DeckEndTimestamp.format("hh"));
					$("#deckEndMM").val(pj.DeckEndTimestamp.format("mm"));
					$("#deckEndMer").val(pj.DeckEndTimestamp.format("a"));

					var html = "";
					html += "<label style=\"width:102px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
					html += pj.DeckEndTimestamp.format("DD/MM/YYYY");
					html += "</label>";
					html += "<label style=\"width:25px; display: inline;\">at";
					html += "</label>";
					html += "<label style=\"width:30px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
					html += pj.DeckEndTimestamp.format("hh");
					html += "</label>";
					html += "</label>";
					html += "<label style=\"width:25px; display: inline;\">:";
					html += "</label>";
					html += "<label style=\"width:30px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
					html += pj.DeckEndTimestamp.format("mm");
					html += "</label>";
					html += "<label style=\"width:150px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
					html += pj.DeckEndTimestamp.format("a");
					html += "</label>";
					$("#dvDeckEndDateTime").html(html);
				}
			}
			else if (pj.PublishMethodType === 2) {
				publishMethod += "Routine,";

				$("#dvDuration").show(); // Display routine controls

				// numberOfCardsPerTimeFrame
				model.deck.numberOfCardsPerTimeFrame = pj.NumberOfCardsPerTimeFrame;
				if (pj.NumberOfCardsPerTimeFrame == 1) {
					publishMethod += " 1 pulse";
				}
				else {
					publishMethod += " " + pj.NumberOfCardsPerTimeFrame + "  pulses";
				}

				// PerTimeFrameType and PeriodFrameType
				$("#routine_interval_for_week").css("display", "none");
				$("#routine_interval_for_moonth").css("display", "none");

				model.deck.perTimeFrameType = pj.PerTimeFrameType;
				model.deck.periodRule = -1;
				switch (pj.PerTimeFrameType) {
					case 1: // per day
						publishMethod += " per day";
						break;
					case 2: // per week
						//publishMethod += " per week every ";
						publishMethod += " per week";

						switch (pj.PeriodFrameType) {
							case 1:
								model.deck.periodRule = 1;
								//publishMethod += "Monday";
								break;
							case 2:
								model.deck.periodRule = 2;
								//publishMethod += "Tuesday";
								break;
							case 3:
								model.deck.periodRule = 3;
								//publishMethod += "Wednesday";
								break;
							case 4:
								model.deck.periodRule = 4;
								//publishMethod += "Thursday";
								break;
							case 5:
								model.deck.periodRule = 5;
								//publishMethod += "Friday";
								break;
							case 6:
								model.deck.periodRule = 6;
								//publishMethod += "Saturday";
								break;
							case 7:
								model.deck.periodRule = 7;
								//publishMethod += "Sunday";
								break;
							default:
								break;
						}

						break;
					case 3: // per month
						publishMethod += "/mth on the ";

						switch (pj.PeriodFrameType) {
							case 1:
								model.deck.periodRule = 1;
								publishMethod += "first work day of the month";
								break;
							case 2:
								model.deck.periodRule = 2;
								publishMethod += "laast work day of the month";
								break;
							case 3:
								model.deck.periodRule = 3;
								publishMethod += "particular date of the month";
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}

				// Duration
				$("#routine_duration").val(pj.Duration);

				if (pj.Duration < 0) {
					$("#lbDuration").html("Infinite");
				}
				else if (pj.Duration == 1) {
					$("#lbDuration").html("1 day");
				}
				else if (pj.Duration > 1 && pj.Duration < 60) {
					$("#lbDuration").html(pj.Duration + " days");
				}
				else if (pj.Duration == 60) {
					$("#lbDuration").html("2 months");
				}
				else if (pj.Duration == 60) {
					$("#lbDuration").html("3 months");
				}
				else {
					// do nothing
				}
			}
			else {
				// do nothing
			}
			$("#spPublishMethod").html(publishMethod);

			// Set start date
			var timezone = parseFloat($("#main_content_hfTimezone").val());
			if (pj.DeckStartTimestamp != null) {
				pj.DeckStartTimestamp = moment.utc(new Date(parseInt(pj.DeckStartTimestamp.replace("/Date(", "").replace(")/", ""), 10))).add(timezone, 'hours');
				$("#deckStartDate").val(pj.DeckStartTimestamp.format("DD/MM/YYYY"));
				$("#deckStartHH").val(pj.DeckStartTimestamp.format("hh"));
				$("#deckStartMM").val(pj.DeckStartTimestamp.format("mm"));
				$("#deckStartMer").val(pj.DeckStartTimestamp.format("a"));

				var html = "";
				html += "<label style=\"width:102px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
				html += pj.DeckStartTimestamp.format("DD/MM/YYYY");
				html += "</label>";
				html += "<label style=\"width:25px; display: inline;\">at";
				html += "</label>";
				html += "<label style=\"width:30px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
				html += pj.DeckStartTimestamp.format("hh");
				html += "</label>";
				html += "</label>";
				html += "<label style=\"width:25px; display: inline;\">:";
				html += "</label>";
				html += "<label style=\"width:30px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
				html += pj.DeckStartTimestamp.format("mm");
				html += "</label>";
				html += "<label style=\"width:150px; display: inline-block; margin-bottom: 0px; padding: 0.5em; text-align:left \">";
				html += pj.DeckStartTimestamp.format("a");
				html += "</label>";
				$("#dvDeckStartDateTime").html(html);

			}

			// Set privacy
			if (pj.TargetedDepartments.length > 0) // Selected departments 
			{
				for (idx = 0; idx < pj.TargetedDepartments.length; idx++) {
					record = pj.TargetedDepartments[idx];
					model.deck.targetedDepartmentIds.push(record.Id);
					html = "";
					html = html + "<div class=\"tag " + record.Id + "\">";
					html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + record.Title + "</span>";
					html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + record.Id + "');\">x</a>";
					html = html + "</div>";
					$(".department.tags").append(html);
				}

				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selectd_department = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
				$("#selectDepartmentLinkButton").css("color", "blue");
				$("#selectDepartmentLinkButton").css("cursor", "pointer");
				$("#selectDepartmentLinkButton").click(displayDepartmentSelector);
			}
			$("#checkbox_selected_departments").prop("checked", model.deck.targetedDepartmentIds.length > 0);

			if (pj.TargetedUsers.length > 0)  // Selected personnel 
			{
				for (idx = 0; idx < pj.TargetedUsers.length; idx++) {
					record = pj.TargetedUsers[idx];
					model.deck.targetedUserIds.push(record.UserId);
					html = "";
					html = html + "<div class=\"tag " + record.UserId + "\">";
					html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + record.FirstName + " " + record.LastName + "(" + record.Email + ")" + "</span>";
					html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + record.UserId + "');\">x</a>";
					html = html + "</div>";
					$(".personnel.tags").append(html);
				}

				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selected_personnel = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
			}
			$("#checkbox_selected_personnel").prop("checked", model.deck.targetedUserIds.length > 0);
			$("#checkbox_privacy").prop("checked", (!($("#checkbox_selected_personnel").prop("checked") || $("#checkbox_selected_departments").prop("checked"))));

			/*******************************/
			// turn on/off controller
			switch (model.deck.editMode) {
				case EDIT_MODE_UPCOMING_OR_UNLISTED:
					// can edit everything
					$(".fixed").css("display", "none");
					$(".unfixed").css("display", "inline-block");
					break;
				case EDIT_MODE_LIVE:
					// title, status, end-date, duration					
					$(".fixed").css("display", "inline-block");
					$(".unfixed").css("display", "none");

					// title
					$("#deckTitle").css("display", "");
					$("#deckTitleCounter").css("display", "");
					$("#lbDeckTitle").css("display", "none");
					// status
					$("#dvStatus").css("display", "inline-block");
					$("#lbStatus").css("display", "none");

					// end-date
					if (pj.PublishMethodType === 1) // Publish method is "Schedule"
					{
						$("#dvDeckEndDate").css("display", "inline-block");
						$("#dvDeckEndDateTime").css("display", "none");
					}

					// duration
					$("#dvRoutineDuration").css("display", "inline-block");
					$("#lbDuration").css("display", "none");

					// privacy
					$("#checkbox_privacy").prop("disabled", true);
					$("#checkbox_selected_departments").prop("disabled", true);
					$("#selectDepartmentLinkButton").css("color", "#C1C1C1");
					$("#selectDepartmentLinkButton").css("cursor", "default");
					$("#selectDepartmentLinkButton").prop('onclick', null).off('click');
					$("#checkbox_selected_personnel").prop("disabled", true);
					$("#text_selected_personnel").css("display", "none");
					break;
				case EDIT_MODE_COMPLETED:
					// cannot edit everything
					$(".fixed").css("display", "inline-block");
					$(".unfixed").css("display", "none");

					// privacy
					$("#checkbox_privacy").prop("disabled", true);
					$("#checkbox_selected_departments").prop("disabled", true);
					$("#selectDepartmentLinkButton").css("color", "#C1C1C1");
					$("#selectDepartmentLinkButton").css("cursor", "default");
					$("#selectDepartmentLinkButton").prop('onclick', null).off('click');
					$("#checkbox_selected_personnel").prop("disabled", true);
					$("#text_selected_personnel").css("display", "none");

					// other
					$("#lbApplyChanges").css("display", "none");
					$(".import-decks.survey-bar__search__button").css("display", "none");
					$(".add-pulse.survey-bar__search__button").css("display", "none");
					break;
				default:
					break;
			}

			model.deck.Pulses = pj.Pulses;
			displayPulseList(model.deck.Pulses);
			$(".card.card-level-1 .cascade-card").click();
		}

		////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////

		function clonePulse(pulse, logicLevel) {
			var card;
			card = new DynamicPulse($("#main_content_hfDeckId").val());

			//copy pulse to card
			card.ActionTaken = pulse.ActionTaken;
			card.Actions = pulse.Actions;
			card.CardType = pulse.CardType;
			card.DeckId = pulse.DeckId;
			card.DeletedDate = pulse.DeletedDate;
			card.Description = pulse.Description;
			card.EndDate = pulse.EndDate;
			card.EndDateString = pulse.EndDateString;
			card.HasIncomingLogic = pulse.HasIncomingLogic;
			card.HasOutgoingLogic = pulse.HasOutgoingLogic;
			card.ImageUrl = pulse.ImageUrl;
			card.IsAnonymous = pulse.IsAnonymous;
			card.IsComplusory = pulse.IsComplusory;
			card.IsForDepartment = pulse.IsForDepartment;
			card.IsForEveryone = pulse.IsForEveryone;
			card.IsForUser = pulse.IsForUser;
			card.IsPrioritized = pulse.IsPrioritized;
			if (logicLevel) {
				card.LogicLevel = logicLevel;
			} else {
				card.LogicLevel = pulse.LogicLevel;
			}
			card.MaxRangeLabel = pulse.MaxRangeLabel;
			card.MinRangeLabel = pulse.MinRangeLabel;
			card.NumberOfOptions = pulse.NumberOfOptions;
			card.Option1Content = pulse.Option1Content;
			card.Option1Id = pulse.Option1Id;
			card.Option1NextCardId = pulse.Option1NextCardId;
			card.Option2Content = pulse.Option2Content;
			card.Option2Id = pulse.Option2Id;
			card.Option2NextCardId = pulse.Option2NextCardId;
			card.Option3Content = pulse.Option3Content;
			card.Option3Id = pulse.Option3Id;
			card.Option3NextCardId = pulse.Option3NextCardId;
			card.Option4Content = pulse.Option4Content;
			card.Option4Id = pulse.Option4Id;
			card.Option4NextCardId = pulse.Option4NextCardId;
			card.Option5Content = pulse.Option5Content;
			card.Option5Id = pulse.Option5Id;
			card.Option5NextCardId = pulse.Option5NextCardId;
			card.Option6Content = pulse.Option6Content;
			card.Option6Id = pulse.Option6Id;
			card.Option6NextCardId = pulse.Option6NextCardId;
			card.Option7Content = pulse.Option7Content;
			card.Option7Id = pulse.Option7Id;
			card.Option7NextCardId = pulse.Option7NextCardId;
			card.Option8Content = pulse.Option8Content;
			card.Option8Id = pulse.Option8Id;
			card.Option8NextCardId = pulse.Option8NextCardId;
			card.Options = pulse.Options;
			card.Ordering = pulse.Ordering;
			card.ParentCard = pulse.ParentCard;
			card.ParentOption = pulse.ParentOption;
			card.ProgressStatus = pulse.ProgressStatus;
			card.PulseType = pulse.PulseType;
			card.QuestionType = pulse.QuestionType;
			card.RangeType = pulse.RangeType;
			card.RespondentChart = pulse.RespondentChart;
			card.SavedDate = pulse.SavedDate;
			card.SelectedOption = pulse.SelectedOption;
			card.SelectedRange = pulse.SelectedRange;
			card.StartDate = pulse.StartDate;
			card.StartDateString = pulse.StartDateString;
			card.Status = pulse.Status;
			card.TargetedDepartments = pulse.TargetedDepartments;
			card.TargetedUsers = pulse.TargetedUsers;
			card.TextAnswer = pulse.TextAnswer;
			card.TextAnswers = pulse.TextAnswers;
			card.Title = pulse.Title;
			card.TotalRespondents = pulse.TotalRespondents;


			//card.PulseId = pulse.PulseId;
			//card.cardLabel = pulse.cardLabel;

			model.deck.Pulses.push(card);

			return card;
		}

		function duplicatePulseChildren(originalPulseId, parentPulse, logicLevelShift) {
			var idx, pulseChildrenIdList;
			var cardId, pulse, childPulse;

			// Get list of children to clone
			pulseChildrenIdList = getPulseChildren(originalPulseId);

			for (idx = 0; idx < pulseChildrenIdList.length; idx++) {
				// Add pulse to model
				cardId = pulseChildrenIdList[idx];
				pulse = getPulse(cardId);
				childPulse = clonePulse(pulse, pulse.LogicLevel - logicLevelShift);

				// Update parent option n id
				parentPulse["Option" + (idx + 1).toString() + "Id"] = parentPulse.PulseId + "_" + (idx + 1).toString();
				parentPulse["Option" + (idx + 1).toString() + "NextCardId"] = childPulse.PulseId;

				html = getCardHtml(childPulse, parentPulse.PulseId, false); // need to feed parent card Id

				$(".card-list .card:last").after(html);
				loadPulseInfo(childPulse);

				if (getPulseChildren(cardId).length > 0) {
					duplicatePulseChildren(cardId, childPulse, logicLevelShift);
				}
			}
		}

		function duplicatePulse(button) {
			var card, cardId, pulse, newPulse, html, holderHtml, newPulseId;
			var pulseChildrenIdList, idx, childPulse, logicLevelShift;
			card = $(button).parents(".card");
			cardId = card.data("cardid");
			pulse = getPulse(cardId);
			newPulse = clonePulse(pulse, 1);
			html = getCardHtml(newPulse, null, false);
			holderHtml = getHolderHtml(html);
			$(".card-list").append(holderHtml);
			loadPulseInfo(newPulse);

			// Check for logic level shift
			if (pulse.LogicLevel) {
				logicLevelShift = pulse.LogicLevel - 1;
			}

			duplicatePulseChildren(cardId, newPulse, logicLevelShift);
		}

		function getCardHeaderHtml(pulseId, isAddCard) {
			var html = "";

			html = html + "<div class=\"grid__inner\" style=\"background-color:transparent;\">";
			html = html + "    <div style=\"float:right; width:50%;\">";
			html = html + "        <div class=\"grid__span--11\">";
			html = html + "            <button type=\"button\" class=\"survey-bar__search__button save-pulse-button\" onclick=\"savePulseCard(this);\">Save</button>";
			html = html + "        </div>";
			html = html + "        <div class=\"grid__span--1 grid__span--last\">";
			html = html + "            <div class=\"grid-question-expandable\">";
			if (!isAddCard) {
				html = html + "                <button type=\"button\" class=\"to-mini-card\" onclick=\"switchToMiniCard(this);\"><i class=\"fa fa-caret-square-o-down fa-lg icon\"></i></button>";
			}
			html = html + "            </div>";
			html = html + "        </div>";
			html = html + "    </div>";
			html = html + "    <div class=\"tabs--styled\">";
			html = html + "        <ul class=\"tabs__list\">";
			html = html + "            <li class=\"tabs__list__item\">";
			html = html + "                <a class=\"tabs__link active\" href=\"javascript:void(0);\">Pulse Type</a>";
			html = html + "            </li>";
			html = html + "        </ul>";
			html = html + "    </div>";
			html = html + "</div>";

			return html;
		}

		function getCardFooterHtml(isAddCard) {
			var html = "";

			html = html + "<div class=\"grid__inner\" style=\"background-color:transparent;\">";
			html = html + "    <div class=\"grid__span--4\">";
			html = html + "        Exact card sample from the APP. <br />";
			html = html + "        Uploaded image up to 200 x 200 and no bigger than 60KB.";
			html = html + "    </div>";
			html = html + "    <div class=\"grid__span--2\">";
			html = html + "        <button type=\"button\" class=\"duplicate-card\" onclick=\"duplicatePulse(this);\">Duplicate pulse</button>";
			html = html + "    </div>";
			html = html + "    <div class=\"grid__span--6 grid__span--last\">";
			if (isAddCard) {
				html = html + "        <button type=\"button\" style=\"background-color:red;float:right;\" onclick=\"cancelAddCard(this);\">Cancel</button>";
			}
			else {
				html = html + "        <button type=\"button\" style=\"background-color:red;float:right;\" onclick=\"deleteCard(this);\">Delete pulse</button>";
			}
			html = html + "    </div>";
			html = html + "</div>";

			return html;
		}

		function getLogicRoutingHtml(canCreate) {
			var html = "";

			html = html + "<div class=\"grid__inner logic-answer\" style=\"background-color:transparent;\">";
			html = html + "    <div class=\"grid__span--3\">When answer is</div>";
			html = html + "    <div class=\"grid__span--3\"><label class=\"pulse-option\" style=\"border-bottom:1px solid #ccc\">&nbsp;</label></div>";
			html = html + "    <div class=\"grid__span--2\">Advance to</div>";
			html = html + "    <div class=\"grid__span--4 grid__span--last\">";
			html = html + "        <select class=\"advanceTo-select\" onchange=\"logicRouteUpdate(this);\" style=\"width:100%;\">";
			html = html + "            <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
			if (canCreate) {
				html = html + "            <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse</option>";
			}
			html = html + "        </select>";
			html = html + "    </div>";
			html = html + "</div>";

			return html;
		}

		function getNextIndex(idx) {
			switch (idx) {
				case 0:
					return "A";
				case 1:
					return "B";
				case 2:
					return "C";
				case 3:
					return "D";
				case 4:
					return "E";
				case 5:
					return "F";
				case 6:
					return "G";
				case 7:
					return "H";
			}
		}

		function updateLogicOptions(dropdown, cardId) {
			var card, desiredOptionCount, idx, canCreate;
			var currentOptionCount,
				optionCorrection;

			canCreate = false;

			if (dropdown) {
				card = $(dropdown).parents(".card");
				canCreate = !card.hasClass("card-level-3");
			} else if (cardId) {
				card = $(".card-list .card." + cardId);
				canCreate = !card.hasClass("card-level-3");
			} else {
				return;
			}

			desiredOptionCount = parseInt($(card).find(".select-option").val(), 10);
			currentOptionCount = $(card).find("input.logic-pulse-option").length;
			optionCorrection = desiredOptionCount - currentOptionCount;

			if (optionCorrection > 0) {
				// Need to add
				for (idx = 0; idx < optionCorrection; idx++) {
					// Add option
					$(card).find(".option-list").append(
						"<input type=\"text\" class=\"logic-pulse-option\" placeholder=\"Option " + (currentOptionCount + idx + 1).toString() + "\" style=\"width:40%; border-radius:1em; margin: 5px; padding: 5px; display: inline-block\" onchange=\"logicPulseOptionUpdate(this);\" />");
					$(card).find(".card-logic").append(getLogicRoutingHtml(canCreate));
				}
			}
			if (optionCorrection < 0) {
				// Need to remove 
				$(card).find(".option-list").children(".logic-pulse-option").slice(optionCorrection).remove();
				$(card).find(".card-logic .logic-answer").slice(optionCorrection).remove();
			}
		}

		function recursiveRemovePulse(pulseId) {
			var pulse, idx;

			pulse = getPulse(pulseId);

			if (pulse.CardType == 4) { // if is logic card, recursive remove
				var selectOptionsCount = parseInt($(".card." + pulseId + " .select-option").val(), 10);
				if (!isNaN(selectOptionsCount)) {
					// for each option recursiveRemovePulse

					for (idx = 0; idx < selectOptionsCount; idx++) {
						if (pulse["Option" + (idx + 1).toString() + "NextCardId"]) {
							if (pulse["Option" + (idx + 1).toString() + "NextCardId"] != "PULSESETEND") {
								recursiveRemovePulse(pulse["Option" + (idx + 1).toString() + "NextCardId"]);
							}
						}
					}
				}
			} else {

			}

			// Remove this from model
			model.deck.Pulses.splice(model.deck.Pulses.indexOf(pulse), 1);
			// Remove this card from UI
			$(".card." + pulse.PulseId).remove();

		}


		function logicPulseOptionUpdate(textbox) {
			// update the logic 
			var card, optionIndex, logicAnswer;
			card = $(textbox).parents(".card");
			optionIndex = $(textbox).index();
			$(textbox).parents(".card").find(".card-logic .logic-answer label.pulse-option")[optionIndex].innerText = $(textbox).val();
		}

		function textCounter(input, label, maxcounter) {
			$("#" + label + "").html(maxcounter - input.value.length);
			if (input.value.length > maxcounter) {
				$("#div_deckTitle").css("border-color", "red");
				$("#" + label + "").css("color", "red");
			}
			else {
				$("#div_deckTitle").css("border-color", "#DDD");
				$("#" + label + "").css("color", "#DDD");
			}
		}

		function selectAllDepartments(cbSelectAll) {
			if (cbSelectAll.checked) {
				$("#department_checkboxlist input[type=checkbox]").prop("checked", true);
			}
			else {
				$("#department_checkboxlist input[type=checkbox]").prop("checked", false);
			}
		}

		function isDeckDataValid() {
		    debugger;
			// Title
			if ($("#deckTitle").val().trim().length == 0) {
				ShowToast('Please give this a title', 2);
				return false;
			}

			if ($("#deckTitle").val().trim().length > DECK_TITLE_COUNT_MAX) {
				toastr.error("Title length maximum is 50.");
				return false;
			}

			// Srart date
			startDateString = $("#deckStartDate").val() + " " + $("#deckStartHH").val() + ":" + $("#deckStartMM").val() + " " + $("#deckStartMer").val();
			if (!moment(startDateString, "DD/MM/YYYY h:m a", true).isValid()) {
				startDateString = null;
				ShowToast('Please enter a valid start date', 2);
				return false;
			}
			else
			{
			    model.deck.startDate = moment(startDateString, "DD/MM/YYYY h:m a", true);
			}

			// End date
			if (model.deck.publishMethodType === 1) // Publish method is "Schedute".
			{
				endDateString = $("#deckEndDate").val() + " " + $("#deckEndHH").val() + ":" + $("#deckEndMM").val() + " " + $("#deckEndMer").val();
				if (!moment(endDateString, "DD/MM/YYYY h:m a", true).isValid()) {
					endDateString = null;
					ShowToast('Please enter a valid end date', 2);
					return false;
				}
				else {

				    model.deck.endDate = moment(endDateString, "DD/MM/YYYY h:m a", true);

				    if (model.deck.endDate.isBefore(model.deck.startDate) || model.deck.endDate.isSame(model.deck.startDate)) {
						endDateString = null;
						ShowToast('Your date range is invalid', 2);
						return false;
					}
				}
			}

			// Privacy
			if (!$("#checkbox_privacy").is(":checked") && !$("#checkbox_selected_departments").is(":checked") && !$("#checkbox_selected_personnel").is(":checked")) {
				ShowToast('Please set a privacy setting', 2);
				return false;
			}

			if ($("#checkbox_selected_departments").is(":checked")) // Departments for Privacy
			{
				if (model.deck.targetedDepartmentIds.length == 0) {
					ShowToast('Please select at least one department', 2);
					return false;
				}
			}

			if ($("#checkbox_selected_personnel").is(":checked")) // Personnels for Privacy
			{
				if (model.deck.targetedUserIds.length == 0) {
					ShowToast('Please select at least one personnel', 2);
					return false;
				}
			}

			return true;
		}

		function getPulseChildren(pulseId) {
			var idx, children = [];
			$("[data-parentcardid='" + pulseId + "']").each(function (idx, ele) {
				children.push($(ele).data("cardid"));
			});
			return children;
		}

		(function ($) {
			"use strict";

			timezone = parseFloat($("#main_content_hfTimezone").val());

			// Privacy personnel related javascript functions -- START

			$("#text_selected_personnel").autocomplete({
				source: "/api/personnel?companyid=" + $("#main_content_hfCompanyId").val() + "&userid=" + $("#main_content_hfAdminUserId").val(),
				minLength: 1,
				response: function (event, ui) {
					for (var i = 0; i < ui.content.length; i++) {
						if (model.deck.targetedUserIds.indexOf(ui.content[i].value) != -1) {
							ui.content.splice(i, 1);
							if (i != ui.content.length - 1) {
								i--;
							}
						}
					}
				},
				select: function (event, ui) {
					var html;
					if (model.deck.targetedUserIds.indexOf(ui.item.value) == -1) {
						model.deck.targetedUserIds.push(ui.item.value);

						html = "";
						html = html + "<div class=\"tag " + ui.item.value + "\">";
						html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + ui.item.label + "</span>";
						html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + ui.item.value + "');\">x</a>";
						html = html + "</div>";
						$(".personnel.tags").append(html);

						$("#checkbox_selected_personnel").prop("checked", model.deck.targetedUserIds.length > 0);
						var everyoneChecked = !($("#checkbox_selected_departments").prop("checked") || $("#checkbox_selected_personnel").prop("checked"));
						$("#checkbox_privacy").prop("checked", everyoneChecked);
					}
					return false;
				},
				focus: function (event, ui) {
					event.preventDefault();
				},
				close: function (event, ui) {
					$("#text_selected_personnel").val("");
				}
			});

			// Privacy personnel related javascript functions -- END

			$(".publish_method").change(function () {
				if (this.value == "1") { // schedule
					// Display end date
					$(".publish_method.schedule-settings").show();
					// Hide routine controls
					$(".publish_method.routine-settings").hide();
				}
				if (this.value == "2") { // routine
					// Hide end date
					$(".publish_method.schedule-settings").hide();
					// Display routine controls
					$(".publish_method.routine-settings").show();
				}
			});

			$("#routine_period").change(function () {
				var selectedValue = $("#routine_period").find(":selected").val();
				$("#routine_interval_for_week").css("display", "none");
				$("#routine_interval_for_moonth").css("display", "none");
				switch (parseInt(selectedValue)) {
					case 1: // per day
						// do nothing
						break;
					case 2: // per week
						$("#routine_interval_for_week").css("display", "inline");
						//$("#routine_interval_for_duration").css("display", "inline");
						break;
					case 3: // per month
						$("#routine_interval_for_moonth").css("display", "inline");
						//$("#routine_interval_for_duration").css("display", "inline");
						break;
					default:
						break;
				}

			});

			$("#testButton").click(function () {
				jQuery.ajax({
					type: "GET",
					url: "/api/PulseResponsiveCard",
					contentType: "application/json; charset=utf-8",
					data: {},
					dataType: "json",
					beforeSend: function (xhr, settings) {
						ShowProgressBar();
					},
					success: function (d, status, xhr) {

					},
					error: function (xhr, status, error) {

					},
					complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
						HideProgressBar();
					}
				});
			});

			$(document).ready(function () {

				$("#deckStartDate").datepicker({
					beforeShow: function () {
						setTimeout(function () {
							$('.ui-datepicker').css('z-index', 99999);
						}, 0);
					},
					dateFormat: 'dd/mm/yy'
				});
				$("#deckEndDate").datepicker({
					beforeShow: function () {
						setTimeout(function () {
							$('.ui-datepicker').css('z-index', 99999);
						}, 0);
					},
					dateFormat: 'dd/mm/yy'
				});

				// Add a new card
				$("button.add-pulse").click(addNewCard);

				$("button.import-decks").click(function () {
					// call get decks api
					jQuery.ajax({
						type: "GET",
						url: "/api/PulseResponsiveDeck?adminUserId=" + $("#main_content_hfAdminUserId").val() + "&companyId=" + $("#main_content_hfCompanyId").val(),
						contentType: "application/json; charset=utf-8",
						data: {},
						dataType: "json",
						beforeSend: function (xhr, settings) {
							ShowProgressBar();
						},
						success: function (d, status, xhr) {
							if (d.Success) {
								deckList = d.Decks;

								// Render deck
								var html, count = 0;
								$("#deck_checkboxlist").children().remove();
								for (var i = 0; i < deckList.length; i++) {
									if (deckList[i].Id != $("#main_content_hfDeckId").val() && deckList[i].NumberOfCards > 0) {
										html = "<label><input type=\"checkbox\" value=\"" + deckList[i].Id + "\" />" + deckList[i].Title + "</label><br />";
										$("#deck_checkboxlist").append(html);
										count++;
									}
								}

								if (count == 0) {
									$("#deck_checkboxlist").append("There is no available decks for importing");
								}

								$("#mpe_backgroundElement").show();
								$("#plSelectDecks").show();

							} else {
								ShowToast(d.ErrorMessage, 2);
							}
						},
						error: function (xhr, status, error) {
							ShowToast(error, 2);
						},
						complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
							HideProgressBar();
						}
					});
				});

				$(".survey-bar__search__button.analytics").click(function () {
					window.open("/DynamicPulse/ResponsivePulseAnalytic/" + $("#main_content_hfDeckId").val());
				});

				$("#lbApplyChanges").click(function () {
					// Check input
					if (!isDeckDataValid()) {
						return;
					}

					var HTTP_METHOD = "POST";
					saveToModelDeck();
					if ($("#main_content_hfDeckId").val().trim().length > 0) {
						HTTP_METHOD = "POST"; // update all, just status
					} else {
						HTTP_METHOD = "PUT"; // create 
					}

					jQuery.ajax({
						type: HTTP_METHOD,
						url: "/api/PulseResponsiveDeck",
						contentType: "application/json; charset=utf-8",
						data: JSON.stringify(model.deck),
						dataType: "json",
						beforeSend: function (xhr, settings) {
							ShowProgressBar();
						},
						success: function (d, status, xhr) {
							if (d.Success) {
								if (HTTP_METHOD == "PUT") {
									$("#main_content_hfDeckId").val(d.DeckId);
									ShowToast('Deck added', 1);
								} else {
									ShowToast('Deck updated', 1);

								}
								RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
							} else {
								ShowToast(d.ErrorMessage, 2);
							}
						},
						error: function (xhr, status, error) {
							ShowToast(error, 2);
						},
						complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
							HideProgressBar();
						}
					});
				});

				loadDeck();
				saveToModelDeck();

				$(".XXXcard-list").sortable({

					items: ".card.mini-card",
					cancel: ".full-card",
					axis: "y",
					forcePlaceholderSize: true,
					dropOnEmpty: true,
					placeholder: ".mini-card-holder",
					stop: function (e, ui) {

					},
					update: function (event, ui) {

						var origCardContainer,
							origCardContainerPos, origCardContainerRow,
							currentCardContainerPos, currentCardContainerRow,
							otherPulse, prevCardContainerRow, nextCardContainerRow;
						var idx, $cardHolders, cardIdx, cardCount, $cards, $card, newLabel, regex, match;

						origCardContainer = $(".mini-card-holder:not(:has(:visible(true)))");

						otherPulse = ui.item.siblings();

						origCardContainerRow = origCardContainer.parent();
						currentCardContainerRow = ui.item.parent().parent();

						origCardContainerPos = $(".mini-card-holder").index(origCardContainer);
						currentCardContainerPos = $(".mini-card-holder").index(ui.item.parent());

						if (origCardContainerPos < currentCardContainerPos) { // implies moving card downwards
							prevCardContainerRow = origCardContainerRow;
							while (prevCardContainerRow.attr("class") !== currentCardContainerRow.attr("class")) {
								nextCardContainerRow = prevCardContainerRow.next();
								// Move the card in the next card container to prevCardContainerRow
								if (nextCardContainerRow.attr("class") == currentCardContainerRow.attr("class")) {
									ui.item.after(origCardContainer.children(":hidden"));
									prevCardContainerRow.children(".mini-card-holder").append(otherPulse);
								} else {
									prevCardContainerRow.children(".mini-card-holder").append(
									nextCardContainerRow.children(".mini-card-holder").children(".card"));
									nextCardContainerRow.children(".mini-card-holder").children(".card").remove();
								}

								prevCardContainerRow = nextCardContainerRow;
							} // end-while; end of moving pulse, rename numbers

							var $cardHolders = $(".card-list .card-holder");
							for (idx = 0; idx < $cardHolders.length; idx++) {
								$($cardHolders[idx]).find(".card-label").text("RP" + (idx + 1).toString());
							}

						} else { // implies moving card upwards
							prevCardContainerRow = origCardContainerRow;
							while (prevCardContainerRow.attr("class") !== currentCardContainerRow.attr("class")) {
								nextCardContainerRow = prevCardContainerRow.prev();
								// Move the card in the next card container to prevCardContainerRow
								if (nextCardContainerRow.attr("class") == currentCardContainerRow.attr("class")) {
									ui.item.after(origCardContainer.children(":hidden"));
									prevCardContainerRow.children(".mini-card-holder").append(otherPulse);
								} else {
									prevCardContainerRow.children(".mini-card-holder").append(
									nextCardContainerRow.children(".mini-card-holder").children(".card"));
									nextCardContainerRow.children(".mini-card-holder").children(".card").remove();
								}
								prevCardContainerRow = nextCardContainerRow;
							} // end-while; end of moving pulse, rename numbers

							var $cardHolders = $(".card-list .card-holder");
							for (idx = 0; idx < $cardHolders.length; idx++) {
								$cards = $($cardHolders[idx]).find(".card");
								cardCount = $($cardHolders[idx]).find(".card").length;
								for (cardIdx = 0; cardIdx < cardCount; cardIdx++) {
									$card = $($cards[cardIdx]);

									if ($card.hasClass("card-level-1")) {
										newLabel = "RP" + ($(".card-list .card-holder").index($cardHolders[idx]) + 1).toString();
										$card.find(".card-label").text(newLabel);
									} else if ($card.hasClass("card-level-2")) {
										// Parse RP label and update it to new
										regex = /RP\d+(\w+)/ig;
										match = regex.exec($card.find(".card-label").text());
										if (match) {
											newLabel = "RP" + ($(".card-list .card-holder").index($cardHolders[idx]) + 1).toString() + match[1];
											$card.find(".card-label").text(newLabel);
										}
									} else if ($card.hasClass("card-level-3")) {
										// Parse RP label and update it to new

										regex = /RP\d+(\w+)/ig;
										match = regex.exec($card.find(".card-label").text());
										if (match) {
											newLabel = "RP" + ($(".card-list .card-holder").index($cardHolders[idx]) + 1).toString() + match[1];
											$card.find(".card-label").text(newLabel);
										}
									}
								}
							}
						}
					}
				});

				$(".card-publish-date .cardStartDate").datepicker({
					dateFormat: 'dd M yy, DD'
				});
			});
		}(jQuery));

	</script>
</asp:Content>
