﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.DynamicPulse
{
    public partial class ResponsivePulse : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        private List<String> iconUrlList = null;

        public void GetIconData()
        {
            TopicSelectIconResponse response = asc.SelectAllTopicIcons(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
            if (response.Success)
            {
                iconUrlList = response.TopicIconUrls;
            }
        }

        public DeckCardOption GetDeckCardOption(CassandraService.Entity.PulseDynamic pd, int set)
        {
            DeckCardOption result = new DeckCardOption();
            if (set == 1)
            {
                result.OptionId = pd.Option1Id;
                result.Content = pd.Option1Content;
                //result.NextPulse = "";
            }

            switch (set)
            {
                case 1:
                    result.OptionId = pd.Option1Id;
                    result.Content = pd.Option1Content;
                    break;
                case 2:
                    result.OptionId = pd.Option2Id;
                    result.Content = pd.Option2Content;
                    break;
                case 3:
                    result.OptionId = pd.Option3Id;
                    result.Content = pd.Option3Content;
                    break;
                case 4:
                    result.OptionId = pd.Option4Id;
                    result.Content = pd.Option4Content;
                    break;
                case 5:
                    result.OptionId = pd.Option5Id;
                    result.Content = pd.Option5Content;
                    break;
                case 6:
                    result.OptionId = pd.Option6Id;
                    result.Content = pd.Option6Content;
                    break;
                case 7:
                    result.OptionId = pd.Option7Id;
                    result.Content = pd.Option7Content;
                    break;
                case 8:
                    result.OptionId = pd.Option8Id;
                    result.Content = pd.Option8Content;
                    break;
            }

            return result;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            ViewState["manager_user_id"] = managerInfo.UserId;
            ViewState["company_id"] = managerInfo.CompanyId;

            this.hfAdminUserId.Value = ViewState["manager_user_id"].ToString();
            this.hfCompanyId.Value = ViewState["company_id"].ToString();
            this.hfTimezone.Value = Convert.ToString(managerInfo.TimeZone);

            //  if have deck id, 
            //  1.  fetch deck info 
            //  2.  load deck info into deck header

            String deckId = String.Empty;

            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["DeckId"] != null)
                {
                    deckId = Page.RouteData.Values["DeckId"].ToString().Trim();
                }

                if (!string.IsNullOrWhiteSpace(deckId))
                {
                    PulseSelectDeckResponse selectResponse = asc.SelectFullDetailDeck(managerInfo.UserId, managerInfo.CompanyId, deckId, false);
                    this.hfDeckId.Value = deckId;
                    if (selectResponse.Success)
                    {
                        hfDeck.Value = javascriptSerializer.Serialize(selectResponse.Deck);

                        int numberOfOptions = 0;
                        int optIdx = 0;
                        CassandraService.Entity.PulseDynamic pd = null;
                        foreach (Pulse p in selectResponse.Deck.Pulses)
                        {
                            pd = p as CassandraService.Entity.PulseDynamic;
                            if (pd != null)
                            {
                                // Fill up options
                                numberOfOptions = pd.NumberOfOptions;
                                if ((numberOfOptions > 0) && (pd.Options == null))
                                {
                                    pd.Options = new List<DeckCardOption>();
                                    for (optIdx = 0; optIdx < numberOfOptions; optIdx++)
                                    {
                                        pd.Options.Add(GetDeckCardOption(pd, (optIdx + 1)));
                                    }
                                }
                            }
                        }

                        hfPulseList.Value = javascriptSerializer.Serialize(selectResponse.Deck.Pulses);

                        ltlActionName.Text = "Edit 'Responsive' Pulse";

                        #region For new data structure
                        //App_Code.Entity.DynamicPulse.ResponsivePulse.Deck deck = new App_Code.Entity.DynamicPulse.ResponsivePulse.Deck();
                        //deck.Id = selectResponse.Deck.Id;
                        //deck.Title = selectResponse.Deck.Title;
                        //deck.IsCompulsory = selectResponse.Deck.IsCompulsory;
                        //deck.Status = selectResponse.Deck.Status;
                        //deck.IsPrioritized = selectResponse.Deck.IsPrioritized;
                        //deck.IsAnonymous = selectResponse.Deck.IsDeckAnonymous;
                        //deck.PublishMethodType = selectResponse.Deck.PublishMethodType;
                        //deck.StartTimestamp = selectResponse.Deck.DeckStartTimestamp;
                        //deck.EndTimestamp = selectResponse.Deck.DeckEndTimestamp.Value;
                        //deck.NumberOfCardsPerTimeFrame = selectResponse.Deck.NumberOfCardsPerTimeFrame;
                        //deck.PerTimeFrameType = selectResponse.Deck.PerTimeFrameType;
                        //deck.PeriodFrameType = selectResponse.Deck.PeriodFrameType;
                        //deck.Duration = selectResponse.Deck.Duration;
                        //deck.TargetedDepartments = selectResponse.Deck.TargetedDepartments;
                        //deck.TargetedUsers = selectResponse.Deck.TargetedUsers;
                        //deck.IsForEveryone = selectResponse.Deck.IsForEveryone;
                        //deck.IsForDepartment = selectResponse.Deck.IsForDepartment;
                        //deck.IsForUser = selectResponse.Deck.IsForUser;
                        //if (selectResponse.Deck.Pulses != null && selectResponse.Deck.Pulses.Count > 0)
                        //{
                        //    deck.Pulses = new List<App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse>();

                        //    List<App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse> Pulses = new List<App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse>();

                        //    // Step 1. Get Level 1 Pulses.
                        //    for (int i = 0; i < selectResponse.Deck.Pulses.Count; i++)
                        //    {
                        //        if (selectResponse.Deck.Pulses[i].LogicLevel == 0 || selectResponse.Deck.Pulses[i].LogicLevel == 1)
                        //        {
                        //            App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse pulse = new App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse();
                        //            pulse.PulseId = selectResponse.Deck.Pulses[i].PulseId;
                        //            pulse.DeckId = selectResponse.Deck.Id;
                        //            pulse.QuestionType = selectResponse.Deck.Pulses[i].QuestionType;
                        //            pulse.QuestionContent = selectResponse.Deck.Pulses[i].Title;
                        //            pulse.Description = selectResponse.Deck.Pulses[i].Description;
                        //            pulse.ImageUrl = selectResponse.Deck.Pulses[i].ImageUrl;
                        //            pulse.RangeType = selectResponse.Deck.Pulses[i].RangeType;
                        //            pulse.MinRangeLabel = selectResponse.Deck.Pulses[i].MinRangeLabel;
                        //            pulse.MaxRangeLabel = selectResponse.Deck.Pulses[i].MaxRangeLabel;
                        //            pulse.StartDate = selectResponse.Deck.Pulses[i].StartDate;
                        //            pulse.LogicLevel = selectResponse.Deck.Pulses[i].LogicLevel;
                        //            pulse.IsCustomized = selectResponse.Deck.Pulses[i].IsCustomized;
                        //            if (selectResponse.Deck.Pulses[i].Options != null && selectResponse.Deck.Pulses[i].Options.Count > 0)
                        //            {
                        //                pulse.Options = new List<App_Code.Entity.DynamicPulse.ResponsivePulse.Option>();
                        //                for (int j = 0; j < selectResponse.Deck.Pulses[i].Options.Count; j++)
                        //                {
                        //                    App_Code.Entity.DynamicPulse.ResponsivePulse.Option option = new App_Code.Entity.DynamicPulse.ResponsivePulse.Option();
                        //                    option.OptionId = selectResponse.Deck.Pulses[i].Options[j].OptionId;
                        //                    option.PulseId = selectResponse.Deck.Pulses[i].PulseId;
                        //                    option.Content = selectResponse.Deck.Pulses[i].Options[j].Content;
                        //                    option.Ordering = selectResponse.Deck.Pulses[i].Options[j].NumberOfSelection;
                        //                    option.NextPulseId = selectResponse.Deck.Pulses[i].Options[j].NextPulseId;
                        //                    option.NextPulseLabel = selectResponse.Deck.Pulses[i].Options[j].NextPulse.Ordering;
                        //                }
                                        

                        //            }
                        //            else
                        //            {
                        //                pulse.Options = null;
                        //            }




                        //            /****/
                        //            pulse.ParentPulseId = null;
                        //            if (selectResponse.Deck.Pulses[i].LogicLevel == 0)
                        //            {
                        //                pulse.ChildrenPulse = null;
                        //            }
                        //            else
                        //            {
                        //                pulse.ChildrenPulse = new List<App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse>();
                        //            }
                        //            deck.Pulses.Add(pulse);
                        //        }
                        //    }

                        //    deck.Pulses = deck.Pulses.OrderBy(p => p.StartDate).ToList();


                        //    // Step 2. Get Leve 2 Pulses
                        //    for (int i = 0; i < selectResponse.Deck.Pulses.Count; i++)
                        //    {
                        //        if (selectResponse.Deck.Pulses[i].LogicLevel == 1)
                        //        {
                        //            App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse pulse = new App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse();
                        //            pulse.PulseId = selectResponse.Deck.Pulses[i].PulseId;
                        //            pulse.DeckId = selectResponse.Deck.Id;
                        //            pulse.QuestionType = selectResponse.Deck.Pulses[i].QuestionType;
                        //            pulse.QuestionContent = selectResponse.Deck.Pulses[i].Title;
                        //            pulse.Description = selectResponse.Deck.Pulses[i].Description;
                        //            pulse.ImageUrl = selectResponse.Deck.Pulses[i].ImageUrl;
                        //            pulse.RangeType = selectResponse.Deck.Pulses[i].RangeType;
                        //            pulse.MinRangeLabel = selectResponse.Deck.Pulses[i].MinRangeLabel;
                        //            pulse.MaxRangeLabel = selectResponse.Deck.Pulses[i].MaxRangeLabel;
                        //            pulse.StartDate = selectResponse.Deck.Pulses[i].StartDate;
                        //            pulse.LogicLevel = selectResponse.Deck.Pulses[i].LogicLevel;
                        //            pulse.IsCustomized = selectResponse.Deck.Pulses[i].IsCustomized;
                        //            /****/
                        //            pulse.ParentPulseId = selectResponse.Deck.Pulses[i].ParentCard.PulseId;
                        //            pulse.ChildrenPulse = new List<App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse>();
                        //            App_Code.Entity.DynamicPulse.ResponsivePulse.Pulse ParentPulse = deck.Pulses.Find(p => p.PulseId.Contains(selectResponse.Deck.Pulses[i].ParentCard.PulseId));
                        //            ParentPulse.ChildrenPulse.Add(pulse);
                        //            int index = deck.Pulses.FindIndex(p => p.PulseId.Contains(selectResponse.Deck.Pulses[i].ParentCard.PulseId));
                        //            deck.Pulses[index] = ParentPulse;
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    deck.Pulses = null;
                        //}
                        #endregion
                    }
                }
                else
                {
                    plSearchQuestion.Visible = false;
                }
            }
        }

        protected void lbAnalytics_Click(object sender, EventArgs e)
        {

        }

        protected void lbSearch_Click(object sender, EventArgs e)
        {
            try
            {
               
                PulseSelectDeckResponse selectResponse = asc.SelectFullDetailDeck(managerInfo.UserId, managerInfo.CompanyId, hfDeckId.Value, false, tbSearch.Text.Trim());
                if (selectResponse.Success)
                {
                    hfDeck.Value = javascriptSerializer.Serialize(selectResponse.Deck);

                    int numberOfOptions = 0;
                    int optIdx = 0;
                    CassandraService.Entity.PulseDynamic pd = null;
                    foreach (Pulse p in selectResponse.Deck.Pulses)
                    {
                        pd = p as CassandraService.Entity.PulseDynamic;
                        if (pd != null)
                        {
                            // Fill up options
                            numberOfOptions = pd.NumberOfOptions;
                            if ((numberOfOptions > 0) && (pd.Options == null))
                            {
                                pd.Options = new List<DeckCardOption>();
                                for (optIdx = 0; optIdx < numberOfOptions; optIdx++)
                                {
                                    pd.Options.Add(GetDeckCardOption(pd, (optIdx + 1)));
                                }
                            }
                        }
                    }

                    hfPulseList.Value = javascriptSerializer.Serialize(selectResponse.Deck.Pulses);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "refreshListDisplay", "loadDeck(); saveToModelDeck();", true);

                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}