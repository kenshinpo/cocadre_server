﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AnnouncementPulse.aspx.cs" Inherits="AdminWebsite.DynamicPulse.AnnouncementPulse" MasterPageFile="~/Navi.Master" Title="Announcement Pulse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

    <div id="plSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Select Department</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <input id="selectAllDepartment" type="checkbox" onchange="selectAllDepartments(this);" />
                        <label for="selectAllDepartment">Select All</label>
                    </div>
                    <hr />
                    <div class="accessrights">
                        <span id="department_checkboxlist">
                            <label>
                                <input type="checkbox" value="D04461505c52d44d8be0ff0d80c6a2408" />Human Resource
                            </label>
                            <br />
                        </span>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:saveDepartmentSelection()">Select</a>
            <a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDepartmentSelector()">Cancel</a>
        </div>
    </div>

    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }

        .invalid-field {
            border-bottom-color: #ff0000 !important;
        }

        .grey-text {
            color: #ccc;
        }

        .chevron:hover {
            color: #839423 !important;
            cursor: pointer;
        }

        .text-colour {
            width: 16px;
            height: 16px;
            border: solid 2px #ccc;
            display: inline-block;
        }

            .text-colour.selected {
                border: solid 2px blue !important;
            }

        .ios-toggle,
        .ios-toggle:active {
            position: absolute;
            top: -5000px;
            height: 0;
            width: 0;
            opacity: 0;
            border: none;
            outline: none;
        }

        .checkbox-label {
            display: block;
            position: relative;
            padding: 10px;
            margin-bottom: 20px;
            font-size: 12px;
            line-height: 16px;
            width: 100%;
            height: 36px;
            /*border-radius*/
            -webkit-border-radius: 18px;
            -moz-border-radius: 18px;
            border-radius: 18px;
            background: #f8f8f8;
            cursor: pointer;
        }

            .checkbox-label:before {
                content: '';
                display: block;
                position: absolute;
                z-index: 1;
                line-height: 34px;
                text-indent: 40px;
                height: 36px;
                width: 36px;
                /*border-radius*/
                -webkit-border-radius: 100%;
                -moz-border-radius: 100%;
                border-radius: 100%;
                top: 0px;
                left: 0px;
                right: auto;
                background: white;
                /*box-shadow*/
                -webkit-box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
                -moz-box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
                box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
            }

            .checkbox-label:after {
                content: attr(data-off);
                display: block;
                position: absolute;
                z-index: 0;
                top: 0;
                left: -300px;
                padding: 10px;
                height: 100%;
                width: 300px;
                text-align: right;
                color: #bfbfbf;
                white-space: nowrap;
            }

        .ios-toggle:checked + .checkbox-label {
            /*box-shadow*/
            -webkit-box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
            -moz-box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
            box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
        }

            .ios-toggle:checked + .checkbox-label:before {
                left: calc(100% - 36px);
                /*box-shadow*/
                -webkit-box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
                -moz-box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
                box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
            }

            .ios-toggle:checked + .checkbox-label:after {
                content: attr(data-on);
                left: 60px;
                width: 36px;
            }

        .onoffswitch {
            position: relative;
            width: 76px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }

        .onoffswitch-checkbox {
            display: none;
        }

        .onoffswitch-label {
            display: block;
            overflow: hidden;
            cursor: pointer;
            border-radius: 20px;
        }

        .onoffswitch-inner {
            display: block;
            width: 200%;
            margin-left: -100%;
            transition: margin 0.3s ease-in 0s;
        }

            .onoffswitch-inner:before, .onoffswitch-inner:after {
                display: block;
                float: left;
                width: 50%;
                height: 30px;
                padding: 0;
                line-height: 30px;
                font-size: 13px;
                color: white;
                font-weight: bold;
                box-sizing: border-box;
            }

            .onoffswitch-inner:before {
                content: "YES";
                padding-left: 10px;
                background-color: #709EEB;
                color: #F2F2F2;
            }

            .onoffswitch-inner:after {
                content: "NO";
                padding-right: 10px;
                background-color: #4d4d4d;
                color: #F2F2F2;
                text-align: right;
            }

        .onoffswitch-switch {
            display: block;
            width: 26px;
            height: 26px;
            margin: 2px;
            background: #F2F2F2;
            position: absolute;
            bottom: 0px;
            right: 46px;
            border-radius: 20px;
            transition: all 0.3s ease-in 0s;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
            margin-left: 0;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
            right: 0px;
        }
    </style>

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTimezone" />
    <asp:HiddenField runat="server" ID="hfPulseId" />
    <asp:HiddenField runat="server" ID="hfPulse" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/DynamicPulse/AnnouncementPulseFeedList" style="color: #000;">Dynamic <span>pulse</span></a></div>
        <div class="appbar__meta">
            <a href="/DynamicPulse/AnnouncementPulseFeedList">Announcement Pulse</a>
        </div>
        <div class="appbar__meta">
            <span class="pulse_action" style="color: gray;">Add 'Announcement' Pulse</span>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">

        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item"><a class="data__sidebar__link data__sidebar__link--active">Announcement Pulse</a></li>
                <li class="data__sidebar__list__item"><a class="data__sidebar__link" href="/DynamicPulse/ResponsivePulseFeedList">Responsive Pulse</a></li>
                <li class="data__sidebar__list__item"><a class="data__sidebar__link" href="/DynamicPulse/ClarityPulseFeedList">Clarity Pulse</a></li>
            </ul>
        </aside>

        <div class="data__content">
            <div style="margin: 30px;">
                <div>
                    <h2><span class="pulse_action">Add</span> 'Announcement' Pulse</h2>
                    <div>
                        <asp:LinkButton ID="lbViewAnalytics" runat="server" CssClass="btn secondary" Text="View Analytics" Style="float: right; width: auto;" OnClick="lbViewAnalytics_Click" Visible="false" />
                    </div>
                </div>
                <p>
                    Manually generate an 'Announcement' pulse card on the CoCadre APP.
                </p>

                <div class="tabs tabs--styled" style="min-width: 1000px;">
                    <ul class='tabs__list'>
                        <li class='tabs__list__item'><a class="tabs__link" href="#tab-card-info">Card Info</a></li>
                        <li class='tabs__list__item'><a class="tabs__link" href="#tab-privacy">Privacy</a></li>
                    </ul>
                    <div id="tab-card-info">
                        &nbsp;
                        <div class="grid__inner" style="background: 0;">
                            <div class="grid__span--9">
                                <div class="grid__inner pulse-card" style="background: 0;">
                                    <div class="grid__span--1" style="margin-top: 120px; font-size: 7em; color: blue; text-align: right; margin-right: 0;"></div>

                                    <div class="grid__span--10" style="margin-right: 10px;">
                                        <div id="pulseCard" style="position: relative; height: 285px; width: 680px; border: 2px solid #ccc; border-radius: .5em; background-image: none; background-repeat: no-repeat; background-position: center; margin: 0 auto; border-left: 8px solid #F25C4E;">

                                            <div style="position: absolute; width: 100%;">
                                                <div class="text_container">
                                                    <input type="text" id="pulseCardTitle"
                                                        style="margin: 5px 1em; resize: none; overflow: no-display; overflow-wrap: break-word; display: block; width: 560px; height: auto; background-color: white; font-size: 14pt; position: relative; color: black; border: 1px solid #ccc; border-radius: .5em; padding: 0.75em 2em 0.75em 0.75em;" placeholder="Title" onkeyup="letterCounter(this, TEXT_MAX_LENGTH_TITLE);" />
                                                    <span id="titleCounter" class="letterCount" style="position: absolute; left: 550px; top: 35px;">999</span>
                                                </div>

                                                <div class="text_container">
                                                    <textarea id="pulseCardText" 
                                                        rows="3"
                                                        style="margin: 0px 1em; resize: none; overflow: no-display; overflow-wrap: break-word; display: block; width: 560px; min-height: 80px; height: auto; background-color: white; font-size: 14pt; position: relative; color: black; border: 1px solid #ccc; border-radius: .5em;"
                                                        placeholder="Description" onkeyup="letterCounter(this, TEXT_MAX_LENGTH_DESCRIPTION);"></textarea>
                                                    <span id="descriptionCounter" class="letterCount" style="position: absolute; left: 550px; top: 135px;">999</span>
                                                </div>
                                                <hr />

                                                <span style="float: right; cursor: default; background-color: white; border-radius: 0.1em; font-weight: bold; font-size: 24pt; height: 48px; border: 2px solid #F25C4E; color: #F25C4E; padding: 0 1em; margin-right: .3em;">Acknowledge</span>

                                            </div>

                                        </div>
                                        <p>
                                            &nbsp;
                                        </p>


                                        <h3>Publish method</h3>

                                        <h5>Scheduled</h5>

                                        <div>
                                            <span style="display: inline;">
                                                <label style="width: 100px; display: inline;">Start date</label>
                                                <input type="text" id="start_date" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" />
                                                at
                                                <input type="text" id="start_hh" maxlength="2" style="width: 4em; display: inline;" placeholder="hh" onchange="validateHour(this);" />:
                                                <input type="text" id="start_mm" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" />
                                                <select id="start_meridiem" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px;">
                                                    <option value="am">am</option>
                                                    <option value="pm">pm</option>
                                                </select>
                                            </span>
                                        </div>

                                        <div>
                                            <span style="width: 100px; display: inline;">End date</span>
                                            <label style="display: inline; margin-left: 10px;">
                                                <input type="radio" name="end_date" class="end_date forever" value="forever" checked="checked" />
                                                <span>Forever</span>
                                            </label>
                                            <br />
                                            <label style="display: inline; margin-left: 66px;">
                                                <input type="radio" name="end_date" class="end_date specific_date" value="specific_date" />
                                                <span style="display: inline;">
                                                    <input type="text" id="end_date" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" />
                                                    at
                                                    <input type="text" id="end_hh" maxlength="2" style="width: 4em; display: inline;" placeholder="hh" onchange="validateHour(this);" />:
                                                    <input type="text" id="end_mm" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" />
                                                    <select id="end_meridiem" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px;">
                                                        <option value="am">am</option>
                                                        <option value="pm">pm</option>
                                                    </select>
                                                </span>
                                            </label>
                                        </div>
                                        <div>
                                            <span style="display: inline;">
                                                <label style="width: 100px; display: inline;">Status</label>
                                                <select id="select_status" style="width: 12em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px; margin-left: 10px;">
                                                    <option value="1">Unlisted</option>
                                                    <option value="2">Active</option>
                                                    <option value="3">Hidden</option>
                                                </select>
                                            </span>
                                        </div>

                                        <p>
                                            <a href="/DynamicPulse/AnnouncementPulseFeedList" class="btn">CANCEL</a>
                                            <button type="button" class="btn" id="button_create" style="display: none;">CREATE</button>
                                            <button type="button" class="btn" id="button_update" style="display: none;">UPDATE</button>
                                        </p>

                                    </div>

                                    <div class="grid__span--1" style="margin-top: 120px; font-size: 7em; color: blue; text-align: left;"></div>
                                </div>
                            </div>
                            <div class="grid__span--3">
                                <label>
                                    <i class="fa fa-star" style="color: #FFC700"></i>
                                    Priority Overrule <i class="fa fa-info-circle" style="color: #E6E6E6"></i>
                                </label>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="checkbox_priorityOverrule" style="display: none" onchange="setPriorityOverrule(this);" />
                                    <label class="onoffswitch-label" for="checkbox_priorityOverrule">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- content -->
                    </div>

                    <div id="tab-privacy">
                        <p>
                            <label for="checkbox_privacy" style="margin-top: 15px;">
                                <input type="checkbox" id="checkbox_privacy" onchange="checkbox_selected_everyone();" checked="checked" />
                                <i class="fa fa-users" style="color: #999999"></i>Everyone
                            </label>
                        </p>
                        <hr />
                        <p>
                            <label for="checkbox_selected_departments">
                                <input type="checkbox" id="checkbox_selected_departments" onchange="checkbox_selected_departments_change();" />
                                <i class="fa fa-briefcase" style="color: #999999"></i>Selected departments
                            </label>
                            <div class="department tags"></div>
                            (<span id="selectDepartmentLinkButton" style="color: #C1C1C1; cursor: default;">+ Add more department</span>)
                        </p>
                        <br />
                        <p>
                            <label for="checkbox_selected_personnel">
                                <input type="checkbox" id="checkbox_selected_personnel" onchange="checkbox_selected_personnel_change();" />
                                <i class="fa fa-user" style="color: #999999"></i>Selected personnel
                            </label>
                            <div class="personnel tags"></div>
                            <input type="text" id="text_selected_personnel" style="width: 300px" />
                        </p>
                    </div>
                    <!-- end tab-privacy -->
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        "use strict";

        const TEXT_MAX_LENGTH_TITLE = 28;
        const TEXT_MAX_LENGTH_DESCRIPTION = 200;

        var model =
            {
                DepartmentList: null,
                ImageArray: ['/img/bg_survey_1a_big.png', '/img/bg_survey_2a_big.png', '/img/bg_survey_3a_big.png'],
                UseCustomImage: false,
                CustomImageDataUrl: null,
                Title: "",
                OptionalText: "TODO",
                TextColor: "#000000",
                StartDate: "",
                BackgroundFilter: false,
                PriorityOverrule: false,
                Status: 1,
                TargetedDepartmentIds: [],
                TargetedUserIds: [],
                Privacy:
                    {
                        Everyone: true,
                        Selected_Departments: false,
                        Selected_Personnel: false
                    }
            };

        var startDateString, endDateString;

        function checkbox_selected_everyone() {
            if ($("#checkbox_privacy").is(":checked")) // Privacy is everyone
            {
                // Update Model
                model.Privacy.Everyone = true;
                model.Privacy.Selected_Departments = false;
                model.Privacy.Selected_Personnel = false;

                // Update UI
                $("#checkbox_selected_departments").prop("checked", false);
                $("#checkbox_selected_personnel").prop("checked", false);
                $("#selectDepartmentLinkButton").css("color", "#C1C1C1");
                $("#selectDepartmentLinkButton").css("cursor", "default");
                $("#selectDepartmentLinkButton").prop('onclick', null).off('click');
            }
            else // Privacy is not everyone
            {
                // Update Model
                model.Privacy.Everyone = false;
            }
        }

        function checkbox_selected_personnel_change() {
            if ($("#checkbox_selected_personnel").is(":checked")) // Personnels is included in Privacy.
            {
                // Update Model
                model.Privacy.Everyone = false;
                model.Privacy.Selected_Personnel = true;

                // Update UI
                $("#checkbox_privacy").prop("checked", false);
            }
            else  // Personnels is not included in Privacy.
            {
                // Update Model
                model.Privacy.Selected_Personnel = false;
            }
        }

        function checkbox_selected_departments_change() {
            if ($("#checkbox_selected_departments").is(":checked")) // Departments is included in Privacy.
            {
                // Update Model
                model.Privacy.Everyone = false;
                model.Privacy.Selected_Departments = true;

                // Update UI
                $("#checkbox_privacy").prop("checked", false);
                $("#selectDepartmentLinkButton").css("color", "blue");
                $("#selectDepartmentLinkButton").css("cursor", "pointer");
                $("#selectDepartmentLinkButton").click(displayDepartmentSelector);
            }
            else // Departments is not included in Privacy.
            {
                // Update Model
                model.Privacy.Selected_Departments = false;

                // Update UI
                $("#selectDepartmentLinkButton").css("color", "#C1C1C1");
                $("#selectDepartmentLinkButton").css("cursor", "default");
                $("#selectDepartmentLinkButton").prop('onclick', null).off('click');
            }
        }

        function selectAllDepartments(cbSelectAll) {
            if (cbSelectAll.checked) {
                $("#department_checkboxlist input[type=checkbox]").prop("checked", true);
            }
            else {
                $("#department_checkboxlist input[type=checkbox]").prop("checked", false);
            }
        }

        function displayDepartmentSelector() {
            var companyId, adminUserId, ajaxPromise;
            companyId = $("#main_content_hfCompanyId").val();
            adminUserId = $("#main_content_hfAdminUserId").val();

            // Ajax call to fetch 
            ajaxPromise = jQuery.ajax({
                type: "GET",
                url: "/api/Company?userid=" + adminUserId + "&companyid=" + companyId,
                contentType: "application/json; charset=utf-8",
                data: {},
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        model.DepartmentList = d.Departments;
                    }
                },
                error: function (xhr, status, error) {
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    // Render department

                    var idx, departmentObj, html, checked;
                    $("#department_checkboxlist").children().remove();
                    for (idx = 0; idx < model.DepartmentList.length; idx++) {
                        checked = "";
                        if (model.TargetedDepartmentIds.indexOf(model.DepartmentList[idx].Id) >= 0) {
                            checked = "checked=\"checked\"";
                        }

                        html = "<label><input type=\"checkbox\" " + checked + " value=\"" + model.DepartmentList[idx].Id + "\" />" + model.DepartmentList[idx].Title + "</label><br />";

                        $("#department_checkboxlist").append(html);
                    }

                    HideProgressBar();
                    $("#mpe_backgroundElement").show();
                    $("#plSelectDepartment").show();
                }
            });
        }

        function hideDepartmentSelector() {
            $("#mpe_backgroundElement").hide();
            $("#plSelectDepartment").hide();
            $("#checkbox_selected_departments").prop("checked", model.TargetedDepartmentIds.length > 0);
        }

        function saveDepartmentSelection() {
            var idx, $checkedItems = [], html;

            // Clear targetedDepartmentIds 
            model.TargetedDepartmentIds = [];
            $(".department.tags").children().remove();

            // Re-populate targetedDepartmentIds with selected department id
            $checkedItems = $("#plSelectDepartment :checked");
            for (idx = 0; idx < $checkedItems.length; idx++) {
                model.TargetedDepartmentIds.push($checkedItems[idx].value);
                html = "";
                html = html + "<div class=\"tag " + $checkedItems[idx].value + "\">";
                html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + $($checkedItems[idx]).parent().text() + "</span>";
                html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + $checkedItems[idx].value + "');\">x</a>";
                html = html + "</div>";
                $(".department.tags").append(html);
            }
            hideDepartmentSelector();
        }

        function removeDepartment(departmentId) {
            var foundIndex = model.TargetedDepartmentIds.indexOf(departmentId);

            if (foundIndex >= 0) {
                // Update Model
                model.TargetedDepartmentIds.splice(foundIndex, 1);

                // Update UI
                $(".department.tags .tag." + departmentId).remove();
            }

            $("#checkbox_selected_departments").prop("checked", model.TargetedDepartmentIds.length > 0);
            $("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
        }

        function removePersonnel(personnelId) {
            var foundIndex = model.TargetedUserIds.indexOf(personnelId);

            if (foundIndex >= 0) {
                // Update Model
                model.TargetedUserIds.splice(foundIndex, 1);

                // Update UI
                $(".personnel.tags .tag." + personnelId).remove();
            }

            $("#checkbox_selected_personnel").prop("checked", model.TargetedUserIds.length > 0);
            //var everyoneChecked = !($("#checkbox_selected_departments").prop("checked") || $("#checkbox_selected_personnel").prop("checked"));
            //$("#checkbox_privacy").prop("checked", !$("#checkbox_selected_personnel").prop("checked"));
        }

        // Privacy:Personnel-related functions
        function setPriorityOverrule(input) {
            model.PriorityOverrule = input.checked;
        }

        function removeAttachedImg(input) {

            model.UseCustomImage = false;
            model.CustomImageDataUrl = null;

            $("#pulseCard").css("backgroundImage", "none");

            // hide mini-x
            $(".remove-image-icon").hide();

            // show attach image icon
            $(".upload-option-image").show();
        }

        function displayCardPreviewImage(input, maxWidth, maxHeight, cardId, optionId) {
            var image = new Image();

            if (input.files && input.files[0]) {
                var t = input.files[0].type;
                var s = input.files[0].size / (1024 * 1024);
                if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
                    if (s > 5) {
                        toastr.error('Uploaded photo exceeded 5MB.');
                    } else {
                        var fileReader = new FileReader();
                        fileReader.addEventListener("load", function () {
                            var image = new Image();
                            image.src = this.result;

                            model.UseCustomImage = true;
                            model.CustomImageDataUrl = {
                                base64: this.result,
                                extension: $("#selected_background").val().substr($("#selected_background").val().lastIndexOf(".") + 1),
                                width: image.width,
                                height: image.height
                            };


                            $("#pulseCard").css("backgroundImage", "url(" + this.result + ")");

                            // display mini-x
                            $(".remove-image-icon").show();

                            // hide attach image icon
                            $(".upload-option-image").hide();

                        }, false);
                        fileReader.readAsDataURL(input.files[0]);
                    }
                } else {
                    toastr.error('File extension is invalid.');
                }
            }
        } // end function displayCardPreviewImage(input, maxWidth, maxHeight) {

        function validateDate(input) {
            input.className = input.className.replace("invalid-field", "");
            if (!moment(input.value, "DD/MM/YYYY", true).isValid()) {
                input.className = "invalid-field";
            }
        }
        function validateHour(input) {
            input.className = input.className.replace("invalid-field", "");

            if (!moment(input.value, "h", true).isValid()) {
                input.className = "invalid-field";
            }
        }
        function validateMinute(input) {
            input.className = input.className.replace("invalid-field", "");

            if (!moment(input.value, "m", true).isValid()) {
                input.className = "invalid-field";
            }
        }

        function loadCardInfo() {

            var pj, html, idx, record;

            // Update local Javascript model
            if ($("#main_content_hfPulse").val().trim().length > 0) {
                pj = JSON.parse($("#main_content_hfPulse").val().trim());

                $(".pulse_action").html("Edit");

                model.Actions = pj.Actions;
                model.BackgroundUrl = pj.BackgroundUrl;
                model.CardType = pj.CardType;
                model.Description = pj.Description;
                model.EndDate = pj.EndDate; // *

                model.IsPrioritized = pj.IsPrioritized;
                model.ProgressStatus = pj.ProgressStatus;
                model.PulseId = pj.PulseId;
                model.PulseType = pj.PulseType;
                model.StartDate = pj.StartDate; // *
                model.Status = pj.Status;
                model.TextColor = pj.TextColor;
                model.Title = pj.Title;

                if (model.StartDate.length > 0) {
                    model.StartDate = moment.utc(moment(model.StartDate).format("DD/MM/YYYY hh:mm a"), "DD/MM/YYYY hh:mm a");
                }
                if (model.EndDate) {
                    model.EndDate = moment.utc(moment(model.EndDate).format("DD/MM/YYYY hh:mm a"), "DD/MM/YYYY hh:mm a");
                }

                // update model's privacy
                if (pj.TargetedDepartments.length > 0) {
                    for (idx = 0; idx < pj.TargetedDepartments.length; idx++) {
                        record = pj.TargetedDepartments[idx];
                        model.TargetedDepartmentIds.push(record.Id);
                        html = "";
                        html = html + "<div class=\"tag " + record.Id + "\">";
                        html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + record.Title + "</span>";
                        html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + record.Id + "');\">x</a>";
                        html = html + "</div>";
                        $(".department.tags").append(html);
                    }

                    $("#selectDepartmentLinkButton").css("color", "blue");
                    $("#selectDepartmentLinkButton").css("cursor", "pointer");
                    $("#selectDepartmentLinkButton").click(displayDepartmentSelector);
                }
                $("#checkbox_selected_departments").prop("checked", model.TargetedDepartmentIds.length > 0);

                if (pj.TargetedUsers.length > 0) {
                    for (idx = 0; idx < pj.TargetedUsers.length; idx++) {
                        record = pj.TargetedUsers[idx];
                        model.TargetedUserIds.push(record.UserId);
                        html = "";
                        html = html + "<div class=\"tag " + record.UserId + "\">";
                        html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + record.FirstName + " " + record.LastName + "(" + record.Email + ")" + "</span>";
                        html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + record.UserId + "');\">x</a>";
                        html = html + "</div>";
                        $(".personnel.tags").append(html);
                    }
                }
                $("#checkbox_selected_personnel").prop("checked", model.TargetedUserIds.length > 0);

                $("#checkbox_privacy").prop("checked", (!($("#checkbox_selected_personnel").prop("checked") || $("#checkbox_selected_departments").prop("checked"))));

                $("#button_update").show();
            } else {
                $("#button_create").show();
            }

            // Update Pulse Card UI
            $("#pulseCardTitle").val(model.Title);
            if (model.Title == null || model.Title == undefined) {
                $("#titleCounter").html(TEXT_MAX_LENGTH_TITLE);
            }
            else {
                $("#titleCounter").html(TEXT_MAX_LENGTH_TITLE - model.Title.length);
            }

            $("#pulseCardText").val(model.Description);
            if (model.Description == null || model.Description == undefined) {
                $("#descriptionCounter").html(TEXT_MAX_LENGTH_DESCRIPTION);
            }
            else {
                $("#descriptionCounter").html(TEXT_MAX_LENGTH_DESCRIPTION - model.Description.length);
            }

            var startDate_moment, endDate_moment;
            if (model.StartDate != null && model.StartDate != "") {
                startDate_moment = moment.utc(model.StartDate, "DD/MM/YYYY hh:mm a");
                $("#start_date").val(startDate_moment.format("DD/MM/YYYY"));
                $("#start_hh").val(startDate_moment.format("hh"));
                $("#start_mm").val(startDate_moment.format("mm"));
                $("#start_meridiem").val(startDate_moment.format("a"));
            }

            if (model.EndDate) {
                $(".end_date.specific_date").prop("checked", true)
                endDate_moment = moment.utc(model.EndDate, "DD/MM/YYYY hh:mm a");
                $("#end_date").val(endDate_moment.format("DD/MM/YYYY"));
                $("#end_hh").val(endDate_moment.format("hh"));
                $("#end_mm").val(endDate_moment.format("mm"));
                $("#end_meridiem").val(endDate_moment.format("a"));
            } else {
                $(".end_date.forever").prop("checked", true)
            }
            $("#select_status").val(model.Status);

            $(".text-colour").removeClass("selected");
            if (model.TextColor === "#000000") {
                $(".text-colour.black-text").addClass("selected");
            } else {
                $(".text-colour.white-text").addClass("selected");
            }

            $("#checkbox_priorityOverrule").prop("checked", model.IsPrioritized);

            if ((model.BackgroundUrl) && (model.BackgroundUrl.length > 0)) {
                $("#pulseCard").css("backgroundImage", "url(" + model.BackgroundUrl + ")");
                $(".upload-option-image").hide();
                $(".remove-image-icon").show();
            }
        }

        function isValidForm() {
            // Title
            if ($("#pulseCardTitle").val().trim().length == 0) {
                ShowToast('Please give this a title', 2);
                return false;
            }

            // Description
            // description is not compulsory. 2017/02/10
            //if ($("#pulseCardText").val().trim().length == 0) {
            //    ShowToast('Please enter a description', 2);
            //    return false;
            //}

            // Srart date
            var momentSDate;
            startDateString = $("#start_date").val() + " " + $("#start_hh").val() + ":" + $("#start_mm").val() + " " + $("#start_meridiem").val();
            if (!moment(startDateString, "DD/MM/YYYY hh:mm a", true).isValid()) {
                startDateString = null;
                ShowToast('Please enter a valid start date', 2);
                return false;
            }
            else {
                momentSDate = moment.utc(startDateString, "DD/MM/YYYY hh:mm a");
            }

            // End date
            var momentEDate;
            if ($(".end_date:checked").val() === "specific_date") {
                endDateString = $("#end_date").val() + " " + $("#end_hh").val() + ":" + $("#end_mm").val() + " " + $("#end_meridiem").val();
                if (!moment(endDateString, "DD/MM/YYYY hh:mm a", true).isValid()) {
                    endDateString = null;
                    ShowToast('Please enter a valid end date', 2);
                    return false;
                }
                else {
                    momentEDate = moment.utc(endDateString, "DD/MM/YYYY hh:mm a");
                    if (momentEDate.isBefore(momentSDate) || momentEDate.isSame(momentSDate)) {
                        endDateString = null;
                        ShowToast('Your date range is invalid', 2);
                        return false;
                    }
                }
            }

            // Privacy
            if (!$("#checkbox_privacy").is(":checked") && !$("#checkbox_selected_departments").is(":checked") && !$("#checkbox_selected_personnel").is(":checked")) {
                ShowToast('Please set a privacy setting', 2);
                return false;
            }

            if ($("#checkbox_selected_departments").is(":checked")) // Departments for Privacy
            {
                if (model.TargetedDepartmentIds.length == 0) {
                    ShowToast('Please select at least one department', 2);
                    return false;
                }
            }

            if ($("#checkbox_selected_personnel").is(":checked")) // Personnels for Privacy
            {
                if (model.TargetedUserIds.length == 0) {
                    ShowToast('Please select at least one personnel', 2);
                    return false;
                }
            }

            return true;
        }

        (function ($) {
            $(document).ready(function () {

                $("#start_date").datepicker({
                    beforeShow: function () {
                        setTimeout(function () {
                            $('.ui-datepicker').css('z-index', 99999);
                        }, 0);
                    },
                    dateFormat: 'dd/mm/yy'
                });
                $("#end_date").datepicker({
                    beforeShow: function () {
                        setTimeout(function () {
                            $('.ui-datepicker').css('z-index', 99999);
                        }, 0);
                    },
                    dateFormat: 'dd/mm/yy', defaultDate: +7
                });

                $('textarea[maxlength]').keyup(function () {
                    var limit = parseInt($(this).attr('maxlength'));
                    var text = $(this).val();
                    var chars = text.length;
                    //if (chars > limit) {
                    //    var new_text = text.substr(0, limit);
                    //    $(this).val(new_text);
                    //}
                });

                $("#text_selected_personnel").autocomplete({
                    source: "/api/personnel?companyid=" + $("#main_content_hfCompanyId").val() + "&userid=" + $("#main_content_hfAdminUserId").val(),
                    minLength: 1,
                    response: function (event, ui) {
                        for (var i = 0; i < ui.content.length; i++) {
                            if (model.TargetedUserIds.indexOf(ui.content[i].value) != -1) {
                                ui.content.splice(i, 1);
                                if (i != ui.content.length - 1) {
                                    i--;
                                }
                            }
                        }
                    },
                    select: function (event, ui) {
                        var html;
                        if (model.TargetedUserIds.indexOf(ui.item.value) == -1) {
                            model.TargetedUserIds.push(ui.item.value);

                            html = "";
                            html = html + "<div class=\"tag " + ui.item.value + "\">";
                            html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + ui.item.label + "</span>";
                            html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + ui.item.value + "');\">x</a>";
                            html = html + "</div>";
                            $(".personnel.tags").append(html);

                            $("#checkbox_selected_personnel").prop("checked", model.TargetedUserIds.length > 0);
                            var everyoneChecked = !($("#checkbox_selected_departments").prop("checked") || $("#checkbox_selected_personnel").prop("checked"));
                            $("#checkbox_privacy").prop("checked", everyoneChecked);
                        }
                        return false;
                    },
                    focus: function (event, ui) {
                        event.preventDefault();
                    },
                    close: function (event, ui) {
                        $("#text_selected_personnel").val("");
                    }
                });

                $("#button_create").click(function () {
                    // Check input
                    if (!isValidForm()) {
                        return;
                    }

                    // Make ajax call 
                    var ajaxData;
                    ajaxData = {
                        'pulseId': $("#main_content_hfPulseId").val(),
                        'adminUserId': $("#main_content_hfAdminUserId").val(),
                        'companyId': $("#main_content_hfCompanyId").val(),
                        'title': $("#pulseCardTitle").val(),
                        'description': $("#pulseCardText").val(),
                        'status': $("#select_status").val(),
                        'startDate': $("#start_date").val() + " " + $("#start_hh").val() + ":" + $("#start_mm").val() + " " + $("#start_meridiem").val(),
                        'endDate': $("#end_date").val() + " " + $("#end_hh").val() + ":" + $("#end_mm").val() + " " + $("#end_meridiem").val(),
                        'isPrioritized': model.PriorityOverrule,
                        'targetedDepartmentIds': model.TargetedDepartmentIds,
                        'targetedUserIds': model.TargetedUserIds,
                        'useCustomImage': model.UseCustomImage,
                        'customImageDataUrl': model.CustomImageDataUrl
                    }

                    // for timezone
                    var timezone = parseFloat($("#main_content_hfTimezone").val());
                    if (!moment(ajaxData.startDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.startDate = null;
                    } else {
                        ajaxData.startDate = moment(ajaxData.startDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
                    }

                    if (!moment(ajaxData.endDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.endDate = null;
                    } else {
                        ajaxData.endDate = moment(ajaxData.endDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
                    }

                    if ($(".end_date:checked").val() === "forever") {
                        ajaxData.endDate = null;
                    }

                    // targeted departments
                    if (!$("#checkbox_selected_departments").is(":checked")) {
                        ajaxData.targetedDepartmentIds = null;
                    }

                    // targeted personnel
                    if (!$("#checkbox_selected_personnel").is(":checked")) {
                        ajaxData.targetedUserIds = null;
                    }

                    jQuery.ajax({
                        type: "PUT",
                        url: "/api/PulseAnnouncementCard",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(ajaxData),
                        dataType: "json",
                        beforeSend: function (xhr, settings) {
                            ShowProgressBar();
                        },
                        success: function (d, status, xhr) {
                            if (d.Success) {
                                ShowToast("Pulse added", 1);
                                RedirectPage('/DynamicPulse/AnnouncementPulseFeedList', 300);
                            } else {
                                ShowToast(d.ErrorMessage, 2);
                            }
                        },
                        error: function (xhr, status, error) {
                            ShowToast(error, 2);
                        },
                        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                            HideProgressBar();
                        }
                    });
                });

                $("#button_update").click(function () {
                    // Check input
                    if (!isValidForm()) {
                        return;
                    }

                    // Make ajax call 
                    var ajaxData;
                    ajaxData = {
                        'pulseId': $("#main_content_hfPulseId").val(),
                        'adminUserId': $("#main_content_hfAdminUserId").val(),
                        'companyId': $("#main_content_hfCompanyId").val(),
                        'title': $("#pulseCardTitle").val(),
                        'isFilterApplied': model.IsAppliedFilter,
                        'textColor': model.TextColor,
                        'backgroundUrl': model.BackgroundUrl,
                        'description': $("#pulseCardText").val(),
                        'status': $("#select_status").val(),
                        'startDate': $("#start_date").val() + " " + $("#start_hh").val() + ":" + $("#start_mm").val() + " " + $("#start_meridiem").val(),
                        'endDate': $("#end_date").val() + " " + $("#end_hh").val() + ":" + $("#end_mm").val() + " " + $("#end_meridiem").val(),
                        'isPrioritized': model.PriorityOverrule,
                        'targetedDepartmentIds': model.TargetedDepartmentIds,
                        'targetedUserIds': model.TargetedUserIds,
                        'useCustomImage': model.UseCustomImage,
                        'customImageDataUrl': model.CustomImageDataUrl
                    }

                    // for timezone
                    var timezone = parseFloat($("#main_content_hfTimezone").val());
                    if (!moment(ajaxData.startDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.startDate = null;
                    } else {
                        ajaxData.startDate = moment.utc(moment(ajaxData.startDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss"), "YYYY-MM-DD HH:mm:ss");
                    }

                    if (!moment(ajaxData.endDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.endDate = null;
                    } else {
                        ajaxData.endDate = moment.utc(moment(ajaxData.endDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss"), "YYYY-MM-DD HH:mm:ss");
                    }

                    if ($(".end_date:checked").val() === "forever") {
                        ajaxData.endDate = null;
                    }

                    // targeted departments
                    if (!$("#checkbox_selected_departments").is(":checked")) {
                        ajaxData.targetedDepartmentIds = null;
                    }

                    // targeted personnel
                    if (!$("#checkbox_selected_personnel").is(":checked")) {
                        ajaxData.targetedUserIds = null;
                    }

                    jQuery.ajax({
                        type: "POST",
                        url: "/api/PulseAnnouncementCard",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(ajaxData),
                        dataType: "json",
                        beforeSend: function (xhr, settings) {
                            ShowProgressBar();
                        },
                        success: function (d, status, xhr) {
                            if (d.Success) {
                                ShowToast("Pulse updated", 1);
                                RedirectPage('/DynamicPulse/AnnouncementPulseFeedList', 300);
                            } else {
                                ShowToast(d.ErrorMessage, 2);
                            }
                        },
                        error: function (xhr, status, error) {
                            ShowToast(error, 2);
                        },
                        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                            HideProgressBar();
                        }
                    });
                });

                // Set publisgh start date
                var publish_datetime;
                publish_datetime = moment();
                $("#start_date").val(publish_datetime.format("DD/MM/YYYY"));
                $("#start_hh").val(publish_datetime.format("hh"));
                $("#start_mm").val(publish_datetime.format("mm"));
                $("#start_meridiem").val(publish_datetime.format("a"));

                // hfPulse
                loadCardInfo();

            });
        }(jQuery));
    </script>
</asp:Content>
