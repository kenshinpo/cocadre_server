﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ClarityPulse.aspx.cs" Inherits="AdminWebsite.DynamicPulse.ClarityPulse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="/css/croppie.css" />
    <script src="/js/croppie.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

    <!-- Crop Image -->
    <div id="popup_crop_image" class="popup" style="left: 50%; top: 50%; height: auto; overflow: auto; position: absolute; z-index: 9001; margin: 0 auto; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: none;">
        <h1 class="popup__title">Crop image</h1>
        <div class="popup__content">
            <div class="container" style="width: 870px;">
                <div class="photocrop">
                    <div id="uploadCrop"></div>
                </div>
            </div>
            <div class="label">
            </div>
            <p class="error">
            </p>
        </div>
        <div class="popup__action">
            <div id="btnCrop" class="popup__action__item popup__action__item--cta">CROP & SAVE</div>
            <div id="btnCancelCrop" class="popup__action__item popup__action__item--others">Cancel</div>
        </div>
    </div>
    <!-- / Crop Image -->

    <div id="plSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Select Department</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <input id="selectAllDepartment" type="checkbox" onclick="selectAllDepartments(this);" />
                        <label for="selectAllDepartment">Select All</label>
                    </div>
                    <hr />
                    <div class="accessrights">
                        <span id="department_checkboxlist">
                            <label>
                                <input type="checkbox" value="D04461505c52d44d8be0ff0d80c6a2408" />Human Resource
                            </label>
                            <br />
                        </span>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:saveDepartmentSelection()">Select</a>
            <a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDepartmentSelector()">Cancel</a>
        </div>
    </div>

    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }

        .invalid-field {
            border-bottom-color: #ff0000 !important;
        }

        .grey-text {
            color: #ccc;
        }

        .chevron:hover {
            color: #839423 !important;
            cursor: pointer;
        }

        .text-colour {
            width: 25px;
            height: 25px;
            border: solid 2px #ccc;
            display: inline-block;
        }

            .text-colour.selected {
                border: solid 2px blue !important;
            }

        .ios-toggle,
        .ios-toggle:active {
            position: absolute;
            top: -5000px;
            height: 0;
            width: 0;
            opacity: 0;
            border: none;
            outline: none;
        }

        .checkbox-label {
            display: block;
            position: relative;
            padding: 10px;
            margin-bottom: 20px;
            font-size: 12px;
            line-height: 16px;
            width: 100%;
            height: 36px;
            /*border-radius*/
            -webkit-border-radius: 18px;
            -moz-border-radius: 18px;
            border-radius: 18px;
            background: #f8f8f8;
            cursor: pointer;
        }

            .checkbox-label:before {
                content: '';
                display: block;
                position: absolute;
                z-index: 1;
                line-height: 34px;
                text-indent: 40px;
                height: 36px;
                width: 36px;
                /*border-radius*/
                -webkit-border-radius: 100%;
                -moz-border-radius: 100%;
                border-radius: 100%;
                top: 0px;
                left: 0px;
                right: auto;
                background: white;
                /*box-shadow*/
                -webkit-box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
                -moz-box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
                box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
            }

            .checkbox-label:after {
                content: attr(data-off);
                display: block;
                position: absolute;
                z-index: 0;
                top: 0;
                left: -300px;
                padding: 10px;
                height: 100%;
                width: 300px;
                text-align: right;
                color: #bfbfbf;
                white-space: nowrap;
            }

        .ios-toggle:checked + .checkbox-label {
            /*box-shadow*/
            -webkit-box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
            -moz-box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
            box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
        }

            .ios-toggle:checked + .checkbox-label:before {
                left: calc(100% - 36px);
                /*box-shadow*/
                -webkit-box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
                -moz-box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
                box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
            }

            .ios-toggle:checked + .checkbox-label:after {
                content: attr(data-on);
                left: 60px;
                width: 36px;
            }

        .onoffswitch {
            position: relative;
            width: 76px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }

        .onoffswitch-checkbox {
            display: none;
        }

        .onoffswitch-label {
            display: block;
            overflow: hidden;
            cursor: pointer;
            border-radius: 20px;
        }

        .onoffswitch-inner {
            display: block;
            width: 200%;
            margin-left: -100%;
            transition: margin 0.3s ease-in 0s;
        }

            .onoffswitch-inner:before, .onoffswitch-inner:after {
                display: block;
                float: left;
                width: 50%;
                height: 30px;
                padding: 0;
                line-height: 30px;
                font-size: 13px;
                color: white;
                font-weight: bold;
                box-sizing: border-box;
            }

            .onoffswitch-inner:before {
                content: "YES";
                padding-left: 10px;
                background-color: #709EEB;
                color: #F2F2F2;
            }

            .onoffswitch-inner:after {
                content: "NO";
                padding-right: 10px;
                background-color: #4d4d4d;
                color: #F2F2F2;
                text-align: right;
            }

        .onoffswitch-switch {
            display: block;
            width: 26px;
            height: 26px;
            margin: 2px;
            background: #F2F2F2;
            position: absolute;
            bottom: 0px;
            right: 46px;
            border-radius: 20px;
            transition: all 0.3s ease-in 0s;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
            margin-left: 0;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
            right: 0px;
        }

        .text-placeholder::-webkit-input-placeholder {
            color: rgba(176, 176, 176, 1);
        }

        .text-placeholder:-moz-placeholder { /* Firefox 18- */
            color: rgba(176, 176, 176, 1);
        }

        .text-placeholder::-moz-placeholder { /* Firefox 19+ */
            color: rgba(176, 176, 176, 1);
        }

        .text-placeholder:-ms-input-placeholder {
            color: rgba(176, 176, 176, 1);
        }
    </style>

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTimezone" />
    <asp:HiddenField runat="server" ID="hfPulseId" />
    <asp:HiddenField runat="server" ID="hfPulse" />
    <asp:HiddenField runat="server" ID="hfBackgroundUrls" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/DynamicPulse/ClarityPulseFeedList" style="color: #000;">Dynamic <span>pulse</span></a></div>
        <div class="appbar__meta">
            <a href="/DynamicPulse/ClarityPulseFeedList">Clarity Pulse</a>
        </div>
        <div class="appbar__meta">
            <span class="pulse_action" style="color: gray;">Add 'Clarity' Pulse</span>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">

        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item"><a class="data__sidebar__link" href="/DynamicPulse/AnnouncementPulseFeedList">Announcement Pulse</a></li>
                <li class="data__sidebar__list__item"><a class="data__sidebar__link" href="/DynamicPulse/ResponsivePulseFeedList">Responsive Pulse</a></li>
                <li class="data__sidebar__list__item"><a class="data__sidebar__link data__sidebar__link--active">Clarity Pulse</a></li>
            </ul>
        </aside>

        <div class="data__content">
            <div style="margin: 30px;">
                <div>
                    <h2><span class="pulse_action">Add</span> 'Clarity' Pulse</h2>
                    <div>
                        <asp:LinkButton ID="lbViewAnalytics" runat="server" CssClass="btn secondary" Text="View Analytics" OnClick="lbViewAnalytics_Click" Style="float: right; width: auto;" Visible="false" />
                    </div>
                </div>
                <p>
                    Manually generate an 'Clarity' pulse card on the CoCadre APP.<br />
                    'Clarity' pulse cannot be deleted and will re-appear again within the Time Schedule.
                </p>
                <br />
                <div class="tabs tabs--styled" style="min-width: 1000px;">
                    <ul class='tabs__list'>
                        <li class='tabs__list__item'><a class="tabs__link" href="#tab-card-info">Card Info</a></li>
                        <li class='tabs__list__item'><a class="tabs__link" href="#tab-privacy">Privacy</a></li>
                    </ul>
                    <div id="tab-card-info" style="margin-top: 20px;">
                        <div class="grid__inner" style="background: 0;">
                            <div class="grid__span--9">
                                <label for="text_title" style="font-size: 14pt; font-weight: bold; width: auto; margin-right: 20px; float: left;">Title</label>
                                <div id="div_title" style="border-width: 0px 0px 1px; border-style: solid; border-color: #DDD; border-image: none; width: 80%; margin-left: 60px; position: relative;">
                                    <input id="text_title" class="text-placeholder" style="border: currentColor; border-image: none; width: 97%;" onkeyup="textCounter(this, 'titleCounter', titleCounterMax);" type="text" placeholder="Enter your title (Reference purpose only)" />
                                    <span id="titleCounter" style="right: 5px; bottom: 15px; color: #DDD; position: absolute;">50</span>
                                </div>

                                <div class="grid__inner pulse-card" style="background: 0; margin-top: 30px;">
                                    <div class="grid__span--1 chevron back" style="margin-top: 120px; font-size: 7em; color: blue; text-align: right; margin-right: 0;">&lsaquo; </div>

                                    <div class="grid__span--10" style="margin-right: 10px;">
                                        <div id="pulseCard" style="height: 361.5px; width: 585px; border: 2px solid #ccc; border-radius: 2em; background-image: none; background-repeat: no-repeat; background-position: center; margin: 0 auto; background-size: contain;">
                                            <div id="pulseBgFilter"
                                                style="width: 100%; height: 100%; border-radius: 2em;">
                                                <div class="remove-image-icon" style="display: none; height: 20px; width: 20px; background-color: rgba(0, 0, 0, 0.6); color: white; text-align: center; border-radius: 2em; float: right; margin: 1em 1em 0 0;" onclick="removeAttachedImg(this);"><i class="fa fa-times" aria-hidden="true"></i></div>

                                                <textarea id="pulseCardText" style="resize: none; overflow: no-display; overflow-wrap: break-word; display: block; width: 500px; min-height: 80px; height: auto; background-color: transparent; font-size: 14pt; position: relative; top: 240px; margin: 0 auto; color: #000000; border: 1px solid #ccc; border-radius: .5em;" placeholder="(Optional text)"></textarea>

                                                <label class="upload-option-image" style="position: relative; top: 100px; margin: 0 auto; left: 0; text-align: center;">
                                                    <input type="file" id="selected_background" accept="image/*" title="Add image" style="display: none" />
                                                </label>
                                            </div>
                                        </div>
                                        <p>
                                            &nbsp;
                                        </p>

                                        <p style="color: #999;">
                                            Click on arrow to change background.<br />
                                        </p>
                                        <p>
                                            &nbsp;
                                        </p>
                                        <h3>Publish method</h3>
                                        <h5>Scheduled</h5>
                                        <div>
                                            <span style="display: inline;">
                                                <label style="width: 100px; display: inline;">Start date</label>
                                                <input type="text" id="start_date" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" />
                                                at
                                                <input type="text" id="start_hh" maxlength="2" style="width: 4em; display: inline;" placeholder="hh" onchange="validateHour(this);" />:
                                                <input type="text" id="start_mm" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" />
                                                <select id="start_meridiem" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px;">
                                                    <option value="am">am</option>
                                                    <option value="pm">pm</option>
                                                </select>
                                            </span>
                                        </div>

                                        <div>
                                            <span style="width: 100px; display: inline;">End date</span>
                                            <label style="display: inline; margin-left: 10px;">
                                                <input type="radio" name="end_date" class="end_date forever" value="forever" checked="checked" />
                                                Forever
                                            </label>
                                            <br />
                                            <label style="display: inline; margin-left: 66px;">
                                                <input type="radio" name="end_date" class="end_date specific_date" value="specific_date" />
                                                <span style="display: inline;">
                                                    <input type="text" id="end_date" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" />
                                                    at
                                                    <input type="text" id="end_hh" maxlength="2" style="width: 4em; display: inline;" placeholder="hh" onchange="validateHour(this);" />:
                                                    <input type="text" id="end_mm" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" />
                                                    <select id="end_meridiem" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px;">
                                                        <option value="am">am</option>
                                                        <option value="pm">pm</option>
                                                    </select>
                                                </span>
                                            </label>
                                        </div>

                                        <div>
                                            <span style="display: inline;">
                                                <label style="width: 100px; display: inline;">Status</label>
                                                <select id="select_status" style="width: 12em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px; margin-left: 10px;">
                                                    <option value="1">Unlisted</option>
                                                    <option value="2">Active</option>
                                                    <option value="3">Hidden</option>
                                                </select>
                                            </span>
                                        </div>

                                        <p>
                                            <a href="/DynamicPulse/ClarityPulseFeedList" class="btn">CANCEL</a>
                                            <button type="button" class="btn" id="button_create" style="display: none;">CREATE</button>
                                            <button type="button" class="btn" id="button_update" style="display: none;">UPDATE</button>
                                        </p>

                                    </div>

                                    <div class="grid__span--1 chevron forward" style="margin-top: 120px; font-size: 7em; color: blue; text-align: left;">&rsaquo; </div>
                                </div>
                            </div>
                            <div class="grid__span--3">
                                <div>
                                    <label style="font-weight: bold; font-size: 14pt;">
                                        Background Filter
                                    </label>
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="checkbox_backgroundFilter" style="display: none" onchange="setBackgroundFilter(this);" />
                                        <label class="onoffswitch-label" for="checkbox_backgroundFilter">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>

                                <div style="display: inline-block; margin-top: 10px;">
                                    <label style="font-weight: bold; font-size: 14pt;">
                                        Text Colour
                                    </label>
                                    <div class="black-text text-colour selected" data-color="#000000" style="background-color: #000000;" onclick="setTextColor(this);"></div>
                                    <div class="white-text text-colour" data-color="#ffffff" style="background-color: #ffffff;" onclick="setTextColor(this);"></div>

                                </div>
                            </div>
                        </div>
                        <!-- content -->
                    </div>


                    <div id="tab-privacy">
                        <p>
                            <label for="checkbox_privacy" style="margin-top: 15px;">
                                <input type="checkbox" id="checkbox_privacy" onchange="checkbox_selected_everyone();" checked="checked" />
                                <i class="fa fa-users" style="color: #999999"></i>Everyone
                            </label>
                        </p>
                        <hr />
                        <p>
                            <label for="checkbox_selected_departments">
                                <input type="checkbox" id="checkbox_selected_departments" onchange="checkbox_selected_departments_change();" />
                                <i class="fa fa-briefcase" style="color: #999999"></i>Selected departments
                            </label>
                            <div class="department tags"></div>
                            (<span id="selectDepartmentLinkButton" style="color: #C1C1C1; cursor: default;">+ Add more department</span>)
                        </p>
                        <p>
                            <label for="checkbox_selected_personnel">
                                <input type="checkbox" id="checkbox_selected_personnel" onchange="checkbox_selected_personnel_change();" />
                                <i class="fa fa-user" style="color: #999999"></i>Selected personnel
                            </label>
                            <div class="personnel tags"></div>
                            <input type="text" id="text_selected_personnel" style="width: 300px" />
                        </p>
                    </div>
                    <!-- end tab-privacy -->
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        "use strict";

        var model =
            {
                DepartmentList: null,
                ImageArray: JSON.parse($("#main_content_hfBackgroundUrls").val()),
                UseCustomImage: false,
                CustomImageDataUrl: null,
                Title: "",
                OptionalText: "TODO",
                TextColor: "#000000",
                StartDate: "",
                IsAppliedFilter: false,
                PriorityOverrule: false,
                Status: 1,
                TargetedDepartmentIds: [],
                TargetedUserIds: [],
                Privacy:
                    {
                        Everyone: true,
                        Selected_Departments: false,
                        Selected_Personnel: false
                    }
            };

        var startDateString, endDateString;
        var IMAGE_MAX_SIZE = 2; // unit: MB
        var IMAGE_WIDTH = 390;
        var IMAGE_HEIGHT = 241;
        var imageBase64, imageExtension;
        var titleCounterMax = 50;

        function checkbox_selected_everyone() {
            if ($("#checkbox_privacy").is(":checked")) // Privacy is everyone
            {
                // Update Model
                model.Privacy.Everyone = true;
                model.Privacy.Selected_Departments = false;
                model.Privacy.Selected_Personnel = false;

                // Update UI
                $("#checkbox_selected_departments").prop("checked", false);
                $("#checkbox_selected_personnel").prop("checked", false);
                $("#selectDepartmentLinkButton").css("color", "#C1C1C1");
                $("#selectDepartmentLinkButton").css("cursor", "default");
                $("#selectDepartmentLinkButton").prop('onclick', null).off('click');
            }
            else // Privacy is not everyone
            {
                // Update Model
                model.Privacy.Everyone = false;
            }
        }

        function checkbox_selected_personnel_change() {
            if ($("#checkbox_selected_personnel").is(":checked")) // Personnels is included in Privacy.
            {
                // Update Model
                model.Privacy.Everyone = false;
                model.Privacy.Selected_Personnel = true;

                // Update UI
                $("#checkbox_privacy").prop("checked", false);
            }
            else  // Personnels is not included in Privacy.
            {
                // Update Model
                model.Privacy.Selected_Personnel = false;
            }
        }

        function checkbox_selected_departments_change() {
            if ($("#checkbox_selected_departments").is(":checked")) // Departments is included in Privacy.
            {
                // Update Model
                model.Privacy.Everyone = false;
                model.Privacy.Selected_Departments = true;

                // Update UI
                $("#checkbox_privacy").prop("checked", false);
                $("#selectDepartmentLinkButton").css("color", "blue");
                $("#selectDepartmentLinkButton").css("cursor", "pointer");
                $("#selectDepartmentLinkButton").click(displayDepartmentSelector);
            }
            else // Departments is not included in Privacy.
            {
                // Update Model
                model.Privacy.Selected_Departments = false;

                // Update UI
                $("#selectDepartmentLinkButton").css("color", "#C1C1C1");
                $("#selectDepartmentLinkButton").css("cursor", "default");
                $("#selectDepartmentLinkButton").prop('onclick', null).off('click');
            }
        }

        function selectAllDepartments(cbSelectAll) {
            if (cbSelectAll.checked) {
                $("#department_checkboxlist input[type=checkbox]").prop("checked", true);
            }
            else {
                $("#department_checkboxlist input[type=checkbox]").prop("checked", false);
            }
        }

        function displayDepartmentSelector() {
            var companyId, adminUserId, ajaxPromise;
            companyId = $("#main_content_hfCompanyId").val();
            adminUserId = $("#main_content_hfAdminUserId").val();

            // Ajax call to fetch 
            ajaxPromise = jQuery.ajax({
                type: "GET",
                url: "/api/Company?userid=" + adminUserId + "&companyid=" + companyId,
                contentType: "application/json; charset=utf-8",
                data: {},
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        model.DepartmentList = d.Departments;
                    }
                },
                error: function (xhr, status, error) {
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    // Render department

                    var idx, departmentObj, html, checked;
                    $("#department_checkboxlist").children().remove();
                    for (idx = 0; idx < model.DepartmentList.length; idx++) {
                        checked = "";
                        if (model.TargetedDepartmentIds.indexOf(model.DepartmentList[idx].Id) >= 0) {
                            checked = "checked=\"checked\"";
                        }

                        html = "<label><input type=\"checkbox\" " + checked + " value=\"" + model.DepartmentList[idx].Id + "\" />" + model.DepartmentList[idx].Title + "</label><br />";

                        $("#department_checkboxlist").append(html);
                    }

                    HideProgressBar();
                    $("#mpe_backgroundElement").show();
                    $("#plSelectDepartment").show();
                }
            });
        }

        function hideDepartmentSelector() {
            $("#mpe_backgroundElement").hide();
            $("#plSelectDepartment").hide();
            $("#checkbox_selected_departments").prop("checked", model.TargetedDepartmentIds.length > 0);
        }

        function saveDepartmentSelection() {
            var idx, $checkedItems = [], html;

            // Clear targetedDepartmentIds 
            model.TargetedDepartmentIds = [];
            $(".department.tags").children().remove();

            // Re-populate targetedDepartmentIds with selected department id
            $checkedItems = $("#plSelectDepartment :checked");
            for (idx = 0; idx < $checkedItems.length; idx++) {
                model.TargetedDepartmentIds.push($checkedItems[idx].value);
                html = "";
                html = html + "<div class=\"tag " + $checkedItems[idx].value + "\">";
                html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + $($checkedItems[idx]).parent().text() + "</span>";
                html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + $checkedItems[idx].value + "');\">x</a>";
                html = html + "</div>";
                $(".department.tags").append(html);
            }
            hideDepartmentSelector();
        }

        function removeDepartment(departmentId) {
            var foundIndex = model.TargetedDepartmentIds.indexOf(departmentId);

            if (foundIndex >= 0) {
                // Update Model
                model.TargetedDepartmentIds.splice(foundIndex, 1);

                // Update UI
                $(".department.tags .tag." + departmentId).remove();
            }

            $("#checkbox_selected_departments").prop("checked", model.TargetedDepartmentIds.length > 0);
            $("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
        }

        function removePersonnel(personnelId) {
            var foundIndex = model.TargetedUserIds.indexOf(personnelId);

            if (foundIndex >= 0) {
                // Update Model
                model.TargetedUserIds.splice(foundIndex, 1);

                // Update UI
                $(".personnel.tags .tag." + personnelId).remove();
            }

            $("#checkbox_selected_personnel").prop("checked", model.TargetedUserIds.length > 0);
            //var everyoneChecked = !($("#checkbox_selected_departments").prop("checked") || $("#checkbox_selected_personnel").prop("checked"));
            //$("#checkbox_privacy").prop("checked", !$("#checkbox_selected_personnel").prop("checked"));
        }

        // Privacy:Personnel-related functions
        function updateCardPreview() {
            // Update textarea box
            if (model.IsAppliedFilter) {
                $("#pulseBgFilter").css("background", "linear-gradient(to top, rgba(0,0,0,0.5) 0%,rgba(0,0,0,0) 100%)");
            } else {
                $("#pulseBgFilter").css("background", "");
            }
            // Update textarea color
            $("#pulseCardText").css("color", model.TextColor);
        }

        function setTextColor(input) {
            // model.TextColor = input.style.backgroundColor;
            model.TextColor = $(input).data("color");
            $(".text-colour").removeClass("selected");
            input.className = input.className + " selected";
            updateCardPreview();
        }

        function setBackgroundFilter(input) {
            model.IsAppliedFilter = input.checked;
            updateCardPreview();
        }

        function setPriorityOverrule(input) {
            model.PriorityOverrule = input.checked;
        }

        function removeAttachedImg(input) {

            model.UseCustomImage = false;
            model.CustomImageDataUrl = null;

            $("#pulseCard").css("backgroundImage", "none");

            // hide mini-x
            $(".remove-image-icon").hide();

            // show attach image icon
            $(".upload-option-image").show();
        }

        function displayCardPreviewImage(input, maxWidth, maxHeight, cardId, optionId) {
            var image = new Image();

            if (input.files && input.files[0]) {
                var t = input.files[0].type;
                var s = input.files[0].size / (1024 * 1024);
                if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
                    if (s > 5) {
                        toastr.error('Uploaded photo exceeded 5MB.');
                    } else {
                        var fileReader = new FileReader();
                        fileReader.addEventListener("load", function () {
                            var image = new Image();
                            image.src = this.result;

                            model.UseCustomImage = true;
                            model.CustomImageDataUrl = {
                                base64: this.result,
                                extension: $("#selected_background").val().substr($("#selected_background").val().lastIndexOf(".") + 1),
                                width: image.width,
                                height: image.height
                            };


                            $("#pulseCard").css("backgroundImage", "url(" + this.result + ")");

                            // display mini-x
                            $(".remove-image-icon").show();

                            // hide attach image icon
                            $(".upload-option-image").hide();

                        }, false);
                        fileReader.readAsDataURL(input.files[0]);
                    }
                } else {
                    toastr.error('File extension is invalid.');
                }
            }
        } // end function displayCardPreviewImage(input, maxWidth, maxHeight) {

        function validateDate(input) {
            input.className = input.className.replace("invalid-field", "");
            if (!moment(input.value, "DD/MM/YYYY", true).isValid()) {
                input.className = "invalid-field";
            }
        }
        function validateHour(input) {
            input.className = input.className.replace("invalid-field", "");

            if (!moment(input.value, "h", true).isValid()) {
                input.className = "invalid-field";
            }
        }
        function validateMinute(input) {
            input.className = input.className.replace("invalid-field", "");

            if (!moment(input.value, "m", true).isValid()) {
                input.className = "invalid-field";
            }
        }

        function loadCardInfo() {
            var pj, html, idx, record;

            // Update local Javascript model
            if ($("#main_content_hfPulse").val().trim().length > 0) {
                pj = JSON.parse($("#main_content_hfPulse").val().trim());

                $(".pulse_action").html("Edit");

                model.Actions = pj.Actions;
                model.BackgroundUrl = pj.BackgroundUrl;
                model.CardType = pj.CardType;
                model.Description = pj.Description;
                model.EndDate = pj.EndDate; // *

                model.IsAppliedFilter = pj.IsAppliedFilter;
                model.IsPrioritized = pj.IsPrioritized;
                model.ProgressStatus = pj.ProgressStatus;
                model.PulseId = pj.PulseId;
                model.PulseType = pj.PulseType;
                model.StartDate = pj.StartDate; // *
                model.Status = pj.Status;
                model.TextColor = pj.TextColor;
                model.Title = pj.Title;


                // for timezone
                var timezone = parseFloat($("#main_content_hfTimezone").val());
                if (model.StartDate.length > 0) {
                    model.StartDate = moment.utc(moment(model.StartDate).format("DD/MM/YYYY hh:mm a"), "DD/MM/YYYY hh:mm a");
                }
                if (model.EndDate) {

                    model.EndDate = moment.utc(moment(model.EndDate).format("DD/MM/YYYY hh:mm a"), "DD/MM/YYYY hh:mm a");
                }

                if (model.BackgroundUrl.indexOf("default") == -1) {
                    if ((pj.BackgroundUrl) && (pj.BackgroundUrl > 0)) {
                        model.CustomImageDataUrl = pj.BackgroundUrl;
                        model.UseCustomImage = true;
                    }
                }

                // update model's privacy
                if (pj.TargetedDepartments.length > 0) {
                    for (idx = 0; idx < pj.TargetedDepartments.length; idx++) {
                        record = pj.TargetedDepartments[idx];
                        model.TargetedDepartmentIds.push(record.Id);
                        html = "";
                        html = html + "<div class=\"tag " + record.Id + "\">";
                        html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + record.Title + "</span>";
                        html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + record.Id + "');\">x</a>";
                        html = html + "</div>";
                        $(".department.tags").append(html);
                    }

                    $("#selectDepartmentLinkButton").css("color", "blue");
                    $("#selectDepartmentLinkButton").css("cursor", "pointer");
                    $("#selectDepartmentLinkButton").click(displayDepartmentSelector);
                }
                $("#checkbox_selected_departments").prop("checked", model.TargetedDepartmentIds.length > 0);

                if (pj.TargetedUsers.length > 0) {
                    for (idx = 0; idx < pj.TargetedUsers.length; idx++) {
                        record = pj.TargetedUsers[idx];
                        model.TargetedUserIds.push(record.UserId);
                        html = "";
                        html = html + "<div class=\"tag " + record.UserId + "\">";
                        html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + record.FirstName + " " + record.LastName + "(" + record.Email + ")" + "</span>";
                        html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + record.UserId + "');\">x</a>";
                        html = html + "</div>";
                        $(".personnel.tags").append(html);
                    }
                }
                $("#checkbox_selected_personnel").prop("checked", model.TargetedUserIds.length > 0);

                $("#checkbox_privacy").prop("checked", (!($("#checkbox_selected_personnel").prop("checked") || $("#checkbox_selected_departments").prop("checked"))));

                $("#button_update").show();
            } else {
                $("#button_create").show();
            }

            // Update Pulse Card UI
            $("#text_title").val(model.Title);
            $("#titleCounter").html(titleCounterMax - model.Title.length);

            $("#pulseCardText").val(model.Description);
            var startDate_moment, endDate_moment;
            if (model.StartDate != null && model.StartDate != "") {
                startDate_moment = moment.utc(model.StartDate, "DD/MM/YYYY hh:mm a")//;.add(timezone, 'hours');
                $("#start_date").val(startDate_moment.format("DD/MM/YYYY"));
                $("#start_hh").val(startDate_moment.format("hh"));
                $("#start_mm").val(startDate_moment.format("mm"));
                $("#start_meridiem").val(startDate_moment.format("a"));
            }

            if (model.EndDate) {
                $(".end_date.specific_date").prop("checked", true)
                endDate_moment = moment.utc(model.EndDate, "DD/MM/YYYY hh:mm a")//;.add(timezone, 'hours');
                $("#end_date").val(endDate_moment.format("DD/MM/YYYY"));
                $("#end_hh").val(endDate_moment.format("hh"));
                $("#end_mm").val(endDate_moment.format("mm"));
                $("#end_meridiem").val(endDate_moment.format("a"));
            } else {
                $(".end_date.forever").prop("checked", true)
            }
            $("#select_status").val(model.Status);

            $(".text-colour").removeClass("selected");
            if (model.TextColor === "#000000") {
                $(".text-colour.black-text").addClass("selected");
            } else {
                $(".text-colour.white-text").addClass("selected");
            }

            $("#checkbox_backgroundFilter").prop("checked", model.IsAppliedFilter);

            if ((model.BackgroundUrl) && (model.BackgroundUrl.length > 0)) {
                $("#pulseCard").css("backgroundImage", "url(" + model.BackgroundUrl + ")");
                $(".upload-option-image").hide();
                $(".remove-image-icon").show();
            }

            updateCardPreview();
        }

        function textCounter(input, label, maxcounter) {

            //div_title
            $("#" + label + "").html(maxcounter - input.value.length);
            if (input.value.length > maxcounter) {
                $("#div_title").css("border-color", "red");
                $("#" + label + "").css("color", "red");
            }
            else {
                $("#div_title").css("border-color", "#DDD");
                $("#" + label + "").css("color", "#DDD");
            }
        }

        function isValidForm() {
            // Title
            if ($("#text_title").val().trim().length == 0) {
                ShowToast('Please give this a title', 2);
                return false;
            }

            if ($("#text_title").val().trim().length > titleCounterMax) {
                toastr.error("Title length maximum is 50.");
                return false;
            }

            // Srart date
            var momentSDate;
            startDateString = $("#start_date").val() + " " + $("#start_hh").val() + ":" + $("#start_mm").val() + " " + $("#start_meridiem").val();
            if (!moment(startDateString, "DD/MM/YYYY hh:mm a", true).isValid()) {
                startDateString = null;
                ShowToast('Please enter a valid start date', 2);
                return false;
            }
            else {
                momentSDate = moment.utc(startDateString, "DD/MM/YYYY hh:mm a");
            }

            // End date
            var momentEDate;
            if ($(".end_date:checked").val() === "specific_date") {
                endDateString = $("#end_date").val() + " " + $("#end_hh").val() + ":" + $("#end_mm").val() + " " + $("#end_meridiem").val();
                if (!moment(endDateString, "DD/MM/YYYY hh:mm a", true).isValid()) {
                    endDateString = null;
                    ShowToast('Please enter a valid end date', 2);
                    return false;
                }
                else {
                    momentEDate = moment.utc(endDateString, "DD/MM/YYYY hh:mm a");
                    if (momentEDate.isBefore(momentSDate) || momentEDate.isSame(momentSDate)) {
                        endDateString = null;
                        ShowToast('Your date range is invalid', 2);
                        return false;
                    }
                }
            }

            // Privacy
            if (!$("#checkbox_privacy").is(":checked") && !$("#checkbox_selected_departments").is(":checked") && !$("#checkbox_selected_personnel").is(":checked")) {
                ShowToast('Please set a privacy setting', 2);
                return false;
            }

            if ($("#checkbox_selected_departments").is(":checked")) // Departments for Privacy
            {
                if (model.TargetedDepartmentIds.length == 0) {
                    ShowToast('Please select at least one department', 2);
                    return false;
                }
            }

            if ($("#checkbox_selected_personnel").is(":checked")) // Personnels for Privacy
            {
                if (model.TargetedUserIds.length == 0) {
                    ShowToast('Please select at least one personnel', 2);
                    return false;
                }
            }

            // Background
            if (model.UseCustomImage) {
                if (model.CustomImageDataUrl == null || model.CustomImageDataUrl.base64 == null || model.CustomImageDataUrl.base64.length == 0) {
                    ShowToast('Please select at least one background image', 2);
                    return false;
                }
            }
            else {
                if (model.BackgroundUrl == null || model.BackgroundUrl.length == 0) {
                    ShowToast('Please select at least one background image', 2);
                    return false;
                }
            }
            return true;
        }

        function popupCropImage() {
            $('#selected_background').on('change', function () {
                if (this.files && this.files[0]) {
                    var type = this.files[0].type;
                    imageExtension = type;
                    var size = this.files[0].size / (1024 * 1024);

                    if (type.toLowerCase() == "image/png" || type.toLowerCase() == "image/jpeg" || type.toLowerCase() == "image/jpg") {
                        if (size > IMAGE_MAX_SIZE) {
                            ShowToast('Uploaded image exceeded ' + IMAGE_MAX_SIZE + 'MB.', 2);
                        }
                        else {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                var image = new Image();
                                image.src = e.target.result;
                                image.onload = function () {

                                    var crop = $('#uploadCrop');
                                    crop.croppie({
                                        enableExif: true,
                                        viewport: {
                                            width: 390,
                                            height: 241,
                                            type: 'square'
                                        },
                                        boundary: {
                                            width: 585,
                                            height: 362
                                        },
                                        url: e.target.result
                                    });

                                    $('#selected_background').val("");

                                    showPopWindow('popup_crop_image');
                                };
                            }
                            reader.readAsDataURL(this.files[0]);

                        }
                    }
                    else {
                        ShowToast('File extension is invalid.', 2);
                    }
                }
            });

            $('#btnCrop').on('click', function () {
                var crop = $('#uploadCrop');
                crop.croppie('result', {
                    type: 'base64',
                    size: {
                        width: 1170,
                        height: 723
                    },
                    format: 'png'
                }).then(function (resp) {
                    $("#pulseCard").css("backgroundImage", "url(" + resp + ")");
                    model.UseCustomImage = true;
                    model.CustomImageDataUrl =
                        {
                            base64: resp,
                            extension: "png",
                            width: 1170,
                            height: 723
                        };

                    crop.html("");

                    // display mini-x
                    $(".remove-image-icon").show();
                    // hide attach image icon
                    $(".upload-option-image").hide();

                    hidePopWindow('popup_crop_image');
                });
            });

            $('#btnCancelCrop').on('click', function () {
                $('#ifAddImage').val('');
                model.UseCustomImage = false;
                model.CustomImageDataUrl = null;
                var crop = $('#uploadCrop');
                crop.html("");
                hidePopWindow('popup_crop_image');
            });
        }

        function showPopWindow(eleId) {
            $('#popup_masker').css('display', 'inline-block');
            $('#' + eleId).css('display', 'block');
        }

        function hidePopWindow(eleId) {
            $('#popup_masker').css('display', 'none');
            $('#' + eleId).css('display', 'none');
        }


        (function ($) {
            $(document).ready(function () {

                popupCropImage();

                $("#start_date").datepicker({
                    beforeShow: function () {
                        setTimeout(function () {
                            $('.ui-datepicker').css('z-index', 99999);
                        }, 0);
                    },
                    dateFormat: 'dd/mm/yy'
                });
                $("#end_date").datepicker({
                    beforeShow: function () {
                        setTimeout(function () {
                            $('.ui-datepicker').css('z-index', 99999);
                        }, 0);
                    },
                    dateFormat: 'dd/mm/yy', defaultDate: +7
                });

                $("#text_selected_personnel").autocomplete({
                    source: "/api/personnel?companyid=" + $("#main_content_hfCompanyId").val() + "&userid=" + $("#main_content_hfAdminUserId").val(),
                    minLength: 1,
                    delay: 500,
                    response: function (event, ui) {
                        for (var i = 0; i < ui.content.length; i++) {
                            if (model.TargetedUserIds.indexOf(ui.content[i].value) != -1) {
                                ui.content.splice(i, 1);
                                if (i != ui.content.length - 1) {
                                    i--;
                                }
                            }
                        }
                    },
                    select: function (event, ui) {
                        var html;
                        if (model.TargetedUserIds.indexOf(ui.item.value) == -1) {
                            model.TargetedUserIds.push(ui.item.value);

                            html = "";
                            html = html + "<div class=\"tag " + ui.item.value + "\">";
                            html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + ui.item.label + "</span>";
                            html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + ui.item.value + "');\">x</a>";
                            html = html + "</div>";
                            $(".personnel.tags").append(html);

                            $("#checkbox_selected_personnel").prop("checked", model.TargetedUserIds.length > 0);
                            var everyoneChecked = !($("#checkbox_selected_departments").prop("checked") || $("#checkbox_selected_personnel").prop("checked"));
                            $("#checkbox_privacy").prop("checked", everyoneChecked);
                        }
                        return false;
                    },
                    focus: function (event, ui) {
                        event.preventDefault();
                    },
                    close: function (event, ui) {
                        $("#text_selected_personnel").val("");
                    }
                });

                $("#button_create").click(function () {
                    // Check input
                    if (!isValidForm()) {
                        return;
                    }

                    // Make ajax call 
                    var ajaxData

                    ajaxData = {
                        'pulseId': $("#main_content_hfPulseId").val(),
                        'adminUserId': $("#main_content_hfAdminUserId").val(),
                        'companyId': $("#main_content_hfCompanyId").val(),
                        'title': $("#text_title").val(),
                        'isFilterApplied': model.IsAppliedFilter,
                        'textColor': model.TextColor,
                        'backgroundUrl': model.BackgroundUrl,
                        'description': $("#pulseCardText").val(),
                        'status': $("#select_status").val(),
                        'startDate': $("#start_date").val() + " " + $("#start_hh").val() + ":" + $("#start_mm").val() + " " + $("#start_meridiem").val(),
                        'endDate': $("#end_date").val() + " " + $("#end_hh").val() + ":" + $("#end_mm").val() + " " + $("#end_meridiem").val(),
                        'isPrioritized': model.PriorityOverrule,
                        'targetedDepartmentIds': model.TargetedDepartmentIds,
                        'targetedUserIds': model.TargetedUserIds,
                        'useCustomImage': model.UseCustomImage,
                        'customImageDataUrl': model.CustomImageDataUrl
                    }

                    // for timezone
                    var timezone = parseFloat($("#main_content_hfTimezone").val());

                    if (!moment(ajaxData.startDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.startDate = null;
                    } else {
                        ajaxData.startDate = moment.utc(ajaxData.startDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
                    }

                    if (!moment(ajaxData.endDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.endDate = null;
                    } else {
                        ajaxData.endDate = moment.utc(ajaxData.endDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
                    }

                    if ($(".end_date:checked").val() === "forever") {
                        ajaxData.endDate = null;
                    }

                    // targeted departments
                    if (!$("#checkbox_selected_departments").is(":checked")) {
                        ajaxData.targetedDepartmentIds = null;
                    }

                    // targeted personnel
                    if (!$("#checkbox_selected_personnel").is(":checked")) {
                        ajaxData.targetedUserIds = null;
                    }

                    jQuery.ajax({
                        type: "PUT",
                        url: "/api/PulseBannerCard",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(ajaxData),
                        dataType: "json",
                        beforeSend: function (xhr, settings) {
                            ShowProgressBar();
                        },
                        success: function (d, status, xhr) {
                            if (d.Success) {
                                ShowToast("Pulse added", 1);
                                RedirectPage('/DynamicPulse/ClarityPulseFeedList', 300);
                            } else {
                                ShowToast(d.ErrorMessage, 2);
                            }
                        },
                        error: function (xhr, status, error) {
                            ShowToast(error, 2);
                        },
                        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                            HideProgressBar();
                        }
                    });
                });

                $("#button_update").click(function () {
                    // Check input
                    if (!isValidForm()) {
                        return;
                    }

                    // Make ajax call 
                    var ajaxData;
                    ajaxData = {
                        'pulseId': $("#main_content_hfPulseId").val(),
                        'adminUserId': $("#main_content_hfAdminUserId").val(),
                        'companyId': $("#main_content_hfCompanyId").val(),
                        'title': $("#text_title").val(),
                        'isFilterApplied': model.IsAppliedFilter,
                        'textColor': model.TextColor,
                        'backgroundUrl': model.BackgroundUrl,
                        'description': $("#pulseCardText").val(),
                        'status': $("#select_status").val(),
                        'startDate': $("#start_date").val() + " " + $("#start_hh").val() + ":" + $("#start_mm").val() + " " + $("#start_meridiem").val(),
                        'endDate': $("#end_date").val() + " " + $("#end_hh").val() + ":" + $("#end_mm").val() + " " + $("#end_meridiem").val(),
                        'isPrioritized': model.PriorityOverrule,
                        'targetedDepartmentIds': model.TargetedDepartmentIds,
                        'targetedUserIds': model.TargetedUserIds,
                        'useCustomImage': model.UseCustomImage,
                        'customImageDataUrl': model.CustomImageDataUrl
                    }

                    // for timezone
                    var timezone = parseFloat($("#main_content_hfTimezone").val());

                    if (!moment(ajaxData.startDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.startDate = null;
                    } else {
                        ajaxData.startDate = moment.utc(ajaxData.startDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
                    }

                    if (!moment(ajaxData.endDate, "DD/MM/YYYY hh:mm a", true).isValid()) {
                        ajaxData.endDate = null;
                    } else {
                        ajaxData.endDate = moment.utc(ajaxData.endDate, "DD/MM/YYYY hh:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
                    }

                    if ($(".end_date:checked").val() === "forever") {
                        ajaxData.endDate = null;
                    }

                    // targeted departments
                    if (!$("#checkbox_selected_departments").is(":checked")) {
                        ajaxData.targetedDepartmentIds = null;
                    }

                    // targeted personnel
                    if (!$("#checkbox_selected_personnel").is(":checked")) {
                        ajaxData.targetedUserIds = null;
                    }

                    jQuery.ajax({
                        type: "POST",
                        url: "/api/PulseBannerCard",
                        contentType: "application/json; charset=utf-8",
                        data: JSON.stringify(ajaxData),
                        dataType: "json",
                        beforeSend: function (xhr, settings) {
                            ShowProgressBar();
                        },
                        success: function (d, status, xhr) {
                            if (d.Success) {
                                ShowToast("Pulse updated", 1);
                                RedirectPage('/DynamicPulse/ClarityPulseFeedList', 300);
                            } else {
                                ShowToast(d.ErrorMessage, 2);
                            }
                        },
                        error: function (xhr, status, error) {
                            ShowToast(error, 2);
                        },
                        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                            HideProgressBar();
                        }
                    });
                });

                $(".chevron.back").click(function () {
                    var imageIdx, imageUrl, imageFound;
                    imageFound = false;
                    imageUrl = $("#pulseCard").css("backgroundImage").slice(4, -1).replace(/"/g, "");

                    for (imageIdx = 0; imageIdx < model.ImageArray.length; imageIdx++) {
                        if (imageUrl == model.ImageArray[imageIdx]) {
                            imageFound = true;

                            if ((imageIdx - 1) < 0) {
                                if (model.UseCustomImage) {
                                    $("#pulseCard").css("backgroundImage", "url(" + model.CustomImageDataUrl.base64 + ")");
                                    $(".upload-option-image").hide();
                                    $(".remove-image-icon").show();
                                }
                                else {
                                    $("#pulseCard").css("backgroundImage", "none");
                                    $(".upload-option-image").show();
                                    $(".remove-image-icon").hide();
                                }
                                model.BackgroundUrl = null;
                            } else {
                                $("#pulseCard").css("backgroundImage", "url(" + model.ImageArray[(imageIdx - 1)] + ")");
                                $(".upload-option-image").hide();
                                $(".remove-image-icon").hide();
                                model.BackgroundUrl = model.ImageArray[(imageIdx - 1)];
                            }
                        }
                    }

                    if (!imageFound) { // is uploaded image
                        //if (model.UseCustomImage) {
                        //    $("#pulseCard").css("backgroundImage", "url(" + model.CustomImageDataUrl + ")");
                        //    $(".upload-option-image").hide();
                        //    $(".remove-image-icon").show();
                        //} else {
                        //    $("#pulseCard").css("backgroundImage", "none");
                        //    $(".upload-option-image").show();
                        //    $(".remove-image-icon").hide();
                        //}
                        $("#pulseCard").css("backgroundImage", "url(" + model.ImageArray[model.ImageArray.length - 1] + ")");
                        $(".upload-option-image").hide();
                        $(".remove-image-icon").hide();
                        model.BackgroundUrl = model.ImageArray[model.ImageArray.length - 1];
                    }

                });

                $(".chevron.forward").click(function () {
                    var imageIdx, imageUrl, imageFound;
                    imageFound = false;
                    imageUrl = $("#pulseCard").css("backgroundImage").slice(4, -1).replace(/"/g, "");
                    for (imageIdx = 0; imageIdx < model.ImageArray.length; imageIdx++) {
                        if (imageUrl == model.ImageArray[imageIdx]) {
                            imageFound = true;
                            if ((imageIdx + 1) < model.ImageArray.length) {
                                $("#pulseCard").css("backgroundImage", "url(" + model.ImageArray[(imageIdx + 1)] + ")");
                                $(".upload-option-image").hide();
                                $(".remove-image-icon").hide();
                                model.BackgroundUrl = model.ImageArray[(imageIdx + 1)];
                            }
                            else {
                                if (model.UseCustomImage) {
                                    $("#pulseCard").css("backgroundImage", "url(" + model.CustomImageDataUrl.base64 + ")");
                                    $(".upload-option-image").hide();
                                    $(".remove-image-icon").show();
                                }
                                else {
                                    $("#pulseCard").css("backgroundImage", "none");
                                    $(".upload-option-image").show();
                                    $(".remove-image-icon").hide();
                                }
                                model.BackgroundUrl = null;
                            }
                        }
                    }

                    if (!imageFound)// is uploaded image
                    {
                        $("#pulseCard").css("backgroundImage", "url(" + model.ImageArray[0] + ")");
                        $(".upload-option-image").hide();
                        $(".remove-image-icon").hide();
                        model.BackgroundUrl = model.ImageArray[0];
                    }
                });

                // Set publisgh start date
                var publish_datetime;
                publish_datetime = moment();
                $("#start_date").val(publish_datetime.format("DD/MM/YYYY"));
                $("#start_hh").val(publish_datetime.format("hh"));
                $("#start_mm").val(publish_datetime.format("mm"));
                $("#start_meridiem").val(publish_datetime.format("a"));

                // hfPulse
                loadCardInfo();

            });
        }(jQuery));
    </script>
</asp:Content>

