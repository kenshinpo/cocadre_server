﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AnnouncementPulseFeedList.aspx.cs" Inherits="AdminWebsite.DynamicPulse.AnnoucementPulseFeedList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();

            $("#main_content_txtDateFrom").datepicker({
                beforeShow: function () {
                    setTimeout(function () {
                        $('.ui-datepicker').css('z-index', 99999);
                    }, 0);
                },
                dateFormat: 'dd/mm/yy'
            });
            $("#main_content_txtDateTo").datepicker({
                beforeShow: function () {
                    setTimeout(function () {
                        $('.ui-datepicker').css('z-index', 99999);
                    }, 0);
                },
                dateFormat: 'dd/mm/yy'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();

            $("#main_content_txtDateFrom").datepicker({
                beforeShow: function () {
                    setTimeout(function () {
                        $('.ui-datepicker').css('z-index', 99999);
                    }, 0);
                },
                dateFormat: 'dd/mm/yy'
            });
            $("#main_content_txtDateTo").datepicker({
                beforeShow: function () {
                    setTimeout(function () {
                        $('.ui-datepicker').css('z-index', 99999);
                    }, 0);
                },
                dateFormat: 'dd/mm/yy'
            });
        })
    </script>
    <style>
        .grey-text {
            color: #ccc;
        }

        .text-placeholder::-webkit-input-placeholder {
            color: rgba(176, 176, 176, 1);
        }

        .text-placeholder:-moz-placeholder { /* Firefox 18- */
            color: rgba(176, 176, 176, 1);
        }

        .text-placeholder::-moz-placeholder { /* Firefox 19+ */
            color: rgba(176, 176, 176, 1);
        }

        .text-placeholder:-ms-input-placeholder {
            color: rgba(176, 176, 176, 1);
        }
    </style>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfSurveyId" runat="server" />
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfSurveyName" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/DynamicPulse/AnnouncementPulse" class="mfb-component__button--main" data-mfb-label="Add 'Announcement' pulse">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Delete -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Dynamic Pulse
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionSurveyTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbAction_Click" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Delete -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Dynamic <span>pulse</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="surveyCountLiteral" runat="server" />
        </div>
        <div class="appbar__action" style="width: auto;">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">

        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item"><a class="data__sidebar__link data__sidebar__link--active">Announcement Pulse</a></li>
                <li class="data__sidebar__list__item"><a class="data__sidebar__link" href="/DynamicPulse/ResponsivePulseFeedList">Responsive Pulse</a></li>
                <li class="data__sidebar__list__item"><a class="data__sidebar__link" href="/DynamicPulse/ClarityPulseFeedList">Clarity Pulse</a></li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Date & time range</label>
                        <br />
                        <span style="display: inline;">
                            <asp:TextBox ID="txtDateFrom" runat="server" MaxLength="10" CssClass="text-placeholder" Style="width: 110px; display: inline; padding: 0 0 3px 0;" placeholder="From" />
                            <span>at</span>
                            <asp:TextBox ID="txtHourFrom" runat="server" MaxLength="2" CssClass="text-placeholder" Style="width: 30px; display: inline; padding: 0 0 3px 4px;" placeholder="hh" Text="00" />
                            <asp:TextBox ID="txtMinuteFrom" runat="server" MaxLength="2" CssClass="text-placeholder" Style="width: 30px; display: inline; padding: 0 0 3px 4px;" placeholder="mm" Text="00" />
                            <asp:DropDownList ID="ddlMeridiemFrom" runat="server" Style="margin: 0; width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 3px; background-color: transparent;">
                                <asp:ListItem Value="am" Text="am" Selected="True" />
                                <asp:ListItem Value="pm" Text="pm" />
                            </asp:DropDownList>
                        </span>
                        <span style="display: inline;">
                            <asp:TextBox ID="txtDateTo" runat="server" MaxLength="10" CssClass="text-placeholder" Style="width: 110px; display: inline; padding: 0 0 3px 0;" placeholder="To" />
                            <span>at</span>
                            <asp:TextBox ID="txtHourTo" runat="server" MaxLength="2" CssClass="text-placeholder" Style="width: 30px; display: inline; padding: 0 0 3px 4px;" placeholder="hh" Text="11" />
                            <asp:TextBox ID="txtMinuteTo" runat="server" MaxLength="2" CssClass="text-placeholder" Style="width: 30px; display: inline; padding: 0 0 3px 4px;" placeholder="mm" Text="59" />
                            <asp:DropDownList ID="ddlMeridiemTo" runat="server" Style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 3px; background-color: transparent;">
                                <asp:ListItem Value="am" Text="am" />
                                <asp:ListItem Value="pm" Text="pm" Selected="True" />
                            </asp:DropDownList>
                        </span>
                    </div>
                    <div class="pad">
                        <label>Search Pulse</label>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Type pulse's description" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" runat="server" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" OnClick="ibFilterSurvey_Click" />
                    </div>
                    <div class="pad">
                        <label>By Progress</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterProgress" runat="server" AutoPostBack="true" AppendDataBoundItems="true" onchange="ShowProgressBar('Filtering');" OnSelectedIndexChanged="FilterListView">
                            </asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvSurvey" runat="server" OnItemCommand="lvSurvey_ItemCommand" OnItemEditing="lvSurvey_ItemEditing" OnItemDeleting="lvSurvey_ItemDeleting"
                        OnSorting="lvSurvey_Sorting" OnItemDataBound="lvSurvey_ItemDataBound" OnItemCreated="lvSurvey_ItemCreated">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 60px; padding: 15px 20px;">
                                            <!--Icon-->
                                        </th>
                                        <th style="width: 25%; padding: 15px 5px;">
                                            <!--Date-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortPulseDate" ID="lbSortPulseDate" OnClientClick="ShowProgressBar('Filtering');" Text="Date" />
                                        </th>
                                        <th style="width: 25%; padding: 15px 5px;">
                                            <!--Title-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortPulseTitle" ID="lbSortPulseTitle" OnClientClick="ShowProgressBar('Filtering');" Text="Title" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Progress-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortPulseProgress" ID="lbSortPulseProgress" OnClientClick="ShowProgressBar('Filtering');" Text="Progress" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Status-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortPulseStatus" ID="lbSortPulseStatus" OnClientClick="ShowProgressBar('Filtering');" Text="Status" />
                                        </th>
                                        <th class="no-sort" style="width: 40px; padding: 15px 5px;">
                                            <!--Analytic Icon-->
                                        </th>
                                        <th class="no-sort" style="width: auto; padding: 15px 15px;">
                                            <!--Action Menu-->
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr runat="server">
                                <td class="no-sort" style="padding: 15px 5px; text-align: center;">
                                    <!--Icon-->
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Date-->
                                    <asp:LinkButton ID="ltDate" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Title-->
                                    <asp:LinkButton ID="lbTitle" Style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; color: #999;" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Progress-->
                                    <asp:LinkButton ID="lbProgress" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Status-->
                                    <asp:LinkButton ID="lbStatus" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px; text-align: center;">
                                    <!--Analytic Icon-->
                                    <asp:LinkButton ID="lbReport" runat="server" CommandName="ViewAnalytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>'>
                                        <asp:Image ID="imgReport" runat="server" ImageUrl="~/Img/icon_report.png" Width="35px" />
                                    </asp:LinkButton>
                                </td>
                                <td style="padding: 15px 15px;">
                                    <!--Action Menu-->
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Text="Edit Pulse" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbHide" ClientIDMode="AutoID" runat="server" CommandName="Hide" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Text="Hide Pulse" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbActivate" ClientIDMode="AutoID" runat="server" CommandName="Activate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Text="Activate Pulse" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PulseId") %>' Text="Delete Pulse" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
