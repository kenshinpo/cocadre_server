﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="PulseAnalytic.aspx.cs" Inherits="AdminWebsite.DynamicPulse.PulseAnalytic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/Css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        #analytic-wrapper .data__content {
            background: #F5F5F5;
        }

        #pulse-analytic {
            margin-top: 30px;
        }

        .filter-section {
            margin: 0 auto;
        }

            .filter-section .search-input {
                font-size: 18px;
                padding-left: 50px !important;
            }

            .filter-section .fa {
                font-size: 25px;
            }

        .invisible {
            visibility: hidden;
        }

        .mdl-selectfield {
            width: 220px !important;
        }

        .progress.stacked .progress-bar {
            border-top: 1px solid #cccccc;
            border-bottom: 1px solid #cccccc;
        }

            .progress.stacked .progress-bar:first-child {
                border-left: 1px solid #cccccc;
            }

            .progress.stacked .progress-bar:last-child {
                border-right: 1px solid #cccccc;
            }

        #saved-count {
            font-size: 65px;
            padding-left: 10px;
            vertical-align: middle;
        }

        .small-note {
            text-align: left;
            font-size: 10px;
            width: 65%;
            margin: 0 auto;
            line-height: 12px;
            padding-bottom: 13px;
            padding-top: 5px;
        }

        .dynatable-per-page {
            display: none;
        }

        .dynatable-pagination-links {
            float: none;
            text-align: center;
            padding: 20px;
        }

        .dynatable-active-page {
            background: #cccccc;
        }

        #doughnut-chart {
            vertical-align: middle;
        }

        .chart-container {
            display: inline-block;
            vertical-align: middle;
        }

        .doughnutchart-info {
            vertical-align: middle;
            display: inline-block;
            padding: 10px;
        }

        .type-content {
            padding: 10px;
        }

        .chart-title {
            font-weight: 700;
            font-size: 14px;
        }

        #chart-percentage {
            color: #FF8300;
            position: absolute;
            top: 50px;
            left: 50px;
            width: 50px;
            height: 50px;
            text-align: center;
            vertical-align: middle;
            font-size: 26px;
            margin: 0px;
            line-height: 50px;
        }

        .progress-bar.color3 {
            background-color: #ffffff;
        }

        .legend-icon > .legend-color3 {
            background: #ffffff;
            border: 1px solid #cccccc;
        }

        .list-group-item > .list-percentage {
            float: right;
        }

        .big-progress-group {
            padding-bottom: 80px;
        }

        /*DoNothing = 0,
        Next = 1,
        Skip = 2,
        SaveToInbox = 3,
        Delete = 4,
        Submit = 5,
        Acknowledge = 6,
        PlayTopic = 7,
        StartEvent = 8,
        StartSurvey = 9,
        AcceptChallenge = 10,*/

        .color1 {
            color: #cccccc;
        }

        .color2 {
            color: #69b2dd;
        }

        .color7 {
            color: #aad933;
        }

        .rounded-indicator.color1 {
            background: #69b2dd;
        }

        .rounded-indicator.color2 {
            background: #aad933;
        }

        .rounded-indicator.color3 {
            background: #cccccc;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfPulseId" />

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>Dynamic</strong> <span>Pulse</span></div>
        <div class="appbar__meta"><a href="/DynamicPulse/AnnouncementPulseFeedList" id="root-link">Pulse Feed</a></div>
        <div class="appbar__meta">
            <!--<a id="second-link"></a>-->
            <span style="color: black;" id="second-link"></span>
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->
    
    <div id="analytic-wrapper" class="data">

        <div class="data__content">

            <div id="pulse-analytic" class="container" style="width: 100%;">

                <div style="width: 100%; margin: 0 auto;">
                    <!-- TITLE -->
                    <div class="title-section" style="width: 100%; margin: 0 auto;">
                        <div class="analytic-info">
                            <h2 id="analytic-title" class="ellipsis-title">Fire Drill</h2>
                            <p id="pulse-type"><i class="fa fa-bullhorn" aria-hidden="true"></i><span class="pulse-type">Announcement Pulse</span></p>
                        </div>
                        <div class="header-left">
                            <p class="align-left"><span id="start-date-string"></span></p>
                        </div>
                        <div class="header-right">
                            <p class="align-left"><span id="data-date-string"></span></p>
                            <p id="progress-status" class="status"><span class="rounded-indicator"></span>Completed</p>
                        </div>
                    </div>

                    <div id="overview-tab" class="tabs tabs--styled">
                        <ul class="tabs__list">
                            <li class="tabs__list__item">
                                <a class="tabs__link" href="#responders-report">Responders report</a>
                            </li>
                        </ul>
                        <div class="tabs__panels">
                            <!-- OVERVIEW TAB -->
                            <div class="tabs__panels__item overview" id="responders-report">

                                <div class="filter-section text-center">
                                    <div class="option-filter invisible">
                                        <label class="select-label">Filter</label>
                                        <div class="mdl-selectfield" style="width: 250px; float: left; margin-right: 20px; height: 40px;">
                                            <select name="status" id="status">
                                                <option selected="selected" value="">All</option>
                                                <option value="Completed">Completed</option>
                                                <option value="Incomplete">Incomplete</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="search-filter">
                                        <div class="input-group" style="position: relative;">
                                            <i class="fa fa-search"></i>
                                            <input id="hidden_name" class="search-input" type="text" placeholder="Search Responders" value="" />
                                        </div>
                                    </div>
                                </div>

                                <div id="card-left" class="card card-7-col animated fadeInUp">
                                    <!-- DYNAMICALLY GENERATE CONTENT FOR THIS CARD -->
                                </div>

                                <div id="card-right" class="card card-3-col animated fadeInUp">

                                    <!-- DYNAMICALLY GENERATE CONTENT FOR THIS CARD -->
                                </div>

                                <div id="personnel-card" class="card full-row animated fadeInUp no-padding">
                                    <div class="card-header" style="padding: 20px 20px 0px 20px;">
                                        <h1 class="title">Responders list</h1>
                                    </div>
                                    <div class="table-content">
                                        <table id="responder-table" class="responder-table"></table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
        <script src="/Js/Chart.js"></script>
        <script>
            $(function () {

                var CompanyId = $('#main_content_hfCompanyId').val();
                var AdminUserId = $('#main_content_hfAdminUserId').val();
                var PulseId = $('#main_content_hfPulseId').val();

                var card_right_html = "";
                var card_left_html = "";
                var legend_html = "";
                var progress_html = "";
                var table_html = "";

                var responder_table = $('#responder-table');
                var responder_data = [];

                function fetchData() {
                    $('#pulse-analytic').addClass('hidden');
                    $('#pulse-analytic').removeClass('animated fadeInUp');

                    ShowProgressBar();
                    $.ajax({
                        type: "POST",
                        url: '/Api/PulseAnalytics/SelectFeedAnalytic',
                        data: {
                            "CompanyId": CompanyId,
                            "AdminUserId": AdminUserId,
                            "PulseId": PulseId
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            HideProgressBar();
                            if (res.Success) {
                                $('#pulse-analytic').removeClass('hidden');
                                $('#pulse-analytic').addClass('animated fadeInUp');

                                var pulse = res.Pulse;
                                var leftChart = res.LeftChart;
                                var rightChart = res.RightChart;
                                var personnelList = res.PersonnelList;

                                $('#analytic-title').html(pulse.Title);
                                $('#second-link').html(pulse.Title);

                                //Pulse type
                                //Announcement = 1,
                                //Clarity = 2

                                if (pulse.PulseType == 1) {
                                    $('#pulse-type').html('<i class="fa fa-bullhorn" aria-hidden="true"></i> <span class="pulse-type">Announcement Pulse</span>');
                                    $('#root-link').prop('href', '/DynamicPulse/AnnouncementPulseFeedList');
                                } else if (pulse.PulseType == 2) {
                                    $('#pulse-type').html('<i class="fa fa-newspaper-o" aria-hidden="true"></i> <span class="pulse-type">Clarity Pulse</span>');
                                    $('#root-link').prop('href', '/DynamicPulse/ClarityPulseFeedList');
                                } else {
                                    // do nothing
                                }
                                //progressStatus
                                //Upcoming = 1,
                                //Live = 2,
                                //Completed = 3

                                if (pulse.ProgressStatus == 1) {
                                    $('#progress-status').html('<span class="rounded-indicator color1"></span> Upcoming');
                                } else if (pulse.ProgressStatus == 2) {
                                    $('#progress-status').html('<span class="rounded-indicator color2"></span> Live');
                                } else if (pulse.ProgressStatus == 3) {
                                    $('#progress-status').html('<span class="rounded-indicator color3"></span> Completed');
                                }

                                if (pulse.EndDateString) {
                                    $('#start-date-string').html('Start from ' + pulse.StartDateString + " to " + pulse.EndDateString);
                                    $('#data-date-string').html('Showing data from ' + pulse.StartDateString + " to " + pulse.EndDateString);
                                } else {
                                    $('#start-date-string').html('Since ' + pulse.StartDateString);
                                    $('#data-date-string').html('Showing data since ' + pulse.StartDateString);
                                }


                                // LEFT CHART
                                if (leftChart.Type == 1) {
                                    if (leftChart.Stats.length > 1) {

                                        $.each(leftChart.Stats, function (k, v) {
                                            legend_html += '<div class="legend-item"><div class="legend-icon"><div class="legend-color' + (k + 1) + '"></div></div>';
                                            legend_html += '<label>' + v.Name + '</label></div>';

                                            progress_html += '<div class="progress-bar color' + (k + 1) + '"  style="width:' + v.Percentage + '%;">';

                                            if (k == 0 && v.Percentage < 10) {
                                                progress_html += '<span class="value" style="left:-5px;">' + v.Count + '</span>';
                                                progress_html += '<span class="percentage color' + (k + 1) + '" style="width:' + v.Percentage + '%; left:-17px;">' + v.Percentage + '%</span></div>';
                                            } else {
                                                progress_html += '<span class="value">' + v.Count + '</span>';
                                                progress_html += '<span class="percentage color' + (k + 1) + '" style="width:' + v.Percentage + '%;">' + v.Percentage + '%</span></div>';
                                            }

                                        });

                                        card_left_html += '<div class="card-header">';
                                        card_left_html += '<h1 class="title">' + leftChart.Title + '</h1>';
                                        card_left_html += '<div class="legends">';
                                        card_left_html += legend_html;
                                        card_left_html += '</div></div>';
                                        card_left_html += '<div class="type-content"><div class="progress stacked" style="padding-left: 17px;padding-right: 10px;">';
                                        card_left_html += progress_html + '</div>';

                                        $('#card-left').empty();
                                        $('#card-left').append(card_left_html);
                                    } else {
                                        card_left_html += '<div class="card-header">';
                                        card_left_html += '<h1 class="title">' + leftChart.Title + '</h1>';
                                        card_left_html += '</div>';
                                        card_left_html += '<div class="type-content">';
                                        card_left_html += ' <ul class="list-group big-progress-group" >';
                                        card_left_html += ' <li class="list-group-item"> ';
                                        card_left_html += '<span id="completed-percentage" class="list-percentage">330</span>';
                                        card_left_html += '<div class="progress">';
                                        card_left_html += '<div id="completed-bar" class="progress-bar color2" style="width:100%;"></div>';
                                        card_left_html += '</div></li></ul>';
                                        card_left_html += '</div>';
                                        $('#card-left').empty();
                                        $('#card-left').append(card_left_html);
                                    }
                                }


                                // RIGHT CHART
                                if (rightChart.Type == 2) {
                                    card_right_html += '<div class="card-header">';
                                    card_right_html += '<h1 class="title">' + rightChart.Title + '</h1>';
                                    card_right_html += '</div>';
                                    card_right_html += '<div class="legends" style="top:48px;">';
                                    card_right_html += '<div class="legend-item">';
                                    card_right_html += ' <div class="legend-icon"><div class="legend-color1" style="background:#FF8300;"></div></div>';
                                    card_right_html += '<label>Viewed</label></div>';
                                    card_right_html += '<div class="legend-item">';
                                    card_right_html += ' <div class="legend-icon"><div class="legend-color1" style="background:#6e6e6e;"></div></div>';
                                    card_right_html += '<label>Have not view</label></div></div>';
                                    card_right_html += '<div class="type-content">';
                                    card_right_html += ' <div class="chart-container" style="position:relative;">';
                                    card_right_html += ' <canvas id="doughnut-chart" width="150" height="150" ></canvas>';
                                    card_right_html += '<label id="chart-percentage">' + rightChart.Stats[0].Percentage + '%</label>';
                                    card_right_html += '</div>';
                                    card_right_html += '<div class="doughnutchart-info">';
                                    card_right_html += '<label class="chart-title">' + rightChart.Stats[0].Name + '</label>';
                                    card_right_html += '<p class="chart-data"><span id="count">' + rightChart.Stats[0].Count + '</span> of <span id="total">' + rightChart.MaxCount + '</span></p>';
                                    card_right_html += '</div>';
                                    card_right_html += '</div>';
                                    card_right_html += '</div>';

                                    $('#card-right').empty();
                                    $('#card-right').append(card_right_html);

                                    var ctx = document.getElementById("doughnut-chart").getContext("2d");
                                    var chart_data = [
                                        { value: rightChart.Stats[0].Count, color: "#FF8300" },
                                        { value: rightChart.MaxCount - rightChart.Stats[0].Count, color: "#6e6e6e" }
                                    ];

                                    var doughnutChart = new Chart(ctx).Doughnut(chart_data, {
                                        percentageInnerCutout: 75,
                                        animationEasing: "linear",
                                        animateScale: false,
                                        animationSteps: 50,
                                        segmentShowStroke: false
                                    });

                                } else if (rightChart.Type == 3) {
                                    card_right_html += '<div class="card-header">';
                                    card_right_html += '<h1 class="title">' + rightChart.Title + '</h1>';
                                    card_right_html += '</div>';
                                    card_right_html += '<div class="type-content align-center">';
                                    card_right_html += '<h1 class="text-center" style="margin-bottom:0px;">';
                                    card_right_html += '<img src="/Img/type3-icon.png" style="vertical-align:middle;"/>';
                                    card_right_html += '<span id="saved-count" style="vertical-align:middle;">' + rightChart.Stats[0].Count + '</span>';
                                    card_right_html += '</h1>';
                                    card_right_html += '<p class="small-note">' + rightChart.Disclaimer + '</p>';
                                    card_right_html += '</div>';
                                    $('#card-right').empty();
                                    $('#card-right').append(card_right_html);
                                }


                                //PERSONNEL CARD
                                $('#personnel-card .card-header .title').html(personnelList.Title);

                                table_html += '<thead>';
                                table_html += '<th data-dynatable-column="no" style="width:10%">No</th>';
                                table_html += '<th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width:30%">Name</th>';
                                table_html += '<th class="hidden" data-dynatable-column="hidden_name">Hidden name</th>';
                                table_html += '<th data-dynatable-column="last_updated" data-dynatable-sorts="last_updated" style="width:20%">Date</th>';
                                table_html += '<th data-dynatable-column="department" data-dynatable-sorts="department" style="width:20%">Department</th>';

                                if (personnelList.IsActionRequired) {
                                    table_html += '<th data-dynatable-column="action" style="width:20%">Action</th>';
                                    table_html += '</thead><tbody></tbody>';

                                    $.each(personnelList.Users, function (k, v) {
                                        responder_data.push({
                                            "no": k + 1,
                                            "responder_name": v.User.FirstName + " " + v.User.LastName,
                                            "hidden_name": v.User.FirstName + " " + v.User.LastName,
                                            "last_updated": v.FirstSeenDateString,
                                            "department": v.User.Departments[0].Title,
                                            "action": '<span class="color' + (v.ActionCode + 1) + '">' + v.ActionName + '</span>'
                                        })
                                    });

                                    $('#responder-table').append(table_html);

                                } else {
                                    table_html += '</thead><tbody></tbody>';

                                    $.each(personnelList.Users, function (k, v) {
                                        responder_data.push({
                                            "no": k + 1,
                                            "responder_name": v.User.FirstName + " " + v.User.LastName,
                                            "hidden_name": v.User.FirstName + " " + v.User.LastName,
                                            "last_updated": v.FirstSeenDateString,
                                            "department": v.User.Departments[0].Title
                                        })
                                    });

                                    $('#responder-table').append(table_html);
                                }

                                responder_table.dynatable({
                                    features: {
                                        paginate: true,
                                        search: false,
                                        sorting: true,
                                        recordCount: false,
                                        pushState: false,
                                    },
                                    dataset: {
                                        records: responder_data,
                                        sorts: { 'no': 1 }
                                    },
                                    inputs: {
                                        queries: $(' #hidden_name')
                                    }
                                });
                                
                              
                                var dynatable = responder_table.data('dynatable');

                                if (typeof dynatable.records !== "undefined") {
                                    dynatable.records.updateFromJson({ records: responder_data });
                                    dynatable.records.init();
                                }
                                dynatable.paginationPerPage.set(15);
                                dynatable.process();


                                responder_table.data('dynatable').paginationPerPage.set(15);
                                responder_table.data('dynatable').process();

                                setTimeout(function () {
                                    responder_table.css('display', 'table');
                                    responder_table.addClass('animated fadeIn');
                                }, 500);
                            }
                            else { // ERROR RESPONSE
                                $('#pulse-analytic').empty();
                                $('#pulse-analytic').html('<p class="align-center">' + res.ErrorMessage + '</p>');
                            }
                        }

                    });
                }

                responder_table.bind('dynatable:afterUpdate', function (e, dynatable) {
                    setTimeout(function () {
                        responder_table.css('display', 'table');
                        responder_table.addClass('animated fadeIn');
                    }, 500);
                });

                // CUSTOM TABLE SEARCH FILTER FUNCTION
                responder_table.bind('dynatable:init', function (e, dynatable) {

                    dynatable.queries.functions['hidden_name'] = function (record, queryValue) {
                        return record.hidden_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                    };
                });

                fetchData();
            });
        </script>
    </div>
</asp:Content>
