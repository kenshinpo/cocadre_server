﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="RespondersAnswerList.aspx.cs" Inherits="AdminWebsite.DynamicPulse.RespondersAnswerList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
         .appbar__meta a {
            color:#000;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfDeckId" />
    <asp:HiddenField runat="server" ID="hfPulseId" />

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><a href="/Survey/List" style="color:#000;">Dynamic <span>pulse</span></a></div>
        <div class="appbar__meta"><a href="" id="survey-root-link">Responsive Pulse</a></div>
        <div class="appbar__meta"><a href="" class="pulse-title"></a></div>
        <div class="appbar__meta"><a href="" class="question"></a></div>
        <div class="appbar__meta breadcrumb-participant" ></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="content-wrapper" class="data" style="overflow-y:scroll;">

        <div id="responsive-pulse" class="container" style="width:100%;">

            <div style="width:100%; margin:20px auto;">

                <div class="title-section" style="width:100%; margin: 0 auto;">
                    <div class="full-row">
                        <div class="card-7-col no-padding">
                            <h1 class="pulse-title no-margin ellipsis-title"></h1>
                        </div>
                        <div class="pulse-status card-3-col align-right no-padding">
                        </div>
                    </div>
                    <div class="full-row">
                        <div class="card-7-col no-padding">
                            <p class="align-left inline-block-mid" >Showing data from <span class="start-date"></span> <span class="end-date"></span></p>
                        </div>
                        <div class="card-3-col align-right no-padding">
                            <p>End date: <span class="end-date"></span></p>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div id="single-survey-table-wrapper" class="card card-full animated fadeInUp no-padding">
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="/js/vendor/moment-with-locales.js"></script>
    <script src="/Js/Chart.js"></script>
    <script>
        $(function () {

            var apiUrl = '/Api/PulseAnalytics/';
            var single_table_html = [];
            var companyId = $('#main_content_hfCompanyId').val();
            var adminUserId = $('#main_content_hfAdminUserId').val();
            var deckId = $('#main_content_hfDeckId').val();
            var pulseId = $('#main_content_hfPulseId').val();

            var single_table_html = [];

            function setStatus(statusCode) {

                var status = ""

                if (statusCode == 1) {
                    status = "Upcoming";
                } else if (statusCode == 2) {
                    status = "Live";
                } else if (statusCode == 3) {
                    status = "Completed";
                }

                var status_html = "";
                status_html += '<div class="legend-item" style="float:right;">';
                status_html += '<div class="legend-icon">';
                status_html += '<div class="legend-color' + statusCode + '" style="border-radius:50%;"></div>';
                status_html += '</div>';
                status_html += '<label>' + status + '</label>';
                status_html += '</div>';

                $('.pulse-status').empty();
                $('.pulse-status').html(status_html);
            }

            function fetchData() {
                $('#responsive-pulse').addClass('hidden');
                $('#responsive-pulse').removeClass('animated fadeInUp');

                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: apiUrl + 'SelectDeckCardCustomAnswerAnalytic',
                    data: {
                        "CompanyId": companyId,
                        "AdminUserId": adminUserId,
                        "DeckId": deckId,
                        "PulseId": pulseId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {
                            $('#responsive-pulse').removeClass('hidden');
                            $('#responsive-pulse').addClass('animated fadeInUp');

                            $('#single-survey-table-wrapper').removeClass('hidden animated fadeOut');
                            $('#single-survey-table-wrapper').css('display', 'table');
                            $('#single-survey-table-wrapper').addClass('animated fadeInUp');

                            var deck = res.Deck;
                            var pulse = res.Pulse;

                            setStatus(deck.Status);
                            $('.pulse-title').html(deck.Title);
                            $('#pulse-stat .stat-title').html('RP' + pulse.Ordering);
                            $('.question').html('RP' + pulse.Ordering + ": " + pulse.Title);
                            $('.start-date').html(deck.DeckStartTimestampString);

                            if (deck.DeckEndTimestampString) {
                                $('.end-date').html('to ' + deck.DeckEndTimestampString);
                            } else {
                                $('.end-date').html('until now');
                            }

                            single_table_data = [];
                            single_table_html = "";

                            single_table_html += '<div class="card-header">';
                            single_table_html += '<div class="grid-question-number">';
                            single_table_html += '<div style="color:#ddd;">';
                            single_table_html += '<div class="number">RP' + pulse.Ordering;
                            single_table_html += '</div>';
                            single_table_html += '<div class="code">Code: ' + pulse.PulseId + '</div>';
                            single_table_html += '</div> ';
                            single_table_html += '</div>';
                            
                            single_table_html += '<div class="grid-question">';
                            if (pulse.ImageUrl) {
                                single_table_html += '<div class="question-image" style="width: 100px; height: auto; background: #fff; float: left;margin-right: 10px;"><img src="' + pulse.ImageUrl + '" /></div>';
                            }
                            single_table_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Question</label>';
                            single_table_html += ' <p class="question">' + pulse.Title + '</p>';
                            single_table_html += '</div>';
                            single_table_html += '<div class="grid-type align-center">';
                            single_table_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Type</label>';

                            single_table_html += '<div class="type-image" style="width:25px; height:25px; margin:0 auto;">';
                            if (pulse.CardType == 1) {
                                single_table_html += '<img src="/Img/icon_selectone.png" />';
                            } else if (pulse.CardType == 2) {
                                single_table_html += '<img src="/Img/icon_numberrange.png" />';
                            } else if (pulse.CardType == 3) {
                                single_table_html += '<img src="/Img/icon_text.png" />';
                            } else if (pulse.CardType == 4) {
                                single_table_html += '<img src="/Img/icon_selectone.png" />';
                            }
                            single_table_html += '</div>';

                            single_table_html += '</div>';
                            single_table_html += '<div class="grid-overall">';
                            single_table_html += '<small>Out of</small>';
                            single_table_html += '<label class="response_number">' + pulse.TotalRespondents + '</label>';
                            single_table_html += '<small>responses</small>';
                            single_table_html += '</div>';
                            single_table_html += '</div>';
                            single_table_html += ' <div class="table-content">';
                            single_table_html += ' <table id="single-survey-table">';
                            single_table_html += '<thead>';
                            single_table_html += '<th data-dynatable-column="no" style="width:10%">No</th>';
                            single_table_html += '<th data-dynatable-column="responses" data-dynatable-sorts="responses" style="width:50%">Responses</th>';

                            single_table_html += '<th data-dynatable-column="name" data-dynatable-sorts="name" style="width:20%">Name</th>';
                            single_table_html += '<th data-dynatable-column="department" data-dynatable-sorts="department" style="width:20%">Department</th>';

                            single_table_html += '</thead>';
                            single_table_html += '<tbody></tbody>';
                            single_table_html += '</table>';
                            single_table_html += ' </div>';

                            $("#single-survey-table-wrapper").empty();
                            $("#single-survey-table-wrapper").append(single_table_html);

                            if (pulse.TextAnswers && pulse.TextAnswers.length > 0) {

                                $.each(pulse.TextAnswers, function (key, value) {
                                    single_table_data.push({
                                        "no": key + 1,
                                        "responses": value.Content,
                                        "name": value.AnsweredByUser.FirstName + " " + value.AnsweredByUser.LastName,
                                        "department": value.AnsweredByUser.Departments[0].Title
                                    });

                                });
                               

                                var single_survey_table = $('#single-survey-table').dynatable({
                                    features: {
                                        paginate: false,
                                        search: false,
                                        sorting: true,
                                        recordCount: false,
                                        pushState: false,
                                    },
                                    dataset: {
                                        records: single_table_data,
                                        sorts: { 'no': 1 }
                                    }
                                }).data('dynatable');

                                single_survey_table.settings.dataset.originalRecords = single_table_data;
                                single_survey_table.process();


                                var dynatable = $('#single-survey-table').data('dynatable');
                                if (typeof dynatable.records !== "undefined") {
                                    dynatable.records.updateFromJson({ records: single_table_data });
                                    dynatable.records.init();
                                }
                                dynatable.paginationPerPage.set(15);
                                dynatable.process();
                                dynatable.dom.update();

                            }
                        } else {
                            $('#content-wrapper').prepend('<p class="align-center" style="margin-top:70px; width:100%;">No analytic for the selected pulse. </p>');
                        }
                    }
                });
            }

            fetchData();
        });
    </script>

</asp:Content>
