﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.DynamicPulse
{
    public partial class RespondersAnswerList : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            ViewState["manager_user_id"] = managerInfo.UserId;
            ViewState["company_id"] = managerInfo.CompanyId;

            this.hfAdminUserId.Value = ViewState["manager_user_id"].ToString();
            this.hfCompanyId.Value = ViewState["company_id"].ToString();

            String deckId = String.Empty;
            String pulseId = String.Empty;

            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["DeckId"] != null)
                {
                    deckId = Page.RouteData.Values["DeckId"].ToString().Trim();
                }

                if (Page.RouteData.Values["PulseId"] != null)
                {
                    pulseId = Page.RouteData.Values["PulseId"].ToString().Trim();
                }

                if (!string.IsNullOrWhiteSpace(deckId))
                {
                    PulseSelectDeckResponse selectResponse = asc.SelectFullDetailDeck(managerInfo.UserId, managerInfo.CompanyId, deckId, true);
                    this.hfDeckId.Value = deckId;
                }

                if (!string.IsNullOrWhiteSpace(pulseId))
                {
                    PulseSelectDeckResponse selectResponse = asc.SelectFullDetailDeck(managerInfo.UserId, managerInfo.CompanyId, deckId, true);
                    this.hfPulseId.Value = pulseId;

                }

            }
        }
    }
}