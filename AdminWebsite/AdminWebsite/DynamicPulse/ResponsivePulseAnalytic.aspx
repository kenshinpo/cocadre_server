﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ResponsivePulseAnalytic.aspx.cs" Inherits="AdminWebsite.DynamicPulse.ResponsivePulseAnalytic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
	<style>
		.header-padding {
			padding: 20px 30px;
		}

		.header-border {
			border-bottom: 1px solid #ccc;
		}

		.secondary-label {
			color: #ccc;
			margin: 0px;
		}

		.chart-content {
			display: flex;
			display: -webkit-box;
			display: -ms-flexbox;
			display: -webkit-flex;
			justify-content: center;
			align-items: center;
			align-content: center;
			height: 85%;
			padding: 0px;
		}

		.chart-section {
			display: flex;
			display: -webkit-box;
			display: -ms-flexbox;
			display: -webkit-flex;
			justify-content: center;
			align-content: center;
			align-items: center;
			/*position: absolute;
			margin-top: 20px;
			text-align:center;
			top: 50%;
			left: 50%;
			width: 100%;
			padding-left: 30px;
			padding-right: 30px;
			transform: translate(-50%, -50%);
			-webkit-transform: translate(-50%, -50%);
			-moz-transform: translate(-50%, -50%);
			-o-transform: translate(-50%, -50%);
			-webkit-filter: blur(0);
			-webkit-backface-visibility: hidden;
			-webkit-transform: scale(1.0, 1.0);
			-webkit-font-smoothing: antialiased;*/
		}

		.hv-center {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
			-webkit-transform: translate(-50%, -50%);
			-moz-transform: translate(-50%, -50%);
			-o-transform: translate(-50%, -50%);
		}

		.card-content table th {
			background: none;
			border: 1px solid #ccc;
			padding: 10px;
		}

		table .question-img {
			width: 50px;
			height: 50px;
			display: inline-block;
			vertical-align: top;
			overflow: hidden;
		}

		table .question-label {
			display: inline-block;
			vertical-align: top;
		}

		table th {
			font-weight: 700;
		}

		table td {
			border: 1px solid #ccc;
			padding: 15px;
		}

			table td:first-child, table th:first-child {
				border-left: 0px;
			}

			table td:last-child, table th:last-child {
				border-right: 0px;
			}

		.default-btn {
			background: #f1f1f1;
			border: 1px solid #ccc;
			color: #545454;
			text-align: center;
			border-radius: 0px;
			padding: 5px 15px;
		}

		.progress-number {
			width: 20%;
			display: inline-block;
			vertical-align: middle;
		}

		.squared-progress {
			display: block;
			border: 2px solid #FF8300;
			border-radius: 0px;
			height: 15px;
			margin: 10px;
			display: inline-block;
			width: 65%;
			vertical-align: middle;
		}

		.squared-bar {
			width: 70%;
			margin: 0px;
			background-color: #FF8300;
			border-radius: 0px;
		}

		.table-separator {
			height: 5px;
			background: #c0c0c0;
		}

			.table-separator td {
				padding: 0px;
			}

		.clickable-row {
			cursor: pointer;
			transition: all 0.5s ease;
			-moz-transition: all 0.5s ease;
			-webkit-transition: all 0.5s ease;
			-ms-transition: all 0.5s ease;
			-o-transition: all 0.5s ease;
		}

			.clickable-row:hover {
				background: #f1f1f1;
			}

		.chart-title {
			font-weight: 700;
		}

		#chart-percentage {
			font-size: 26px;
			color: #FF8300;
		}

		.stat-code {
			word-break: break-all;
			line-height: 14px;
			display: block;
		}

		.appbar__meta a {
			color: #000;
		}

		.error-msg {
			display: none;
		}
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

	<asp:HiddenField runat="server" ID="hfAdminUserId" />
	<asp:HiddenField runat="server" ID="hfCompanyId" />
	<asp:HiddenField runat="server" ID="hfDeckId" />
	<asp:HiddenField runat="server" ID="hfPulseId" />

	<!-- App Bar -->
	<div id="st-trigger-effects" class="appbar">
		<a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
		<div class="appbar__title"><a href="/DynamicPulse/ResponsivePulseFeedList" style="color: #000;">Dynamic <span>pulse</span></a></div>
		<div class="appbar__meta"><a href="" id="survey-root-link">Responsive Pulse</a></div>
		<div class="appbar__meta"><a href="" class="pulse-title"></a></div>
		<div class="appbar__meta"><a href="" class="question"></a></div>
		<div class="appbar__meta breadcrumb-participant"></div>
		<div class="appbar__action">
			<a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
		</div>
	</div>
	<!-- /App Bar -->

	<div id="content-wrapper" class="data" style="overflow-y: scroll;">

		<div id="responsive-pulse" class="container" style="width: 100%;">

			<div style="width: 100%; margin: 20px auto;">

				<div class="title-section" style="width: 100%; margin: 0 auto;">
					<div class="full-row">
						<div class="card-7-col no-padding">
							<h1 class="pulse-title no-margin ellipsis-title"></h1>
						</div>
						<div class="pulse-status card-3-col align-right no-padding">
						</div>
					</div>
					<div class="full-row">
						<div class="card-7-col no-padding">
							<p class="align-left inline-block-mid" style="margin-bottom: 0px; margin-top: 10px;">Showing data from <span class="start-date"></span><span class="end-date"></span></p>
							<div class="mdl-selectfield inline-block-mid" style="width: 250px; float: right; margin-left: 20px; height: 40px;">
								<select name="filter" id="filter">
									<option selected="selected" value="">All Department</option>
									<option value="Completed">Completed</option>
									<option value="Incomplete">Incomplete</option>
								</select>
							</div>
						</div>
						<div class="card-3-col align-right no-padding">
							<p style="margin-top: 10px;">End date <span class="end-date"></span></p>
						</div>
					</div>

				</div>

				<div class="clearfix"></div>

				<div class="error-msg"></div>

				<div class="card-container" style="display: flex;">
					<div id="pulse-stat" class="card card-7-col animated fadeInUp no-padding" style="align-items: stretch; min-height: 250px;">
						<div class="additional-header header-padding header-border hidden">
							<div class="source inline-block-top" style="width: 49%;">
								<label style="font-weight: 700;">Source:</label>
								<p>RP3: Do you prefer eating out or eating in?</p>
							</div>
							<div class="option inline-block-top" style="width: 49%;">
								<label style="font-weight: 700;">Option:</label>
								<p>I prefer eating in....</p>
							</div>
						</div>
						<div class="card-header header-padding header-border">
							<div class="inline-block-top" style="width: 19%; padding-right: 15px;">
								<h1 class="stat-title secondary-label ellipsis-title"></h1>
								<small class="stat-code secondary-label"></small>
							</div>
							<div class="inline-block-top" style="width: 49%;">
								<p class="question-label secondary-label">Question</p>
								<p class="question">Types of food you like?</p>
								<p class="desc-label secondary-label">Description</p>
								<p class="desc">Types of food you like?</p>
							</div>
							<div class="align-right inline-block-top" style="width: 29%;">
								<p class="filterBy" class="secondary-label">All Department</p>
							</div>
						</div>
						<div class="card-content">
							<p class="error-msg">No information.</p>
							<div class="card-stat">
								<div class="question-type align-center inline-block-top" style="width: 15%;">
									<label style="color: #ddd; font-size: 16px; line-height: 16px;">Type</label>
									<div class="type-image" style="width: 25px; height: 25px; margin: 0 auto;">
										<img src="/Img/icon_text.png" />
									</div>
								</div>
								<div class="result-table inline-block-top" style="width: 70%;">
								</div>
							</div>
						</div>
					</div>

					<div id="respondent-chart" class="card card-3-col animated fadeInUp" style="align-items: stretch;">
						<div class="card-header">
							<h1 class="title">Respondents</h1>
						</div>
						<div class="card-content chart-content">
							<div class="chart-section">
								<div class="chart-container inline-block-mid" style="position: relative; width: 150px;">
									<canvas id="doughnut-chart" width="150" height="150"></canvas>
									<label id="chart-percentage" class="hv-center">0%</label>
								</div>
								<div class="doughnutchart-info inline-block-mid" style="padding: 10px;">
									<label class="chart-title">Personnel</label>
									<p class="chart-data"><span id="count"></span><span>of </span><span id="total"></span></p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="full-row align-center animated fadeInUp">
					<div class="input-group" style="position: relative; margin: 0 auto;">
						<i class="fa fa-search"></i>
						<input name="search" class="search-input" type="text" placeholder="Search or Click on the other Pulse below to display report" value="" />
					</div>
				</div>

				<div class="full-row ">
					<div class="card no-padding animated fadeInUp">
						<div class="legends">
							<div class="legend-item">
								<div class="legend-icon">
									<img src="/Img/icon_logic_redirect.png" />
								</div>
								<label>Redirect</label>
							</div>
							<div class="legend-item">
								<div class="legend-icon">
									<img src="/Img/icon_logic_destination.png" />
								</div>
								<label>Destination</label>
							</div>
						</div>

						<div class="card-header header-padding">
							<h3 class="title no-margin" style="font-size: 14px;">All Pulse Published</h3>
						</div>

						<div class="card-content no-padding ">
							<table id="pulse-table" class="no-margin">
								<thead>
									<th width="10%">Card</th>
									<th width="50%">Question</th>
									<th width="10%">Type</th>
									<th width="10%">Logic</th>
									<th width="20%">Respondents</th>
									<%--<th width="10%">Report</th>--%>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
	<script src="/Js/Chart.js"></script>
	<script>
		$(function () {
			var apiUrl = '/Api/PulseAnalytics/';
			var table_html = "";
			var result_list_html = "";
			var pulseId = "";
			var companyId = $('#main_content_hfCompanyId').val();
			var adminUserId = $('#main_content_hfAdminUserId').val();
			var deckId = $('#main_content_hfDeckId').val();
			var newPulseId = $('#main_content_hfPulseId').val();
			var searchId = "";
            var pulseData = [];
            var departmentName = "";

			function setStatus(statusCode) {

				var status = ""

				if (statusCode == 1) {
					status = "Upcoming";
				} else if (statusCode == 2) {
					status = "Live";
				} else if (statusCode == 3) {
					status = "Completed";
				}

				var status_html = "";
				status_html += '<div class="legend-item" style="float:right;">';
				status_html += '<div class="legend-icon">';
				status_html += '<div class="legend-color' + statusCode + '" style="border-radius:50%;"></div>';
				status_html += '</div>';
				status_html += '<label>' + status + '</label>';
				status_html += '</div>';

				$('.pulse-status').empty();
				$('.pulse-status').html(status_html);
			}

			function truncateStr(str, length, ending) {
				if (length == null) {
					length = 100;
				}
				if (ending == null) {
					ending = '...';
				}
				if (str.length > length) {
					return str.substring(0, length - ending.length) + ending;
				} else {
					return str;
				}
			}

			function generateTableRow(value) {

				table_html += '<tr class="clickable-row animated fadeIn" >';
				//col 1
				table_html += '<td class="clickable" data-pulseid="' + value.PulseId + '">';
				table_html += '<h1 class="secondary-label">RP' + value.Ordering + '</h1>';
				table_html += '</td>';
				//col 2
				table_html += '<td class="clickable" data-pulseid="' + value.PulseId + '">';
				table_html += '<div style="display:flex;">';
				if (value.ImageUrl) {
					table_html += '<div class="question-img"><img src="' + value.ImageUrl + '" /></div>';
				}
				table_html += '<div class="question-label" style="padding-left: 15px;">';
				table_html += '<p>' + value.Title + '</p>';
				table_html += '</div>';
				table_html += '</div>';
				table_html += '</td>';

				//col 3
				table_html += '<td class="align-center">';
				if (value.CardType == 1) {
					table_html += '<img src="/Img/icon_selectone.png" />';
				} else if (value.CardType == 2) {
					table_html += '<img src="/Img/icon_numberrange.png" />';
				} else if (value.CardType == 3) {
					table_html += '<img src="/Img/icon_text.png" />';
				} else if (value.CardType == 4) {
					table_html += '<img src="/Img/icon_selectone.png" />';
				}
				table_html += '</td>';

				//col 4
				table_html += '<td class="align-center clickable" data-pulseid="' + value.PulseId + '">';
				if (value.HasOutgoingLogic) {
					table_html += '<div style="width:20px; height:20px;margin:5px; display: inline-block;"><img src="/Img/icon_logic_redirect.png" style="width:100%;"/></div>';
				}

				if (value.HasIncomingLogic) {
					table_html += '<div style="width:20px; height:20px; margin:5px; display: inline-block;"><img src="/Img/icon_logic_destination.png" style="width:100%;"/></div>';
				}

				table_html += '</td>';
				//col 5 
				table_html += '<td class="clickable" data-pulseid="' + value.PulseId + '">';
				table_html += '<div class="progress-number align-center" >';
				table_html += '<span class="number-top">' + value.RespondentChart.Stats[0].Count + '</span>';
				table_html += '<hr style="margin:0px;"/>';
				table_html += '<span class="number-bottom">' + value.RespondentChart.MaxCount + '</span>';
				table_html += '</div>';
				table_html += '<div class="progress squared-progress">';
				table_html += '<div class="progress-bar squared-bar" style="width: ' + (value.RespondentChart.Stats[0].Count / value.RespondentChart.MaxCount) * 100 + '%;"></div>';
				table_html += '</div>';
				table_html += '</td>';

				table_html += '</tr>';

			}

			function generateTable(filteredData) {
				table_html = "";
				var pulses = [];
				if (filteredData) {
					pulses = filteredData;
				} else {
					pulses = pulseData;
				}

				if (pulses.length > 0) {
					$.each(pulses, function (key, value) {

						if (pulses[key - 1] && (!hasAlphabet(pulses[key - 1].Ordering)) && (!hasAlphabet(value.Ordering)) && pulses[key + 1] && (hasAlphabet(pulses[key + 1].Ordering))) { //next row is sub logic content

							if (key !== 0) {
								table_html += '<tr class="table-separator"><td colspan="5"></td></tr>'; //append separator on top
							}
							generateTableRow(value);

						} else if ((hasAlphabet(value.Ordering)) && pulses[key + 1] && (!hasAlphabet(pulses[key + 1].Ordering))) {
							generateTableRow(value); // append separator on bottom
							table_html += '<tr class="table-separator"><td colspan="5"></td></tr>';
						} else {
							generateTableRow(value);
						}
					});
				} else {
					table_html += '<tr class="animated fadeIn"><td colspan="5">No data matched your search criteria.</td></tr>';
				}

				$('#pulse-table tbody').empty();
				$('#pulse-table tbody').html(table_html);
			}


			function generateChart(chart) {
				$('#respondent-chart .title').html(chart.Title);
				$('#respondent-chart .chart-title').html(chart.Stats[0].Name);
				$('#chart-percentage').html(chart.Stats[0].Percentage + '%');
				$('#count').html(chart.Stats[0].Count);
				$('#total').html(chart.MaxCount);

				var ctx = document.getElementById("doughnut-chart").getContext("2d");
				ctx.canvas.width = 150;
				ctx.canvas.height = 150;
				ctx.canvas.style.width = 150;
				ctx.canvas.style.height = 150;

				var chart_data = [
					{ value: chart.Stats[0].Count, color: "#FF8300" },
					{ value: chart.MaxCount - chart.Stats[0].Count, color: "#6e6e6e" }
				];

				var doughnutChart = new Chart(ctx).Doughnut(chart_data, {
					percentageInnerCutout: 75,
					animationEasing: "linear",
					animateScale: false,
					animationSteps: 50,
					segmentShowStroke: false
				});
			}

			function hasAlphabet(str) {
				return /[a-zA-Z]/.test(str);
			}

			function generateStatResult(options, cardType, answers, pulseId) {

				var html = "";

				if (cardType == 1) {
					html += '<img src="/Img/icon_selectone.png" />';
				} else if (cardType == 2) {
					html += '<img src="/Img/icon_numberrange.png" />';
				} else if (cardType == 3) {
					html += '<img src="/Img/icon_text.png" />';
				} else if (cardType == 4) {
					html += '<img src="/Img/icon_selectone.png" />';
				}

				$('.type-image').html(html);

				result_list_html = "";

				result_list_html += '<ul class="list-group type' + cardType + ' style="width:85%;">';

				if (cardType == 3) {

					$.each(answers, function (key, value) {
						result_list_html += '<li class="list-group-item">';
						result_list_html += '<span class="list-answer-count">' + value.NumberOfAlike + '</span>';
						result_list_html += '<p>' + value.Content + '</p>';
						result_list_html += '</li>';
					});
					result_list_html += '<a style="display:block; text-align:center; margin-top:10px;" href="/DynamicPulse/RespondersAnswerList/' + deckId + '/' + pulseId + '" target="_blank" class="align-center see-more-btn-s" data-cardid="RSC3242685b1e704763894030d857c15439">See more</a>';

				} else {

					$.each(options, function (key, value) {
						result_list_html += '<li class="list-group-item">';
						result_list_html += '<span class="list-value" style="float: none; position: absolute; right: 100px;">' + value.NumberOfSelection + '</span>';
						result_list_html += '<span class="list-percentage color' + (key + 1) + '">' + value.PercentageOfSelection + '%</span>';
						result_list_html += '<p>' + value.Content + '</p>';
						result_list_html += '<div class="progress">';
						result_list_html += '<div class="progress-bar color' + (key + 1) + '" style="width: ' + value.PercentageOfSelection + '%;"></div>';
                        result_list_html += '</div>';
                        if (departmentName == null || departmentName == undefined)
                        {
                            result_list_html += '<a href="/DynamicPulse/RespondersReportList/' + deckId + '/' + pulseId + '/' + value.OptionId + '" class="viewResponder_link" target="_blank" data-optionid="' + value.OptionId + '" style="position:absolute; top:10px; width:20px; right:-50px;">';
                        }
                        else
                        {
                            result_list_html += '<a href="/DynamicPulse/RespondersReportList/' + deckId + '/' + pulseId + '/' + value.OptionId + '/' + departmentName + '" class="viewResponder_link" target="_blank" data-optionid="' + value.OptionId + '" style="position:absolute; top:10px; width:20px; right:-50px;">';
                        }
						
						result_list_html += '<img src="/Img/icon_result.png">';
						result_list_html += '</a>';
						result_list_html += '</li>';
					});

				}

				result_list_html += '</ul>';

				$('#pulse-stat .result-table').empty();
				$('#pulse-stat .result-table').html(result_list_html);
			}

			function refreshPulseStat(pulseId, searchId) {
				$('.card-container').addClass('hidden');
				$('.card-container').removeClass('animated fadeInUp');

				ShowProgressBar();
				$.ajax({
					type: "POST",
					url: apiUrl + 'SelectDeckCardAnalytic',
					data: {
						"CompanyId": companyId,
						"AdminUserId": adminUserId,
						"DeckId": deckId,
						"PulseId": pulseId,
						"SearchId": searchId
					},
					crossDomain: true,
					dataType: 'json',
					success: function (res) {
						HideProgressBar();
						if (res.Success) {
							$('.card-container').removeClass('hidden');
							$('.card-container').addClass('animated fadeInUp');
							$('.error-msg').empty();
							$('#content-wrapper').animate({
								'scrollTop': 0
							}, 1000);

							var stats = res.PulseStats;
							var chart = res.PulseStats.Chart;
							var options = stats.Pulse.Options;

							generateChart(chart);

							pulseId = stats.Pulse.PulseId;

							if (stats.Pulse.ParentCard && stats.Pulse.ParentOption) {
								toggleAdditionalHeader(stats.Pulse.ParentCard, stats.Pulse.ParentOption);
							} else {
								if (!$('.additional-header').hasClass('hidden')) {
									$('.additional-header').addClass('hidden');
								}
							}

							$('#pulse-stat .stat-title').html('RP' + stats.Pulse.Ordering);
							$('#pulse-stat .stat-code').html('Code ' + stats.Pulse.PulseId);
							$('.question').html(stats.Pulse.Title);

							if (stats.Pulse.Description && stats.Pulse.Description != "") {
								$('.desc-label .desc').css('display', 'block');
								$('.desc').html(stats.Pulse.Description);
							} else {
								$('.desc-label .desc').css('display', 'none');
							}

							if ((options && options.length > 0) || (stats.Pulse.TextAnswers && stats.Pulse.TextAnswers.length > 0)) {
								$('#pulse-stat .error-msg').css('display', 'none');
								$('#pulse-stat .card-stat').css('display', 'block');
								generateStatResult(options, stats.Pulse.CardType, stats.Pulse.TextAnswers, stats.Pulse.PulseId);
							} else {
								$('#pulse-stat .error-msg').css('display', 'block');
								$('#pulse-stat .card-stat').css('display', 'none');
							}

						} else {

							$('#content-wrapper').animate({
								'scrollTop': 0
							}, 1000);

							$('.error-msg').empty();
							$('.error-msg').append('<p class="align-center error-msg" style="margin-top:70px; margin-bottom:70px; width:100%;">No analytic for the selected pulse. </p>');
						}
					}
				});
			}

			function toggleAdditionalHeader(source, option) {
				var source_content = 'RP' + source.Ordering + ": " + source.Title;
				var option_content = option.Content;
				$('.additional-header').removeClass('hidden');
				$('.source p').html(truncateStr(source_content, 50, '...'));
				$('.option p').html(truncateStr(option_content, 50, '...'));
			}

			function setFilterOption(searchTerms) {
				var option = "";
				$.each(searchTerms, function (key, value) {
					option += '<option value="' + value.Id + '" data-name="' + value.Content + '">' + truncateStr(value.Content, 25, '...') + '</option>';
				});
				$('select[name=filter]').empty();
				$('select[name=filter]').append(option);
			}

			function fetchData(searchId) {
				$('#responsive-pulse').addClass('hidden');
				$('#responsive-pulse').removeClass('animated fadeInUp');

				ShowProgressBar();
				$.ajax({
					type: "POST",
					url: apiUrl + 'SelectDeckAnalytic',
					data: {
						"CompanyId": companyId,
						"AdminUserId": adminUserId,
						"DeckId": deckId,
						"SearchId": searchId
					},
					crossDomain: true,
					dataType: 'json',
					success: function (res) {
                        HideProgressBar();
						if (res.Success && res.Deck.Pulses.length > 0 && !_.isEmpty(res.PulseStats)) {
							if (!res.Deck.IsDeckAnonymous && (res.PulseStats.Chart.Stats[0].Count > res.Deck.AnonymityCount)) {
								$('#responsive-pulse').removeClass('hidden');
								$('#responsive-pulse').addClass('animated fadeInUp');
								var deck = res.Deck;
								var pulses = res.Deck.Pulses;

								var stats = res.PulseStats;
								var chart = res.PulseStats.Chart;
								var options = stats.Pulse.Options;
								var searchTerms = res.SearchTerms;

								pulseData = pulses;

								generateChart(chart);

								pulseId = stats.Pulse.PulseId;

								setStatus(deck.Status);
								setFilterOption(searchTerms);

								if (stats.Pulse.ParentCard && stats.Pulse.ParentOption) {
									toggleAdditionalHeader(stats.Pulse.ParentCard, stats.Pulse.ParentOption);
								} else {
									if (!$('.additional-header').hasClass('hidden')) {
										$('.additional-header').addClass('hidden');
									}
								}

								$('.pulse-title').html(deck.Title);
								$('a.pulse-title').attr('href', '/DynamicPulse/ResponsivePulse/' + deck.Id);

								$('#pulse-stat .stat-title').html('RP' + stats.Pulse.Ordering);
								$('#pulse-stat .stat-code').html('Code ' + stats.Pulse.PulseId);
								$('.question').html(stats.Pulse.Title);
								$('.start-date').html(deck.DeckStartTimestampString);

								if (deck.DeckEndTimestampString) {
									$('.end-date').html(' to ' + deck.DeckEndTimestampString);
								} else {
									$('.end-date').html('until now');
								}

								if (newPulseId == "") {
									if ((options && options.length > 0) || (stats.Pulse.TextAnswers && stats.Pulse.TextAnswers.length > 0)) {
										$('#pulse-stat .error-msg').css('display', 'none');
										$('#pulse-stat .card-stat').css('display', 'block');
										generateStatResult(options, stats.Pulse.CardType, stats.Pulse.TextAnswers, stats.Pulse.PulseId);
									} else {
										$('#pulse-stat .error-msg').css('display', 'block');
										$('#pulse-stat .card-stat').css('display', 'none');
									}
								}

								generateTable();
							}
							else {
								var html = "";
								html += "<div style='width: 100%; margin-top: 70px; text-align: center;'>";
								html += "   <div style='width: 60%; margin: 0px auto;'>";

								html += "       <div style='text-align: left; display: inline-block; float: left;'>";
								html += "           <lable style='color: rgba(80, 80, 80, 1); font-size: 1.5em; font-weight: bold;'>";
								html += "               " + res.Deck.Title;
								html += "           </lable>";
								html += "           <div style=' font-size: 1.2em; margin-top: 10px;'>";
								html += "               <span style='color: rgba(80, 80, 80, 1); display: inline-block;'>Showing date from</span>";
								html += "               <lable style='color: rgba(80, 80, 80, 1); font-weight: bold; display: inline-block;'>";
								html += "                   " + res.Deck.DeckStartTimestampString;
								html += "               </lable>";
								html += "               <span style='color: rgba(80, 80, 80, 1); display: inline-block;'> to </span>";
								html += "               <lable style='color: rgba(80, 80, 80, 1); font-weight: bold; display: inline-block;'>";
								html += "                   " + res.Deck.DeckEndTimestampString;
								html += "               </lable>";
								html += "           </div>";
								html += "       </div>";

								html += "       <div style='color: rgba(80, 80, 80, 1); float: right; display: inline-block; text-align: left;'>";
								if (res.Deck.Status == 1) {
									html += "           <i class='fa fa-circle' style='color: rgb(254, 202, 2); font-size: 1.5em;'></i><span style='font-size: 1.5em;'> Upcoming</span>";
								}
								else if (res.Deck.Status == 2) {
									html += "           <i class='fa fa-circle' style='color: rgb(148, 207, 0); font-size: 1.5em;'></i><span style='font-size: 1.5em;'> Live</span>";
								}
								else if (res.Deck.Status == 3) {
									html += "           <i class='fa fa-circle' style='color: rgb(20, 64, 79); font-size: 1.5em;'></i><span style='font-size: 1.5em;'> Completed</span>";
								}
								else {
									// do nothing
								}
								html += "           <div style='font-size: 1.2em; margin-top: 10px;'>";
								html += "               <span style='color: rgba(80, 80, 80, 1); display: inline-block;'>End date </span>";
								html += "               <lable style='color: rgba(80, 80, 80, 1); font-weight: bold; display: inline-block;'>";
								html += "                   " + res.Deck.DeckEndTimestampString;
								html += "               </lable>";
								html += "           </div>";

								html += "       </div>";
								html += "   </div>";
								html += "   <div style='font-size: 1.2em; margin-top: 200px;'>";
								html += "       <span style='padding: 20px 50px; border: solid 1px rgba(63, 209, 182, 1); background-color: rgba(63, 209, 182, 1); border-radius: 20px; color: #FFF; font-size: 1.2em;'>Pulse is on Anonymous " + res.Deck.AnonymityCount + "</span>";
								html += "   </div>";
								html += "   <label style='font-size: 1.2em; margin-top: 50px;'>Analytics is currently unavailable as there are less than " + res.Deck.AnonymityCount + " participating personnel</label>";
								html += "</div>";
								$('#content-wrapper').prepend(html);
							}
						} else {
							//$('#responsive-pulse').empty();
							$('#content-wrapper').prepend('<p class="align-center" style="margin-top:70px; width:100%;">No analytic for the selected pulse. </p>');
						}
					}
				});
			}

			fetchData();

			if (newPulseId != "") {
				refreshPulseStat(newPulseId);
			}

			$(document).on('click', '.clickable', function (e) {
				e.preventDefault();
				var pulseId = $(this).data('pulseid');
				refreshPulseStat(pulseId);
			});

			$(document).on('click', '.detailBtn', function (e) {
				e.preventDefault();
				console.log("detail");
			});

            $('select[name=filter]').on('change', function () {
				var selected_filter = $(this).find(':selected').data('name');
                searchId = $(this).val();

                if ($(this).prop('selectedIndex') != 0)
                {
                    departmentName = selected_filter;
                }

				$('.filterBy').html(truncateStr(selected_filter, 25, '...'));
				fetchData(searchId);
			});

			var timer;
			var delay = 600;

			$('input[name=search]').bind('input', function () {
				window.clearTimeout(timer);
				var this_element = $(this);
				timer = window.setTimeout(function () {

					var searchText = this_element.val().toLowerCase();

					if (searchText) {
						var filteredArr = _.filter(pulseData, function (elem) {
							console.log(elem.Title.toLowerCase());
							return elem.Title.toLowerCase().indexOf(searchText) > 0;
						});
						generateTable(filteredArr);
					} else {
						generateTable();
					}

				}, delay);

			});

		});
	</script>
</asp:Content>
