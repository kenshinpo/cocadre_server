﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ResponsivePulseCreate.aspx.cs" Inherits="AdminWebsite.DynamicPulse.ResponsivePulseCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
	<link href="/css/ion.rangeSlider/ion.rangeSlider.css" rel="stylesheet" />
	<link href="/css/ion.rangeSlider/ion.rangeSlider.skinNice.css" rel="stylesheet" />
	<style type="text/css">
		.ui-autocomplete-loading {
			background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
		}

		.invalid-field {
			border-bottom-color: #ff0000 !important;
		}

		/* CSS for on-off switch */
		.onoffswitch {
			position: relative;
			width: 76px;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
		}

		.onoffswitch-checkbox {
			display: none;
		}

		.onoffswitch-label {
			display: block;
			overflow: hidden;
			cursor: pointer;
			border-radius: 20px;
		}

		.onoffswitch-inner {
			display: block;
			width: 200%;
			margin-left: -100%;
			transition: margin 0.3s ease-in 0s;
		}

			.onoffswitch-inner:before, .onoffswitch-inner:after {
				display: block;
				float: left;
				width: 50%;
				height: 30px;
				padding: 0;
				line-height: 30px;
				font-size: 13px;
				color: white;
				font-weight: bold;
				box-sizing: border-box;
			}

			.onoffswitch-inner:before {
				content: "YES";
				padding-left: 10px;
				background-color: #709EEB;
				color: #F2F2F2;
			}

			.onoffswitch-inner:after {
				content: "NO";
				padding-right: 10px;
				background-color: #4d4d4d;
				color: #F2F2F2;
				text-align: right;
			}

		.onoffswitch-switch {
			display: block;
			width: 26px;
			height: 26px;
			margin: 2px;
			background: #F2F2F2;
			position: absolute;
			bottom: 0px;
			right: 46px;
			border-radius: 20px;
			transition: all 0.3s ease-in 0s;
		}

		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
			margin-left: 0;
		}

		.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
			right: 0px;
		}

		.input-field {
			background-color: transparent !important;
			border: none !important;
			border-bottom: 1px solid #ddd !important;
		}

		.duration_tip, .anonymous_tip {
			position: absolute;
			/*top: 0px;*/
			right: 0px;
			opacity: 0;
			display: block !important;
			background: #ffffff;
			width: 275px;
			border: 1px solid #ccc;
			padding: 10px;
			z-index: -1000;
			box-shadow: 0px 0px 10px #cccccc;
			transition: all 0.3s ease;
			-webkit-transition: all 0.3s ease;
			-ms-transition: all 0.3s ease;
			-moz-transition: all 0.3s ease;
		}

		.form__row.duration__tip:hover .duration_tip {
			opacity: 1;
		}

		.tooltip-icon:hover + .anonymous_tip {
			opacity: 1;
			z-index: 1000;
		}

		/*

		*/
		button.pulse-type {
			background-color: white;
			color: #ccc;
		}

			button.pulse-type.active {
				background-color: #3f75d2;
				color: #fff;
			}

		select.card-publish-date {
			border: none;
			border-bottom: 1px solid #ccc;
			color: #ccc;
		}

		button.to-mini-card, button.duplicate-card, button.cascade-card {
			background: none;
			color: #4683ea;
			border: none;
			padding: 0 0 0 10px;
		}

		.card-level-2 {
			margin-left: 1.5em;
		}

		.card-level-3 {
			margin-left: 3em;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

	<div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

	<div id="plSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
		<h1 class="popup__title">Select Department</h1>
		<div class="popup__content">
			<fieldset class="form">
				<div class="container">
					<div class="accessrights">
						<input id="selectAllDepartment" type="checkbox" onclick="selectAllDepartments(this);" />
						<label for="selectAllDepartment">Select All</label>
					</div>
					<hr />
					<div class="accessrights departments">
						<span id="department_checkboxlist">
							<label>
								<input type="checkbox" value="D04461505c52d44d8be0ff0d80c6a2408" />Human Resource</label><br />
						</span>
					</div>
				</div>
			</fieldset>
		</div>
		<div class="popup__action">
			<a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:saveDepartmentSelection()">Select</a>
			<a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDepartmentSelector()">Cancel</a>
		</div>
	</div>

	<asp:HiddenField runat="server" ID="hfAdminUserId" />
	<asp:HiddenField runat="server" ID="hfCompanyId" />
	<asp:HiddenField runat="server" ID="hfTimezone" />
	<asp:HiddenField runat="server" ID="hfDeckId" />
	<asp:HiddenField runat="server" ID="hfDeck" />
	<asp:HiddenField runat="server" ID="hfPulseList" />

	<div class="data__content">

		<!-- App Bar -->
		<div class="appbar">
			<div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
			<div class="appbar__title"><a href="/DynamicPulse/ResponsivePulseFeedList" style="color: #000;">Dynamic <span>Pulse</span></a></div>
			<div class="appbar__meta">
				<asp:Literal ID="ltlActionName" runat="server" Text="Add 'Responsive' Pulse" />
			</div>
			<div class="appbar__action">
			</div>
		</div>
		<!-- / App Bar -->

		<div class="responsive-pulse-root">
			<div class="container">

				<div class="card card-header grid">
					<div class="grid__inner" style="background-color: transparent;">
						<!-- PULSE LOGO START -->
						<div class="grid__span--2">
							<div style="text-align: center;">
								<img id="main_content_imgTopic" style="width: 120px; height: 120px;" src="/Img/icon_responsive_pulse.png" />
							</div>
						</div>
						<!-- PULSE LOGO END -->

						<div class="grid__span grid__span--10 grid__span--last" style="background-color: transparent;">
							<!-- CARD HEADER (ROW HEADER) START -->
							<div class="grid__inner" style="background-color: transparent;">

								<div style="float: right; width: 50%;">
									<div class="grid__span--11">
										<button id="lbApplyChanges" type="button" class="survey-bar__search__button">Create</button>
										<a class="survey-bar__search__button" href="/DynamicPulse/ResponsivePulseFeedList">Cancel</a>
									</div>
								</div>

								<div class="tabs tabs--styled">
									<ul class="tabs__list">
										<li class="tabs__list__item">
											<a class="tabs__link" href="#pulse-deck-info">Deck  Info</a>
										</li>
										<li class="tabs__list__item">
											<a class="tabs__link" href="#pulse-deck-privacy">Privacy</a>
										</li>
									</ul>
									<div class="tabs__panels">
										<!-- PULSE DECK INFO (CONTENT) START -->
										<div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-info">
											<div class="grid__inner" style="background-color: transparent;">
												<div class="grid__span--8">

													<div class="">
														<div class="form__row">
															<label for="deckTitle" style="width: auto; font-size: 1.2em; font-weight: bold; margin-right: 20px; vertical-align: middle; display: inline-block;">Title</label>
															<div id="div_deckTitle" style="border-width: 0px 0px 1px; border-style: solid; border-color: rgb(221, 221, 221); border-image: none; width: 80%; vertical-align: middle; display: inline-block; position: relative;">
																<input id="deckTitle" class="text-placeholder" style="border: currentColor; border-image: none; width: 97%; margin: 0px;" onkeyup="textCounter(this, 'deckTitleCounter', DECK_TITLE_COUNT_MAX);" type="text" placeholder="Title of deck" />
																<span id="deckTitleCounter" style="right: 5px; bottom: 10px; color: #DDD; position: absolute;">50</span>
															</div>
														</div>

														<div class="form__row">
															<div class="grid">
																<div class="grid__inner" style="background-color: transparent;">
																	<div class="grid__span--8">
																		<h3>Publish method</h3>

																		<h5>
																			<label style="display: inline; margin-left: 10px;">
																				<input type="radio" name="publish_method" class="publish_method publish_method-schedule" value="1" checked="checked" />
																				Schedule
																			</label>
																			<label style="display: inline; margin-left: 10px;">
																				<input type="radio" name="publish_method" class="publish_method publish_method-routine" value="2" />
																				Routine
																			</label>
																		</h5>

																		<div>
																			<span style="display: inline;">
																				<span style="width: 70px; text-align: right; display: inline-block;">Start date</span>
																				<input type="text" id="deckStartDate" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" class="input-field" />
																				<span>at</span>
																				<input id="deckStartHH" style="border-width: 0px 0px 1px; border-style: none none solid; border-color: currentColor currentColor rgb(221, 221, 221); border-image: none; width: 4em; display: inline;" onchange="validateHour(this);" type="text" maxlength="2" placeholder="hh" value="12" />
																				<span>:</span>
																				<input type="text" id="deckStartMM" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" class="input-field" value="00" />
																				<select id="deckStartMer" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 12px;">
																					<option value="am">am</option>
																					<option value="pm">pm</option>
																				</select>
																			</span>
																		</div>

																		<div class="publish_method schedule-settings">
																			<span style="width: 70px; text-align: right; display: inline-block;">End date</span>
																			<label style="display: inline;">
																				<span style="display: inline;">
																					<input type="text" id="deckEndDate" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" class="input-field" />
																					<span>at</span>
																					<input type="text" id="deckEndHH" maxlength="2" style="width: 4em; display: inline;" placeholder="hh" onchange="validateHour(this);" class="input-field" value="11" />
																					<span>:</span>
																					<input type="text" id="deckEndMM" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" class="input-field" value="59" />
																					<select id="deckEndMer" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 12px;">
																						<option value="am">am</option>
																						<option value="pm" selected="selected">pm</option>
																					</select>
																				</span>
																			</label>
																		</div>

																		<div class="publish_method routine-settings" style="display: none;">
																			<select id="routine_card" style="display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px; width: 100px;">
																				<option value="1" selected="selected">1 pulse</option>
																				<option value="2">2 pulses</option>
																				<option value="3">3 pulses</option>
																				<option value="4">4 pulses</option>
																				<option value="5">5 pulses</option>
																				<option value="6">6 pulses</option>
																				<option value="7">7 pulses</option>
																			</select>
																			<select id="routine_period" style="display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px; width: 100px; margin-left: 15px;">
																				<option value="1" selected="selected">Per day</option>
																				<option value="2">Per week</option>
																				<option value="3">Per month</option>
																			</select>
																			<br />
																			<select id="routine_interval_for_week" style="border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px; display: none; width: 220px;">
																				<option value="1" selected="selected">Every Monday</option>
																				<option value="2">Every Tuesday</option>
																				<option value="3">Every Wednesday</option>
																				<option value="4">Every Thursday</option>
																				<option value="5">Every Friday</option>
																				<option value="6">Every Saturday</option>
																				<option value="7">Every Sunday</option>
																			</select>
																			<select id="routine_interval_for_moonth" style="border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px; display: none; width: 220px;">
																				<option value="1" selected="selected">First work day of the month</option>
																				<option value="2">Last work day of the month</option>
																				<option value="3">Particular date of the month</option>
																			</select>
																			<br />
																			<div>
																				<span style="color: #C1C1C1; margin-right: 20px;">Duration</span>
																				<select id="routine_duration" style="border: none; border-bottom: 1px solid #ddd; padding-bottom: 10px; width: 220px; display: inline;">
																					<option value="-1" selected="selected">Infinite</option>
																					<option value="1">1 day</option>
																					<option value="2">2 days</option>
																					<option value="3">3 days</option>
																					<option value="4">4 days</option>
																					<option value="5">5 days</option>
																					<option value="6">6 days</option>
																					<option value="7">7 days</option>
																					<option value="8">8 days</option>
																					<option value="9">9 days</option>
																					<option value="10">10 days</option>
																					<option value="11">11 days</option>
																					<option value="12">12 days</option>
																					<option value="13">13 days</option>
																					<option value="14">14 days</option>
																					<option value="15">15 days</option>
																					<option value="16">16 days</option>
																					<option value="17">17 days</option>
																					<option value="18">18 days</option>
																					<option value="19">19 days</option>
																					<option value="20">20 days</option>
																					<option value="21">21 days</option>
																					<option value="22">22 days</option>
																					<option value="23">23 days</option>
																					<option value="24">24 days</option>
																					<option value="25">25 days</option>
																					<option value="26">26 days</option>
																					<option value="27">27 days</option>
																					<option value="28">28 days</option>
																					<option value="29">29 days</option>
																					<option value="30">30 days</option>
																					<option value="60">2 months</option>
																					<option value="90">3 months</option>
																				</select>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<p>
															&nbsp;
														</p>
													</div>
												</div>
												<div class="grid__span--4">
													<div class="">

														<div class="form__row" style="display: none;">
															<label style="font-weight: 700; float: left"><span>Compulsory </span></label>
															<i class="fa fa-info-circle tooltip-icon" style="color: rgb(230, 230, 230); font-size: 1.4em; margin-left: 5px;"></i>
															<div class="anonymous_tip" style="display: none; top: 20px;">
																<label style="font-weight: 700;">Compulsory</label>
																Card must be answered and acknowledged to submit. If not, it will be shifted to the back of the pile of Pulse cards.
															</div>
															<div class="mdl-selectfield">
																<select id="select_compulsory" class="" style="display: inline-block; padding-bottom: 4px;">
																	<option value="false">No, default</option>
																	<option value="true" selected="selected">Yes</option>
																</select>
															</div>
														</div>

														<div class="form__row" style="display: none;">
															<label style="font-weight: 700;">Status</label>
															<div class="mdl-selectfield">
																<select id="select_status" class="" style="display: inline-block; padding-bottom: 4px;">
																	<option value="1" selected="selected">Unlisted</option>
																	<option value="2">Active</option>
																	<option value="3">Hidden</option>
																</select>
															</div>
														</div>


														<div class="form__row">
															<label style="font-weight: 700; float: left">
																<i class="fa fa-star" style="color: #FFC700"></i>
																<span>Priority Overrule </span>
																<%--<i class="fa fa-info-circle" style="color: rgb(230, 230, 230); font-size: 1.4em;"></i>--%>
															</label>
															<div class="mdl-selectfield">
																<select id="select_priority" style="display: inline-block; padding-bottom: 4px;">
																	<option value="false" selected="selected">No</option>
																	<option value="true">Yes</option>
																</select>
															</div>
														</div>

														<div class="form__row anonymous__tip">
															<%--<asp:CheckBox ID="cbIsAnonymous" runat="server" Text="Anonymous Pulse" CssClass="setting-checkbox" Style="float: left;" />--%>
															<label style="font-weight: 700; display: inline-block;">Anonymity</label>
															<i class="fa fa-info-circle tooltip-icon" style="color: rgb(230, 230, 230); font-size: 1.4em; margin-left: 5px;"></i>
															<div class="anonymous_tip" style="display: none; top: 20px;">
																<img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />
																<label style="font-weight: 700;">Anonymous Pulse</label>
																Participant's detail will not be disclosed in the Final Report.
															</div>
														</div>

														<div class="mdl-selectfield" style="width: 100%; margin-top: 1.5em;">
															<select id="anonymity_count" style="display: inline-block; padding-bottom: 4px;">
																<option value="0" selected="selected">OFF</option>
																<option value="1">Anonymous 1</option>
																<option value="3">Anonymous 3</option>
																<option value="5">Anonymous 5</option>
																<option value="7">Anonymous 7</option>
																<option value="9">Anonymous 9</option>
																<option value="11">Anonymous 11</option>
															</select>


															<%--<asp:DropDownList ID="ddlAnonymityCount" runat="server">
																<asp:ListItem Text="OFF" Value="0" Selected="True" />
																<asp:ListItem Text="Anonymous 1" Value="1" />
																<asp:ListItem Text="Anonymous 3" Value="3" />
																<asp:ListItem Text="Anonymous 5" Value="5" />
																<asp:ListItem Text="Anonymous 7" Value="7" />
																<asp:ListItem Text="Anonymous 11" Value="11" />
															</asp:DropDownList>--%>
														</div>

													</div>
												</div>
											</div>

										</div>
										<!-- PULSE DECK INFO (CONTENT) END -->

										<!-- PULSE DECK PRIVACY (CONTENT) START -->
										<div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-privacy">
											<div id="tab-privacy">
												&nbsp;
											<p>
												<label for="checkbox_privacy">
													<input type="checkbox" id="checkbox_privacy" onchange="checkbox_selected_everyone();" checked="checked" />
													<i class="fa fa-users" style="color: #999999"></i>Everyone
												</label>
											</p>
												<hr />

												<p>
													<label for="checkbox_selected_departments">
														<input type="checkbox" id="checkbox_selected_departments" onchange="checkbox_selected_departments_change();" />
														<i class="fa fa-briefcase" style="color: #999999"></i>Selected departments
													</label>
													<div class="department tags"></div>
													(<span id="selectDepartmentLinkButton" style="color: #C1C1C1; cursor: default;">+ Add more department</span>)
												</p>

												<p>
													<label for="checkbox_selected_personnel">
														<input type="checkbox" id="checkbox_selected_personnel" onchange="checkbox_selected_personnel_change();" />
														<i class="fa fa-user" style="color: #999999"></i>Selected personnel
													</label>
													<div class="personnel tags"></div>

													<input type="text" id="text_selected_personnel" style="width: 300px; border: none; border-bottom: 1px solid #ccc;" />
												</p>

											</div>
										</div>
										<!-- PULSE DECK PRIVACY (CONTENT) END -->
									</div>
								</div>
							</div>
							<!-- CARD HEADER (ROW HEADER) END -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="/Js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
	<script type="text/javascript">

		// General model representing a responsive pulse survey
		function DynamicPulse(deckId) {
			this.ActionTaken = 0;
			this.Actions = null;
			this.CardType = 0;
			this.DeckId = deckId;
			this.DeletedDate = "0001-01-01T00=00=00";
			this.Description = null;
			this.EndDate = null;
			this.EndDateString = null;
			this.HasIncomingLogic = false;
			this.HasOutgoingLogic = false;
			this.ImageUrl = null;
			this.IsAnonymous = false;
			this.AnonymityCount = 0;
			this.IsComplusory = false;
			this.IsForDepartment = false;
			this.IsForEveryone = false;
			this.IsForUser = false;
			this.IsPrioritized = false;
			this.LogicLevel = 1;
			this.MaxRangeLabel = null;
			this.MinRangeLabel = null;
			this.NumberOfOptions = 0;
			this.Option1Content = null;
			this.Option1Id = null;
			this.Option1NextCardId = null;
			this.Option2Content = null;
			this.Option2Id = null;
			this.Option2NextCardId = null;
			this.Option3Content = null;
			this.Option3Id = null;
			this.Option3NextCardId = null;
			this.Option4Content = null;
			this.Option4Id = null;
			this.Option4NextCardId = null;
			this.Option5Content = null;
			this.Option5Id = null;
			this.Option5NextCardId = null;
			this.Option6Content = null;
			this.Option6Id = null;
			this.Option6NextCardId = null;
			this.Option7Content = null;
			this.Option7Id = null;
			this.Option7NextCardId = null;
			this.Option8Content = null;
			this.Option8Id = null;
			this.Option8NextCardId = null;
			this.Options = null;
			this.Ordering = null;
			this.ParentCard = null;
			this.ParentOption = null;
			this.ProgressStatus = 0;
			this.PulseId = null;
			this.PulseType = 0;
			this.QuestionType = 0;
			this.RangeType = 0;
			this.RespondentChart = null;
			this.SavedDate = "0001-01-01T00=00=00";
			this.SelectedOption = null;
			this.SelectedRange = 0;
			this.StartDate = moment().utc();
			this.StartDateString = null;
			this.Status = 0;

			this.TargetedDepartments = null;
			this.TargetedUsers = null;
			this.TextAnswer = null;
			this.TextAnswers = null;
			this.Title = null;
			this.TotalRespondents = 0;

			// Custom update
			this.Options = [];
			this.UseCustomImage = false;
			this.CustomImageDataUrl = "";

			// this.PulseId = "undefined" + ($(".card-list .card-level-1").length + 1).toString();
			this.PulseId = "undefined" + ($(".card-list .card").length + 1).toString();
		}

		function PulseDeck() {
			this.deckId = $("#main_content_hfDeckId").val();
			this.adminUserId = $("#main_content_hfAdminUserId").val();
			this.companyId = $("#main_content_hfCompanyId").val();
			this.title = $("#deckTitle").val();
			this.deckType = "";
			this.isCompulsory = ($("#select_compulsory").val() === "true");
			this.isAnonymous = $("#main_content_cbIsAnonymous").prop("checked");
			this.anonymityCount = $("#anonymity_count").val();
			this.publishMethodType = $('input[name=publish_method]:checked').val(); //$(".publish_method:checked").val();
			this.status = parseInt($("#select_status").val());
			this.startDate = $("#deckStartDate").val() + " " + $("#deckStartHH").val() + "=" + $("#deckStartMM").val() + " " + $("#deckStartMer").val();
			this.endDate = $("#deckEndDate").val() + " " + $("#deckEndHH").val() + "=" + $("#deckEndMM").val() + " " + $("#deckEndMer").val();
			this.isPrioritized = false;
			this.targetedDepartmentIds = [];
			this.targetedUserIds = [];
			this.numberOfCardsPerTimeFrame = 0;
			this.perTimeFrameType = 0;
			this.privacy = new Privacy();
			this.periodRule = 0;
			this.Pulses = [];
			this.action = 1;
			this.duration = -1;
		}

		function DeckCardOption() {
			this.OptionId = null;
			this.Content = null;
			this.NextPulse = null;
			this.NumberOfSelection = 0;
			this.PercentageOfSelection = 0;
		}

		function Privacy() {
			this.everyone = true;
			this.selectd_department = false;
			this.selected_personnel = false;
		}

		var model = {
			deck: new PulseDeck()
		};

		var DECK_TITLE_COUNT_MAX = 50;
		var startDateString, endDateString;
		var departmentList;

		////////////////////////////////////////////////////////////////////////////////

		// Privacy related javascript functions -- START

		function checkbox_selected_everyone() {
			// Update UI
			$("#checkbox_selected_departments").prop("checked", false);
			$("#checkbox_selected_personnel").prop("checked", false);
		}

		function checkbox_selected_departments_change() {
			if ($("#checkbox_selected_departments").is(":checked")) // Departments is included in Privacy.
			{
				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selectd_department = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
				$("#selectDepartmentLinkButton").css("color", "blue");
				$("#selectDepartmentLinkButton").css("cursor", "pointer");
				$("#selectDepartmentLinkButton").click(displayDepartmentSelector);
			}
			else // Departments is not included in Privacy.
			{
				// Update Model
				model.deck.privacy.selectd_department = false;

				// Update UI
				$("#selectDepartmentLinkButton").css("color", "#C1C1C1");
				$("#selectDepartmentLinkButton").css("cursor", "default");
				$("#selectDepartmentLinkButton").prop('onclick', null).off('click');
			}
		}

		function displayDepartmentSelector() {
			var companyId, adminUserId, ajaxPromise;

			companyId = $("#main_content_hfCompanyId").val();
			adminUserId = $("#main_content_hfAdminUserId").val();

			// Ajax call to fetch 
			ajaxPromise = jQuery.ajax({
				type: "GET",
				url: "/api/Company?userid=" + adminUserId + "&companyid=" + companyId,
				contentType: "application/json; charset=utf-8",
				data: {},
				dataType: "json",
				beforeSend: function (xhr, settings) {
					ShowProgressBar();
				},
				success: function (d, status, xhr) {
					if (d.Success) {
						departmentList = d.Departments;
					}
				},
				error: function (xhr, status, error) {
				},
				complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
					// Render department

					var idx, departmentObj, html, checked, isAllChecked = true;
					$("#department_checkboxlist").children().remove();
					for (idx = 0; idx < departmentList.length; idx++) {
						checked = "";
						if (model.deck.targetedDepartmentIds.indexOf(departmentList[idx].Id) >= 0) {
							checked = "checked=\"checked\"";
						}
						else {
							isAllChecked = false;
						}

						html = "<label><input class=\"cbDepartment " + departmentList[idx].Id + "\" type=\"checkbox\" " + checked + " value=\"" + departmentList[idx].Id + "\" onclick=\"checkbox_selectd_department_click('" + departmentList[idx].Id + "');\" />" + departmentList[idx].Title + "</label>";

						$("#department_checkboxlist").append(html);


						if (isAllChecked) {
							$("#selectAllDepartment").prop("checked", true);
						}
						else {
							$("#selectAllDepartment").prop("checked", false);;
						}
					}

					HideProgressBar();
					$("#mpe_backgroundElement").show();
					$("#plSelectDepartment").show();
				}
			});
		}

		function checkbox_selectd_department_click(departmentId) {
			if ($(".cbDepartment." + departmentId).is(':checked')) {
				var cbDepartments = $("#department_checkboxlist").children();
				var isAllDepartmentChecked = true;
				for (var i = 0; i < cbDepartments.length; i++) {
					console.debug($(".cbDepartment." + $($(cbDepartments[i]).children()[0]).val()).is(':checked'));
					if (!$(".cbDepartment." + $($(cbDepartments[i]).children()[0]).val()).is(':checked')) {
						isAllDepartmentChecked = false;
						break;
					}
				}
				$("#selectAllDepartment").prop("checked", isAllDepartmentChecked);;
			}
			else {
				$("#selectAllDepartment").prop("checked", false);;
			}
		}

		function checkbox_selected_personnel_change() {
			if ($("#checkbox_selected_personnel").is(":checked")) // Personnels is included in Privacy.
			{
				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selected_personnel = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
			}
			else  // Personnels is not included in Privacy.
			{
				// Update Model
				model.deck.privacy.selected_personnel = false;
			}
		}

		function hideDepartmentSelector() {
			$("#mpe_backgroundElement").hide();
			$("#plSelectDepartment").hide();
		}

		function saveDepartmentSelection() {
			var idx, $checkedItems = [], html;

			// Clear targetedDepartmentIds 
			model.deck.targetedDepartmentIds = [];
			$(".department.tags").children().remove();

			// Re-populate targetedDepartmentIds with selected department id
			$checkedItems = $("#plSelectDepartment .departments :checked");
			for (idx = 0; idx < $checkedItems.length; idx++) {
				model.deck.targetedDepartmentIds.push($checkedItems[idx].value);
				html = "";
				html = html + "<div class=\"tag " + $checkedItems[idx].value + "\">";
				html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + $($checkedItems[idx]).parent().text() + "</span>";
				html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + $checkedItems[idx].value + "');\">x</a>";
				html = html + "</div>";
				$(".department.tags").append(html);
			}

			hideDepartmentSelector();

			$("#checkbox_selected_departments").prop("checked", model.deck.targetedDepartmentIds.length > 0);
			$("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
		}

		function removeDepartment(departmentId) {
			var foundIndex = model.deck.targetedDepartmentIds.indexOf(departmentId);

			if (foundIndex >= 0) {
				model.deck.targetedDepartmentIds.splice(foundIndex, 1);

				// Update UI
				$(".department.tags .tag." + departmentId).remove();
			}

			$("#checkbox_selected_departments").prop("checked", model.deck.targetedDepartmentIds.length > 0);
			$("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
		}

		function removePersonnel(personnelId) {
			var foundIndex = model.deck.targetedUserIds.indexOf(personnelId);

			if (foundIndex >= 0) {
				model.deck.targetedUserIds.splice(foundIndex, 1);

				// Update UI
				$(".personnel.tags .tag." + personnelId).remove();
			}

			$("#checkbox_selected_personnel").prop("checked", model.deck.targetedUserIds.length > 0);
		}

		function setPriorityOverrule(input) {
			model.deck.isPrioritized = input.checked;
		}

		// Privacy related javascript functions -- END

		////////////////////////////////////////////////////////////////////////////////

		// Validate publish date time -- START

		function validateDate(input) {
			input.className = input.className.replace("invalid-field", "");
			if (!moment(input.value, "DD/MM/YYYY", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function validateHour(input) {
			input.className = input.className.replace("invalid-field", "");

			if (!moment(input.value, "h", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function validateMinute(input) {
			input.className = input.className.replace("invalid-field", "");

			if (!moment(input.value, "m", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		// Validate publish date time -- END

		function displayCardPreviewImage(input, maxWidth, maxHeight, cardId, optionId) {
			var image = new Image();

			if (input.files && input.files[0]) {
				var t = input.files[0].type;
				var s = input.files[0].size / (1024 * 1024);
				var filename = input.files[0].name;
				var fileExtension = input.files[0].name.substr(input.files[0].name.lastIndexOf(".") + 1);

				if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
					if (s > 5) {
						toastr.error('Uploaded photo exceeded 5MB.');
					} else {
						var fileReader = new FileReader();
						fileReader.addEventListener("load", function () {
							var image = new Image();
							image.src = this.result;

							var card, cardImage, cardid;
							card = $(input).parents(".card");
							card.find(".card-image").css("backgroundImage", "url(" + this.result + ")");
							cardid = card.data("cardid");

							// find model for this pulse and attach
							var model, idx, pulse;
							pulse = getPulse(cardid);
							if (pulse) {
								pulse.UseCustomImage = true;
								pulse.CustomImageDataUrl = {
									base64: this.result,
									extension: fileExtension,
									width: image.width,
									height: image.height
								};
							}

							// display mini-x
							card.find(".remove-image-icon").show();

							// hide attach image icon
							card.find(".upload-option-image").hide();

						}, false);
						fileReader.readAsDataURL(input.files[0]);
					}
				} else {
					toastr.error('File extension is invalid.');
				}
			}
		} // end function displayCardPreviewImage(input, maxWidth, maxHeight) {

		function removeAttachedImg(input) {

			model.UseCustomImage = false;
			model.CustomImageDataUrl = null;

			//$("#pulseCard").css("backgroundImage", "none");
			var card;
			card = $(input).parents(".card");
			card.find(".card-image").css("backgroundImage", "none");

			// hide mini-x
			card.find(".remove-image-icon").hide();
			//$(".remove-image-icon").hide();

			// show attach image icon
			$(".upload-option-image").show();
		}

		function saveToModelDeck() {
			// Save UI component data to model
			// Title
			model.deck.title = $("#deckTitle").val();
			// Compulsory
			model.deck.isCompulsory = ($("#select_compulsory").val() === "true");
			// Anonymous 
			model.deck.isAnonymous = $("#main_content_cbIsAnonymous").prop("checked");
			model.deck.anonymityCount = $("#anonymity_count").val();
			// Publish method
			model.deck.publishMethodType = $('input[name=publish_method]:checked').val(); //$(".publish_method:checked").val();
			// Status
			model.deck.status = parseInt($("#select_status").val());
			// Priority Overrule
			model.deck.isPrioritized = ($("#select_priority").val() === "true");

			// Start Date
			var timezone = parseFloat($("#main_content_hfTimezone").val());
			model.deck.startDate = $("#deckStartDate").val() + " " + $("#deckStartHH").val() + ":" + $("#deckStartMM").val() + " " + $("#deckStartMer").val();
			if (moment(model.deck.startDate, "DD/MM/YYYY HH:mm a", true).isValid()) {
				model.deck.startDate = moment(model.deck.startDate, "DD/MM/YYYY HH:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
			} else {
				model.deck.startDate = null;
			}

			// Publish method 
			if ($('input[name=publish_method]:checked').val() === "1")  // Publish method is "Schedute".
			{
				// End Date
				model.deck.endDate = $("#deckEndDate").val() + " " + $("#deckEndHH").val() + ":" + $("#deckEndMM").val() + " " + $("#deckEndMer").val();
				if (moment(model.deck.endDate, "DD/MM/YYYY HH:mm a", true).isValid()) {
					model.deck.endDate = moment(model.deck.endDate, "DD/MM/YYYY HH:mm a").add(-timezone, 'hours').format("YYYY-MM-DD HH:mm:ss");
				} else {
					model.deck.endDate = null;
				}
			}
			else // Publish method is "Routine".
			{
				// numberOfCardsPerTimeFrame
				model.deck.numberOfCardsPerTimeFrame = $("#routine_card").val();
				// perTimeFrameType
				model.deck.perTimeFrameType = $("#routine_period").val();
				// Duration
				model.deck.duration = $("#routine_duration").val();

				// periodRule
				if (model.deck.perTimeFrameType == 1) // Per day
				{
					model.deck.periodRule = 0;
				}
				else if (model.deck.perTimeFrameType == 2) // Per week
				{
					model.deck.periodRule = $("#routine_interval_for_week").val();
				}
				else if (model.deck.perTimeFrameType == 3) // Per month
				{
					model.deck.periodRule = $("#routine_interval_for_moonth").val();
				}
				else {
					// do nothing
				}
			}

			// Privacy
			if ($("#checkbox_privacy").is(":checked")) // Everyone for Privacy
			{
				model.deck.privacy.everyone = true;
				model.deck.privacy.selectd_department = false;
				model.deck.privacy.selected_personnel = false;
			}
			else {
				model.deck.privacy.everyone = false;
				model.deck.privacy.selectd_department = $("#checkbox_selected_departments").is(":checked");
				model.deck.privacy.selected_personnel = $("#checkbox_selected_personnel").is(":checked");
			}

		}

		function saveToModelCard($card) {
			var pulse, cardId, parentCardId, cardType, optionIdx;
			var options, $pulseOptions;
			cardId = $card.data("cardid");
			parentCardId = $card.data("parentcardid");
			cardType = $card.find("button.pulse-type.active").data("pulsetype");
			pulse = getPulse(cardId);

			if (!pulse) {
				return;
			}

			if (cardType == 4) {
				$pulseOptions = $card.find("input.logic-pulse-option");
			} else {
				$pulseOptions = $card.find("input.pulse-option");
			}

			if (cardType === 2) { // Get range info
				pulse.RangeType = $card.find(".select-range").val();
				pulse.MinRangeLabel = $card.find(".range-pulse.minimum-value-label").val();
				pulse.MaxRangeLabel = $card.find(".range-pulse.maximum-value-label").val();
			} else {
				pulse.RangeType = 0;
				pulse.MinRangeLabel = "";
				pulse.MaxRangeLabel = "";
			}

			pulse.NumberOfOptions = $pulseOptions.length;
			pulse.Options = [];
			for (idx = 0; idx < $pulseOptions.length; idx++) {
				$pulseOption = $($pulseOptions[idx]);
				optionIdx = idx + 1;
				pulse["Option" + optionIdx + "Id"] = "";
				pulse["Option" + optionIdx + "Content"] = $pulseOption.val();
				pulse["Option" + optionIdx + "NextCardId"] = null;
				pulse.Options.push({
					'Content': $pulseOption.val(),
					'NextPulse': null
				});
			}

			pulse.CardType = cardType;
			pulse.Title = $card.find(".pulseCardQuestion").val();
			pulse.Description = $card.find(".pulseCardDescription").val();
			return pulse;
		}

		function convertPulseToAjaxJson(pulse) {
			var ajaxData, idx, parentOptionNumber;

			parentOptionNumber = 0;

			if ((pulse) && (pulse.CardType == 4) && (pulse.ParentCard)) {
				for (idx = 1; idx <= 8; idx++) {
					if (pulse.PulseId == pulse.ParentCard["Option" + idx + "NextCardId"]) {
						parentOptionNumber = idx;
						break;
					}
				}
			}

			ajaxData = {
				'cardId': pulse.PulseId,
				'deckId': pulse.DeckId,
				'adminUserId': $("#main_content_hfAdminUserId").val(),
				'companyId': $("#main_content_hfCompanyId").val(),
				'title': pulse.Title,
				'description': pulse.Description,
				'cardType': pulse.CardType,
				'questionType': 0,
				'startDate': pulse.StartDate,
				'numberOfOptions': pulse.NumberOfOptions,
				'rangeType': pulse.RangeType,
				'imageUrl': "",
				'minRangeLabel': pulse.MinRangeLabel,
				'maxRangeLabel': pulse.MaxRangeLabel,
				'Options': pulse.Options,
				'useCustomImage': pulse ? pulse.UseCustomImage : false,
				'customImageDataUrl': pulse ? pulse.CustomImageDataUrl : "",
				'parentCardId': pulse.ParentCard ? pulse.ParentCard.PulseId : "",
				'parentOptionNumber': parentOptionNumber
			};

			return ajaxData;
		}

		function savePulseCard(button) {
			var card, cardType, ajaxData, idx, cardId, parentCardId, HTTP_METHOD;
			var options, $pulseOption, $pulseOptions;
			var rangeType, minValueLabel, maxValueLabel;
			var pulse, modelPulse;
			var opSuccessful, ajaxPromise;

			opSuccessful = false;
			options = [];
			pulse = saveToModelCard($(button).parents(".card"));
			if (!pulse) {
				toastr.error("Error while saving card data.");
			}

			var ajaxJson = convertPulseToAjaxJson(pulse);

			if ((pulse) && (!pulse.PulseId.startsWith("undefined"))) {
				HTTP_METHOD = "POST";
			} else {
				ajaxJson.cardId = "";
				HTTP_METHOD = "PUT";
			}

			ajaxPromise = jQuery.ajax({
				type: HTTP_METHOD,
				url: "/api/PulseResponsiveCard",
				contentType: "application/json; charset=utf-8",
				data: JSON.stringify(ajaxJson),
				dataType: "json",
				beforeSend: function (xhr, settings) {
					ShowProgressBar();
				},
				success: function (d, status, xhr) {
					opSuccessful = true;
					if (d.Success) {
						if (HTTP_METHOD == "PUT") {
							// replace pulseid of card html class
							// TODO: update pulse id, // data-cardid
							var prevPulseId = pulse.PulseId;

							$(".card." + pulse.PulseId + " .card-code").text("Code " + d.Pulse.PulseId);
							$(".card." + prevPulseId).addClass(d.Pulse.PulseId);
							$(".card." + prevPulseId).data("cardid", d.Pulse.PulseId);
							$(".card." + prevPulseId).attr("data-cardid", d.Pulse.PulseId);
							$(".card." + prevPulseId).removeClass(prevPulseId);
							pulse = d.Pulse;
							modelPulse = getPulse(prevPulseId);
							modelPulse.PulseId = pulse.PulseId;

							toastr.info("Card added.");

						} else {
							toastr.info("Card updated.");
						}
					} else {
						toastr.error(d.ErrorMessage);
					}
				},
				error: function (xhr, status, error) {
					toastr.error(error);
				},
				complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
					HideProgressBar();
				}
			});

			jQuery.when(ajaxPromise).done(function (ajaxResult) {
				opSuccessful = ajaxResult.Success;
				return opSuccessful;
			});
		}

		function textCounter(input, label, maxcounter) {

			//div_titlediv_deckTitle
			$("#" + label + "").html(maxcounter - input.value.length);
			if (input.value.length > maxcounter) {
				$("#div_deckTitle").css("border-color", "red");
				$("#" + label + "").css("color", "red");
			}
			else {
				$("#div_deckTitle").css("border-color", "#DDD");
				$("#" + label + "").css("color", "#DDD");
			}
		}

		function selectAllDepartments(cbSelectAll) {
			$("#department_checkboxlist input[type=checkbox]").prop("checked", cbSelectAll.checked);
		}

		function isDeckDataValid() {
			// Title
			if ($("#deckTitle").val().trim().length == 0) {
				ShowToast('Please give this a title', 2);
				return false;
			}

			if ($("#deckTitle").val().trim().length > DECK_TITLE_COUNT_MAX) {
				toastr.error("Title length maximum is 50.");
				return false;
			}

			// Srart date
			var momentSDate;
			startDateString = $("#deckStartDate").val() + " " + $("#deckStartHH").val() + ":" + $("#deckStartMM").val() + " " + $("#deckStartMer").val();
			if (!moment(startDateString, "DD/MM/YYYY hh:mm a", true).isValid()) {
				startDateString = null;
				ShowToast('Please enter a valid start date', 2);
				return false;
			}
			else {
				momentSDate = moment.utc(startDateString, "DD/MM/YYYY hh:mm a");
			}


			// End date
			var momentEDate;
			if ($('input[name=publish_method]:checked').val() === "1") // Publish method is "Schedute".
			{
				endDateString = $("#deckEndDate").val() + " " + $("#deckEndHH").val() + ":" + $("#deckEndMM").val() + " " + $("#deckEndMer").val();
				if (!moment(endDateString, "DD/MM/YYYY hh:mm a", true).isValid()) {
					endDateString = null;
					ShowToast('Please enter a valid end date', 2);
					return false;
				}
				else {
					momentEDate = moment.utc(endDateString, "DD/MM/YYYY hh:mm a");

					if (momentEDate.isBefore(momentSDate) || momentEDate.isSame(momentSDate)) {
						endDateString = null;
						ShowToast('Your date range is invalid', 2);
						return false;
					}
				}

			}

			// Privacy
			if (!$("#checkbox_privacy").is(":checked") && !$("#checkbox_selected_departments").is(":checked") && !$("#checkbox_selected_personnel").is(":checked")) {
				ShowToast('Please set a privacy setting', 2);
				return false;
			}

			if ($("#checkbox_selected_departments").is(":checked")) // Departments for Privacy
			{
				if (model.deck.targetedDepartmentIds.length == 0) {
					ShowToast('Please select at least one department', 2);
					return false;
				}
			}

			if ($("#checkbox_selected_personnel").is(":checked")) // Personnels for Privacy
			{
				if (model.deck.targetedUserIds.length == 0) {
					ShowToast('Please select at least one personnel', 2);
					return false;
				}
			}

			return true;
		}

		(function ($) {
			"use strict";

			// Privacy personnel related javascript functions -- START

			$("#text_selected_personnel").autocomplete({
				source: "/api/personnel?companyid=" + $("#main_content_hfCompanyId").val() + "&userid=" + $("#main_content_hfAdminUserId").val(),
				minLength: 1,
				response: function (event, ui) {
					for (var i = 0; i < ui.content.length; i++) {
						if (model.deck.targetedUserIds.indexOf(ui.content[i].value) != -1) {
							ui.content.splice(i, 1);
							if (i != ui.content.length - 1) {
								i--;
							}
						}
					}
				},
				select: function (event, ui) {
					var html;
					if (model.deck.targetedUserIds.indexOf(ui.item.value) == -1) {
						model.deck.targetedUserIds.push(ui.item.value);

						html = "";
						html = html + "<div class=\"tag " + ui.item.value + "\">";
						html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + ui.item.label + "</span>";
						html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + ui.item.value + "');\">x</a>";
						html = html + "</div>";
						$(".personnel.tags").append(html);

						$("#checkbox_selected_personnel").prop("checked", model.deck.targetedUserIds.length > 0);
						var everyoneChecked = !($("#checkbox_selected_departments").prop("checked") || $("#checkbox_selected_personnel").prop("checked"));
						$("#checkbox_privacy").prop("checked", everyoneChecked);
					}
					return false;
				},
				focus: function (event, ui) {
					event.preventDefault();
				},
				close: function (event, ui) {
					$("#text_selected_personnel").val("");
				}
			});

			// Privacy personnel related javascript functions -- END

			$(".publish_method").change(function () {
				if (this.value == "1") { // schedule
					// Display end date
					$(".publish_method.schedule-settings").show();
					// Hide routine controls
					$(".publish_method.routine-settings").hide();
				}
				if (this.value == "2") { // routine
					// Hide end date
					$(".publish_method.schedule-settings").hide();
					// Display routine controls
					$(".publish_method.routine-settings").show();
				}
			});

			$(document).ready(function () {

				$("#deckStartDate").datepicker({
					beforeShow: function () {
						setTimeout(function () {
							$('.ui-datepicker').css('z-index', 99999);
						}, 0);
					},
					dateFormat: 'dd/mm/yy'
				});
				$("#deckEndDate").datepicker({
					beforeShow: function () {
						setTimeout(function () {
							$('.ui-datepicker').css('z-index', 99999);
						}, 0);
					},
					dateFormat: 'dd/mm/yy'
				});

				var timezone = parseFloat($("#main_content_hfTimezone").val());
				var nowDateTime = moment.utc().add(timezone, 'hours');

				$("#deckStartDate").val(nowDateTime.add(1, 'days').format("DD/MM/YYYY"));
				$("#deckEndDate").val(nowDateTime.add(29, 'days').format("DD/MM/YYYY"));

				$("button.add-pulse").click(function () {
					var html, card;
					card = new DynamicPulse($("#main_content_hfDeckId").val());
					card.CardType = 1;
					model.deck.Pulses.push(card);
					html = getCardHtml(card);
					$(".card-list").append(html);
				});

				$("#lbApplyChanges").click(function () {
					// Check input
					if (!isDeckDataValid()) {
						return;
					}

					var HTTP_METHOD = "POST";
					saveToModelDeck();

					if ($("#main_content_hfDeckId").val().trim().length > 0) {
						HTTP_METHOD = "POST"; // update all, just status
					} else {
						HTTP_METHOD = "PUT"; // create 
					}

					jQuery.ajax({
						type: HTTP_METHOD,
						url: "/api/PulseResponsiveDeck",
						contentType: "application/json; charset=utf-8",
						data: JSON.stringify(model.deck),
						dataType: "json",
						beforeSend: function (xhr, settings) {
							ShowProgressBar();
						},
						success: function (d, status, xhr) {
							if (d.Success) {
								if (HTTP_METHOD == "PUT") {
									$("#main_content_hfDeckId").val(d.DeckId);
									ShowToast('Deck added', 1);
								} else {
									ShowToast('Deck updated', 1);

								}
								RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
							} else {
								ShowToast(d.ErrorMessage, 2);
							}
						},
						error: function (xhr, status, error) {
							ShowToast(error, 2);
						},
						complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
							HideProgressBar();
						}
					});
				});
			});

		}(jQuery));

	</script>
</asp:Content>
