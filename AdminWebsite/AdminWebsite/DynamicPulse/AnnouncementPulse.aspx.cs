﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.DynamicPulse
{
    public partial class AnnouncementPulse : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            ViewState["manager_user_id"] = managerInfo.UserId;
            ViewState["company_id"] = managerInfo.CompanyId;

            this.hfAdminUserId.Value = ViewState["manager_user_id"].ToString();
            this.hfCompanyId.Value = ViewState["company_id"].ToString();
            this.hfTimezone.Value = Convert.ToString(managerInfo.TimeZone);

            String pulseId = String.Empty;

            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["PulseId"] != null)
                {
                    pulseId = Page.RouteData.Values["PulseId"].ToString().Trim();
                }

                if (!string.IsNullOrWhiteSpace(pulseId))
                {
                    PulseSelectResponse selectResponse = asc.SelectFullDetailAnnouncement(managerInfo.UserId, managerInfo.CompanyId, pulseId);
                    this.hfPulseId.Value = pulseId;

                    if (selectResponse.Success)
                    {
                        if (selectResponse.Pulse.StartDate != null)
                        {
                            selectResponse.Pulse.StartDate = new DateTime(selectResponse.Pulse.StartDate.Year, selectResponse.Pulse.StartDate.Month, selectResponse.Pulse.StartDate.Day, selectResponse.Pulse.StartDate.Hour, selectResponse.Pulse.StartDate.Minute, selectResponse.Pulse.StartDate.Second, DateTimeKind.Utc);//.AddHours(managerInfo.TimeZone);
                        }
                        if (selectResponse.Pulse.EndDate != null)
                        {
                            selectResponse.Pulse.EndDate = new DateTime(selectResponse.Pulse.EndDate.Value.Year, selectResponse.Pulse.EndDate.Value.Month, selectResponse.Pulse.EndDate.Value.Day, selectResponse.Pulse.EndDate.Value.Hour, selectResponse.Pulse.EndDate.Value.Minute, selectResponse.Pulse.EndDate.Value.Second, DateTimeKind.Utc);//AddHours(managerInfo.TimeZone);
                        }

                        hfPulse.Value = javascriptSerializer.Serialize(selectResponse.Pulse);

                        lbViewAnalytics.Visible = true;
                    }
                }
            }
        }

        protected void lbViewAnalytics_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(string.Format("/DynamicPulse/PulseAnalytic/{0}", this.hfPulseId.Value), false);
            }
            catch (Exception ex)
            {

            }
        }
    }
}