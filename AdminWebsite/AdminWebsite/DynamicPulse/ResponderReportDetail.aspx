﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ResponderReportDetail.aspx.cs" Inherits="AdminWebsite.DynamicPulse.ResponderReportDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .indent1 {
            margin-left: 20px;
        }
        .indent2 {
            margin-left: 40px;
        }
        .row-separator {
            border-top: 2px solid #fff;
            border-bottom: 0px;
        }
        .level1-indicator {
            position: absolute;
            top:50%;
            left:0;
            width: 10px;
            height:10px;
            background:#fff;
            box-shadow: 1px 1px 2px #ccc;
            border: 1px solid #ccc;
            border-radius: 50%;
        }
        .level2-indicator {
            width: 20px;
            height:40px;
            border-left:1px solid #ccc;
            border-bottom: 1px solid #ccc;
            position: absolute;
            top:5px;
            left:20px;
        }
        .left-border {
            height:100%;
            width:1px;
            background:#ccc;
            position: absolute;
            left:0px;
            top:0px;
        }
        .list-group-item {
            position: relative;
            padding: 5px 0px;
            font-size: 14px;
            clear: both;
        }
        .list-group-item > .list-value {
            width: 190px;
        }
        .card-col {
            padding: 20px;
        }
        .appbar__meta a {
            color:#000;
        }
        /*.card-col:first-child {
            padding-left: 0px;
        }
        .card-col:last-child {
            padding-right: 0px;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfDeckId" />
    <asp:HiddenField runat="server" ID="hfUserId" />

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><a href="/Survey/List" style="color:#000;">Dynamic <span>pulse</span></a></div>
        <div class="appbar__meta"><a href="" id="survey-root-link">Responsive Pulse</a></div>
        <div class="appbar__meta"><a href="" class="pulse-title"></a></div>
        <div class="appbar__meta"><a href="" class="question"></a></div>
        <div class="appbar__meta breadcrumb-participant" ></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="content-wrapper" class="data" style="overflow-y:scroll;">

        <div id="responsive-pulse" class="container" style="width:100%;">

            <div style="width:100%; margin:20px auto;">

                <div class="title-section" style="width:95%; margin: 0 auto;">
                    <div class="inline-block-mid" style="width:69%;">
                        <h1 class="pulse-title ellipsis-title">Catering food check</h1>
                        <p class="align-left">Showing data from <span class="start-date"></span> <span class="end-date"></span></p>
                    </div>
                    <div class="inline-block-mid " style="width:29%;">
                        <ul class="list-group ">
                            <li class="list-group-item">
                                
                                Name:<span id="user-name" class="list-value"></span>
                            </li>
                            <li class="list-group-item">
                                
                                Department:<span id="user-department" class="list-value"></span>
                            </li>
                            <li class="list-group-item">
                                
                                Last Update:<span id="user-last-update" class="list-value"></span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="report-content">
                    
                </div>
                
            </div>
        </div>
    </div>

    <script>
        $(function () {
         
            var row_html = "";
            var companyId = $('#main_content_hfCompanyId').val();
            var adminUserId = $('#main_content_hfAdminUserId').val();
            var deckId = $('#main_content_hfDeckId').val();
            var userId = $('#main_content_hfUserId').val();

            var apiUrl = '/Api/PulseAnalytics/';

            function hasAlphabet(str) {
                return /[a-zA-Z]/.test(str);
            }

            function getNumOfAlphabet(str) {

                if (hasAlphabet(str)) {
                    return str.match(/[a-zA-Z]+/g)[0].length;
                } else {
                    return 0;
                }
                
            }

            function generateRow(value, indentLevel) {

                row_html = "";
                row_html += '<div class="full-row" style="position:relative;">';

                if (indentLevel == 1) {
                    row_html += '<div class="level1-indicator"></div>';
                } else if (indentLevel == 2) {
                    row_html += '<div class="level2-indicator"></div>';
                }

                row_html += '<div class="card animated no-padding fadeInUp indent' + indentLevel + '" >';
                row_html += '<div class="inline-block-top card-col" style="width: 15%; padding-right:15px;">';
                row_html += '<h1 class="stat-title secondary-label">RP' + value.Ordering+ '</h1>';
                row_html += '<small class="stat-code secondary-label" style="word-break: break-all;line-height: 14px; display: block;">Code ' + value.PulseId + '</small>';
                row_html += '</div>';
                row_html += '<div class="inline-block-top card-col" style="width: 42%;">';
                row_html += '<div style="display:flex;">';

                if (value.ImageUrl) {
                    row_html += '<div class="question-img" style="width:50px; height:50px; background:#ccc;"><img src="' + value.ImageUrl + '" /></div>';
                }

                row_html += '<div class="question-label" style="padding-left: 15px;">';
                row_html += '<p class="question-label secondary-label">Question</p>';
                row_html += '<p class="question">' + value.Title + '</p>';
                row_html += '</div>';
                row_html += '</div>';

                row_html += '</div>';
                row_html += '<div class="inline-block-top card-col align-center" style="width: 17%;">';
                row_html += '<label class="secondary-label">Logic</label>';

                if (value.HasOutgoingLogic) {
                    row_html += '<div style="width:20px; height:20px;margin:5px; display: inline-block;"><img src="/Img/icon_logic_redirect.png" style="width:100%;"/></div>';
                }

                if (value.HasIncomingLogic) {
                    row_html += '<div style="width:20px; height:20px;margin:5px; display: inline-block;"><img src="/Img/icon_logic_destination.png" style="width:100%;"/></div>';
                }
                
                row_html += '</div>';
                row_html += '<div class="inline-block-top card-col align-left" style="width: 25%; border-left:1px solid #ccc; padding-left:20px; min-height:100px;">';
                row_html += '<label class="secondary-label">Answer</label>';

                if (value.CardType == 3) {
                    row_html += '<p>' + value.TextAnswer.Content ? value.TextAnswer.Content : 'No answer.' + '</p>';
                } else if (value.CardType == 2) {
                    row_html += '<p>' + value.SelectedRange ? value.SelectedRange : 'No answer.' + '</p>';
                } else {
                    row_html += '<p>' + value.SelectedOption.Content ? value.SelectedOption.Content : 'No answer.' + '</p>';
                }

                
                row_html += '</div>';


                row_html += '</div>';
                row_html += '</div>';
                
                $('.report-content').append(row_html);
            }

            //var pulses = [
            //    { Ordering: '1' },
            //    { Ordering: '2' },
            //    { Ordering: '3' },
            //    { Ordering: '3A' },
            //    { Ordering: '3AA' },
            //    { Ordering: '3AB' },
            //    { Ordering: '4' },
            //    { Ordering: '4A' },
            //    { Ordering: '4AA' },
            //    { Ordering: '4AB' },
            //    { Ordering: '5' },
            //    { Ordering: '6' }
            //];

            

            function fetchData() {
                $('#responsive-pulse').addClass('hidden');
                $('#responsive-pulse').removeClass('animated fadeInUp');

                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: apiUrl + 'SelectDeckAnalyticByUser',
                    data: {
                        "CompanyId": companyId,
                        "AdminUserId": adminUserId,
                        "DeckId": deckId,
                        "AnsweredByUserId": userId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {
                            $('#responsive-pulse').removeClass('hidden');
                            $('#responsive-pulse').addClass('animated fadeInUp');

                            var deck = res.Deck;
                            var user = res.AnsweredByUser;
                            var pulses = deck.Pulses;
                            

                            $('.start-date').html(deck.DeckStartTimestampString);

                            if (deck.DeckEndTimestampString) {
                                $('.end-date').html('to ' + deck.DeckEndTimestampString);
                            } else {
                                $('.end-date').html('until now');
                            }

                            $('#user-last-update').html(res.LastUpdatedDateString);
                            

                            $('.pulse-title').html(deck.Title);
                            $('#user-name').html(user.FirstName + " " + user.LastName);
                            $('#user-department').html(user.Departments[0].Title);
                            $('#user-last-update').html();

                            if (pulses.length > 0) {
                                $.each(pulses, function (key, value) {

                                    if (pulses[key - 1] && (!hasAlphabet(pulses[key - 1].Ordering)) && (!hasAlphabet(value.Ordering)) && pulses[key + 1] && (hasAlphabet(pulses[key + 1].Ordering))) { //next row is sub logic content

                                        if (key !== 0) {
                                            $('.report-content').append('<hr class="row-separator"/>');
                                        }
                                        generateRow(value, getNumOfAlphabet(value.Ordering));

                                    } else if ((hasAlphabet(value.Ordering)) && pulses[key + 1] && (!hasAlphabet(pulses[key + 1].Ordering))) {
                                        generateRow(value, getNumOfAlphabet(value.Ordering));
                                        $('.report-content').append('<hr class="row-separator" />');
                                    } else {
                                        generateRow(value, getNumOfAlphabet(value.Ordering));
                                    }
                                });
                            }
                        } else {
                            $('#content-wrapper').prepend('<p class="align-center" style="margin-top:70px; width:100%;">No analytic for the selected pulse. </p>');
                        }
                    }
                });
            }

            fetchData();
        });
    </script>

</asp:Content>
