﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;

namespace AdminWebsite.DynamicPulse
{
    public partial class ResponsivePulseFeedList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                ViewState["currentSortField"] = "lbSortPulseDate";
                ViewState["currentSortDirection"] = "asc";

                // Databinding for filter drop down lists

                #region databinding for ddlFilterProgress
                Dictionary<int, string> progressDictionary = new Dictionary<int, string>()
                {
                    { 0, "All" },
                    { 1, "Upcoming" },
                    { 2, "Live" },
                    { 3, "Completed" }
                };

                ddlFilterProgress.DataSource = progressDictionary;
                ddlFilterProgress.DataTextField = "Value";
                ddlFilterProgress.DataValueField = "Key";
                ddlFilterProgress.SelectedValue = "0";
                ddlFilterProgress.DataBind();
                #endregion databinding for ddlFilterProgress

                RefreshPulseListView();
            }

            tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");
        }

        private void RefreshPulseListView()
        {
            ListView lv = this.lvSurvey;

            string adminUserId = this.adminInfo.UserId;
            string companyId = this.adminInfo.CompanyId;

            DateTime? filteredStartDate = null;
            DateTime? filteredEndDate = null;
            string containsName = null;
            int filteredProgress = 0;

            if (!String.IsNullOrWhiteSpace(tbFilterKeyWord.Text))
            {
                containsName = tbFilterKeyWord.Text.Trim();
            }

            if (!string.IsNullOrWhiteSpace(ddlFilterProgress.SelectedValue))
            {
                if (!int.TryParse(ddlFilterProgress.SelectedValue, out filteredProgress))
                {
                    filteredProgress = 0;
                }
            }

            DateTime parsedDateTime;
            string dateString = string.Empty;
            if ((txtDateFrom.Text.Trim().Length > 0) && (txtHourFrom.Text.Trim().Length > 0) && (txtMinuteFrom.Text.Trim().Length > 0))
            {
                dateString = string.Format("{0} {1}:{2} {3}", txtDateFrom.Text.Trim(), txtHourFrom.Text.Trim(), txtMinuteFrom.Text.Trim(), ddlMeridiemFrom.SelectedValue);

                if (DateTime.TryParseExact(dateString, "d/M/yyyy H:m tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDateTime))
                {
                    filteredStartDate = parsedDateTime.AddHours(-adminInfo.TimeZone);
                }
            }
            if ((txtDateTo.Text.Trim().Length > 0) && (txtHourTo.Text.Trim().Length > 0) && (txtMinuteTo.Text.Trim().Length > 0))
            {
                dateString = string.Format("{0} {1}:{2} {3}", txtDateTo.Text.Trim(), txtHourTo.Text.Trim(), txtMinuteTo.Text.Trim(), ddlMeridiemTo.SelectedValue);

                if (DateTime.TryParseExact(dateString, "d/M/yyyy H:m tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDateTime))
                {
                    filteredEndDate = parsedDateTime.AddHours(-adminInfo.TimeZone);
                }
            }

            PulseSelectAllDecksResponse response = asc.SelectAllDeckBasic(
                this.adminInfo.UserId,
                this.adminInfo.CompanyId,
                filteredStartDate, filteredEndDate,
                containsName,
                filteredProgress);


            if (response.Success)
            {
                this.lvSurvey.DataSource = response.Decks;
                this.lvSurvey.DataBind();

                switch (ViewState["currentSortField"].ToString())
                {
                    case "lbSortPulseDate":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderBy(r => r.DeckStartTimestamp).ToList();
                        }
                        else
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderByDescending(r => r.DeckStartTimestamp).ToList();
                        }
                        this.lvSurvey.DataBind();
                        break;

                    case "lbSortPulseTitle":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderBy(r => r.Title).ToList();
                        }
                        else
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderByDescending(r => r.Title).ToList();
                        }
                        this.lvSurvey.DataBind();
                        break;

                    case "lbSortPulseProgress":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderBy(r => r.Progress).ToList();
                        }
                        else
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderByDescending(r => r.Progress).ToList();
                        }
                        this.lvSurvey.DataBind();
                        break;

                    case "lbSortPulseStatus":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderBy(r => r.Status).ToList();
                        }
                        else
                        {
                            this.lvSurvey.DataSource = response.Decks.OrderByDescending(r => r.Status).ToList();
                        }
                        this.lvSurvey.DataBind();
                        break;

                    default:
                        this.lvSurvey.DataSource = response.Decks;
                        this.lvSurvey.DataBind();
                        break;
                }

                surveyCountLiteral.Text = string.Format("Pulse Feed ({0} Decks)", response.Decks.Count);
            }
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void FilterListView(object sender, EventArgs e)
        {
            RefreshPulseListView();
        }

        protected void ibFilterSurvey_Click(object sender, ImageClickEventArgs e)
        {
            RefreshPulseListView();
        }

        protected void lvSurvey_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }

        protected void lvSurvey_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            string pulseType = string.Empty;
            string pulseId = string.Empty;
            string action = string.Empty;
            String toastMsg = String.Empty;

            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    string[] args = e.CommandArgument.ToString().Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if (args.Length == 1)
                    {
                        pulseId = args[0];
                        Response.Redirect("/DynamicPulse/ResponsivePulse/" + pulseId);
                    }
                }
                else if (e.CommandName.Equals("Hide", StringComparison.InvariantCultureIgnoreCase))
                {
                    lbAction.CommandArgument = "3/" + e.CommandArgument;
                    ltlActionName.Text = "Hide ";
                    lbAction.Text = "Hide";
                    ltlActionMsg.Text = "This deck will be hidden. Confirm?";
                    mpePop.Show();
                }
                else if (e.CommandName.Equals("Activate", StringComparison.InvariantCultureIgnoreCase))
                {
                    lbAction.CommandArgument = "2/" + e.CommandArgument;
                    ltlActionName.Text = "Activate ";
                    lbAction.Text = "Activate";
                    ltlActionMsg.Text = "This deck will be active. Confirm?";
                    mpePop.Show();
                }
                else if (e.CommandName.Equals("ViewAnalytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    String js = "window.open('" + string.Format("/DynamicPulse/ResponsivePulseAnalytic/{0}", e.CommandArgument) + "','');";
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "OpenNewTab", js, true);
                }
                else if (e.CommandName.Equals("Delete", StringComparison.InvariantCultureIgnoreCase))
                {
                    lbAction.CommandArgument = "-1/" + e.CommandArgument;
                    ltlActionName.Text = "Delete ";
                    lbAction.Text = "Delete";
                    ltlActionMsg.Text = "This deck will be gone. Confirm?";
                    mpePop.Show();
                }
                else
                {
                    // do bothing
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            string pulseType = string.Empty;
            string deckId = string.Empty;
            string action = string.Empty;
            String toastMsg = String.Empty;

            try
            {
                string[] args = lbAction.CommandArgument.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                if (args.Length == 2)
                {
                    action = args[0];
                    deckId = args[1];
                }

                if (action == "-1")
                {
                    PulseUpdateResponse response = asc.DeleteDeck(adminInfo.UserId, adminInfo.CompanyId, deckId);
                    if (response.Success)
                    {
                        mpePop.Hide();
                        RefreshPulseListView();
                        toastMsg = "Deck deleted.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        Log.Error("DeleteDeck.Success is false. ErrorMessage: " + response.ErrorMessage);
                        mpePop.Show();
                        toastMsg = "Failed to delete deck, please check your internet connection.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                else if (action == "3")
                {
                    PulseUpdateResponse response = asc.UpdateDeckStatus(adminInfo.UserId, adminInfo.CompanyId, deckId, 3);
                    if (response.Success)
                    {
                        mpePop.Hide();
                        RefreshPulseListView();
                        toastMsg = "Pulse is hidden.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        Log.Error("UpdateDeckStatus.Success is false. ErrorMessage: " + response.ErrorMessage);
                        mpePop.Show();
                        toastMsg = "Failed to change status, please check your internet connection.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                else if (action == "2")
                {
                    PulseUpdateResponse response = asc.UpdateDeckStatus(adminInfo.UserId, adminInfo.CompanyId, deckId, 2);
                    if (response.Success)
                    {
                        mpePop.Hide();
                        RefreshPulseListView();
                        toastMsg = "Pulse is active";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        Log.Error("UpdatePulseFeedStatus.Success is false. ErrorMessage: " + response.ErrorMessage);
                        mpePop.Show();
                        toastMsg = response.ErrorMessage; // "Failed to change status, please check your internet connection.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbActionCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        protected void lvSurvey_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {

        }

        protected void lvSurvey_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            this.RefreshPulseListView();
        }

        protected void lvSurvey_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                ListView lv = sender as ListView;
                LinkButton headerLinkButton = null;
                string[] linkButtonControlList = new[] { "lbSortPulseDate", "lbSortPulseTitle", "lbSortPulseStatus", "lbSortPulseProgress" };

                // Remove the up-down arrows from each header
                foreach (string lbControlId in linkButtonControlList)
                {
                    headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                    if (headerLinkButton != null)
                    {
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                    }
                }

                // Add sort direction back to the field in question
                headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                if (headerLinkButton != null)
                {
                    if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                    }
                    else
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                    }
                }


                List<PulseDeck> decks = this.lvSurvey.DataSource as List<PulseDeck>;

                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    #region Srart date - End date
                    LinkButton ltDate = e.Item.FindControl("ltDate") as LinkButton;
                    ltDate.Text = decks[e.Item.DataItemIndex].DeckStartTimestamp.AddHours(adminInfo.TimeZone).ToString("dd/MM/yyyy");
                    if (decks[e.Item.DataItemIndex].DeckEndTimestamp != null)
                    {
                        ltDate.Text += " - " + decks[e.Item.DataItemIndex].DeckEndTimestamp.Value.AddHours(adminInfo.TimeZone).ToString("dd/MM/yyyy");
                    }
                    #endregion

                    #region Progress
                    LinkButton lbProgress = e.Item.FindControl("lbProgress") as LinkButton;
                    if (decks[e.Item.DataItemIndex].Progress == (int)Pulse.ProgressStatusEnum.Upcoming)
                    {
                        lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(254, 204, 0, 1)'></i> Upcoming";
                    }
                    else if (decks[e.Item.DataItemIndex].Progress == (int)Pulse.ProgressStatusEnum.Live)
                    {
                        lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(148, 207, 0, 1)'></i> Live";
                    }
                    else if (decks[e.Item.DataItemIndex].Progress == (int)Pulse.ProgressStatusEnum.Completed)
                    {
                        lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(178, 178, 178, 1)'></i> Completed";
                    }
                    else
                    {
                        // do nothing
                    }
                    #endregion


                    #region Status
                    LinkButton lbStatus = e.Item.FindControl("lbStatus") as LinkButton;
                    if (decks[e.Item.DataItemIndex].Status == (int)Pulse.PulseStatusEnum.Unlisted)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Unlisted";
                    }
                    else if (decks[e.Item.DataItemIndex].Status == (int)Pulse.PulseStatusEnum.Active)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(0, 117, 254, 1)'></i> Active";
                    }
                    else if (decks[e.Item.DataItemIndex].Status == (int)Pulse.PulseStatusEnum.Hidden)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Hidden";
                    }
                    else
                    {
                        // do nothing
                    }
                    #endregion


                    #region Action
                    LinkButton lbHide = e.Item.FindControl("lbHide") as LinkButton;
                    LinkButton lbActivate = e.Item.FindControl("lbActivate") as LinkButton;

                    lbHide.Visible = false;
                    lbActivate.Visible = false;

                    if (decks[e.Item.DataItemIndex].Status == (int)Pulse.PulseStatusEnum.Hidden || decks[e.Item.DataItemIndex].Status == (int)Pulse.PulseStatusEnum.Unlisted)
                    {
                        lbActivate.Visible = true;
                    }
                    else if (decks[e.Item.DataItemIndex].Status == (int)Pulse.PulseStatusEnum.Active)
                    {
                        lbHide.Visible = true;
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvSurvey_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                List<Pulse> pulses = this.lvSurvey.DataSource as List<Pulse>;

                if (pulses == null || pulses.Count == 0)
                {
                    if (e.Item.ItemType == ListViewItemType.EmptyItem)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        if (ddlFilterProgress.SelectedIndex > 0 || !String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                        {
                            if (String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                            {
                                ltlEmptyMsg.Text = @"Filter """ + ddlFilterProgress.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                            }
                            else
                            {
                                ltlEmptyMsg.Text = @"Filter """ + tbFilterKeyWord.Text.Trim() + @""" + """ + ddlFilterProgress.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                            }
                        }
                        else
                        {
                            ltlEmptyMsg.Text = @"Welcome to CoCadre.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding a pulse by clicking on the green button.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}