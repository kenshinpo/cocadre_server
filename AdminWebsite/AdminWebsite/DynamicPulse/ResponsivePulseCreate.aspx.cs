﻿using AdminWebsite.App_Code.Entity;
using System;

namespace AdminWebsite.DynamicPulse
{
    public partial class ResponsivePulseCreate : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            ViewState["manager_user_id"] = managerInfo.UserId;
            ViewState["company_id"] = managerInfo.CompanyId;

            this.hfAdminUserId.Value = ViewState["manager_user_id"].ToString();
            this.hfCompanyId.Value = ViewState["company_id"].ToString();
            this.hfTimezone.Value = Convert.ToString(managerInfo.TimeZone);
        }
    }
}