﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="TestPulseUI.aspx.cs" Inherits="AdminWebsite.DynamicPulse.TestPulseUI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .indent1 {
            display: none;
            margin-left: 20px;
        }
        .indent2 {
            display: none ;
            margin-left: 40px;
        }
        .indent-indicator {
            display: none;
        }
        .row-separator {
            border-top: 2px solid #fff;
            border-bottom: 0px;
            margin-top: 30px;
            margin-bottom: 30px;
        }
        .level1-indicator {
            position: absolute;
            top:50%;
            left:0;
            width: 10px;
            height:10px;
            background:#fff;
            box-shadow: 1px 1px 2px #ccc;
            border: 1px solid #ccc;
            border-radius: 50%;
        }
        .level2-indicator {
            width: 20px;
            height:40px;
            border-left:1px solid #ccc;
            border-bottom: 1px solid #ccc;
            position: absolute;
            top:5px;
            left:20px;
        }
        .left-border {
            height:100%;
            width:1px;
            background:#ccc;
            position: absolute;
            left:0px;
            top:0px;
        }
        .list-group-item {
            position: relative;
            padding: 5px 0px;
            font-size: 14px;
            clear: both;
        }
        .list-group-item > .list-value {
            width: 190px;
        }
        .card-col {
            padding: 20px;
        }
        .appbar__meta a {
            color:#000;
        }
        .toggle-expand, .toggle-hide {
            position:absolute; 
            display:block; 
            top:15px; 
            right:70px; 
            width:25px; 
            height:25px; 
            background: #4683ea; 
            text-align:center; 
            border-radius: 50% 50%; 
            vertical-align:middle;
        }
        
        .toggle-expand i , .toggle-hide i{
            color:#fff;
        }
        .toggle-showdetail, .toggle-hidedetail {
            position:absolute; 
            display:block; 
            top:15px; 
            right:15px;
        }
        .toggle-showdetail i, .toggle-hidedetail i {
            color: #ccc;
        }

        .fake-layer1
        {
            position:absolute;
            width: 100%;
            height:5px;
            bottom:-5px;
            left:0px;
            display:block;
            background: white;
            border-radius: 2px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .fake-layer2 {
            position:absolute;
            width: 98%;
            height:5px;
            bottom:-10px;
            left:2%;
            display:block;
            background: white;
            border-radius: 2px;
            border: 1px solid rgba(0, 0, 0, 0.2);
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .drag-indicator {
            width: 15px;
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            border-left: 2px solid #ccc;
            border-right: 2px solid #ccc;
            height: 100%;
            position: absolute;
            left:0px;
            top:0px;
            vertical-align: middle;
            text-align:center;
            z-index:10;
            cursor:move;
            display:block; /* hide first */
            opacity:0.4;
        }

        .drag-indicator .dot-icon {
            height: 100%;
            display: -webkit-box;  /* OLD - iOS 6-, Safari 3.1-6, BB7 */
            display: -ms-flexbox;  /* TWEENER - IE 10 */
            display: -webkit-flex; /* NEW - Safari 6.1+. iOS 7.1+, BB10 */
            display: flex;         /* NEW, Spec - Firefox, Chrome, Opera */
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .drag-indicator .fa {
            display: block;
            font-size: 6px;
            color:#ccc;
            vertical-align: middle;
            display: block;
            margin: 2px;
        }

        .drag-indicator:hover {
            opacity: 1;
        }

        .toggle-hide, .toggle-hidedetail {
            display:none;
        }
        
        .card {
            min-height: 110px;
        } 

        .date-card {
            min-height: 110px;
            margin: 1.5em auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><a href="/Survey/List" style="color:#000;">TEST <span>UI</span></a></div>
        <div class="appbar__meta"><a href="" id="survey-root-link"></a></div>
        <div class="appbar__meta"><a href="" class="pulse-title"></a></div>
        <div class="appbar__meta"><a href="" class="question"></a></div>
        <div class="appbar__meta breadcrumb-participant" ></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="content-wrapper" class="data" style="overflow-y:scroll;">

        <div class="container" style="width:100%;">

            <div style="width:20%; margin:20px auto; display: inline-block; vertical-align:top; float:left;">
                <div class="date-card index-0 ">
                    <select name="date-index-0" class="date-select">
                        <option>02 May 2016 Tuesday 12:39am</option>
                    </select>
                </div>
                <div class="date-card index-1 ">
                    <select name="date-index-0" class="date-select">
                        <option>02 May 2016 Tuesday 12:39am</option>
                    </select>
                </div>
                <div class="date-card index-2 ">
                    <select name="date-index-0" class="date-select">
                        <option>02 May 2016 Tuesday 12:39am</option>
                    </select>
                </div>
                <div class="date-card index-3 ">
                    <select name="date-index-0" class="date-select">
                        <option>02 May 2016 Tuesday 12:39am</option>
                    </select>
                </div>
                <div class="date-card index-4 ">
                    <select name="date-index-0" class="date-select">
                        <option>02 May 2016 Tuesday 12:39am</option>
                    </select>
                </div>
                <div class="date-card index-5 ">
                    <select name="date-index-0" class="date-select">
                        <option>02 May 2016 Tuesday 12:39am</option>
                    </select>
                </div>
            </div>
            <div style="width:100%; clear:both;">
                <div id="sortableTable">
                    <table>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Desc</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="sortTbody">
                        </tbody>
                    </table>
                </div>
            </div>
            <div style="width:80%; margin:20px auto; display:inline-block;vertical-align:top;float:left;">




                <div id="sortable" class="report-content">
                    
                </div>
                
            </div>
            
        </div>
    </div>

    <!-- Latest Sortable -->
    <script src="http://rubaxa.github.io/Sortable/Sortable.js"></script>
    <script>
        $(function () {
         
            var isDraggable = true;
            var row_html = "";

            function hasAlphabet(str) {
                return /[a-zA-Z]/.test(str);
            }

            function getNumOfAlphabet(str) {

                if (hasAlphabet(str)) {
                    return str.match(/[a-zA-Z]+/g)[0].length;
                } else {
                    return 0;
                }
                
            }

            function mockTable() {
                mock_html = "";

                for (var i = 0; i < 10; i++) {
                    mock_html += '<tr>';
                    mock_html += '<td>No ' + i + '</td>';
                    mock_html += '<td>Title ' + i + '</td>';
                    mock_html += '<td>Desc ' + i + '</td>';
                    mock_html += '<td>Action ' + i + '</td>';
                    mock_html += '</tr>';
                }

                $('#sortableTable table tbody').empty();
                $('#sortableTable table tbody').append(mock_html);

                Sortable.create(sortTbody, {
                    animation: 150,
                    //handle: '.drag-indicator',
                    onEnd: function (e) {
                        console.log(e.oldIndex);
                        console.log(e.newIndex);
                    }
                });
            }

            mockTable();
            var parentOrder = "";

            function generateRow(value, indentLevel) {

                row_html = "";
                child_html = "";
                
                //append fake layer to indicate it has child underneath
                if (value.hasChild) {

                    parentOrder = value.Ordering;

                    row_html += '<div class="full-row parent parent-'+value.Ordering+'" style="position:relative;">';
                    
                    row_html += '<div class="fake-layer fake-layer1"></div>';
                    row_html += '<div class="fake-layer fake-layer2"></div>';
                } else if (indentLevel > 0) {
                    row_html += '<div class="full-row child child-' + parentOrder + '" style="position:relative;">';
                } else {
                    row_html += '<div class="full-row" style="position:relative;">';
                    
                }

                if (indentLevel == 1) {
                    row_html += '<div class="indent-indicator level1-indicator"></div>';
                } else if (indentLevel == 2) {
                    row_html += '<div class="indent-indicator level2-indicator"></div>';
                }

                row_html += '<div class="card no-padding animated fadeInUp indent' + indentLevel + '" >';

                if (indentLevel == 0 || value.hasChild) {
                    row_html += '<div class="drag-indicator animated fadeInLeft"><div class="dot-icon"><i class="fa fa-circle" aria-hidden="true"></i><i class="fa fa-circle" aria-hidden="true"></i><i class="fa fa-circle" aria-hidden="true"></i></div></div>';
                }

                row_html += '<div class="inline-block-top card-col" style="width: 15%; padding-right:15px;">';
                row_html += '<h1 class="stat-title secondary-label">RP' + value.Ordering+ '</h1>';
                row_html += '<small class="stat-code secondary-label" style="word-break: break-all;line-height: 14px; display: block;">Code ' + value.PulseId + '</small>';
                row_html += '</div>';
                row_html += '<div class="inline-block-top card-col" style="width: 37%;">';
                row_html += '<div style="display:flex;">';

                if (value.ImageUrl) {
                    row_html += '<div class="question-img" style="width:50px; height:50px; background:#ccc;"><img src="' + value.ImageUrl + '" /></div>';
                }

                row_html += '<div class="question-label" style="padding-left: 15px;">';
                row_html += '<p class="question-label secondary-label">Question</p>';
                row_html += '<p class="question">' + value.Title + '</p>';
                row_html += '</div>';
                row_html += '</div>';

                row_html += '</div>';
                row_html += '<div class="inline-block-top card-col align-center" style="width: 10%;">';
                row_html += '<label class="secondary-label">Type</label>';

                 row_html += '<div style="width:20px; height:20px;margin:5px; display: inline-block;"><img src="/Img/icon_logic_redirect.png" style="width:100%;"/></div>';

                row_html += '</div>';
                row_html += '<div class="inline-block-top card-col align-center" style="width: 17%;">';
                row_html += '<label class="secondary-label">Logic</label>';

                if (value.HasOutgoingLogic) {
                    row_html += '<div style="width:20px; height:20px;margin:5px; display: inline-block;"><img src="/Img/icon_logic_redirect.png" style="width:100%;"/></div>';
                }

                if (value.HasIncomingLogic) {
                    row_html += '<div style="width:20px; height:20px;margin:5px; display: inline-block;"><img src="/Img/icon_logic_destination.png" style="width:100%;"/></div>';
                }
                
                row_html += '</div>';
                row_html += '<div class="inline-block-top card-col align-left" style="width: 20%; padding-left:20px; min-height:100px; position:relative;">';
                row_html += '<a href="" style="display: inline-block; padding-top: 15px; padding-left:20px;"><img src="/Img/icon_result.png"></a>';

                if (value.hasChild) {
                    row_html += '<a href="" class="toggle-expand" data-child="child-' + value.Ordering + '" data-parent="parent-' + value.Ordering + '"><i class="fa fa-2x fa-angle-down" aria-hidden="true"></i></a>';
                    row_html += '<a href="" class="toggle-hide" data-child="child-' + value.Ordering + '" data-parent="parent-' + value.Ordering + '"><i class="fa fa-2x fa-angle-up" aria-hidden="true"></i></a>';
                }

                row_html += '<a href="" class="toggle-showdetail" ><i class="fa fa-2x fa-angle-down" aria-hidden="true"></i></a>'
                row_html += '<a href="" class="toggle-hidedetail" ><i class="fa fa-2x fa-angle-up" aria-hidden="true"></i></a>'
                row_html += '</div>';

                row_html += '<div class="expanded-content"></div>';
                row_html += '</div>';
                
                if (value.hasChild) {
                    row_html += '<div class="children"></div>';
                }

                row_html += '</div>';

                $('.report-content').append(row_html);

                // hackish method to group children inside parent
                if ($('.child-' + parentOrder).length > 0) {
                    var selector = '.parent-' + parentOrder + ' .children';
                    $(selector).append($('.child-' + parentOrder));
                }
                
            }

            var pulses = [
                { Ordering: '1', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic:true },
                { Ordering: '2', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '3', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '3A', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '3AA', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '3AB', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '4', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '4A', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '4AA', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '4AB', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '5', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true },
                { Ordering: '6', PulseId: 'TESTID', CardType: '0', ImageUrl: "", Title: "TEST", HasOutgoingLogic: true, HasIncomingLogic: true }
            ];


            function fetchData() {

                if (pulses.length > 0) {
                    $.each(pulses, function (key, value) {
                        pulses[key].hasChild = false;

                        // check the following contains trailing alphabet but not for itself, then set it hasChild
                        if (pulses[key + 1] && hasAlphabet(pulses[key + 1].Ordering) && !hasAlphabet(value.Ordering)) { 

                            pulses[key].hasChild = true;
                        }

                        if (pulses[key - 1] && (!hasAlphabet(pulses[key - 1].Ordering)) && (!hasAlphabet(value.Ordering)) && pulses[key + 1] && (hasAlphabet(pulses[key + 1].Ordering))) { //next row is sub logic content

                            if (key !== 0) {
                                //$('.report-content').append('<hr class="row-separator"/>');
                            }

                            generateRow(value, getNumOfAlphabet(value.Ordering));

                        } else if ((hasAlphabet(value.Ordering)) && pulses[key + 1] && (!hasAlphabet(pulses[key + 1].Ordering))) {

                            generateRow(value, getNumOfAlphabet(value.Ordering));
                            //$('.report-content').append('<hr class="row-separator" />');
                        } else {
                            generateRow(value, getNumOfAlphabet(value.Ordering));
                        }
                    });
                }
            }
                       
            fetchData();

            if (isDraggable) {
                Sortable.create(sortable, {
                    animation: 150,
                    handle: '.drag-indicator',
                    onEnd: function (e) {
                        console.log(e.oldIndex);
                        console.log(e.newIndex);
                    }
                });
            } else {
                $('.drag-indicator').css('display', 'none');
            }
            

            $(document).on('click', '.toggle-expand', function (e) {
                e.preventDefault();
                $(this).css('display', 'none');
                $(this).parent().find('.toggle-hide').css('display', 'block');
                var child = $(this).data('child');
                var parent = $(this).data('parent');
                var selector_fakelayer = "." + parent + " .fake-layer";
                var selector_indent1 = "." + child + " .indent1";
                var selector_indent2 = "." + child + " .indent2";
                var selector_indicator = "." + child + " .indent-indicator";

                $(selector_fakelayer).addClass('animated fadeOut');

                $(selector_indent1).css('display', 'block');
                $(selector_indent2).css('display', 'block');
                $(selector_indicator).css('display', 'block');

            })

            $(document).on('click', '.toggle-hide', function (e) {
                e.preventDefault();
                $(this).css('display', 'none');
                $(this).parent().find('.toggle-expand').css('display', 'block');
                var child = $(this).data('child');
                var parent = $(this).data('parent');
                var selector_fakelayer = "." + parent + " .fake-layer";
                var selector_indent1 = "." + child + " .indent1";
                var selector_indent2 = "." + child + " .indent2";
                var selector_indicator = "." + child + " .indent-indicator";

                $(selector_fakelayer).removeClass('animated fadeOut');
                $(selector_fakelayer).addClass('animated fadeIn');
                $(selector_indent1).css('display', 'none');
                $(selector_indent2).css('display', 'none');
                $(selector_indicator).css('display', 'none');

            });

            $(document).on('click', '.toggle-showdetail', function (e) {
                e.preventDefault();
                $(this).css('display', 'none');
                $(this).parent().parent().find('.toggle-hidedetail').css('display', 'block');
                $(this).parent().parent().find('.expanded-content').css('height', '300px');
            });

            $(document).on('click', '.toggle-hidedetail', function (e) {
                e.preventDefault();
                $(this).css('display', 'none');
                $(this).parent().parent().find('.toggle-showdetail').css('display', 'block');
                $(this).parent().parent().find('.expanded-content').css('height', '0px');
            });
        });
    </script>

</asp:Content>
