﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo managerInfo = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.managerInfo = Session["admin_info"] as ManagerInfo;
        }

        protected void lbChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check password.
                if (!tbNewPassword.Text.Equals(tbConfirmPassword.Text))
                {
                    ShowToastMsgAndHideProgressBar("Your passwords does not match", MessageUtility.TOAST_TYPE_ERROR, true);
                    return;
                }
                #endregion

                #region Step 2. Call API.
                AuthenticationUpdateResponse response = asc.UpdatePasswordForManager(managerInfo.UserId, managerInfo.Email, managerInfo.CompanyId, tbNewPassword.Text, null, managerInfo.AccountType.Code);
                if (response.Success)
                {
                    ShowToastMsgAndHideProgressBar("Password has been updated. Directing to login page...", MessageUtility.TOAST_TYPE_INFO, true);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Logout', 1000);", true);
                }
                else
                {
                    Log.Error("AuthenticationUpdateResponse.Success is false. Error message: " + response.ErrorMessage);
                    ShowToastMsgAndHideProgressBar("Failed to update password. Please contact support@cocadre.com", MessageUtility.TOAST_TYPE_ERROR, true);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType, bool isShowPop)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }
    }
}