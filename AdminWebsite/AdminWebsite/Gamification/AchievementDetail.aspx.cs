﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Gamification
{
    public partial class AchievementDetail : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                hfManagerId.Value = managerInfo.UserId;
                hfCompanyId.Value = managerInfo.CompanyId;
                hfAchievementId.Value = "";

                if (Page.RouteData.Values["AchievementId"] != null)
                {
                    ViewState["achievement_id"] = Page.RouteData.Values["AchievementId"].ToString();
                    hfAchievementId.Value = Page.RouteData.Values["AchievementId"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}