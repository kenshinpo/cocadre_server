﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CassandraService.Entity.Gamification;

namespace AdminWebsite.Gamification
{
    public partial class AchievementList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo managerInfo = null;
        private List<Achievement> achievements = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                hfManagerId.Value = managerInfo.UserId;
                hfCompanyId.Value = managerInfo.CompanyId;

                if (!IsPostBack)
                {
                    #region Step 1. Get Achievements
                    RefreshListView();
                    #endregion
                }

                tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RefreshListView()
        {
            GetAllAchievement();
            if (achievements != null)
            {
                lvAchievement.DataSource = achievements;
                lvAchievement.DataBind();
                if (achievements != null && achievements.Count > 1)
                {
                    ltlCount.Text = achievements.Count + " achievements";
                }
                else if (achievements != null && achievements.Count == 1)
                {
                    ltlCount.Text = "1 achievement";
                }
                else
                {
                    ltlCount.Text = "0 achievement";
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        public void GetAllAchievement()
        {
            GamificationSelectAllAchievementResponse response = asc.SelectAllAchievements(managerInfo.UserId, managerInfo.CompanyId, tbFilterKeyWord.Text.Trim());
            if (response.Success)
            {
                if (string.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                {
                    hfCanResort.Value = "true";
                }
                else
                {
                    hfCanResort.Value = "false";
                }

                achievements = response.Achievements;

                for (int i = 0; i < achievements.Count; i++)
                {
                    ClientScript.RegisterArrayDeclaration("achievements", "new Achievement('" + achievements[i].Id + "', '" + achievements[i].Title + "', " + achievements[i].Ordering + ")");
                }
            }
            else
            {
                Log.Error("GamificationSelectAllAchievementResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        protected void lvAchievement_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                LinkButton lbDescription = e.Item.FindControl("lbDescription") as LinkButton;

                if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].Type == (int)CassandraService.Entity.Gamification.AchievementTypeEnum.PlayToGet)
                {
                    if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].RuleType == (int)CassandraService.Entity.Gamification.AchievementRuleTypeEnum.AllTopics)
                    {
                        if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes == 1)
                        {
                            lbDescription.Text = "All Topics, Play any game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " time";
                        }
                        else
                        {
                            lbDescription.Text = "All Topics, Play any game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " times";
                        }

                    }
                    else if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].RuleType == (int)CassandraService.Entity.Gamification.AchievementRuleTypeEnum.SpecificTopic)
                    {
                        if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes == 1)
                        {
                            lbDescription.Text = ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].Topics[0].TopicTitle + ", Play game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " time";
                        }
                        else
                        {
                            lbDescription.Text = ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].Topics[0].TopicTitle + ", Play game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " times";
                        }
                    }
                    else if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].RuleType == (int)CassandraService.Entity.Gamification.AchievementRuleTypeEnum.MultipleTopicsByEither)
                    {
                        if(((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes == 1)
                        { 
                        lbDescription.Text = "Multiple Topic, Play either game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " time";
                        }
                        else
                        {
                            lbDescription.Text = "Multiple Topic, Play either game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " times";
                        }
                    }
                    else if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].RuleType == (int)CassandraService.Entity.Gamification.AchievementRuleTypeEnum.MultipleTopicsByEach)
                    {
                        if(((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes == 1)
                        {
                            lbDescription.Text = "Multiple Topic, Play each game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " time";
                        }
                        else
                        {
                            lbDescription.Text = "Multiple Topic, Play each game for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].PlayTimes + " times";
                        }
                    }
                    else
                    {
                        // do nothing
                    }
                }
                else if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].Type == (int)CassandraService.Entity.Gamification.AchievementTypeEnum.FailButGet)
                {
                    if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].FailTimes == 1)
                    {
                        lbDescription.Text = "Never say die, Keep trying. Failed " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].FailTimes + " streak";
                    }
                    else
                    {
                        lbDescription.Text = "Never say die, Keep trying. Failed " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].FailTimes + " streaks";
                    }

                }
                else if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].Type == (int)CassandraService.Entity.Gamification.AchievementTypeEnum.PerfectAndGet)
                {
                    if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].RuleType == (int)CassandraService.Entity.Gamification.AchievementRuleTypeEnum.SpecificTopic)
                    {
                        lbDescription.Text = "Achieve Perfect Score for " + ((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].Topics[0].TopicTitle;
                    }
                    else if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].RuleType == (int)CassandraService.Entity.Gamification.AchievementRuleTypeEnum.MultipleTopicsByEither)
                    {
                        lbDescription.Text = "Achieve Perfect Score for either topic, Multiple Topics";
                    }
                    else if (((List<Achievement>)lvAchievement.DataSource)[e.Item.DataItemIndex].RuleType == (int)CassandraService.Entity.Gamification.AchievementRuleTypeEnum.MultipleTopicsByEach)
                    {
                        lbDescription.Text = "Achieve Perfect Score for each topic, Multiple Topics";
                    }
                    else
                    {
                        // do nothing
                    }
                }
                else
                {
                    // do nothing
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ibFilterKeyWord_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                RefreshListView();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvAchievement_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.EmptyItem)
                {
                    Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                    if (!String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                    {

                        ltlEmptyMsg.Text = @"Filter """ + tbFilterKeyWord.Text.Trim() + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                    }
                    else
                    {
                        ltlEmptyMsg.Text = @"Welcome to Gamification Achievements Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding an Achievements by clicking on the orange button.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                GamificationCreateAchievementResponse response = asc.DeleteAchievement(managerInfo.UserId, managerInfo.CompanyId, lbAction.CommandArgument);
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    mpePop.Hide();
                    RefreshListView();
                    toastMsg = hfAchievementTitle.Value + " has been deleted.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("GamificationCreateAchievementResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    mpePop.Show();
                    toastMsg = "Failed to delete achievement, please check your internet connection.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvAchievement_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Gamification/AchievementDetail/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Analytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Gamification/GamificationAnalytic/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Del", StringComparison.InvariantCultureIgnoreCase))
                {
                    Label lblTitle = e.Item.FindControl("lblTitle") as Label;
                    System.Web.UI.WebControls.Image imgAchievement = e.Item.FindControl("imgAchievement") as System.Web.UI.WebControls.Image;

                    hfAchievementId.Value = e.CommandArgument.ToString();

                    lbAction.CommandArgument = e.CommandArgument.ToString();
                    ltlActionName.Text = "Delete ";
                    lbAction.Text = "Delete";
                    ltlActionMsg.Text = "All achievers' history <b>" + lblTitle.Text + "</b> will be deleted. Are you sure?";

                    imgAction.ImageUrl = imgAchievement.ImageUrl;
                    ltlActionTitle.Text = lblTitle.Text;
                    mpePop.Show();
                }
                else
                {
                    // do nothing
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvAchievement_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }
    }
}