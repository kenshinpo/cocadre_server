﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ExperiencePoints.aspx.cs" Inherits="AdminWebsite.Gamification.ExperiencePoints" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Gamification <span>Experience Points</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlCount" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: bolder;">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Experience Points</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Gamification/Leaderboard">Leaderboard</a>
                </li>

                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Gamification/AchievementList">Achievements</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">
        </div>
    </div>
</asp:Content>
