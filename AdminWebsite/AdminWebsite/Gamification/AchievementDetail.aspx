﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AchievementDetail.aspx.cs" Inherits="AdminWebsite.Gamification.AchievementDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <script src="/js/Gamification/AchievementDetail.js"></script>
    <link rel="stylesheet" href="/css/Gamification/croppie.css" />
    <script src="/js/Gamification/croppie.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }
    </style>


    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

    <div id="plBadge" class="popup" style="min-width: 50%; min-height: 50%; margin: 0px auto; z-index: 9001; bottom: 50%; right: 50%; display: none; object-fit: contain !important; transform: translate(50%, 51%); -o-transform: translate(50%, 51%); -webkit-transform: translate(50%, 51%); -moz-transform: translate(50%, 51%); max-width: 80%; max-height: 60%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Select your badge template</h1>
        <div class="popup__content" style="min-height: 150px;">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <label style="display: inline-block; width: auto;">Choose your color</label>

                        <div class="badgeColorList">
                        </div>

                        <div class="badgeStyleList" style="margin: 20px; text-align: center;">
                        </div>

                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a class="popup__action__item popup__action__item--cta" style="color: rgba(254, 30, 38, 1);" href="javascript:confirmBadge();">SELECT</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hideChoiceBadgePopup();">CANCEL</a>
        </div>
    </div>

    <div id="plCustomizeBadge" class="popup" style="min-width: 50%; min-height: 45%; width: auto; height: auto; margin: 0px auto; z-index: 9001; bottom: 50%; right: 50%; display: none; object-fit: contain !important; transform: translate(50%, 50%); -o-transform: translate(50%, 50%); -webkit-transform: translate(50%, 50%); -moz-transform: translate(50%, 50%); max-width: 80%; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Crop your badge</h1>
        <div class="popup__content" style="min-height: 150px;">
            <fieldset class="form">
                <div class="upload-wrap">
                    <div id="uploadCrop"></div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a class="popup__action__item popup__action__item--cta" style="color: rgba(254, 30, 38, 1);" href="javascript:cropBadge();">CROP</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hideCustomizeBadgePopup();">CANCEL</a>
        </div>
    </div>


    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfAchievementId" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Gamification <span>Achievement</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlCount" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: bolder;">Manage</div>
            <ul class="data__sidebar__list" style="margin-top: 20px;">
                <%--                
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Gamification/ExperiencePoints">Experience Points</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Gamification/Leaderboard">Leaderboard</a>
                </li>
                --%>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Gamification/AchievementList">Achievements</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">

            <div style="display: inline-block;">
                <div class="action_name" style="margin: 30px 150px 0px 40px; font-size: 2em; font-weight: bolder; width: 350px;">Create a new achievement</div>

                <div style="margin: 30px 40px;">
                    <label style="width: 60px; text-align: right; color: rgba(135, 135, 135, 1); font-size: 1.5em; vertical-align: top; display: inline-block; margin-top: 10px;">Title</label>
                    <div class="text_container" style="margin-left: 5%; width: 80%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                        <input class="badge_title" style="padding: 15px; border-image: none; width: 100% !important; vertical-align: middle;" onkeydown="return (event.keyCode!=13);" onkeyup="letterCounter(this, 20); setBadgeText(this.value);" onblur="letterCounter(this, 20); setBadgeText(this.value);" type="text" placeholder="Enter title's name" />
                        <span class="letterCount" style="right: 0px; bottom: 25px; position: absolute;">20</span>
                    </div>
                </div>

                <div style="margin: 30px 40px;">
                    <label style="width: 60px; text-align: right; color: rgba(135, 135, 135, 1); font-size: 1.5em; vertical-align: top; display: inline-block; margin-top: 10px;">Type</label>
                    <div class="mdl-selectfield" style="margin-left: 5%; display: inline-block; width: 80%;">
                        <select class="achievement_type" onchange="changeType(this);" style="margin-bottom: 0px;">
                            <option value="1">Play to get</option>
                            <option value="2">Fail but get</option>
                            <option value="3">Perfect and get</option>
                        </select>
                    </div>
                </div>

                <div class="achievement_container" style="margin: 30px 40px;">
                </div>

                <div>
                    <a class="popup__action__item popup__action__item--cancel create_achievement" style="color: rgba(32, 135, 255, 1); display: none;" href="javascript:createAchievement();">CREATE</a>
                    <a class="popup__action__item popup__action__item--cancel edit_achievement" style="color: rgba(32, 135, 255, 1); display: none;" href="javascript:editAchievement();">Edit</a>
                    <a class="popup__action__item popup__action__item--cancel" href="/Gamification/AchievementList">CANCEL</a>
                </div>
            </div>
            <div style="vertical-align: top; display: inline-block;">
                <img id="badgeImg" style="width: 300px; height: 300px; margin-top: 20px; display: inline-block; position: relative; display: none;" />
                <canvas id="badgeCanvas" style="width: 300px; height: 300px; margin-top: 20px; display: inline-block; position: relative;"></canvas>
                <div style="vertical-align: top; display: inline-block; margin-left: 30px;">
                    <a class="btn" href="javascript:showChoiceBadgePopup();" style="margin-top: 100px; width: 180px;">SELECT YOUR BADGE</a>

                    <label style="margin-top: 20px;">
                        <span class="btn" style="border: 0px currentColor; border-image: none; width: 180px; color: rgba(87, 87, 87, 1); background-color: rgba(230, 230, 230, 1);"><i class="fa fa-plus" aria-hidden="true"></i>UPLOAD BADGE</span>
                        <input class="choiceImg" type="file" accept="image/*" style="display: none;" onchange="choiceImg(this);" />
                    </label>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
