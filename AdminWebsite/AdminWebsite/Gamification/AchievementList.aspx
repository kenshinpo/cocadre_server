﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AchievementList.aspx.cs" Inherits="AdminWebsite.Gamification.AchievementList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script src="/Js/Gamification/jquery-sortable.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
            resetSortingTable();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
            resetSortingTable();
        })

        function Achievement(id, title, ordering) {
            this.Id = id;
            this.Title = title;
            this.Ordering = ordering;
        }

        function resetSortingTable() {
            if ($("#main_content_hfCanResort").val() === "true")
            {
                // Sortable rows
                var oldIndex;
                $('.sorted_table').sortable({
                    handle: 'div.sorting',
                    containerSelector: 'table',
                    itemPath: '> tbody',
                    itemSelector: 'tr',
                    placeholder: '<tr class="placeholder"/>',
                    onDragStart: function ($item, container, _super) {
                        oldIndex = $item.index();
                        _super($item, container);
                    },
                    onDrop: function ($item, container, _super) {
                        var field,
                            newIndex = $item.index();

                        if (newIndex != oldIndex) {
                            // new array
                            var newAchievements = [];

                            if (newIndex < oldIndex) {
                                for (var i = 0; i < newIndex; i++) {
                                    newAchievements.push(achievements[i]);
                                }

                                newAchievements.push(achievements[oldIndex]);

                                for (var i = newIndex; i < achievements.length; i++) {
                                    if (i != oldIndex) {
                                        newAchievements.push(achievements[i]);
                                    }
                                }
                            }
                            else {
                                for (var i = 0; i < (newIndex + 1) ; i++) {
                                    if (i != oldIndex) {
                                        newAchievements.push(achievements[i]);
                                    }
                                }

                                newAchievements.push(achievements[oldIndex]);

                                for (var i = (newIndex + 1) ; i < achievements.length; i++) {
                                    if (i != oldIndex) {
                                        newAchievements.push(achievements[i]);
                                    }
                                }
                            }

                            achievements = newAchievements;
                            // Call API
                            reorderAchievements();
                            // Call API

                        }
                        _super($item, container);
                    }
                });
            }
            else
            {
                //$(".achievementItem").off("mouseover");
                $(".achievementItem").removeAttr('onmouseover');
            }
        }

        function reorderAchievements() {
            try {
                var achievementIds = [];
                for (var i = 0; i < achievements.length; i++) {
                    achievementIds.push(achievements[i].Id);
                }

                var request = {
                    'CompanyId': $("#main_content_hfCompanyId").val(),
                    'ManagerId': $("#main_content_hfManagerId").val(),
                    'AchievementIds': achievementIds
                };

                jQuery.ajax({
                    type: "POST",
                    url: "/Api/Gamification/ReorderingAchievements",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(request),
                    dataType: "json",
                    crossDomain: true,
                    beforeSend: function (xhr, settings) {
                        ShowProgressBar();
                    },
                    success: function (d, status, xhr) {
                        if (d.Success) {
                            ShowToast("Sorting order has been updated", 1);
                            RedirectPage("/Gamification/AchievementList", 100);
                        } else {
                            ShowToast(d.ErrorMessage, 2);
                        }
                    },
                    error: function (xhr, status, error) {
                        ShowToast(error, 2);
                    },
                    complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                        HideProgressBar();
                    }
                });
            } catch (e) {
                console.error(e.toString());
            }
        }

        function showDragButton(element) {
            $(element).find(".sorting").css("visibility", "visible");;

        }

        function hideDragButton(element) {
            $(element).find(".sorting").css("visibility", "hidden");;
        }
    </script>

    <style>
        body.dragging, body.dragging * {
            cursor: move !important;
        }

        .dragged {
            position: absolute;
            opacity: 0.5;
            z-index: 2000;
        }

        ol.example li.placeholder {
            position: relative;
            /** More li styles **/
        }

            ol.example li.placeholder:before {
                position: absolute;
                /** Define arrowhead **/
            }
    </style>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hfManagerId" />
            <asp:HiddenField runat="server" ID="hfCompanyId" />
            <asp:HiddenField runat="server" ID="hfCanResort" />
            <asp:HiddenField ID="hfAchievementId" runat="server" />
            <asp:HiddenField ID="hfAchievementTitle" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/Gamification/AchievementDetail" class="mfb-component__button--main" data-mfb-label="Add Achievement" style="background: rgba(255, 150, 0, 1);">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Acitve, Delete -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Achievement
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgAction" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClientClick="ShowProgressBar();" OnClick="lbAction_Click" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Acitve, Delete -->
        </ContentTemplate>
    </asp:UpdatePanel>




    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Gamification <span>Achievements</span></div>
        <div class="appbar__meta">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Literal ID="ltlCount" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: bolder;">Manage</div>
            <ul class="data__sidebar__list" style="margin-top: 20px;">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Achievements</a>
                </li>
            </ul>

            <div class="data__sidebar__title" style="margin-top: 200px;">Filters</div>

            <div class="pad">
                <label>Search Badge</label>
                <asp:UpdatePanel ID="upFilter" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Enter the Badge's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" OnClick="ibFilterKeyWord_Click" runat="server" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvAchievement" runat="server" OnItemDataBound="lvAchievement_ItemDataBound" OnItemCreated="lvAchievement_ItemCreated" OnItemCommand="lvAchievement_ItemCommand" OnItemEditing="lvAchievement_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table class="dataTable hover sorted_table">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="padding: 0px 0px; width: 20px;"></th>
                                        <th class="no-sort" style="width: 40px; padding: 20px 20px;">
                                            <span>No</span>
                                        </th>
                                        <th style="width: 35%; padding: 20px 20px;">
                                            <span>Name</span>
                                        </th>
                                        <th style="width: 25%; padding: 20px 20px;">
                                            <span>Description</span>
                                        </th>
                                        <th style="width: 15%; padding: 20px 20px;">
                                            <span>No of Achievers</span>
                                        </th>
                                        <th class="no-sort" style="width: 15%;"></th>
                                        <th class="no-sort" style="width: auto;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr class="achievementItem" onmouseover="showDragButton(this);" onmouseout="hideDragButton(this);">
                                <td style="padding: 0px 0px;">
                                    <div class="sorting" style="border: solid 2px rgba(204, 204, 204, 1); height: 91px; visibility: visible; cursor: move; text-align: center; display: flex; align-items: center; visibility: hidden;">
                                        <i class="fa fa-ellipsis-v" style="padding: 0px; color: rgba(204, 204, 204, 1); font-size: 2.5em; cursor: move; margin: 0 auto;"></i>
                                    </div>
                                </td>
                                <td style="padding: 10px 20px;">
                                    <asp:LinkButton ID="lbOrdering" Text='<%# DataBinder.Eval(Container.DataItem, "Ordering") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 10px 20px;">
                                    <asp:LinkButton ID="lbTitle" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>'>
                                        <asp:Image ID="imgAchievement" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "IconImageUrl").ToString().Replace("original","small") %>' Width="75px" Height="75px" Style="display: inline-block; vertical-align: middle;" />
                                        <asp:Label ID="lblTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' Style="vertical-align: middle; display: inline-block; color: #999; margin-left: 15px;" />
                                    </asp:LinkButton>
                                </td>
                                <td style="padding: 10px 20px;">
                                    <asp:LinkButton ID="lbDescription" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' Style="color: #999;" Text='<%# DataBinder.Eval(Container.DataItem, "RuleDescription") %>' />
                                </td>

                                <td style="padding: 10px 20px;">
                                    <asp:LinkButton ID="lbNoOfAchiever" ClientIDMode="AutoID" runat="server" CommandName="Analytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>' Style="color: #999;" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfAchievers") %>' />
                                </td>
                                <td style="padding: 10px 20px;">
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Del" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' Text="Delete" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbAnalytics" ClientIDMode="AutoID" runat="server" CommandName="Analytics" Text="View Analytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>' />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
