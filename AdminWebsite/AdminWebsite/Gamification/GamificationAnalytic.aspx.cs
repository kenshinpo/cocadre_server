﻿using AdminWebsite.App_Code.Entity;
using CassandraService.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Gamification
{
    public partial class GamificationAnalytic : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            this.hfAdminUserId.Value = managerInfo.UserId;
            this.hfCompanyId.Value = managerInfo.CompanyId;

            if (!Page.IsPostBack)
            {
                if (Page.RouteData.Values["AchievementId"] != null)
                {
                    hfAchievementId.Value = Page.RouteData.Values["AchievementId"].ToString().Trim();
                }
                else
                {
                    Response.Redirect("/Gamification/AchievementList");
                }
            }
        }
    }
}