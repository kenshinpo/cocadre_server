﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="GamificationAnalytic.aspx.cs" Inherits="AdminWebsite.Gamification.GamificationAnalytic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/Css/ca-style.css" />
	<link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
	<style>
		#analytic-wrapper .data__content {
			background: #F5F5F5;
		}

		#education-analytic {
			margin-top: 30px;
		}

		.filter-section {
			margin: 0 auto;
		}

			.filter-section .search-input {
				font-size: 18px;
				padding-left: 50px !important;
			}

			.filter-section .fa {
				font-size: 25px;
			}

		.invisible {
			visibility: hidden;
		}

		.mdl-selectfield {
			width: 220px !important;
		}

		.dynatable-per-page {
			display: none;
		}

		.dynatable-pagination-links {
			float: none;
			text-align: center;
			padding: 20px;
		}

		.dynatable-active-page {
			background: #cccccc;
		}
	
		.topic-img {
			width: 60px;
			height: 60px;
			display: inline-block;
			vertical-align:middle;
			margin-right: 15px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

	<asp:HiddenField runat="server" ID="hfAdminUserId" />
	<asp:HiddenField runat="server" ID="hfCompanyId" />
	<asp:HiddenField runat="server" ID="hfAchievementId" />

	<!-- App Bar -->
	<div id="st-trigger-effects" class="appbar">
		<a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
		<div class="appbar__title"><strong>Gamification</strong> <span>console</span></div>
		<div class="appbar__meta"><a href="/Gamification/AchievementList" id="root-link"></a></div>
		<div class="appbar__meta">
			<!--<a id="second-link"></a>-->
			<span style="color: black;" id="second-link"></span>
		</div>
		<div class="appbar__action">
			<a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
		</div>
	</div>
	<!-- /App Bar -->
	
	<div id="analytic-wrapper" class="data">

		<div class="data__content">

			<div id="education-analytic" class="container" style="width: 100%;">

				<div style="width: 100%; margin: 0 auto;">
					<!-- TITLE -->
					<div class="title-section" style="width: 100%; margin: 0 auto;">
						<div class="analytic-info">
							<h2 id="analytic-title" class="ellipsis-title"></h2>
						</div>
						<div class="header-left">
							<ul class="header-list">
								<li>
									<span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Rules:</span>
									<span class="rules-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
								</li>
								<li>
									<span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Sorting No:</span>
									<span class="sorting-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
								</li>
								<li>
									<span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Created On:</span>
									<span class="date-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
								</li>
							</ul>
							<%--<p class="align-left"><span id="start-date-string" style="font-size: 16px;color: #727272;"></span></p>--%>
						</div>
						<div class="header-right">
							<p class="align-right" style="margin-top:50px;">
								<span style="display:inline-block; font-size: 16px;color: #727272;">No of Achievers:</span>
								<span id="achiever-count" style="font-size: 16px;color: #000;">12</span>
							</p>
						</div>
					</div>

					<div id="overview-tab" class="tabs tabs--styled" style="clear:both; ">
						<ul class="tabs__list">
							<li class="tabs__list__item">
								<a class="tabs__link" href="#achievers">Achievers</a>
							</li>
						</ul>
						<div class="tabs__panels">
							<!-- OVERVIEW TAB -->
							<div class="tabs__panels__item overview" id="achievers">

								<div class="filter-section text-center" style="clear:both; margin-top: 10px; padding-left: 20px;">
									<div class="option-filter">
										<div class="mdl-selectfield" style="width: 250px; float: left; margin-right: 20px; height: 40px;">
											<select name="status" id="status">
											</select>
										</div>
									</div>
									<div class="search-filter">
										<div class="input-group" style="position: relative;">
											<i class="fa fa-search"></i>
											<input id="hidden_name" class="search-input" type="text" placeholder="Search Achievers" value="" />
										</div>
									</div>
								</div>

								<div id="personnel-card" class="card full-row animated fadeInUp no-padding">
									<div class="table-content" style="position:relative;">
										<div class="table-progress" style="width:100%; height:100%; position:absolute; text-align:center; top:0; left:0; background:rgba(255,255,255,0.8); z-index: 2; display:none;">
											<div style="position:absolute; top:45%; width:100%;">
												<div class="spinner">
													<div class="rect1"></div>
													<div class="rect2"></div>
													<div class="rect3"></div>
													<div class="rect4"></div>
													<div class="rect5"></div>
												</div>
												<div id="ProgressText" style="font-size: 15px; color: #1B3563;">Processing...</div>
											</div>
											
										</div>
										<table id="responder-table" class="responder-table" style="margin:0px;"></table>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
	 <script>
			$(function () {

				var CompanyId = $('#main_content_hfCompanyId').val();
				var ManagerId = $('#main_content_hfAdminUserId').val();
				var AchievementId = $('#main_content_hfAchievementId').val();
				var SearchId = "";

				var progress_html = "";
				var table_html = "";

				var responder_table = $('#responder-table');
				var responder_data = [];
			
				function generateTable(userList) {
					responder_data = [];

					table_html = "";
					table_html += '<thead>';
					table_html += '<th data-dynatable-column="no" style="width:10%">No</th>';
					table_html += '<th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width:40%">Name</th>';
					table_html += '<th class="hidden" data-dynatable-column="hidden_name">Hidden name</th>';
					table_html += '<th data-dynatable-column="date" data-dynatable-sorts="date" style="width:20%">Date</th>';
					table_html += '<th data-dynatable-column="department" data-dynatable-sorts="department" style="width:30%">Department</th>';
					table_html += '</thead><tbody></tbody>';

					$.each(userList, function (k, v) {

						var nameHTML = "";

						nameHTML = '<a href="/Gamification/UserAchievements/' + v.User.UserId + '" target="_blank">' + v.User.FirstName + " " + v.User.LastName + '</a>';
					   
						responder_data.push({
							"no": k + 1,
							"responder_name": nameHTML,
							"hidden_name": v.User.FirstName + " " + v.User.LastName,
							"date": v.UnlockedOnTimestampString,
							"department": v.User.Departments[0].Title
						})
					});

					$('#responder-table').append(table_html);

					responder_table.dynatable({
						features: {
							paginate: true,
							search: false,
							sorting: true,
							recordCount: false,
							pushState: false,
						},
						dataset: {
							records: responder_data,
							sorts: { 'no': 1 }
						},
						inputs: {
							queries: $(' #hidden_name')
						}
					});


					var dynatable = responder_table.data('dynatable');

					if (typeof dynatable.records !== "undefined") {
						dynatable.records.updateFromJson({ records: responder_data });
						dynatable.records.init();
					}
					dynatable.paginationPerPage.set(15);
					dynatable.process();


					responder_table.data('dynatable').paginationPerPage.set(15);
					responder_table.data('dynatable').process();

					setTimeout(function () {
						responder_table.css('display', 'table');
						responder_table.addClass('animated fadeIn');
					}, 500);
				}

				function refreshTable(id) {
					$.ajax({
						type: "POST",
						url: '/Api/Gamification/SelectAchievementResult',
						data: {
							"CompanyId": CompanyId,
							"ManagerId": ManagerId,
							"AchievementId": AchievementId,
							"SearchId": id
						},
						crossDomain: true,
						dataType: 'json',
						success: function (res) {

							if (res.Success) {
								$('.table-progress').css('display', 'none');
								var achievers = res.Achievers;
								$('#responder-table').empty();
								generateTable(achievers);
							}

						}

					});
				}

				function fetchData() {
					$('#education-analytic').addClass('hidden');
					$('#education-analytic').removeClass('animated fadeInUp');

					ShowProgressBar();
					$.ajax({
						type: "POST",
						url: '/Api/Gamification/SelectAchievementResult',
						data: {
							"CompanyId": CompanyId,
							"ManagerId": ManagerId,
							"AchievementId": AchievementId,
							"SearchId": ""
						},
						crossDomain: true,
						dataType: 'json',
						success: function (res) {
							HideProgressBar();
							if (res.Success) {
								$('#education-analytic').removeClass('hidden');
								$('#education-analytic').addClass('animated fadeInUp');

								var achievement = res.Achievement;
								var achievers = res.Achievers;
								var searchTerms = res.SearchTerms;

								var titleHTML = achievement.Title;
								var rulesHTML = achievement.TypeDescription;

								if (achievement.RuleDescription) {
									rulesHTML += ', '+achievement.RuleDescription;
								}

								$('.rules-label').html(rulesHTML);
								$('.date-label').html(achievement.CreatedOnTimestampString);
								$('.sorting-label').html(achievement.Ordering);

								if (searchTerms.length > 0) {
									$.each(searchTerms, function (k, v) {
										$('select').append('<option value="' + v.Id + '">' + v.Content + '</option>');
									});
								}

								if (achievement.IconImageUrl) {
									titleHTML = '<img src="' + achievement.IconImageUrl + '" class="topic-img" />' + titleHTML;
								} 

								$('#analytic-title').html(titleHTML);
								$('#root-link').html(achievement.Title);
								$('#achiever-count').html(achievement.NumberOfAchievers);

								$('#responder-table').empty();
								generateTable(achievers);
								
							}
							else { // ERROR RESPONSE
								$('#education-analytic').empty();
								$('#education-analytic').html('<p class="align-center">' + res.ErrorMessage + '</p>');
							}
						}
					});
				}

				responder_table.bind('dynatable:afterUpdate', function (e, dynatable) {
					setTimeout(function () {
						responder_table.css('display', 'table');
						responder_table.addClass('animated fadeIn');
					}, 1500);
				});

				// CUSTOM TABLE SEARCH FILTER FUNCTION
				responder_table.bind('dynatable:init', function (e, dynatable) {

					dynatable.queries.functions['hidden_name'] = function (record, queryValue) {
						return record.hidden_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
					};
				});

				fetchData();

				$('#status').on('change', function (e) {
					var id = $(this).val();
					$('.table-progress').css('display', 'block');
					refreshTable(id);
				});
			});
		</script>
</asp:Content>
