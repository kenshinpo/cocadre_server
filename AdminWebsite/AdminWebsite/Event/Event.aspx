﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Event.aspx.cs" Inherits="AdminWebsite.Event.Event" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfEventId" runat="server" />
            <!-- Delete Event Form -->
            <asp:Panel ID="popup_deleteevent" runat="server" CssClass="popup popup--deleteeventform" Width="100%" Style="display: none;">
                <h1 class="popup__title">Delete Event</h1>
                <div class="popup__content popup__content--nominheight">

                    <div class="container">
                        <p>
                            Confirm deletion of the event 
                            <strong>
                                <asp:Literal ID="ltlDelEventTitle" runat="server" />
                            </strong>?
                        </p>
                    </div>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" Text="Delete" OnClick="lbDelete_Click" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbDelCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbDelCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Delete Event Form -->
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_deleteevent"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Floating Action Button -->
    <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
        <li class="mfb-component__wrap">
            <a href="/Event/EventCreate" class="mfb-component__button--main" data-mfb-label="Add Event">
                <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                <i class="mfb-component__main-icon--active fa fa-calendar"></i>
            </a>
        </li>
    </ul>
    <!-- /Floating Action Button -->

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Event <span>manager</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlEventCount" runat="server" />
            Event
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" href="#"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- / App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">

            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Event</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <div class="pad">
                <label>By Status</label>
                <div class="mdl-selectfield">
                    <asp:UpdatePanel ID="upFilterByStatus" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlEventStatus" runat="server" OnSelectedIndexChanged="ddlEventStatus_SelectedIndexChanged" AutoPostBack="true" onchange="ShowProgressBar();">
                                <asp:ListItem Text="All" Value="0" />
                                <asp:ListItem Text="Live" Value="1" />
                                <asp:ListItem Text="Completed" Value="2" />
                                <asp:ListItem Text="Upcoming" Value="3" />
                                <asp:ListItem Text="Suspended" Value="-1" />
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <!-- Legend Bar -->
            <div class="event-manager__legend-bar">
                <div class="event-manager__legend-bar__title">Legend</div>
                <div class="event-manager__legend-bar__legends-box">
                    <div class="event-manager__legend-bar__legends-box__legend">
                        <div class="event-manager__status event-manager__status--live"></div>
                        <span>Live</span>
                    </div>
                    <div class="event-manager__legend-bar__legends-box__legend">
                        <div class="event-manager__status event-manager__status--completed"></div>
                        <span>Completed</span>
                    </div>
                    <div class="event-manager__legend-bar__legends-box__legend">
                        <div class="event-manager__status event-manager__status--upcoming"></div>
                        <span>Upcoming</span>
                    </div>
                    <div class="event-manager__legend-bar__legends-box__legend">
                        <div class="event-manager__status event-manager__status--suspended"></div>
                        <span>Suspended</span>
                    </div>
                </div>
            </div>
            <!-- / Legend Bar-->

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upEvents" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvEvents" runat="server" OnItemDataBound="lvEvents_ItemDataBound" OnDataBound="lvEvents_DataBound" OnItemCommand="lvEvents_ItemCommand">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" Text="Welcome to Event Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding an event by clicking on the green button." />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table class="dataTable hover">
                                <thead>
                                    <tr>
                                        <th class="no-sort" style="width: 30px;">Status</th>
                                        <th>Events</th>
                                        <th class="no-sort">Description</th>
                                        <th class="no-sort">Topic</th>
                                        <th>Start</th>
                                        <th class="no-sort">End</th>
                                        <th>Participants</th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Panel ID="plStatus" runat="server" />
                                </td>
                                <td>
                                    <%--<%# DataBinder.Eval(Container.DataItem, "Title")  %>--%>
                                    <asp:LinkButton ID="LinkButton1" runat="server" ClientIDMode="AutoID" CommandName="EditEvent" Text='<%# DataBinder.Eval(Container.DataItem, "Title")  %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' Style="color: #999;" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbDescription" runat="server" ClientIDMode="AutoID" CommandName="EditEvent" Text='<%# DataBinder.Eval(Container.DataItem, "Description")  %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' Style="color: #999;" />
                                    <%--<%# DataBinder.Eval(Container.DataItem, "Description")  %>--%>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbTopicCount" runat="server" ClientIDMode="AutoID" CommandName="EditEvent" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfTopics")  %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' Style="color: #999;" />
                                    <%--<%# DataBinder.Eval(Container.DataItem, "NumberOfTopics")  %>--%>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbStartTimestamp" runat="server" ClientIDMode="AutoID" CommandName="EditEvent" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' Style="color: #999;">
                                    <%# DataBinder.Eval(Container.DataItem, "StartTimestamp","{0:dd/MM/yyyy}")%>
                                    <br />
                                    <%# DataBinder.Eval(Container.DataItem, "StartTimestamp","{0:hh:mm tt}")%> 
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbEndTimestamp" runat="server" ClientIDMode="AutoID" CommandName="EditEvent" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' Style="color: #999;">
                                    <%# DataBinder.Eval(Container.DataItem, "EndTimestamp","{0:dd/MM/yyyy}")%>
                                    <br />
                                    <%# DataBinder.Eval(Container.DataItem, "EndTimestamp","{0:hh:mm tt}")%>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:Panel ID="plParticipants" runat="server">
                                        <asp:LinkButton ID="ltParticipants" runat="server" ClientIDMode="AutoID" CommandName="EditEvent" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' Style="color: #999;" />
                                        <%--<asp:Literal ID="ltlParticipants" runat="server" />--%>
                                    </asp:Panel>
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" runat="server" ClientIDMode="AutoID" CommandName="EditEvent" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDel" runat="server" ClientIDMode="AutoID" CommandName="DelEvent" Text="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EventId")  %>' />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
