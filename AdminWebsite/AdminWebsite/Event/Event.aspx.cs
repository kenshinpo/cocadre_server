﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.Event
{
    public partial class Event : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private List<CassandraService.Entity.Event> eventList;

        public void GetAllEvent()
        {
            try
            {
                EventSelectAllResponse response = asc.SelectAllEvents(managerInfo.UserId, managerInfo.CompanyId, Convert.ToInt16(ddlEventStatus.SelectedValue));
                if (response.Success)
                {
                    eventList = response.Events;
                    for (int i = 0; i < eventList.Count; i++)
                    {
                        eventList[i].StartTimestamp = eventList[i].StartTimestamp.AddHours(managerInfo.TimeZone);
                        eventList[i].EndTimestamp = eventList[i].EndTimestamp.AddHours(managerInfo.TimeZone);
                        eventList[i].BannerVisibleDate = eventList[i].BannerVisibleDate.AddHours(managerInfo.TimeZone);
                    }
                    ltlEventCount.Text = Convert.ToString(response.Events.Count);
                }
                else
                {
                    Log.Error("EventSelectAllResponse.Success is false. Error message: " + response.ErrorMessage);
                    eventList = null;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    GetAllEvent();
                    lvEvents.DataSource = eventList;
                    lvEvents.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvEvents_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (eventList != null && eventList.Count > 0)
                {
                    Panel plStatus = e.Item.FindControl("plStatus") as Panel;
                    Panel plParticipants = e.Item.FindControl("plParticipants") as Panel;
                    LinkButton ltParticipants = e.Item.FindControl("ltParticipants") as LinkButton;

                    if (eventList[e.Item.DataItemIndex].Status == (int)CassandraService.Entity.Event.EventStatusEnum.Completed)
                    {
                        plStatus.CssClass = "event-manager__status event-manager__status--completed";
                    }
                    else if (eventList[e.Item.DataItemIndex].Status == (int)CassandraService.Entity.Event.EventStatusEnum.Live)
                    {
                        plStatus.CssClass = "event-manager__status event-manager__status--live";
                    }
                    else if (eventList[e.Item.DataItemIndex].Status == (int)CassandraService.Entity.Event.EventStatusEnum.Suspended)
                    {
                        plStatus.CssClass = "event-manager__status event-manager__status--suspended";
                    }
                    else if (eventList[e.Item.DataItemIndex].Status == (int)CassandraService.Entity.Event.EventStatusEnum.Upcoming)
                    {
                        plStatus.CssClass = "event-manager__status event-manager__status--upcoming";
                    }
                    else
                    {
                        plStatus.CssClass = "";
                    }

                    if (eventList[e.Item.DataItemIndex].ParticipantType == (int)CassandraService.Entity.Event.ParticipantTypeEnum.Department)
                    {
                        plParticipants.CssClass = "event-manager__participants event-manager__participants--department";
                        ltParticipants.Text = "Department";
                    }
                    else if (eventList[e.Item.DataItemIndex].ParticipantType == (int)CassandraService.Entity.Event.ParticipantTypeEnum.Everyone)
                    {
                        plParticipants.CssClass = "event-manager__participants event-manager__participants--everyone";
                        ltParticipants.Text = "Everyone";
                    }
                    else if (eventList[e.Item.DataItemIndex].ParticipantType == (int)CassandraService.Entity.Event.ParticipantTypeEnum.Personnel)
                    {
                        plParticipants.CssClass = "event-manager__participants event-manager__participants--personnel";
                        ltParticipants.Text = "Personnel";
                    }
                    else
                    {
                        plParticipants.CssClass = "";
                        ltParticipants.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ddlEventStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAllEvent();
                lvEvents.DataSource = eventList;
                lvEvents.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvEvents_DataBound(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbDelCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                EventUpdateResponse response = asc.DeleteEvent(managerInfo.UserId, managerInfo.CompanyId, hfEventId.Value);
                if (response.Success)
                {
                    mpePop.Hide();
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    MessageUtility.ShowToast(this.Page, "Survey question deleted", MessageUtility.TOAST_TYPE_INFO);
                    GetAllEvent();
                    lvEvents.DataSource = eventList;
                    lvEvents.DataBind();
                }
                else
                {
                    Log.Error("EventUpdateResponse.Success is false. Error message: " + response.ErrorMessage);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    MessageUtility.ShowToast(this.Page, "Failed to delete survey question", MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvEvents_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("EditEvent"))
                {
                    Response.Redirect("/Event/EventEdit/" + e.CommandArgument.ToString(), false);
                }
                else if (e.CommandName.Equals("DelEvent"))
                {
                    EventSelectResponse response = asc.SelectEvent(managerInfo.UserId, e.CommandArgument.ToString(), managerInfo.CompanyId);
                    if (response.Success)
                    {
                        hfEventId.Value = e.CommandArgument.ToString();
                        ltlDelEventTitle.Text = response.Event.Title;
                        mpePop.Show();
                    }
                    else
                    {
                        Log.Error("EventSelectResponse.Success is false. Error message: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}