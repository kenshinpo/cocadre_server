﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EventCreate.aspx.cs" Inherits="AdminWebsite.Event.EventCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="/Css/Calendar.css" />

    <style>
        #main_content_cblAvailableTopics,
        #main_content_cblUnavailableTopics,
        #main_content_cblAvailableDepartments,
        #main_content_cblUnavailableDepartments {
            margin-bottom: 20px;
            display: block;
            clear: both;
            height: 100%;
            padding-bottom: 20px;
        }

            #main_content_cblAvailableTopics input,
            #main_content_cblAvailableTopics label,
            #main_content_cblUnavailableTopics input,
            #main_content_cblUnavailableTopics label,
            #main_content_cblAvailableDepartments input,
            #main_content_cblAvailableDepartments label,
            #main_content_cblUnavailableDepartments input,
            #main_content_cblUnavailableDepartments label {
                display: inline;
                overflow: hidden;
                margin: 0px;
                vertical-align: middle;
                padding: 5px;
            }

            #main_content_cblAvailableTopics li,
            #main_content_cblUnavailableTopics li {
                width: 48%;
                padding: 0px;
                display: inline-block;
            }

            #main_content_cblAvailableDepartments li,
            #main_content_cblUnavailableDepartments li {
                width: 100%;
                padding: 0px;
                display: inline-block;
            }

        #main_content_popup_adduser {
            overflow: visible !important;
        }

        .event-manager__add-item-container .item label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .event-manager__add-item-container {
            width: 100%;
        }
        .event-manager__preview-event, .event-manager__add-event {
            display: inline-table;
        }
        .event-manager__preview-event .imageslider #slider .overlay__days-left__txt {
            font-size:8px;
        }

    </style>

    <script type="text/javascript">
        function textCounter(textBoxId, labelId, maxCount) {
            var textbox = document.getElementById(textBoxId);
            var label = document.getElementById(labelId);
            var countLength = maxCount - textbox.value.length;
            if (countLength < 0) {
                textbox.style.borderColor = "red";
            }
            else {
                textbox.style.borderColor = "";
            }
            label.innerHTML = countLength;
        }

        function preview() {
            var tbEventDescription = $get("<%=tbEventDescription.ClientID%>");

            if ($get("<%=tbEventTitle.ClientID%>").value.length > 15) {
                var msg = $get("<%=tbEventTitle.ClientID%>").value;
                msg = msg.substring(0, 16) + "...";
                $('#spEventTitle').html(msg);
            }
            else {
                $('#spEventTitle').html($get("<%=tbEventTitle.ClientID%>").value);
            }

            if ($get("<%=tbEventDescription.ClientID%>").value.length > 100) {
                var msg = $get("<%=tbEventDescription.ClientID%>").value;
                msg = msg.substring(0, 101) + "...";
                $('#divEventDesc').html(msg);
            }
            else {
                $('#divEventDesc').html($get("<%=tbEventDescription.ClientID%>").value);
            }
        }

        /* For search */
        var _timer = 0;
        function RefreshUpdatePanel(event, elementId) {
            if (event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 91 || event.keyCode == 93 || event.keyCode == 144 || (event.keyCode > 15 && event.keyCode < 21) || (event.keyCode > 32 && event.keyCode < 41) || (event.keyCode > 111 && event.keyCode < 124)) {
                return false;
            }
            if (_timer) {
                window.clearTimeout(_timer);
            }
            _timer = window.setTimeout(function () {
                __doPostBack(elementId, '');
            }, 500);

            var progress = $('#imgLoading');
            progress.fadeToggle();
        }

        $(document).on("keydown", function (e) {
            if (e.which === 8 && !$(e.target).is("input, textarea")) {
                e.preventDefault();
            }
        });

        function showLoading() {
            var progress = $('#imgLoading');
            progress.fadeToggle();
        }

        function SetFocus(elementId) {
            var textBox = document.getElementById(elementId);
            var elemLen = textBox.value.length;
            if (document.selection) {
                // Set focus
                // Use IE Ranges
                var oSel = document.selection.createRange();
                // Reset position to 0 & then set at end
                oSel.moveStart('character', -elemLen);
                oSel.moveStart('character', elemLen);
                oSel.moveEnd('character', 0);
                oSel.select();
            }
            else if (textBox.selectionStart || textBox.selectionStart == '0') {
                // Firefox/Chrome
                textBox.selectionStart = elemLen;
                textBox.selectionEnd = elemLen;
            }
            textBox.focus();
        }
        /* For search */

        /// Check banner datetime
        function checkBannerDatetime() {
            try {
                // Rule: Banner date must be earlier than Start date
                var startDateTimne;
                var bannerDateTime;

                // Check input
                if (moment($('#main_content_tbStartDate').val() + " " + $('#main_content_tbStartHour').val() + ":" + $('#main_content_tbStartMinute').val() + " " + $('#main_content_ddlStartMer').val(), "DD-MM-YYYY h:mm A").isValid()) {
                    startDateTimne = moment($('#main_content_tbStartDate').val() + " " + $('#main_content_tbStartHour').val() + ":" + $('#main_content_tbStartMinute').val() + " " + $('#main_content_ddlStartMer').val(), "DD-MM-YYYY h:mm A").format("YYYY-MM-DDTHH:mm:ss");
                }
                else {
                    ReloadErrorToast();
                    toastr.error('Please enter correct date.');
                    return false;
                }

                if (moment($('#main_content_tbBannerDate').val() + " " + $('#main_content_tbBannerHour').val() + ":" + $('#main_content_tbBannerMinute').val() + " " + $('#main_content_ddlBannerMer').val(), "DD-MM-YYYY h:mm A").isValid()) {
                    bannerDateTime = moment($('#main_content_tbBannerDate').val() + " " + $('#main_content_tbBannerHour').val() + ":" + $('#main_content_tbBannerMinute').val() + " " + $('#main_content_ddlBannerMer').val(), "DD-MM-YYYY h:mm A").format("YYYY-MM-DDTHH:mm:ss");
                }
                else {
                    ReloadErrorToast();
                    toastr.error('Please enter correct date.');
                    return false;
                }

                // Check rule.
                if (!(moment(bannerDateTime).isBefore(startDateTimne)))
                {
                        ReloadErrorToast();
                        toastr.error('Banner date should be earlier than start date.');
                        return false;
                }

            } catch (e) {
                ReloadErrorToast();
                toastr.error('Please enter correct date.');
            }
        }
        /// Check banner datetime

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:UpdatePanel ID="upPop" runat="server" UpdateMode="Conditional">
        <ContentTemplate>

            <!-- Active: add department -->
            <asp:Panel ID="popup_adddepartment" runat="server" CssClass="popup popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">Select Department
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p style="font-weight: bold;">Available</p>
                            <asp:CheckBoxList ID="cblAvailableDepartments" runat="server" RepeatLayout="OrderedList" />
                            <hr />
                            <p style="font-weight: bold;">Unavailable</p>
                            <asp:CheckBoxList ID="cblUnavailableDepartments" runat="server" RepeatLayout="OrderedList" />
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbSelectDepartment" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" Text="Select" OnClick="lbSelectDepartment_Click" />
                    <asp:LinkButton ID="lbSelectDepartmentCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Active: add department -->

            <!-- Active: add user -->
            <asp:Panel ID="popup_adduser" runat="server" CssClass="popup popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">Select User
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <asp:UpdatePanel ID="upUser" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Repeater ID="rtPreviewSelectedUsers" runat="server" OnItemCommand="rtPreviewSelectedUsers_ItemCommand">
                                        <HeaderTemplate>
                                            <div class="event-manager__add-item-container">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="item">
                                                <label><%# DataBinder.Eval(Container.DataItem, "FirstName") + " " + DataBinder.Eval(Container.DataItem, "LastName") %></label>
                                                <asp:LinkButton ID="lbRemove" runat="server" ToolTip="Remove topic" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId") %>' ClientIDMode="AutoID">
                                                        <div class="close"></div>    
                                                </asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <div class="suspendedsearch" style="width: 60%;">
                                        <asp:TextBox ID="tbSearchKey" runat="server" placeholder="Search Personnel" onkeydown="return (event.keyCode!=13);showLoading();" MaxLength="50" OnTextChanged="tbSearchKey_TextChanged" autocomplete="off" />
                                        <img id="imgLoading" src="/Img/circle_loading.gif" style="display: none;" />
                                        <div class="suggestions">
                                            <asp:Repeater ID="rtSearchResult" runat="server" OnItemDataBound="rtSearchResult_ItemDataBound" OnItemCommand="rtSearchResult_ItemCommand">
                                                <HeaderTemplate>
                                                    <ul class="suggestions__list">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li class="suggestions__list__item">
                                                        <asp:LinkButton ID="lbAddUser" runat="server" CssClass="suggestions__link" ClientIDMode="AutoID" CommandName="AddUser" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId")+ "," + DataBinder.Eval(Container.DataItem, "FirstName") +"," + DataBinder.Eval(Container.DataItem, "LastName") %>'>
                                                            <span class="suggestions__name">
                                                                <asp:Literal ID="ltlUserName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName")  %>' />
                                                            </span>
                                                            <span class="suggestions__email"><%# DataBinder.Eval(Container.DataItem, "Email") %></span>
                                                        </asp:LinkButton></li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </fieldset>
                    <div class="popup__action">
                        <asp:LinkButton ID="lbSelectUser" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" Text="Select" OnClick="lbSelectUser_Click" />
                        <asp:LinkButton ID="lbSelectUserCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                    </div>
                </div>

            </asp:Panel>
            <!-- /Active: add user -->

            <!-- Active: add topics -->
            <asp:Panel ID="popup_addtopic" runat="server" CssClass="popup popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">Select Topic </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p style="font-weight: bold;">Available</p>
                            <asp:CheckBoxList ID="cblAvailableTopics" runat="server" RepeatLayout="OrderedList" />
                            <hr />
                            <p style="font-weight: bold;">Unavailable</p>
                            <asp:CheckBoxList ID="cblUnavailableTopics" runat="server" RepeatLayout="OrderedList" />
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbSelectTopic" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" Text="Select" OnClick="lbSelectTopic_Click" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Active: add topics -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_addtopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Event <span>manager</span></div>
        <div class="appbar__meta">Add Event</div>
    </div>
    <!-- / App Bar -->
    <div class="data">
        <aside class="data__sidebar filter">

            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Event</a> </li>
            </ul>
        </aside>
        <div class="data__content">

            <!-- Add Event Form -->
            <div class="content event-manager__add-event" style="display:inline-table;">
                <div class="content__pad">
                    <table class="datatable">
                        <tbody>
                            <tr>
                                <td class="event-left-column-labels">Title</td>
                                <td class="event-right-column-fields">
                                    <div class="form__row">
                                        <asp:TextBox ID="tbEventTitle" runat="server" placeholder="Enter your title" CssClass="full-border" />
                                        <asp:Label ID="lblEventTitleCount" runat="server" CssClass="lettercount" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Description</td>
                                <td class="event-right-column-fields">
                                    <div class="form__row">
                                        <asp:TextBox ID="tbEventDescription" runat="server" placeholder="Enter your description" CssClass="full-border" TextMode="MultiLine" />
                                        <asp:Label ID="lblEventDescriptionCount" runat="server" CssClass="lettercount" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Start date</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" class="form__row">
                                        <ContentTemplate>
                                            <asp:TextBox ID="tbStartDate" runat="server" placeholder="dd/mm/yyyy" CssClass="date" AutoPostBack="true" OnTextChanged="tbEventDate_TextChanged" onkeydown="return false;" />
                                            <ajaxToolkit:CalendarExtender runat="server" TargetControlID="tbStartDate" ID="ceStart" CssClass="ajax__calendar" Format="dd/MM/yyyy" />
                                            <span class="text-vertical-center">at</span>
                                            <asp:TextBox ID="tbStartHour" runat="server" placeholder="hh" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="12" AutoPostBack="true" OnTextChanged="tbEventDate_TextChanged" />
                                            <span class="text-vertical-center">:</span>
                                            <asp:TextBox ID="tbStartMinute" runat="server" placeholder="mm" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="00" AutoPostBack="true" OnTextChanged="tbEventDate_TextChanged" />
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlStartMer" runat="server" OnSelectedIndexChanged="ddlEventDate_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="am" Value="am" />
                                                    <asp:ListItem Text="pm" Value="pm" />
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="lblStartDate" runat="server" Text="Label" Visible="false" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">End date</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" class="form__row">
                                        <ContentTemplate>
                                            <asp:TextBox ID="tbEndDate" runat="server" placeholder="dd/mm/yyyy" CssClass="date" AutoPostBack="true" OnTextChanged="tbEventDate_TextChanged" onkeydown="return false;" />
                                            <ajaxToolkit:CalendarExtender runat="server" TargetControlID="tbEndDate" CssClass="ajax__calendar" Format="dd/MM/yyyy" ID="ceEnd" />
                                            <span class="text-vertical-center">at</span>
                                            <asp:TextBox ID="tbEndHour" runat="server" placeholder="hh" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="12" AutoPostBack="true" OnTextChanged="tbEventDate_TextChanged" />
                                            <span class="text-vertical-center">:</span>
                                            <asp:TextBox ID="tbEndMinute" runat="server" placeholder="mm" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="00" AutoPostBack="true" OnTextChanged="tbEventDate_TextChanged" />
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlEndMer" runat="server" OnSelectedIndexChanged="ddlEventDate_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="am" Value="am" />
                                                    <asp:ListItem Text="pm" Value="pm" />
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="lblEndDate" runat="server" Text="Label" Visible="false" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Participant</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="form__row" style="display: inline-block;">
                                        <ContentTemplate>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlParticipant" runat="server" OnSelectedIndexChanged="ddlParticipant_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="Everyone" Value="1" Selected="True" />
                                                    <asp:ListItem Text="Department VS Department" Value="2" />
                                                    <asp:ListItem Text="Personnel VS Personnel" Value="3" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="subtext">Everyone that can see the selected topics only.</div>
                                            <asp:Repeater ID="rtSelectedDepartment" runat="server" OnItemCommand="rtSelectedDepartment_ItemCommand">
                                                <HeaderTemplate>
                                                    <div class="event-manager__add-item-container">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="item">
                                                        <label><%# DataBinder.Eval(Container.DataItem, "Title") %></label>
                                                        <asp:LinkButton ID="lbRemove" runat="server" ToolTip="Remove participant" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' ClientIDMode="AutoID">
                                                        <div class="close"></div>    
                                                        </asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton ID="lbAddDepartment" runat="server" CssClass="add-link" Visible="false" OnClick="lbAddDepartment_Click"><i class="fa fa-plus"></i>Add departments</asp:LinkButton>
                                            <asp:Repeater ID="rtSelectedUser" runat="server" OnItemCommand="rtSelectedUser_ItemCommand">
                                                <HeaderTemplate>
                                                    <div class="event-manager__add-item-container">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="item">
                                                        <label><%# DataBinder.Eval(Container.DataItem, "FirstName") + " " + DataBinder.Eval(Container.DataItem, "LastName") %></label>
                                                        <asp:LinkButton ID="lbRemove" runat="server" ToolTip="Remove user" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId") %>' ClientIDMode="AutoID">
                                                        <div class="close"></div>    
                                                        </asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton ID="lbAddUser" runat="server" CssClass="add-link" Visible="false" OnClick="lbAddUser_Click"><i class="fa fa-plus"></i>Add users</asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Select topic</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" class="form__row">
                                        <ContentTemplate>
                                            <asp:Repeater ID="rtSelectedTopic" runat="server" OnItemCommand="rtSelectedTopic_ItemCommand">
                                                <HeaderTemplate>
                                                    <div class="event-manager__add-item-container">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="item">
                                                        <label><%# DataBinder.Eval(Container.DataItem, "TopicTitle") %></label>
                                                        <asp:LinkButton ID="lbRemove" runat="server" ToolTip="Remove topic" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") %>' ClientIDMode="AutoID">
                                                        <div class="close"></div>    
                                                        </asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <br />
                                            <asp:LinkButton ID="lbAddTopic" runat="server" CssClass="add-link" OnClick="lbAddTopic_Click"><i class="fa fa-plus"></i>Add topics</asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Banner privacy</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" class="form__row">
                                        <ContentTemplate>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlBannerPrivacy" runat="server">
                                                    <asp:ListItem Text="Everyone" Value="1" />
                                                </asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Banner visible date</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" class="form__row">
                                        <ContentTemplate>
                                            <asp:TextBox ID="tbBannerDate" runat="server" placeholder="dd/mm/yyyy" CssClass="date" onkeydown="return false;" onchange="checkBannerDatetime();" />
                                            <ajaxToolkit:CalendarExtender runat="server" TargetControlID="tbBannerDate" ID="ceBanner" CssClass="ajax__calendar" Format="dd/MM/yyyy" />
                                            <span class="text-vertical-center">at</span>
                                            <asp:TextBox ID="tbBannerHour" runat="server" placeholder="hh" TextMode="Number" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="12" onchange="checkBannerDatetime();" />
                                            <span class="text-vertical-center">:</span>
                                            <asp:TextBox ID="tbBannerMinute" runat="server" placeholder="mm" TextMode="Number" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="00" onchange="checkBannerDatetime();" />
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlBannerMer" runat="server" onchange="checkBannerDatetime();">
                                                    <asp:ListItem Text="am" Value="am" />
                                                    <asp:ListItem Text="pm" Value="pm" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="subtext">You may arrange the date closer to the start date if not the banner will be shown to everyone once it is published</div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Scoring</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" class="form__row">
                                        <ContentTemplate>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlScoring" runat="server">
                                                    <asp:ListItem Text="Top scorer's score" Value="1" />
                                                </asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Calculated by</td>
                                <td class="event-right-column-fields">
                                    <asp:UpdatePanel ID="UpdatePanel9" runat="server" class="form__row">
                                        <ContentTemplate>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlCalculated" runat="server">
                                                    <asp:ListItem Text="Accumulative" Value="1" />
                                                    <asp:ListItem Text="Unique" Value="2" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="subtext">Winner determined by calculating the total correct questions answered</div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Show results</td>
                                <td class="event-right-column-fields">
                                    <div class="form__row">
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlShowResults" runat="server">
                                                <asp:ListItem Text="Yes" Value="true" />
                                                <asp:ListItem Text="No" Value="false" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="subtext">Show results during and after the end of the event in the app</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Result visibility</td>
                                <td class="event-right-column-fields">
                                    <div class="form__row">
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlResultVisibility" runat="server">
                                                <asp:ListItem Text="1 day after the event" Value="1" />
                                                <asp:ListItem Text="2 days after the event" Value="2" />
                                                <asp:ListItem Text="3 days after the event" Value="3" />
                                                <asp:ListItem Text="4 days after the event" Value="4" />
                                                <asp:ListItem Text="5 days after the event" Value="5" />
                                                <asp:ListItem Text="6 days after the event" Value="6" />
                                                <asp:ListItem Text="1 week after the event" Value="7" />
                                                <asp:ListItem Text="2 weeks after the event" Value="14" />
                                                <asp:ListItem Text="3 weeks after the event" Value="21" />
                                                <asp:ListItem Text="1 month after the event" Value="30" Selected="True" />
                                                <asp:ListItem Text="2 months after the event" Value="60" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="subtext">Result of the event will still be visible for the number of days selected</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="event-left-column-labels">Leaderboard</td>
                                <td class="event-right-column-fields">
                                    <div class="form__row">
                                        <div class="mdl-selectfield">
                                            <asp:DropDownList ID="ddlLeaderboard" runat="server" />
                                        </div>
                                        <div class="subtext">Number of top personnel to be shown</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="event-right-column-fields">
                                    <asp:LinkButton ID="lbPublish" runat="server" Text="PUBLISH" CssClass="btn" OnClientClick="ShowProgressBar();" OnClick="lbPublish_Click" />
                                    <a href="/Event/Event" class="popup__action__item popup__action__item--cancel">CANCEL</a> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- / Add Event Form -->
            <!-- Preview Event -->
            <asp:HiddenField ID="hfEventBgImg" runat="server" Value="https://s3-ap-southeast-1.amazonaws.com/cocadre/event-banners/event-banner_1_original.jpg" />
            <script>
                function SelectBgImg(imgValue) {
                    $("#<%= hfEventBgImg.ClientID%>").val(imgValue);
                }
            </script>
            <div class="content event-manager__preview-event" style="display:inline-table;">
                <div class="imageslider">
                    <div id="slider">
                        <!-- Slider Setup -->
                        <input checked type="radio" name="slider" id="slide1" selected="false" onchange="SelectBgImg('https://s3-ap-southeast-1.amazonaws.com/cocadre/event-banners/event-banner_1_original.jpg');" />
                        <input type="radio" name="slider" id="slide2" selected="false" onchange="SelectBgImg('https://s3-ap-southeast-1.amazonaws.com/cocadre/event-banners/event-banner_2_original.jpg');" />
                        <input type="radio" name="slider" id="slide3" selected="false" onchange="SelectBgImg('https://s3-ap-southeast-1.amazonaws.com/cocadre/event-banners/event-banner_3_original.jpg');" />
                        <!-- / Slider Setup -->
                        <!-- The Slider -->
                        <div id="slides">
                            <div id="overflow">
                                <div class="inner">
                                    <article>
                                        <div class="info">
                                        </div>
                                        <img src="https://s3-ap-southeast-1.amazonaws.com/cocadre/event-banners/event-banner_1_original.jpg" />
                                    </article>
                                    <article>
                                        <div class="info">
                                        </div>
                                        <img src="https://s3-ap-southeast-1.amazonaws.com/cocadre/event-banners/event-banner_2_original.jpg" />
                                    </article>
                                    <article>
                                        <div class="info">
                                        </div>
                                        <img src="https://s3-ap-southeast-1.amazonaws.com/cocadre/event-banners/event-banner_3_original.jpg" />
                                    </article>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="overlay">
                                        <div class="overlay__days-left-backing"></div>
                                        <div class="overlay__banner-header">Event: <span id="spEventTitle"></span></div>
                                        <div class="overlay__days-left">
                                            <div class="overlay__days-left__txt">
                                                <asp:Literal ID="ltlInfoDay1" runat="server" />
                                            </div>
                                            <div class="overlay__days-left__count">
                                                <asp:Literal ID="ltlInfoDayCount" runat="server" />
                                            </div>
                                            <div class="overlay__days-left__txt">
                                                <asp:Literal ID="ltlInfoDay2" runat="server" />
                                            </div>
                                        </div>
                                        <asp:Image ID="imgTopic" CssClass="overlay__topic-icon" runat="server" ImageUrl="~/Img/event-topic-icon-all.png" />
                                        <div class="overlay__topic-description" id="divEventDesc">
                                        </div>
                                        <div class="overlay__topic-title">
                                            <asp:Literal ID="ltlTopicTitle" runat="server" Text="Topic title" />
                                        </div>
                                        <img class="overlay__play-btn" src="../img/play-colleague.png" />
                                        <div class="overlay__event-details">
                                            <asp:Literal ID="ltlParticipant" runat="server" Text="Everyone" />
                                            <br />
                                            <asp:Literal ID="ltlScoring" runat="server" Text="Top scorer's score" />

                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <!-- / The Slider -->

                        <!-- Controls and Active Slide Display -->
                        <div id="controls" class="controls">
                            <label for="slide1"></label>
                            <label for="slide2"></label>
                            <label for="slide3"></label>
                        </div>
                        <!-- / Controls and Active Slide Display -->
                    </div>
                </div>
                <div class="subtext">Result of the event will still be visible for the number of days selected</div>
            </div>
            <!-- / Preview Event -->
        </div>
    </div>
</asp:Content>
