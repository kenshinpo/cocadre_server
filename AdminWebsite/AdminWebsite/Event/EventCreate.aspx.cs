﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Event
{
    public partial class EventCreate : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();

        private const int MAX_COUNT_EVENT_TITLE = 25;
        private const int MAX_COUNT_EVENT_DESCRIPTION = 120;

        private List<Department> availableDepartments;
        private List<Department> unavailableDepartments;
        private List<User> listUser;
        private List<Topic> availableTopics;
        private List<Topic> unavailableTopics;

        public void GetTopics()
        {
            try
            {
                // Get selected userId
                List<string> selectedUserId = new List<string>();
                List<User> selectedUsers = ViewState["selected_user"] as List<User>;
                if (selectedUsers != null && selectedUsers.Count != 0)
                {
                    for (int i = 0; i < selectedUsers.Count; i++)
                    {
                        selectedUserId.Add(selectedUsers[i].UserId);
                    }
                }

                // Get selected departmentId
                List<string> selectedDepartmentId = new List<string>();
                List<Department> selectedDepartments = ViewState["selected_department"] as List<Department>;
                if (selectedDepartments != null && selectedDepartments.Count != 0)
                {
                    for (int i = 0; i < selectedDepartments.Count; i++)
                    {
                        selectedDepartmentId.Add(selectedDepartments[i].Id);
                    }
                }

                EventSelectTopicResponse response = asc.SelectTopicsForEvent(managerInfo.UserId, managerInfo.CompanyId, Convert.ToInt16(ddlParticipant.SelectedValue), selectedUserId, selectedDepartmentId);
                if (response.Success)
                {
                    availableTopics = response.AvailableTopics;
                    unavailableTopics = response.UnavailableTopics;
                }
                else
                {
                    availableTopics = null;
                    unavailableTopics = null;
                    Log.Error("EventSelectTopicResponse.Success is false. Error message: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void GetDepartment()
        {
            try
            {
                // Get selected topicId
                List<string> selectedTopicId = new List<string>();
                List<Topic> selectedTopics = ViewState["selected_topic"] as List<Topic>;
                if (selectedTopics != null && selectedTopics.Count != 0)
                {
                    for (int i = 0; i < selectedTopics.Count; i++)
                    {
                        selectedTopicId.Add(selectedTopics[i].TopicId);
                    }
                }

                EventSelectDepartmentResponse response = asc.SelectDepartmentsForEvent(managerInfo.UserId, managerInfo.CompanyId, selectedTopicId);
                if (response.Success)
                {
                    availableDepartments = response.AvailableDepartments;
                    unavailableDepartments = response.UnavailableDepartments;
                }
                else
                {
                    availableDepartments = null;
                    unavailableDepartments = null;
                    Log.Error("EventSelectDepartmentResponse.Success is false. Error message: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void GetUser()
        {
            try
            {
                if (!String.IsNullOrEmpty(tbSearchKey.Text.Trim()))
                {
                    // Get selected topicId
                    List<string> selectedTopicId = new List<string>();
                    List<Topic> selectedTopics = ViewState["selected_topic"] as List<Topic>;
                    if (selectedTopics != null && selectedTopics.Count != 0)
                    {
                        for (int i = 0; i < selectedTopics.Count; i++)
                        {
                            selectedTopicId.Add(selectedTopics[i].TopicId);
                        }
                    }

                    EventSelectUserResponse response = asc.SelectUsersForEvent(managerInfo.UserId, managerInfo.CompanyId, selectedTopicId, tbSearchKey.Text.Trim());
                    if (response.Success)
                    {
                        List<User> searchResultUsers = response.Users;
                        List<User> selectedUsers = ViewState["selected_user"] as List<User>;
                        if (selectedUsers != null && selectedUsers.Count > 0)
                        {
                            for (int i = 0; i < selectedUsers.Count; i++)
                            {
                                searchResultUsers.RemoveAll(u => u.UserId == selectedUsers[i].UserId);
                            }
                        }

                        listUser = searchResultUsers;
                    }
                    else
                    {
                        listUser = null;
                        Log.Error("EventSelectUserResponse.Success is false. Error message: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void CheckEventDateTime()
        {
            DateTime today = DateTime.UtcNow;

            try
            {
                DateTime dtStart = DateTime.ParseExact(tbStartDate.Text + " " + tbStartHour.Text.PadLeft(2, '0') + ":" + tbStartMinute.Text.PadLeft(2, '0') + " " + ddlStartMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).ToUniversalTime();

                DateTime dtEnd = DateTime.ParseExact(tbEndDate.Text + " " + tbEndHour.Text.PadLeft(2, '0') + ":" + tbEndMinute.Text.PadLeft(2, '0') + " " + ddlEndMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).ToUniversalTime();


                lblStartDate.Text = dtStart.ToString("yyyy/MM/dd hh:mm ttt");
                lblEndDate.Text = dtEnd.ToString("yyyy/MM/dd hh:mm ttt");

                #region 1. Start Date > End Date
                if (dtStart > dtEnd)
                {
                    MessageUtility.ShowToast(this.Page, "End date should not be early start date", MessageUtility.TOAST_TYPE_ERROR);
                    ltlInfoDay1.Text = String.Empty;
                    ltlInfoDayCount.Text = String.Empty;
                    ltlInfoDay2.Text = String.Empty;
                    return;
                }
                #endregion

                #region 2. End <= Today
                if (!(dtEnd > today))
                {
                    MessageUtility.ShowToast(this.Page, "End date should not be in the past", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                if (dtStart > today)
                {
                    ltlInfoDay1.Text = "Coming in";
                    if (dtStart.Subtract(today).Days > 0)
                    {
                        if (dtStart.Subtract(today).Days > 1)
                        {
                            if (dtStart.Subtract(today).Days > 99)
                            {
                                ltlInfoDayCount.Text = "99+";
                            }
                            else
                            {
                                ltlInfoDayCount.Text = dtStart.Subtract(today).Days.ToString();
                            }
                            ltlInfoDay2.Text = "days";
                        }
                        else
                        {
                            ltlInfoDayCount.Text = dtStart.Subtract(today).Days.ToString();
                            ltlInfoDay2.Text = "day";
                        }
                    }
                    else
                    {
                        if (dtStart.Subtract(today).Hours > 0)
                        {
                            ltlInfoDayCount.Text = dtStart.Subtract(today).Hours.ToString();
                            if (dtStart.Subtract(today).Hours > 1)
                            {
                                ltlInfoDay2.Text = "hours";
                            }
                            else
                            {
                                ltlInfoDay2.Text = "hour";
                            }
                        }
                        else
                        {
                            if (dtStart.Subtract(today).Minutes > 0)
                            {
                                ltlInfoDayCount.Text = dtStart.Subtract(today).Minutes.ToString();
                                if (dtStart.Subtract(today).Minutes > 1)
                                {
                                    ltlInfoDay2.Text = "minutes";
                                }
                                else
                                {
                                    ltlInfoDay2.Text = "minute";
                                }
                            }
                        }
                    }
                }
                else
                {
                    int diffDays = dtEnd.Subtract(today).Days;

                    ltlInfoDay1.Text = "<br />";

                    if (diffDays > 1)
                    {
                        ltlInfoDay2.Text = "days left";
                        if (diffDays > 99)
                        {
                            ltlInfoDayCount.Text = "99+";
                        }
                        else
                        {
                            ltlInfoDayCount.Text = diffDays.ToString();
                        }
                    }
                    else
                    {
                        ltlInfoDayCount.Text = diffDays.ToString();
                        ltlInfoDay2.Text = "day left";
                    }
                }
            }
            catch (Exception)
            {
                MessageUtility.ShowToast(this.Page, "Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR);
                ltlInfoDay1.Text = String.Empty;
                ltlInfoDayCount.Text = String.Empty;
                ltlInfoDay2.Text = String.Empty;
            }
        }

        public void CheckBannerDateTime()
        {
            // Rule: Banner date < Start date
            try
            {
                DateTime dtStart = DateTime.ParseExact(tbStartDate.Text + " " + tbStartHour.Text.PadLeft(2, '0') + ":" + tbStartMinute.Text.PadLeft(2, '0') + " " + ddlStartMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture);

                DateTime dtBanner = DateTime.ParseExact(tbBannerDate.Text + " " + tbBannerHour.Text.PadLeft(2, '0') + ":" + tbBannerMinute.Text.PadLeft(2, '0') + " " + ddlBannerMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture);

                if (!(dtBanner < dtStart))
                {
                    MessageUtility.ShowToast(this.Page, "Banner date should be earlier than start date.", MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception)
            {
                MessageUtility.ShowToast(this.Page, "Please enter correct date.", MessageUtility.TOAST_TYPE_ERROR);
            }
        }

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["admin_info"] == null)
                {
                    Response.Redirect("/Logout", false);
                    return;
                }
                managerInfo = Session["admin_info"] as ManagerInfo;

                if (!IsPostBack)
                {
                    ViewState["selected_user"] = new List<User>();
                    ViewState["selected_department"] = new List<Department>();
                    ViewState["selected_topic"] = new List<Topic>();

                    tbEventTitle.MaxLength = MAX_COUNT_EVENT_TITLE;
                    lblEventTitleCount.Text = Convert.ToString(MAX_COUNT_EVENT_TITLE);
                    tbEventDescription.MaxLength = MAX_COUNT_EVENT_DESCRIPTION;
                    lblEventDescriptionCount.Text = Convert.ToString(MAX_COUNT_EVENT_DESCRIPTION);

                    tbEventTitle.Attributes["onkeyup"] = String.Format("textCounter('{0}','{1}', {2}); preview();", tbEventTitle.ClientID, lblEventTitleCount.ClientID, MAX_COUNT_EVENT_TITLE);
                    tbEventDescription.Attributes["onkeyup"] = String.Format("textCounter('{0}','{1}', {2}); preview();", tbEventDescription.ClientID, lblEventDescriptionCount.ClientID, MAX_COUNT_EVENT_DESCRIPTION);

                    DateTime today = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day);
                    tbStartDate.Text = today.AddDays(1).ToString("dd/MM/yyyy");
                    tbEndDate.Text = today.AddDays(8).ToString("dd/MM/yyyy");
                    ltlInfoDay1.Text = "Coming in";
                    ltlInfoDayCount.Text = "1";
                    ltlInfoDay2.Text = "day";
                    ceEnd.StartDate = today.AddDays(1);

                    tbBannerDate.Text = today.ToString("dd/MM/yyyy");

                    ddlLeaderboard.Items.Clear();
                    for (int i = 0; i < 50; i++)
                    {
                        ddlLeaderboard.Items.Add(new ListItem((i + 1).ToString(), (i + 1).ToString()));
                    }
                }
                else
                {
                    if (tbEventTitle.Text.Trim().Length > MAX_COUNT_EVENT_TITLE)
                    {
                        tbEventTitle.Style.Add("border-color", "red");
                    }
                    else
                    {
                        tbEventTitle.Style.Remove("border-color");
                    }

                    if (tbEventDescription.Text.Trim().Length > MAX_COUNT_EVENT_DESCRIPTION)
                    {
                        tbEventDescription.Style.Add("border-color", "red");
                    }
                    else
                    {
                        tbEventDescription.Style.Remove("border-color");
                    }
                    lblEventTitleCount.Text = (MAX_COUNT_EVENT_TITLE - tbEventTitle.Text.Trim().Length).ToString();
                    lblEventDescriptionCount.Text = (MAX_COUNT_EVENT_DESCRIPTION - tbEventDescription.Text.Trim().Length).ToString();

                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "Preview", "preview();", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void tbEventDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CheckEventDateTime();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ddlEventDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckEventDateTime();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void tbBannerDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBannerDateTime();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbPublish_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check Data
                #region Step 1.1 Check Title
                if (String.IsNullOrEmpty(tbEventTitle.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the Event title.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (tbEventTitle.Text.Trim().Length > 25)
                {
                    ShowToastMsgAndHideProgressBar("Event title is too long.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Step 1.2 Check Description
                if (String.IsNullOrEmpty(tbEventDescription.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the Event description.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (tbEventDescription.Text.Trim().Length > 120)
                {
                    ShowToastMsgAndHideProgressBar("Event description is too long.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Step 1.3 Check Event Date
                DateTime dtStart;
                try
                {
                    dtStart = DateTime.ParseExact(tbStartDate.Text + " " + tbStartHour.Text.PadLeft(2, '0') + ":" + tbStartMinute.Text.PadLeft(2, '0') + " " + ddlStartMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).AddHours(-managerInfo.TimeZone);
                }
                catch (Exception)
                {
                    ShowToastMsgAndHideProgressBar("Start date is invalid.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                DateTime dtEnd;
                try
                {
                    dtEnd = DateTime.ParseExact(tbEndDate.Text + " " + tbEndHour.Text.PadLeft(2, '0') + ":" + tbEndMinute.Text.PadLeft(2, '0') + " " + ddlEndMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).AddHours(-managerInfo.TimeZone);
                }
                catch (Exception)
                {
                    ShowToastMsgAndHideProgressBar("End date is invalid.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                DateTime dtToday = DateTime.UtcNow;
                if (dtEnd < dtStart)
                {
                    ShowToastMsgAndHideProgressBar("Please check your start and end dates", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (!(dtEnd > dtToday))
                {
                    ShowToastMsgAndHideProgressBar("Please check your start and end dates", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Step 1.4 Check Banner Date
                DateTime dtBanner;
                try
                {
                    dtBanner = DateTime.ParseExact(tbBannerDate.Text + " " + tbBannerHour.Text.PadLeft(2, '0') + ":" + tbBannerMinute.Text.PadLeft(2, '0') + " " + ddlBannerMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).AddHours(-managerInfo.TimeZone);
                }
                catch (Exception)
                {
                    ShowToastMsgAndHideProgressBar("Banner date is invalid.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (!(dtBanner < dtStart))
                {
                    ShowToastMsgAndHideProgressBar("Banner date should be earlier than start date.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Step 1.5 Topics
                List<String> listTopicId = new List<String>();
                List<Topic> selectedTopics = ViewState["selected_topic"] as List<Topic>;
                if (selectedTopics.Count == 0)
                {
                    ShowToastMsgAndHideProgressBar("Please choose a topic.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                else
                {
                    for (int i = 0; i < selectedTopics.Count; i++)
                    {
                        listTopicId.Add(selectedTopics[i].TopicId);
                    }
                }
                #endregion

                #region Step 1.6 Departments
                List<String> listDepartmentId = new List<String>();
                if (Convert.ToInt16(ddlParticipant.SelectedValue) == (int)CassandraService.Entity.Event.ParticipantTypeEnum.Department)
                {
                    List<Department> selectedDepartments = ViewState["selected_department"] as List<Department>;
                    if (selectedDepartments == null || selectedDepartments.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please choose a department.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedDepartments.Count; i++)
                        {
                            listDepartmentId.Add(selectedDepartments[i].Id);
                        }
                    }
                }
                #endregion

                #region Step 1.7 Users
                List<String> listUserId = new List<String>();
                if (Convert.ToInt16(ddlParticipant.SelectedValue) == (int)CassandraService.Entity.Event.ParticipantTypeEnum.Personnel)
                {
                    List<User> selectedUsers = ViewState["selected_user"] as List<User>;
                    if (selectedUsers == null || selectedUsers.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please choose a user.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedUsers.Count; i++)
                        {
                            listUserId.Add(selectedUsers[i].UserId);
                        }
                    }
                }
                #endregion

                #region Step 1.8 Event type
                int evenetType = (int)CassandraService.Entity.Event.EventTypeEnum.OneTopic;
                if (listTopicId.Count > 1)
                {
                    evenetType = (int)CassandraService.Entity.Event.EventTypeEnum.MultipleTopics;
                }
                #endregion

                #endregion

                #region Setp 2. Call API
                EventCreateResponse response = asc.CreateEvent(managerInfo.UserId,
                                                                managerInfo.CompanyId,
                                                                tbEventTitle.Text.Trim(),
                                                                tbEventDescription.Text.Trim(),
                                                                dtStart,
                                                                dtEnd,
                                                                evenetType,
                                                                Convert.ToInt16(ddlParticipant.SelectedValue),
                                                                Convert.ToInt16(ddlBannerPrivacy.SelectedValue),
                                                                dtBanner,
                                                                Convert.ToInt16(ddlScoring.SelectedValue),
                                                                Convert.ToInt16(ddlCalculated.SelectedValue),
                                                                Convert.ToBoolean(ddlShowResults.SelectedValue),
                                                                Convert.ToInt16(ddlResultVisibility.SelectedValue),
                                                                Convert.ToInt16(ddlLeaderboard.SelectedValue),
                                                                hfEventBgImg.Value,
                                                                listTopicId,
                                                                listDepartmentId,
                                                                listUserId);

                if (response.Success)
                {
                    ShowToastMsgAndHideProgressBar("Event have been created.", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Event/Event/',300);", true);
                }
                else
                {
                    Log.Error("EventCreateResponse.Success is false. Error message: " + response.ErrorMessage);
                    ShowToastMsgAndHideProgressBar("Event not created.", MessageUtility.TOAST_TYPE_ERROR);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ddlBannerDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBannerDateTime();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAddTopic_Click(object sender, EventArgs e)
        {
            try
            {
                bool isParticipantRight = true;
                if (ddlParticipant.SelectedValue.Equals("1"))// Everyone
                {
                    // nothing
                }
                else if (ddlParticipant.SelectedValue.Equals("2")) // Department VS Department
                {
                    List<Department> selectedDepartments = ViewState["selected_department"] as List<Department>;
                    if (selectedDepartments.Count < 1)
                    {
                        isParticipantRight = false;
                    }
                }
                else if (ddlParticipant.SelectedValue.Equals("3")) // Personnel VS Personnel
                {
                    List<User> selectedDepartments = ViewState["selected_user"] as List<User>;
                    if (selectedDepartments.Count < 1)
                    {
                        isParticipantRight = false;
                    }
                }
                else // Other
                {

                }

                if (isParticipantRight)
                {
                    GetTopics();
                    cblAvailableTopics.Items.Clear();
                    List<Topic> selectedTopics = ViewState["selected_topic"] as List<Topic>;
                    for (int i = 0; i < availableTopics.Count; i++)
                    {
                        cblAvailableTopics.Items.Add(new ListItem(availableTopics[i].TopicTitle, availableTopics[i].TopicId + "," + availableTopics[i].TopicLogoUrl));

                        for (int j = 0; j < selectedTopics.Count; j++)
                        {
                            if (availableTopics[i].TopicId.Equals(selectedTopics[j].TopicId))
                            {
                                cblAvailableTopics.Items[i].Selected = true;
                                break;
                            }
                        }
                    }

                    cblUnavailableTopics.Items.Clear();
                    for (int k = 0; k < unavailableTopics.Count; k++)
                    {
                        cblUnavailableTopics.Items.Add(new ListItem(unavailableTopics[k].TopicTitle, unavailableTopics[k].TopicId + "," + unavailableTopics[k].TopicLogoUrl, false));
                    }

                    mpePop.PopupControlID = "popup_addtopic";
                    mpePop.Show();
                    upPop.Update();
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Please select participant.", MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ddlParticipant_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ResetSetting();
                if (ddlParticipant.SelectedValue.Equals("1"))// Everyone
                {
                    ltlParticipant.Text = "Everyone";
                    // Calculated by
                    ddlCalculated.Items.Add(new ListItem("Accumulative", ((int)CassandraService.Entity.Event.CalculationTypeEnum.Accumulative).ToString()));
                    ddlCalculated.Items.Add(new ListItem("Unique", ((int)CassandraService.Entity.Event.CalculationTypeEnum.Unique).ToString()));
                }
                else if (ddlParticipant.SelectedValue.Equals("2")) // Department VS Department
                {
                    lbAddDepartment.Visible = true;
                    ltlParticipant.Text = "Department VS Department";
                    // Banner privacy
                    ddlBannerPrivacy.Items.Add(new ListItem("Participants only", ((int)CassandraService.Entity.Event.BannerPrivacyTypeEnum.Participant).ToString()));
                    // Scoring
                    ddlScoring.Items.Add(new ListItem("Top department's score", ((int)CassandraService.Entity.Event.ScoringTypeEnum.TopDepartment).ToString()));
                    // Calculated by
                    ddlCalculated.Items.Add(new ListItem("Total Accumulative", ((int)CassandraService.Entity.Event.CalculationTypeEnum.TotalAccumulative).ToString()));
                    ddlCalculated.Items.Add(new ListItem("Total Unique", ((int)CassandraService.Entity.Event.CalculationTypeEnum.TotalUnique).ToString()));
                    ddlCalculated.Items.Add(new ListItem("Average Accumulative", ((int)CassandraService.Entity.Event.CalculationTypeEnum.AverageAccumulative).ToString()));
                    ddlCalculated.Items.Add(new ListItem("Average Unique", ((int)CassandraService.Entity.Event.CalculationTypeEnum.AverageUnique).ToString()));
                }
                else if (ddlParticipant.SelectedValue.Equals("3")) // Personnel VS Personnel
                {
                    lbAddUser.Visible = true;
                    ltlParticipant.Text = "Personnel VS Personnel";
                    // Banner privacy
                    ddlBannerPrivacy.Items.Add(new ListItem("Participants only", ((int)CassandraService.Entity.Event.BannerPrivacyTypeEnum.Participant).ToString()));
                    // Calculated by
                    ddlCalculated.Items.Add(new ListItem("Accumulative", ((int)CassandraService.Entity.Event.CalculationTypeEnum.Accumulative).ToString()));
                    ddlCalculated.Items.Add(new ListItem("Unique", ((int)CassandraService.Entity.Event.CalculationTypeEnum.Unique).ToString()));
                }
                else // Other
                {
                    // nothing
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        private void ResetSetting()
        {
            ViewState["selected_department"] = new List<Department>();
            rtSelectedDepartment.DataSource = new List<Department>();
            rtSelectedDepartment.DataBind();
            ViewState["selected_user"] = new List<User>();
            rtSelectedUser.DataSource = new List<User>();
            rtSelectedUser.DataBind();
            ViewState["selected_topic"] = new List<Topic>();
            rtSelectedTopic.DataSource = new List<Topic>();
            rtSelectedTopic.DataBind();
            lbAddDepartment.Visible = false;
            lbAddUser.Visible = false;
            // Banner privacy
            ddlBannerPrivacy.Items.Clear();
            ddlBannerPrivacy.Items.Add(new ListItem("Everyone", ((int)CassandraService.Entity.Event.BannerPrivacyTypeEnum.Everyone).ToString()));
            // Scoring
            ddlScoring.Items.Clear();
            ddlScoring.Items.Add(new ListItem("Top scorer's score", ((int)CassandraService.Entity.Event.ScoringTypeEnum.TopScorer).ToString()));
            // Calculated by
            ddlCalculated.Items.Clear();

            // Pewview
            imgTopic.ImageUrl = "~/Img/event-topic-icon-all.png";
            ltlTopicTitle.Text = "Topic title";
            ltlParticipant.Text = "Everyone";
        }

        protected void lbActionCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSelectTopic_Click(object sender, EventArgs e)
        {
            try
            {
                List<Topic> selectedTopics = new List<Topic>();
                for (int i = 0; i < cblAvailableTopics.Items.Count; i++)
                {
                    if (cblAvailableTopics.Items[i].Selected)
                    {
                        selectedTopics.Add(new Topic { TopicId = cblAvailableTopics.Items[i].Value.Split(',')[0], TopicLogoUrl = cblAvailableTopics.Items[i].Value.Split(',')[1], TopicTitle = cblAvailableTopics.Items[i].Text });
                    }
                }
                ViewState["selected_topic"] = selectedTopics;
                rtSelectedTopic.DataSource = selectedTopics;
                rtSelectedTopic.DataBind();

                if (selectedTopics.Count == 1) // One topic
                {
                    imgTopic.ImageUrl = selectedTopics[0].TopicLogoUrl;
                    ltlTopicTitle.Text = selectedTopics[0].TopicTitle;
                }
                else if (selectedTopics.Count > 1) // Multi topics
                {
                    imgTopic.ImageUrl = "../Img/event-topic-icon-all.png";
                    ltlTopicTitle.Text = "Multiple Topics";
                }
                else // No topic
                {
                    imgTopic.ImageUrl = "../Img/event-topic-icon-all.png";
                    ltlTopicTitle.Text = "Topic title";
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSelectedTopic_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Remove"))
                {
                    List<Topic> selectedTopics = ViewState["selected_topic"] as List<Topic>;
                    selectedTopics.RemoveAll(t => t.TopicId.Equals(e.CommandArgument.ToString()));
                    ViewState["selected_topic"] = selectedTopics;
                    rtSelectedTopic.DataSource = selectedTopics;
                    rtSelectedTopic.DataBind();

                    if (selectedTopics.Count == 1) // One topic
                    {
                        imgTopic.ImageUrl = selectedTopics[0].TopicLogoUrl;
                        ltlTopicTitle.Text = selectedTopics[0].TopicTitle;
                    }
                    else if (selectedTopics.Count > 1) // Multi topics
                    {
                        imgTopic.ImageUrl = "../Img/event-topic-icon-all.png";
                        ltlTopicTitle.Text = "Multiple Topics";
                    }
                    else // No topic
                    {
                        imgTopic.ImageUrl = "../Img/event-topic-icon-all.png";
                        ltlTopicTitle.Text = "Topic title";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAddDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                GetDepartment();
                cblAvailableDepartments.Items.Clear();
                List<Department> selectedDepartments = ViewState["selected_department"] as List<Department>;
                for (int i = 0; i < availableDepartments.Count; i++)
                {
                    cblAvailableDepartments.Items.Add(new ListItem(availableDepartments[i].Title, availableDepartments[i].Id));

                    for (int j = 0; j < selectedDepartments.Count; j++)
                    {
                        if (availableDepartments[i].Id.Equals(selectedDepartments[j].Id))
                        {
                            cblAvailableDepartments.Items[i].Selected = true;
                            break;
                        }
                    }
                }

                cblUnavailableDepartments.Items.Clear();
                for (int k = 0; k < unavailableDepartments.Count; k++)
                {
                    cblUnavailableDepartments.Items.Add(new ListItem(unavailableDepartments[k].Title, unavailableDepartments[k].Id, false));
                }

                mpePop.PopupControlID = "popup_adddepartment";
                mpePop.Show();
                upPop.Update();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSelectDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                List<Department> selectedDepartments = new List<Department>();
                for (int i = 0; i < cblAvailableDepartments.Items.Count; i++)
                {
                    if (cblAvailableDepartments.Items[i].Selected)
                    {
                        selectedDepartments.Add(new Department { Id = cblAvailableDepartments.Items[i].Value, Title = cblAvailableDepartments.Items[i].Text });
                    }
                }
                ViewState["selected_department"] = selectedDepartments;
                rtSelectedDepartment.DataSource = selectedDepartments;
                rtSelectedDepartment.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSelectedDepartment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Remove"))
                {
                    List<Department> selectedDepartments = ViewState["selected_department"] as List<Department>;
                    selectedDepartments.RemoveAll(d => d.Id.Equals(e.CommandArgument.ToString()));
                    ViewState["selected_department"] = selectedDepartments;
                    rtSelectedDepartment.DataSource = selectedDepartments;
                    rtSelectedDepartment.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAddUser_Click(object sender, EventArgs e)
        {
            try
            {
                List<User> selectedUsers = ViewState["selected_user"] as List<User>;
                rtPreviewSelectedUsers.DataSource = selectedUsers;
                rtPreviewSelectedUsers.DataBind();

                tbSearchKey.Attributes.Add("onkeyup", string.Format("RefreshUpdatePanel(event, '{0}');", tbSearchKey.ClientID));
                mpePop.PopupControlID = "popup_adduser";
                mpePop.Show();
                upPop.Update();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void tbSearchKey_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetUser();
                rtSearchResult.DataSource = listUser;
                rtSearchResult.DataBind();
                upUser.Update();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetFocus", string.Format("SetFocus('{0}');", tbSearchKey.ClientID), true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddUser"))
                {
                    List<User> selectedList = ViewState["selected_user"] as List<User>;
                    String[] value = Convert.ToString(e.CommandArgument).Split(',');
                    User user = new User();
                    user.UserId = value[0];
                    user.FirstName = value[1];
                    user.LastName = value[2];
                    selectedList.Add(user);
                    rtPreviewSelectedUsers.DataSource = selectedList;
                    rtPreviewSelectedUsers.DataBind();
                    ViewState["selected_user"] = selectedList;
                    tbSearchKey.Text = String.Empty;
                    GetUser();
                    rtSearchResult.DataSource = listUser;
                    rtSearchResult.DataBind();
                    upUser.Update();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtPreviewSelectedUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Remove"))
                {
                    List<User> selectedUsers = ViewState["selected_user"] as List<User>;
                    selectedUsers.RemoveAll(u => u.UserId.Equals(e.CommandArgument.ToString()));
                    ViewState["selected_user"] = selectedUsers;
                    rtPreviewSelectedUsers.DataSource = selectedUsers;
                    rtPreviewSelectedUsers.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSelectUser_Click(object sender, EventArgs e)
        {
            try
            {
                List<User> selectedUsers = ViewState["selected_user"] as List<User>;
                rtSelectedUser.DataSource = selectedUsers;
                rtSelectedUser.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSelectedUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Remove"))
                {
                    List<User> selectedUsers = ViewState["selected_user"] as List<User>;
                    selectedUsers.RemoveAll(u => u.UserId.Equals(e.CommandArgument.ToString()));
                    ViewState["selected_user"] = selectedUsers;
                    rtSelectedUser.DataSource = selectedUsers;
                    rtSelectedUser.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}