﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace AdminWebsite.App_Code.Entity
{
    public class DynamicPulse
    {
        public class ResponsivePulse
        {
            [DataContract]
            public class Deck
            {
                [DataMember]
                public string Id { get; set; }
                [DataMember]
                public string Title { get; set; }
                [DataMember]
                public bool IsCompulsory { get; set; }
                [DataMember]
                public int Status { get; set; }
                [DataMember]
                public bool IsPrioritized { get; set; }
                [DataMember]
                public bool IsAnonymous { get; set; }
                [DataMember]
                public int PublishMethodType { get; set; }
                [DataMember]
                public DateTime StartTimestamp { get; set; }
                [DataMember]
                public DateTime? EndTimestamp { get; set; }
                [DataMember]
                public int NumberOfCardsPerTimeFrame { get; set; }

                [DataMember]
                public int PerTimeFrameType { get; set; }

                [DataMember]
                public int PeriodFrameType { get; set; }

                [DataMember]
                public int Duration { get; set; }

                /* privacy */
                [DataMember]
                public List<CassandraService.Entity.Department> TargetedDepartments { get; set; }
                [DataMember]
                public List<CassandraService.Entity.User> TargetedUsers { get; set; }
                [DataMember]
                public bool IsForEveryone { get; set; }

                [DataMember]
                public bool IsForDepartment { get; set; }
                [DataMember]
                public bool IsForUser { get; set; }
                /**********/
                [DataMember]
                public List<Pulse> Pulses { get; set; }
            }

            [DataContract]
            public class Pulse
            {
                [DataMember]
                public string PulseId { get; set; }
                [DataMember]
                public string DeckId { get; set; }
                [DataMember]
                public string Label { get; set; }
                [DataMember]
                public int QuestionType { get; set; }
                [DataMember]
                public string QuestionContent { get; set; }
                [DataMember]
                public string Description { get; set; }
                [DataMember]
                public string ImageUrl { get; set; }
                [DataMember]
                public List<Option> Options { get; set; }
                [DataMember]
                public int RangeType { get; set; }
                [DataMember]
                public string MinRangeLabel { get; set; }
                [DataMember]
                public string MaxRangeLabel { get; set; }
                [DataMember]
                public DateTime StartDate { get; set; }
                [DataMember]
                public string Ordering { get; set; }
                [DataMember]
                public int LogicLevel { get; set; }
                [DataMember]
                public bool IsCustomized { get; set; }
                [DataMember]
                public string ParentPulseId { get; set; }
                [DataMember]
                public List<Pulse> ChildrenPulse { get; set; }
            }

            [DataContract]
            public class Option
            {
                [DataMember]
                public string OptionId { get; set; }
                [DataMember]
                public string PulseId { get; set; }
                [DataMember]
                public string Content { get; set; }
                [DataMember]
                public int Ordering { get; set; }
                [DataMember]
                public string NextPulseId { get; set; }
                [DataMember]
                public string NextPulseLabel { get; set; }
            }
        }
    }

}