﻿using System;
using System.Collections.Generic;
using CassandraService.Entity;

namespace AdminWebsite.App_Code.Entity
{
    [Serializable]
    public class ManagerInfo
    {
        public String UserId { get; set; }
        public String CompanyId { get; set; }
        public String CompanyName { get; set; }
        public String CompanyLogoUrl { get; set; }
        public String CompanyAdminImageUrl { get; set; }
        public String CompnayWebsiteHeader { get; set; }
        public String PrimaryManagerId { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Email { get; set; }
        public String ProfileImageUrl { get; set; }
        public User.AccountType AccountType { get; set; }
        public List<CassandraService.Entity.Module> AccessModules { get; set; }
        public List<CassandraService.Entity.Module> AccessSystemModules { get; set; }
        public double TimeZone { get; set; }
        public List<Company> Companies { get; set; }

        public bool IsLogined { get; set; }
    }
}