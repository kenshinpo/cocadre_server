﻿using System;
using System.Collections.Generic;

namespace AdminWebsite.App_Code.Entity
{
    [Serializable]
    public class ImportedUser
    {
        public int No { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public int Gender { get; set; }
        public DateTime Birthday { get; set; }
        public string AddressCountryName { get; set; }
        public string Address { get; set; }
        public string AddressPostalCode { get; set; }
        public string PhoneCountryCode { get; set; }
        public string PhoneCountryName { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public List<string> DataErrors { get; set; }

        public string JobLevel { get; set; }
    }
}