﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWebsite.App_Code.Entity
{
    [Serializable]
    public class SurveyCard
    {
        public class ContentImage
        {
            public string base64 { get; set; }
            public string extension { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class Option
        {
            public string contentText { get; set; }
            public ContentImage contentImg { get; set; }
            public bool hasLogic { get; set; }
            public string logicCardId { get; set; }
        }


        public int type { get; set; }
        public String contentText { get; set; }
        public List<ContentImage> contentImgs { get; set; }
        public List<Option> options { get; set; }
        public int optionRangeStartFrom { get; set; }
        public int optionRangeMiddle { get; set; }
        public int optionRangeMin { get; set; }
        public int optionRangeMax { get; set; }
        public string optionRangeMiddleLabel  { get; set; }
        public string optionRangeMaxLabel { get; set; }
        public string optionRangeMinLabel { get; set; }
        public bool isAllowSkipped { get; set; }
        public bool isTickOtherAnswer { get; set; }
        public string customCommand { get; set; }
        public bool isRandomOption { get; set; }
        public int minOptions { get; set; }
        public int maxOptions { get; set; }
        public int diplayBg { get; set; }
        public string note { get; set; }
        public bool isPageBreak { get; set; }

    }
}