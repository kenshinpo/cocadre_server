﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWebsite.App_Code.ServiceRequests
{
    public class BasicRequest
    {
        public string CompanyId { get; set; }
        public string ManagerId { get; set; }
    }
}