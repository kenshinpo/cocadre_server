﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdminWebsite.App_Code.ServiceRequests
{
    public class MatchUpGetListRequest : BasicRequest
    {
        public string ContainsName { get; set; }
    }

    public class MatchUpImportQuestionsRequest : BasicRequest
    {
        public string TopicId { get; set; }
        public string CategoryId { get; set; }
        public string ZipFileBase64String { get; set; }
    }
}