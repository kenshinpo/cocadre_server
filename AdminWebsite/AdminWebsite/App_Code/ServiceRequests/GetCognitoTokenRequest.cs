﻿using System;

namespace AdminWebsite.App_Code.ServiceRequests
{
    [Serializable]
    public class GetCognitoTokenRequest
    {
        public String CompanyId { get; set; }
        public String UserId { get; set; }
    }
}