﻿using System;

namespace AdminWebsite.App_Code.ServiceResponses
{
    [Serializable]
    public class GetCognitoTokenResponse
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public String ErrorMessage { get; set; }

        public String Token { get; set; }

        public String RoleArn { get; set; }
    }
}