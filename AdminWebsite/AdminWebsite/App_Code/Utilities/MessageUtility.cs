﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace AdminWebsite.App_Code.Utilities
{
    public class MessageUtility
    {
        public const int TOAST_TYPE_INFO = 1;
        public const int TOAST_TYPE_ERROR = 2;
        public static void ShowToast(Page page, String message, int toastType)
        {
            switch (toastType)
            {       
                case TOAST_TYPE_INFO:
                    ScriptManager.RegisterStartupScript(page, typeof(Page), "toast", @"ReloadNormalToast();toastr.info('" + message.Replace(@"'", @"\'") + "');", true);
                    break;
                case TOAST_TYPE_ERROR:
                    ScriptManager.RegisterStartupScript(page, typeof(Page), "toast", @"ReloadErrorToast();toastr.error('" + message.Replace(@"'", @"\'") + "');", true);
                    break;
                default:
                    break;
            }
        }
    }
}