﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Web.Configuration;
using AdminWebsite.App_Code.ServiceRequests;
using AdminWebsite.App_Code.ServiceResponses;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Newtonsoft.Json;

namespace AdminWebsite.App_Code.Utilities.AWS
{
    public class S3Utility
    {
        public static IAmazonS3 GetIAmazonS3(String companyId, String managerUserId)
        {
            try
            {
                #region Step 1 Get AWS Cognito token and RoleArn from WebAPI server.
                GetCognitoTokenRequest request = new GetCognitoTokenRequest();
                request.CompanyId = companyId;
                request.UserId = managerUserId;
                byte[] postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(request));
                HttpWebRequest httpRequest = HttpWebRequest.Create(WebConfigurationManager.AppSettings["WebAPI_Url_GetCognitoToken"].ToString()) as HttpWebRequest;
                httpRequest.Method = "POST";
                httpRequest.Timeout = 30000;
                httpRequest.ContentType = "application/json";
                httpRequest.ContentLength = postData.Length;
                using (Stream st = httpRequest.GetRequestStream())
                {
                    st.Write(postData, 0, postData.Length);
                }
                string result = "";
                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                GetCognitoTokenResponse response = JsonConvert.DeserializeObject<GetCognitoTokenResponse>(result);
                #endregion

                #region Step 2. Create IAmazonS3 by Cognito token and RoleArn.
                AnonymousAWSCredentials cred = new AnonymousAWSCredentials();
                AmazonSecurityTokenServiceClient stsClient = new AmazonSecurityTokenServiceClient(cred, RegionEndpoint.APSoutheast1);
                AssumeRoleWithWebIdentityRequest stsReq = new AssumeRoleWithWebIdentityRequest();
                stsReq.RoleArn = response.RoleArn;
                stsReq.RoleSessionName = "AdminWebsiteSession";
                stsReq.WebIdentityToken = response.Token;
                AssumeRoleWithWebIdentityResponse stsResp = stsClient.AssumeRoleWithWebIdentity(stsReq);
                IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(stsResp.Credentials, RegionEndpoint.APSoutheast1);
                #endregion

                return s3Client;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                return null;
            }
        }

        public static void CheckFolderOnS3(IAmazonS3 s3Client, String bucketName, String folderName)
        {
            try
            {
                #region Step 1. Checking if the bucket exit.
                ListBucketsResponse findBucketresponse = s3Client.ListBuckets();
                bool isBucketExisting = false;
                foreach (S3Bucket b in findBucketresponse.Buckets)
                {
                    if (b.BucketName.Equals(bucketName))
                    {
                        isBucketExisting = true;
                        break;
                    }
                }
                #endregion

                #region Step 2. If bucket is not existing, create a bucket.
                if (!isBucketExisting)
                {
                    PutBucketRequest request = new PutBucketRequest();
                    request.BucketName = bucketName;
                    s3Client.PutBucket(request);
                }
                #endregion

                #region Step 3. Checking if the folder exists.
                ListObjectsRequest findFolderRequest = new ListObjectsRequest();
                findFolderRequest.BucketName = bucketName;
                findFolderRequest.Delimiter = "/";
                findFolderRequest.Prefix = folderName;
                ListObjectsResponse findFolderResponse = s3Client.ListObjects(findFolderRequest);
                List<String> commonPrefixes = findFolderResponse.CommonPrefixes;
                Boolean folderExists = commonPrefixes.Any();
                #endregion

                #region Step 4. 如果沒有資料夾，則建立資料夾。
                if (!folderExists)
                {
                    PutObjectRequest folderRequest = new PutObjectRequest();
                    folderRequest.BucketName = bucketName;
                    String folderKey = folderName + "/";
                    folderRequest.Key = folderKey;
                    folderRequest.InputStream = new MemoryStream(new byte[0]);
                    PutObjectResponse folderResponse = s3Client.PutObject(folderRequest);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }


        public static void UploadImageToS3(IAmazonS3 s3Client, Byte[] fileBytes, String bucketName, String folderName, String fileName, Dictionary<String, String> metadata)
        {
            try
            {
                using (var ms = new MemoryStream(fileBytes))
                {
                    TransferUtility fileTransferUtility = new TransferUtility(s3Client);

                    TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = bucketName + "/" + folderName,
                        InputStream = ms,
                        StorageClass = S3StorageClass.Standard,
                        PartSize = ms.Length,
                        Key = fileName,
                        CannedACL = S3CannedACL.PublicRead,
                    };

                    if (metadata.ContainsKey("Width"))
                    {
                        fileTransferUtilityRequest.Metadata.Add("Width", metadata["Width"]);
                    }
                    if (metadata.ContainsKey("Height"))
                    {
                        fileTransferUtilityRequest.Metadata.Add("Height", metadata["Height"]);
                    }
                    fileTransferUtility.Upload(fileTransferUtilityRequest);
                    GetBucketLocationResponse respnese = s3Client.GetBucketLocation(bucketName + "/" + folderName);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}