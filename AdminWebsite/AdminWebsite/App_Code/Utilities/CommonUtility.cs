﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AdminWebsite.App_Code
{
    public class CommonUtility
    {
        public static RegionInfo GetCountry()
        {
            CultureInfo culture = GetCulture();
            if (culture != null)
                return new RegionInfo(culture.LCID);

            return null;
        }

        public static CultureInfo GetCulture()
        {
            string[] languages = HttpContext.Current.Request.UserLanguages;

            if (languages == null || languages.Length == 0)
                return null;

            try
            {
                string language = languages[0].ToLowerInvariant().Trim();
                return CultureInfo.CreateSpecificCulture(language);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }
    }
}