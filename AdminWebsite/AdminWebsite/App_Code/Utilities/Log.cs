﻿using System;
using System.Web.UI;
using log4net;

namespace AdminWebsite.App_Code.Utilities
{
    public class Log
    {
        private static ILog LOG = LogManager.GetLogger("AdminWebsiteLog");

        public static void Debug(String message)
        {
            LOG.Debug(message);
        }

        public static void Info(String message)
        {
            LOG.Info(message);
        }

        public static void Warn(String message)
        {
            LOG.Warn(message);
        }

        public static void Error(String message, Page page = null)
        {
            LOG.Error(message);
            if (page != null)
            {
                page.Response.Redirect("/ErrorPage", false);
            }
        }

        public static void Error(String message, Exception exception, Page page = null)
        {
            LOG.Error(message, exception);
            if (page != null)
            {
                page.Response.Redirect("/ErrorPage", false);
            }
        }
    }
}