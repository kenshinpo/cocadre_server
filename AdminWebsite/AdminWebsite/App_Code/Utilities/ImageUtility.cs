﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace AdminWebsite.App_Code.Utilities
{
    public class ImageUtility
    {
        public static ImageFormat GetImageFormat(Image img)
        {
            try
            {
                if (img.RawFormat.Equals(ImageFormat.Jpeg))
                    return ImageFormat.Jpeg;
                if (img.RawFormat.Equals(ImageFormat.Bmp))
                    return ImageFormat.Bmp;
                if (img.RawFormat.Equals(ImageFormat.Png))
                    return ImageFormat.Png;
                if (img.RawFormat.Equals(ImageFormat.Emf))
                    return ImageFormat.Emf;
                if (img.RawFormat.Equals(ImageFormat.Exif))
                    return ImageFormat.Exif;
                if (img.RawFormat.Equals(ImageFormat.Gif))
                    return ImageFormat.Gif;
                if (img.RawFormat.Equals(ImageFormat.Icon))
                    return ImageFormat.Icon;
                if (img.RawFormat.Equals(ImageFormat.MemoryBmp))
                    return ImageFormat.MemoryBmp;
                if (img.RawFormat.Equals(ImageFormat.Tiff))
                    return ImageFormat.Tiff;
                else
                    return ImageFormat.Wmf;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }

        public static Image ResizeImage(Image imgToResize, Size size, ImageFormat enuType)
        {
            try
            {
                int sourceWidth = imgToResize.Width;
                int sourceHeight = imgToResize.Height;

                float nPercent = 0;
                float nPercentW = 0;
                float nPercentH = 0;

                nPercentW = ((float)size.Width / (float)sourceWidth);
                nPercentH = ((float)size.Height / (float)sourceHeight);

                if (nPercentH < nPercentW)
                    nPercent = nPercentH;
                else
                    nPercent = nPercentW;

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);

                Bitmap b;

                if (enuType == ImageFormat.Png)
                    b = new Bitmap(destWidth, destHeight, PixelFormat.Format32bppArgb);
                else if (enuType == ImageFormat.Gif)
                    b = new Bitmap(destWidth, destHeight);
                else
                    b = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

                Graphics g = Graphics.FromImage((Image)b);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
                g.Dispose();
                return (Image)b;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }

        public static byte[] ImageToByteArray(Image image, ImageFormat imageFormat)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                image.Save(ms, imageFormat);
                byte[] imageBytes = ms.ToArray();
                return imageBytes;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }

        public static String FilterBase64String(String text)
        {
            try
            {
                String base64String = text.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                string dummyData = base64String.Trim().Replace("%", "").Replace(",", "").Replace(" ", "+");
                if (dummyData.Length % 4 > 0)
                {
                    dummyData = dummyData.PadRight(dummyData.Length + 4 - dummyData.Length % 4, '=');
                }
                base64String = dummyData;
                return base64String;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                throw;
            }
        }
    }
}