﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="AdminWebsite.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="no-js" lang="">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <title>Sign In - Cocadre</title>
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="scss/app.css" />
    <style>
        html,
        body {
            background: white;
        }

        .companies a {
            height: 130px !important;
        }

        .index-company-name {
            width: 100% !important;
            height: auto !important;
            padding: 3px !important;
            display: block;
            color: #000;
            font-size: 16px;
            text-align: center;
            line-height: 18px;
            position: relative !important;
            margin-bottom: 30px;
        }
    </style>
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-ui-1.12.1.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="login">

            <asp:MultiView ID="login_pnl" runat="server" ActiveViewIndex="0">
                <asp:View ID="login_view" runat="server">
                    <div class="login__header">
                        <a href="http://www.cocadre.com">
                            <div class="login__header__logo">Cocadre</div>
                        </a>
                        <div class="login__header__title">Sign in to your Admin Console</div>
                    </div>
                    <div class="login__card card">
                        <img src="img/image-head.png" alt="" class="login__card__image">
                        <asp:TextBox ID="username" Text="" runat="server" placeholder="Email"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqUsername" CssClass="error-msg" ErrorMessage="Please enter your username." runat="server" ControlToValidate="username" Display="Dynamic" role="alert"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="pwd" runat="server" placeholder="Password" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqPassword" CssClass="error-msg" ErrorMessage="Please enter your password." runat="server" ControlToValidate="pwd" Display="Dynamic" role="alert"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cv" runat="server" CssClass="error-msg"></asp:CustomValidator>
                        <asp:Button ID="btnSignIn" runat="server" Text="Sign In" CssClass="login__card__submit btn brandcolor" OnClick="btnSignIn_Click" />
                    </div>

                    <div class="login__footer">
                        <a href="/ForgotPassword" class="login__card__help" style="text-align: center;">Forgot your password?</a>
                    </div>
                </asp:View>
                <asp:View ID="viewSystemManager" runat="server">
                    <div class="login__header">
                        <div class="login__header__logo">Cocadre</div>
                    </div>

                    <div class="tabs tabs--styled">
                        <ul class="tabs__list">

                            <li class="tabs__list__item">
                                <a class="tabs__link tab-managed" href="#tab-managed">Managed</a>
                            </li>
                            <li class="tabs__list__item">
                                <a class="tabs__link tab-master-console" href="#tab-master-console">Master Console</a>
                            </li>
                        </ul>
                        <div class="tabs__panels">
                            <!-- PULSE DECK INFO (CONTENT) START -->

                            <div class="tabs__panels__item add-topic__info__details--basicinfo" id="tab-managed">

                                <asp:ListView ID="lvCompaniesForSystem" runat="server" OnItemDataBound="lvCompanies_ItemDataBound" OnItemCommand="lvCompanies_ItemCommand">
                                    <LayoutTemplate>
                                        <div class="companies">
                                            <ul>
                                                <li id="ItemPlaceholder" runat="server"></li>
                                            </ul>
                                        </div>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <asp:LinkButton ID="lbCompany" runat="server" CssClass="cool-black" CommandName="Click">
                                                <asp:Image ID="imgCompany" runat="server" Width="540px" Height="110px" />
                                                <asp:Label ID="lblCompanyName" runat="server" CssClass="index-company-name" />
                                            </asp:LinkButton></li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </div>
                            <div class="tabs__panels__item add-topic__info__details--basicinfo" id="tab-master-console">
                                <div class="adminconsole">
                                    <div class="adminconsole__main">
                                        <asp:Repeater ID="rtModule" runat="server" OnItemDataBound="rtModule_ItemDataBound">
                                            <HeaderTemplate>
                                                <div class="adminconsole__main__container">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlModule" CssClass="sectionlink" runat="server" Target="_blank">
                                                    <span class="sectionlink__image">
                                                        <asp:Image ID="imgModule" runat="server" />
                                                    </span>
                                                    <asp:Label ID="lblTitle" runat="server" CssClass="sectionlink__title" />
                                                    <asp:Label ID="lbldescription" runat="server" CssClass="sectionlink__description" />
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>

                <asp:View ID="viewCompanyManager" runat="server">
                    <div class="login__header">
                        <div class="login__header__logo">Cocadre</div>
                    </div>
                    <asp:ListView ID="lvCompanies" runat="server" OnItemDataBound="lvCompanies_ItemDataBound" OnItemCommand="lvCompanies_ItemCommand">
                        <LayoutTemplate>
                            <div class="companies">
                                <ul>
                                    <li id="ItemPlaceholder" runat="server"></li>
                                </ul>
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:LinkButton ID="lbCompany" runat="server" CssClass="cool-black" CommandName="Click">
                                    <asp:Image ID="imgCompany" runat="server" Width="540px" Height="110px" />
                                    <asp:Label ID="lblCompanyName" runat="server" CssClass="index-company-name" />
                                </asp:LinkButton></li>
                        </ItemTemplate>
                    </asp:ListView>
                </asp:View>
            </asp:MultiView>
        </div>
    </form>
    <script src="js/app-min.js"></script>
</body>
</html>
