﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SingleTopicAnalytic.aspx.cs" Inherits="AdminWebsite.Analytics.SingleTopicAnalytic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .ca-chartdraw {
            width:100%;
            min-height:300px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta"><a href="/Analytics/MatchUpImprovement" id="root-link">MatchUp Improvement</a></div>
        <div class="appbar__meta" id="topic-breadcrumb"></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="matchup-wrapper" class="data" >
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Analytics/MatchUpImprovement">MatchUp Improvement</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="feed_posts">Feed Posts</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="storage">Storage Utilization</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="relationship">Relationship</a>
                </li>--%>
            </ul>
        </aside>

        <div class="data__content">

            <div id="matchup_improvement" class="container" style="width:100%;">
                <div style="width:100%; margin:0 auto;">

                <!-- TITLE -->
                <div class="title-section" style="width:100%; margin: 0 auto;">
                    <div class="matchup-info">
                        <h1></h1>
                        <p class="align-left">Showing data from <span id="survey-start-date"></span> - <span id="survey-end-date"></span></p>
                    </div>
                </div>

                <!-- ACCUMULATED QUESTION ATTEMPTS -->
                <div class="card card-6-col animated fadeInUp" style="position:relative; min-height:330px;">
                    <div class="card-header">
                        <h1 class="title">Accumulated Question Attempts</h1>
                        <div class="attempt-legends" style="position: absolute;top: 10px; right: 25px;">
                            <ul class="inline-ul">
                                <li><div class="indicator color1"></div> Correct</li>
                                <li><div class="indicator color3"></div> Wrong</li>
                            </ul>
                        </div>
                    </div>
                    <div class="chart-section" style="width:100%; height:auto;">
                        <ul class="list-group big-progress-group" >
                            <li class="list-group-item"> 
                                <span id="completed-percentage" class="list-percentage">0</span>
                                <div class="progress">
                                    <div id="completed-bar" class="progress-bar color1" style="width:0%;"></div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <span id="incomplete-percentage" class="list-percentage " >0</span>
                                <div class="progress">
                                    <div id="incompleted-bar" class="progress-bar color3" style="width:0%;"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <!-- TOPIC SUMMARY -->
                <!--
                    #created-on
                    #question-inbank
                    #total-matchup
                    #total-topic-attempt
                    #user-engaged-no
                    #average-topic-engaged
                    #popularity-rank
                -->
                <div class="card card-4-col animated fadeInUp">
                    <div class="card-header">
                        <h1 class="title">Topic Summary</h1>
                    </div>
                    <div class="small-card-content">
                        <ul class="list-group">
                            <li class="list-group-item"> 
                                <span id="created-on" class="list-text-value">0</span>
                                Created on:
                            </li>
                            <li class="list-group-item">
                                <span id="question-inbank" class="list-text-value">0</span>
                                Questions in bank:
                            </li>
                            <li class="list-group-item">
                                <span id="total-matchup" class="list-text-value">0</span>
                                Total MatchUps:
                            </li>
                            <li class="list-group-item">
                                <span id="user-engaged-no" class="list-text-value">-</span>
                                No of users engaged:
                            </li>
                            <li class="list-group-item">
                                <span id="average-topic-engaged" class="list-text-value">0</span>
                                Average match per user:
                            </li>
                            <%--<li class="list-group-item">
                                <span id="popularity-rank" class="list-text-value">50 out of 56 topics</span>
                                Popularity rank:
                            </li>--%>
                        </ul>
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- TOPIC ANALYTICS -->
                <div class="card full-row animated fadeInUp" style="position:relative;">
                    <div class="card-header">
                        <h1 class="title">Topic Analytics</h1>
                    </div>
                    <div class="chart-section" style="width:100%; height:auto;">
                        <%--<div style="background:#ccc; width:90%; height:300px;"></div>--%>
                        <div id="chartContainer" style="width:70%; height:350px; padding-left:50px; position:relative;">
                            
                        </div>
                    </div>
                    <div class="chart-footer">
                        <label class="info-label">From <span id="start_date" class="start_date"></span> to <span id="end_date" class="end_date"></span></label>
                        <div class="chart-info">
                            <%--<div class="small-info">
                                <label><span id="first_attempt_value">0</span> (<span id="first_attempt_percentage">0</span>%)</label>
                            </div>--%>
                            <div class="chart-border"></div>
                            <label id="first_attempt" class="response_number">0</label>
                            <small>questions</small>
                            <p>First Attempt Correct</p>
                        </div>
                        <div class="chart-info">
                            <%--<div class="small-info">
                                <label><span id="total_failed_value">0</span> (<span id="total_failed_percentage">0</span>%)</label>
                            </div>--%>
                            <label id="total_failed" class="response_number">0</label>
                            <small>questions</small>
                            <p>Total failed attempts</p>
                        </div>
                        <div class="chart-total-responder">
                            <div class="border-box">
                                <small>Out of</small>
                                <label id="total_attempts" class="response_number">0</label>
                                <small class="highlight">questions</small>
                                <p>Total Attempts</p>
                            </div>
                        </div>
                    </div>
                    <div class="chart-footer">
                        <div class="chart-info">
                            <%--<div class="small-info">
                                <label><span id="2nd_attempt_value">0</span> (<span id="2nd_attempt_percentage">0</span>%)</label>
                            </div>--%>
                            <div class="chart-border"></div>
                            <label id="2nd_attempt" class="response_number">0</label>
                            <small>questions</small>
                            <p class="color4">2nd Attempt Correct</p>
                        </div>
                        <div class="chart-info">
                            <%--<div class="small-info">
                                <label><span id="3rd_attempt_value">0</span> (<span id="3rd_attempt_percentage">0</span>%)</label>
                            </div>--%>
                            <div class="chart-border"></div>
                            <label id="3rd_attempt" class="response_number">0</label>
                            <small>questions</small>
                            <p style="color:#4b93e3;">3rd & above<br />Attempt Correct</p>
                        </div>
                        <div class="chart-info">
                            <%--<div class="small-info">
                                <label><span id="2nd_attempt_wrong_value">0</span> (<span id="2nd_attempt_wrong_percentage">0</span>%)</label>
                            </div>--%>
                            <div class="chart-border"></div>
                            <label id="2nd_attempt_wrong" class="response_number">0</label>
                            <small>questions</small>
                            <p style="color:#ffae00;">2nd Attempt<br />Wrong</p>
                        </div>
                        <div class="chart-info">
                           <%-- <div class="small-info">
                                <label><span id="3rd_attempt_wrong_value">0</span> (<span id="3rd_attempt_wrong_percentage">0</span>%)</label>
                            </div>--%>
                            <label id="3rd_attempt_wrong" class="response_number">0</label>
                            <small>questions</small>
                            <p class="color3">3rd & above<br />Attempt Wrong</p>
                        </div>
                        
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- QUESTION TABLE -->
                <div class="card full-row animated fadeInUp no-padding">
                    <div class="card-header" style="padding: 20px;">
                        <div class="filter-section">                  
                            <div class="mdl-selectfield" style="width:20%;float:left;margin-right: 20px; height:40px;">
                                <select name="question-filter" id="question-filter">
	                                <%--<option selected="selected" value="">Question published date</option>--%>
	                                <option value="1" selected="selected">Hardest Questions</option>
                                    <option value="2">Easiest Questions</option>
                                </select>
                            </div> 
                            <div class="table-legends">
                                <div class="status-legends text-right" >
                                    <label>Status</label>
                                    <ul class="inline-ul">
                                        <li><div class="indicator active-indicator"></div> Active</li>
                                        <li><div class="indicator hidden-indicator"></div> Hidden</li>
                                    </ul>
                                </div>
                                <div class="attempt-legends" >
                                    <label>Attempts</label>
                                    <ul class="inline-ul">
                                        <li><div class="indicator color1"></div> Correct</li>
                                        <li><div class="indicator color3"></div> Wrong</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="table-content">
                        <table id="question-table" class="responder-table">
                            <thead>
                                <th data-dynatable-column="date" style="width:13%">Since</th>
                                <th data-dynatable-column="topic" style="width:42%">Question</th>
                                <th data-dynatable-column="topic_search" class="hidden"></th>
                                <th data-dynatable-column="status" data-dynatable-sorts="status" style="width:8%">Status</th>
                                <th data-dynatable-column="question_attempt" data-dynatable-sorts="question_attempt" style="width:25%">Attempts</th>
                                <th data-dynatable-column="analytic" data-dynatable-sorts="analytic" style="width:12%">Analytic</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="text-align:center; padding: 20px; ">
                            <a id="all-question-link" href="#" target="_blank" style="color:#000;">All Questions</a>
                        </div>
                    </div>
                </div>

                <!-- MOST FREQUENT DEPARTMENT TABLE -->
                <%--<div class="card full-row animated fadeInUp no-padding">
                    <div class="card-header" style="padding: 20px;">
                        <label class="table-title">Most Frequent Department</label>
                    </div>

                    <div class="clearfix"></div>

                    <div class="table-content">
                        <table id="department-table" class="responder-table">
                            <thead>
                                <th data-dynatable-column="rank" style="width:10%">Rank</th>
                                <th data-dynatable-column="department" style="width:40%">Departments</th>
                                <th data-dynatable-column="topic_engagement" data-dynatable-sorts="topic_engagement" style="width:25%">Topic Engagement</th>
                                <th data-dynatable-column="accumulative_question_attempt" data-dynatable-sorts="accumulative_question_attempt" style="width:25%">Accumulative Question Attempts</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="text-align:center; padding: 20px; ">
                            <a href="/Analytics/AllDepartments.aspx" target="_blank" style="color:#000;">All Departments</a>
                        </div>
                    </div>
                </div>--%>


                <!-- ALL PERSONNEL TABLE -->
               <%-- <div class="card full-row animated fadeInUp no-padding">
                    <div class="card-header" style="padding: 20px;">
                        <label class="table-title">Most Active Users on All Topics</label>
                    </div>

                    <div class="clearfix"></div>

                    <div class="table-content">
                        <table id="personnel-table" class="responder-table">
                            <thead>
                                <th data-dynatable-column="rank" style="width:10%">Rank</th>
                                <th data-dynatable-column="personnel" style="width:25%">Personnels</th>
                                <th data-dynatable-column="department" style="width:15%">Department</th>
                                <th data-dynatable-column="topic_engagement" data-dynatable-sorts="topic_engagement" style="width:25%">Topic Engagement</th>
                                <th data-dynatable-column="accumulative_question_attempt" data-dynatable-sorts="accumulative_question_attempt" style="width:25%">Accumulative Question Attempts</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="text-align:center; padding: 20px; ">
                            <a href="/Analytics/AllPersonnel.aspx" target="_blank" style="color:#000;">All Personnel</a>
                        </div>
                    </div>
                </div>--%>


            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="/js/vendor/moment-with-locales.js"></script>
    <script src="/js/vendor/canvasjs.js"></script>
    <script src="/Js/Chart.js"></script>
    <script>
        $(function () {
            var attempt_data = [];

            var question_table = $('#question-table');
            var question_table_data = [];

            $('#all-question-link').attr('href', '/Analytics/AllQuestions/' + TopicId);

            function generateQuestionTable() {
                question_table.bind('dynatable:afterUpdate', function (e, dynatable) {
                    setTimeout(function () {
                        question_table.css('display', 'table');
                        question_table.addClass('animated fadeIn');
                    }, 500);
                });

                question_table.bind('dynatable:init', function (e, dynatable) {

                    dynatable.queries.functions['topic_search'] = function (record, queryValue) {
                        return record.topic_search.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                    };

                });

                question_table.dynatable({
                    features: {
                        paginate: false,
                        search: false,
                        sorting: true,
                        recordCount: false,
                        pushState: false,
                    },
                    dataset: {
                        records: question_table_data,
                        sorts: { 'no': 1 }
                    },
                    inputs: {
                        queries: $(' #topic_search')
                    }
                });

                var dynatable = question_table.data('dynatable');

                if (typeof dynatable.records !== "undefined") {
                    dynatable.records.updateFromJson({ records: question_table_data });
                    dynatable.records.init();
                }
                dynatable.paginationPerPage.set(15);
                dynatable.process();

                question_table.data('dynatable').settings.dataset.records = question_table_data;
                question_table.data('dynatable').dom.update();

            }

            ///////////////////////

            function FetchData() {
                $('.progress-bar').css('width', '0%');
                $('#matchup_improvement').addClass('hidden');
                $('#matchup_improvement').removeClass('animated fadeInUp');
                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: '/Api/Analytics/SelectTopicOverall',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "TopicId": TopicId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {
                            $('#matchup_improvement').removeClass('hidden');
                            $('#matchup_improvement').addClass('animated fadeInUp');
                            var summary = res.TopicSummary;
                            var attempt = res.QuestionAttempt;
                            var analytic = res.Analytics;
                            var question = res.Questions;

                            var dateNow = new Date();
                            var dateThen = new Date(summary.Topic.CreatedOnTimestampString);

                            $('#survey-start-date').html(summary.Topic.CreatedOnTimestampString);
                            $('#survey-end-date').html(moment(dateNow).format('DD/MM/YYYY hh:mm A'));
                            $('#topic-breadcrumb').html(summary.Topic.TopicTitle);

                            $('.matchup-info h1').html('<img id="survey-icon" src="' + summary.Topic.TopicLogoUrl + '" /> ' + summary.Topic.TopicTitle);

                            $('#created-on').html(summary.Topic.CreatedOnTimestampString);
                            $('#question-inbank').html(summary.Topic.TotalNumberOfQuestions);
                            $('#total-matchup').html(summary.TotalMatchUp);
                            $('#user-engaged-no').html(summary.TotalUserEngaged + "/" + summary.TotalTargetedAudience + " Unique Users");
                            $('#average-topic-engaged').html(summary.AverageMatchUpEngaged.toFixed(2));
                              
                            $(' #completed-percentage').html(attempt.Correct);
                            $(' #incomplete-percentage').html(attempt.Incorrect);
                            $('#completed-bar').css('width', (attempt.Correct/ (attempt.Correct+attempt.Incorrect))* 100 +"%" );
                            $(' #incompleted-bar').css('width', (attempt.Incorrect / (attempt.Correct + attempt.Incorrect)) * 100 + "%");
                                 
                            $('#first_attempt').html(analytic.FirstAttemptCorrect);
                            $('#total_attempts').html(analytic.SecondAttemptCorrect + analytic.FirstAttemptCorrect + analytic.ThirdAboveAttemptCorrect + analytic.TotalFailedAttempt);
                            $('#total_failed').html(analytic.TotalFailedAttempt);
                            $('#2nd_attempt').html(analytic.SecondAttemptCorrect);
                            $('#3rd_attempt').html(analytic.ThirdAboveAttemptCorrect);
                            $('#2nd_attempt_wrong').html(analytic.SecondAttemptIncorrect);
                            $('#3rd_attempt_wrong').html(analytic.ThirdAboveAttemptIncorrect);

                            attempt_data.push({"type": "stackedColumn", "dataPoints":[]}); //correct 
                            attempt_data.push({"type": "stackedColumn", "dataPoints":[]}); //incorrect

                            $.each(analytic.Attempts, function (key, value) {
                                attempt_data[0].dataPoints.push({ "y": value.Correct, "label": value.Attempt, "attempts": value.Correct, "hours": "Correct", "attempt_no":key+1 });
                                attempt_data[1].dataPoints.push({ "y": -value.Incorrect, "label": value.Attempt, "attempts": -value.Incorrect, "hours": "Wrong", "attempt_no": key + 1 });
                            });

                            var chart = new CanvasJS.Chart("chartContainer",
                            {
                                axisY: {
                                    maximum: analytic.MaxCorrect,
                                    minimum: -analytic.MaxIncorrect,
                                    //interval: 25,
                                    //valueFormatString: "000#",
                                    //suffix: " hrs",
                                    gridThickness: 1,
                                    lineThickness: 1,
                                    tickThickness: 1

                                },
                                dataPointWidth: 50,
                                animationEnabled: true,
                                axisX: {
                                    lineThickness: 1,
                                    labelAutoFit: true,
                                    labelMaxWidth: 100
                                },
                                data: attempt_data
                            });

                            chart.render();

                            $('#chartContainer').append('<label style="position:absolute; top:-10px;left:-10px;color:#000;">Correct Attempts</label><label style="position:absolute; bottom:10px; left:-5px; color:#000;"> Wrong Attempts</label>');

                        }
                    }
                });
            }


            ///////////////////////

            function ToggleDifficulty(diff) {
                //ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: '/Api/Analytics/SelectQuestionAttempts',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "TopicId": TopicId,
                        "Metric": diff,
                        "Limit": 5
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        //HideProgressBar();
                        if (res.Success) {
                            var question = res.Questions;

                            $.each(question, function (key, value) {
                                var status = ""; var topicString = "";

                                if (value.Question.Status == 1) {
                                    status = "active";
                                } else {
                                    status = "hidden";
                                }

                                if (value.Question.ContentImageUrl != undefined) {
                                    topicString = '<div class="question-image"><img src="' + value.Question.ContentImageUrl + '" /></div><p class="text">' + value.Question.Content + '</p>';
                                } else {
                                    topicString = '<div class="question-image"></div><p class="text">' + value.Question.Content + '</p>';
                                }

                                question_table_data.push({
                                    "date": value.Question.CreatedOnTimestampString,
                                    "topic": topicString,
                                    "topic_search": value.Question.Content,
                                    "status": '<div class="indicator ' + status + '-indicator"></div>',
                                    "question_attempt": '<ul class="list-group"><li class="list-group-item"><span class="list-percentage " >' + value.Correct + '</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + (value.Correct / (value.Incorrect + value.Correct))*100 + '%;"></div></div></li><li class="list-group-item"><span class="list-percentage" >' + value.Incorrect + '</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + (value.Incorrect / (value.Incorrect + value.Correct))*100 + '%"></div></div></li></ul>',
                                    "analytic": '<button class="btn btn-info btn-detail" data-questionid="' + value.Question.Id + '" data-topicid="' + TopicId + '">Details</button>'
                                });

                            });
                            generateQuestionTable();
                        }
                    }
                });
            }

            $(document).on('click', '.btn-detail', function (e) {
                e.preventDefault();

                //var params = $.param({
                //    "TopicId": $(this).data('topicid'),
                //    "QuestionId": $(this).data('questionid')
                //});

                //var url = window.location.href;
                //var hash = location.hash;
                //url = window.location.protocol + "//" + window.location.host + "/Analytics/QuestionDetail.aspx/";

                //if (url.indexOf("?") < 0)
                //    url += "?" + params;
                //else
                //    url += "&" + params;

                //window.open(url + hash, '_blank');

                url = window.location.protocol + "//" + window.location.host + "/Analytics/QuestionDetail/" + $(this).data('topicid') + "/" + $(this).data('questionid');
                window.open(url, '_blank');
            });

            $('select#question-filter').on('change', function (e) {
                question_table_data = [];
                ToggleDifficulty($(this).val());
            });

           

            //var department_table = $('#department-table');
            //var department_table_data = [
            //    {
            //        "rank": "1",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    },
            //    {
            //        "rank": "1",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    },
            //    {
            //        "rank": "1",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    },
            //    {
            //        "rank": "1",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    }
            //];

            //function generateDepartmentTable() {
            //    department_table.bind('dynatable:afterUpdate', function (e, dynatable) {
            //        setTimeout(function () {
            //            department_table.css('display', 'table');
            //            department_table.addClass('animated fadeIn');
            //        }, 500);

            //    });

            //    department_table.bind('dynatable:init', function (e, dynatable) {

            //        dynatable.queries.functions['department'] = function (record, queryValue) {
            //            return record.department.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
            //        };

            //    });


            //    department_table.dynatable({
            //        features: {
            //            paginate: false,
            //            search: false,
            //            sorting: false,
            //            recordCount: false
            //        },
            //        dataset: {
            //            records: department_table_data
            //        },
            //        inputs: {
            //            queries: $('#department')
            //        }
            //    });

            //}

            //var personnel_table = $('#personnel-table');
            //var personnel_table_data = [
            //    {
            //        "rank": "1",
            //        "personnel": "Leong Guo Hao",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    },
            //    {
            //        "rank": "1",
            //        "personnel": "Leong Guo Hao",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    },
            //    {
            //        "rank": "1",
            //        "personnel": "Leong Guo Hao",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    },
            //    {
            //        "rank": "1",
            //        "personnel": "Leong Guo Hao",
            //        "department": 'Marketing',
            //        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
            //        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
            //    }
            //];

            //function generatePersonnelTable() {
            //    personnel_table.bind('dynatable:afterUpdate', function (e, dynatable) {
            //        setTimeout(function () {
            //            personnel_table.css('display', 'table');
            //            personnel_table.addClass('animated fadeIn');
            //        }, 500);

            //    });

            //    personnel_table.bind('dynatable:init', function (e, dynatable) {

            //        dynatable.queries.functions['personnel'] = function (record, queryValue) {
            //            return record.personnel.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
            //        };

            //    });


            //    personnel_table.dynatable({
            //        features: {
            //            paginate: false,
            //            search: false,
            //            sorting: false,
            //            recordCount: false
            //        },
            //        dataset: {
            //            records: personnel_table_data
            //        },
            //        inputs: {
            //            queries: $('#personnel')
            //        }
            //    });

            //}


            //generatePersonnelTable();

            //generateDepartmentTable();

            //generateQuestionTable();
            FetchData();
            ToggleDifficulty(1);
        });
    </script>
    </div>
</asp:Content>
