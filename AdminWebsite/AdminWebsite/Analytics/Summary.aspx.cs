﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.Analytics
{
    public partial class Summary : Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    AnalyticsSelectBasicResponse response = asc.SelectBasicReport(adminInfo.UserId, adminInfo.CompanyId);
                    if (response.Success)
                    {
                        ltlActiveUserCount.Text = Convert.ToString(response.BasicDau.TotalUniqueActiveUsers);
                        ltlTotalUserCount.Text = Convert.ToString(response.BasicDau.TotalCurrentUsers);
                        ltlReportDate.Text = response.BasicDau.CurrentDate.ToString("MMM dd yyyy");
                        ltlDiffNumber.Text = response.BasicDau.DifferenceInNumber.ToString();
                        ltlDiffPercentage.Text = response.BasicDau.DifferenceInPercentage.ToString("F2");

                        HtmlGenericControl iLast60 = plConnected.FindControl("iLast60") as HtmlGenericControl;
                        HtmlGenericControl pLast60 = plConnected.FindControl("pLast60") as HtmlGenericControl;
                        if (response.BasicDau.DifferenceInNumber < 0)
                        {
                            iLast60.Attributes.Add("class", "fa fa-arrow-circle-o-down ca-red");
                            pLast60.Attributes.Add("class", "ca-red");
                        }
                        else
                        {
                            iLast60.Attributes.Add("class", "fa fa-arrow-circle-o-up ca-green");
                            pLast60.Attributes.Add("class", "ca-green");
                        }

                        // For MatchUp Improvement
                        int totalAttempt = response.BasicTopicAttempts.FirstAttemptCorrect + response.BasicTopicAttempts.SecondAttemptCorrect + response.BasicTopicAttempts.ThirdAboveAttemptCorrect + response.BasicTopicAttempts.TotalFailedAttempt;
                        String jsCommand = String.Format("drawDAUChart({0},{1});", response.BasicDau.TotalUniqueActiveUsers, response.BasicDau.TotalCurrentUsers);
                        jsCommand += String.Format("drawMatchUpImprovementChart({0},{1},{2},{3},{4});", response.BasicTopicAttempts.SecondAttemptCorrect, response.BasicTopicAttempts.ThirdAboveAttemptCorrect, response.BasicTopicAttempts.SecondAttemptIncorrect, response.BasicTopicAttempts.ThirdAboveAttemptIncorrect, totalAttempt);
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "DrawChart", jsCommand, true);
                    }
                    else
                    {
                        Log.Error("AnalyticsSelectBasicResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}