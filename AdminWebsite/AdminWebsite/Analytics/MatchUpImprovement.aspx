﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="MatchUpImprovement.aspx.cs" Inherits="AdminWebsite.Analytics.MatchUpImprovement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .ca-chartdraw {
            width:100%;
            min-height:300px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta"><a href="MatchUpImprovement" id="root-link">MatchUp Improvement</a></div>
        <div class="appbar__meta"><a href="" id="second-link" class="hidden"></a></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.curs
                or='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="matchup-wrapper" class="data" >
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="MatchUpImprovement">MatchUp Improvement</a>
                </li>
               <%-- <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="feed_posts">Feed Posts</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="storage">Storage Utilization</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="relationship">Relationship</a>
                </li>--%>
            </ul>
        </aside>

        <div class="data__content">

            <div id="matchup_improvement" class="container" style="width:100%;">
            <div style="width:100%; margin:0 auto;">

                <!-- TITLE -->
                <div class="title-section" style="width:100%; margin: 0 auto;">
                    <div class="matchup-info">
                        <h1><img id="survey-icon" src="/Img/icon-quiz.png" /> MatchUp Improvement</h1>
                        <p class="align-left">Showing data from <span id="survey-start-date"></span> - <span id="survey-end-date"></span></p>
                    </div>
                    <div class="header-side">
                        <%--<h2 style="text-align:right; padding-top:20px;">Since beginning</h2>--%>
                    </div>
                </div>

                <!-- MATCHUP HEAT MAP -->
                <div class="card card-7-col ">
                    <div class="card-header" style="position:relative;">
                        <h1 class="title">MatchUp Activity Heat Map</h1>
                        
                        <div class="color_toggle_wrapper" style="position:absolute; right:90px ;top:0px;">
                            <button id="toggleColor" class="btn" style="padding: 5px 10px;font-size: 11px;border: 1px solid #FF9600;background: none; color: #FF9600;font-weight: normal; border-radius: 0px;">Change Color</button>
                        </div>
                    </div>
					    
                    <div class="chart-section" style="width:100%; height:350px; position:relative;">
                         <div id="chartContainer" style="width:90%; height:350px;"></div>
                        <div class="chart-legends" >
                            <label>Attempts</label>
                            
                            <div class="chart-legend-container"></div>
                            <%--<div class="chart-legend-item"><div class="color-indicator" style="background:#ab0900;"></div><span>600+</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#df1900;"></div><span>500</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ff512d;"></div><span>400</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ff8e75;"></div><span>300</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ffc4ba;"></div><span>200</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ac00a5;"></div><span>100</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#df00c9;"></div><span>95</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ff22e4;"></div><span>90</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ff72ea;"></div><span>85</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ffb9f6;"></div><span>80</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#05409b;"></div><span>75</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#1d5fca;"></div><span>70</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#4b93e3;"></div><span>65</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#8ab9ec;"></div><span>60</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#c0d9f5;"></div><span>55</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#009b0e;"></div><span>50</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#00ca33;"></div><span>45</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#41e358;"></div><span>40</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#86ed94;"></div><span>35</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#bef5c6;"></div><span>30</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ab4500;"></div><span>20</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#de6800;"></div><span>15</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ff9b27;"></div><span>10</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ffc173;"></div><span>5</span></div>
                            <div class="chart-legend-item"><div class="color-indicator" style="background:#ffdeba;"></div><span>1 to 4</span></div>--%>
                        </div>
                    </div>
                </div>

                <!-- OVERVIEW -->
                <div class="card card-3-col ">
                    <div class="card-header">
                        <h1 class="title">Overview</h1>
                    </div>
                    <div class="small-card-content">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span id="total-topics" class="list-value">0</span>
                                Total Topics:
                            </li>
                            <li class="list-group-item">
                                <span id="total-uniquequestion" class="list-value">0</span>
                                Total Unique Questions:
                            </li>
                            <li class="list-group-item">
                                <span id="total-matchup" class="list-value">0</span>
                                Total MatchUps:
                            </li>
                            <li class="list-group-item">
                                <span id="total-topic-attempt" class="list-value">0</span>
                                Average Topic attempt:<br />
                                per Unique user 
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- MATCHUP ACTIVE USERS -->
               
                <div class="card full-row ">
                    <div class="card-header">
                        <h1 class="title">MatchUp Active Users</h1>
                    </div>
                    <div class="chart-section" style="width:100%; height:100%; position:relative;">
                        <div id="ca-maingraphlegend" class="ca-legend" style="margin-top:0px; margin-bottom:20px;">
                            <table style="font-size: 14px; color: #545454; margin-top:0px;">
                                <tbody>
                                    <tr>
                                        <td colspan="4" align="left" style="padding:20px;">Click on the bar to display Daily Topic Activities</td>
                                    </tr>
                                    <tr>
                                        <td class="legendColorBox">
                                            <div style="padding: 1px">
                                                <div style="width: 4px; height: 0; border: 5px solid #ff9600; overflow: hidden"></div>
                                            </div>
                                        </td>
                                        <td class="legendLabel">Active User</td>
                                        <td class="legendColorBox">
                                            <div style="width: 10px; height: 10px; border: 2px solid #ff2851; border-radius: 5px; overflow: hidden"></div>
                                        </td>
                                        <td class="legendLabel">Average time spent</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="ca-maingraph" class="ca-chartdraw" style="position:relative;"></div>
                    </div>
                    <div class="chart-footer">
                        <label class="info-label">As of <span id="start_date" class="start_date">-</span></label>
                        <div class="chart-info">
                            <div class="small-info">
                                <label><span id="daily_unique_value">0</span> (<span id="daily_unique_percentage">0</span>%)</label>
                            </div>
                            <div class="chart-border"></div>
                            <label id="daily_unique" class="response_number">0</label>
                            <small>unique users</small>
                            <p>Yesterday</p>
                        </div>
                        <div class="chart-info">
                            <div class="small-info">
                                <label><span id="weekly_unique_value">0</span> (<span id="weekly_unique_percentage">0</span>%)</label>
                            </div>
                            <div class="chart-border"></div>
                            <label id="weekly_unique" class="response_number">0</label>
                            <small>unique users</small>
                            <p>Last 7 days</p>
                        </div>
                        <div class="chart-info">
                            <div class="small-info">
                                <label><span id="monthly_unique_value">0</span> (<span id="monthly_unique_percentage">0</span>%)</label>
                            </div>
                            <div class="chart-border"></div>
                            <label id="monthly_unique" class="response_number">0</label>
                            <small>unique users</small>
                            <p>Last 30 days</p>
                        </div>
                        <div class="chart-info">
                            <%--<div class="small-info">
                                <label><span id="average_unique_value">0</span> (<span id="average_unique_percentage">0</span>%)</label>
                            </div>--%>
                            <label id="average_timespent" class="response_number">0</label>
                            <small>Average time spent</small>
                            <p>per user over last 30 days</p>
                        </div>
                    </div>
                </div>

                <div class="full-row" style="margin-top:30px;">
                    <h2>Since beginning</h2>
                </div>
                

                <!-- ALL TOPIC TABLE -->
                <div class="card full-row no-padding">
                    <div class="card-header" style="padding: 20px;">
                        <div class="filter-section">                  
                            <div class="mdl-selectfield" style="width:20%;float:left;margin-right: 20px; height:40px;">
                                <select name="difficulty-filter" id="difficulty-filter">
	                                <%--<option selected="selected" value="">Topics published date</option>--%>
	                                <option selected="selected" value="1">Hardest Topic</option>
                                    <option value="2">Easiest Topic</option>
                                </select>
                            </div> 
                            <div class="table-legends">
                                <div class="status-legends text-right">
                                    <label>Status</label>
                                    <ul class="inline-ul">
                                        <li><div class="indicator unlisted-indicator"></div> Unlisted</li>
                                        <li><div class="indicator active-indicator"></div> Active</li>
                                        <li><div class="indicator hidden-indicator"></div> Hidden</li>
                                    </ul>
                                </div>
                                <div class="attempt-legends" >
                                    <label>Attempts</label>
                                    <ul class="inline-ul">
                                        <li><div class="indicator color1"></div> Correct</li>
                                        <li><div class="indicator color3"></div> Wrong</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="table-content">
                        <table id="topic-table" class="responder-table">
                            <thead>
                                <th data-dynatable-column="date" style="width:10%">Since</th>
                                <th data-dynatable-column="topic" style="width:30%">Topic</th>
                                <th data-dynatable-column="question_no" data-dynatable-sorts="question_no" style="width:15%">No of Questions</th>
                                <th data-dynatable-column="status" data-dynatable-sorts="status" style="width:8%">Status</th>
                                <th data-dynatable-column="question_attempt" data-dynatable-sorts="question_attempt" style="width:25%">Question Attempts</th>
                                <th data-dynatable-column="analytic" data-dynatable-sorts="analytic" style="width:12%">Analytic</th>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="text-align:center; padding: 20px; ">
                            <a href="/Analytics/AllTopics.aspx" target="_blank" style="color:#000;">All Topics</a>
                        </div>
                        
                    </div>

                </div>

            </div>
        </div>
    
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="../js/vendor/flot/jquery.flot.js"></script>
    <script src="../js/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="../js/vendor/flot/jquery.flot.time.js"></script>
    <script src="../js/vendor/flot/jquery.flot.pie.min.js"></script>
    <script src="../js/vendor/flot/jquery.flot.tooltip.js"></script>
    <script src="../js/vendor/moment-with-locales.js"></script>
    <script src="../js/vendor/canvasjs.js"></script>
    <script src="/Js/Chart.js"></script>
    <script>
        $(function () {
            
            //var CompanyId = 'C8b28091502514318bdcf48bec7c69129';
            //var AdminUserId = 'Udb0e704ee7244d21aea7930c27709fee';

            var heatmap_data = [];
            var heatmap = [];

            var dau_users = [];
            var dau_timespent = [];

            var maingraphobj = '';
            var hrsgraphobj = '';
            var starting_month = '';
            var ending_month = '';

            var maxTimeTaken = "";
            var maxUsers = "";
            var arrInterval = [];
            var userArrInterval = [];

            var timeInterval ="";

            var userInterval = "";
            var colorThemeId = 1;

            var topic_table = $('#topic-table');
            var topic_table_data = [];
            var legendHtml = "";
            var heatmap_chart = "";

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime() + (8 * 3600 * 1000); //hours * seconds * miliseconds
            }

            function usersFormatter(val, axis) {
                console.log(axis);
                return val + " users";
            }

            function timeTickFormatter(val, axis) {
                var minutes = Math.floor(val / 60);
                var seconds = val - minutes * 60;
                var timestring = minutes + " mins ";
                //console.log(axis);
                //console.log(minutes+" mins "+seconds+" sec");
                //var hours = Math.floor();
                //var minutes = (val - hours) * 60;
                return minutes;
            }

            function drawDAUChart()
            {
                starting_month = moment(dau_users[0][0]).format("MMM");
                ending_month =  moment(dau_users[dau_users.length - 1][0]).format("MMM");

                var maingraph = [
                    {
                        label: "Active User",
                        data: dau_users,
                        color: "#ff9600",
                        bars: {
                            show: true,
                            align: "center",
                            barWidth: 60 * 60 * 10000,
                            fill: 1
                        }
                    },
                    {
                        label: "Average time spent",
                        data: dau_timespent,
                        yaxis: 2,
                        color: "#ff2851",
                        points: {
                            fillColor: "#fff",
                            color: "#ff2851",
                            lineWidth: 1,
                            show: true
                        },
                        lines: {
                            show: true,
                            fill: true,
                            lineWidth: 1,
                            color: "#ff2851",
                            fillColor: 'rgba(255,150,0,0.2)'
                        },
                        shadowSize: 0
                    }
                ];

                //console.log(dau_timespent);
                //console.log(arrInterval);

                var mainoptions = {
                    legend: {
                        show: false
                    },
                    xaxis: {
                        mode: "time",
                        timeformat: "%d",
                        tickSize: [1, "day"],
                        tickLength: 0
                    },
                    yaxes: [{
                        min: 0,
                        ticks: userArrInterval //dauUsersTick
                    }, {
                        alignTicksWithAxis: true,
                        position: "right",
                        min: 0,
                        max: maxTimeTaken,
                        ticks: arrInterval //dauAveTimeSpentTick
                    }],
                    grid: {
                        borderWidth: 0,
                        borderColor: "#ccc",
                        clickable: true,
                        hoverable: true
                    },
                    tooltip: {
                        show: true,
                        content: function (label, xval, yval, flotItem) {

                            if (label == "Active User") {
                                return "%s | Total user: %y";
                            } else {
                                var minutes = Math.floor((yval) / 60);
                                var seconds = (yval) - minutes * 60;
                                var timestring = "";

                                if (minutes > 0) {
                                    timestring += minutes + " mins ";
                                }

                                if (seconds > 0) {
                                    timestring += seconds + " sec ";
                                }

                                return "%s | Time taken: " +timestring;
                            }
                            
                        },
                        defaultTheme: false
                    }
                };

                

                maingraphobj = $.plot('#ca-maingraph', maingraph, mainoptions);

                $('#ca-maingraph').append('<label id="starting_month" style="position: absolute;bottom: -6px; left: -10px;  font-weight: 700;">' + starting_month + '</label><label id="ending_month" style="position:absolute; right: 0px; bottom: -6px; font-weight: 700;">' + ending_month + '</label>');

                $('#ca-maingraph').bind('plotclick', function (event, pos, item) {
                    if (item) {
                        console.log(item);
                        var date = new Date(item.datapoint[0]).getDate();
                        var month = moment(new Date(item.datapoint[0])).format("MMM");
                        var year = new Date(item.datapoint[0]).getFullYear();
                        var dateString = date + "-" + month + "-" + year;

                        url = window.location.protocol + "//" + window.location.host + "/Analytics/DailyTopicActivities/" + dateString;
                        window.open(url, '_blank');
                    }
                });

            }

            function generateTopicTable() {
                topic_table.bind('dynatable:afterUpdate', function (e, dynatable) {
                    setTimeout(function () {
                        topic_table.css('display', 'table');
                        topic_table.addClass('animated fadeIn');
                    }, 500);
                });

                topic_table.dynatable({
                    features: {
                        paginate: false,
                        search: false,
                        sorting: true,
                        recordCount: false,
                        pushState: false,
                    },
                    dataset: {
                        records: topic_table_data,
                        sorts: { 'no': 1 }
                    }
                });


                var dynatable = topic_table.data('dynatable');

                if (typeof dynatable.records !== "undefined") {
                    dynatable.records.updateFromJson({ records: topic_table_data });
                    dynatable.records.init();
                }
                dynatable.paginationPerPage.set(15);
                dynatable.process();
                
                topic_table.data('dynatable').settings.dataset.records = topic_table_data;
                topic_table.data('dynatable').dom.update();

            }

            

            ///////TOGGLE DIFFICULTY////////
            function ToggleDifficulty(diff) {
                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: '/Api/Analytics/SelectTopicAttempts',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "Metric": diff,
                        "Limit": 5
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {
                            var topic = res.Topics;

                            // TOPIC TABLE GENERATION
                            $.each(topic, function (key, value) {
                                topic_table_data.push({
                                    "date": value.Topic.CreatedOnTimestampString,
                                    "topic": '<img src="' + value.Topic.TopicLogoUrl + '" /> ' + value.Topic.TopicTitle,
                                    "question_no": value.Topic.TotalNumberOfQuestions,
                                    "status": '<div class="indicator ' + value.Topic.Status.Title.toLowerCase() + '-indicator"></div>',
                                    "question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >' + value.Correct + '</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((value.Correct / (value.Correct + value.Incorrect)) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >' + value.Incorrect + '</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((value.Incorrect / (value.Incorrect + value.Correct)) * 100) + '%"></div></div></li></ul>',
                                    "analytic": '<button class="btn btn-info btn-detail " data-topicid="' + value.Topic.TopicId + '">Details</button>'
                                });
                            });

                            generateTopicTable();
                        }
                    }
                });
            }
            /////////////////////////////////

            $(document).on('click', '.btn-detail', function (e) {
                e.preventDefault();
                var topicId = $(this).data('topicid');

                url = window.location.protocol + "//" + window.location.host + "/Analytics/SingleTopicAnalytic/" + topicId;
                window.open(url, '_blank');
            });


            $('#difficulty-filter').on('change', function () {
                topic_table_data = [];
                ToggleDifficulty($(this).val());
            });

            

            function toggleArrow(result) {
                var icon = "";
                if (result > 0) {
                    icon = '<i class="fa fa-arrow-up color1"></i> ' + result;
                } else if (result < 0) {
                    icon = '<i class="fa fa-arrow-down color3"></i> ' + (-result);
                } else {
                    icon = "" + result;
                }

                return icon;
            }

            var maxAttempt = 25;
            var arrSize = 25;
            var colorArr = [];

            function generateColorArr() {
                colorArr = [
                { 'color1': '#ab0900', 'color3': '#fbefb3', 'color2': '#36729e', 'count': generateLabel(25), 'min': generateLabel(24), 'max': generateLabel(26) },
                { 'color1': '#df1900', 'color3': '#faf5ab', 'color2': '#3683a7', 'count': generateLabel(24), 'min': generateLabel(23), 'max': generateLabel(25) },
                { 'color1': '#ff512d', 'color3': '#f4f8a3', 'color2': '#3796af', 'count': generateLabel(23), 'min': generateLabel(22), 'max': generateLabel(24) },
                { 'color1': '#ff8e75', 'color3': '#e9f79c', 'color2': '#37acb8', 'count': generateLabel(22), 'min': generateLabel(21), 'max': generateLabel(23) },
                { 'color1': '#ffc4ba', 'color3': '#dcf594', 'color2': '#37c2c0', 'count': generateLabel(21), 'min': generateLabel(20), 'max': generateLabel(22) },
                { 'color1': '#ac00a5', 'color3': '#cdf38d', 'color2': '#38c9b8', 'count': generateLabel(20), 'min': generateLabel(19), 'max': generateLabel(21) },
                { 'color1': '#df00c9', 'color3': '#bef186', 'color2': '#3dcdad', 'count': generateLabel(19), 'min': generateLabel(18), 'max': generateLabel(20) },
                { 'color1': '#ff22e4', 'color3': '#adef7f', 'color2': '#42d1a2', 'count': generateLabel(18), 'min': generateLabel(17), 'max': generateLabel(19) },
                { 'color1': '#ff72ea', 'color3': '#9ced78', 'color2': '#48d597', 'count': generateLabel(17), 'min': generateLabel(16), 'max': generateLabel(18) },
                { 'color1': '#ffb9f6', 'color3': '#8aea72', 'color2': '#4dd88c', 'count': generateLabel(16), 'min': generateLabel(15), 'max': generateLabel(17) },
                { 'color1': '#05409b', 'color3': '#76e86b', 'color2': '#53dc82', 'count': generateLabel(15), 'min': generateLabel(14), 'max': generateLabel(16) },
                { 'color1': '#1d5fca', 'color3': '#65e567', 'color2': '#59df78', 'count': generateLabel(14), 'min': generateLabel(13), 'max': generateLabel(15) },
                { 'color1': '#4b93e3', 'color3': '#5fe270', 'color2': '#5fe270', 'count': generateLabel(13), 'min': generateLabel(12), 'max': generateLabel(14) },
                { 'color1': '#8ab9ec', 'color3': '#59df78', 'color2': '#65e567', 'count': generateLabel(12), 'min': generateLabel(11), 'max': generateLabel(13) },
                { 'color1': '#c0d9f5', 'color3': '#53dc82', 'color2': '#76e86b', 'count': generateLabel(11), 'min': generateLabel(10), 'max': generateLabel(12) },
                { 'color1': '#009b0e', 'color3': '#4dd88c', 'color2': '#8aea72', 'count': generateLabel(10), 'min': generateLabel(9), 'max': generateLabel(11) },
                { 'color1': '#00ca33', 'color3': '#48d597', 'color2': '#9ced78', 'count': generateLabel(9), 'min': generateLabel(8), 'max': generateLabel(10) },
                { 'color1': '#41e358', 'color3': '#42d1a2', 'color2': '#adef7f', 'count': generateLabel(8), 'min': generateLabel(7), 'max': generateLabel(9) },
                { 'color1': '#86ed94', 'color3': '#3dcdad', 'color2': '#bef186', 'count': generateLabel(7), 'min': generateLabel(6), 'max': generateLabel(8) },
                { 'color1': '#bef5c6', 'color3': '#38c9b8', 'color2': '#cdf38d', 'count': generateLabel(6), 'min': generateLabel(5), 'max': generateLabel(7) },
                { 'color1': '#02ADA5', 'color3': '#37c2c0', 'color2': '#dcf594', 'count': generateLabel(5), 'min': generateLabel(4), 'max': generateLabel(6) },
                { 'color1': '#2ABFB8', 'color3': '#37acb8', 'color2': '#e9f79c', 'count': generateLabel(4), 'min': generateLabel(3), 'max': generateLabel(5) },
                { 'color1': '#65D6D1', 'color3': '#3796af', 'color2': '#f4f8a3', 'count': generateLabel(3), 'min': generateLabel(2), 'max': generateLabel(4) },
                { 'color1': '#C7FCFA', 'color3': '#3683a7', 'color2': '#faf5ab', 'count': generateLabel(2), 'min': generateLabel(1), 'max': generateLabel(3) },
                { 'color1': '#E8FFFE', 'color3': '#36729e', 'color2': '#fbefb3', 'count': generateLabel(1), 'min': 0, 'max': generateLabel(2) }];
            }

            function generateLabel(index) {

                if (maxAttempt < 25) {
                    maxAttempt = 25;
                } 

                var interval = Math.ceil(maxAttempt / arrSize);

                return (index * interval);
            }


            function generateLegend() {
                legendHtml = "";
                $.each(colorArr, function (k, v) {

                    if (colorThemeId == 1) {
                        legendHtml += '<div class="chart-legend-item"><div class="color-indicator" style="background:' + v.color1 + ';"></div><span>' + generateLabel((colorArr.length) - k) + '</span></div>';
                    } else {
                        legendHtml += '<div class="chart-legend-item"><div class="color-indicator" style="background:' + v.color2 + ';"></div><span>' + generateLabel((colorArr.length) - k) + '</span></div>';
                    }

                });
                $('.chart-legend-container').empty();
                $('.chart-legend-container').append(legendHtml);
            }

            function getColor(count) {

                for (var i = 0; i < colorArr.length; i++) {

                    if (colorThemeId == 1) {
                        if (count <= 0) {
                            return colorArr[colorArr.length - 1].color1;
                        } else if (count > colorArr[i].min && count < colorArr[i].max) {
                            return colorArr[i].color1;
                        }
                    } else {
                        if (count <= 0) {
                            return colorArr[colorArr.length - 1].color2;
                        } else if (count > colorArr[i].min && count < colorArr[i].max) {
                            return colorArr[i].color2;
                        }
                    }
                }
            }


            function generateHeatMap() {
                // HEAT MAP GENERATION
                $.each(heatmap, function (k, v) {
                    $.each(heatmap[k].Hours, function (a, b) {
                        heatmap_data[a].dataPoints[k] = { "y": -100, "label": v.DayString, "attempts": b.AttemptCount, "hours": b.HourString, "color": getColor(b.AttemptCount) };
                    });
                });

                heatmap_chart = new CanvasJS.Chart("chartContainer",
                {
                    axisY: {
                        maximum: 0000,
                        minumum: -2400,
                        viewportMinimum: -2400,
                        viewportMaximum: 0000,
                        interval: 400,
                        labelFormatter: function (e) {
                            var pad = "0000";
                            var str = "" + (-(e.value));
                            return pad.substring(0, pad.length - str.length) + str;
                        },
                        //valueFormatString: "000#",

                        suffix: " hrs",
                        gridThickness: 1,
                        lineThickness: 1,
                        tickThickness: 1
                    },
                    animationEnabled: true,
                    axisX: {
                        lineThickness: 1
                    },
                    data: heatmap_data
                });

                heatmap_chart.render();
            }

            ///////FETCH DATA////////
            function FetchHeatMapData(colorThemeId) {
                $('#matchup_improvement').addClass('hidden');
                $('#matchup_improvement').removeClass('animated fadeInUp');

                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: '/Api/Analytics/SelectMatchUpOverall',
                    data: {
                        "CompanyId": CompanyId, //"Cb92677d021754d27802629872a13d006"
                        "AdminUserId": AdminUserId //U837895830b1e42c08340244214090cf1
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {

                            heatmap = res.HeatMap.Days;
                            var report = res.Dau.Reports;
                            var overview = res.Overview;
                            var dau = res.Dau;
                            var topic = res.Topics;

                            if (res.HeatMap.MaxCount) {
                                maxAttempt = res.HeatMap.MaxCount;
                                //maxAttempt = 48;
                                generateColorArr();
                            }                            

                            generateLegend();

                            $('#matchup_improvement').removeClass('hidden');
                            $('#matchup_improvement').addClass('animated fadeInUp');

                            //console.log(res.Dau.MaxTimeTaken);
                            //res.Dau.MaxTimeTaken = res.Dau.MaxTimeTaken + 300;
                            if (res.Dau.MaxTimeTaken > 0) {
                                maxTimeTaken = Math.ceil(res.Dau.MaxTimeTaken/ 100) * 100 ;
                            } else {
                                maxTimeTaken = 100; //if the given max time taken is 0, set it 100sec to construct time y axis
                            }

                            if (res.Dau.MaxUserCount % 10 == 0) {
                                maxUsers = res.Dau.MaxUserCount + 5;
                            } else {
                                maxUsers = Math.ceil(res.Dau.MaxUserCount / 10) * 10;
                            }
                           
                            timeInterval = Math.ceil(maxTimeTaken / 5);
                            userInterval = Math.ceil(maxUsers / 5);
                            
                            for (var i = 1; i <= 5; i++) {

                                var minutes = Math.floor((timeInterval * i) / 60);
                                var seconds = (timeInterval * i) - minutes * 60;
                                var timestring = "";

                                if (minutes > 0) {
                                    timestring += minutes + " mins ";
                                }

                                if (seconds > 0) {
                                    timestring += seconds + " sec ";
                                }

                                if (i == 5) {
                                    arrInterval.push([timeInterval * i, timestring]);
                                    userArrInterval.push([userInterval * i, userInterval * i + " users"]);
                                } else {
                                    arrInterval.push([timeInterval * i, timestring]);
                                    userArrInterval.push([userInterval * i, userInterval * i + " "]);
                                }
                            }


                            $('#survey-start-date').html(report[0].DateString);
                            $('#start_date').html(report[0].DateString);
                            $('#survey-end-date').html(report[report.length - 1].DateString);
                            $('#daily_unique_value').html(toggleArrow(dau.Today.DifferenceInNumber));
                            $('#daily_unique_percentage').html(toggleArrow(dau.Today.DifferenceInPercentage));
                            $('#daily_unique').html(dau.Today.TotalUniqueUsers);
                            $('#weekly_unique_value').html(toggleArrow(dau.LastSevenDays.DifferenceInNumber));
                            $('#weekly_unique_percentage').html(toggleArrow(dau.LastSevenDays.DifferenceInPercentage) );
                            $('#weekly_unique').html(dau.LastSevenDays.TotalUniqueUsers);
                            $('#monthly_unique_value').html(toggleArrow(dau.LastThirtyDays.DifferenceInNumber));
                            $('#monthly_unique_percentage').html(toggleArrow(dau.LastThirtyDays.DifferenceInPercentage));
                            $('#monthly_unique').html(dau.LastThirtyDays.TotalUniqueUsers);
                            $('#average_unique_value').html();
                            $('#average_unique_percentage').html();
                            $('#average_timespent').html(dau.AverageTimeSpentString);

                            $('#total-topics').html(overview.TotalTopics);
                            $('#total-uniquequestion').html(overview.TotalQuestions);
                            $('#total-matchup').html(overview.TotalMatchUps);
                            $('#total-topic-attempt').html(overview.AverageTopicAttempts.toFixed(2));
                            
                            for (var i = 0; i < 24; i++) {
                                heatmap_data.push({ "type": "stackedColumn", "dataPoints": [] });
                            }

                            

                            generateHeatMap();

                            // ACTIVE USERS CHART
                            $.each(report, function (key, value) {
                                var year = new Date(value.DateString).getFullYear();
                                var month = new Date(value.DateString).getMonth() + 1;
                                var day = new Date(value.DateString).getDate();
                                dau_users.push([gd(year, month, day), value.UserCount]);
                                dau_timespent.push([gd(year, month, day), value.AverageTimeTaken ]);
                                //console.log(value.AverageTimeTaken);
                            });

                            drawDAUChart();

                            // TOPIC TABLE GENERATION
                            $.each(topic, function (key, value) {
                                topic_table_data.push({
                                    "date": value.Topic.CreatedOnTimestampString,
                                    "topic": '<img src="' + value.Topic.TopicLogoUrl + '" /> ' + value.Topic.TopicTitle,
                                    "question_no": value.Topic.TotalNumberOfQuestions,
                                    "status": '<div class="indicator ' + value.Topic.Status.Title.toLowerCase() + '-indicator"></div>',
                                    "question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >' + value.Correct + '</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((value.Correct / (value.Correct + value.Incorrect)) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >' + value.Incorrect + '</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((value.Incorrect / (value.Incorrect + value.Correct)) * 100) + '%"></div></div></li></ul>',
                                    "analytic": '<button class="btn btn-info btn-detail" data-topicid="' + value.Topic.TopicId + '">Details</button>'
                                });
                            });

                            generateTopicTable();
                        }
                    }
                });
            }


            $('#toggleColor').on('click', function (e) {
                e.preventDefault();
                if (colorThemeId == 1) {
                    colorThemeId = 2;
                } else {
                    colorThemeId = 1;
                }

                generateLegend();
                generateHeatMap();
            });

            FetchHeatMapData(colorThemeId);
            ////////////////
        });
    </script>
</asp:Content>
