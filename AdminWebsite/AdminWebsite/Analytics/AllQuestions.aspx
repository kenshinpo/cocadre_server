﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AllQuestions.aspx.cs" Inherits="AdminWebsite.Analytics.AllQuestions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta"><a href="/Analytics/MatchUpImprovement" id="root-link">MatchUp Improvement</a></div>
        <div class="appbar__meta"><a href="" id="second-link" ></a>All Questions</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="matchup-wrapper" class="data" >
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Analytics/MatchUpImprovement">MatchUp Improvement</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="feed_posts">Feed Posts</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="storage">Storage Utilization</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="relationship">Relationship</a>
                </li>--%>
            </ul>
        </aside>

        <div class="data__content">

            <div id="matchup_improvement" class="container" style="width:100%;">
                <div style="width:100%; margin:0 auto;">

                    <!-- TITLE -->
                    <div class="title-section" style="width:100%; margin: 0 auto;">
                        <div class="matchup-info">
                            <h1><img id="survey-icon" src="/Img/icon-quiz.png" /> MatchUp Improvement</h1>
                            <%--<p class="align-left">Showing data from <span id="survey-start-date"></span> - <span id="survey-end-date"></span></p>--%>
                        </div>
                        <div class="header-side">
                            <h2 style="text-align:right; padding-top:20px;">Since beginning</h2>
                        </div>
                    </div>

                    <div class="search-section">
                        <div class="input-group" style="position:relative;">
                            <i class="fa fa-search" style="left:15px;"></i>
                            <input id="topic_search" class="search-input" type="text" placeholder="Search Topics" value="" style="margin:0px;"/>
                        </div>
                    </div>

                    <!-- ALL TOPIC TABLE -->
                    <div class="card full-row animated fadeInUp no-padding">
                        <div class="card-header" style="padding: 20px;">
                            <div class="filter-section">                  
                                <div class="mdl-selectfield" style="width:20%;float:left;margin-right: 20px; height:40px;">
                                    <select name="question-filter" id="question-filter">
	                                    <option value="1">Hardest Questions</option>
                                        <option value="2">Easiest Questions</option>
                                    </select>
                                </div> 
                                <div class="table-legends">
                                    <div class="status-legends text-right" >
                                        <label>Status</label>
                                        <ul class="inline-ul">
                                            <li><div class="indicator active-indicator"></div> Active</li>
                                            <li><div class="indicator hidden-indicator"></div> Hidden</li>
                                        </ul>
                                    </div>
                                    <div class="attempt-legends" >
                                        <label>Attempts</label>
                                        <ul class="inline-ul">
                                            <li><div class="indicator color1"></div> Correct</li>
                                            <li><div class="indicator color3"></div> Wrong</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="table-content">
                            <table id="question-table" class="responder-table">
                                <thead>
                                    <th data-dynatable-column="date" style="width:13%">Listed</th>
                                    <th data-dynatable-column="topic" style="width:42%">Topic</th>
                                    <th data-dynatable-column="topic_search" class="hidden"></th>
                                    <th data-dynatable-column="status" data-dynatable-sorts="status" style="width:8%">Status</th>
                                    <th data-dynatable-column="question_attempt" data-dynatable-sorts="question_attempt" style="width:25%">Attempts</th>
                                    <th data-dynatable-column="analytic" data-dynatable-sorts="analytic" style="width:12%">Analytic</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <!-- PAGINATION -->
                    <%--<nav class="align-center">
                        <ul class="pagination">
                            <li class="disabled">
                                <a href="#"><span><</span></a>
                            </li>
                            <li class="active">
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#"><span>></span></a>
                            </li>
                        </ul>
                    </nav>--%>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
        <script src="/js/vendor/moment-with-locales.js"></script>
        <script src="/Js/Chart.js"></script>
        <script>
            $(function () {
            
                //var CompanyId = 'C8b28091502514318bdcf48bec7c69129';
                //var AdminUserId = 'Udb0e704ee7244d21aea7930c27709fee';

                //function getUrlParameter(sParam) {
                //    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                //        sURLVariables = sPageURL.split('&'),
                //        sParameterName,
                //        i;

                //    for (i = 0; i < sURLVariables.length; i++) {
                //        sParameterName = sURLVariables[i].split('=');

                //        if (sParameterName[0] === sParam) {
                //            return sParameterName[1] === undefined ? true : sParameterName[1];
                //        }
                //    }
                //};

                //var TopicId = getUrlParameter('TopicId');

                var question_table = $('#question-table');
                var question_table_data = [];

                function generateQuestionTable() {
                    question_table.bind('dynatable:afterUpdate', function (e, dynatable) {
                        setTimeout(function () {
                            question_table.css('display', 'table');
                            question_table.addClass('animated fadeIn');
                        }, 500);
                    });

                    question_table.bind('dynatable:init', function (e, dynatable) {
                        dynatable.queries.functions['topic_search'] = function (record, queryValue) {
                            return record.topic_search.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                        };

                    });

                    question_table.dynatable({
                        features: {
                            paginate: false,
                            search: false,
                            sorting: true,
                            recordCount: false,
                            pushState: false,
                        },
                        dataset: {
                            records: question_table_data,
                            sorts: { 'no': 1 }
                        },
                        inputs: {
                            queries: $(' #topic_search')
                        }
                    });


                    var dynatable = question_table.data('dynatable');

                    if (typeof dynatable.records !== "undefined") {
                        dynatable.records.updateFromJson({ records: question_table_data });
                        dynatable.records.init();
                    }
                    dynatable.paginationPerPage.set(15);
                    dynatable.process();

                    question_table.data('dynatable').settings.dataset.records = question_table_data;
                    question_table.data('dynatable').dom.update();

                }

                function FetchTableData(diff) {
                    $('#matchup_improvement').addClass('hidden');
                    $('#matchup_improvement').removeClass('animated fadeInUp');
                    ShowProgressBar();
                    $.ajax({
                        type: "POST",
                        url: '/Api/Analytics/SelectQuestionAttempts',
                        data: {
                            "CompanyId": CompanyId,
                            "AdminUserId": AdminUserId,
                            "TopicId": TopicId,
                            "Metric": diff,
                            "Limit": 0
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            HideProgressBar();
                            if (res.Success) {
                                $('#matchup_improvement').removeClass('hidden');
                                $('#matchup_improvement').addClass('animated fadeInUp');
                                var question = res.Questions;

                                //var dateNow = new Date();
                                //var dateThen = new Date(summary.Topic.CreatedOnTimestampString);

                                //$('#survey-start-date').html(summary.Topic.CreatedOnTimestampString);
                                //$('#survey-end-date').html(moment(dateNow).format('DD/MM/YYYY hh:mm A'));

                                // TOPIC TABLE GENERATION
                                $.each(question, function (key, value) {
                                    var status = ""; var topicString = "";

                                    if (value.Question.Status == 1) {
                                        status = "active";
                                    } else {
                                        status = "hidden";
                                    }

                                    if (value.Question.ContentImageUrl != undefined) {
                                        topicString = '<div class="question-image"><img src="' + value.Question.ContentImageUrl + '" /></div><p class="text">' + value.Question.Content + '</p>';
                                    } else {
                                        topicString = '<div class="question-image"></div><p class="text">' + value.Question.Content + '</p>';
                                    }

                                    question_table_data.push({
                                        "date": value.Question.CreatedOnTimestampString,
                                        "topic": topicString,
                                        "topic_search": value.Question.Content,
                                        "status": '<div class="indicator ' + status + '-indicator"></div>',
                                        "question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >' + value.Correct + '</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((value.Correct / (value.Correct + value.Incorrect)) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >' + value.Incorrect + '</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((value.Incorrect / (value.Incorrect + value.Correct)) * 100) + '%"></div></div></li></ul>',
                                        "analytic": '<button class="btn btn-info btn-detail" data-questionid="' + value.Question.Id + '" data-topicid="' + TopicId + '">Details</button>'
                                    });
                                });

                                generateQuestionTable();
                            }
                        }
                    });
                }

                $(document).on('click', '.btn-detail', function (e) {
                    e.preventDefault();

                    //var params = $.param({
                    //    "TopicId": $(this).data('topicid'),
                    //    "QuestionId": $(this).data('questionid')
                    //});

                    //var url = window.location.href;
                    //var hash = location.hash;
                    //url = window.location.protocol + "//" + window.location.host + "/Analytics/QuestionDetail.aspx/";

                    //if (url.indexOf("?") < 0)
                    //    url += "?" + params;
                    //else
                    //    url += "&" + params;

                    //window.open(url + hash, '_blank');

                    url = window.location.protocol + "//" + window.location.host + "/Analytics/QuestionDetail/" + $(this).data('topicid') + "/" + $(this).data('questionid');
                    window.open(url, '_blank');
                });

                $('select#question-filter').on('change', function (e) {
                    question_table_data = [];
                    FetchTableData($(this).val());
                });

                FetchTableData(1);

            });
        </script>
</asp:Content>
