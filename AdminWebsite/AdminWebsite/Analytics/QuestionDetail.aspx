﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="QuestionDetail.aspx.cs" Inherits="AdminWebsite.Analytics.QuestionDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .ca-chartdraw {
            width:100%;
            min-height:300px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta"><a href="/Analytics/MatchUpImprovement" id="root-link">MatchUp Improvement</a></div>
        <div class="appbar__meta" id="question_breadcrumb"><a href="" id="second-link"></a></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="matchup-wrapper" class="data" >
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Analytics/MatchUpImprovement">MatchUp Improvement</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="feed_posts">Feed Posts</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="storage">Storage Utilization</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="relationship">Relationship</a>
                </li>--%>
            </ul>
        </aside>

        <div class="data__content">

            <div id="matchup_improvement" class="container" style="width:100%;">
                <div style="width:100%; margin:0 auto;">

                    <!-- ACCUMULATED QUESTION ATTEMPTS -->
                    <div class="card full-row animated fadeInUp" style="position:relative; min-height:330px;">
                        <div class="half-col">
                            <h1 class="title">Question <span id="question_id"></span></h1>

                            <p id="question_text"></p>

                            <div class="answer-attempt-container">
                                <label class="title">Answers Attempt</label>
                                <ol class="answer-attempt-list">
                                   
                                </ol>
                            </div>
                        </div>
                        <div class="half-col"  style="position:relative;">
                            <label>Showing data from <span class="start_date">-</span> to <span class="end_date">-</span></label>
                            <div class="card-header" style="margin-top:20px;">
                                <h1 class="title">Total Attempts</h1>
                                <div class="attempt-legends" style="position: absolute;top: 50px; right: 25px;">
                                    <ul class="inline-ul">
                                        <li><div class="indicator color1"></div> Correct</li>
                                        <li><div class="indicator color3"></div> Wrong</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="chart-section" style="width:100%; height:auto;">

                                <ul class="list-group med-progress-group" >
                                    <li class="list-group-item">
                                        
                                        <span id="correct-attempt" class="list-value">123</span>
                                        <div class="progress" style="padding-right:50px;">
                                            <div id="completed-bar" class="progress-bar color1" style="width:0%;"></div>
                                            <span id="correct-attempt-percentage" class="list-percentage color1" style="position:absolute;"></span>
                                        </div>
                                    </li> 
                                    <li class="list-group-item">
                                        <span id="wrong-attempt" class="list-value"></span>
                                        
                                        <div class="progress" style="padding-right:50px;">
                                            <div id="incompleted-bar" class="progress-bar color3" style="width:0%;"></div>
                                            <span id="wrong-attempt-percentage" class="list-percentage color3" style="position:absolute;"></span>
                                        </div>
                                    </li>
                                </ul>

                                <div class="clearfix"></div>

                                <div class="chart-content" style="margin-top:50px;">
                                    <label class="title">Analytic</label>
                                    <div id="chartContainer" style="width:100%; height:350px; padding-left:50px; position:relative;"></div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="chart-footer">
                            <label class="info-label">From <span class="start_date">-</span> to <span class="end_date">-</span></label>
                            <div class="chart-info">
                                <div class="chart-border"></div>
                                <label id="first_attempt" class="response_number">0</label>
                                <small>questions</small>
                                <p>First Attempt Correct</p>
                            </div> 
                            <div class="chart-info">
                                <label id="total_failed" class="response_number">0</label>
                                <small>questions</small>
                                <p>Total failed attempts</p>
                            </div>
                            <div class="chart-total-responder">
                                <div class="border-box">
                                    <small>Out of</small>
                                    <label id="total_attempts" class="response_number">0</label>
                                    <small class="highlight">questions</small>
                                    <p>Total Attempts</p>
                                </div>
                            </div>
                        </div>
                        <div class="chart-footer">
                            <div class="chart-info">
                                <div class="chart-border"></div>
                                <label id="2nd_attempt" class="response_number">0</label>
                                <small>questions</small>
                                <p class="color4">2nd Attempt Correct</p>
                            </div>
                            <div class="chart-info">
                                <div class="chart-border"></div>
                                <label id="3rd_attempt" class="response_number">0</label>
                                <small>questions</small>
                                <p style="color:#4b93e3;">3rd & above<br />Attempt Correct</p>
                            </div>
                            <div class="chart-info">
                                <div class="chart-border"></div>
                                <label id="2nd_attempt_wrong" class="response_number">0</label>
                                <small>questions</small>
                                <p style="color:#ffae00;">2nd Attempt<br />Wrong</p>
                            </div>
                            <div class="chart-info">
                                <label id="3rd_attempt_wrong" class="response_number">0</label>
                                <small>questions</small>
                                <p class="color3">3rd & above<br />Attempt Wrong</p>
                            </div>
                        
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
        <script src="/js/vendor/moment-with-locales.js"></script>
        <script src="/js/vendor/canvasjs.js"></script>
        <script src="/Js/Chart.js"></script>
        <script>
            $(function () {
                ///////////////////////
                //var CompanyId = 'C8b28091502514318bdcf48bec7c69129';
                //var AdminUserId = 'Udb0e704ee7244d21aea7930c27709fee';
                var attempt_data = [];

                //function getUrlParameter(sParam) {
                //    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                //        sURLVariables = sPageURL.split('&'),
                //        sParameterName,
                //        i;

                //    for (i = 0; i < sURLVariables.length; i++) {
                //        sParameterName = sURLVariables[i].split('=');

                //        if (sParameterName[0] === sParam) {
                //            return sParameterName[1] === undefined ? true : sParameterName[1];
                //        }
                //    }
                //};

                //var TopicId = getUrlParameter('TopicId');
                //var QuestionId = getUrlParameter('QuestionId');

                function FetchData() {
                    $('#matchup_improvement').addClass('hidden');
                    $('#matchup_improvement').removeClass('animated fadeInUp');
                    ShowProgressBar();
                    $.ajax({
                        type: "POST",
                        url: '/Api/Analytics/SelectOptionAttempts',
                        data: {
                            "CompanyId": CompanyId,
                            "AdminUserId": AdminUserId,
                            "TopicId": TopicId,
                            "QuestionId": QuestionId
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            HideProgressBar();
                            if (res.Success) {
                                $('#matchup_improvement').removeClass('hidden');
                                $('#matchup_improvement').addClass('animated fadeInUp');
                                var html = "";
                                var question = res.Question;
                                var option = res.Options;
                                var analytic = res.Analytics;

                                $('#question_breadcrumb').html('Question ' + question.Question.Id);
                                $('#question_id').html(question.Question.Id);
                                $('#question_text').html(question.Question.Content);

                                $('#correct-attempt-percentage').html(((question.Correct / (question.Correct + question.Incorrect)) * 100).toFixed(2) + "%");
                                $('#wrong-attempt-percentage').html(((question.Incorrect / (question.Correct + question.Incorrect)) * 100).toFixed(2) + "%");
                                $('#completed-bar').css('width', (question.Correct / (question.Correct +question.Incorrect)) * 100 +"%" );
                                $('#incompleted-bar ').css('width', (question.Incorrect / (question.Correct +question.Incorrect)) * 100 +"%" );
                                $('#wrong-attempt ').html(question.Incorrect);
                                $('#correct-attempt ').html(question.Correct);

                                      
                                $('#first_attempt').html(analytic.FirstAttemptCorrect);
                                $('#total_failed').html(analytic.TotalFailedAttempt);
                                $('#total_attempts').html(analytic.FirstAttemptCorrect + analytic.SecondAttemptCorrect + analytic.ThirdAboveAttemptCorrect + analytic.TotalFailedAttempt);
                                $('#2nd_attempt').html(analytic.SecondAttemptCorrect);
                                $('#3rd_attempt').html(analytic.ThirdAboveAttemptCorrect);
                                $('#2nd_attempt_wrong').html(analytic.SecondAttemptIncorrect);
                                $('#3rd_attempt_wrong').html(analytic.ThirdAboveAttemptIncorrect);

                                var dateNow = new Date();

                                $('.start_date').html(question.Question.CreatedOnTimestampString);
                                $('.end_date').html(moment(dateNow).format('DD/MM/YYYY hh:mm A'));
                                
                                $.each(option.Options, function (key, value) {

                                    html += '<li class="answer-item">';
                                    html += ' <p class="question">'+value.Content+'</p>';
                                    html += '<div class="progress" style="margin:0px;padding-right:50px;">';

                                    if (key > 0) {
                                        html += '<div id="question1-bar" class="progress-bar color3" style="width:' + ((value.SelectedCount / option.MaxCount) * 100) + '%;"></div>';
                                    } else {
                                        html += '<div id="question1-bar" class="progress-bar color1" style="width:' + ((value.SelectedCount / option.MaxCount) * 100) + '%;"></div>';
                                    }

                                    html += '<span id="question1-percentage" class="list-percentage " style="position:absolute;">'+value.SelectedCount+'</span>';
                                    html += '</div>';
                                    html += '</li>';

                                });

                                $('.answer-attempt-list').append(html);

                                attempt_data.push({ "type": "stackedColumn", "dataPoints": [] }); //correct 
                                attempt_data.push({ "type": "stackedColumn", "dataPoints": [] }); //incorrect
                              
                                $.each(analytic.Attempts, function (key, value) {
                                    attempt_data[0].dataPoints.push({ "y": value.Correct, "label": value.Attempt, "attempts": value.Correct, "hours": "Correct", "attempt_no":key+1 });
                                    attempt_data[1].dataPoints.push({ "y": -value.Incorrect, "label": value.Attempt, "attempts": -value.Incorrect, "hours": "Wrong", "attempt_no": key + 1 });
                                });

                                var chart = new CanvasJS.Chart("chartContainer",
                                {
                                    axisY: {
                                        maximum: analytic.MaxCorrect,
                                        minumum: -analytic.MaxIncorrect,
                                        //interval: 250,
                                        //valueFormatString: "000#",
                                        //suffix: " hrs",
                                        gridThickness: 1,
                                        lineThickness: 1,
                                        tickThickness: 1

                                    },
                                    dataPointWidth: 50,
                                    animationEnabled: true,
                                    axisX: {
                                        lineThickness: 1,
                                        //labelMaxWidth: 80,
                                        //labelWrap: true,
                                        labelAutoFit: true,
                                    },
                                    data: attempt_data

                                });

                                chart.render();

                                $('#chartContainer').append('<label style="position:absolute; top:-17px;left:-18px;">Correct Attempts</label><label style="position:absolute; bottom:10px; left:-18px;">Wrong Attempts</label>');
                            }
                        }
                    });
                }

                FetchData();

                
            });
        </script>
</asp:Content>
