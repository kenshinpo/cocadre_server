﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="DailyActiveUsers.aspx.cs" Inherits="AdminWebsite.Analytics.DailyActiveUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="../css/vendor/pure-min.css" />
    <link rel="stylesheet" href="../css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTimezone" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta">Daily Active Users</div>
        <div class="appbar__action">
        </div>
    </div>
    <!-- / App Bar -->
    <div class="data">
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="MatchUpImprovement">MatchUp Improvement</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">

            <div class="pure-g">

                <div class="pure-u-3-4 ca-details" style="position: relative;">

                    <div id='chartDetail'>
                        <a href="" id="closeDetail">
                            <i class="fa fa-2x fa-times"></i>
                        </a>
                        
                        <div class="spinner"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>

                        <div class="chart-detail-content">

                            <div class="detail-header">
                                <a href="#" class="toggle-date-btn prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
                                <h1 class="date-title"> 08 Jun 2017</h1>
                                <a href="#" class="toggle-date-btn next"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                            </div>

                            <div class="detail-body">
                                <table id="chart-detail-table" class="responder-table">
                                    <thead>
                                        <th data-dynatable-column="user" style="width:30%">User</th>
                                        <th data-dynatable-column="department" data-dynatable-sorts="department" style="width:20%">Department</th>
                                        <th data-dynatable-column="start_time" data-dynatable-sorts="start_time" style="width:15%">Start time</th>
                                        <th data-dynatable-column="end_time" data-dynatable-sorts="end_time" style="width:15%">End time</th>
                                        <th data-dynatable-column="duration" data-dynatable-sorts="duration" style="width:20%">Duration</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <div class="detail-footer">
                                    <div class="footer-block">
                                        <h1><span id="totalTimeTaken"></span></h1>
                                        <span>Total time taken</span>
                                    </div>
                                    <div class="footer-block">
                                        <h1><span id="averageTimePerUser"></span></h1>
                                        <span>Average time taken per user</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="ca-detailshead">
                        <h2>Daily Active Users</h2>
                        <div id="ca-daterange">
                            <span style="font-weight: bolder;">From</span>
                            <input id="startDate" type="text" style="width: 150px;" onchange="changeDauDate(true);" />
                            <span style="font-weight: bolder;">to</span>
                            <input id="endDate" type="text" style="width: 150px;" onchange="changeDauDate(false);" />
                            <br />
                            <label style="margin-right: 15px;">Data shown are in UTC</label>
                        </div>
                    </div>
                    <div id="ca-maingraphlegend" class="ca-legend">
                        <table style="font-size: smaller; color: #545454">
                            <tbody>
                                <tr>
                                    <td class="legendColorBox">
                                        <div style="border: 1px solid #ccc; padding: 1px">
                                            <div style="width: 4px; height: 0; border: 5px solid #ff9600; overflow: hidden"></div>
                                        </div>
                                    </td>
                                    <td class="legendLabel">Active User</td>
                                    <td class="legendColorBox">
                                        <div style="width: 10px; height: 10px; border: 2px solid #ff2851; border-radius: 5px; overflow: hidden"></div>
                                    </td>
                                    <td class="legendLabel">Average time spent</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="ca-maingraph" class="ca-chartdraw" style="position: relative;">
                    </div>

                    <div id="ca-statsummary">
                        <span class="ca-gray small" id="ltlDauDate">As of</span>
                        <div class="pure-g">
                            <div class="pure-u-1-4 ca-statbox">
                                <div class="pure-g">
                                    <div class="pure-u-1-2">
                                        <h1>
                                            <span id="ltlDauUniqueUsers"></span>
                                        </h1>
                                        <span class="ca-gray">unique users</span>
                                    </div>
                                    <div class="pure-u-1-2 ca-statdivider">
                                        <div id="plDauUniUsers">
                                            <span id="ltlDauDiffNumber"></span>
                                            (<span id="ltlDauDiffPercentage"></span>)
                                        </div>
                                    </div>
                                </div>
                                Daily active users
                            </div>
                            <div class="pure-u-1-4 ca-statbox">
                                <div class="pure-g">
                                    <div class="pure-u-1-2">
                                        <h1>
                                            <span id="ltlLast7DaysUniqueUsers"></span>
                                        </h1>
                                        <span class="ca-gray">unique users</span>
                                    </div>
                                    <div class="pure-u-1-2 ca-statdivider">
                                        <div id="plLast7DaysUniUsers">
                                            <span id="ltlLast7DaysDiffNumber"></span>
                                            (<span id="ltlLast7DaysDiffPercentage"></span>)
                                        </div>
                                    </div>
                                </div>
                                Last 7 days
                            </div>
                            <div class="pure-u-1-4 ca-statbox">
                                <div class="pure-g">
                                    <div class="pure-u-1-2">
                                        <h1>
                                            <span id="LtlLast30DaysUniqueUsers"></span>
                                        </h1>
                                        <span class="ca-gray">unique users</span>
                                    </div>
                                    <div class="pure-u-1-2">
                                        <div id="plLast30DaysUniUsers">
                                            <span id="LtlLast30DaysDiffNumber"></span>
                                            (<span id="LtlLast30DaysDiffPercentage"></span>)
                                        </div>
                                    </div>
                                </div>
                                Last 30 days
                            </div>

                            <div class="pure-u-1-4 ca-statbox">
                                <div id="ca-licenses">
                                    Out of
                                    <h1>
                                        <span id="ltlTotalUsers"></span>
                                    </h1>
                                    <span class="ca-orange">User Licenses</span>
                                </div>
                            </div>

                            <div class="pure-u-1-4">
                                <div id="ca-mobilestats">
                                    <div class="ca-mobileicons">
                                        <i class="fa fa-apple"></i>
                                        <i class="fa fa-android"></i>
                                    </div>
                                    <div class="ca-mobilegraph">
                                        <div id="ca-applestats">
                                            <span id="ltlIosUsers"></span>
                                        </div>
                                        <div id="ca-applestatstxt" style="margin-left: 10px;">
                                            <span id="ltlIosUsersPercentage"></span>
                                            %
                                        </div>
                                        <div id="ca-androidstats">
                                            <span id="ltlAndroidUsers"></span>
                                        </div>
                                        <div id="ca-androidstatstxt" style="margin-left: 10px;">
                                            <span id="ltlAndroidUsersPercentage"></span>
                                            %
                                        </div>
                                    </div>
                                    <p class="clear ca-gray">Average number of unique devices over last 30 days.</p>
                                </div>
                            </div>
                            <div class="pure-u-1-4">
                                &nbsp;
                            </div>
                            <div class="pure-u-1-2 ca-statbox">
                                <div class="pure-g">
                                    <div class="pure-u-1">
                                        <h1>
                                            <span id="ltlAveTimeSpent"></span>
                                        </h1>
                                        <span>Average time spent</span>
                                    </div>
                                </div>
                                <span class="ca-gray">per user over last 30 days</span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="pure-u-1-4">
                </div>
            </div>

            <hr />

            <div class="pure-g">
                <div class="pure-u-3-4 ca-detailstable">
                    <h2>DAU Chart</h2>
                    <span class="ca-gray">Data shown in 30 days period.</span>
                    <table class="pure-table">
                        <thead>
                            <tr>
                                <th rowspan="2" style="border-top-left-radius: 8px;">Date</th>
                                <th colspan="3" style="border-bottom: 1px solid #ccc;">Monthly Active User</th>
                                <th rowspan="2">Attendance Rate</th>
                                <th colspan="2" style="border-top-right-radius: 8px; border-bottom: 1px solid #ccc;">Time Span</th>
                            </tr>
                            <tr>
                                <th style="border-left: 1px solid #ccc;">Total</th>
                                <th>Inactive User</th>
                                <th>Active User</th>
                                <th>Total Time</th>
                                <th>Average Time per Active User</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Last 30 days</td>
                            <td>
                                <span id="ltlDauChartTotalUser"></span>
                            </td>
                            <td>
                                <span id="ltlDauChartInactiveUser"></span>
                            </td>
                            <td>
                                <span id="ltlDauChartActiveUser"></span>
                            </td>
                            <td>
                                <span id="ltlDauChartAttRate"></span>
                                %</td>
                            <td>
                                <span id="ltlDauChartTotalTime"></span>
                                <br />
                            </td>
                            <td>
                                <span id="ltlDauChartAveTime"> </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- vendor includes -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="../js/vendor/flot/jquery.flot.min.js"></script>
    <script src="../js/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="../js/vendor/flot/jquery.flot.time.js"></script>
    <script src="../js/vendor/flot/jquery.flot.pie.min.js"></script>
    <script src="../js/vendor/moment-with-locales.js"></script>
    <script src="/js/Analytics/DailyActiveUsers.js"></script>
</asp:Content>
