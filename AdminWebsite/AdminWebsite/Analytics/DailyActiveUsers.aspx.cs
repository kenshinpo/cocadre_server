﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Web.UI;

namespace AdminWebsite.Analytics
{
    public partial class DailyActiveUsers : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }

            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                hfManagerId.Value = managerInfo.UserId;
                hfCompanyId.Value = managerInfo.CompanyId;
                hfTimezone.Value = managerInfo.TimeZone.ToString();


                if (!IsPostBack)
                {
                    // APOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO! Change here!!!
                    //AnalyticsSelectDetailDauResponse response = asc.SelectDau(adminInfo.UserId, adminInfo.CompanyId, DateTime.UtcNow.Date.AddDays(-1), DateTime.UtcNow.Date.AddDays(-1));
                    //if (response.Success)
                    //{
                    //    ltlDauDate.Text = response.DetailDau.CurrentDate.ToString("MMMM dd, yyyy");
                    //    #region Unique users of DAU
                    //    ltlDauUniqueUsers.Text = response.DetailDau.CurrentDateDau.TotalUniqueActiveUsers.ToString();
                    //    if (response.DetailDau.CurrentDateDau.DifferenceInNumber < 0)
                    //    {
                    //        plDauUniUsers.CssClass = "ca-changelabel ca-red";
                    //    }
                    //    else
                    //    {
                    //        plDauUniUsers.CssClass = "ca-changelabel ca-green";
                    //    }
                    //    ltlDauDiffNumber.Text = response.DetailDau.CurrentDateDau.DifferenceInNumber.ToString();
                    //    ltlDauDiffPercentage.Text = response.DetailDau.CurrentDateDau.DifferenceInPercentage.ToString("F2");
                    //    #endregion

                    //    ltlAveTimeSpent.Text = response.DetailDau.AverageTimeSpentPerUser.ToString();

                    //    #region Unique users of Last 7 Days
                    //    ltlLast7DaysUniqueUsers.Text = response.DetailDau.LastSevenDaysDau.TotalUniqueActiveUsers.ToString();
                    //    if (response.DetailDau.LastSevenDaysDau.DifferenceInNumber < 0)
                    //    {
                    //        plLast7DaysUniUsers.CssClass = "ca-changelabel ca-red";
                    //    }
                    //    else
                    //    {
                    //        plLast7DaysUniUsers.CssClass = "ca-changelabel ca-green";
                    //    }
                    //    ltlLast7DaysDiffNumber.Text = response.DetailDau.LastSevenDaysDau.DifferenceInNumber.ToString();
                    //    ltlLast7DaysDiffPercentage.Text = response.DetailDau.LastSevenDaysDau.DifferenceInPercentage.ToString("F2");
                    //    #endregion

                    //    #region Unique users of Last 30 Days
                    //    LtlLast30DaysUniqueUsers.Text = response.DetailDau.LastThirtyDaysDau.TotalUniqueActiveUsers.ToString();
                    //    if (response.DetailDau.LastThirtyDaysDau.DifferenceInNumber < 0)
                    //    {
                    //        plLast30DaysUniUsers.CssClass = "ca-changelabel ca-red";
                    //    }
                    //    else
                    //    {
                    //        plLast30DaysUniUsers.CssClass = "ca-changelabel ca-green";
                    //    }
                    //    LtlLast30DaysDiffNumber.Text = response.DetailDau.LastThirtyDaysDau.DifferenceInNumber.ToString();
                    //    LtlLast30DaysDiffPercentage.Text = response.DetailDau.LastThirtyDaysDau.DifferenceInPercentage.ToString("F2");
                    //    #endregion

                    //    ltlTotalUsers.Text = response.DetailDau.TotalCurrentUsers.ToString();
                    //    ltlIosUsers.Text = response.DetailDau.NumberOfIOS.ToString();
                    //    ltlIosUsersPercentage.Text = response.DetailDau.PercentageOfIOS.ToString("F0");
                    //    ltlAndroidUsers.Text = response.DetailDau.NumberOfAndroid.ToString();
                    //    ltlAndroidUsersPercentage.Text = response.DetailDau.PercentageOfAndroid.ToString("F0");

                    //    ltlDauChartTotalUser.Text = response.DauChart.TotalCurrentUsers.ToString();
                    //    ltlDauChartInactiveUser.Text = (response.DauChart.TotalCurrentUsers - response.DauChart.TotalUniqueActiveUsers).ToString();
                    //    ltlDauChartActiveUser.Text = response.DauChart.TotalUniqueActiveUsers.ToString();
                    //    ltlDauChartAttRate.Text = response.DauChart.AttendanceRatePercentage.ToString("F0");
                    //    ltlDauChartTotalTime.Text = response.DauChart.TotalHours.Replace(" ", "<br />");
                    //    ltlDauChartAveTime.Text = response.DauChart.AverageTimeSpentPerUser.ToString();

                    //    String dauUsersData = String.Empty, dauAveTimeSpentData = String.Empty, dauUsersTick = String.Empty, dauAveTimeSpentTick = String.Empty;
                    //    int highestActiveUser = 0, highestAveTimeSpent = 0;


                    //    for (int i = 0; i < response.DetailDau.DauColumns.Count; i++)
                    //    {
                    //        ClientScript.RegisterArrayDeclaration("dauUsersData", "new Array(gd(" + response.DetailDau.DauColumns[i].XAxisYear + ", " + response.DetailDau.DauColumns[i].XAxisMonth + ", " + response.DetailDau.DauColumns[i].XAxisDay + "), " + response.DetailDau.DauColumns[i].NumberOfActiveUsers + ")");
                    //        ClientScript.RegisterArrayDeclaration("dauAveTimeSpentData", "new Array(gd(" + response.DetailDau.DauColumns[i].XAxisYear + ", " + response.DetailDau.DauColumns[i].XAxisMonth + ", " + response.DetailDau.DauColumns[i].XAxisDay + "), " + response.DetailDau.DauColumns[i].AverageHourSpent + ")");

                    //        if (response.DetailDau.DauColumns[i].NumberOfActiveUsers > highestActiveUser)
                    //        {
                    //            highestActiveUser = response.DetailDau.DauColumns[i].NumberOfActiveUsers;
                    //        }

                    //        if (response.DetailDau.DauColumns[i].AverageHourSpent > highestAveTimeSpent)
                    //        {
                    //            highestAveTimeSpent = response.DetailDau.DauColumns[i].AverageHourSpent;
                    //        }
                    //    }

                    //    #region generate yaxes - ticks (Active User format)
                    //    /*
                    //     * yaxes - ticks (Active User format) :
                    //       [[200, '200'], [400, '400'], [600, '600'], [800, '800'], [1000, '1000 users']]
                    //     */

                    //    int increment = 0;
                    //    increment = (highestActiveUser / 4) + 1;
                    //    if (increment * 4 <= highestActiveUser)
                    //    {
                    //        increment++;
                    //    }

                    //    highestActiveUser = increment * 4;
                    //    for (int i = 0; i < highestActiveUser + 1; i += increment)
                    //    {
                    //        if (i != 0)
                    //        {
                    //            if (i == highestActiveUser)
                    //            {
                    //                ClientScript.RegisterArrayDeclaration("chartActiveUserY", "new Array(" + i + ", '" + i + " users')");
                    //            }
                    //            else
                    //            {
                    //                ClientScript.RegisterArrayDeclaration("chartActiveUserY", "new Array(" + i + ", '" + i + "')");
                    //            }
                    //        }
                    //    }
                    //    #endregion

                    //    #region generate yaxes - ticks (Average time spent format)
                    //    /*
                    //     * yaxes - ticks (Average time spent format) :
                    //       [[2, '2'], [4, '4'], [6, '6'], [8, '8'], [10, '10hrs']]
                    //     */

                    //    increment = 0;
                    //    increment = (highestAveTimeSpent / 4) + 1;
                    //    if (increment * 4 <= highestAveTimeSpent)
                    //    {
                    //        increment++;
                    //    }
                    //    highestAveTimeSpent = increment * 4;
                    //    for (int i = 0; i < highestAveTimeSpent + 1; i += increment)
                    //    {
                    //        if (i != 0)
                    //        {
                    //            if (i == highestAveTimeSpent)
                    //            {
                    //                ClientScript.RegisterArrayDeclaration("chartAveTimeY", "new Array(" + i + ", '" + i + "hrs')");
                    //            }
                    //            else
                    //            {
                    //                ClientScript.RegisterArrayDeclaration("chartAveTimeY", "new Array(" + i + ", '" + i + "')");
                    //            }
                    //        }
                    //    }

                    //    #endregion

                    //    dauUsersTick = "[[200, '200'], [400, '400'], [600, '600'], [800, '800'], [1000, '1000 users']]";
                    //    dauAveTimeSpentTick = "[[2, '2'], [4, '4'], [6, '6'], [8, '8'], [10, '10hrs']]";

                    //    String jsCommand = String.Format("drawDAUChart();setmobilegraph({0}, {1});", response.DetailDau.PercentageOfIOS.ToString("F2"), response.DetailDau.PercentageOfAndroid.ToString("F2"));
                    //    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "draw", jsCommand, true);

                    //}
                    //else
                    //{
                    //    Log.Error("AnalyticsSelectDetailDauResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    //}
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}