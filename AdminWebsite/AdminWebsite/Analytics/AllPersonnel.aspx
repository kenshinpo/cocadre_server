﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AllPersonnel.aspx.cs" Inherits="AdminWebsite.Analytics.AllPersonnel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta"><a href="/Analytics/MatchUpImprovement" id="root-link">MatchUp Improvement</a>></div>
        <div class="appbar__meta"><a href="" id="second-link" ></a>All Personnel</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="matchup-wrapper" class="data" >
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Analytics/MatchUpImprovement">MatchUp Improvement</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="feed_posts">Feed Posts</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="storage">Storage Utilization</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="relationship">Relationship</a>
                </li>--%>
            </ul>
        </aside>

        <div class="data__content">

            <div id="matchup_improvement" class="container" style="width:100%;">
                <div style="width:100%; margin:0 auto;">

                    <!-- TITLE -->
                    <div class="title-section" style="width:100%; margin: 0 auto;">
                        <div class="matchup-info">
                            <h1><img id="survey-icon" src="/Img/icon-quiz.png" /> MatchUp Improvement</h1>
                            <p class="align-left">Showing data from <span id="survey-start-date"></span> - <span id="survey-end-date"></span></p>
                        </div>
                        <div class="header-side">
                            <h2 style="text-align:right; padding-top:20px;">Last 30 days</h2>
                        </div>
                    </div>

                    <div class="search-section">
                        <div class="input-group" style="position:relative;">
                            <i class="fa fa-search" style="left:15px;"></i>
                            <input id="personnel" class="search-input" type="text" placeholder="Search Personnel" value="" style="margin:0px;"/>
                        </div>
                    </div>

                    <!-- ALL PERSONNEL TABLE -->
                    <div class="card full-row animated fadeInUp no-padding">
                        <div class="card-header" style="padding: 20px;">
                            <label class="table-title">Most Active Users on All Topics</label>
                        </div>

                        <div class="clearfix"></div>

                        <div class="table-content">
                            <table id="personnel-table" class="responder-table">
                                <thead>
                                    <th data-dynatable-column="rank" style="width:10%">Rank</th>
                                    <th data-dynatable-column="personnel" style="width:25%">Personnels</th>
                                    <th data-dynatable-column="department" style="width:15%">Department</th>
                                    <th data-dynatable-column="topic_engagement" data-dynatable-sorts="topic_engagement" style="width:25%">Topic Engagement</th>
                                    <th data-dynatable-column="accumulative_question_attempt" data-dynatable-sorts="accumulative_question_attempt" style="width:25%">Accumulative Question Attempts</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- PAGINATION -->
                    <nav class="align-center">
                        <ul class="pagination">
                            <li class="disabled">
                                <a href="#"><span><</span></a>
                            </li>
                            <li class="active">
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#"><span>></span></a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
        <script src="/js/vendor/moment-with-locales.js"></script>
        <script src="/Js/Chart.js"></script>
        <script>
            $(function () {
            
                var personnel_table = $('#personnel-table');
                var perPage = 2;
                var currentPage = 1;

                var personnel_table_data = [
                    {
                        "rank": "1",
                        "personnel":"Leong Guo Hao",
                        "department": 'Marketing',
                        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
                        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ( (55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
                    },
                    {
                        "rank": "1",
                        "personnel": "Leong Guo Hao",
                        "department": 'Marketing',
                        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
                        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
                    },
                    {
                        "rank": "1",
                        "personnel": "Leong Guo Hao",
                        "department": 'Marketing',
                        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
                        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
                    },
                    {
                        "rank": "1",
                        "personnel": "Leong Guo Hao",
                        "department": 'Marketing',
                        "topic_engagement": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color11" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li></ul>',
                        "accumulative_question_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage " >123</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((55 / 93) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" >23</span><div class="progress"><div id="incompleted-bar" class="progress-bar color3" style="width:' + ((64 / 123) * 100) + '%"></div></div></li></ul>'
                    }
                ];


                function generatePersonnelTable() {
                    personnel_table.bind('dynatable:afterUpdate', function (e, dynatable) {
                        setTimeout(function () {
                            personnel_table.css('display', 'table');
                            personnel_table.addClass('animated fadeIn');
                        }, 500);

                    });

                    personnel_table.bind('dynatable:init', function (e, dynatable) {

                        dynatable.queries.functions['personnel'] = function (record, queryValue) {
                            return record.personnel.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                        };
                       
                    });


                    personnel_table.dynatable({
                        features: {
                            paginate: false,
                            search: false,
                            sorting: true,
                            recordCount: false,
                            pushState: false,
                        },
                        dataset: {
                            records: personnel_table_data,
                            sorts: { 'no': 1 }
                        },
                        inputs: {
                            queries: $('#personnel')
                        }
                    });


                    var dynatable = personnel_table.data('dynatable');

                    if (typeof dynatable.records !== "undefined") {
                        dynatable.records.updateFromJson({ records: personnel_table_data });
                        dynatable.records.init();
                    }
                    dynatable.paginationPerPage.set(15);
                    dynatable.process();


                }


                generatePersonnelTable();
                //var dynatable = $('#department-table').data('dynatable');
                //dynatable.paginationPerPage.set(2);
                //dynatable.paginationPage.set(1);
                //dynatable.process();

            });
        </script>
</asp:Content>