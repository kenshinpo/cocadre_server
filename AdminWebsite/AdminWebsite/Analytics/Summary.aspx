﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Summary.aspx.cs" Inherits="AdminWebsite.Analytics.Summary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="../css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta">Summary</div>
        <div class="appbar__action">
        </div>
    </div>
    <!-- / App Bar -->

    <div id="matchup-wrapper" class="data">
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="MatchUpImprovement">MatchUp Improvement</a>
                </li>
                <%--
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="feed_posts">Feed Posts</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="storage">Storage Utilization</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="relationship">Relationship</a>
                </li>
                --%>
            </ul>
        </aside>

        <div class="data__content">


            <div class="chips ca-dashwidget">
                <div class="chips__container">
                    <div class="ca-widgethead">
                        <h2><strong>DAU</strong> Daily Active Users</h2>
                        <a class="btn secondary" href="DailyActiveUsers">Detailed report</a>
                    </div>

                    <div class="ca-widgetupdated ca-gray">
                        <asp:Literal ID="ltlReportDate" runat="server" />
                    </div>
                    <div class="ca-widgetdetails">

                        <div class="ca-widgetchart">
                            <h3>Unique User <span class="ca-gray">(Past 30 days)</span></h3>
                            <div id="ca-widget-dau" class="ca-chartdraw"></div>
                            <div id="ca-widget-daulegend" class="ca-legend"></div>
                            <div id="ca-widget-daunumbers" class="ca-widgetnumbers">
                                <h2>
                                    <asp:Literal ID="ltlActiveUserCount" runat="server" />
                                </h2>
                                <h4 class="ca-gray">
                                    <asp:Literal ID="ltlTotalUserCount" runat="server" />
                                </h4>
                            </div>
                        </div>

                        <asp:Panel ID="plConnected" runat="server" CssClass="ca-widgetchange">
                            <h3>Connected<br />
                                <span>(Past 60 days)</span>
                            </h3>

                            <i id="iLast60" runat="server" class="fa fa-arrow-circle-o-up ca-green"></i>
                            <p id="pLast60" runat="server" class="ca-green">
                                <em>
                                    <asp:Literal ID="ltlDiffNumber" runat="server" />
                                    (<asp:Literal ID="ltlDiffPercentage" runat="server" />%)</em>
                            </p>
                        </asp:Panel>
                    </div>
                </div>
            </div>

            <!---->
            <div class="chips ca-dashwidget">
                <div class="chips__container">
                    <div class="ca-widgethead">
                        <h2><strong>MatchUp</strong> Improvement</h2>
                        <a class="btn secondary" href="MatchUpImprovement">Detailed report</a>
                    </div>

                    <div class="ca-widgetupdated ca-gray">
                        Since beginning
                    </div>
                    <div class="ca-widgetdetails">

                        <div class="ca-widgetchart" style="width: 100%;">
                            <h3>Attempted question</h3>
                            <div id="ca-widget-matchup_improvement" class="ca-chartdraw"></div>
                            <div id="ca-widget-matchup_improvementlegend" class="ca-legend"></div>
                            <div id="ca-widget-matchup_improvementnumbers" class="ca-widgetnumbers">
                                <h2 id="matchup_improvement_total"></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')
    </script>
    <script src="../js/ca-script.js"></script>
    <script src="../js/vendor/flot/jquery.flot.js"></script>
    <script src="../js/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="../js/vendor/flot/jquery.flot.pie.js"></script>

    <script>
        function drawDAUChart(activeUser, totalUser) {
            var dauWidget = [
			{ color: '#ff9600', label: "Inactive users", data: totalUser - activeUser },
			{ color: '#0076ff', label: "Active users", data: activeUser }
            ];

            $.plot('#ca-widget-dau', dauWidget, {
                series: {
                    pie: {
                        innerRadius: 0.6,
                        show: true
                    }

                },
                legend: {
                    noColumns: 2,
                    container: $("#ca-widget-daulegend")
                }
            });
        }

        function drawMatchUpImprovementChart(secondCorrect, thirdCorrect, secondIncorrect, thirdIncorrect, totalAttempt) {
            var dauWidget = [
			    { color: '#44DB5E', label: "2nd attempt correct", data: secondCorrect, order:1, type:'correct' },
                { color: '#449FD5', label: "3rd & above correct", data: thirdCorrect, order: 2, type: 'correct' },
                { color: '#FF2851', label: "3rd & above wrong", data: thirdIncorrect, order: 4, type: 'wrong' },
                { color: '#FF9600', label: "2nd attempt wrong", data: secondIncorrect, order: 3, type: 'wrong' }
            ];

            $.plot('#ca-widget-matchup_improvement', dauWidget, {
                series: {
                    pie: {
                        innerRadius: 0.6,
                        show: true,
                        showRegionHighlight: true
                    }

                },
                legend: {
                    //show:false,
                    //labelFormatter: function (label, series) {
                    //    var arr = dauWidget;
                    //    arr.sort(function (a, b) {
                    //        return a.order - b.order;
                    //    });
                        
                    //    return '<div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid'+arr[]+';overflow:hidden"></div></div>'
                    //},
                    noColumns: 2,
                    container: $("#ca-widget-matchup_improvementlegend")
                }
            });

            var arr = dauWidget;
            arr.sort(function (a, b) {
                return a.order - b.order;
            });

            $('#ca-widget-matchup_improvementlegend .legendColorBox').each(function (index, item) {
                $(this).html('<div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid ' + arr[index].color+ ';overflow:hidden"></div></div>');
            });

            $('#ca-widget-matchup_improvementlegend .legendLabel').each(function (index, item) {
                $(this).html(arr[index].label);
            });

            $("#matchup_improvement_total").text(totalAttempt);
        }
    </script>

</asp:Content>
