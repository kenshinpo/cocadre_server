﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using System;
using System.Web.UI;

namespace AdminWebsite.Analytics
{
    public partial class DailyTopicActivities : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout", false);
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                String jsCommand = @"
                    var CompanyId = '" + managerInfo.CompanyId + @"';
                    var AdminUserId = '" + managerInfo.UserId + @"';
                    var Datestamp = '" + Page.RouteData.Values["Date"].ToString().Replace("-", " ") + @"';
                ";

                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "defineVar", jsCommand, true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}