﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="DailyTopicActivities.aspx.cs" Inherits="AdminWebsite.Analytics.DailyTopicActivities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>Analytics</strong> <span>console</span></div>
        <div class="appbar__meta"><a href="/Analytics/MatchUpImprovement" id="root-link">MatchUp Improvement</a></div>
        <div class="appbar__meta"><a href="" id="second-link" ></a>All Topics</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="matchup-wrapper" class="data" >
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/Summary">Summary</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="/Analytics/DailyActiveUsers">Daily Active User</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Analytics/MatchUpImprovement">MatchUp Improvement</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="feed_posts">Feed Posts</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="storage">Storage Utilization</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link" href="relationship">Relationship</a>
                </li>--%>
            </ul>
        </aside>

        <div class="data__content">

            <div id="matchup_improvement" class="container" style="width:100%;">
                <div style="width:100%; margin:0 auto;">

                    <!-- TITLE -->
                    <div class="title-section" style="width:100%; margin: 0 auto;">
                        <div class="matchup-info">
                            <h1>Daily Topic Activities</h1>
                        </div>
                        <div class="header-side">
                            <div class="mdl-selectfield" style="width:50%;float:right;margin-right: 0px; height:40px;">
                                
                                <select name="date-filter" id="date-filter" style="border-bottom:0px;">
	                                
                                </select>
                            </div> 
                        </div>
                    </div>

                     <div class="card full-row animated fadeInUp">
                        <div class="card-header">
                            <div class="filter-section">                  
                                <label class="select-label">Sort by</label>
                                <div class="mdl-selectfield" style="width:20%;float:left;margin-right: 20px; height:40px;">
                                    <select name="topic-filter" id="topic-filter">
                                        <option selected="selected" value="1">Alphabetical order</option>
	                                    <option value="2">Accumulated Attempts</option>
	                                    <option value="3">Unique Users</option>
                                        
                                    </select>
                                </div> 
                                <div class="attempts-legends text-right" style="float:right;">
                                    <ul class="inline-ul">
                                        <li><div class="indicator unique-indicator"></div> Unique User</li>
                                        <li><div class="indicator accumulated-indicator"></div> Accumulated Attempt</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>

                        <div class="card-content">
                            
                            <label class="msg" style="text-align:center;">Opps! There is no data on the selected date.</label>

                            <div class="card-content-left">
                                
                            </div>
                            <div class="card-content-right">
                                
                            </div>
                            <label style="margin-top:20px; clear:both; padding-top:20px; display:block;font-style:italic;">An unique user could've played more than one topic.</label>
                        </div>

                        <!-- PAGINATION -->
                        <%--<nav class="align-center">
                            <ul class="pagination">
                                <li class="disabled">
                                    <a href="#"><span><</span></a>
                                </li>
                                <li class="active">
                                    <a href="#">1</a>
                                </li>
                                <li>
                                    <a href="#">2</a>
                                </li>
                                <li>
                                    <a href="#"><span>></span></a>
                                </li>
                            </ul>
                        </nav>--%>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
        <script src="/js/vendor/moment-with-locales.js"></script>
        <script src="/Js/Chart.js"></script>
        <script>
            $(function () {

                var left_html = "";
                var right_html = "";

                function getUrlParameter(sParam) {
                    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

                    for (i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('=');

                        if (sParameterName[0] === sParam) {
                            return sParameterName[1] === undefined ? true : sParameterName[1];
                        }
                    }
                };

                //var CompanyId = 'C8b28091502514318bdcf48bec7c69129';
                //var AdminUserId = 'Udb0e704ee7244d21aea7930c27709fee';
                //var Datestamp = decodeURIComponent(getUrlParameter('date'));
                
                //GENERATE DATE 3 DAYS AGO
                for (var i = 0; i < 4; i++) {

                    if (i > 0) {
                        var date = new Date(Datestamp);
                        date.setDate(date.getDate() - i);
                        var dateString = date.getDate() + " " + moment(date).format('MMM') + " " + date.getFullYear();
                        $('select#date-filter').append($('<option></option>').val(dateString).html(dateString));
                    } else {
                        $('select#date-filter').append($('<option selected="selected"></option>').val(Datestamp).html(Datestamp));                      
                    }
                }

                // 3 DAYS AHEAD
                for (var i = 1; i < 4; i++) {
                    var date = new Date(Datestamp);
                    date.setDate(date.getDate() + i);
                    var dateString = date.getDate() + " " + moment(date).format('MMM') + " " + date.getFullYear();
                    $('select#date-filter').prepend($('<option></option>').val(dateString).html(dateString));
                }

                $('select#date-filter').on('change', function (e) {
                    $('.card-content-left').empty();
                    $('.card-content-right').empty();
                    left_html = "";
                    right_html = "";
                    var selected_date = $(this).val();
                    Datestamp = selected_date;
                    FetchData(1);
                });

                $('#topic-filter').on('change', function (e) {
                   
                    //var selected = $(this).val() + "-indicator";
                    $('.card-content-left').empty();
                    $('.card-content-right').empty();
                    left_html = "";
                    right_html = "";
                    FetchData($(this).val());

                    //if (selected == "alphabetical-indicator") {
                    //    $('.card-content .accumulated-indicator').css('display', 'block');
                    //    $('.card-content .unique-indicator').css('display', 'block');
                    //} else {
                    //    $('.card-content .accumulated-indicator').css('display', 'none');
                    //    $('.card-content .unique-indicator').css('display', 'none');
                    //    $('.card-content .' + selected).css('display', 'block');
                    //}                    
                });
              

                ///////FETCH DATA////////
                //1 - alphabetical sort
                //2 - accumulated sort
                //3 - unique sort
                function FetchData(sorting) {
                    $('#matchup_improvement').addClass('hidden');
                    $('#matchup_improvement').removeClass('animated fadeInUp');
                    ShowProgressBar();
                    $.ajax({
                        type: "POST",
                        url: '/Api/Analytics/SelectDailyTopicActivities',
                        data: {
                            "CompanyId": CompanyId,
                            "AdminUserId": AdminUserId,
                            "Datestamp": Datestamp
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            HideProgressBar();
                            if (res.Success) {
                                $('#matchup_improvement').removeClass('hidden');
                                $('#matchup_improvement').addClass('animated fadeInUp');
                                var topic = res.Topics;
                                var maxCount = res.MaxCount;

                                if (topic.length > 0) {

                                    $('.card-content .msg').css('display', 'none');

                                    //SORTING
                                    if (sorting == 1) {
                                        topic.sort(function (a, b) {
                                            if (a.Topic.TopicTitle == b.Topic.TopicTitle) { return 0; }
                                            if (a.Topic.TopicTitle > b.Topic.TopicTitle) {
                                                return 1;
                                            } else {
                                                return -1;
                                            }
                                        });
                                    } else if (sorting == 2) {
                                        topic.sort(function (a, b) {
                                            if (a.TotalAttempts == b.TotalAttempts) { return 0; }
                                            if (a.TotalAttempts > b.TotalAttempts) {
                                                return 1;
                                            } else {
                                                return -1;
                                            }
                                        });
                                        topic.reverse();
                                    } else if (sorting == 3) {
                                        topic.sort(function (a, b) {
                                            if (a.TotalUniqueUsers == b.TotalUniqueUsers) { return 0; }
                                            if (a.TotalUniqueUsers > b.TotalUniqueUsers) {
                                                return 1;
                                            } else {
                                                return -1;
                                            }
                                        });
                                        topic.reverse();
                                    }

                                    

                                    $.each(topic, function (key, value) {
                                        if (key % 2 == 0) {
                                            left_html += '<div class="activity-item">';
                                            left_html += '<div class="activity-label">';
                                            left_html += '<label>' + value.Topic.TopicTitle + '</label>';
                                            left_html += '</div>';
                                            left_html += '<div class="activity-progress">';
                                            left_html += '<div class="progress stacked" style="position:relative;overflow: visible;">';
                                            left_html += '<div class="progress-bar unique-indicator" style="width:' + (value.TotalUniqueUsers / maxCount) * 100 + '%;position:absolute; height:20px; left:0px; z-index:2;">';
                                            left_html += '<span class="value">' + value.TotalUniqueUsers + '</span>';
                                            left_html += '</div>';
                                            left_html += '<div class="progress-bar accumulated-indicator" style="width:' + (value.TotalAttempts / maxCount) * 100 + '%;position:absolute; height:20px; left:0px; z-index:1;top:20px;">';
                                            left_html += ' <span class="align-value">' + value.TotalAttempts + '</span>';
                                            left_html += '</div>';
                                            left_html += '</div>';
                                            left_html += '</div>';
                                            left_html += '</div>';
                                            left_html += '<div class="clearfix"></div>';
                                        } else {
                                            right_html += '<div class="activity-item">';
                                            right_html += '<div class="activity-label">';
                                            right_html += '<label>' + value.Topic.TopicTitle + '</label>';
                                            right_html += '</div>';
                                            right_html += '<div class="activity-progress">';
                                            right_html += '<div class="progress stacked" style="position:relative;overflow: visible;">';
                                            right_html += '<div class="progress-bar unique-indicator" style="width:' + (value.TotalUniqueUsers / maxCount) * 100 + '%; position:absolute; height:20px; left:0px; z-index:2;">';
                                            right_html += '<span class="value">' + value.TotalUniqueUsers + '</span>';
                                            right_html += '</div>';
                                            right_html += '<div class="progress-bar accumulated-indicator" style="width:' + (value.TotalAttempts / maxCount) * 100 + '%;position:absolute; height:20px; left:0px; z-index:1;top:20px;">';
                                            right_html += ' <span class="align-value">' + value.TotalAttempts + '</span>';
                                            right_html += '</div>';
                                            right_html += '</div>';
                                            right_html += '</div>';
                                            right_html += '</div>';
                                            right_html += '<div class="clearfix"></div>';
                                        }
                                    });

                                    $('.card-content-left').append(left_html);
                                    $('.card-content-right').append(right_html);

                                } else {
                                    $('.card-content .msg').css('display', 'block');
                                }
                                

                                

                            }
                        }
                    });
                }

                FetchData(1);

                //for (var i = 0; i < 20; i++) {
                //    if (i % 2 == 0) {
                //        left_html += '<div class="activity-item">';
                //        left_html += '<div class="activity-label">';
                //        left_html += '<label>Code of Conduct</label>';
                //        left_html += '</div>';
                //        left_html += '<div class="activity-progress">';
                //        left_html += '<div class="progress stacked">';
                //        left_html += '<div class="progress-bar unique-indicator" style="width:43%;">';
                //        left_html += '<span class="value">12</span>';
                //        left_html += '</div>';
                //        left_html += '<div class="progress-bar accumulated-indicator" style="width:43%;">';
                //        left_html += ' <span class="align-value">12</span>';
                //        left_html += '</div>';
                //        left_html += '</div>';
                //        left_html += '</div>';
                //        left_html += '</div>';
                //        left_html += '<div class="clearfix"></div>';
                //    } else {
                //        right_html += '<div class="activity-item">';
                //        right_html += '<div class="activity-label">';
                //        right_html += '<label>Code of Conduct</label>';
                //        right_html += '</div>';
                //        right_html += '<div class="activity-progress">';
                //        right_html += '<div class="progress stacked">';
                //        right_html += '<div class="progress-bar unique-indicator" style="width:43%;">';
                //        right_html += '<span class="value">12</span>';
                //        right_html += '</div>';
                //        right_html += '<div class="progress-bar accumulated-indicator" style="width:43%;">';
                //        right_html += ' <span class="align-value">12</span>';
                //        right_html += '</div>';
                //        right_html += '</div>';
                //        right_html += '</div>';
                //        right_html += '</div>';
                //        right_html += '<div class="clearfix"></div>';
                //    }
                //}
                //$('.card-content-left').append(left_html);
                //$('.card-content-right').append(right_html);
            });
        </script>
</asp:Content>
