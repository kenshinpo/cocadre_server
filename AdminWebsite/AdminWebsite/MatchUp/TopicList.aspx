﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="TopicList.aspx.cs" Inherits="AdminWebsite.MatchUp.TopicList" %>

<asp:Content ID="cttMain" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfTopicId" runat="server" />
            <asp:HiddenField ID="hfTopicName" runat="server" />
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/MatchUp/TopicCreate" class="mfb-component__button--main" data-mfb-label="Add Topics">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Delete Topic -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Topic
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgActionTopic" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionTopicTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbAction_Click" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Delete Topic -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Match Up <span>console</span></div>
        <div class="appbar__meta">Topics</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Topic</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MatchUp/Category">Category</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Topic</label>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Type the topic's title" MaxLength="30" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" runat="server" OnClick="ibFilterTopic_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                    <div class="pad">
                        <label>By Department</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterDepartment" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" onchange="ShowProgressBar('Filtering');" />
                        </div>
                    </div>
                    <div class="pad">
                        <label>By Category</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" onchange="ShowProgressBar('Filtering');" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upTopicList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvTopic" runat="server" OnItemDataBound="lvTopic_ItemDataBound" OnItemCommand="lvTopic_ItemCommand" OnItemCreated="lvTopic_ItemCreated" OnDataBound="lvTopic_DataBound">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 40px;"></th>
                                        <th>Topic</th>
                                        <th>Category</th>
                                        <th>Questions</th>
                                        <th>Status</th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <div class="constrained">
                                        <asp:LinkButton ID="lbIcon" ClientIDMode="AutoID" runat="server" CommandName="UpdateTopic" Style="color: #999;">
                                            <asp:Image ID="imgTpoic" runat="server" />
                                        </asp:LinkButton>
                                    </div>
                                </td>

                                <td>
                                    <asp:LinkButton ID="lbTopicName" ClientIDMode="AutoID" runat="server" CommandName="UpdateTopic" Style="color: #999;" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbCategoryName" ClientIDMode="AutoID" runat="server" CommandName="UpdateTopic" Style="color: #999;" />
                                    <%--<asp:Label ID="lblCategoryName" runat="server" />--%>
                                    <asp:Label ID="lblCategoryId" runat="server" Visible="false" />
                                </td>
                                <td>
                                     <asp:LinkButton ID="lbQuestionCount" ClientIDMode="AutoID" runat="server" CommandName="UpdateTopic" Style="color: #999;" />
                                    <%--<asp:Label ID="lblQuestionNo" runat="server" />--%>

                                </td>
                                <td>
                                    <asp:LinkButton ID="lbStatus" ClientIDMode="AutoID" runat="server" CommandName="UpdateTopic" Style="color: #999;" />
                                    <%--<asp:Label ID="lblTopicStatus" runat="server" />--%>
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="UpdateTopic" Visible="false" Text="Edit Topic" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbActive" ClientIDMode="AutoID" runat="server" CommandName="ActiveTopic" Visible="false" Text="Activate Topic" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbHide" ClientIDMode="AutoID" runat="server" CommandName="HideTopic" Visible="false" Text="Hide Topic" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="DeleteTopic" Visible="false" Text="Delete Topic" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
