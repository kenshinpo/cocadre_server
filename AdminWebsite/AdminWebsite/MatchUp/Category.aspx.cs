﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.MatchUp
{
    public partial class Category : Page
    {
        private ManagerInfo adminInfor;
        private AdminService asc = new AdminService();
        private List<TopicCategory> categoryList;

        public void GetCategoryList()
        {
            try
            {
                CategorySelectAllResponse response = asc.SelectAllCategories(adminInfor.UserId, adminInfor.CompanyId);
                if (response.Success)
                {
                    categoryList = response.TopicCategories;
                    categoryList.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
                    if (!Convert.ToBoolean(ViewState["IsSortByNameAcsend"]))
                    {
                        categoryList.Reverse();
                    }
                }
                else
                {
                    Log.Error("CategorySelectAllResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfor = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    ViewState["IsSortByNameAcsend"] = true;
                    GetCategoryList();
                    lvCategory.DataSource = categoryList;
                    lvCategory.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvCategory_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (categoryList != null && categoryList.Count > 0)
                {
                    if (e.Item.ItemType == ListViewItemType.DataItem)
                    {
                        LinkButton lbName = e.Item.FindControl("lbName") as LinkButton;
                        HyperLink hlNoOfTopics = e.Item.FindControl("hlNoOfTopics") as HyperLink;
                        LinkButton lbRename = e.Item.FindControl("lbRename") as LinkButton;
                        LinkButton lbDelete = e.Item.FindControl("lbDelete") as LinkButton;

                        lbName.Text = categoryList[e.Item.DataItemIndex].Title;
                        hlNoOfTopics.Text = Convert.ToString(categoryList[e.Item.DataItemIndex].TotalNumberOfTopics);
                        hlNoOfTopics.NavigateUrl = "/MatchUp/TopicList/" + categoryList[e.Item.DataItemIndex].Id;
                        lbName.CommandArgument = categoryList[e.Item.DataItemIndex].Id;
                        lbRename.CommandArgument = categoryList[e.Item.DataItemIndex].Id;
                        lbDelete.CommandArgument = categoryList[e.Item.DataItemIndex].Id;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbOpenAddCategory_Click(object sender, EventArgs e)
        {
            try
            {
                tbAddName.Text = String.Empty;
                mpePop.PopupControlID = "popup_addcategory";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAddDone_Click(object sender, EventArgs e)
        {

            try
            {
                CategoryCreateResponse response = asc.CreateCategory(adminInfor.UserId, adminInfor.CompanyId, tbAddName.Text);
                if (response.Success)
                {
                    mpePop.Hide();
                    GetCategoryList();
                    lvCategory.DataSource = categoryList;
                    lvCategory.DataBind();
                    MessageUtility.ShowToast(this.Page, tbAddName.Text + " has been created.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvCategory_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Rename") || e.CommandName.Equals("Remove"))
                {
                    CategorySelectResponse response = asc.GetCategoryDetail(adminInfor.UserId, adminInfor.CompanyId, e.CommandArgument.ToString());
                    if (response.Success)
                    {
                        hfCategoryId.Value = response.TopicCategory.Id;
                        hfCategoryTitle.Value = response.TopicCategory.Title;

                        if (e.CommandName.Equals("Rename"))
                        {
                            tbRenameName.Text = response.TopicCategory.Title;
                            mpePop.PopupControlID = "popup_renamecategory";
                            mpePop.Show();
                        }
                        else if (e.CommandName.Equals("Remove"))
                        {
                            if (response.TopicCategory.TotalNumberOfTopics == 0)
                            {
                                ltlDeleteMsg.Text = "<p>Are you sure you want to delete this category : <b>" + response.TopicCategory.Title + "</b> ?</p>";
                                lbDelete.Visible = true;
                                lbDelCancel.Text = "Cancel";
                            }
                            else
                            {
                                ltlDeleteMsg.Text = @"<p class='error'>There are still topic(s) left in the category!<br /> For safety reasons, only empty categories can be deleted.</p>
                           <p><img src='/img/tips_icon.png' width='16px' height='20px' /> You can move the topics to other categories in the <a href='/MatchUp/TopicList/" + response.TopicCategory.Id + "'>topic console</a>.</p>";
                                lbDelete.Visible = false;
                                lbDelCancel.Text = "Exit";
                            }
                            mpePop.PopupControlID = "popup_deletecategory";
                            mpePop.Show();
                        }
                        else
                        {
                            Log.Info("There is no this command name in the lvCategory.");
                        }
                    }
                    else
                    {
                        Log.Error("CategorySelectResponse.Success is false. ErrorMessage : " + response.ErrorMessage, this.Page);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbRenameSave_Click(object sender, EventArgs e)
        {
            try
            {
                CategoryUpdateResponse response = asc.UpdateCategory(adminInfor.UserId, adminInfor.CompanyId, hfCategoryId.Value, tbRenameName.Text);
                if (response.Success)
                {
                    mpePop.Hide();
                    GetCategoryList();
                    lvCategory.DataSource = categoryList;
                    lvCategory.DataBind();
                    MessageUtility.ShowToast(this.Page, hfCategoryTitle.Value + " has been updated to " + tbRenameName.Text, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, response.ErrorMessage.Replace("name", "name " + tbRenameName.Text.Trim()), MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CategoryDeleteResponse response = asc.DeleteCategory(adminInfor.UserId, adminInfor.CompanyId, hfCategoryId.Value);
                if (response.Success)
                {
                    mpePop.Hide();
                    GetCategoryList();
                    lvCategory.DataSource = categoryList;
                    lvCategory.DataBind();
                    MessageUtility.ShowToast(this.Page, hfCategoryTitle.Value + " has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, "Delete category failed.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSortByName_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean isSortByNameAcsend = Convert.ToBoolean(ViewState["IsSortByNameAcsend"]);
                ViewState["IsSortByNameAcsend"] = !isSortByNameAcsend;
                GetCategoryList();
                lvCategory.DataSource = categoryList;
                lvCategory.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvCategory_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (categoryList == null || categoryList.Count == 0)
                {
                    if (e.Item.ItemType == ListViewItemType.EmptyItem)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        ltlEmptyMsg.Text = @"Category is empty.<br /><img src='/img/tips_icon.png' width='16' height='20' /> You can add category with the add button at the bottom right corner of this page.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}