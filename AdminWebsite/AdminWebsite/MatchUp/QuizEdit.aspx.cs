﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.ServiceRequests;
using AdminWebsite.App_Code.ServiceResponses;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using AjaxControlToolkit;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;
using Newtonsoft.Json;

namespace AdminWebsite.MatchUp
{
    public partial class QuizEdit : System.Web.UI.Page
    {
        private static ILog Log = LogManager.GetLogger("AdminWebsiteLog");

        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();

        private const int QUESTION_TYPE_1 = 1;
        private const int QUESTION_TYPE_2 = 2;
        private const int QUESTION_TYPE_3 = 3;

        private List<TopicCategory> categoryList = null;
        private List<Department> departmentList = null;
        private List<Topic.DropdownNumberOfQuestions> noQuestionList = null;
        private List<String> listIconUrl = null;
        private List<ChallengeQuestion> questions = null;
        private Topic topic = null;

        private int countOfType1Question = 140;
        private int countOfType1Answer = 66;
        private int countOfType2Question = 391;
        private int countOfType2Answer = 36;
        private int countOfType3Question = 69;
        private int countOfType3Answer = 36;

        public void GetCategoryList()
        {
            try
            {
                if (categoryList == null)
                {
                    CategorySelectAllResponse response = asc.SelectAllCategories(ViewState["admin_user_id"].ToString(), ViewState["company_id"].ToString());
                    if (response.Success)
                    {
                        categoryList = response.TopicCategories;
                        categoryList = categoryList.OrderBy(o => o.Title).ToList();
                    }
                    else
                    {
                        Log.Error("SelectAllCategories request is failed. result error msg: " + response.ErrorMessage);
                        categoryList = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                categoryList = null;
            }
        }

        public void GetDepartmentList()
        {
            try
            {
                if (departmentList == null)
                {
                    DepartmentListResponse response = asc.GetAllDepartment(ViewState["admin_user_id"].ToString(), ViewState["company_id"].ToString());
                    if (response.Success)
                    {
                        List<Department> list = response.Departments;
                        list.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
                        departmentList = list;
                    }
                    else
                    {
                        Log.Error("GetAllDepartment.Success is false. ErrorMessage: " + response.ErrorMessage);
                        departmentList = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                departmentList = null;
            }
        }

        public void GetNoQuestionPerGame()
        {
            try
            {
                if (noQuestionList == null)
                {
                    noQuestionList = asc.SelectNumberOfQuestionsForDropdown();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                departmentList = null;
            }
        }
        public void GetTopicDetail()
        {
            //if (topic == null)
            //{
            String keyWord = tbSearchQuestion.Text.Trim();

            TopicSelectResponse response = asc.SelectTopicDetail(ViewState["topic_id"].ToString(), ViewState["admin_user_id"].ToString(), ViewState["company_id"].ToString(), keyWord);
            if (response.Success)
            {
                topic = response.Topic;
                questions = response.Topic.Questions;
                questions = questions.OrderBy(o => o.CreatedOnTimestamp).ToList();
            }
            else
            {
                Log.Error("TopicSelectResponse.Success is false. Error message: " + response.ErrorMessage);
            }
            //}
        }

        public void GetIconData()
        {
            if (listIconUrl == null)
            {
                TopicSelectIconResponse response = asc.SelectAllTopicIcons(ViewState["admin_user_id"].ToString(), ViewState["company_id"].ToString());
                if (response.Success)
                {
                    listIconUrl = response.TopicIconUrls;
                }
            }
        }

        public void ChangeProgressBar()
        {
            if (questions == null)
            {
                GetTopicDetail();
            }

            int countActive = 0;
            for (int i = 0; i < questions.Count; i++)
            {
                if (questions[i].Status == 1)
                {
                    countActive++;
                }
            }

            hfTopicQuestionCount.Value = Convert.ToString(countActive);
            plProgress.Visible = true;
            ltlRecommendedQns.Visible = true;

            if (countActive == 0)
            {
                plProgress.CssClass = "topic__info__progress__bar progress-bar-indication";
                lblProgress.Attributes.Add("style", "width: 0%");
                ltlProgress.Text = "0 out of minimum " + ddlNoQuestionPerGame.SelectedValue + " questions";
            }
            else
            {
                if (countActive < Convert.ToInt16(ddlNoQuestionPerGame.SelectedValue))
                {
                    plProgress.CssClass = "topic__info__progress__bar progress-bar-indication";
                    int progress = (countActive / Convert.ToInt16(ddlNoQuestionPerGame.SelectedValue)) * 100;
                    lblProgress.Attributes.Add("style", "width: " + progress + "%");
                    ltlProgress.Text = countActive + " out of minimum " + Convert.ToInt16(ddlNoQuestionPerGame.SelectedValue) + " questions";
                }
                else if (countActive < 100)
                {
                    plProgress.CssClass = "topic__info__progress__bar progress-bar-indication progress-bar-indication--recommended";
                    lblProgress.Attributes.Add("style", "width: " + countActive + "%");
                    ltlProgress.Text = "Recommended " + countActive + "/100";
                }
                else
                {
                    plProgress.Visible = false;
                    ltlRecommendedQns.Visible = false;
                }
            }
            ltlRecommendedQns.Text = " Your topic must consist of at least " + ddlNoQuestionPerGame.SelectedValue + " questions";
            upProgress.Update();
        }

        public void SetNewQuestionTemplate(int questionType)
        {
            ViewState["QuestionType"] = questionType;
            plAction01.CssClass = "tabs__list__item";
            plAction02.CssClass = "tabs__list__item";
            plAction03.CssClass = "tabs__list__item";

            tbQuestion.BorderColor = System.Drawing.Color.Empty;
            tbAnswer1.BorderColor = System.Drawing.Color.Empty;
            tbAnswer2.BorderColor = System.Drawing.Color.Empty;
            tbAnswer3.BorderColor = System.Drawing.Color.Empty;
            tbAnswer4.BorderColor = System.Drawing.Color.Empty;

            int countOfQuestion = 0;
            int countOfAnswer = 0;
            switch (questionType)
            {
                case 1:
                    plAction01.CssClass = "tabs__list__item active";
                    plFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat01";
                    countOfQuestion = countOfType1Question;
                    countOfAnswer = countOfType1Answer;
                    plImg.Visible = false;
                    break;
                case 2:
                    plAction02.CssClass = "tabs__list__item active";
                    plFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat02";
                    countOfQuestion = countOfType2Question;
                    countOfAnswer = countOfType2Answer;
                    plImg.Visible = false;
                    break;
                case 3:
                    plAction03.CssClass = "tabs__list__item active";
                    plFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat03";
                    countOfQuestion = countOfType3Question;
                    countOfAnswer = countOfType3Answer;
                    plImg.Visible = true;
                    break;
                default:
                    break;
            }

            lblQuestionCount.Text = Convert.ToString(countOfQuestion - tbQuestion.Text.Length);
            if (Convert.ToInt16(lblQuestionCount.Text) < 0)
            {
                tbQuestion.BorderColor = System.Drawing.Color.Red;
            }

            lblAnswer1Count.Text = Convert.ToString(countOfAnswer - tbAnswer1.Text.Length);
            if (Convert.ToInt16(lblAnswer1Count.Text) < 0)
            {
                tbAnswer1.BorderColor = System.Drawing.Color.Red;
            }

            lblAnswer2Count.Text = Convert.ToString(countOfAnswer - tbAnswer2.Text.Length);
            if (Convert.ToInt16(lblAnswer2Count.Text) < 0)
            {
                tbAnswer2.BorderColor = System.Drawing.Color.Red;
            }

            lblAnswer3Count.Text = Convert.ToString(countOfAnswer - tbAnswer3.Text.Length);
            if (Convert.ToInt16(lblAnswer3Count.Text) < 0)
            {
                tbAnswer3.BorderColor = System.Drawing.Color.Red;
            }

            lblAnswer4Count.Text = Convert.ToString(countOfAnswer - tbAnswer4.Text.Length);
            if (Convert.ToInt16(lblAnswer4Count.Text) < 0)
            {
                tbAnswer4.BorderColor = System.Drawing.Color.Red;
            }
            SetNewQuestionTextCounter(questionType);
            String jsCommand = String.Format("NewQuestionPreviewChangeType({0},'{1}','{2}','{3}','{4}','{5}');", questionType, tbQuestion.ClientID, tbAnswer1.ClientID, tbAnswer2.ClientID, tbAnswer3.ClientID, tbAnswer4.ClientID);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "NewQuestionPreviewChangeType", jsCommand, true);
        }

        public void SetNewQuestionTextCounter(int questionType)
        {
            int countOfQuestion = 0;
            int countOfAnswer = 0;
            switch (questionType)
            {
                case QUESTION_TYPE_1:
                    countOfQuestion = countOfType1Question;
                    countOfAnswer = countOfType1Answer;
                    break;
                case QUESTION_TYPE_2:
                    countOfQuestion = countOfType2Question;
                    countOfAnswer = countOfType2Answer;
                    break;
                case QUESTION_TYPE_3:
                    countOfQuestion = countOfType3Question;
                    countOfAnswer = countOfType3Answer;
                    break;
                default:
                    break;
            }
            tbQuestion.Attributes["onkeyup"] = String.Format("NewQuestionTextCounter('{0}','{1}','{2}', 1, 'new_question_recommend_reading_time'); NewQuestionPreview('{3}',0);", tbQuestion.ClientID, lblQuestionCount.ClientID, countOfQuestion, tbQuestion.ClientID);
            tbAnswer1.Attributes["onkeyup"] = String.Format("NewQuestionTextCounter('{0}','{1}','{2}', 2, 'new_question_recommend_answering_time'); NewQuestionPreview('{3}',1);", tbAnswer1.ClientID, lblAnswer1Count.ClientID, countOfAnswer, tbAnswer1.ClientID);
            tbAnswer2.Attributes["onkeyup"] = String.Format("NewQuestionTextCounter('{0}','{1}','{2}', 2, 'new_question_recommend_answering_time'); NewQuestionPreview('{3}',2);", tbAnswer2.ClientID, lblAnswer2Count.ClientID, countOfAnswer, tbAnswer2.ClientID);
            tbAnswer3.Attributes["onkeyup"] = String.Format("NewQuestionTextCounter('{0}','{1}','{2}', 2, 'new_question_recommend_answering_time'); NewQuestionPreview('{3}',3);", tbAnswer3.ClientID, lblAnswer3Count.ClientID, countOfAnswer, tbAnswer3.ClientID);
            tbAnswer4.Attributes["onkeyup"] = String.Format("NewQuestionTextCounter('{0}','{1}','{2}', 2, 'new_question_recommend_answering_time'); NewQuestionPreview('{3}',4);", tbAnswer4.ClientID, lblAnswer4Count.ClientID, countOfAnswer, tbAnswer4.ClientID);

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "NewQuestionRecommendedReadingTime", String.Format("NewQuestionTextCounter('{0}','{1}','{2}', 1, 'new_question_recommend_reading_time'); NewQuestionPreview('{3}',0);", tbQuestion.ClientID, lblQuestionCount.ClientID, countOfQuestion, tbQuestion.ClientID), true);

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "NewQuestionRecommendedAnsweringTime", String.Format("NewQuestionTextCounter('{0}','{1}','{2}', 2, 'new_question_recommend_answering_time'); NewQuestionPreview('{3}',1);", tbAnswer1.ClientID, lblAnswer1Count.ClientID, countOfAnswer, tbAnswer1.ClientID), true);
        }

        public void SetEditQuestionTextCounter(int questionType, TextBox tbQuestion, TextBox tbAnswer1, TextBox tbAnswer2, TextBox tbAnswer3, TextBox tbAnswer4, Label lblQuestionCount, Label lblAnswer1Count, Label lblAnswer2Count, Label lblAnswer3Count, Label lblAnswer4Count, Label lblPreviewQuestion, Label lblPreviewAnswer1, Label lblPreviewAnswer2, Label lblPreviewAnswer3, Label lblPreviewAnswer4, Label lblEditQuestionRecommendedReadingTime, Label lblEditQuestionRecommendedAnsweringTime)
        {
            int countOfQuestion = 0;
            int countOfAnswer = 0;
            switch (questionType)
            {
                case QUESTION_TYPE_1:
                    countOfQuestion = countOfType1Question;
                    countOfAnswer = countOfType1Answer;
                    break;
                case QUESTION_TYPE_2:
                    countOfQuestion = countOfType2Question;
                    countOfAnswer = countOfType2Answer;
                    break;
                case QUESTION_TYPE_3:
                    countOfQuestion = countOfType3Question;
                    countOfAnswer = countOfType3Answer;
                    break;
                default:
                    break;
            }
            tbQuestion.Attributes["onkeyup"] = String.Format("EditQuestionTextCounter('{0}','{1}','{2}', 1, '{5}'); EditQuestionPreview('{3}','{4}');", tbQuestion.ClientID, lblQuestionCount.ClientID, countOfQuestion, tbQuestion.ClientID, lblPreviewQuestion.ClientID, lblEditQuestionRecommendedReadingTime.ClientID);
            tbAnswer1.Attributes["onkeyup"] = String.Format("EditQuestionTextCounter('{0}','{1}','{2}', 2, '{5}'); EditQuestionPreview('{3}','{4}');", tbAnswer1.ClientID, lblAnswer1Count.ClientID, countOfAnswer, tbAnswer1.ClientID, lblPreviewAnswer1.ClientID, lblEditQuestionRecommendedAnsweringTime.ClientID);
            tbAnswer2.Attributes["onkeyup"] = String.Format("EditQuestionTextCounter('{0}','{1}','{2}', 2, '{5}'); EditQuestionPreview('{3}','{4}');", tbAnswer2.ClientID, lblAnswer2Count.ClientID, countOfAnswer, tbAnswer2.ClientID, lblPreviewAnswer2.ClientID, lblEditQuestionRecommendedAnsweringTime.ClientID);
            tbAnswer3.Attributes["onkeyup"] = String.Format("EditQuestionTextCounter('{0}','{1}','{2}', 2, '{5}'); EditQuestionPreview('{3}','{4}');", tbAnswer3.ClientID, lblAnswer3Count.ClientID, countOfAnswer, tbAnswer3.ClientID, lblPreviewAnswer3.ClientID, lblEditQuestionRecommendedAnsweringTime.ClientID);
            tbAnswer4.Attributes["onkeyup"] = String.Format("EditQuestionTextCounter('{0}','{1}','{2}', 2, '{5}'); EditQuestionPreview('{3}','{4}');", tbAnswer4.ClientID, lblAnswer4Count.ClientID, countOfAnswer, tbAnswer4.ClientID, lblPreviewAnswer4.ClientID, lblEditQuestionRecommendedAnsweringTime.ClientID);
        }
        public void SetEditQuestionTemplate(int questionType, Panel plEditAction01, Panel plEditAction02, Panel plEditAction03)
        {
            switch (questionType)
            {
                case 1:
                    plEditAction01.CssClass = "tabs__list__item active";
                    break;
                case 2:
                    plEditAction02.CssClass = "tabs__list__item active";
                    break;
                case 3:
                    plEditAction03.CssClass = "tabs__list__item active";
                    break;
                default:
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["admin_info"] == null)
                {
                    Response.Redirect("/Logout", false);
                    return;
                }
                managerInfo = Session["admin_info"] as ManagerInfo;

                if (!IsPostBack)
                {
                    if (Page.RouteData.Values["TopicId"] == null)
                    {
                        Response.Redirect("TopicList", false);
                    }
                    else
                    {
                        ViewState["topic_id"] = Encryption.DecryptByDES(Page.RouteData.Values["TopicId"].ToString(), WebConfigurationManager.AppSettings["DES_KEY"].ToString(), WebConfigurationManager.AppSettings["DES_IV"].ToString());
                        ViewState["admin_user_id"] = managerInfo.UserId;
                        ViewState["company_id"] = managerInfo.CompanyId;

                        hfManagerId.Value = managerInfo.UserId;
                        hfCompanyId.Value = managerInfo.CompanyId;
                        hfTopicId.Value = ViewState["topic_id"].ToString();
                        

                        Page.Form.Attributes.Add("enctype", "multipart/form-data");

                        GetTopicDetail();
                        tbTopictitle.Text = topic.TopicTitle;
                        tbTopicDescription.Text = topic.TopicDescription;
                        imgTopic.ImageUrl = topic.TopicLogoUrl;

                        #region Get category.
                        cbCategory.Items.Clear();
                        GetCategoryList();
                        if (categoryList != null)
                        {
                            for (int i = 0; i < categoryList.Count; i++)
                            {
                                cbCategory.Items.Add(new ListItem(categoryList[i].Title, Convert.ToString(categoryList[i].Id)));
                                if (topic.TopicCategory.Id == categoryList[i].Id)
                                {
                                    cbCategory.Items[i].Selected = true;
                                    hfCategoryId.Value = topic.TopicCategory.Id;
                                }
                            }
                        }
                        #endregion

                        #region Get Privacy.
                        if (topic.IsForEveryone)
                        {
                            rbPrivacyEveryone.Checked = true;
                            cbCheckAllDept.Checked = false;
                            cbCheckAllDept.Enabled = false;
                            cblDepartment.ClearSelection();
                            cblDepartment.Enabled = false;

                            cblDepartment.Items.Clear();
                            GetDepartmentList();
                            if (departmentList != null)
                            {
                                for (int i = 0; i < departmentList.Count; i++)
                                {
                                    cblDepartment.Items.Add(new ListItem(departmentList[i].Title, Convert.ToString(departmentList[i].Id)));
                                }
                            }
                        }
                        else
                        {
                            rbPrivacyDepartment.Checked = true;
                            cblDepartment.Items.Clear();
                            GetDepartmentList();
                            if (departmentList != null)
                            {
                                for (int i = 0; i < departmentList.Count; i++)
                                {
                                    cblDepartment.Items.Add(new ListItem(departmentList[i].Title, Convert.ToString(departmentList[i].Id)));
                                    for (int j = 0; j < topic.TargetedDepartments.Count; j++)
                                    {
                                        if (departmentList[i].Id == topic.TargetedDepartments[j].Id && topic.TargetedDepartments[j].IsTargetedForTopic)
                                        {
                                            cblDepartment.Items[i].Selected = true;
                                        }
                                    }
                                }
                            }

                            Boolean isSelectAllDept = true;
                            for (int i = 0; i < cblDepartment.Items.Count; i++)
                            {
                                if (!cblDepartment.Items[i].Selected)
                                {
                                    isSelectAllDept = false;
                                    break;
                                }
                            }
                            cbCheckAllDept.Checked = isSelectAllDept;
                        }
                        #endregion

                        #region Get No of questions per game.
                        ddlNoQuestionPerGame.Items.Clear();
                        GetNoQuestionPerGame();
                        if (noQuestionList != null)
                        {
                            for (int i = 0; i < noQuestionList.Count; i++)
                            {
                                ddlNoQuestionPerGame.Items.Add(new ListItem(noQuestionList[i].DisplayText, Convert.ToString(noQuestionList[i].Value)));
                                if (noQuestionList[i].Value == topic.SelectedNumberOfQuestions)
                                {
                                    ddlNoQuestionPerGame.Items[i].Selected = true;
                                }
                            }
                        }
                        #endregion

                        #region Icon data
                        GetIconData();
                        rtIcon.DataSource = listIconUrl;
                        rtIcon.DataBind();
                        #endregion

                        #region Topic status
                        ddlTopicStatus.Items.Clear();
                        hfTopicStatus.Value = Convert.ToString(topic.Status.Code);
                        switch (topic.Status.Code)
                        {
                            case 1: // UNLISTED
                                {
                                    ddlTopicStatus.Items.Add(new ListItem("Unlisted", "1"));
                                    ddlTopicStatus.Items.Add(new ListItem("Active", "2"));
                                    ddlTopicStatus.Items[0].Selected = true;
                                }
                                break;

                            case 2: // ACTIVE
                                {
                                    ddlTopicStatus.Items.Add(new ListItem("Active", "2"));
                                    ddlTopicStatus.Items.Add(new ListItem("Hide", "3"));
                                    ddlTopicStatus.Items[0].Selected = true;
                                }
                                break;

                            case 3: // HIDDEN
                                {
                                    ddlTopicStatus.Items.Add(new ListItem("Active", "2"));
                                    ddlTopicStatus.Items.Add(new ListItem("Hide", "3"));
                                    ddlTopicStatus.Items[1].Selected = true;
                                }
                                break;

                            default:
                                {
                                    ddlTopicStatus.Items.Add(new ListItem("Deleted", "-1"));
                                    ddlTopicStatus.Items[0].Selected = true;
                                }
                                break;
                        }
                        #endregion

                        #region Topic progress
                        ChangeProgressBar();
                        #endregion

                        #region Get questions
                        listQns.DataSource = questions;
                        listQns.DataBind();
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    GetIconData();
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = listIconUrl[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = listIconUrl[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpeAddIcon.Hide();
                    upEditTopicIcon.Update();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rbPrivacyEveryone_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cbCheckAllDept.Checked = false;
                cbCheckAllDept.Enabled = false;

                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    cblDepartment.Items[i].Selected = false;
                }
                cblDepartment.Enabled = false;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rbPrivacyDepartment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cbCheckAllDept.Checked = true;
                cbCheckAllDept.Enabled = true;

                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    cblDepartment.Items[i].Selected = true;
                }
                cblDepartment.Enabled = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpeAddIcon.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                mpeAddIcon.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void cbCheckAllDept_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbCheckAllDept.Checked)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        cblDepartment.Items[i].Selected = true;
                    }
                }
                else
                {
                    cblDepartment.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void cblDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Boolean isCheckAllDepartment = true;
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (!cblDepartment.Items[i].Selected)
                    {
                        isCheckAllDepartment = false;
                        break;
                    }
                }
                cbCheckAllDept.Checked = isCheckAllDepartment;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void listQns_ItemDataBound(object sender, AjaxControlToolkit.AccordionItemEventArgs e)
        {
            try
            {
                if (e.AccordionItem.ItemType == AjaxControlToolkit.AccordionItemType.Header)
                {
                    Literal ltlMinQNo = e.AccordionItem.FindControl("ltlMinQNo") as Literal;
                    Literal ltlMinQId = e.AccordionItem.FindControl("ltlMinQId") as Literal;
                    LinkButton lbMinQShow = e.AccordionItem.FindControl("lbMinQShow") as LinkButton;
                    System.Web.UI.WebControls.Image imgMinQ = e.AccordionItem.FindControl("imgMinQ") as System.Web.UI.WebControls.Image;
                    Literal ltlMinQContents = e.AccordionItem.FindControl("ltlMinQContents") as Literal;
                    DropDownList ddlMinQStatus = e.AccordionItem.FindControl("ddlMinQStatus") as DropDownList;
                    Literal ltlMinQStatus = e.AccordionItem.FindControl("ltlMinQStatus") as Literal;

                    ddlMinQStatus.Attributes.Add("onclick", "stopOpening();");
                    ddlMinQStatus.Attributes.Add("questionid", questions[e.AccordionItem.DataItemIndex].Id);
                    ltlMinQNo.Text = "Q" + (e.AccordionItem.DataItemIndex + 1);
                    ltlMinQId.Text = questions[e.AccordionItem.DataItemIndex].Id;
                    lbMinQShow.CommandArgument = questions[e.AccordionItem.DataItemIndex].Id;

                    if (!String.IsNullOrEmpty(questions[e.AccordionItem.DataItemIndex].ContentImageUrl))
                    {
                        imgMinQ.ImageUrl = questions[e.AccordionItem.DataItemIndex].ContentImageUrl;
                    }
                    else
                    {
                        imgMinQ.Visible = false;
                    }
                    //if (questions[e.AccordionItem.DataItemIndex].Content.Length > 40)
                    //{
                    //    ltlMinQContents.Text = questions[e.AccordionItem.DataItemIndex].Content.Substring(0, 40) + "...";
                    //}
                    //else
                    //{
                    ltlMinQContents.Text = questions[e.AccordionItem.DataItemIndex].Content;
                    //}

                    if (questions[e.AccordionItem.DataItemIndex].Status == 1)
                    {
                        ddlMinQStatus.Items[0].Selected = true;
                    }
                    else
                    {
                        ddlMinQStatus.Items[1].Selected = true;
                    }

                    ltlMinQStatus.Text = ddlMinQStatus.SelectedItem.Text;
                }
                else if (e.AccordionItem.ItemType == AjaxControlToolkit.AccordionItemType.Content)
                {
                    #region define controller
                    HiddenField hfQuestionType = e.AccordionItem.FindControl("hfQuestionType") as HiddenField;
                    Literal ltlMaxQNo = e.AccordionItem.FindControl("ltlMaxQNo") as Literal;
                    Literal ltlMaxQId = e.AccordionItem.FindControl("ltlMaxQId") as Literal;
                    LinkButton lbMaxQShow = e.AccordionItem.FindControl("lbMaxQShow") as LinkButton;
                    LinkButton lbMaxSave = e.AccordionItem.FindControl("lbMaxSave") as LinkButton;
                    LinkButton lbEditDelete = e.AccordionItem.FindControl("lbEditDelete") as LinkButton;
                    TextBox tbEditQuestion = e.AccordionItem.FindControl("tbEditQuestion") as TextBox;
                    TextBox tbEditAnswer1 = e.AccordionItem.FindControl("tbEditAnswer1") as TextBox;
                    TextBox tbEditAnswer2 = e.AccordionItem.FindControl("tbEditAnswer2") as TextBox;
                    TextBox tbEditAnswer3 = e.AccordionItem.FindControl("tbEditAnswer3") as TextBox;
                    TextBox tbEditAnswer4 = e.AccordionItem.FindControl("tbEditAnswer4") as TextBox;

                    Label lblEditQuestionCount = e.AccordionItem.FindControl("lblEditQuestionCount") as Label;
                    Label lblEditAnswer1Count = e.AccordionItem.FindControl("lblEditAnswer1Count") as Label;
                    Label lblEditAnswer2Count = e.AccordionItem.FindControl("lblEditAnswer2Count") as Label;
                    Label lblEditAnswer3Count = e.AccordionItem.FindControl("lblEditAnswer3Count") as Label;
                    Label lblEditAnswer4Count = e.AccordionItem.FindControl("lblEditAnswer4Count") as Label;
                    Label lblEditPreviewQuestion = e.AccordionItem.FindControl("lblEditPreviewQuestion") as Label;
                    Label lblEditPreviewQuestionAnswer1 = e.AccordionItem.FindControl("lblEditPreviewQuestionAnswer1") as Label;
                    Label lblEditPreviewQuestionAnswer2 = e.AccordionItem.FindControl("lblEditPreviewQuestionAnswer2") as Label;
                    Label lblEditPreviewQuestionAnswer3 = e.AccordionItem.FindControl("lblEditPreviewQuestionAnswer3") as Label;
                    Label lblEditPreviewQuestionAnswer4 = e.AccordionItem.FindControl("lblEditPreviewQuestionAnswer4") as Label;

                    Label lblEditQuestionRecommendedReadingTime = e.AccordionItem.FindControl("lblEditQuestionRecommendedReadingTime") as Label;
                    Label lblEditQuestionRecommendedAnsweringTime = e.AccordionItem.FindControl("lblEditQuestionRecommendedAnsweringTime") as Label;

                    DropDownList ddlEditQuestionReadingTime = e.AccordionItem.FindControl("ddlEditQuestionReadingTime") as DropDownList;
                    DropDownList ddlEditQuestionAnsweringTime = e.AccordionItem.FindControl("ddlEditQuestionAnsweringTime") as DropDownList;
                    DropDownList ddlEditQuestionDifficulty = e.AccordionItem.FindControl("ddlEditQuestionDifficulty") as DropDownList;
                    DropDownList ddlEditQuestionScoring = e.AccordionItem.FindControl("ddlEditQuestionScoring") as DropDownList;
                    DropDownList ddlEditQuestionStatus = e.AccordionItem.FindControl("ddlEditQuestionStatus") as DropDownList;

                    Panel plEditAction01 = e.AccordionItem.FindControl("plEditAction01") as Panel;
                    Panel plEditAction02 = e.AccordionItem.FindControl("plEditAction02") as Panel;
                    Panel plEditAction03 = e.AccordionItem.FindControl("plEditAction03") as Panel;
                    Panel plEditFormat = e.AccordionItem.FindControl("plEditFormat") as Panel;
                    Panel plEditImg = e.AccordionItem.FindControl("plEditImg") as Panel;
                    Panel plEditQuestionImgPre = e.AccordionItem.FindControl("plEditQuestionImgPre") as Panel;
                    Panel plEditQuestionImg = e.AccordionItem.FindControl("plEditQuestionImg") as Panel;
                    Panel plEditPreview = e.AccordionItem.FindControl("plEditPreview") as Panel;
                    Panel plEditPreviewImg = e.AccordionItem.FindControl("plEditPreviewImg") as Panel;

                    System.Web.UI.WebControls.Image imgEditPreviewImg = e.AccordionItem.FindControl("imgEditPreviewImg") as System.Web.UI.WebControls.Image;

                    FileUpload fuEditQuestion = e.AccordionItem.FindControl("fuEditQuestion") as FileUpload;

                    HiddenField hfOriImageMD5 = e.AccordionItem.FindControl("hfOriImageMD5") as HiddenField;


                    HtmlContainerControl lblUpload = e.AccordionItem.FindControl("lblUpload") as HtmlContainerControl;
                    lblUpload.Attributes.Add("for", fuEditQuestion.ClientID);

                    #endregion

                    #region Set value
                    hfQuestionType.Value = Convert.ToString(questions[e.AccordionItem.DataItemIndex].QuestionType);
                    ltlMaxQNo.Text = "Q" + (e.AccordionItem.DataItemIndex + 1);
                    ltlMaxQId.Text = questions[e.AccordionItem.DataItemIndex].Id;
                    lbEditDelete.CommandArgument = questions[e.AccordionItem.DataItemIndex].Id;
                    lbMaxSave.CommandArgument = questions[e.AccordionItem.DataItemIndex].Id;
                    tbEditQuestion.Text = questions[e.AccordionItem.DataItemIndex].Content;
                    tbEditAnswer1.Text = questions[e.AccordionItem.DataItemIndex].FirstChoice;
                    tbEditAnswer2.Text = questions[e.AccordionItem.DataItemIndex].SecondChoice;
                    tbEditAnswer3.Text = questions[e.AccordionItem.DataItemIndex].ThirdChoice;
                    tbEditAnswer4.Text = questions[e.AccordionItem.DataItemIndex].FourthChoice;

                    lblEditPreviewQuestion.Text = questions[e.AccordionItem.DataItemIndex].Content;
                    lblEditPreviewQuestionAnswer1.Text = questions[e.AccordionItem.DataItemIndex].FirstChoice;
                    lblEditPreviewQuestionAnswer2.Text = questions[e.AccordionItem.DataItemIndex].SecondChoice;
                    lblEditPreviewQuestionAnswer3.Text = questions[e.AccordionItem.DataItemIndex].ThirdChoice;
                    lblEditPreviewQuestionAnswer4.Text = questions[e.AccordionItem.DataItemIndex].FourthChoice;

                    String jsCommand = String.Format("PreviewImage(this,'{0}','{1}','{2}','{3}','{4}','{5}');", imgEditPreviewImg.ClientID, hfEditQuestionImg.ClientID, hfEditQuestionImgExt.ClientID, hfEditQuestionImgWidth.ClientID, hfEditQuestionImgHeight.ClientID, hfEditQuestionImgIsNew.ClientID);
                    fuEditQuestion.Attributes.Add("onchange", jsCommand);

                    for (int i = 0; i < ddlEditQuestionReadingTime.Items.Count; i++)
                    {
                        if (float.Parse(ddlEditQuestionReadingTime.Items[i].Value) == questions[e.AccordionItem.DataItemIndex].TimeAssignedForReading)
                        {
                            ddlEditQuestionReadingTime.Items[i].Selected = true;
                            break;
                        }
                    }
                    for (int i = 0; i < ddlEditQuestionAnsweringTime.Items.Count; i++)
                    {
                        if (float.Parse(ddlEditQuestionAnsweringTime.Items[i].Value) == questions[e.AccordionItem.DataItemIndex].TimeAssignedForAnswering)
                        {
                            ddlEditQuestionAnsweringTime.Items[i].Selected = true;
                            break;
                        }
                    }
                    for (int i = 0; i < ddlEditQuestionDifficulty.Items.Count; i++)
                    {
                        if (Convert.ToInt16(ddlEditQuestionDifficulty.Items[i].Value) == questions[e.AccordionItem.DataItemIndex].DifficultyLevel)
                        {
                            ddlEditQuestionDifficulty.Items[i].Selected = true;
                            break;
                        }
                    }
                    for (int i = 0; i < ddlEditQuestionScoring.Items.Count; i++)
                    {
                        if (Convert.ToInt16(ddlEditQuestionScoring.Items[i].Value) == questions[e.AccordionItem.DataItemIndex].ScoreMultiplier)
                        {
                            ddlEditQuestionScoring.Items[i].Selected = true;
                            break;
                        }
                    }
                    for (int i = 0; i < ddlEditQuestionStatus.Items.Count; i++)
                    {
                        if (Convert.ToInt16(ddlEditQuestionStatus.Items[i].Value) == questions[e.AccordionItem.DataItemIndex].Status)
                        {
                            ddlEditQuestionStatus.Items[i].Selected = true;
                            break;
                        }
                    }

                    int countOfQuestion = 0;
                    int countOfAnswer = 0;
                    switch (questions[e.AccordionItem.DataItemIndex].QuestionType)
                    {
                        case QUESTION_TYPE_1:
                            plEditAction01.CssClass = "tabs__list__item active";
                            plEditFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat01";
                            plEditImg.Visible = false;
                            countOfQuestion = countOfType1Question;
                            countOfAnswer = countOfType1Answer;
                            plEditPreview.CssClass = "question-preview";
                            break;
                        case QUESTION_TYPE_2:
                            plEditAction02.CssClass = "tabs__list__item active";
                            plEditFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat02";
                            plEditImg.Visible = false;
                            countOfQuestion = countOfType2Question;
                            countOfAnswer = countOfType2Answer;
                            plEditPreview.CssClass = "question-preview question-preview--format02";
                            break;
                        case QUESTION_TYPE_3:
                            plEditAction03.CssClass = "tabs__list__item active";
                            plEditFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat03";
                            plEditImg.Visible = true;
                            countOfQuestion = countOfType3Question;
                            countOfAnswer = countOfType3Answer;
                            plEditPreview.CssClass = "question-preview question-preview--format02";
                            plEditPreviewImg.Visible = true;
                            imgEditPreviewImg.ImageUrl = questions[e.AccordionItem.DataItemIndex].ContentImageUrl;
                            hfOriImageMD5.Value = questions[e.AccordionItem.DataItemIndex].ContentImageMd5;
                            break;
                        default:
                            break;
                    }

                    lblEditQuestionCount.Text = Convert.ToString(countOfQuestion - tbEditQuestion.Text.Length);
                    if (Convert.ToInt16(lblEditQuestionCount.Text) < 0)
                    {
                        tbEditQuestion.BorderColor = System.Drawing.Color.Red;
                    }

                    lblEditAnswer1Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer1.Text.Length);
                    if (Convert.ToInt16(lblEditAnswer1Count.Text) < 0)
                    {
                        tbEditAnswer1.BorderColor = System.Drawing.Color.Red;
                    }

                    lblEditAnswer2Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer2.Text.Length);
                    if (Convert.ToInt16(lblEditAnswer2Count.Text) < 0)
                    {
                        tbEditAnswer2.BorderColor = System.Drawing.Color.Red;
                    }

                    lblEditAnswer3Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer3.Text.Length);
                    if (Convert.ToInt16(lblEditAnswer3Count.Text) < 0)
                    {
                        tbEditAnswer3.BorderColor = System.Drawing.Color.Red;
                    }

                    lblEditAnswer4Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer4.Text.Length);
                    if (Convert.ToInt16(lblEditAnswer4Count.Text) < 0)
                    {
                        tbEditAnswer4.BorderColor = System.Drawing.Color.Red;
                    }

                    // Recommended
                    string recommendedSecond = "0";
                    if (tbEditQuestion.Text.Length < 11)
                    {
                        recommendedSecond = "1.0";
                    }
                    else if (tbEditQuestion.Text.Length > 10 && tbEditQuestion.Text.Length < 21)
                    {
                        recommendedSecond = "1.5";
                    }
                    else if (tbEditQuestion.Text.Length > 20 && tbEditQuestion.Text.Length < 31)
                    {
                        recommendedSecond = "2";
                    }
                    else if (tbEditQuestion.Text.Length > 30 && tbEditQuestion.Text.Length < 41)
                    {
                        recommendedSecond = "2.3";
                    }
                    else if (tbEditQuestion.Text.Length > 40 && tbEditQuestion.Text.Length < 51)
                    {
                        recommendedSecond = "2.5";
                    }
                    else if (tbEditQuestion.Text.Length > 50 && tbEditQuestion.Text.Length < 61)
                    {
                        recommendedSecond = "3";
                    }
                    else if (tbEditQuestion.Text.Length > 60 && tbEditQuestion.Text.Length < 71)
                    {
                        recommendedSecond = "3.5";
                    }
                    else if (tbEditQuestion.Text.Length > 70 && tbEditQuestion.Text.Length < 81)
                    {
                        recommendedSecond = "4";
                    }
                    else if (tbEditQuestion.Text.Length > 80 && tbEditQuestion.Text.Length < 91)
                    {
                        recommendedSecond = "4.5";
                    }
                    else if (tbEditQuestion.Text.Length > 90 && tbEditQuestion.Text.Length < 101)
                    {
                        recommendedSecond = "5";
                    }
                    else if (tbEditQuestion.Text.Length > 100 && tbEditQuestion.Text.Length < 121)
                    {
                        recommendedSecond = "5.5";
                    }
                    else if (tbEditQuestion.Text.Length > 120 && tbEditQuestion.Text.Length < 141)
                    {
                        recommendedSecond = "6";
                    }
                    else if (tbEditQuestion.Text.Length > 140 && tbEditQuestion.Text.Length < 161)
                    {
                        recommendedSecond = "6.5";
                    }
                    else if (tbEditQuestion.Text.Length > 160 && tbEditQuestion.Text.Length < 181)
                    {
                        recommendedSecond = "7";
                    }
                    else if (tbEditQuestion.Text.Length > 180 && tbEditQuestion.Text.Length < 201)
                    {
                        recommendedSecond = "8";
                    }
                    else if (tbEditQuestion.Text.Length > 200 && tbEditQuestion.Text.Length < 221)
                    {
                        recommendedSecond = "9";
                    }
                    else
                    {
                        recommendedSecond = "10";
                    }

                    lblEditQuestionRecommendedReadingTime.Text = recommendedSecond;

                    // Recommanded answering time
                    if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 4)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "10";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 3 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 25)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "12";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 24 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 50)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "13";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 49 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 75)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "14";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 74 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 100)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "15";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 99 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 120)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "16";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 119 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 130)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "17";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 129 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 150)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "18";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 149 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 165)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "19";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 164 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 185)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "20";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 184 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 200)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "21";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 199 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 225)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "22";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 224 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 250)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "23";
                    }
                    else if ((tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) > 249 && (tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length) < 270)
                    {
                        lblEditQuestionRecommendedAnsweringTime.Text = "24";
                    }
                    else
                    {
                        recommendedSecond = "25";
                    }

                    lblEditQuestionRecommendedReadingTime.Text = recommendedSecond;

                    #endregion

                    SetEditQuestionTextCounter(questions[e.AccordionItem.DataItemIndex].QuestionType,
                                              tbEditQuestion, tbEditAnswer1, tbEditAnswer2, tbEditAnswer3, tbEditAnswer4,
                                              lblEditQuestionCount, lblEditAnswer1Count, lblEditAnswer2Count, lblEditAnswer3Count, lblEditAnswer4Count,
                                              lblEditPreviewQuestion, lblEditPreviewQuestionAnswer1, lblEditPreviewQuestionAnswer2, lblEditPreviewQuestionAnswer3, lblEditPreviewQuestionAnswer4,
                                              lblEditQuestionRecommendedReadingTime, lblEditQuestionRecommendedAnsweringTime);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ddlQuestionStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddlQuestionStatus = sender as DropDownList;
                string adminUserId = ViewState["admin_user_id"].ToString();
                string companyId = ViewState["company_id"].ToString();
                string topicId = ViewState["topic_id"].ToString();
                string categoryId = hfCategoryId.Value.ToString();
                string questionId = ddlQuestionStatus.Attributes["questionid"].ToString();
                int newQuestionStatus = Int32.Parse(ddlQuestionStatus.SelectedValue);

                if (!string.IsNullOrEmpty(topicId) && !string.IsNullOrEmpty(categoryId))
                {
                    QuestionUpdateResponse response = asc.UpdateQuestionStatus(adminUserId, companyId, topicId, categoryId, questionId, newQuestionStatus);

                    Log.Error(string.Format("Response success: {0} and response message: {1}", response.Success, response.ErrorMessage));
                    if (response.Success)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('Updated question status');", true);
                    }
                    else
                    {
                        if (ddlQuestionStatus.SelectedValue.Equals("1"))
                        {
                            ddlQuestionStatus.SelectedIndex = 0;
                        }
                        else
                        {
                            ddlQuestionStatus.SelectedIndex = 1;
                        }
                        ChangeProgressBar();

                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('Question status not updated. " + response.ErrorMessage + "');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check input data
                if (String.IsNullOrEmpty(tbTopictitle.Text.Trim()))
                {
                    ScriptManager.RegisterStartupScript(lbUpdate, typeof(LinkButton), "toast", "toastr.info('Topic title cannot be empty.');", true);
                    return;
                }
                #endregion

                #region Step 2. Check status
                #region status logic
                /*
                 * Status -1 : deleted
                 *         1 : unlisted
                 *         2 : active
                 *         3 : hide
                 *************************
                 *  Status change to 2:
                 *  if question count < setting:
                 *      status 1 => 1, 2 => 3, 3 => 3
                 *  else
                 *      status 1 => 2, 2 => 2, 3 => 2
                 *------------------------      
                 *  Status change to 1:
                 *  only 1 => 1
                 *------------------------
                 *  Status change to 3:
                 *  only 2 => 3 and 3 => 3
                 */
                #endregion
                String updateMsg = String.Empty;
                int updateStatus = 0;

                GetTopicDetail();

                int countActive = 0;
                for (int i = 0; i < questions.Count; i++)
                {
                    if (questions[i].Status == 1)
                    {
                        countActive++;
                    }
                }

                if (ddlTopicStatus.SelectedValue.Equals("2"))
                {
                    if (countActive < Convert.ToInt16(ddlNoQuestionPerGame.SelectedValue))
                    {
                        if (Convert.ToInt16(hfTopicStatus.Value) == 2)
                        {
                            updateStatus = 3;
                            updateMsg = "Topic cannot be activated as the number of active questions is less than the number of selected question in Settings.";
                        }
                        else
                        {
                            updateStatus = Convert.ToInt16(hfTopicStatus.Value);
                            updateMsg = "Topic cannot be activated as the number of active questions is less than the number of selected question in Settings.";
                        }
                    }
                    else
                    {
                        updateStatus = 2;
                    }
                }
                else if (ddlTopicStatus.SelectedValue.Equals("1"))
                {
                    updateStatus = 1;
                }
                else if (ddlTopicStatus.SelectedValue.Equals("3"))
                {
                    updateStatus = 3;
                }
                #endregion

                #region Step 2. Check department
                bool isPrivacyForEveryone = false;
                List<String> listDepartment = new List<String>();
                if (rbPrivacyDepartment.Checked)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        if (cblDepartment.Items[i].Selected)
                        {
                            listDepartment.Add(cblDepartment.Items[i].Value);
                        }
                    }

                    if (listDepartment.Count == 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "ReloadToast();toastr.info('Please at least choose a department.');", true);
                        return;
                    }
                }
                else if (rbPrivacyEveryone.Checked)
                {
                    isPrivacyForEveryone = true;
                }
                #endregion

                #region Step 3. Check category
                GetCategoryList();

                String categoryID = String.Empty;
                String categoryTitle = cbCategory.SelectedItem.Text;
                for (int i = 0; i < categoryList.Count; i++)
                {
                    if (categoryList[i].Title.ToLower().Equals(cbCategory.SelectedItem.Text.Trim().ToLower()))
                    {
                        categoryID = categoryList[i].Id;
                        break;
                    }
                }
                #endregion

                #region Call API
                TopicUpdateResponse response = asc.UpdateTopic(ViewState["admin_user_id"].ToString(),
                                                               ViewState["company_id"].ToString(),
                                                               ViewState["topic_id"].ToString(),
                                                               tbTopictitle.Text.Trim(),
                                                               imgTopic.ImageUrl,
                                                               tbTopicDescription.Text,
                                                               categoryID,
                                                               categoryTitle,
                                                               updateStatus,
                                                               listDepartment,
                                                               Convert.ToInt16(ddlNoQuestionPerGame.SelectedValue),
                                                               isPrivacyForEveryone
                                                               );
                if (response.Success)
                {
                    //Session["admin_user_id"] = ViewState["admin_user_id"].ToString();
                    //Session["company_id"] = ViewState["company_id"].ToString();
                    hfCategoryId.Value = response.NewTopicCategoryId;
                    ChangeProgressBar();
                    MessageUtility.ShowToast(this.Page, "Update topic succeeded. " + updateMsg, MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                else
                {
                    Log.Error("Update topic failed. Error: " + response.ErrorMessage);
                    MessageUtility.ShowToast(this.Page, response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ddlNoQuestionPerGame_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ChangeProgressBar();
                //ScriptManager.RegisterStartupScript(ddlNoQuestionPerGame, typeof(DropDownList), "Reload", "TabFunction();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbBasicInfo_Click(object sender, EventArgs e)
        {
            try
            {
                //lbBasicInfo.CssClass = "tabs__link active";
                //lbDepartment.CssClass = "tabs__link";
                //lbSettings.CssClass = "tabs__link";
                //plBasicInfo.Visible = true;
                //plDepartment.Visible = false;
                //plSettings.Visible = false;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                //lbBasicInfo.CssClass = "tabs__link";
                //lbDepartment.CssClass = "tabs__link active";
                //lbSettings.CssClass = "tabs__link";
                //plBasicInfo.Visible = false;
                //plDepartment.Visible = true;
                //plSettings.Visible = false;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbSettings_Click(object sender, EventArgs e)
        {
            try
            {
                //lbBasicInfo.CssClass = "tabs__link";
                //lbDepartment.CssClass = "tabs__link";
                //lbSettings.CssClass = "tabs__link active";
                //plBasicInfo.Visible = false;
                //plDepartment.Visible = false;
                //plSettings.Visible = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbNewQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                GetTopicDetail();
                if (questions == null)
                {
                    questions = new List<ChallengeQuestion>();
                }
                ViewState["QuestionType"] = 1;
                plNewQuestion.Visible = true;
                ltlNewQuestionNo.Text = "Q" + (questions.Count + 1);
                String questionId = "CQ" + Guid.NewGuid().ToString().Replace("-", "");
                ltlNewQuestionId.Text = questionId;
                lblQuestionCount.Text = Convert.ToString(countOfType1Question);
                lblAnswer1Count.Text = Convert.ToString(countOfType1Answer);
                lblAnswer2Count.Text = Convert.ToString(countOfType1Answer);
                lblAnswer3Count.Text = Convert.ToString(countOfType1Answer);
                lblAnswer4Count.Text = Convert.ToString(countOfType1Answer);
                tbQuestion.Text = String.Empty;
                tbAnswer1.Text = String.Empty;
                tbAnswer2.Text = String.Empty;
                tbAnswer3.Text = String.Empty;
                tbAnswer4.Text = String.Empty;
                ddlNewQuestionReadingTime.ClearSelection();
                ddlNewQuestionReadingTime.Items[1].Selected = true;
                ddlNewQuestionAnsweringTime.ClearSelection();
                ddlNewQuestionAnsweringTime.Items[0].Selected = true;
                ddlNewQuestionDifficulty.ClearSelection();
                ddlNewQuestionDifficulty.Items[0].Selected = true;
                ddlNewQuestionScoring.ClearSelection();
                ddlNewQuestionScoring.Items[0].Selected = true;
                ddlNewQuestionStatus.ClearSelection();
                ddlNewQuestionStatus.Items[0].Selected = true;
                lbNewQuestion.Visible = false;
                SetNewQuestionTextCounter(QUESTION_TYPE_1);
                SetNewQuestionTemplate(QUESTION_TYPE_1);

                String jsCommand = String.Format("PreviewImage(this,'{0}','{1}','{2}','{3}','{4}','{5}');", imgNewQuestionPreview.ClientID, hfNewQuestionImg.ClientID, hfNewQuestionImgExt.ClientID, hfNewQuestionImgWidth.ClientID, hfNewQuestionImgHeight.ClientID, hfEditQuestionImgIsNew.ClientID);
                fuNewQuestion.Attributes.Add("onchange", jsCommand);
                Page.SetFocus(lbBottom);

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbNewQuestionFormat01_Click(object sender, EventArgs e)
        {
            try
            {
                SetNewQuestionTemplate(QUESTION_TYPE_1);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbNewQuestionFormat02_Click(object sender, EventArgs e)
        {
            try
            {
                SetNewQuestionTemplate(QUESTION_TYPE_2);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbNewQuestionFormat03_Click(object sender, EventArgs e)
        {
            try
            {
                SetNewQuestionTemplate(QUESTION_TYPE_3);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbNewQuestionSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check data
                String errorMsg = string.Empty;
                String imgBase64String = String.Empty;
                String imgFormat = String.Empty;
                int imgWidth = 0;
                int imgHeight = 0;
                int questionType = Convert.ToInt16(ViewState["QuestionType"]);
                if (String.IsNullOrEmpty(tbQuestion.Text.Trim()))
                {
                    errorMsg = "Please fill in a question.";
                    SetNewQuestionTemplate(questionType);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                    return;
                }

                if (String.IsNullOrEmpty(tbAnswer1.Text.Trim()))
                {
                    errorMsg = "Please fill the correct answer";
                    SetNewQuestionTemplate(questionType);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                    return;
                }

                if (String.IsNullOrEmpty(tbAnswer2.Text.Trim()))
                {
                    errorMsg = "Please fill the answer 02.";
                    SetNewQuestionTemplate(questionType);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                    return;
                }

                if (String.IsNullOrEmpty(tbAnswer3.Text.Trim()))
                {
                    errorMsg = "Please fill the answer 03.";
                    SetNewQuestionTemplate(questionType);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                    return;
                }

                if (String.IsNullOrEmpty(tbAnswer4.Text.Trim()))
                {
                    errorMsg = "Please fill the answer 04.";
                    SetNewQuestionTemplate(questionType);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                    return;
                }

                switch (questionType)
                {
                    case QUESTION_TYPE_1:
                        if (tbQuestion.Text.Length > countOfType1Question || tbAnswer1.Text.Length > countOfType1Answer || tbAnswer2.Text.Length > countOfType1Answer || tbAnswer3.Text.Length > countOfType1Answer || tbAnswer4.Text.Length > countOfType1Answer)
                        {
                            errorMsg = "The content length should be kept within the text limit";
                            SetNewQuestionTemplate(questionType);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                            return;
                        }
                        break;

                    case QUESTION_TYPE_2:
                        if (tbQuestion.Text.Length > countOfType2Question || tbAnswer1.Text.Length > countOfType2Answer || tbAnswer2.Text.Length > countOfType2Answer || tbAnswer3.Text.Length > countOfType2Answer || tbAnswer4.Text.Length > countOfType2Answer)
                        {
                            errorMsg = "The content length should be kept within the text limit";
                            SetNewQuestionTemplate(questionType);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                            return;
                        }
                        break;

                    case QUESTION_TYPE_3:
                        imgBase64String = hfNewQuestionImg.Value.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");

                        string dummyData = imgBase64String.Trim().Replace("%", "").Replace(",", "").Replace(" ", "+");
                        if (dummyData.Length % 4 > 0)
                        {
                            dummyData = dummyData.PadRight(dummyData.Length + 4 - dummyData.Length % 4, '=');
                        }
                        imgBase64String = dummyData;

                        imgFormat = hfNewQuestionImgExt.Value.Replace("image/", "");

                        if (String.IsNullOrEmpty(imgBase64String) || String.IsNullOrEmpty(imgFormat))
                        {
                            errorMsg = "Please choose a image for question.";
                            SetNewQuestionTemplate(questionType);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                            return;
                        }

                        if (tbQuestion.Text.Length > countOfType3Question || tbAnswer1.Text.Length > countOfType3Answer || tbAnswer2.Text.Length > countOfType3Answer || tbAnswer3.Text.Length > countOfType3Answer || tbAnswer4.Text.Length > countOfType3Answer)
                        {
                            errorMsg = "The content length should be kept within the text limit";
                            SetNewQuestionTemplate(questionType);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                            return;
                        }
                        break;

                    default:
                        break;
                }
                #endregion

                #region Step 2. Upload question image to S3. (original, large, medium and small size)
                String imageUrl = String.Empty;
                StringBuilder sbMD5 = new StringBuilder();

                if (questionType == 3 && !String.IsNullOrEmpty(imgBase64String) && !String.IsNullOrEmpty(imgFormat))
                {
                    imgWidth = Convert.ToInt16(hfNewQuestionImgWidth.Value);
                    imgHeight = Convert.ToInt16(hfNewQuestionImgHeight.Value);

                    #region Step 2.1 Get AWS Cognito token and RoleArn from WebAPI server.
                    GetCognitoTokenRequest request = new GetCognitoTokenRequest();
                    request.CompanyId = ViewState["company_id"].ToString();
                    request.UserId = ViewState["admin_user_id"].ToString();
                    byte[] postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(request));
                    HttpWebRequest httpRequest = HttpWebRequest.Create(WebConfigurationManager.AppSettings["WebAPI_Url_GetCognitoToken"].ToString()) as HttpWebRequest;
                    httpRequest.Method = "POST";
                    httpRequest.Timeout = 30000;
                    httpRequest.ContentType = "application/json";
                    httpRequest.ContentLength = postData.Length;
                    using (Stream st = httpRequest.GetRequestStream())
                    {
                        st.Write(postData, 0, postData.Length);
                    }
                    string result = "";
                    using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                    {
                        using (StreamReader sr = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = sr.ReadToEnd();
                        }
                    }

                    App_Code.ServiceResponses.GetCognitoTokenResponse response = JsonConvert.DeserializeObject<App_Code.ServiceResponses.GetCognitoTokenResponse>(result);
                    #endregion

                    #region Step 2.2. Create IAmazonS3 by Cognito token and RoleArn.
                    AnonymousAWSCredentials cred = new AnonymousAWSCredentials();
                    AmazonSecurityTokenServiceClient stsClient = new AmazonSecurityTokenServiceClient(cred, RegionEndpoint.APSoutheast1);
                    AssumeRoleWithWebIdentityRequest stsReq = new AssumeRoleWithWebIdentityRequest();
                    stsReq.RoleArn = response.RoleArn;
                    stsReq.RoleSessionName = "AdminWebsiteSession";
                    stsReq.WebIdentityToken = response.Token;
                    AssumeRoleWithWebIdentityResponse stsResp = stsClient.AssumeRoleWithWebIdentity(stsReq);
                    IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(stsResp.Credentials, RegionEndpoint.APSoutheast1);
                    #endregion

                    #region Step 2.3. Checking if the folder exists. (/cocadre-{CompanyId}/topics/{TopicId}/{QuestionId}/{yyyyMMddHHmmssfff.jpg})
                    String bucketName = "cocadre-" + ViewState["company_id"].ToString().ToLower();
                    String folderName = "topics/" + ViewState["topic_id"].ToString() + "/" + ltlNewQuestionId.Text;

                    ListObjectsRequest findFolderRequest = new ListObjectsRequest();
                    findFolderRequest.BucketName = bucketName;
                    findFolderRequest.Delimiter = "/";
                    findFolderRequest.Prefix = folderName;
                    ListObjectsResponse findFolderResponse = s3Client.ListObjects(findFolderRequest);
                    List<String> commonPrefixes = findFolderResponse.CommonPrefixes;
                    Boolean folderExists = commonPrefixes.Any();
                    #endregion

                    #region Step 2.4. 如果沒有資料夾，則建立資料夾。
                    if (!folderExists)
                    {
                        PutObjectRequest folderRequest = new PutObjectRequest();

                        folderRequest.BucketName = bucketName;
                        String folderKey = folderName + "/";
                        folderRequest.Key = folderKey;
                        folderRequest.InputStream = new MemoryStream(new byte[0]);
                        PutObjectResponse folderResponse = s3Client.PutObject(folderRequest);
                    }
                    #endregion

                    #region Step 2.5. 上傳原始圖檔。
                    Dictionary<String, String> metadatas = new Dictionary<string, string>();
                    String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
                    byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                    MemoryStream ms = new MemoryStream(imgBytes);
                    System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);

                    String fileName = PROCESS_TIMESTAMP + "_original" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(imgWidth));
                    metadatas.Add("Height", Convert.ToString(imgHeight));
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    #endregion

                    #region Step 2.6. 如果原圖比 1920 * 1920 還大，就做 1920 * 1920 的縮圖。否則直接存原圖。
                    System.Drawing.Image newImage;
                    if (imgWidth > 1920 || imgHeight > 1920)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                        fileName = PROCESS_TIMESTAMP + "_large" + "." + imgFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = PROCESS_TIMESTAMP + "_large" + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion

                    #region Step 2.7. 如果原圖比 1080 * 1080 還大，就做 1080 * 1080 的縮圖。否則直接存原圖。
                    if (imgWidth > 1080 || imgHeight > 1080)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                        fileName = PROCESS_TIMESTAMP + "_medium" + "." + imgFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = PROCESS_TIMESTAMP + "_medium" + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion

                    #region Step 2.8. 如果原圖比 540 * 540 還大，就做 540 * 540 的縮圖。否則直接存原圖。
                    if (imgWidth > 540 || imgHeight > 540)
                    {
                        newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                        fileName = PROCESS_TIMESTAMP + "_small" + "." + imgFormat.ToLower();
                        metadatas.Clear();
                        metadatas.Add("Width", Convert.ToString(newImage.Width));
                        metadatas.Add("Height", Convert.ToString(newImage.Height));
                        S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                    }
                    else
                    {
                        fileName = PROCESS_TIMESTAMP + "_small" + "." + imgFormat.ToLower();
                        S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                    }
                    #endregion

                    #region Step 2.9. Get profile image url
                    imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + PROCESS_TIMESTAMP + "_original." + imgFormat.ToLower();
                    #endregion

                    #region Step 3.0 Get
                    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                    byte[] hash = md5.ComputeHash(imgBytes);

                    for (int i = 0; i < hash.Length; i++)
                    {
                        sbMD5.Append(hash[i].ToString("X2"));
                    }
                    #endregion
                }
                #endregion

                #region Step 3. Call API
                QuestionCreateResponse questionCreateresponse = asc.CreateNewQuestion(
                                                  ViewState["admin_user_id"].ToString(),
                                                  ViewState["company_id"].ToString(),
                                                  ltlNewQuestionId.Text,
                                                  ViewState["topic_id"].ToString(),
                                                  hfCategoryId.Value.ToString(),
                                                  Convert.ToInt16(ViewState["QuestionType"]),
                                                  tbQuestion.Text,
                                                  imageUrl,
                                                  sbMD5.ToString(),
                                                  "#000000",
                                                  1,
                                                  tbAnswer1.Text,
                                                  null,
                                                  null,
                                                  tbAnswer2.Text,
                                                  null,
                                                  null,
                                                  tbAnswer3.Text,
                                                  null,
                                                  null,
                                                  tbAnswer4.Text,
                                                  null,
                                                  null,
                                                  float.Parse(ddlNewQuestionReadingTime.SelectedValue),
                                                  float.Parse(ddlNewQuestionAnsweringTime.SelectedValue),
                                                  Convert.ToInt16(ddlNewQuestionDifficulty.SelectedValue),
                                                  Convert.ToInt16(ddlNewQuestionScoring.SelectedValue),
                                                  1,
                                                  Convert.ToInt16(ddlNewQuestionStatus.SelectedValue)
                                                  );


                #endregion

                if (questionCreateresponse.Success)
                {
                    hfNewQuestionImg.Value = String.Empty;
                    hfNewQuestionImgWidth.Value = String.Empty;
                    hfNewQuestionImgHeight.Value = String.Empty;
                    hfNewQuestionImgExt.Value = String.Empty;
                    plNewQuestion.Visible = false;
                    lbNewQuestion.Visible = true;
                    GetTopicDetail();
                    listQns.DataSource = questions;
                    listQns.DataBind();
                    ChangeProgressBar();
                    errorMsg = "Question created successfully.";
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                }
                else
                {
                    errorMsg = "Question failed to create.";
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                    Log.Debug("CreateNewQuestion GG. Error: " + questionCreateresponse.ErrorMessage);
                    Page.SetFocus(lbNewQuestionSave);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                String errorMsg = "Question failed to create.";
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                Page.SetFocus(lbNewQuestionSave);
            }
        }

        protected void lbNewQuestionCancel_Click(object sender, EventArgs e)
        {
            try
            {
                plNewQuestion.Visible = false;
                lbNewQuestion.Visible = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbDeleteCancel_Click(object sender, EventArgs e)
        {
            mpeDeleteQuestion.Hide();
        }

        protected void lbEditDelete_Click(object sender, EventArgs e)
        {
            mpeDeleteQuestion.Show();
        }

        protected void listQns_ItemCommand(object sender, CommandEventArgs e)
        {
            try
            {
                ViewState["QuestionId"] = e.CommandArgument.ToString();
                if (e.CommandName.Equals("DeleteQuestion"))
                {
                    hfQuestionId.Value = e.CommandArgument.ToString();
                    mpeDeleteQuestion.Show();
                }
                else //  Other Command (EditQuestion, EditQuestionType1, EditQuestionType2, EditQuestionType3);
                {
                    int questionType = 0;
                    int countOfQuestion = 0;
                    int countOfAnswer = 0;

                    HiddenField hfQuestionType = ((AccordionCommandEventArgs)e).Container.FindControl("hfQuestionType") as HiddenField;
                    Literal ltlMaxQId = ((AccordionCommandEventArgs)e).Container.FindControl("ltlMaxQId") as Literal;

                    Panel plEditAction01 = ((AccordionCommandEventArgs)e).Container.FindControl("plEditAction01") as Panel;
                    Panel plEditAction02 = ((AccordionCommandEventArgs)e).Container.FindControl("plEditAction02") as Panel;
                    Panel plEditAction03 = ((AccordionCommandEventArgs)e).Container.FindControl("plEditAction03") as Panel;
                    Panel plEditFormat = ((AccordionCommandEventArgs)e).Container.FindControl("plEditFormat") as Panel;
                    Panel plEditImg = ((AccordionCommandEventArgs)e).Container.FindControl("plEditImg") as Panel;
                    Panel plEditPreviewImg = ((AccordionCommandEventArgs)e).Container.FindControl("plEditPreviewImg") as Panel;
                    Panel plEditPreview = ((AccordionCommandEventArgs)e).Container.FindControl("plEditPreview") as Panel;
                    plEditAction01.CssClass = "tabs__list__item";
                    plEditAction02.CssClass = "tabs__list__item";
                    plEditAction03.CssClass = "tabs__list__item";

                    TextBox tbEditQuestion = ((AccordionCommandEventArgs)e).Container.FindControl("tbEditQuestion") as TextBox;
                    TextBox tbEditAnswer1 = ((AccordionCommandEventArgs)e).Container.FindControl("tbEditAnswer1") as TextBox;
                    TextBox tbEditAnswer2 = ((AccordionCommandEventArgs)e).Container.FindControl("tbEditAnswer2") as TextBox;
                    TextBox tbEditAnswer3 = ((AccordionCommandEventArgs)e).Container.FindControl("tbEditAnswer3") as TextBox;
                    TextBox tbEditAnswer4 = ((AccordionCommandEventArgs)e).Container.FindControl("tbEditAnswer4") as TextBox;
                    tbEditQuestion.BorderColor = System.Drawing.Color.Empty;
                    tbEditAnswer1.BorderColor = System.Drawing.Color.Empty;
                    tbEditAnswer2.BorderColor = System.Drawing.Color.Empty;
                    tbEditAnswer3.BorderColor = System.Drawing.Color.Empty;
                    tbEditAnswer4.BorderColor = System.Drawing.Color.Empty;

                    Label lblEditQuestionCount = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditQuestionCount") as Label;
                    Label lblEditAnswer1Count = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditAnswer1Count") as Label;
                    Label lblEditAnswer2Count = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditAnswer2Count") as Label;
                    Label lblEditAnswer3Count = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditAnswer3Count") as Label;
                    Label lblEditAnswer4Count = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditAnswer4Count") as Label;

                    Label lblEditPreviewQuestion = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditPreviewQuestion") as Label;
                    Label lblEditPreviewQuestionAnswer1 = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditPreviewQuestionAnswer1") as Label;
                    Label lblEditPreviewQuestionAnswer2 = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditPreviewQuestionAnswer2") as Label;
                    Label lblEditPreviewQuestionAnswer3 = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditPreviewQuestionAnswer3") as Label;
                    Label lblEditPreviewQuestionAnswer4 = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditPreviewQuestionAnswer4") as Label;
                    lblEditPreviewQuestion.Text = tbEditQuestion.Text;
                    lblEditPreviewQuestionAnswer1.Text = tbEditAnswer1.Text;
                    lblEditPreviewQuestionAnswer2.Text = tbEditAnswer2.Text;
                    lblEditPreviewQuestionAnswer3.Text = tbEditAnswer3.Text;
                    lblEditPreviewQuestionAnswer4.Text = tbEditAnswer4.Text;

                    DropDownList ddlEditQuestionReadingTime = ((AccordionCommandEventArgs)e).Container.FindControl("ddlEditQuestionReadingTime") as DropDownList;
                    DropDownList ddlEditQuestionAnsweringTime = ((AccordionCommandEventArgs)e).Container.FindControl("ddlEditQuestionAnsweringTime") as DropDownList;
                    DropDownList ddlEditQuestionDifficulty = ((AccordionCommandEventArgs)e).Container.FindControl("ddlEditQuestionDifficulty") as DropDownList;
                    DropDownList ddlEditQuestionScoring = ((AccordionCommandEventArgs)e).Container.FindControl("ddlEditQuestionScoring") as DropDownList;
                    DropDownList ddlEditQuestionStatus = ((AccordionCommandEventArgs)e).Container.FindControl("ddlEditQuestionStatus") as DropDownList;

                    HiddenField hfOriImageMD5 = ((AccordionCommandEventArgs)e).Container.FindControl("hfOriImageMD5") as HiddenField;

                    Label lblEditQuestionRecommendedReadingTime = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditQuestionRecommendedReadingTime") as Label;
                    Label lblEditQuestionRecommendedAnsweringTime = ((AccordionCommandEventArgs)e).Container.FindControl("lblEditQuestionRecommendedAnsweringTime") as Label;


                    System.Web.UI.WebControls.Image imgEditPreviewImg = ((AccordionCommandEventArgs)e).Container.FindControl("imgEditPreviewImg") as System.Web.UI.WebControls.Image;


                    if (e.CommandName.Equals("EditQuestion"))
                    {
                        EditQuestion(ltlMaxQId.Text,
                                    Convert.ToInt16(hfQuestionType.Value),
                                    tbEditQuestion.Text,
                                    tbEditAnswer1.Text,
                                    tbEditAnswer2.Text,
                                    tbEditAnswer3.Text,
                                    tbEditAnswer4.Text,
                                    hfOriImageMD5.Value,
                                    imgEditPreviewImg.ImageUrl,
                                    "#FFFFFF",
                                    float.Parse(ddlEditQuestionReadingTime.SelectedValue),
                                    float.Parse(ddlEditQuestionAnsweringTime.SelectedValue),
                                    Convert.ToInt16(ddlEditQuestionDifficulty.SelectedValue),
                                    Convert.ToInt16(ddlEditQuestionScoring.SelectedValue),
                                    Convert.ToInt16(ddlEditQuestionStatus.SelectedValue),
                                    plEditAction01,
                                    plEditAction02,
                                    plEditAction03
                                    );
                        ChangeProgressBar();
                    }
                    else
                    {
                        if (e.CommandName.Equals("EditQuestionType1"))
                        {
                            questionType = 1;
                        }
                        else if (e.CommandName.Equals("EditQuestionType2"))
                        {
                            questionType = 2;
                        }
                        else if (e.CommandName.Equals("EditQuestionType3"))
                        {
                            questionType = 3;
                        }

                        #region
                        hfQuestionType.Value = Convert.ToString(questionType);
                        switch (questionType)
                        {

                            case 1:
                                plEditAction01.CssClass = "tabs__list__item active";
                                plEditFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat01";
                                countOfQuestion = countOfType1Question;
                                countOfAnswer = countOfType1Answer;
                                plEditImg.Visible = false;
                                plEditPreview.CssClass = "question-preview";
                                plEditPreviewImg.Visible = false;
                                break;
                            case 2:
                                plEditAction02.CssClass = "tabs__list__item active";
                                plEditFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat02";
                                countOfQuestion = countOfType2Question;
                                countOfAnswer = countOfType2Answer;
                                plEditImg.Visible = false;
                                plEditPreview.CssClass = "question-preview question-preview--format02";
                                plEditPreviewImg.Visible = false;
                                break;
                            case 3:
                                plEditAction03.CssClass = "tabs__list__item active";
                                plEditFormat.CssClass = "tabs__panels__item tabs__panels__item--questionformat03";
                                countOfQuestion = countOfType3Question;
                                countOfAnswer = countOfType3Answer;
                                plEditImg.Visible = true;
                                plEditPreview.CssClass = "question-preview question-preview--format02";
                                plEditPreviewImg.Visible = true;
                                //imgEditPreviewImg.ImageUrl = hfEditQuestionImg.Value;
                                break;
                            default:
                                break;
                        }

                        lblEditQuestionCount.Text = Convert.ToString(countOfQuestion - tbEditQuestion.Text.Length);
                        if (Convert.ToInt16(lblEditQuestionCount.Text) < 0)
                        {
                            tbEditQuestion.BorderColor = System.Drawing.Color.Red;
                        }

                        lblEditAnswer1Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer1.Text.Length);
                        if (Convert.ToInt16(lblEditAnswer1Count.Text) < 0)
                        {
                            tbEditAnswer1.BorderColor = System.Drawing.Color.Red;
                        }

                        lblEditAnswer2Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer2.Text.Length);
                        if (Convert.ToInt16(lblEditAnswer2Count.Text) < 0)
                        {
                            tbEditAnswer2.BorderColor = System.Drawing.Color.Red;
                        }

                        lblEditAnswer3Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer3.Text.Length);
                        if (Convert.ToInt16(lblEditAnswer3Count.Text) < 0)
                        {
                            tbEditAnswer3.BorderColor = System.Drawing.Color.Red;
                        }

                        lblEditAnswer4Count.Text = Convert.ToString(countOfAnswer - tbEditAnswer4.Text.Length);
                        if (Convert.ToInt16(lblEditAnswer4Count.Text) < 0)
                        {
                            tbEditAnswer4.BorderColor = System.Drawing.Color.Red;
                        }

                        lblEditQuestionRecommendedReadingTime.Text = calculateRecommendedReadingTime(tbEditQuestion.Text.Length);
                        lblEditQuestionRecommendedAnsweringTime.Text = calculateRecommendedAnsweringTime(tbEditAnswer1.Text.Length + tbEditAnswer2.Text.Length + tbEditAnswer3.Text.Length + tbEditAnswer4.Text.Length);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "HideProgressBar();", true);
            }
        }

        private string calculateRecommendedReadingTime(int questionTextLength)
        {
            if (questionTextLength < 11)
            {
                return "1.0";
            }
            else if (questionTextLength > 10 && questionTextLength < 21)
            {
                return "1.5";
            }
            else if (questionTextLength > 20 && questionTextLength < 31)
            {
                return "2";
            }
            else if (questionTextLength > 30 && questionTextLength < 41)
            {
                return "2.3";
            }
            else if (questionTextLength > 40 && questionTextLength < 51)
            {
                return "2.5";
            }
            else if (questionTextLength > 50 && questionTextLength < 61)
            {
                return "3";
            }
            else if (questionTextLength > 60 && questionTextLength < 71)
            {
                return "3.5";
            }
            else if (questionTextLength > 70 && questionTextLength < 81)
            {
                return "4";
            }
            else if (questionTextLength > 80 && questionTextLength < 91)
            {
                return "4.5";
            }
            else if (questionTextLength > 90 && questionTextLength < 101)
            {
                return "5";
            }
            else if (questionTextLength > 100 && questionTextLength < 121)
            {
                return "5.5";
            }
            else if (questionTextLength > 120 && questionTextLength < 141)
            {
                return "6";
            }
            else if (questionTextLength > 140 && questionTextLength < 161)
            {
                return "6.5";
            }
            else if (questionTextLength > 160 && questionTextLength < 181)
            {
                return "7";
            }
            else if (questionTextLength > 180 && questionTextLength < 201)
            {
                return "8";
            }
            else if (questionTextLength > 200 && questionTextLength < 221)
            {
                return "9";
            }
            else
            {
                return "10";
            }
        }

        private string calculateRecommendedAnsweringTime(int answerTextTotalLength)
        {
            if (answerTextTotalLength < 4)
            {
                return "10";
            }
            else if (answerTextTotalLength > 3 && answerTextTotalLength < 25)
            {
                return "12";
            }
            else if (answerTextTotalLength > 24 && answerTextTotalLength < 50)
            {
                return "13";
            }
            else if (answerTextTotalLength > 49 && answerTextTotalLength < 75)
            {
                return "14";
            }
            else if (answerTextTotalLength > 74 && answerTextTotalLength < 100)
            {
                return "15";
            }
            else if (answerTextTotalLength > 99 && answerTextTotalLength < 120)
            {
                return "16";
            }
            else if (answerTextTotalLength > 119 && answerTextTotalLength < 130)
            {
                return "17";
            }
            else if (answerTextTotalLength > 129 && answerTextTotalLength < 150)
            {
                return "18";
            }
            else if (answerTextTotalLength > 149 && answerTextTotalLength < 165)
            {
                return "19";
            }
            else if (answerTextTotalLength > 164 && answerTextTotalLength < 185)
            {
                return "20";
            }
            else if (answerTextTotalLength > 184 && answerTextTotalLength < 200)
            {
                return "21";
            }
            else if (answerTextTotalLength > 199 && answerTextTotalLength < 225)
            {
                return "22";
            }
            else if (answerTextTotalLength > 224 && answerTextTotalLength < 250)
            {
                return "23";
            }
            else if (answerTextTotalLength > 249 && answerTextTotalLength < 270)
            {
                return "24";
            }
            else
            {
                return "25";
            }
        }

        public void EditQuestion(String questionId, int questionType, String questionContent, String answer1Content, String answer2Content, String answer3Content, String answer4Content, String oriImageMD5, String oriImageUrl, String imgBackgroundColorCode, float readingTime, float answeringTime, int level, int scoreMultiple, int questionStatus, Panel plEditAction01, Panel plEditAction02, Panel plEditAction03)
        {
            #region Step 1. Check data
            String errorMsg = string.Empty;
            String imgBase64String = String.Empty;
            String imgFormat = String.Empty;
            Boolean isNewImg = false;
            String imageUrl = String.Empty;
            int imgWidth = 0;
            int imgHeight = 0;
            if (String.IsNullOrEmpty(questionContent.Trim()))
            {
                errorMsg = "Please fill in a question.";
                SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                return;
            }

            if (String.IsNullOrEmpty(answer1Content.Trim()))
            {
                errorMsg = "Please fill the correct answer";
                SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                return;
            }

            if (String.IsNullOrEmpty(answer2Content.Trim()))
            {
                errorMsg = "Please fill the answer 02.";
                SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                return;
            }

            if (String.IsNullOrEmpty(answer3Content.Trim()))
            {
                errorMsg = "Please fill the answer 03.";
                SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                return;
            }

            if (String.IsNullOrEmpty(answer4Content.Trim()))
            {
                errorMsg = "Please fill the answer 04.";
                SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                return;
            }

            switch (questionType)
            {
                case QUESTION_TYPE_1:
                    if (questionContent.Length > countOfType1Question || answer1Content.Length > countOfType1Answer || answer2Content.Length > countOfType1Answer || answer3Content.Length > countOfType1Answer || answer4Content.Length > countOfType1Answer)
                    {
                        errorMsg = "The content length should be kept within the text limit";
                        SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                        return;
                    }
                    break;

                case QUESTION_TYPE_2:
                    if (questionContent.Length > countOfType2Question || answer1Content.Length > countOfType2Answer || answer2Content.Length > countOfType2Answer || answer3Content.Length > countOfType2Answer || answer4Content.Length > countOfType2Answer)
                    {
                        errorMsg = "The content length should be kept within the text limit";
                        SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                        return;
                    }
                    break;

                case QUESTION_TYPE_3:
                    if (questionContent.Length > countOfType3Question || answer1Content.Length > countOfType3Answer || answer2Content.Length > countOfType3Answer || answer3Content.Length > countOfType3Answer || answer4Content.Length > countOfType3Answer)
                    {
                        errorMsg = "The content length should be kept within the text limit";
                        SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                        return;
                    }
                    imageUrl = oriImageUrl;

                    #region Check change new image
                    if (!String.IsNullOrEmpty(hfEditQuestionImg.Value))
                    {
                        isNewImg = Convert.ToBoolean(hfEditQuestionImgIsNew.Value);
                    }

                    if (isNewImg)
                    {
                        imgBase64String = hfEditQuestionImg.Value.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                        string dummyData = imgBase64String.Trim().Replace("%", "").Replace(",", "").Replace(" ", "+");
                        if (dummyData.Length % 4 > 0)
                        {
                            dummyData = dummyData.PadRight(dummyData.Length + 4 - dummyData.Length % 4, '=');
                        }
                        imgBase64String = dummyData;

                        imgFormat = hfEditQuestionImgExt.Value.Replace("image/", "");
                        imgWidth = Convert.ToInt16(hfEditQuestionImgWidth.Value);
                        imgHeight = Convert.ToInt16(hfEditQuestionImgHeight.Value);

                        if (String.IsNullOrEmpty(imgBase64String) || String.IsNullOrEmpty(imgFormat))
                        {
                            errorMsg = "Please choose a image for question.";
                            SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                            return;
                        }
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(imageUrl))
                        {
                            errorMsg = "Please choose a image for question.";
                            SetEditQuestionTemplate(questionType, plEditAction01, plEditAction02, plEditAction03);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                            return;
                        }
                    }

                    #endregion
                    break;

                default:
                    break;
            }
            #endregion

            #region Step 2. Upload question image to S3. (original, large, medium and small size)

            StringBuilder sbMD5 = new StringBuilder(oriImageMD5);


            if (questionType == 3 && isNewImg && !String.IsNullOrEmpty(imgBase64String) && !String.IsNullOrEmpty(imgFormat))
            {
                String PROCESS_TIMESTAMP = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                #region Step 2.1 Get AWS Cognito token and RoleArn from WebAPI server.
                GetCognitoTokenRequest request = new GetCognitoTokenRequest();
                request.CompanyId = ViewState["company_id"].ToString();
                request.UserId = ViewState["admin_user_id"].ToString();
                byte[] postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(request));
                HttpWebRequest httpRequest = HttpWebRequest.Create(WebConfigurationManager.AppSettings["WebAPI_Url_GetCognitoToken"].ToString()) as HttpWebRequest;
                httpRequest.Method = "POST";
                httpRequest.Timeout = 30000;
                httpRequest.ContentType = "application/json";
                httpRequest.ContentLength = postData.Length;
                using (Stream st = httpRequest.GetRequestStream())
                {
                    st.Write(postData, 0, postData.Length);
                }
                string result = "";
                using (HttpWebResponse httpResponse = httpRequest.GetResponse() as HttpWebResponse)
                {
                    using (StreamReader sr = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }

                App_Code.ServiceResponses.GetCognitoTokenResponse response = JsonConvert.DeserializeObject<App_Code.ServiceResponses.GetCognitoTokenResponse>(result);
                #endregion

                #region Step 2.2. Create IAmazonS3 by Cognito token and RoleArn.
                AnonymousAWSCredentials cred = new AnonymousAWSCredentials();
                AmazonSecurityTokenServiceClient stsClient = new AmazonSecurityTokenServiceClient(cred, RegionEndpoint.APSoutheast1);
                AssumeRoleWithWebIdentityRequest stsReq = new AssumeRoleWithWebIdentityRequest();
                stsReq.RoleArn = response.RoleArn;
                stsReq.RoleSessionName = "AdminWebsiteSession";
                stsReq.WebIdentityToken = response.Token;
                AssumeRoleWithWebIdentityResponse stsResp = stsClient.AssumeRoleWithWebIdentity(stsReq);
                IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(stsResp.Credentials, RegionEndpoint.APSoutheast1);
                #endregion

                #region Step 2.3. Checking if the folder exists. (/cocadre-{CompanyId}/topics/{TopicId}/{QuestionId}/{yyyyMMddHHmmssfff.jpg})
                String bucketName = "cocadre-" + ViewState["company_id"].ToString().ToLower();
                String folderName = "topics/" + ViewState["topic_id"].ToString() + "/" + questionId;

                ListObjectsRequest findFolderRequest = new ListObjectsRequest();
                findFolderRequest.BucketName = bucketName;
                findFolderRequest.Delimiter = "/";
                findFolderRequest.Prefix = folderName;
                ListObjectsResponse findFolderResponse = s3Client.ListObjects(findFolderRequest);
                List<String> commonPrefixes = findFolderResponse.CommonPrefixes;
                Boolean folderExists = commonPrefixes.Any();
                #endregion

                #region Step 2.4. 如果沒有資料夾，則建立資料夾。
                if (!folderExists)
                {
                    PutObjectRequest folderRequest = new PutObjectRequest();

                    folderRequest.BucketName = bucketName;
                    String folderKey = folderName + "/";
                    folderRequest.Key = folderKey;
                    folderRequest.InputStream = new MemoryStream(new byte[0]);
                    PutObjectResponse folderResponse = s3Client.PutObject(folderRequest);
                }
                #endregion

                #region Step 2.5. 上傳原始圖檔。
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                MemoryStream ms = new MemoryStream(imgBytes);
                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);

                String fileName = PROCESS_TIMESTAMP + "_original" + "." + imgFormat.ToLower();
                metadatas.Clear();
                metadatas.Add("Width", Convert.ToString(imgWidth));
                metadatas.Add("Height", Convert.ToString(imgHeight));
                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                #endregion

                #region Step 2.6. 如果原圖比 1920 * 1920 還大，就做 1920 * 1920 的縮圖。否則直接存原圖。
                System.Drawing.Image newImage;
                if (imgWidth > 1920 || imgHeight > 1920)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                    fileName = PROCESS_TIMESTAMP + "_large" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = PROCESS_TIMESTAMP + "_large" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion

                #region Step 2.7. 如果原圖比 1080 * 1080 還大，就做 1080 * 1080 的縮圖。否則直接存原圖。
                if (imgWidth > 1080 || imgHeight > 1080)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                    fileName = PROCESS_TIMESTAMP + "_medium" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = PROCESS_TIMESTAMP + "_medium" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion

                #region Step 2.8. 如果原圖比 540 * 540 還大，就做 540 * 540 的縮圖。否則直接存原圖。
                if (imgWidth > 540 || imgHeight > 540)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                    fileName = PROCESS_TIMESTAMP + "_small" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = PROCESS_TIMESTAMP + "_small" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion

                #region Step 2.9. Get profile image url
                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + PROCESS_TIMESTAMP + "_original." + imgFormat.ToLower();
                #endregion

                #region Step 3.0 Get
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                byte[] hash = md5.ComputeHash(imgBytes);

                for (int i = 0; i < hash.Length; i++)
                {
                    sbMD5.Append(hash[i].ToString("X2"));
                }
                #endregion
            }
            #endregion

            #region Step 3. Call API
            QuestionUpdateResponse questionUpdateResponse = asc.UpdateQuestion(
                                              ViewState["admin_user_id"].ToString(),
                                              ViewState["company_id"].ToString(),
                                              questionId,
                                              ViewState["topic_id"].ToString(),
                                              hfCategoryId.Value.ToString(),
                                              questionType,
                                              questionContent,
                                              imageUrl,
                                              sbMD5.ToString(),
                                              imgBackgroundColorCode,
                                              1,
                                              answer1Content,
                                              null,
                                              null,
                                              answer2Content,
                                              null,
                                              null,
                                              answer3Content,
                                              null,
                                              null,
                                              answer4Content,
                                              null,
                                              null,
                                              readingTime,
                                              answeringTime,
                                              level,
                                              scoreMultiple,
                                              1,
                                              questionStatus
                                              );


            #endregion

            #region Step 4. Repsonse
            if (questionUpdateResponse.Success)
            {
                hfEditQuestionImg.Value = String.Empty;
                hfEditQuestionImgWidth.Value = String.Empty;
                hfEditQuestionImgHeight.Value = String.Empty;
                hfEditQuestionImgExt.Value = String.Empty;
                hfEditQuestionImgIsNew.Value = String.Empty;

                if (questionUpdateResponse.ErrorCode == -03003)
                {
                    errorMsg = "Question updated successfully. Status not updated.";
                }
                else if (questionUpdateResponse.ErrorCode == -13003)
                {
                    errorMsg = questionUpdateResponse.ErrorMessage;
                }
                else
                {
                    errorMsg = "Question updated successfully.";
                }

                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                GetTopicDetail();
                listQns.DataSource = questions;
                listQns.DataBind();
                ChangeProgressBar();
            }
            else
            {
                errorMsg = "Question failed to update.";
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + errorMsg + "');HideProgressBar();", true);
                Log.Debug("CreateNewQuestion GG. Error: " + questionUpdateResponse.ErrorMessage);
            }
            #endregion
        }

        protected void lbDeleteQuestion_Click(object sender, EventArgs e)
        {
            try
            {
                String questionId = ViewState["QuestionId"].ToString();
                if (!String.IsNullOrEmpty(questionId))
                {
                    String msg = String.Empty;
                    QuestionUpdateResponse response = asc.UpdateQuestionStatus(ViewState["admin_user_id"].ToString(), ViewState["company_id"].ToString(), ViewState["topic_id"].ToString(), hfCategoryId.Value.ToString(), questionId, -1);
                    if (response.Success)
                    {
                        msg = "Question deleted successfully.";
                        mpeDeleteQuestion.Hide();
                        GetTopicDetail();
                        listQns.DataSource = questions;
                        listQns.DataBind();
                        ChangeProgressBar();
                    }
                    else
                    {
                        Log.Debug("Delete Question failed. Error: " + response.ErrorMessage);
                        msg = "Question failed to delete. You do not meet the required number of active questions.";
                    }
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "toastr.info('" + msg + "'); HideProgressBar();", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
        protected override void OnError(EventArgs e)
        {
            if (Global.IsMaxRequestExceededException(this.Server.GetLastError()))
            {
                this.Server.ClearError();
                this.Server.Transfer("FileSizeTooBig");
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GetTopicDetail();
                listQns.DataSource = questions;
                listQns.DataBind();
                plNewQuestion.Visible = false;
                lbNewQuestion.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GetTopicDetail();
                listQns.DataSource = questions;
                listQns.DataBind();
                plNewQuestion.Visible = false;
                lbNewQuestion.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}