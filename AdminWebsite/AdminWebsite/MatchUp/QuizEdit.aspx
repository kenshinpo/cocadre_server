﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="QuizEdit.aspx.cs" Inherits="AdminWebsite.MatchUp.QuizEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script src="/js/MatchUp/QuizEdit.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            TabFunction();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            TabFunction();
        });

        (function ($) {
            "use strict";

            $(document).ready(function () {
                $("#btnExport").prop("href", "/Api/MatchUp/Export?ManagerId=" + $("#main_content_hfManagerId").val() + "&CompanyId=" + $("#main_content_hfCompanyId").val() + "&TopicId=" + $("#main_content_hfTopicId").val());
            });
        }(jQuery));
    </script>

    <asp:HiddenField ID="hfNewQuestionImg" runat="server" />
    <asp:HiddenField ID="hfNewQuestionImgWidth" runat="server" />
    <asp:HiddenField ID="hfNewQuestionImgHeight" runat="server" />
    <asp:HiddenField ID="hfNewQuestionImgExt" runat="server" />
    <asp:HiddenField ID="hfEditQuestionImg" runat="server" />
    <asp:HiddenField ID="hfEditQuestionImgWidth" runat="server" />
    <asp:HiddenField ID="hfEditQuestionImgHeight" runat="server" />
    <asp:HiddenField ID="hfEditQuestionImgExt" runat="server" />
    <asp:HiddenField ID="hfEditQuestionImgIsNew" runat="server" />
    <asp:HiddenField runat="server" ID="hfTopicId" />
    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />

    <asp:LinkButton ID="lbEmpty" runat="server"></asp:LinkButton>
    <asp:LinkButton ID="lbEmpty1" runat="server"></asp:LinkButton>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Match Up <span>console</span></div>
        <div class="appbar__meta">Edit Topic</div>
        <div class="appbar__action" style="width: 600px;">
            <div style="font-size: small;">
                <%--
                <label style="margin: 0px; display: inline-block;">
                    <span class="btn" style="border: 0px currentColor; border-image: none; width: 180px; color: rgba(87, 87, 87, 1); background-color: rgba(230, 230, 230, 1);"><i class="fa fa-plus" aria-hidden="true"></i>UPLOAD ZIP</span>
                    <input class="choiceFile" type="file" accept=".zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed" style="display: none;" onchange="choiceZipFile(this);" />
                </label>
                <a class="btn" id="btnImport" style="margin: 0px; vertical-align: middle; float: none; display: inline-block;" href="javascript:importMatchUpQuestions();">Import Questions</a>
                <a class="btn" id="btnExport" style="margin: 0px; vertical-align: middle; float: none; display: inline-block;">Export</a>
                    --%>
            </div>
        </div>
    </div>
    <!-- / App Bar -->

    <!-- Floating Action Button -->
    <asp:UpdatePanel ID="upAddQuestion" runat="server">
        <ContentTemplate>
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <asp:LinkButton ID="lbNewQuestion" runat="server" CssClass="mfb-component__button--main" data-mfb-label="All questions and answers will be randomized." OnClick="lbNewQuestion_Click" OnClientClick="collapseMe();ResetNewQuestionSetting();">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-file-text-o"></i>
                    </asp:LinkButton>
                </li>
            </ul>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- /Floating Action Button -->

    <!-- Reveal: Add Topic Icon -->
    <asp:UpdatePanel ID="upAddIcon" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="popup_addtopicicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Topic Icon</h1>
                <%--<div class="popup__content">
                        <fieldset class="form">
                            <div class="grid">
                                <div class="grid__inner">
                                    <div class="grid__span grid__span--6">
                                        <a href="#popup_addtopicicon_url" class="btn secondary popup--addphoto__web popup-with-form"><i class="fa fa-globe"></i>Add Photo from Web</a>
                                    </div>
                                    <div class="grid__span grid__span--6 grid__span--last">
                                        <a href="#popup_addtopicicon_selected" class="btn popup--addphoto__local popup-with-form"><i class="fa fa-plus"></i>Upload Photo</a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>--%>
                <div class="topicicons">
                    <p>Select from list</p>
                    <asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand">
                        <HeaderTemplate>
                            <div class="topicicons__list">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <label class="topicicons__icon">
                                <asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
                                    <asp:Image ID="imgChooseIcon" runat="server" />
                                </asp:LinkButton>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="popup__action">
                    <%--<asp:LinkButton ID="lbAddIconSave" CssClass="popup__action__item popup__action__item--cta" runat="server">Save</asp:LinkButton>--%>
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbAddCancel_Click">Cancel</asp:LinkButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ajaxToolkit:ModalPopupExtender ID="mpeAddIcon" runat="server"
        TargetControlID="lbEmpty"
        PopupControlID="popup_addtopicicon"
        BackgroundCssClass="mfp-bg"
        DropShadow="false">
    </ajaxToolkit:ModalPopupExtender>
    <!-- / Reveal: Add Topic Icon -->

    <!-- Reveal: Delete Question -->
    <asp:UpdatePanel ID="upDeleteQuestion" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="popup_deletequestion" runat="server" CssClass="popup popup--deletequestion" Width="100%" Style="display: none;">
                <asp:HiddenField ID="hfQuestionId" runat="server" />
                <h1 class="popup__title">Delete Question</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="label">Confirm deletion of question?</div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDeleteQuestion" runat="server" CssClass="popup__action__item popup__action__item--confirm" OnClick="lbDeleteQuestion_Click" OnClientClick="ShowProgressBar('Deleting...');">Delete</asp:LinkButton>
                    <asp:LinkButton ID="lbDeleteCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" OnClick="lbDeleteCancel_Click">Cancel</asp:LinkButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ajaxToolkit:ModalPopupExtender ID="mpeDeleteQuestion" runat="server"
        TargetControlID="lbEmpty1"
        PopupControlID="popup_deletequestion"
        BackgroundCssClass="mfp-bg"
        DropShadow="false">
    </ajaxToolkit:ModalPopupExtender>
    <!-- / Reveal: Delete Question -->

    <div class="data">
        <div class="data__content" onscroll="sticky_div();">
            <!-- Edit  Topic -->
            <asp:UpdatePanel ID="upEditTopic" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="container" style="">
                        <div class="card add-topic">
                            <div class="add-topic__icon">
                                <asp:UpdatePanel ID="upEditTopicIcon" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
                                            <asp:Image ID="imgTopic" ImageUrl="~/img/icon-topicicon-default.png" runat="server" />
                                            <label><small>Choose topic icon</small></label>
                                        </asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="add-topic__info">
                                <asp:HiddenField ID="hfTopicQuestionCount" runat="server" />
                                <asp:UpdatePanel ID="upProgress" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="lbNewQuestionSave" />
                                    </Triggers>
                                    <ContentTemplate>

                                        <div class="add-topic__info__progress">
                                            <asp:Panel ID="plProgress" runat="server" CssClass="topic__info__progress__bar progress-bar-indication">
                                                <asp:Label ID="lblProgress" runat="server" CssClass="meter" />
                                                <p>
                                                    <asp:Literal ID="ltlProgress" runat="server" />
                                                </p>
                                            </asp:Panel>
                                            <asp:Literal ID="ltlRecommendedQns" runat="server" />
                                        </div>

                                        <div class="add-topic__info__action">
                                            <asp:LinkButton ID="lbUpdate" runat="server" CssClass="btn" Text="Apply Changes" OnClick="lbUpdate_Click" OnClientClick="ShowProgressBar();" />
                                            <asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/MatchUp/TopicList" Text="Cancel" CssClass="btn secondary" />
                                        </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <div class="add-topic__info__details">
                                    <div class="tabs tabs--styled">
                                        <ul class="tabs__list">
                                            <li class="tabs__list__item">
                                                <a class="tabs__link" href="#topic-basic-info">Basic Info</a>
                                                <%--<asp:LinkButton ID="lbBasicInfo" runat="server" Text="Basic Info" CssClass="tabs__link active" OnClick="lbBasicInfo_Click" />--%>
                                            </li>
                                            <li class="tabs__list__item">
                                                <a class="tabs__link" href="#topic-department">Privacy</a>
                                                <%--<asp:LinkButton ID="lbDepartment" runat="server" Text="Privacy" CssClass="tabs__link" OnClick="lbDepartment_Click" />--%>
                                            </li>
                                            <li class="tabs__list__item">
                                                <a class="tabs__link" href="#topic-settings">Settings</a>
                                                <%--<asp:LinkButton ID="lbSettings" runat="server" Text="Settings" CssClass="tabs__link" OnClick="lbSettings_Click" />--%>
                                            </li>
                                        </ul>
                                        <div class="tabs__panels">
                                            <div class="tabs__panels__item add-topic__info__details--basicinfo" id="topic-basic-info">
                                                <%--<asp:Panel ID="plBasicInfo" runat="server" CssClass="tabs__panels__item add-topic__info__details--basicinfo" Visible="true">--%>
                                                <div class="column--input">
                                                    <div class="form__row">
                                                        Topic Title:
                                                <asp:TextBox ID="tbTopictitle" runat="server" placeholder="Topic Title" MaxLength="20" />
                                                    </div>
                                                    <div class="form__row">
                                                        Topic Description:
                                                <asp:TextBox ID="tbTopicDescription" runat="server" MaxLength="120" placeholder="Description" />
                                                    </div>
                                                </div>
                                                <div class="column--choice">
                                                    <div class="form__row">
                                                        <label>Category</label>
                                                        <asp:HiddenField ID="hfCategoryId" runat="server" />
                                                        <ajaxToolkit:ComboBox ID="cbCategory" runat="server" AutoCompleteMode="Suggest" CssClass="combos" MaxLength="20" />
                                                    </div>
                                                    <div class="form__row">
                                                        <label>Status</label>
                                                        <asp:HiddenField ID="hfTopicStatus" runat="server" />
                                                        <div class="mdl-selectfield">
                                                            <asp:DropDownList ID="ddlTopicStatus" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--</asp:Panel>--%>
                                            </div>
                                            <div class="tabs__panels__item add-topic__info__details--department" id="topic-department">
                                                <%--<asp:Panel ID="plDepartment" runat="server" CssClass="tabs__panels__item add-topic__info__details--department" Visible="false">--%>
                                                <asp:UpdatePanel ID="plCheckDepartment" runat="server" Class="departments-list--asp">
                                                    <ContentTemplate>
                                                        <asp:RadioButton ID="rbPrivacyEveryone" Text="Everyone" GroupName="PrivacySetting" runat="server" AutoPostBack="true" OnCheckedChanged="rbPrivacyEveryone_CheckedChanged" /><br />
                                                        <p>Topic can be seen by everyone including newly added departments.</p>
                                                        <asp:RadioButton ID="rbPrivacyDepartment" Text="Selected department only" GroupName="PrivacySetting" runat="server" AutoPostBack="true" OnCheckedChanged="rbPrivacyDepartment_CheckedChanged" />
                                                        <p>Topic can be seen by department(s) only. Only selected departments will be able to see this.</p>
                                                        <hr />
                                                        <asp:CheckBox ID="cbCheckAllDept" runat="server" Text="Select All" AutoPostBack="true" OnCheckedChanged="cbCheckAllDept_CheckedChanged" />
                                                        <br />
                                                        <asp:CheckBoxList ID="cblDepartment" runat="server" RepeatLayout="Flow" RepeatColumns="6" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="cblDepartment_SelectedIndexChanged" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <%--</asp:Panel>--%>
                                            </div>
                                            <div class="tabs__panels__item add-topic__info__details--settings" id="topic-settings">
                                                <%--<asp:Panel ID="plSettings" runat="server" CssClass="tabs__panels__item add-topic__info__details--settings" Visible="false">--%>
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" Class="departments-list--asp">
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlNoQuestionPerGame" />
                                                    </Triggers>
                                                    <ContentTemplate>
                                                        <div class="form__row">
                                                            <label>No of questions per game</label>
                                                            <div class="mdl-selectfield">
                                                                <asp:DropDownList ID="ddlNoQuestionPerGame" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNoQuestionPerGame_SelectedIndexChanged" />
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <%--</asp:Panel>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- / Edit  Topic -->

            <!-- / Questions  Bar -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <script>
                        function sticky_div() {
                            var window_top = $(window).scrollTop();

                            var divTopId = $('#sticky_anchor');
                            if (!divTopId.length) {
                                return;
                            }
                            var div_top = divTopId.offset().top; // get the offset of the div from the top of page

                            // var div_top = $('#sticky-anchor').offset().top;
                            if (window_top + 160 > div_top) {
                                $('.sticky').addClass('stick');
                                $('#sticky_anchor').addClass('stick');

                            } else {
                                $('.sticky').removeClass('stick');
                                $('#sticky_anchor').removeClass('stick');

                            }
                        }
                    </script>
                    <div id="sticky_anchor"></div>
                    <div class="questions-bar sticky">
                        <div class="container">
                            <%--<div class="questions-bar__status">
                        <div class="questions-bar__status__inner">
                            <label>Show Status</label>
                            <select class="cs-select cs-skin-border">
                                <option value="0" selected="">All</option>
                                <option value="1">Unlisted</option>
                                <option value="2">Active</option>
                                <option value="3">Hide</option>
                            </select>
                        </div>
                    </div>--%>
                            <div class="questions-bar__search">
                                <asp:TextBox ID="tbSearchQuestion" runat="server" placeholder="Search question" CssClass="questions-bar__search__input" />
                                <asp:LinkButton ID="lbSearch" runat="server" CssClass="questions-bar__search__button" OnClick="lbSearch_Click" OnClientClick="ShowProgressBar();">
                                    <i class="fa fa-search"></i>
                                </asp:LinkButton>
                                <%--<asp:Button runat="server" ID="btnSearch" CssClass="questions-bar__search__button" OnClick="btnSearch_Click"><i class="fa fa-search"></i></asp:Button>--%>
                                <%--<button type="submit" class="questions-bar__search__button"><i class="fa fa-search"></i></button>--%>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- / Questions  Bar -->

            <!-- Edit Question -->
            <asp:UpdatePanel ID="upQuestionList" runat="server">
                <ContentTemplate>
                    <style>
                        .hide_min_header {
                            display: none;
                        }
                    </style>
                    <script>
                        function collapseMe() {
                            var behavior = $get("<%=listQns.ClientID%>").AccordionBehavior;
                            behavior.set_SelectedIndex(-1);
                        }
                        function stopOpening(e) {
                            if (!e) var e = window.event;
                            e.cancelBubble = true;
                            if (e.stopPropagation) e.stopPropagation();
                        }
                    </script>

                    <ajaxToolkit:Accordion
                        ID="listQns"
                        runat="Server"
                        SelectedIndex="0"
                        HeaderSelectedCssClass="hide_min_header"
                        ContentCssClass="add-question__info__setup"
                        AutoSize="None"
                        FadeTransitions="false"
                        TransitionDuration="1"
                        FramesPerSecond="40"
                        RequireOpenedPane="false"
                        SuppressHeaderPostbacks="true"
                        OnItemDataBound="listQns_ItemDataBound"
                        OnItemCommand="listQns_ItemCommand">
                        <HeaderTemplate>
                            <div class="container">
                                <div class="card question">
                                    <div class="add-question__questionnumber">
                                        <div class="number">
                                            <asp:Literal ID="ltlMinQNo" runat="server" />
                                        </div>
                                        <label>
                                            Code<br />
                                            <asp:Literal ID="ltlMinQId" runat="server" />
                                        </label>
                                    </div>
                                    <div class="add-question__info">
                                        <div class="add-question__info__header">
                                            <div class="add-question__info__header__action">
                                                <asp:LinkButton ID="lbMinQShow" runat="server" CssClass="btn transparent" CommandName="MaxQuestion">
                                            <i class="fa fa-caret-square-o-down fa-lg icon"></i>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="question__summary">
                                        <div class="question__image">
                                            <asp:Image ID="imgMinQ" runat="server" />
                                        </div>
                                        <div class="question__title">
                                            <label>Question</label>
                                            <asp:Literal ID="ltlMinQContents" runat="server" />
                                        </div>
                                        <div class="question__status">
                                            <label>Status</label>
                                            <div class="mdl-selectfield" style="display: none;">
                                                <asp:DropDownList ID="ddlMinQStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlQuestionStatus_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Active</asp:ListItem>
                                                    <asp:ListItem Value="2">Inactive</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div style="font-weight: bolder;">
                                                <asp:Literal ID="ltlMinQStatus" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <script type="text/javascript">
                                function EditQuestionTextCounter(textBoxId, labelId, maxCount, type, recommendedId) {
                                    var textbox = document.getElementById(textBoxId);
                                    var label = document.getElementById(labelId);
                                    var countLength = maxCount - textbox.value.length;
                                    if (countLength < 0) {
                                        textbox.style.borderColor = "red";
                                    }
                                    else {
                                        textbox.style.borderColor = "";
                                    }
                                    label.innerHTML = countLength;

                                    // Recommended
                                    var lblRecommended = document.getElementById(recommendedId);
                                    if (type == 1) {
                                        if (textbox.value.length < 11) {
                                            lblRecommended.innerHTML = '1.0';
                                        }
                                        else if (textbox.value.length > 10 && textbox.value.length < 21) {
                                            lblRecommended.innerHTML = '1.5';
                                        }
                                        else if (textbox.value.length > 20 && textbox.value.length < 31) {
                                            lblRecommended.innerHTML = '2';
                                        }
                                        else if (textbox.value.length > 30 && textbox.value.length < 41) {
                                            lblRecommended.innerHTML = '2.3';
                                        }
                                        else if (textbox.value.length > 40 && textbox.value.length < 51) {
                                            lblRecommended.innerHTML = '2.5';
                                        }
                                        else if (textbox.value.length > 50 && textbox.value.length < 61) {
                                            lblRecommended.innerHTML = '3';
                                        }
                                        else if (textbox.value.length > 60 && textbox.value.length < 71) {
                                            lblRecommended.innerHTML = '3.5';
                                        }
                                        else if (textbox.value.length > 70 && textbox.value.length < 81) {
                                            lblRecommended.innerHTML = '4';
                                        }
                                        else if (textbox.value.length > 80 && textbox.value.length < 91) {
                                            lblRecommended.innerHTML = '4.5';
                                        }
                                        else if (textbox.value.length > 90 && textbox.value.length < 101) {
                                            lblRecommended.innerHTML = '5';
                                        }
                                        else if (textbox.value.length > 100 && textbox.value.length < 121) {
                                            lblRecommended.innerHTML = '5.5';
                                        }
                                        else if (textbox.value.length > 120 && textbox.value.length < 141) {
                                            lblRecommended.innerHTML = '6';
                                        }
                                        else if (textbox.value.length > 140 && textbox.value.length < 161) {
                                            lblRecommended.innerHTML = '6.5';
                                        }
                                        else if (textbox.value.length > 160 && textbox.value.length < 181) {
                                            lblRecommended.innerHTML = '7';
                                        }
                                        else if (textbox.value.length > 180 && textbox.value.length < 201) {
                                            lblRecommended.innerHTML = '8';
                                        }
                                        else if (textbox.value.length > 200 && textbox.value.length < 221) {
                                            lblRecommended.innerHTML = '9';
                                        }
                                        else {
                                            lblRecommended.innerHTML = '10';
                                        }
                                    }
                                    else if (type == 2) // answering time
                                    {
                                        var tbAnswer1 = document.getElementById(textBoxId.substring(0, textBoxId.length - 3) + "1" + textBoxId.substring(textBoxId.length - 2, textBoxId.length));
                                        var tbAnswer2 = document.getElementById(textBoxId.substring(0, textBoxId.length - 3) + "2" + textBoxId.substring(textBoxId.length - 2, textBoxId.length));
                                        var tbAnswer3 = document.getElementById(textBoxId.substring(0, textBoxId.length - 3) + "3" + textBoxId.substring(textBoxId.length - 2, textBoxId.length));
                                        var tbAnswer4 = document.getElementById(textBoxId.substring(0, textBoxId.length - 3) + "4" + textBoxId.substring(textBoxId.length - 2, textBoxId.length));

                                        var totalTypeCount = tbAnswer1.value.length + tbAnswer2.value.length + tbAnswer3.value.length + tbAnswer4.value.length;

                                        if (totalTypeCount < 4) {
                                            lblRecommended.innerHTML = '10';
                                        }
                                        else if (totalTypeCount > 3 && totalTypeCount < 25) {
                                            lblRecommended.innerHTML = '12';
                                        }
                                        else if (totalTypeCount > 24 && totalTypeCount < 50) {
                                            lblRecommended.innerHTML = '13';
                                        }
                                        else if (totalTypeCount > 49 && totalTypeCount < 75) {
                                            lblRecommended.innerHTML = '14';
                                        }
                                        else if (totalTypeCount > 74 && totalTypeCount < 100) {
                                            lblRecommended.innerHTML = '15';
                                        }
                                        else if (totalTypeCount > 99 && totalTypeCount < 120) {
                                            lblRecommended.innerHTML = '16';
                                        }
                                        else if (totalTypeCount > 119 && totalTypeCount < 130) {
                                            lblRecommended.innerHTML = '17';
                                        }
                                        else if (totalTypeCount > 129 && totalTypeCount < 150) {
                                            lblRecommended.innerHTML = '18';
                                        }
                                        else if (totalTypeCount > 149 && totalTypeCount < 165) {
                                            lblRecommended.innerHTML = '19';
                                        }
                                        else if (totalTypeCount > 164 && totalTypeCount < 185) {
                                            lblRecommended.innerHTML = '20';
                                        }
                                        else if (totalTypeCount > 184 && totalTypeCount < 200) {
                                            lblRecommended.innerHTML = '21';
                                        }
                                        else if (totalTypeCount > 199 && totalTypeCount < 225) {
                                            lblRecommended.innerHTML = '22';
                                        }
                                        else if (totalTypeCount > 224 && totalTypeCount < 250) {
                                            lblRecommended.innerHTML = '23';
                                        }
                                        else if (totalTypeCount > 249 && totalTypeCount < 270) {
                                            lblRecommended.innerHTML = '24';
                                        }
                                        else {
                                            lblRecommended.innerHTML = '25';
                                        }
                                    }
                                }

                                function EditQuestionPreview(textBoxId, labelId) {
                                    var textBox = document.getElementById(textBoxId);
                                    var label = document.getElementById(labelId);
                                    label.innerHTML = textBox.value;
                                }
                            </script>
                            <div class="container">
                                <div class="card add-question">
                                    <div class="add-question__questionnumber">
                                        <div class="number">
                                            <asp:HiddenField ID="hfQuestionType" runat="server" />
                                            <asp:Literal ID="ltlMaxQNo" runat="server" />
                                        </div>
                                        <label>
                                            Code<br />
                                            <asp:Literal ID="ltlMaxQId" runat="server" />
                                        </label>
                                    </div>
                                    <div class="add-question__info">
                                        <div class="add-question__info__header">
                                            <div class="add-question__info__header__action">
                                                <asp:HyperLink ID="lbMaxQShow" runat="server" CssClass="btn transparent" NavigateUrl="javascript:collapseMe();">
                                             <i class="fa fa-caret-square-o-up fa-lg icon"></i>
                                                </asp:HyperLink>
                                                <asp:LinkButton ID="lbMaxSave" runat="server" CssClass="btn" Text="Save" CommandName="EditQuestion" OnClientClick="ShowProgressBar();" />
                                            </div>
                                            <div class="add-question__info__header__title">Display</div>
                                        </div>
                                        <div class="add-question__info__setup">
                                            <div class="add-question__info__setup__questiontype">
                                                <div id="Div1" class="tabs--questionformat">
                                                    <ul class="tabs__list">
                                                        <asp:Panel ID="plEditAction01" runat="server" CssClass="tabs__list__item">
                                                            <asp:LinkButton ID="lbEditQuestionFormat01" runat="server" CommandName="EditQuestionType1">
                                                                <span>Question Format 01</span>
                                                                <asp:Image ID="imgEditFormat01" runat="server" ImageUrl="~/img/qformat01.png" />
                                                            </asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="plEditAction02" runat="server" CssClass="tabs__list__item">
                                                            <asp:LinkButton ID="lbEditQuestionFormat02" runat="server" CommandName="EditQuestionType2">
                                                                <span>Question Format 02</span>
                                                                <asp:Image ID="imgEditFormat02" runat="server" ImageUrl="~/img/qformat02.png" />
                                                            </asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="plEditAction03" runat="server" CssClass="tabs__list__item">
                                                            <asp:LinkButton ID="lbEditQuestionFormat03" runat="server" CommandName="EditQuestionType3">
                                                                <span>Question Format 03</span>
                                                                <asp:Image ID="imgEditFormat03" runat="server" ImageUrl="~/img/qformat03.png" />
                                                            </asp:LinkButton>
                                                        </asp:Panel>
                                                    </ul>
                                                    <div class="tabs__panels">
                                                        <asp:Panel ID="plEditFormat" runat="server" CssClass="tabs__panels__item tabs__panels__item--questionformat01">
                                                            <div class="form__row form__row--question">
                                                                <asp:TextBox ID="tbEditQuestion" placeholder="Question" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                                <asp:Label ID="lblEditQuestionCount" runat="server" CssClass="lettercount" />
                                                            </div>
                                                            <asp:Panel ID="plEditImg" runat="server" Visible="false">
                                                                <div class="backgroundscheme" style="float: none; display: none">
                                                                    <div class="button-group">
                                                                        <label>
                                                                            <input type="radio" name="button-group" value="item" checked="checked" />
                                                                            <span class="button-group-item">White Background</span>
                                                                        </label>
                                                                        <label>
                                                                            <input type="radio" name="button-group" value="other-item" /><span class="button-group-item">Black Background</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="uploadcontrols" style="width: 100%; float: none;">
                                                                    <label id="lblUpload" runat="server" style="cursor: pointer;" class="btn secondary">
                                                                        <asp:FileUpload ID="fuEditQuestion" runat="server" accept="image/*" Style="display: none" />
                                                                        <i class="fa fa-plus"></i>Upload Photo
                                                                    </label>
                                                                </div>
                                                                <br />
                                                            </asp:Panel>
                                                            <div>
                                                                <div class="form__row form__row--answer">
                                                                    <asp:TextBox ID="tbEditAnswer1" placeholder="Correct Answer" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                                    <asp:Label ID="lblEditAnswer1Count" runat="server" CssClass="lettercount" />
                                                                </div>
                                                                <div class="form__row form__row--answer">
                                                                    <asp:TextBox ID="tbEditAnswer2" placeholder="Answer 02" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                                    <asp:Label ID="lblEditAnswer2Count" runat="server" CssClass="lettercount" />
                                                                </div>
                                                                <div class="form__row form__row--answer">
                                                                    <asp:TextBox ID="tbEditAnswer3" placeholder="Answer 03" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                                    <asp:Label ID="lblEditAnswer3Count" runat="server" CssClass="lettercount" />
                                                                </div>
                                                                <div class="form__row form__row--answer">
                                                                    <asp:TextBox ID="tbEditAnswer4" placeholder="Answer 04" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                                    <asp:Label ID="lblEditAnswer4Count" runat="server" CssClass="lettercount" />
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="add-question__info__setup__preview">

                                                <!-- Question Preview Format -->
                                                <asp:Panel ID="plEditPreview" runat="server" CssClass="question-preview">
                                                    <div class="question-preview__header">
                                                        <div class="question-preview__header__score">
                                                            <div class="Media">
                                                                <img class="Media-figure" src="/img/image-01.jpg" alt="" />
                                                                <div class="Preview-body">
                                                                    <span>User 1</span>
                                                                    <div class="score">Score</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="question-preview__header__timer">Time Left <span>10</span></div>
                                                        <div class="question-preview__header__score question-preview__header__score--user02">
                                                            <div class="Media">
                                                                <img class="Media-figure" src="/img/image-01.jpg" alt="" />
                                                                <div class="Preview-body">
                                                                    <span>User 2</span>
                                                                    <div class="score">Score</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="question-preview__question">
                                                        <p>
                                                            <div class="question-preview__content">
                                                                <%--<div style="width: 100%; font-size: 1.25rem;">--%>
                                                                <div style="word-wrap: break-word;">
                                                                    <asp:Label ID="lblEditPreviewQuestion" runat="server" />
                                                                </div>
                                                            </div>
                                                        </p>
                                                    </div>
                                                    <asp:Panel ID="plEditPreviewImg" runat="server" CssClass="question-preview__image" Visible="false">
                                                        <asp:HiddenField ID="hfOriImageMD5" runat="server" />
                                                        <asp:Image ID="imgEditPreviewImg" runat="server" />
                                                    </asp:Panel>
                                                    <%--<div id="edit_question_img" class="question-preview__image" style="display: none">
                                                <img src="/img/image-01.jpg" alt="">
                                            </div>--%>
                                                    <div class="question-preview__answers">
                                                        <div class="question-preview__answer">
                                                            <div style="word-wrap: break-word;">
                                                                <asp:Label ID="lblEditPreviewQuestionAnswer1" runat="server" Style="word-wrap: break-word;" />
                                                            </div>
                                                        </div>
                                                        <div class="question-preview__answer">
                                                            <div style="word-wrap: break-word;">
                                                                <asp:Label ID="lblEditPreviewQuestionAnswer2" runat="server" Style="word-wrap: break-word;" />
                                                            </div>
                                                        </div>
                                                        <div class="question-preview__answer">
                                                            <div style="word-wrap: break-word;">
                                                                <asp:Label ID="lblEditPreviewQuestionAnswer3" runat="server" Style="word-wrap: break-word;" />
                                                            </div>
                                                        </div>
                                                        <div class="question-preview__answer">
                                                            <div style="word-wrap: break-word;">
                                                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                                <asp:Label ID="lblEditPreviewQuestionAnswer4" runat="server" Style="word-wrap: break-word;" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <!-- / Question Preview Format -->

                                            </div>
                                            <div class="add-question__info__setup__choice">
                                                <div class="form__row">
                                                    <label style="font-weight: bold;">
                                                        Recommended Reading Time:
                                                        <asp:Label ID="lblEditQuestionRecommendedReadingTime" runat="server" />
                                                        s</label>
                                                    <label>Reading Time</label>
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlEditQuestionReadingTime" runat="server">
                                                            <asp:ListItem Text="1.0 seconds" Value="1.0" />
                                                            <asp:ListItem Text="1.3 seconds" Value="1.3" />
                                                            <asp:ListItem Text="1.5 seconds" Value="1.5" />
                                                            <asp:ListItem Text="1.7 seconds" Value="1.7" />
                                                            <asp:ListItem Text="1.9 seconds" Value="1.9" />
                                                            <asp:ListItem Text="2.0 seconds" Value="2.0" />
                                                            <asp:ListItem Text="2.1 seconds" Value="2.1" />
                                                            <asp:ListItem Text="2.3 seconds" Value="2.3" />
                                                            <asp:ListItem Text="2.5 seconds" Value="2.5" />
                                                            <asp:ListItem Text="3.0 seconds" Value="3.0" />
                                                            <asp:ListItem Text="3.5 seconds" Value="3.5" />
                                                            <asp:ListItem Text="4.0 seconds" Value="4.0" />
                                                            <asp:ListItem Text="4.5 seconds" Value="4.5" />
                                                            <asp:ListItem Text="5.0 seconds" Value="5.0" />
                                                            <asp:ListItem Text="5.5 seconds" Value="5.5" />
                                                            <asp:ListItem Text="6.0 seconds" Value="6.0" />
                                                            <asp:ListItem Text="6.5 seconds" Value="6.5" />
                                                            <asp:ListItem Text="7.0 seconds" Value="7.0" />
                                                            <asp:ListItem Text="8.0 seconds" Value="8.0" />
                                                            <asp:ListItem Text="9.0 seconds" Value="9.0" />
                                                            <asp:ListItem Text="10.0 seconds" Value="10.0" />
                                                            <asp:ListItem Text="11.0 seconds" Value="11.0" />
                                                            <asp:ListItem Text="12.0 seconds" Value="12.0" />
                                                            <asp:ListItem Text="13.0 seconds" Value="13.0" />
                                                            <asp:ListItem Text="14.0 seconds" Value="14.0" />
                                                            <asp:ListItem Text="15.0 seconds" Value="15.0" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <label style="font-weight: bold;">
                                                        Recommended Answering Time:
                                                        <asp:Label ID="lblEditQuestionRecommendedAnsweringTime" runat="server" />
                                                        s</label>
                                                    <label>Answering Time</label>
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlEditQuestionAnsweringTime" runat="server">
                                                            <asp:ListItem Text="10 seconds" Value="10" />
                                                            <asp:ListItem Text="12 seconds" Value="12" />
                                                            <asp:ListItem Text="13 seconds" Value="13" />
                                                            <asp:ListItem Text="14 seconds" Value="14" />
                                                            <asp:ListItem Text="15 seconds" Value="15" />
                                                            <asp:ListItem Text="16 seconds" Value="16" />
                                                            <asp:ListItem Text="17 seconds" Value="17" />
                                                            <asp:ListItem Text="18 seconds" Value="18" />
                                                            <asp:ListItem Text="19 seconds" Value="19" />
                                                            <asp:ListItem Text="20 seconds" Value="20" />
                                                            <asp:ListItem Text="21 seconds" Value="21" />
                                                            <asp:ListItem Text="22 seconds" Value="22" />
                                                            <asp:ListItem Text="23 seconds" Value="23" />
                                                            <asp:ListItem Text="24 seconds" Value="24" />
                                                            <asp:ListItem Text="25 seconds" Value="25" />
                                                            <asp:ListItem Text="30 seconds" Value="30" />
                                                            <asp:ListItem Text="35 seconds" Value="35" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <label>Difficulty</label>
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlEditQuestionDifficulty" runat="server">
                                                            <asp:ListItem Text="Easy" Value="1" />
                                                            <asp:ListItem Text="Normal" Value="2" />
                                                            <asp:ListItem Text="Hard - End Game x2 score" Value="3" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <label>Scoring</label>
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlEditQuestionScoring" runat="server">
                                                            <asp:ListItem Text="Default - 1x Score" Value="1" />
                                                            <asp:ListItem Text="2x Original Score" Value="2" />
                                                            <asp:ListItem Text="3x Original Score" Value="3" />
                                                            <asp:ListItem Text="4x Original Score" Value="4" />
                                                            <asp:ListItem Text="5x Original Score" Value="5" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <label>Status</label>
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlEditQuestionStatus" runat="server">
                                                            <asp:ListItem Text="Active" Value="1" />
                                                            <asp:ListItem Text="Inactive" Value="2" />
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <asp:LinkButton ID="lbEditDelete" runat="server" CssClass="btn confirm right" CommandName="DeleteQuestion" Text="Delete Question" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:Accordion>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- / Edit Question -->



            <!-- New  Question -->
            <asp:UpdatePanel ID="upNewQuestion" runat="server" ChildrenAsTriggers="true" UpdateMode="Always">
                <ContentTemplate>
                    <script type="text/javascript">
                        function NewQuestionTextCounter(textBoxId, labelId, maxCount, type, recommendedId) {
                            var textbox = document.getElementById(textBoxId);
                            var label = document.getElementById(labelId);
                            var countLength = maxCount - textbox.value.length;
                            if (countLength < 0) {
                                textbox.style.borderColor = "red";
                            }
                            else {
                                textbox.style.borderColor = "";
                            }
                            label.innerHTML = countLength;

                            // Recommended
                            var span = document.getElementById(recommendedId);
                            if (type == 1) {
                                if (textbox.value.length < 11) {
                                    span.innerHTML = '1.0';
                                }
                                else if (textbox.value.length > 10 && textbox.value.length < 21) {
                                    span.innerHTML = '1.5';
                                }
                                else if (textbox.value.length > 20 && textbox.value.length < 31) {
                                    span.innerHTML = '2';
                                }
                                else if (textbox.value.length > 30 && textbox.value.length < 41) {
                                    span.innerHTML = '2.3';
                                }
                                else if (textbox.value.length > 40 && textbox.value.length < 51) {
                                    span.innerHTML = '2.5';
                                }
                                else if (textbox.value.length > 50 && textbox.value.length < 61) {
                                    span.innerHTML = '3';
                                }
                                else if (textbox.value.length > 60 && textbox.value.length < 71) {
                                    span.innerHTML = '3.5';
                                }
                                else if (textbox.value.length > 70 && textbox.value.length < 81) {
                                    span.innerHTML = '4';
                                }
                                else if (textbox.value.length > 80 && textbox.value.length < 91) {
                                    span.innerHTML = '4.5';
                                }
                                else if (textbox.value.length > 90 && textbox.value.length < 101) {
                                    span.innerHTML = '5';
                                }
                                else if (textbox.value.length > 100 && textbox.value.length < 121) {
                                    span.innerHTML = '5.5';
                                }
                                else if (textbox.value.length > 120 && textbox.value.length < 141) {
                                    span.innerHTML = '6';
                                }
                                else if (textbox.value.length > 140 && textbox.value.length < 161) {
                                    span.innerHTML = '6.5';
                                }
                                else if (textbox.value.length > 160 && textbox.value.length < 181) {
                                    span.innerHTML = '7';
                                }
                                else if (textbox.value.length > 180 && textbox.value.length < 201) {
                                    span.innerHTML = '8';
                                }
                                else if (textbox.value.length > 200 && textbox.value.length < 221) {
                                    span.innerHTML = '9';
                                }
                                else {
                                    span.innerHTML = '10';
                                }
                            }
                            else if (type == 2) // answering time
                            {
                                //textBoxId = main_content_tbAnswer1, main_content_tbAnswer2, main_content_tbAnswer3, main_content_tbAnswer4
                                var tbAnswer1 = document.getElementById(textBoxId.substring(0, textBoxId.length - 1) + "1");
                                var tbAnswer2 = document.getElementById(textBoxId.substring(0, textBoxId.length - 1) + "2");
                                var tbAnswer3 = document.getElementById(textBoxId.substring(0, textBoxId.length - 1) + "3");
                                var tbAnswer4 = document.getElementById(textBoxId.substring(0, textBoxId.length - 1) + "4");

                                var totalTypeCount = tbAnswer1.value.length + tbAnswer2.value.length + tbAnswer3.value.length + tbAnswer4.value.length;

                                if (totalTypeCount < 4) {
                                    span.innerHTML = '10';
                                }
                                else if (totalTypeCount > 3 && totalTypeCount < 25) {
                                    span.innerHTML = '12';
                                }
                                else if (totalTypeCount > 24 && totalTypeCount < 50) {
                                    span.innerHTML = '13';
                                }
                                else if (totalTypeCount > 49 && totalTypeCount < 75) {
                                    span.innerHTML = '14';
                                }
                                else if (totalTypeCount > 74 && totalTypeCount < 100) {
                                    span.innerHTML = '15';
                                }
                                else if (totalTypeCount > 99 && totalTypeCount < 120) {
                                    span.innerHTML = '16';
                                }
                                else if (totalTypeCount > 119 && totalTypeCount < 130) {
                                    span.innerHTML = '17';
                                }
                                else if (totalTypeCount > 129 && totalTypeCount < 150) {
                                    span.innerHTML = '18';
                                }
                                else if (totalTypeCount > 149 && totalTypeCount < 165) {
                                    span.innerHTML = '19';
                                }
                                else if (totalTypeCount > 164 && totalTypeCount < 185) {
                                    span.innerHTML = '20';
                                }
                                else if (totalTypeCount > 184 && totalTypeCount < 200) {
                                    span.innerHTML = '21';
                                }
                                else if (totalTypeCount > 199 && totalTypeCount < 225) {
                                    span.innerHTML = '22';
                                }
                                else if (totalTypeCount > 224 && totalTypeCount < 250) {
                                    span.innerHTML = '23';
                                }
                                else if (totalTypeCount > 249 && totalTypeCount < 270) {
                                    span.innerHTML = '24';
                                }
                                else {
                                    span.innerHTML = '25';
                                }

                            }
                        }

                        function NewQuestionPreview(textBoxId, type) {
                            var textBox = document.getElementById(textBoxId);
                            var content;
                            switch (type) {
                                case 0:
                                    content = document.getElementById('new_question_content');
                                    break;
                                case 1:
                                    content = document.getElementById('new_question_answer1');
                                    break;
                                case 2:
                                    content = document.getElementById('new_question_answer2');
                                    break;
                                case 3:
                                    content = document.getElementById('new_question_answer3');
                                    break;
                                case 4:
                                    content = document.getElementById('new_question_answer4');
                                    break;
                                default:
                                    break;
                            }
                            content.innerHTML = textBox.value;
                        }

                        function ResetNewQuestionSetting() {
                            var hfNewQuestionImg = $get("<%=hfNewQuestionImg.ClientID%>");
                            var hfNewQuestionImgWidth = $get("<%=hfNewQuestionImgWidth.ClientID%>");
                            var hfNewQuestionImgHeight = $get("<%=hfNewQuestionImgHeight.ClientID%>");
                            var hfNewQuestionImgExt = $get("<%=hfNewQuestionImgExt.ClientID%>");
                            hfNewQuestionImg.value = "";
                            hfNewQuestionImgWidth.value = "";
                            hfNewQuestionImgHeight.value = "";
                            hfNewQuestionImgExt.value = "";
                        }

                        function NewQuestionPreviewChangeType(type, tbQuestionId, tbAnswer1Id, tbAnswer2Id, tbAnswer3Id, tbAnswer4Id) {
                            var tbQuestionId = document.getElementById(tbQuestionId);
                            var tbAnswer1Id = document.getElementById(tbAnswer1Id);
                            var tbAnswer2Id = document.getElementById(tbAnswer2Id);
                            var tbAnswer3Id = document.getElementById(tbAnswer3Id);
                            var tbAnswer4Id = document.getElementById(tbAnswer4Id);
                            new_question_content.innerHTML = tbQuestionId.value;
                            new_question_answer1.innerHTML = tbAnswer1Id.value;
                            new_question_answer2.innerHTML = tbAnswer2Id.value;
                            new_question_answer3.innerHTML = tbAnswer3Id.value;
                            new_question_answer4.innerHTML = tbAnswer4Id.value;

                            switch (type) {
                                case 1:
                                    new_question_preview.className = 'question-preview';
                                    new_question_img.style.display = 'none';
                                    break;
                                case 2:
                                    new_question_preview.className = 'question-preview question-preview--format02';
                                    new_question_img.style.display = 'none';
                                    break;
                                case 3:
                                    new_question_preview.className = 'question-preview question-preview--format02';
                                    new_question_img.style.display = '';
                                    var img = $get("<%=imgNewQuestionPreview.ClientID%>");
                                    var hiddenImage = $get("<%=hfNewQuestionImg.ClientID%>");
                                    img.src = hiddenImage.value;
                                    break;
                                default:
                                    break;
                            }
                        }

                        function PreviewImage(input, imgId, hdImgBase64Id, hdImgExtId, hdImgWidthId, hdImgHeightId, hdImgIsNew) {
                            if (input.files && input.files[0]) {
                                var t = input.files[0].type;
                                var s = input.files[0].size / (1024 * 1024);
                                if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg" || t.toLowerCase() == "image/bmp") {
                                    if (s > 5) {
                                        toastr.info('Uploaded photo exceeded 5MB.');
                                    } else {
                                        var fileReader = new FileReader();
                                        var image = new Image();
                                        fileReader.onload = function (e) {
                                            image.src = e.target.result;
                                            image.onload = function () {
                                                var w = this.width,
                                                    h = this.height;
                                                if ((w > 2560 || h > 2560)) {
                                                    toastr.info('Uploaded photo exceeded 2560 x 2560.');
                                                }
                                                else {
                                                    var img = $get(imgId);
                                                    var hiddenImage = $get(hdImgBase64Id);
                                                    var hiddenImageExt = $get(hdImgExtId);
                                                    var hfNewQuestionImgWidth = $get(hdImgWidthId);
                                                    var hfNewQuestionImgHeight = $get(hdImgHeightId);
                                                    var hfEditQuestionImgIsNew = $get(hdImgIsNew);

                                                    hiddenImageExt.setAttribute('value', t);
                                                    hiddenImage.setAttribute('value', this.src);
                                                    hfNewQuestionImgWidth.setAttribute('value', w);
                                                    hfNewQuestionImgHeight.setAttribute('value', h);
                                                    hfEditQuestionImgIsNew.setAttribute('value', 'true');
                                                    img.src = this.src;
                                                }
                                            };
                                        }
                                    }
                                }
                                else {
                                    toastr.info('File extension is invalid.');
                                }
                                fileReader.readAsDataURL(input.files[0]);
                            }
                        }

                        function SetPreviewBg(divId, colorCode, hdImgBgId) {
                            var div = $get(divId);
                            div.style.backgroundColor = colorCode;
                            var hdImgBg = $get(hdImgBgId);
                            hdImgBg.setAttribute('value', colorCode);
                        }
                    </script>
                    <asp:Panel ID="plNewQuestion" runat="server" CssClass="container" Visible="false">
                        <div class="card add-question">
                            <div class="add-question__questionnumber">
                                <div class="number">
                                    <asp:Literal ID="ltlNewQuestionNo" runat="server" />
                                </div>
                                <label>
                                    Code<br />
                                    <asp:Literal ID="ltlNewQuestionId" runat="server" />
                                </label>
                            </div>
                            <div class="add-question__info">
                                <div class="add-question__info__header">
                                    <div class="add-question__info__header__action">
                                        <asp:LinkButton ID="lbNewQuestionCancel" runat="server" CssClass="btn secondary" Text="Cancel" OnClick="lbNewQuestionCancel_Click" />
                                        <asp:LinkButton ID="lbNewQuestionSave" runat="server" CssClass="btn" OnClick="lbNewQuestionSave_Click" Text="Create" OnClientClick="ShowProgressBar();" />
                                    </div>
                                    <div class="add-question__info__header__title">Display</div>
                                </div>
                                <div class="add-question__info__setup">
                                    <div class="add-question__info__setup__questiontype">
                                        <div class="tabs--questionformat">
                                            <ul class="tabs__list">
                                                <asp:Panel ID="plAction01" runat="server" CssClass="tabs__list__item active">
                                                    <asp:LinkButton ID="lbNewQuestionFormat01" runat="server" OnClick="lbNewQuestionFormat01_Click">
                                                        <span>Question Format 01</span>
                                                        <asp:Image ID="imgFormat01" runat="server" ImageUrl="~/img/qformat01.png" />
                                                    </asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="plAction02" runat="server" CssClass="tabs__list__item">
                                                    <asp:LinkButton ID="lbNewQuestionFormat02" runat="server" OnClick="lbNewQuestionFormat02_Click">
                                                        <span>Question Format 02</span>
                                                        <asp:Image ID="imgFormat02" runat="server" ImageUrl="~/img/qformat02.png" />
                                                    </asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="plAction03" runat="server" CssClass="tabs__list__item">
                                                    <asp:LinkButton ID="lbNewQuestionFormat03" runat="server" OnClick="lbNewQuestionFormat03_Click">
                                                        <span>Question Format 03</span>
                                                        <asp:Image ID="imgFormat03" runat="server" ImageUrl="~/img/qformat03.png" />
                                                    </asp:LinkButton>
                                                </asp:Panel>
                                            </ul>
                                            <div class="tabs__panels">
                                                <asp:Panel ID="plFormat" runat="server" CssClass="tabs__panels__item tabs__panels__item--questionformat01">
                                                    <div class="form__row form__row--question">
                                                        <asp:TextBox ID="tbQuestion" placeholder="Question" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                        <asp:Label ID="lblQuestionCount" runat="server" CssClass="lettercount" />
                                                    </div>
                                                    <asp:Panel ID="plImg" runat="server" Visible="false">
                                                        <div class="backgroundscheme" style="float: none; display: none">
                                                            <div class="button-group">
                                                                <label>
                                                                    <input type="radio" name="button-group" value="item" checked="checked" />
                                                                    <span class="button-group-item">White Background</span>
                                                                </label>
                                                                <label>
                                                                    <input type="radio" name="button-group" value="other-item" />
                                                                    <span class="button-group-item">Black Background</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="uploadcontrols" style="width: 100%">
                                                            <asp:FileUpload ID="fuNewQuestion" runat="server" /><br />
                                                            <br />
                                                        </div>
                                                    </asp:Panel>
                                                    <div>
                                                        <div class="form__row form__row--answer">
                                                            <asp:TextBox ID="tbAnswer1" placeholder="Correct Answer" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                            <asp:Label ID="lblAnswer1Count" runat="server" CssClass="lettercount" />
                                                        </div>
                                                        <div class="form__row form__row--answer">
                                                            <asp:TextBox ID="tbAnswer2" placeholder="Answer 02" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                            <asp:Label ID="lblAnswer2Count" runat="server" CssClass="lettercount" />
                                                        </div>
                                                        <div class="form__row form__row--answer">
                                                            <asp:TextBox ID="tbAnswer3" placeholder="Answer 03" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                            <asp:Label ID="lblAnswer3Count" runat="server" CssClass="lettercount" />
                                                        </div>
                                                        <div class="form__row form__row--answer">
                                                            <asp:TextBox ID="tbAnswer4" placeholder="Answer 04" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                            <asp:Label ID="lblAnswer4Count" runat="server" CssClass="lettercount" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-question__info__setup__preview">
                                        <!-- Question Preview Format -->
                                        <div id="new_question_preview" class="question-preview">
                                            <div class="question-preview__header">
                                                <div class="question-preview__header__score">
                                                    <div class="Media">
                                                        <img class="Media-figure" src="/img/image-01.jpg" alt="" />
                                                        <div class="Preview-body">
                                                            <span>User 1</span>
                                                            <div class="score">Score</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="question-preview__header__timer">Time Left <span>10</span></div>
                                                <div class="question-preview__header__score question-preview__header__score--user02">
                                                    <div class="Media">
                                                        <img class="Media-figure" src="/img/image-01.jpg" alt="" />
                                                        <div class="Preview-body">
                                                            <span>User 2</span>
                                                            <div class="score">Score</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="question-preview__question">
                                                <div id="new_question_content" style="word-wrap: break-word; white-space: pre;"></div>
                                            </div>
                                            <div id="new_question_img" class="question-preview__image" style="display: none">
                                                <asp:Image ID="imgNewQuestionPreview" runat="server" />
                                            </div>
                                            <div class="question-preview__answers">
                                                <div class="question-preview__answer">
                                                    <div id="new_question_answer1" style="word-wrap: break-word;">
                                                    </div>
                                                </div>
                                                <div class="question-preview__answer">
                                                    <div id="new_question_answer2" style="word-wrap: break-word;">
                                                    </div>
                                                </div>
                                                <div class="question-preview__answer">
                                                    <div id="new_question_answer3" style="word-wrap: break-word;">
                                                    </div>
                                                </div>
                                                <div class="question-preview__answer">
                                                    <div id="new_question_answer4" style="word-wrap: break-word;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / Question Preview Format -->
                                    </div>
                                    <div class="add-question__info__setup__choice">
                                        <div class="form__row">
                                            <label style="font-weight: bold;">
                                                Recommended Reading Time:
                                                <lable id="new_question_recommend_reading_time">0</lable>
                                                s</label>
                                            <label>Reading Time</label>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlNewQuestionReadingTime" runat="server">
                                                    <asp:ListItem Text="1.0 seconds" Value="1.0" />
                                                    <asp:ListItem Text="1.3 seconds" Value="1.3" />
                                                    <asp:ListItem Text="1.5 seconds" Value="1.5" />
                                                    <asp:ListItem Text="1.7 seconds" Value="1.7" />
                                                    <asp:ListItem Text="1.9 seconds" Value="1.9" />
                                                    <asp:ListItem Text="2.0 seconds" Value="2.0" />
                                                    <asp:ListItem Text="2.1 seconds" Value="2.1" />
                                                    <asp:ListItem Text="2.3 seconds" Value="2.3" />
                                                    <asp:ListItem Text="2.5 seconds" Value="2.5" />
                                                    <asp:ListItem Text="3.0 seconds" Value="3.0" />
                                                    <asp:ListItem Text="3.5 seconds" Value="3.5" />
                                                    <asp:ListItem Text="4.0 seconds" Value="4.0" />
                                                    <asp:ListItem Text="4.5 seconds" Value="4.5" />
                                                    <asp:ListItem Text="5.0 seconds" Value="5.0" />
                                                    <asp:ListItem Text="5.5 seconds" Value="5.5" />
                                                    <asp:ListItem Text="6.0 seconds" Value="6.0" />
                                                    <asp:ListItem Text="6.5 seconds" Value="6.5" />
                                                    <asp:ListItem Text="7.0 seconds" Value="7.0" />
                                                    <asp:ListItem Text="8.0 seconds" Value="8.0" />
                                                    <asp:ListItem Text="9.0 seconds" Value="9.0" />
                                                    <asp:ListItem Text="10.0 seconds" Value="10.0" />
                                                    <asp:ListItem Text="11.0 seconds" Value="11.0" />
                                                    <asp:ListItem Text="12.0 seconds" Value="12.0" />
                                                    <asp:ListItem Text="13.0 seconds" Value="13.0" />
                                                    <asp:ListItem Text="14.0 seconds" Value="14.0" />
                                                    <asp:ListItem Text="15.0 seconds" Value="15.0" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form__row">
                                            <label style="font-weight: bold;">
                                                Recommended Answering Time:
                                                <lable id="new_question_recommend_answering_time">0</lable>
                                                s</label>
                                            <label>Answering Time</label>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlNewQuestionAnsweringTime" runat="server">
                                                    <asp:ListItem Text="10 seconds" Value="10" />
                                                    <asp:ListItem Text="12 seconds" Value="12" />
                                                    <asp:ListItem Text="13 seconds" Value="13" />
                                                    <asp:ListItem Text="14 seconds" Value="14" />
                                                    <asp:ListItem Text="15 seconds" Value="15" />
                                                    <asp:ListItem Text="16 seconds" Value="16" />
                                                    <asp:ListItem Text="17 seconds" Value="17" />
                                                    <asp:ListItem Text="18 seconds" Value="18" />
                                                    <asp:ListItem Text="19 seconds" Value="19" />
                                                    <asp:ListItem Text="20 seconds" Value="20" />
                                                    <asp:ListItem Text="21 seconds" Value="21" />
                                                    <asp:ListItem Text="22 seconds" Value="22" />
                                                    <asp:ListItem Text="23 seconds" Value="23" />
                                                    <asp:ListItem Text="24 seconds" Value="24" />
                                                    <asp:ListItem Text="25 seconds" Value="25" />
                                                    <asp:ListItem Text="30 seconds" Value="30" />
                                                    <asp:ListItem Text="35 seconds" Value="35" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form__row">
                                            <label>Difficulty</label>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlNewQuestionDifficulty" runat="server">
                                                    <asp:ListItem Text="Easy" Value="1" />
                                                    <asp:ListItem Text="Normal" Value="2" />
                                                    <asp:ListItem Text="Hard - End Game x2 score" Value="3" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form__row">
                                            <label>Scoring</label>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlNewQuestionScoring" runat="server">
                                                    <asp:ListItem Text="Default - 1x Score" Value="1" />
                                                    <asp:ListItem Text="2x Original Score" Value="2" />
                                                    <asp:ListItem Text="3x Original Score" Value="3" />
                                                    <asp:ListItem Text="4x Original Score" Value="4" />
                                                    <asp:ListItem Text="5x Original Score" Value="5" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form__row">
                                            <label>Status</label>
                                            <div class="mdl-selectfield">
                                                <asp:DropDownList ID="ddlNewQuestionStatus" runat="server">
                                                    <asp:ListItem Text="Active" Value="1" />
                                                    <asp:ListItem Text="Inactive" Value="2" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- / New Question -->

            <asp:LinkButton ID="lbBottom" runat="server" />

        </div>
    </div>
</asp:Content>
