﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;

namespace AdminWebsite.MatchUp
{
    public partial class TopicCreate : System.Web.UI.Page
    {
        private static ILog Log = LogManager.GetLogger("AdminWebsiteLog");

        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();

        private List<TopicCategory> categoryList = null;
        private List<Department> departmentList = null;
        private List<Topic.DropdownNumberOfQuestions> noQuestionList = null;
        private List<String> listIconUrl = null;

        public void GetCategoryList()
        {
            try
            {
                if (categoryList == null)
                {
                    CategorySelectAllResponse response = asc.SelectAllCategories(adminInfo.UserId, adminInfo.CompanyId);
                    if (response.Success)
                    {
                        categoryList = response.TopicCategories;
                        categoryList = categoryList.OrderBy(o => o.Title).ToList();
                    }
                    else
                    {
                        Log.Error("SelectAllCategories request is failed. result error msg: " + response.ErrorMessage);
                        categoryList = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                categoryList = null;
            }
        }

        public void GetDepartmentList()
        {
            try
            {
                if (departmentList == null)
                {
                    DepartmentListResponse response = asc.GetAllDepartment(adminInfo.UserId, adminInfo.CompanyId);
                    if (response.Success)
                    {
                        List<Department> list = response.Departments;
                        list.Sort((x, y) => { return x.Title.CompareTo(y.Title); });
                        departmentList = list;
                    }
                    else
                    {
                        Log.Error("GetAllDepartment.Success is false. ErrorMessage: " + response.ErrorMessage);
                        departmentList = null;
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                departmentList = null;
            }
        }

        public void GetNoQuestionPerGame()
        {
            try
            {
                if (noQuestionList == null)
                {
                    noQuestionList = asc.SelectNumberOfQuestionsForDropdown();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                departmentList = null;
            }
        }

        public void GetIconData()
        {
            if (listIconUrl == null)
            {
                TopicSelectIconResponse response = asc.SelectAllTopicIcons(adminInfo.UserId, adminInfo.CompanyId);
                if (response.Success)
                {
                    listIconUrl = response.TopicIconUrls;
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["admin_info"] == null)
                {
                    Response.Redirect("/Logout");
                    return;
                }
                adminInfo = Session["admin_info"] as ManagerInfo;

                if (!IsPostBack)
                {
                    #region Get category.
                    cbCategory.Items.Clear();
                    GetCategoryList();
                    if (categoryList != null)
                    {
                        for (int i = 0; i < categoryList.Count; i++)
                        {
                            cbCategory.Items.Add(new ListItem(categoryList[i].Title, Convert.ToString(categoryList[i].Id)));

                        }
                        if (categoryList.Count != 0)
                        {
                            cbCategory.Items[0].Selected = true;
                        }
                    }
                    #endregion

                    #region Get department.
                    cblDepartment.Items.Clear();
                    GetDepartmentList();
                    if (departmentList != null)
                    {
                        for (int i = 0; i < departmentList.Count; i++)
                        {
                            cblDepartment.Items.Add(new ListItem(departmentList[i].Title, Convert.ToString(departmentList[i].Id)));
                        }
                    }
                    #endregion

                    #region Get No of questions per game.
                    ddlNoQuestionPerGame.Items.Clear();
                    GetNoQuestionPerGame();
                    if (noQuestionList != null)
                    {
                        for (int i = 0; i < noQuestionList.Count; i++)
                        {
                            ddlNoQuestionPerGame.Items.Add(new ListItem(noQuestionList[i].DisplayText, Convert.ToString(noQuestionList[i].Value)));
                            if (noQuestionList[i].Value == 7)
                            {
                                ddlNoQuestionPerGame.Items[i].Selected = true;
                            }
                        }
                    }
                    #endregion

                    #region Icon data
                    GetIconData();
                    rtIcon.DataSource = listIconUrl;
                    rtIcon.DataBind();
                    #endregion
                    imgTopic.ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/default_topic_icon.png";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void cbCheckAllDept_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbCheckAllDept.Checked)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        cblDepartment.Items[i].Selected = true;
                    }
                }
                else
                {
                    cblDepartment.ClearSelection();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void cblDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Boolean isCheckAllDepartment = true;
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (!cblDepartment.Items[i].Selected)
                    {
                        isCheckAllDepartment = false;
                        break;
                    }
                }
                cbCheckAllDept.Checked = isCheckAllDepartment;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                mpeAddIcon.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpeAddIcon.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    GetIconData();
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    imgChooseIcon.ImageUrl = listIconUrl[e.Item.ItemIndex];
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    lbChooseIcon.CommandArgument = listIconUrl[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    Log.Debug(e.CommandArgument.ToString());
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpeAddIcon.Hide();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbCreate_Click(object sender, EventArgs e)
        {
            #region Step 1. Check input data.
            if (String.IsNullOrEmpty(tbTopictitle.Text))
            {
                ScriptManager.RegisterStartupScript(lbCreate, typeof(LinkButton), "toast", "ReloadToast();toastr.info('Please enter topic title.');", true);
                return;
            }

            if (String.IsNullOrEmpty(cbCategory.SelectedValue.Trim()))
            {
                ScriptManager.RegisterStartupScript(lbCreate, typeof(LinkButton), "toast", "ReloadToast();toastr.info('Please at least choose a category.');", true);
                return;
            }

            bool isPrivacyForEveryone = false;
            List<String> listDepartment = new List<String>();
            if (rbPrivacyDepartment.Checked)
            {
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (cblDepartment.Items[i].Selected)
                    {
                        listDepartment.Add(cblDepartment.Items[i].Value);
                    }
                }

                if (listDepartment.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(lbCreate, typeof(LinkButton), "toast", "ReloadToast();toastr.info('Please at least choose a department.');", true);
                    return;
                }
            }
            else if (rbPrivacyEveryone.Checked)
            {
                isPrivacyForEveryone = true;
            }

            #endregion

            #region Step 2. Check category
            GetCategoryList();

            String categoryID = String.Empty;
            String categoryTitle = cbCategory.SelectedItem.Text;
            for (int i = 0; i < categoryList.Count; i++)
            {
                if (categoryList[i].Title.ToLower().Equals(cbCategory.SelectedItem.Text.Trim().ToLower()))
                {
                    categoryID = categoryList[i].Id;
                    break;
                }
            }
            #endregion

            #region call api
            TopicCreateResponse response = asc.CreateTopic(adminInfo.UserId,
                                                           adminInfo.CompanyId,
                                                           tbTopictitle.Text,
                                                           imgTopic.ImageUrl,
                                                           tbTopicDescription.Text,
                                                           categoryID,
                                                           categoryTitle,
                                                           listDepartment,
                                                           Convert.ToInt16(ddlNoQuestionPerGame.SelectedValue),
                                                           isPrivacyForEveryone
                                                           );
            if (response.Success)
            {
                ScriptManager.RegisterStartupScript(lbAddIcon, typeof(LinkButton), "toast", "ReloadToast();toastr.info('Topic created successfully');", true);
                Response.AppendHeader("Refresh", "1; URL='/MatchUp/QuizEdit/" + Encryption.EncryptByDES(response.TopicId, WebConfigurationManager.AppSettings["DES_KEY"].ToString(), WebConfigurationManager.AppSettings["DES_IV"].ToString()) + "'");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toast", "ReloadToast();toastr.info('Failed to create topic');", true);
                Log.Error("Failed to create topic. ErrorMessage: " + response.ErrorMessage);
            }
            #endregion
        }

        protected void cbCategory_TextChanged(object sender, EventArgs e)
        {
            try
            {
                cbCategory.SelectedValue = "";
                Log.Debug("Text: " + cbCategory.Text);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rbPrivacyEveryone_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cbCheckAllDept.Checked = false;
                cbCheckAllDept.Enabled = false;

                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    cblDepartment.Items[i].Selected = false;
                }
                cblDepartment.Enabled = false;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rbPrivacyDepartment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cbCheckAllDept.Checked = true;
                cbCheckAllDept.Enabled = true;

                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    cblDepartment.Items[i].Selected = true;
                }
                cblDepartment.Enabled = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}