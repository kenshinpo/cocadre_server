﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="TopicCreate.aspx.cs" Inherits="AdminWebsite.MatchUp.TopicCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <asp:LinkButton ID="lbEmpty" runat="server"></asp:LinkButton>
    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Match Up console</div>
        <div class="appbar__meta">Create Topic</div>
        <div class="appbar__action">
            <%--<a href=""><i class="fa fa-ellipsis-v"></i></a>
            <a href=""><i class="fa fa-question-circle"></i></a>--%>
            <%--<a class="data-sidebar-toggle" href="#"><i class="fa fa-filter"></i></a>--%>
        </div>
    </div>
    <!-- / App Bar -->

    <!-- Reveal: Add Topic Icon -->
    <asp:UpdatePanel ID="plAddIcon" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="popup_addtopicicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Topic Icon</h1>
                <%--<div class="popup__content">
                        <fieldset class="form">
                            <div class="grid">
                                <div class="grid__inner">
                                    <div class="grid__span grid__span--6">
                                        <a href="#popup_addtopicicon_url" class="btn secondary popup--addphoto__web popup-with-form"><i class="fa fa-globe"></i>Add Photo from Web</a>
                                    </div>
                                    <div class="grid__span grid__span--6 grid__span--last">
                                        <a href="#popup_addtopicicon_selected" class="btn popup--addphoto__local popup-with-form"><i class="fa fa-plus"></i>Upload Photo</a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>--%>
                <div class="topicicons">
                    <p>Select from list</p>
                    <asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand">
                        <HeaderTemplate>
                            <div class="topicicons__list">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <label class="topicicons__icon">
                                <asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
                                    <asp:Image ID="imgChooseIcon" runat="server" />
                                </asp:LinkButton>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="popup__action">
                    <%--<asp:LinkButton ID="lbAddIconSave" CssClass="popup__action__item popup__action__item--cta" runat="server">Save</asp:LinkButton>--%>
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbAddCancel_Click">Cancel</asp:LinkButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ajaxToolkit:ModalPopupExtender ID="mpeAddIcon" runat="server"
        TargetControlID="lbEmpty"
        PopupControlID="popup_addtopicicon"
        BackgroundCssClass="mfp-bg"
        DropShadow="false">
    </ajaxToolkit:ModalPopupExtender>
    <!-- / Reveal: Add Topic Icon -->

    <!-- Reveal: Add Topic Icon - URL -->
    <div id="popup_addtopicicon_url" class="mfp-hide popup popup--addtopicicon popup--addtopicicon--url">
        <h1 class="popup__title">Add New Topic Icon</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="grid">
                    <div class="grid__inner">
                        <div class="grid__span grid__span--6">
                            <a href="#popup_addtopicicon_url" class="btn secondary popup--addphoto__web popup-with-form"><i class="fa fa-globe"></i>Add Photo from Web</a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="form__row popup--addphoto__web__url">
                        <div class="main">
                            <input id="" name="" type="url" placeholder="Paste a URL" required="">
                        </div>
                        <div class="side"></div>
                    </div>
                    <p class="error clear">Please provide a file with a dimension of 360x360 or bigger</p>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a href="#popup_addtopicicon" class="popup__action__item popup-with-form">Previous</a>
        </div>
    </div>
    <!-- / Reveal: Add Topic Icon URL -->
    <!-- Reveal: Selected Icon -->
    <div id="popup_addtopicicon_selected" class="mfp-hide popup popup--addtopicicon popup--addtopicicon--selected">
        <h1 class="popup__title">Selected Icon</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="photocrop">
                        <pre>Selected Icon Here</pre>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a href="#popup_addtopicicon" class="popup__action__item popup__action__item--others popup-with-form">Previous</a>
            <a href="#" class="popup__action__item popup__action__item--cta popup-with-form">Save</a>
            <a href="#" class="popup__action__item popup__action__item--cancel">Cancel</a>
        </div>
    </div>
    <!-- / Reveal: Add Topic Icon -->

    <!-- New  Topic -->
    <div class="container" style="">
        <div class="card add-topic">
            <div class="add-topic__icon">
                <asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
                    <asp:Image ID="imgTopic" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/default_topic_icon.png" runat="server" />
                    <label><small>Choose topic icon</small></label>
                </asp:LinkButton>
            </div>
            <div class="add-topic__info">
                <div class="add-topic__info__action" style="float: right">
                    <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn" OnClick="lbCreate_Click">Create</asp:LinkButton>
                    <asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/MatchUp/TopicList" Text="Cancel" CssClass="btn secondary" />
                </div>
                <div class="add-topic__info__details">
                    <div id="" class="tabs tabs--styled">
                        <ul class="tabs__list">
                            <li class="tabs__list__item">
                                <a class="tabs__link" href="#topic-basic-info">Basic Info</a>
                            </li>
                            <li class="tabs__list__item">
                                <a class="tabs__link" href="#topic-department">Privacy</a>
                            </li>
                            <li class="tabs__list__item">
                                <a class="tabs__link" href="#topic-settings">Settings</a>
                            </li>
                        </ul>
                        <div class="tabs__panels">
                            <div class="tabs__panels__item add-topic__info__details--basicinfo" id="topic-basic-info">
                                <div class="column--input">
                                    <div class="form__row">
                                        Topic Title: 
                                        <asp:TextBox ID="tbTopictitle" runat="server" placeholder="Topic Title" MaxLength="20" />
                                    </div>
                                    <div class="form__row">
                                        Topic Description: 
                                        <asp:TextBox ID="tbTopicDescription" runat="server" MaxLength="120" placeholder="Description" />
                                    </div>
                                </div>
                                <div class="column--choice">
                                    <div class="form__row">
                                        <label>Category</label>
                                        <div>
                                            <ajaxToolkit:ComboBox ID="cbCategory" runat="server" AutoCompleteMode="Suggest" CssClass="combos" MaxLength="30" />
                                            <asp:HiddenField ID="hfCategoryId" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs__panels__item add-topic__info__details--department" id="topic-department">
                                <asp:UpdatePanel ID="plCheckDepartment" runat="server" Class="departments-list--asp">
                                    <ContentTemplate>
                                        <asp:RadioButton ID="rbPrivacyEveryone" Text="Everyone" GroupName="PrivacySetting" runat="server" Checked="true" AutoPostBack="true" OnCheckedChanged="rbPrivacyEveryone_CheckedChanged"   /><br />
                                        <p>Topic can be seen by everyone including newly added departments.</p>
                                        <asp:RadioButton ID="rbPrivacyDepartment" Text="Selected department only" GroupName="PrivacySetting" runat="server" AutoPostBack="true" OnCheckedChanged="rbPrivacyDepartment_CheckedChanged" />
                                        <p>Topic can be seen by department(s) only. Only selected departments will be able to see this.</p>
                                        <hr />
                                        <asp:CheckBox ID="cbCheckAllDept" runat="server" Text="Select All" Enabled="false" AutoPostBack="true" OnCheckedChanged="cbCheckAllDept_CheckedChanged" />
                                        <br />
                                        <asp:CheckBoxList ID="cblDepartment" runat="server" Enabled="false" RepeatLayout="Flow" RepeatColumns="6" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="cblDepartment_SelectedIndexChanged" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="tabs__panels__item add-topic__info__details--settings" id="topic-settings">
                                <div class="form__row">
                                    <label>No of questions per game</label><br />
                                    <div class="mdl-selectfield">
                                        <asp:DropDownList ID="ddlNoQuestionPerGame" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / New  Topic -->
</asp:Content>
