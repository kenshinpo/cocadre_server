﻿<%@ Page Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Category.aspx.cs" Inherits="AdminWebsite.MatchUp.Category" %>

<asp:Content ID="cttMain" ContentPlaceHolderID="main_content" runat="server">

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfCategoryTitle" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <asp:LinkButton ID="lbOpenAddCategory" runat="server" CssClass="mfb-component__button--main" data-mfb-label="Add Category" OnClick="lbOpenAddCategory_Click">
                <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                <i class="mfb-component__main-icon--active fa fa-user-plus"></i>
                    </asp:LinkButton>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Add Category -->
            <asp:Panel ID="popup_addcategory" runat="server" CssClass="popup popup--addcategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Add a Category</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Category</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbAddName" runat="server" placeholder="Title of your category" MaxLength="20" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAddDone" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbAddDone_Click" OnClientClick="ShowProgressBar();" Text="Done" />
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Add Category -->

            <!-- Rename Category -->
            <asp:Panel ID="popup_renamecategory" runat="server" CssClass="popup popup--renamecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Rename Category</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Category</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbRenameName" runat="server" placeholder="Title of your category" MaxLength="20" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbRenameSave" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbRenameSave_Click" OnClientClick="ShowProgressBar();" Text="Save" />
                    <asp:LinkButton ID="lbRenameCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Rename Category -->

            <!-- Delete Category -->
            <asp:Panel ID="popup_deletecategory" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Delete Category</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <asp:Literal ID="ltlDeleteMsg" runat="server" />
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbDelete_Click" Text="Delete"  OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbDelCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Delete Category -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_addcategory"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Match Up <span>console</span></div>
        <div class="appbar__meta">Category</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MatchUp/TopicList">Topic</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Category</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upCategoryList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvCategory" runat="server" OnItemDataBound="lvCategory_ItemDataBound" OnItemCommand="lvCategory_ItemCommand" OnItemCreated="lvCategory_ItemCreated">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th>
                                            <asp:LinkButton ID="lbSortByName" runat="server" OnClick="lbSortByName_Click" Text="Category" />
                                        </th>
                                        <th>No. of Topics</th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server">
                                    </tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lbName" ClientIDMode="AutoID" runat="server" CommandName="Rename" style="color: #999;" />
                                    <%--<asp:Label ID="lblName" runat="server" />--%>
                                </td>
                                <td>
                                    <asp:HyperLink ID="hlNoOfTopics" runat="server" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="btnChips" runat="server" CssClass="fa fa-ellipsis-v" OnClientClick="return false;"></asp:LinkButton>
                                    <asp:Panel ID="chipsPnl" runat="server" CssClass="chips">
                                        <ul>
                                            <li>
                                                <asp:LinkButton ID="lbRename" ClientIDMode="AutoID" runat="server" CommandName="Rename" Text="Rename Category" />
                                            </li>
                                            <li>
                                                <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Remove" Text="Delete Category" />
                                            </li>
                                        </ul>
                                    </asp:Panel>
                                    <ajaxToolkit:PopupControlExtender ID="PopEx" runat="server"
                                        TargetControlID="btnChips"
                                        PopupControlID="chipsPnl"
                                        Position="Left"
                                        OffsetX="-170" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
