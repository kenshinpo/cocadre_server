﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using log4net;

namespace AdminWebsite.MatchUp
{
    public partial class TopicList : Page
    {
        private ManagerInfo adminInfor;
        private AdminService asc = new AdminService();

        private List<Department> departmentList = null;
        private List<TopicCategory> categoryList = null;
        private List<Topic> topicList = null;

        public void GetDepartmentList()
        {
            try
            {
                if (departmentList == null)
                {
                    DepartmentListResponse response = asc.GetAllDepartment(adminInfor.UserId, adminInfor.CompanyId);
                    if (response.Success)
                    {
                        departmentList = response.Departments;
                        departmentList = departmentList.OrderBy(o => o.Title).ToList();
                    }
                    else
                    {
                        Log.Error("DepartmentListResponse.Success is false. ErrorMessage: " + response.ErrorMessage, this.Page);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        public void GetCategoryList()
        {
            try
            {
                if (categoryList == null)
                {
                    CategorySelectAllResponse response = asc.SelectAllCategories(adminInfor.UserId, adminInfor.CompanyId);
                    if (response.Success)
                    {
                        categoryList = response.TopicCategories;
                        categoryList = categoryList.OrderBy(o => o.Title).ToList();
                    }
                    else
                    {
                        Log.Error("CategorySelectAllResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        public void GetTopicList(string categoryId, string departmentId, string containName)
        {
            try
            {
                if(string.IsNullOrEmpty(containName))
                {
                    containName = null;
                }
                else
                {
                    containName = containName.Trim();
                }

                TopicSelectAllBasicResponse response = asc.SelectTopicBasicByCategoryAndDepartment(adminInfor.UserId, adminInfor.CompanyId, categoryId, departmentId, containName);
                if (response.Success)
                {
                    topicList = response.Topics;
                    topicList = topicList.OrderBy(o => o.TopicTitle).ToList();
                }
                else
                {
                    Log.Error("TopicSelectAllBasicResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfor = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    String categoryId = String.Empty;
                    if (Page.RouteData.Values["CategoryId"] != null)
                    {
                        categoryId = Page.RouteData.Values["CategoryId"].ToString();
                    }

                    #region Get departments.
                    ddlFilterDepartment.Items.Clear();
                    ddlFilterDepartment.Items.Add(new ListItem("All", String.Empty));
                    GetDepartmentList();
                    if (departmentList != null)
                    {
                        for (int i = 0; i < departmentList.Count; i++)
                        {
                            ddlFilterDepartment.Items.Add(new ListItem(departmentList[i].Title, Convert.ToString(departmentList[i].Id)));
                        }
                    }
                    #endregion

                    #region Get category.
                    ddlFilterCategory.Items.Clear();
                    ddlFilterCategory.Items.Add(new ListItem("All", String.Empty));
                    GetCategoryList();
                    if (categoryList != null)
                    {
                        for (int i = 0; i < categoryList.Count; i++)
                        {
                            ddlFilterCategory.Items.Add(new ListItem(categoryList[i].Title, categoryList[i].Id));
                            if (categoryId == categoryList[i].Id)
                            {
                                ddlFilterCategory.Items[i + 1].Selected = true;
                            }
                        }
                    }
                    #endregion

                    #region Get topic.
                    GetTopicList(ddlFilterCategory.SelectedValue, ddlFilterDepartment.SelectedValue, tbFilterKeyWord.Text);
                    lvTopic.DataSource = topicList;
                    lvTopic.DataBind();
                    #endregion
                }

                tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvTopic_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (topicList != null && topicList.Count > 0)
                {
                    if (e.Item.ItemType == ListViewItemType.DataItem)
                    {
                        System.Web.UI.WebControls.Image imgTpoic = e.Item.FindControl("imgTpoic") as System.Web.UI.WebControls.Image;
                        LinkButton lbIcon = e.Item.FindControl("lbIcon") as LinkButton;
                        LinkButton lbTopicName = e.Item.FindControl("lbTopicName") as LinkButton;
                        //Label lblCategoryName = e.Item.FindControl("lblCategoryName") as Label;
                        LinkButton lbCategoryName = e.Item.FindControl("lbCategoryName") as LinkButton;
                        Label lblCategoryId = e.Item.FindControl("lblCategoryId") as Label;
                        //Label lblQuestionNo = e.Item.FindControl("lblQuestionNo") as Label;
                        LinkButton lbQuestionCount = e.Item.FindControl("lbQuestionCount") as LinkButton;
                        //Label lblTopicStatus = e.Item.FindControl("lblTopicStatus") as Label;
                        LinkButton lbStatus = e.Item.FindControl("lbStatus") as LinkButton;

                        LinkButton lbEdit = e.Item.FindControl("lbEdit") as LinkButton;
                        LinkButton lbActive = e.Item.FindControl("lbActive") as LinkButton;
                        LinkButton lbHide = e.Item.FindControl("lbHide") as LinkButton;
                        LinkButton lbDelete = e.Item.FindControl("lbDelete") as LinkButton;


                        imgTpoic.ImageUrl = topicList[e.Item.DataItemIndex].TopicLogoUrl;
                        lbTopicName.Text = topicList[e.Item.DataItemIndex].TopicTitle;
                        lbCategoryName.Text = topicList[e.Item.DataItemIndex].TopicCategory.Title;
                        lblCategoryId.Text = topicList[e.Item.DataItemIndex].TopicCategory.Id;
                        lbQuestionCount.Text = Convert.ToString(topicList[e.Item.DataItemIndex].TotalNumberOfQuestions);
                        lbStatus.Text = topicList[e.Item.DataItemIndex].Status.Title;

                        lbIcon.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbTopicName.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbCategoryName.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbQuestionCount.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbStatus.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbEdit.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbActive.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbHide.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;
                        lbDelete.CommandArgument = topicList[e.Item.DataItemIndex].TopicId;

                        // UNLISTED
                        if (topicList[e.Item.DataItemIndex].Status.Code == 1)
                        {
                            lbEdit.Visible = true;
                            lbDelete.Visible = true;
                        }
                        // ACTIVE
                        else if (topicList[e.Item.DataItemIndex].Status.Code == 2)
                        {
                            lbEdit.Visible = true;
                            lbHide.Visible = true;
                            lbDelete.Visible = true;
                        }
                        // HIDDEN
                        else if (topicList[e.Item.DataItemIndex].Status.Code == 3)
                        {
                            lbEdit.Visible = true;
                            lbActive.Visible = true;
                            lbDelete.Visible = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                #region Get topic.
                GetTopicList(ddlFilterCategory.SelectedValue, ddlFilterDepartment.SelectedValue, tbFilterKeyWord.Text);
                lvTopic.DataSource = topicList;
                lvTopic.DataBind();
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvTopic_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                LinkButton lbTopicName = e.Item.FindControl("lbTopicName") as LinkButton;
                Label lblCategoryId = e.Item.FindControl("lblCategoryId") as Label;
                System.Web.UI.WebControls.Image imgTpoic = e.Item.FindControl("imgTpoic") as System.Web.UI.WebControls.Image;

                hfTopicId.Value = e.CommandArgument.ToString();
                hfTopicName.Value = lbTopicName.Text;
                hfCategoryId.Value = lblCategoryId.Text;

                if (e.CommandName.Equals("UpdateTopic"))
                {
                    String topicId = e.CommandArgument.ToString();
                    Response.Redirect("/MatchUp/QuizEdit/" + Encryption.EncryptByDES(topicId, WebConfigurationManager.AppSettings["DES_KEY"].ToString(), WebConfigurationManager.AppSettings["DES_IV"].ToString()), false);
                }
                else
                {
                    if (e.CommandName.Equals("ActiveTopic"))
                    {
                        ltlActionName.Text = "Activate ";
                        lbAction.Text = "Activate";
                        lbAction.CommandArgument = "2";
                        ltlActionMsg.Text = "Activate <b>" + lbTopicName.Text + "</b>. Confirm?";
                    }
                    else if (e.CommandName.Equals("HideTopic"))
                    {
                        ltlActionName.Text = "Hide ";
                        lbAction.Text = "Hide";
                        lbAction.CommandArgument = "3";
                        ltlActionMsg.Text = "Hide <b>" + lbTopicName.Text + "</b>. Confirm?";
                    }
                    else if (e.CommandName.Equals("DeleteTopic"))
                    {
                        ltlActionName.Text = "Delete ";
                        lbAction.Text = "Delete";
                        lbAction.CommandArgument = "-1";
                        ltlActionMsg.Text = "All questions within <b>" + lbTopicName.Text + "</b> will be gone. Confirm?";
                    }
                    imgActionTopic.ImageUrl = imgTpoic.ImageUrl;
                    ltlActionTopicTitle.Text = lbTopicName.Text;
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbActionCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ibFilterTopic_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                #region Get topic.
                GetTopicList(ddlFilterCategory.SelectedValue, ddlFilterDepartment.SelectedValue, tbFilterKeyWord.Text);
                lvTopic.DataSource = topicList;
                lvTopic.DataBind();
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                TopicUpdateResponse response = asc.UpdateTopicStatus(adminInfor.UserId, adminInfor.CompanyId, hfTopicId.Value, hfCategoryId.Value, Convert.ToInt16(lbAction.CommandArgument));
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    mpePop.Hide();
                    #region Get topic.
                    GetTopicList(ddlFilterCategory.SelectedValue, ddlFilterDepartment.SelectedValue, tbFilterKeyWord.Text);
                    lvTopic.DataSource = topicList;
                    lvTopic.DataBind();
                    #endregion


                    if (Convert.ToInt16(lbAction.CommandArgument) == 2)
                    {
                        toastMsg = hfTopicName.Value + " has been set to active.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == 3)
                    {
                        toastMsg = hfTopicName.Value + " has been set to hidden.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == -1)
                    {
                        toastMsg = hfTopicName.Value + " has been deleted.";
                    }
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("DepartmentUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    mpePop.Show();
                    toastMsg = "Failed to change status, please check your internet connection.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvTopic_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (topicList == null || topicList.Count == 0)
                {
                    if (e.Item.ItemType == ListViewItemType.EmptyItem)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        if (ddlFilterDepartment.SelectedIndex > 0 || ddlFilterCategory.SelectedIndex > 0 || !String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                        {
                            if (String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                            {
                                ltlEmptyMsg.Text = @"Filter """ + ddlFilterDepartment.SelectedItem.Text + @""" + """ + ddlFilterCategory.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                            }
                            else
                            {
                                ltlEmptyMsg.Text = @"Filter """ + tbFilterKeyWord.Text.Trim() + @""" + """ + ddlFilterDepartment.SelectedItem.Text + @""" + """ + ddlFilterCategory.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                            }
                        }
                        else
                        {
                            ltlEmptyMsg.Text = @"Topic is empty.<br /><img src='/img/tips_icon.png' width='16' height='20' /> You can add topic with the add button at the bottom right corner of this page.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvTopic_DataBound(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}