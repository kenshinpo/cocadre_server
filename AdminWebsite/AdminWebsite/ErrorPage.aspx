﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="AdminWebsite.ErrorPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/app.css" />
    <title></title>
    <style>
        html,
        body {
            background: white;
        }
    </style>
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <%--<div>
    System Error<br />

        <asp:LinkButton ID="lbReLogin" runat="server" OnClick="lbReLogin_Click">Re-login</asp:LinkButton>
    </div>--%>

        <div class="page-error--container">
            <div class="page-error">
                <a href="/Logout" class="header__logo">CoCadre</a>
                <p class="page-error__summary">There's an error.</p>
                <div class="page-error__details">
                    <p>The server encountered an error and could not complete your request.</p>
                    <p>If the problem persists, please <a href="mailto:admin@cocadre.com?subject=Website Error&body=Please explain the error encountered.">email to admin@cocadre.com</a>.</p>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/app-min.js"></script>
    </form>
</body>
</html>
