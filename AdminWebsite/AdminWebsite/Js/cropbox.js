﻿/**
 * Created by ezgoing on 14/9/2014.
 */

"use strict";
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        factory(jQuery);
    }
}(function ($) {
    var cropbox = function (options, el) {
        var el = el || $(options.imageBox),
            obj =
            {
                state: {},
                ratio: 1,
                options: options,
                imageBox: el,
                thumbBox: el.find(options.thumbBox),
                spinner: el.find(options.spinner),
                image: new Image(),
                getDataURL: function () {
                    var multiplier = 5;
                    var width = this.thumbBox.width() * multiplier,
                        height = this.thumbBox.height() * multiplier,
                        canvas = document.createElement("canvas"),
                        dim = el.css('background-position').split(' '),
                        size = el.css('background-size').split(' '),
                        dx = parseInt(dim[0]) * multiplier - el.width() * multiplier / 2 + width / 2,
                        dy = parseInt(dim[1]) * multiplier - el.height() * multiplier / 2 + height / 2,
                        dw = parseInt(size[0]) * multiplier,
                        dh = parseInt(size[1]) * multiplier,
                        sh = parseInt(this.image.height),
                        sw = parseInt(this.image.width);

                    canvas.width = width;
                    canvas.height = height;
                    var context = canvas.getContext("2d");
                    context.drawImage(this.image, 0, 0, sw, sh, dx, dy, dw, dh);
                    var imageData = canvas.toDataURL('image/jpg');
                    return imageData;
                },
                getBase64: function (imageWidth, imageheight) {
                    var width = imageWidth,
                        height = imageheight,
                        canvas = document.createElement("canvas"),
                        dim = el.css('background-position').split(' '),
                        size = el.css('background-size').split(' '),
                        dx = parseInt(dim[0]) - el.width() / 2 + width / 2,
                        dy = parseInt(dim[1]) - el.height() / 2 + height / 2,
                        dw = parseInt(size[0]),
                        dh = parseInt(size[1]),
                        sh = parseInt(this.image.height),
                        sw = parseInt(this.image.width);

                    canvas.width = width;
                    canvas.height = height;
                    var context = canvas.getContext("2d");
                    context.drawImage(this.image, 0, 0, sw, sh, dx, dy, dw, dh);
                    var imageData = canvas.toDataURL('image/jpg');
                    return imageData;
                },
                getBase64ForCompanyImage: function (imageWidth, imageheight, resizeRate) {
                    var multiplier = 3;
                    var a = this.thumbBox.width();
                    var b = this.thumbBox.height();
                    var width = this.thumbBox.width() * multiplier,
                        height = this.thumbBox.height() * multiplier,
                        canvas = document.createElement("canvas"),
                        dim = el.css('background-position').split(' '),
                        size = el.css('background-size').split(' '),
                        dx = parseInt(dim[0]) * multiplier - el.width() * multiplier / 2 + width / 2,
                        dy = parseInt(dim[1]) * multiplier - el.height() * multiplier / 2 + height / 2,
                        dw = parseInt(size[0]) * multiplier,
                        dh = parseInt(size[1]) * multiplier,
                        sh = parseInt(this.image.height),
                        sw = parseInt(this.image.width);

                    canvas.width = width;
                    canvas.height = height;
                    var context = canvas.getContext("2d");
                    context.drawImage(this.image, 0, 0, sw, sh, dx, dy, dw, dh);

                    var imageData = canvas.toDataURL('image/jpg');
                    return imageData;

                    if (resizeRate != 1) {
                        var img = new Image(200, 200);
                        img.src = imageData;
                        canvas = document.createElement("canvas");
                        canvas.width = width * resizeRate;
                        canvas.height = height * resizeRate;
                        var contextResize = canvas.getContext("2d");
                        contextResize.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
                        imageData = canvas.toDataURL('image/jpg');
                    }
                    return imageData;

                },
                getBlob: function () {
                    var imageData = this.getDataURL();
                    var b64 = imageData.replace('data:image/jpg;base64,', '');
                    var binary = atob(b64);
                    var array = [];
                    for (var i = 0; i < binary.length; i++) {
                        array.push(binary.charCodeAt(i));
                    }
                    return new Blob([new Uint8Array(array)], { type: 'image/jpg' });
                },
                getWidth: function () {
                    return el.width();
                },
                getHeight: function () {
                    return el.height();
                },
                zoomIn: function () {
                    this.ratio *= 1.1;
                    setBackground();
                },
                zoomOut: function () {
                    this.ratio *= 0.9;
                    setBackground();
                }
            },
            setBackground = function () {
                var w = parseInt(obj.image.width) * obj.ratio;
                var h = parseInt(obj.image.height) * obj.ratio;

                // 原本的邏輯
                var pw = (el.width() - w) / 2;
                var ph = (el.height() - h) / 2;
                el.css({
                    'background-image': 'url(' + obj.image.src + ')',
                    'background-size': w + 'px ' + h + 'px',
                    'background-position': pw + 'px ' + ph + 'px',
                    'background-repeat': 'no-repeat'
                });

                // 修正
                //var pw, ph;
                //var backgroundSizeStyle;

                //if (w < el.width() && h < el.height()) {
                //    pw = (el.width() - w) / 2;
                //    ph = (el.height() - h) / 2;

                //    el.css({
                //        'background-image': 'url(' + obj.image.src + ')',
                //        'background-size': w + 'px ' + h + 'px',
                //        'background-position': pw + 'px ' + ph + 'px',
                //        'background-repeat': 'no-repeat'
                //    });
                //}
                //else if (w >= el.width() && h < el.height()) {

                //}
                //else if (w < el.width() && h >= el.height()) {

                //}
                //else {
                //    if (w > h) // 以寬為主
                //    {
                //        ph = (el.height() - h) / 2;
                //        el.css({
                //            'background-image': 'url(' + obj.image.src + ')',
                //            'background-size': 'contain',
                //            //'background-position': '0px ' + ph + 'px',
                //            'background-repeat': 'no-repeat'
                //        });
                //    }
                //    else // 以高為主
                //    {
                //        pw = (el.width() - w) / 2;
                //        el.css({
                //            'background-image': 'url(' + obj.image.src + ')',
                //            'background-size': 'contain',
                //            //'background-position': pw + 'px ' + '0px',
                //            'background-repeat': 'no-repeat'
                //        });
                //    }
                //}
            },
            imgMouseDown = function (e) {
                e.stopImmediatePropagation();

                obj.state.dragable = true;
                obj.state.mouseX = e.clientX;
                obj.state.mouseY = e.clientY;
            },
            imgMouseMove = function (e) {
                e.stopImmediatePropagation();

                if (obj.state.dragable) {
                    var x = e.clientX - obj.state.mouseX;
                    var y = e.clientY - obj.state.mouseY;

                    var bg = el.css('background-position').split(' ');

                    var bgX = x + parseInt(bg[0]);
                    var bgY = y + parseInt(bg[1]);

                    el.css('background-position', bgX + 'px ' + bgY + 'px');

                    obj.state.mouseX = e.clientX;
                    obj.state.mouseY = e.clientY;
                }
            },
            imgMouseUp = function (e) {
                e.stopImmediatePropagation();
                obj.state.dragable = false;
            },
            zoomImage = function (e) {
                e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0 ? obj.ratio *= 1.1 : obj.ratio *= 0.9;
                setBackground();
            }

        obj.spinner.show();
        obj.image.onload = function () {
            obj.spinner.hide();
            setBackground();

            el.bind('mousedown', imgMouseDown);
            el.bind('mousemove', imgMouseMove);
            $(window).bind('mouseup', imgMouseUp);
            el.bind('mousewheel DOMMouseScroll', zoomImage);
        };
        obj.image.src = options.imgSrc;
        el.on('remove', function () { $(window).unbind('mouseup', imgMouseUp) });

        return obj;
    };

    jQuery.fn.cropbox = function (options) {
        return new cropbox(options, this);
    };
}));

