﻿var TEXT_QUESTION_MAX_COUNT = 120;
var TEXT_DESCRIPTION_MAX_COUNT = 136;
var TEXT_OPTION_MAX_COUNT = 15;
const TEXT_RANGE_LABEL_MAX_COUNT = 15;

const LABEL_MAPPING = ["A", "B", "C", "D", "E", "F", "G", "H"];
const WEEK_MAPPING = ["", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

function Pulse(deckId) {
    this.DeckId = deckId;
    this.PulseId = "undefined" + (new Date()).toISOString().replace(/-|T|:|\.|Z/g, "");
    this.Lable = null;
    this.CardType = 1;
    this.PulseType = 3;
    this.LogicLevel = 0;
    this.NumberOfOptions = 2;
    this.Option1Content = "";
    this.Option2Content = "";
    this.Option3Content = "";
    this.Option4Content = "";
    this.Option5Content = "";
    this.Option6Content = "";
    this.Option7Content = "";
    this.Option8Content = "";
    this.UseCustomImage = "";
    this.CustomImageDataUrl = "";
    this.StartDate = moment().utc();
    this.RangeType = 1;
    this.MaxRangeLabel = "";
    this.MinRangeLabel = "";
    this.Title = "";
    this.Description = "";
    this.Options = [];
    this.Ordering = "";
    this.Option1Id = null;
    this.Option1NextCardId = null;
    this.Option2Id = null;
    this.Option2NextCardId = null;
    this.Option3Id = null;
    this.Option3NextCardId = null;
    this.Option4Id = null;
    this.Option4NextCardId = null;
    this.Option5Id = null;
    this.Option5NextCardId = null;
    this.Option6Id = null;
    this.Option6NextCardId = null;
    this.Option7Id = null;
    this.Option7NextCardId = null;
    this.Option8Id = null;
    this.Option8NextCardId = null;
    this.HasIncomingLogic = false;
    this.HasOutgoingLogic = false;
    this.ImageUrl = null;
    this.ParentCard = null;
    this.ParentOptionIndex = 0;
    this.DafaultStartDateTime = null;
    this.IsCustomized = false;
    /***** not sure be used *****/
    this.ActionTaken = 0;
    this.Actions = null;
    this.DeletedDate = "0001-01-01T00=00=00";
    this.EndDate = null;
    this.EndDateString = null;
    this.IsAnonymous = false;
    this.IsComplusory = false;
    this.IsForDepartment = false;
    this.IsForEveryone = false;
    this.IsForUser = false;
    this.IsPrioritized = false;
    this.ParentOption = null;
    this.ProgressStatus = 0;
    this.RespondentChart = null;
    this.SavedDate = "0001-01-01T00=00=00";
    this.SelectedOption = null;
    this.SelectedRange = 0;
    this.StartDateString = null;
    this.Status = 0;
    this.TargetedDepartments = null;
    this.TargetedUsers = null;
    this.TextAnswer = null;
    this.TextAnswers = null;
    this.TotalRespondents = 0;
}

function Option() {
    this.Content = "";

    /***** not sure be used *****/
    this.OptionId = "";
    this.NextPulse = "";
    this.NumberOfSelection = -1;
    this.PercentageOfSelection = -1;
    this.NextPulseId = "";
}

function addNewCard() {
    var html, holderHtml = "", pulse;
    /*** Step 1. Add a new pulse object to model.deck.Pulses array. ***/
    pulse = new Pulse($("#main_content_hfDeckId").val());
    pulse.LogicLevel = 1;
    if (model.deck.Pulses.length == 0) {
        pulse.Lable = "RP1";
        pulse.Ordering = "1";
    }
    else {
        pulse.Lable = "RP" + (parseInt(model.deck.Pulses[0].Ordering.split(".")[0]) + 1);
        pulse.Ordering = (parseInt(model.deck.Pulses[0].Ordering.split(".")[0]) + 1);
    }

    // Determine pulse start date time
    if (model.deck.publishMethodType == 1) // Schedule
    {
        pulse.StartDate = model.deck.startDate; // If Schedule, assume pulse's default date is 1 card per day.
        pulse.DafaultStartDateTime = model.deck.startDate;
    }
    else if (model.deck.publishMethodType == 2) // Routine
    {
        // If Routine, new pulse's start date should be follow as Deck routine role.
        if (model.deck.perTimeFrameType == 1) // per day
        {
            if (model.deck.Pulses.length == 0) // new pulse
            {
                var deckStartDate = moment.utc(model.deck.startDate);
                if (moment(deckStartDate).isBefore(moment.utc())) // start datetime of deck if before right now.
                {
                    // Step 1. 必須先轉換成 user 的 timezone 計算，之後再轉回 UTC。
                    deckStartDate = moment(deckStartDate).add(timezone, 'hours');
                    var newDateTime = moment().format("DD/MM/YYYY") + " " + deckStartDate.format("hh") + ":" + deckStartDate.format("mm") + " " + deckStartDate.format("A");
                    if (moment.utc(newDateTime, "DD/MM/YYYY hh:mm A").add(-timezone, 'hours').isBefore(moment.utc())) // 新的時間小過於此時此刻，必須加多一天。
                    {
                        pulse.StartDate = moment.utc(newDateTime, "DD/MM/YYYY hh:mm A").add(1, 'days').add(-timezone, 'hours');

                    }
                    else {
                        pulse.StartDate = moment.utc(newDateTime, "DD/MM/YYYY hh:mm A").add(-timezone, 'hours');
                    }
                }
                else {
                    pulse.StartDate = model.deck.startDate;
                }
            }
            else {
                // Step 1. Get last non-customized and logicLevel is 1 pulse.
                var lastNonCustomizedPulse = null;
                for (var i = 0; i < model.deck.Pulses.length; i++) {
                    if (model.deck.Pulses[i].LogicLevel <= 1 && !model.deck.Pulses[i].IsCustomized) {
                        lastNonCustomizedPulse = model.deck.Pulses[i];
                        break;
                    }
                }

                // Step 2. If no non-customized pulse in the deck. Get last pulse.
                if (lastNonCustomizedPulse == null) {
                    lastNonCustomizedPulse = model.deck.Pulses[0];
                }

                // Step 3. 計算卡數
                var count = 0;
                for (var i = 0; i < model.deck.Pulses.length; i++) {
                    if (moment.utc(model.deck.Pulses[i].StartDate).isSame(moment.utc(lastNonCustomizedPulse.StartDate))) {
                        count++;
                    }
                }

                // Step 4. 設定日期
                if (count < model.deck.numberOfCardsPerTimeFrame) {
                    pulse.StartDate = moment.utc(lastNonCustomizedPulse.StartDate);
                }
                else {
                    pulse.StartDate = moment.utc(lastNonCustomizedPulse.StartDate).add(1, 'days');
                }

                // Step 5. 確認日期
                if (pulse.StartDate.isSameOrBefore(moment.utc())) {
                    var deckStartDate = moment.utc(model.deck.startDate).add(timezone, 'hours');
                    var newDateTime = moment.utc().format("DD/MM/YYYY") + " " + deckStartDate.format("hh") + ":" + deckStartDate.format("mm") + " " + deckStartDate.format("A");
                    pulse.StartDate = moment.utc(newDateTime, "DD/MM/YYYY hh:mm A").add(-timezone, 'hours').add(1, 'days');
                }
            }

            pulse.DafaultStartDateTime = pulse.StartDate;
        }
        else if (model.deck.perTimeFrameType == 2) // per week
        {
            if (model.deck.Pulses.length == 0) // new pulse
            {
                var deckStartDate = moment.utc(model.deck.startDate);
                var basicDate;

                // Step 1. 必須先轉換成 user 的 timezone 計算，之後再轉回 UTC。
                deckStartDate = deckStartDate.add(timezone, 'hours');

                // Step 2. 設定起始計算日期
                if (moment.utc(model.deck.startDate).isBefore(moment.utc())) // start datetime of deck if before right now.
                {
                    basicDate = moment().format("YYYY/MM/DD") + " " + deckStartDate.format("hh") + ":" + deckStartDate.format("mm") + " " + deckStartDate.format("A");
                    basicDate = moment.utc(basicDate);
                }
                else {
                    basicDate = deckStartDate;
                }

                // Step 3. 計算 pulse 的 start date.
                var weekBasicDate = -1;
                for (var i = 0; i < WEEK_MAPPING.length; i++) {
                    if (basicDate.format("dddd") == WEEK_MAPPING[i]) {
                        weekBasicDate = i;
                        break;
                    }
                }

                var diffDays = model.deck.periodRule - weekBasicDate;
                if (diffDays > 0) {
                    pulse.StartDate = basicDate.add(diffDays, 'days').add(-timezone, 'hours');
                }
                else {
                    pulse.StartDate = basicDate.add((7 + diffDays), 'days').add(-timezone, 'hours');
                }

            }
            else {
                // Step 1. Get last non-customized and logicLevel is 1 pulse.
                var lastNonCustomizedPulse = null;
                for (var i = 0; i < model.deck.Pulses.length; i++) {
                    if (model.deck.Pulses[i].LogicLevel <= 1 && !model.deck.Pulses[i].IsCustomized) {
                        lastNonCustomizedPulse = model.deck.Pulses[i];
                        break;
                    }
                }

                // Step 2. If no non-customized pulse in the deck. Get last pulse.
                if (lastNonCustomizedPulse == null) {
                    lastNonCustomizedPulse = model.deck.Pulses[0];
                }

                // Step 3. 計算卡數
                var count = 0;
                for (var i = 0; i < model.deck.Pulses.length; i++) {
                    if ((model.deck.Pulses[i].LogicLevel == 0 || model.deck.Pulses[i].LogicLevel == 1) && moment.utc(model.deck.Pulses[i].StartDate).isSame(moment.utc(lastNonCustomizedPulse.StartDate))) {
                        count++;
                    }
                }

                // Step 4. 設定日期
                if (count < model.deck.numberOfCardsPerTimeFrame) {
                    pulse.StartDate = moment.utc(lastNonCustomizedPulse.StartDate);
                }
                else {
                    pulse.StartDate = moment.utc(lastNonCustomizedPulse.StartDate).add(7, 'days');
                }

                // Step 5. 確認日期
                if (pulse.StartDate.isSameOrBefore(moment.utc())) {
                    var deckStartDate = moment.utc(model.deck.startDate).add(timezone, 'hours');
                    var basicDate = moment.utc().format("YYYY/MM/DD") + " " + deckStartDate.format("hh") + ":" + deckStartDate.format("mm") + " " + deckStartDate.format("A");
                    basicDate = moment.utc(basicDate, "YYYY/MM/DD hh:mm A");

                    var weekBasicDate = -1;
                    for (var i = 0; i < WEEK_MAPPING.length; i++) {
                        if (basicDate.format("dddd") == WEEK_MAPPING[i]) {
                            weekBasicDate = i;
                            break;
                        }
                    }

                    var diffDays = model.deck.periodRule - weekBasicDate;
                    if (diffDays > 0) {
                        pulse.StartDate = basicDate.add(diffDays, 'days').add(-timezone, 'hours');
                    }
                    else {
                        pulse.StartDate = basicDate.add((7 + diffDays), 'days').add(-timezone, 'hours');
                    }
                }
            }

            pulse.DafaultStartDateTime = pulse.StartDate;
        }
        else  // per month
        {
            if (model.deck.Pulses.length == 0) // new pulse
            {
                //var deckStartDate = moment.utc(moment(model.deck.startDate).format("YYYY/MM/DD HH:mm:ss"), "YYYY/MM/DD HH:mm:ss");
                var deckStartDate = moment.utc(model.deck.startDate);
                var basicDate;

                // Step 1. 必須先轉換成 user 的 timezone 計算，之後再轉回 UTC。
                deckStartDate = deckStartDate.add(timezone, 'hours');

                // Step 2. 設定起始計算日期
                if (moment.utc(model.deck.startDate).isBefore(moment.utc())) // start datetime of deck if before right now.
                {
                    basicDate = moment().format("YYYY MM") + " 01 " + deckStartDate.format("hh:mm A");
                    basicDate = moment.utc(basicDate, "YYYY MM DD hh:mm A");
                }
                else {
                    basicDate = moment.utc(deckStartDate.format("YYYY MM") + " 01 " + deckStartDate.format("HH:mm:ss"), "YYYY MM DD HH:mm:ss");
                }

                // Step 3. 計算 pulse 的 start date.
                basicDate = basicDate.add(1, 'months');
                switch (model.deck.periodRule) {
                    case 1: // first work day of the month

                        if (basicDate.format("dddd") == WEEK_MAPPING[6]) {
                            pulse.StartDate = basicDate.add(2, 'days').add(-timezone, 'hours');
                        }
                        else if (basicDate.format("dddd") == WEEK_MAPPING[7]) {
                            pulse.StartDate = basicDate.add(1, 'days').add(-timezone, 'hours');
                        }
                        else {
                            pulse.StartDate = basicDate.add(-timezone, 'hours');
                        }
                        break;

                    case 2: // laast work day of the month
                        basicDate = basicDate.add(-1, 'days');

                        /* 檢查如果當日剛好是當月的最後一天*/
                        var bDateOnlyDate = moment(basicDate.format("YYYY/MM/DD"), "YYYY/MM/DD");
                        var nowDateOnlyDate = moment(moment().format("YYYY/MM/DD"), "YYYY/MM/DD");
                        if (bDateOnlyDate.isSameOrBefore(nowDateOnlyDate)) {
                            basicDate = moment.utc(basicDate.format("YYYY MM") + " 01 " + basicDate.format("HH:mm:ss"), "YYYY MM DD HH:mm:ss");
                            basicDate = basicDate.add(2, 'months');
                            basicDate = basicDate.add(-1, 'days');
                        }

                        if (basicDate.format("dddd") == WEEK_MAPPING[6]) {
                            pulse.StartDate = basicDate.add(-2, 'days').add(-timezone, 'hours');
                        }
                        else if (basicDate.format("dddd") == WEEK_MAPPING[7]) {
                            pulse.StartDate = basicDate.add(-1, 'days').add(-timezone, 'hours');
                        }
                        else {
                            pulse.StartDate = basicDate.add(-timezone, 'hours');
                        }
                        break;

                    case 3: // particular date of the month
                        // 特別處理
                        if (moment.utc(model.deck.startDate).isBefore(moment.utc())) // start datetime of deck if before right now.
                        {
                            var newDateTime = (deckStartDate.add(1, "months")).format("YYYY MM DD HH:mm:ss");
                            if (moment(newDateTime, 'YYYY MM DD HH:mm:ss', true).isValid()) {
                                pulse.StartDate = moment.utc(newDateTime, "YYYY MM DD HH:mm:ss").add(-timezone, 'hours');
                            }
                            else {
                                basicDate = moment.utc((deckStartDate.add(2, "months")).format("YYYY MM") + " 01 " + deckStartDate.format("HH:mm:ss"), "YYYY MM DD HH:mm:ss");
                                basicDate = basicDate.add(-1, 'days');
                                pulse.StartDate = basicDate.add(-timezone, 'hours');
                            }
                        }
                        else {
                            pulse.StartDate = model.deck.startDate;
                        }
                        break;

                    default:
                        break;
                }
            }
            else {
                // Step 1. Get last non-customized and logicLevel is 1 pulse.
                var lastNonCustomizedPulse = null;
                for (var i = 0; i < model.deck.Pulses.length; i++) {
                    if (model.deck.Pulses[i].LogicLevel <= 1 && !model.deck.Pulses[i].IsCustomized) {
                        lastNonCustomizedPulse = model.deck.Pulses[i];
                        break;
                    }
                }

                // Step 2. If no non-customized pulse in the deck. Get last pulse.
                if (lastNonCustomizedPulse == null) {
                    lastNonCustomizedPulse = model.deck.Pulses[0];
                }

                // Step 3. 計算卡數
                var count = 0;
                for (var i = 0; i < model.deck.Pulses.length; i++) {
                    if (moment.utc(model.deck.Pulses[i].StartDate).isSame(moment.utc(lastNonCustomizedPulse.StartDate))) {
                        count++;
                    }
                }

                // Step 4. 設定日期
                if (count < model.deck.numberOfCardsPerTimeFrame) {
                    pulse.StartDate = moment.utc(lastNonCustomizedPulse.StartDate);
                }
                else {

                    var basicDate = moment.utc(lastNonCustomizedPulse.StartDate).add(1, 'months').add(timezone, 'hours');
                    basicDate = moment.utc(basicDate.format("YYYY MM") + " 01 " + basicDate.format("HH:mm:ss"), "YYYY MM DD HH:mm:ss");
                    switch (model.deck.periodRule) {
                        case 1: // first work day of the month
                            if (basicDate.format("dddd") == WEEK_MAPPING[6]) {
                                pulse.StartDate = basicDate.add(2, 'days').add(-timezone, 'hours');
                            }
                            else if (basicDate.format("dddd") == WEEK_MAPPING[7]) {
                                pulse.StartDate = basicDate.add(1, 'days').add(-timezone, 'hours');
                            }
                            else {
                                pulse.StartDate = basicDate.add(-timezone, 'hours');
                            }
                            break;

                        case 2: // laast work day of the month
                            basicDate = moment.utc(basicDate.format("YYYY MM") + " 01 " + basicDate.format("HH:mm:ss"), "YYYY MM DD HH:mm:ss");
                            basicDate = basicDate.add(1, 'months');
                            basicDate = basicDate.add(-1, 'days');

                            if (basicDate.format("dddd") == WEEK_MAPPING[6]) {
                                pulse.StartDate = basicDate.add(-2, 'days').add(-timezone, 'hours');
                            }
                            else if (basicDate.format("dddd") == WEEK_MAPPING[7]) {
                                pulse.StartDate = basicDate.add(-1, 'days').add(-timezone, 'hours');
                            }
                            else {
                                pulse.StartDate = basicDate.add(-timezone, 'hours');
                            }
                            break;

                        case 3: // particular date of the month
                            var lastPulseDateTime = moment.utc(lastNonCustomizedPulse.StartDate).add(timezone, 'hours');
                            var deckStartDate = moment.utc(model.deck.startDate, "YYYY-MM-DD HH:mm:ss").add(timezone, 'hours');

                            var newDateTime = (lastPulseDateTime.add(1, "months")).format("YYYY MM") + " " + deckStartDate.format("DD HH:mm:ss");
                            if (moment(newDateTime, 'YYYY MM DD HH:mm:ss', true).isValid()) {
                                pulse.StartDate = moment.utc(newDateTime, "YYYY MM DD HH:mm:ss").add(-timezone, 'hours');
                            }
                            else {
                                basicDate = moment.utc((lastPulseDateTime.add(1, "months")).format("YYYY MM") + " 01 " + lastPulseDateTime.format("HH:mm:ss"), "YYYY MM DD HH:mm:ss");
                                basicDate = basicDate.add(-1, 'days');
                                pulse.StartDate = basicDate.add(-timezone, 'hours');
                            }

                            break;

                        default:
                            break;
                    }
                }
            }

            pulse.DafaultStartDateTime = pulse.StartDate;
        }
    }
    else {
        // do nothing
    }

    model.deck.Pulses.unshift(pulse);

    /*** Step 2. 組 new pulse layout ，並插入 $(".card-add") 中。 ***/
    holderHtml += getFullCardHtml(pulse);
    holderHtml += "<hr style=\"border: 1px solid rgb(221, 221, 221); border-image: none;\">";
    $(".card-add").html(holderHtml);

    /*** Step 3. 隱藏 $("button.add-pulse") 按鈕。 ***/
    $("button.add-pulse").hide();

    /*** Step 4. 開啟月曆選日功能。 ***/
    $(".cardStartDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd M yy, DD'
    });
}


function getFullCardHtml(pulse) {
    var holderHtml = "";
    var mStartDate = moment.utc(pulse.StartDate).add(timezone, 'hours');
    var cardLevel = 1;
    if (pulse.LogicLevel > 1) {
        cardLevel = pulse.LogicLevel
    }

    var hideChildcards = "";
    if ($(".card.mini-card." + pulse.PulseId + ".hide-childcards").length > 0) {
        hideChildcards = "hide-childcards";
    }

    holderHtml += "     <div class=\"card full-card " + pulse.PulseId + " card-level-" + cardLevel + " " + hideChildcards + "\" style=\"position: relative;\" data-cardid=\"" + pulse.PulseId + "\"";
    if (pulse.ParentCard != null && pulse.ParentCard != undefined) {
        holderHtml += "data-parentcardid=\"" + pulse.ParentCard.PulseId + "\">";
    }
    else {
        holderHtml += "data-parentcardid=\"\">";
    }
    holderHtml += "         <div style=\"width: 100%;\">";
    if (pulse.Lable) {
        holderHtml += "             <label style=\"width: 23%; height: 90px; color: rgb(204, 204, 204); font-size: 36pt; display: inline-block; font-weight: bolder;\">" + pulse.Lable + "</label>";
    }
    else {
        holderHtml += "             <label style=\"width: 23%; height: 90px; color: rgb(204, 204, 204); font-size: 36pt; display: inline-block; font-weight: bolder;\">RP" + pulse.Ordering + "</label>";
    }
    holderHtml += "             <div style=\"width: 75%; height: 90px; vertical-align: bottom; display: inline-block;\">";
    holderHtml += "                 <div style=\"top: 25px; right: 116px; position: absolute;\">";
    holderHtml += "                     <button IsDisabled style=\"width: 120px; height: 40px;\" onclick=\"savePulse('" + pulse.PulseId + "', false);\" type=\"button\">Save</button>";
    holderHtml += "                 </div>";

    if (pulse.PulseId.indexOf('undefined') != 0) // Edit card mode. Add to mini card button.
    {
        holderHtml += "                 <div>";
        holderHtml += "                     <div class=\"grid-question-expandable\">";
        holderHtml += "                         <button type=\"button\" class=\"to-mini-card\" onclick=\"switchToMiniCard('" + pulse.PulseId + "');\">";
        holderHtml += "                             <i class=\"fa fa-caret-square-o-down fa-lg icon\"></i>";
        holderHtml += "                         </button>";
        holderHtml += "                     </div>";
        holderHtml += "                 </div>";
    }
    holderHtml += "                 <div class=\"tabs--styled\" style=\"vertical-align: bottom;\">";
    holderHtml += "                     <ul class=\"tabs__list\">";
    holderHtml += "                         <li class=\"tabs__list__item\">";
    holderHtml += "                             <a class=\"tabs__link active\" href=\"javascript:void(0);\">Card Type</a>";
    holderHtml += "                         </li>";
    holderHtml += "                     </ul>";
    holderHtml += "                 </div>";
    holderHtml += "                 <div class=\"grid__inner pulse-type-selection-panel\" style=\"margin: 4px 0px; background-color: transparent;\">";
    holderHtml += "                     <div>";
    if (pulse.CardType == 1) {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type select-one-pulse active\" style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 1);\" type=\"button\" data-cardtype=\"1\">";
    }
    else {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type select-one-pulse\" style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 1);\" type=\"button\" data-cardtype=\"1\">";
    }
    holderHtml += "                             <img title=\"Select one\" style=\"width: 15px; vertical-align: middle;\" src=\"/Img/icon_selectone.png\">";
    holderHtml += "                             <span style=\"vertical-align: middle; display: inline-block;\"> Select One</span>";
    holderHtml += "                         </button>";
    if (pulse.CardType == 2) {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type range-pulse active\" style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 2);\" type=\"button\" data-cardtype=\"2\">";
    }
    else {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type range-pulse\" style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 2);\" type=\"button\" data-cardtype=\"2\">";
    }
    holderHtml += "                             <img title=\"Select one\" style=\"width: 15px; vertical-align: middle;\" src=\"/Img/icon_numberrange.png\">";
    holderHtml += "                             <span style=\"vertical-align: middle; display: inline-block;\"> Number Range</span>";
    holderHtml += "                         </button>";
    if (pulse.CardType == 3) {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type text-pulse active\" style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 3);\" type=\"button\" data-cardtype=\"3\">";
    }
    else {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type text-pulse\" style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 3);\" type=\"button\" data-cardtype=\"3\">";
    }
    holderHtml += "                             <img title=\"Select one\" style=\"width: 15px; vertical-align: middle;\" src=\"/Img/icon_text.png\">";
    holderHtml += "                             <span style=\"vertical-align: middle; display: inline-block;\"> Text</span>";
    holderHtml += "                         </button>";
    if (pulse.CardType == 4) {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type logic-pulse active\"  style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 4);\" type=\"button\" data-cardtype=\"4\">";
    }
    else {
        holderHtml += "                         <button disabled=\"disabled\" class=\"pulse-type logic-pulse\"  style=\"margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: left;\" onclick=\"changeCardType(this, 4);\" type=\"button\" data-cardtype=\"4\">";
    }
    holderHtml += "                             <img title=\"Select one\" style=\"width: 15px; vertical-align: middle;\" src=\"/Img/icon_selectone.png\">";
    holderHtml += "                             <span style=\"vertical-align: middle; display: inline-block;\"> Logic</span>";
    holderHtml += "                         </button>";

    if (pulse.PulseId.indexOf('undefined') == 0) // 新 pulse 開啟 button 功能。
    {
        holderHtml = holderHtml.replace(/button disabled=\"disabled\"/g, "button");
    }

    if (model.deck.publishMethodType == 1) // Publish method is "Schedule". It is not allowed to edit start date of card.
    {
        holderHtml += "                         <div style=\"padding: 10px; width: 40%; float: right; display: inline-block;\">";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">Date</span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("DD MMM YYYY") + " " + mStartDate.format("dddd") + "," + "</span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\"> at </span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("hh") + "</span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\"> : </span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("mm") + "</span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("A") + "</span>";
        holderHtml += "                         </div>";
    }
    else // Publish method is "Routine".  It is allowed to edit start date of card.
    {
        holderHtml += "                         <div style=\"padding: 10px; width: 40%; float: right; display: inline-block;\">";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">Date</span>";
        //holderHtml += "                             <input class=\"cardStartDate\" style=\"border-width: 0px 0px 1px; border-style: none none solid; border-color: currentColor currentColor rgb(193, 193, 193); padding: 0.5em; border-image: none; width: 170px; margin-bottom: 0px; display: inline;\" onchange=\"setPulse('" + pulse.PulseId + "', 6, this, null);\" type=\"text\" placeholder=\"dd/mm/yyyy\" value=\"" + mStartDate.format("DD MMM YYYY, dddd") + "\">";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("DD MMM YYYY") + " " + mStartDate.format("dddd") + "," + "</span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\"> at </span>";
        holderHtml += "                             <span class=\"cardStartHH\" style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("hh") + "</span>";
        holderHtml += "                             <span style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\"> : </span>";
        holderHtml += "                             <span class=\"cardStartMM\" style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("mm") + "</span>";
        holderHtml += "                             <span class=\"cardStartMer\" style=\"font-weight: bold; display: inline-block; color: rgba(165, 165, 165, 1);\">" + mStartDate.format("A") + "</span>";
        holderHtml += "                         </div>";
    }
    holderHtml += "                     </div>";
    holderHtml += "                 </div>";
    holderHtml += "             </div>";
    holderHtml += "         </div>";
    /***** card body *****/
    holderHtml += "         <div class=\"pulse-card-container\" style=\"width: 50%; margin-top: 20px; display: inline-block;\">";
    holderHtml += getCardBodyHtml(pulse);
    holderHtml += "         </div>";
    holderHtml += "         <div class=\"card-logic\" style=\"width: 45%; display: inline-block;vertical-align:top;margin-top:20px;margin-left: 20px;\">";
    holderHtml += getCardLogicHtml(pulse);
    holderHtml += "         </div>";
    /***** card body *****/
    holderHtml += "         <div style=\"display: inline-block; background-color: transparent; width: 100%\">";
    holderHtml += "             <p></p>";
    holderHtml += "             <div class=\"grid__span--4\">Uploaded image up to 200 x 200 and no bigger than 60KB.    </div>";
    holderHtml += "             <div class=\"grid__span--6 grid__span--last\" style=\"width: 750px;\">";
    if (pulse.PulseId.indexOf('undefined') == 0) // Add a card mode
    {
        holderHtml += "                 <button style=\"float: right; background-color: red;\" onclick=\"cancelAddCard(this, '" + pulse.PulseId + "');\" type=\"button\">Cancel</button>";
    }
    else // Edit card mode
    {
        holderHtml += "                 <button IsDisabled style=\"float: right; background-color: red;\" onclick=\"showDelPulsePop('" + pulse.PulseId + "', '" + mStartDate.format("DD MMM YYYY, dddd hh:mm A") + "');\" type=\"button\">Delete</button>";
    }
    holderHtml += "             </div>";
    holderHtml += "          </div>";
    holderHtml += "     </div>";

    // 開關 elements
    var isEnabled = true;
    if (pulse.PulseId.indexOf('undefined') < 0) {
        if (model.deck.status != 1) // active / hidden
        {
            if (moment.utc(pulse.StartDate).isSameOrBefore(moment.utc())) {
                isEnabled = false
            }
        }
    }

    if (isEnabled) {
        holderHtml = holderHtml.replace(/IsDisabled/g, "");
    }
    else {
        holderHtml = holderHtml.replace(/IsDisabled/g, "disabled=\"disabled\"");
    }

    return holderHtml;
}

function cancelAddCard(button, pulseId) {
    // Step 1. Get Pulse
    var pulse;
    pulse = getPulse(pulseId);

    // Step 2. Update UI
    if (pulse.ParentCard == null) // Level 1
    {
        // remove child layout
        $(".card-add").html("");
        $("button.add-pulse").show();

        // Update data
        removePulse(pulseId);
    }
    else // Level 2 or 3
    {
        var cardLayout, parentPulseId, parentOptionNo;

        // remove child UI
        cardLayout = $(button).parents(".card.full-card." + pulse.PulseId);
        cardLayout.remove();

        // Update data
        parentPulseId = pulse.ParentCard.PulseId;
        parentOptionNo = pulse.ParentOptionIndex;

        removePulse(pulse.PulseId); // remove children

        pulse = getPulse(parentPulseId);
        switch (parentOptionNo) {
            case 1:
                pulse.Option1NextCardId = "PULSESETEND";
                break;
            case 2:
                pulse.Option2NextCardId = "PULSESETEND";
                break;
            case 3:
                pulse.Option3NextCardId = "PULSESETEND";
                break;
            case 4:
                pulse.Option4NextCardId = "PULSESETEND";
                break;
            case 5:
                pulse.Option5NextCardId = "PULSESETEND";
                break;
            case 6:
                pulse.Option6NextCardId = "PULSESETEND";
                break;
            case 7:
                pulse.Option7NextCardId = "PULSESETEND";
                break;
            case 8:
                pulse.Option8NextCardId = "PULSESETEND";
                break;
            default:
                break;
        }

        switchToMiniCard(parentPulseId);
        switchToFullCard(parentPulseId); // Update parent UI
    }
}


function removePulse(pulseId) {
    if (model.deck.Pulses != null && model.deck.Pulses.length > 0) {
        var index = -1;
        for (var i = 0; i < model.deck.Pulses.length; i++) {
            if (model.deck.Pulses[i].PulseId == pulseId) {
                index = i;
                break;
            }
        }

        if (index > -1) {
            model.deck.Pulses.splice(index, 1);
        }
    }
}

function replacePulse(oldPulseId, newPulse) {
    if (model.deck.Pulses != null && model.deck.Pulses.length > 0) {
        var index = -1;
        for (var i = 0; i < model.deck.Pulses.length; i++) {
            if (model.deck.Pulses[i].PulseId == oldPulseId) {
                index = i;
                break;
            }
        }
        model.deck.Pulses[index] = newPulse;
    }
}

function changeCardType(button, newCardType, pulseId) {
    var cardLayout, pulse;

    if (pulseId)// 透過 pulseId 來找出要改變的 Layout
    {
        cardLayout = $(".card-list .card." + pulseId);
    }
    else // 沒有 cardId，為 new card，透過 button 找出父元素。此情況為 add a new card。
    {
        cardLayout = $(button).parents(".card");
        pulseId = cardLayout.data("cardid");
    }

    // Step 0. Get pulse.
    pulse = getPulse(pulseId);

    // Step 1. Update data.
    pulse.CardType = newCardType;
    if (newCardType == 1 || newCardType == 4) // Select one / Logic
    {
        if (pulse.NumberOfOptions == null || pulse.NumberOfOptions < 2) {
            pulse.NumberOfOptions = 2;
        }

        if (pulse.Option1Content == null) {
            pulse.Option1Content = "";
        }

        if (pulse.Option2Content == null) {
            pulse.Option2Content = "";
        }
    }
    else if (newCardType == 2)// Number Range
    {
        if (pulse.RangeType == null) {
            pulse.RangeType = 1;
        }

    }
    else if (newCardType == 3)// Text
    {
        // do nothing
    }
    else {
        // do nothing
    }

    updatePulse(pulse);

    // Step 2. Update UI.
    cardLayout.find("button.pulse-type").removeClass("active"); // 移除該 card layout 中，change type button UI 設定。
    switch (pulse.CardType) {
        case 1:
            cardLayout.find("button.select-one-pulse").addClass("active");
            cardLayout.find(".pulse-card-container .pulse-card").remove();
            cardLayout.find(".pulse-card-container").append(getCardBodyHtml(pulse));
            cardLayout.find(".card-logic").children().remove();
            break;
        case 2:
            cardLayout.find("button.range-pulse").addClass("active");
            cardLayout.find(".pulse-card-container .pulse-card").remove();
            cardLayout.find(".pulse-card-container").append(getCardBodyHtml(pulse));
            updateRange(cardLayout.find(".select-range")[0], pulse.PulseId);
            cardLayout.find(".card-logic").children().remove();
            break;
        case 3:
            cardLayout.find("button.text-pulse").addClass("active");
            cardLayout.find(".pulse-card-container .pulse-card").remove();
            cardLayout.find(".pulse-card-container").append(getCardBodyHtml(pulse));
            cardLayout.find(".card-logic").children().remove();
            break;
        case 4:
            cardLayout.find("button.logic-pulse").addClass("active");
            cardLayout.find(".pulse-card-container .pulse-card").remove();
            cardLayout.find(".pulse-card-container").append(getCardBodyHtml(pulse));
            cardLayout.find(".card-logic").children().remove();
            cardLayout.find(".card-logic").append(getCardLogicHtml(pulse));
            break;
    }

    updateTextCounter(pulse);
}

function updateOptions(dropdown, pulseId) {
    // Step 0. Get pulse.
    var pulse = getPulse(pulseId);

    // Step 1. Update data.
    if (dropdown) {
        pulse.NumberOfOptions = parseInt(dropdown.value);
    }
    //else {
    //    pulse.NumberOfOptions = 0;
    //}
    updatePulse(pulse);

    // Step 2. Update UI.
    var cardLayout, html = "";
    cardLayout = $(dropdown).parents(".card");
    $(cardLayout).find(".option-list").html("");

    for (var i = 0; i < pulse.NumberOfOptions; i++) {
        var optionDefalutHtml = "";
        optionDefalutHtml += "<div style=\"position: relative; width: 48%; display: inline-block; margin: 5px; \">";
        optionDefalutHtml += "<input IsDisabled class=\"pulse-option option" + (i + 1) + "\" style=\"padding: 10px; height: 50px; border-radius: 0.5em; width: 100%;\" type=\"text\" placeholder onkeyup value >"; // default html
        optionDefalutHtml += "<label class=\"pulseOption" + (i + 1) + "Counter\" style=\"width: auto; right: 17px; bottom: 3px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">99</label>";
        optionDefalutHtml += "</div>";
        optionDefalutHtml = optionDefalutHtml.replace("placeholder", "placeholder=\"Option " + (i + 1) + "\""); // for placeholder
        optionDefalutHtml = optionDefalutHtml.replace("onkeyup", "onkeyup=\"setPulse('" + pulseId + "', 3, this, " + (i + 1) + "); updateCardLogicOptionUI(this, '" + pulse.PulseId + "',  " + (i + 1) + ");\""); // for onkeyup

        if (i == 0) {
            if (pulse.Option1Content != null && pulse.Option1Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option1Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else if (i == 1) {
            if (pulse.Option2Content != null && pulse.Option2Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option2Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else if (i == 2) {
            if (pulse.Option3Content != null && pulse.Option3Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option3Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else if (i == 3) {
            if (pulse.Option4Content != null && pulse.Option4Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option4Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else if (i == 4) {
            if (pulse.Option5Content != null && pulse.Option5Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option5Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else if (i == 5) {
            if (pulse.Option6Content != null && pulse.Option6Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option6Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else if (i == 6) {
            if (pulse.Option7Content != null && pulse.Option7Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option7Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else if (i == 7) {
            if (pulse.Option8Content != null && pulse.Option8Content.length > 0) {
                html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option8Content + "\"");
            }
            else {
                html += optionDefalutHtml.replace("value", "");
            }
        }
        else {
            // do nothing
        }
    }
    $(cardLayout).find(".option-list").append(html);

    // if card is logic type, add logic option
    cardLayout.find(".card-logic").children().remove();
    if (pulse.CardType == 4) {
        cardLayout.find(".card-logic").append(getCardLogicHtml(pulse));
    }

    // update options UI
    updateTextCounter(pulse);
}

function updateRange(dropdown, pulseId) {
    // Step 0. Get pulse.
    var pulse = getPulse(pulseId);
    // Step 1. Update data.
    pulse.RangeType = parseInt(dropdown.value);
    updatePulse(pulse);

    // Step 2. Update UI.
    var cardLayout, html = "";
    cardLayout = $(dropdown).parents(".card");
    $(cardLayout).find(".range-ruler").html("");

    var count, width;
    switch (pulse.RangeType) {
        case 1: // 1 -3
            count = 3;
            break;
        case 2: // 1 - 5
            count = 5;
            break;
        case 3:// 1 - 7
            count = 7;
            break;
        case 4: // 1 - 10
            count = 10;
            break;
        case 5:// 0 - 100%
            count = 11;
            break;
        default:
            // do nothing
            break;
    }

    if (pulse.RangeType == 5) // 0 - 100%
    {
        width = (100 / count) - 0.5;

        for (var i = 0; i < count; i++) {
            if (i == 0) {
                html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>0</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
            }
            else if (i == count - 1) {
                html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>" + i + "0%</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
            }
            else {
                html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>" + i + "0</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
            }
        }
    }
    else {
        width = 100 / count;
        for (var i = 0; i < count; i++) {
            html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>" + (i + 1) + "</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
        }
    }

    $(cardLayout).find(".range-ruler").append(html);
}

function getPulse(pulseId) {
    var cardFound, idx, pulse;

    cardFound = false;
    for (idx = 0; idx < model.deck.Pulses.length; idx++) {
        if (model.deck.Pulses[idx].PulseId == pulseId) {
            return model.deck.Pulses[idx];
        }
        else // 目前不會有此狀況發生。For Pulse 裡還有 pulse 的資料結構。
        {
            if (model.deck.Pulses[idx].CardType == 4) {
                //if (model.deck.Pulses[idx]["Option1Id"])
            }
        }
    }
}

function setPulse(pulseId, argType, element, arg2) {
    var pulse = getPulse(pulseId);
    if (pulse != null) {
        switch (argType) {
            case 1: // Title
                pulse.Title = element.value.trim();
                break;

            case 2: // Description
                pulse.Description = element.value.trim();
                break;

            case 3: // Options
                if (arg2 == 1) // Option1
                {
                    pulse.Option1Content = element.value.trim();
                }
                else if (arg2 == 2)  // Option2
                {
                    pulse.Option2Content = element.value.trim();
                }
                else if (arg2 == 3)  // Option3
                {
                    pulse.Option3Content = element.value.trim();
                }
                else if (arg2 == 4)  // Option4
                {
                    pulse.Option4Content = element.value.trim();
                }
                else if (arg2 == 5)  // Option5
                {
                    pulse.Option5Content = element.value.trim();
                }
                else if (arg2 == 6)  // Option6
                {
                    pulse.Option6Content = element.value.trim();
                }
                else if (arg2 == 7)  // Option7
                {
                    pulse.Option7Content = element.value.trim();
                }
                else if (arg2 == 8)  // Option8
                {
                    pulse.Option8Content = element.value.trim();
                }
                else {
                    // do nothing
                }
                break;

            case 4: // Minimum value of range
                pulse.MinRangeLabel = element.value.trim();
                break;

            case 5: // Maxmum value of range
                pulse.MaxRangeLabel = element.value.trim();
                break;

            case 6: // Start date and time

                var originalDateTime = moment(pulse.StartDate).add(timezone, 'hours');
                var newDateTime = element.value + " " + originalDateTime.format("hh") + ":" + originalDateTime.format("mm") + " " + originalDateTime.format("A");
                if (isDateTimeValid(newDateTime, 1)) {
                    pulse.StartDate = moment(newDateTime, "DD MMM YYYY, dddd hh:mm A").add(-timezone, 'hours');
                }
                else {
                    element.value = originalDateTime.format("DD MMM YYYY, dddd");
                }

                break;
            default:
                break;
        }

        updatePulse(pulse);
    }
}

function updateTextCounter(pulse) {
    // Step 1. Get new text count max
    if ((pulse.ImageUrl != null && pulse.ImageUrl != "") || (pulse.CustomImageDataUrl != null && pulse.CustomImageDataUrl != "")) // has image
    {
        if (pulse.NumberOfOptions == 2 || pulse.NumberOfOptions == 3) {
            TEXT_QUESTION_MAX_COUNT = 76;
            TEXT_DESCRIPTION_MAX_COUNT = 84;
        }
        else if (pulse.NumberOfOptions == 4) {
            TEXT_QUESTION_MAX_COUNT = 57;
            TEXT_DESCRIPTION_MAX_COUNT = 63;
        }
        else {
            TEXT_QUESTION_MAX_COUNT = 76;
            TEXT_DESCRIPTION_MAX_COUNT = 84;
        }
    }
    else // without image
    {
        if (pulse.NumberOfOptions == 2 || pulse.NumberOfOptions == 3) {
            TEXT_QUESTION_MAX_COUNT = 120;
            TEXT_DESCRIPTION_MAX_COUNT = 136;
        }
        else if (pulse.NumberOfOptions == 4) {
            TEXT_QUESTION_MAX_COUNT = 90;
            TEXT_DESCRIPTION_MAX_COUNT = 102;
        }
        else {
            TEXT_QUESTION_MAX_COUNT = 120;
            TEXT_DESCRIPTION_MAX_COUNT = 136;
        }
    }

    if (pulse.NumberOfOptions == 2) {
        TEXT_OPTION_MAX_COUNT = 15;
    }
    else if (pulse.NumberOfOptions == 3) {
        TEXT_OPTION_MAX_COUNT = 9;
    }
    else if (pulse.NumberOfOptions == 4) {
        TEXT_OPTION_MAX_COUNT = 15;
    }
    else {
        TEXT_OPTION_MAX_COUNT = 8;
    }

    // Step 2. Question
    var counter = $(".card.full-card." + pulse.PulseId + " .pulseCardQuestionCounter").html(TEXT_QUESTION_MAX_COUNT - pulse.Title.length);
    if (TEXT_QUESTION_MAX_COUNT - pulse.Title.length < 0) {
        $(".card.full-card." + pulse.PulseId + " .pulseCardQuestionCounter").css("color", "red");
    }
    else {
        $(".card.full-card." + pulse.PulseId + " .pulseCardQuestionCounter").css("color", "rgba(215, 215, 215, 1)");
    }

    // Step 3. Description
    counter = $(".card.full-card." + pulse.PulseId + " .pulseCardDescriptionCounter").html(TEXT_DESCRIPTION_MAX_COUNT - pulse.Description.length);
    if (TEXT_DESCRIPTION_MAX_COUNT - pulse.Description.length < 0) {
        $(".card.full-card." + pulse.PulseId + " .pulseCardDescriptionCounter").css("color", "red");
    }
    else {
        $(".card.full-card." + pulse.PulseId + " .pulseCardDescriptionCounter").css("color", "rgba(215, 215, 215, 1)");
    }

    // Step 4. Options
    if (pulse.NumberOfOptions != null && pulse.NumberOfOptions > 1) {
        var textCount;
        for (var i = 1; i < pulse.NumberOfOptions + 1; i++) {
            switch (i) {
                case 1:
                    if (pulse.Option1Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option1Content.length;
                    }
                    break;
                case 2:
                    if (pulse.Option2Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option2Content.length;
                    }
                    break;
                case 3:
                    if (pulse.Option3Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option3Content.length;
                    }
                    break;
                case 4:
                    if (pulse.Option4Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option4Content.length;
                    }
                    break;
                case 5:
                    if (pulse.Option5Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option5Content.length;
                    }
                    break;
                case 6:
                    if (pulse.Option6Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option6Content.length;
                    }
                    break;
                case 7:
                    if (pulse.Option7Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option7Content.length;
                    }
                    break;
                case 8:
                    if (pulse.Option8Content != null) {
                        textCount = TEXT_OPTION_MAX_COUNT - pulse.Option8Content.length;
                    }
                    break;
                default:
                    // do nothing
                    break;
            }

            $(".card.full-card." + pulse.PulseId + " .pulseOption" + i + "Counter").html(textCount);
            if (textCount < 0) {
                $(".card.full-card." + pulse.PulseId + " .pulseOption" + i + "Counter").css("color", "red");
            }
            else {
                $(".card.full-card." + pulse.PulseId + " .pulseOption" + i + "Counter").css("color", "rgba(215, 215, 215, 1)");
            }

        }
    }
}

function isDateTimeValid(dateTimeString, verifyType) {
    switch (verifyType) {
        case 1: // start date and time of card for routine
            if (!moment(dateTimeString, "DD MMM YYYY, dddd hh:mm A", true).isValid()) {
                ShowToast('The format of Start Date is invalid', 2);
                return false;
            }

            if (moment.utc(dateTimeString, "DD MMM YYYY, dddd hh:mm A").isBefore(moment.utc())) {
                // Start date 不可小過於此時此刻
                ShowToast('The Start Date must be in the future', 2);
                return false;
            }
            break;

        default:
            break;

    }
    return true;
}

function updatePulse(pulse) {
    for (var i = 0; i < model.deck.Pulses.length; i++) {
        if (model.deck.Pulses[i].PulseId == pulse.pulseId) {
            model.deck.Pulses[i] = pulse;
        }
    }

    updateTextCounter(pulse);
}

function getCardBodyHtml(pulse) {
    var html = "", pulseLevel = "pulse-level-", level = 1;

    var isEnabled = true;
    if (pulse.PulseId.indexOf('undefined') < 0) {
        if (model.deck.status != 1) // active / hidden
        {
            if (moment.utc(pulse.StartDate).isSameOrBefore(moment.utc())) {
                isEnabled = false
            }
        }
    }

    if (pulse.LogicLevel > 0) {
        level = pulse.LogicLevel;
    }

    pulseLevel += level;

    if (pulse.CardType == 1 || pulse.CardType == 4)// HTML markup for select-one and logic
    {
        html += "       <div class=\"grid__inner pulse-card select-one-pulse " + pulseLevel + "\" style=\"background-position: center; margin: 0px auto; border-radius: 0.5em; border: 2px solid rgb(204, 204, 204); border-image: none; width: 100%; min-height: 285px; background-image: none; background-repeat: no-repeat; background-color: transparent;\" data-pulselevel=\"" + level + "\">";
        html += "           <div class=\"grid__inner pulse-option-selection-panel\" style=\"margin: 4px; background-color: transparent;\">";
        html += "               <div class=\"grid__span--12 grid__span--last\" style=\"margin: 5px;\">";
        html += "                   <strong>Options</strong>";
        html += "                   <div class=\"mdl-selectfield\" style=\"width: 150px; margin-left: 10px; display: inline-block;\">";
        html += "                       <select IsDisabled class=\"select-option\" style=\"margin: 0px; display: inline-block;\" onchange=\"updateOptions(this,'" + pulse.PulseId + "');\">";
        /***** Option count *****/
        for (var i = 2; i < 7; i++) // Option count change from 8 to 6.
        {
            if (pulse.NumberOfOptions == i) {
                html += "                           <option value=\"" + i + "\" selected=\"selected\">" + i + " Options</option>";
            }
            else {
                html += "                           <option value=\"" + i + "\">" + i + " Options</option>";
            }
        }
        //***** Option count *****/
        html += "                        </select>";
        html += "                   </div>";
        html += "               </div>";
        html += "           </div>";
        html += "           <div class=\"grid__inner\" style=\"background-color: transparent;\">";
        /***** Image *****/
        if (pulse.ImageUrl != null && pulse.ImageUrl.length > 0) {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 30%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: url('" + pulse.ImageUrl + "'); background-size: cover;\">";
            if (isEnabled) {
                html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6);\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
                html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
                html += "                       </div>";
            }
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute; display: none;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        else if (pulse.UseCustomImage) {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 30%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: url('" + pulse.CustomImageDataUrl.base64 + "'); background-size: cover;\">";
            html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6);\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
            html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                       </div>";
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute; display: none;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        else {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 15%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: none; background-size: cover;\">";
            html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6); display: none;\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
            html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                       </div>";
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        /***** Image *****/
        html += "                   </div>";
        html += "               </div>";
        html += "               <div style=\"width: 65%; vertical-align: middle; display: inline-block;\">";
        html += "               <div style=\"position: relative;\">";
        html += "                   <textarea IsDisabled class=\"pulseCardQuestion\" style=\"margin: 5px; border-radius: 0.5em; border: 1px solid rgb(204, 204, 204); border-image: none; width: 100%; height: 150px; color: black; font-size: 14pt; display: block; position: relative; min-height: 150px; background-color: white; resize: none; overflow-wrap: break-word;\" placeholder=\"Question\" rows=\"3\" onkeyup=\"setPulse('" + pulse.PulseId + "', 1, this);\">";
        /***** Title *****/
        if (pulse.Title != null && pulse.Title.length > 0) {
            html += pulse.Title;
        }
        html += "</textarea>";
        if (pulse.Title != null && pulse.Title.length > 0) {
            html += "                   <label class=\"pulseCardQuestionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">" + (TEXT_QUESTION_MAX_COUNT - pulse.Title.length) + "</label>";
        }
        else {
            html += "                   <label class=\"pulseCardQuestionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">" + TEXT_QUESTION_MAX_COUNT + "</label>";
        }

        //***** Title *****/
        html += "               </div>";
        html += "               <div style=\"position: relative;\">";
        html += "                   <textarea IsDisabled class=\"pulseCardDescription\" style=\"margin: 5px; border-radius: 0.5em; border: 1px solid rgb(204, 204, 204); border-image: none; width: 100%; height: 100px; color: black; font-size: 14pt; display: block; position: relative; min-height: 100px; background-color: white; resize: none; overflow-wrap: break-word;\" placeholder=\"Description\" rows=\"3\" onkeyup=\"setPulse('" + pulse.PulseId + "', 2, this);\">";
        /***** Description *****/
        if (pulse.Description != null && pulse.Description.length > 0) {
            html += pulse.Description;
        }

        html += "</textarea>";
        if (pulse.Description != null && pulse.Description.length > 0) {
            html += "                   <label class=\"pulseCardDescriptionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">" + (TEXT_DESCRIPTION_MAX_COUNT - pulse.Description.length) + "</label>";
        }
        else {
            html += "                   <label class=\"pulseCardDescriptionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">" + TEXT_DESCRIPTION_MAX_COUNT + "</label>";
        }
        //***** Description *****/
        html += "               </div>";
        html += "               </div>";
        html += "           </div>";
        html += "           <hr style=\"margin: 0px;\" />";
        html += "           <div class=\"grid__inner\" style=\"background-color: transparent;\">";
        html += "               <div class=\"option-list\" style=\"margin: 0px auto; width: 90%; text-align: left;\">";

        /***** Options *****/
        var optionDefalutHtml = "";
        for (var i = 0; i < pulse.NumberOfOptions; i++) {
            var optionDefalutHtml = "";
            optionDefalutHtml += "<div style=\"position: relative; width: 48%; display: inline-block; margin: 5px; \">";
            optionDefalutHtml += "<input IsDisabled class=\"pulse-option option" + (i + 1) + "\" style=\"padding: 10px; height: 50px; border-radius: 0.5em; width: 100%;\" type=\"text\" placeholder onkeyup value >"; // default html
            optionDefalutHtml += "<label class=\"pulseOption" + (i + 1) + "Counter\" style=\"width: auto; right: 17px; bottom: 3px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">optionTextCounter</label>";
            optionDefalutHtml += "</div>";
            optionDefalutHtml = optionDefalutHtml.replace("placeholder", "placeholder=\"Option " + (i + 1) + "\""); // for placeholder
            optionDefalutHtml = optionDefalutHtml.replace("onkeyup", "onkeyup=\"setPulse('" + pulse.PulseId + "', 3, this, " + (i + 1) + "); updateCardLogicOptionUI(this, '" + pulse.PulseId + "',  " + (i + 1) + ");\""); // for onkeyup

            if (i == 0) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option1Content.length);

                if (pulse.Option1Content != null && pulse.Option1Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option1Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else if (i == 1) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option2Content.length);

                if (pulse.Option2Content != null && pulse.Option2Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option2Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else if (i == 2) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option3Content.length);

                if (pulse.Option3Content != null && pulse.Option3Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option3Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else if (i == 3) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option4Content.length);

                if (pulse.Option4Content != null && pulse.Option4Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option4Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else if (i == 4) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option5Content.length);

                if (pulse.Option5Content != null && pulse.Option5Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option5Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else if (i == 5) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option6Content.length);

                if (pulse.Option6Content != null && pulse.Option6Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option6Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else if (i == 6) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option7Content.length);

                if (pulse.Option7Content != null && pulse.Option7Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option7Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else if (i == 7) {
                optionDefalutHtml = optionDefalutHtml.replace("optionTextCounter", TEXT_OPTION_MAX_COUNT - pulse.Option8Content.length);

                if (pulse.Option8Content != null && pulse.Option8Content.length > 0) {
                    html += optionDefalutHtml.replace("value", "value=\"" + pulse.Option8Content + "\"");
                }
                else {
                    html += optionDefalutHtml.replace("value", "");
                }
            }
            else {
                // do nothing
            }
        }
        //***** Options *****/
        html += "               </div>";
        html += "           </div>";
        html += "       </div>";
        return html;
    }
    else if (pulse.CardType == 2)// HTML markup for number range
    {
        html += "       <div class=\"grid__inner pulse-card number-pulse " + pulseLevel + "\" style=\"background-position: center; margin: 0px auto; border-radius: 0.5em; border: 2px solid rgb(204, 204, 204); border-image: none; width: 100%; min-height: 285px; background-image: none; background-repeat: no-repeat; background-color: transparent;\" data-pulselevel=\"" + level + "\">";
        html += "           <div class=\"grid__inner pulse-option-selection-panel\" style=\"margin: 4px; background-color: transparent;\">";
        html += "               <div class=\"grid__span--12 grid__span--last\" style=\"margin: 5px;\">";
        html += "                   <strong>Range</strong>";
        html += "                   <div class=\"mdl-selectfield\" style=\"width: 150px; margin-left: 10px; display: inline-block;\">";
        html += "                      <select IsDisabled class=\"select-range\" style=\"display:inline-block; margin:0; width:100%; \" onchange=\"updateRange(this, '" + pulse.PulseId + "');\">";
        /***** Range type *****/
        if (pulse.RangeType == 1) {
            html += "                           <option value=\"1\" selected=\"selected\">1 to 3</option>";
            html += "                           <option value=\"2\">1 to 5</option>";
            html += "                           <option value=\"3\">1 to 7</option>";
            html += "                           <option value=\"4\">1 to 10</option>";
            html += "                           <option value=\"5\">0 to 100%</option>";
        }
        else if (pulse.RangeType == 2) {
            html += "                           <option value=\"1\">1 to 3</option>";
            html += "                           <option value=\"2\" selected=\"selected\">1 to 5</option>";
            html += "                           <option value=\"3\">1 to 7</option>";
            html += "                           <option value=\"4\">1 to 10</option>";
            html += "                           <option value=\"5\">0 to 100%</option>";
        } else if (pulse.RangeType == 3) {
            html += "                           <option value=\"1\">1 to 3</option>";
            html += "                           <option value=\"2\">1 to 5</option>";
            html += "                           <option value=\"3\" selected=\"selected\">1 to 7</option>";
            html += "                           <option value=\"4\">1 to 10</option>";
            html += "                           <option value=\"5\">0 to 100%</option>";
        } else if (pulse.RangeType == 4) {
            html += "                           <option value=\"1\">1 to 3</option>";
            html += "                           <option value=\"2\">1 to 5</option>";
            html += "                           <option value=\"3\">1 to 7</option>";
            html += "                           <option value=\"4\" selected=\"selected\">1 to 10</option>";
            html += "                           <option value=\"5\">0 to 100%</option>";
        } else if (pulse.RangeType == 5) {
            html += "                           <option value=\"1\">1 to 3</option>";
            html += "                           <option value=\"2\">1 to 5</option>";
            html += "                           <option value=\"3\">1 to 7</option>";
            html += "                           <option value=\"4\">1 to 10</option>";
            html += "                           <option value=\"5\" selected=\"selected\">0 to 100%</option>";
        } else {
            html += "                           <option value=\"1\" selected=\"selected\">1 to 3</option>";
            html += "                           <option value=\"2\">1 to 5</option>";
            html += "                           <option value=\"3\">1 to 7</option>";
            html += "                           <option value=\"4\">1 to 10</option>";
            html += "                           <option value=\"5\">0 to 100%</option>";
        }
        //***** Range type *****/
        html += "                       </select>";
        html += "                   </div>";
        html += "               </div>";
        html += "           </div>";
        html += "           <div class=\"grid__inner\" style=\"background-color: transparent;\">";
        /***** Image *****/
        if (pulse.ImageUrl != null && pulse.ImageUrl.length > 0) {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 30%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: url('" + pulse.ImageUrl + "'); background-size: cover;\">";
            if (isEnabled) {
                html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6);\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
                html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
                html += "                       </div>";
            }
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute; display: none;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        else if (pulse.UseCustomImage) {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 30%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: url('" + pulse.CustomImageDataUrl.base64 + "'); background-size: cover;\">";
            html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6);\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
            html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                       </div>";
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute; display: none;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        else {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 15%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: none; background-size: cover;\">";
            html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6); display: none;\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
            html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                       </div>";
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        /***** Image *****/
        html += "                   </div>";
        html += "               </div>";
        html += "               <div style=\"width: 65%; vertical-align: middle; display: inline-block;\">";
        html += "               <div style=\"position: relative;\">";
        html += "                   <textarea IsDisabled class=\"pulseCardQuestion\" style=\"margin: 5px; border-radius: 0.5em; border: 1px solid rgb(204, 204, 204); border-image: none; width: 100%; height: 150px; color: black; font-size: 14pt; display: block; position: relative; min-height: 150px; background-color: white; resize: none; overflow-wrap: break-word;\" placeholder=\"Question\" rows=\"3\" onkeyup=\"setPulse('" + pulse.PulseId + "', 1, this);\">";
        /***** Title *****/
        if (pulse.Title != null && pulse.Title.length > 0) {
            html += pulse.Title;
        }
        html += "</textarea>";
        html += "                   <label class=\"pulseCardQuestionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10;\">" + (TEXT_QUESTION_MAX_COUNT - pulse.Title.length) + "</label>";
        //***** Title *****/
        html += "               </div>";
        html += "               <div style=\"position: relative;\">";
        html += "                   <textarea IsDisabled class=\"pulseCardDescription\" style=\"margin: 5px; border-radius: 0.5em; border: 1px solid rgb(204, 204, 204); border-image: none; width: 100%; height: 100px; color: black; font-size: 14pt; display: block; position: relative; min-height: 100px; background-color: white; resize: none; overflow-wrap: break-word;\" placeholder=\"Description\" rows=\"3\" onkeyup=\"setPulse('" + pulse.PulseId + "', 2, this);\">";
        /***** Description *****/
        if (pulse.Description != null && pulse.Description.length > 0) {
            html += pulse.Description;
        }
        //***** Description *****/
        html += "</textarea>";
        html += "                   <label class=\"pulseCardDescriptionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">" + (TEXT_DESCRIPTION_MAX_COUNT - pulse.Description.length) + "</label>";
        html += "               </div>";
        html += "               </div>";
        html += "           </div>";
        html += "           <div class=\"grid__inner\" style=\"background-color: transparent;\">";
        /***** Minimum value of range *****/
        if (pulse.MinRangeLabel != null && pulse.MinRangeLabel.length > 0) {
            html += "                   <input IsDisabled type=\"text\" placeholder=\"Minimum value label\" class=\"range-pulse minimum-value-label\" style=\"margin: 10px; border-radius: 0.5em; width: 30%; float: left; display: inline-block; padding: 10px;\" onkeyup=\"setPulse('" + pulse.PulseId + "', 4, this)\" maxlength=\"15\" value=\"" + pulse.MinRangeLabel + "\" />";
        }
        else {
            html += "                   <input IsDisabled type=\"text\" placeholder=\"Minimum value label\" class=\"range-pulse minimum-value-label\" style=\"margin: 10px; border-radius: 0.5em; width: 30%; float: left; display: inline-block; padding: 10px;\" onkeyup=\"setPulse('" + pulse.PulseId + "', 4, this)\" maxlength=\"15\" />";
        }
        //***** Minimum value of range *****/

        /***** Maximum value of range *****/
        if (pulse.MaxRangeLabel != null && pulse.MaxRangeLabel.length > 0) {
            html += "                   <input IsDisabled type=\"text\" placeholder=\"Maximum value label\" class=\"range-pulse maximum-value-label\" style=\"margin: 10px; border-radius: 0.5em; width: 30%; float: right; display: inline-block; padding: 10px;\" onkeyup=\"setPulse('" + pulse.PulseId + "', 5, this)\" maxlength=\"15\" value=\"" + pulse.MaxRangeLabel + "\" />";
        }
        else {
            html += "                   <input IsDisabled type=\"text\" placeholder=\"Maximum value label\" class=\"range-pulse maximum-value-label\" style=\"margin: 10px; border-radius: 0.5em; width: 30%; float: right; display: inline-block; padding: 10px;\" onkeyup=\"setPulse('" + pulse.PulseId + "', 5, this)\" maxlength=\"15\" />";
        }
        //***** Maximum value of range *****/
        html += "           </div>";
        html += "           <hr style=\"margin: 0 0\" />";
        html += "           <div class=\"grid__inner range-ruler\" style=\"background-color:transparent;\" style=\"padding: 20px 0px;\">";

        /***** ruler *****/
        var count, width;
        switch (pulse.RangeType) {
            case 1: // 1 -3
                count = 3;
                break;
            case 2: // 1 - 5
                count = 5;
                break;
            case 3:// 1 - 7
                count = 7;
                break;
            case 4: // 1 - 10
                count = 10;
                break;
            case 5:// 0 - 100%
                count = 11;
                break;
            default:
                // do nothing
                break;
        }

        if (pulse.RangeType == 5) // 0 - 100%
        {
            width = (100 / count) - 0.5;

            for (var i = 0; i < count; i++) {
                if (i == 0) {
                    html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>0</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
                }
                else if (i == count - 1) {
                    html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>" + i + "0%</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
                }
                else {
                    html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>" + i + "0</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
                }
            }
        }
        else {
            width = 100 / count;
            for (var i = 0; i < count; i++) {
                html += "<div style=\"width: " + width + "%; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; float: left; display: inline-block; font-weight: 900; position: relative; height: 70px;\"><span>" + (i + 1) + "</span><div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div></div>";
            }
        }
        //***** ruler *****/

        html += "           </div>";
        html += "       </div>";
    }
    else if (pulse.CardType == 3) // HTML markup for text
    {
        html += "       <div class=\"grid__inner pulse-card text-pulse " + pulseLevel + "\" style=\"background-position: center; margin: 0px auto; border-radius: 0.5em; border: 2px solid rgb(204, 204, 204); border-image: none; width: 100%; min-height: 285px; background-image: none; background-repeat: no-repeat; background-color: transparent;\" data-pulselevel=\"" + level + "\">";
        html += "           <div class=\"grid__inner\" style=\"background-color:transparent;\">";
        /***** Image *****/
        if (pulse.ImageUrl != null && pulse.ImageUrl.length > 0) {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 30%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: url('" + pulse.ImageUrl + "'); background-size: cover;\">";
            if (isEnabled) {
                html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6);\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
                html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
                html += "                       </div>";
            }
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute; display: none;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        else if (pulse.UseCustomImage) {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 30%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: url('" + pulse.CustomImageDataUrl.base64 + "'); background-size: cover;\">";
            html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6);\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
            html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                       </div>";
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute; display: none;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        else {
            html += "               <div class=\"pulse-perview-image\" style=\"width: 15%; height: 100%; text-align: center; vertical-align: middle; display: inline-block;\">";
            html += "                   <div class=\"card-image\" style=\"background-position: center; border-radius: 1em; width: 100px; height: 100px; display: inline-block; background-image: none; background-size: cover;\">";
            html += "                       <div class=\"remove-image-icon\" style=\"margin: 3px 3px 0px 0px; border-radius: 2em; width: 20px; height: 20px; text-align: center; color: white; float: right; background-color: rgba(0, 0, 0, 0.6); display: none;\" onclick=\"removeAttachedImg(this, '" + pulse.PulseId + "');\">";
            html += "                           <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                       </div>";
            html += "                       <label class=\"upload-option-image\" style=\"left: 50px; top: 300px; position: absolute;\">";
            html += "                            <input IsDisabled title=\"Add image\" class=\"pulse-image\" style=\"display: none;\" onchange=\"displayPulsePreviewImage(this, '" + pulse.PulseId + "');\" type=\"file\" accept=\"image/*\">";
            html += "                       </label>";
        }
        /***** Image *****/
        html += "                   </div>";
        html += "               </div>";
        html += "               <div style=\"width: 65%; vertical-align: middle; display: inline-block;\">";
        html += "               <div style=\"position: relative;\">";
        html += "                   <textarea IsDisabled class=\"pulseCardQuestion\" style=\"margin: 5px; border-radius: 0.5em; border: 1px solid rgb(204, 204, 204); border-image: none; width: 100%; height: 150px; color: black; font-size: 14pt; display: block; position: relative; min-height: 150px; background-color: white; resize: none; overflow-wrap: break-word;\" placeholder=\"Question\" rows=\"3\" onkeyup=\"setPulse('" + pulse.PulseId + "', 1, this);\">";
        /***** Title *****/
        if (pulse.Title != null && pulse.Title.length > 0) {
            html += pulse.Title;
        }
        html += "</textarea>";
        html += "                   <label class=\"pulseCardQuestionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">" + (TEXT_QUESTION_MAX_COUNT - pulse.Title.length) + "</label>";
        //***** Title *****/
        html += "               </div>";
        html += "               <div style=\"position: relative;\">";
        html += "                   <textarea IsDisabled class=\"pulseCardDescription\" style=\"margin: 5px; border-radius: 0.5em; border: 1px solid rgb(204, 204, 204); border-image: none; width: 100%; height: 100px; color: black; font-size: 14pt; display: block; position: relative; min-height: 100px; background-color: white; resize: none; overflow-wrap: break-word;\" placeholder=\"Description\" rows=\"3\" onkeyup=\"setPulse('" + pulse.PulseId + "', 2, this);\">";
        /***** Description *****/
        if (pulse.Description != null && pulse.Description.length > 0) {
            html += pulse.Description;
        }
        html += "</textarea>";
        html += "                   <label class=\"pulseCardDescriptionCounter\" style=\"width: auto; right: 17px; bottom: 0px; position: absolute; z-index: 10; color: rgba(215, 215, 215, 1);\">" + (TEXT_DESCRIPTION_MAX_COUNT - pulse.Description.length) + "</label>";
        //***** Description *****/
        html += "               </div>";
        html += "               </div>";
        html += "           </div>";
        html += "           <hr style=\"margin: 0px;\" />";
        html += "           <div class=\"grid__inner\" style=\"background-color: transparent;\">";
        html += "               <div class=\"grid__span--12 grid__span--last\">";
        html += "                   <div style=\"margin: 0px auto; width: 50%;\">";
        html += "                       <label style=\"margin: 20px; padding: 30px; border-radius: 10px; border: 2px solid rgba(203, 203, 203, 1); border-image: none; text-align: center; color: rgba(203, 203, 203, 1); font-size: 1.5em; font-weight: bolder;\">Type Answer</label>";
        html += "                   </div>";
        html += "               </div>";
        html += "           </div>";
        html += "       </div>";
    }
    else {
        // do noting
    }

    return html;
}

function displayPulsePreviewImage(input, pulseId) {
    var image = new Image();

    if (input.files && input.files[0]) {
        var t = input.files[0].type;
        var s = input.files[0].size / (1024 * 1024);
        var filename = input.files[0].name;
        var fileExtension = input.files[0].name.substr(input.files[0].name.lastIndexOf(".") + 1);

        if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
            if (s > 5) {
                toastr.error('Uploaded photo exceeded 5MB.');
            } else {
                var fileReader = new FileReader();
                fileReader.addEventListener("load", function () {
                    var image = new Image();
                    image.src = this.result;


                    // Step 0. Get pulse.
                    var pulse = getPulse(pulseId);

                    // Step 1. Update data.
                    pulse.UseCustomImage = true;
                    pulse.CustomImageDataUrl = {
                        base64: this.result,
                        extension: fileExtension,
                        width: image.width,
                        height: image.height
                    };
                    updatePulse(pulse);

                    // Step 2. Update UI.
                    var cardLayout;
                    cardLayout = $(input).parents(".card");
                    cardLayout.find(".card-image").css("backgroundImage", "url(" + this.result + ")");
                    cardLayout.find(".remove-image-icon").show(); // display mini-x
                    cardLayout.find(".upload-option-image").hide(); // hide attach image icon
                    cardLayout.find(".pulse-perview-image").css("width", "30%");

                    input.value = null;
                }, false);
                fileReader.readAsDataURL(input.files[0]);
            }
        } else {
            toastr.error('File extension is invalid.');
        }
    }
}

function removeAttachedImg(input, pulseId) {
    // Step 0. Get pulse.
    var pulse = getPulse(pulseId);

    // Step 1. Update data.
    pulse.UseCustomImage = false;
    pulse.CustomImageDataUrl = null;
    updatePulse(pulse);

    // Step 2. Update UI.
    var cardLayout;
    cardLayout = $(input).parents(".card");
    cardLayout.find(".card-image").css("backgroundImage", "none");
    cardLayout.find(".remove-image-icon").hide(); // hide mini-x
    cardLayout.find(".upload-option-image").show(); // show attach image icon
    cardLayout.find(".pulse-perview-image").css("width", "15%");
}

function updateCardLogicOptionUI(input, pulseId, optionIndex) {
    // Step 0. Get pulse.
    var pulse = getPulse(pulseId);

    // Step 1. Update UI.
    var text;
    switch (optionIndex) {
        case 1:
            text = pulse.Option1Content;
            break;
        case 2:
            text = pulse.Option2Content;
            break;
        case 3:
            text = pulse.Option3Content;
            break;
        case 4:
            text = pulse.Option4Content;
            break;
        case 5:
            text = pulse.Option5Content;
            break;
        case 6:
            text = pulse.Option6Content;
            break;
        case 7:
            text = pulse.Option7Content;
            break;
        case 8:
            text = pulse.Option8Content;
            break;

        default:

            break;
    }

    var cardLayout;
    cardLayout = $(input).parents(".card");
    cardLayout.find(".pulse-option-" + optionIndex).html(text);
    if (text.length > 0) {
        cardLayout.find(".pulse-option-" + optionIndex).css("padding", "0px 10px");
    }
    else {
        cardLayout.find(".pulse-option-" + optionIndex).css("padding", "0px 0px");
    }
}

function getCardLogicHtml(pulse) {
    var html = "", optionCount, idx, canCreate = true;

    if (pulse.CardType == 4) {
        html = "<h3>Logic</h3>";
        canCreate = pulse.LogicLevel < 3;
        for (idx = 0; idx < pulse.NumberOfOptions; idx++) {
            html += "   <div class=\"logic-answer\" style=\"background-color:transparent;\">";
            html += "       <div style=\"width: 48%; display: inline-block;\">";
            html += "           <span style=\"display: inline-block;\">When answer is</span>";
            html += "           <label class=\"pulse-option-" + (idx + 1) + "\" style=\"padding: 0px 0px; border-bottom:1px solid #ccc; display: inline-block; text-overflow: ellipsis; max-width: 170px; overflow: hidden; vertical-align: middle\">";
            if (idx == 0 && pulse.Option1Content != null && pulse.Option1Content.length > 0) {
                html += pulse.Option1Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            }
            else if (idx == 1 && pulse.Option2Content != null && pulse.Option2Content.length > 0) {
                html += pulse.Option2Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            }
            else if (idx == 2 && pulse.Option3Content != null && pulse.Option3Content.length > 0) {
                html += pulse.Option3Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            } else if (idx == 3 && pulse.Option4Content != null && pulse.Option4Content.length > 0) {
                html += pulse.Option4Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            } else if (idx == 4 && pulse.Option5Content != null && pulse.Option5Content.length > 0) {
                html += pulse.Option5Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            } else if (idx == 5 && pulse.Option6Content != null && pulse.Option6Content.length > 0) {
                html += pulse.Option6Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            } else if (idx == 6 && pulse.Option7Content != null && pulse.Option7Content.length > 0) {
                html += pulse.Option7Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            } else if (idx == 7 && pulse.Option8Content != null && pulse.Option8Content.length > 0) {
                html += pulse.Option8Content;
                html = html.replace("padding: 0px 0px", "padding: 0px 10px");
            }
            else {
                html += "Option " + (idx + 1);
            }

            html += "           </label>";
            html += "       </div>";
            html += "       <div style=\"width: 48%; text-align: right; display: inline-block;\">";
            html += "           <span style=\"display: inline-block;\">Advance to</span>";
            html += "           <div class=\"mdl-selectfield\" style=\"width: 150px; margin-left: 10px; display: inline-block; text-align: right;\">";
            html += "               <select IsDisabled class=\"advanceTo-select\" onchange=\"logicRouteUpdate(this, '" + pulse.PulseId + "', " + (idx + 1) + ");\" style=\"padding-bottom: 4px; margin-bottom: 0px; display: inline-block;\">";

            var childerCardLable = "RP" + pulse.Ordering + LABEL_MAPPING[idx];
            switch (idx + 1) {
                case 1:
                    if (pulse.Option1NextCardId != null && pulse.Option1NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option1NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }
                    break;
                case 2:
                    if (pulse.Option2NextCardId != null && pulse.Option2NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option2NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }

                    break;
                case 3:
                    if (pulse.Option3NextCardId != null && pulse.Option3NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option3NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }
                    break;
                case 4:
                    if (pulse.Option4NextCardId != null && pulse.Option4NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option4NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }
                    break;
                case 5:
                    if (pulse.Option5NextCardId != null && pulse.Option5NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option5NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }
                    break;
                case 6:
                    if (pulse.Option6NextCardId != null && pulse.Option6NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option6NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }
                    break;
                case 7:
                    if (pulse.Option7NextCardId != null && pulse.Option7NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option7NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }
                    break;
                case 8:
                    if (pulse.Option8NextCardId != null && pulse.Option8NextCardId != "PULSESETEND") // Option 有對應一張卡。(有小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\">Completion</option>";
                        html = html + "         <option class=\"advanceTo-select-option option-logic\" value=\"" + pulse.Option8NextCardId + "\" selected>" + childerCardLable + "</option>";
                    }
                    else // Option 沒有對應的卡。(無小孩的狀況)
                    {
                        html += "               <option class=\"advanceTo-select-option option-completion\" value=\"completion\" selected>Completion</option>";
                        if (canCreate) {
                            html = html + "         <option class=\"advanceTo-select-option option-create\" value=\"create\">Create a destination pulse, " + childerCardLable + "</option>";
                        }
                    }
                    break;
                default:
                    // do nothing
                    break;
            }
            html += "               </select>";
            html += "           </div>";
            html += "       </div>";
            html += "   </div>";
        }
    }

    return html;
}

function logicRouteUpdate(dropdown, pulseId, optionNo) {
    //debugger;
    var pulse = getPulse(pulseId);

    //switch (optionNo) {
    //    case 1:
    //        if (pulse.Option1NextCardId != null && pulse.Option1NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    case 2:
    //        if (pulse.Option2NextCardId != null && pulse.Option2NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    case 3:
    //        if (pulse.Option3NextCardId != null && pulse.Option3NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    case 4:
    //        if (pulse.Option4NextCardId != null && pulse.Option4NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    case 5:
    //        if (pulse.Option5NextCardId != null && pulse.Option5NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    case 6:
    //        if (pulse.Option6NextCardId != null && pulse.Option6NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    case 7:
    //        if (pulse.Option7NextCardId != null && pulse.Option7NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    case 8:
    //        if (pulse.Option8NextCardId != null && pulse.Option8NextCardId != "") {
    //            return;
    //        }
    //        break;
    //    default:
    //        break;

    //}

    if (pulseId.indexOf('undefined') == 0) {
        if (pulse.ParentCard == null)  // parent is "add a card" mode.
        {
            if (!isPulseValid(pulse)) {
                dropdown.options[0].selected = true;
                return;
            }

            savePulse(pulseId, true, optionNo);
        }
        else  // parent is "Edit" mode. and add a logic-level 3 child.
        {
            if (!isPulseValid(pulse)) {
                dropdown.options[0].selected = true;
                return;
            }

            savePulse(pulseId, true, optionNo);
        }
    }
    else // Edit pulse mode. Parent exist.
    {
        if (dropdown.value != "completion") {
            // Step 1. 將暫時的小孩Id加入。暫時的子ID為："undefined_{parentId}_{optionNo}"
            switch (optionNo) {
                case 1:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option1NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option1NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                case 2:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option2NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option2NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                case 3:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option3NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option3NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                case 4:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option4NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option4NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                case 5:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option5NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option5NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                case 6:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option6NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option6NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                case 7:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option7NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option7NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                case 8:
                    if (dropdown.value.indexOf('PD') == 0 || dropdown.value.indexOf('undefined') == 0) {
                        pulse.Option8NextCardId = dropdown.value;
                        updatePulse(pulse);
                        return;
                    }
                    pulse.Option8NextCardId = "undefined_" + pulseId + "_" + optionNo;
                    break;
                default:
                    // do nothing
                    break;
            }

            // Step 2. 將 parent 的 full card 轉成 mini card
            switchToMiniCard(pulseId);

            // Step 3. 將 parent 的 mini card 伸展到寬度 100%
            $(".card.mini-card." + pulseId).parent().parent().find(".card-date").css('display', 'none');
            $(".card.mini-card." + pulseId).parent().parent().find('.card-date').css('position', 'absolute');
            $(".card.mini-card." + pulseId).parent().parent().find('.mini-card-holder').css('width', '100%');

            // Step 4. 產生新的小孩 pulse，並加入至 pulse array 中。
            var childPulse = new Pulse($("#main_content_hfDeckId").val());
            if (pulse.LogicLevel == 0 || pulse.LogicLevel == 1) {
                childPulse.LogicLevel = 2;
            }
            else {
                childPulse.LogicLevel = 3;
            }

            childPulse.PulseId = "undefined_" + pulseId + "_" + optionNo;
            childPulse.Lable = "RP" + pulse.Ordering + LABEL_MAPPING[optionNo - 1];
            childPulse.Ordering = pulse.Ordering + LABEL_MAPPING[optionNo - 1];
            childPulse.StartDate = pulse.StartDate;
            childPulse.ParentCard = pulse;
            childPulse.ParentOptionIndex = optionNo;
            model.deck.Pulses.unshift(childPulse);

            // Step 5. 更新 UI
            var youngerBrotherPulseId = "";
            var optionCount = 8;
            for (var i = 1 + optionNo; i < 9; i++) {
                if (i == 2 && pulse.Option2NextCardId != "PULSESETEND") {
                    youngerBrotherPulseId = pulse.Option2NextCardId;
                    break;
                }
                else if (i == 3 && pulse.Option3NextCardId != "PULSESETEND") {
                    youngerBrotherPulseId = pulse.Option3NextCardId;
                    break;
                }

                else if (i == 4 && pulse.Option4NextCardId != "PULSESETEND") {
                    youngerBrotherPulseId = pulse.Option4NextCardId;
                    break;
                }
                else if (i == 5 && pulse.Option5NextCardId != "PULSESETEND") {
                    youngerBrotherPulseId = pulse.Option5NextCardId;
                    break;
                }
                else if (i == 6 && pulse.Option6NextCardId != "PULSESETEND") {
                    youngerBrotherPulseId = pulse.Option6NextCardId;
                    break;
                }
                else if (i == 7 && pulse.Option7NextCardId != "PULSESETEND") {
                    youngerBrotherPulseId = pulse.Option7NextCardId;
                    break;
                }
                else if (i == 8 && pulse.Option8NextCardId != "PULSESETEND") {
                    youngerBrotherPulseId = pulse.Option8NextCardId;
                    break;
                }
            }

            if (youngerBrotherPulseId == "") {

                var olderBrotherPulseId = "";
                for (var i = 1 ; i < optionNo  ; i++) {
                    if (i == 1 && pulse.Option1NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option1NextCardId;
                    }
                    else if (i == 2 && pulse.Option2NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option2NextCardId;
                    }
                    else if (i == 3 && pulse.Option3NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option3NextCardId;
                    }
                    else if (i == 4 && pulse.Option4NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option4NextCardId;
                    }
                    else if (i == 5 && pulse.Option5NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option5NextCardId;
                    }
                    else if (i == 6 && pulse.Option6NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option6NextCardId;
                    }
                    else if (i == 7 && pulse.Option7NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option7NextCardId;
                    }
                    else if (i == 8 && pulse.Option8NextCardId != "PULSESETEND") {
                        olderBrotherPulseId = pulse.Option8NextCardId;
                    }
                }

                if (olderBrotherPulseId != "") {
                    $(".card." + olderBrotherPulseId).after(getFullCardHtml(childPulse));
                }
                else {
                    $(".card." + pulse.PulseId).parent().append(getFullCardHtml(childPulse));
                }
            }
            else {
                $(".card." + youngerBrotherPulseId).before(getFullCardHtml(childPulse));
            }
        }
        else // Completion
        {
            // Step 1. Update pulse first.
            switch (optionNo) {
                case 1:
                    pulse.Option1NextCardId = null;
                    break;

                case 2:
                    pulse.Option2NextCardId = null;
                    break;

                case 3:
                    pulse.Option3NextCardId = null;
                    break;

                case 4:
                    pulse.Option4NextCardId = null;
                    break;

                case 5:
                    pulse.Option5NextCardId = null;
                    break;

                case 6:
                    pulse.Option6NextCardId = null;
                    break;

                case 7:
                    pulse.Option7NextCardId = null;
                    break;

                case 8:
                    pulse.Option8NextCardId = null;
                    break;

                default:
                    // do nothing
                    break;
            }

            updatePulse(pulse);
        }
    }
}


function isPulseValid(pulse) {
    // 重新計算 Max Count
    if ((pulse.ImageUrl != null && pulse.ImageUrl != "") || (pulse.CustomImageDataUrl != null && pulse.CustomImageDataUrl != "")) // has image
    {
        if (pulse.NumberOfOptions == 2 || pulse.NumberOfOptions == 3) {
            TEXT_QUESTION_MAX_COUNT = 76;
            TEXT_DESCRIPTION_MAX_COUNT = 84;
        }
        else if (pulse.NumberOfOptions == 4) {
            TEXT_QUESTION_MAX_COUNT = 57;
            TEXT_DESCRIPTION_MAX_COUNT = 63;
        }
        else {
            TEXT_QUESTION_MAX_COUNT = 76;
            TEXT_DESCRIPTION_MAX_COUNT = 84;
        }
    }
    else // without image
    {
        if (pulse.NumberOfOptions == 2 || pulse.NumberOfOptions == 3) {
            TEXT_QUESTION_MAX_COUNT = 120;
            TEXT_DESCRIPTION_MAX_COUNT = 136;
        }
        else if (pulse.NumberOfOptions == 4) {
            TEXT_QUESTION_MAX_COUNT = 90;
            TEXT_DESCRIPTION_MAX_COUNT = 102;
        }
        else {
            TEXT_QUESTION_MAX_COUNT = 120;
            TEXT_DESCRIPTION_MAX_COUNT = 136;
        }
    }

    if (pulse.NumberOfOptions == 2) {
        TEXT_OPTION_MAX_COUNT = 15;
    }
    else if (pulse.NumberOfOptions == 3) {
        TEXT_OPTION_MAX_COUNT = 9;
    }
    else if (pulse.NumberOfOptions == 4) {
        TEXT_OPTION_MAX_COUNT = 15;
    }
    else {
        TEXT_OPTION_MAX_COUNT = 8;
    }

    // Question
    if (pulse.Title.length == 0) {
        ShowToast('Please enter your question', 2);
        return false;
    }
    if (pulse.Title.length > TEXT_QUESTION_MAX_COUNT) {
        // Question should not exceed 150 letters.
        ShowToast('Question should not exceed ' + TEXT_QUESTION_MAX_COUNT + ' letters', 2);
        return false;
    }

    // Description
    // description is not compulsory. 2017/02/10
    //if (pulse.Description.length == 0) {
    //    ShowToast('Please enter your description', 2);
    //    return false;
    //}

    if (pulse.Description.length > TEXT_DESCRIPTION_MAX_COUNT) {
        ShowToast('Description should not exceed ' + TEXT_DESCRIPTION_MAX_COUNT + ' letters', 2);
        return false;
    }

    // StartDateTime // Only check when publish method is Routine
    if (model.deck.publishMethodType == 2) // publish method is Routine 
    {
        // 只需要檢查第一層的時間。第二與第三層的時間，是相依於第一層
        if (pulse.LogicLevel == 0 || pulse.LogicLevel == 1) {
            if (!isDateTimeValid(moment(pulse.StartDate).format("DD MMM YYYY, dddd hh:mm A"), 1)) {
                return false;
            }
        }
    }

    // Options, MinRangeLabel, MaxRangeLabel.
    switch (pulse.CardType) {
        case 1: // Select One
            // Options
            for (var i = 1; i < (pulse.NumberOfOptions + 1) ; i++) {
                if (i == 1) {
                    if (pulse.Option1Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option1Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 2) {
                    if (pulse.Option2Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option2Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 3) {
                    if (pulse.Option3Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option3Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 4) {
                    if (pulse.Option4Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option4Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 5) {
                    if (pulse.Option5Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option5Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 6) {
                    if (pulse.Option6Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option6Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                } else if (i == 7) {
                    if (pulse.Option7Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option7Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                } else if (i == 8) {
                    if (pulse.Option8Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option8Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else {
                    // do nothing
                }
            }

            break;
        case 2: // Number Range
            // MinRangeLabel
            if (pulse.MinRangeLabel.length == 0) {
                ShowToast('Please label your minimum value', 2);
                return false;
            }
            if (pulse.MinRangeLabel.length > TEXT_RANGE_LABEL_MAX_COUNT) {
                ShowToast('Minimum value label should not exceed ' + TEXT_RANGE_LABEL_MAX_COUNT + ' letters', 2);
                return false;
            }

            // MaxRangeLabel
            if (pulse.MaxRangeLabel.length == 0) {
                ShowToast('Please label your maximum value', 2);
                return false;
            }
            if (pulse.MaxRangeLabel.length > TEXT_RANGE_LABEL_MAX_COUNT) {
                ShowToast('Maximum value label should not exceed ' + TEXT_RANGE_LABEL_MAX_COUNT + ' letters', 2);
                return false;
            }
            break;
        case 3: // Text
            // do nothing
            break;
        case 4: // Logic
            // Options
            for (var i = 1; i < (pulse.NumberOfOptions + 1) ; i++) {
                if (i == 1) {
                    if (pulse.Option1Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option1Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 2) {
                    if (pulse.Option2Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option2Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 3) {
                    if (pulse.Option3Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option3Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 4) {
                    if (pulse.Option4Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option4Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 5) {
                    if (pulse.Option5Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option5Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else if (i == 6) {
                    if (pulse.Option6Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option6Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                } else if (i == 7) {
                    if (pulse.Option7Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option7Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                } else if (i == 8) {
                    if (pulse.Option8Content.length == 0) {
                        ShowToast('Please fill in your Option ' + i, 2);
                        return false;
                    }
                    if (pulse.Option8Content.length > TEXT_OPTION_MAX_COUNT) {
                        ShowToast('Option should not exceed ' + TEXT_OPTION_MAX_COUNT + ' letters', 2);
                        return false;
                    }
                }
                else {
                    // do nothing
                }
            }
            break;
        default:
            break;

    }
    return true;
}


function savePulse(pulseId, isForLogic, optionNo) {
    // Step 1. Get pulse.
    var pulse = getPulse(pulseId);
    if (pulse == null || pulse == undefined) {
        console.error('The pulse is not exist. pulseId: ' + pulseId);
        RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
        return;
    }

    // Step 2. Check input data.
    // set Pulse.Options
    pulse.Options = new Array();
    for (var i = 1; i < (pulse.NumberOfOptions + 1) ; i++) {
        var option = new Option();
        switch (i) {
            case 1:
                option.Content = pulse.Option1Content;
                option.NextPulseId = pulse.Option1NextCardId;
                break;
            case 2:
                option.Content = pulse.Option2Content;
                option.NextPulseId = pulse.Option2NextCardId;
                break;
            case 3:
                option.Content = pulse.Option3Content;
                option.NextPulseId = pulse.Option3NextCardId;
                break;
            case 4:
                option.Content = pulse.Option4Content;
                option.NextPulseId = pulse.Option4NextCardId;
                break;
            case 5:
                option.Content = pulse.Option5Content;
                option.NextPulseId = pulse.Option5NextCardId;
                break;
            case 6:
                option.Content = pulse.Option6Content;
                option.NextPulseId = pulse.Option6NextCardId;
                break;
            case 7:
                option.Content = pulse.Option7Content;
                option.NextPulseId = pulse.Option7NextCardId;
                break;
            case 8:
                option.Content = pulse.Option8Content;
                option.NextPulseId = pulse.Option8NextCardId;
                break;
            default:
                // do nothing
                break;
        }
        pulse.Options.push(option);
    }

    if (!isPulseValid(pulse)) {
        return;
    }

    // Step 3. Call API.(/Api/PulseResponsiveCard)
    var ajaxJson = convertPulseToAjaxJson(pulse);
    var HTTP_METHOD, ajaxPromise;

    if ((pulse) && (!pulse.PulseId.startsWith("undefined"))) {
        HTTP_METHOD = "POST";
        ajaxJson.action = 1;
    } else {
        ajaxJson.cardId = "";
        HTTP_METHOD = "PUT";
    }

    ajaxPromise = jQuery.ajax({
        type: HTTP_METHOD,
        url: "/api/PulseResponsiveCard",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(ajaxJson),
        dataType: "json",
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            opSuccessful = true;
            if (d.Success) {
                if (HTTP_METHOD == "PUT") {
                    if (isForLogic) // For logic card.
                    {
                        // Step 1. Update pulse data. 將暫時的小孩Id加入。暫時的子ID為："undefined_{parentId}_{optionNo}"
                        removePulse(pulseId);
                        switch (optionNo) {
                            case 1:
                                d.Pulse.Option1NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            case 2:
                                d.Pulse.Option2NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            case 3:
                                d.Pulse.Option3NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            case 4:
                                d.Pulse.Option4NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            case 5:
                                d.Pulse.Option5NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            case 6:
                                d.Pulse.Option6NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            case 7:
                                d.Pulse.Option7NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            case 8:
                                d.Pulse.Option8NextCardId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                                break;
                            default:
                                // do nothing
                                break;

                        }

                        // Step 2. Update Pulses data.
                        var parentPulse;
                        if (d.Pulse.ParentCard != null) // level 2
                        {
                            parentPulse = getPulse(d.Pulse.ParentCard.PulseId);
                            var parentIndex = -1;
                            for (var i = 0; i < model.deck.Pulses.length; i++) {
                                if (model.deck.Pulses[i].PulseId == d.Pulse.ParentCard.PulseId) {
                                    parentIndex = i;
                                    break;
                                }
                            }

                            var brotherIndex = -1;
                            for (var i = 0; i < model.deck.Pulses.length; i++) {
                                //d.Pulse.ParentCard.FloatOrdering  = 6.5
                                if (parseInt(model.deck.Pulses[i].FloatOrdering) == parseInt(d.Pulse.ParentCard.FloatOrdering)) {
                                    if (model.deck.Pulses[i].FloatOrdering < d.Pulse.FloatOrdering) {
                                        brotherIndex = i;
                                    }
                                }
                            }

                            if (brotherIndex > 0) {
                                model.deck.Pulses.splice((brotherIndex + 1), 0, d.Pulse);
                            }
                            else {
                                model.deck.Pulses.splice((parentIndex + 1), 0, d.Pulse);
                            }

                            switch (pulse.ParentOptionIndex) {
                                case 1:
                                    parentPulse.Option1NextCardId = d.Pulse.PulseId;
                                    break;
                                case 2:
                                    parentPulse.Option2NextCardId = d.Pulse.PulseId;
                                    break;
                                case 3:
                                    parentPulse.Option3NextCardId = d.Pulse.PulseId;
                                    break;
                                case 4:
                                    parentPulse.Option4NextCardId = d.Pulse.PulseId;
                                    break;
                                case 5:
                                    parentPulse.Option5NextCardId = d.Pulse.PulseId;
                                    break;
                                case 6:
                                    parentPulse.Option6NextCardId = d.Pulse.PulseId;
                                    break;
                                case 7:
                                    parentPulse.Option7NextCardId = d.Pulse.PulseId;
                                    break;
                                case 8:
                                    parentPulse.Option8NextCardId = d.Pulse.PulseId;
                                    break;
                                default:
                                    // do nothing
                                    break;
                            }
                        }
                        else // level 1
                        {
                            model.deck.Pulses.unshift(d.Pulse);
                        }

                        // Step 3. Update PulseList.
                        displayPulseList(model.deck.Pulses);
                        $(".card.card-level-1 .cascade-card").click();

                        // 產生新的小孩 pulse，並加入至 pulse array 中。
                        var childPulse = new Pulse($("#main_content_hfDeckId").val());
                        if (d.Pulse.LogicLevel == 0 || d.Pulse.LogicLevel == 1) {
                            childPulse.LogicLevel = 2;
                        }
                        else {
                            childPulse.LogicLevel = 3;
                        }
                        childPulse.PulseId = "undefined_" + d.Pulse.PulseId + "_" + optionNo;
                        childPulse.Lable = "RP" + d.Pulse.Ordering + LABEL_MAPPING[optionNo - 1];
                        childPulse.Ordering = d.Pulse.Ordering + LABEL_MAPPING[optionNo - 1];
                        childPulse.StartDate = d.Pulse.StartDate;
                        childPulse.ParentCard = d.Pulse;
                        childPulse.ParentOptionIndex = optionNo;
                        model.deck.Pulses.unshift(childPulse);

                        // 將 parent 的 mini card 伸展到寬度 100%
                        $(".card.mini-card." + d.Pulse.PulseId).parent().parent().find(".card-date").css('display', 'none');
                        $(".card.mini-card." + d.Pulse.PulseId).parent().parent().find('.card-date').css('position', 'absolute');
                        $(".card.mini-card." + d.Pulse.PulseId).parent().parent().find('.mini-card-holder').css('width', '100%');

                        if (d.Pulse.ParentCard != null) // level 2
                        {
                            $("." + d.Pulse.ParentCard.PulseId + ".card.card-level-1 .cascade-card").click();
                            // 更新 UI
                            $(".card.mini-card." + d.Pulse.PulseId).after(getFullCardHtml(childPulse));
                        }
                        else // Level 1
                        {
                            //  Clear card-add div content and show "add a card" button.
                            $(".card-add").html("");
                            $("button.add-pulse").show();
                            // 更新 UI
                            $(".card.mini-card." + d.Pulse.PulseId).parent().parent().find('.mini-card-holder').append(getFullCardHtml(childPulse));

                        }
                    }
                    else // 一般的 Save card 模式。
                    {
                        if (pulse.ParentCard == null) // 非子卡
                        {
                            // Step 1. Update pulse data.
                            removePulse(pulseId);
                            model.deck.Pulses.unshift(d.Pulse);

                            // Step 2. Clear card-add div content and show "add a card" button.
                            $(".card-add").html("");
                            $("button.add-pulse").show();
                            displayPulseList(model.deck.Pulses);
                            $(".card.card-level-1 .cascade-card").click();
                            ShowToast("Pulse added", 1);
                        }
                        else // 子卡
                        {
                            ShowToast("Pulse added", 1);
                            RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
                        }
                    }
                } else {
                    ShowToast("Pulse updated", 1);
                    RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
                }
            } else {
                toastr.error(d.ErrorMessage);
            }
        },
        error: function (xhr, status, error) {
            toastr.error(error);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function convertPulseToAjaxJson(pulse) {
    var ajaxData;

    ajaxData = {
        'cardId': pulse.PulseId,
        'deckId': pulse.DeckId,
        'adminUserId': $("#main_content_hfAdminUserId").val(),
        'companyId': $("#main_content_hfCompanyId").val(),
        'title': pulse.Title,
        'description': pulse.Description,
        'cardType': pulse.CardType,
        'questionType': 0,
        'startDate': pulse.StartDate,
        'numberOfOptions': pulse.NumberOfOptions,
        'rangeType': pulse.RangeType,
        'imageUrl': pulse.ImageUrl,
        'minRangeLabel': pulse.MinRangeLabel,
        'maxRangeLabel': pulse.MaxRangeLabel,
        'Options': pulse.Options,
        'useCustomImage': pulse ? pulse.UseCustomImage : false,
        'customImageDataUrl': pulse ? pulse.CustomImageDataUrl : "",
        'parentCardId': pulse.ParentCard ? pulse.ParentCard.PulseId : "",
        'parentOptionNumber': pulse.ParentOptionIndex,
        'action': 1
    };

    return ajaxData;
}

function displayPulseList(pulses) {
    var pj, html, holderHtml, idx, record, pulse, parentPulse, cardLevel;
    $(".card-list").html("");
    for (idx = 0; idx < pulses.length; idx++) {
        pulse = pulses[idx];
        parentPulse = getParentPulse(pulse.PulseId);

        if (parentPulse) // The card has parent 
        {
            pulse.ParentCard = parentPulse;
            html = getMiniCardHtml(pulse, parentPulse.PulseId);
        }
        else  // The card does not have parent
        {
            html = getMiniCardHtml(pulse);
        }

        // Find position to append card html
        if (parentPulse) {
            $(".card-list .card-holder:last .card:last").after(html);
        } else {
            if ($(".card-list .card:last").length == 0) {
                holderHtml = getHolderHtml(html, pulse);
                $(".card-list").append(holderHtml);
            } else {
                holderHtml = getHolderHtml(html, pulse);
                $(".card-list .card-holder:last").after(holderHtml);
            }
        }
    }

    $(".card-publish-date .cardStartDate").datepicker({
        dateFormat: 'dd M yy'
    });
}

function getParentPulse(pulseId) {
    var idx, optIdx, parentPulse;
    parentPulse = null;
    for (idx = 0; idx < model.deck.Pulses.length; idx++) {
        for (optIdx = 0; optIdx < model.deck.Pulses[idx].NumberOfOptions; optIdx++) {
            if (model.deck.Pulses[idx]["Option" + (optIdx + 1) + "NextCardId"] == pulseId) {
                parentPulse = model.deck.Pulses[idx];
                break;
            }
        }
    }

    return parentPulse;
}

function getMiniCardHtml(pulse, parentPulseId, hideChildrenHtml) {
    var pulseLevel = 1;

    if (pulse.LogicLevel > 1) {
        pulseLevel = pulse.LogicLevel;
    }

    var mStartDate = moment(pulse.StartDate);
    var html;
    html = "";

    var hideChildcards = "";
    if ($(".card.full-card." + pulse.PulseId + ".hide-childcards").length > 0) {
        hideChildcards = "hide-childcards";
    }

    html = html + " <div class=\"card mini-card " + pulse.PulseId + " card-level-" + pulseLevel + " " + hideChildcards + "\" data-cardId=\"" + pulse.PulseId + "\" data-parentCardId=\"" + parentPulseId + "\">";
    if (pulseLevel == 2) {
        html = html + "<div class=\"indent-indicator level1-indicator\"></div>";
    } else if (pulseLevel == 3) {
        html = html + "<div class=\"indent-indicator level2-indicator\"></div>";
    }
    html = html + "     <div class=\"grid__inner\" style=\"background-color:transparent;\">";
    html = html + "         <div class=\"grid__span--3\">";
    html = html + "             <h2 class=\"card-label\" style=\"font-size: 40px; color: #ccc;margin: 0;\">RP" + pulse.Ordering + "</h2>";
    html = html + "         </div>";
    html = html + "         <div class=\"grid__span grid__span--9 grid__span--last\" style=\"background-color:transparent;\">";
    html = html + "             <div class=\"grid__inner\" style=\"background-color:transparent;\">";
    if (pulse.ImageUrl != null && pulse.ImageUrl.length > 0) {
        html = html + "             <div style='background-position: center; border-radius: 1em; width: 60px; height: 60px; display: inline-block; background-image: url(\"" + pulse.ImageUrl + "\"); background-size: cover; float:left; margin-right:20px;'></div>";
    }
    else {
        html = html + "             <div style='background-position: center; border-radius: 1em; width: 60px; height: 60px; display: inline-block; background-size: cover; float:left; margin-right:20px;'></div>";
    }
    html = html + "                 <div class=\"grid__span--8\" style=\"width:55%\">";
    html = html + "                     <h3 style=\"color:#ccc\">" + pulse.Title + "</h3>" + pulse.Description;
    html = html + "                 </div>";
    html = html + "                 <div class=\"grid__span--1\" style=\"text-align:center;\">";
    html = html + "                     <h3 style=\"color:#ccc\">Type</h3>";
    if (pulse.CardType == 1) {
        html = html + "                     <img src=\"/Img/icon_selectone.png\" />";
    } else if (pulse.CardType == 2) {
        html = html + "                     <img src=\"/Img/icon_numberrange.png\" />";
    } else if (pulse.CardType == 3) {
        html = html + "                     <img src=\"/Img/icon_text.png\" />";
    } else if (pulse.CardType == 4) {
        html = html + "                     <img src=\"/Img/icon_selectone.png\" />";
    }
    html = html + "                </div>";
    html = html + "";
    html = html + "                <div class=\"grid__span--1\" style=\"text-align:center;\">";
    html = html + "                    <h3 style=\"color:#ccc\">Logic</h3>";

    if (pulse.HasIncomingLogic) {
        html = html + "                    <a href=\"#\"><img src=\"/Img/icon_logic_destination.png\" /></a>";
    }
    if (pulse.HasOutgoingLogic) {
        html = html + "                    <a href=\"#\"><img src=\"/Img/icon_logic_redirect.png\" /></a>";
    }

    html = html + "                </div>";
    html = html + "";
    html = html + "                <div class=\"grid__span--1\" style=\"text-align:center;\">";
    html = html + "                    <a onclick=\"window.open('/DynamicPulse/ResponsivePulseAnalytic/" + pulse.DeckId + "/" + pulse.PulseId + "');\" href=\"#\"><img src=\"/Img/icon_result.png\" /></a>";
    html = html + "                </div>";
    html = html + "";
    html = html + "                <div class=\"grid__span--1 grid__span--last\">";
    html = html + "                    <div class=\"grid-question-expandable\">";
    if ((pulseLevel == 1) && (pulse.HasOutgoingLogic)) // pulse is level 1, add toggleFakeLayer function.
    {
        html = html + "                        <button type=\"button\" class=\"cascade-card\" onclick=\"cascadeCards('" + pulse.PulseId + "'); toggleFakeLayer('" + pulse.PulseId + "');\"><i class=\"fa fa-arrow-circle-o-down fa-lg icon\"></i></button>";
    }
    html = html + "                        <button type=\"button\" class=\"to-mini-card\" onclick=\"switchToFullCard('" + pulse.PulseId + "');\"><i class=\"fa fa-caret-square-o-up fa-lg icon\"></i></button>";
    html = html + "                    </div>";
    html = html + "                </div>";
    html = html + "            </div>";
    html = html + "        </div>";
    html = html + "    </div>";
    html = html + "</div>";

    return html;
}

function getHolderHtml(cardHtml, pulse) {
    var mStartDate, startDateString, startTimeHH, startTimeMM, startTimeDay;

    if (pulse) {
        mStartDate = moment.utc(pulse.StartDate).add(timezone, 'hours');
        startDateString = mStartDate.format("DD MMM YYYY");// + " " + mStartDate.format("dddd") + ",";
        startTimeHH = mStartDate.format("hh");
        startTimeMM = mStartDate.format("mm");
        startTimeDay = mStartDate.format("dddd");
    }
    else {
        // new card
        startDateString = "";
        startTimeHH = "";
        startTimeMM = "";
    }

    holderHtml = "";
    holderHtml = holderHtml + "<div class=\"card-holder row-" + ($(".card-list .card-holder").length + 1).toString() + " " + pulse.PulseId + "\">";

    holderHtml = holderHtml + "    <div class=\"grid__span--2 card-date\" style=\"margin-right:1%; width:14%;\">";

    if (pulse.IsCustomized) {
        holderHtml = holderHtml + "    <div style=\"float:left;\">";
        holderHtml = holderHtml + "         <i class=\"fa fa-gears tooltip-icon\" style=\"font-size: 2em; display: inline-block; color:rgba(128, 128, 128, 1);\"></i>";
        holderHtml = holderHtml + "         <div class=\"customized_tip\" style=\"display: none; text-align: left;\">";
        holderHtml = holderHtml + "             <label style=\"font-weight: 700;\">Manual customized date</label>";
        holderHtml = holderHtml + "             <span>This pulse has been scheduled to a specific date</span>";
        holderHtml = holderHtml + "         </div>";
        holderHtml = holderHtml + "    </div>";
    }

    holderHtml = holderHtml + "        <span style=\"float:right; text-align:left; width:80%; \" class=\"card-publish-date\">";

    var canEditShowTime = false;
    if (model.deck.publishMethodType == 1) // Schedule. Cannot change show time of card.
    {
        canEditShowTime = false;
    }
    else // Routine. If show time of card is earlier than now(UTC), cannot change show time of card.
    {
        if (pulse == null || pulse.PulseId == null || pulse.PulseId.indexOf("undefined") !== -1 || moment.utc(pulse.StartDate).isAfter(moment.utc())) {
            canEditShowTime = true;
        }
        else {
            canEditShowTime = false;
        }
    }

    holderHtml = holderHtml + "             <div style=\"border-bottom: 1px solid #808080; height:50px;\">";

    if (canEditShowTime) {
        holderHtml = holderHtml + "             <div style=\"position:relative;\">";
        holderHtml = holderHtml + "                 <input type=\"text\" class=\"input-field cardStartDate\" style=\"position:absolute; z-index:999; height:50px; padding-bottom:30px; display:block; text-align:left; color:rgba(128, 128, 128, 1); border:none !important;\" placeholder=\"dd/mm/yyyy\" onchange=\"showEditDateTimePop('" + pulse.PulseId + "', this);\" value=\"" + startDateString + "\" />";
        holderHtml = holderHtml + "                 <div style=\"position:absolute; top:20px;\">";
        holderHtml = holderHtml + "                        <label class=\"mini-card-day\" style=\"display: inline-block; color:rgba(128, 128, 128, 1);\">" + startTimeDay + "</label>";
        holderHtml = holderHtml + "                        <label class=\"mini-card-hour\" style=\"display: inline-block; color:rgba(128, 128, 128, 1);\">" + startTimeHH + "</label>";
        holderHtml = holderHtml + "                        <label style=\"display: inline-block; color:rgba(128, 128, 128, 1);\"> : </label>";
        holderHtml = holderHtml + "                        <label class=\"mini-card-min\" style=\"display: inline-block; color:rgba(128, 128, 128, 1);\">" + startTimeMM + "</label>";
        holderHtml = holderHtml + "                        <label class=\"mini-card-period\" style=\"display: inline-block; color:rgba(128, 128, 128, 1);\">" + mStartDate.format("A") + "</label>";
        holderHtml = holderHtml + "                  </div>";
        holderHtml = holderHtml + "             </div>";

    }
    else {
        holderHtml = holderHtml + "            <label style=\"color:rgba(128, 128, 128, 1);\">" + startDateString + "</label>";
        holderHtml = holderHtml + "            <label style=\"display: inline-block; color:rgba(128, 128, 128, 1);\">" + startTimeHH + "</label>";
        holderHtml = holderHtml + "            <label style=\"display: inline-block; color:rgba(128, 128, 128, 1);\"> : </label>";
        holderHtml = holderHtml + "            <label style=\"display: inline-block; color:rgba(128, 128, 128, 1);\">" + startTimeMM + "</label>";
        holderHtml = holderHtml + "            <label style=\"display: inline-block; color:rgba(128, 128, 128, 1);\">" + mStartDate.format("A") + "</label>";
    }
    holderHtml = holderHtml + "             </div>";
    holderHtml = holderHtml + "        </span>";
    holderHtml = holderHtml + "    </div>";
    holderHtml = holderHtml + "    <div class=\"grid__span--10 grid__span--last mini-card-holder\" style=\"width:85%; position: relative;\">";
    if (pulse.HasIncomingLogic || pulse.HasOutgoingLogic) {
        holderHtml += "<div class=\"fake-layer fake-layer1\"></div>";
        holderHtml += "<div class=\"fake-layer fake-layer2\"></div>";
    }
    holderHtml = holderHtml + cardHtml;
    holderHtml = holderHtml + "    </div>";
    holderHtml = holderHtml + "</div>";

    return holderHtml;
}

function deleteCard(pulseId) {
    var card, cardId, ajaxData, pulse, pulseIndex;
    var cardsAfterList, idx;
    var cardLabel, $card, cardLevel, newLabel;

    //card = $(button).parents(".card");
    //cardId = card.data("cardid");

    ajaxData = {
        'adminUserId': $("#main_content_hfAdminUserId").val(),
        'companyId': $("#main_content_hfCompanyId").val(),
        'deckId': $("#main_content_hfDeckId").val(),
        'cardId': pulseId
    };


    jQuery.ajax({
        type: "DELETE",
        url: "/api/PulseResponsiveCard",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(ajaxData),
        dataType: "json",
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("Pulse deleted", 1);
                RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function switchToFullCard(pulseId) {
    var pulse = getPulse(pulseId);

    // 隱藏 mini card
    $(".card.mini-card." + pulseId).parent().parent().find('.card-date').css('display', 'none');
    $(".card.mini-card." + pulseId).parent().parent().find('.card-date').css('position', 'absolute');
    $(".card.mini-card." + pulseId).parent().parent().find('.mini-card-holder').css('width', '100%');

    // 將 full card html 加入
    $(".card.mini-card." + pulseId).after(getFullCardHtml(pulse));

    // 移除 mini card
    $(".card.mini-card." + pulseId).remove();

    // update counter
    updateTextCounter(pulse);
}

function switchToMiniCard(pulseId) {
    var html, pulse, pulseId, parentPulse, isFullCardExist = false, grandparentPulse;

    html = "";
    pulse = getPulse(pulseId);
    parentPulse = getParentPulse(pulse.PulseId);

    if (parentPulse == null) {
        html = getMiniCardHtml(pulse);
    }
    else {
        html = getMiniCardHtml(pulse, parentPulse.PulseId);
    }

    $(".card.full-card." + pulseId).after(html);
    $(".card.full-card." + pulseId).remove();

    if (parentPulse != null) {
        $(".card-holder." + parentPulse.PulseId + " .card.full-card").each(function (key, value) {
            isFullCardExist = true;
        });

        grandparentPulse = getParentPulse(parentPulse.PulseId);
    }

    if (grandparentPulse != null) {
        $(".card-holder." + grandparentPulse.PulseId + " .card.full-card").each(function (key, value) {
            isFullCardExist = true;
        });
    }

    if (!isFullCardExist) {
        $(".card.mini-card." + pulseId).parent().parent().find('.card-date').css('display', 'block');
        $(".card.mini-card." + pulseId).parent().parent().find('.mini-card-holder').css('width', '85%');
    }
}


function cascadeCards(pulseId, hideChildCards) {
    // Step 1. Find all full card
    $(".card-holder." + pulseId + " .card.full-card").each(function (key, value) {
        switchToMiniCard($(this).data("cardid"));
    });

    // Step 2. Close children
    var isVisible, pulseChildren, idx, hideChildCards, $iconBtn;

    $iconBtn = $(".card." + pulseId + " button.cascade-card i");

    if (hideChildCards === undefined) {
        hideChildCards = !($(".card." + pulseId).hasClass("hide-childcards"));
        if (hideChildCards) {
            $(".card." + pulseId).addClass("hide-childcards");
            if ($iconBtn.length > 0) {
                $iconBtn.removeClass("fa fa-arrow-circle-o-up fa-lg icon");
                $iconBtn.addClass("fa fa-arrow-circle-o-down fa-lg icon");
            }


        } else {
            $(".card." + pulseId).removeClass("hide-childcards");
            if ($iconBtn.length > 0) {
                $iconBtn.removeClass("fa fa-arrow-circle-o-down fa-lg icon");
                $iconBtn.addClass("fa fa-arrow-circle-o-up fa-lg icon");
            }
        }
    }

    // for each children of this pulse 
    pulseChildren = getPulseChildren(pulseId);

    for (idx = 0; idx < pulseChildren.length; idx++) {
        cascadeCards(pulseChildren[idx], hideChildCards);
        if (hideChildCards) {
            $(".card." + pulseChildren[idx]).hide();
        } else {
            $(".card." + pulseChildren[idx]).removeClass("hide-childcards");
            $(".card." + pulseChildren[idx]).show();
        }
    }
}

function toggleFakeLayer(pulseId) {
    $(".card." + pulseId).parent().find(".fake-layer").toggle();
}

function showDelPulsePop(pulseId, startDateTime) {
    $("#mpe_backgroundElement").show();
    $("#plDelPulse").show();
    $("#spDelPulseTime").html(startDateTime);
    $("#lbDelPulseConfirm").attr("href", "javascript:deleteCard('" + pulseId + "')");
}

function showEditDateTimePop(pulseId, element) {
    var pulse = getPulse(pulseId);
    if (pulse) {
        // Step 1. Get new DateTime
        var originalDateTime = moment.utc(pulse.StartDate).add(timezone, 'hours');
        var elementDateTime = moment.utc(element.value, "DD MMM YYYY");
        var newDateTime = elementDateTime.format("DD MMM YYYY, dddd") + " " + originalDateTime.format("hh") + ":" + originalDateTime.format("mm") + " " + originalDateTime.format("A");

        if (isDateTimeValid(newDateTime, 1)) {
            newDateTime = moment(newDateTime, "DD MMM YYYY, dddd hh:mm A");
            if (!newDateTime.isSame(originalDateTime)) {
                $(".card-holder." + pulseId + " .mini-card-day").html(newDateTime.format("dddd"));

                $("#spDateTimeFrom").html(originalDateTime.format("DD MMM YYYY") + " " + originalDateTime.format("dddd") + " " + originalDateTime.format("hh") + ":" + originalDateTime.format("mm") + " " + originalDateTime.format("A"));
                $("#spDateTimeTo").html(newDateTime.format("DD MMM YYYY") + " " + newDateTime.format("dddd") + " " + newDateTime.format("hh") + ":" + newDateTime.format("mm") + " " + newDateTime.format("A"));
                $("#lbEditCancel").attr("href", "javascript:hideEditDateTime('" + pulseId + "')");
                $("#lbEditDateTimeAll").attr("href", "javascript:updatePulseDateTime('" + pulseId + "', true, '" + newDateTime.format('YYYY/MM/DD HH:mm:ss') + "')");
                if (model.deck.publishMethodType == 2 && (model.deck.perTimeFrameType == 1 || model.deck.perTimeFrameType == 3)) {
                    $("#lbEditDateTimeAll").css("visibility", "hidden");
                }

                $("#lbEditDateTimeOnly").attr("href", "javascript:updatePulseDateTime('" + pulseId + "', false, '" + newDateTime.format('YYYY/MM/DD HH:mm:ss') + "')");
                $("#mpe_backgroundElement").show();
                $("#plEditPulseDateTime").show();
            }
        }
        else {
            element.value = originalDateTime.format("DD MMM YYYY");
        }
    }
}

function updatePulseDateTime(pulseId, isApplyAllCards, newDateTimeString) {
    var pulse = getPulse(pulseId);
    if (pulse) {
        var ajaxData = {
            'action': 2,
            'deckId': $("#main_content_hfDeckId").val(),
            'cardId': pulseId,
            'adminUserId': $("#main_content_hfAdminUserId").val(),
            'companyId': $("#main_content_hfCompanyId").val(),
            'startDate': moment.utc(newDateTimeString, "YYYY/MM/DD HH:mm:ss").add(-timezone, 'hours'),
            'isApplyToAllLaterCards': isApplyAllCards
        };

        jQuery.ajax({
            type: "POST",
            url: "/api/PulseResponsiveCard",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(ajaxData),
            dataType: "json",
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                if (d.Success) {
                    ShowToast("Pulse updated", 1);
                    RedirectPage('/DynamicPulse/ResponsivePulse/' + $("#main_content_hfDeckId").val(), 300);
                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });
    }
}

function searchPulses(element, event) {
    //debugger;
    if (event.keyCode == 13) {
        __doPostBack('ctl00$main_content$lbSearch', '');
    }
}