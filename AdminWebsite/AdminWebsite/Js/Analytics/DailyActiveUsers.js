﻿var timeZone = 0;
var dauUsersData = [];
var dauAveTimeSpentData = [];
var chartActiveUserY = [];
var chartAveTimeY = [];

var oo;

function DauColumn() {
    this.XAxisDate = "";
    this.XAxisDay = "";
    this.XAxisMonth = "";
    this.XAxisYear = "";
    this.NumberOfActiveUsers = 0;
    this.AverageHourSpent = 0;
}

function gd(year, month, day) {
    
    return new Date(year, month, day, 0, 0, 0, 0).getTime() + ((moment().zone() / -60) * 3600 * 1000); //hours * seconds * miliseconds
}

var dau_users = [
    [gd(2015, 11, 15), 182], [gd(2015, 11, 16), 200], [gd(2015, 11, 17), 181], [gd(2015, 11, 18), 174], [gd(2015, 11, 19), 512], [gd(2015, 11, 20), 557], [gd(2015, 11, 21), 831], [gd(2015, 11, 22), 799], [gd(2015, 11, 23), 780], [gd(2015, 11, 24), 668], [gd(2015, 11, 25), 683], [gd(2015, 11, 26), 636], [gd(2015, 11, 27), 557], [gd(2015, 11, 28), 590], [gd(2015, 11, 29), 788], [gd(2015, 11, 30), 355], [gd(2015, 12, 1), 272], [gd(2015, 12, 2), 315], [gd(2015, 12, 3), 463], [gd(2015, 12, 4), 674], [gd(2015, 12, 5), 625], [gd(2015, 12, 6), 758], [gd(2015, 12, 7), 714], [gd(2015, 12, 8), 665], [gd(2015, 12, 9), 625], [gd(2015, 12, 10), 611], [gd(2015, 12, 11), 537], [gd(2015, 12, 12), 551], [gd(2015, 12, 13), 412], [gd(2015, 12, 14), 442]
];

var dau_timespent = [
    [gd(2015, 11, 15), 1.78], [gd(2015, 11, 16), 1.84], [gd(2015, 11, 17), 2.01], [gd(2015, 11, 18), 2.2], [gd(2015, 11, 19), 2.4], [gd(2015, 11, 20), 2.6], [gd(2015, 11, 21), 3], [gd(2015, 11, 22), 3.1], [gd(2015, 11, 23), 3.2], [gd(2015, 11, 24), 3.3], [gd(2015, 11, 25), 3.4], [gd(2015, 11, 26), 3.3], [gd(2015, 11, 27), 3.2], [gd(2015, 11, 28), 3.1], [gd(2015, 11, 29), 3.2], [gd(2015, 11, 30), 3.3], [gd(2015, 12, 1), 3.4], [gd(2015, 12, 2), 3.5], [gd(2015, 12, 3), 3.6], [gd(2015, 12, 4), 4], [gd(2015, 12, 5), 4.2], [gd(2015, 12, 6), 4.4], [gd(2015, 12, 7), 4.6], [gd(2015, 12, 8), 4.8], [gd(2015, 12, 9), 5], [gd(2015, 12, 10), 5.2], [gd(2015, 12, 11), 5.4], [gd(2015, 12, 12), 5.6], [gd(2015, 12, 13), 5.8], [gd(2015, 12, 14), 6.1]
];

var dau_24hrs = [
    [0, 191], [1, 182], [2, 200], [3, 181], [4, 174], [5, 512], [6, 557], [7, 831], [8, 799], [9, 780], [10, 668], [11, 683], [12, 636], [13, 557], [14, 590], [15, 788], [16, 355], [17, 272], [18, 315], [19, 463], [20, 674], [21, 625], [22, 758], [23, 714], [24, 665]];

var applepercent = 30;
var androidpercent = 70;


var maingraph = [
    {
        label: "Active User",
        data: dau_users,
        color: "#ff9600",
        bars: {
            show: true,
            align: "center",
            barWidth: 60 * 60 * 10000,
            fill: 1
        }
    },
    {
        label: "Average time spent",
        data: dau_timespent,
        yaxis: 2,
        color: "#ff2851",
        points: {
            fillColor: "#fff",
            color: "#ff2851",
            lineWidth: 1,
            show: true
        },
        lines: {
            show: true,
            fill: true,
            lineWidth: 1,
            color: "#ff2851",
            fillColor: 'rgba(255,150,0,0.2)'
        },
        shadowSize: 0
    }
];

var mainoptions = {
    legend: {
        show: false
    },
    xaxis: {
        mode: "time",
        timeformat: "%d",
        tickSize: [1, "day"],
        tickLength: 0

    },
    yaxes: [{
        min: 0,
        ticks: [[200, '200'], [400, '400'], [600, '600'], [800, '800'], [1000, '1000 users']]
    }, {
        alignTicksWithAxis: true,
        position: "right",
        ticks: [[2, '2'], [4, '4'], [6, '6'], [8, '8'], [10, '10hrs']]
    }],
    grid: {
        borderWidth: 0,
        borderColor: "#ccc",
        clickable: true
    }
};

var hrsgraph = [
    {
        label: "",
        data: dau_24hrs,
        color: "#8cc63f",
        points: {
            fillColor: "#fff",
            color: "#8cc63f",
            lineWidth: 1,
            show: true
        },
        lines: {
            show: true,
            fill: true,
            lineWidth: 1,
            color: "#8cc63f",
            fillColor: "#8cc63f"
        },
        shadowSize: 0
    }
]

var hrsoptions = {
    grid: {
        borderWidth: 1,
        borderColor: "#ccc"
    },
    xaxis: {
        ticks: [[0, '00:00'], [1, ''], [2, ''], [3, '3:00am'], [4, ''], [5, ''], [6, '6:00am'], [7, ''], [8, ''], [9, '9:00am'], [10, ''], [11, ''], [12, 'Noon'], [13, ''], [14, ''], [15, '3:00pm'], [16, ''], [17, ''], [18, '6:00pm'], [19, ''], [20, ''], [21, '9:00pm'], [22, ''], [23, '23:00']]
    },
    yaxis: {
        ticks: [[1, '1'], [2, '2'], [3, '3 users']]
    }
}
var maingraphobj = '';
var hrsgraphobj = '';
var starting_month = "";
var ending_month = "";

$(document).ready(function () {
    // maingraphobj = $.plot('#ca-maingraph', maingraph, mainoptions);
    //hrsgraphobj = $.plot('#ca-24graph', hrsgraph, hrsoptions);

    $("#startDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'MM dd, yy',
        showOtherMonths: true,
        defaultDate: -31
    });

    $("#endDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'MM dd, yy',
        showOtherMonths: true,
        defaultDate: -1
    });

    if ($("#main_content_hfTimezone").val().length > 0) {
        timeZone = parseInt($("#main_content_hfTimezone").val());
    }

    var startDate = moment.utc(moment().year() + "/" + (moment().month() + 1) + "/" + moment().date() + " 00:00:00", "YYYY/MM/DD HH:mm:ss").add(-30, "days");
    var endDate = moment.utc(moment().year() + "/" + (moment().month() + 1) + "/" + moment().date() + " 00:00:00", "YYYY/MM/DD HH:mm:ss").add(-1, "days");

    $("#startDate").val(startDate.format("MMMM DD, YYYY"));
    $("#endDate").val(endDate.format("MMMM DD, YYYY"));

    getDauDetail();

});

function setmobilegraph(apple, android) {
    $("#ca-applestats").css("width", apple + "%");
    $("#ca-androidstats").css("width", android + "%");
    $("#ca-applestatstxt").html(apple + "%");
    $("#ca-androidstatstxt").html(android + "%");

    $("#ca-applestatstxt").css("left", (apple + 10) + "%");
    $("#ca-androidstatstxt").css("left", (android + 10) + "%");

    if (apple < 20) {
        $("#ca-applestats").css('text-indent', '-9999px');
    } else {
        $("#ca-applestats").css('text-indent', '0px');
    }
    if (android < 20) {
        $("#ca-androidstats").css('text-indent', '-9999px');
    } else {
        $("#ca-androidstats").css('text-indent', '0px');
    }

}

var dayOfWeek = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

function drawDAUChart() {
    starting_month = moment(dauUsersData[dauUsersData.length - 1][0]).format("MMM");
    ending_month = moment(dauUsersData[0][0]).format("MMM");
    var maingraph = [
        {
            label: "Active User",
            data: dauUsersData,
            color: "#ff9600",
            bars: {
                show: true,
                align: "center",
                barWidth: 60 * 60 * 10000,
                fill: true,
                zero: true
            },
            clickable: true
        },
        {
            label: "Average time spent",
            data: dauAveTimeSpentData,
            yaxis: 2,
            color: "#ff2851",
            points: {
                fillColor: "#fff",
                color: "#ff2851",
                lineWidth: 1,
                show: true
            },
            lines: {
                show: true,
                fill: true,
                lineWidth: 1,
                color: "#ff2851",
                fillColor: 'rgba(255,150,0,0.2)'
            },
            shadowSize: 0,
            clickable: true
        }
    ];

    var mainoptions = {
        legend:
        {
            show: false
        },
        clickable: true,
        xaxis:
        {
            mode: "time",
            timeformat: "%d",
            tickSize: [1, "day"],
            //minTickSize: 0,
            //alignTicksWithAxis: 0,
            //transform: function (x) {
            //    console.debug(x);
            //    //return x + 100000000;
            //}
            //show: true,
            //ticks: dayOfWeek,
            //position: "bottom"
        },
        yaxes:
        [
            {
                min: 0,
                minTickSize: 0,
                ticks: chartActiveUserY
            },
            {
                min: 0,
                minTickSize: 0,
                //alignTicksWithAxis: true,
                position: "right",
                ticks: chartAveTimeY
            }
        ],
        grid: {
            borderWidth: 0,
            borderColor: "#ccc",
            clickable: true,
            hoverable: true,
            
        }
    };

    maingraphobj = $.plot('#ca-maingraph', maingraph, mainoptions);

    $('#ca-maingraph').append('<label id="starting_month" style="position: absolute;bottom: -6px; left: 10px;  font-weight: 700;">' + starting_month + '</label><label id="ending_month" style="position:absolute; right: 0px; bottom: -6px; font-weight: 700;">' + ending_month + '</label>');
}

$("<div id='tooltip'></div>").css({
    position: "absolute",
    display: "none",
    border: "1px solid #fdd",
    //padding: "2px",
    color: '#fff',
    padding: "5px 20px",
    "background-color": "#000",
    opacity: 0.80
}).appendTo("body");


$("#ca-maingraph").bind("plothover", function (event, pos, item) {
    if (item) {
        var x = item.datapoint[0].toFixed(2),
            y = item.datapoint[1].toFixed(2);

        $("#tooltip").html(item.series.label +": " + y)
            .css({ top: item.pageY + 5, left: item.pageX + 5 })
            .fadeIn(200);
    } else {
        $("#tooltip").hide();
    }
});

var chart_table_data = [];
var dateString = "";

var chart_table = $('#chart-detail-table');

function generateChartTable() {
    chart_table.bind('dynatable:afterUpdate', function (e, dynatable) {
        setTimeout(function () {
            chart_table.css('display', 'table');
            chart_table.addClass('animated fadeIn');
        }, 500);
    });

    chart_table.bind('dynatable:init', function (e, dynatable) {

        dynatable.queries.functions['search_chart'] = function (record, queryValue) {
            return record.search_chart.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
        };

    });

    chart_table.dynatable({
        features: {
            paginate: true,
            search: false,
            sorting: true,
            recordCount: false,
            pushState: false,
        },
        dataset: {
            records: chart_table_data,
            //sorts: { 'no': 1 },
            perPageDefault: 7
        },
        inputs: {
            queries: $(' #search_chart')
        }
    });

    var dynatable = chart_table.data('dynatable');

    if (typeof dynatable.records !== "undefined") {
        dynatable.records.updateFromJson({ records: chart_table_data });
        dynatable.records.init();
    }

    dynatable.paginationPerPage.set(7);
    setTimeout(function () {
        dynatable.process();
        $('.chart-detail-content .dynatable-pagination-links').css({ 'display': 'block' });

    }, 300);
    

    chart_table.data('dynatable').settings.dataset.records = chart_table_data;
    chart_table.data('dynatable').dom.update();

}

function fetchChartDetail(date) {
    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),//'C8b28091502514318bdcf48bec7c69129', //$("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(), //'Udb0e704ee7244d21aea7930c27709fee',// $("#main_content_hfManagerId").val(),
        'StartDate': date //'2017/07/06' 
    };

    jQuery.ajax({
        type: "POST",
        url: "/Api/Analytics/GetUserActivityPerDay",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        success: function (d, status, xhr) {
            if (d.Success) {
                console.log(d);
                $('#chartDetail .spinner').fadeOut(200);
                //$('#totalTimeTaken'); $('#averageTimePerUser');
                var breakdownReport = d.BreakdownReports;
                chart_table_data = [];

                if (moment(dateString).isSame(moment($("#endDate").val()))) {
                    $('.toggle-date-btn.next').addClass('inactive')
                } else if (moment(dateString).isSame(moment($("#startDate").val()))) {
                    $('.toggle-date-btn.prev').addClass('inactive')
                } else {
                    $('.toggle-date-btn').removeClass('inactive')
                }

                $('.date-title').html(moment(date).format('dddd, D MMMM YYYY'));

                if (breakdownReport.length > 0) {

                    $('.detail-body .no-data').remove();

                    $.each(breakdownReport, function (key, value) {

                        chart_table_data.push({
                            "user": '<img src="' + value.User.ProfileImageUrl + '" /> ' + value.User.FirstName + " " + value.User.LastName,
                            "department": value.User.Departments[0].Title,
                            "start_time": value.StartTimestamp,
                            "end_time": value.EndTimestamp,
                            "duration": value.DurationString
                        });
                    });

                    $('#totalTimeTaken').html(d.TotalTimeTakenString);
                    $('#averageTimePerUser').html(d.AverageTimeTakenPerUserString);

                } else {
                    //$('.detail-body').empty();
                    if (!$('.detail-body').children('.no-data').length > 0) {
                        $('.detail-body').append('<p class="no-data" style="text-align:center; width: 100%; height: 90%; position: absolute;top: 10%;left: 0px;background-color: rgba(255,255,255,1);">No data for this date.</p>')

                    }
                }

                generateChartTable();
            }
        }
    })
}

$('.toggle-date-btn').on('click', function (e) {
    e.preventDefault();
    if ($(this).hasClass('next')) {

        if (moment(dateString).add(1, 'days').isBefore(moment($("#endDate").val())) || moment(dateString).add(1, 'days').isSame(moment($("#endDate").val()))) {
            dateString = moment(dateString).add(1, 'days').format('YYYY/MM/DD');
            fetchChartDetail(dateString);
        } 
    } else {
        if (moment(dateString).subtract(1, 'days').isAfter(moment($("#startDate").val())) || moment(dateString).subtract(1, 'days').isSame(moment($("#startDate").val()))) {
            dateString = moment(dateString).subtract(1, 'days').format('YYYY/MM/DD');
            fetchChartDetail(dateString);
        }

        
    }
})

$("#ca-maingraph").bind("plotclick", function (event, pos, item) {
    if (item) {
        dateString = moment(item.datapoint[0]).format('YYYY/MM/DD');

        $("#chartDetail")
            .css({ width: "100%", height: "100%", top:10, left:10 })
            .fadeIn(200);
        fetchChartDetail(dateString);
    } else {
        $("#chartDetail").fadeOut(500);
    }
});

$('#closeDetail').on('click', function (e) {
    e.preventDefault();
    $("#chartDetail").fadeOut(200);
})

function changeDauDate(isStartDateChanged) {
    var startDate, endDate;
    if (isStartDateChanged) {
        endDate = moment.utc($("#startDate").val() + " 00:00:00", "MMMM DD, YYYY HH:mm:ss").add(29, "days");
        $("#endDate").val(endDate.format("MMMM DD, YYYY"));
    }
    else {
        startDate = moment.utc($("#endDate").val() + " 00:00:00", "MMMM DD, YYYY HH:mm:ss").add(-29, "days");
        $("#startDate").val(startDate.format("MMMM DD, YYYY"));
    }
    getDauDetail();
}

function getDauDetail() {
    // call API
    var startDate = moment.utc($("#startDate").val() + " 00:00:00", "MMMM DD, YYYY HH:mm:ss");//.add(-timeZone, "hours");
    var endDate = moment.utc($("#endDate").val() + " 00:00:00", "MMMM DD, YYYY HH:mm:ss");//.add(-timeZone, "hours");

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'StartDate': startDate,
        'EndDate': endDate
    };

    jQuery.ajax({
        type: "POST",
        url: "/Api/Analytics/GetDau",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                // update UI
                oo = d.DetailDau;

                $("#ltlDauDate").html("As of " + moment.utc(d.DetailDau.CurrentDate).add(timeZone, "hours").format("MMMM DD, YYYY"));
                $("#ltlDauUniqueUsers").html(d.DetailDau.CurrentDateDau.TotalUniqueActiveUsers);

                if (d.DetailDau.CurrentDateDau.DifferenceInNumber < 0) {
                    $("#plDauUniUsers").removeClass().addClass("ca-changelabel ca-red");
                }
                else {
                    $("#plDauUniUsers").removeClass().addClass("ca-changelabel ca-green");
                }

                $("#ltlDauDiffNumber").html(d.DetailDau.CurrentDateDau.DifferenceInNumber);
                $("#ltlDauDiffPercentage").html(d.DetailDau.CurrentDateDau.DifferenceInPercentage.toFixed(2));
                $("#ltlAveTimeSpent").html(d.DetailDau.AverageTimeSpentPerUser);

                $("#ltlLast7DaysUniqueUsers").html(d.DetailDau.LastSevenDaysDau.TotalUniqueActiveUsers);
                if (d.DetailDau.LastSevenDaysDau.DifferenceInNumber < 0) {
                    $("#plLast7DaysUniUsers").removeClass().addClass("ca-changelabel ca-red");
                }
                else {
                    $("#plLast7DaysUniUsers").removeClass().addClass("ca-changelabel ca-green");
                }

                $("#ltlLast7DaysDiffNumber").html(d.DetailDau.LastSevenDaysDau.DifferenceInNumber);
                $("#ltlLast7DaysDiffPercentage").html(d.DetailDau.LastSevenDaysDau.DifferenceInPercentage.toFixed(2));

                $("#LtlLast30DaysUniqueUsers").html(d.DetailDau.LastThirtyDaysDau.TotalUniqueActiveUsers);
                if (d.DetailDau.LastThirtyDaysDau.DifferenceInNumber < 0) {
                    $("#plLast30DaysUniUsers").removeClass().addClass("ca-changelabel ca-red");
                }
                else {
                    $("#plLast30DaysUniUsers").removeClass().addClass("ca-changelabel ca-green");
                }
                $("#LtlLast30DaysDiffNumber").html(d.DetailDau.LastThirtyDaysDau.DifferenceInNumber);
                $("#LtlLast30DaysDiffPercentage").html(d.DetailDau.LastThirtyDaysDau.DifferenceInPercentage.toFixed(2));

                $("#ltlTotalUsers").html(d.DetailDau.TotalCurrentUsers);
                $("#ltlIosUsers").html(d.DetailDau.NumberOfIOS);
                $("#ltlIosUsersPercentage").html(d.DetailDau.PercentageOfIOS.toFixed(0));
                $("#ltlAndroidUsers").html(d.DetailDau.NumberOfAndroid);
                $("#ltlAndroidUsersPercentage").html(d.DetailDau.PercentageOfAndroid.toFixed(0));

                $("#ltlDauChartTotalUser").html(d.DetailDau.TotalCurrentUsers);
                $("#ltlDauChartInactiveUser").html(d.DetailDau.TotalCurrentUsers - d.DauChart.TotalUniqueActiveUsers);
                $("#ltlDauChartActiveUser").html(d.DauChart.TotalUniqueActiveUsers);

                if (d.DauChart.AttendanceRatePercentage < 0) {
                    $("#ltlDauChartAttRate").html("0");
                }
                else {
                    $("#ltlDauChartAttRate").html(d.DauChart.AttendanceRatePercentage.toFixed(0));
                }

                if (d.DauChart.TotalHours == null || d.DauChart.TotalHours == undefined) {
                    d.DauChart.TotalHours = "0";
                }
                $("#ltlDauChartTotalTime").html(d.DauChart.TotalHours.replace(" ", "<br />"));
                $("#ltlDauChartAveTime").html(d.DauChart.AverageTimeSpentPerUser);

                dauUsersData = [];
                dauAveTimeSpentData = [];
                var highestActiveUser = 0;
                var highestAveTimeSpent = 0;

                if (d.DetailDau.DauColumns.length < 30) {
                    if (d.DetailDau.DauColumns.length == 0) {
                        var dauColumn = new DauColumn();
                        dauColumn.XAxisDate = startDate.format("YYYY/MM/DD HH:mm:ss");
                        dauColumn.XAxisDay = startDate.format("DD");
                        dauColumn.XAxisMonth = startDate.format("MM");
                        dauColumn.XAxisYear = startDate.format("YYYY");
                        dauColumn.NumberOfActiveUsers = 0;
                        dauColumn.AverageHourSpent = 0;
                        d.DetailDau.DauColumns.push(dauColumn);
                    }

                    for (var i = d.DetailDau.DauColumns.length; i < 30; i++) {
                        var dauColumn = new DauColumn();
                        dauColumn.XAxisDate = moment.utc(d.DetailDau.DauColumns[i - 1].XAxisDate).add(1, "days");
                        dauColumn.XAxisDay = dauColumn.XAxisDate.format("DD");
                        dauColumn.XAxisMonth = dauColumn.XAxisDate.format("MM");
                        dauColumn.XAxisYear = dauColumn.XAxisDate.format("YYYY");
                        dauColumn.NumberOfActiveUsers = 0;
                        dauColumn.AverageHourSpent = 0;
                        d.DetailDau.DauColumns.push(dauColumn);
                    }
                }

                for (var i = 0; i < d.DetailDau.DauColumns.length; i++) {
                    var dauUserDay = moment.utc(d.DetailDau.DauColumns[i].XAxisYear + "/" + d.DetailDau.DauColumns[i].XAxisMonth + "/" + d.DetailDau.DauColumns[i].XAxisDay + " 00:00:00", "YYYY/MM/DD HH:mm:ss");//.add(-timeZone, "hours");
                    //dauUsersData.push(new Array(gd(d.DetailDau.DauColumns[i].XAxisYear, d.DetailDau.DauColumns[i].XAxisMonth, d.DetailDau.DauColumns[i].XAxisDay), d.DetailDau.DauColumns[i].NumberOfActiveUsers));
                    //dauAveTimeSpentData.push(new Array(gd(d.DetailDau.DauColumns[i].XAxisYear, d.DetailDau.DauColumns[i].XAxisMonth, d.DetailDau.DauColumns[i].XAxisDay), d.DetailDau.DauColumns[i].AverageHourSpent));
                    //debugger;
                    dauUsersData.push(new Array(gd(dauUserDay.year(), dauUserDay.months(), dauUserDay.dates()), d.DetailDau.DauColumns[i].NumberOfActiveUsers));
                    dauAveTimeSpentData.push(new Array(gd(dauUserDay.year(), dauUserDay.months(), dauUserDay.dates()), d.DetailDau.DauColumns[i].AverageHourSpent));

                    //dauUsersData.push(new Array(dauUserDay.dates(), d.DetailDau.DauColumns[i].NumberOfActiveUsers));
                    //dauAveTimeSpentData.push(new Array(dauUserDay.dates(), d.DetailDau.DauColumns[i].AverageHourSpent));

                    if (d.DetailDau.DauColumns[i].NumberOfActiveUsers > highestActiveUser) {
                        highestActiveUser = d.DetailDau.DauColumns[i].NumberOfActiveUsers;
                    }

                    if (d.DetailDau.DauColumns[i].AverageHourSpent > highestAveTimeSpent) {
                        highestAveTimeSpent = d.DetailDau.DauColumns[i].AverageHourSpent;
                    }
                }

                var increment = 0;
                increment = (highestActiveUser / 4) + 1;
                if (increment * 4 <= highestActiveUser) {
                    increment++;
                }

                highestActiveUser = increment * 4;
                chartActiveUserY = [];
                for (var i = 0; i < highestActiveUser + 1; i += increment) {
                    if (i != 0) {
                        if (i == highestActiveUser) {
                            chartActiveUserY.push(new Array(i, i + " users"));
                        }
                        else {
                            chartActiveUserY.push(new Array(i, i));
                        }
                    }
                }

                increment = 0;
                if (highestAveTimeSpent != 0) {
                    increment = (highestAveTimeSpent / 4) + 1;
                }

                if (increment * 4 <= highestAveTimeSpent) {
                    increment++;
                }
                highestAveTimeSpent = increment * 4;
                chartAveTimeY = [];
                for (var i = 0; i < highestAveTimeSpent + 1; i += increment) {
                    if (i != 0) {
                        if (i == highestAveTimeSpent) {
                            chartAveTimeY.push(new Array(i, i + "hrs"));
                        }
                        else {
                            chartAveTimeY.push(new Array(i, i));
                        }
                    }
                }

                drawDAUChart();
                setmobilegraph(d.DetailDau.PercentageOfIOS.toFixed(2), d.DetailDau.PercentageOfAndroid.toFixed(2));

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}