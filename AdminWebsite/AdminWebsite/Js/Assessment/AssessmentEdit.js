﻿const SURVEY_CARD_CONTENT_MAX = 250;
const SURVEY_CARD_OPTION_CONTENT_MAX = 150;
const SURVEY_CARD_NUMBER_RANGE_MIN = -20;
const SURVEY_CARD_NUMBER_RANGE_MAX = 20;
const SURVEY_CARD_OPTION_RANGE_LABEL_MAX = 25;

/* Objects */
var NewCard;

// ContentImage object
function getContentImageObj() {
    return {
        'base64': '',
        'extension': '',
        'width': 0,
        'height': 0
    };
}

// Option object
function getOptionObj() {
    return {
        'contentText': '',
        'contentImg': {
            'base64': '',
            'extension': '',
            'width': 0,
            'height': 0
        },
        'hasLogic': false,
        'logicCardId': '',
        'Allocations': [],
        'Content': '',
        'Images': []
    };

}
/* Objects */

/* Reset a NewCard */
function resetNewCard() {
    NewCard = new Object();
    NewCard.type = 1;
    NewCard.contentText = document.getElementById('tbNewCardContent').value;
    NewCard.contentImgs = new Array();
    NewCard.options = new Array();
    NewCard.options[0] = getOptionObj();
    NewCard.optionRangeStartFrom = 1; // Start from 1: Min, 2: Middle, 3: Max
    NewCard.optionRangeMiddle = 5;
    NewCard.optionRangeMin = 0;
    NewCard.optionRangeMax = 10;
    NewCard.optionRangeMiddleLabel = "";
    NewCard.optionRangeMaxLabel = "";
    NewCard.optionRangeMinLabel = "";
    NewCard.isAllowSkipped = false;
    NewCard.isTickOtherAnswer = false;
    NewCard.customCommand = "";
    NewCard.isRandomOption = false;
    NewCard.minOptions = 1; //Must be > 0 
    NewCard.maxOptions = 1; //Must be > 0 
    NewCard.diplayBg = 1;
    NewCard.note = "";
    NewCard.isPageBreak = false;
    NewCard.totalPointsAllocation = 0;

    // Reset UI
    document.getElementById('tbNewCardContent').value = NewCard.contentText;
    document.getElementById('lblContentCount').innerHTML = SURVEY_CARD_CONTENT_MAX;
    document.getElementById('divNewCardContentImg').style.visibility = "hidden";
    document.getElementById('imgNewCardContentImgPreview').src = "";
    document.getElementById('lblNewCardOption1ContentCount').innerHTML = SURVEY_CARD_OPTION_CONTENT_MAX;
    document.getElementById('divNewCardRangeNonmiddle').style.display = "block";
    document.getElementById('divNewCardRangeMiddle').style.display = "none";
    document.getElementById('cbNewCardAllowSkip').checked = NewCard.isAllowSkipped;
    document.getElementById('cbNewCardTickOther').checked = NewCard.isTickOtherAnswer;
    document.getElementById('tbNewCardCustomCommand').value = NewCard.customCommand;
    document.getElementById('cbNewCardRandomOption').checked = NewCard.isRandomOption;
    document.getElementById('tbNewCardMultiMin').value = NewCard.minOptions;
    document.getElementById('tbNewCardMultiMax').value = NewCard.maxOptions;
    document.getElementById('tbNewCardNotes').value = NewCard.note;
    document.getElementById('cbNewCardPageBreak').checked = NewCard.isPageBreak;
    $('.tbNewCardOptionRangeMin').val(NewCard.optionRangeMin);
    $('.tbNewCardOptionRangeMax').val(NewCard.optionRangeMax);
    $('.tbNewCardOptionRangeMiddle').val(NewCard.optionRangeMiddle);
    $('#lblNewCardOptionRangeMiddleMin').html(NewCard.optionRangeMin);
    $('.tbNewCardOptionRangeMinLabel').val("");
    $('.tbNewCardOptionRangeMaxLabel').val("");
    $('.tbNewCardOptionRangeMiddleLabel').val("");

    document.getElementById('lblNewCardRangeNonmiddleMinCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeNonmiddleMaxCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeMiddleMiddleCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeMiddleMaxCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeMiddleMinCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
}
/* Reset a NewCard */

/* Set NewCard value */
function setNewCardValue(type, controller, arg) {
    var toastMsg = "";

    switch (type) {
        case 1: // type: 1 => NewCard.contentText
            NewCard.contentText = controller.value;
            document.getElementById('lblNewCardPreviewContentText').innerHTML = controller.value;
            break;
        case 2: // type: 2 => NewCard.options.contentText | arg: index of options.
            NewCard.options[arg - 1].contentText = controller.value;
            rebuildNewCardPreviewOptions();
            break;
        case 3: // type: 3 => NewCard.isAllowSkipped
            NewCard.isAllowSkipped = controller.checked;
            document.getElementById('divNewCardPeviewNoPageBreak').style.display = "none";
            document.getElementById('divNewCardPeviewPageBreak').style.display = "none";
            document.getElementById('divNewCardPeviewSkip').style.display = "none";
            if (controller.checked) {
                document.getElementById('divNewCardPeviewSkip').style.display = "";
            }
            else {
                if (NewCard.isPageBreak) {
                    document.getElementById('divNewCardPeviewPageBreak').style.display = "";
                }
                else {
                    document.getElementById('divNewCardPeviewNoPageBreak').style.display = "";
                }
            }
            break;
        case 4: // type: 4 => NewCard.isTickOtherAnswer
            NewCard.isTickOtherAnswer = controller.checked;
            if (controller.checked) {
                document.getElementById('divNewCardCustomCommand').style.visibility = "visible";
                document.getElementById('divNewCardCustomCommand').style.height = "auto";
            }
            else {
                document.getElementById('divNewCardCustomCommand').style.visibility = "hidden";
                document.getElementById('divNewCardCustomCommand').style.height = "0px";
            }

            if (NewCard.customCommand === "") {
                NewCard.customCommand = "Please specify";
            }

            rebuildNewCardPreviewOptions();
            break;
        case 5: // type: 5 => NewCard.isRandomOption
            NewCard.isRandomOption = controller.checked;

            break;
        case 6: // type: 6 => NewCard.customCommand
            if (controller.value === "") {
                NewCard.customCommand = "Please specify";
            }
            else {
                NewCard.customCommand = controller.value;
            }

            rebuildNewCardPreviewOptions();
            break;
        case 7: // type: 7 => NewCard.minOptions
            var min = 1;
            if (isNaN(parseInt(controller.value, 10))) {
                toastMsg = "Please enter a number";
            }
            else {
                min = parseInt(controller.value, 10);
                if (min > 0) {
                    if (min > NewCard.options.length) {
                        toastMsg = "The minimum number of question to be selected cannot be bigger than count of options.";
                    }
                    else {
                        controller.value = min;
                        NewCard.minOptions = min;
                    }
                }
                else {
                    toastMsg = "The minimum number of question to be selected must be bigger than 0.";
                }
            }
            break;
        case 8: // type: 8 => NewCard.MaxOptions
            var max = 1;
            if (isNaN(parseInt(controller.value, 10))) {
                if (controller.value.toLowerCase() === "all" || controller.value.toLowerCase() === "max") {
                    NewCard.maxOptions = NewCard.options.length;
                }
                else {
                    toastMsg = "Please enter a number, all or max";
                }
            }
            else {
                max = parseInt(controller.value, 10);
                if (max > 0) {
                    if (max > NewCard.options.length) {
                        toastMsg = "The maximum number of question to be selected cannot be bigger than count of options.";
                    }
                    else {
                        controller.value = max;
                        NewCard.maxOptions = max;
                    }
                }
                else {
                    toastMsg = "The maximum number of question to be selected must be bigger than 0.";
                }
            }
            break;

        case 9: // type: 9 => NewCard.note
            NewCard.note = controller.value;
            document.getElementById('imgNewCardPreviewNote').title = controller.value;
            break;
        case 10: // type: 10 => NewCard.isPageBreak
            NewCard.isPageBreak = controller.checked;

            document.getElementById('divNewCardPeviewNoPageBreak').style.display = "none";
            document.getElementById('divNewCardPeviewPageBreak').style.display = "none";
            document.getElementById('divNewCardPeviewSkip').style.display = "none";
            if (NewCard.isAllowSkipped) {
                document.getElementById('divNewCardPeviewSkip').style.display = "";
            }
            else {
                if (controller.checked) {
                    document.getElementById('divNewCardPeviewPageBreak').style.display = "";
                }
                else {
                    document.getElementById('divNewCardPeviewNoPageBreak').style.display = "";
                }
            }
            break;
        case 11: // type: 11 => NewCard.optionRangeMin
            // Step 1. 必須輸入
            if (controller.value === "") {
                toastMsg = "Please enter an integer for Minimum value";
                NewCard.optionRangeMin = null;
                break;
            }

            // Step 2. 必須為 int
            if (isNaN(controller.value)) {
                toastMsg = "Please enter an integer for Minimum value";
                NewCard.optionRangeMin = null;
                break;
            }
            if (parseFloat(controller.value).toString().indexOf(".") >= 0) {
                toastMsg = "Please enter an integer for Minimum value";
                NewCard.optionRangeMin = null;
                break;
            }

            // Step 3. 必須在 -20 ~ 19 之間
            if (parseInt(controller.value, 10) < -20 || parseInt(controller.value, 10) > 19) {
                toastMsg = "Minimum value must be within -20 and 19.";
            }

            // Step 4. 必須小於 max
            if (parseInt(controller.value, 10) >= NewCard.optionRangeMax) {
                toastMsg = "Minimum value must be smaller than maximum value.";
            }

            NewCard.optionRangeMin = parseInt(controller.value, 10);

            break;
        case 12: // type: 12 => NewCard.optionRangeMax (for Minimum/Maximum mode)
            // Step 1. 必須輸入
            if (controller.value === "") {
                toastMsg = "Please enter an integer for Maximum value";
                NewCard.optionRangeMax = null;
                break;
            }

            // Step 2. 必須為 int
            if (isNaN(controller.value)) {
                toastMsg = "Please enter an integer for Maximum value";
                NewCard.optionRangeMax = null;
                break;
            }
            if (parseFloat(controller.value).toString().indexOf(".") >= 0) {
                toastMsg = "Please enter an integer for Maximum value";
                NewCard.optionRangeMax = null;
                break;
            }

            // Step 3. 必須在 -19~20 之間
            if (parseInt(controller.value, 10) < -19 || parseInt(controller.value, 10) > 20) {
                toastMsg = "Maximum value must be within -19 and 20.";
            }

            // Step 4. 必須大於 min
            if (parseInt(controller.value, 10) <= NewCard.optionRangeMin) {
                toastMsg = "Maximum value must be bigger than minimum value.";
            }

            NewCard.optionRangeMax = parseInt(controller.value, 10);
            break;
        case 13: // type: 13 => NewCard.diplayBg
            var divNewCardPreview = document.getElementById('divNewCardPreview');
            switch (arg) {
                case 1:
                    divNewCardPreview.style.backgroundImage = "url(/Img/bg_survey_1a_big.png)";
                    break;
                case 2:
                    divNewCardPreview.style.backgroundImage = "url(/Img/bg_survey_2a_big.png)";
                    break;
                case 3:
                    divNewCardPreview.style.backgroundImage = "url(/Img/bg_survey_3a_big.png)";
                    break;
                default:
                    divNewCardPreview.style.backgroundImage = "url(/Img/bg_survey_1a_big.png)";
                    break;
            }
            NewCard.diplayBg = arg;

            break;

        case 14: // type: 14 => NewCard.droplist text

            var html = "", optionList, idx, opt;
            optionList = $("#txareaNewCard").val().trim().replace(/\n\n/g, "").split(/\n/);

            // Reset NewCard options
            NewCard.options = [];

            for (idx = 0; idx < optionList.length; idx++) {
                opt = getOptionObj();
                opt.contentText = optionList[idx];
                NewCard.options.push(opt);
            }
            break;
        case 15: // type: 15 => NewCard.optionRangeStartFrom
            NewCard.optionRangeStartFrom = parseInt(controller.value, 10);

            break;

        case 16: // type: 16 => NewCard.optionRangeMiddle
            // Step 1. 必須輸入
            if (controller.value === "") {
                toastMsg = "Please enter an integer for Middle value";
                NewCard.optionRangeMiddle = null;
                break;
            }

            // Step 2. 必須為 int
            if (isNaN(controller.value)) {
                toastMsg = "Please enter an integer for Middle value";
                NewCard.optionRangeMiddle = null;
                break;
            }
            if (parseFloat(controller.value).toString().indexOf(".") >= 0) {
                toastMsg = "Please enter an integer for Middle value";
                NewCard.optionRangeMiddle = null;
                break;
            }

            // Step 3. 必須在 -19 ~ 19 之間
            if (parseInt(controller.value, 10) < -19 || parseInt(controller.value, 10) > 19) {
                toastMsg = "Middle value must be between -19 and 19.";
            }

            // Step 4. 必須小於 Maximum value
            if (parseInt(controller.value, 10) > NewCard.optionRangeMax) {
                toastMsg = "Middle value must be smaller than  Maximum value.";
            }

            NewCard.optionRangeMiddle = parseInt(controller.value, 10);
            setMinValue(NewCard.optionRangeMiddle, NewCard.optionRangeMax);

            break;

        case 17: // type: 17 => NewCard.optionRangeMiddleLabel
            NewCard.optionRangeMiddleLabel = controller.value.substring(0, 25).trim();
            setNewCardPreviewNumberRange();
            break;
        case 18: // type: 18 => NewCard.optionRangeMaxLabel
            NewCard.optionRangeMaxLabel = controller.value.substring(0, 25).trim();
            setNewCardPreviewNumberRange();
            break;
        case 19: // type: 19 => NewCard.optionRangeMinLabel
            NewCard.optionRangeMinLabel = controller.value.substring(0, 25).trim();
            setNewCardPreviewNumberRange();
            break;

        case 20: // type: 20 => NewCard.optionRangeMax (for Middle mode)
            // Step 1. 必須輸入
            if (controller.value === "") {
                toastMsg = "Please enter an integer for Maximum value";
                NewCard.optionRangeMax = null;
                break;
            }

            // Step 2. 必須為 int
            if (isNaN(controller.value)) {
                toastMsg = "Please enter an integer for Maximum value";
                NewCard.optionRangeMax = null;
                break;
            }
            if (parseFloat(controller.value).toString().indexOf(".") >= 0) {
                toastMsg = "Please enter an integer for Maximum value";
                NewCard.optionRangeMax = null;
                break;
            }

            // Step 3. 必須在 Middle value ~ 20 之間
            if (parseInt(controller.value, 10) <= NewCard.optionRangeMiddle || parseInt(controller.value, 10) > 20) {
                toastMsg = "Maximum value must be within Middle value and 20.";
            }

            NewCard.optionRangeMax = parseInt(controller.value, 10);
            setMinValue(NewCard.optionRangeMiddle, NewCard.optionRangeMax);
            break;

        case 21: // type: 21 => NewCard.totalPointsAllocation
            NewCard.totalPointsAllocation = controller.value
            // Comment until we work on preview
            //document.getElementById('lblNewCardPreviewContentText').innerHTML = controller.value;
            break;
        case 22: // type 22 => for edit 
            //type, controller, arg
            break;
        default:
            break;

    }

    if (toastMsg.length > 0) {
        ReloadErrorToast();
        toastr.error(toastMsg);
    }
}
/* Set NewQuestion value */


function setNewCardOptionAllocationValue(e) {
    // Find NewCard option index
    var cardOption, cardOptionIdx, option, optionAllocationElement, optionAllocationElementIdx, optionAllocation;
    cardOption = $(e).parents(".card-option");
    cardOptionIdx = $(e).parents(".options-container").children().index(cardOption);
    
    optionAllocationElement = $(e).parents(".option-allocation");
    optionAllocationElementIdx = $(e).parents(".option-tabulation-allocation").children().index(optionAllocationElement)

    option = NewCard.options[cardOptionIdx];
    optionAllocation = option.Allocations[optionAllocationElementIdx];

    // Update UI to optionAllocation
    optionAllocation.multiplier = optionAllocationElement.children(".allocate-weight").val();
    optionAllocation.tabulationId = optionAllocationElement.find(".allocate-tabulation").val();
}

function cardOptionAllocationValue(e) {
    
    var card, cardIdx, cardModel, cardOption, cardOptionIdx, option, optionAllocationElement, optionAllocationElementIdx, optionAllocation;
    var cardModel, cardOptionModel, cardOptionAllocationModel;

    card = $(e).parents(".card");
    cardIdx = $(e).parents("#cardList").children().index(card);

    cardOption = $(e).parents(".card-option");
    cardOptionIdx = $(e).parents(".options-container").children().index(cardOption);

    optionAllocationElement = $(e).parents(".option-allocation");
    optionAllocationElementIdx = $(e).parents(".option-tabulation-allocation").children().index(optionAllocationElement)

    cardModel = model.assessment.Cards[cardIdx];
    cardOptionModel = cardModel.Options[cardOptionIdx];
    cardOptionAllocationModel = cardOptionModel.Allocations[optionAllocationElementIdx];

    if (cardOptionAllocationModel) {
        if ($(e).hasClass("allocate-tabulation")) {
            cardOptionAllocationModel.TabulationId = $(e).children(":selected").val();
            
        }
        cardOptionAllocationModel.ValueOperator = 3;
        cardOptionAllocationModel.TabulationOperator = 1;
        
        if ($(e).hasClass("allocate-weight")) {
            cardOptionAllocationModel.Value = $(e).val();
        }
        
        updateLocalJSON();
    }
}


/* Calculate Minimun value */
function setMinValue(middleValue, maxValue) {
    var minValue = middleValue * 2 - maxValue;
    if (minValue < -20 || minValue > 18) {
        ReloadErrorToast();
        toastr.error("Please enter a correct value for Middle and Maximum value.");
    }
    else {
        $('#lblNewCardOptionRangeMiddleMin').html(minValue);
        NewCard.optionRangeMin = minValue;
        setNewCardPreviewNumberRange();
    }
}
/* Calculate Minimun value */

/* For set NewCard type */
function setNewCardType(type) {
    var divId, divElement;
    // Reset all div of Card Type 
    for (var i = 0; i < 7; i++) {
        // NewCard type edit area
        divId = "divNewCardType" + (i + 1);

        divElement = document.getElementById(divId);
        if (divElement !== null) {
            document.getElementById(divId).style.background = "#FFFFFF";
            document.getElementById(divId).style.color = "#CCCCCC";
            divId += "Layout";
            document.getElementById(divId).style.visibility = "hidden";
            document.getElementById(divId).style.display = "none";
            document.getElementById(divId).style.height = "0px";

            // NewCard type preview area
            divId = "divNewCardType" + (i + 1) + "PreviewLayout";
            document.getElementById(divId).style.display = "none";
            document.getElementById(divId).style.height = "0px";
        }
        
    }
    // Reset "Behaviour" and "No of question to be select"
    document.getElementById('divNewCardBehaviour').style.visibility = "hidden";
    document.getElementById('divNewCardBehaviour').style.display = "none";
    document.getElementById('divNewCardBehaviour').style.height = "0px";
    document.getElementById('divNewCardBehaviour').style.overflow = "auto";
    document.getElementById('divNewCardTickOther').style.visibility = "hidden";
    document.getElementById('divNewCardTickOther').style.display = "none";
    document.getElementById('divNewCardTickOther').style.height = "0px";
    document.getElementById('divNewCardTickOther').style.overflow = "auto";
    document.getElementById('divNewCardRandomOption').style.visibility = "hidden";
    document.getElementById('divNewCardRandomOption').style.display = "none";
    document.getElementById('divNewCardRandomOption').style.height = "0px";
    document.getElementById('divNewCardRandomOption').style.overflow = "auto";
    document.getElementById('divNewCardMulti').style.visibility = "hidden";
    document.getElementById('divNewCardMulti').style.display = "none";
    document.getElementById('divNewCardMulti').style.height = "0px";
    document.getElementById('divNewCardMulti').style.overflow = "auto";

    // NewCard type edit area
    divId = "divNewCardType" + type;
    document.getElementById(divId).style.background = "#0075FF";
    document.getElementById(divId).style.color = "#F2F4F7";
    if ((type === 2) || (type === 7)) { // Type 2's layout is same as type 1's.
        divId = "divNewCardType1";
    }
    divId += "Layout";
    document.getElementById(divId).style.visibility = "visible";
    document.getElementById(divId).style.display = "block";
    document.getElementById(divId).style.height = "auto";

    // NewCard type preview area
    divId = "divNewCardType" + type + "PreviewLayout";
    document.getElementById(divId).style.display = "block";
    document.getElementById(divId).style.height = "auto";
    NewCard.type = type;
    // "Behaviour" and "No of question to be select" Layout

    document.getElementById('assignTotalPoints').style.display = "none";
    switch (type) {
        case 1: // Select one
            document.getElementById('divNewCardBehaviour').style.visibility = "visible";
            document.getElementById('divNewCardBehaviour').style.display = "block";
            document.getElementById('divNewCardBehaviour').style.height = "auto";
            document.getElementById('divNewCardTickOther').style.visibility = "visible";
            document.getElementById('divNewCardTickOther').style.display = "block";
            document.getElementById('divNewCardTickOther').style.height = "auto";
            refreshOptionFieldUI();
            break;
        case 2: // Multi choice
            document.getElementById('divNewCardBehaviour').style.visibility = "visible";
            document.getElementById('divNewCardBehaviour').style.display = "block";
            document.getElementById('divNewCardBehaviour').style.height = "auto";
            document.getElementById('divNewCardMulti').style.visibility = "visible";
            document.getElementById('divNewCardMulti').style.display = "block";
            document.getElementById('divNewCardMulti').style.height = "auto";
            refreshOptionFieldUI();
            break;
        case 3: // Text
            document.getElementById('divNewCardBehaviour').style.visibility = "visible";
            document.getElementById('divNewCardBehaviour').style.display = "block";
            document.getElementById('divNewCardBehaviour').style.height = "auto";
            break
        case 4: // Number range
            document.getElementById('divNewCardBehaviour').style.visibility = "visible";
            document.getElementById('divNewCardBehaviour').style.display = "block";
            document.getElementById('divNewCardBehaviour').style.height = "auto";
            setNewCardPreviewNumberRange();
            if (NewCard.optionRangeStartFrom !== 2) // start from Minimum/Maximum
            {
                document.getElementById('divNewCardRangeNonmiddle').style.display = "block";
                document.getElementById('divNewCardRangeMiddle').style.display = "none";
            }
            else // satrt from Middle
            {
                document.getElementById('divNewCardRangeMiddle').style.display = "block";
                document.getElementById('divNewCardRangeNonmiddle').style.display = "none";
            }

            break;
        case 5: // Drop list
            document.getElementById('divNewCardBehaviour').style.visibility = "visible";
            document.getElementById('divNewCardBehaviour').style.display = "block";
            document.getElementById('divNewCardBehaviour').style.height = "auto";
            break;
        case 6: // Instructions
            break
        case 7: // Accumulation
            document.getElementById('divNewCardBehaviour').style.visibility = "visible";
            document.getElementById('divNewCardBehaviour').style.display = "block";
            document.getElementById('divNewCardBehaviour').style.height = "auto";
            document.getElementById('divNewCardTickOther').style.visibility = "visible";
            document.getElementById('divNewCardTickOther').style.display = "block";
            document.getElementById('divNewCardTickOther').style.height = "auto";
            document.getElementById('assignTotalPoints').style.display = "block";
            refreshOptionFieldUI();
            break;
        default:
            break;
    }

    NewCard.type = type;
    rebuildNewCardPreviewOptions();
}
/* For set NewCard type */

function previewImage(input, maxWidth, maxHeight, type, arg) // arg: "index of contentImgs", "index of options".
{
    if (input.files && input.files[0]) {
        var t = input.files[0].type;
        var s = input.files[0].size / (1024 * 1024);
        if (t.toLowerCase() === "image/png" || t.toLowerCase() === "image/jpeg" || t.toLowerCase() === "image/jpg") {
            if (s > 5) {
                toastr.error('Uploaded photo exceeded 5MB.');
            } else {
                var fileReader = new FileReader();
                var image = new Image();
                fileReader.onload = function (e) {
                    image.src = e.target.result;
                    image.onload = function () {
                        var w = this.width,
                            h = this.height;
                        if ((w > maxWidth || h > maxHeight)) {
                            toastr.error('Uploaded photo exceeded ' + maxWidth + ' x ' + maxHeight + '.');
                        }
                        else {
                            switch (type) {
                                case 1: // New Card > Content Image
                                    document.getElementById('imgNewCardContentImgPreview').src = this.src;
                                    document.getElementById('divNewCardContentImg').style.visibility = "visible";
                                    document.getElementById('divNewCardContentImg').style.height = "auto";
                                    document.getElementById('imgNewCardPreviewContentImg').src = this.src;

                                    var contentImg = getContentImageObj();
                                    contentImg.base64 = this.src;
                                    contentImg.extension = t;
                                    contentImg.width = w;
                                    contentImg.height = h;
                                    NewCard.contentImgs[arg - 1] = contentImg;

                                    break;
                                case 2: // New Card > Option Image
                                    var id = "imgNewOptionContent" + arg + "ImgPreview";
                                    document.getElementById('imgNewOption' + arg + 'ContentImgPreview').src = this.src;
                                    document.getElementById('divNewCardOption' + arg + 'ContentImg').style.visibility = "visible";
                                    document.getElementById('divNewCardOption' + arg + 'ContentImg').style.height = "auto";

                                    NewCard.options[arg - 1].contentImg.base64 = this.src;
                                    NewCard.options[arg - 1].contentImg.extension = t;
                                    NewCard.options[arg - 1].contentImg.width = w;
                                    NewCard.options[arg - 1].contentImg.height = h;

                                    rebuildNewCardPreviewOptions();

                                    break;
                                default:
                                    break;
                            }
                        }
                    };
                }
            }
        }
        else {
            toastr.error('File extension is invalid.');
        }
        fileReader.readAsDataURL(input.files[0]);
    }
}

function removeContentImg(type, arg) {
    switch (type) {
        case 1: // NewCard.contentImgs[arg]
            document.getElementById('imgNewCardContentImgPreview').src = "";
            document.getElementById('divNewCardContentImg').style.visibility = "hidden";
            document.getElementById('divNewCardContentImg').style.height = "0px";
            document.getElementById('imgNewCardPreviewContentImg').src = "";

            NewCard.contentImgs.splice(arg - 1, 1);
            break;
        case 2: // NewCard.options[arg].contentImg
            document.getElementById('imgNewOption' + arg + 'ContentImgPreview').src = "";
            document.getElementById('divNewCardOption' + arg + 'ContentImg').style.visibility = "hidden";
            document.getElementById('divNewCardOption' + arg + 'ContentImg').style.height = "0px";

            NewCard.options[arg - 1].contentImg.base64 = "";
            NewCard.options[arg - 1].contentImg.extension = "";
            NewCard.options[arg - 1].contentImg.width = 0;
            NewCard.options[arg - 1].contentImg.height = 0;

            rebuildNewCardPreviewOptions();
            break;
        default:
            break;
    }
}

function refreshOptionFieldUI() {
    var newCardOption, allocationIdx, selectedProp;
    if (NewCard.options === null || NewCard.options.length === 0) {
        NewCard.options = new Array();
        NewCard.options[0] = getOptionObj();
    }

    var html = "";
    for (var i = 0; i < NewCard.options.length; i++) {
        newCardOption = NewCard.options[i];
        html +=
                '<div style="margin-bottom: 10px" class="card-option" id="divNewCardOption' + (i + 1) + '">' +
                    '<a class="remove-option-link" onclick="removeNewCardOptionField(' + i + ');" href="javascript:void(0);">- Remove option</a>' +
                    '<textarea name="tbNewCardOption' + (i + 1) + '" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,\'lblNewCardOption' + (i + 1) + 'ContentCount\', 150); setNewCardValue(2, this, ' + (i + 1) + ');" onblur="setNewCardValue(2, this, ' + (i + 1) + ');" placeholder="Option ' + (i + 1) + '" rows="2" cols="20">';
        if (NewCard.options[i].contentText !== null && NewCard.options[i].contentText !== "") {
            html += NewCard.options[i].contentText;
        }
        html += '</textarea>' +
                    '<span id="lblNewCardOption' + (i + 1) + 'ContentCount" class="survey-lettercount">';
        if (NewCard.options[i].contentText !== null && NewCard.options[i].contentText !== "") {
            html += SURVEY_CARD_OPTION_CONTENT_MAX - NewCard.options[i].contentText.length;
        }
        else {
            html += SURVEY_CARD_OPTION_CONTENT_MAX;
        }

        html += '</span>' +
                    '<label class="upload-option-image" for="fuNewCardOption' + (i + 1) + 'ContentImg">' +
                        '<input type="file" id="fuNewCardOption' + (i + 1) + 'ContentImg" accept="image/*" title="Add image" style="display: none" onchange="previewImage(this, 2560, 2560 ,2, ' + (i + 1) + ');" />' +
                    '</label>';

        if (NewCard.options[i].contentImg.base64 !== null && NewCard.options[i].contentImg.base64 !== "") {
            html +=
                '<div id="divNewCardOption' + (i + 1) + 'ContentImg" style="visibility: visible; height: auto; overflow: auto;" class="option-image-content">' +
                        '<div class="survey-cross" onclick="removeContentImg(2, ' + (i + 1) + ');"><i class="fa fa-times" aria-hidden="true"></i></div>' +
                        '<img id="imgNewOption' + (i + 1) + 'ContentImgPreview" src="' + NewCard.options[i].contentImg.base64 + '" />';
        }
        else {
            html +=
                '<div id="divNewCardOption' + (i + 1) + 'ContentImg" style="visibility: hidden; height: 0px; overflow: auto;" class="option-image-content">' +
                        '<div class="survey-cross" onclick="removeContentImg(2, ' + (i + 1) + ');"><i class="fa fa-times" aria-hidden="true"></i></div>' +
                        '<img id="imgNewOption' + (i + 1) + 'ContentImgPreview" />';
        }
        html += '</div>';

        html += '<div class="option-tabulation-allocation">';
        for (allocationIdx = 0; allocationIdx < newCardOption.Allocations.length; allocationIdx++) {
            selectedProp = "";
            html = html + "<div class=\"option-allocation\">";
            html = html + "    Each point allocated to the answer, system will multiply the value of ";
            html = html + "    <input type=\"number\" class=\"allocate-weight\" onkeyup=\"setNewCardOptionAllocationValue(this);\" value=\"" + newCardOption.Allocations[allocationIdx].multiplier + "\" /> and add to ";
            html = html + "    <div class=\"mdl-selectfield\">";
            html = html + "        <select class=\"allocate-tabulation\" onchange=\"setNewCardOptionAllocationValue(this);\">";
            html = html + "             <option value=\"\"></option>";
            var idx = 0;
            for (idx = 0; idx < model.assessment.Tabulations.length; idx++) {
                if (model.assessment.Tabulations[idx].TabulationId === newCardOption.Allocations[allocationIdx].tabulationId) {
                    selectedProp = "selected='selected'";
                } else {
                    selectedProp = "";
                }
                html = html + "            <option value=\"" + model.assessment.Tabulations[idx].TabulationId + "\" " + selectedProp + ">" + model.assessment.Tabulations[idx].Label + "</option>";
            }
            html = html + "        </select>";
            html = html + "    </div>";
            html = html + "</div>";
            //html = html + "<div class=\"option-tabulation-allocation\"></div>";
            //html = html + "<div>";
            //html = html + "    <span class=\"clickable\" onclick=\"addOptionAllocation(this);\">Add option allocation</span>";
            //html = html + "</div>";
        }
        html += '</div>'; // close div.option-tabulation-allocation
        html += '<div>';
        html += '    <span class="clickable" onclick="addOptionAllocation(this);">Add option allocation</span>';
        html += '</div>';
        html += '</div>';
    }

    //if (NewCard.type == 7) {
    //    html = html + "<div class=\"assign-total-points\" style=\"clear:both;\">";
    //    html = html + "    <label style=\"font-weight: bold\">Total Points <img src=\"/Img/icon_note_small.png\" /></span></label>";
    //    html = html + "    <span>Points add up value <input onkeydown=\"return (event.keyCode!=13);\" type=\"number\" style=\"width:133px;display:inline-block;\" onkeyup=\"setNewCardValue(21, this, null);\" onblur=\"setNewCardValue(21, this, null);\" /></span><br />";
    //    html = html + "</div>";
    //}

    $("#divNewCardOptions").html(html);
    $("#tbNewCardMultiMax").val(NewCard.options.length);
    NewCard.maxOptions = NewCard.options.length;    
    rebuildNewCardPreviewOptions();
}

function resetOptionField() {
    document.getElementById('divNewCardOptions').innerHTML =
        '<div style="margin-bottom: 10px">' +
            '<textarea name="tbNewCardOption1" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,\'lblNewCardOption1ContentCount\', 150); setNewCardValue(2, this, 1);" onblur="setNewCardValue(2, this, 1);" placeholder="Option 1" rows="2" cols="20"></textarea>' +
            '<span id="lblNewCardOption1ContentCount" class="survey-lettercount"></span>' +
            '<label class="upload-option-image" for="fuNewCardOption1ContentImg">' +
                '<input type="file" id="fuNewCardOption1ContentImg" accept="image/*" title="Add image" style="display: none" onchange="previewImage(this, 2560, 2560 ,2, 1);" />' +
            '</label>' +
            '<div id="divNewCardOption1ContentImg" style="visibility: hidden; height: 0px; overflow: auto;" class="option-image-content">' +
                '<div class="survey-cross" onclick="removeContentImg(2, 1);"><i class="fa fa-times" aria-hidden="true"></i></div>' +
                '<img id="imgNewOption1ContentImgPreview" />' +
            '</div>' +
        '</div>';
}

function addOptionField() {
    if (NewCard.options === null) {
        NewCard.options = new Array();
        NewCard.options[0] = getOptionObj();
    }

    var count = NewCard.options.length;
    NewCard.options[count] = getOptionObj();

    refreshOptionFieldUI();
}

function removeNewCardOptionField(ordering) {
    NewCard.options.splice(ordering, 1);
    refreshOptionFieldUI();
}

function fakeAnimation() {
    $('.card.add-question').removeClass('animated fadeInDown');
    $('.card.add-question').addClass('animated fadeOutDown');
}


function checkNewCardInputData() {
    var toastMsg = "";

    if (NewCard.contentText.length === 0) {
        toastMsg = "Please fill in a question.";
    }
    else {
        if (NewCard.contentText.length > SURVEY_CARD_CONTENT_MAX) {
            toastMsg = "Question content length connot bigger than " + SURVEY_CARD_CONTENT_MAX + ".";
        }
    }

    if (NewCard.type === 3 || NewCard.type === 6 || NewCard.type === 4) {
        NewCard.options = [];
    }
    else {
        if (NewCard.type !== 5) {
            for (var i = 0; i < NewCard.options.length; i++) {
                if (NewCard.options[i].contentText.length === 0) {
                    toastMsg = "Please fill all options.";
                }
                else {
                    if (NewCard.options[i].contentText.length > SURVEY_CARD_OPTION_CONTENT_MAX) {
                        toastMsg = "Option content length connot bigger than " + SURVEY_CARD_OPTION_CONTENT_MAX + ".";
                    }
                }
            }
        }
    }

    // Special case checking for various special case
    if (NewCard.type === 2) { // Make sure that MCQ Maximum cannot be less than Minimum
        if (NewCard.maxOptions < NewCard.minOptions) {
            toastMsg = "Maximum cannot be less then Minimum";
        }
    }

    if (NewCard.type === 4) { // Make sure the minimum value <= maximum value 
        var rangeMin, rangeMax, rangeMid;

        if (NewCard.optionRangeStartFrom === 2) // Middle mode
        {
            // check Minimum, Maximum, Middle value
            if (NewCard.optionRangeMin === null || NewCard.optionRangeMiddle === null || NewCard.optionRangeMax === null) {
                toastMsg = "Please enter a correct value for Middle and Maximum value";
            }
            if (NewCard.optionRangeMax > 20 || NewCard.optionRangeMax <= NewCard.optionRangeMiddle || NewCard.optionRangeMax <= NewCard.optionRangeMin || NewCard.optionRangeMax < -18) {
                toastMsg = "Please enter a correct value for Middle and Maximum value";
            }
            if (NewCard.optionRangeMin < -20 || NewCard.optionRangeMin >= NewCard.optionRangeMiddle || NewCard.optionRangeMin >= NewCard.optionRangeMax || NewCard.optionRangeMin > 18) {
                toastMsg = "Please enter a correct value for Middle and Maximum value";
            }
            if (NewCard.optionRangeMiddle < -19 || NewCard.optionRangeMiddle <= NewCard.optionRangeMin || NewCard.optionRangeMiddle >= NewCard.optionRangeMax) {
                toastMsg = "Please enter a correct value for Middle and Maximum value";
            }

            // check label
            if (NewCard.optionRangeMinLabel.length === 0) {
                toastMsg = "Please label your minimum value.";
            }

            if (NewCard.optionRangeMiddleLabel.length === 0) {
                toastMsg = "Please label your middle value.";
            }

            if (NewCard.optionRangeMaxLabel.length === 0) {
                toastMsg = "Please label your maximum value.";
            }

            if (NewCard.optionRangeMinLabel.length > 25) {
                toastMsg = "Minimum value’s label is too long.";
            }

            if (NewCard.optionRangeMiddleLabel.length > 25) {
                toastMsg = "Middle value’s label is too long.";
            }

            if (NewCard.optionRangeMaxLabel.length > 25) {
                toastMsg = "Maximum value’s label is too long.";
            }
        }
        else // Minimum/Maximum mode
        {
            // check Minimum, Maximum value
            if (NewCard.optionRangeMin === null || NewCard.optionRangeMax === null) {
                toastMsg = "Please enter a correct value for Minimum and Maximum value";
            }
            if (NewCard.optionRangeMax > 20 || NewCard.optionRangeMax <= NewCard.optionRangeMin || NewCard.optionRangeMax < -19) {
                toastMsg = "Please enter a correct value for Minimum and Maximum value";
            }
            if (NewCard.optionRangeMin < -20 || NewCard.optionRangeMin >= NewCard.optionRangeMax || NewCard.optionRangeMin > 19) {
                toastMsg = "Please enter a correct value for Minimum and Maximum value";
            }

            NewCard.optionRangeMiddle = 0;
            NewCard.optionRangeMiddleLabel = "";
        }
    }

    if (NewCard.type === 5) { // drop list
        $("#txareaNewCard").val($("#txareaNewCard").val().trim().replace(/\n\n/g, "").split(/\n/));
    }

    if (document.getElementById('tbNewCardCustomCommand').value === "") {
        NewCard.customCommand = "Please specify";
    }

    if (toastMsg.length > 0) {
        HideProgressBar();
        ReloadErrorToast();
        toastr.error(toastMsg);
        return false;
    }
    else {
        fakeAnimation();
        cardObjectToJson(1);
        return true;
    }
}

function rebuildNewCardPreviewOptions() // For Card type 1(Select one), 2(Multi choice)
{
    if (NewCard.type === 1 || NewCard.type === 2) {
        var html = "";

        if (NewCard.options !== null && NewCard.options.length > 0) {
            for (var i = 0; i < NewCard.options.length; i++) {
                if (NewCard.type === 1) {
                    html += "<div class=\"preview-option\">";
                    if (NewCard.options[i].contentImg !== null && NewCard.options[i].contentImg.base64.length > 0) {
                        html += "    <img class=\"preview-option-image\" src=\"" + NewCard.options[i].contentImg.base64 + "\" />";
                    }
                    html += "   <div class=\"preview-option-content\">" + NewCard.options[i].contentText + "</div>";
                    html += "   <div class=\"clear-float\"></div>";
                    html += "</div>";
                }
                else if (NewCard.type === 2) {
                    html += "<div class=\"preview-option-multi\">";
                    html += "   <div class=\"preview-option\" style=\"width: 70%; float: left;\">";
                    if (NewCard.options[i].contentImg !== null && NewCard.options[i].contentImg.base64.length > 0) {
                        html += "       <img class=\"preview-option-image\" src=\"" + NewCard.options[i].contentImg.base64 + "\" />";
                    }
                    html += "           <div class=\"preview-option-content\">" + NewCard.options[i].contentText + "</div>";
                    html += "           <div class=\"clear-float\"></div>";
                    html += "   </div>";
                    html += "   <img class=\"preview-option-tick\" src=\"/Img/icon_multichoice_tick.png\"";
                    if ((NewCard.options[i].contentImg !== null && NewCard.options[i].contentImg.base64.length > 0) || NewCard.options[i].contentText.length > 0) {
                        html += " />";
                    }
                    else {
                        html += " style=\"visibility: hidden;\" />";
                    }

                    html += "   <div class=\"clear-float\"></div>";
                    html += "</div>";
                }
            }
        }

        if (NewCard.isTickOtherAnswer) {
            html += "<div class=\"preview-others\">";
            html += "   <div class=\"preview-others-content\">Others:</div>";
            html += "   <div class=\"preview-command-content\">" + NewCard.customCommand + "</div>";
            html += "   <div class=\"clear-float\"></div>";
            html += "</div>";
        }

        document.getElementById('divNewCardType' + NewCard.type + 'PreviewLayout').innerHTML = html;
    }
}

function setNewCardPreviewNumberRange() {
    var startFrom;
    if (NewCard.startFrom === 1) // start from min
    {
        startFrom = NewCard.optionRangeMin;
    }
    else if (NewCard.startFrom === 2) // start from mid
    {
        startFrom = NewCard.optionRangeMiddle;
    }
    else if (NewCard.startFrom === 3) // start from max
    {
        startFrom = NewCard.optionRangeMax;
    }
    else // Unknow 
    {
        startFrom = NewCard.optionRangeMin;
    }

    var rangeIdx, rangeValues;
    rangeValues = [];
    for (rangeIdx = NewCard.optionRangeMin; rangeIdx <= NewCard.optionRangeMax; rangeIdx++) {
        rangeValues.push(rangeIdx);
    }

    $("#range").ionRangeSlider({
        min: NewCard.optionRangeMin,
        max: NewCard.optionRangeMax,
        from: startFrom,
        values: rangeValues,
        onFinish: function (data) {
            console.log("onFinish");
            if ((NewCard.optionRangeMinLabel) && (NewCard.optionRangeMinLabel.length > 0)) {
                $(".preview-numberrange .irs-min").text(NewCard.optionRangeMinLabel);
            } else {
                $(".preview-numberrange .irs-min").text(NewCard.optionRangeMin.toString());
            }

            if ((NewCard.optionRangeMaxLabel) && (NewCard.optionRangeMaxLabel.length > 0)) {
                $(".preview-numberrange .irs-max").text(NewCard.optionRangeMaxLabel);
            } else {
                $(".preview-numberrange .irs-max").text(NewCard.optionRangeMax.toString());
            }
        }
    });
    if ((NewCard.optionRangeMinLabel) && (NewCard.optionRangeMinLabel.length > 0)) {
        $(".preview-numberrange .irs-min").text(NewCard.optionRangeMinLabel);
    } else {
        $(".preview-numberrange .irs-min").text(NewCard.optionRangeMin.toString());
    }

    if ((NewCard.optionRangeMaxLabel) && (NewCard.optionRangeMaxLabel.length > 0)) {
        $(".preview-numberrange .irs-max").text(NewCard.optionRangeMaxLabel);
    } else {
        $(".preview-numberrange .irs-max").text(NewCard.optionRangeMax.toString());
    }

}

function updateSelectPreview(txarea) {
    var html = "", optionList, idx;

    // Clear options and add the default
    $(".preview-droplist option").remove();
    $(".preview-droplist").append("<option>Please select</option>");

    optionList = $(txarea).val().trim().replace(/\n\n/g, "\n").split(/\n/);

    for (idx = 0; idx < optionList.length; idx++) {
        $(".preview-droplist").append("<option>" + optionList[idx] + "</option>");
    }
}

function resetRangeType(type) {
    // NewCard value
    NewCard.optionRangeMiddleLabel = "";
    NewCard.optionRangeMaxLabel = "";
    NewCard.optionRangeMinLabel = "";
    NewCard.optionRangeMiddle = 5;
    NewCard.optionRangeMin = 0;
    NewCard.optionRangeMax = 10;

    // UI
    if (type === 1) // Minimum/Maximum
    {
        $('#divNewCardRangeNonmiddle').css('display', 'block');
        $('#divNewCardRangeMiddle').css('display', 'none');
        $('#sltRangeNonmiddle').val("1");
        $('.tbNewCardOptionRangeMin').val(NewCard.optionRangeMin);
        $('.tbNewCardOptionRangeMax').val(NewCard.optionRangeMax);
        $('.tbNewCardOptionRangeMiddle').val(NewCard.optionRangeMiddle);
    }
    else if (type === 2) // Middle
    {
        $('#divNewCardRangeNonmiddle').css('display', 'none');
        $('#divNewCardRangeMiddle').css('display', 'block');
        $('.tbNewCardOptionRangeMin').val(NewCard.optionRangeMin);
        $('.tbNewCardOptionRangeMax').val(NewCard.optionRangeMax);
        $('.tbNewCardOptionRangeMiddle').val(NewCard.optionRangeMiddle);
        $('#lblNewCardOptionRangeMiddleMin').html(NewCard.optionRangeMin);
    }

    $('.tbNewCardOptionRangeMinLabel').val("");
    $('.tbNewCardOptionRangeMaxLabel').val("");
    $('.tbNewCardOptionRangeMiddleLabel').val("");
    document.getElementById('lblNewCardRangeNonmiddleMinCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeNonmiddleMaxCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeMiddleMiddleCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeMiddleMaxCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
    document.getElementById('lblNewCardRangeMiddleMinCount').innerHTML = SURVEY_CARD_OPTION_RANGE_LABEL_MAX;
}