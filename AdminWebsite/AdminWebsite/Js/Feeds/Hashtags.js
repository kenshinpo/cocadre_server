﻿var timezone;
var orderingHashtag;

(function ($) {

    $(document).ready(function () {
        timezone = parseInt($("#hfTimezone").val());
        loadHashTags();

        Sortable.create(sortTbody, {
            animation: 150,
            //handle: '.drag-indicator',
            onEnd: function (e) {

                //debugger;
                var tempHashtagArray = orderingHashtag.slice();
                tempHashtagArray[e.newIndex - 1] = orderingHashtag[e.oldIndex - 1];
                if (e.newIndex > e.oldIndex) {
                    for (var i = (e.oldIndex -1); i < (e.newIndex - 1) ; i++) {
                        tempHashtagArray[i] = orderingHashtag[i + 1];
                    }
                }
                else {
                    for (var i = (e.newIndex); i < e.oldIndex  ; i++) {
                        tempHashtagArray[i] = orderingHashtag[i - 1];
                    }
                }

                var request =
                    {
                        "CompanyId": $("#hfCompanyId").val(),
                        "AdminUserId": $("#hfManagerId").val(),
                        "Hashtags": tempHashtagArray
                    };
                $.ajax({
                    type: "POST",
                    url: "/FeedHashtags/ReorderingHashtag",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(request),
                    dataType: "json",
                    crossDomain: true,
                    beforeSend: function (xhr, settings) {
                        ShowProgressBar();
                    },
                    success: function (d, status, xhr) {
                        if (d.Success) {
                            ShowToast("Reordering hashtags", 1);
                            RedirectPage("/Feed/Hashtags", 300);

                        } else {
                            ShowToast(d.ErrorMessage, 2);
                        }
                    },
                    error: function (xhr, status, error) {
                        ShowToast(error, 2);
                    },
                    complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                        HideProgressBar();
                    }
                });
            }
        });
    });
}(jQuery));


function loadHashTags() {
    var request =
        {
            "CompanyId": $("#hfCompanyId").val(),
            "AdminUserId": $("#hfManagerId").val()
        }

    $.ajax({
        type: "POST",
        url: "/FeedHashtags/GetHashtagList",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {

                // Step 1. 組 html
                if (d.RecommendedTags !== null && d.RecommendedTags.length > 0) {
                    var layout = $(".hashtags_body");
                    var html = "";
                    orderingHashtag = [];
                    for (var i = 0; i < d.RecommendedTags.length; i++) {
                        orderingHashtag.push(d.RecommendedTags[i].HashTag.replace("#",""));

                        /*************************/
                        html += "   <tr>";
                        html += "       <td>";
                        html += "           <span style=\"color: rgba(133, 133, 133, 1);\">";
                        html += d.RecommendedTags[i].Ordering;
                        html += "           </span>";
                        html += "       </td>";
                        html += "       <td>";
                        html += "           <span style=\"color: rgba(133, 133, 133, 1);\">";
                        html += d.RecommendedTags[i].HashTag;
                        html += "           </span>";
                        html += "       </td>";
                        html += "       <td>";
                        html += "           <span style=\"color: rgba(133, 133, 133, 1);\">";
                        var createDateTime = moment(d.RecommendedTags[i].CreatedOnTimestamp).add('hours', timezone);
                        html += createDateTime.format("DD/MM/YYYY HH:mm")
                        html += "           </span>";
                        html += "       </td>";
                        html += "       <td>";
                        html += "           <div class=\"post__user__action\">";
                        html += "               <div class=\"chips--02\">";
                        html += "                   <div class=\"chips--02__container\">";
                        html += "                       <i class=\"fa fa-ellipsis-v chips--02__button\"></i>";
                        html += "                       <ul class=\"chips--02__menu\">";
                        html += "                           <li>";
                        html += "                               <a  href=\"javascript:showRenamePopup('" + d.RecommendedTags[i].HashTag + "');\">Rename</a>";
                        html += "                           </li>";
                        html += "                           <li>";
                        html += "                               <a  href=\"javascript:showDelPopup('" + d.RecommendedTags[i].HashTag + "');\">Delete</a>";
                        html += "                           </li>";
                        html += "                       </ul>";
                        html += "                   </div>";
                        html += "               </div>";
                        html += "           </div>";
                        html += "       </td>";
                        html += "   </tr>";
                    }

                    layout.append(html);
                    initChips();
                    $("#spNoData").hide();
                }
                else {
                    $("#spNoData").show();
                }

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function saveHashTag() {
    // Step 1. check input
    var element = $("#iptHashtag");
    if (!isDataValid(element.val().trim())) {
        return;
    }

    // Step 2. Call API
    var request =
        {
            "CompanyId": $("#hfCompanyId").val(),
            "AdminUserId": $("#hfManagerId").val(),
            "Hashtag": element.val().trim()
        }

    $.ajax({
        type: "POST",
        url: "/FeedHashtags/AddHashtag",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("Added a hashtag", 1);
                RedirectPage("/Feed/Hashtags", 300);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function delHashTag(hashTag) {
    // Step 1. Call API
    var request =
        {
            "CompanyId": $("#hfCompanyId").val(),
            "AdminUserId": $("#hfManagerId").val(),
            "Hashtag": hashTag
        }

    $.ajax({
        type: "POST",
        url: "/FeedHashtags/DelHashtag",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("Deleted the hashtag", 1);
                RedirectPage("/Feed/Hashtags", 300);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function renameHashTag(oldHashTag) {
    // Step 1. check input
    var element = $("#iptRenameHashtag");
    if (!isDataValid(element.val().trim())) {
        return;
    }

    // Step 2. Call API
    var request =
        {
            "CompanyId": $("#hfCompanyId").val(),
            "AdminUserId": $("#hfManagerId").val(),
            "OldHashtag": oldHashTag,
            "NewHashtag": $("#iptRenameHashtag").val()
        }

    $.ajax({
        type: "POST",
        url: "/FeedHashtags/RenameHashtag",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("Updated the hashtag", 1);
                RedirectPage("/Feed/Hashtags", 300);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function isDataValid(hashtag) {
    if (hashtag.length < 1) {
        ShowToast("Please enter your hashtag", 2);
        return false;
    }

    var pattern = /[^a-zA-Z0-9\u0100-\u3000\u3003-\uFFFF_]/g;
    if (pattern.test(hashtag)) {
        ShowToast("Only alphabets, numbers and _ can be used.", 2);
        return false;
    }

    return true;
}

function changeSubmitStyle() {
    var iptHashtag = $("#iptHashtag");
    var btnSubmit = $("#btnSubmit");
    if (iptHashtag.val().length > 0) {
        btnSubmit.css('background', 'rgba(0, 117, 254, 1)').css('color', 'white');
        btnSubmit.removeAttr('disabled');
    }
    else {
        btnSubmit.css('background', 'rgba(203, 203, 203, 1)').css('color', 'white');
        btnSubmit.attr('disabled', 'disabled');

    }
}

function showDelPopup(hashtag) {
    $("#popupBackground").show();
    $("#popupDel").show();
    $("#spDelHashtag").html(hashtag);
    $("#lbDelConfirm").attr("href", "javascript:delHashTag('" + hashtag + "')");
}

function hideDelPopup() {
    $("#popupBackground").hide();
    $("#popupDel").hide();
}

function showRenamePopup(hashtag) {
    $("#popupBackground").show();
    $("#popupRename").show();
    $("#iptRenameHashtag").val(hashtag.replace("#", ""));
    $("#lbRenameConfirm").attr("href", "javascript:renameHashTag('" + hashtag + "')");
}

function hideRenamePopup() {
    $("#popupBackground").hide();
    $("#popupRename").hide();
}