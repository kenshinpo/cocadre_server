﻿var zipFileBase64String = "";

function exportMatchUp() {
    try {
        window.open("/Api/MatchUp/Export?ManagerId=" + $("#main_content_hfManagerId").val() + "&CompanyId=" + $("#main_content_hfCompanyId").val() + "&TopicId=" + $("#main_content_hfTopicId").val());
    } catch (e) {
        console.error(e.toString());
    }
}

function choiceZipFile(element) {
    if (element.files && element.files[0]) {
        var file = element.files[0];
        if (file.type.toLowerCase() == "application/octet-stream" || file.type.toLowerCase() == "application/zip" || file.type.toLowerCase() == "application/x-zip-compressed") {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                zipFileBase64String = e.target.result;
                $(".choiceFile").val("");
            };
            fileReader.readAsDataURL(file);
        }
        else {
            ShowToast("Supported format: *.zip", 2);
        }
    }
    else {
        console.info("No file be selected.");
    }
}


function importMatchUpQuestions() {
    try {
        // Zip file to base64String
        if (zipFileBase64String == null || zipFileBase64String == undefined || zipFileBase64String == "") {
            ShowToast("Please select a zip file", 2);
            return;
        }

        var request = {
            'CompanyId': $("#main_content_hfCompanyId").val(),
            'ManagerId': $("#main_content_hfManagerId").val(),
            'TopicId': $("#main_content_hfTopicId").val(),
            'CategoryId': $("#main_content_hfCategoryId").val(),
            'ZipFileBase64String': zipFileBase64String.replace("data:application/octet-stream;base64,", "").replace("data:application/zip;base64,", "").replace("data:application/x-zip-compressed;base64,", "")
        };

        jQuery.ajax({
            type: "POST",
            url: "/Api/MatchUp/ImportQuestions",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            dataType: "json",
            crossDomain: true,
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                debugger;
                if (d.Success) {
                    window.location.reload();
                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });

    } catch (e) {
        console.error(e.toString());
    }
}
