﻿$(function () {

    var personnel_table = $('#personnel-table');
    var personnel_table_data = [];
    var department_table = $('#department-table');
    var department_table_data = [];

     function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
     };

     var CompanyId = getUrlParameter('CompanyId');
     var AdminUserId = getUrlParameter('AdminUserId');
     var CategoryId = getUrlParameter('CategoryId');
     var TopicId = getUrlParameter('TopicId');
     var CardId = getUrlParameter('CardId');
     var OptionId = getUrlParameter('OptionId');


     $('#question_id').html(CardId);

    function fetchOverview() {

        ShowProgressBar();

        $.ajax({
            type: "POST",
            url: '/adminrs/selectresultoverview',
            data: {
                "CompanyId": CompanyId,
                "AdminUserId": AdminUserId,
                "CategoryId": CategoryId,
                "TopicId": TopicId
            },
            crossDomain: true,
            dataType: 'json',
            success: function (res) {
                HideProgressBar();

                if (res.Success) {
                    var report = res.Overview.Report;
                    var report_arr = res.Overview.Report.ReportList;
                    var topic = res.Overview.Topic;

                    $('#survey-root-link').html(topic.Title);
                    $('#survey-root-link').attr('href', "/Survey/Edit/" + TopicId + "/" + CategoryId);
                    $('#simple-survey-link').attr('href', '/Survey/Analytic/' + TopicId + '/' + CategoryId);
                    $('#survey-topic').html(topic.Title);
                    $('#survey-icon').attr('src', topic.IconUrl);


                    if (report_arr.length > 0) {
                        $('#survey-start-date').html(report_arr[0].DatestampString);
                        $('#survey-end-date').html(report_arr[report_arr.length - 1].DatestampString);
                    }
                }
            }
        });
    }

    function generateTable() {
        ShowProgressBar();
        $.ajax({
            type: "POST",
            url: '/adminRS/SelectOptionResult',
            data: {
                "CompanyId": CompanyId,
                "AdminUserId": AdminUserId,
                "CategoryId": CategoryId,
                "TopicId": TopicId,
                "CardId": CardId,
                "OptionId": OptionId
            },
            crossDomain: true,
            dataType: 'json',
            success: function (res) {
                HideProgressBar();
                if (res.Success) {
                    var html = "";

                    var card = res.Card;
                    var personnel = res.Responders;
                    var department = res.Departments;

                    $('.code').html('Code:'+card.CardId);
                    $('.question').html(card.Content);

                    if (card.HasCustomAnswer && card.CustomAnswer) {
                         $('.answer').html(card.CustomAnswer.Content);
                    } else if (card.Type == 4) {
                        $('.answer').html(card.SelectedRange);
                    } else {
                        $('.answer').html(card.Options[0].Content);
                    }
                    
                    $('.number').html('P' + card.Paging + ' | Q' + card.Ordering);

                    if (card.Type == 1) {
                        html = '<img src="/Img/icon_selectone.png" />';
                    } else if (card.Type == 2) {
                        html = '<img src="/Img/icon_multichoice.png" />';
                    } else if (card.Type == 3) {
                        html = '<img src="/Img/icon_text.png" />';
                    } else if (card.Type == 4) {
                        html += '<img src="/Img/icon_numberrange.png" />';
                    } else if (card.Type == 5) {
                        html = '<img src="/Img/icon_droplist.png" />';
                    } else if (card.Type == 6) {
                        html = '<img src="/Img/icon_instructions.png />';
                    }

                    $('.type-image').html(html);

                    //PERSONNEL TABLE GENERATION
                    if (personnel.length > 0) {
                        $('.error_msg').css('display', 'none');
                        $.each(personnel, function (key, value) {
                            personnel_table_data.push({
                                "no": key + 1,
                                "profile_img": '<img class="rounded-img" src="'+value.ProfileImageUrl+'"/>',
                                "responder_name":  value.FirstName + " " + value.LastName ,
                                "department-filter": value.Departments[0].Title
                            });
                        });


                        personnel_table.dynatable({
                            features: {
                                paginate: false,
                                search: false,
                                sorting: true,
                                recordCount: false,
                                pushState: false
                            },
                            dataset: {
                                records: personnel_table_data,
                                sorts: { 'no': 1 }
                            },
                            inputs: {
                                queries: $('#department-filter, #responder_name')
                            }
                        })

                        var dynatable = personnel_table.data('dynatable');
                        if (typeof dynatable.records !== "undefined") {
                            dynatable.records.updateFromJson({ records: personnel_table_data });
                            dynatable.records.init();
                        }
                        dynatable.paginationPerPage.set(15);
                        dynatable.process();


                    } else {
                        $('.error_msg').css('display', 'block');
                    }

                    //DEPARTMENT TABLE GENERATION
                    if (department.length > 0) {
                        $('.error_msg').css('display', 'none');
                        $.each(department, function (key, value) {
                            department_table_data.push({
                                "no": key + 1,
                                "department_name": value.Title,
                                "count": value.CountOfUsers
                            });
                        });

                        department_table.dynatable({
                            features: {
                                paginate: false,
                                search: false,
                                sorting: true,
                                recordCount: false,
                                pushState: false,
                            },
                            dataset: {
                                records: department_table_data,
                                sorts: { 'no': 1 }
                            },
                            inputs: {
                                queries: $(' #department_name')
                            }
                        })

                        var dynatable = department_table.data('dynatable');
                        if (typeof dynatable.records !== "undefined") {
                            dynatable.records.updateFromJson({ records: department_table_data });
                            dynatable.records.init();
                        }
                        dynatable.paginationPerPage.set(15);
                        dynatable.process();
                    } else {
                        $('.error_msg').css('display', 'block');
                    }

                } else {
                    $('.table-content').prepend('<p class="align-center">Opps! Something went wrong.</p>');
                }
            }
        });
    }

    personnel_table.bind('dynatable:afterUpdate', function (e, dynatable) {
        setTimeout(function () {
            personnel_table.css('display', 'table');
            personnel_table.addClass('animated fadeIn');
        }, 500);
    });

    // CUSTOM TABLE SEARCH FILTER FUNCTION
    personnel_table.bind('dynatable:init', function (e, dynatable) {

        dynatable.queries.functions['responder_name'] = function (record, queryValue) {
            return record.responder_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
        };
    });

    department_table.bind('dynatable:afterUpdate', function (e, dynatable) {
        setTimeout(function () {
            department_table.css('display', 'table');
            department_table.addClass('animated fadeIn');
        }, 500);
    });

    // CUSTOM TABLE SEARCH FILTER FUNCTION
    department_table.bind('dynatable:init', function (e, dynatable) {

        dynatable.queries.functions['department_name'] = function (record, queryValue) {
            return record.department_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
        };
    });

    fetchOverview();
    generateTable();
});