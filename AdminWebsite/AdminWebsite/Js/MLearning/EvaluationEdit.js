﻿const CARD_TYPE_ARTICLE = 1;
const CARD_TYPE_VIDEO = 2;
const CARD_TYPE_INSTRUCTIONS = 3;
const CARD_TYPE_SELECTONE = 4;
const CARD_TYPE_MULTICHOICE = 5;
const CARD_TYPE_SLIDES = 6;

const UPLOAD_FILE_TYPE_IMAGE = 1;
const UPLOAD_FILE_TYPE_VIDEO_LANDSCAPE = 2;
const UPLOAD_FILE_TYPE_PDF_PORTRAIT = 3;
const UPLOAD_FILE_TYPE_PDF_LANDSCAPE = 4;
const UPLOAD_FILE_TYPE_SLIDESHARE = 5;
const UPLOAD_FILE_TYPE_VIDEO_PORTRAIT = 6;

const EXAM_STATUS_DELETED = -1;
const EXAM_STATUS_UNLISTED = 1;
const EXAM_STATUS_ACTIVE = 2;
const EXAM_STATUS_HIDDEN = 3;

const EXAM_PROGRESS_STATUS_UPCOMING = 1;
const EXAM_PROGRESS_STATUS_LIVE = 2;
const EXAM_PROGRESS_STATUS_COMPLETED = 3;

const TEXT_MAX_ARTICLE_TITLE = 60; // Default: 60
const TEXT_MAX_VIDEO_TITLE = 60; // Default: 60
const TEXT_MAX_VIDEO_DESCRIPTION = 140; // Default: 140
const TEXT_MAX_INSTRUCTIONS = 300; // Default: 300
const TEXT_MAX_QUESTION = 140; // Default: 140
const TEXT_MAX_OPTION = 60; // Default: 60
const TEXT_MAX_SLIDES_TITLE = 60; // Default: 60
const TEXT_MAX_SLIDES_DESCRIPTION = 140; // Default: 140

const OPTION_MAX_SCORE = 20;

// CARD_ACTION_TYPE constants - defines card 的行為動作
const CARD_ACTION_TYPE = {};
Object.defineProperty(CARD_ACTION_TYPE, "SHOW_DELETE", { get: function () { return 1; } });
Object.defineProperty(CARD_ACTION_TYPE, "HIDE_DELETE", { get: function () { return 2; } });

const CARD_TYPE_REVERT = [0, CARD_TYPE_SELECTONE, CARD_TYPE_MULTICHOICE, CARD_TYPE_INSTRUCTIONS, CARD_TYPE_ARTICLE, CARD_TYPE_VIDEO, CARD_TYPE_SLIDES];

var exam;

function CreateMLCardRequest(card) {
    this.CompanyId = $("#main_content_hfCompanyId").val();
    this.ManagerId = $("#main_content_hfManagerUserId").val();
    this.CategoryId = $("#main_content_hfCategoryId").val();
    this.ExamId = $("#main_content_hfExamId").val();
    this.CardId = card.CardId;
    this.CardType = card.CardType;
    this.MinOptionToSelect = card.MinOptionToSelect;
    this.MaxOptionToSelect = card.MaxOptionToSelect;
    this.BackgroundType = card.BackgroundType;
    this.Note = card.Note;
    this.HasPageBreak = card.HasPageBreak;

    if (card.CardType == CARD_TYPE_ARTICLE) {
        this.ArticlePages = card.ArticlePages;
        this.Content = null;
        this.Description = null;
        this.UploadFile = null;
        this.Options = null;
        this.IsOptionRandomized = true;
    }
    else if (card.CardType == CARD_TYPE_VIDEO) {
        this.ArticlePages = null;
        this.Content = card.VideoTitle;
        this.Description = card.VideoDescription;
        this.UploadFile = card.VideoContent;
        this.Options = null;
        this.IsOptionRandomized = true;
    }
    else if (card.CardType == CARD_TYPE_INSTRUCTIONS) {
        this.ArticlePages = null;
        this.Content = card.InstructionsText;
        this.Description = null;
        this.UploadFile = card.InstructionsImg;
        this.Options = null;
        this.IsOptionRandomized = true;
    }
    else if (card.CardType == CARD_TYPE_SELECTONE) {
        this.ArticlePages = null;
        this.Content = card.QuestionText;
        this.Description = null;
        this.UploadFile = card.QuestionImg;
        this.Options = card.Options;
        this.IsOptionRandomized = true;
    }
    else if (card.CardType == CARD_TYPE_MULTICHOICE) {
        this.ArticlePages = null;
        this.Content = card.QuestionText;
        this.Description = null;
        this.UploadFile = card.QuestionImg;
        this.Options = card.Options;
        this.IsOptionRandomized = card.IsOptionRandomized;
    }
    else if (card.CardType == CARD_TYPE_SLIDES) {
        this.ArticlePages = null;
        this.Content = card.SlidesTitle;
        this.Description = card.SlidesDescription;
        this.UploadFile = card.SlidesContent;
        this.Options = null;
        this.IsOptionRandomized = true;
    }
    else {
        // do nothing
    }
}

function Exam() {
    this.CategoryId = $("#main_content_hfCategoryId").val();
    this.ExamId = $("#main_content_hfExamId").val();
    this.Cards = [];
    this.IconUrl = "";
    this.Title = "";
    this.Author = "";
    this.Status = -1;
    this.ProgressStatus = -1;
    this.StartDate = null;
}

function Card(examId) {
    this.EaxmId = examId;
    this.CardId = "undefined" + (new Date()).toISOString().replace(/-|T|:|\.|Z/g, "");
    this.CardType = 1;
    this.Paging = -1;
    this.Ordering = -1;

    // Article
    this.ArticlePages = [];
    // Video
    this.VideoTitle = "";
    this.VideoDescription = "";
    this.VideoContent = null;
    // Instructions
    this.InstructionsText = "";
    this.InstructionsImg = null;
    // Select One / Multi Choice
    this.QuestionText = "";
    this.QuestionImg = null;
    this.Options = [];
    // Slides
    this.SlidesTitle = "";
    this.SlidesDescription = "";
    this.SlidesContent = null;
    this.IsUsingSlideShare = false;
    this.SlideShareUrl = "";
    // Behaviour
    this.IsOptionRandomized = true;
    this.MinOptionToSelect = 1;
    this.MaxOptionToSelect = 1;
    // Display
    this.BackgroundType = 1;
    // Notes
    this.Note = "";
    // Page Break
    this.HasPageBreak = false;
    // Others
    this.HasUploadedContent = false;
}

function Option() {
    this.OptionId = null;
    this.Content = "";
    this.Score = 1;
    this.Ordering = -1;
    this.HasUploadedContent = false;
    this.UploadedContent = null;
}

function PageContent() {
    this.Title = "";
    this.Content = "";
}

function UploadContent() {
    this.UploadId = null;
    this.Url = "";
    this.UploadType = 1;
    this.Ordering = -1;

    this.IsNewFile = false;
    this.Base64 = "";
    this.Extension = "";
    this.Width = -1;
    this.Height = -1;
}


$(function () {

    TabFunction();
    rebindJs();

    var pj = JSON.parse($("#main_content_hfExam").val().trim());

    exam = new Exam();
    exam.IconUrl = pj.IconUrl;
    exam.Title = pj.Title;
    exam.Author = pj.Author;
    exam.Status = pj.Status;
    exam.ProgressStatus = pj.ProgressStatus;
    exam.StartDate = moment.utc(pj.StartDate);

    for (var i = 0; i < pj.Cards.length; i++) {
        exam.Cards.push(convertToViewCardModel(pj.Cards[i]));
    }

    if (exam.Status > EXAM_STATUS_UNLISTED && exam.ProgressStatus != EXAM_PROGRESS_STATUS_UPCOMING) {
        $("#btnAddNewCard").css("display", "none");
    }

    displayCardList(exam.Cards);
});

function convertToViewCardModel(backendCardObject) {
    var card = new Card(exam.ExamId);
    card.CardId = backendCardObject.CardId;
    card.CardType = CARD_TYPE_REVERT[backendCardObject.Type];
    card.Paging = backendCardObject.Paging;
    card.Ordering = backendCardObject.Ordering;
    card.IsOptionRandomized = backendCardObject.IsOptionRandomized;
    card.MinOptionToSelect = backendCardObject.MinOptionToSelect;
    card.MaxOptionToSelect = backendCardObject.MaxOptionToSelect;
    card.BackgroundType = backendCardObject.BackgroundType;
    card.Note = backendCardObject.Note;
    card.HasPageBreak = backendCardObject.HasPageBreak;
    card.HasUploadedContent = backendCardObject.HasUploadedContent;

    if (card.CardType == CARD_TYPE_ARTICLE) {
        for (var j = 0; j < backendCardObject.PagesContent.length; j++) {
            var pc = new PageContent();
            pc.Title = backendCardObject.PagesContent[j].Title;
            pc.Content = backendCardObject.PagesContent[j].Content;
            card.ArticlePages.push(pc);
        }
    }
    else if (card.CardType == CARD_TYPE_VIDEO) {
        card.VideoTitle = backendCardObject.Content;
        card.VideoDescription = backendCardObject.Description;
        if (backendCardObject.HasUploadedContent) {
            var uc = new UploadContent();
            uc.UploadId = backendCardObject.UploadedContent[0].UploadId;
            uc.Url = backendCardObject.UploadedContent[0].Url;
            uc.UploadType = backendCardObject.UploadedContent[0].UploadType;
            uc.Ordering = backendCardObject.UploadedContent[0].Ordering;
            card.VideoContent = uc;
        }
    }
    else if (card.CardType == CARD_TYPE_INSTRUCTIONS) {
        card.InstructionsText = backendCardObject.Content;
        if (backendCardObject.HasUploadedContent) {
            var uc = new UploadContent();
            uc.UploadId = backendCardObject.UploadedContent[0].UploadId;
            uc.Url = backendCardObject.UploadedContent[0].Url;
            uc.UploadType = backendCardObject.UploadedContent[0].UploadType;
            uc.Ordering = backendCardObject.UploadedContent[0].Ordering;
            card.InstructionsImg = uc;
        }
    }
    else if (card.CardType == CARD_TYPE_SELECTONE || card.CardType == CARD_TYPE_MULTICHOICE) {
        card.QuestionText = backendCardObject.Content;
        if (backendCardObject.HasUploadedContent) {
            var uc = new UploadContent();
            uc.UploadId = backendCardObject.UploadedContent[0].UploadId;
            uc.Url = backendCardObject.UploadedContent[0].Url;
            uc.UploadType = backendCardObject.UploadedContent[0].UploadType;
            uc.Ordering = backendCardObject.UploadedContent[0].Ordering;
            card.QuestionImg = uc;
        }

        for (var j = 0; j < backendCardObject.Options.length; j++) {
            var op = new Option();
            op.OptionId = backendCardObject.Options[j].OptionId;
            op.Content = backendCardObject.Options[j].Content;
            op.Score = backendCardObject.Options[j].Score;
            op.Ordering = backendCardObject.Options[j].Ordering;
            op.HasUploadedContent = backendCardObject.Options[j].HasUploadedContent;
            if (op.HasUploadedContent) {
                var uc = new UploadContent();
                uc.UploadId = backendCardObject.Options[j].UploadedContent[0].UploadId;
                uc.Url = backendCardObject.Options[j].UploadedContent[0].Url;
                uc.UploadType = backendCardObject.Options[j].UploadedContent[0].UploadType;
                uc.Ordering = backendCardObject.Options[j].UploadedContent[0].Ordering;
                op.UploadedContent = uc;
            }
            card.Options.push(op);
        }
    }
    else if (card.CardType == CARD_TYPE_SLIDES) {
        card.SlidesTitle = backendCardObject.Content;
        card.SlidesDescription = backendCardObject.Description;
        if (backendCardObject.HasUploadedContent) {
            var uc = new UploadContent();
            uc.UploadId = backendCardObject.UploadedContent[0].UploadId;
            uc.Url = backendCardObject.UploadedContent[0].Url;
            uc.UploadType = backendCardObject.UploadedContent[0].UploadType;
            uc.Ordering = backendCardObject.UploadedContent[0].Ordering;
            card.SlidesContent = uc;
        }
    }
    else {
        // do nothing
    }
    return card;
}

function displayCardList(cards) {
    $("#cardList").html("");
    for (var i = 0; i < cards.length; i++) {
        $("#cardList").append(getMiniCardHtml(cards[i]));
    }
}

function showTip(type) {
    switch (type) {
        case 1:
            $(".form__row .duration_tip").css('display', 'block');
            $(".form__row .duration_tip").css('opacity', '1');
            break;
        default:
            break;
    }
}

function hideTip(type) {
    switch (type) {
        case 1:
            $(".form__row .duration_tip").css('display', 'none');
            $(".form__row .duration_tip").css('opacity', '0');
            break;
        default:
            break;
    }
}

function rebindJs() {
    $("#main_content_tbStartDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd/mm/yy'
    });

    $("#main_content_tbEndDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd/mm/yy'
    });
}

function getCard(cardId) {
    for (var i = 0; i < exam.Cards.length; i++) {
        if (exam.Cards[i].CardId === cardId) {
            return exam.Cards[i];
        }
    }
}

function updateCard(card, needUpdateUI) {
    for (var i = 0; i < exam.Cards.length; i++) {
        if (exam.Cards[i].CardId === card.CardId) {
            exam.Cards[i] = card;
        }
    }

    // update Item-UI
    if (needUpdateUI) {
        if (card.CardId.indexOf("undefined") == 0) {
            $("#newCard").html(getFullCardHtml(card));
        }
        else {
            var pageBreakLayout = $(".pagebreak." + card.CardId);
            pageBreakLayout.remove();

            var cardLayout = $(".card.full-card." + card.CardId);
            cardLayout.after(getFullCardHtml(card));
            cardLayout.remove();

            turnOffFeatures();
        }
    }

    // update Preview-UI
    if (card.CardType == CARD_TYPE_VIDEO) {
        $(".preview." + card.CardId + " .previewVideoTitlte").html(card.VideoTitle);
        $(".preview." + card.CardId + " .previewVideoDescription").html(card.VideoDescription);
    }
    else if (card.CardType == CARD_TYPE_SLIDES) {
        $(".preview." + card.CardId + " .previewSlidesTitlte").html(card.SlidesTitle);
        $(".preview." + card.CardId + " .previewSlidesDescription").html(card.SlidesDescription);
    }
    else {
        $(".card." + card.CardId + " .preview").html(getPreviewHtml(card));
    }
}

function addNewCard() {
    var card = new Card($("#main_content_hfExamId").val());
    if (exam.Cards == null || exam.Cards.length == 0) {
        card.Paging = 1;
        card.Ordering = 1;
    }
    else {
        card.Paging = exam.Cards[exam.Cards.length - 1].Paging;
        card.Ordering = exam.Cards[exam.Cards.length - 1].Ordering + 1;

        if (exam.Cards[exam.Cards.length - 1].HasPageBreak) {
            card.Paging = exam.Cards[exam.Cards.length - 1].Paging + 1;
        }
    }
    card.HasPageBreak = true;
    card.ArticlePages.push(new PageContent());
    exam.Cards.push(card);
    $("#newCard").html(getFullCardHtml(card));
    $("#btnAddNewCard").css("visibility", "hidden");
}

function removeNewCard(cardId) {
    var index = -1;
    for (var i = 0; i < exam.Cards.length; i++) {
        if (exam.Cards[i].CardId === cardId) {
            index = i;
            break;
        }
    }

    if (index > -1) {
        exam.Cards.splice(index, 1);
        $("#newCard").html("");
        $("#btnAddNewCard").css("visibility", "visible");
    }
}

function changeCardType(cardId, cardType) {
    try {
        var card = getCard(cardId);

        card.CardType = cardType;
        if (cardType == CARD_TYPE_ARTICLE && (card.ArticlePages == null || card.ArticlePages.length == 0)) {
            card.ArticlePages = [];
            card.ArticlePages.push(new PageContent());
        }
        else if ((cardType == CARD_TYPE_SELECTONE || cardType == CARD_TYPE_MULTICHOICE) && (card.Options == null || card.Options.length < 2)) {
            card.Options = [];
            for (var i = card.Options.length; i < 2; i++) {
                card.Options.push(new Option());
            }
        }
        else if (cardType == CARD_TYPE_SLIDES && card.SlidesContent == null) {
            var uc = new UploadContent();
            uc.UploadType = UPLOAD_FILE_TYPE_SLIDESHARE;
            uc.Url = "";
            uc.IsNewFile = true;
            card.SlidesContent = uc;
        }
        else {
            // do nothing
        }

        updateCard(card, true);

        // for Silde
        if (card.CardType == CARD_TYPE_SLIDES && card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_LANDSCAPE) {
            if (card.SlidesContent.IsNewFile) {
                renderPDF(card.SlidesContent.Base64, card.CardId, true);
            }
            else {
                renderPDF(card.SlidesContent.Url, card.CardId, false);
            }
        }
        else if (card.CardType == CARD_TYPE_SLIDES && card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_PORTRAIT) {
            if (card.SlidesContent.IsNewFile) {
                showPDF(card.SlidesContent.Base64, card.CardId, true);
            }
            else {
                showPDF(card.SlidesContent.Url, card.CardId, false);
            }
        }
        else {
            // do nothing
        }

    } catch (e) {
        console.error(e.toString());
    }
}

function addArticlePage(cardId) {
    try {
        var card = getCard(cardId);
        card.ArticlePages.push(new PageContent());
        updateCard(card, true);
    } catch (e) {
        console.error(e.toString());
    }
}

function removeArticlePage(cardId, pageIndex) {
    try {
        var card = getCard(cardId);
        if (card.ArticlePages.length > 1) {
            card.ArticlePages.splice(pageIndex, 1);
            updateCard(card, true);
        }
    } catch (e) {
        console.error(e.toString());
    }
}

function playVideo(element) {
    var videoPlay = $(element).parents(".video_content").find(".video_player")[0];
    videoPlay.play();
    $(element).hide();
}

function videoEnded(element) {
    $(element).parents(".video_content").find(".video_play_btn").show();
}

function addOption(cardId) {
    try {
        var card = getCard(cardId);
        card.Options.push(new Option());
        updateCard(card, true);
    } catch (e) {
        console.error(e.toString());
    }
}

function removeOption(cardId, optionIndex) {
    try {
        var card = getCard(cardId);
        if (card.Options.length > 1) {
            card.Options.splice(optionIndex, 1);
            updateCard(card, true);
        }
    } catch (e) {
        console.error(e.toString());
    }
}

function shrinkToMiniCard(cardId) {
    var card = getCard(cardId);
    var pageBreakLayout = $(".pagebreak." + cardId);
    pageBreakLayout.remove();

    var fullCardLayout = $(".card.full-card." + cardId);
    fullCardLayout.after(getMiniCardHtml(card));
    fullCardLayout.remove();
}

function isCardValid(card) {
    if (exam.Status > EXAM_STATUS_UNLISTED) {
        if (exam.StartDate.isSameOrBefore(moment.utc())) {
            ShowToast('This evaluation is in progress or completed. It cannot be edited.', 2);
            return false;
        }
    }

    if (card.CardType == CARD_TYPE_ARTICLE) {
        // Article Page
        if (card.ArticlePages == null || card.ArticlePages.length == 0) {
            ShowToast('Please full in an Article', 2);
            return false;
        }

        for (var i = 0; i < card.ArticlePages.length; i++) {
            // Article Page Title
            if (card.ArticlePages[i].Title == null || card.ArticlePages[i].Title.length == 0) {
                ShowToast("Please fill in Page " + (i + 1) + "'s Title", 2);
                return false;
            }

            if (card.ArticlePages[i].Title.length > TEXT_MAX_ARTICLE_TITLE) {
                ShowToast("Page " + (i + 1) + "'s Title is longer than " + TEXT_MAX_ARTICLE_TITLE + " letters", 2);
                return false;
            }

            // Article Page Content
            if (card.ArticlePages[i].Content == null || card.ArticlePages[i].Content.length == 0) {
                ShowToast("Please fill in Page " + (i + 1) + "'s Content", 2);
                return false;
            }
        }
    }
    else if (card.CardType == CARD_TYPE_VIDEO) {
        // Video Title
        if (card.VideoTitle == null || card.VideoTitle.length == 0) {
            ShowToast('Please give this video a title', 2);
            return false;
        }

        if (card.VideoTitle.length > TEXT_MAX_VIDEO_TITLE) {
            ShowToast('Video title is longer than ' + TEXT_MAX_VIDEO_TITLE + ' letters', 2);
            return false;
        }

        // Video Description
        // description is not compulsory. 2017/02/10
        //if (card.VideoDescription == null || card.VideoDescription.length == 0) {
        //    ShowToast('Please give this video a description', 2);
        //    return false;
        //}

        if (card.VideoDescription.length > TEXT_MAX_VIDEO_DESCRIPTION) {
            ShowToast('Video description is longer than ' + TEXT_MAX_VIDEO_DESCRIPTION + ' letters ', 2);
            return false;
        }

        // Video File
        if (card.VideoContent == null || card.VideoContent.Url == null || card.VideoContent.Url.length == 0) {
            ShowToast('Please import a video', 2);
            return false;
        }
    }
    else if (card.CardType == CARD_TYPE_INSTRUCTIONS) {
        // Instructions Text
        if (card.InstructionsText == null || card.InstructionsText.length == 0) {
            ShowToast('Your instruction textbox is empty', 2);
            return false;
        }

        if (card.InstructionsText.length > TEXT_MAX_INSTRUCTIONS) {
            ShowToast('Instruction is longer than ' + TEXT_MAX_INSTRUCTIONS + ' letters ', 2);
            return false;
        }
    }
    else if (card.CardType == CARD_TYPE_SELECTONE || card.CardType == CARD_TYPE_MULTICHOICE) {
        // Question
        if (card.QuestionText == null || card.QuestionText.length == 0) {
            ShowToast('Please fill in your Question', 2);
            return false;
        }

        if (card.QuestionText.length > TEXT_MAX_QUESTION) {
            ShowToast('Your Question is longer than ' + TEXT_MAX_QUESTION + ' letters', 2);
            return false;
        }

        // Option
        if (card.Options == null || card.Options.length < 2) {
            ShowToast('You will need two or more options filled', 2);
            return false;
        }

        var hasScore = false;
        var correctCount = 0;
        for (var i = 0; i < card.Options.length; i++) {
            // Option Text
            if (card.Options[i].Content == null || card.Options[i].Content.length == 0) {
                ShowToast('You have an option empty', 2);
                return false;
            }

            if (card.Options[i].Content.length > TEXT_MAX_OPTION) {
                ShowToast('Your option is longer than ' + TEXT_MAX_OPTION + ' letter', 2);
                return false;
            }

            if (card.Options[i].Score > 0) {
                correctCount++;
                if (!hasScore) {
                    hasScore = true;
                }
            }
        }

        if (card.CardType == CARD_TYPE_SELECTONE) {
            for (var i = 1; i < card.Options.length; i++) {
                card.Options[i].Score = 0;
            }
        }

        if (card.CardType == CARD_TYPE_MULTICHOICE) {
            // Option Score
            if (!hasScore) {
                ShowToast('Please assign a score for one or more options', 2);
                return false;
            }

            // Minimum of options to be selected
            if (card.MinOptionToSelect == undefined || card.MinOptionToSelect < 1 || card.MinOptionToSelect > correctCount) {
                ShowToast('Please set a positive value, and lower or equal to ' + correctCount + ' for Minimum Value', 2);
                return false;
            }

            // Maximum of options to be selected
            if (card.MaxOptionToSelect == undefined || card.MaxOptionToSelect > card.Options.length || card.MaxOptionToSelect < card.MinOptionToSelect || card.MaxOptionToSelect < correctCount) {
                ShowToast('Maximum value should be between ' + correctCount + ' and ' + card.Options.length, 2);
                return false;
            }
        }
    }
    else if (card.CardType == CARD_TYPE_SLIDES) {
        // Slides Title
        if (card.SlidesTitle == null || card.SlidesTitle.length == 0) {
            ShowToast('Please give this slides a title', 2);
            return false;
        }

        if (card.SlidesTitle.length > TEXT_MAX_SLIDES_TITLE) {
            ShowToast('Slides title is longer than ' + TEXT_MAX_SLIDES_TITLE + ' letters', 2);
            return false;
        }

        // Slides Description
        // description is not compulsory. 2017/02/10
        //if (card.SlidesDescription == null || card.SlidesDescription.length == 0) {
        //    ShowToast('Please give this slides a description', 2);
        //    return false;
        //}

        if (card.SlidesDescription.length > TEXT_MAX_SLIDES_DESCRIPTION) {
            ShowToast('Slides description is longer than ' + TEXT_MAX_SLIDES_DESCRIPTION + ' letters ', 2);
            return false;
        }

        // Slides File
        if (card.SlidesContent == null) {
            ShowToast('Please add a content for this type', 2);
            return false;
        }

        if (card.SlidesContent.IsNewFile) {
            if (card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_SLIDESHARE) {
                if (card.SlidesContent.Url == null || card.SlidesContent.Url == "") {
                    ShowToast('Please enter a valid SlideShare Link', 2);
                    return false;
                }

                if (card.SlidesContent.Url.indexOf("http://www.slideshare.net/slideshow/embed_code/key/") < 0) {
                    ShowToast('Please enter a valid SlideShare Link', 2);
                    return false;
                }
            }
            else {
                if (card.SlidesContent == null || card.SlidesContent.Base64 == null || card.SlidesContent.Base64.length == 0) {
                    ShowToast('Please import a PDF file', 2);
                    return false;
                }
            }
        }
    }
    else {
        // do nothing
    }
    return true;
}

function setCardValue(cardId, attrType, element, arg) {
    try {
        var card = getCard(cardId);
        switch (attrType) {
            case 1: // attrType: 1 => Card.ArticlePages[i].Title
                card.ArticlePages[arg].Title = element.value;
                updateCard(card, false);
                break;

            case 2: // attrType: 2 => Card.ArticlePages[i].Content
                card.ArticlePages[arg].Content = element.value;
                updateCard(card, false);
                break;

            case 3: // attrType: 3 => Card.VideoTitle
                card.VideoTitle = element.value;
                updateCard(card, false);
                break;

            case 4: // attrType: 4 => Card.VideoDescription
                card.VideoDescription = element.value;
                updateCard(card, false);
                break;

            case 5: // attrType: 5 => Card.VideoContent. Add
                if (element.files && element.files[0]) {
                    var file = element.files[0];
                    if (file.size < (100 * 1024 * 1024 + 1)) // File size max : 100 MB
                    {
                        if (file.type.toLowerCase() == "video/mp4") {
                            var fileReader = new FileReader();
                            fileReader.onload = function (e) {
                                try {
                                    if (e.target.result == null || e.target.result == undefined) {
                                        ShowToast("This file cannot be uploaded through IE, try using another browsers.", 2);
                                    }
                                    else {
                                        var videoContent = new UploadContent();
                                        videoContent.IsNewFile = true;
                                        videoContent.UploadType = parseInt($("input[name=" + cardId + "_VIDEO_MODE]:checked").val());
                                        videoContent.Base64 = e.target.result;
                                        videoContent.Url = window.URL.createObjectURL(file);
                                        videoContent.Extension = file.type;
                                        card.VideoContent = videoContent;

                                        updateCard(card, true);
                                    }
                                } catch (e) {
                                    console.error(e.toString());
                                }
                            }
                            fileReader.readAsDataURL(file);
                        }
                        else {
                            ShowToast("Supported format: *.mp4", 2);
                        }
                    }
                    else {
                        ShowToast("Video supports up to 100MB", 2);
                    }
                }
                else {
                    console.info("No file be selected.");
                }
                break;

            case 6: // attrType: 6 => Card.VideoContent. Remove
                card.VideoContent = null;
                card.HasUploadedContent = false;
                updateCard(card, true);
                break;

            case 7: // attrType: 7 => Card.InstructionsText
                card.InstructionsText = element.value;
                updateCard(card, false);
                break;

            case 8: // attrType: 8 => Card.InstructionsImg. Add
                if (element.files && element.files[0]) {
                    var file = element.files[0];
                    if (file.type.toLowerCase() == "image/png" || file.type.toLowerCase() == "image/jpeg" || file.type.toLowerCase() == "image/jpg") {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                            var image = new Image();
                            image.src = e.target.result;
                            image.onload = function () {
                                var imageContent = new UploadContent();
                                imageContent.IsNewFile = true;
                                imageContent.UploadType = UPLOAD_FILE_TYPE_IMAGE;
                                imageContent.Base64 = e.target.result;
                                imageContent.Url = window.URL.createObjectURL(file);
                                imageContent.Width = this.width;
                                imageContent.Height = this.height;
                                imageContent.Extension = file.type;
                                card.InstructionsImg = imageContent;
                                card.HasUploadedContent = true;

                                updateCard(card, true);
                            };
                        }

                        fileReader.readAsDataURL(file);
                    }
                    else {
                        // 不允許的格式
                        ShowToast("Supported format: *.png, *.jpg", 2);
                    }
                }
                else {
                    console.info("No file be selected.");
                }
                break;

            case 9: // attrType: 9 => Card.InstructionsImg. Remove
                card.InstructionsImg = null;
                card.HasUploadedContent = false;
                updateCard(card, true);
                break;

            case 10: // attrType: 10 => Card.QuestionText
                card.QuestionText = element.value;
                updateCard(card, false);
                break;

            case 11: // attrType: 11 => Card.QuestionImg. Add
                if (element.files && element.files[0]) {
                    var file = element.files[0];
                    if (file.type.toLowerCase() == "image/png" || file.type.toLowerCase() == "image/jpeg" || file.type.toLowerCase() == "image/jpg") {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                            var image = new Image();
                            image.src = e.target.result;
                            image.onload = function () {
                                var imageContent = new UploadContent();
                                imageContent.IsNewFile = true;
                                imageContent.UploadType = UPLOAD_FILE_TYPE_IMAGE;
                                imageContent.Base64 = e.target.result;
                                imageContent.Url = window.URL.createObjectURL(file);
                                imageContent.Width = this.width;
                                imageContent.Height = this.height;
                                imageContent.Extension = file.type;
                                card.QuestionImg = imageContent;
                                card.HasUploadedContent = true;
                                updateCard(card, true);
                            };
                        }

                        fileReader.readAsDataURL(file);
                    }
                    else {
                        // 不允許的格式
                        ShowToast("Supported format: *.png, *.jpg", 2);
                    }
                }
                else {
                    console.info("No file be selected.");
                }
                break;

            case 12: // attrType: 12 => Card.QuestionImg. Remove
                card.QuestionImg = null;
                card.HasUploadedContent = false;
                updateCard(card, true);
                break;

            case 13: // attrType: 13 => Card.Options[i].Content
                card.Options[arg].Content = element.value;
                updateCard(card, false);
                break;

            case 14: // attrType: 14 => Card.Options[i].UploadedContent. Add
                if (element.files && element.files[0]) {
                    var file = element.files[0];
                    if (file.type.toLowerCase() == "image/png" || file.type.toLowerCase() == "image/jpeg" || file.type.toLowerCase() == "image/jpg") {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                            var image = new Image();
                            image.src = e.target.result;
                            image.onload = function () {
                                var imageContent = new UploadContent();
                                imageContent.IsNewFile = true;
                                imageContent.UploadType = UPLOAD_FILE_TYPE_IMAGE;
                                imageContent.Base64 = e.target.result;
                                imageContent.Url = window.URL.createObjectURL(file);
                                imageContent.Width = this.width;
                                imageContent.Height = this.height;
                                imageContent.Extension = file.type;
                                card.Options[arg].UploadedContent = imageContent;
                                card.Options[arg].HasUploadedContent = true;
                                updateCard(card, true);
                            };
                        }

                        fileReader.readAsDataURL(file);
                    }
                    else {
                        // 不允許的格式
                        ShowToast("Supported format: *.png, *.jpg", 2);
                    }
                }
                else {
                    console.info("No file be selected.");
                }
                break;

            case 15: // attrType: 15 => Card.Options[i].UploadedContent. Remove
                card.Options[arg].UploadedContent = null;
                card.Options[arg].HasUploadedContent = false;
                updateCard(card, true);
                break;

            case 16: // attrType: 16 => Card.Options[i].Score
                card.Options[arg].Score = parseInt(element.value);
                updateCard(card, true);
                break;

            case 17: // attrType: 17 => Card.MinOptionToSelect
                card.MinOptionToSelect = parseInt(element.value);
                updateCard(card, false);
                break;

            case 18: // attrType: 18 => Card.MaxOptionToSelect
                card.MaxOptionToSelect = parseInt(element.value);
                updateCard(card, false);
                break;

            case 19: // attrType: 19 => Card.Note
                card.Note = element.value;
                updateCard(card, false);
                break;

            case 20: // attrType: 20 => Card.IsOptionRandomized
                card.IsOptionRandomized = element.checked;
                updateCard(card, false);
                break;

            case 21: // attrType: 21 => Card.HasPageBreak
                card.HasPageBreak = element.checked;
                updateCard(card, true);
                break;

            case 22: // attrType: 22 => Card.SlidesTitle
                card.SlidesTitle = element.value;
                updateCard(card, false);
                break;

            case 23: // attrType: 23 => Card.SlidesDescription
                card.SlidesDescription = element.value;
                updateCard(card, false);
                break;

            case 24: // attrType: 24 => Card.SlidesContent. Add
                if (element.files && element.files[0]) {
                    var file = element.files[0];
                    if (file.type.toLowerCase() == "application/pdf") {
                        var fileReader = new FileReader();
                        fileReader.onload = function (e) {
                            var uc = new UploadContent();
                            uc.IsNewFile = true;
                            uc.UploadType = arg;
                            uc.Base64 = e.target.result;
                            uc.Extension = file.type;
                            card.SlidesContent = uc;
                            updateCard(card, true);

                            if (arg == UPLOAD_FILE_TYPE_PDF_PORTRAIT) {
                                showPDF(uc.Base64, card.CardId, true);
                            }
                            else if (arg == UPLOAD_FILE_TYPE_PDF_LANDSCAPE) {
                                renderPDF(uc.Base64, card.CardId, true);
                            }
                            else {
                                // do nothing
                            }
                        }

                        fileReader.readAsDataURL(file);
                    }
                    else {
                        ShowToast("Supported format: *.pdf", 2);
                    }
                }
                else {
                    console.info("No file be selected.");
                }
                break;

            case 25: // attrType: 25 => Card.SlideShareUrl
                if (element.value != null && element.value != "") {
                    if (element.value.indexOf("www.slideshare.net/secret/") < 0 && element.value.indexOf("www.slideshare.net/slideshow/embed_code/") < 0) {
                        ShowToast("Please enter a valid SlideShare Link", 2);
                    }
                    else {
                        var uc = new UploadContent();
                        uc.IsNewFile = true;
                        uc.UploadType = UPLOAD_FILE_TYPE_SLIDESHARE;
                        uc.Url = "http://www.slideshare.net/slideshow/embed_code/key/" + element.value.substring((element.value.lastIndexOf("/") + 1), element.value.length);
                        card.SlidesContent = uc;
                        updateCard(card, true);
                    }
                }
                break;

            case 26: // attrType: 26 => Card.IsUsingSlideShare
                var uc = new UploadContent();
                uc.UploadType = UPLOAD_FILE_TYPE_SLIDESHARE;
                card.SlidesContent = uc;
                updateCard(card, true);
                break;

            case 27: // attrType: 27 => Card.VideoContent.UploadType
                if (card.VideoContent == null || card.VideoContent == undefined) // Add mode
                {
                    var videoContent = new UploadContent();
                    videoContent.UploadType = parseInt($("input[name=" + cardId + "_VIDEO_MODE]:checked").val());
                    card.VideoContent = videoContent;
                }
                else // Edit mode
                {
                    card.VideoContent.UploadType = parseInt($("input[name=" + cardId + "_VIDEO_MODE]:checked").val());
                }
                break;

            default:
                break;
        }
    } catch (e) {
        console.error(e.toString());
    }
}

function getFullCardHtml(card) {
    var html = "";
    if (card.CardId.indexOf("undefined") == 0) {
        html += "           <div class=\"card add-question " + card.CardId + " \">";
    }
    else {
        html += "           <div class=\"card full-card " + card.CardId + " cansort ui-sortable-handle" + card.CardId + " \">";
    }
    html += "               <div class=\"add-question__questionnumber\">";
    html += "                   <span class=\"number\" style=\"color: rgba(204, 204, 204, 1);\">";
    html += "                       P" + card.Paging;
    html += "                   </span>";
    html += "                   <hr style=\"height: 2px; border: none; color: rgba(204, 204, 204, 1); background-color: rgba(204, 204, 204, 1);\" />";
    html += "                   <span class=\"number\" style=\"color: rgba(204, 204, 204, 1);\">";
    html += "                       Q" + card.Ordering;
    html += "                   </span>";
    html += "               </div>";
    html += "               <div class=\"add-question__info\">";
    html += "                   <div class=\"add-question__info__header\">";
    html += "                       <div class=\"add-question__info__header__action\">";
    if (card.CardId.indexOf("undefined") == 0) {
        html += "                           <a class=\"btn secondary\" href=\"javascript:removeNewCard('" + card.CardId + "');\">Cancel</a>";
        html += "                           <a class=\"btn\" href=\"javascript:createCard('" + card.CardId + "');\">Create</a>";
    }
    else {
        html += "                               <a href=\"javascript:shrinkToMiniCard('" + card.CardId + "');\" class=\"grid-question-expandable\" style=\"marge: 10px;\">";
        html += "                                   <i class=\"fa fa-caret-square-o-up fa-lg icon\" style=\"font-size: 1.8em;\"></i>";
        html += "                               </a>";

        if (!(exam.Status > EXAM_STATUS_UNLISTED && exam.ProgressStatus != EXAM_PROGRESS_STATUS_UPCOMING)) {
            html += "                           <a class=\"btn\" href=\"javascript:editCard('" + card.CardId + "');\">Apply Change</a>";
        }
    }

    html += "                         </div>";
    html += "                         <div class=\"add-question__info__header__title\" style=\"font-weight: bold;\">Type</div>";
    html += "                   </div>";
    html += "                   <div class=\"add-question__info__setup\">";
    html += "                       <div class=\"add-question__info__setup__questiontype\" style=\"width: 42%; margin-right: 1%;\">";
    html += "                           <div class=\"tabs--questionformat\">";
    html += "                               <div class=\"tabs__list\">";
    html += "                                   <div class=\"tabs__list__item\" style=\"margin: 0px 15px 10px 0px; cursor: pointer; font-size: 1.15em; padding: 5px 10px; border-radius: 5px; text-align: left; width: 30%;" + ((card.CardType == CARD_TYPE_ARTICLE) ? " color: rgb(242, 244, 247); background: rgb(0, 117, 255);" : " color: rgb(204, 204, 204);") + "\" onclick=\"changeCardType('" + card.CardId + "', " + CARD_TYPE_ARTICLE + ");\">";
    html += "                                       <i class=\"fa fa-file-text-o\" aria-hidden=\"true\"></i>";
    html += "                                       <label style=\"margin: 0px; display: inline-block;\">Article</label>";
    html += "                                   </div>";
    html += "                                   <div class=\"tabs__list__item\" style=\"margin: 0px 15px 10px 0px; cursor: pointer; font-size: 1.15em; padding: 5px 10px; border-radius: 5px; text-align: left; width: 30%;" + ((card.CardType == CARD_TYPE_VIDEO) ? " color: rgb(242, 244, 247); background: rgb(0, 117, 255);" : " color: rgb(204, 204, 204);") + "\" onclick=\"changeCardType('" + card.CardId + "', " + CARD_TYPE_VIDEO + ");\">";
    html += "                                       <i class=\"fa fa-youtube-play\" aria-hidden=\"true\"></i>";
    html += "                                       <label style=\"margin: 0px; display: inline-block;\">Video</label>";
    html += "                                   </div>";
    html += "                                   <div class=\"tabs__list__item\" style=\"margin: 0px 15px 10px 0px; cursor: pointer; font-size: 1.15em; padding: 5px 10px; border-radius: 5px; text-align: left; width: 30%;" + ((card.CardType == CARD_TYPE_SLIDES) ? " color: rgb(242, 244, 247); background: rgb(0, 117, 255);" : " color: rgb(204, 204, 204);") + "\" onclick=\"changeCardType('" + card.CardId + "', " + CARD_TYPE_SLIDES + ");\">";
    html += "                                       <i class=\"fa fa-clone\" aria-hidden=\"true\"></i>";
    html += "                                       <label style=\"margin: 0px; display: inline-block;\">PDF & Slides</label>";
    html += "                                   </div>";
    html += "                                   <div class=\"tabs__list__item\" style=\"margin: 0px 15px 10px 0px; cursor: pointer; font-size: 1.15em; padding: 5px 10px; border-radius: 5px; text-align: left; width: 30%;" + ((card.CardType == CARD_TYPE_INSTRUCTIONS) ? " color: rgb(242, 244, 247); background: rgb(0, 117, 255);" : " color: rgb(204, 204, 204);") + "\" onclick=\"changeCardType('" + card.CardId + "', " + CARD_TYPE_INSTRUCTIONS + ");\">";
    html += "                                       <i class=\"fa fa-info-circle\" aria-hidden=\"true\"></i>";
    html += "                                       <label style=\"margin: 0px; display: inline-block;\">Instructions</label>";
    html += "                                   </div>";
    html += "                                   <div class=\"tabs__list__item\" style=\"margin: 0px 15px 10px 0px; cursor: pointer; font-size: 1.15em; padding: 5px 10px; border-radius: 5px; text-align: left; width: 30%;" + ((card.CardType == CARD_TYPE_SELECTONE) ? " color: rgb(242, 244, 247); background: rgb(0, 117, 255);" : " color: rgb(204, 204, 204);") + "\" onclick=\"changeCardType('" + card.CardId + "', " + CARD_TYPE_SELECTONE + ");\">";
    html += "                                       <i class=\"fa fa-dot-circle-o\" aria-hidden=\"true\"></i>";
    html += "                                       <label style=\"margin: 0px; display: inline-block;\">Select one</label>";
    html += "                                   </div>";
    html += "                                   <div class=\"tabs__list__item\" style=\"margin: 0px 15px 10px 0px; cursor: pointer; font-size: 1.15em; padding: 5px 10px; border-radius: 5px; text-align: left; width: 30%;" + ((card.CardType == CARD_TYPE_MULTICHOICE) ? " color: rgb(242, 244, 247); background: rgb(0, 117, 255);" : " color: rgb(204, 204, 204);") + "\" onclick=\"changeCardType('" + card.CardId + "', " + CARD_TYPE_MULTICHOICE + ");\">";
    html += "                                       <i class=\"fa fa-check-square\" aria-hidden=\"true\"></i>";
    html += "                                       <label style=\"margin: 0px; display: inline-block;\">Multi choice</label>";
    html += "                                   </div>";
    html += "                               </div>";
    html += "                               <div class=\"tabs__panels\">";
    html += "                                   <div class=\"tabs__panels__item tabs__panels__item\">";
    html += getCardItemHtml(card);
    html += "                                   </div>";
    html += "                               </div>";
    html += "                           </div>";
    html += "                       </div>";
    html += "                       <div class=\"add-question__info__setup__preview preview\" style=\"width: 32%;\">";
    html += getPreviewHtml(card);
    html += "                       </div>";
    html += "                       <div class=\"add-question__info__setup__choice\" style=\"width: 22%;\">";
    html += "                           <div class=\"card_behaviour\" " + ((card.CardType == CARD_TYPE_MULTICHOICE) ? "" : " style=\"display: none;\"") + ">";
    html += "                               <label style=\"font-weight: bold;\">Behaviour</label>";
    html += "                               <label class=\"setting-checkbox\">";
    html += "                                   <input class=\"multichoice-behaviour-randomize\" type=\"checkbox\" onchange=\"setCardValue('" + card.CardId + "', 20, this, null);\" " + ((card.IsOptionRandomized) ? "checked" : "") + " />";
    html += "                                   <span>Randomize option order</span>";
    html += "                               </label>";
    html += "                               <div class=\"clear-float\"></div>";
    html += "                               <br />";
    html += "                           </div>";
    html += "                           <div class=\"card_multichoice_setting\" " + ((card.CardType == CARD_TYPE_MULTICHOICE) ? "" : " style=\"display: none;\"") + ">";
    html += "                               <label style=\"font-weight: bold;\">No of options to be selected</label>";
    html += "                               <span>Minimum:</span>";
    html += "                               <input type=\"number\" class=\"multichoice-seleted-min\" style=\"width: auto; display: inline;\" onblur=\"setCardValue('" + card.CardId + "', 17, this, null);\" value=\"" + card.MinOptionToSelect + "\" /><br />";
    html += "                               <span>Maximum:</span>";
    html += "                               <input type=\"number\" class=\"multichoice-seleted-max\" style=\"width: auto; display: inline;\" onblur=\"setCardValue('" + card.CardId + "', 18, this, null);\" value=\"" + card.MaxOptionToSelect + "\" /><br />";
    html += "                               <div class=\"clear-float\"></div>";
    html += "                               <br />";
    html += "                           </div>";
    html += "                           <div style=\"display: none;\">";
    html += "                               <label style=\"font-weight: bold;\">Display</label>";
    html += "                               <div>";
    html += "                                   <label>Background</label>";
    html += "                                   <!-- Background -->";
    html += "                                   <div class=\"imageslider\" style=\"margin: 10px auto auto; width: 85%;\">";
    html += "                                       <div id=\"slider\">";
    html += "                                           <!-- Slider Setup -->";
    html += "                                           <input name=\"slider\" id=\"slide1\" type=\"radio\" checked=\"\" selected=\"false\" />";
    html += "                                           <input name=\"slider\" id=\"slide2\" type=\"radio\" selected=\"false\" />";
    html += "                                           <input name=\"slider\" id=\"slide3\" type=\"radio\" selected=\"false\" />";
    html += "                                           <!-- / Slider Setup -->";
    html += "                                           <!-- The Slider -->";
    html += "                                           <div id=\"slides\" style=\"margin-top: 0px;\">";
    html += "                                               <div id=\"overflow\">";
    html += "                                                   <div class=\"inner\">";
    html += "                                                       <article>";
    html += "                                                           <div class=\"info\"></div>";
    html += "                                                           <img src=\"/Img/bg_survey_1a_small.png\" style=\"visibility: hidden;\" />";
    html += "                                                       </article>";
    html += "                                                       <article>";
    html += "                                                           <div class=\"info\"></div>";
    html += "                                                           <img src=\"/Img/bg_survey_2a_small.png\" />";
    html += "                                                       </article>";
    html += "                                                       <article>";
    html += "                                                           <div class=\"info\"></div>";
    html += "                                                           <img src=\"/Img/bg_survey_3a_small.png\" />";
    html += "                                                       </article>";
    html += "                                                   </div>";
    html += "                                               </div>";
    html += "                                           </div>";
    html += "                                           <!-- / The Slider -->";
    html += "                                           <!-- Controls and Active Slide Display -->";
    html += "                                           <div class=\"controls\" id=\"controls\" style=\"top: 38%; display: none;\">";
    html += "                                               <label for=\"slide1\"></label>";
    html += "                                               <label for=\"slide2\"></label>";
    html += "                                               <label for=\"slide3\"></label>";
    html += "                                           </div>";
    html += "                                           <!-- / Controls and Active Slide Display -->";
    html += "                                       </div>";
    html += "                                   </div>";
    html += "                                   <!-- Background -->";
    html += "                                   <br />";
    html += "                               </div>";
    html += "                           </div>";
    html += "                           <div style=\"display: none;\">";
    html += "                               <label style=\"font-weight: bold;\">Notes</label>";
    html += "                               <input type=\"text\" maxlength=\"150\" placeholder=\"Enter notes\" onkeyup=\"setCardValue('" + card.CardId + "', 19, this, null);\" value=\"" + card.Note + "\" />";
    html += "                               <span style=\"color: rgb(151, 151, 151);\">Notes will be seen by user during the survey via the</span>";
    html += "                               <img src=\"/Img/icon_note_small.png\" />";
    html += "                               <span style=\"color: rgb(151, 151, 151);\">icon</span>";
    html += "                               <br />";
    html += "                           </div>";
    html += "                           <p></p>";
    html += "                           <div>";
    html += "                               <label class=\"setting-checkbox\">";
    html += "                                   <input class=\"card-setting-pagebreak\" type=\"checkbox\"  onchange=\"setCardValue('" + card.CardId + "', 21, this, null);\" " + ((card.HasPageBreak) ? "checked" : "") + " />";
    html += "                                   <span style=\"font-weight: bolder;\">Page Break</span>";
    html += "                               </label>";
    html += "                               <span style=\"color: rgb(151, 151, 151);\">Page break prevent user from going back to pervious page with a NEXT button</span>";
    html += "                               <div class=\"clear-float\"></div>";
    html += "                           </div>";
    if (!(card.CardId.indexOf("undefined") == 0) && !(exam.Status > EXAM_STATUS_UNLISTED && exam.ProgressStatus != EXAM_PROGRESS_STATUS_UPCOMING)) {
        html += "                           <div style=\"right: 30px; bottom: 30px; margin-top: 50px; display: block; position: absolute;\">";
        html += "                               <a class=\"btn confirm right\" href=\"javascript:actionCard(" + CARD_ACTION_TYPE.SHOW_DELETE + ", '" + card.CardId + "');\">Delete Card</a>";
        html += "                           </div>";
    }
    html += "                       </div>";
    html += "                   </div>";
    html += "               </div>";
    html += "           </div>";
    if (card.CardId.indexOf("undefined") == 0) {
        html += "           <hr style=\"margin-top: 35px; border-bottom-color: rgb(204, 204, 204); border-bottom-width: 2px; border-bottom-style: solid;\" />";
    }

    if (card.HasPageBreak) {
        html += "<div class=\"pagebreak " + card.CardId + "\"><hr style=\"clear: both; border-top-color: rgb(156, 62, 30); border-top-width: 4px; border-top-style: solid;\"></div>";
    }
    return html;
}

function getPreviewHtml(card) {
    var html = "";
    html += "                           <!-- Card Preview Format -->";
    html += "                           <div class=\"preview " + card.CardId + "\" style='overflow: hidden; position: relative;'>";
    if (card.CardType === CARD_TYPE_ARTICLE) {
        html += "                               <div style=\"padding: 20px;\">";
        html += "                                   <Img src=\"/Img/MLearning/icon_preivew_menu.png\" style=\"width: 25px;\">";
        html += "                                   <Img src=\"/Img/MLearning/icon_preivew_aa.png\" style=\"width: 25px; margin-left: 5%;\">";
        html += "                                   <i class=\"fa fa-file-o\" aria-hidden=\"true\" style=\"font-size: 1.5em; margin-left: 5%;\"></i>";
        html += "                                   <span style=\"font-size: 1.4em;\">" + card.Ordering + "/" + exam.Cards.length + "</span>";
        html += "                                   <Img src=\"/Img/MLearning/icon_preivew_more.png\" style=\"top: 30px; width: 25px; right: 25px; position: absolute;\">";
        html += "                               </div>";
        html += "                               <div style=\"overflow-y: scroll; height: 500px;\">";
        html += "                                   <label style=\"margin: 0px 20px; text-align: justify; color: rgb(101, 106, 110); font-family: lato; font-size: 21px; font-style: normal; font-weight: 900; font-stretch: normal; word-break: break-all;\">";
        html += card.ArticlePages[0].Title;
        html += "                                   </label>";
        html += "                                   <label style=\"margin: 10px 20px; text-align: justify; color: rgb(101, 106, 110); font-family: lato; font-size: 21px; font-style: normal; font-weight: 500; font-stretch: normal; word-break: break-all;\">";
        html += card.ArticlePages[0].Content
        html += "                                   </label>";
        html += "                               </div>";
        html += "                               <div style=\"margin: 0px 0px 0px -40%; border-radius: 4px; left: 50%; width: 80%; height: 40px; bottom: 20px; position: absolute; box-shadow: inset 0px 0.5px 1px 0px rgba(255,255,255,0.5); background-color: rgb(118, 181, 255);\">";
        html += "                                   <label style=\"height: 20px; text-align: center; color: rgb(255, 255, 255); font-family: MavenPro; font-size: 20px; text-shadow: 0px 0.5px 0.5px rgba(0,0,0,0.5);\">Next</label>";
        html += "                               </div>";
    }
    else if (card.CardType === CARD_TYPE_VIDEO) {
        html += "                               <div style=\"padding: 20px;\">";
        html += "                                   <div style=\"width: 85%\">";
        html += "                                       <img src=\"" + exam.IconUrl + "\" style=\"width: 45px; vertical-align: text-bottom;\" />";
        html += "                                       <div style=\"display: inline-block; margin-left: 5px;\">";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 17px; font-weight: bold; letter-spacing: 0.3px; text-align: left; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5); margin: 0px;\">" + exam.Title + "</label>";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 12px; letter-spacing: 0.2px; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5);\">By " + exam.Author.FirstName + " " + exam.Author.LastName + "</label>";
        html += "                                       </div>";
        html += "                                   </div>";
        html += "                                   <Img src=\"/Img/MLearning/icon_preivew_more.png\" style=\"top: 30px; width: 25px; right: 25px; position: absolute;\">";
        html += "                                   <div style=\"width: 100%; margin: 5px;\">";
        html += "                                       <i class=\"fa fa-file-o\" aria-hidden=\"true\" style=\"font-size: 1.2em;\"></i>";
        html += "                                       <span style=\"font-size: 1.1em;\">" + card.Ordering + "/" + exam.Cards.length + "</span>";
        html += "                                   </div>";
        html += "                               </div>";
        html += "                               <div style=\"overflow-y: scroll; height: 500px;\">";
        html += "                                   <div style=\"margin: 0px 20px;\">";
        html += "                                       <span class=\"previewVideoTitlte\" style=\"font-family: MavenPro; font-size: 17px; font-weight: bold; letter-spacing: 0.3px; text-align: left; color: black; text-overflow : ellipsis; width: 85%; overflow: hidden; white-space : nowrap; display : inline-block;\">" + card.VideoTitle + "</span>";
        html += "                                   </div>";

        if (card.VideoContent != null && card.VideoContent.Url != null && card.VideoContent.Url.length > 0) {
            html += "                                   <div style=\"margin: 0px 20px;\">";
            html += "                                       <video style=\"padding: 8px; border-radius: 7.5px; border-image: none; width: 100%; border: solid 0.5px #979797;\" src=\"" + card.VideoContent.Url + "\"></video>";
            html += "                                   </div>";
        }
        html += "                                   <label class=\"previewVideoDescription\" style=\"margin: 10px 20px; text-align: justify; color: #656a6e; font-family: lato; font-size: 12px; font-style: normal; font-weight: 500; font-stretch: normal; word-break: break-all; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5); font-family: MavenPro;\">";
        html += card.VideoDescription
        html += "                                   </label>";
        html += "                               </div>";

    }
    else if (card.CardType === CARD_TYPE_INSTRUCTIONS) {
        html += "                               <div style=\"padding: 20px;\">";
        html += "                                   <div style=\"width: 85%\">";
        html += "                                       <img src=\"" + exam.IconUrl + "\" style=\"width: 45px; vertical-align: text-bottom;\" />";
        html += "                                       <div style=\"display: inline-block; margin-left: 5px;\">";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 17px; font-weight: bold; letter-spacing: 0.3px; text-align: left; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5); margin: 0px;\">" + exam.Title + "</label>";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 12px; letter-spacing: 0.2px; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5);\">By " + exam.Author.FirstName + " " + exam.Author.LastName + "</label>";
        html += "                                       </div>";
        html += "                                   </div>";
        html += "                                   <Img src=\"/Img/MLearning/icon_preivew_more.png\" style=\"top: 30px; width: 25px; right: 25px; position: absolute;\">";
        html += "                                   <div style=\"width: 100%; margin: 5px;\">";
        html += "                                       <i class=\"fa fa-file-o\" aria-hidden=\"true\" style=\"font-size: 1.2em;\"></i>";
        html += "                                       <span style=\"font-size: 1.1em;\">" + card.Ordering + "/" + exam.Cards.length + "</span>";
        html += "                                   </div>";
        html += "                               </div>";
        html += "                               <div style=\"padding: 0px 20px;\">";
        html += "                                   <div style=\"overflow-y: auto; height: 450px;\">";
        if (card.InstructionsImg != null && card.InstructionsImg.Url != null && card.InstructionsImg.Url.length > 0) {
            html += "                                   <img src=\"" + card.InstructionsImg.Url + "\" style=\"padding: 8px; border-radius: 7.5px; border-image: none; width: 100%; border: solid 0.5px #979797;\">";
        }
        html += "                                       <label style=\"margin: 0px 20px; text-align: center; color: rgb(101, 106, 110); font-family: lato; font-size: 17px; font-style: normal; font-weight: 900; font-stretch: normal; word-break: break-all;\">";
        html += card.InstructionsText;
        html += "                                       </label>";
        html += "                                </div>";
        html += "                            </div>";
    }
    else if (card.CardType === CARD_TYPE_SELECTONE || card.CardType === CARD_TYPE_MULTICHOICE) {
        html += "                               <div style=\"padding: 20px;\">";
        html += "                                   <div style=\"width: 85%\">";
        html += "                                       <img src=\"" + exam.IconUrl + "\" style=\"width: 45px; vertical-align: text-bottom;\" />";
        html += "                                       <div style=\"display: inline-block; margin-left: 5px;\">";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 17px; font-weight: bold; letter-spacing: 0.3px; text-align: left; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5); margin: 0px;\">" + exam.Title + "</label>";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 12px; letter-spacing: 0.2px; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5);\">By " + exam.Author.FirstName + " " + exam.Author.LastName + "</label>";
        html += "                                       </div>";
        html += "                                   </div>";
        html += "                                   <Img src=\"/Img/MLearning/icon_preivew_more.png\" style=\"top: 30px; width: 25px; right: 25px; position: absolute;\">";
        html += "                                   <div style=\"width: 100%; margin: 5px;\">";
        html += "                                       <i class=\"fa fa-file-o\" aria-hidden=\"true\" style=\"font-size: 1.2em;\"></i>";
        html += "                                       <span style=\"font-size: 1.1em;\">" + card.Ordering + "/" + exam.Cards.length + "</span>";
        html += "                                   </div>";
        html += "                               </div>";
        html += "                               <div style=\"padding: 0px 20px;\">";
        html += "                                   <div style=\"overflow-y: auto; height: 470px;\">";
        if (card.QuestionImg != null && card.QuestionImg.Url != null && card.QuestionImg.Url.length > 0) {
            html += "                                   <img src=\"" + card.QuestionImg.Url + "\" style=\"padding: 8px; border-radius: 7.5px; border-image: none; width: 100%; border: solid 0.5px #979797;\">";
        }
        html += "                                       <label style=\"margin: 0px 20px; text-align: center; color: rgb(101, 106, 110); font-family: lato; font-size: 17px; font-style: normal; font-weight: 900; font-stretch: normal; word-break: break-all;\">";
        html += card.QuestionText;
        html += "                                       </label>";
        for (var i = 0; i < card.Options.length; i++) {
            if ((card.Options[i].Content != null && card.Options[i].Content != "") || (card.Options[i].UploadedContent != null && card.Options[i].UploadedContent.Url != null && card.Options[i].UploadedContent.Url.length > 0)) {
                html += "                                       <div style=\"margin-top: 25px; width: 100%; border-radius: 4px; background-color: rgba(206, 205, 206, 0.1); box-shadow: 0 5px 5px 0 rgba(101, 105, 110, 0.2); border: solid 0.5px #cecdce; display:flex; padding:10px; \">";
                if (card.Options[i].UploadedContent != null && card.Options[i].UploadedContent.Url != null && card.Options[i].UploadedContent.Url.length > 0) {
                    html += "                                           <div style=\"width: 30%; vertical-align : middle;\">";
                    html += "                                               <img src=\"" + card.Options[i].UploadedContent.Url + "\" style=\"width: 100%; height:auto;\" />";
                    html += "                                           </div>";
                }
                html += "                                           <div style=\"width: 70%;\">";
                html += "                                               <label style=\" margin:0px 5px; width: 100%; color: #656a6e; font-family: MavenPro; font-size: 17px; word-break: break-all; \">";
                html += card.Options[i].Content;
                html += "                                               </label>";
                html += "                                           </div>";
                html += "                                       </div>";
            }
        }
        html += "                                </div>";
        html += "                            </div>";
    }
    else if (card.CardType === CARD_TYPE_SLIDES) {
        html += "                               <div style=\"padding: 20px;\">";
        html += "                                   <div style=\"width: 85%\">";
        html += "                                       <img src=\"" + exam.IconUrl + "\" style=\"width: 45px; vertical-align: text-bottom;\" />";
        html += "                                       <div style=\"display: inline-block; margin-left: 5px;\">";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 17px; font-weight: bold; letter-spacing: 0.3px; text-align: left; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5); margin: 0px;\">" + exam.Title + "</label>";
        html += "                                           <label style=\"font-family: MavenPro; font-size: 12px; letter-spacing: 0.2px; color: #656a6e; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5);\">By " + exam.Author.FirstName + " " + exam.Author.LastName + "</label>";
        html += "                                       </div>";
        html += "                                   </div>";
        html += "                                   <Img src=\"/Img/MLearning/icon_preivew_more.png\" style=\"top: 30px; width: 25px; right: 25px; position: absolute;\">";
        html += "                                   <div style=\"width: 100%; margin: 5px;\">";
        html += "                                       <i class=\"fa fa-file-o\" aria-hidden=\"true\" style=\"font-size: 1.2em;\"></i>";
        html += "                                       <span style=\"font-size: 1.1em;\">" + card.Ordering + "/" + exam.Cards.length + "</span>";
        html += "                                   </div>";
        html += "                               </div>";
        html += "                               <div style=\"overflow-y: scroll; height: 600px;\">";
        html += "                                   <div style=\"margin: 0px 20px;\">";
        html += "                                       <span class=\"previewSlidesTitlte\" style=\"font-family: MavenPro; font-size: 17px; font-weight: bold; letter-spacing: 0.3px; text-align: left; color: black; text-overflow : ellipsis; width: 85%; overflow: hidden; white-space : nowrap; display : inline-block;\">" + card.SlidesTitle + "</span>";
        html += "                                   </div>";

        if (card.SlidesContent != null && card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_SLIDESHARE && card.SlidesContent.Url != null && card.SlidesContent.Url.length > 0) {
            html += "                                   <div style=\"margin: 0px 20px;\">";
            html += "                                       <iframe src=\"" + card.SlidesContent.Url + "\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;\" allowfullscreen></iframe>";
            html += "                                   </div>";
        }
        else if (card.SlidesContent != null && (card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_PORTRAIT || card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_LANDSCAPE) && ((card.SlidesContent.Base64 != null && card.SlidesContent.Base64.length > 0) || (card.SlidesContent.Url != null && card.SlidesContent.Url.length > 0))) {
            html += "                                   <div style=\"margin: 0px 20px;\">";
            html += "                                       <div class=\"previewPdfContent\" style=\"width: 100%; height: auto; margin-bottom: 10px;\">";
            html += "                                           <div class=\"perviewPdfCanvas " + card.CardId + "\" style=\"margin: auto;\"></div>";
            html += "                                       </div>";
            html += "                                   </div>";
        }
        else {
            // do nothing
        }
        html += "                                   <label class=\"previewSlidesDescription\" style=\"margin: 10px 20px; text-align: justify; color: #656a6e; font-family: lato; font-size: 12px; font-style: normal; font-weight: 500; font-stretch: normal; word-break: break-all; text-shadow: 0 0.5px 0.5px rgba(0, 0, 0, 0.5); font-family: MavenPro;\">";
        html += card.SlidesDescription
        html += "                                   </label>";
        html += "                               </div>";
    }
    else {
        // do nothing
    }
    html += "                               <div style=\"margin: 0px 0px 0px -40%; border-radius: 4px; left: 50%; width: 80%; height: 40px; bottom: 20px; position: absolute; box-shadow: inset 0px 0.5px 1px 0px rgba(255,255,255,0.5); background-color: rgb(118, 181, 255);\">";
    html += "                                   <label style=\"height: 20px; text-align: center; color: rgb(255, 255, 255); font-family: MavenPro; font-size: 20px; text-shadow: 0px 0.5px 0.5px rgba(0,0,0,0.5);\">";
    if (card.HasPageBreak) {
        html += "Next";
    }
    else {
        html += "<img src=\"/Img/MLearning/icon_preivew_arrow.png\" style=\"width: 60px; vertical-align: bottom;\" />";
    }
    html += "                                   </label>";
    html += "                               </div>";
    html += "                           </div>";
    html += "                           <!-- / Card Preview Format -->";
    return html;
}

function getCardItemHtml(card) {
    var html = "";
    if (card.CardType === CARD_TYPE_ARTICLE) {
        html += "   <!-- Article -->";
        html += "   <div class=\"form__row form__row--question\">";
        html += "       <div class=\"options-container\" style=\"margin: 0px;\">";
        for (var i = 0; i < card.ArticlePages.length; i++) {
            html += "           <label style=\"font-weight: bold; margin-bottom: 5px; display: inline-block;\">Page " + (i + 1) + "</label>";
            if (card.ArticlePages.length != 1) {
                html += "           <a class=\"remove-option-link\"  href=\"javascript:removeArticlePage('" + card.CardId + "', " + i + ");\">- Remove page</a>";
            }
            html += "           <div class=\"text_container\" style=\"border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
            html += "               <input class=\"question-title\" style=\"margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"letterCounter(this, " + TEXT_MAX_ARTICLE_TITLE + "); setCardValue('" + card.CardId + "', 1, this, " + i + ");\" type=\"text\" placeholder=\"Article Title\" value=\"" + card.ArticlePages[i].Title + "\" />";
            html += "               <span class=\"letterCount\" style=\"right: 5px; bottom: 5px; position: absolute;\">" + (TEXT_MAX_ARTICLE_TITLE - card.ArticlePages[i].Title.length) + "</span>";
            html += "           </div>";
            html += "           <div style=\"margin-top: 10px; margin-bottom: 10px; border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
            html += "               <textarea class=\"card-question\" style=\"margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; height: 130px; overflow: hidden; vertical-align: middle;\" placeholder=\"Article Content\" onkeyup=\"setCardValue('" + card.CardId + "', 2, this, " + i + ");\">" + card.ArticlePages[i].Content + "</textarea>";
            html += "               <span class=\"letterCount\" style=\"right: 5px; bottom: 5px; position: absolute; display: none;\">500</span>";
            html += "           </div>";
        }
        html += "       </div>";
        html += "       <br />";
        if (card.ArticlePages.length < 10) {
            html += "       <a class=\"options-container add-option-link\" style=\"float: right;\" href=\"javascript:addArticlePage('" + card.CardId + "');\">+ Add page</a>";
        }
        html += "   </div>";
        html += "   <!-- /Article -->";
    }
    else if (card.CardType === CARD_TYPE_VIDEO) {
        html += "   <!-- Video -->";
        html += "   <div class=\"form__row form__row--question\">";
        html += "       <label style=\"font-weight: bold; margin-bottom: 5px;\">Video</label>";

        html += "       <div style=\"width: 100%; text-align: center; margin-bottom: 10px;\">";
        html += "            <div style=\"width: 40%; display: inline-block;\">";
        html += "                <label style=\"display: inline;\">";
        if (card.VideoContent == null || card.VideoContent.UploadType == UPLOAD_FILE_TYPE_VIDEO_LANDSCAPE) {
            html += "                    <input name=\"" + card.CardId + "_VIDEO_MODE\" class=\"slide-mode\" type=\"radio\" value=\"" + UPLOAD_FILE_TYPE_VIDEO_LANDSCAPE + "\" onchange=\"setCardValue('" + card.CardId + "', 27, this, null);\" checked=\"checked\" />";
        }
        else {
            html += "                    <input name=\"" + card.CardId + "_VIDEO_MODE\" class=\"slide-mode\" type=\"radio\" value=\"" + UPLOAD_FILE_TYPE_VIDEO_LANDSCAPE + "\" onchange=\"setCardValue('" + card.CardId + "', 27, this, null);\"  />";
        }
        html += "                    <span style=\"display: inline-block; \">Landscape</span>";
        html += "                </label>";
        html += "            </div>";
        html += "            <div style=\"width: 40%; display: inline-block; \">";
        html += "                <label style=\"display: inline; \">";
        if (card.VideoContent != null && card.VideoContent != undefined && card.VideoContent.UploadType == UPLOAD_FILE_TYPE_VIDEO_PORTRAIT) {
            html += "                    <input name=\"" + card.CardId + "_VIDEO_MODE\" class=\"slide- mode\" type=\"radio\" value=\"" + UPLOAD_FILE_TYPE_VIDEO_PORTRAIT + "\" onchange=\"setCardValue('" + card.CardId + "', 27, this, null);\" checked=\"checked\" />";
        }
        else {
            html += "                    <input name=\"" + card.CardId + "_VIDEO_MODE\" class=\"slide- mode\" type=\"radio\" value=\"" + UPLOAD_FILE_TYPE_VIDEO_PORTRAIT + "\" onchange=\"setCardValue('" + card.CardId + "', 27, this, null);\" />";
        }
        html += "                    <span style=\"display: inline-block; \">Portrait</span>";
        html += "                </label>";
        html += "            </div>";
        html += "        </div>";

        if (card.VideoContent == null) {
            html += "	    <div style=\"width: 100%; height: auto; display: none; \">";
            html += "		    <video class=\"video_player\" style=\"left: 0px; top: 0px; width: 100%;\" ended=\"videoEnded(this);\"></video>";
        }
        else {
            html += "	    <div class=\"video_content\" style=\"width: 100%; height: auto; \">";
            html += "		    <video class=\"video_player\" style=\"left: 0px; top: 0px; width: 100%;\" onended=\"videoEnded(this);\" src=\"" + card.VideoContent.Url + "\"></video>";
        }

        html += "		    <div class=\"remove-video\" style=\"cursor: pointer; background: rgba(0, 0, 0, 0.6); color: white; padding: 0px 0.5em; top: 60px; text-align: right; right: 10px; vertical-align: top; position: absolute; z-index: 3;\" onclick=\"setCardValue('" + card.CardId + "', 6, this, null);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
        html += "		    <img class=\"video_play_btn\" style=\"cursor: pointer; left: calc(100% - 55%); top: calc(100% - 75%); display: block; position: absolute;\" onclick=\"playVideo(this);\" src=\"/img/Media Play1-WF.gif\" />";
        html += "	    </div>";


        html += "       <div class=\"text_container\" style=\"border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative; marge-top:5px;\">";
        html += "           <input class=\"question-title\" style=\"margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"letterCounter(this, " + TEXT_MAX_VIDEO_TITLE + "); setCardValue('" + card.CardId + "', 3, this, null);\" type=\"text\" placeholder=\"Video Title\" value=\"" + card.VideoTitle + "\" />";
        html += "           <span class=\"letterCount\" style=\"right: 5px; bottom: 5px; position: absolute;\">" + (TEXT_MAX_VIDEO_TITLE - card.VideoTitle.length) + "</span>";
        html += "       </div>";
        html += "       <label style=\"display:inline\">";
        html += "           <span class=\"options-container add-option-link\" style=\"color: rgb(70, 131, 234); margin-top: 10px; float: right; cursor: pointer;\">+ Upload video</span>";
        html += "           <input type=\"file\" accept=\"video/mp4\" style=\"display:none;\" onchange=\"setCardValue('" + card.CardId + "', 5, this, null);\"/>";
        html += "       </label>";
        html += "       <div class=\"text_container\" style=\"margin-bottom: 10px; border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative; margin-top: 10px;\">";
        html += "           <textarea class=\"card-question\" style=\"margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; height: 130px; overflow: hidden; vertical-align: middle;\" placeholder=\"Video Description\" onkeyup=\"letterCounter(this, " + TEXT_MAX_VIDEO_DESCRIPTION + "); setCardValue('" + card.CardId + "', 4, this, null);\">" + card.VideoDescription + "</textarea>";
        html += "           <span class=\"letterCount\" style=\"right: 5px; bottom: 5px; position: absolute;\">" + (TEXT_MAX_VIDEO_DESCRIPTION - card.VideoDescription.length) + "</span>";
        html += "       </div>";
        html += "   </div>";
        html += "   <!-- /Video -->";
    }
    else if (card.CardType === CARD_TYPE_INSTRUCTIONS) {
        html += "   <!-- Instructions -->";
        html += "   <div class=\"form__row form__row--question\">";
        html += "       <div class=\"text_container\" style=\"border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
        html += "           <textarea class=\"card-question\" style=\"margin: 0px; padding: 15px 40px 15px 40px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; height: 100px; overflow: hidden; vertical-align: middle;\" placeholder=\"Instructions\" onkeyup=\"letterCounter(this, " + TEXT_MAX_INSTRUCTIONS + "); setCardValue('" + card.CardId + "', 7, this, null);\">" + card.InstructionsText + "</textarea>";
        html += "           <span class=\"letterCount\" style=\"right: 5px; top: 70px; position: absolute;\">" + (TEXT_MAX_INSTRUCTIONS - card.InstructionsText.length) + "</span>";
        html += "           <label class=\"upload-card-image\" style=\"position: absolute; bottom: 0px; top: 10px;\">";
        html += "               <input class=\"question-add-image\" type=\"file\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"setCardValue('" + card.CardId + "', 8, this, null);\" />";
        html += "           </label>";
        if (card.InstructionsImg != null) {
            html += "           <div style=\"height: auto; overflow: auto; margin-top: 0px;\" class=\"question-image-content\">";
        }
        else {
            html += "           <div style=\"height: auto; overflow: auto; margin-top: 0px; visibility: hidden;\" class=\"question-image-content\">";
        }

        html += "               <div class=\"survey-cross\" onclick=\"setCardValue('" + card.CardId + "', 9, this, null);\">";
        html += "                   <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
        html += "               </div>";
        if (card.InstructionsImg != null) {
            html += "               <img src=\"" + card.InstructionsImg.Url + "\" style=\"height: auto;\" />";
        }
        else {
            html += "               <img style=\"height: auto;\" />";
        }
        html += "           </div>";
        html += "       </div>";
        html += "   </div>";
        html += "   <!-- /Instructions -->";
    }
    else if (card.CardType === CARD_TYPE_SELECTONE) {
        html += "   <!-- Select One or Multi Choice -->";
        html += "   <div class=\"form__row form__row--question\">";
        html += "       <div class=\"text_container\" style=\"border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
        html += "           <textarea class=\"card-question\" style=\"margin: 0px; padding: 15px 40px 15px 40px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; height: 100px; overflow: hidden; vertical-align: middle;\" placeholder=\"Question\" onkeyup=\"letterCounter(this, " + TEXT_MAX_QUESTION + "); setCardValue('" + card.CardId + "', 10, this, null);\">" + card.QuestionText + "</textarea>";
        html += "           <span class=\"letterCount\" style=\"right: 5px; top: 70px; position: absolute;\">" + (TEXT_MAX_QUESTION - card.QuestionText.length) + "</span>";
        html += "           <label class=\"upload-card-image\" style=\"position: absolute; bottom: 0px; top: 10px;\">";
        html += "               <input class=\"question-add-image\" type=\"file\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"setCardValue('" + card.CardId + "', 11, this, null);\" />";
        html += "           </label>";
        if (card.QuestionImg != null) {
            html += "           <div style=\"height: auto; overflow: auto; margin-top: 0px;\" class=\"question-image-content\">";
        }
        else {
            html += "           <div style=\"height: auto; overflow: auto; margin-top: 0px; visibility: hidden;\" class=\"question-image-content\">";
        }
        html += "               <div class=\"survey-cross\" onclick=\"setCardValue('" + card.CardId + "', 12, this, null);\">";
        html += "                   <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
        html += "               </div>";
        if (card.QuestionImg != null) {
            html += "               <img src=\"" + card.QuestionImg.Url + "\" style=\"height: auto;\" />";
        }
        else {
            html += "               <img style=\"height: auto;\" />";
        }
        html += "           </div>";
        html += "       </div>";
        html += "       <div id=\"divNewCardOptions\" class=\"options-container\" style=\"margin-top: 10px;\">";
        for (var i = 0; i < card.Options.length; i++) {
            html += "           <div style=\"margin-bottom: 10px\">";

            html += "               <div style=\"display: inline-block; width: 100%;\">";
            if (i == 0) {
                html += "                   <label style=\"font-weight: bolder; display: inline-block;\">Correct option</label>";
            }
            if (card.Options.length > 2) {
                html += "                   <a class=\"remove-option-link\" href=\"javascript:removeOption('" + card.CardId + "', " + i + ");\">- Remove option</a>";
            }
            html += "               </div>";

            html += "               <div class=\"text_container\" style=\"display: inline-block; width: 85%;\">";
            if (i == 0) {
                html += "                   <textarea class=\"option-content\" style=\"border-color: " + ((card.Options[i].Content.length > TEXT_MAX_OPTION) ? "red" : "rgba(176, 220, 65, 1)") + " ;\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"letterCounter(this, " + TEXT_MAX_OPTION + ", 'red', 'rgba(176, 220, 65, 1)'); setCardValue('" + card.CardId + "', 13, this, " + i + ");\" placeholder=\"Option " + (i + 1) + "\" rows=\"2\" cols=\"20\">" + card.Options[i].Content + "</textarea>";
            }
            else {
                html += "                   <textarea class=\"option-content\" style=\"border-color: " + ((card.Options[i].Content.length > TEXT_MAX_OPTION) ? "red" : "") + " ;\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"letterCounter(this, " + TEXT_MAX_OPTION + "); setCardValue('" + card.CardId + "', 13, this, " + i + ");\" placeholder=\"Option " + (i + 1) + "\" rows=\"2\" cols=\"20\">" + card.Options[i].Content + "</textarea>";
            }
            html += "                   <span class=\"survey-lettercount letterCount\">" + (TEXT_MAX_OPTION - card.Options[i].Content.length) + "</span>";
            html += "               </div>";
            if (i == 0) {
                html += "               <div class=\"mdl-selectfield\" style=\"width: 60px; display: inline-block;\">";
                html += "                   <select class=\"option-score\" onchange=\"setCardValue('" + card.CardId + "', 16, this, " + i + ");\">";
                for (var j = 0; j < OPTION_MAX_SCORE; j++) {
                    html += "                       <option value=\"" + (j + 1) + "\" " + ((card.Options[i].Score == (j + 1)) ? "selected" : "") + ">" + (j + 1) + "</option>";
                }
                html += "                   </select>";
                html += "               </div>";
            }
            html += "               <label class=\"upload-option-image\">";
            html += "                   <input class=\"option-add-image\" type=\"file\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"setCardValue('" + card.CardId + "', 14, this, " + i + ");\" />";
            html += "               </label>";

            if (card.Options[i].UploadedContent != null) {
                html += "               <div style=\"height: auto; overflow: auto;\" class=\"option-image-content\">";
            }
            else {
                html += "               <div style=\"visibility: hidden; height: 0px; overflow: auto;\" class=\"option-image-content\">";
            }
            html += "                   <div class=\"survey-cross\" onclick=\"setCardValue('" + card.CardId + "', 15, this, " + i + ");\">";
            html += "                      <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                   </div>";
            if (card.Options[i].UploadedContent != null) {
                html += "                   <img src=\"" + card.Options[i].UploadedContent.Url + "\" />";
            }
            else {
                html += "                   <img />";
            }

            html += "               </div>";
            html += "           </div>";
        }

        html += "      </div>";
        html += "      <a class=\"options-container add-option-link\" style=\"float: right;\" href=\"javascript:addOption('" + card.CardId + "');\">+ Add option</a>";
        html += "   </div>";
        html += "   <!-- /Select One or Multi Choice -->";
    }
    else if (card.CardType === CARD_TYPE_MULTICHOICE) {
        html += "   <!-- Multi Choice -->";
        html += "   <div class=\"form__row form__row--question\">";
        html += "       <div class=\"text_container\" style=\"border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
        html += "           <textarea class=\"card-question\" style=\"margin: 0px; padding: 15px 40px 15px 40px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; height: 100px; overflow: hidden; vertical-align: middle;\" placeholder=\"Question\" onkeyup=\"letterCounter(this, " + TEXT_MAX_QUESTION + "); setCardValue('" + card.CardId + "', 10, this, null);\">" + card.QuestionText + "</textarea>";
        html += "           <span class=\"letterCount\" style=\"right: 5px; top: 70px; position: absolute;\">" + (TEXT_MAX_QUESTION - card.QuestionText.length) + "</span>";
        html += "           <label class=\"upload-card-image\" style=\"position: absolute; bottom: 0px; top: 10px;\">";
        html += "               <input class=\"question-add-image\" type=\"file\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"setCardValue('" + card.CardId + "', 11, this, null);\" />";
        html += "           </label>";
        if (card.QuestionImg != null) {
            html += "           <div style=\"height: auto; overflow: auto; margin-top: 0px;\" class=\"question-image-content\">";
        }
        else {
            html += "           <div style=\"height: auto; overflow: auto; margin-top: 0px; visibility: hidden;\" class=\"question-image-content\">";
        }
        html += "               <div class=\"survey-cross\" onclick=\"setCardValue('" + card.CardId + "', 12, this, null);\">";
        html += "                   <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
        html += "               </div>";
        if (card.QuestionImg != null) {
            html += "               <img src=\"" + card.QuestionImg.Url + "\" style=\"height: auto;\" />";
        }
        else {
            html += "               <img style=\"height: auto;\" />";
        }
        html += "           </div>";
        html += "       </div>";
        html += "       <div id=\"divNewCardOptions\" class=\"options-container\" style=\"margin-top: 10px;\">";
        for (var i = 0; i < card.Options.length; i++) {
            html += "           <div style=\"margin-bottom: 10px\">";
            html += "               <div style=\"display: inline-block; width: 100%;\">";
            if (card.Options.length > 2) {
                html += "                   <a class=\"remove-option-link\" href=\"javascript:removeOption('" + card.CardId + "', " + i + ");\">- Remove option</a>";
            }
            html += "               </div>";
            html += "               <div class=\"text_container\" style=\"display: inline-block; width: 85%;\">";
            if (card.Options[i].Score > 0) {
                html += "                   <textarea class=\"option-content\" style=\"border-color: " + ((card.Options[i].Content.length > TEXT_MAX_OPTION) ? "red" : "rgba(176, 220, 65, 1)") + "\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"letterCounter(this, " + TEXT_MAX_OPTION + ", 'red', 'rgba(176, 220, 65, 1)'); setCardValue('" + card.CardId + "', 13, this, " + i + ");\" placeholder=\"Option " + (i + 1) + "\" rows=\"2\" cols=\"20\">" + card.Options[i].Content + "</textarea>";
            }
            else {
                html += "                   <textarea class=\"option-content\" style=\"border-color: " + ((card.Options[i].Content.length > TEXT_MAX_OPTION) ? "red" : "") + " ;\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"letterCounter(this, " + TEXT_MAX_OPTION + "); setCardValue('" + card.CardId + "', 13, this, " + i + ");\" placeholder=\"Option " + (i + 1) + "\" rows=\"2\" cols=\"20\">" + card.Options[i].Content + "</textarea>";
            }
            html += "                   <span class=\"survey-lettercount letterCount\">" + (TEXT_MAX_OPTION - card.Options[i].Content.length) + "</span>";
            html += "               </div>";
            html += "               <div class=\"mdl-selectfield\" style=\"width: 60px; display: inline-block;\">";
            html += "                   <select class=\"option-score\" onchange=\"setCardValue('" + card.CardId + "', 16, this, " + i + ");\">";
            for (var j = 0; j < (OPTION_MAX_SCORE + 1); j++) {
                html += "                       <option value=\"" + (j) + "\" " + ((card.Options[i].Score == (j)) ? "selected" : "") + ">" + (j) + "</option>";
            }
            html += "                   </select>";
            html += "               </div>";
            html += "               <label class=\"upload-option-image\">";
            html += "                   <input type=\"file\" class=\"option-add-image\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"setCardValue('" + card.CardId + "', 14, this, " + i + ");\" />";
            html += "               </label>";

            if (card.Options[i].UploadedContent != null) {
                html += "               <div style=\"height: auto; overflow: auto;\" class=\"option-image-content\">";
            }
            else {
                html += "               <div style=\"visibility: hidden; height: 0px; overflow: auto;\" class=\"option-image-content\">";
            }
            html += "                   <div class=\"survey-cross\" onclick=\"setCardValue('" + card.CardId + "', 15, this, " + i + ");\">";
            html += "                      <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                   </div>";
            if (card.Options[i].UploadedContent != null) {
                html += "                   <img src=\"" + card.Options[i].UploadedContent.Url + "\" />";
            }
            else {
                html += "                   <img />";
            }

            html += "               </div>";
            html += "           </div>";
        }

        html += "      </div>";
        html += "      <a class=\"options-container add-option-link\" style=\"float: right;\" href=\"javascript:addOption('" + card.CardId + "');\">+ Add option</a>";
        html += "   </div>";
        html += "   <!-- /Multi Choice -->";
    }
    else if (card.CardType === CARD_TYPE_SLIDES) {
        html += "   <!-- Slides -->";
        html += "   <div class=\"form__row form__row--question\">";
        html += "       <label style=\"font-weight: bold; margin-bottom: 5px;\">Slides</label>";

        if (card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_SLIDESHARE) // using SlideShare
        {
            html += "       <div class=\"pdf_content\" style=\"width: 100%; height: auto;\">";
            if (card.SlidesContent.Url != null && card.SlidesContent.Url != "") {
                html += "           <iframe src=\"" + card.SlidesContent.Url + "\" width=\"427\" height=\"356\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;\" allowfullscreen></iframe>";
            }
        }
        else if (card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_PORTRAIT) {
            if (card.SlidesContent != null && (card.SlidesContent.Url != null && card.SlidesContent.Url != "")) {
                html += "       <div class=\"pdf_content\" style=\"width: 100%; height: auto; margin-bottom: 10px;\">";
                html += "           <embed src=\"" + card.SlidesContent.Url + "\" width=\"427\" height=\"356\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;\" allowfullscreen></embed>";
            }
            else if (card.SlidesContent != null && (card.SlidesContent.Base64 != null && card.SlidesContent.Base64 != "")) {
                html += "       <div class=\"pdf_content\" style=\"width: 100%; height: auto; margin-bottom: 10px;\">";
                html += "           <div id=\"pdfCanvas_" + card.CardId + "\" class=\"pdfCanvas " + card.CardId + "\" style=\"margin: auto; height: 300px; overflow-y: scroll;\"></div>";
            }
            else {
                html += "	    <div class=\"pdf_content\" style=\"width: 100%; height: auto; display: none; \">";
            }
        }
        else if (card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_LANDSCAPE) {
            if (card.SlidesContent != null && (card.SlidesContent.Url != null && card.SlidesContent.Url != "") || (card.SlidesContent.Base64 != null && card.SlidesContent.Base64 != "")) {
                html += "       <div class=\"pdf_content\" style=\"width: 100%; height: auto; margin-bottom: 10px;\">";
                html += "           <div class=\"pdfCanvas " + card.CardId + "\" style=\"margin: auto;\"></div>";
                html += "           <div style=\"background: rgba(50, 50, 50, 1); width: 100%; text-align: center; color: white; font-size: 12px; font-weight: 300; font-style: italic; line-height: 50px; margin:0 10px;\">";
                html += "               <div class=\"pdf-toolbar\" style=\"width:150px; text-align:center; margin: 0 auto;\">";
                html += "                   <div class=\"pdf-prev\" style=\"display: inline-block; vertical-align:middle; border-bottom: 10px solid transparent; border-top: 10px solid transparent; border-right: 18px solid #fff; width: 0px;\" onclick=\"pdfPerv('" + card.CardId + "');\"></div>";
                html += "                   <label style=\"display: inline-block; padding-left:10px; padding-right:10px;\">";
                html += "                       <span class=\"pageNum " + card.CardId + "\">99</span>";
                html += "                       <span> of </span>";
                html += "                       <span class=\"pageTotal " + card.CardId + "\">999</span>";
                html += "                   </label>";
                html += "                   <div class=\"pdf-next\" style=\"display: inline-block; vertical-align:middle;border-bottom: 10px solid transparent; border-top: 10px solid transparent; border-left: 18px solid #fff; width: 0px;\" onclick=\"pdfNext('" + card.CardId + "');\"></div>";
                html += "               </div>";
                html += "           </div>";
            }
            else {
                html += "	    <div class=\"pdf_content\" style=\"width: 100%; height: auto; display: none; \">";
            }
        }
        else {
            // do nothing
        }
        html += "	    </div>";
        html += "       <div style=\"width: 100%; text-align: center; margin-bottom: 10px;\">";

        html += "           <div style=\"width: 30%; display: inline-block;\">";
        html += "               <label style=\" display: inline;\">";
        html += "                   <input class=\"slide-mode\" name=\"" + card.CardId + "_PDF_MODE\" type=\"radio\" " + ((card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_SLIDESHARE) ? " checked=\"checked\"" : "") + " onchange=\"setCardValue('" + card.CardId + "', 26, this, null);\" value=\"" + UPLOAD_FILE_TYPE_SLIDESHARE + "\">";
        html += "                   <span style=\"display: inline-block; \">SlideShare Link</span>";
        html += "               </label>";
        html += "           </div>";

        html += "           <div style=\"width: 30%; display: inline-block;\">";
        html += "               <label style=\" display: inline;\">";
        html += "                   <input class=\"slide-mode\" name=\"" + card.CardId + "_PDF_MODE\" type=\"radio\" " + ((card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_PORTRAIT) ? " checked=\"checked\"" : "") + " onchange=\"$('#" + card.CardId + "_PDF_MODE_PORTRAIT').trigger('click');\" value=\"" + UPLOAD_FILE_TYPE_PDF_PORTRAIT + "\">";
        html += "                   <span style=\"display: inline-block; \">PDF Viewer</span>";
        html += "                   <input type=\"file\" id=\"" + card.CardId + "_PDF_MODE_PORTRAIT\" accept=\"application/pdf\" style=\"display:none;\" onchange=\"setCardValue('" + card.CardId + "', 24, this, " + UPLOAD_FILE_TYPE_PDF_PORTRAIT + ");\" />";
        html += "               </label>";
        html += "           </div>";

        html += "           <div style=\"width: 30%; display: inline-block;\">";
        html += "               <label style=\" display: inline;\">";
        html += "                   <input class=\"slide-mode\" name=\"" + card.CardId + "_PDF_MODE\" type=\"radio\" " + ((card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_LANDSCAPE) ? " checked=\"checked\"" : "") + " onchange=\"$('#" + card.CardId + "_PDF_MODE_LANDSCAPE').trigger('click');\" value=\"" + UPLOAD_FILE_TYPE_PDF_LANDSCAPE + "\">";
        html += "                   <span style=\"display: inline-block; \">Slide Viewer</span>";
        html += "                   <input type=\"file\" id=\"" + card.CardId + "_PDF_MODE_LANDSCAPE\" accept=\"application/pdf\" style=\"display:none;\" onchange=\"setCardValue('" + card.CardId + "', 24, this, " + UPLOAD_FILE_TYPE_PDF_LANDSCAPE + ");\" />";
        html += "               </label>";
        html += "           </div>";


        html += "       </div>";

        if (card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_SLIDESHARE) {
            html += "           <div>";
            html += "               <input class=\"slide-slideshare\" type=\"text\" value=\"" + card.SlidesContent.Url + "\"  placeholder=\"SlideShare Link\" style=\"width: 95%; display: inline-block;\" onblur=\"setCardValue('" + card.CardId + "', 25, this, null);\" />";
            html += "           </div>";
        }


        html += "       <div class=\"text_container\" style=\"border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative; marge-top:5px;\">";
        html += "           <input class=\"question-title\" style=\"margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"letterCounter(this, " + TEXT_MAX_SLIDES_TITLE + "); setCardValue('" + card.CardId + "', 22, this, null);\" type=\"text\" placeholder=\"Slides Title\" value=\"" + card.SlidesTitle + "\" />";
        html += "           <span class=\"letterCount\" style=\"right: 5px; bottom: 5px; position: absolute;\">" + (TEXT_MAX_SLIDES_TITLE - card.SlidesTitle.length) + "</span>";
        html += "       </div>";

        html += "       <div class=\"text_container\" style=\"margin-bottom: 10px; border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative; margin-top: 10px;\">";
        html += "           <textarea class=\"card-question\" style=\"margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; height: 130px; overflow: hidden; vertical-align: middle;\" placeholder=\"Slides Description\" onkeyup=\"letterCounter(this, " + TEXT_MAX_SLIDES_DESCRIPTION + "); setCardValue('" + card.CardId + "', 23, this, null);\">" + card.SlidesDescription + "</textarea>";
        html += "           <span class=\"letterCount\" style=\"right: 5px; bottom: 5px; position: absolute;\">" + (TEXT_MAX_SLIDES_DESCRIPTION - card.SlidesDescription.length) + "</span>";
        html += "       </div>";
        html += "   </div>";
        html += "   <!-- /Slides -->";
    }
    else {
        // do nothing
    }

    return html;
}

function getMiniCardHtml(card) {
    var html = "";
    html += "<div class=\"card mini-card " + card.CardId + " cansort ui-sortable-handle\" data-ordering=\"" + card.Ordering + "\" data-paging=\"" + card.Paging + "\" data-cardid=\"" + card.CardId + "\">";
    html += "	<div class=\"grid-question-number\">";
    html += "		<div>";
    html += "			<div style=\"color: rgb(221, 221, 221);\">";
    html += "				<div class=\"number\" style=\"font-size: 1.8rem; font-weight: bold; margin-bottom: 5px;\">P" + card.Paging + " | Q" + card.Ordering + "</div>";
    html += "				<div>Code: " + card.CardId + "</div>";
    html += "			</div>";
    html += "		</div>";
    html += "	</div>";
    html += "	<div class=\"grid-question\">";
    html += "		<label style=\"color: rgb(221, 221, 221);\"></label>";
    html += "		<div class=\"image-placeholder\">";
    if (card.CardType == CARD_TYPE_INSTRUCTIONS && card.InstructionsImg != null) {
        html += "           <img src=\"" + card.InstructionsImg.Url + "\">";
    }
    else if (card.CardType == CARD_TYPE_SELECTONE && card.QuestionImg != null) {
        html += "           <img src=\"" + card.QuestionImg.Url + "\">";
    }
    else if (card.CardType == CARD_TYPE_MULTICHOICE && card.QuestionImg != null) {
        html += "           <img src=\"" + card.QuestionImg.Url + "\">";
    }
    else {
        // do nothing
    }
    html += "       </div>";
    html += "		<div class=\"grid__span--6\">";
    html += "			<label style=\"color: rgb(204, 204, 204); font-weight: bold; font-size: 1.1em;\">";

    if (card.CardType == CARD_TYPE_ARTICLE) {
        html += "Article";
    }
    else if (card.CardType == CARD_TYPE_VIDEO) {
        html += "Video";
    }
    else if (card.CardType == CARD_TYPE_INSTRUCTIONS) {
        html += "Instructions";
    }
    else {
        html += "Question";
    }

    html += "           </label>";
    html += "			<div style=\"overflow:hidden; text-overflow:ellipsis; white-space: nowrap; \">";

    if (card.CardType == CARD_TYPE_ARTICLE) {
        html += card.ArticlePages[0].Title;
    }
    else if (card.CardType == CARD_TYPE_VIDEO) {
        html += card.VideoTitle;
    }
    else if (card.CardType == CARD_TYPE_INSTRUCTIONS) {
        html += card.InstructionsText;
    }
    else if (card.CardType == CARD_TYPE_SLIDES) {
        html += card.SlidesTitle;
    }
    else {
        html += card.QuestionText;
    }

    html += "           </div>";
    html += "		</div>";
    html += "	</div>";
    html += "	<div class=\"grid-question-icon\">";
    html += "		<label style=\"color: rgb(204, 204, 204); font-weight: bold; font-size: 1.1em;\">Type</label>";
    html += "		<div>";

    if (card.CardType == CARD_TYPE_ARTICLE) {
        html += "<i class=\"fa fa-file-text-o\" aria-hidden=\"true\" style=\"font-size: 1.8em; color: rgba(170, 170, 170, 1);\"></i>";
    }
    else if (card.CardType == CARD_TYPE_VIDEO) {
        html += "<i class=\"fa fa-youtube-play\" aria-hidden=\"true\" style=\"font-size: 1.8em; color: rgba(170, 170, 170, 1);\"></i>";
    }
    else if (card.CardType == CARD_TYPE_INSTRUCTIONS) {
        html += "<i class=\"fa fa-info-circle\" aria-hidden=\"true\" style=\"font-size: 1.8em; color: rgba(170, 170, 170, 1);\"></i>";
    }
    else if (card.CardType == CARD_TYPE_SELECTONE) {
        html += "<i class=\"fa fa-dot-circle-o\" aria-hidden=\"true\" style=\"font-size: 1.8em; color: rgba(170, 170, 170, 1);\"></i>";
    }
    else if (card.CardType == CARD_TYPE_MULTICHOICE) {
        html += "<i class=\"fa fa-check-square\" aria-hidden=\"true\" style=\"font-size: 1.8em; color: rgba(170, 170, 170, 1);\"></i>";
    }
    else if (card.CardType == CARD_TYPE_SLIDES) {
        html += "<i class=\"fa fa-clone\" aria-hidden=\"true\" style=\"font-size: 1.8em; color: rgba(170, 170, 170, 1);\"></i>";
    }
    else {
        // do nothing
    }
    html += "       </div>";
    html += "	</div>";
    html += "	<div class=\"grid-question-icon\">";
    html += "		<label style=\"color: rgb(204, 204, 204); font-weight: bold;\"></label>";
    html += "		<div><span></span></div>";
    html += "	</div>";
    html += "	<div class=\"grid-question-icon\">";
    html += "		<label style=\"color: rgb(204, 204, 204); font-weight: bold; visibility: hidden;\">Analytics</label>";
    html += "		<div style=\" visibility: hidden;\"><a tabindex=\"0\" href=\"javascript:void(0);\"><img title=\"Analytics\" src=\"/Img/icon_result.png\"></a></div>";
    html += "	</div>";
    html += "	<div class=\"grid-question-expandable\"><a tabindex=\"0\" href=\"javascript:getCardDetail('" + card.CardId + "');\"><i class=\"fa fa-caret-square-o-down fa-lg icon\"></i></a></div>";
    html += "</div>";

    if (card.HasPageBreak) {
        html += "<div class=\"pagebreak " + card.CardId + "\"><hr style=\"clear: both; border-top-color: rgb(156, 62, 30); border-top-width: 4px; border-top-style: solid;\"></div>";
    }

    return html;
}
//////////////////
//  API ZONE    //
//////////////////
function createCard(cardId) {
    try {
        var card = getCard(cardId);
        if (!isCardValid(card)) {
            return;
        }

        var request = new CreateMLCardRequest(card);
        jQuery.ajax({
            type: "POST",
            url: "/MLEvaluation/CreateCard",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            dataType: "json",
            crossDomain: true,
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                if (d.Success) {
                    ShowToast("Added card", 1);
                    RedirectPage("/MLearning/EvaluationEdit/" + $("#main_content_hfExamId").val() + "/" + $("#main_content_hfCategoryId").val(), 300);
                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });

    } catch (e) {
        console.error(e.toString());
    }
}

function getCardDetail(cardId) {

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerUserId").val(),
        'CardId': cardId,
        'TopicId': $("#main_content_hfExamId").val()
    };
    jQuery.ajax({
        type: "POST",
        url: "/MLEvaluation/GetCardDetail",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                var card = convertToViewCardModel(d.Card);
                for (var i = 0; i < exam.Cards.length; i++) {
                    if (exam.Cards[i].CardId == card.CardId) {
                        exam.Cards[i] = card;
                        break;
                    }
                }
                var pageBreakLayout = $(".pagebreak." + cardId);
                pageBreakLayout.remove();

                var miniCardLayout = $(".card.mini-card." + cardId);
                miniCardLayout.after(getFullCardHtml(card));
                miniCardLayout.remove();

                if (card.CardType == CARD_TYPE_SLIDES && card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_LANDSCAPE) {
                    renderPDF(card.SlidesContent.Url, card.CardId, false);
                }
                else if (card.CardType == CARD_TYPE_SLIDES && card.SlidesContent.UploadType == UPLOAD_FILE_TYPE_PDF_PORTRAIT) {
                    showPDF(card.SlidesContent.Url, card.CardId, false);
                }
                else {
                    // do nothing
                }

                turnOffFeatures();
            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}


function deleteCard(cardId) {
    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerUserId").val(),
        'CardId': cardId,
        'TopicId': $("#main_content_hfExamId").val(),
        'CategoryId': $("#main_content_hfCategoryId").val()
    };

    jQuery.ajax({
        type: "POST",
        url: "/MLEvaluation/DeleteCard",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("Deleted card", 1);
                RedirectPage("/MLearning/EvaluationEdit/" + $("#main_content_hfExamId").val() + "/" + $("#main_content_hfCategoryId").val(), 300);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function searchCards(event) {
    if (event == null || event == undefined || event.keyCode == 13) {
        var request = {
            'CompanyId': $("#main_content_hfCompanyId").val(),
            'ManagerId': $("#main_content_hfManagerUserId").val(),
            'TopicId': $("#main_content_hfExamId").val(),
            'CategoryId': $("#main_content_hfCategoryId").val(),
            'KeyWord': $("#tbSearch").val()
        };

        jQuery.ajax({
            type: "POST",
            url: "/MLEvaluation/SearchCards",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            dataType: "json",
            crossDomain: true,
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                if (d.Success) {
                    var cards = d.Topic.Cards;
                    exam.Cards = [];
                    for (var i = 0; i < cards.length; i++) {
                        exam.Cards.push(convertToViewCardModel(cards[i]));
                    }

                    displayCardList(exam.Cards);
                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });
    }
}

function editCard(cardId) {
    var card = getCard(cardId);
    if (!isCardValid(card)) {
        return;
    }

    var request = new CreateMLCardRequest(card);
    jQuery.ajax({
        type: "POST",
        url: "/MLEvaluation/UpdateCard",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("Updated card", 1);
                RedirectPage("/MLearning/EvaluationEdit/" + $("#main_content_hfExamId").val() + "/" + $("#main_content_hfCategoryId").val(), 300);
            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

//////////////////
//  API ZONE    //
//////////////////

function actionCard(actionType, cardId) {
    if (actionType == CARD_ACTION_TYPE.SHOW_DELETE) {
        $("#mpe_backgroundElement").show();
        $("#plDeleteCard").show();
        var card = getCard(cardId);
        var showText = "Page " + card.Paging + " / Question " + card.Ordering;

        $("#spDelCardTitle").html(showText);
        $("#actionDelCard").attr("href", "javascript:deleteCard('" + cardId + "');");
    }
    else if (actionType == CARD_ACTION_TYPE.HIDE_DELETE) {
        $("#mpe_backgroundElement").hide();
        $("#plDeleteCard").hide();
    }
}

function base64ToUint8Array(base64) {
    try {
        var raw = window.atob(base64);
        var uint8Array = new Uint8Array(raw.length);
        for (var i = 0; i < raw.length; i++) {
            uint8Array[i] = raw.charCodeAt(i);
        }
        return uint8Array;
    } catch (e) {
        console.error(e.toString());
        return null;
    }

}

// using PDFJS
function showPDF(base64DataOrUrl, cardId, isBase64) {
    if (isBase64) {
        base64DataOrUrl = base64DataOrUrl.replace("data:application/pdf;base64,", "");
        base64DataOrUrl = base64ToUint8Array(base64DataOrUrl);
    }


    var pdfCanvasContainer = $(".pdfCanvas." + cardId);
    var pdfPerivewCanvasContainer = $(".perviewPdfCanvas." + cardId);

    function renderPage(page) {
        var viewport = page.getViewport(1);
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var renderContext = {
            canvasContext: ctx,
            viewport: viewport
        };

        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // 重新計算比例
        if (pdfCanvasContainer.width() < viewport.width) {
            viewport = page.getViewport(((pdfCanvasContainer.width() - 20) / viewport.width));
            renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };

            canvas.height = viewport.height;
            canvas.width = viewport.width;
        }


        pdfCanvasContainer.append(canvas);

        page.render(renderContext);
    }

    function renderPreviewPage(page) {
        var viewport = page.getViewport(1);
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');
        var renderContext = {
            canvasContext: ctx,
            viewport: viewport
        };

        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // 重新計算比例
        if (pdfPerivewCanvasContainer.width() < viewport.width) {
            viewport = page.getViewport(((pdfPerivewCanvasContainer.width()) / viewport.width));
            renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };

            canvas.height = viewport.height;
            canvas.width = viewport.width;
        }


        pdfPerivewCanvasContainer.append(canvas);

        page.render(renderContext);
    }

    function renderPages(pdfDoc) {
        for (var num = 1; num <= pdfDoc.numPages; num++) {
            if (num == 1) {
                pdfDoc.getPage(num).then(renderPreviewPage);
            }
            pdfDoc.getPage(num).then(renderPage);
        }
    }

    PDFJS.disableWorker = true;
    PDFJS.getDocument(base64DataOrUrl).then(renderPages);
}

// using touchPDF.js
function renderPDF(base64DataOrUrl, cardId, isBase64) {
    if (isBase64) {
        base64DataOrUrl = base64DataOrUrl.replace("data:application/pdf;base64,", "");
        base64DataOrUrl = base64ToUint8Array(base64DataOrUrl);
    }

    $(".pdfCanvas." + cardId).pdf({
        source: base64DataOrUrl,
        title: "",
        disableZoom: true,
        disableKeys: true,
        showToolbar: false,
        loaded: function () {
            $(".pageNum." + cardId).html("1");
            $(".pageTotal." + cardId).html($(".pdfCanvas." + cardId).pdf("getTotalPages"));
        }
    });

    $(".perviewPdfCanvas." + cardId).pdf({
        source: base64DataOrUrl,
        title: "",
        disableZoom: true,
        disableKeys: true,
        showToolbar: false,
        loaded: function () {
            $(".pageNum." + cardId).html("1");
            $(".pageTotal." + cardId).html($(".pdfCanvas." + cardId).pdf("getTotalPages"));
        }
    });
}

function pdfPerv(cardId) {
    $(".pdfCanvas." + cardId).pdf("previous");
    $(".pageNum." + cardId).html($(".pdfCanvas." + cardId).pdf("getPageNumber"));
    $(".pageTotal." + cardId).html($(".pdfCanvas." + cardId).pdf("getTotalPages"));
}


function pdfNext(cardId) {
    $(".pdfCanvas." + cardId).pdf("next");
    $(".pageNum." + cardId).html($(".pdfCanvas." + cardId).pdf("getPageNumber"));
    $(".pageTotal." + cardId).html($(".pdfCanvas." + cardId).pdf("getTotalPages"));
}

function turnOffFeatures() {
    if (exam.Status > 1 && exam.ProgressStatus != 1) {
        $(".card.full-card .tabs__list__item").css("cursor", "");
        $(".card.full-card .tabs__list__item").prop("onclick", null).off("click");
        $(".card.full-card .tabs__list__item").attr("onclick", "").unbind("click");
        $(".card.full-card .card-setting-pagebreak").attr("disabled", true);
        $(".card.full-card .question-title").attr("disabled", true);
        $(".card.full-card .remove-video").css("display", "none");
        $(".card.full-card .card-question").attr("disabled", true);
        $(".card.full-card .question-add-image").attr("disabled", true);
        $(".card.full-card .upload-card-image").css("cursor", "default");
        $(".card.full-card .option-content").attr("disabled", true);
        $(".card.full-card .option-add-image").attr("disabled", true);
        $(".card.full-card .upload-option-image").css("cursor", "default");
        $(".card.full-card .option-score").attr("disabled", true);
        $(".card.full-card .options-container.add-option-link").css("display", "none");
        $(".card.full-card .remove-option-link").css("display", "none");
        $(".card.full-card .multichoice-seleted-min").attr("disabled", true);
        $(".card.full-card .multichoice-seleted-max").attr("disabled", true);
        $(".card.full-card .multichoice-behaviour-randomize").attr("disabled", true);
        $(".card.full-card .slide-mode").attr("disabled", true);
        $(".card.full-card .slide-slideshare").attr("disabled", true);
        $(".card.full-card .survey-cross").css("display", "none");
    }
}
