﻿////////////////////////////////////////////////////////////////////////////////
// (Global) -- (Global) -- (Global)  -- (Global)  -- (Global)  -- (Global)  -- (Global)  -- (Global) 

var timeZone = 0;
var assignment = null;

////////////////////////////////////////
// Constants

// DRUATION_TYPE constants -
const DRUATION_TYPE = {};
Object.defineProperty(DRUATION_TYPE, "PRERPETUAL", { get: function () { return 1; } });
Object.defineProperty(DRUATION_TYPE, "SCHEDULE", { get: function () { return 2; } });

// PARTICIPANTS_TYPE constants -
const PARTICIPANTS_TYPE = {};
Object.defineProperty(PARTICIPANTS_TYPE, "FOLLOWEDUCATION", { get: function () { return 1; } });
Object.defineProperty(PARTICIPANTS_TYPE, "CUSTOM", { get: function () { return 2; } });



////////////////////////////////////////
// Model Classes
function Assignment() {
    this.Id = "";
    this.DruationType = DRUATION_TYPE.PRERPETUAL;
    this.StartDate = moment.utc(moment().year() + "/" + (moment().month() + 1) + "/" + moment().date(), "YYYY/MM/DD");
    this.EndDate = moment.utc(moment().year() + "/" + (moment().month() + 1) + "/" + moment().date(), "YYYY/MM/DD").add(1, "months").add(1, "days").add(-60, "seconds");
    this.TargetEducations = [];
    this.ParticipantsType = PARTICIPANTS_TYPE.FOLLOWEDUCATION;
    this.IsTargetedDepartments = false;
    this.TargetDepartments = [];
    this.IsTargetedUsers = false;
    this.TargetUsers = [];
    this.IsRemindful = false;
    this.FrequencyDays = 1;
}

function Education() {
    this.Id = "";
    this.Title = "";
}

function Department() {
    this.Id = "";
    this.Title = "";
    this.IsAvailable = true;
}

function User() {
    this.Id = "";
    this.FirstName = "";
    this.LastName = "";
    this.Email = "";
    this.IsAvailable = true;
}


////////////////////////////////////////
// Ad-hoc commonly used functions

function updateUI() {
    // Duration
    $("input[name=durationType][value=" + assignment.DruationType + "]").prop('checked', true);
    // Start date 
    $("#startDate").val(assignment.StartDate.format("DD/MM/YYYY"));
    $("#startDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd/mm/yy',
        showOtherMonths: true
    });
    $("#startDateHH").val(assignment.StartDate.format("hh"));
    $("#startDateMM").val(assignment.StartDate.format("mm"));
    $("#startDateMR").val(assignment.StartDate.format("a"));

    // End date
    if (assignment.DruationType === DRUATION_TYPE.SCHEDULE) // show EndDate
    {
        $("#dvEndDate").css("display", "block");
        $("#endDate").val(assignment.EndDate.format("DD/MM/YYYY"));
        $("#endDate").datepicker({
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999);
                }, 0);
            },
            dateFormat: 'dd/mm/yy',
            showOtherMonths: true
        });
        $("#endDateHH").val(assignment.EndDate.format("hh"));
        $("#endDateMM").val(assignment.EndDate.format("mm"));
        $("#endDateMR").val(assignment.EndDate.format("a"));
    }
    else // hide EndDate
    {
        $("#dvEndDate").css("display", "none");
    }

    // TargetEducations
    if (assignment.TargetEducations != null) {
        var html = "";
        for (var i = 0; i < assignment.TargetEducations.length; i++) {
            html = html + "<div class=\"tag " + assignment.TargetEducations[i].Id + "\">";
            html = html + "    <span class=\"education-tag\" style=\"padding-left:10px;\">" + assignment.TargetEducations[i].Title + "</span>";
            html = html + "    <a class=\"tag__icon\" href=\"javascript:setAssignmentValue(4, this, '" + assignment.TargetEducations[i].Id + "');\">x</a>";
            html = html + "</div>";
        }
        $(".education.tags").html(html);
    }

    // ParticipantsType
    $("#ddlParticipantsType").val(assignment.ParticipantsType);
    if (assignment.ParticipantsType == PARTICIPANTS_TYPE.FOLLOWEDUCATION) {
        $("#dvTargetDapartments").css("display", "none");
        $("#dvTargetUsers").css("display", "none");
    }
    else {
        $("#dvTargetDapartments").css("display", "block");
        $("#dvTargetUsers").css("display", "block");
    }

    // TargetDepartments
    if (assignment.IsTargetedDepartments) {
        $("#cbTargetDepartment").prop("checked", true);
    }
    else {
        $("#cbTargetDepartment").prop("checked", false);
    }

    if (assignment.TargetDepartments != null) {
        var html = "";
        for (var i = 0; i < assignment.TargetDepartments.length; i++) {
            if (assignment.TargetDepartments[i].IsAvailable) {
                html = html + "<div class=\"tag " + assignment.TargetDepartments[i].Id + "\">";
            }
            else {
                html = html + "<div class=\"tag " + assignment.TargetDepartments[i].Id + "\" style=\"border-color: rgba(255, 29, 37, 1); color: white; background-color: rgba(255, 29, 37, 1);\">";
            }
            html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + assignment.TargetDepartments[i].Title + "</span>";
            if (assignment.TargetDepartments[i].IsAvailable) {
                html = html + "    <a class=\"tag__icon\" href=\"javascript:setAssignmentValue(7, this, '" + assignment.TargetDepartments[i].Id + "');\">x</a>";
            }
            else {
                html = html + "    <a class=\"tag__icon\" href=\"javascript:setAssignmentValue(7, this, '" + assignment.TargetDepartments[i].Id + "');\" style=\"color: white;\">x</a>";
            }
            html = html + "</div>";
        }
        $(".department.tags").html(html);
    }

    // TargetUsers
    if (assignment.IsTargetedUsers) {
        $("#cbTargetUser").prop("checked", true);
    }
    else {
        $("#cbTargetUser").prop("checked", false);
    }

    if (assignment.TargetUsers != null) {
        var html = "";
        for (var i = 0; i < assignment.TargetUsers.length; i++) {
            if (assignment.TargetUsers[i].IsAvailable) {
                html = html + "<div class=\"tag " + assignment.TargetUsers[i].Id + "\">";
            }
            else {
                html = html + "<div class=\"tag " + assignment.TargetUsers[i].Id + "\" style=\"border-color: rgba(255, 29, 37, 1); color: white; background-color: rgba(255, 29, 37, 1);\">";
            }
            html = html + "    <span class=\"user-tag\" style=\"padding-left:10px;\">" + assignment.TargetUsers[i].FirstName + " " + assignment.TargetUsers[i].LastName + "(" + assignment.TargetUsers[i].Email + ")</span>";
            if (assignment.TargetUsers[i].IsAvailable) {
                html = html + "    <a class=\"tag__icon\" href=\"javascript:setAssignmentValue(9, this, '" + assignment.TargetUsers[i].Id + "');\">x</a>";
            }
            else {
                html = html + "    <a class=\"tag__icon\" href=\"javascript:setAssignmentValue(9, this, '" + assignment.TargetUsers[i].Id + "');\" style=\"color: white;\">x</a>";
            }
            html = html + "</div>";
        }
        $(".user.tags").html(html);
    }

    // Reminder & FrequencyDays
    if (assignment.IsRemindful) {
        $("#ddlReminder").val("on");
        $("#dvFrequency").css("display", "block");
    }
    else {
        $("#ddlReminder").val("off");
        $("#dvFrequency").css("display", "none");
    }

    $("#tbFrequency").val(assignment.FrequencyDays);
}

function setAssignmentValue(attrType, element, arg) {
    switch (attrType) {
        case 1: // Duration
            assignment.DruationType = parseInt($("input[name=durationType]:checked").val());
            break;

        case 2: // StartDate
            var startDateText = $("#startDate").val() + " " + $("#startDateHH").val() + ":" + $("#startDateMM").val() + " " + $("#startDateMR").val();
            if (moment(startDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
                assignment.StartDate = moment(startDateText, "DD/MM/YYYY hh:mm a");
            }
            else {
                ShowToast("Start date 錯誤!", 2);
                return;
            }
            break;

        case 3: // EndDate
            var endDateText = $("#endDate").val() + " " + $("#endDateHH").val() + ":" + $("#endDateMM").val() + " " + $("#endDateMR").val();
            if (moment(endDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
                if (moment(endDateText, "DD/MM/YYYY hh:mm a").isBefore(assignment.StartDate)) {
                    ShowToast("End date 錯誤! 不可早於開始時間", 2);
                    return;
                }
                else {
                    assignment.EndDate = moment(endDateText, "DD/MM/YYYY hh:mm a");
                }
            }
            else {
                ShowToast("End date 錯誤!", 2);
                return;
            }
            break;

        case 4: // Remove TargetEducations
            for (var i = 0; i < assignment.TargetEducations.length; i++) {
                if (assignment.TargetEducations[i].Id === arg) {
                    assignment.TargetEducations.splice(i, 1);
                    break;
                }
            }
            if (assignment.ParticipantsType == PARTICIPANTS_TYPE.CUSTOM) {
                checkPrivacy();
            }
            break;

        case 5: // Participants
            assignment.ParticipantsType = parseInt($("#ddlParticipantsType").val());
            break;

        case 6: // Set TargetDepartment
            assignment.TargetDepartments = [];

            var selectedAvailableDepartments = $('input:checkbox:checked[name="cbAvailableDepartments"]').map(function () { return $(this).val(); }).get();
            for (var i = 0; i < selectedAvailableDepartments.length; i++) {
                var department = new Department();
                department.Id = selectedAvailableDepartments[i].split(",")[0];
                department.Title = selectedAvailableDepartments[i].split(",")[1];
                department.IsAvailable = true;
                assignment.TargetDepartments.push(department);
            }

            var selectedUnavailableDepartments = $('input:checkbox:checked[name="cbUnavailableDepartments"]').map(function () { return $(this).val(); }).get();
            for (var i = 0; i < selectedUnavailableDepartments.length; i++) {
                var department = new Department();
                department.Id = selectedUnavailableDepartments[i].split(",")[0];
                department.Title = selectedUnavailableDepartments[i].split(",")[1];
                department.IsAvailable = false;
                assignment.TargetDepartments.push(department);
            }

            hidePopup();
            break;

        case 7: // Remove TargetDepartment
            for (var i = 0; i < assignment.TargetDepartments.length; i++) {
                if (assignment.TargetDepartments[i].Id == arg) {
                    assignment.TargetDepartments.splice(i, 1);
                    break;
                }
            }
            break;

        case 8: // Set TargetUser

            break;

        case 9: // Remove TargetUser
            for (var i = 0; i < assignment.TargetUsers.length; i++) {
                if (assignment.TargetUsers[i].Id == arg) {
                    assignment.TargetUsers.splice(i, 1);
                    break;
                }
            }
            break;

        case 10: // Reminder
            assignment.IsRemindful = (element.value === "on");
            break;

        case 11: // Frequency day
            if (parseInt(element.value) > 0) {
                assignment.FrequencyDays = parseInt(element.value);
            }
            else {
                ShowToast("Frequency Days 必須大於或等於 1", 2);
                return;
            }
            break;

        case 12: // IsTargetDepartments
            assignment.IsTargetedDepartments = $("#cbTargetDepartment").prop('checked');
            break;

        case 13: // IsTargetUsers
            assignment.IsTargetedUsers = $("#cbTargetUser").prop('checked');
            break;

        default:
            break;
    }
    updateUI();
}

function hidePopup() {
    $("#mpe_backgroundElement").hide();
    $("#dvSelectDepartment").hide();
}

function isDataValid() {
    // StartDate
    var startDateText = $("#startDate").val() + " " + $("#startDateHH").val() + ":" + $("#startDateMM").val() + " " + $("#startDateMR").val();
    if (moment(startDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
        assignment.StartDate = moment(startDateText, "DD/MM/YYYY hh:mm a");
    }
    else {
        ShowToast("Start date 錯誤!", 2);
        return false;
    }

    // EndDate
    if (assignment.DruationType == DRUATION_TYPE.SCHEDULE) {
        var endDateText = $("#endDate").val() + " " + $("#endDateHH").val() + ":" + $("#endDateMM").val() + " " + $("#endDateMR").val();
        if (moment(endDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
            if (moment(endDateText, "DD/MM/YYYY hh:mm a").isBefore(assignment.StartDate)) {
                ShowToast("End date 錯誤! 不可早於開始時間", 2);
                return false;
            }
            else {
                assignment.EndDate = moment(endDateText, "DD/MM/YYYY hh:mm a");
            }
        }
        else {
            ShowToast("End date 錯誤!", 2);
            return false;
        }
    }

    // TargetEducations
    if (assignment.TargetEducations.length == 0) {
        ShowToast("請先選擇一個Education", 2);
        return false;
    }

    // Participants
    if (assignment.ParticipantsType == PARTICIPANTS_TYPE.CUSTOM) {
        if (!assignment.IsTargetedDepartments && !assignment.IsTargetedUsers) {
            ShowToast("請至少選擇一個要綁定的對象", 2);
            return false;
        }
    }

    // TargetDepartments
    if (assignment.IsTargetedDepartments) {
        var c = 0;
        for (var i = 0; i < assignment.TargetDepartments.length; i++) {
            if (assignment.TargetDepartments[i].IsAvailable) {
                c++;
            }
        }

        if (c == 0) {
            ShowToast("請至少選擇一個 department", 2);
            return false;
        }
    }

    // TargetUser
    if (assignment.IsTargetedUsers) {
        var c = 0;
        for (var i = 0; i < assignment.TargetUsers.length; i++) {
            if (assignment.TargetUsers[i].IsAvailable) {
                c++;
            }
        }

        if (c == 0) {
            ShowToast("請至少選擇一個 user", 2);
            return false;
        }
    }

    // FrequencyDay
    if (assignment.IsRemindful) {
        if (parseInt($("#tbFrequency").val()) > 0) {
            assignment.FrequencyDays = parseInt($("#tbFrequency").val());
        }
        else {
            ShowToast("Frequency Days 必須大於或等於 1", 2);
            return false;
        }
    }

    return true;
}

////////////////////////////////////////
// Event handlers
(function ($) {
    "use strict";

    $(document).ready(function () {
        // Get Timezone
        if ($("#main_content_hfTimezone").val().length > 0) {
            timeZone = parseInt($("#main_content_hfTimezone").val());
        }

        // Set Assignment
        if ($("#main_content_hfAssignmentId").val() == null || $("#main_content_hfAssignmentId").val() == undefined || $("#main_content_hfAssignmentId").val() === "") // Create mode
        {
            assignment = new Assignment();
        }
        else // Edit mode
        {
            // call Get detail API
        }

        updateUI();

        // element event;
        educationAutoComplete();
        personnelAutoComplete();
    });

}(jQuery));

function RequestObject() {
    this.CompanyId = $("#main_content_hfCompanyId").val();
    this.ManagerId = $("#main_content_hfManagerId").val();
    this.TargetEducationIds = [];
    for (var i = 0; i < assignment.TargetEducations.length; i++) {
        this.TargetEducationIds.push(assignment.TargetEducations[i].Id);
    }
    this.DruationType = assignment.DruationType;
    this.StartDate = assignment.StartDate;
    if (assignment.DruationType == DRUATION_TYPE.PRERPETUAL) {
        this.EndDate = null;
    }
    else {
        this.EndDate = assignment.EndDate;
    }
    this.ParticipantsType = assignment.ParticipantsType;
    this.FrequencyDays = assignment.FrequencyDays;
    this.TargetedDepartmentIds = [];
    for (var i = 0; i < assignment.TargetDepartments.length; i++) {
        this.TargetedDepartmentIds.push(assignment.TargetDepartments[i].Id);
    }
    this.TargetedUserIds = [];
    for (var i = 0; i < assignment.TargetUsers.length; i++) {
        this.TargetedUserIds.push(assignment.TargetUsers[i].Id);
    }
}

////////////////////////////////////////
// API
function educationAutoComplete() {
    $("#tbSearchEducation").autocomplete({
        source: function (request, callback) {
            var data = {
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'ManagerId': $("#main_content_hfManagerId").val(),
                'CategoryId': null,
                'ContainsName': request.term
            };
            $.ajax({
                url: "/api/MLEducation/GetAutoCompleteList",
                data: data,
                dataType: "json",
                type: "POST",
                success: function (r) {
                    if (r.Success) {
                        callback(r.Educations);
                    }
                }
            });
        },

        minLength: 1,
        response: function (event, ui) {
            if (ui != null) {
                for (var i = 0; i < ui.content.length; i++) {
                    for (var j = 0; j < assignment.TargetEducations.length; j++) {
                        if (assignment.TargetEducations[j].Id == ui.content[i].education_id) {
                            ui.content.splice(i, 1);
                            if (i != ui.content.length - 1) {
                                i--;
                            }
                            break;
                        }
                    }
                }
            }
        },
        select: function (event, ui) {
            var isExisting = false;
            for (var i = 0; i < assignment.TargetEducations.length; i++) {
                if (assignment.TargetEducations[i].Id == ui.item.value) {
                    isExisting = true;
                    break;
                }
            }

            if (!isExisting) {
                var education = new Education();
                education.Title = ui.item.label;
                education.Id = ui.item.value;
                assignment.TargetEducations.push(education);

                var html = "";
                html = "";
                html = html + "<div class=\"tag " + ui.item.value + "\">";
                html = html + "    <span class=\"education-tag\" style=\"padding-left:10px;\">" + ui.item.label + "</span>";
                html = html + "    <a class=\"tag__icon\" href=\"javascript:setAssignmentValue(4, this, '" + ui.item.value + "');\">x</a>";
                html = html + "</div>";
                $(".education.tags").append(html);

                if (assignment.ParticipantsType == PARTICIPANTS_TYPE.CUSTOM) {
                    checkPrivacy(); // Check privacy again!
                }
            }
            return false;
        },
        focus: function (event, ui) {
            event.preventDefault();
        },
        close: function (event, ui) {
            $("#tbSearchEducation").val("");
        }
    });
}

function personnelAutoComplete() {
    $("#tbSearchUser").autocomplete({
        source: "/api/personnel?companyid=" + $("#main_content_hfCompanyId").val() + "&userid=" + $("#main_content_hfManagerId").val(),
        minLength: 1,
        response: function (event, ui) {
            if (ui != null) {
                for (var i = 0; i < ui.content.length; i++) {
                    for (var j = 0; j < assignment.TargetUsers.length; j++) {
                        if (assignment.TargetUsers[j].Id == ui.content[i].value) {
                            ui.content.splice(i, 1);
                            if (i != ui.content.length - 1) {
                                i--;
                            }
                            break;
                        }
                    }
                }
            }
        },
        select: function (event, ui) {
            var isExisting = false;
            for (var i = 0; i < assignment.TargetUsers.length; i++) {
                if (assignment.TargetUsers[i].Id == ui.item.value) {
                    isExisting = true;
                    break;
                }
            }

            if (!isExisting) {
                var user = new User();
                user.Id = ui.item.value;
                user.FirstName = ui.item.first_name;
                user.LastName = ui.item.last_name;
                user.Email = ui.item.email;
                user.IsAvailable = false;
                assignment.TargetUsers.push(user);

                var html = "";
                html = "";
                html = html + "<div class=\"tag " + ui.item.value + "\">";
                html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + ui.item.FirstName + " " + ui.item.LastName + "(" + ui.item.Email + ")</span>";
                html = html + "    <a class=\"tag__icon\" href=\"javascript:setAssignmentValue(4, this, '" + ui.item.value + "');\">x</a>";
                html = html + "</div>";
                $(".user.tags").append(html);

                if (assignment.ParticipantsType == PARTICIPANTS_TYPE.CUSTOM) {
                    checkPrivacy(); // Check privacy again!
                }
            }
            return false;
        },
        focus: function (event, ui) {
            event.preventDefault();
        },
        close: function (event, ui) {
            $("#tbSearchUser").val("");
        }
    });
}

function checkPrivacy() {
    if (assignment.TargetEducations.length == 0) {
        assignment.TargetDepartments = [];
        assignment.TargetUsers = [];
        updateUI();
        ShowToast("You have not selected any Education", 2);
        return;
    }

    var targetEducationIds = [];
    for (var i = 0; i < assignment.TargetEducations.length; i++) {
        targetEducationIds.push(assignment.TargetEducations[i].Id);
    }

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'TargetEducationIds': targetEducationIds
    };

    jQuery.ajax({
        type: "POST",
        url: "/Api/MLAssignment/GetPrivacy",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            debugger;
            if (d.Success) {
                // Departments
                for (var h = 0; h < assignment.TargetDepartments.length; h++) {
                    assignment.TargetDepartments[h].IsAvailable = false;
                    for (var i = 0; i < d.AvailableDepartments.length; i++) {
                        if (assignment.TargetDepartments[h].Id === d.AvailableDepartments[i].Id) {
                            assignment.TargetDepartments[h].IsAvailable = true;
                            break;
                        }
                    }
                }

                // Users
                for (var h = 0; h < assignment.TargetUsers.length; h++) {
                    assignment.TargetUsers[h].IsAvailable = false;
                    for (var i = 0; i < d.AvailableUsers.length; i++) {
                        if (assignment.TargetUsers[h].Id === d.AvailableUsers[i].UserId) {
                            assignment.TargetUsers[h].IsAvailable = true;
                            break;
                        }
                    }
                }

                updateUI();
            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function showPopup(popupType) {
    switch (popupType) {
        case 1: // TargetDepartments
            if (assignment.TargetEducations == null || assignment.TargetEducations.length == 0) {
                ShowToast("You have not selected any Education", 2);
                return;
            }
            else {
                var targetEducationIds = [];
                for (var i = 0; i < assignment.TargetEducations.length; i++) {
                    targetEducationIds.push(assignment.TargetEducations[i].Id);
                }

                var request = {
                    'CompanyId': $("#main_content_hfCompanyId").val(),
                    'ManagerId': $("#main_content_hfManagerId").val(),
                    'TargetEducationIds': targetEducationIds
                };

                jQuery.ajax({
                    type: "POST",
                    url: "/Api/MLAssignment/GetDepartmentPrivacy",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(request),
                    dataType: "json",
                    crossDomain: true,
                    beforeSend: function (xhr, settings) {
                        ShowProgressBar();
                    },
                    success: function (d, status, xhr) {
                        if (d.Success) {
                            var html = "";
                            for (var i = 0; i < d.AvailableDepartments.length; i++) {
                                html += "<label style=\"color: rgba(104, 104, 104, 1); font-size: 1.1em; display: inline;\">";

                                var isChecked = false;
                                for (var j = 0; j < assignment.TargetDepartments.length; j++) {
                                    if (d.AvailableDepartments[i].Id === assignment.TargetDepartments[j].Id) {
                                        isChecked = true;
                                        break;
                                    }
                                }

                                html += "   <input type=\"checkbox\" name=\"cbAvailableDepartments\" style=\"margin: 0px; vertical-align: middle;\" value=\"" + d.AvailableDepartments[i].Id + "," + d.AvailableDepartments[i].Title + "\" " + (isChecked ? "checked" : "") + ">";
                                html += "   <span style=\"display: inline-block; vertical-align: middle; margin-right: 10px;\">" + d.AvailableDepartments[i].Title + "</span>";
                                html += "</label>";
                                html += "<br />";
                            }
                            $("#dvAvailableDepartments").html(html);

                            html = "";
                            for (var i = 0; i < d.UnavailableDepartments.length; i++) {
                                html += "<label style=\"color: rgba(104, 104, 104, 1); font-size: 1.1em; display: inline;\">";


                                var isChecked = false;
                                for (var j = 0; j < assignment.TargetDepartments.length; j++) {
                                    if (d.UnavailableDepartments[i].Id === assignment.TargetDepartments[j].Id) {
                                        isChecked = true;
                                        break;
                                    }
                                }

                                html += "   <input type=\"checkbox\" name=\"cbUnavailableDepartments\" style=\"margin: 0px; vertical-align: middle;\" value=\"" + d.UnavailableDepartments[i].Id + "," + d.UnavailableDepartments[i].Title + "\" " + (isChecked ? "checked" : "") + " disabled=\"disabled\">";
                                html += "   <span style=\"display: inline-block; vertical-align: middle; margin-right: 10px;\">" + d.UnavailableDepartments[i].Title + "</span>";
                                html += "</label>";
                                html += "<br />";
                            }
                            $("#dvUnavailableDepartments").html(html);

                            $("#mpe_backgroundElement").show();
                            $("#dvSelectDepartment").show();
                        } else {
                            ShowToast(d.ErrorMessage, 2);
                        }
                    },
                    error: function (xhr, status, error) {
                        ShowToast(error, 2);
                    },
                    complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                        HideProgressBar();
                    }
                });
            }
            break;

        default:
            break;
    }
}

function createAssignment() {
    // Check data 

    if (!isDataValid()) {
        return;
    }
    var request = new RequestObject();
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/MLAssignment/CreateAssignments",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("成功", 1);
                RedirectPage("/MLearning/AssignmentList", 300);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}