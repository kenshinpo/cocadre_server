﻿function showTip(type) {
    switch (type) {
        case 1:
            $(".form__row .duration_tip").css('display', 'block');
            $(".form__row .duration_tip").css('opacity', '1');
            break;
        default:
            break;
    }
}

function hideTip(type) {
    switch (type) {
        case 1:
            $(".form__row .duration_tip").css('display', 'none');
            $(".form__row .duration_tip").css('opacity', '0');
            break;
        default:
            break;
    }
}

function rebindJs() {
    $("#main_content_tbStartDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd/mm/yy'
    });


    $("#main_content_tbEndDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd/mm/yy'
    });
}