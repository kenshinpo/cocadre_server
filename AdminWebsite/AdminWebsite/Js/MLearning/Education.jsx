﻿//const plPreviewStyle = {
//    width: "500px",
//    margin: "0px auto",
//    z-index: "9001", 
//    left: "50%",
//    top: "50%",
//    display: "none",
//    object-fit: "contain !important",
//    transform: "translate(-50 %, -50 %)",
//            max - width: "600px",
//                max - height: "80%",
//                    position: "absolute",
//                        overflow: "auto"
//};

class PreviewEducation extends React.Component {
    render() {
        return (
            <div className="popup" style={{ width: "500px", margin: "0px auto", display: "none" }}>
                <h1 className="popup__title">Preview</h1>
                <div className="topicicons">
                    Content
                </div>
                <div className="popup__action">
                    <a className="popup__action__item popup__action__item--cancel">CANCEL</a>
                </div>
            </div >
        );
    }
}


ReactDOM.render(
    <PreviewEducation />,
    document.getElementById('plPreview')
);
