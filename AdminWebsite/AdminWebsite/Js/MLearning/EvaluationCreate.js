﻿var selectedIcon = null;

function Icon() {
    this.Id = "";
    this.Url = "";
    this.ColorCode = "";
}

function showTip(type) {
    switch (type) {
        case 1:
            $(".form__row .duration_tip").css('display', 'block');
            $(".form__row .duration_tip").css('opacity', '1');
            break;
        default:
            break;
    }
}

function hideTip(type) {
    switch (type) {
        case 1:
            $(".form__row .duration_tip").css('display', 'none');
            $(".form__row .duration_tip").css('opacity', '0');
            break;
        default:
            break;
    }
}

function rebindJs() {
    $("#main_content_tbStartDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd/mm/yy'
    });


    $("#main_content_tbEndDate").datepicker({
        beforeShow: function () {
            setTimeout(function () {
                $('.ui-datepicker').css('z-index', 99999);
            }, 0);
        },
        dateFormat: 'dd/mm/yy'
    });
}

function showPopup(actionType) {
    $("#mpe_backgroundElement").show();

    switch (actionType) {
        case 1: // Icon Generater
            $("#dvEditIcon").show();
            $("#colorPicker").spectrum({
                color: "red",
                showButtons: false
            });

            var Icons = [];
            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_1.png");
            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_2.png");
            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_3.png");
            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_4.png");
            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_5.png");

            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_5.png");
            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_5.png");
            Icons.push("https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_5.png");

            var html = "";
            for (var i = 0; i < Icons.length; i++) {
                html += "<div style=\"border-radius: 5px; display: inline-block; padding: 30px; cursor: pointer; " + ((selectedIcon.Url == Icons[i].Url) ? "background-color: rgba(204, 204, 204, 1);" : "") + " " + ((i == 1) ? "margin: 0px 40px;" : "") + "\" onclick=\"setBadgeStyle(" + (i + 1) + ")\">";
                html += "   <img src=\"" + Icons[i] + "\" style=\"background-color: #" + selectedIcon.ColorCode + "; height: 60px;\" />";
                html += "</div>";
            }

            $(".badgeStyleList").html(html);

            break;

        default:
            break;
    }
}

function hidePopup(actionType) {
    $("#mpe_backgroundElement").hide();

    switch (actionType) {
        case 1: // Icon Generater
            $("#dvEditIcon").hide();
            break;

        default:
            break;
    }
}