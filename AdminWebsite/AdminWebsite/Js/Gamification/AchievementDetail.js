﻿// ACHIEVEMENT_TYPE constants -
const ACHIEVEMENT_TYPE = {};
Object.defineProperty(ACHIEVEMENT_TYPE, "PLAY_TO_GET", { get: function () { return 1; } });
Object.defineProperty(ACHIEVEMENT_TYPE, "FAIL_BUT_GET", { get: function () { return 2; } });
Object.defineProperty(ACHIEVEMENT_TYPE, "PERFECT_AND_GET", { get: function () { return 3; } });

// ACHIEVEMENT_RULE constants -
const ACHIEVEMENT_RULE = {};
Object.defineProperty(ACHIEVEMENT_RULE, "PLAY_TO_GET_ALL_TOPICS", { get: function () { return 1; } });
Object.defineProperty(ACHIEVEMENT_RULE, "PLAY_TO_GET_SPECIFIC_TOPIC", { get: function () { return 2; } });
Object.defineProperty(ACHIEVEMENT_RULE, "PLAY_TO_GET_MULTIPLE_TOPICS", { get: function () { return 3; } });
Object.defineProperty(ACHIEVEMENT_RULE, "FAIL_BUT_GET_ALL_TOPICS", { get: function () { return 4; } });
Object.defineProperty(ACHIEVEMENT_RULE, "PERFECT_AND_GET_SPECIFIC_TOPIC", { get: function () { return 5; } });
Object.defineProperty(ACHIEVEMENT_RULE, "PERFECT_AND_GET_MULTIPLE_TOPICS", { get: function () { return 6; } });

// BADGE_COLOR constants -
const BADGE_COLOR = {};
Object.defineProperty(BADGE_COLOR, "ORANGE", { get: function () { return 1; } });
Object.defineProperty(BADGE_COLOR, "YELLOW", { get: function () { return 2; } });
Object.defineProperty(BADGE_COLOR, "PINK", { get: function () { return 3; } });
Object.defineProperty(BADGE_COLOR, "RED", { get: function () { return 4; } });
Object.defineProperty(BADGE_COLOR, "PURPLE", { get: function () { return 5; } });
Object.defineProperty(BADGE_COLOR, "BLUE", { get: function () { return 6; } });
Object.defineProperty(BADGE_COLOR, "LIGHTBLUE", { get: function () { return 7; } });
Object.defineProperty(BADGE_COLOR, "LIGHTGREEN", { get: function () { return 8; } });
Object.defineProperty(BADGE_COLOR, "GREEN", { get: function () { return 9; } });
//Object.defineProperty(BADGE_COLOR, "GREY", { get: function () { return 10; } });

const BADGE_COLOR_LENGTH = 9;
const BADGE_COLOR_ARRAY = ["0", "orange", "yellow", "pink", "red", "purple", "blue", "lightblue", "lightgreen", "green"];
const BADGE_COLOR_CODE_ARRAY = ["#FFFFFF", "#FF9600", "#FFBB00", "#FF2851", "#C1272D", "#AC00FF", "#0095FF", "#54C7FC", "#95D000", "#5EAA00", "#808080"];

// BADGE_STYLE constants -
const BADGE_STYLE = {};
Object.defineProperty(BADGE_STYLE, "WAVEYWITHSOLIDLINE", { get: function () { return 1; } });
Object.defineProperty(BADGE_STYLE, "CURVEY", { get: function () { return 2; } });
Object.defineProperty(BADGE_STYLE, "WAVEYWITHDASHEDLINE", { get: function () { return 3; } });

const BADGE_STYLE_LENGTH = 3;
const BADGE_STYLE_ARRAY = ["0", "a", "c", "b"];

// MULTI_TOPIC_MODE constants -
const MULTI_TOPIC_MODE = {};
Object.defineProperty(MULTI_TOPIC_MODE, "EITHER_TOPIC", { get: function () { return 1; } });
Object.defineProperty(MULTI_TOPIC_MODE, "EACH_TOPIC", { get: function () { return 2; } });

// TEXT_MAX constants -
const TEXT_MAX = {}
Object.defineProperty(TEXT_MAX, "TITLE", { get: function () { return 20; } });

// COUNT_MAX constants -
const COUNT_MAX = {}
Object.defineProperty(COUNT_MAX, "PLAY_TO_GET_ALL_TOPICS", { get: function () { return 9999; } });
Object.defineProperty(COUNT_MAX, "PLAY_TO_GET_SPECIFIC_TOPIC", { get: function () { return 999; } });
Object.defineProperty(COUNT_MAX, "PLAY_TO_GET_MULTIPLE_TOPICS", { get: function () { return 999; } });
Object.defineProperty(COUNT_MAX, "FAIL_BUT_GET_ALL_TOPICS", { get: function () { return 9999; } });
/******************************/
var achievement = undefined;


function Achievement() {
    this.Id = "";
    this.Title = "";
    this.Type = -1;
    this.Rule = -1;
    this.TargetTopics = [];
    this.BadgeColor = 0;
    this.BadgeStyle = 0;
    this.IsBadgeCustomized = false;
    this.TempBadgeColor = 1;
    this.TempBadgeStyle = 1;
    this.IsForEach = false;
    this.PlayTimes = 1;
    this.FailTimes = 1;
    this.FileContent = null;
}

function Topic() {
    this.Id = "";
    this.Title = "";
}

function BadgeFileContent() {
    this.Url = "";
    this.IsNewFile = false;
    this.Base64 = "";
    this.Extension = "";
    this.Width = -1;
    this.Height = -1;
}

(function ($) {
    "use strict";

    $(document).ready(function () {
        if ($("#main_content_hfAchievementId").val() == null || $("#main_content_hfAchievementId").val() == "") // Create mode
        {
            $(".action_name").html("Create a new achievement");

            achievement = new Achievement();
            achievement.Type = ACHIEVEMENT_TYPE.PLAY_TO_GET;
            achievement.Rule = ACHIEVEMENT_RULE.PLAY_TO_GET_ALL_TOPICS;
            achievement.TargetTopics = [];
            achievement.BadgeColor = BADGE_COLOR.ORANGE;
            achievement.BadgeStyle = BADGE_STYLE.WAVEYWITHSOLIDLINE;
            achievement.IsBadgeCustomized = false;
            achievement.TempBadgeColor = BADGE_COLOR.ORANGE;
            achievement.TempBadgeStyle = BADGE_STYLE.WAVEYWITHSOLIDLINE;
            achievement.PlayTimes = 1;
            achievement.FailTimes = 1;
            achievement.IsForEach = false;
            achievement.FileContent = new BadgeFileContent();
            achievement.FileContent.IsNewFile = true;

            $(".achievement_container").html(getAchievementHtml());
            $(".create_achievement").show();
            drawBadge();
        }
        else // Edit mode
        {
            $(".action_name").html("Edit achievement");
            $(".edit_achievement").show();
            getAchievementDetail();
        }
    });

}(jQuery));

function changeType(element) {

    switch (parseInt(element.value)) {
        case ACHIEVEMENT_TYPE.PLAY_TO_GET:
            achievement.Type = ACHIEVEMENT_TYPE.PLAY_TO_GET;
            achievement.Rule = ACHIEVEMENT_RULE.PLAY_TO_GET_ALL_TOPICS;
            achievement.TargetTopics = [];
            achievement.PlayTimes = 1;
            achievement.FailTimes = 1;
            achievement.IsForEach = false;
            break;

        case ACHIEVEMENT_TYPE.FAIL_BUT_GET:
            achievement.Type = ACHIEVEMENT_TYPE.FAIL_BUT_GET;
            achievement.Rule = ACHIEVEMENT_RULE.FAIL_BUT_GET_ALL_TOPICS;
            achievement.TargetTopics = [];
            achievement.PlayTimes = 1;
            achievement.FailTimes = 1;
            achievement.IsForEach = false;
            break;

        case ACHIEVEMENT_TYPE.PERFECT_AND_GET:
            achievement.Type = ACHIEVEMENT_TYPE.PERFECT_AND_GET;
            achievement.Rule = ACHIEVEMENT_RULE.PERFECT_AND_GET_SPECIFIC_TOPIC;
            achievement.TargetTopics = [];
            achievement.PlayTimes = 1;
            achievement.FailTimes = 1;
            achievement.IsForEach = false;
            break;

        default:
            // do nothing.
            break;
    }

    $(".achievement_container").html(getAchievementHtml());
    autocomplete();
}

function changeRule(achievementType, element) {
    achievement.Type = achievementType;
    achievement.Rule = parseInt(element.value);

    $(".achievement_container").html(getAchievementHtml());
    autocomplete();
}

function getAchievementHtml() {

    var html = "";
    if (achievement.Rule > 0 && achievement.Rule < 4) // Type: Play to get. Rule: All topics, Specific topic and Multiple topics.
    {
        html += "		<!-- Type: Play to get -->";
        html += "		<div class=\"type_play_to_get\">";
        html += "			<label style=\"width: 60px; text-align: right; color: rgba(135, 135, 135, 1); font-size: 1.5em; vertical-align: top; display: inline-block; margin-top: 10px;\">Rules</label>";
        html += "		    <div style=\"margin-left: 5%; display: inline-block; width: 80%;\">";
        html += "		        <div class=\"mdl-selectfield\" style=\"width: 100%;\">";
        html += "			        <select onchange=\"changeRule(" + ACHIEVEMENT_TYPE.PLAY_TO_GET + ", this);\" style=\"margin-bottom: 0px;\">";
        html += "				        <option value=\"" + ACHIEVEMENT_RULE.PLAY_TO_GET_ALL_TOPICS + "\" " + ((achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_ALL_TOPICS) ? " selected=\"selected\"" : "") + ">All topics</option>";
        html += "				        <option value=\"" + ACHIEVEMENT_RULE.PLAY_TO_GET_SPECIFIC_TOPIC + "\" " + ((achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_SPECIFIC_TOPIC) ? " selected=\"selected\"" : "") + ">Specific topic</option>";
        html += "				        <option value=\"" + ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS + "\" " + ((achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS) ? " selected=\"selected\"" : "") + ">Multiple topics</option>";
        html += "			        </select>";
        html += "			    </div>";

        if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_ALL_TOPICS) {
            html += "			    <!-- Rule: all topics -->";
            html += "			    <div class=\"rules_all_topics\" style=\"margin-top: 10px;\">";
            html += "				    <label style=\"display: inline-block;\">Play any game for </label>";
            html += "				    <input type=\"text\" style=\"width: 50px; display: inline-block;\" onkeypress=\"return allowOnlyNumber(event);\" onblur=\"setPlayTimes(this.value);\" value=\"" + achievement.PlayTimes + "\" />";
            html += "				    <label style=\"display: inline-block;\">times</label>";
            html += "			    </div>";
            html += "			    <!-- /Rule: all topics -->";
        }
        else {
            if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_SPECIFIC_TOPIC) {
                html += "			<!-- Rule: specific topic -->";
                html += "			<div class=\"rules_specific_topic\" style=\"margin-top: 10px;\">";
                html += "				<div>";
                html += "					<label style=\"display: inline-block;\">Play game for </label>";
                html += "					<input type=\"text\" style=\"width: 50px; display: inline-block;\" onkeypress=\"return allowOnlyNumber(event);\" onblur=\"setPlayTimes(this.value);\" value=\"" + achievement.PlayTimes + "\" />";
                html += "					<label style=\"display: inline-block;\">times</label>";
                html += "				</div>";
            }
            else if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS) {
                html += "			<!-- Rule: multiple topics -->";
                html += "			<div class=\"rules_specific_topic\" style=\"margin-top: 10px;\">";
                html += "				<div>";
                html += "					<label style=\"display: inline-block; vertical-align: middle;\">Play</label>";
                html += "		            <div class=\"mdl-selectfield\" style=\"width: 150px; display: inline-block; vertical-align: middle;\">";
                html += "			            <select style=\"width: 100%; margin-bottom:0px;\" onchange=\"setMultiTopicMode(this);\">";
                html += "				            <option value=\"" + MULTI_TOPIC_MODE.EITHER_TOPIC + "\" " + (achievement.IsForEach ? "" : " selected") + ">either topic</option>";
                html += "				            <option value=\"" + MULTI_TOPIC_MODE.EACH_TOPIC + "\" " + (achievement.IsForEach ? " selected" : "") + ">each topic</option>";
                html += "			            </select>";
                html += "				    </div>";
                html += "					<label style=\"display: inline-block; vertical-align: middle;\">for</label>";
                html += "					<input type=\"text\" style=\"width: 50px; display: inline-block; vertical-align: middle; margin-bottom: 0px;\" onkeypress=\"return allowOnlyNumber(event);\" onblur=\"setPlayTimes(this.value);\" value=\"" + achievement.PlayTimes + "\" />";
                html += "					<label style=\"display: inline-block; vertical-align: middle;\">times</label>";
                html += "				</div>";
            }

            html += "				<div class=\"tags\" style=\"margin-top: 20px; max-width: 450px;\">";
            for (var i = 0; i < achievement.TargetTopics.length; i++) {
                html += "					<div class=\"tag\">";
                html += "						<span class=\"personnel-tag\" style=\"padding-left:10px;\">" + achievement.TargetTopics[i].Title + "</span>";
                html += "						<a class=\"tag__icon\" href=\"javascript:removeTargetTopic('" + achievement.TargetTopics[i].Id + "');\">x</a>";
                html += "					</div>";
            }
            html += "				</div>";
            html += "				<div style=\"margin-top: 20px;\">";
            html += "					<input class=\"search_topic\" type=\"text\" style=\"width: 200px; display: inline-block;\" placeholder=\"Search topic\" />";
            html += "				</div>";
            html += "			</div>";

            if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_SPECIFIC_TOPIC) {
                html += "			<!-- /Rule: specific topic -->";
            }
            else if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS) {
                html += "			<!-- /Rule: multiple topics -->";
            }
        }

        html += "			</div>";
        html += "		</div>";
        html += "		<!-- /Type: Play to get -->";
    }
    else if (achievement.Rule == ACHIEVEMENT_RULE.FAIL_BUT_GET_ALL_TOPICS) // Type: Fail but get. Rule: all topics.
    {
        html += "		<!-- Type: Fail but get -->";
        html += "		<div class=\"type_fail_but_get\">";
        html += "			<label style=\"width: 60px; text-align: right; color: rgba(135, 135, 135, 1); font-size: 1.5em; vertical-align: top; display: inline-block; margin-top: 10px;\">Rules</label>";
        html += "		    <div style=\"margin-left: 5%; display: inline-block; width: 80%;\">";
        html += "			    <!-- Rule: All topics -->";
        html += "			    <div class=\"rule_fail_but_get_all_topics\" style=\"margin-top: 10px;\">";
        html += "				    <label>Never say die. Keep trying.</label>";
        html += "			        <div>";
        html += "				        <label style=\"display: inline-block;\">Failed</label>";
        html += "				        <input type=\"text\" style=\"width: 50px; display: inline-block;\" onkeypress=\"return allowOnlyNumber(event);\" onblur=\"setFailTimes(this.value);\" value=\"" + achievement.FailTimes + "\" />";
        html += "				        <label style=\"display: inline-block;\">streaks</label>";
        html += "			        </div>";
        html += "			    </div>";
        html += "			    <!-- /Rule: All topics -->";
        html += "			</div>";
        html += "		</div>";
        html += "		<!-- /Type: Fail but get -->";
    }
    else if (achievement.Rule > ACHIEVEMENT_RULE.FAIL_BUT_GET_ALL_TOPICS) // Type: Perfect and get. Rule: Specific topics.
    {
        html += "		<!-- Type: Perfect and get -->";
        html += "		<div class=\"type_Perfect_and_get\">";
        html += "			<label style=\"width: 60px; text-align: right; color: rgba(135, 135, 135, 1); font-size: 1.5em; vertical-align: top; display: inline-block; margin-top: 10px;\">Rules</label>";
        html += "		    <div style=\"margin-left: 5%; display: inline-block; width: 80%;\">";
        html += "		        <div class=\"mdl-selectfield\" style=\"width: 100%;\">";
        html += "			        <select onchange=\"changeRule(" + ACHIEVEMENT_TYPE.PERFECT_AND_GET + ", this);\">";
        html += "				        <option value=\"" + ACHIEVEMENT_RULE.PERFECT_AND_GET_SPECIFIC_TOPIC + "\" " + ((achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_SPECIFIC_TOPIC) ? " selected=\"selected\"" : "") + ">Specific topic</option>";
        html += "				        <option value=\"" + ACHIEVEMENT_RULE.PERFECT_AND_GET_MULTIPLE_TOPICS + "\" " + ((achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_MULTIPLE_TOPICS) ? " selected=\"selected\"" : "") + ">Multiple topic</option>";
        html += "			        </select>";
        html += "			    </div>";

        if (achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_SPECIFIC_TOPIC) {
            html += "			    <!-- Rule: Specific topics -->";
            html += "			    <div class=\"rule_perfect_and_get_specific_topics\" style=\"margin-top: 10px;\">";
            html += "			        <div>";
            html += "				        <label style=\"display: inline-block;\">Achieve Perfect Score for</label>";
            html += "			        </div>";
        }
        else if (achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_MULTIPLE_TOPICS) {
            html += "			    <!-- Rule: Multiple topics -->";
            html += "			    <div class=\"rule_perfect_and_get_multiple_topics\" style=\"margin-top: 10px;\">";
            html += "			        <div>";
            html += "				        <label style=\"display: inline-block; vertical-align: middle;\">Achieve Perfect Score for</label>";
            html += "                       <div class=\"mdl-selectfield\" style=\"width: 150px; vertical-align: middle; display: inline-block;\">";
            html += "			                <select style=\" margin-bottom: 0px; width: 100%;\" onchange=\"setMultiTopicMode(this);\">";
            html += "			                    <option value=\"" + MULTI_TOPIC_MODE.EITHER_TOPIC + "\" " + (achievement.IsForEach ? "" : " selected") + ">either topic</option>";
            html += "				                <option value=\"" + MULTI_TOPIC_MODE.EACH_TOPIC + "\" " + (achievement.IsForEach ? " selected" : "") + ">each topic</option>";
            html += "			                </select>";
            html += "			            </div>";
            html += "			        </div>";
        }
        else {
            // do nothing
        }

        html += "				    <div class=\"tags\" style=\"margin-top: 20px; max-width: 450px;\">";
        for (var i = 0; i < achievement.TargetTopics.length; i++) {
            html += "					    <div class=\"tag\">";
            html += "						    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + achievement.TargetTopics[i].Title + "</span>";
            html += "						    <a class=\"tag__icon\" href=\"javascript:removeTargetTopic('" + achievement.TargetTopics[i].Id + "');\">x</a>";
            html += "					    </div>";
        }
        html += "				    </div>";
        html += "				    <div style=\"margin-top: 20px;\">";
        html += "					    <input class=\"search_topic\" type=\"text\" style=\"width: 200px; display: inline-block;\" placeholder=\"Search topic\" />";
        html += "				    </div>";
        html += "			    </div>";

        if (achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_SPECIFIC_TOPIC) {
            html += "			    <!-- /Rule: Specific topics -->";
        }
        else if (achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_MULTIPLE_TOPICS) {
            html += "			    <!-- /Rule: Multiple topics -->";
        }
        else {
            // do nothing
        }

        html += "			</div>";
        html += "		</div>";
        html += "		<!-- /Type: Perfect and get -->";
    }
    else {
        // do nothing
    }

    return html;
}

function autocomplete() {
    $(".search_topic").autocomplete({
        source: function (request, callback) {
            var data = {
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'ManagerId': $("#main_content_hfManagerId").val(),
                'ContainsName': request.term
            };
            $.ajax({
                url: "/Api/MatchUp/GetList",
                data: data,
                dataType: "json",
                type: "POST",
                success: function (r) {
                    if (r.Success) {
                        callback(r.Topics);
                    }
                }
            });
        },

        minLength: 1,
        response: function (event, ui) {
            if (ui != null) {
                for (var i = 0; i < ui.content.length; i++) {
                    for (var j = 0; j < achievement.TargetTopics.length; j++) {
                        if (achievement.TargetTopics[j].Id == ui.content[i].topic_id) {
                            ui.content.splice(i, 1);
                            if (i != ui.content.length - 1) {
                                i--;
                            }
                            break;
                        }
                    }
                }
            }

        },
        select: function (event, ui) {
            var isExisting = false;
            for (var i = 0; i < achievement.TargetTopics.length; i++) {
                if (achievement.TargetTopics[i].Id == ui.item.value) {
                    isExisting = true;
                    break;
                }
            }

            if (!isExisting) {
                var topic = new Topic();
                topic.Title = ui.item.label;
                topic.Id = ui.item.value;
                achievement.TargetTopics.push(topic);

                var html = "";
                html = "";
                html = html + "<div class=\"tag " + ui.item.value + "\">";
                html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + ui.item.label + "</span>";
                html = html + "    <a class=\"tag__icon\" href=\"javascript:removeTargetTopic('" + ui.item.value + "');\">x</a>";
                html = html + "</div>";
                $(".tags").append(html);
            }
            return false;
        },
        focus: function (event, ui) {
            event.preventDefault();
        },
        close: function (event, ui) {
            $(".search_topic").val("");
        }
    });

}

function removeTargetTopic(topicId) {
    var index = -1;
    for (var i = 0; i < achievement.TargetTopics.length; i++) {
        if (achievement.TargetTopics[i].Id == topicId) {
            index = i;
        }
    }

    if (index > -1) {
        achievement.TargetTopics.splice(index, 1);

        var html = "";
        for (var i = 0; i < achievement.TargetTopics.length; i++) {
            html += "					    <div class=\"tag\">";
            html += "						    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + achievement.TargetTopics[i].Title + "</span>";
            html += "						    <a class=\"tag__icon\" href=\"javascript:removeTargetTopic('" + achievement.TargetTopics[i].Id + "');\">x</a>";
            html += "					    </div>";
        }
        $(".tags").html(html);
    }
}

function setPlayTimes(playTimes) {
    try {
        playTimes = parseInt(playTimes);
        if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_ALL_TOPICS) {
            if (playTimes < 1 || playTimes > COUNT_MAX.PLAY_TO_GET_ALL_TOPICS) {
                ShowToast("Please input a number between 1 to " + COUNT_MAX.PLAY_TO_GET_ALL_TOPICS, 2);
                return;
            }
        }
        else if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_SPECIFIC_TOPIC) {
            if (playTimes < 1 || playTimes > COUNT_MAX.PLAY_TO_GET_SPECIFIC_TOPIC) {
                ShowToast("Please input a number between 1 to " + COUNT_MAX.PLAY_TO_GET_SPECIFIC_TOPIC, 2);
                return;
            }
        }
        else if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS) {
            if (playTimes < 1 || playTimes > COUNT_MAX.PLAY_TO_GET_MULTIPLE_TOPICS) {
                ShowToast("Please input a number between 1 to " + COUNT_MAX.PLAY_TO_GET_MULTIPLE_TOPICS, 2);
                return;
            }
        }
        else {
            // do nothing.
            return;
        }

        achievement.PlayTimes = playTimes;
    } catch (e) {
        ShowToast("Please type in a positive number", 2);
    }
}

function setFailTimes(failTimes) {
    try {
        failTimes = parseInt(failTimes);
        if (achievement.Rule == ACHIEVEMENT_RULE.FAIL_BUT_GET_ALL_TOPICS) {
            if (failTimes < 1 || failTimes > COUNT_MAX.FAIL_BUT_GET_ALL_TOPICS) {
                ShowToast("Please input a number between 1 to " + COUNT_MAX.FAIL_BUT_GET_ALL_TOPICS, 2);
                return;
            }
        }
        else {
            // do nothing.
            return;
        }

        achievement.FailTimes = failTimes;
    } catch (e) {
        ShowToast("Please type in a positive number", 2);
    }
}

function setMultiTopicMode(element) {
    try {
        achievement.IsForEach = (element.value == MULTI_TOPIC_MODE.EACH_TOPIC);
    } catch (e) {

    }
}


function setBadgeText(text) {
    if (text.length > TEXT_MAX.TITLE) {
        text = text.substring(0, TEXT_MAX.TITLE);
    }

    achievement.Title = text;
    $(".badge_title").val(text);

    if (!achievement.IsIconCustomized) {
        $(".badge_text").html(text);
        achievement.FileContent.IsNewFile = true;
        drawBadge();
    }
}

function showChoiceBadgePopup() {
    $(".badgeColorList").html(getBadgeColorHtml());
    $(".badgeStyleList").html(getBadgeStyleHtml());

    $("#mpe_backgroundElement").show();
    $("#plBadge").show();
}

function hideChoiceBadgePopup() {
    $("#mpe_backgroundElement").hide();
    $("#plBadge").hide();
}

function showCustomizeBadgePopup() {
    $("#mpe_backgroundElement").show();
    $("#plCustomizeBadge").show();
}

function hideCustomizeBadgePopup() {
    $('#uploadCrop').croppie('destroy');
    $("#mpe_backgroundElement").hide();
    $("#plCustomizeBadge").hide();
}


function setBadgeColor(badgeColor) {
    achievement.TempBadgeColor = badgeColor;
    $(".badgeColorList").html(getBadgeColorHtml());
    $(".badgeStyleList").html(getBadgeStyleHtml());
}

function getBadgeColorHtml() {
    var html = "";
    for (var i = 0; i < BADGE_COLOR_LENGTH; i++) {
        if ((i + 1) == BADGE_COLOR.ORANGE) {
            html += "<div class=\"badge_color orange\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.ORANGE) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.ORANGE + ");\">";
            html += "   <div style=\"background-color: rgba(255, 150, 0, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.YELLOW) {
            html += "<div class=\"badge_color yellow\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.YELLOW) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.YELLOW + ");\">";
            html += "   <div style=\"background-color: rgba(255, 187, 0, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.PINK) {
            html += "<div class=\"badge_color pink\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.PINK) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.PINK + ");\">";
            html += "   <div style=\"background-color: rgba(255, 40, 81, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.RED) {
            html += "<div class=\"badge_color red\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.RED) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.RED + ");\">";
            html += "   <div style=\"background-color: rgba(193, 39, 45, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.PURPLE) {
            html += "<div class=\"badge_color purple\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.PURPLE) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.PURPLE + ");\">";
            html += "   <div style=\"background-color: rgba(172, 0, 255, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.BLUE) {
            html += "<div class=\"badge_color blue\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.BLUE) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.BLUE + ");\">";
            html += "   <div style=\"background-color: rgba(0, 149, 255, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.LIGHTBLUE) {
            html += "<div class=\"badge_color lightblue\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.LIGHTBLUE) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.LIGHTBLUE + ");\">";
            html += "   <div style=\"background-color: rgba(84, 199, 252, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.LIGHTGREEN) {
            html += "<div class=\"badge_color lightgreen\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.LIGHTGREEN) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.LIGHTGREEN + ");\">";
            html += "   <div style=\"background-color: rgba(149, 208, 0, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.GREEN) {
            html += "<div class=\"badge_color green\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.GREEN) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.GREEN + ");\">";
            html += "   <div style=\"background-color: rgba(94, 170, 0, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else if ((i + 1) == BADGE_COLOR.GREY) {
            html += "<div class=\"badge_color grey\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.GREY) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.GREY + ");\">";
            html += "   <div style=\"background-color: rgba(128, 128, 128, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }
        else {
            // do nothing. Default color is grey.
            html += "<div class=\"badge_color grey\" style=\"" + ((achievement.TempBadgeColor == BADGE_COLOR.GREY) ? "border: 1px solid rgba(171, 171, 171, 1);" : "") + " display: inline-block; margin:3px; cursor: pointer;\" onclick=\"setBadgeColor(" + BADGE_COLOR.GREY + ");\">";
            html += "   <div style=\"background-color: rgba(128, 128, 128, 1); width: 75px; height: 25px; margin: 5px;\"></div>";
        }

        html += "</div>";
    }

    return html;
}

function getBadgeStyleHtml() {
    var html = "";
    for (var i = 0; i < BADGE_STYLE_LENGTH ; i++) {
        html += "<div style=\"border-radius: 5px; width: 230px; height: 260px; display: inline-block; padding: 30px; cursor: pointer; " + ((achievement.TempBadgeStyle == (i + 1)) ? "background-color: rgba(204, 204, 204, 1);" : "") + " " + ((i == 1) ? "margin: 0px 40px;" : "") + "\" onclick=\"setBadgeStyle(" + (i + 1) + ")\">";
        html += "   <img src=\"/Img/Gamification/Achievement/badge_" + BADGE_COLOR_ARRAY[achievement.TempBadgeColor] + "_" + BADGE_STYLE_ARRAY[(i + 1)] + ".png\" />";
        html += "</div>";
    }
    return html;
}

function setBadgeStyle(badgeStyle) {
    achievement.TempBadgeStyle = badgeStyle;
    $(".badgeColorList").html(getBadgeColorHtml());
    $(".badgeStyleList").html(getBadgeStyleHtml());
}

function confirmBadge() {
    achievement.BadgeColor = achievement.TempBadgeColor;
    achievement.BadgeStyle = achievement.TempBadgeStyle;
    achievement.IsBadgeCustomized = false;
    var fileContent = new BadgeFileContent();
    fileContent.IsNewFile = true;
    fileContent.Url = "/Img/Gamification/Achievement/badge_" + BADGE_COLOR_ARRAY[achievement.BadgeColor] + "_" + BADGE_STYLE_ARRAY[achievement.BadgeStyle] + ".png";
    achievement.FileContent = fileContent;

    drawBadge();
    hideChoiceBadgePopup();
}

function drawBadge() {
    if (!achievement.IsBadgeCustomized) {
        var canvas = document.getElementById('badgeCanvas');
        var context = canvas.getContext('2d');
        var image = new Image();

        image.onload = function () {
            var w = this.width,
                h = this.height;
            canvas.width = w;
            canvas.height = h;
            context.drawImage(this, 0, 0, w, h);

            if (!achievement.IsBadgeCustomized) {
                context.font = "40pt  Calibri";
                context.fillStyle = "white";
                context.textAlign = "center";
                var lineHeight = 70;
                var offsetHeight = 20;
                switch (achievement.BadgeStyle) {

                    case BADGE_STYLE.WAVEYWITHSOLIDLINE:
                        offsetHeight = 20;
                        break;

                    case BADGE_STYLE.CURVEY:
                        offsetHeight = 30;
                        break;

                    case BADGE_STYLE.WAVEYWITHDASHEDLINE:
                        offsetHeight = 30;
                        break;
                    default:
                        break;
                }

                // 計算要顯示的字
                var maxWidth = 380;
                var words = achievement.Title.split('');
                var lineWords = '';
                var lineCount = 1;
                var lineWordsArray = [];

                for (var n = 0; n < words.length; n++) {
                    var tempLine = lineWords + words[n] + '';
                    var metrics = context.measureText(tempLine);
                    if (metrics.width > maxWidth && n > 0) {
                        // next line
                        lineCount++;
                        lineWords = '' + words[n];
                    }
                    else {
                        lineWords = tempLine;
                    }
                    lineWordsArray[lineCount - 1] = lineWords;
                }
                // 計算要顯示的字

                var textStartY = (h / 2 - lineHeight) - lineWordsArray.length * offsetHeight;

                for (var i = 0; i < lineWordsArray.length; i++) {
                    textStartY = textStartY + lineHeight;
                    context.fillText(lineWordsArray[i], w / 2, textStartY);
                }

                $("#badgeImg").hide();
                $("#badgeCanvas").show();
            }
        }

        image.src = "/Img/Gamification/Achievement/badge_" + BADGE_COLOR_ARRAY[achievement.BadgeColor] + "_" + BADGE_STYLE_ARRAY[achievement.BadgeStyle] + ".png";
    }
    else {
        if (achievement.FileContent.IsNewFile) {
            $("#badgeImg").prop("src", achievement.FileContent.Base64);
        }
        else {
            $("#badgeImg").prop("src", achievement.FileContent.Url);
        }
        $("#badgeImg").show();
        $("#badgeCanvas").hide();
    }

}

function choiceImg(element) {
    if (element.files && element.files[0]) {
        var file = element.files[0];
        if (file.type.toLowerCase() == "image/png" || file.type.toLowerCase() == "image/jpeg" || file.type.toLowerCase() == "image/jpg") {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    var imgWidth = this.width;
                    var imgHeight = this.height;
                    var imgType = file.type;
                    var imgBlob = window.URL.createObjectURL(file);
                    var imgBase64 = e.target.result;

                    var crop = $('#uploadCrop');
                    crop.croppie({
                        enableExif: true,
                        viewport: {
                            width: 375,
                            height: 375,
                            type: 'circle'
                        },
                        boundary: {
                            width: 500,
                            height: 500
                        },
                        url: imgBase64
                    });

                    $('.cr-slider').css("display", "inline-block");

                    showCustomizeBadgePopup();
                };
            }

            fileReader.readAsDataURL(file);

            $(".choiceImg").val("");
        }
        else {
            ShowToast("Supported format: *.png, *.jpg", 2);
        }
    }
    else {
        ShowToast("No file be selected.");
        console.info("No file be selected.");
    }
}

function cropBadge() {
    var crop = $('#uploadCrop');
    crop.croppie('result', {
        type: 'canvas',
        format: 'viewport'
    }).then(function (resp) {
        achievement.IsBadgeCustomized = true;
        achievement.FileContent = new BadgeFileContent();
        achievement.FileContent.IsNewFile = true;
        achievement.FileContent.Base64 = resp;
        achievement.FileContent.Extension = resp.split(";")[0].replace("data:image/", "");
        achievement.FileContent.Width = 750;
        achievement.FileContent.Height = 750;
        drawBadge();
    });

    hideCustomizeBadgePopup();
}

function updateUI() {
    // Title
    $('.badge_title').val(achievement.Title);
    $('.letterCount').html(TEXT_MAX.TITLE - achievement.Title.length);

    // Type
    $(".achievement_type").children().each(function () {
        if (parseInt($(this).val()) == achievement.Type) {
            this.selected = true;
        }
    });

    // Rule
    $(".achievement_container").html(getAchievementHtml());

    drawBadge();

    autocomplete();
}



/***********************************/

function isDataValid() {
    // Title
    if (achievement.Title == null || achievement.Title.length == 0) {
        ShowToast("Please fill in Title", 2);
        return false;
    }

    if (achievement.Title.length > TEXT_MAX.TITLE) {
        ShowToast("Title is longer than " + TEXT_MAX.TITLE + " letters", 2);
        return false;
    }

    // Rule
    if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_SPECIFIC_TOPIC) {
        if (achievement.TargetTopics.length == 0) {
            ShowToast("Please select a topic", 2);
            return false;
        }

        if (achievement.TargetTopics.length > 1) {
            ShowToast("This rule is for one topic only", 2);
            return false;
        }
    }
    else if (achievement.Rule == ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS) {
        if (achievement.TargetTopics.length == 0) {
            ShowToast("Please select a topic", 2);
            return false;
        }
    }
    else if (achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_SPECIFIC_TOPIC) {
        if (achievement.TargetTopics.length == 0) {
            ShowToast("Please select a topic", 2);
            return false;
        }

        if (achievement.TargetTopics.length > 1) {
            ShowToast("This rule is for one topic only", 2);
            return false;
        }
    }
    else if (achievement.Rule == ACHIEVEMENT_RULE.PERFECT_AND_GET_MULTIPLE_TOPICS) {
        if (achievement.TargetTopics.length == 0) {
            ShowToast("Please select a topic", 2);
            return false;
        }
    }

    // Image
    if (achievement.IsBadgeCustomized) {

    }

    return true;
}

function convertToViewModel(backendObject) {
    achievement = new Achievement();
    achievement.Id = backendObject.Id;
    achievement.Title = backendObject.Title;
    achievement.Type = backendObject.Type;

    if (backendObject.Type == ACHIEVEMENT_TYPE.PLAY_TO_GET) {
        if (backendObject.RuleType == 1) // AllTopics
        {
            achievement.Rule = ACHIEVEMENT_RULE.PLAY_TO_GET_ALL_TOPICS;
        }
        else if (backendObject.RuleType == 2) //SpecificTopic
        {
            achievement.Rule = ACHIEVEMENT_RULE.PLAY_TO_GET_SPECIFIC_TOPIC;
        }
        else if (backendObject.RuleType == 3) //MultipleTopicsByEither
        {
            achievement.Rule = ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS;
            achievement.IsForEach = false;
        }
        else if (backendObject.RuleType == 4) // MultipleTopicsByEach
        {
            achievement.Rule = ACHIEVEMENT_RULE.PLAY_TO_GET_MULTIPLE_TOPICS;
            achievement.IsForEach = true;
        }
        else {
            // do nothing
        }

        achievement.PlayTimes = backendObject.PlayTimes;
    }
    else if (backendObject.Type == ACHIEVEMENT_TYPE.FAIL_BUT_GET) {
        achievement.Rule = ACHIEVEMENT_RULE.FAIL_BUT_GET_ALL_TOPICS;
        achievement.FailTimes = backendObject.FailTimes;
    }
    else if (backendObject.Type == ACHIEVEMENT_TYPE.PERFECT_AND_GET) {
        if (backendObject.RuleType == 2) //SpecificTopic
        {
            achievement.Rule = ACHIEVEMENT_RULE.PERFECT_AND_GET_SPECIFIC_TOPIC;
        }
        else if (backendObject.RuleType == 3) //MultipleTopicsByEither
        {
            achievement.Rule = ACHIEVEMENT_RULE.PERFECT_AND_GET_MULTIPLE_TOPICS;
            achievement.IsForEach = false;
        }
        else if (backendObject.RuleType == 4) // MultipleTopicsByEach
        {
            achievement.Rule = ACHIEVEMENT_RULE.PERFECT_AND_GET_MULTIPLE_TOPICS;
            achievement.IsForEach = true;
        }
        else {
            // do nothing
        }
    }
    else {
        achievement = null;
    }

    // Target Topics
    if (backendObject.Topics != null && backendObject.Topics.length > 0) {
        achievement.TargetTopics = [];
        for (var i = 0; i < backendObject.Topics.length; i++) {
            var topic = new Topic();
            topic.Id = backendObject.Topics[i].TopicId;
            topic.Title = backendObject.Topics[i].TopicTitle;
            achievement.TargetTopics.push(topic);
        }
    }

    // Badge
    if (backendObject.IsIconCustomized) //Customized Badge
    {
        achievement.IsBadgeCustomized = true;

        var fileContent = new BadgeFileContent();
        fileContent.Url = backendObject.IconImageUrl;
        fileContent.IsNewFile = false;
        achievement.FileContent = fileContent;
    }
    else // Default Badge
    {
        achievement.IsBadgeCustomized = false;
        achievement.BadgeStyle = backendObject.TemplateType;
        achievement.TempBadgeStyle = backendObject.TemplateType;

        for (var i = 0; i < BADGE_COLOR_CODE_ARRAY.length; i++) {
            if (backendObject.IconColor == BADGE_COLOR_CODE_ARRAY[i]) {
                achievement.BadgeColor = i;
                achievement.TempBadgeColor = i;
                break;
            }
        }

        var fileContent = new BadgeFileContent();
        fileContent.Url = backendObject.IconImageUrl;
        fileContent.IsNewFile = false;
        achievement.FileContent = fileContent;
    }
}


function RequestObject() {
    this.CompanyId = $("#main_content_hfCompanyId").val();
    this.ManagerId = $("#main_content_hfManagerId").val();
    this.AchievementId = achievement.Id;
    this.Title = achievement.Title;
    this.Type = achievement.Type;
    this.Rule = achievement.Rule;
    this.IsIconCustomized = achievement.IsBadgeCustomized;
    this.BadgeStyle = achievement.BadgeStyle;
    this.BadgeColor = achievement.BadgeColor;
    this.IsForEach = achievement.IsForEach;
    this.PlayTimes = achievement.PlayTimes;
    this.FailTimes = achievement.FailTimes;

    if (achievement.FileContent.IsNewFile) {
        if (!achievement.IsBadgeCustomized) {
            var canvas = document.getElementById("badgeCanvas");
            achievement.FileContent.Base64 = canvas.toDataURL();
            achievement.FileContent.Extension = canvas.toDataURL().split(";")[0].replace("data:image/", "");
            achievement.FileContent.Width = canvas.width;
            achievement.FileContent.Height = canvas.height;
        }
    }
    this.UploadFile = achievement.FileContent;

    this.TargetTopicIds = [];
    for (var i = 0; i < achievement.TargetTopics.length; i++) {
        this.TargetTopicIds.push(achievement.TargetTopics[i].Id);
    }
}

///////// API ZONE /////////
function createAchievement() {
    try {
        if (!isDataValid()) {
            return;
        }

        var request = new RequestObject();
        jQuery.ajax({
            type: "POST",
            url: "/Api/Gamification/CreateAchievement",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            dataType: "json",
            crossDomain: true,
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                if (d.Success) {
                    ShowToast("Added Achievement", 1);
                    RedirectPage("/Gamification/AchievementList", 300);
                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });
    } catch (e) {
        console.error(e.toString());
    }
}

function getAchievementDetail() {
    try {

        var request = {
            'CompanyId': $("#main_content_hfCompanyId").val(),
            'ManagerId': $("#main_content_hfManagerId").val(),
            'AchievementId': $("#main_content_hfAchievementId").val()
        };

        jQuery.ajax({
            type: "POST",
            url: "/Api/Gamification/GetAchievementDetail",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            dataType: "json",
            crossDomain: true,
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                if (d.Success) {
                    convertToViewModel(d.Achievement);

                    if (achievement != null) {
                        updateUI();
                    }
                    else {
                        ShowToast("Error", 1);
                        RedirectPage("/Gamification/AchievementList", 300);
                    }

                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });
    } catch (e) {
        console.error(e.toString());
    }
}

function editAchievement() {
    try {
        if (!isDataValid()) {
            return;
        }

        var request = new RequestObject();
        jQuery.ajax({
            type: "POST",
            url: "/Api/Gamification/EditAchievement",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            dataType: "json",
            crossDomain: true,
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                if (d.Success) {
                    ShowToast("Edited Achievement", 1);
                    //RedirectPage("/Gamification/AchievementDetail/" + achievement.Id, 300);
                    RedirectPage("/Gamification/AchievementList", 300);
                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });
    } catch (e) {
        console.error(e.toString());
    }
}
///////// API ZONE /////////