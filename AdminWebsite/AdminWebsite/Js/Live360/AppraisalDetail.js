﻿////////////////////////////////////////////////////////////////////////////////
// (Global) -- (Global) -- (Global)  -- (Global)  -- (Global)  -- (Global)  -- (Global)  -- (Global) 

var timeZone = 0;
var appraisal = null;

////////////////////////////////////////
// Constants
// Progress constants -
const PROGRESS = {};
Object.defineProperty(PROGRESS, "LIVE", { get: function () { return 1; } });
Object.defineProperty(PROGRESS, "UPCOMING", { get: function () { return 2; } });
Object.defineProperty(PROGRESS, "COMPLETED", { get: function () { return 3; } });

// Status constants -
const STATUS = {};
Object.defineProperty(STATUS, "DELETED", { get: function () { return -1; } });
Object.defineProperty(STATUS, "UNLISTED", { get: function () { return 1; } });
Object.defineProperty(STATUS, "ACTIVE", { get: function () { return 2; } });
Object.defineProperty(STATUS, "HIDDEN", { get: function () { return 3; } });

// Publish Method constants -
const PUBLISH_METHOD = {};
Object.defineProperty(PUBLISH_METHOD, "SCHEDULE", { get: function () { return 1; } });
Object.defineProperty(PUBLISH_METHOD, "PRERPETUAL", { get: function () { return 2; } });


// Anonymity constants -
const ANONYMITY_TYPE = {};
Object.defineProperty(ANONYMITY_TYPE, "USER_DEFINE", { get: function () { return 1; } });

// COUNT_MAX constants -
const COUNT_MAX = {}
Object.defineProperty(COUNT_MAX, "TITLE", { get: function () { return 80; } });
Object.defineProperty(COUNT_MAX, "CONTEXT", { get: function () { return 200; } });
Object.defineProperty(COUNT_MAX, "CATEGORY", { get: function () { return 40; } });
Object.defineProperty(COUNT_MAX, "QUESTION", { get: function () { return 90; } });

// COUNT_MAX constants -
const APPRAISAL_TYPE = {}
Object.defineProperty(APPRAISAL_TYPE, "ALL", { get: function () { return 0; } });
Object.defineProperty(APPRAISAL_TYPE, "LIKERT7", { get: function () { return 1; } });
Object.defineProperty(APPRAISAL_TYPE, "BARS7", { get: function () { return 2; } });
// Appraisal Type Label
const APPRAISAL_TYPE_LABEL = ["ALL", "Likert7", "BARS7"];

/******************************/

////////////////////////////////////////
// Model Classes
function Appraisal(appraisalType) {
    this.Id = "";
    this.Type = appraisalType;
    this.TypeLabel = APPRAISAL_TYPE_LABEL[appraisalType];
    this.Title = "";
    this.Context = "";
    this.PublishMethod = PUBLISH_METHOD.SCHEDULE;
    this.StartDate = moment.utc(moment().year() + "/" + (moment().month() + 1) + "/" + moment().date(), "YYYY/MM/DD");
    this.EndDate = moment.utc(moment().year() + "/" + (moment().month() + 1) + "/" + moment().date(), "YYYY/MM/DD").add(1, "months").add(1, "days").add(-60, "seconds");
    this.IsPrioritized = false;
    this.Anonymity = ANONYMITY_TYPE.USER_DEFINE;
    this.IsCustomizeQuestion = true;
    this.IsTargetedEveryone = true;
    this.IsTargetedDepartments = false;
    this.IsTargetedUsers = false;
    this.TargetDepartments = [];
    this.TargetUsers = [];
    this.IsCreateMode = true;
    this.Progress = 0;
    this.Status = 0;
    this.OptionCount = 7;
    this.MinLabel = "";
    this.MaxLabel = "";
    this.MidLabel = "";
    /*************************/
    this.Categories = [];
}

function Department() {
    this.Id = "";
    this.Title = "";
}

function User() {
    this.Id = "";
    this.FirstName = "";
    this.LastName = "";
    this.Email = "";
}

function Category() {
    this.Id = "undefined" + (new Date()).toISOString().replace(/-|T|:|\.|Z/g, "");
    this.Title = "";
    this.Cards = [];
}

function Card() {
    this.CategoryId = "";
    this.Id = "undefined" + (new Date()).toISOString().replace(/-|T|:|\.|Z/g, "");
    this.Ordering = -1;
    this.Content = "";
    this.MinLabel = "";
    this.MaxLabel = "";
    this.Options = [];
}

function Option() {
    this.CardId = "";
    this.Id = "";
    this.Content = "";
}

////////////////////////////////////////
// Ad-hoc commonly used functions

function updateUI() {
    // Appraisal Type Action
    $(".appraisal_type_action").prop("href", "/Live360/" + appraisal.TypeLabel + "List"); // hyper link url
    $(".survey-bar__search__button").prop("href", "/Live360/" + appraisal.TypeLabel + "List"); // hyper link url

    $(".appraisal_type_action span").html(appraisal.TypeLabel);

    if (appraisal.IsCreateMode) {
        $(".title_bar_action").html("Add " + appraisal.TypeLabel);
    }
    else {
        $(".title_bar_action").html("Edit " + appraisal.TypeLabel);
    }

    ///

    // Title
    $("#tbTitle").val(appraisal.Title);
    $("#tbTitle").parents(".text_container").find(".letterCount").html(COUNT_MAX.TITLE - appraisal.Title.length);

    // Context
    if (appraisal.Context.length == 0) {
        $("#tbContext").empty();
    }
    else {
        $("#tbContext").val(appraisal.Context);
    }
    $("#tbContext").parents(".text_container").find(".letterCount").html(COUNT_MAX.CONTEXT - appraisal.Context.length);

    // Publish Method
    if (appraisal.IsCreateMode) {
        $("input[name=publish_method][value=" + appraisal.PublishMethod + "]").prop('checked', true);
    }
    else {
        $("#ddlPublishMethod").val(appraisal.PublishMethod);
    }

    // Start date 
    $("#startDate").val(appraisal.StartDate.format("DD/MM/YYYY"));
    $("#startDateHH").val(appraisal.StartDate.format("hh"));
    $("#startDateMM").val(appraisal.StartDate.format("mm"));
    $("#startDateMR").val(appraisal.StartDate.format("a"));

    // End date
    if (appraisal.PublishMethod === PUBLISH_METHOD.SCHEDULE) // show EndDate
    {
        $("#dvEndDate").css("visibility", "visible");
        $("#endDate").val(appraisal.EndDate.format("DD/MM/YYYY"));
        $("#endDateHH").val(appraisal.EndDate.format("hh"));
        $("#endDateMM").val(appraisal.EndDate.format("mm"));
        $("#endDateMR").val(appraisal.EndDate.format("a"));
    }
    else // hide EndDate
    {
        $("#dvEndDate").css("visibility", "hidden");
    }

    // Priority Overrule
    $("#ddlPriorityOverrule").val(appraisal.IsPrioritized.toString());

    // Anonymity
    $("#ddlAnonymous").val(appraisal.Anonymity.toString());

    // Customize Question 
    $("#ddlCustomizeQuestion").val(appraisal.IsCustomizeQuestion.toString());

    // Status
    if (!appraisal.IsCreateMode) {
        $("#ddlStatus").find('option').remove();
        if (appraisal.Status == STATUS.UNLISTED) {
            $("#ddlStatus").append("<option value='" + STATUS.UNLISTED + "'>Unlisted</option>");
            $("#ddlStatus").append("<option value='" + STATUS.ACTIVE + "'>Active</option>");

            $("#ddlStatus").children().each(function () {
                if ($(this).text() == "Unlisted") {
                    $(this).attr("selected", "true");
                }
            });
        }
        else if (appraisal.Status == STATUS.ACTIVE) {
            $("#ddlStatus").append("<option value='" + STATUS.ACTIVE + "'>Active</option>");
            $("#ddlStatus").append("<option value='" + STATUS.HIDDEN + "'>Hidden</option>");

            $("#ddlStatus").children().each(function () {
                if ($(this).text() == "Active") {
                    $(this).attr("selected", "true");
                }
            });
        }
        else if (appraisal.Status == STATUS.HIDDEN) {
            $("#ddlStatus").append("<option value='" + STATUS.ACTIVE + "'>Active</option>");
            $("#ddlStatus").append("<option value='" + STATUS.HIDDEN + "'>Hidden</option>");

            $("#ddlStatus").children().each(function () {
                if ($(this).text() == "Hidden") {
                    $(this).attr("selected", "true");
                }
            });
        }
        else {
            // do nothing
        }
    }

    // Privacy
    if (appraisal.IsTargetedEveryone) {
        $("#cbTargetEveryone").prop("checked", true);
        $("#cbTargetDepartment").prop("checked", false);
        $("#cbTargetUser").prop("checked", false);

        $("#lblAddDepartment").css("color", "rgb(153, 153, 153)");
        $("#lblAddDepartment").css("cursor", "default");

        $("#lblAddDepartment").attr("disabled", "disabled");
    }
    else {
        $("#cbTargetEveryone").prop("checked", false);

        if (appraisal.IsTargetedDepartments) {
            $("#cbTargetDepartment").prop("checked", true);
            $("#lblAddDepartment").attr("disabled", false);
            $("#lblAddDepartment").css("color", "rgb(0, 118, 255, 1)");
            $("#lblAddDepartment").css("cursor", "pointer");
        }
        else {
            $("#cbTargetDepartment").prop("checked", false);
        }

        if (appraisal.IsTargetedUsers) {
            $("#cbTargetUser").prop("checked", true);
        }
        else {
            $("#cbTargetUser").prop("checked", false);
        }
    }

    if (appraisal.TargetDepartments != null) {
        var html = "";
        for (var i = 0; i < appraisal.TargetDepartments.length; i++) {
            html = html + "<div class=\"tag " + appraisal.TargetDepartments[i].Id + "\">";
            html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + appraisal.TargetDepartments[i].Title + "</span>";
            html = html + "    <a class=\"tag__icon\" href=\"javascript:setAppraisalValue(10, this, '" + appraisal.TargetDepartments[i].Id + "');\">x</a>";
            html = html + "</div>";
        }
        $(".department.tags").html(html);
    }

    if (appraisal.TargetUsers != null) {
        var html = "";
        for (var i = 0; i < appraisal.TargetUsers.length; i++) {
            html = html + "<div class=\"tag " + appraisal.TargetUsers[i].Id + "\">";
            html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + appraisal.TargetUsers[i].FirstName + " " + appraisal.TargetUsers[i].LastName + "(" + appraisal.TargetUsers[i].Email + ")</span>";
            html = html + "    <a class=\"tag__icon\" href=\"javascript:setAppraisalValue(11, this, '" + appraisal.TargetUsers[i].Id + "');\">x</a>";
            html = html + "</div>";
        }
        $(".user.tags").html(html);
    }
}

function refreshCategoryListUI() {
    /*********Category*************/
    var html = "";
    for (var i = 0; i < appraisal.Categories.length; i++) {
        html += getFullCategoryHtml(appraisal.Categories[i]);
    }

    $(".dvCategoryList").html(html);
}

function setAppraisalValue(attrType, element, arg) {
    var needUpdate = true;

    switch (attrType) {
        case 1: // Title
            appraisal.Title = $("#tbTitle").val();
            break;

        case 2: // Context
            appraisal.Context = $("#tbContext").val();
            break;

        case 3: // Publish Method
            if (appraisal.IsCreateMode) {

                appraisal.PublishMethod = parseInt($("input[name=publish_method]:checked").val());
            }
            else {
                appraisal.PublishMethod = parseInt($("#ddlPublishMethod").val());
            }
            break;

        case 4: // Start Date
            var startDateText = $("#startDate").val() + " " + $("#startDateHH").val() + ":" + $("#startDateMM").val() + " " + $("#startDateMR").val();
            if (moment(startDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
                appraisal.StartDate = moment(startDateText, "DD/MM/YYYY hh:mm a");
            }
            else {
                ShowToast("Please enter a valid start date", 2);
                return;
            }
            break;

        case 5: // EndDate
            var endDateText = $("#endDate").val() + " " + $("#endDateHH").val() + ":" + $("#endDateMM").val() + " " + $("#endDateMR").val();
            if (moment(endDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
                if (moment(endDateText, "DD/MM/YYYY hh:mm a").isBefore(appraisal.StartDate)) {
                    ShowToast("Your date range is invalid", 2);
                    return;
                }
                else {
                    appraisal.EndDate = moment(endDateText, "DD/MM/YYYY hh:mm a");
                }
            }
            else {
                ShowToast("Please enter a valid end date", 2);
                return;
            }
            break;

        case 6: // Privacy: targer everyonr
            appraisal.IsTargetedEveryone = true;
            appraisal.IsTargetedDepartments = false;
            appraisal.IsTargetedUsers = false;
            break;

        case 7: // Privacy: targer department
            if (appraisal.IsTargetedDepartments) {
                appraisal.IsTargetedDepartments = false;
            }
            else {
                appraisal.IsTargetedEveryone = false;
                appraisal.IsTargetedDepartments = true;
            }
            break;

        case 8: // Privacy: targer user
            if (appraisal.IsTargetedUsers) {
                appraisal.IsTargetedUsers = false;
            }
            else {
                appraisal.IsTargetedEveryone = false;
                appraisal.IsTargetedUsers = true;
            }
            break;

        case 9: // Add target depeartment
            appraisal.TargetDepartments = [];

            var selectedDepartments = $('input:checkbox:checked[name="cbDepartments"]').map(function () { return $(this).val(); }).get();
            for (var i = 0; i < selectedDepartments.length; i++) {
                var department = new Department();
                department.Id = selectedDepartments[i].split(",")[0];
                department.Title = selectedDepartments[i].split(",")[1];
                appraisal.TargetDepartments.push(department);
            }

            hidePopup(1);
            break;

        case 10: // Remove TargetDepartment
            for (var i = 0; i < appraisal.TargetDepartments.length; i++) {
                if (appraisal.TargetDepartments[i].Id == arg) {
                    appraisal.TargetDepartments.splice(i, 1);
                    break;
                }
            }
            break;

        case 11: // Remove TargetUser
            for (var i = 0; i < appraisal.TargetUsers.length; i++) {
                if (appraisal.TargetUsers[i].Id == arg) {
                    appraisal.TargetUsers.splice(i, 1);
                    break;
                }
            }
            break;

        case 12: // Priority Overrule
            appraisal.IsPrioritized = ($("#ddlPriorityOverrule").val() === "true");
            break;

        case 13: // Customize Question
            appraisal.IsCustomizeQuestion = ($("#ddlCustomizeQuestion").val() === "true");
            break;

        case 14: // Satus
            appraisal.Status = $("#ddlStatus").val();
            needUpdate = false;
            break;

        case 15: // Max count
            appraisal.Status = $("#ddlStatus").val();
            needUpdate = false;
            break;

        default:
            break;
    }

    if (needUpdate) {
        updateUI();
    }
}

function getCategory(categoryId) {
    for (var i = 0; i < appraisal.Categories.length; i++) {
        if (appraisal.Categories[i].Id === categoryId)
            return appraisal.Categories[i];
    }

    return null;
}

function setCategory(categoryId, element, argType) {
    var category = getCategory(categoryId);
    category.Title = element.value;

    for (var i = 0; i < appraisal.Categories.length; i++) {
        if (appraisal.Categories[i].Id === categoryId) {
            appraisal.Categories[i] = category;
            break;
        }
    }
}

function removeCategory(categoryId) {
    if (appraisal.Categories != null && appraisal.Categories.length > 0) {
        var index = -1;
        for (var i = 0; i < appraisal.Categories.length; i++) {
            if (appraisal.Categories[i].Id == categoryId) {
                index = i;
                break;
            }
        }

        if (index > -1) {
            appraisal.Categories.splice(index, 1);
        }
    }
}

function getCard(categoryId, cardId) {
    var category = getCategory(categoryId);
    for (var i = 0; i < category.Cards.length; i++) {
        if (category.Cards[i].Id === cardId)
            return category.Cards[i];
    }

    return null;
}

function setCard(categoryId, cardId, argType, element) {
    var category = getCategory(categoryId);
    var card = getCard(categoryId, cardId);

    switch (argType) {
        case 1: // Content
            card.Content = element.value;
            break;

        case 2: // Min Label
            card.MinLabel = element.value;
            break;

        case 3: // Max Label
            card.MaxLabel = element.value;
            break;

        default:
            break;

    }

    for (var i = 0; i < category.Cards.length; i++) {
        if (category.Cards[i].Id == cardId) {
            category.Cards[i] = card;
        }
    }

    for (var i = 0; i < appraisal.Categories.length; i++) {
        if (appraisal.Categories[i].Id === categoryId) {
            appraisal.Categories[i] = category;
            break;
        }
    }
}


function removeCard(categoryId, cardId) {
    var category = getCategory(categoryId);

    if (category.Cards != null && category.Cards.length > 0) {
        var index = -1;
        for (var i = 0; i < category.Cards.length; i++) {
            if (category.Cards[i].Id == cardId) {
                index = i;
                break;
            }
        }

        if (index > -1) {
            // reordering 
            for (var j = index; j < category.Cards.length; j++) {
                category.Cards[j].Ordering = category.Cards[j].Ordering - 1;
            }
            category.Cards.splice(index, 1);
        }

        for (var i = 0; i < appraisal.Categories.length; i++) {
            if (appraisal.Categories[i].Id === categoryId) {
                appraisal.Categories[i] = category;
                break;
            }
        }
    }
}
function isDataValid() {
    // Titlt
    if (appraisal.Title.length == 0) {
        ShowToast("Please fill in Title", 2);
        return false;
    }

    if (appraisal.Title.length > COUNT_MAX.TITLE) {
        ShowToast("Title is longer than " + COUNT_MAX.TITLE + " letters", 2);
        return false;
    }

    // Context
    if (appraisal.Context.length == 0) {
        ShowToast("Please fill in Context", 2);
        return false;
    }

    if (appraisal.Context.length > COUNT_MAX.CONTEXT) {
        ShowToast("Context is longer than " + COUNT_MAX.CONTEXT + " letters", 2);
        return false;
    }

    // StartDate
    var startDateText = $("#startDate").val() + " " + $("#startDateHH").val() + ":" + $("#startDateMM").val() + " " + $("#startDateMR").val();
    if (moment(startDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
        appraisal.StartDate = moment.utc(startDateText, "DD/MM/YYYY hh:mm a");
    }
    else {
        ShowToast("Please enter a valid start date", 2);
        return false;
    }

    // EndDate
    if (appraisal.PublishMethod == PUBLISH_METHOD.SCHEDULE) {
        var endDateText = $("#endDate").val() + " " + $("#endDateHH").val() + ":" + $("#endDateMM").val() + " " + $("#endDateMR").val();
        if (moment(endDateText, "DD/MM/YYYY hh:mm a", true).isValid()) {
            if (moment(endDateText, "DD/MM/YYYY hh:mm a").isBefore(appraisal.StartDate)) {
                ShowToast("Your date range is invalid", 2);
                return false;
            }
            else {
                appraisal.EndDate = moment.utc(endDateText, "DD/MM/YYYY hh:mm a");
            }
        }
        else {
            ShowToast("Please enter a valid end date", 2);
            return false;
        }
    }
    else {
        appraisal.EndDate = null;
    }

    // Privacy
    if (appraisal.IsTargetedEveryone) {
        appraisal.IsTargetedDepartments = false;
        appraisal.TargetDepartments = [];
        appraisal.IsTargetedUsers = false
        appraisal.TargetUsers = [];
    }
    else {
        if (appraisal.IsTargetedDepartments && appraisal.TargetDepartments.length == 0) {
            ShowToast("You have not selected any Department", 2);
            return false;
        }

        if (appraisal.IsTargetedUsers && appraisal.TargetUsers.length == 0) {
            ShowToast("You have not selected any Personnel", 2);
            return false;
        }
    }

    return true;
}

function isCategoryDataValid(category) {
    // Titlt
    if (category.Title.length == 0) {
        ShowToast("Please fill in Title of Catrgory", 2);
        return false;
    }

    return true;
}

function isCardDataValid(card) {
    // Titlt
    if (card.Content.length == 0) {
        ShowToast("Please fill in Question of Card", 2);
        return false;
    }

    return true;
}

function selectAllDepartments(element) {
    $("#department_checkboxlist input[type=checkbox]").prop("checked", element.checked);
}

function RequestObject() {
    this.CompanyId = $("#main_content_hfCompanyId").val();
    this.ManagerId = $("#main_content_hfManagerId").val();
    this.AppraisalId = appraisal.Id;
    this.AppraisalType = appraisal.Type;
    this.Title = appraisal.Title;
    this.Description = appraisal.Context;
    this.IsPrioritized = appraisal.IsPrioritized;
    this.Anonymity = appraisal.Anonymity;
    this.IsCustomizeQuestion = appraisal.IsCustomizeQuestion;
    this.TargetedDepartmentIds = [];
    if (appraisal.IsTargetedDepartments) {
        for (var i = 0; i < appraisal.TargetDepartments.length; i++) {
            this.TargetedDepartmentIds.push(appraisal.TargetDepartments[i].Id);
        }
    }

    this.TargetedUserIds = [];
    if (appraisal.IsTargetedUsers) {
        for (var i = 0; i < appraisal.TargetUsers.length; i++) {
            this.TargetedUserIds.push(appraisal.TargetUsers[i].Id);
        }
    }

    this.PublishMethod = appraisal.PublishMethod;
    this.StartDate = appraisal.StartDate.add(-timeZone, "hours");
    if (appraisal.PublishMethod == PUBLISH_METHOD.PRERPETUAL) {
        this.EndDate = null;
    }
    else {
        this.EndDate = appraisal.EndDate.add(-timeZone, "hours");
    }

    appraisal.Status = $("#ddlStatus").val();
    this.Status = appraisal.Status;
}

/**************  Category  ****************/
function getEditCategoryHtml(category) {
    var html = "";
    html += "<div class=\"card\">";
    html += "   <div class=\"grid-question\" style=\"width: calc(100% - 200px);\">";
    if (category.Id.indexOf('undefined') == 0) {
        html += "   <label style=\"font-weight: bolder; padding: 10px;\">Create Category</label>";
        html += "        <input class=\"survey-bar__search__input input-field\" type=\"text\" placeholder=\"Type your category\" style=\"border: solid #ddd; border-width: 0 0 1px;\" onblur=\"setCategory('" + category.Id + "', this, 1);\" />";
    }
    else {
        html += "   <label style=\"font-weight: bolder; padding: 10px;\">Edit Category</label>";
        html += "        <input class=\"survey-bar__search__input input-field\" type=\"text\" placeholder=\"Type your category\" style=\"border: solid #ddd; border-width: 0 0 1px;\" onblur=\"setCategory('" + category.Id + "', this, 1);\" value=\"" + category.Title + "\" />";
    }
    html += "    </div>";
    html += "    <div class=\"grid-question-icon\" style=\"width: 200px;\">";
    html += "        <button class=\"add-pulse survey-bar__search__button\" style=\"margin: 10px; float: left;\" type=\"button\" onclick=\"switchCategoryMode('" + category.Id + "', false);\">Cancel</button>";
    if (category.Id.indexOf('undefined') == 0) {
        html += "        <button class=\"add-pulse survey-bar__search__button\" style=\"margin: 10px; float: left;\" type=\"button\" onclick=\"createCategory('" + category.Id + "');\">Done</button>";
    }
    else {
        html += "        <button class=\"add-pulse survey-bar__search__button\" style=\"margin: 10px; float: left;\" type=\"button\" onclick=\"updateCategory('" + category.Id + "');\">Done</button>";
    }

    html += "    </div>";
    html += "</div>";

    return html;
}

function getFullCategoryHtml(category) {
    var html = "";
    html += "    <div class=\"category_layout " + category.Id + "\" data-categoryid=\"" + category.Id + "\" data-ordering=\"1\" style=\"margin-top: 20px;\">";
    html += "        <div class=\"category_detail\" >";
    html += getCategoryDetailHtml(category);
    html += "        </div>";
    html += "        <hr style=\"width: 100%;\" />";

    html += "        <div style=\"width: 100%; display: inline-block;\">";
    html += "           <button class=\"add-card survey-bar__search__button\" style=\"margin: 0px; visibility: visible;\" onclick=\"addCard('" + category.Id + "');\" type=\"button\">+ Add Question</button>";
    html += "       </div>";

    html += "        <div class=\"dvNewCard\" style=\"width: 100%; display: inline-block;\">";
    html += "       </div>";

    html += "        <div class=\"dvCardList " + category.Id + "\" style=\"margin-top: 10px;\">";
    for (var i = 0; i < category.Cards.length; i++) {
        html += getCardLayput(category.Cards[i]);
    }
    html += "        </div>";
    html += "    </div>";
    html += "    <hr class=\"category_hr " + category.Id + "\" style=\"width: 100%; border-width: 5px; margin-bottom: 3em;\" />";

    return html;
}

function getCategoryDetailHtml(category) {
    var html = "";
    html += "            <div class=\"grid-question-number\" style=\"width: calc(100% - 150px); display: inline; float: left; margin: 0px;\">";
    html += "                <label style=\"font-weight: bolder; font-size: 1.2em;\">" + category.Title + "</label>";
    html += "            </div>";
    html += "            <div class=\"grid-question-icon\" style=\"width: 150px; float: left;\">";
    html += "                <span style=\"padding: 10px; text-align: right; color: rgba(57, 137, 242, 1); font-weight: bold; cursor: pointer;\" onclick=\"switchCategoryMode('" + category.Id + "', true); \">Edit</span>";
    html += "                <span style=\"padding: 10px; text-align: right; color: rgba(57, 137, 242, 1); font-weight: bold; cursor: pointer;\" onclick=\"showPopup(2, '" + category.Id + "'); \">Delete</span>";
    html += "            </div>";

    return html;
}

function addCategory() {
    var category = new Category();
    appraisal.Categories.push(category);
    $(".dvNewCategory").html(getEditCategoryHtml(category));
    $(".add-category").css("visibility", "hidden");

}

function switchCategoryMode(categoryId, isEditMode) {
    if (categoryId.indexOf('undefined') == 0) // Remove temp category.
    {
        $(".dvNewCategory").html("");
        $(".add-category").css("visibility", "visible");
        removeCategory(categoryId);
    }
    else {
        var category = getCategory(categoryId);
        if (isEditMode) {
            $(".category_layout." + categoryId + " .category_detail").html(getEditCategoryHtml(category));
        }
        else {
            $(".category_layout." + categoryId + " .category_detail").html(getCategoryDetailHtml(category));
        }
    }
}


/**************  Card  ****************/
function getEditCardHtml(card) {
    var isDisplay = true;
    if (appraisal.Progress == PROGRESS.LIVE && appraisal.Status == STATUS.ACTIVE) {
        isDisplay = false;
    }

    var html = "";
    html += "    <div class=\"card-edit card full-card " + card.Id + "\" style=\"position: relative;\" data-cardid=\"" + card.Id + "\" data-categoryid=\"" + card.CategoryId + "\">";
    html += "        <div style=\"width: 100%; position: relative;\">";
    html += "            <label class=\"card-ordering\" style=\"width: 120px; height: 90px; color: rgb(204, 204, 204); font-size: 36pt; font-weight: bolder; display: inline-block;\">Q" + card.Ordering + "</label>";
    html += "            <div style=\"width: 75%; height: 90px; vertical-align: bottom; display: inline-block;\">";
    html += "                <div class=\"tabs--styled\" style=\"vertical-align: bottom;\">";
    html += "                    <ul class=\"tabs__list\">";
    html += "                        <li class=\"tabs__list__item\"><a class=\"tabs__link active\" href=\"javascript:void(0);\">Card Type</a></li>";
    html += "                    </ul>";
    html += "                </div>";
    html += "                <div class=\"grid__inner pulse-type-selection-panel\" style=\"margin: 4px 0px; background-color: transparent;\">";
    html += "                    <div>";
    html += "                        <button class=\"pulse-type select-one-pulse\" style=\"border-color: rgba(143, 187, 242, 1); margin: 10px 10px 0px 0px; padding: 8px; width: 120px; text-align: center; background-color: rgba(143, 187, 242, 1);\" type=\"button\" data-cardtype=\"1\">";
    html += "                            <span style=\"vertical-align: middle; display: inline-block;\">" + appraisal.TypeLabel + "</span>";
    html += "                        </button>";
    html += "                        <div style=\"padding: 10px; width: 40%; float: right; display: inline-block;\">";
    html += "                            <span style=\"color: rgba(165, 165, 165, 1); font-weight: bold; display: inline-block;\">Date</span>";
    html += "                            <span style=\"color: rgba(165, 165, 165, 1); font-weight: bold; display: inline-block;\">" + appraisal.StartDate.format("DD MMM YYYY dddd") + ",</span>";
    html += "                            <span style=\"color: rgba(165, 165, 165, 1); font-weight: bold; display: inline-block;\">at </span>";
    html += "                            <span style=\"color: rgba(165, 165, 165, 1); font-weight: bold; display: inline-block;\">" + appraisal.StartDate.format("hh") + "</span>";
    html += "                            <span style=\"color: rgba(165, 165, 165, 1); font-weight: bold; display: inline-block;\">: </span>";
    html += "                            <span style=\"color: rgba(165, 165, 165, 1); font-weight: bold; display: inline-block;\">" + appraisal.StartDate.format("mm") + "</span>";
    html += "                            <span style=\"color: rgba(165, 165, 165, 1); font-weight: bold; display: inline-block;\">" + appraisal.StartDate.format("A") + "</span>";
    html += "                        </div>";
    html += "                    </div>";
    html += "                </div>";
    html += "            </div>";
    html += "                <div style=\"top: 0px; right: 50px; position: absolute;\">";
    if (card.Id.indexOf('undefined') == 0) {
        html += "                    <button class=\"save-card\" style=\"width: 120px; height: 40px; " + ((!isDisplay) ? "visibility: hidden;" : "") + "\" onclick=\"createCard('" + card.CategoryId + "', '" + card.Id + "');\" type=\"button\">Save</button>";
    }
    else {
        html += "                    <button class=\"save-card\" style=\"width: 120px; height: 40px; " + ((!isDisplay) ? "visibility: hidden;" : "") + "\" onclick=\"updateCard('" + card.CategoryId + "', '" + card.Id + "');\" type=\"button\">Save</button>";
    }
    html += "                </div>";
    if (card.Id.indexOf('undefined') < 0) {
        html += "                <div>";
        html += "                   <div class=\"grid-question-expandable\" style=\"top: 0px; right: 0px; position: absolute;\">";
        html += "                       <button class=\"to-mini-card\" onclick=\"switchCardMode('" + card.CategoryId + "', '" + card.Id + "', false);\" type=\"button\" style=\"background: none; padding: 0px 0px 0px 10px; border: currentColor; border-image: none; color: rgb(70, 131, 234);\">";
        html += "                           <i class=\"fa fa-caret-square-o-down fa-lg icon\"></i>";
        html += "                       </button>";
        html += "                   </div>";
        html += "               </div>";
    }
    html += "        </div>";

    /*********/
    if (appraisal.Type == APPRAISAL_TYPE.LIKERT7) {
        html += "        <div class=\"pulse-card-container\" style=\"width: 50%; margin-top: 20px; display: inline-block;\">";
        html += "            <div class=\"grid__inner pulse-card number-pulse pulse-level-1\" style=\"background-position: center; margin: 0px auto; border-radius: 0.5em; border: 2px solid rgb(204, 204, 204); border-image: none; width: 100%; min-height: 285px; background-image: none; background-repeat: no-repeat; background-color: transparent;\">";
        html += "                <div class=\"grid__inner\" style=\"text-align: center; background-color: transparent;\">";
        html += "                    <div style=\"margin: 20px; width: 80%; vertical-align: middle; display: inline-block;\">";
        html += "                        <div class=\"text_container\" style=\"position: relative;\">";
        html += "                            <textarea class=\"cardQuestion\" style=\"margin: 5px; border-radius: 0.5em; border: 1px solid rgb(204, 204, 204); border-image: none; width: 100%; height: 150px; color: black; font-size: 14pt; display: block; position: relative; min-height: 150px; background-color: white; resize: none; overflow-wrap: break-word;\" onkeyup=\"setCard('" + card.CategoryId + "', '" + card.Id + "', 1, this); letterCounter(this, " + COUNT_MAX.QUESTION + ", 'red', '#DDD');\" placeholder=\"Question\" rows=\"3\" " + ((!isDisplay) ? "disabled=\"disabled\"" : "") + ">" + card.Content + "</textarea>";
        html += "                            <label class=\"letterCount\" style=\"width: auto; right: 17px; bottom: 0px; color: rgb(215, 215, 215); position: absolute; z-index: 10;\">" + (COUNT_MAX.QUESTION - card.Content.length) + "</label>";
        html += "                        </div>";
        html += "                    </div>";
        html += "                </div>";
        html += "                <div class=\"grid__inner\" style=\"background-color: transparent;\">";
        html += "                    <label class=\"range-pulse min-value-label\" style=\"margin: 10px; padding: 10px; border-radius: 0.5em; border: 1px solid rgb(221, 221, 221); border-image: none; width: auto; color: rgba(145, 145, 145, 1); float: left; display: inline-block;\" >" + appraisal.MinLabel + "</label>";
        html += "                    <label class=\"range-pulse mid-value-label\" style=\"margin: 10px; padding: 10px; border-radius: 0.5em; border: 1px solid rgb(221, 221, 221); border-image: none; left: 275px; width: auto; color: rgba(145, 145, 145, 1); float: left; display: inline-block; position: absolute;\" >" + appraisal.MidLabel + "</label>";
        html += "                    <label class=\"range-pulse max-value-label\" style=\"margin: 10px; padding: 10px; border-radius: 0.5em; border: 1px solid rgb(221, 221, 221); border-image: none; width: auto; color: rgba(145, 145, 145, 1); float: left; display: inline-block; float: right;\" >" + appraisal.MaxLabel + "</label>";
        html += "                </div>";
        html += "                <hr style=\"margin: 0px;\" />";
        html += "                <div class=\"grid__inner range-ruler\" style=\"background-color: transparent;\">";
        html += "                    <div style=\"width: 14.28%; height: 70px; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; font-weight: 900; float: left; display: inline-block; position: relative;\">";
        html += "                        <span>1</span>";
        html += "                        <div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div>";
        html += "                    </div>";
        html += "                    <div style=\"width: 14.28%; height: 70px; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; font-weight: 900; float: left; display: inline-block; position: relative;\">";
        html += "                        <span>2</span>";
        html += "                        <div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div>";
        html += "                    </div>";
        html += "                    <div style=\"width: 14.28%; height: 70px; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; font-weight: 900; float: left; display: inline-block; position: relative;\">";
        html += "                        <span>3</span>";
        html += "                        <div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div>";
        html += "                    </div>";
        html += "                    <div style=\"width: 14.28%; height: 70px; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; font-weight: 900; float: left; display: inline-block; position: relative;\">";
        html += "                        <span>4</span>";
        html += "                        <div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div>";
        html += "                    </div>";
        html += "                    <div style=\"width: 14.28%; height: 70px; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; font-weight: 900; float: left; display: inline-block; position: relative;\">";
        html += "                        <span>5</span>";
        html += "                        <div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div>";
        html += "                    </div>";
        html += "                    <div style=\"width: 14.28%; height: 70px; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; font-weight: 900; float: left; display: inline-block; position: relative;\">";
        html += "                        <span>6</span>";
        html += "                        <div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div>";
        html += "                    </div>";
        html += "                    <div style=\"width: 14.28%; height: 70px; text-align: center; color: rgba(203, 203, 203, 1); font-size: 2em; font-weight: 900; float: left; display: inline-block; position: relative;\">";
        html += "                        <span>7</span>";
        html += "                        <div style=\"left: 50.5%; height: 20px; bottom: 0px; border-right-color: rgb(204, 204, 204); border-right-width: 2px; border-right-style: solid; position: absolute;\"></div>";
        html += "                    </div>";
        html += "                </div>";
        html += "            </div>";
        html += "        </div>";
    }
    else if (appraisal.Type == APPRAISAL_TYPE.BARS7) {
        html += "        <div class=\"card-container\" style=\"width: 100%; margin-top: 20px; display: inline-block;  text-align: center;\">";
        html += "           <div class=\"card-question\" style=\"width: calc(100% - 650px); vertical-align: top; display: inline-block; margin: 20px;\">";
        html += "               <div class=\"text_container\" style=\"position: relative; width: 10ㄈ0%;\">";
        html += "                   <textarea style=\"padding-right: 30px; padding-left: 40px;\" onkeydown=\"return (event.keyCode!=13);\"  placeholder=\"Question\" rows=\"2\" cols=\"20\">" + card.Content + "</textarea>";
        html += "                   <label class=\"letterCount\" style=\"width: auto; right: 20px; bottom: 5px; color: rgb(215, 215, 215); position: absolute; z-index: 10;\">" + (COUNT_MAX.QUESTION - card.Content.length) + "</label>";
        html += "               </div>";
        html += "               <label class=\"upload-card-image\" for=\"fuCardQuestionImg_" + card.Id + "\" style=\"bottom: 50px;\" >";
        html += "                   <input title=\"Add image\" id=\"fuCardQuestionImg_" + card.Id + "\" style=\"display: none;\" onchange=\"previewImage(this, 2560, 2560 ,1, 1);\" type=\"file\" accept=\"image/*\">";
        html += "               </label>";
        html += "               <div class=\"question-image-content " + card.Id + "\"  style=\"height: 0px; overflow: auto; visibility: hidden;\">";
        html += "                   <div class=\"survey-cross\" onclick=\"removeContentImg(1, " + card.Id + ");\">";
        html += "                       <i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
        html += "                   </div>";
        html += "                   <img class=\"question-img " + card.Id + "\" >";
        html += "               </div>";
        html += "               <div class=\"card-question-option-list\" style=\"overflow: auto; display: block; visibility: visible;\">";

        for (var i = 0; i < card.Options.length; i++) {
            html += "                   <div style=\"width: 90%; margin-bottom: 10px; float: right;\">";
            html += "                       <a class=\"remove-option-link\" href=\"javascript:void(0);\">- Remove option</a>";
            html += "                       <textarea class=\"option-content\" onkeydown=\"return (event.keyCode!=13);\" placeholder=\"Option " + (i + 1) + "\" rows=\"2\" cols=\"20\">" + card.Options[i].Content + "</textarea>";
            html += "                       <span class=\"survey-lettercount\">150</span>";
            html += "                       <label class=\"upload-option-image\" for=\"fuNewCardOption2ContentImg\" style=\"bottom: 51px;\">";
            html += "                           <input title=\"Add image\" id=\"fuNewCardOption2ContentImg\" style=\"display: none;\" onchange=\"previewImage(this, 2560, 2560 ,2, 2);\" type=\"file\" accept=\"image/*\" />";
            html += "                       </label>";
            html += "                       <div class=\"option-image-content\" style=\"height: 0px; overflow: auto; visibility: hidden;\">";
            html += "                           <div class=\"survey-cross\" onclick=\"removeContentImg(2, 2);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i>";
            html += "                       </div>";
            html += "                       <img id=\"imgNewOption2ContentImgPreview\"></div>";
            html += "                   </div>";
        }

        html += "               </div>";
        html += "           </div>";
        html += "           <div class=\"card-preivew\" style=\"width: 400px; display: inline-block; padding: 20px;\">";
        html += "               <div class=\"card-preivew\" style=\"width: 400px; background-color: red; height: 250px; display: inline-block; padding: 20px;\">";
        html += "               </div>";
        html += "           </div>";
        html += "        </div>";
    }

    /************/

    html += "        <div class=\"card-logic\" style=\"width: 45%; margin-top: 20px; margin-left: 20px; vertical-align: top; display: inline-block;\"></div>";
    html += "        <div style=\"width: 100%; display: inline-block; background-color: transparent;\">";
    if (card.Id.indexOf('undefined') < 0) {
        html += "            <button style=\"float: right; background-color: red; " + ((!isDisplay) ? "visibility: hidden;" : "") + "\" onclick=\"showPopup(3, '" + card.CategoryId + "', '" + card.Id + "');\" type=\"button\">Delete</button>";
    }
    else {
        html += "            <button style=\"float: right; background-color: red; " + ((!isDisplay) ? "visibility: hidden;" : "") + "\" onclick=\"switchCardMode('" + card.CategoryId + "', '" + card.Id + "', false);\" type=\"button\">Cancel</button>";
    }
    html += "        </div>";
    html += "    </div>";
    return html;
}

function getCardLayput(card) {
    var html = "";
    html += "<div class=\"card_detail " + card.CategoryId + " " + card.Id + "\">";
    html += getMiniCardHtml(card);
    html += "</div>";
    return html;
}

function getMiniCardHtml(card) {
    var html = "";

    html += " <div class=\"card-mini card-holder row-" + card.Ordering + " " + card.Id + "\">";
    html += "     <div class=\"grid__span--10 grid__span--last mini-card-holder\" style=\"width: 100%; position: relative; float: none;\">";
    html += "         <div class=\"card mini-card " + card.Id + "\" data-cardid=\"" + card.Id + "\" data-categoryid=\"" + card.CategoryId + "\" style=\"padding: 0px;\">";
    html += "             <div class=\"grid__inner\" style=\"background-color: transparent;\">";
    html += "                 <div class=\"grid__span--3\" style=\"width: auto;\">";
    html += "                     <div class=\"card-label\" style=\"margin: 0px; width: 70px; text-align: center; color: white; font-size: 30px; background-color: rgba(245, 166, 35, 1); vertical-align: middle;\">";
    html += "                         <label class=\"card-ordering\" style=\"padding: 45px 0px; margin: 0px;\">Q" + card.Ordering + "</label>";
    html += "                     </div>";
    html += "                 </div>";
    html += "                 <div class=\"grid__span grid__span--9 grid__span--last\" style=\"background-color: transparent;\">";
    html += "                     <div class=\"grid__inner\" style=\"background-color: transparent;\">";
    html += "                         <div class=\"grid__span--8\" style=\"width: 60%;\">";
    html += "                             <label style=\"font-weight: bolder; margin: 20px; font-size: 1.3em;\">Question</label>";
    html += "                             <label style=\"color: rgb(204, 204, 204); margin: 20px; font-size: 1.2em;\">" + card.Content + "</label>";
    html += "                         </div>";
    html += "                         <div class=\"grid__span--1\" style=\"text-align: center; width: 20%;\">";
    html += "                             <label style=\"font-weight: bolder; margin: 20px; font-size: 1.3em;\">Type</label>";
    html += "                             <label style=\"color: rgb(204, 204, 204); margin: 20px; font-size: 1.2em;\">" + appraisal.TypeLabel + "</label>";
    html += "                         </div>";
    html += "                         <div class=\"grid__span--1\" style=\"text-align: center; width: 10%; visibility:hidden;\">";
    html += "                             <label style=\"font-weight: bolder; margin: 20px; font-size: 1.3em;\" onclick=\"window.open('/DynamicPulse/ResponsivePulseAnalytic/PDD98de113ca36d4a72a19ae9cdfc04c89d/PDC6fa3fd74363a4cb8942dde63b1c041c2');\">";
    html += "                                 <img src=\"/Img/icon_result.png\" />";
    html += "                             </label>";
    html += "                         </div>";
    html += "                         <div class=\"grid__span--1 grid__span--last\" style=\"text-align: center; width: 10%;\">";
    html += "                             <label style=\"font-weight: bolder; margin: 20px; font-size: 1.3em;\">";
    html += "                                 <button class=\"to-mini-card\" style=\"background: none; padding: 0px 0px 0px 10px; border: currentColor; border-image: none; top: 15px; right: 15px; color: rgb(70, 131, 234); position: absolute;\" onclick=\"switchCardMode('" + card.CategoryId + "', '" + card.Id + "' ,true);\" type=\"button\"><i class=\"fa fa-caret-square-o-up fa-lg icon\"></i></button>";
    html += "                             </label>";
    html += "                         </div>";
    html += "                     </div>";
    html += "                 </div>";
    html += "             </div>";
    html += "         </div>";
    html += "     </div>";
    html += " </div>";
    return html;
}

function addCard(categoryId) {
    var category = getCategory(categoryId);
    var card = new Card();
    card.CategoryId = categoryId;
    card.Ordering = category.Cards.length + 1;
    if (appraisal.Type == APPRAISAL_TYPE.BARS7)
    {
        card.Options = [];
        var option = new Option();
        option.CardId = card.Id;
        dsfdsfdsf

        card.Options.push(new Option());
    }


    category.Cards.push(card);
    $(".category_layout." + categoryId + " .dvNewCard").html(getEditCardHtml(card));
    $(".category_layout." + categoryId + " .add-card").css("visibility", "hidden");

}

function switchCardMode(categoryId, cardId, isEditMode) {
    if (cardId.indexOf('undefined') == 0) // Remove temp category.
    {
        $(".category_layout." + categoryId + " .dvNewCard").html("");
        $(".category_layout." + categoryId + " .add-card").css("visibility", "visible");
        removeCard(categoryId, card);

    }
    else {
        var card = getCard(categoryId, cardId);
        if (isEditMode) {
            $(".card_detail." + categoryId + "." + card.Id).html(getEditCardHtml(card));
        }
        else {
            $(".card_detail." + categoryId + "." + card.Id).html(getMiniCardHtml(card));
        }
    }
}


function hidePopup(popupType) {
    $("#mpe_backgroundElement").hide();

    switch (popupType) {
        case 1:
            $("#dvSelectDepartment").hide();
            break;

        case 2:
            $("#dvDeleteCategory").hide();
            break;

        case 3:
            $("#dvDeleteCard").hide();
            break;

        case 4:
            $("#dvAlert").hide();
            break;

        default:
            break;
    }
}

function disableElements() {
    $("#ddlPriorityOverrule").prop("disabled", true);
    $("#ddlAnonymous").prop("disabled", true);
    $("#ddlCustomizeQuestion").prop("disabled", true);
    //$("#ddlPublishMethod").prop("disabled", true);
    $("#startDate").prop("disabled", true);
    $("#startDate").css("color", "rgba(149, 149, 149, 1)");
    $("#startDateHH").prop("disabled", true);
    $("#startDateHH").css("color", "rgba(149, 149, 149, 1)");
    $("#startDateMM").prop("disabled", true);
    $("#startDateMM").css("color", "rgba(149, 149, 149, 1)");
    $("#startDateMR").prop("disabled", true);
    //$("#endDate").prop("disabled", true);
    //$("#endDate").css("color", "rgba(149, 149, 149, 1)");
    //$("#endDateHH").prop("disabled", true);
    //$("#endDateHH").css("color", "rgba(149, 149, 149, 1)");
    //$("#endDateMM").prop("disabled", true);
    //$("#endDateMM").css("color", "rgba(149, 149, 149, 1)");
    //$("#endDateMR").prop("disabled", true);
    $("#cbTargetEveryone").prop("disabled", true);
    $("#cbTargetDepartment").prop("disabled", true);
    $("#cbTargetUser").prop("disabled", true);

    $("#lblAddDepartment").css("color", "rgb(153, 153, 153)");
    $("#lblAddDepartment").css("cursor", "default");
    $("#lblAddDepartment").attr("disabled", "disabled");
    $("#tbSearchUser").css("display", "none");
    $(".department.tags a").css("visibility", "hidden");
    $(".user.tags a").css("visibility", "hidden");

    $(".add-category").css("display", "none");

    $(".dvCategoryList .grid-question-icon span").css("visibility", "hidden");
    $(".dvCategoryList .add-card").css("visibility", "hidden");
    $(".dvCategoryList .save-card").css("visibility", "hidden");

    showPopup(4, null, null);
    //dvCategoryList
}

////////////////////////////////////////
// Event handlers
(function ($) {
    "use strict";

    $(document).ready(function () {

        $("#startDate").datepicker({
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999);
                }, 0);
            },
            dateFormat: 'dd/mm/yy',
            showOtherMonths: true
        });

        $("#endDate").datepicker({
            beforeShow: function () {
                setTimeout(function () {
                    $('.ui-datepicker').css('z-index', 99999);
                }, 0);
            },
            dateFormat: 'dd/mm/yy',
            showOtherMonths: true
        });

        if ($("#main_content_hfTimezone").val().length > 0) {
            timeZone = parseInt($("#main_content_hfTimezone").val());
        }

        personnelAutoComplete();

        if ($("#main_content_hfAppraisalId").val() == null || $("#main_content_hfAppraisalId").val() == undefined || $("#main_content_hfAppraisalId").val() == "") {
            $("#main_content_hfAppraisalId").val();
            var appraisalType = parseInt($("#main_content_hfAppraisalType").val());
            appraisal = new Appraisal(appraisalType);
            updateUI();
        }
        else {
            appraisal = getAppraisalDetail($("#main_content_hfAppraisalId").val());
        }
    });
}(jQuery));


////////////////////////////////////////
// API

function showPopup(popupType, arg, arg2) {
    switch (popupType) {
        case 1: // TargetDepartments
            var request = {
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'ManagerId': $("#main_content_hfManagerId").val()
            };

            jQuery.ajax({
                type: "POST",
                url: "/Api/Live360/GetDepartments",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(request),
                dataType: "json",
                crossDomain: true,
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        var html = "";
                        for (var i = 0; i < d.Departments.length; i++) {
                            html += "<label>";

                            var isChecked = false;
                            for (var j = 0; j < appraisal.TargetDepartments.length; j++) {
                                if (d.Departments[i].Id === appraisal.TargetDepartments[j].Id) {
                                    isChecked = true;
                                    break;
                                }
                            }

                            html += "   <input type=\"checkbox\" name=\"cbDepartments\"  value=\"" + d.Departments[i].Id + "," + d.Departments[i].Title + "\" style=\"margin: 0px; display: inline-block;\" " + (isChecked ? "checked" : "") + " />";
                            html += "   <span style=\"margin: 0px; display: inline-block;\">&nbsp;" + d.Departments[i].Title + "</span>";
                            html += "</label>";
                            html += "<br />";
                        }
                        $("#department_checkboxlist").html(html);

                        $("#mpe_backgroundElement").show();
                        $("#dvSelectDepartment").show();
                    } else {
                        ShowToast(d.ErrorMessage, 2);
                    }
                },
                error: function (xhr, status, error) {
                    ShowToast(error, 2);
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });

            break;

        case 2: // Delete Category
            var category = getCategory(arg);
            if (category.Cards == null || category.Cards == undefined || category.Cards.length == 0 || category.Cards[0].Id.indexOf('undefined') == 0) {
                $("#delCategoryMsg").html("Delete category: " + category.Title + ". Confirm?");
                $("#hlDelCategory").prop("href", "javascript:deleteCategory('" + arg + "');");
            }
            else {
                $("#delCategoryMsg").html("You can only delete empty categories.");
                $("#hlDelCategory").css("display", "none");
            }

            $("#mpe_backgroundElement").show();
            $("#dvDeleteCategory").show();
            break;

        case 3: // Delete Card
            var card = getCard(arg, arg2);

            $("#delCardMsg").html("Delete cards: " + card.Content + ". Confirm?");
            $("#hlDelCard").prop("href", "javascript:deleteCard('" + arg + "', '" + arg2 + "');");

            $("#mpe_backgroundElement").show();
            $("#dvDeleteCard").show();
            break;

        case 4: // Alert
            $("#mpe_backgroundElement").show();
            $("#dvAlert").show();
            break;

        default:
            break;
    }
}

function personnelAutoComplete() {
    $("#tbSearchUser").autocomplete({
        source: "/api/personnel?companyid=" + $("#main_content_hfCompanyId").val() + "&userid=" + $("#main_content_hfManagerId").val(),
        minLength: 1,
        response: function (event, ui) {
            if (ui != null) {
                for (var i = 0; i < ui.content.length; i++) {
                    for (var j = 0; j < appraisal.TargetUsers.length; j++) {
                        if (appraisal.TargetUsers[j].Id == ui.content[i].value) {
                            ui.content.splice(i, 1);
                            if (i != ui.content.length - 1) {
                                i--;
                            }
                            break;
                        }
                    }
                }
            }
        },
        select: function (event, ui) {
            var isExisting = false;
            for (var i = 0; i < appraisal.TargetUsers.length; i++) {
                if (appraisal.TargetUsers[i].Id == ui.item.value) {
                    isExisting = true;
                    break;
                }
            }

            if (!isExisting) {
                var user = new User();
                user.Id = ui.item.value;
                user.FirstName = ui.item.first_name;
                user.LastName = ui.item.last_name;
                user.Email = ui.item.email;
                user.IsAvailable = false;
                appraisal.TargetUsers.push(user);

                var html = "";
                html = "";
                html = html + "<div class=\"tag " + user.Id + "\">";
                html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + user.FirstName + " " + user.LastName + "(" + user.Email + ")</span>";
                html = html + "    <a class=\"tag__icon\" href=\"javascript:setAppraisalValue(11, this, '" + user.Id + "');\">x</a>";
                html = html + "</div>";
                $(".user.tags").append(html);
            }
            return false;
        },
        focus: function (event, ui) {
            event.preventDefault();
        },
        close: function (event, ui) {
            if (event.currentTarget != null) {
                $("#tbSearchUser").val("");
            }
        }
    });
}

function createAppraisal() {
    if (!isDataValid()) {
        return;
    }

    var request = new RequestObject();
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/CreateAppraisal",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast(appraisal.TypeLabel + " created", 1);
                RedirectPage("/Live360/" + appraisal.TypeLabel + "Detail/" + d.AppraisalId, 300);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function updateAppraisal() {
    if (!isDataValid()) {
        return;
    }
    var request = new RequestObject();
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/UpdateAppraisal",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast(appraisal.TypeLabel + " updated", 1);
                RedirectPage("/Live360/" + appraisal.TypeLabel + "Detail/" + request.AppraisalId, 300);

            } else {
                if (d.ErrorCode == -90024) {
                    $("#ddlStatus").children().each(function () {
                        if ($(this).text() == "Unlisted") {
                            $(this).attr("selected", "true");
                        }
                    });
                }
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function getAppraisalDetail(appraisalId) {
    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'AppraisalId': appraisalId
    };

    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/GetAppraisalDetail",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                appraisal = new Appraisal();
                appraisal.IsCreateMode = false;
                appraisal.Id = d.Template.AppraisalId;
                appraisal.Type = d.Template.AppraisalType;
                appraisal.TypeLabel = APPRAISAL_TYPE_LABEL[d.Template.AppraisalType];
                appraisal.Title = d.Template.Title;
                appraisal.Context = d.Template.Description;
                appraisal.PublishMethod = d.Template.PublishMethod;
                appraisal.StartDate = moment.utc(d.Template.StartDate).add(timeZone, "hours");

                if (d.Template.EndDate != null && d.Template.EndDate != undefined) {
                    appraisal.EndDate = moment.utc(d.Template.EndDate).add(timeZone, "hours");
                }
                else {

                    appraisal.EndDate = appraisal.StartDate.clone();
                    appraisal.EndDate.add(1, "months");
                }

                appraisal.IsPrioritized = d.Template.IsPrioritized;
                appraisal.Anonymity = d.Template.Anonymity;
                appraisal.IsCustomizeQuestion = d.Template.IsUserAllowToCreateQuestions;
                appraisal.Progress = d.Template.Progress;
                appraisal.Status = d.Template.Status;
                if (appraisal.Type == APPRAISAL_TYPE.LIKERT7) {
                    appraisal.MinLabel = d.Template.Likert7Scale[0].Label;
                    appraisal.MidLabel = d.Template.Likert7Scale[3].Label;
                    appraisal.MaxLabel = d.Template.Likert7Scale[6].Label;
                }


                appraisal.Categories = [];
                for (var i = 0; i < d.Template.Categories.length; i++) {
                    var category = new Category();
                    category.Id = d.Template.Categories[i].CategoryId;
                    category.Title = d.Template.Categories[i].Title
                    category.Cards = [];
                    for (var j = 0; j < d.Template.Categories[i].Cards.length; j++) {
                        var card = new Card();
                        card.CategoryId = d.Template.Categories[i].CategoryId;
                        card.Id = d.Template.Categories[i].Cards[j].CardId;
                        card.Ordering = d.Template.Categories[i].Cards[j].Ordering;
                        card.Content = d.Template.Categories[i].Cards[j].Content;
                        category.Cards.push(card);
                    }
                    appraisal.Categories.push(category);
                }

                appraisal.IsTargetedEveryone = true;
                appraisal.IsTargetedDepartments = false;
                appraisal.IsTargetedUsers = false;
                appraisal.TargetDepartments = [];
                appraisal.TargetUsers = [];

                if (!d.Template.Privacy.IsForEveryone) {

                    appraisal.IsTargetedEveryone = false;

                    if (d.Template.Privacy.IsForDepartment) {
                        appraisal.IsTargetedDepartments = true;
                        for (var i = 0; i < d.Template.Privacy.TargetedDepartments.length; i++) {
                            var department = new Department()
                            department.Id = d.Template.Privacy.TargetedDepartments[i].Id;
                            department.Title = d.Template.Privacy.TargetedDepartments[i].Title;
                            appraisal.TargetDepartments.push(department);
                        }
                    }

                    if (d.Template.Privacy.IsForUser) {
                        appraisal.IsTargetedUsers = true;
                        for (var i = 0; i < d.Template.Privacy.TargetedUsers.length; i++) {
                            var user = new User()
                            user.Id = d.Template.Privacy.TargetedUsers[i].UserId;
                            user.FirstName = d.Template.Privacy.TargetedUsers[i].FirstName;
                            user.LastName = d.Template.Privacy.TargetedUsers[i].LastName;
                            user.Email = d.Template.Privacy.TargetedUsers[i].Email;
                            appraisal.TargetUsers.push(user);
                        }
                    }
                }

                updateUI();
                refreshCategoryListUI();
                if (appraisal.Progress == PROGRESS.LIVE && appraisal.Status == STATUS.ACTIVE) {
                    disableElements();
                }

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function createCategory(categoryId) {
    var category = getCategory(categoryId);

    if (!isCategoryDataValid(category)) {
        return;
    }

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'AppraisalId': appraisal.Id,
        'Title': category.Title
    };
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/CreateAppraisalCategory",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                // update model
                var newCategory = new Category();
                newCategory.Id = d.CategoryId;
                newCategory.Title = category.Title;
                removeCategory(category.Id);
                appraisal.Categories.push(newCategory);

                // update UI
                $(".dvCategoryList").find(".category_layout." + category.Id).remove();
                $(".dvCategoryList").find(".category_hr." + category.Id).remove();
                $(".dvNewCategory").html("");
                $(".add-category").css("visibility", "visible");
                $(".dvCategoryList").append(getFullCategoryHtml(newCategory));

                ShowToast("Category created", 1);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function updateCategory(categoryId) {
    var category = getCategory(categoryId);

    if (!isCategoryDataValid(category)) {
        return;
    }

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'AppraisalId': appraisal.Id,
        'CategoryId': category.Id,
        'Title': category.Title
    };
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/UpdateAppraisalCategory",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                ShowToast("Category updated", 1);
                $(".category_layout." + categoryId + " .category_detail").html(getCategoryDetailHtml(category));

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function deleteCategory(categoryId) {

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'AppraisalId': appraisal.Id,
        'CategoryId': categoryId
    };
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/DeleteAppraisalCategory",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                // update model
                removeCategory(categoryId);

                // update UI
                $(".dvCategoryList").find(".category_layout." + categoryId).remove();
                $(".dvCategoryList").find(".category_hr." + categoryId).remove();
                hidePopup(2);

                ShowToast("Category deleted", 1);

            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function createCard(categoryId, cardId) {
    var card = getCard(categoryId, cardId);

    if (!isCardDataValid(card)) {
        return;
    }

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'AppraisalId': appraisal.Id,
        'CategoryId': card.CategoryId,
        'CardContent': card.Content,
        'Options': null
    };
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/CreateAppraisalCard",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                // update model
                var newCard = new Card();
                newCard.Id = d.CardId;
                newCard.CategoryId = categoryId;
                newCard.Content = card.Content;
                newCard.Ordering = card.Ordering;
                removeCard(card.CategoryId, card.Id);
                var category = getCategory(categoryId);
                category.Cards.push(newCard);

                for (var i = 0; i < appraisal.Categories.length; i++) {
                    if (appraisal.Categories[i].Id === categoryId) {
                        appraisal.Categories[i] = category;
                        break;
                    }
                }

                // update UI
                $(".category_layout." + categoryId).find(".dvNewCard").html("");
                $(".category_layout." + categoryId).find(".add-card").css("visibility", "visible");
                $(".dvCardList." + categoryId).append(getCardLayput(newCard));

                ShowToast("Card created", 1);
            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function updateCard(categoryId, cardId) {
    var card = getCard(categoryId, cardId);

    if (!isCardDataValid(card)) {
        return;
    }

    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'AppraisalId': appraisal.Id,
        'CategoryId': card.CategoryId,
        'CardId': card.Id,
        'CardContent': card.Content,
        'Options': null
    };
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/UpdateAppraisalCard",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                // update UI
                $(".card_detail." + categoryId + "." + cardId).html(getCardLayput(card));

                ShowToast("Card updated", 1);
            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}

function deleteCard(categoryId, cardId) {
    var request = {
        'CompanyId': $("#main_content_hfCompanyId").val(),
        'ManagerId': $("#main_content_hfManagerId").val(),
        'AppraisalId': appraisal.Id,
        'CategoryId': categoryId,
        'CardId': cardId

    };
    // call API
    jQuery.ajax({
        type: "POST",
        url: "/Api/Live360/DeleteAppraisalCard",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(request),
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr, settings) {
            ShowProgressBar();
        },
        success: function (d, status, xhr) {
            if (d.Success) {
                var indexCard = -1;
                var category = getCategory(categoryId);

                for (var i = 0; i < category.Cards.length; i++) {
                    if (category.Cards[i].Id == cardId) {
                        indexCard = i;
                        break;
                    }
                }

                // update UI
                for (var i = indexCard; i < category.Cards.length; i++) {
                    $(".card_detail." + categoryId + "." + category.Cards[i].Id).find(".card-ordering").html("Q" + (category.Cards[i].Ordering - 1));
                }
                $(".card_detail." + categoryId + "." + cardId).remove();
                hidePopup(3);

                // update model
                removeCard(categoryId, cardId);

                ShowToast("Card deleted", 1);
            } else {
                ShowToast(d.ErrorMessage, 2);
            }
        },
        error: function (xhr, status, error) {
            ShowToast(error, 2);
        },
        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            HideProgressBar();
        }
    });
}