﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Module
{
    public partial class ModuleList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        private String companyId = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }

            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                // ViewState variables for used with sorting of listview
                //ViewState["currentSortField"] = "lbSortAssessmentTitle";
                //ViewState["currentSortDirection"] = "asc";
            } // if (!Page.IsPostBack)

            // Get company Id
            if (Page.RouteData.Values["CompanyId"] != null)
            {
                companyId = Page.RouteData.Values["CompanyId"].ToString();
            }

            // Load survey list
            RefreshContentListView();
        }

        private void RefreshContentListView()
        {
            ListView lv = this.lvModule;
            LinkButton lb;

            // Fetch data and sort
            CassandraService.Entity.Module module = new CassandraService.Entity.Module();
            List<CassandraService.Entity.Module> masterModuleList = module.GetAllModules().OrderBy(r => r.Key).ToList();
            List<CassandraService.Entity.Module> companyModuleList = module.GetAllModules(companyId).OrderBy(r => r.Key).ToList();
            
            List<ModuleViewModel> moduleVMList = new List<ModuleViewModel>();

            foreach (CassandraService.Entity.Module current_module in masterModuleList)
            {
                ModuleViewModel vm = new ModuleViewModel();

                CassandraService.Entity.Module foundModule = companyModuleList.Where(r => r.Key == current_module.Key).FirstOrDefault();
                vm.Key = current_module.Key;
                vm.Description = current_module.Description;
                vm.Title = current_module.Title;
                vm.IconUrl = current_module.IconUrl;
                vm.IconUrlOff = current_module.IconUrlOff;
                vm.IconUrlOn = current_module.IconUrlOn;
                vm.RightsDescription = current_module.RightsDescription;

                if (foundModule == null)
                {
                    vm.HaveAccess = false;
                }
                else
                {
                    vm.HaveAccess = true;
                }

                moduleVMList.Add(vm);
            }

            this.lvModule.DataSource = moduleVMList.OrderBy(r => r.Title).ToList();
            this.lvModule.DataBind();

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        // Listview event handlers
        protected void lvSurvey_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            System.Web.UI.WebControls.LinkButton srcComponent = null;
            srcComponent = e.CommandSource as System.Web.UI.WebControls.LinkButton;
            int moduleId;

            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Response.Redirect(string.Format("/Module/List/{0}", e.CommandArgument), false);
                    // Context.ApplicationInstance.CompleteRequest();

                    // Update DB update
                    // companyId, e.CommandArgument
                    
                    if (int.TryParse(e.CommandArgument.ToString(), out moduleId))
                    {
                        if (srcComponent.Text.Equals("True", StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Delete record
                            asc.DeleteSystemModule(companyId, moduleId);
                        }
                        else
                        {
                            // Add record
                            asc.CreateSystemModule(companyId, moduleId);
                        }

                        // Refresh Content ListView
                        RefreshContentListView();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        protected void lvSurvey_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }

    }

    public class ModuleViewModel : CassandraService.Entity.Module
    {
        public bool HaveAccess { get; set; }

        public ModuleViewModel()
        {
            this.HaveAccess = false;
        }
    }
}