﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ModuleList.aspx.cs" Inherits="AdminWebsite.Module.ModuleList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>


    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/" style="color: #000;">Module <span>console</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="surveyCountLiteral" runat="server" />
        </div>
        <div class="appbar__action">
            <%--<a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
            <div style="font-size: small;">
                <span style="color: black">Status</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #FECA02"></i>Draft&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #94CF00"></i>Live&nbsp;&nbsp;&nbsp;&nbsp;
            </div>--%>
        </div>
    </div>
    <!-- /App Bar -->


    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Module/Companies">Company List</a>
                </li>

<%--                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Company List</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/Subscription">Subscription</a>
                </li>--%>
            </ul>

            <%--<div id="main_content_upFilter">
                <div class="pad"></div>
            </div>--%>

            <%--<div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Assessment</label>
                        <asp:TextBox ID="tbFilterSurvey" runat="server" placeholder="Type the assessment's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterSurvey" runat="server" OnClick="ibFilterSurvey_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>--%>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    
<asp:ListView ID="lvModule" runat="server" OnItemCommand="lvSurvey_ItemCommand" OnItemEditing="lvSurvey_ItemEditing">
    <EmptyDataTemplate>
        <br />
        <br />
        <p style="color: #999; text-align: center;">
            <asp:Literal ID="ltlEmptyMsg" runat="server" />
        </p>
    </EmptyDataTemplate>
    <LayoutTemplate>
        <table runat="server" class="dataTable hover">
            <thead>
                <tr runat="server">
                    <th class="no-sort" style="width: 40px;"></th>
                    <th>
                        Title
                        <%--<asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentTitle" ID="lbSortAssessmentTitle" OnClientClick="ShowProgressBar('Filtering');">Title</asp:LinkButton>--%>

                    </th>
                    <th>
                        Description
                        <%--<asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentCoach" ID="lbSortAssessmentCoach" OnClientClick="ShowProgressBar('Filtering');">Description</asp:LinkButton>--%>

                    </th>
                    <th>
                        Have access
                        <%--<asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentStatus" ID="lbSortAssessmentStatus" OnClientClick="ShowProgressBar('Filtering');">Have access</asp:LinkButton>--%>

                    </th>
                    <th class="no-sort"></th>
                    <th class="no-sort" style="width: 5%;"></th>
                </tr>
            </thead>
            <tbody>
                <tr id="ItemPlaceholder" runat="server"></tr>
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr>
            <td>
                <asp:HiddenField runat="server" ID="ModuleIdHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />
                <%--<div class="constrained" style="display: inline-block;">
                    <asp:Image ID="imgSurvey" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "IconUrl") %>' runat="server" />
                </div>--%>
            </td>
            <td>
                <asp:Literal ID="Literal1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>'></asp:Literal>
                <%--<asp:LinkButton ID="lbAssessmentName" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Key") + "/"   %>' Style="color: #999;" />--%>
            </td>
            <td>
                <asp:Literal ID="CategoryLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Description") %>'></asp:Literal>
            </td>
            <td>
                
                <asp:LinkButton ID="lbChangeStatus" Text='<%# DataBinder.Eval(Container.DataItem, "HaveAccess") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Key") %>' />

                <%--
                    <asp:Literal Mode="PassThrough" Text="<i class='fa fa-circle' style='color:#94CF00'></i> Active" runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "Status").ToString() == "2" %>' />
                    <asp:Literal Mode="PassThrough" Text="<i class='fa fa-circle' style='color:#FECA02'></i> Unlisted/Hidden" runat="server" Visible='<%# ((DataBinder.Eval(Container.DataItem, "Status").ToString() == "1") || (DataBinder.Eval(Container.DataItem, "Status").ToString() == "3")) %>' />
                --%>
            </td>
            <td>
<%--                <div class="post__user__action">
                    <div class="chips--02">
                        <div class="chips--02__container">
                            <i class="fa fa-ellipsis-v chips--02__button"></i>
                            <ul class="chips--02__menu">
                                <li>
                                    <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Edit assessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Key") + "/"  %>' />
                                </li>
                                <li>
                                    <asp:LinkButton ID="lbRename" ClientIDMode="AutoID" runat="server" CommandName="RenameAssessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Key") %>' Text="Rename assessment" />
                                </li>

                                <li>
                                    <asp:LinkButton ID="lbDuplicate" ClientIDMode="AutoID" runat="server" CommandName="DuplicateAssessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Key") %>' Text="Duplicate assessment" />
                                </li>

                                <li>
                                    <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="DeleteAssessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Key") %>' Text="Delete assessment" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
--%>
            </td>
            <td></td>
        </tr>
    </ItemTemplate>
</asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
