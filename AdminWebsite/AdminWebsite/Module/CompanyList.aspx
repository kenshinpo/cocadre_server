﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="CompanyList.aspx.cs" Inherits="AdminWebsite.Module.CompanyList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/" style="color: #000;">Module <span>console</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="surveyCountLiteral" runat="server" />
        </div>
        <div class="appbar__action">
            <%--<a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
            <div style="font-size: small;">
                <span style="color: black">Status</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #FECA02"></i>Draft&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #94CF00"></i>Live&nbsp;&nbsp;&nbsp;&nbsp;
            </div>--%>
        </div>
    </div>
    <!-- /App Bar -->

<div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage Access</div>
            <%--<ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Access</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/Coach">Coach</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/Subscription">Subscription</a>
                </li>
            </ul>--%>

            <%--<div id="main_content_upFilter">
                <div class="pad"></div>
            </div>--%>

            <%--<div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Assessment</label>
                        <asp:TextBox ID="tbFilterSurvey" runat="server" placeholder="Type the assessment's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterSurvey" runat="server" OnClick="ibFilterSurvey_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>--%>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    
<asp:ListView ID="lvCompany" runat="server" OnItemCommand="lvSurvey_ItemCommand" OnItemEditing="lvSurvey_ItemEditing">
    <EmptyDataTemplate>
        <br />
        <br />
        <p style="color: #999; text-align: center;">
            <asp:Literal ID="ltlEmptyMsg" runat="server" />
        </p>
    </EmptyDataTemplate>
    <LayoutTemplate>
        <table runat="server" class="dataTable hover">
            <thead>
                <tr runat="server">
                    <th class="no-sort" style="width: 40px;"></th>
                    <th>
                        Company name
                        <%--<asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortCompanyName" ID="lbSortCompanyName" OnClientClick="ShowProgressBar('Filtering');">Company name</asp:LinkButton>--%>

                    </th>
                    <%--<th>
                        <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentCoach" ID="lbSortAssessmentCoach" OnClientClick="ShowProgressBar('Filtering');">Description</asp:LinkButton></th>
                    <th>
                        <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentStatus" ID="lbSortAssessmentStatus" OnClientClick="ShowProgressBar('Filtering');">Status</asp:LinkButton></th>--%>
                    <th class="no-sort"></th>
                    <th class="no-sort" style="width: 5%;"></th>
                </tr>
            </thead>
            <tbody>
                <tr id="ItemPlaceholder" runat="server"></tr>
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr>
            <td>
                <asp:HiddenField runat="server" ID="ModuleIdHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "CompanyId") %>' />
                <div class="constrained" style="display: inline-block;">
                    <asp:Image ID="imgSurvey" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "CompanyLogoUrl") %>' runat="server" />
                </div>
            </td>
            <td>
                <asp:LinkButton ID="lbAssessmentName" Text='<%# DataBinder.Eval(Container.DataItem, "CompanyTitle") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CompanyId") + "/"   %>' Style="color: #999;" /></td>
            
            <td>
                <div class="post__user__action">
                    <div class="chips--02">
                        <div class="chips--02__container">
                            <i class="fa fa-ellipsis-v chips--02__button"></i>
                            <ul class="chips--02__menu">
                                <li>
                                    <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Update access" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CompanyId") + "/"  %>' />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </td>
            <td></td>
        </tr>
    </ItemTemplate>
</asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
