﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Module
{
    public partial class CompanyList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }

            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                String categoryId = String.Empty;
            } // if (!Page.IsPostBack)

            // Load survey list
            RefreshContentListView();
        }

        private void RefreshContentListView()
        {
            ListView lv = this.lvCompany;
            LinkButton lb;

            // Fetch data and sort


            //CassandraService.Entity.Module module = new CassandraService.Entity.Module();
            //List<CassandraService.Entity.Module> masterModuleList = module.GetAllModules().OrderBy(r => r.Key).ToList();
            List<CassandraService.Entity.Company> companyList = this.adminInfo.Companies;
            lv.DataSource = companyList.OrderBy(r => r.CompanyTitle).ToList();
            lv.DataBind();

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        // Listview event handlers
        protected void lvSurvey_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Module/List/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        protected void lvSurvey_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }
    }
}