﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SurveyCreate.aspx.cs" Inherits="AdminWebsite.Survey.SurveyCreate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<style>
		.radiobuttonlist-inline td {
			padding:5px;
		}
		.radiobuttonlist-inline label {
			display:inline;
		}
		.inline-list label, 
		.inline-list select {
			display:inline;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
	<asp:LinkButton ID="lbEmpty" runat="server"></asp:LinkButton>
	<!-- App Bar -->
	<div class="appbar">
		<div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
		<div class="appbar__title">Survey console</div>
		<div class="appbar__meta">Create Survey</div>
		<div class="appbar__action">
			<a href="javascript:void(0);"><i class="fa fa-ellipsis-v"></i></a>
			<a href="javascript:void(0);"><i class="fa fa-question-circle"></i></a>
			<a class="data-sidebar-toggle" href="javascript:void(0);"><i class="fa fa-filter"></i></a>
		</div>
	</div>
	<!-- / App Bar -->

	<asp:UpdatePanel ID="plAddIcon" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Panel ID="popup_addtopicicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
				<h1 class="popup__title">Choose Survey Icon</h1>
				<div class="topicicons">
					<p>Select from list</p>
					<asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand">
						<HeaderTemplate>
							<div class="topicicons__list">
						</HeaderTemplate>
						<ItemTemplate>
							<label class="topicicons__icon">
								<asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
									<asp:Image ID="imgChooseIcon" runat="server" />
								</asp:LinkButton>
							</label>
						</ItemTemplate>
						<FooterTemplate>
							</div>
						</FooterTemplate>
					</asp:Repeater>
				</div>
				<div class="popup__action">
					<asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbAddCancel_Click">Cancel</asp:LinkButton>
				</div>
			</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>

	<div class="data__content" onscroll="sticky_div();">

		<!-- New  Survey -->
		<div class="container">
			<div class="card add-topic">
				<div class="add-topic__icon">
					<asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
						<asp:Image ID="imgTopic" ImageUrl="~/img/icon-topicicon-default.png" runat="server" />
						<label><small>Choose topic icon</small></label>
					</asp:LinkButton>
				</div>
				<div class="add-topic__info">
					<div class="add-topic__info__action" style="float: right">
						<asp:LinkButton ID="lbCreate" runat="server" CssClass="btn" OnClick="lbCreate_Click">Create</asp:LinkButton>
						<asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/Survey/SurveyList" Text="Cancel" CssClass="btn secondary" />
						<asp:LinkButton ID="lbPreview" runat="server" CssClass="btn secondary">Preview</asp:LinkButton>
					</div>
					<div class="add-topic__info__details">
						<div id="" class="tabs tabs--styled">
							<ul class="tabs__list">
								<li class="tabs__list__item">
									<a class="tabs__link" href="#survey-info">Survey</a>
								</li>
								<li class="tabs__list__item">
									<a class="tabs__link" href="#survey-settings">Settings</a>
								</li>
								<%--
								<li class="tabs__list__item">
									<a class="tabs__link" href="#survey-scoring">scoring</a>
								</li>
								<li class="tabs__list__item">
									<a class="tabs__link" href="#survey-share">Share</a>
								</li>
								<li class="tabs__list__item">
									<a class="tabs__link" href="#survey-privacy">Privacy</a>
								</li>
								--%>
							</ul>
							<div class="tabs__panels">
								<div class="tabs__panels__item add-topic__info__details--basicinfo" id="survey-info">
									<div class="column--input">
										<div class="form__row">
											Title: 
											<asp:TextBox ID="tbTopictitle" runat="server" placeholder="Survey Title" MaxLength="20" />
										</div>
										<div class="form__row">
											Description: 
											<asp:TextBox ID="tbTopicDescription" runat="server" MaxLength="120" placeholder="Description" />
										</div>
									</div>
									<div class="column--choice">
										<div class="form__row">
											<label>Category</label>
											<div>
												<ajaxToolkit:ComboBox ID="cbCategory" runat="server" AutoCompleteMode="Suggest" CssClass="combos" MaxLength="30" AppendDataBoundItems="true">
													<asp:ListItem Text="Type or select category" Selected="True" />
												</ajaxToolkit:ComboBox>
												<asp:HiddenField ID="hfCategoryId" runat="server" />
											</div>
										</div>
										<div class="form__row">
											<label>Status</label>
											<div>
												<ajaxToolkit:ComboBox ID="cbStatus" runat="server" AutoCompleteMode="Suggest" DropDownStyle="DropDownList" CssClass="combos" MaxLength="30" AppendDataBoundItems="true">
													<asp:ListItem Text="Select Status" Selected="True" />
												</ajaxToolkit:ComboBox>
												<asp:HiddenField ID="HiddenField1" runat="server" />
											</div>
										</div>
									
									</div>
								</div>
								<div class="tabs__panels__item add-topic__info__details--settings" id="survey-settings">
									<div class="form__row">

										<label>No of questions per game</label><br />
										<div class="mdl-selectfield">
											<asp:DropDownList ID="ddlNoQuestionPerGame" runat="server" />
										</div>

									</div>
								</div>
							</div>
						
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- / New  Survey -->

	<asp:ListView runat="server" id="QuestionListView" OnItemDataBound="QuestionListView_ItemDataBound" OnItemCommand="QuestionListView_ItemCommand">
		<LayoutTemplate>
			<div class="container">
				<div id="ItemPlaceholder" runat="server"></div>
				
				<div class="" style="height:300px;"><!-- This should always be at the bottom -->
					<div class="column--input" style="float:left;">
						<div>All questions and answers will be randomized.</div>
					</div>
					<div class="column--input" style="float:right;">
						<asp:Button Text="+ Add a question" runat="server" />
					</div>
				</div>
			</div>

		</LayoutTemplate>
		<ItemTemplate>
			<div class="card add-question">
				<div class="add-question__questionnumber">
					<div class="number">
						<asp:Literal ID="QuestionNoLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuestionNo") %>' />
					</div>
					<label>
						Code<br />
						<asp:Literal ID="QuestionIdLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuestionId") %>' />
					</label>
				</div>

				<div class="add-question__info">
					<div class="add-question__info__header">
						<div class="add-question__info__header__action">
							<asp:LinkButton ID="lbNewQuestionCancel" runat="server" CssClass="btn secondary" Text="Cancel" OnClick="lbNewQuestionCancel_Click" />
							<asp:LinkButton ID="lbNewQuestionSave" runat="server" CssClass="btn" OnClick="lbNewQuestionSave_Click" Text="Create" OnClientClick="ShowProgressBar();" />
						</div>
						<div class="add-question__info__header__title">Question Type</div>
					</div>

					<div class="add-question__info__setup__questiontype">
						<div class="form__row">
							<asp:RadioButtonList runat="server" id="QuestionTypeRadioButtonList" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="radiobuttonlist-inline">
								<asp:ListItem Text="Select one" Value="1" />
								<asp:ListItem Text="Multi choice" Value="2" />
								<asp:ListItem Text="Text" Value="3" />
								<asp:ListItem Text="Number range" Value="4" />
								<asp:ListItem Text="Drop list" Value="5" />
								<asp:ListItem Text="Instructions" Value="6" />
							</asp:RadioButtonList>
						</div>

						<div class="form__row">

							<div>
								<asp:TextBox ID="QuestionTextBox" placeholder="Question" TextMode="MultiLine" runat="server" 
									onkeydown="return (event.keyCode!=13);" Text='<%# DataBinder.Eval(Container.DataItem, "QuestionText") %>' />
							</div>
							&nbsp;
							<asp:Panel runat="server" ID="MultipleAnswerPanel" Visible="false">
								<div>
									<asp:ListView runat="server" ID="MultipleAnswerListView">
										<LayoutTemplate>
											<div runat="server" id="ItemPlaceHolder"></div>
										</LayoutTemplate>
										<ItemTemplate>
											<div class="form__row form__row--answer">
												<asp:TextBox ID="tbEditAnswer1" placeholder="Answer" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" Text='<%# DataBinder.Eval(Container.DataItem, "Text") %>' />
												<asp:Label ID="lblEditAnswer1Count" runat="server" CssClass="lettercount" />
												<asp:LinkButton Text="X" CommandName="DeleteAnswer" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' runat="server" />
												<asp:Image ImageUrl="" runat="server" />
												<asp:HiddenField runat="server" ID="QuestionAnswernid" Value='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />
											</div>
										</ItemTemplate>
									</asp:ListView>
								</div>
							</asp:Panel>
							<asp:Panel runat="server" ID="NumberRangePanel" Visible="false">
								<div>
									<asp:Literal Text="Number range" runat="server" /><br />
									<asp:Label Text="Minimum value" runat="server" />
									<asp:TextBox runat="server" id="MinimumValueTextBox" Text="" TextMode="Number" />

									<asp:Label Text="Maximum value" runat="server" />
									<asp:TextBox runat="server" id="MaximumValueTextBox" Text="" TextMode="Number" />

								</div>
							</asp:Panel>
							<asp:Panel runat="server" ID="InstructionPanel" Visible="false">
								<div>
									display panel for instruction panel
								</div>
							</asp:Panel>

							&nbsp;
							<div>
								<asp:Button Text="Add Answer" CommandName="AddAnswer" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "QuestionId") %>' runat="server" />
							</div>
						</div>

					</div>

					<div class="add-question__info__setup__preview">
						<!-- Add graphical preview here -->
						<asp:Panel ID="plEditPreview" runat="server" CssClass="question-preview">
							<div class="question-preview__header">
								<div class="question-preview__header__score">
									<div class="Media">
										<img class="Media-figure" src="/img/image-01.jpg" alt="" />
										<div class="Preview-body">
											<span>User 1</span>
											<div class="score">Score</div>
										</div>
									</div>
								</div>
								<div class="question-preview__header__timer">Time Left <span>10</span></div>
								<div class="question-preview__header__score question-preview__header__score--user02">
									<div class="Media">
										<img class="Media-figure" src="/img/image-01.jpg" alt="" />
										<div class="Preview-body">
											<span>User 2</span>
											<div class="score">Score</div>
										</div>
									</div>
								</div>
							</div>
							<div class="question-preview__question">
								<p>
									<div class="question-preview__content">
										<asp:Label ID="lblEditPreviewQuestion" runat="server" />
									</div>
								</p>
							</div>
							<asp:Panel ID="plEditPreviewImg" runat="server" CssClass="question-preview__image" Visible="false">
								<asp:HiddenField ID="hfOriImageMD5" runat="server" />
								<asp:Image ID="imgEditPreviewImg" runat="server" />
							</asp:Panel>

							<div class="question-preview__answers">
								<div class="question-preview__answer">
									<div style="word-wrap: break-word;">
										<asp:Label ID="lblEditPreviewQuestionAnswer1" runat="server" Style="word-wrap: break-word; white-space: pre;" />
									</div>
								</div>
								<div class="question-preview__answer">
									<div style="word-wrap: break-word;">
										<asp:Label ID="lblEditPreviewQuestionAnswer2" runat="server" Style="word-wrap: break-word; white-space: pre;" />
									</div>
								</div>
								<div class="question-preview__answer">
									<div style="word-wrap: break-word;">
										<asp:Label ID="lblEditPreviewQuestionAnswer3" runat="server" Style="word-wrap: break-word; white-space: pre;" />
									</div>
								</div>
								<div class="question-preview__answer">
									<div style="word-wrap: break-word;">
										<asp:Literal ID="Literal1" runat="server"></asp:Literal>
										<asp:Label ID="lblEditPreviewQuestionAnswer4" runat="server" Style="word-wrap: break-word; white-space: pre;" />
									</div>
								</div>
							</div>
						</asp:Panel>
						<!-- / Question Preview Format -->
					</div>

					<div class="add-question__info__setup__choice">
						<div class="form__row">
							<asp:Label Text="Status" runat="server" />
							<asp:DropDownList runat="server" id="QuestionStatus">
								<asp:ListItem Text="Active" />
								<asp:ListItem Text="Unlist" />
							</asp:DropDownList>
						</div>
						
						<div class="form__row inline-list">
							<asp:Label Text="Behaviour" runat="server" /><br />
							<asp:CheckBox Text="Allow question to be skipped" runat="server" id="AllowQuestionToBeSkippedCheckBox" TextAlign="Right" /> <br />
							<asp:CheckBox Text="Tick &quot;other&quot; and type your own answer" runat="server" id="AllowOthersCheckBox" /> <br />
							<asp:CheckBox Text="Randomize option order" runat="server" id="RandomizeOptionOrderCheckBox" /> <br />
						</div>

						<div class="form__row">
							<asp:Label Text="Display" runat="server" /><br />
							<asp:Label Text="Answers" runat="server" />
							<asp:DropDownList runat="server">
								<asp:ListItem Text="Auto Columns" />
								<asp:ListItem Text="1" />
								<asp:ListItem Text="2" />
								<asp:ListItem Text="3" />
								<asp:ListItem Text="4" />
								<asp:ListItem Text="5" />
							</asp:DropDownList>
						</div>

						<div class="form__row inline-list">
							<asp:CheckBox Text="Page Break" runat="server" id="PageBreak" />
						</div>
					</div>

				</div>
				
				<div class="add-question__info">
					<div style="float:left; width:400px;">
						<div class="inline-list">
							<asp:DropDownList runat="server">
								<asp:ListItem Text="When answer is" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
							<asp:DropDownList runat="server">
										<asp:ListItem Text="Please select answer" />
										<asp:ListItem Text="text2" />
									</asp:DropDownList>
							<asp:LinkButton Text="X" runat="server"></asp:LinkButton>
						</div>
						<div class="inline-list">
							<asp:DropDownList runat="server">
										<asp:ListItem Text="Skip to question" />
										<asp:ListItem Text="text2" />
									</asp:DropDownList>
							<asp:DropDownList runat="server">
										<asp:ListItem Text="Please select question" />
										<asp:ListItem Text="text2" />
									</asp:DropDownList>
						</div>
					</div>

					<div style="float:right;">
						<button>Delete question</button>
					</div>
				</div>
				
			</div>
		</ItemTemplate>
		
	</asp:ListView>

	</div>

	<script type="text/javascript">
		//function sticky_div() {
		//    var window_top = $(window).scrollTop();

		//    var divTopId = $('#sticky_anchor');
		//    if (!divTopId.length) {
		//        return;
		//    }
		//    var div_top = divTopId.offset().top; // get the offset of the div from the top of page

		//    // var div_top = $('#sticky-anchor').offset().top;
		//    if (window_top + 160 > div_top) {
		//        $('.sticky').addClass('stick');
		//        $('#sticky_anchor').addClass('stick');

		//    } else {
		//        $('.sticky').removeClass('stick');
		//        $('#sticky_anchor').removeClass('stick');

		//    }
		//}
	</script>
</asp:Content>
