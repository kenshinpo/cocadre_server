﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Survey
{
    public partial class SurveyCreate : System.Web.UI.Page
    {
        private static ILog Log = LogManager.GetLogger("AdminWebsiteLog");

        private App_Code.Entity.ManagerInfo adminInfo;
        private CassandraService.ServiceInterface.AdminService asc = new CassandraService.ServiceInterface.AdminService();
        private List<String> listIconUrl = null;
        private List<CassandraService.Entity.SurveyQuestion> surveyQuestionList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetupSampleQuestion();


            //sampleaa
            if (!Page.IsPostBack)
            {
                SetupCategoryDropDownList();
                SetupStatusDropDownList();


            }
        }

        public void SetupCategoryDropDownList()
        {
            Dictionary<string, string> category = new Dictionary<string, string>();
            category.Add("cat1", "cat1");
            category.Add("cat2", "cat2");
            category.Add("cat3", "cat3");
            category.Add("cat4", "cat4");

            this.cbCategory.DataTextField = "Key";
            this.cbCategory.DataValueField = "Value";
            this.cbCategory.DataSource = category;
            this.cbCategory.DataBind();

        }

        public void SetupStatusDropDownList()
        {
            Dictionary<string, string> category = new Dictionary<string, string>();
            category.Add("List", "cat1");
            category.Add("Unlist", "cat2");
            category.Add("Hidden", "cat3");

            this.cbCategory.DataTextField = "Key";
            this.cbCategory.DataValueField = "Value";
            this.cbCategory.DataSource = category;
            this.cbCategory.DataBind();
        }

        public void SetupSampleQuestion()
        {
            CassandraService.Entity.SurveyQuestion q1;

            // SETUP SAMPLE DATA
            this.surveyQuestionList = new List<CassandraService.Entity.SurveyQuestion>();

            q1 = new CassandraService.Entity.SurveyQuestion
            {
                QuestionNo = "1",
                QuestionId = "questionId_1",
                QuestionText = "some q1",
                QuestionType = CassandraService.Entity.Survey.RSQuestionType.SelectOne
            };
            q1.AnswerList.Add(new CassandraService.Entity.SurveyQuestionAnswer
            {
                Id = "questionId_1_answerId_1",
                Text = "Question 1 Sample Answer 1"
            });
            this.surveyQuestionList.Add(q1);

            q1 = new CassandraService.Entity.SurveyQuestion
            {
                QuestionNo = "2",
                QuestionId = "questionId_2",
                QuestionText = "some q2",
                QuestionType = CassandraService.Entity.Survey.RSQuestionType.SelectOne
            };
            q1.AnswerList.Add(new CassandraService.Entity.SurveyQuestionAnswer
            {
                Id = "questionId_2_answerId_1",
                Text = "Question 2 Sample Answer 1"
            });
            this.surveyQuestionList.Add(q1);

            q1 = new CassandraService.Entity.SurveyQuestion
            {
                QuestionNo = "3",
                QuestionId = "questionId_3",
                QuestionText = "some q3",
                QuestionType = CassandraService.Entity.Survey.RSQuestionType.SelectOne
            };
            q1.AnswerList.Add(new CassandraService.Entity.SurveyQuestionAnswer
            {
                Id = "questionId_3_answerId_1",
                Text = "Question 3 Sample Answer 1"
            });
            this.surveyQuestionList.Add(q1);

            this.QuestionListView.DataSource = this.surveyQuestionList;
            this.QuestionListView.DataBind();
        }

        public void SetCategoryDropDownList()
        {

        }

        public void SetSurveyStatusDropDownList()
        {

        }

        protected void lbAddCancel_Click(object sender, EventArgs e)
        {

        }
        protected void lbAddIcon_Click(object sender, EventArgs e)
        {

        }

        protected void lbCreate_Click(object sender, EventArgs e)
        {
        }


        ////////////////////////////////////////
        // Event handlers for buttons on question card
        ////////////////////////////////////////

        public void lbNewQuestionCancel_Click(object sender, EventArgs e)
        {

        }
        public void lbNewQuestionSave_Click(object sender, EventArgs e)
        {

        }
        


        ////////////////////////////////////////
        // Survey Icon selector
        ////////////////////////////////////////

        public void GetIconData()
        {
            if (listIconUrl == null)
            {
                CassandraService.ServiceResponses.TopicSelectIconResponse response = asc.SelectAllTopicIcons(adminInfo.UserId, adminInfo.CompanyId);
                if (response.Success)
                {
                    listIconUrl = response.TopicIconUrls;
                }
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    GetIconData();
                    //System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    //imgChooseIcon.ImageUrl = listIconUrl[e.Item.ItemIndex];
                    //LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    //lbChooseIcon.CommandArgument = listIconUrl[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    Log.Debug(e.CommandArgument.ToString());
                    //imgTopic.ImageUrl = e.CommandArgument.ToString();
                    //mpeAddIcon.Hide();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        ////////////////////////////////////////
        // QuestionListView Events Handlers
        ////////////////////////////////////////

        protected void QuestionListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            //QuestionTypeRadioButtonList.
            //e.Item.FindControl("QuestionTypeRadioButtonList").

            CassandraService.Entity.SurveyQuestion surveyQuestion = e.Item.DataItem as CassandraService.Entity.SurveyQuestion;
            System.Web.UI.WebControls.Panel displayPanel = null;
            System.Web.UI.WebControls.RadioButtonList questionTypeRadioButtonList = e.Item.FindControl("QuestionTypeRadioButtonList") as System.Web.UI.WebControls.RadioButtonList;


            if ((surveyQuestion != null) & (questionTypeRadioButtonList != null))
            {
                questionTypeRadioButtonList.SelectedValue = ((int)surveyQuestion.QuestionType).ToString();

                // Set display depending on the QuestionType
                //MultipleAnswerPanel
                //OpenTextPanel
                //NumberRangePanel
                //InstructionPanel

                switch (surveyQuestion.QuestionType)
                {
                    case CassandraService.Entity.Survey.RSQuestionType.Instructions:
                        displayPanel = e.Item.FindControl("InstructionPanel") as System.Web.UI.WebControls.Panel;
                        if (displayPanel != null)
                            displayPanel.Visible = true;
                        break;
                    case CassandraService.Entity.Survey.RSQuestionType.NumberRange:
                        displayPanel = e.Item.FindControl("NumberRangePanel") as System.Web.UI.WebControls.Panel;
                        if (displayPanel != null)
                            displayPanel.Visible = true;
                        break;
                    //case CassandraService.Entity.Survey.RSQuestionType.Text:
                    //    displayPanel = e.Item.FindControl("OpenTextPanel") as System.Web.UI.WebControls.Panel;
                    //    if (displayPanel != null)
                    //        displayPanel.Visible = true;
                    //    break;
                    case CassandraService.Entity.Survey.RSQuestionType.DropList:
                    case CassandraService.Entity.Survey.RSQuestionType.MultiChoice:
                    case CassandraService.Entity.Survey.RSQuestionType.SelectOne:
                        displayPanel = e.Item.FindControl("MultipleAnswerPanel") as System.Web.UI.WebControls.Panel;
                        if (displayPanel != null)
                            displayPanel.Visible = true;
                        break;
                }

                System.Web.UI.WebControls.ListView multipleAnswerListView = e.Item.FindControl("MultipleAnswerListView") as System.Web.UI.WebControls.ListView;
                if (multipleAnswerListView != null)
                {
                    multipleAnswerListView.DataSource = surveyQuestion.AnswerList;
                    multipleAnswerListView.DataBind();
                }



            }
            
            
        }

        protected void QuestionListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {

        }
    }
}

