﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
namespace AdminWebsite.Survey
{
    public partial class SurveyAnalyticDetail : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout", false);
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                String jsCommand = @"
                    var CompanyId = '" + managerInfo.CompanyId + @"';
                    var AdminUserId = '" + managerInfo.UserId + @"';
                    var TopicId = '" + Page.RouteData.Values["TopicId"].ToString() + @"';
                    var CategoryId = '" + Page.RouteData.Values["CategoryId"].ToString() + @"';
                    var CardId = '" + Page.RouteData.Values["CardId"].ToString() + @"';
                ";

                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "defineVar", jsCommand, true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}