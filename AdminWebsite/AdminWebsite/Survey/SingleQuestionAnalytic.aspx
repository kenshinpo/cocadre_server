﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SingleQuestionAnalytic.aspx.cs" Inherits="AdminWebsite.Survey.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .error_msg { text-align: center; display: none;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title">Responsive <span>survey</span></div>
        <div class="appbar__meta"><a href="" id="survey-root-link"></a></div>
        <div class="appbar__meta"><a href="" id="simple-survey-link">Overview</a></div>
        <div class="appbar__meta"><span id="question_id"></span></div>
        <div class="appbar__meta breadcrumb-participant" ></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="survey-wrapper" class="data" style="overflow-y:scroll;">

        <div id="responsive-survey" class="container" style="width:100%;">

            <div style="width:100%; margin:0 auto;">

                <div class="title-section" style="width:80%; margin: 0 auto;">
                    <div class="survey-info">
                        <h1><img id="survey-icon" src="" /><span id="survey-topic" class="ellipsis-title"></span></h1>
                        <p class="align-left">Showing data from <span id="survey-start-date"></span> - <span id="survey-end-date"></span></p>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="tabs tabs--styled">
                    <ul class="tabs__list">
                        <li class="tabs__list__item">
                            <a class="tabs__link" href="#personnel">Personnel</a>
                        </li>
                        <li class="tabs__list__item">
                            <a class="tabs__link" href="#department">Department</a>
                        </li>
                    </ul>
                    <div class="tabs__panels">
                        <!-- PERSONNEL TAB -->
                        <div class="tabs__panels__item overview" id="personnel">
                            <div id="personnel-table-section " class="responders-report">
                                <div class="filter-section">                  
                                    <%--<label class="select-label">Show</label>--%>
                                    <%--<div class="mdl-selectfield" style="width:250px;float:left;margin-right: 20px; height:40px;">
                                        <select name="department-filter" id="department-filter">
	                                        <option selected="selected" value="">All</option>
	                                        <option value="Management">Management</option>
                                        </select>
                                    </div>    --%>
                                    <div class="input-group" style="position:relative;">
                                            <i class="fa fa-search" style="left:15px;"></i>
                                            <input id="responder_name" class="search-input" type="text" placeholder="Search Personnel" value="" style="margin:0px;"/>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="card animated fadeInUp no-padding">
                                    <div class="card-header" style="padding:10px 20px 0px 20px;">
                                        <div class="grid-question-number" style="width:17%;">
                                            <div style="color:#ddd;">
                                                <div class="number" style="font-size:24px;"></div>
                                                <div class="code"></div>
                                            </div>
                                        </div>
                                        <div class="grid-question" style="width:60%;">
                                            <label style="color:#ddd; font-size:16px; line-height:16px;">Question</label>
                                            <p class="question"></p>
                                            <label style="color:#ddd; font-size:16px; line-height:16px;">Answer</label>
                                            <p class="answer"></p>
                                        </div>
                                        <div class="grid-type align-center">
                                            <label style="color:#ddd; font-size:16px; line-height:16px;">Type</label>
                                            <div class="type-image" style="width:25px; height:25px; margin:0 auto;"></div>
                                        </div>
                                    </div>
                                    <div class="table-content" style="clear:both;">
                                        <p class="error_msg">There are no data.</p>
                                        <table id="personnel-table" class="responder-table">
                                            <thead>
                                                <th data-dynatable-column="no" style="width:10%">No</th>
                                                <th data-dynatable-column="profile_img" style="width:10%"></th>
                                                <th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width:50%">Name</th>
                                                <th data-dynatable-column="department-filter" data-dynatable-sorts="department-filter" style="width:30%">Department</th>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <!-- DEPARTMENT TAB -->
                        <div class="tabs__panels__item overview" id="department">
                            <div id="department-table-section " class="responders-report">
                                <div class="filter-section">                      
                                    <div class="input-group" style="position:relative;">
                                            <i class="fa fa-search" style="left:15px;"></i>
                                            <input id="department_name" class="search-input" type="text" placeholder="Search Department" value="" style="margin:0px;"/>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="card animated fadeInUp no-padding">
                                    <div class="card-header" style="padding:10px 20px 0px 20px;">
                                        <div class="grid-question-number" style="width:17%;">
                                            <div style="color:#ddd;">
                                                <div class="number" style="font-size:24px;"></div>
                                                <div class="code"></div>
                                            </div>
                                        </div>
                                        <div class="grid-question" style="width:60%;">
                                            <label style="color:#ddd; font-size:16px; line-height:16px;">Question</label>
                                            <p class="question"></p>
                                            <label style="color:#ddd; font-size:16px; line-height:16px;">Answer</label>
                                            <p class="answer"></p>
                                        </div>
                                        <div class="grid-type align-center">
                                            <label style="color:#ddd; font-size:16px; line-height:16px;">Type</label>
                                            <div class="type-image" style="width:25px; height:25px; margin:0 auto;"></div>
                                        </div>
                                    </div>
                                    <div class="table-content " style="clear:both;">
                                        <p class="error_msg">There are no data.</p>
                                        <table id="department-table" class="responder-table">
                                            <thead>
                                                <th data-dynatable-column="no" style="width:10%">No</th>
                                                <th data-dynatable-column="department_name" data-dynatable-sorts="department_name" style="width:60%">Department</th>
                                                <th data-dynatable-column="count" data-dynatable-sorts="count" style="width:30%">Count</th>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="/js/vendor/moment-with-locales.js"></script>
    <script src="/Js/Chart.js"></script>
    <script src="/Js/Survey/SingleQuestionAnalytic.js"></script>
</asp:Content>


