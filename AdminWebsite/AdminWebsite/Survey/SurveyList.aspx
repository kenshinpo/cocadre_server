﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SurveyList.aspx.cs" Inherits="AdminWebsite.Survey.SurveyList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfSurveyId" runat="server" />
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfSurveyName" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/Survey/Edit" class="mfb-component__button--main" data-mfb-label="Add Survey">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Delete Survey -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Survey
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgActionSurvey" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionSurveyTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbAction_Click" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Delete Survey -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/" style="color: #000;">Responsive <span>Survey</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="surveyCountLiteral" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Responsive Surveys</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Survey/Category">Category</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Survey</label>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Type the survey's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" runat="server" OnClick="ibFilterSurvey_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                    <div class="pad">
                        <label>By Category</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterCategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged" AppendDataBoundItems="true" onchange="ShowProgressBar('Filtering');">
                                <asp:ListItem Text="All" Value="" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvSurvey" runat="server" OnItemCommand="lvSurvey_ItemCommand" OnSorting="lvSurvey_Sorting" OnItemDataBound="lvSurvey_ItemDataBound" OnItemCreated="lvSurvey_ItemCreated" OnItemEditing="lvSurvey_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 60px; padding: 15px 20px;">
                                            <!--Icon-->
                                        </th>
                                        <th style="width: 20%; padding: 15px 5px;">
                                            <!--Title-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortSurveyTitle" ID="lbSortSurveyTitle" OnClientClick="ShowProgressBar('Filtering');" Text="Title" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 15%; padding: 15px 5px;">
                                            <!--Category-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortSurveyCategory" ID="lbSortSurveyCategory" OnClientClick="ShowProgressBar('Filtering');" Text="Category" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 60px; padding: 15px 5px">
                                            <!--Number Of Questions-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortTotalNumberOfQuestions" ID="lbSortTotalNumberOfQuestions" OnClientClick="ShowProgressBar('Filtering');" Text="Questions" Style="min-width: 60px;" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px">
                                            <!--Progress-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortSurveyProgress" ID="lbSortSurveyProgress" OnClientClick="ShowProgressBar('Filtering');" Text="Progress" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px">
                                            <!--Status-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortSurveyStatus" ID="lbSortSurveyStatus" OnClientClick="ShowProgressBar('Filtering');" Text="Status" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px">
                                            <!--Participants-->
                                            <asp:Label runat="server" Style="min-width: 80px;">Participants</asp:Label>
                                        </th>
                                        <th class="no-sort" style="width: 40px; padding: 15px 5px">
                                            <!--Analytic Icon-->
                                        </th>
                                        <th class="no-sort" style="width: auto; padding: 15px 15px;">
                                            <!--Action Menu-->
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="padding: 15px 20px; text-align: center;">
                                    <!--Icon-->
                                    <asp:HiddenField runat="server" ID="SurveyIdHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "TopicId") %>' />
                                    <div class="constrained" style="display: inline-block;">
                                        <asp:LinkButton ID="lbIcon" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")  %>' Style="color: #999;">
                                            <asp:Image ID="imgSurvey" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "IconUrl") %>' runat="server" />
                                        </asp:LinkButton>
                                    </div>
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Title-->
                                    <asp:LinkButton ID="lbSurveyName" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")  %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Category-->
                                    <asp:LinkButton ID="lbCategory" Text='<%# DataBinder.Eval(Container.DataItem, "RSCategory.Title") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")  %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Number Of Questions-->
                                    <asp:LinkButton ID="lbQuestionCount" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfCards") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")  %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Progress-->
                                    <asp:LinkButton ID="lbProgress" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")  %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Status-->
                                    <asp:LinkButton ID="lbStatus" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")  %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Participants-->
                                    <asp:LinkButton ID="lbParticipants" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId") %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px; text-align: center;">
                                    <!--Analytic Icon-->
                                    <asp:LinkButton ID="lbReport" runat="server" CommandName="Analytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId") %>'>
                                        <asp:Image ID="imgReport" runat="server" ImageUrl="~/Img/icon_report.png" Width="35px" />
                                    </asp:LinkButton>
                                </td>
                                <td style="padding: 15px 15px;">
                                    <!--Action Menu-->
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Edit survey" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId") %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbActivate" ClientIDMode="AutoID" runat="server" CommandName="Activate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "," + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")%>' Text="Activate survey" Visible='<%# ((DataBinder.Eval(Container.DataItem, "Status").ToString() == "1") || (DataBinder.Eval(Container.DataItem, "Status").ToString() == "3")) %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbHide" ClientIDMode="AutoID" runat="server" CommandName="Hide" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "," + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")%>' Text="Hide survey" Visible='<%# DataBinder.Eval(Container.DataItem, "Status").ToString() == "2" %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="DeleteSurvey" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "," + DataBinder.Eval(Container.DataItem, "RSCategory.CategoryId")%>' Text="Delete survey" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
