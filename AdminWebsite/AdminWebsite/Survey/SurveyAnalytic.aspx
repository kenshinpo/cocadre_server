﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SurveyAnalytic.aspx.cs" Inherits="AdminWebsite.Survey.SurveyAnalytic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
	<style>
		.error_msg {
			text-align: center;
			display: none;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

	<!-- App Bar -->
	<div id="st-trigger-effects" class="appbar">
		<a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
		<div class="appbar__title"><a href="/Survey/List" style="color: #000;">Responsive <span>survey</span></a></div>
		<div class="appbar__meta"><a href="" id="survey-root-link"></a></div>
		<div class="appbar__meta"><a href="" id="simple-survey-link">Overview</a></div>
		<div class="appbar__meta"><a href="" id="responder_report_link" class="hidden">Responders report</a></div>
		<div class="appbar__meta breadcrumb-participant"></div>
		<div class="appbar__action">
			<a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
		</div>
	</div>
	<!-- /App Bar -->

	<div id="survey-wrapper" class="data" style="overflow-y: scroll;">

		<div id="responsive-survey" class="container" style="width: 100%;">

			<div style="width: 100%; margin: 0 auto;">

				<div class="title-section" style="width: 80%; margin: 0 auto;">
					<div class="survey-info">
						<h1>
							<img id="survey-icon" src="" /><span id="survey-topic" class="ellipsis-title"></span></h1>
						<p class="align-left">Showing data from <span id="survey-start-date"></span>- <span id="survey-end-date"></span></p>
					</div>
					<div class="user-info hidden">
						<ul class="list-group">
							<li class="list-group-item">
								<span id="user-name" class="list-value"></span>
								Name:
							</li>
							<li class="list-group-item">
								<span id="user-department" class="list-value"></span>
								Department:
							</li>
							<li class="list-group-item">
								<span id="user-status" class="list-value"></span>
								Status:
							</li>
							<li class="list-group-item">
								<span id="user-last-update" class="list-value"></span>
								Last Update:
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>

				<!-- SELECTED SURVEY RESULT FROM SEE MORE -->
				<div id="single-survey-table-wrapper" class="card animated fadeInUp no-padding">
				</div>

				<!-- START OF OVERVIEW PAGE -->
				<div id="overview-tab" class="tabs tabs--styled">
					<ul class="tabs__list">
						<li class="tabs__list__item">
							<a class="tabs__link" href="#overview">Overview</a>
						</li>
						<li class="tabs__list__item">
							<a id="tab_respondersreport" class="tabs__link" href="#responders-report">Responders report</a>
						</li>
						<li class="tabs__list__item">
							<a id="tab_feedback" class="tabs__link" href="#responders-feedback">Feedback</a>
						</li>
					</ul>
					<div class="tabs__panels">
						<!-- OVERVIEW TAB -->
						<div class="tabs__panels__item overview" id="overview">
							<div class="card card-7-col animated fadeInUp">
								<div class="card-header">
									<h1 class="title">Responders report</h1>
								</div>
								<div class="chart-section" style="width: 100%; height: 336px; position: relative;">
									<div class="legends"></div>
									<canvas id="responderChart" width="800" height="335"></canvas>
								</div>
								<div class="chart-footer">
									<label class="info-label">Starting from <span id="start_date" class="start_date"></span></label>
									<div class="chart-info">
										<div class="chart-border"></div>
										<label id="response_completed" class="response_number">0</label>
										<small>responses</small>
										<p>Completed since start</p>
									</div>
									<div class="chart-info">
										<div class="chart-border"></div>
										<label id="response_bouncerate" class="response_number">0</label>
										<small>responses</small>
										<p>Bounce rate</p>
									</div>
									<div class="chart-info">
										<label id="response_completionrate" class="response_number">0</label>
										<small>Survey completion rate</small>
										<p>Since beginning</p>
									</div>
									<div class="chart-total-responder">
										<div class="border-box">
											<small>Out of</small>
											<label id="response_totalresponder" class="response_number">0</label>
											<small class="highlight">Responders</small>
										</div>
									</div>
								</div>
							</div>

							<!-- SUMMARY -->
							<div class="card card-3-col animated fadeInUp">
								<div class="card-header">
									<h1 class="title">Summary</h1>
								</div>
								<div class="small-card-content">
									<ul class="list-group">
										<li class="list-group-item">
											<span id="total-pages" class="list-value"></span>
											Total Pages:
										</li>
										<li class="list-group-item">
											<span id="total-question" class="list-value"></span>
											Total Questions:
										</li>
										<%--<li class="list-group-item">
											<span id="total-days" class="list-value"></span>
											Days Running:
										</li>--%>
										<li class="list-group-item">
											<span id="average-completion" class="list-value"></span>
											Average Completion<br />Time Per Person:
										</li>
									</ul>
								</div>
							</div>

							<!-- PERSONNEL COMPLETED -->
							<div class="card card-3-col animated fadeInUp">
								<div class="card-header">
									<h1 class="title">Personnel completed</h1>
								</div>
								<div class="small-card-content">
									<ul class="list-group">
										<li class="list-group-item">
											<span id="completed-value" class="list-value"></span>
											<span id="completed-percentage" class="list-percentage color1"></span>
											Completed
											<div class="progress">
												<div id="completed-bar" class="progress-bar color1"></div>
											</div>
										</li>
										<li class="list-group-item">
											<span id="incomplete-value" class="list-value"></span>
											<span id="incomplete-percentage" class="list-percentage color2"></span>
											Incomplete
											<div class="progress">
												<div id="incompleted-bar" class="progress-bar color2"></div>
											</div>
										</li>
										<li class="list-group-item">
											<span id="absent-value" class="list-value"></span>
											<span id="absent-percentage" class="list-percentage color3"></span>
											Absent
											<div class="progress">
												<div id="absent-bar" class="progress-bar color3"></div>
											</div>
										</li>
									</ul>
								</div>
							</div>

							<!-- ROW FOR BUTTON -->
							<div class="full-row align-center">
								<button id="generate" class="btn-default animated fadeIn">Generate Live Data</button>
							</div>

							<!-- LIVE DATA SECTION -->
							<div id="live-data-section"></div>

							<!-- ANONYMITY -->
							<div id="anonymity" style="text-align: center; display: none; margin-top: 30px;">
								<div style="font-size: 1.2em;">
									<span id="anonymity_text1" style="padding: 10px 50px; border: solid 1px rgba(63, 209, 182, 1); background-color: rgba(63, 209, 182, 1); border-radius: 20px; color: #FFF; font-size: 1.2em;">Pluse is on Anonymous</span>
								</div>
								<label id="anonymity_text2" style="font-size: 1.2em; padding: 30px">Analytics not available yet as there is less than 999 personnel's participation</label>
							</div>

						</div>

						<!-- RESPONDERS REPORT TAB -->
						<div class="tabs__panels__item responders-report" id="responders-report">

							<!-- USER REPORT -->
							<div id="user-report-section"></div>

							<!-- RESPONDER TABLE -->
							<div id="responder-table-section">
								<div class="filter-section">
									<label class="select-label">Show</label>
									<div class="mdl-selectfield" style="width: 250px; float: left; margin-right: 20px; height: 40px;">
										<%--<label for="status">Show</label>--%>
										<select name="status" id="status">
											<option selected="selected" value="">All</option>
											<option value="Completed">Completed</option>
											<option value="Incomplete">Incomplete</option>
										</select>
									</div>
									<div class="input-group" style="position: relative;">
										<i class="fa fa-search"></i>
										<input id="hidden_name" class="search-input" type="text" placeholder="Search Responders" value="" />
									</div>
								</div>

								<div class="clearfix"></div>

								<div class="card animated fadeInUp ">

									<!-- LEGENDS -->
									<div class="legends">
										<div class="legend-item">
											<div class="legend-icon">
												<div class="legend-color1"></div>
											</div>
											<label>Completed</label>
										</div>
										<div class="legend-item">
											<div class="legend-icon">
												<div class="legend-color2"></div>
											</div>
											<label>Incomplete</label>
										</div>
										<div class="legend-item">
											<div class="legend-icon">
												<div class="legend-color3"></div>
											</div>
											<label>Absent</label>
										</div>
									</div>

									<div class="card-header">
										<h1 class="title">Personnel completion</h1>
									</div>

									<div class="progress stacked">
										<div id="progress-bar-completed" class="progress-bar color1">
											<span id="personnel-completed-value" class="value">12</span>
											<span id="personnel-completed-percentage" class="percentage color1">12%</span>
										</div>
										<div id="progress-bar-incompleted" class="progress-bar color2">
											<span id="personnel-incompleted-value" class="value">12</span>
											<span id="personnel-incompleted-percentage" class="percentage color2">12%</span>
										</div>
										<div id="progress-bar-absent" class="progress-bar color3">
											<span id="personnel-absent-value" class="value">12</span>
											<span id="personnel-absent-percentage" class="percentage color3">12%</span>
										</div>
									</div>
								</div>

								<div id="personnel_list" class="card animated fadeInUp no-padding">
									<div class="card-header" style="padding: 20px 20px 0px 20px;">
										<h1 class="title">Personnel list <span id="list-status">- All</span></h1>
									</div>
									<div class="table-content">
										<p class="error_msg">There are no data at this moment.</p>
										<table id="responder-table">
											<thead>
												<th data-dynatable-column="no" style="width: 10%">No</th>
												<th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width: 30%">Name</th>
												<th class="hidden" data-dynatable-column="hidden_name">Hidden name</th>
												<th data-dynatable-column="last_updated" data-dynatable-sorts="last_updated" style="width: 20%">Last update</th>
												<th data-dynatable-column="department" data-dynatable-sorts="department" style="width: 20%">Department</th>
												<th data-dynatable-column="status" style="width: 20%">Status</th>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<!-- FEEDBACK TAB -->
						<div class="tabs__panels__item responders-report" id="responders-feedback">
							<div id="feedback_list" class="card animated fadeInUp no-padding">
								<div class="card-header" style="padding: 20px 20px 0px 20px;">
									<h1 class="title">Feedback</h1>
								</div>
								<p class="error_msg">There are no data at this moment.</p>
								<div class="table-content">
									<table id="feedback-table" class="responder-table">
										<thead>
											<th data-dynatable-column="date" style="width: 20%">Date</th>
											<th data-dynatable-column="feedback" style="width: 80%">Feedback</th>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
	<script src="/js/vendor/moment-with-locales.js"></script>
	<script src="/Js/Chart.js"></script>
	<script src="/Js/Survey/SurveyAnalytic.js"></script>
</asp:Content>


