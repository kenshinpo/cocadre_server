﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SurveyEdit_bak.aspx.cs" Inherits="AdminWebsite.Survey.SurveyEdit_Bak" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <style>
        .inline-checkbox label {
            display:inline-block;
        }
        .radiobuttonlist-inline td {
            padding:5px;
        }
        .radiobuttonlist-inline label {
            display:inline;
        }
        .inline-list label, 
        .inline-list select {
            display:inline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <asp:LinkButton ID="lbEmpty" runat="server"></asp:LinkButton>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Survey Creation</div>
        <div class="appbar__meta">Survey Id: RS123123</div>
        <div class="appbar__action"></div>
    </div>
    <!-- / App Bar -->

        <!-- Reveal: Add Topic Icon -->
    <asp:UpdatePanel ID="upAddIcon" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="popup_addsurveyicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Topic Icon</h1>
                <div class="topicicons">
                    <p>Select from list</p>
                    <asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand">
                        <HeaderTemplate>
                            <div class="topicicons__list">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <label class="topicicons__icon">
                                <asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
                                    <asp:Image ID="imgChooseIcon" runat="server" />
                                </asp:LinkButton>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbAddCancel_Click">Cancel</asp:LinkButton>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ajaxToolkit:ModalPopupExtender ID="mpeAddIcon" runat="server"
        TargetControlID="lbEmpty"
        PopupControlID="popup_addsurveyicon"
        BackgroundCssClass="mfp-bg"
        DropShadow="false">
    </ajaxToolkit:ModalPopupExtender>
    <!-- / Reveal: Add Topic Icon -->

    <div class="data">
        <aside class="data__sidebar filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <%--<a class="data__sidebar__link data__sidebar__link--active">Survey</a>--%>
                    <a class="data__sidebar__link" href="/Survey/List">Survey</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Category</a>
                    <%--<a class="data__sidebar__link" href="/Survey/Category">Category</a>--%>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Survey/Analytics">Analytics</a>
                </li>
            </ul>
        </aside>

        <div class="data__content" onscroll="sticky_div();">

            <div class="container">
                <asp:LinkButton ID="lbPreview" runat="server" CssClass="btn secondary">Live Survey Preview</asp:LinkButton>
                <div class="card survey-header">
                    <!--
                    3 columns 
                    ---------
                    1 col  icon
                    1 col  title, introduction, closing words
                    1 col  action
                    -->
                    <div class="add-topic__icon">
                        <asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
                            <asp:Image ID="imgTopic" ImageUrl="~/img/icon-topicicon-default.png" runat="server" />
                            <label><small>Choose topic icon</small></label>
                        </asp:LinkButton>
                    </div>

                    <div class="add-topic__info">
                        <div class="" style="float: right">
                            <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn" OnClick="lbCreate_Click">Create</asp:LinkButton>
                            <asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/Survey/SurveyList" Text="Cancel" CssClass="btn secondary" />
                        </div>

                        <div class="add-topic__info__details">
                            <div id="" class="tabs tabs--styled">
                                <ul class="tabs__list">
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#survey-info">Title</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#survey-participants">Participants</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#survey-settings">Settings</a>
                                    </li>
                                </ul>
                                <div class="tabs__panels">
                                    <div class="tabs__panels__item add-topic__info__details--basicinfo" id="survey-info">
                                        <div class="column--input">
                                            <div class="form__row">
                                                Title: 
                                                <asp:TextBox ID="tbSurveyTitle" placeholder="Survey Title" MaxLength="120" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" />
                                                <asp:Label ID="lbltbSurveyTitleCount" runat="server" CssClass="lettercount" />
                                            </div>
                                            <div class="form__row">
                                                Introduction: 
                                                <asp:TextBox ID="tbSurveyIntroduction" runat="server" MaxLength="120" placeholder="Introduction" TextMode="MultiLine"  />
                                                <asp:Label ID="tbSurveyIntroductionCount" runat="server" CssClass="lettercount" />
                                            </div>
                                            <div class="form__row">
                                                Closing words: 
                                                <asp:TextBox ID="tbSurveyClosing" runat="server" MaxLength="120" placeholder="Closing words" TextMode="MultiLine"  />
                                                <asp:Label ID="tbSurveyClosingCount" runat="server" CssClass="lettercount" />
                                            </div>

                                        </div>
                                        <div class="column--choice">
                                            <div class="form__row">
                                                <label>Category</label>
                                                <div>
                                                    <ajaxToolkit:ComboBox ID="cbCategory" runat="server" AutoCompleteMode="Suggest" CssClass="combos" MaxLength="30" AppendDataBoundItems="true">
                                                        <asp:ListItem Text="Type or select category" Selected="True" />
                                                    </ajaxToolkit:ComboBox>
                                                    <asp:HiddenField ID="hfCategoryId" runat="server" />
                                                </div>
                                            </div>
                                            <div class="form__row">
                                                <label>Status</label>
                                                <div>
                                                    <ajaxToolkit:ComboBox ID="cbStatus" runat="server" AutoCompleteMode="Suggest" DropDownStyle="DropDownList" CssClass="combos" MaxLength="30" AppendDataBoundItems="true">
                                                        <asp:ListItem Text="Select Status" Selected="True" />
                                                        <asp:ListItem Text="Unlisted" Value="1" />
                                                        <asp:ListItem Text="Active" Value="2" />
                                                        <asp:ListItem Text="Hidden" Value="3" />
                                                        <asp:ListItem Text="Deleted" Value="-1" />
                                                    </ajaxToolkit:ComboBox>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                                </div>
                                            </div>
                                    
                                        </div>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--basicinfo" id="survey-participants">
                                        <asp:CheckBox Text="Everyone" runat="server" CssClass="inline-checkbox" id="EveryoneCheckBox" />
                                        <ul>
                                            <li><asp:CheckBox Text="Selected departments" runat="server" CssClass="inline-checkbox participants" /></li>
                                            <li><asp:CheckBox Text="Selected personnel" runat="server" CssClass="inline-checkbox participants" /></li>
                                        </ul>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--settings" id="survey-settings">
                                        <div class="form__row">
                                            
                                            <ul>
                                                <li>
                                                    <asp:CheckBox Text="Random question order for entire quiz" ID="RandomQuestionOrderCheckBox" runat="server" CssClass="inline-checkbox" />
                                                </li>
                                                <li>
                                                    <asp:CheckBox Text="Allow return to previous question" ID="AllowPreviousQuestionCheckBox" runat="server" CssClass="inline-checkbox" />
                                                </li>
                                                <li>
                                                    <asp:CheckBox Text="Show progress bar" ID="ShowProgressBarCheckBox" runat="server" CssClass="inline-checkbox" />
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            
            </div>

            <div class="container">
                
            </div>

            <div class="container">

                <asp:UpdatePanel id="upEditQuestion" runat="server">
                            <ContentTemplate>

                <asp:FormView runat="server" ID="fvQuestion" Visible="false" DefaultMode="Insert" OnItemCommand="fvQuestion_ItemCommand" OnItemInserting="fvQuestion_ItemInserting" OnModeChanging="fvQuestion_ModeChanging">
                    <InsertItemTemplate>

                        <div class="card add-question">

                            <div class="add-question__questionnumber">
                                <div class="number">
                                    <asp:Literal ID="QuestionNoLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuestionNo") %>' />
                                </div>
                                <label>
                                    Code<br />
                                    <asp:Literal ID="QuestionIdLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "QuestionId") %>' />
                                </label>
                            </div>

                            <div class="add-question__info">
                                <div class="add-question__info__header">
                                    <div class="add-question__info__header__action">
                                        <asp:LinkButton ID="lbNewQuestionCancel" runat="server" CssClass="btn secondary" Text="Cancel" CommandName="Cancel" OnClick="lbNewQuestionCancel_Click" />
                                        <asp:LinkButton ID="lbNewQuestionSave" runat="server" CssClass="btn" OnClick="lbNewQuestionSave_Click" CommandName="Insert" Text="Create" OnClientClick="ShowProgressBar();" />
                                    </div>
                                    <div class="add-question__info__header__title">Create</div>
                                </div>

                                <div class="add-question__info__setup__questiontype">

                                    <div class="form__row">
                                        <asp:RadioButtonList runat="server" id="QuestionTypeRadioButtonList" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="radiobuttonlist-inline" OnSelectedIndexChanged="QuestionTypeRadioButtonList_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="Select one" Value="1" />
                                            <asp:ListItem Text="Multi choice" Value="2" />
                                            <asp:ListItem Text="Text" Value="3" />
                                            <asp:ListItem Text="Number range" Value="4" />
                                            <asp:ListItem Text="Drop list" Value="5" />
                                            <asp:ListItem Text="Instructions" Value="6" />
                                        </asp:RadioButtonList>
                                    </div>

                                    <div class="form__row">
                                        <div>
                                            <div style="width:50px;">
                                                <asp:LinkButton ID="lbAddQuestionIcon" runat="server" OnClick="lbAddQuestionIcon_Click">
                                                    <asp:Image ID="Image1" ImageUrl="~/img/icon-topicicon-default-50px.png" runat="server" />
                                                </asp:LinkButton>
                                            </div>
                                                
                                            <div style="width:100%;">
                                                <asp:TextBox ID="QuestionTextBox" placeholder="Question text" TextMode="MultiLine" runat="server" 
                                                    onkeydown="return (event.keyCode!=13);" 
                                                    onkeyup="EditQuestionPreview('QuestionTextBox');"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "QuestionText") %>' />
                                            </div>
                                        </div>
                                        &nbsp;
                                        <asp:Panel runat="server" ID="MultipleAnswerPanel" Visible="true">
                                            <div>
                                                <asp:ListView runat="server" ID="MultipleAnswerListView" OnItemCommand="MultipleAnswerListView_ItemCommand">
                                                    <LayoutTemplate>
                                                        <div runat="server" id="ItemPlaceHolder"></div>
                                                        <div>
                                                            <asp:Button Text="Add Answer" CommandName="AddAnswer" runat="server" ID="AddAnswerButton" />
                                                        </div>
                                                    </LayoutTemplate>
                                                    <ItemTemplate>
                                                        <div class="form__row form__row--answer">
                                                            <div style="width:50px;">
                                                            <asp:LinkButton ID="lbAddQuestionIcon" runat="server" OnClick="lbAddQuestionIcon_Click">
                                                                    <asp:Image ID="Image1" ImageUrl="~/img/icon-topicicon-default-50px.png" runat="server" />
                                                                </asp:LinkButton>
                                                            </div>
	                                                        <asp:TextBox ID="tbEditAnswer1" placeholder="Answer" TextMode="MultiLine" runat="server" onkeydown="return (event.keyCode!=13);" Text='<%# DataBinder.Eval(Container.DataItem, "Text") %>' />
	                                                        <asp:Label ID="lblEditAnswer1Count" runat="server" CssClass="lettercount" />
                                                            <asp:LinkButton Text="X" CommandName="DeleteAnswer" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id") %>' runat="server" />
                                                            <asp:HiddenField runat="server" ID="QuestionAnswernid" Value='<%# DataBinder.Eval(Container.DataItem, "Id") %>' />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="NumberRangePanel" Visible="false">
                                            <div>
                                                <asp:Literal Text="Number range" runat="server" /><br />
                                                <asp:Label Text="Minimum value" runat="server" />
                                                <asp:TextBox runat="server" id="MinimumValueTextBox" Text="" TextMode="Number" />

                                                <asp:Label Text="Maximum value" runat="server" />
                                                <asp:TextBox runat="server" id="MaximumValueTextBox" Text="" TextMode="Number" />

                                            </div>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="InstructionPanel" Visible="false">
                                            <div>
                                                display panel for instruction panel
                                            </div>
                                        </asp:Panel>
                                        &nbsp;
                                    </div>
                                </div>

                                <div class="add-question__info__setup__preview">
                                    <!-- Add graphical preview here -->
		                            <asp:Panel ID="plEditPreview" runat="server" CssClass="question-preview">
			                            <div class="question-preview__header">
				                            <div class="question-preview__header__score">
<%--					                            <div class="Media">
						                            <img class="Media-figure" src="/img/image-01.jpg" alt="" />
						                            <div class="Preview-body">
							                            <span>User 1</span>
							                            <div class="score">Score</div>
						                            </div>
					                            </div>--%>
				                            </div>
				                            <div class="question-preview__header__timer">Time Left <span>10</span></div>
				                            <div class="question-preview__header__score question-preview__header__score--user02">
					                            <%--<div class="Media">
						                            <img class="Media-figure" src="/img/image-01.jpg" alt="" />
						                            <div class="Preview-body">
							                            <span>User 2</span>
							                            <div class="score">Score</div>
						                            </div>
					                            </div>--%>
				                            </div>
			                            </div>
			                            <div class="question-preview__question">
				                            <p>
					                            <div class="question-preview__content">
						                            <asp:Label ID="lblEditPreviewQuestion" runat="server" />
					                            </div>
				                            </p>
			                            </div>
			                            <asp:Panel ID="plEditPreviewImg" runat="server" CssClass="question-preview__image" Visible="false">
				                            <asp:HiddenField ID="hfOriImageMD5" runat="server" />
				                            <asp:Image ID="imgEditPreviewImg" runat="server" />
			                            </asp:Panel>

                                        <asp:Repeater runat="server" id="PreviewAnswersRepeater">
                                            <HeaderTemplate>
                                                <div class="question-preview__answers">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="question-preview__answer">
					                                <div style="word-wrap: break-word;">
						                                <asp:Label ID="lblEditPreviewQuestionAnswer" runat="server" Style="word-wrap: break-word; white-space: pre;" Text='<%# DataBinder.Eval(Container.DataItem, "Text") %>' />
					                                </div>
				                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>

		                            </asp:Panel>
		                            <!-- / Question Preview Format -->
                                </div>

                                <div class="add-question__info__setup__choice">
                                    <div class="form__row">
                                        <asp:Label Text="Status" runat="server" />
                                        <asp:DropDownList runat="server" id="QuestionStatus">
                                            <asp:ListItem Text="Active" />
                                            <asp:ListItem Text="Unlist" />
                                        </asp:DropDownList>
                                    </div>
                        
                                    <div class="form__row inline-list">
                                        <asp:Label Text="Behaviour" runat="server" /><br />
                                        <asp:CheckBox Text="Allow question to be skipped" runat="server" id="AllowQuestionToBeSkippedCheckBox" TextAlign="Right" CssClass="inline-checkbox" /> <br />
                                        <asp:CheckBox Text="Tick &quot;other&quot; and type your answer" runat="server" id="AllowOthersCheckBox" CssClass="inline-checkbox" /> <br />
                                        <asp:CheckBox Text="Randomize option order" runat="server" id="RandomizeOptionOrderCheckBox" CssClass="inline-checkbox" /> <br />

                                        <asp:Label Text="Time Limit (sec)" runat="server" />
                                        <asp:TextBox runat="server" id="TimeLimitTextBox" placeholder="Time Limit (sec)" />
                                    </div>

                                    <div class="form__row">
                                        <asp:Label Text="Display" runat="server" /><br />
                                        <asp:Label Text="Answers" runat="server" />
                                        <asp:DropDownList runat="server">
                                            <asp:ListItem Text="Auto Columns" />
                                            <asp:ListItem Text="1" />
                                            <asp:ListItem Text="2" />
                                            <asp:ListItem Text="3" />
                                            <asp:ListItem Text="4" />
                                            <asp:ListItem Text="5" />
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form__row inline-list">
                                        <asp:CheckBox Text="Page Break" runat="server" id="PageBreak" />
                                    </div>
                                </div>

                            </div>
                
                            <div class="add-question__info">
                                <div style="float:left; width:400px;">
                                    <div class="inline-list">
                                        <asp:DropDownList runat="server">
                                            <asp:ListItem Text="When answer is" />
                                            <asp:ListItem Text="text2" />
                                        </asp:DropDownList>
                                        <asp:DropDownList runat="server">
                                                    <asp:ListItem Text="Please select answer" />
                                                    <asp:ListItem Text="text2" />
                                                </asp:DropDownList>
                                        <asp:LinkButton Text="X" runat="server"></asp:LinkButton>
                                    </div>
                                    <div class="inline-list">
                                        <asp:DropDownList runat="server">
                                                    <asp:ListItem Text="Skip to question" />
                                                    <asp:ListItem Text="text2" />
                                                </asp:DropDownList>
                                        <asp:DropDownList runat="server">
                                                    <asp:ListItem Text="Please select question" />
                                                    <asp:ListItem Text="text2" />
                                                </asp:DropDownList>
                                    </div>
                                </div>

                                <div style="float:right;">
                                    <button>Delete question</button>
                                </div>
                            </div>
                
                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <p>
                            write out the formviews edit item template here
                        </p>
                    </EditItemTemplate>
                </asp:FormView>
<asp:Panel ID="popup_addsurveyquestionicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
	<h1 class="popup__title">Choose Question Icon</h1>
	<div class="topicicons">
		<p>Select from list</p>
		<asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand">
			<HeaderTemplate>
				<div class="topicicons__list">
			</HeaderTemplate>
			<ItemTemplate>
				<label class="topicicons__icon">
					<asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
						<asp:Image ID="imgChooseIcon" runat="server" />
					</asp:LinkButton>
				</label>
			</ItemTemplate>
			<FooterTemplate>
				</div>
			</FooterTemplate>
		</asp:Repeater>
	</div>
	<div class="popup__action">
		<asp:LinkButton ID="LinkButton1" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbAddCancel_Click">Cancel</asp:LinkButton>
	</div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="mpeSurveyQuestion" runat="server"
	TargetControlID="lbEmpty2"
	PopupControlID="popup_addsurveyquestionicon"
	BackgroundCssClass="mfp-bg"
	DropShadow="false">
</ajaxToolkit:ModalPopupExtender>
                                <asp:LinkButton ID="lbEmpty2" runat="server" />
                                </ContentTemplate>
                        </asp:UpdatePanel>

                <asp:ListView runat="server" ID="lvQuestion" OnItemCommand="lvQuestion_ItemCommand">
                    <LayoutTemplate>
                        <table id="QuestionList" class="question-list-table">
                            <thead>
                                <tr>
                                    <td colspan="8">
                                        <div class="questions-bar__search">
                                            <asp:TextBox ID="tbSearchQuestion" runat="server" placeholder="Search question" CssClass="questions-bar__search__input" />
                                            <asp:LinkButton ID="lbSearch" runat="server" CssClass="questions-bar__search__button" OnClick="lbSearch_Click" OnClientClick="ShowProgressBar();"><i class="fa fa-search"></i></asp:LinkButton>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th colspan="4">
                                        <asp:LinkButton ID="lbAddQuestion" runat="server" CssClass="btn secondary" CommandName="AddQuestion">Add Question</asp:LinkButton>
                                    </th>
                                    <th colspan="4" style="text-align:right;">
                                        <span style="color:black">Status</span>&nbsp;
                                        <i class="fa fa-circle" style="color:#FECA02"></i> Unlisted&nbsp;
                                        <i class="fa fa-circle" style="color:#94CF00"></i> Active&nbsp;
                                        <i class="fa fa-circle" style="color:#9A9A9A"></i> Ended&nbsp;
                                    </th>
                                </tr>
                                <tr>
                                    <th class="no-sort"></th>
                                    <th>Page</th>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Logic</th>
                                    <th class="no-sort"></th>
                                </tr>
                            </thead>
                            <tbody class="question-sortable">
                                <tr id="ItemPlaceholder" runat="server"></tr>
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="sort-handler sortable">
                            <td>
                                <span ><i class="fa fa-arrows-v fa-lg"></i></span>
                                <span class=""><i class="fa fa-angle-up fa-lg"></i></span>
                                <span class=""><i class="fa fa-angle-down fa-lg"></i></span>
                            </td>
	                        <td>
                                <%--<span class="sort-handler"><i class="fa fa-arrows-v fa-2x"></i></span>--%>
                                <asp:Literal Text='<%# DataBinder.Eval(Container.DataItem, "PageNo") %>' runat="server" />
	                        </td>
	                        <td><asp:Literal Text='<%# DataBinder.Eval(Container.DataItem, "QuestionNo") %>' runat="server" /></td>
	                        <td>Name1</td>
	                        <td>Status1</td>
	                        <td>Type1</td>
	                        <td>Logic1</td>
	                        <th class="no-sort">
                                <span class=""><i class="fa fa-ellipsis-h fa-lg"></i></span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyItemTemplate>
                        <p>this is empty item template</p>
                    </EmptyItemTemplate>
                    <EmptyDataTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <td colspan="7">
                                        <div class="questions-bar__search">
                                            <asp:TextBox ID="tbSearchQuestion" runat="server" placeholder="Search question" CssClass="questions-bar__search__input" />
                                            <asp:LinkButton ID="lbSearch" runat="server" CssClass="questions-bar__search__button" OnClick="lbSearch_Click" OnClientClick="ShowProgressBar();"><i class="fa fa-search"></i></asp:LinkButton>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <th colspan="3">
                                        <asp:LinkButton ID="lbAddQuestion" runat="server" CssClass="btn secondary" CommandName="AddQuestion">Add Question</asp:LinkButton>
                                    </th>
                                    <th colspan="4">
                                        <div style="float:right;">
                                            <span style="color:black">Status</span>&nbsp;
                                            <i class="fa fa-circle" style="color:#FECA02"></i> Unlisted&nbsp;
                                            <i class="fa fa-circle" style="color:#94CF00"></i> Active&nbsp;
                                            <i class="fa fa-circle" style="color:#9A9A9A"></i> Ended&nbsp;
                                        </div>
                                    
                                    </th>
                                </tr>
                                <tr>
                                    <th>Page</th>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Type</th>
                                    <th>Logic</th>
                                    <th class="no-sort"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="7">No questions defined.</td>
                                </tr>
                            </tbody>
                        </table>
                    </EmptyDataTemplate>

                    <InsertItemTemplate>
                        <p>
                            Do your item insertion form here
                        </p>
                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <p>
                            Do you item edit form here
                        </p>
                    </EditItemTemplate>
                    
                </asp:ListView>
            </div>
            
        </div>
    </div>

    
    <script type="text/javascript">
        function EditQuestionPreview(id) {
            var srcCtrl, srcCtrlId, dstCtrl, dstCtrlId;

            if (id == "QuestionTextBox") {
                srcCtrlId = '<%= fvQuestion.FindControl("QuestionTextBox").ClientID %>';
                dstCtrlId = '<%= fvQuestion.FindControl("lblEditPreviewQuestion").ClientID %>';
                $("#" + dstCtrlId).text($("#" + srcCtrlId).val());
            }
        }

        (function ($) {
            "use strict";

            function EditQuestionTextCounter(textBoxId, labelId, maxCount) {
                var textbox = document.getElementById(textBoxId);
                var label = document.getElementById(labelId);
                var countLength = maxCount - textbox.value.length;
                if (countLength < 0) {
                    textbox.style.borderColor = "red";
                }
                else {
                    textbox.style.borderColor = "";
                }
                label.innerHTML = countLength;
            }

            function EditQuestionPreview(textBoxId, labelId) {
                var textBox = document.getElementById(textBoxId);
                var label = document.getElementById(labelId);
                label.innerHTML = textBox.value;
            }

    

            $(document).ready(function () {
                // Return a helper with preserved width of cells
                //var fixHelper = function (e, ui) {
                //    ui.children().each(function () {
                //        $(this).width($(this).width());
                //    });
                //    return ui;
                //};

                //var fixHelperModified = function (e, tr) {
                //    var $originals = tr.children();
                //    var $helper = tr.clone();
                //    $helper.children().each(function (index) {
                //        $(this).width($originals.eq(index).width())
                //    });
                //    return $helper;
                //};

                //$(".question-sortable").sortable({
                //    helper: fixHelperModified
                //}).disableSelection();

                $("#<%= EveryoneCheckBox.ClientID %>").click(function (e) {
                    $(".inline-checkbox.participants input[type=checkbox]").prop("checked", e.currentTarget.checked);
                });

                $(".question-sortable").sortable({
                    stop: function (event, ui) {
                        window.alert("do postback action");
                    }
                });

            });

        }(jQuery));
    </script>


</asp:Content>
