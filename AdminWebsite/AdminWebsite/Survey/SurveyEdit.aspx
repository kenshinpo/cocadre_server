﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SurveyEdit.aspx.cs" Inherits="AdminWebsite.Survey.SurveyEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="/Css/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <script type="text/javascript">
        $(function () {
            TabFunction();

            //$('#main_content_lbNewQuestionCancel').on('click', function () {
            //    $('#animateCard').removeClass();
            //    $('#animateCard').addClass('animated fadeOut');
            //});

            //$('#main_content_lbAdd').on('click', function () {

            //    $('#animateCard').removeClass();
            //    $('#animateCard').addClass('animated fadeInDown');

            //});
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            TabFunction();
        })

        /* For create / edit new survey */
        function partcipantsCheck(type) {
            var cbParticipantsEveryone = $get("<%=cbParticipantsEveryone.ClientID%>");
            var cbParticipantsDepartment = $get("<%=cbParticipantsDepartment.ClientID%>");
            var cbParticipantsPersonnel = $get("<%=cbParticipantsPersonnel.ClientID%>");
            if (type == 1) // Selected Everyone
            {
                if (cbParticipantsEveryone.checked) {
                    cbParticipantsDepartment.checked = false;
                    cbParticipantsPersonnel.checked = false;
                }
            }
            else if (type == 2) // Selected departments or personnel
            {
                if (cbParticipantsDepartment.checked || cbParticipantsPersonnel.checked) {
                    cbParticipantsEveryone.checked = false;
                }
            }
        }
        /* For create / edit new survey */

        /* For search personnel */
        var delayTimer;
        function RefreshUpdatePanel(event) {
            if (event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 91 || event.keyCode == 93 || event.keyCode == 144 || (event.keyCode > 15 && event.keyCode < 21) || (event.keyCode > 32 && event.keyCode < 41) || (event.keyCode > 111 && event.keyCode < 124)) {
                return false;
            }
            clearTimeout(delayTimer);
            delayTimer = setTimeout(function () {

                __doPostBack('<%= tbSearchKey.ClientID %>', '');
            }, 1000);

            var progress = $('#imgLoading');
            progress.fadeToggle();
        }

        $(document).on("keydown", function (e) {
            if (e.which === 8 && !$(e.target).is("input, textarea")) {
                e.preventDefault();
            }
        });

        function showLoading() {
            var progress = $('#imgLoading');

            progress.fadeToggle();
        }

        function SetFocus() {
            var textBox = document.getElementById('<%= tbSearchKey.ClientID %>');
            var elemLen = textBox.value.length;
            if (document.selection) {
                // Set focus
                // Use IE Ranges
                var oSel = document.selection.createRange();
                // Reset position to 0 & then set at end
                oSel.moveStart('character', -elemLen);
                oSel.moveStart('character', elemLen);
                oSel.moveEnd('character', 0);
                oSel.select();
            }
            else if (textBox.selectionStart || textBox.selectionStart == '0') {
                // Firefox/Chrome
                textBox.selectionStart = elemLen;
                textBox.selectionEnd = elemLen;
            }
            textBox.focus();
        }
        /* For search personnel */

        function cardObjectToJson(type) {
            switch (type) {
                case 1: // NewCard
                    document.getElementById('<%= hfNewCardJson.ClientID%>').value = JSON.stringify(NewCard);;
                    break;
                default:
                    break
            }
        }

    </script>
    <script src="/js/Survey/SurveyEdit.js"></script>

    <asp:HiddenField ID="hfNewCardJson" runat="server" />
    <asp:HiddenField ID="hfTopicId" runat="server" />
    <asp:HiddenField ID="hfCategoryId" runat="server" />
    <asp:HiddenField ID="hfAdminUserId" runat="server" />
    <asp:HiddenField ID="hfCompanyId" runat="server" />

    <script src="/js/hammer.min.js"></script>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/Survey/List" style="color: #000;">Responsive <span>survey</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlActionName" runat="server" Text="Create a survey" />
        </div>
        <div class="appbar__action">
            <asp:LinkButton ID="lbPreview" runat="server" CssClass="btn secondary" OnClick="lbPreview_Click">Live Survey Preview</asp:LinkButton>
        </div>
    </div>
    <!-- / App Bar -->

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfJsonStore" runat="server" />
            <asp:HiddenField ID="hfLiveSurveyPreviewJson" runat="server" />

            <!-- Reveal: Add Survey Icon -->
            <asp:Panel ID="popup_addtopicicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Survey Icon</h1>
                <div class="topicicons">
                    <p>Select from list</p>
                    <asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand" OnItemCreated="rtIcon_ItemCreated">
                        <HeaderTemplate>
                            <div class="topicicons__list">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <label class="topicicons__icon">
                                <asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
                                    <asp:Image ID="imgChooseIcon" runat="server" />
                                </asp:LinkButton>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbIconCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Add Survey Icon -->

            <!-- Reveal: Alert Popup -->
            <asp:Panel ID="popup_alertpopup" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Edit Survey</h1>
                <div class="topicicons" style="text-align: center; border-top-style: none;">
                    <label style="background: rgba(0, 117, 254, 1); margin: -15px auto 0px; padding: 5px; border-radius: 24px; width: 200px; color: white;">Survey is Active and Live/Hidden</label><br />
                    <label style="color: red;">
                        Editing will affect the Analytics,
                        <br />
                        therefore some features will be locked.</label>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAlertPopupCancel" CssClass="popup__action__item popup__action__item" runat="server" Text="OK" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Alert Popup -->

            <!--  Live Survey Preview -->
            <link href="/css/ion.rangeSlider/ion.rangeSlider.css" rel="stylesheet" />
            <link href="/css/ion.rangeSlider/ion.rangeSlider.skinNice.css" rel="stylesheet" />
            <asp:Panel ID="popup_livesurveypreview" runat="server" CssClass="popup " Width="100%" Style="display: none;">
                <h1 class="popup__title">Live Survey Preview</h1>
                <style>
                    .popup_phonepreview .question,
                    .popup_phonepreview .answer,
                    .popup_phonepreview .mcq-answer {
                        margin: 6px;
                    }

                    .popup_phonepreview .content {
                        background-color: #eee;
                        border: solid #eee 4px;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .popup_phonepreview .answer .content:hover {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .popup_phonepreview .mcq-answer .content:hover {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    table.mcq tbody tr td.text div.answer,
                    .preview-body .question .content {
                        background-color: #eee;
                        border: solid #eee 4px;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .preview-body .question .plain-content {
                        background-color: transparent;
                        border: solid transparent 4px;
                        padding: 4px;
                        text-align: center;
                    }

                    .preview-body .answer .content {
                        background-color: #eee;
                        border: solid #eee 4px;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .preview-body .content-selected {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        text-align: left;
                    }

                    .preview-body .content-instruction {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        text-align: center;
                    }

                    table.mcq td {
                        margin: 0;
                        padding: 0;
                    }

                        table.mcq td.checkbox {
                            width: 20px;
                        }

                        table.mcq td.text {
                            padding-left: 10px;
                        }

                    .mcq-instruction {
                        font-size: smaller;
                        background-color: #eee;
                        padding: 0 1em;
                    }

                    .tbNewCardOptionRangeMin, .tbNewCardOptionRangeMax, .tbNewCardOptionRangeMiddle, #lblNewCardOptionRangeMiddleMin {
                        display: inline-block;
                        width: 100px !important;
                        vertical-align: middle;
                        margin-left: 10px;
                    }

                    .tbNewCardOptionRangeMinLabel, .tbNewCardOptionRangeMaxLabel, .tbNewCardOptionRangeMiddleLabel {
                        display: inline-block;
                        margin-bottom: 5px !important;
                        padding: 10px 0px !important;
                        width: 92% !important;
                    }

                    #sltRangeNonmiddle, .maxminSelect {
                        border: 0px;
                        border-bottom: #ccc 1px solid;
                        width: 100%;
                        padding: 5px 0px;
                    }

                    label.tip {
                        color: #ccc;
                        font-size: 12px;
                    }

                    .inline-label {
                        display: inline-block;
                    }

                    label.inline-label.radio {
                        width: 195px;
                    }

                    .option_section {
                        margin-bottom: 30px;
                    }

                        .option_section .survey-lettercount {
                            float: none;
                        }

                    .remove-option-link {
                        float: right;
                        color: #fff;
                        background: #ed4444;
                        border: 1px solid #ed4444;
                        padding: 2px 10px;
                        font-size: 10px;
                        line-height: 16px;
                        transition: all 0.3s ease;
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                    }

                        .remove-option-link:hover {
                            background: #ffffff;
                            color: #ed4444;
                            border: 1px solid #ed4444;
                        }

                    .add-option-link {
                        float: right;
                        color: #fff;
                        background: #74d257;
                        padding: 5px 15px;
                        border: 1px solid #74d257;
                        font-size: 12px;
                        line-height: 16px;
                        transition: all 0.3s ease;
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                    }

                        .add-option-link:hover {
                            background: #ffffff;
                            color: #74d257;
                            border: 1px solid #74d257;
                        }

                    .question-image-content, .option-image-content, .cardContentImageWrapper {
                        visibility: visible;
                        height: auto;
                        overflow: auto;
                        border: 1px solid #ccc;
                        text-align: center;
                        background: url('/Img/grid.png') repeat;
                        /*display: flex;
                        display: -webkit-box;
                        display: -ms-flexbox;
                        align-content: center;
                        justify-content: center;*/
                    }

                        .question-image-content img, .option-image-content img, .cardContentImageWrapper img {
                            /*padding: 30px;*/
                            width: auto;
                            height: auto;
                            margin: 0 auto;
                        }

                    .survey-cross {
                        padding-top: 0px;
                        padding-left: 0px;
                        align-items: center;
                        justify-content: center;
                    }

                        .survey-cross i {
                            color: #fff;
                        }
                    /*
                    table.mcq tbody tr{
                        border: 2px dashed transparent;
                    }
                    table.mcq tbody tr:hover {
                        border: 2px dashed red;
                    }
                    */
                </style>
                <script type="text/javascript" src="/Js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
                <script type="text/javascript">
                    var data = [], currentQuestionIndex, topicJson;

                    function chooseOne(div) {
                        var q = data[currentQuestionIndex];

                        if ($(div).hasClass("content-selected")) {
                            $(div).removeClass("content-selected");
                            if ($(div).parent().parent().find("div.answer div.content-selected").length <= 0)
                                setPreviewCardFooterDisplay(q);
                        } else {

                            $(div).parent().parent().find("div.answer div.content-selected").removeClass("content-selected");
                            $(div).addClass("content-selected");
                            //$(".popup_phonepreview .footer-button").hide();
                            //if (q.HasPageBreak) {
                            //    $(".popup_phonepreview .next-page-button").show();
                            //} else {
                            //    $(".popup_phonepreview .next-question-button").show();
                            //}
                        }
                    }
                    function checkChange(checkbox) {
                        var q = data[currentQuestionIndex];

                        if ($(checkbox).prop("checked") == false) {
                            $(checkbox).parent().parent().removeClass("content-selected");
                            if ($(checkbox).parent().parent().parent().parent().find(":checked").length <= 0)
                                setPreviewCardFooterDisplay(q);
                        } else {
                            $(".popup_phonepreview .footer-button").hide();
                            if (q.HasPageBreak) {
                                $(".popup_phonepreview .next-page-button").show();
                            } else {
                                $(".popup_phonepreview .next-question-button").show();
                            }
                        }
                    }
                    function hidePreviewOverlay(div) {
                        $(".popup_phonepreview div.popup-overlay").hide();
                    }
                    function displayPreviewImage(div) {
                        $(".popup_phonepreview div.popup-overlay div.message").html($(div).parent().children("span").text());
                        $(".popup_phonepreview div.popup-overlay div.message").show();
                        $(".popup_phonepreview div.popup-overlay img")[0].src = div.src;
                        $(".popup_phonepreview div.popup-overlay img").show();
                        $(".popup_phonepreview div.popup-overlay").show();
                    }
                    function displayInfo(fa) {
                        $(".popup_phonepreview div.popup-overlay img").hide();
                        $(".popup_phonepreview div.popup-overlay div.message").html("<h1>Info</h1>" + $(fa).data("note"));
                        $(".popup_phonepreview div.popup-overlay div.message").show();
                        $(".popup_phonepreview div.popup-overlay").show();
                    }

                    function nextButtonAction() {
                        var idx, qIdx,
                            ansid, q = data[currentQuestionIndex];

                        if (q.ToBeSkipped == true) {
                            currentQuestionIndex++;
                            if (data.length == currentQuestionIndex)
                                displayClosing(topicJson);
                            else
                                displayQuestion(data[currentQuestionIndex]);
                            return;
                        }

                        // Validate question
                        if (q.Type == 1) { // selectone
                            if ($("div.answer div.content-selected").length <= 0) {
                                return; // don't know to display error message or do nothing (ie. return)
                            }

                            if (q.HasToLogic) {
                                ansid = $("div.answer div.content-selected").data("answer");

                                // search for card and display that card
                                for (idx = 0; idx < data.length; idx++) {
                                    if (data[idx].CardId === ansid) {
                                        currentQuestionIndex = idx;
                                        displayQuestion(data[currentQuestionIndex]);
                                        break;
                                    }
                                }
                                return;
                            }

                        } else if (q.Type == 2) { // multi-choice
                            var checkedItemCount = $(".mcq-answer input[type=checkbox]:checked").length;

                            if ((checkedItemCount < q.MiniOptionToSelect) || (checkedItemCount > q.MaxOptionToSelect)) {
                                return; // don't know to display error message or do nothing (ie. return)
                            }

                            //if (q.HasToLogic) { // This logic is flawed for multi-choice because of ambiguity of how logic is defined for question
                            //    ansid = $("div.mcq-answer div.content-selected").data("answer");

                            //    // search for card and display that card
                            //    for (idx = 0; idx < data.length; idx++) {
                            //        if (data[idx].CardId === ansid) {
                            //            currentQuestionIndex = idx;
                            //            displayQuestion(data[currentQuestionIndex]);
                            //            break;
                            //        }
                            //    }
                            //    return;
                            //}

                        } else if (q.Type == 5) { // drop list

                            if ($("div.answer select option:selected").val() == "") {
                                return; // don't know to display error message or do nothing (ie. return)
                            }

                            // No logic for drop list
                            //if (q.logic.length > 0) {
                            //    ansid = $("div.answer select option:selected").val();
                            //    for (idx = 0; idx < q.logic.length; idx++) {
                            //        if (q.logic[idx].when == ansid) { // Then need to find the index of goto question here
                            //            for (qIdx = currentQuestionIndex; qIdx < data.length; qIdx++) {
                            //                if (q.logic[idx].goto == data[qIdx].CardId) {
                            //                    currentQuestionIndex = qIdx;
                            //                    displayQuestion(data[currentQuestionIndex]);
                            //                    break;
                            //                }
                            //            }
                            //            break;
                            //        }
                            //    }
                            //    return;
                            //}
                        }

                        // Assume validation cleared; proceed to the next question
                        currentQuestionIndex++;
                        if (data.length == currentQuestionIndex)
                            displayClosing(topicJson);
                        else
                            displayQuestion(data[currentQuestionIndex]);
                    }

                    function displaySelectQuestion(q) {
                        var html, idx;

                        // Remove all question/options
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display question
                        html = "<div class=\"question\">" +
                                "	<div class=\"plain-content\">" + q.Content + "</div>";
                        for (idx = 0; idx < q.CardImages.length; idx++) {
                            html = html + "    <div style=\"width:100%; text-align:center;\" class=\"image\">" +
                                            "        <img style=\"width:250px;vertical-align:middle\" src=\"" + q.CardImages[idx].ImageUrl + "\" onclick=\"displayPreviewImage(this);\">" +
                                            "    </div>";
                        }
                        html = html + "</div>";
                        $(".popup_phonepreview .preview-body").append(html);

                        // Display answer(s)
                        for (idx = 0; idx < q.Options.length; idx++) {
                            html = "<div class=\"answer\">" +
                                    "	<div class=\"content\" onclick=\"chooseOne(this);\"  data-answer=\"" + q.Options[idx].NextCardId + "\" >";

                            if ((q.Options[idx].hasOwnProperty("Images")) && (q.Options[idx].Images.length > 0)) {
                                html = html + "        <img style=\"width:50px;vertical-align:middle\" src=\"" + q.Options[idx].Images[0].ImageUrl + "\" onclick=\"displayPreviewImage(this);\">";
                            }

                            html = html + "        <span>" + q.Options[idx].Content + "</span>" +
                                    "	</div>" +
                                    "</div>";
                            $(".popup_phonepreview .preview-body").append(html);
                        }
                    }
                    function displayMcqQuestion(q) {
                        var html, idx;

                        // Remove all question/options
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display question
                        html = "<div class=\"question\">" +
                                "	<div class=\"plain-content\">" + q.Content + "</div>";
                        for (idx = 0; idx < q.CardImages.length; idx++) {
                            html = html + "    <div style=\"width:100%; text-align:center;\" class=\"image\">" +
                                            "        <img style=\"width:250px;vertical-align:middle\" src=\"" + q.CardImages[idx] + "\">" +
                                            "    </div>";
                        }
                        html = html + "</div>";
                        $(".popup_phonepreview .preview-body").append(html);

                        // Decide what to display for multi-choice instructions
                        if ((q.MiniOptionToSelect == 0) && (q.MaxOptionToSelect == q.Options.length))
                            $(".popup_phonepreview .preview-body").append("<div class=\"mcq-instruction\">Select all that apply</div>");
                        else
                            $(".popup_phonepreview .preview-body").append("<div class=\"mcq-instruction\">Selection: Min: " + q.MiniOptionToSelect + "&nbsp;&nbsp;&nbsp;&nbsp;Max: " + q.MaxOptionToSelect + "</div>");

                        // Display answer(s)
                        for (idx = 0; idx < q.Options.length; idx++) {
                            html = "<div class=\"mcq-answer\">" +
                                    "	<label class=\"content\" data-answer=\"" + q.Options[idx].NextCardId + "\" >" +
                                    "        <div style=\"float:right;\"><input type=\"checkbox\" onchange=\"checkChange(this);\" /></div> ";

                            if ((q.Options[idx].hasOwnProperty("img")) && (q.Options[idx].img.length > 0)) {
                                html = html + "        <img style=\"width:50px;vertical-align:middle\" src=\"" + q.Options[idx].img + "\">";
                            }

                            html = html + "        <span>" + q.Options[idx].Content + "</span>" +
                                        "	</label>" +
                                    "</div>";
                            $(".popup_phonepreview .preview-body").append(html);

                            //$(".popup_phonepreview .preview-body").append(
                            //    "<div class=\"mcq-answer\">" +
                            //    "	<label class=\"content\">" +
                            //    "        <div style=\"float:right;\"><input type=\"checkbox\" /></div>" + 
                            //    "		<div class=\"mcq-text\">" + q.Options[idx] + "</div>" +
                            //    "	</label>" +
                            //    "</div>");
                        }
                    }
                    function displayListQuestion(q) {

                        // Display question text in yellow div
                        $(".popup_phonepreview .preview-body").children().remove();
                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"question\">" +
                            "	<div class=\"content\">" + q.Content +
                            "	</div>" +
                            "</div>");

                        var html, idx;
                        html = "<div class=\"answer\">" +
                        "	<div class=\"content mdl-selectfield\">" +
                        "		<select>";
                        html = html + "			<option value=\"\">Please select</option>";
                        for (idx = 0; idx < q.Options.length; idx++) {
                            html = html + "			<option value=\"" + q.Options[idx].OptionId + "\">" + q.Options[idx].Content + "</option>";
                        }
                        html = html + "		</select>" +
                        "	</div>" +
                        "</div>";

                        $(".popup_phonepreview .preview-body").append(html);

                    }
                    function displayRangeQuestion(q) {

                        $(".popup_phonepreview .preview-body").children().remove();

                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"question\">" +
                            "	<div class=\"content\">" + q.Content +
                            "	</div>" +
                            "</div>");

                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"answer\">" +
                            "   <div class=\"content\">" +
                            "       <input type=\"text\" id=\"inputRange\" name=\"inputRange\" value=\"\" />" +
                            "   </div>" +
                            "</div>");

                        var rangeIdx, rangeValues;
                        rangeValues = [];
                        for (rangeIdx = q.MinRange; rangeIdx <= q.MaxRange; rangeIdx++) {
                            rangeValues.push(rangeIdx);
                        }

                        //$("#inputRange").ionRangeSlider({
                        //    min: q.MinRange,
                        //    max: q.MaxRange,
                        //    from: q.MinRange
                        //});

                        $("#inputRange").ionRangeSlider({
                            min: q.MinRange,
                            max: q.MaxRange,
                            from: q.MinRange,
                            values: rangeValues,
                            onFinish: function (data) {
                                console.log("onFinish");

                                if ((q.MinRangeLabel) && (q.MinRangeLabel.length > 0)) {
                                    $(".preview-body .content .irs-min").text(q.MinRangeLabel);
                                } else {
                                    $(".preview-body .content .irs-min").text(q.MinRange.toString());
                                }

                                if ((q.MaxRangeLabel) && (q.MaxRangeLabel.length > 0)) {
                                    $(".preview-body .content .irs-max").text(q.MaxRangeLabel);
                                } else {
                                    $(".preview-body .content .irs-max").text(q.MaxRange.toString());
                                }
                            }
                        });

                        if ((q.MinRangeLabel) && (q.MinRangeLabel.length > 0)) {
                            $(".preview-body .content .irs-min").text(q.MinRangeLabel);
                        } else {
                            $(".preview-body .content .irs-min").text(q.MinRange.toString());
                        }

                        if ((q.MaxRangeLabel) && (q.MaxRangeLabel.length > 0)) {
                            $(".preview-body .content .irs-max").text(q.MaxRangeLabel);
                        } else {
                            $(".preview-body .content .irs-max").text(q.MaxRange.toString());
                        }
                    }
                    function displayTextQuestion(q) {

                        // Display question text in yellow div
                        $(".popup_phonepreview .preview-body").children().remove();
                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"question\">" +
                            "	<div class=\"content\">" + q.Content +
                            "	</div>" +
                            "</div>");
                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"answer\">" +
                            "   <div class=\"content\">" +
                            "       <textarea rows=\"8\" placeholder=\"Write here\"></textarea>" +
                            "   </div>" +
                            "</div>");
                    }
                    function displayInstructionQuestion(q) {

                        var idx, html = "";
                        // Display question text in yellow div
                        $(".popup_phonepreview .preview-body").children().remove();

                        html = "<div class=\"question\">" +
                            "	<div class=\"content content-instruction\" style=\"margin-bottom:1em;\" >" + q.Content +
                            "	</div>";

                        if (q.CardImages.length > 0) {
                            for (idx = 0; idx < q.CardImages.length; idx++) {
                                html = html + "<div style=\"text-align:center\"><img style=\"width:50px;vertical-align:middle\" src=\"" + q.CardImages[0].ImageUrl + "\" onclick=\"displayPreviewImage(this);\"></div>";
                            }
                        }

                        html = html + "</div>";

                        $(".popup_phonepreview .preview-body").append(html);

                        //$(".popup_phonepreview .preview-body").append(
                        //    "<div class=\"question\">" +
                        //    "	<div class=\"content content-instruction\">" + q.Content +
                        //    "	</div>" +
                        //    "</div>");

                    }

                    function displayIntroduction(topic) {
                        var html;

                        $(".popup_phonepreview .progress-bar-indication").hide();

                        // Remove all question/options
                        $(".popup_phonepreview .preview-info-bar").children().remove();
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display
                        html = "<div style=\"margin: 0 1em; position:absolute;bottom:50px\">" +
                                "	<div>" +
                                "		<img align=\"left\" src=\"" + topic.TopicIconUrl + "\" style=\"width:50px;vertical-align:top;margin:0 .5em 0 0\" />" +
                                "		<span>" + topic.TopicTitle + "</span>" +
                                "	</div>" +
                                "	<hr />" +
                                "	<div>" + topic.Introduction + "</div>" +
                                "</div>";

                        $(".popup_phonepreview .preview-body").append(html);

                        // Remove all buttons except for Start button
                        $(".popup_phonepreview .footer-button").hide();
                        $(".popup_phonepreview .start-survey-button").show();
                    }
                    function displayClosing(topic) {

                        $(".popup_phonepreview .progress-bar-indication").hide();

                        // Remove all question/options
                        $(".popup_phonepreview .preview-info-bar").children().remove();
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display
                        html = "<div style=\"position: absolute;top: 40%;text-align: center;width: 100%;\">" +
                                "	<h2>" + topic.ClosingWords + "</h2>" +
                                "</div>"
                        $(".popup_phonepreview .preview-body").append(html);

                        // Remove all buttons except for Done button
                        $(".popup_phonepreview .footer-button").hide();
                        $(".popup_phonepreview .done-survey-button").show();
                    }

                    function setPreviewCardCustomAnswerDisplay(q) {
                        if (q.HasCustomAnswer) {
                            $(".popup_phonepreview .preview-body").append(
                                "<div class=\"answer\">" +
                                "    <div class=\"plain-content\">" +
                                "        <div class=\"content\" onclick=\"chooseOne(this);\" style=\"display:inline-block;padding:1px;cursor:default;\" >Others:</div>" +
                                "        <input type=\"text\" placeholder=\"Please specify\" autocomplete=\"off\" style=\"display:inline-block;width:initial;padding:9px;\">" +
                                "    </div>" +
                                "</div>"
                            );
                        }
                    }
                    function setPreviewCardHeaderDisplay(q) {
                        // Check whether to display info icon
                        $(".popup_phonepreview .preview-info-bar").children().remove();
                        if (q.Note.length > 0) {
                            $(".popup_phonepreview .preview-info-bar").append("<i class=\"fa fa-info-circle\" data-note=\"" + q.Note + "\" onclick=\"displayInfo(this);\"></i>");
                        }
                    }
                    function setPreviewCardFooterDisplay(q) {
                        $(".popup_phonepreview .footer-button").hide();
                        if (q.HasPageBreak) {
                            $(".popup_phonepreview .next-page-button").show();
                        } else if (q.ToBeSkipped) {
                            $(".popup_phonepreview .skip-question-button").show();
                        } else {
                            $(".popup_phonepreview .next-question-button").show();
                        }
                    }

                    function startSurvey() {
                        if (data.length > 0) {
                            currentQuestionIndex = 0;
                            displayQuestion(data[currentQuestionIndex]);
                        }
                    }
                    function closeSurvey() {
                        $find("mpe").hide();
                    }

                    function displayQuestion(q) {   // q should be a question (aka Topic)
                        // All question share a common order of display
                        $(".popup_phonepreview").css("background-image", "url(/Img/bg_survey_" + q.BackgroundType + "a_big.png)");

                        setPreviewCardHeaderDisplay(q);

                        switch (q.Type) {
                            case 1: // "select"
                                displaySelectQuestion(q);
                                break;
                            case 2: // "mcq"
                                displayMcqQuestion(q);
                                break;
                            case 5: // "list"
                                displayListQuestion(q);
                                break;
                            case 4: // "range"
                                displayRangeQuestion(q);
                                break;
                            case 3: // "text"
                                displayTextQuestion(q);
                                break;
                            case 6: // "instruction"
                                displayInstructionQuestion(q);
                                break;
                        }

                        setPreviewCardCustomAnswerDisplay(q);

                        setPreviewCardFooterDisplay(q);
                    } // end function displayQuestion(q) 

                    $(document).ready(function () {
                        // read data from main_content_hfJsonStore
                        if ($("#<%=hfLiveSurveyPreviewJson.ClientID %>").val().length > 0) {
                            topicJson = JSON.parse($("#<%=hfLiveSurveyPreviewJson.ClientID %>").val());
                            // comment out for now; use the handcrafted json sample survey
                            data = topicJson.Cards;

                            displayIntroduction(topicJson);


                            // Define touch events (using hammerjs)
                            var myElement = $(".popup_phonepreview")[0];

                            // Create Hammer Manager
                            // A hammer manager is a container of all the recognizer instances for your element. 
                            // It sets up the input event listeners, and sets the touch-action property for you on the element.
                            var mc = new Hammer.Manager(myElement);

                            // Create Recognizers
                            var swipeRecognizer = new Hammer.Swipe({
                                event: "swipe",
                                threshold: 10,
                                velocity: 0.3,
                                direction: 24,
                                pointers: 1
                            });

                            mc.add([swipeRecognizer]);

                            mc.on("swipeup swipedown tap press", function (ev) { // listen to events...
                                var currentCard = data[currentQuestionIndex];

                                if ((currentCard === undefined) ||
                                    (currentCard.HasPageBreak) ||
                                    (currentCard.ToBeSkipped)
                                    ) {   // do nothing
                                    return;
                                }

                                // Else advance to next question
                                currentQuestionIndex++;
                                if (data.length == currentQuestionIndex)
                                    displayClosing(topicJson);
                                else
                                    displayQuestion(data[currentQuestionIndex]);
                                return;
                                // check question
                            });
                        }
                    });

                </script>
                <asp:Button ID="fakeSubmit" runat="server" Style="display: none;" OnClick="fakeSubmit_Click" />
                <div class="popup_phonepreview" style="width: 300px; height: 480px; border: solid 5px #999; margin: 0 auto; position: relative;">
                    <div class="popup-overlay" style="background-color: #fcfcfc; width: 100%; height: 100%; z-index: 10; position: absolute; display: none; text-align: center;" onclick="hidePreviewOverlay(this);">
                        <div class="message">
                            <h1>Info</h1>
                            hello world
                        </div>
                        <img src="" />
                    </div>
                    <div class="progress-bar-indication" style="border: 0; background-color: #ccc;">
                        <span class="meter" style="width: 60%; height: 4px; border: 0; background-color: #ffd800;"></span>
                    </div>

                    <div class="preview-info-bar" style="text-align: right; padding: 0 .5em;">
                        <i class="fa fa-info-circle"></i>
                    </div>

                    <div class="preview-body" style="height: 420px; display: inline-block; overflow-y: auto; overflow-x: hidden; width: 100%; position: relative;">
                    </div>

                    <footer style="width: 100%; position: absolute; bottom: 0; border-top: 1px solid #aaa;">
                        <button type="button" class="next-page-button footer-button" style="text-align: center; width: 100%; border-radius: 0; display: none;" onclick="nextButtonAction();">Next</button>
                        <button type="button" class="next-question-button footer-button" style="text-align: center; width: 100%; border-radius: 0; display: none;" onclick="nextButtonAction();"><i class="fa fa-chevron-down"></i></button>
                        <div class="skip-question-button footer-button" style="color: #222; text-align: right; margin-right: 4px; display: inline; float: right; cursor: pointer;" onclick="nextButtonAction();">Skip</div>

                        <button type="button" class="start-survey-button footer-button" style="text-align: center; width: 100%; border-radius: 0; background-color: #FFC700; display: none;" onclick="startSurvey();">Start</button>
                        <button type="button" class="done-survey-button footer-button" style="text-align: center; width: 100%; border-radius: 0; background-color: #FFC700; display: none;" onclick="closeSurvey();">Done</button>
                    </footer>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="LinkButton3" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- / Live Survey Preview -->

            <!-- Select Department -->
            <asp:Panel ID="plSelectDepartment" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Select Department</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="accessrights">
                                <asp:CheckBox ID="cbAllDepartment" runat="server" Text="Select All" AutoPostBack="true" OnCheckedChanged="cbAllDepartment_CheckedChanged" />
                            </div>
                            <hr />
                            <div class="accessrights">
                                <asp:CheckBoxList ID="cblDepartment" runat="server" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="cblDepartment_SelectedIndexChanged" />
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbSelectDepartmentSelect" CssClass="popup__action__item popup__action__item--cta" runat="server" Text="Select" OnClick="lbSelectDepartmentSelect_Click" />
                    <asp:LinkButton ID="lbSelectDepartmentCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Select Department -->

            <!-- Select Personnel -->
            <asp:Panel ID="plSelectPersonnel" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Select Personnel</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="accessrights">
                                <asp:CheckBox ID="CheckBox1" runat="server" Text="Select All" AutoPostBack="true" OnCheckedChanged="cbAllDepartment_CheckedChanged" />
                            </div>
                            <hr />
                            <div class="accessrights">
                                <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="cblDepartment_SelectedIndexChanged" />
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="LinkButton1" CssClass="popup__action__item popup__action__item--cta" runat="server" Text="Select" OnClick="lbSelectDepartmentSelect_Click" />
                    <asp:LinkButton ID="LinkButton2" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Select Personnel -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" BehaviorID="mpe" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_addtopicicon"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="data">
        <div class="data__content" onscroll="sticky_div();">
            <!-- Edit  Survey -->
            <div class="container">
                <div class="card add-topic">
                    <asp:UpdatePanel ID="upEditSurvey" runat="server" class="add-topic__icon">
                        <ContentTemplate>
                            <asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
                                <asp:Image ID="imgTopic" ImageUrl="~/img/icon-topicicon-default.png" runat="server" Width="120" Height="120" />
                                <label><small>Choose a survey icon</small></label>
                            </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="add-topic__info">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" class="add-topic__info__action" style="width: 35%;">
                            <ContentTemplate>
                                <asp:LinkButton ID="lbReports" runat="server" CssClass="btn secondary" Text="View Analytics" Style="float: none; width: 120px;" OnClick="lbReports_Click" />
                                <asp:LinkButton ID="lbSurvey" runat="server" CssClass="btn" Text="Apply Changes" OnClick="lbSurvey_Click" OnClientClick="ShowProgressBar();" />
                                <asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/Survey/List" Text="Cancel" CssClass="btn secondary" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="add-topic__info__details">
                            <div class="tabs tabs--styled">
                                <ul class="tabs__list">
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#topic-basic-info">Basic Info</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#topic-department">Participants</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#topic-settings">Settings</a>
                                    </li>
                                </ul>
                                <div class="tabs__panels">
                                    <div class="tabs__panels__item add-topic__info__details--basicinfo" id="topic-basic-info">
                                        <div class="column--input">
                                            <div class="form__row">
                                                <label>
                                                    Survey Title:
                                                </label>
                                                <asp:TextBox ID="tbSurveyTitle" runat="server" CssClass="topic-detail" placeholder="Name of your survey" onkeydown="return (event.keyCode!=13);" />
                                                <asp:Label ID="lblSurveyTitleCount" runat="server" CssClass="topic-lettercount" />
                                            </div>
                                            <div class="clear-float" style="height: 15px;"></div>
                                            <div class="form__row">
                                                <label>
                                                    Introduction:
                                                </label>
                                                <asp:TextBox ID="tbSurveyIntroduction" runat="server" CssClass="topic-detail" placeholder="Enter your introduction" onkeydown="return (event.keyCode!=13);" />
                                                <asp:Label ID="lblSurveyIntroductionCount" runat="server" CssClass="topic-lettercount" />
                                            </div>
                                            <div class="clear-float" style="height: 15px;"></div>
                                            <div class="form__row">
                                                <label>
                                                    Closing words:
                                                </label>
                                                <asp:TextBox ID="tbSurveyClosingWords" runat="server" CssClass="topic-detail" placeholder="Ex. Thank you for the survey participation" onkeydown="return (event.keyCode!=13);" />
                                                <asp:Label ID="lblSurveyClosingWordsCount" runat="server" CssClass="topic-lettercount" />
                                            </div>
                                            <div class="clear-float" style="height: 15px;"></div>
                                            <label style="color: #999;">* Go to 'Settings' to set the Start Date and End Date for the survey.</label>
                                        </div>
                                        <style>
                                            #main_content_UpdatePanel6 {
                                                margin-bottom: 20px;
                                                margin-top: 20px;
                                            }

                                                #main_content_UpdatePanel5 input, #main_content_UpdatePanel5 label, #main_content_UpdatePanel6 input, #main_content_UpdatePanel6 label {
                                                    display: inline-block;
                                                    width: auto;
                                                }

                                                    #main_content_UpdatePanel5 label:first-child, #main_content_UpdatePanel6 label:first-child {
                                                        display: block;
                                                    }

                                            .mdl-selectfield {
                                                width: 90px;
                                                display: inline-block;
                                            }

                                                .mdl-selectfield select {
                                                    padding: 0.65em;
                                                    margin-bottom: 0px;
                                                }

                                            #main_content_UpdatePanel6 input.time, #main_content_UpdatePanel5 input.time {
                                                width: 80px;
                                                text-align: center;
                                            }

                                            .duration_tip, .anonymous_tip {
                                                position: absolute;
                                                /*top: 0px;*/
                                                right: 0px;
                                                opacity: 1;
                                                display: block;
                                                background: #ffffff;
                                                width: 275px;
                                                border: 1px solid #ccc;
                                                padding: 10px;
                                                z-index: 1000;
                                                box-shadow: 0px 0px 10px #cccccc;
                                                transition: all 0.3s ease;
                                                -webkit-transition: all 0.3s ease;
                                                -ms-transition: all 0.3s ease;
                                                -moz-transition: all 0.3s ease;
                                            }

                                            .form__row.duration__tip:hover .duration_tip {
                                                opacity: 1;
                                            }

                                            .form__row.anonymous_tip_img:hover .anonymous_tip {
                                                opacity: 1;
                                            }
                                        </style>
                                        <div class="column--choice">
                                            <asp:Panel ID="plProgressStatus" runat="server" CssClass="form__row" Visible="false">
                                                <label style="font-weight: 700;">Progress</label>
                                                <asp:Literal ID="ltlProgressStatus" runat="server" />
                                            </asp:Panel>
                                            <div class="form__row">
                                                <label style="font-weight: 700;">Category</label>
                                                <ajaxToolkit:ComboBox ID="cbCategory" runat="server" AutoCompleteMode="Suggest" CssClass="combos" MaxLength="20" />
                                            </div>
                                            <div class="form__row duration__tip">
                                                <label style="font-weight: 700;">Duration</label>
                                                <a style="cursor: pointer;" onclick="$('.tabs.tabs--styled').easytabs('select', '#topic-settings');">Edit</a>
                                                <img src="/Img/icon_note_small.png" title="Go to 'Settings' to set the Start Date and End Date for the survey." />
                                                <div class="duration_tip" style="display: none; top: 11px;">
                                                    <label style="font-weight: 700;">Duration</label>
                                                    Go to 'Settings' to set the Start Date and End Date for the survey.
                                                </div>
                                            </div>

                                            <div class="form__row">
                                                <label style="font-weight: 700;">Status</label>
                                                <div class="mdl-selectfield" style="width: 100%; margin-top:1.5em;">
                                                    <asp:DropDownList ID="ddlStatus" runat="server" />
                                                </div>
                                            </div>

                                            <div class="form__row anonymous__tip" style="margin:0px;">
                                                <label style="display: inline-block; font-weight: 700;">Anonymity</label>
                                                <img class="anonymous_tip_img" style="display: inline-block;" src="/Img/icon_note_small.png" title="Participant's name will not be display in the Survey Report" onmouseover="$('#anonymousTip').css('display','block');" onmouseout="$('#anonymousTip').css('display','none');" />
                                                <div id="anonymousTip" class="anonymous_tip" style="display: none; top: 20px;">
                                                    <img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />
                                                    <label style="font-weight: 700;">Anonymous Survey</label>
                                                    Participant's detail will not be disclosed in the Final Report.
                                                </div>
                                            </div>
                                            <div class="mdl-selectfield" style="width: 100%; margin-top:1.5em;">
                                                <asp:DropDownList ID="ddlAnonymityCount" runat="server">
                                                    <asp:ListItem Text="OFF" Value="0" Selected="True" />
                                                    <asp:ListItem Text="Anonymous 1" Value="1" />
                                                    <asp:ListItem Text="Anonymous 3" Value="3" />
                                                    <asp:ListItem Text="Anonymous 5" Value="5" />
                                                    <asp:ListItem Text="Anonymous 7" Value="7" />
                                                    <asp:ListItem Text="Anonymous 11" Value="11" />
                                                </asp:DropDownList>
                                            </div>



                                            <div class="form__row anonymous__tip" style="display: none;">
                                                <asp:CheckBox ID="cbIsAnonymous" runat="server" Text="Anonymous Survey" CssClass="setting-checkbox" Style="float: left; width: 150px;" />
                                                <img class="anonymous_tip_img" src="/Img/icon_note_small.png" title="Participant's name will not be display in the Survey Report" />
                                                <div style="display: none; top: 20px;">
                                                    <img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />
                                                    <label style="font-weight: 700;">Anonymous Survey</label>
                                                    Participant's detail will not be disclosed in the Final Report.
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--department" id="topic-department">
                                        <div style="font-size: 1.1em;">
                                            <asp:CheckBox ID="cbParticipantsEveryone" runat="server" Checked="true" onclick="partcipantsCheck(1);" /><i class="fa fa-users" style="color: #999999"></i> Everyone
                                        </div>

                                        <hr />
                                        <div style="font-size: 1.1em;">
                                            <asp:CheckBox ID="cbParticipantsDepartment" runat="server" onclick="partcipantsCheck(2);" /><i class="fa fa-briefcase" style="color: #999999"></i> Selected departments
                                        </div>
                                        <div>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rtParticipantsDepartment" runat="server" OnItemCommand="rtParticipantsDepartment_ItemCommand">
                                                        <HeaderTemplate>
                                                            <div class="tags">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <div class="tag">
                                                                <asp:Label ID="lblTagName" runat="server" CssClass="tag__label" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' />
                                                                <asp:LinkButton ID="lbTagRemove" runat="server" CssClass="tag__icon" Text="x" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>' CommandName="Remove" />
                                                            </div>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <br />
                                                    <asp:LinkButton ID="lbAddParticipantsDepartment" runat="server" Text="+ Add more department" OnClick="lbAddParticipantsDepartment_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <br />
                                        <div style="font-size: 1.1em;">
                                            <asp:CheckBox ID="cbParticipantsPersonnel" runat="server" onclick="partcipantsCheck(2);" /><i class="fa fa-user" style="color: #999999"></i> Selected personnel
                                        </div>
                                        <div>
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                <ContentTemplate>
                                                    <asp:Repeater ID="rtParticipantsPersonnel" runat="server" OnItemCommand="rtParticipantsPersonnel_ItemCommand">
                                                        <HeaderTemplate>
                                                            <div class="tags">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <div class="tag">
                                                                <asp:Label ID="lblTagName" runat="server" CssClass="tag__label" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName")%>' />
                                                                <asp:LinkButton ID="lbTagRemove" runat="server" CssClass="tag__icon" Text="x" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId")%>' CommandName="Remove" />
                                                            </div>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <br />
                                                    <div class="suspendedsearch" style="width: 60%;">
                                                        <asp:TextBox ID="tbSearchKey" runat="server" placeholder="Search Personnel" onkeydown="return (event.keyCode!=13);showLoading();" onkeyup="RefreshUpdatePanel(event);" MaxLength="50" OnTextChanged="tbSearchKey_TextChanged" autocomplete="off" />
                                                        <img id="imgLoading" src="/Img/circle_loading.gif" style="display: none;" />
                                                        <div class="suggestions">
                                                            <asp:Repeater ID="rtSearchResult" runat="server" OnItemCommand="rtSearchResult_ItemCommand" OnItemDataBound="rtSearchResult_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <ul class="suggestions__list">
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <li class="suggestions__list__item">
                                                                        <asp:LinkButton ID="lbAddUser" runat="server" CssClass="suggestions__link" ClientIDMode="AutoID" CommandName="AddUser" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId")+ "," + DataBinder.Eval(Container.DataItem, "FirstName") +"," + DataBinder.Eval(Container.DataItem, "LastName") %>'>
                                                                            <span class="suggestions__name">
                                                                                <asp:Literal ID="ltlUserName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName")  %>' />
                                                                            </span>
                                                                            <span class="suggestions__email"><%# DataBinder.Eval(Container.DataItem, "Email") %></span>
                                                                        </asp:LinkButton></li>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </ul>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--settings" id="topic-settings">
                                        <div>
                                            <h3>Schedule</h3>
                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                <ContentTemplate>
                                                    <label>Start date</label>
                                                    <asp:TextBox ID="tbScheduleStartDate" runat="server" placeholder="dd/mm/yyyy" CssClass="date" AutoPostBack="true" onkeydown="return false;" />
                                                    <ajaxToolkit:CalendarExtender runat="server" TargetControlID="tbScheduleStartDate" ID="ceStart" CssClass="ajax__calendar" Format="dd/MM/yyyy" />
                                                    <span class="text-vertical-center">at</span>
                                                    <asp:TextBox ID="tbScheduleStartHour" runat="server" placeholder="hh" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="12" AutoPostBack="true" />
                                                    <span class="text-vertical-center">:</span>
                                                    <asp:TextBox ID="tbScheduleStartMinute" runat="server" placeholder="mm" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="00" AutoPostBack="true" />
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlScheduleStartMer" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="am" Value="am" />
                                                            <asp:ListItem Text="pm" Value="pm" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:Label ID="lblScheduleStartDate" runat="server" Visible="false" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                <ContentTemplate>
                                                    <label>End date</label>
                                                    <asp:RadioButton ID="rbScheduleEndDateForever" runat="server" GroupName="ScheduleEndDate" Text="Forever" Checked="true" />
                                                    <br />
                                                    <asp:RadioButton ID="rbScheduleEndDateCustom" runat="server" GroupName="ScheduleEndDate" />
                                                    <asp:TextBox ID="tbScheduleEndDate" runat="server" placeholder="dd/mm/yyyy" CssClass="date" AutoPostBack="true" onkeydown="return false;" />
                                                    <ajaxToolkit:CalendarExtender runat="server" TargetControlID="tbScheduleEndDate" ID="ceEnd" CssClass="ajax__calendar" Format="dd/MM/yyyy" />
                                                    <span class="text-vertical-center">at</span>
                                                    <asp:TextBox ID="tbScheduleEndHour" runat="server" placeholder="hh" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="12" AutoPostBack="true" />
                                                    <span class="text-vertical-center">:</span>
                                                    <asp:TextBox ID="tbScheduleEndMinute" runat="server" placeholder="mm" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="00" AutoPostBack="true" />
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlScheduleEndMer" runat="server" AutoPostBack="true">
                                                            <asp:ListItem Text="am" Value="am" />
                                                            <asp:ListItem Text="pm" Value="pm" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:Label ID="lblScheduleEndDate" runat="server" Visible="false" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>


                                        <%--<div class="form__row">
                                            <asp:CheckBox ID="cbSettingsRandomQuestion" runat="server" Text="Randomize question order for entire quiz" CssClass="setting-checkbox" />
                                            <div class="clear-float"></div>
                                            <asp:CheckBox ID="cbSettingsShowProgressBar" runat="server" Text="Show progress bar" CssClass="setting-checkbox" />
                                            <div class="clear-float"></div>
                                            <asp:CheckBox ID="cbSettingsFeedback" runat="server" Text="Allow feedbacks and suggestions" CssClass="setting-checkbox" />
                                            <div class="clear-float"></div>
                                            <asp:CheckBox ID="cbSettingsAllowPrevious" runat="server" Text="Allow return to previous question" CssClass="setting-checkbox" />
                                            <div class="clear-float"></div>
                                            <asp:CheckBox ID="cbSettingsSaveHistory" runat="server" Text="Allow participants to view survey in history" CssClass="setting-checkbox" />
                                            <div class="clear-float"></div>
                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / Edit Survey -->

            <!-- / Questions  Bar -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <script>
                        function sticky_div() {
                            var window_top = $(window).scrollTop();

                            var divTopId = $('#sticky_anchor');
                            if (!divTopId.length) {
                                return;
                            }
                            var div_top = divTopId.offset().top; // get the offset of the div from the top of page

                            // var div_top = $('#sticky-anchor').offset().top;
                            if (window_top + 160 > div_top) {
                                $('.sticky').addClass('stick');
                                $('#sticky_anchor').addClass('stick');

                            } else {
                                $('.sticky').removeClass('stick');
                                $('#sticky_anchor').removeClass('stick');

                            }
                        }
                    </script>
                    <asp:Panel ID="plSearchQuestion" runat="server" Visible="false">
                        <div id="sticky_anchor"></div>
                        <div class="survey-bar sticky">
                            <div class="container">
                                <div class="survey-bar__search">
                                    <asp:TextBox ID="tbSearchQuestion" runat="server" placeholder="Search question" CssClass="survey-bar__search__input" />
                                    <asp:LinkButton ID="lbAdd" runat="server" CssClass="survey-bar__search__button" Text="+ Add a Question" OnClick="lbAdd_Click" />
                                    <%--<asp:LinkButton ID="lbSearch" runat="server" CssClass="survey-bar__search__button" OnClientClick="ShowProgressBar();">
                                    <i class="fa fa-search"></i>
                                    </asp:LinkButton>
                                    --%>
                                    <a onclick="ShowProgressBar(); return fetchAndRefreshListDisplay();" class="survey-bar__search__button" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- / Questions  Bar -->

            <!-- New Card -->
            <div id="animateCard">
                <asp:UpdatePanel ID="upNewQuestion" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="plNewQuestion" runat="server" CssClass="container" Visible="false">
                            <div id="add-question-card" class="card add-question ">
                                <div class="add-question__questionnumber">
                                    <div class="number">
                                        <asp:Literal ID="ltlNewQuestionPaging" runat="server" />
                                    </div>
                                    <hr />
                                    <div class="number">
                                        <asp:Literal ID="ltlNewCardOrdering" runat="server" />
                                    </div>
                                </div>
                                <div class="add-question__info">
                                    <div class="add-question__info__header">
                                        <div class="add-question__info__header__action">
                                            <asp:LinkButton ID="lbNewQuestionCancel" runat="server" CssClass="btn secondary" Text="Cancel" OnClick="lbNewQuestionCancel_Click" />
                                            <asp:LinkButton ID="lbNewQuestionSave" runat="server" CssClass="btn" Text="Create" OnClientClick="ShowProgressBar();  return checkNewCardInputData();" OnClick="lbNewQuestionSave_Click" />
                                        </div>
                                        <div class="add-question__info__header__title">Question Type</div>
                                    </div>
                                    <div class="add-question__info__setup">
                                        <div class="add-question__info__setup__questiontype">
                                            <div class="tabs--questionformat">
                                                <ul class="tabs__list">
                                                    <div id="divNewCardType1" class="tabs__list__item" style="margin: 2px; background: #0075FF; color: #F2F4F7; cursor: pointer;" onclick="setNewCardType(1);">
                                                        Select one
                                                    </div>
                                                    <div id="divNewCardType2" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(2);">
                                                        Multi choice
                                                    </div>
                                                    <div id="divNewCardType3" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(3);">
                                                        Text
                                                    </div>
                                                    <div id="divNewCardType4" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(4);">
                                                        Number range
                                                    </div>
                                                    <div id="divNewCardType5" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(5);">
                                                        Drop list
                                                    </div>
                                                    <div id="divNewCardType6" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(6);">
                                                        Instructions
                                                    </div>
                                                </ul>
                                                <div class="tabs__panels">
                                                    <div class="tabs__panels__item tabs__panels__item">
                                                        <div class="form__row form__row--question">
                                                            <div>
                                                                <textarea id="tbNewCardContent" style="padding-left: 40px;" onkeydown="return (event.keyCode!=13);" onblur="setNewCardValue(1, this, null);" placeholder="Question" rows="2" cols="20" onkeyup="textCounter(this,'lblContentCount', 250); setNewCardValue(1, this, null);"></textarea>
                                                                <span id="lblContentCount" class="survey-lettercount"></span>
                                                                <label class="upload-card-image" for="fuNewCardContentImg">
                                                                    <input type="file" id="fuNewCardContentImg" accept="image/*" title="Add image" style="display: none" onchange="previewImage(this, 2560, 2560 ,1, 1);" />
                                                                </label>
                                                                <div id="divNewCardContentImg" style="visibility: hidden; height: 0px; overflow: auto;" class="question-image-content">
                                                                    <div class="survey-cross" onclick="removeContentImg(1, 1);">
                                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                                    </div>
                                                                    <img id="imgNewCardContentImgPreview" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="divNewCardType1Layout" style="visibility: visible; display: block; overflow: auto;" class="form__row form__row--question">
                                                            <div id="divNewCardOptions" class="options-container">
                                                                <div style="margin-bottom: 10px" id="divNewCardOption1">
                                                                    <textarea name="tbNewCardOption1" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption1ContentCount', 150); setNewCardValue(2, this, 1);" onblur="setNewCardValue(2, this, 1);" placeholder="Option 1" rows="2" cols="20"></textarea>
                                                                    <span id="lblNewCardOption1ContentCount" class="survey-lettercount"></span>
                                                                    <label class="upload-option-image" for="fuNewCardOption1ContentImg">
                                                                        <input type="file" id="fuNewCardOption1ContentImg" accept="image/*" title="Add image" style="display: none" onchange="previewImage(this, 2560, 2560 ,2, 1);" /></label><div id="divNewCardOption1ContentImg" style="visibility: hidden; height: 0px; overflow: auto;" class="option-image-content">
                                                                            <div class="survey-cross" onclick="removeContentImg(2, 1);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                            <img id="imgNewOption1ContentImgPreview" />
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <a href="javascript:void(0);" class="options-container add-option-link" onclick="addOptionField()" style="float: right;">+ Add option</a>
                                                        </div>
                                                        <div id="divNewCardType2Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question">Type 2 Layout</div>
                                                        <div id="divNewCardType3Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question"></div>
                                                        <div id="divNewCardType4Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question">
                                                            <label style="margin-bottom: 15px;">
                                                                Number range
                                                                <br />
                                                                (Minimum: -20 to Maximum: 20)
                                                            </label>
                                                            <div class="option_section">
                                                                Slider starts from<br />
                                                                <label class="inline-label radio">
                                                                    <input type="radio" name="numberRangeStartFrom" value="1" checked="checked" onchange="resetRangeType(1); setNewCardValue(15, this, null);">Minimum/Maximum</label>
                                                                <label class="inline-label radio">
                                                                    <input type="radio" name="numberRangeStartFrom" value="2" onchange="resetRangeType(2); setNewCardValue(15, this, null);">Middle</label>
                                                            </div>
                                                            <div id="divNewCardRangeNonmiddle">
                                                                <select id="sltRangeNonmiddle" onchange="setNewCardValue(15, this, null);">
                                                                    <option value="1">Start from Minimum</option>
                                                                    <option value="3">Start from Maximum</option>
                                                                </select>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Minimum value</label>
                                                                    <input name="tbNewCardOptionRangeMin" class="tbNewCardOptionRangeMin nonmiddle" onblur="setNewCardValue(11, this, null);" type="number" /><input name="tbNewCardOptionRangeMinLabel" class="tbNewCardOptionRangeMinLabel" type="text" placeholder="Label(Optional)" onkeyup="textCounter(this,'lblNewCardRangeNonmiddleMinCount', 25); setNewCardValue(19, this, null);" /><span id="lblNewCardRangeNonmiddleMinCount" class="survey-lettercount"></span><label class="tip">Eg. Poor, Bad, Need improvement, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Maximum value</label>
                                                                    <input name="tbNewCardOptionRangeMax" class="tbNewCardOptionRangeMax nonmiddle" onblur="setNewCardValue(12, this, null);" type="number" /><input type="text" name="tbNewCardOptionRangeMaxLabel" class="tbNewCardOptionRangeMaxLabel" placeholder="Label(Optional)" onkeyup="textCounter(this,'lblNewCardRangeNonmiddleMaxCount', 25); setNewCardValue(18, this, null);" /><span id="lblNewCardRangeNonmiddleMaxCount" class="survey-lettercount"></span><label class="tip">Eg. Excellent, Great, Very good, Amazing, etc.</label>
                                                                </div>
                                                            </div>
                                                            <div id="divNewCardRangeMiddle" style="display: none;">
                                                                <div class="option_section">
                                                                    <label class="inline-label">Middle value</label>
                                                                    <input name="tbNewCardOptionRangeMiddle" class="tbNewCardOptionRangeMiddle middle" onblur="setNewCardValue(16, this, null);" type="number" /><input type="text" name="tbNewCardOptionRangeMiddleLabel" class="tbNewCardOptionRangeMiddleLabel" placeholder="Label" onkeyup="textCounter(this,'lblNewCardRangeMiddleMiddleCount', 25); setNewCardValue(17, this, null);" /><span id="lblNewCardRangeMiddleMiddleCount" class="survey-lettercount"></span><label class="tip">Eg. Average, Neutral, So-so, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Maximum value</label>
                                                                    <input name="tbNewCardOptionRangeMax" class="tbNewCardOptionRangeMax middle" onblur="setNewCardValue(20, this, null);" type="number" /><input type="text" name="tbNewCardOptionRangeMaxLabel" class="tbNewCardOptionRangeMaxLabel" placeholder="Label" onkeyup="textCounter(this,'lblNewCardRangeMiddleMaxCount', 25); setNewCardValue(18, this, null);" /><span id="lblNewCardRangeMiddleMaxCount" class="survey-lettercount"></span><label class="tip">Eg. Excellent, Great, Very good, Amazing, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Minimum value</label>
                                                                    <label id="lblNewCardOptionRangeMiddleMin"></label>
                                                                    <input type="text" name="tbNewCardOptionRangeMinLabel" class="tbNewCardOptionRangeMinLabel" placeholder="Label" onkeyup="textCounter(this,'lblNewCardRangeMiddleMinCount', 25); setNewCardValue(19, this, null);" /><span id="lblNewCardRangeMiddleMinCount" class="survey-lettercount"></span><label class="tip">Eg. Poor, Bad, Need improvement, etc.</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="divNewCardType5Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question">
                                                            <textarea rows="16" id="txareaNewCard" class="droplist_options" placeholder="Please type in your options here, subsequent options separated by 'Enter' key." onkeyup="updateSelectPreview(this);setNewCardValue(14, this, null);" style="overflow-y: auto;"></textarea>
                                                            <label>Please type in your options here, subsequent options separated by 'Enter' key.</label>
                                                        </div>
                                                        <div id="divNewCardType6Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question"></div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add-question__info__setup__preview">
                                            <!-- Card Preview Format -->
                                            <div id="divNewCardPreview" class="survey-preview" style="background-image: url(/Img/bg_survey_1a_big.png); position: relative;">
                                                <div>
                                                    <!-- For Note -->
                                                    <img class="right" id="imgNewCardPreviewNote" src="/Img/icon_note_small.png" />
                                                </div>
                                                <div class="preview-question">
                                                    <!-- For CardContentText, CardContentImgs  -->
                                                    <span class="preview-question-content" id="lblNewCardPreviewContentText" style="color: black;"></span>
                                                    <img class="preview-question-image" id="imgNewCardPreviewContentImg" />
                                                </div>
                                                <div id="divNewCardPreviewTypes">
                                                    <div id="divNewCardType1PreviewLayout"></div>

                                                    <div id="divNewCardType2PreviewLayout" style="display: none; height: 0px;"></div>
                                                    <div id="divNewCardType3PreviewLayout" style="display: none; height: 0px;">
                                                        <div class="preview-textarea">
                                                            <div class="preview-textarea-content">Answer area</div>
                                                        </div>
                                                    </div>
                                                    <div id="divNewCardType4PreviewLayout" style="display: none; height: 0px; overflow: auto;">
                                                        <div class="preview-numberrange">
                                                            <div>
                                                                <input type="text" id="range" value="" name="range" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divNewCardType5PreviewLayout" style="display: none; height: 0px;">
                                                        <select class="preview-droplist">
                                                            <option>Please Select</option>
                                                        </select>
                                                    </div>
                                                    <div id="divNewCardType6PreviewLayout" style="display: none; height: 0px;">
                                                    </div>
                                                </div>
                                                <!-- For PageBreak, IsAllowSkipped -->
                                                <div class="preview-footer-content" id="divNewCardPeviewNoPageBreak" style="background: rgba(0, 153, 253, 1); color: white; position: absolute; bottom: 0;"><span class="center">No page break</span></div>
                                                <div class="preview-footer-content" id="divNewCardPeviewPageBreak" style="background: rgba(0, 153, 253, 1); color: white; position: absolute; bottom: 0; display: none;"><span class="center">Next</span></div>
                                                <div class="preview-footer-content" id="divNewCardPeviewSkip" style="color: rgba(150, 176, 216, 1); position: absolute; bottom: 0; display: none;"><span class="right">Skip</span></div>
                                            </div>
                                            <!-- / Card Preview Format -->
                                        </div>
                                        <div class="add-question__info__setup__choice">
                                            <div id="divNewCardBehaviour">
                                                <label style="font-weight: bold">Behaviour</label>
                                                <span class="setting-checkbox">
                                                    <input name="cbNewCardAllowSkip" id="cbNewCardAllowSkip" onclick="setNewCardValue(3, this, null);" type="checkbox" /><label for="cbNewCardAllowSkip">Allow question to be skipped</label>
                                                </span>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardTickOther">
                                                    <span class="setting-checkbox">
                                                        <input name="cbNewCardTickOther" id="cbNewCardTickOther" onclick="setNewCardValue(4, this, null);" type="checkbox" />
                                                        <label for="cbNewCardTickOther">Tick 'other' and type your own answer</label>
                                                    </span>
                                                </div>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardCustomCommand" style="visibility: hidden; height: 0px; overflow: auto;">
                                                    Custom command
                                                    <input id="tbNewCardCustomCommand" onkeyup="setNewCardValue(6, this, null);" onblur="setNewCardValue(6, this, null);" type="text" placeholder="Please specify (Default)" />
                                                </div>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardRandomOption" style="visibility: hidden; height: 0px; overflow: auto;">
                                                    <span class="setting-checkbox">
                                                        <input name="cbNewCardRandomOption" id="cbNewCardRandomOption" onclick="setNewCardValue(5, this, null);" type="checkbox" />
                                                        <label for="cbNewCardRandomOption">Randomize option order</label>
                                                    </span>
                                                </div>
                                                <div class="clear-float"></div>

                                            </div>
                                            <br />
                                            <div id="divNewCardMulti" style="visibility: hidden; height: 0px;">
                                                <label style="font-weight: bold">No of options to be selected</label>
                                                Minimum
                                                <input name="tbNewCardMultiMin" id="tbNewCardMultiMin" onblur="setNewCardValue(7, this, null);" type="number" value="1" /><br />
                                                Maximum
                                                <input name="tbNewCardMultiMax" id="tbNewCardMultiMax" onblur="setNewCardValue(8, this, null);" type="number" value="1" /><br />
                                            </div>
                                            <br />
                                            <div id="divNewCardDisplay">
                                                <label style="font-weight: bold">Display</label>
                                                <div>
                                                    Background
                                                    <!-- Background -->
                                                    <div class="imageslider" style="margin: 10px auto auto; width: 85%;">
                                                        <div id="slider">
                                                            <!-- Slider Setup -->
                                                            <input name="slider" id="slide1" onchange="setNewCardValue(13, this, 1);" type="radio" selected="false" checked="">
                                                            <input name="slider" id="slide2" onchange="setNewCardValue(13, this, 2);" type="radio" selected="false">
                                                            <input name="slider" id="slide3" onchange="setNewCardValue(13, this, 3);" type="radio" selected="false">
                                                            <!-- / Slider Setup -->
                                                            <!-- The Slider -->
                                                            <div id="slides" style="margin-top: 0px;">
                                                                <div id="overflow">
                                                                    <div class="inner">
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_1a_small.png">
                                                                        </article>
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_2a_small.png">
                                                                        </article>
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_3a_small.png">
                                                                        </article>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- / The Slider -->
                                                            <!-- Controls and Active Slide Display -->
                                                            <div class="controls" id="controls" style="top: 38%; display: none;">
                                                                <label for="slide1"></label>
                                                                <label for="slide2"></label>
                                                                <label for="slide3"></label>
                                                            </div>
                                                            <!-- / Controls and Active Slide Display -->
                                                        </div>
                                                    </div>
                                                    <!-- Background -->

                                                </div>
                                                <br />
                                            </div>
                                            <br />
                                            <div id="divNewCardNotes">
                                                <label style="font-weight: bold">Notes</label>
                                                <input name="tbNewCardNotes" id="tbNewCardNotes" onkeydown="return (event.keyCode!=13);" onblur="setNewCardValue(9, this, null);" type="text" maxlength="150" placeholder="Enter notes">
                                                <span style="color: #979797">Notes will be seen by user during the survey via the
                                                    <img src="/Img/icon_note_small.png" />icon</span>
                                            </div>
                                            <br />
                                            <div id="divNewCardPageBreak">
                                                <span class="setting-checkbox">
                                                    <input name="cbNewCardPageBreak" id="cbNewCardPageBreak" onclick="setNewCardValue(10, this, null);" type="checkbox" />
                                                    <label for="cbNewCardPageBreak">Page Break</label>
                                                </span>
                                                <div class="clear-float"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border-bottom: 2px solid #cccccc; margin-top: 35px;" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <asp:LinkButton ID="lbNewQuestionLocation" runat="server" />
            <!-- / New Card -->

            <style>
                .column {
                    width: 100%;
                    margin-right: .5%;
                    min-height: 300px;
                    background: #fff;
                    float: left;
                }

                    .column .dragbox {
                        margin: 5px 2px 20px;
                        background: #fff;
                        position: relative;
                        border: 1px solid #ddd;
                        -moz-border-radius: 5px;
                        -webkit-border-radius: 5px;
                    }

                        .column .dragbox h2 {
                            margin: 0;
                            font-size: 12px;
                            padding: 5px;
                            background: #f0f0f0;
                            color: #000;
                            border-bottom: 1px solid #eee;
                            font-family: Verdana;
                            cursor: move;
                        }

                .dragbox-content {
                    background: #fff;
                    min-height: 100px;
                    margin: 5px;
                    font-family: 'Lucida Grande', Verdana;
                    font-size: 0.8em;
                    line-height: 1.5em;
                    /*display: none;*/
                }

                .column .placeholder {
                    background: #f0f0f0;
                    border: 1px dashed #ddd;
                }

                .dragbox h2.collapse {
                    background: #f0f0f0 url('collapse.png') no-repeat top right;
                }

                .dragbox h2 .configure {
                    font-size: 11px;
                    font-weight: normal;
                    margin-right: 30px;
                    float: right;
                }

                .selected-card-type {
                    background: #0075FF !important;
                    color: #F2F4F7 !important;
                }
            </style>

            <div id="cardList" class="container">
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function updateTopicHeader(topic) {
            if (topic) { // if parameter is not null or undefined

            }
        }

        function displayTopicCardList(cards) {
            if (cards) {// if parameter is not null or undefined
                if (cards.length <= 0) // no cards; do nothing
                    return;
                // display card 
            }
        }

        function selectSkipOrPagebreak(cardId) {
            var cardDivSelector;
            cardDivSelector = "div.card." + cardId;

            $(cardDivSelector + " .preview-footer-content.no-page-break").hide();
            $(cardDivSelector + " .preview-footer-content.page-break").hide();
            $(cardDivSelector + " .preview-footer-content.skip").hide();

            if ($(cardDivSelector + " .setting-ToBeSkipped").is(":checked")) {
                $(cardDivSelector + " .preview-footer-content.skip").show();
            }
            else {
                if ($(cardDivSelector + " .setting-PageBreak").is(":checked")) {
                    $(cardDivSelector + " .preview-footer-content.page-break").show();
                }
                else {
                    $(cardDivSelector + " .preview-footer-content.no-page-break").show();
                }
            }
        }

        function updateListCard(card) {
            var cardDivSelector, html, disabledAttr;

            cardDivSelector = "div.card." + card.CardId;

            $(cardDivSelector).addClass("opencard");

            html = "" +
"    <div class=\"add-question__questionnumber\">" +
"        <div class=\"number paging\">" +
"            P" + card.Paging +
"        </div>" +
"        <hr>" +
"        <div class=\"number ordering\">" +
"            Q" + card.Ordering +
"        </div>" +
"    </div>" +
"    <div class=\"add-question__info\">" +
"        <div class=\"add-question__info__header\">" +
"            <div class=\"add-question__info__header__action\">" +
"                <a id=\"main_content_lbNewQuestionCancel\" class=\"btn secondary\" href=\"javascript:displayMiniCard('" + card.CardId + "')\">Cancel</a>" +
"                <a onclick=\"ShowProgressBar(); return updateCard('" + card.CardId + "');\" class=\"btn updateCard\" href=\"javascript:void(0);\">Save</a>" +
"            </div>" +
"            <div class=\"add-question__info__header__title\">Question Type</div>" +
"        </div>" +
"        <div class=\"add-question__info__setup\">" +
"            <div class=\"add-question__info__setup__questiontype\">" +
"                <div class=\"tabs--questionformat\">";

            if (canChangeCardFormat()) {
                disabledAttr = "";
                html = html +
            "                    <ul class=\"tabs__list\">" +
            "                        <div class=\"cardtype1 tabs__list__item selected-card-type\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(1, '" + card.CardId + "');\">Select one</div>" +
            "                        <div class=\"cardtype2 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(2, '" + card.CardId + "');\">Multi choice</div>" +
            "                        <div class=\"cardtype3 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(3, '" + card.CardId + "');\">Text</div>" +
            "                        <div class=\"cardtype4 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(4, '" + card.CardId + "');\">Number range</div>" +
            "                        <div class=\"cardtype5 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(5, '" + card.CardId + "');\">Drop list</div>" +
            "                        <div class=\"cardtype6 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(6, '" + card.CardId + "');\">Instructions</div>" +
            "                    </ul>";
            } else {
                disabledAttr = "disabled";
                html = html +
            "                    <ul class=\"tabs__list\">" +
            "                        <div class=\"cardtype1 tabs__list__item selected-card-type\" style=\"margin: 2px; color: #CCCCCC; \" >Select one</div>" +
            "                        <div class=\"cardtype2 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Multi choice</div>" +
            "                        <div class=\"cardtype3 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Text</div>" +
            "                        <div class=\"cardtype4 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Number range</div>" +
            "                        <div class=\"cardtype5 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC;\">Drop list</div>" +
            "                        <div class=\"cardtype6 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Instructions</div>" +
            "                    </ul>";
            }


            html = html +
            "                    <div class=\"tabs__panels\">" +
            "                        <div class=\"tabs__panels__item tabs__panels__item\">" +
            "                            <div class=\"form__row form__row--question\">" +
            "                                <div>";

            html = html +
            "<div>" +
            "    <textarea class=\"tbNewCardContent\" data-optionid=\"" + card.CardId + "\"  style=\"padding-left: 40px;\" onkeydown=\"return (event.keyCode!=13);\" placeholder=\"Question\" rows=\"2\" cols=\"20\" onkeyup=\"updateOptionTextCount(this, SURVEY_CARD_CONTENT_MAX); $('div.card." + card.CardId + " .preview-question-content').text(this.value); \">" + card.Content + "</textarea>" +
            "    <span class=\"survey-lettercount " + card.CardId + "\">" + (SURVEY_CARD_CONTENT_MAX - card.Content.length) + "</span>" +
            "    <label class=\"upload-card-image\" for=\"fileUploadForQuestion_" + card.CardId + "\">" +
            "        <input type=\"file\" id=\"fileUploadForQuestion_" + card.CardId + "\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"displayCardPreviewImage(this, 2560, 2560 , '" + card.CardId + "');\">" +
            "    </label>";
            if (card.CardImages.length > 0) {
                html = html +
                "    <div class=\"cardContentImage cardContentImageWrapper " + card.CardId + "\" style=\"visibility: visible; height: auto; overflow: auto; margin-top:-23px;\">" +
                "        <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.CardId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>" +
                "        <img class=\"questionimage cardContentImage cardimage " + card.CardId + "\" src=\"" + card.CardImages[0].ImageUrl + "\">" +
                    "    </div>";
            } else {
                html = html +
                "    <div class=\"cardContentImage cardContentImageWrapper " + card.CardId + "\" style=\"visibility: hidden; height: 0px; overflow: auto; margin-top:-23px;\">" +
                    "   <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.CardId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>" +
                    "   <img class=\"questionimage cardContentImage cardimage " + card.CardId + "\" src=\"\">" +
                    "</div>";
            }

            html = html + "</div>" +
            "                 </div>" +
            "                            </div>" +
            "                            <div class=\"form__row form__row--option options-container\"></div>" +
            "                        </div>" +
            "                    </div>" +
            "                </div>" +
            "            </div>" +
            "            <div class=\"add-question__info__setup__preview\">" +

            "               <!-- Card Preview Format -->" +
            "               <div class=\"survey-preview\" style=\"background-image: url(/Img/bg_survey_1a_big.png); position:relative; \">" +
            "                   <div>" +
            "                       <!-- For Note -->" +
            "                       <img class=\"right\" src=\"/Img/icon_note_small.png\" />" +
            "                   </div>" +
            "                   <div class=\"preview-question\"> " +
            "                       <!-- For CardContentText, CardContentImgs  -->" +
            "                       <span class=\"preview-question-content\" style=\"color: black;\">" + card.Content + "</span>" +
            "                       <img class=\"preview-question-image " + card.CardId + "\" src=\"" + ((card.CardImages.length < 1 || card.CardImages[0].ImageUrl.length < 1) ? '' : card.CardImages[0].ImageUrl) + "\" />" +
            "                   </div>" +
            "                   <div>" +
            "                       <div class=\"divEditCardType1PreviewLayout\">Type 1</div>" +
            "                       <div class=\"divEditCardType2PreviewLayout\" style=\"height: 0px;\">Type 2</div>" +
            "                       <div class=\"divEditCardType3PreviewLayout\" style=\"height: 0px;\">" +
            "                           <div class=\"preview-textarea\">" +
            "                               <div class=\"preview-textarea-content\">Answer area</div>" +
            "                           </div>" +
            "                       </div>" +
            "                       <div class=\"divEditCardType4PreviewLayout\" style=\"height: 0px; overflow: auto;\">" +
            "                           <div class=\"preview-numberrange\">" +
            "                               <div>" +
            "                                   <input type=\"text\" id=\"range\" value=\"\" name=\"range\" />" +
            "                               </div>" +
            "                           </div>" +
            "                       </div>" +
            "                       <div class=\"divEditCardType5PreviewLayout\" style=\"height: 0px;\ \">" +
            "                           <select class=\"preview-droplist\">" +
            "                               <option>Please Select</option>" +
            "                           </select>" +
            "                       </div>" +
            "                       <div class=\"divEditCardType6PreviewLayout\" style=\"height: 0px;\"></div>" +
            "                   </div>" +
            "                   <!-- For PageBreak, IsAllowSkipped -->";
            if (card.ToBeSkipped) {
                html = html + " <div class=\"preview-footer-content no-page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none;\"><span class=\"center\">No page break</span></div>" +
            "                   <div class=\"preview-footer-content page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none;\"><span class=\"center\">Next</span></div>" +
            "                   <div class=\"preview-footer-content skip\" style=\"color: rgba(150, 176, 216, 1); position:absolute; bottom: 0;\"><span class=\"right\">Skip</span></div>";
            }
            else {
                if (card.HasPageBreak) {
                    html = html + "                   <div class=\"preview-footer-content no-page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none; \"><span class=\"center\">No page break</span></div>" +
            "                   <div class=\"preview-footer-content page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0;\"><span class=\"center\">Next</span></div>" +
            "                   <div class=\"preview-footer-content skip\" style=\"color: rgba(150, 176, 216, 1); position:absolute; bottom: 0; display: none;\"><span class=\"right\">Skip</span></div>";
                }
                else {
                    html = html + "                   <div class=\"preview-footer-content no-page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0;\"><span class=\"center\">No page break</span></div>" +
                    "                   <div class=\"preview-footer-content page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none;\"><span class=\"center\">Next</span></div>" +
                    "                   <div class=\"preview-footer-content skip\" style=\"color: rgba(150, 176, 216, 1); position:absolute; bottom: 0; display: none;\"><span class=\"right\">Skip</span></div>";
                }
            }
            html = html + "               </div>" +
            "               <!-- / Card Preview Format -->" +

            "            </div>" +
            "            <div class=\"cardSettingSection add-question__info__setup__choice\">" +
            "                <div class=\"cardSetting cardBehaviour\">" +
            "                    <label style=\"font-weight: bold\">Behaviour</label>" +
            "                    <span class=\"setting-checkbox\"><input class=\"setting-ToBeSkipped\" type=\"checkbox\" onclick=\"selectSkipOrPagebreak('" + card.CardId + "');\" " + disabledAttr + " ><label>Allow question to be skipped</label></span>" +
            "                    <div class=\"clear-float\"></div>" +
            "                    <div>" +
            "                        <span class=\"setting-checkbox\"><input class=\"setting-HasCustomAnswer\" type=\"checkbox\" onchange=\"updatePreview('" + card.CardId + "');\" onclick=\" this.checked? $(\'div.card." + card.CardId + " .cardSettingSection .cardSetting div.setting-CustomAnswer').show() : $(\'div.card." + card.CardId + " .cardSettingSection .cardSetting div.setting-CustomAnswer').hide() \" " + disabledAttr + " /><label>Tick 'other' and type your own answer</label></span>" +
            "                    </div>" +
            "                    <div class=\"clear-float\"></div>" +
            "                    <div class=\"setting-CustomAnswer\" style=\"height: auto; overflow: auto;\">" +
            "                        Custom answer" +
            "                        <input class=\"setting-CustomAnswer\" type=\"text\" onkeyup=\"updatePreview('" + card.CardId + "');\" placeholder=\"Please specify (Default)\" " + disabledAttr + ">" +
            "                    </div>" +
            "                    <div class=\"clear-float\"></div>" +
            "                    <div style=\"height: 0px; overflow: auto; visibility: hidden;\">" +
            "                        <span class=\"setting-checkbox\"><input class=\"setting-IsOptionRandomized\" type=\"checkbox\" " + disabledAttr + " /><label>Randomize option order</label></span>" +
            "                    </div>" +
            "                    <div class=\"clear-float\"></div>" +
            "" +
            "                </div><br />" +
            "                <div class=\"cardSetting cardMultiChoice\">" +
            "                    <label style=\"font-weight: bold\">No of options to be selected</label>" +
            "                    Minimum" +
            "                    <input type=\"number\" class=\"setting-MinimumSelection\" value=\"" + card.MiniOptionToSelect + "\" onkeyup=\"setNoOptionsSelect(\'" + card.CardId + "\', \'MiniOptionToSelect\', this);\" /><br>" +
            "                    Maximum" +
            "                    <input type=\"number\" class=\"setting-MaximumSelection\" value=\"" + card.MaxOptionToSelect + "\" onkeyup=\"setNoOptionsSelect(\'" + card.CardId + "\', \'MaxOptionToSelect\', this);\" />" +
            "                    <br>" +
            "                </div><br />" +
            "                <div class=\"cardSetting displaySection title\">" +
            "                    <label style=\"font-weight: bold\">Display</label>" +
            "                    <div>" +
            "                        Background" +
            "                        <!-- Background -->" +
            "                        <input class=\"setting-BackgroundType\" type=\"hidden\">" +
            "                        <div class=\"imageslider\" style=\"margin: 10px auto auto; width: 85%;\">" +
            "                           <div id=\"slider\">" +
            "                           <!-- Slider Setup -->" +
            "                                <input id=\"bgSlider1" + card.CardId + "\" name=\"bgSlider" + card.CardId + "\" onchange=\"$('div.card." + card.CardId + " .setting-BackgroundType').val(1); $('div.card." + card.CardId + " .survey-preview').css('background-image', 'url(/Img/bg_survey_1a_big.png)'); \" type=\"radio\" class=\"setting-BackgroundType1\" >" +
            "                                <input id=\"bgSlider2" + card.CardId + "\" name=\"bgSlider" + card.CardId + "\" onchange=\"$('div.card." + card.CardId + " .setting-BackgroundType').val(2); $('div.card." + card.CardId + " .survey-preview').css('background-image', 'url(/Img/bg_survey_2a_big.png)'); \" type=\"radio\" class=\"setting-BackgroundType2\" >" +
            "                                <input id=\"bgSlider3" + card.CardId + "\" name=\"bgSlider" + card.CardId + "\" onchange=\"$('div.card." + card.CardId + " .setting-BackgroundType').val(3); $('div.card." + card.CardId + " .survey-preview').css('background-image', 'url(/Img/bg_survey_3a_big.png)'); \" type=\"radio\" class=\"setting-BackgroundType3\">" +
            "                                <!-- / Slider Setup -->" +
            "" +
            "                                <!-- The Slider -->" +
            "                                <div id=\"slides\" class=\"slides\" style=\"margin-top: 0px;\">" +
            "                                    <div id=\"overflow\">" +
            "                                        <div class=\"inner\">" +
            "                                            <article>" +
            "                                                <div class=\"info\"></div>" +
            "                                                <img src=\"/Img/bg_survey_1a_small.png\">" +
            "                                           </article>" +
            "                                            <article>" +
            "                                                <div class=\"info\"></div>" +
            "                                               <img src=\"/Img/bg_survey_2a_small.png\">" +
            "                                            </article>" +
            "                                            <article>" +
            "                                                <div class=\"info\"></div>" +
            "                                                <img src=\"/Img/bg_survey_3a_small.png\">" +
            "                                            </article>" +
            "                                        </div>" +
            "                                   </div>" +
            "                                </div>" +
            "                                <!-- / The Slider -->" +
            "" +
            "                               <!-- Controls and Active Slide Display -->" +
            "                                <div class=\"controls\" id=\"controls\" style=\"top: 38%; display: none;\">" +
            "                                    <label for=\"bgSlider1" + card.CardId + "\"></label>" +
            "                                    <label for=\"bgSlider2" + card.CardId + "\"></label>" +
            "                                    <label for=\"bgSlider3" + card.CardId + "\"></label>" +
            "                                </div>" +
            "                                <!-- / Controls and Active Slide Display -->" +
            "                          </div>" +
            "                        </div>" +
            "                        <!-- Background -->" +
            "                    </div>" +
            "                    <br>" +
            "                </div><br />" +
            "                <div class=\"cardSetting displaySection notes\">" +
            "                    <label style=\"font-weight: bold\">Notes</label>" +
            "                    <input class=\"setting-Notes\" type=\"text\" placeholder=\"Enter notes\" onkeydown=\"return (event.keyCode!=13);\" value=\"" + card.Note + "\" onkeyup=\"$('div.card." + card.CardId + " .survey-preview img.right').prop('title', this.value);\" >" +
            "                    <span style=\"color: #979797\">Notes will be seen by user during the survey via the <img src=\"/Img/icon_note_small.png\"> icon</span>" +
            "                </div><br />" +
            "                <div class=\"cardSetting displaySection pagebreak\">" +
            "                    <span class=\"setting-checkbox\"><input class=\"setting-PageBreak\" type=\"checkbox\" " + disabledAttr + " onclick=\"selectSkipOrPagebreak('" + card.CardId + "');\"><label>Page Break</label></span>" +
            "                    <div class=\"clear-float\"></div>" +
            "                </div>" +
            "                <div class=\"cardSetting displaySection deleteButton\">" +
            "                    <a class=\"btn confirm right\" href=\"javascript:deleteCard('" + card.CardId + "');\">Delete Card</a>" +
            "                </div>" +
            "            </div>" +
            "        </div>" +
            "    </div>";

            $(cardDivSelector).html(""); // Clear contents of the card
            $(cardDivSelector).append(html); // Change the card to 

            // Find card and update the local Json to new card
            var rsTopic;
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === card.CardId) {
                    rsTopic.Cards[idx] = card;
                    $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));
                    break;
                }
            }
            setCardDisplay(card.Type, card.CardId);
        }

        function setNoOptionsSelect(cardId, key, element) {
            var rsTopic, card;
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    if (key == "MaxOptionToSelect") {
                        card.MaxOptionToSelect = element.value;
                    }
                    else if (key == "MiniOptionToSelect") {
                        card.MiniOptionToSelect = element.value;
                    }
                    else {
                        // other 
                    }
                    rsTopic.Cards[idx] = card;
                    $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));
                    break;
                }
            }
        }

        function removeAttachedImg(cardId, optionId) {
            var imgElement, imgDivElement, imgPreviewElement;

            if (optionId == null) // Remove image of question.
            {
                imgDivElement = $(".cardContentImage." + cardId)[0];
                imgElement = $(".cardimage." + cardId)[0];
                if (imgElement === undefined)
                    return;

                imgElement.src = "";
                imgDivElement.style.visibility = "hidden";
                imgDivElement.style.height = "0px";

                imgPreviewElement = $(".preview-question-image." + cardId)[0];
                imgPreviewElement.src = "";
            }
            else // Remove image of options.
            {

            }
        }

        function displayCardPreviewImage(input, maxWidth, maxHeight, cardId, optionId) {
            var image = new Image();

            if (input.files && input.files[0]) {
                var t = input.files[0].type;
                var s = input.files[0].size / (1024 * 1024);
                if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
                    if (s > 5) {
                        toastr.error('Uploaded photo exceeded 5MB.');
                    } else {

                        var previewElementImgPlaceHolder, previewElementImg, imgPreviewElement;

                        if (optionId === undefined) {
                            previewElementImgPlaceHolder = $("div.card." + cardId + " div.cardContentImage")[0];
                            previewElementImg = $("div.card." + cardId + " img.cardContentImage")[0];
                            imgPreviewElement = $(".preview-question-image." + cardId)[0];
                        } else {
                            previewElementImgPlaceHolder = $("div.card." + cardId + " div.cardContentImage." + optionId)[0];
                            previewElementImg = $("div.card." + cardId + " img.cardContentImage." + optionId)[0];
                            //remove option img
                            imgPreviewElement = $(".preview-option-image." + optionId)[0];
                        }

                        $(previewElementImg).data('extension', t);

                        var fileReader = new FileReader();
                        fileReader.addEventListener("load", function () {
                            previewElementImg.src = fileReader.result;
                            previewElementImgPlaceHolder.style.visibility = "visible";
                            previewElementImgPlaceHolder.style.height = "auto";
                            //previewElementImgPlaceHolder.style.

                            if (imgPreviewElement)
                                imgPreviewElement.src = fileReader.result;

                            var image = new Image();
                            image.src = this.result;
                            $(previewElementImg).data('width', image.width);
                            $(previewElementImg).data('height', image.height);

                        }, false);
                        fileReader.readAsDataURL(input.files[0]);
                    }
                } else {
                    toastr.error('File extension is invalid.');
                }
            }
        } // end function displayCardPreviewImage(input, maxWidth, maxHeight) {

        function fetchCardFromWebAPI(cardId) {
            var ajaxData,
                cardPromise, cardLogicPromise;

            ajaxData = { // data format to select specific card
                'Action': 'GET',
                'TopicId': $("#main_content_hfTopicId").val(),
                'CategoryId': $("#main_content_hfCategoryId").val(),
                'CardId': cardId,
                'AdminUserId': $("#main_content_hfAdminUserId").val(),
                'CompanyId': $("#main_content_hfCompanyId").val()
            };

            cardPromise = jQuery.ajax({
                type: "POST",
                url: "/api/RSCard",
                data: JSON.stringify(ajaxData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        // updateListCard(d.Card);
                    } else {
                        ReloadErrorToast();
                        toastr.error("Operation failed. " + d.ErrorMessage);
                        return false;
                    }
                },
                error: function (xhr, status, error) {
                    ReloadErrorToast();
                    toastr.error("Error connecting to server.");
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete

                }
            });

            cardLogicPromise = jQuery.ajax({
                type: "POST",
                url: "/api/RSLogic",
                data: JSON.stringify(ajaxData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (d, status, xhr) {

                    if (d.Success) {

                    } else {
                        ReloadErrorToast();
                        toastr.error("Operation failed. " + d.ErrorMessage);
                        return false;
                    }
                },
                error: function (xhr, status, error) {
                    ReloadErrorToast();
                    toastr.error("Error connecting to server.");
                },
                complete: function (xhr, status) {
                    // Do any task that you want to do after the ajax call is complete
                }
            });

            jQuery.when(cardPromise, cardLogicPromise).done(function (cardResult, cardLogicResult) {
                if (cardResult[0].Success && cardLogicResult[0].Success) { // Update card if both result is successful
                    var card = cardResult[0].Card;
                    card["LogicCards"] = cardLogicResult[0].Cards;
                    updateListCard(card);
                } else { // Else display error message
                    if (cardResult[0].Success == false) {
                        ReloadErrorToast();
                        toastr.error("Error fetching card details. " + cardResult[0].ErrorMessage);
                    } else if (cardLogicResult[0].Success == false) {
                        ReloadErrorToast();
                        toastr.error("Error fetching card logic. " + cardLogicResult[0].ErrorMessage);
                    }
                }

                HideProgressBar();
            });
        }

        function updateCard(cardId) {
            var ajaxData, cardDivSelector,
                imgSelector, $img, imgJson,
                card, idx, optionIdx, tempOptions,
                hasError;
            hasError = false;
            cardDivSelector = "div.card." + cardId;

            // Find and update card
            var rsTopic;
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    break;
                }
            }

            var cardSelectedMode, $cardOptions, cardOptionIdx, tempIdx;
            var $selectedCardType = $(cardDivSelector + " ul.tabs__list .tabs__list__item.selected-card-type");

            /***********************/
            // Do update of card depending on ui. 
            // validation and set new value.
            if (card == null) {
                errorToReload("Invalid Question, refreshing page...", true);
                return false;
            }

            /*** 1. Card type ***/
            //  Figure which mode of editing is this
            if ($selectedCardType.hasClass("cardtype1")) card.Type = 1;
            if ($selectedCardType.hasClass("cardtype2")) card.Type = 2;
            if ($selectedCardType.hasClass("cardtype3")) card.Type = 3;
            if ($selectedCardType.hasClass("cardtype4")) card.Type = 4;
            if ($selectedCardType.hasClass("cardtype5")) card.Type = 5;
            if ($selectedCardType.hasClass("cardtype6")) card.Type = 6;

            if (card.Type == null || card.Type < 1 || card.Type > 6) {
                errorToReload("Invalid Question, refreshing page...", true);
                return false;
            }

            /*** 2. Card content text & images ***/
            //  Update question text to card
            card.Content = $(cardDivSelector + " textarea.tbNewCardContent").val().trim();
            if (card.Content.length == 0) {
                errorToReload("Please type in a question.", false);
                return false;
            }
            if (card.Content.length > 250) {
                errorToReload("Question length is too long.", false);
                return false;
            }

            // reset all the images on the card's question and options
            card.HasImage = false;
            card.CardImages = [];
            // Update Card (question) images
            $img = $(cardDivSelector + " img.questionimage.cardContentImage:visible");
            if (($img.length > 0) && ($img.attr("src").length > 0)) {
                card.HasImage = true;
                card.CardImages.push({
                    ImageUrl: JSON.stringify({
                        width: $img.data("width"),
                        height: $img.data("height"),
                        base64: $img.prop("src"),
                        extension: $img.data("extension")
                    })
                });
            }

            /*** 3. Background, Note and PageBreak ***/
            card.BackgroundType = parseInt($(cardDivSelector + " .setting-BackgroundType").val(), 10);
            card.Note = $(cardDivSelector + " .setting-Notes").val();
            card.HasPageBreak = $(cardDivSelector + " .setting-PageBreak").prop("checked");

            /*** 4. Options & Behaviour ***/
            if (card.Type == 1 || card.Type == 2) // 1 = select-one, 2 = multi-choice
            {
                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = $(cardDivSelector + " .setting-HasCustomAnswer").prop("checked");
                card.CustomAnswerInstruction = $(cardDivSelector + " input.setting-CustomAnswer").val().trim();
                if (card.CustomAnswerInstruction.length > 20) {
                    errorToReload("Other's custom answer is too long.", false);
                    return false;
                }
                card.IsOptionRandomized = $(cardDivSelector + " .setting-IsOptionRandomized").prop("checked");

                // No of options to be selected
                if (card.Type == 1) {
                    card.MaxOptionToSelect = 0;
                    card.MiniOptionToSelect = 0;
                }
                else {
                    card.MaxOptionToSelect = $(cardDivSelector + " .setting-MaximumSelection").val();
                    card.MiniOptionToSelect = $(cardDivSelector + " .setting-MinimumSelection").val();

                    //if (card.MaxOptionToSelect < 1) {
                    //    errorToReload("Minimum selection should be at least 1.", false);
                    //    return false;
                    //}

                    if (card.MiniOptionToSelect < 1) {
                        errorToReload("Minimum selection should be at least 1.", false);
                        return false;
                    }

                    if (card.MaxOptionToSelect <= card.MiniOptionToSelect) {
                        errorToReload("Maximum selection value should not be smaller than Minimum selection value.", false);
                        return false;
                    }
                }

                // Options
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";

                var optionId, option = new Object();
                $cardOptions = $(cardDivSelector + " textarea.option-content");
                if ($cardOptions.length == 0) {
                    errorToReload("Please add an option.", false);
                    return false;
                }
                else {

                    for (optionIdx = 0; optionIdx < $cardOptions.length; optionIdx++) {
                        option = null;
                        optionId = $($cardOptions[optionIdx]).data("optionid");

                        // find the option on card
                        for (idx = 0; idx < card.Options.length; idx++) {
                            if (card.Options[idx].OptionId == optionId) {
                                option = card.Options[idx];
                                break;
                            }
                        }

                        if (optionId.startsWith("__")) {
                            option.OptionId = "";
                        }

                        // Update text
                        option.Content = $($cardOptions[optionIdx]).val();
                        if (option.Content.length == 0) {
                            errorToReload("Please fill in your option.", false);
                            return false;
                        }
                        if (option.Content.length > 150) {
                            errorToReload("Option length is too long.", false);
                            return false;
                        }

                        // Update image
                        $img = $(cardDivSelector + " img.optionimage.cardContentImage." + optionId + ":visible");

                        if (($img.length > 0) && ($img.prop("src").length > 0)) {
                            option.HasImage = true;
                            option.Images = []; // Zero image array first
                            option.Images.push({
                                ImageUrl: JSON.stringify({
                                    width: $img.data("width"),
                                    height: $img.data("height"),
                                    base64: $img.prop("src"),
                                    extension: $img.data("extension")
                                })
                            });
                        } else {
                            option.HasImage = false;
                        }

                        // Update logic
                        if (card.Type == 1) {
                            if (($("input#cbCardOptionLogic_" + optionId).prop("checked")) &&
                                ($(cardDivSelector + " .logic-content." + optionId + " .selectedLogic").length > 0)) {
                                option.NextCardId = $(cardDivSelector + " .logic-content." + optionId + " .selectedLogic").data("optionid");
                            } else {
                                option.NextCardId = "";
                            }
                        }
                        else {
                            option.NextCardId = "";
                        }

                        option.Ordering = optionIdx + 1;
                        card.Options[optionIdx] = option;
                    }

                    if (card.Type == 2) {
                        if (card.MiniOptionToSelect == null || card.MiniOptionToSelect == 0) {
                            errorToReload("Your minimum selectable value is invaild.", false);
                            return false;
                        }

                        if (card.MiniOptionToSelect > card.Options.length) {
                            errorToReload("Your minimum selectable value is invaild.", false);
                            return false;
                        }

                        if (card.MaxOptionToSelect > card.Options.length) {
                            errorToReload("Your maximum selectable value is invaild.", false);
                            return false;
                        }

                        if ((card.MaxOptionToSelect <= card.MinOptionToSelect)) {
                            errorToReload("Maximum selectable value must be bigger than minimum selectable value.", false);
                            return false;
                        }
                    }
                }
            }
            else if (card.Type == 3) // 3 = Text
            {
                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.Options = [];
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";
            }
            else if (card.Type == 4) // 4 = number-range
            {
                var numberRangeSelection = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .option_section :checked").val()
                if (numberRangeSelection == 2) // Slider start from Middle (Middle:2)
                {
                    card.StartRangePosition = 2;
                    card.MidRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMiddle").val();
                    card.MidRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMiddleLabel").val();
                    if (card.MidRangeLabel.length == 0) {
                        errorToReload("Please give the middle value a label", false);
                        return false;
                    }
                    if (card.MidRangeLabel.length > 25) {
                        errorToReload("The Middle label is too long", false);
                        return false;
                    }
                    card.MaxRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMax").val();
                    card.MaxRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMaxLabel").val();
                    if (card.MaxRangeLabel.length == 0) {
                        errorToReload("Please give the maximum value a label", false);
                        return false;
                    }
                    if (card.MaxRangeLabel.length > 25) {
                        errorToReload("Maximum label is too long", false);
                        return false;
                    }
                    card.MinRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .lblMinimun").text();
                    card.MinRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMinLabel").val();
                    if (card.MinRangeLabel.length == 0) {
                        errorToReload("Please give the minimum value a label", false);
                        return false;
                    }
                    if (card.MinRangeLabel.length > 25) {
                        errorToReload("Minimum label is too long", false);
                        return false;
                    }
                }
                else  // Slider start from Minimum/Maximum (Minimum:1, Maximum:3)
                {
                    numberRangeSelection = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .maxminSelect").val();
                    card.StartRangePosition = numberRangeSelection;
                    card.MidRange = 5;
                    card.MidRangeLabel = "";
                    card.MaxRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMax").val();
                    card.MaxRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMaxLabel").val();
                    if (card.MaxRangeLabel.length > 25) {
                        errorToReload("Maximum label is too long", false);
                        return false;
                    }
                    card.MinRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMin").val();
                    card.MinRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMinLabel").val();
                    if (card.MinRangeLabel.length > 25) {
                        errorToReload("Minimum label is too long", false);
                        return false;
                    }
                }

                if ((numberRangeSelection == 1) || (numberRangeSelection == 3)) {
                    if (isNaN(parseInt(card.MaxRange, 10))) {
                        errorToReload("Please enter an integer for Maximum value.", false);
                        return false;
                    }

                    if (isNaN(parseInt(card.MinRange, 10))) {
                        errorToReload("Please enter an integer for Minimum value.", false);
                        return false;
                    }

                    if (parseInt(card.MinRange, 10) >= parseInt(card.MaxRange, 10)) {
                        errorToReload("Your Minimum value and Maximum value does not tally", false);
                        return false;
                    }
                }
                if (numberRangeSelection == 2) {
                    if (isNaN(parseInt(card.MidRange, 10))) {
                        errorToReload("Please enter an integer for Middle value.", false);
                        return false;
                    }

                    if (isNaN(parseInt(card.MaxRange, 10))) {
                        errorToReload("Please enter an integer for Maximum value.", false);
                        return false;
                    }

                    if ((parseInt(card.MinRange, 10) >= parseInt(card.MaxRange, 10)) || (parseInt(card.MinRange, 10) >= parseInt(card.MidRange, 10))) {
                        errorToReload("Your Minimum value and Maximum value does not tally", false);
                        return false;
                    }
                }

                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.Options = [];
            }
            else if (card.Type == 5) // 5 = drop-list
            {
                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";

                if (card.Options == null) {
                    card.Options = [];
                }

                tempOptions = $(cardDivSelector + " textarea.droplist_options").val().trim().replace(/\n\n/g, "\n").split(/\n/);
                for (idx = 0; idx < tempOptions.length; idx++) {
                    if (tempOptions[idx] == "" || tempOptions[idx].length == 0) {
                        errorToReload("Please fill in your option", false);
                        return false;
                    }
                    else if (tempOptions[idx].length > 150) {
                        errorToReload("Option length is too long", false);
                        return false;
                    }
                    else {
                        var option = new Object();
                        option.Content = tempOptions[idx];
                        option.HasImage = false;
                        option.Ordering = idx + 1;
                        card.Options[idx] = option;
                    }
                }
            }
            else if (card.Type == 6) // 6 = Instructions
            {
                // Behaviour
                card.ToBeSkipped = false;
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.Options = [];
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";
            }
            else // Other mode 
            {

            }
            /***********************/

            // Update local Json store
            rsTopic.Cards[idx] = card;
            $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));
            $("div#cardList div.card." + cardId).removeClass("opencard");

            ajaxData = { // data format to select specific card
                'Action': 'UPDATE',
                'TopicId': $("#main_content_hfTopicId").val(),
                'CategoryId': $("#main_content_hfCategoryId").val(),
                'CardId': cardId,
                'AdminUserId': $("#main_content_hfAdminUserId").val(),
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'UserData': JSON.stringify(card)
            };

            jQuery.ajax({
                type: "POST",
                url: "/api/RSCardUpdate",
                data: JSON.stringify(ajaxData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        toastr.info("Update Successful");
                        fetchAndRefreshListDisplay();
                    } else {
                        errorToReload("Update unsuccessful. " + d.ErrorMessage, true);
                        return false;
                    }
                },
                error: function (xhr, status, error) {
                    errorToReload("Connection Error, Refreshing page...", true);
                    return false;
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });

            if ((rsTopic.Cards[idx].CardImages.length > 0) &&
                (card.CardImages[0].ImageUrl.slice(0, 4).toLowerCase() !== "http")) {
                rsTopic.Cards[idx].CardImages[0].ImageUrl = JSON.parse(rsTopic.Cards[idx].CardImages[0].ImageUrl).base64;
                $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));
            }
        }

        function getMiniCardMarkup(card, mode) {
            var html = "";
            html = html + "    <div class=\"grid-question-number\">";
            html = html + "        <div>";
            html = html + "            <div style=\"color:#ddd;\">";
            html = html + "                <div class=\"number\" style=\"font-size:1.8rem;font-weight:bold;margin-bottom:5px;\">P" + card.Paging + " | Q" + card.Ordering + "</div>";
            html = html + "                <div class=\"\">Code: " + card.CardId + "</div>";
            html = html + "            </div>";
            html = html + "        </div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question\">";
            html = html + "        <label style=\"color:#ddd;\"></label>";
            html = html + "        <div class=\"image-placeholder\">";
            if ((card.HasImage) && (card.CardImages.length > 0))
                html = html + "        <img src=\"" + card.CardImages[0].ImageUrl + "\"/>";
            html = html + "        </div>";
            html = html + "        <div class=\"grid__span--6\">";
            html = html + "             <label style=\"color:#ccc;font-weight:bold;\">Question</label>";
            html = html + "             <div>" + card.Content + "</div>";
            html = html + "        </div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question-icon\">";
            html = html + "        <label style=\"color:#ccc;font-weight:bold;\">Type</label><div>";

            //html = html + "        <img src=\"http://placehold.it/60x60\" title=\"Type\"/>";

            if (card.Type == 1) { // select one icon
                html = html + "<img src=\"/Img/icon_selectone.png\" title=\"Select one\"/>";
            } else if (card.Type == 2) { // multi=choice icon
                html = html + "<img src=\"/Img/icon_multichoice.png\" title=\"Multi choice\"/>";
            } else if (card.Type == 3) { // text icon
                html = html + "<img src=\"/Img/icon_text.png\" title=\"Text\"/>";
            } else if (card.Type == 4) { // range icon
                html = html + "<img src=\"/Img/icon_numberrange.png\" title=\"Number range\"/>";
            } else if (card.Type == 5) { // drop list icon
                html = html + "<img src=\"/Img/icon_droplist.png\" title=\"Dropdown list\"/>";
            } else if (card.Type == 6) { // instruction icon
                html = html + "<img src=\"/Img/icon_instructions.png\" title=\"Instruction\"/>";
            }

            html = html + "    </div></div>";
            html = html + "    <div class=\"grid-question-icon\">";
            html = html + "        <label style=\"color:#ccc;font-weight:bold;\">Logic</label>";
            html = html + "        <div>";
            if (card.HasFromLogic)
                html = html + "<img src=\"/Img/icon_logic_destination.png\" title=\"Destination\"/>";
            if (card.HasToLogic)
                html = html + "<img src=\"/Img/icon_logic_redirect.png\" title=\"Source\"/>";
            else
                html = html + "<span></span>";
            html = html + "        </div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question-icon\">";
            html = html + "        <label style=\"color:#ccc;font-weight:bold;visibility:hidden;\">Analytics</label>";
            html = html + "        <div><a href=\"/Survey/Analytic/" + $("#main_content_hfTopicId").val() + "/" + $("#main_content_hfCategoryId").val() + "\"><img src=\"/Img/icon_result.png\" title=\"Analytics\"/></a></div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question-expandable\">";
            html = html + "        <a href=\"javascript:fetchCardFromWebAPI('" + card.CardId + "');\"><i class=\"fa fa-caret-square-o-down fa-lg icon\"></i></a>";
            html = html + "    </div>";
            //html = html + "</div>";

            if (mode === "contentOnly")
                return html;

            //return "<div class=\"card cansort " + card.CardId + "\" data-cardid=\"" + card.CardId + "\">" + html + "</div>";

            if (card.HasFromLogic || card.HasToLogic || card.HasPageBreak) {
                if (card.HasPageBreak)
                    return "<div class=\"card " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div> <div class=\"pagebreak " + card.CardId + "\"><hr style=\"clear:both;border-top:#9C3E1E 4px solid;\" /></div>";
                else
                    return "<div class=\"card " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div>";
            } else {
                if (card.HasPageBreak)
                    return "<div class=\"card cansort " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div> <div class=\"pagebreak " + card.CardId + "\"><hr style=\"clear:both;border-top:#9C3E1E 4px solid;\" /></div>";
                else
                    return "<div class=\"card cansort " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div>";
            }

        }

        function displayMiniCard(cardId) {
            // Find the card with cardId
            var rsTopic, idx, cardDivSelector, html;

            cardDivSelector = "div.card." + cardId;

            if ($(cardDivSelector).hasClass("opencard"))
                $(cardDivSelector).removeClass("opencard");

            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    html = getMiniCardMarkup(rsTopic.Cards[idx], "contentOnly");
                    $(cardDivSelector).html(""); // Clear contents of the card
                    $(cardDivSelector).append(html); // Change the card to 
                    break;
                }
            }
        }

        function updateNumberRangeCounter(textarea, cardId, maxCount) {
            $(textarea).siblings(".survey-lettercount").text((maxCount - textarea.value.length).toString());

            if ($(textarea).hasClass("tbNewCardOptionRangeMinLabel")) {
                if ($(textarea).val().trim().length > 0) {
                    $(".preview-numberrange .irs-min").text($(textarea).val());
                } else {
                    $(".preview-numberrange .irs-min").text($(textarea).siblings(".tbNewCardOptionRangeMin.nonmiddle").val());
                }
            }

            if ($(textarea).hasClass("tbNewCardOptionRangeMaxLabel")) {
                if ($(textarea).val().trim().length > 0) {
                    $(".preview-numberrange .irs-max").text($(textarea).val());
                } else {
                    $(".preview-numberrange .irs-max").text($(textarea).siblings(".tbNewCardOptionRangeMax.nonmiddle").val());
                }
            }
        }

        function updateOptionTextCount(textarea, maxCount) {
            // Find label
            var $textarea = $(textarea);
            var optionid = $textarea.data("optionid");
            var cardid = $textarea.data("cardid");
            var label;// = $(".survey-lettercount." + optionid)[0];

            if (optionid === "")
                label = $textarea.siblings(".survey-lettercount")[0];
            else
                label = $(".survey-lettercount." + optionid)[0];

            var countLength = maxCount - $textarea.val().length;

            if (countLength < 0) {
                textarea.style.borderColor = "red";
            } else {
                textarea.style.borderColor = "";
            }

            label.innerHTML = countLength;

            // find card and update option
            var rsTopic, idx, found, cardId, card;
            cardId = $(textarea).data("cardid");
            card = null;
            found = false;
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            if (rsTopic.Cards !== undefined) {
                for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                    if (rsTopic.Cards[idx].CardId == cardId) {
                        card = rsTopic.Cards[idx];
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                for (idx = 0; idx < card.Options.length; idx++) {
                    if (card.Options[idx].OptionId === optionid) {
                        card.Options[idx].Content = textarea.value;
                        break;
                    }
                }
                $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));
            }

            updatePreview(cardid);
        }

        function canChangeCardFormat() {
            var rsTopic;
            var isChangable = true;
            // TODO: Read 3 items: survey start-datetime, end-datetime and survey status
            // Base on the 3 items, determine if this card should be changable.

            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());

            if ((rsTopic.Status !== 1) && (rsTopic.ProgressStatus === 2)) {
                isChangable = false;
            }
            else {
                isChangable = true;
            }


            return isChangable;
        }

        function getNumberRangeMarkup(startType, card) {
            var html = "";

            if (startType == 2) {
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Middle value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMiddle\" class=\"tbNewCardOptionRangeMiddle middle\" type=\"number\" value=\"" + (card.MidRange || card.MidRange === 0 ? card.MidRange : "") + "\" onblur=\"setNumberRangeMinValue('" + card.CardId + "');\" >";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMiddleLabel\" class=\"tbNewCardOptionRangeMiddleLabel\" placeholder=\"Label\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MidRangeLabel ? card.MidRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeMiddleMiddleCount survey-lettercount\">" + (25 - (card.MidRangeLabel ? card.MidRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Average, Neutral, So-so, etc.</label>";
                html = html + "    </div>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Maximum value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMax\" class=\"tbNewCardOptionRangeMax middle\" type=\"number\" value=\"" + (card.MaxRange || card.MaxRange === 0 ? card.MaxRange : "") + "\" onblur=\"setNumberRangeMinValue('" + card.CardId + "');\">";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMaxLabel\" class=\"tbNewCardOptionRangeMaxLabel\" placeholder=\"Label\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MaxRangeLabel ? card.MaxRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeMiddleMaxCount survey-lettercount\">" + (25 - (card.MaxRangeLabel ? card.MaxRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Excellent, Great, Very good, Amazing, etc.</label>";
                html = html + "    </div>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Minimum value</label>";
                html = html + "        <label class=\"lblMinimun\">" + (card.MinRange || card.MinRange === 0 ? card.MinRange : "0") + "</label>";
                html = html + "        ";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMinLabel\" class=\"tbNewCardOptionRangeMinLabel\" placeholder=\"Label\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MinRangeLabel ? card.MinRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeMiddleMinCount survey-lettercount\">" + (25 - (card.MinRangeLabel ? card.MinRangeLabel : '').length) + "</span>";
                html = html + "              ";
                html = html + "        <label class=\"tip\">Eg. Poor, Bad, Need improvement, etc.</label>";
                html = html + "    </div>";
            } else {
                html = html + "    <select class=\"maxminSelect\">";
                html = html + "        <option value=\"1\">Start from Minimum</option>";
                html = html + "        <option value=\"3\">Start from Maximum</option>";
                html = html + "    </select>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Minimum value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMin\" class=\"tbNewCardOptionRangeMin nonmiddle\" type=\"number\" value=\"" + (card.MinRange || card.MinRange === 0 ? card.MinRange : "") + "\">";
                html = html + "        <input name=\"tbNewCardOptionRangeMinLabel\" class=\"tbNewCardOptionRangeMinLabel\" type=\"text\" placeholder=\"Label(Optional)\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "',25);\" value=\"" + (card.MinRangeLabel ? card.MinRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeNonmiddleMinCount survey-lettercount\">" + (25 - (card.MinRangeLabel ? card.MinRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Poor, Bad, Need improvement, etc.</label>";
                html = html + "    </div>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Maximum value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMax\" class=\"tbNewCardOptionRangeMax nonmiddle\" type=\"number\" value=\"" + (card.MaxRange || card.MaxRange === 0 ? card.MaxRange : "") + "\">";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMaxLabel\" class=\"tbNewCardOptionRangeMaxLabel\" placeholder=\"Label(Optional)\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MaxRangeLabel ? card.MaxRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeNonmiddleMaxCount survey-lettercount\">" + (25 - (card.MaxRangeLabel ? card.MaxRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Excellent, Great, Very good, Amazing, etc.</label>";
                html = html + "    </div>";
            }

            return html;
        }

        function setNumberRangeMinValue(cardId) {
            ReloadErrorToast();
            var mid, max, min;
            mid = $("div.card." + cardId + " .tbNewCardOptionRangeMiddle.middle").val();
            max = $("div.card." + cardId + " .tbNewCardOptionRangeMax.middle").val();

            if (mid == "" || isNaN(mid) || parseFloat(mid).toString().indexOf(".") >= 0) {
                toastr.error("Please enter an integer for Middle value.");
                return false;
            }

            mid = parseInt(mid);
            if (mid > 19 || mid < -19) {
                toastr.error("Middle value must be within -19 and 19.");
                return false;
            }

            if (max == "" || isNaN(max) || parseFloat(max).toString().indexOf(".") >= 0) {
                toastr.error("Please enter an integer for Maximum value.");
                return false;
            }

            max = parseInt(max);
            if (max <= mid || max > 20 || max < -18) {
                toastr.error("Maximum value must be within -18 and 20 and must be bigger than Middle value.");
                return false;
            }

            min = mid * 2 - max;
            $("div.card." + cardId + " .lblMinimun").html(min);
        }

        function changeCardRangeType(cardId, newRangeType) {
            var idx, rsTopic, found, html = "";

            found = false;
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    found = true;
                }
            }

            html = getNumberRangeMarkup(newRangeType, card);

            $("div.card." + cardId + " .divCardRangeContainer").html(html);

            if (newRangeType == 2) {
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMiddle").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMiddleLabel").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMax").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMaxLabel").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblMinimun").text("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMinLabel").val("");

                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblNewCardRangeMiddleMiddleCount.survey-lettercount").text("25");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblNewCardRangeMiddleMaxCount.survey-lettercount").text("25");

                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblNewCardRangeMiddleMinCount.survey-lettercount").text("25");
            } else {
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMin").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMinLabel").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMax").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMaxLabel").val("");
            }
        }

        function getCardDisplayMarkup(mode, card) {
            var idx, logicCardIdx, html = "", textarea_val, disabledAttr;

            if (canChangeCardFormat()) {
                disabledAttr = "";
            } else {
                disabledAttr = "disabled";
            }

            /* for preview layout */
            var cardDivSelector;
            cardDivSelector = "div.card." + card.CardId;
            for (var i = 1; i < 7; i++) {
                $(cardDivSelector + " .divEditCardType" + i + "PreviewLayout").css("display", "none");
                $(cardDivSelector + " .divEditCardType" + i + "PreviewLayout").css("height", "0px");
            }

            $(cardDivSelector + " .divEditCardType" + mode + "PreviewLayout").css("display", "block");
            $(cardDivSelector + " .divEditCardType" + mode + "PreviewLayout").css("height", "0px");
            /* for preview layout */

            if ((mode == 1) || (mode == 2)) {  // select one or multi choice
                html = "<span class=\"cardOptionList options-container\">";

                var previewOptionsHtml = "";

                // foreach option add input tag
                if (card.Options.length > 0) {
                    for (idx = 0; idx < card.Options.length; idx++) {
                        html = html + "<div style=\"margin-bottom: 10px\">";

                        if (canChangeCardFormat()) {
                            html = html + "    <a href=\"javascript:void(0);\" class=\"remove-option-link\" onclick=\"removeOptionField('" + card.CardId + "', '" + card.Options[idx].OptionId + "');\">- Remove option</a>";
                        }
                        html = html + "    <textarea class=\"option-content " + card.Options[idx].OptionId + "\" data-optionid=\"" + card.Options[idx].OptionId + "\" data-cardid=\"" + card.CardId + "\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"updateOptionTextCount(this, 150);\" placeholder=\"Option\" rows=\"2\" cols=\"20\">" + card.Options[idx].Content + "</textarea>";
                        html = html + "    <span class=\"survey-lettercount " + card.Options[idx].OptionId + "\">" + (150 - card.Options[idx].Content.length) + "</span>";
                        html = html + "    <label class=\"upload-option-image\" for=\"fuCardOptionContentImg_" + card.Options[idx].OptionId + "\">";
                        html = html + "        <input id=\"fuCardOptionContentImg_" + card.Options[idx].OptionId + "\" type=\"file\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"displayCardPreviewImage(this, 2560, 2560, '" + card.CardId + "', '" + card.Options[idx].OptionId + "');\">";
                        html = html + "    </label>";
                        if (card.Options[idx].Images.length > 0) {
                            html = html + "    <div class=\"divNewCardOption1ContentImg cardContentImage cardContentImageWrapper " + card.Options[idx].OptionId + "\" style=\"visibility: visible; height: auto; overflow: auto; margin-top:-23px;\" class=\"option-image-content\">";
                            html = html + "        <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.Options[idx].OptionId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                            html = html + "        <img class=\"optionimage cardContentImage cardimage " + card.Options[idx].OptionId + "\" src=\"" + card.Options[idx].Images[0].ImageUrl + "\">";
                            html = html + "    </div>";
                        } else {
                            html = html + "    <div class=\"divNewCardOption1ContentImg cardContentImage cardContentImageWrapper " + card.Options[idx].OptionId + "\" style=\"visibility: hidden; height: 0px; overflow: auto; margin-top:-23px;\" class=\"option-image-content\">";
                            html = html + "        <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.Options[idx].OptionId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                            html = html + "        <img class=\"optionimage cardContentImage cardimage " + card.Options[idx].OptionId + "\">";
                            html = html + "    </div>";
                        }

                        if (mode == 1) {
                            html = html + "    <div style=\"margin-left: 50px; \">";
                            html = html + "        <div style=\"width: 60%; float: left;\">";
                            html = html + "            <span class=\"setting-checkbox\">";
                            if (card.Options[idx].NextCardId !== "") {
                                html = html + "                <input id=\"cbCardOptionLogic_" + card.Options[idx].OptionId + "\" class=\"cbNewCardOption1Logic " + card.Options[idx].OptionId + "\" type=\"checkbox\" checked=\"checked\" " + disabledAttr + " />";
                            }
                            else {
                                html = html + "                <input id=\"cbCardOptionLogic_" + card.Options[idx].OptionId + "\" class=\"cbNewCardOption1Logic " + card.Options[idx].OptionId + "\" type=\"checkbox\" " + disabledAttr + " />";
                            }
                            html = html + "                <label for=\"cbCardOptionLogic_" + card.Options[idx].OptionId + "\">If user select this answer skip to</label>";
                            html = html + "            </span>";
                            html = html + "        </div>";
                            html = html + "        <div class=\"logic-dropdown\" style=\"width: 100%; float: right; clear:both;\">";

                            if (canChangeCardFormat()) {
                                html = html + "            <div class=\"logic-button " + card.Options[idx].OptionId + "\" onclick=\"displayLogicContent(this);\" style=\"width:100%; overflow:hidden; margin-top:5px; margin-bottom:10px;\">";
                            }
                            else {
                                html = html + "            <div class=\"logic-button " + card.Options[idx].OptionId + "\"  style=\"width:100%; overflow:hidden; margin-top:5px; margin-bottom:10px;\" >";
                            }

                            if (card.Options[idx].NextCardId !== "") {
                                if (card.Options[idx].NextCardId === "RSEND") {
                                    html = html + "End of survey";
                                } else {
                                    html = html + truncateString(card.Options[idx].NextCard.Content);
                                }
                            }
                            html = html + "                <div style=\"float: right; font-size: xx-small;\">▼</div>";
                            html = html + "            </div>";
                            html = html + "            <div class=\"logic-content " + card.Options[idx].OptionId + "\" style=\"display: none; z-index: 9;\">";
                            html = html + "                <!-- Call javascript after click and substring-->";
                            // i should find card["LogicCards"] here
                            if (card.hasOwnProperty("LogicCards") && card["LogicCards"].length) {
                                for (logicCardIdx = 0; logicCardIdx < card["LogicCards"].length; logicCardIdx++) {
                                    if (card.Options[idx].NextCardId == card["LogicCards"][logicCardIdx].CardId)
                                        html = html + "                <a href=\"javascript:void(0);\" data-optionid=\"" + card["LogicCards"][logicCardIdx].CardId + "\" onclick=\"selectLogic(this);\" class=\"selectedLogic\">" + truncateString(card["LogicCards"][logicCardIdx].Content) + "</a>";
                                    else
                                        html = html + "                <a href=\"javascript:void(0);\" data-optionid=\"" + card["LogicCards"][logicCardIdx].CardId + "\" onclick=\"selectLogic(this);\">" + truncateString(card["LogicCards"][logicCardIdx].Content) + "</a>";
                                }
                            }
                            html = html + "            </div>";
                            html = html + "        </div>";
                            html = html + "        <div class=\"clear-float\"></div>";
                            html = html + "    </div>";
                        }

                        html = html + "</div>";

                        /* for preview layout */
                        if (card.Options != null && card.Options.length > 0) {
                            //for (var i = 0; i < card.Options.length; i++) {
                            if (mode == 1) {
                                previewOptionsHtml += "<div class=\"preview-option\">";
                                if (card.Options[idx].Images != null && card.Options[idx].Images.length > 0 && card.Options[idx].Images[0].ImageUrl.length > 0) {
                                    previewOptionsHtml += "    <img class=\"preview-option-image " + card.Options[idx].OptionId + "\" src=\"" + card.Options[idx].Images[0].ImageUrl + "\" />";
                                }
                                previewOptionsHtml += "   <div class=\"preview-option-content\">" + card.Options[idx].Content + "</div>";
                                previewOptionsHtml += "   <div class=\"clear-float\"></div>";
                                previewOptionsHtml += "</div>";
                            }
                            else if (mode == 2) {
                                previewOptionsHtml += "<div class=\"preview-option-multi\">";
                                previewOptionsHtml += "   <div class=\"preview-option\" style=\"width: 70%; float: left;\">";
                                if (card.Options[idx].Images != null && card.Options[idx].Images.length > 0 && card.Options[idx].Images[0].ImageUrl.length > 0) {
                                    previewOptionsHtml += "       <img class=\"preview-option-image " + card.Options[idx].OptionId + "\" src=\"" + card.Options[idx].Images[0].ImageUrl + "\" />";
                                }
                                previewOptionsHtml += "           <div class=\"preview-option-content\">" + card.Options[idx].Content + "</div>";
                                previewOptionsHtml += "           <div class=\"clear-float\"></div>";
                                previewOptionsHtml += "   </div>";
                                previewOptionsHtml += "   <img class=\"preview-option-tick\" src=\"/Img/icon_multichoice_tick.png\"";
                                if ((card.Options[idx].Images != null && card.Options[idx].Images.length > 0 && card.Options[idx].Images[0].ImageUrl.length > 0) || card.Options[idx].Content.length > 0) {
                                    previewOptionsHtml += " />";
                                }
                                else {
                                    previewOptionsHtml += " style=\"visibility: hidden;\" />";
                                }
                                previewOptionsHtml += "   <div class=\"clear-float\"></div>";
                                previewOptionsHtml += "</div>";
                            }
                            //}
                        }
                        /* for preview layout */
                    }
                }

                if (mode == 1 && card.HasCustomAnswer) {
                    previewOptionsHtml += "<div class=\"preview-others\">";
                    previewOptionsHtml += "   <div class=\"preview-others-content\">Others:</div>";
                    previewOptionsHtml += "   <div class=\"preview-command-content\">" + card.CustomAnswerInstruction + "</div>";
                    previewOptionsHtml += "   <div class=\"clear-float\"></div>";
                    previewOptionsHtml += "</div>";
                }

                $(cardDivSelector + " .divEditCardType" + mode + "PreviewLayout").html(previewOptionsHtml);

                html = html + "</span>";
                if (canChangeCardFormat()) {
                    html = html + "<a style=\"float:right;\" href=\"javascript:\" class=\"add-option-link\" onclick=\"addNewOptionField('" + card.CardId + "')\">+ Add option</a>";
                }
            }
            else if (mode == 3) {
                /* for preview layout */
            }
            else if (mode == 4) {
                /* for preview layout */
                html = "";
                html = html + "<div class=\"cardSetting cardNumberRange\" style=\"visibility: visible; overflow: auto; height: auto;\" class=\"form__row form__row--question\">";
                html = html + "     <label style=\"margin-bottom:15px;\">Number range <br />(Minimum: -20 to Maximum: 20)</label>";
                html = html + "     <div class=\"option_section\">";
                html = html + "         Slider starts from<br />";
                html = html + "         <label class=\"inline-label radio\">";
                html = html + "             <input type=\"radio\" name=\"numberRangeStartFrom_" + card.CardId + "\" value=\"1\"";
                if (!(card.StartRangePosition === 2)) {
                    html = html + "             checked=\"checked\"";
                }
                html = html + "                 onchange=\"changeCardRangeType('" + card.CardId + "', 1); \">Minimum/Maximum";
                html = html + "         </label>";
                html = html + "         <label class=\"inline-label radio\">";
                html = html + "             <input type=\"radio\" name=\"numberRangeStartFrom_" + card.CardId + "\" value=\"2\"";
                if ((card.StartRangePosition === 2)) {
                    html = html + "             checked=\"checked\"";
                }
                html = html + "                 onchange=\"changeCardRangeType('" + card.CardId + "', 2); \">Middle";
                html = html + "         </label>"; html = html + "     </div>";
                html = html + "    <div class=\"divCardRangeContainer\" style=\"display: block;\">";
                html = html + getNumberRangeMarkup(card.StartRangePosition, card);
                html = html + "    </div>";

                html = html + "</div>";

                var rangeIdx, rangeValues;
                rangeValues = [];
                for (rangeIdx = card.MinRange; rangeIdx <= card.MaxRange; rangeIdx++) {
                    rangeValues.push(rangeIdx);
                }

                $(".preview-numberrange #range").ionRangeSlider({
                    min: card.MinRange,
                    max: card.MaxRange,
                    from: card.MinRange,
                    values: rangeValues,
                    onFinish: function (data) {
                        console.log("onFinish");
                        if ((card.MinRangeLabel) && (card.MinRangeLabel.length > 0)) {
                            $(".preview-numberrange .irs-min").text(card.MinRangeLabel);
                        } else {
                            $(".preview-numberrange .irs-min").text(card.MinRange.toString());
                        }

                        if ((card.MaxRangeLabel) && (card.MaxRangeLabel.length > 0)) {
                            $(".preview-numberrange .irs-max").text(card.MaxRangeLabel);
                        } else {
                            $(".preview-numberrange .irs-max").text(card.MaxRange.toString());
                        }
                    }
                });
                if ((card.MinRangeLabel) && (card.MinRangeLabel.length > 0)) {
                    $(".preview-numberrange .irs-min").text(card.MinRangeLabel);
                } else {
                    $(".preview-numberrange .irs-min").text(card.MinRange.toString());
                }

                if ((card.MaxRangeLabel) && (card.MaxRangeLabel.length > 0)) {
                    $(".preview-numberrange .irs-max").text(card.MaxRangeLabel);
                } else {
                    $(".preview-numberrange .irs-max").text(card.MaxRange.toString());
                }

            }
            else if (mode == 5) {
                /* for preview layout */
                textarea_val = "";
                for (idx = 0; idx < card.Options.length; idx++) {
                    if (textarea_val.length > 0)
                        textarea_val = textarea_val + "\n";
                    textarea_val = textarea_val + card.Options[idx].Content;
                }

                html = "";
                html = html + "    <textarea rows=\"16\" class=\"droplist_options\" placeholder=\"Please type in your options here, subsequent options separated by 'Enter' key.\" onkeyup=\"updateSelectPreview(this);\" style=\"overflow-y:auto;\" >" + textarea_val + "</textarea>";
                html = html + "    <label>Please type in your options here, subsequent options separated by 'Enter' key.</label>";

                updateSelectPreview($(html)[0]);

            }
            else if (mode == 6) {
                html = "";

                /* for preview layout */
            }
            else {
                html = "";
            }

            return html;
        }

        function displayLogicContent(btn) {
            $(btn).siblings(".logic-content").toggle();
        }

        function selectLogic(option) {
            $(option).siblings().removeClass("selectedLogic");
            $(option).addClass("selectedLogic");
            $(option).parent().siblings(".logic-button").text($(option).text());
            $(option).parent().hide();
        }

        function truncateString(string) {
            console.log(string.length);
            if (string.length >= 50) {
                return string.substring(0, 40) + "...";
            } else {
                return string;
            }
        }



        function setCardSettingDisplay(mode, card) {
            // hide all
            var cardDivSelector, cardSettingSelector, idx, html;

            // Hide all card settings first
            cardDivSelector = "div.card." + card.CardId;
            cardSettingSelector = cardDivSelector + " .cardSettingSection .cardSetting";
            $(cardSettingSelector).hide();

            // Then display the card settings for the mode
            $(cardSettingSelector + ".cardBehaviour").show();   // display behaviour
            $(".setting-HasCustomAnswer").parent().hide();
            $(".setting-CustomAnswer").hide();

            if (mode == 1) { // select one
                $(".setting-HasCustomAnswer").parent().show();
                $(".setting-CustomAnswer").show();

                if (card.hasOwnProperty("HasCustomAnswer") && card.HasCustomAnswer) {
                    $(cardSettingSelector + " .setting-HasCustomAnswer").prop("checked", card.HasCustomAnswer);
                    if (card.hasOwnProperty("CustomAnswerInstruction"))
                        $(cardSettingSelector + " input.setting-CustomAnswer").val(card.CustomAnswerInstruction);
                    $(cardSettingSelector + " div.setting-CustomAnswer").show();

                } else {
                    $(cardSettingSelector + " .setting-HasCustomAnswer").prop("checked", false);
                    $(cardSettingSelector + " div.setting-CustomAnswer").hide();
                }

                if (card.hasOwnProperty("IsOptionRandomized") && card.IsOptionRandomized)
                    $(cardSettingSelector + " .setting-IsOptionRandomized").prop("checked", card.IsOptionRandomized);
                else
                    $(cardSettingSelector + " .setting-IsOptionRandomized").prop("checked", false);


            } else if (mode == 2) { // multi-choice
                $(cardSettingSelector + ".cardMultiChoice").show(); // display the minimum and maximum
            }
            else if (mode == 6) {
                $(cardSettingSelector + ".cardBehaviour").hide();
            }

            // All must show background, notes and pagebreak
            if (card.hasOwnProperty("BackgroundType")) {
                $(cardSettingSelector + " .setting-BackgroundType").val(card.BackgroundType);

                if (card.BackgroundType == 1) {
                    $(cardSettingSelector + " .setting-BackgroundType1").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_1a_big.png)");
                }
                else if (card.BackgroundType == 2) {
                    $(cardSettingSelector + " .setting-BackgroundType2").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_2a_big.png)");
                }
                else if (card.BackgroundType == 3) {
                    $(cardSettingSelector + " .setting-BackgroundType3").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_3a_big.png)");
                }
                else {
                    // Other situation
                    $(cardSettingSelector + " .setting-BackgroundType1").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_1a_big.png)");
                }
            }
            else {
                $(cardSettingSelector + " .setting-BackgroundType1").prop("checked", true);
                $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_1a_big.png)");
            }

            if (card.hasOwnProperty("ToBeSkipped") && card.ToBeSkipped)
                $(cardSettingSelector + " .setting-ToBeSkipped").prop("checked", card.ToBeSkipped);
            else
                $(cardSettingSelector + " .setting-ToBeSkipped").prop("checked", false);

            $(cardSettingSelector + " .setting-Notes").val(card.Note);
            $(cardDivSelector + " .survey-preview img.right").prop("title", card.Note);

            if (card.hasOwnProperty("HasPageBreak") && card.HasPageBreak)
                $(cardSettingSelector + " .setting-PageBreak").prop("checked", card.HasPageBreak);
            $(cardSettingSelector + ".displaySection").show(); // display notes and page break
        }

        function deleteCard(cardId) {
            ReloadErrorToast();
            var ajaxData;

            ajaxData = { // data format to select specific card
                'Action': 'UPDATE',
                'TopicId': $("#main_content_hfTopicId").val(),
                'CategoryId': $("#main_content_hfCategoryId").val(),
                'CardId': cardId,
                'AdminUserId': $("#main_content_hfAdminUserId").val(),
                'CompanyId': $("#main_content_hfCompanyId").val()
            };

            jQuery.ajax({
                type: "POST",
                url: "/api/RSCardDelete",
                data: JSON.stringify(ajaxData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        //updateListCard(d.Card);
                        // Update the card in local JSON
                        //      Find the card in local JSON
                        //      Remove the card
                        //      Save to local JSON
                        // Remove the card from display
                        var cardIdx, rsTopic, $cards, displayedCardIdx, html, cardDivSelector, found;
                        rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
                        $cards = $("#cardList div.card");
                        if (rsTopic !== null) {
                            for (cardIdx = 0; cardIdx < rsTopic.Cards.length; cardIdx++) {
                                if (rsTopic.Cards[cardIdx].CardId == cardId) {
                                    rsTopic.Cards.splice(cardIdx, 1);
                                    $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));
                                    $("#cardList div.card." + cardId).remove();
                                    fetchAndRefreshListDisplay();
                                    break;
                                }
                            }
                        }
                    } else {
                        toastr.error("Operation failed. " + d.ErrorMessage);
                    }
                },
                error: function (xhr, status, error) {
                    toastr.error("Error connecting to server.");
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });
        }

        function setCardDisplay(mode, cardId) {
            var rsTopic, idx, cardDivSelector, html, card;

            cardDivSelector = "div.card." + cardId;

            // search and find card om json store
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    card.Type = mode;
                    $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));

                    // Get and set html markup for question/answer section
                    html = getCardDisplayMarkup(mode, card);
                    $(cardDivSelector + " div.form__row.form__row--option").children().remove();
                    $(cardDivSelector + " div.form__row.form__row--option").append(html);

                    // Get and set html markup for preview section // comment out for now

                    // Get and set html markup for question settings 
                    setCardSettingDisplay(mode, card);

                    // Clear other buttons and set selected button
                    $(cardDivSelector + " ul.tabs__list .tabs__list__item").removeClass("selected-card-type");
                    $(cardDivSelector + " ul.tabs__list .tabs__list__item.cardtype" + mode).addClass("selected-card-type");
                    break;
                } // 
            } // end-for search card
        }

        function fetchAndRefreshListDisplay() {
            ReloadErrorToast();
            var ajaxData;

            ajaxData = { // data format to select specific card
                'Action': 'REFRESH',
                'TopicId': $("#main_content_hfTopicId").val(),
                'CategoryId': $("#main_content_hfCategoryId").val(),
                'AdminUserId': $("#main_content_hfAdminUserId").val(),
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'CardName': $("#main_content_tbSearchQuestion").val().trim().length <= 0 ? null : $("#main_content_tbSearchQuestion").val().trim()
                //,'UserData': newCardPostion
            };

            jQuery.ajax({
                type: "POST",
                url: "/api/RStopic",
                data: JSON.stringify(ajaxData),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {

                    // Merge fetch data with what is Json store
                    var localRSTopic, remoteCardIdx, localCardIdx, cardFound;
                    localRSTopic = JSON.parse($("#main_content_hfJsonStore").val());

                    // For each card returned by server
                    // Find the corresponding card in hfJsonStore // $("#cardList div.card")
                    // if the card is currently open, update P and Q numbers only
                    // else replace then entire card
                    for (remoteCardIdx = 0; remoteCardIdx < d.Topic.Cards.length; remoteCardIdx++) {
                        cardFound = false;
                        for (localCardIdx = 0; localCardIdx < localRSTopic.Cards.length; localCardIdx++) {
                            if (localRSTopic.Cards[localCardIdx].CardId == d.Topic.Cards[remoteCardIdx].CardId) {
                                // Card found
                                // Check if card is open
                                if ($("div.card." + d.Topic.Cards[remoteCardIdx].CardId).hasClass("opencard")) {
                                    // update card P&Q numbers
                                    localRSTopic.Cards[localCardIdx].Paging = d.Topic.Cards[remoteCardIdx].Paging;
                                    localRSTopic.Cards[localCardIdx].Ordering = d.Topic.Cards[remoteCardIdx].Ordering;
                                } else {
                                    // replace minicard details
                                    localRSTopic.Cards[localCardIdx] = d.Topic.Cards[remoteCardIdx];
                                }
                                break;
                            }
                        }

                        if (cardFound === false) {
                            // new remote card, add to store
                            localRSTopic.Cards.push(d.Topic.Cards[remoteCardIdx]);
                        }
                    }

                    // Store back to hfJsonStore, then refresh list display
                    //$("#main_content_hfJsonStore").val(JSON.stringify(d.Topic));
                    $("#main_content_hfJsonStore").val(JSON.stringify(localRSTopic));
                    refreshListDisplay(d.Topic.Cards);
                },
                error: function (xhr, status, error) {
                    toastr.error("Error connecting to server.");
                },
                complete: function (xhr, status) {
                    // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });
        }

        function refreshListDisplay(cardsToDisplay) {
            var cardIdx, rsTopic, $cards, displayedCardIdx, html, cardDivSelector, found, card;

            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            $cards = $("#cardList div.card");

            if (cardsToDisplay) {
                $("#cardList").children().remove();
                for (cardIdx = 0; cardIdx < cardsToDisplay.length; cardIdx++) {
                    card = cardsToDisplay[cardIdx];
                    html = getMiniCardMarkup(card, card.Type);
                    $("#cardList").append(html);
                }
                HideProgressBar();
                return;
            }

            if (rsTopic !== null) {
                //$("#cardList").children().remove();
                for (cardIdx = 0; cardIdx < rsTopic.Cards.length; cardIdx++) {
                    card = rsTopic.Cards[cardIdx];
                    found = false;
                    // find the card in displayed card list
                    for (displayedCardIdx = 0; displayedCardIdx < $cards.length; displayedCardIdx++) {
                        if ($($cards[displayedCardIdx]).data("cardid") == rsTopic.Cards[cardIdx].CardId) {
                            // Update the contents
                            found = true;
                            html = getMiniCardMarkup(rsTopic.Cards[cardIdx], "contentOnly");
                            cardDivSelector = "div.card." + rsTopic.Cards[cardIdx].CardId;

                            // Check pagebreak
                            if (rsTopic.Cards[cardIdx].HasPageBreak) {
                                if ($("div.pagebreak." + rsTopic.Cards[cardIdx].CardId).length <= 0) {
                                    $("div.card." + rsTopic.Cards[cardIdx].CardId).after("<div class=\"pagebreak " + rsTopic.Cards[cardIdx].CardId + "\"><hr style=\"clear:both;border-top:#9C3E1E 4px solid;\" /></div>");
                                }
                            } else {
                                $("div.pagebreak." + rsTopic.Cards[cardIdx].CardId).remove();
                            }

                            $(cardDivSelector).html(""); // Clear contents of the card
                            $(cardDivSelector).append(html); // Change the card to 
                            break;
                        }
                    }

                    if (!found) {
                        html = getMiniCardMarkup(card, card.Type);
                        $("#cardList").append(html);
                    }
                }
            }

            // Filter #cardList based on cardsToDisplay
            //if (cardsToDisplay) {
            //    var cardListChildren = $("#cardList").children();
            //    var cardListChildrenCount = $("#cardList").children().length;
            //    var childrenIdx, displayCardIdx, displayCardFound;
            //    for (childrenIdx = 0; childrenIdx < cardListChildrenCount; childrenIdx++) {

            //        displayCardFound = false;

            //        for (displayCardIdx = 0; displayCardIdx < cardsToDisplay.length; displayCardIdx++) {
            //            cardsToDisplay[displayCardIdx] == "";
            //        }

            //        if ($("#cardList .card." + cardsToDisplay[0].CardId).length) {

            //        }

            //    }
            //}

            HideProgressBar();
        }

        function updateNewCardNumber(page_number, question_number) {
            $(".add-question__questionnumber .number")[0].innerText = "P" + page_number;
            $(".add-question__questionnumber .number")[1].innerText = "Q" + question_number;
        }

        function createNewOption() {
            var option = {
                AnsweredByUser: null,
                Content: "Yes",
                HasImage: false,
                Images: [],
                IsCustom: false,
                IsSelected: false,
                NextCard: null,
                NextCardId: null,
                NumberOfSelection: 0,
                OptionId: null,
                Ordering: 0,
                PercentageOfSelection: 0,
            };

            return option;
        }

        function addNewOptionField(cardId) {
            var rsTopic, idx, found, card, html;

            card = null;
            found = false;
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            if (rsTopic.Cards !== undefined) {
                for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                    if (rsTopic.Cards[idx].CardId == cardId) {
                        card = rsTopic.Cards[idx];
                        found = true;
                        break;
                    }
                }
            }

            if (found) {

                // Update card options
                var optionIdx, tempOption, $image, imageSrc, $logic, $logicText;
                for (optionIdx in card.Options) { // foreach option in card
                    tempOption = card.Options[optionIdx];

                    // Update card content
                    tempOption.Content = $("div.card." + card.CardId + " .option-content." + tempOption.OptionId).val();

                    // Update card image
                    $image = $(".optionimage.cardContentImage.cardimage." + tempOption.OptionId);
                    imageSrc = $image.attr("src");

                    console.log(imageSrc);
                    if (imageSrc) { // has attached image
                        if (imageSrc.startsWith("data:")) { // handles cases where new image are attached
                            if (tempOption.Images.length <= 0) { // no image in state; add image to state
                                tempOption.Images.push({
                                    ImageUrl: imageSrc
                                });
                                // update card
                            } else { // has image state; update image state
                                tempOption.Images[0].ImageId = "";
                                tempOption.Images[0].ImageUrl = imageSrc;
                            }
                        } // else implies original image since last post
                    } else { // no or removed attached image
                        if (tempOption.Images.length > 0) {
                            tempOption.Images.splice(0, 1);
                        }
                    }

                    // Update card logic
                    $logic = $("div.card." + card.CardId + " .cbNewCardOption1Logic." + tempOption.OptionId);

                    if ($logic.prop("checked")) { // logic is checked
                        tempOption.NextCardId = $("div.card." + card.CardId + " .logic-content." + tempOption.OptionId + " .selectedLogic").data("optionid");
                        var logicCardIdx;
                        for (logicCardIdx = 0; logicCardIdx < card.LogicCards.length; logicCardIdx++) {
                            if (card.LogicCards[logicCardIdx].CardId == tempOption.NextCardId) {
                                tempOption.NextCard = card.LogicCards[logicCardIdx];
                                break;
                            }
                        }
                    } else { // logic is not checked or is unchecked
                        tempOption.NextCardId = "";
                        tempOption.NextCard = null;
                    }
                }

                card.Options.push({
                    AnsweredByUser: null,
                    Content: "",
                    HasImage: false,
                    Images: [],
                    IsCustom: false,
                    IsSelected: false,
                    NextCard: null,
                    NextCardId: "",
                    NumberOfSelection: 0,
                    OptionId: "__" + card.CardId + "_" + (card.Options.length + 1).toString(),
                    Ordering: (card.Options.length + 1),
                    PercentageOfSelection: 0,
                });

                $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));

                setCardDisplay(card.Type, card.CardId);
                updateListCard(card);

            } else {
                // Handling for new card
                var nextOptionNum = ($("#divNewCardOptions").children().length + 1).toString();

                html = "";
                html = html + "<div style=\"margin-bottom: 10px\">";
                html = html + "    <textarea name=\"tbNewCardOption" + nextOptionNum + "\" class=\"option-content\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"textCounter(this,'lblNewCardOption" + nextOptionNum + "ContentCount', 150); setNewCardValue(2, this, " + nextOptionNum + ");\" onblur=\"setNewCardValue(2, this, " + nextOptionNum + ");\" placeholder=\"Option " + nextOptionNum + "\" rows=\"2\" cols=\"20\"></textarea>";
                html = html + "    <span id=\"lblNewCardOption" + nextOptionNum + "ContentCount\" class=\"survey-lettercount\">150</span>";
                html = html + "    <label class=\"upload-option-image\" for=\"fuNewCardOption" + nextOptionNum + "ContentImg\">";
                html = html + "        <input type=\"file\" id=\"fuNewCardOption" + nextOptionNum + "ContentImg\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"previewImage(this, 2560, 2560 ,2, " + nextOptionNum + ");\">";
                html = html + "    </label>";
                html = html + "    <div id=\"divNewCardOption" + nextOptionNum + "ContentImg\" style=\"visibility: hidden; height: 0px; overflow: auto;\" class=\"option-image-content\">";
                html = html + "        <div class=\"survey-cross\" onclick=\"removeContentImg(2, " + nextOptionNum + ");\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                html = html + "        <img id=\"imgNewOption" + nextOptionNum + "ContentImgPreview\">";
                html = html + "    </div>";
                html = html + "</div>";

                $("#divNewCardOptions").append(html);

                if (NewCard) {
                    //NewCard.options[NewCard.options.length + 1] = getOptionObj();
                    NewCard.options.push(getOptionObj());
                }
            }
        }

        function removeOptionField(cardId, optionId) {
            var rsTopic, idx, found, card, html;

            card = null;
            found = false;
            rsTopic = JSON.parse($("#main_content_hfJsonStore").val());
            if (rsTopic.Cards !== undefined) {
                for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                    if (rsTopic.Cards[idx].CardId == cardId) {
                        card = rsTopic.Cards[idx];
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                // Find and splice option 
                for (idx = 0; idx < card.Options.length; idx++) {
                    if (card.Options[idx].OptionId === optionId) {
                        card.Options.splice(idx, 1);
                        break;
                    }
                }

                // Update card options
                var optionIdx, tempOption, $image, imageSrc, $logic, $logicText;
                for (optionIdx in card.Options) { // foreach option in card
                    tempOption = card.Options[optionIdx];

                    // Update card content
                    tempOption.Content = $("div.card." + card.CardId + " .option-content." + tempOption.OptionId).val();

                    // Update card image
                    $image = $(".optionimage.cardContentImage.cardimage." + tempOption.OptionId);
                    imageSrc = $image.attr("src");

                    console.log(imageSrc);
                    if (imageSrc) { // has attached image
                        if (imageSrc.startsWith("data:")) { // handles cases where new image are attached
                            if (tempOption.Images.length <= 0) { // no image in state; add image to state
                                tempOption.Images.push({
                                    ImageUrl: imageSrc
                                });
                                // update card
                            } else { // has image state; update image state
                                tempOption.Images[0].ImageId = "";
                                tempOption.Images[0].ImageUrl = imageSrc;
                            }
                        } // else implies original image since last post
                    } else { // no or removed attached image
                        if (tempOption.Images.length > 0) {
                            tempOption.Images.splice(0, 1);
                        }
                    }

                    // Update card logic
                    $logic = $("div.card." + card.CardId + " .cbNewCardOption1Logic." + tempOption.OptionId);

                    if ($logic.prop("checked")) { // logic is checked
                        tempOption.NextCardId = $("div.card." + card.CardId + " .logic-content." + tempOption.OptionId + " .selectedLogic").data("optionid");
                        var logicCardIdx;
                        for (logicCardIdx = 0; logicCardIdx < card.LogicCards.length; logicCardIdx++) {
                            if (card.LogicCards[logicCardIdx].CardId == tempOption.NextCardId) {
                                tempOption.NextCard = card.LogicCards[logicCardIdx];
                                break;
                            }
                        }
                    } else { // logic is not checked or is unchecked
                        tempOption.NextCardId = "";
                        tempOption.NextCard = null;
                    }
                }


                $("#main_content_hfJsonStore").val(JSON.stringify(rsTopic));

                setCardDisplay(card.Type, card.CardId);
                updateListCard(card);

            } else {
                // Handling for new card

                //var nextOptionNum = ($("#spanNewCardOptions").children().length + 1).toString();

                //html = "";
                //html = html + "<div style=\"margin-bottom: 10px\">";
                //html = html + "    <textarea name=\"tbNewCardOption" + nextOptionNum + "\" class=\"option-content\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"textCounter(this,'lblNewCardOption" + nextOptionNum + "ContentCount', 150); setNewCardValue(2, this, " + nextOptionNum + ");\" onblur=\"setNewCardValue(2, this, " + nextOptionNum + ");\" placeholder=\"Option " + nextOptionNum + "\" rows=\"2\" cols=\"20\"></textarea>";
                //html = html + "    <span id=\"lblNewCardOption" + nextOptionNum + "ContentCount\" class=\"survey-lettercount\">150</span>";
                //html = html + "    <label class=\"upload-option-image\" for=\"fuNewCardOption" + nextOptionNum + "ContentImg\">";
                //html = html + "        <input type=\"file\" id=\"fuNewCardOption" + nextOptionNum + "ContentImg\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"previewImage(this, 2560, 2560 ,2, " + nextOptionNum + ");\">";
                //html = html + "    </label>";
                //html = html + "    <div id=\"divNewCardOption" + nextOptionNum + "ContentImg\" style=\"visibility: hidden; height: 0px; overflow: auto;\" class=\"option-image-content\">";
                //html = html + "        <div class=\"survey-cross\" onclick=\"removeContentImg(2, " + nextOptionNum + ");\">X</div>";
                //html = html + "        <img id=\"imgNewOption" + nextOptionNum + "ContentImgPreview\">";
                //html = html + "    </div>";
                //html = html + "</div>";

                //$("#spanNewCardOptions").append(html);

                //if (NewCard) {
                //    //NewCard.options[NewCard.options.length + 1] = getOptionObj();
                //    NewCard.options.push(getOptionObj());
                //}
            }
        }

        function updatePreview(cardId) {
            var card;
            var rsTopic, idx, html, found, card, cardImage;

            $("div.card." + cardId + " .divEditCardType1PreviewLayout").children().remove();
            $("div.card." + cardId + " .divEditCardType2PreviewLayout").children().remove();

            var $optionContent = $("div.card." + cardId + " .cardOptionList.options-container .option-content");
            for (idx = 0; idx < $optionContent.length; idx++) {
                html = "";
                html = html + "<div class=\"preview-option\">   ";
                cardImage = $($optionContent[idx]).siblings(".cardContentImage").children("img.cardContentImage")[0];
                if (cardImage) {
                    html = html + "<img class=\"preview-option-image " + $($optionContent[idx]).data("optionid") + "\" src=\"" + cardImage.src + "\">";
                }

                html = html + "    <div class=\"preview-option-content\">" + $optionContent[idx].value + "</div>   ";
                html = html + "    <div class=\"clear-float\"></div>";
                html = html + "</div>";
                $("div.card." + cardId + " .divEditCardType1PreviewLayout").append(html);
                $("div.card." + cardId + " .divEditCardType2PreviewLayout").append(html);
            }

            var isTickOtherAnswer = $("div.card." + cardId + " .setting-HasCustomAnswer").prop('checked');
            if (isTickOtherAnswer) {
                html = "";
                html += "<div class=\"preview-others\">";
                html += "   <div class=\"preview-others-content\">Others:</div>";
                html += "   <div class=\"preview-command-content\">" + $("div.card." + cardId + " input.setting-CustomAnswer").val() + "</div>";
                html += "   <div class=\"clear-float\"></div>";
                html += "</div>";
                $("div.card." + cardId + " .divEditCardType1PreviewLayout").append(html);
            }
        }

        $(document).ready(function () {
            "use strict";

            // Read JSON from hiddenfield variable 
            // Render each item in json list 
            var cardIdx;

            if ($("#main_content_hfJsonStore").val() == "")
                return;

            var rsTopic = JSON.parse($("#main_content_hfJsonStore").val());

            if (rsTopic !== null) {
                // foreach card in rsTopic.Cards
                // Display rsTopic.Cards[0]
                for (cardIdx = 0; cardIdx < rsTopic.Cards.length; cardIdx++) {
                    $("#cardList").append(getMiniCardMarkup(rsTopic.Cards[cardIdx]));
                }
            }

            // $($("#cardList div.card.cansort")[13]).removeClass("cansort");

            $("#cardList").sortable({
                items: "> .card.cansort",
                cancel: ".opencard",
                axis: "y",
                stop: function (e, ui) {
                    // on stop send signal, send ajax request updating of card's new position

                    // first find the new position
                    var ajaxData, tempCardIdx, targetCardId, $cardDivs, newCardPostion, prevCardDiv, nextCardDiv;

                    targetCardId = ui.item.data("cardid");
                    $cardDivs = $("#cardList div.card");

                    newCardPostion = 1; // default to 1
                    for (tempCardIdx = 0; $cardDivs.length; tempCardIdx++) {
                        if ($($cardDivs[tempCardIdx]).data("cardid") == targetCardId) {
                            newCardPostion = tempCardIdx + 1;
                            prevCardDiv = $cardDivs[tempCardIdx - 1];
                            nextCardDiv = $cardDivs[tempCardIdx + 1];
                            break;
                        }
                    }

                    // If same position, do nothing
                    if (newCardPostion === $("div.card." + targetCardId).data("ordering"))
                        return;

                    // Do checking; card cannot move outside of page
                    if (prevCardDiv !== undefined) {
                        if ($("div.card." + targetCardId).data("paging") !== $(prevCardDiv).data("paging")) {
                            $("div.card." + $(prevCardDiv).data("cardid"))
                            if ($("div.pagebreak." + $(prevCardDiv).data("cardid")).length <= 0) {
                                ReloadErrorToast();
                                toastr.error("Cannot move to another page.");
                                $("#cardList").sortable("cancel");
                                return;
                            }
                        }
                    }

                    // Send ajax call for re-ordering of cards
                    ajaxData = { // data format to select specific card
                        'Action': 'REORDER',
                        'TopicId': $("#main_content_hfTopicId").val(),
                        'CategoryId': $("#main_content_hfCategoryId").val(),
                        'CardId': targetCardId,
                        'AdminUserId': $("#main_content_hfAdminUserId").val(),
                        'CompanyId': $("#main_content_hfCompanyId").val(),
                        'UserData': newCardPostion
                    };

                    jQuery.ajax({
                        type: "POST",
                        url: "/api/RSCardRearrange",
                        data: JSON.stringify(ajaxData),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function (xhr, settings) {
                            ShowProgressBar();
                        },
                        success: function (d, status, xhr) {
                            if (d.Success) {
                                // if success make another ajax call to fetch list
                                fetchAndRefreshListDisplay();
                            } else {
                                ReloadErrorToast();
                                toastr.error("Operation failed. " + d.ErrorMessage + " Sorting will be reverted.");
                                $("#cardList").sortable("cancel");
                            }
                        },
                        error: function (xhr, status, error) {
                            HideProgressBar();
                            ReloadErrorToast();
                            toastr.error("Error connecting to server.");
                        },
                        complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                        }
                    });
                }
            });
        });

        function errorToReload(errorMsg, isNeedReload) {
            ReloadErrorToast();
            toastr.error(errorMsg);
            HideProgressBar();
            if (isNeedReload) {
                setTimeout(function () {
                    window.location.reload(1);
                }, 2000);
            }
        }
    </script>
</asp:Content>
