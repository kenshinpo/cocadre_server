﻿using AdminWebsite.App_Code.Entity;
using CassandraService.ServiceInterface;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Survey
{
    public class db
    {
        public static List<CassandraService.Entity.SurveyCategory> GetSurveyCategories()
        {
            List<CassandraService.Entity.SurveyCategory> result = new List<CassandraService.Entity.SurveyCategory>();
            result.Add(new CassandraService.Entity.SurveyCategory
            {
                Id = Guid.NewGuid().ToString(),
                ListedDateTime = DateTime.Now.Subtract(new TimeSpan(5, 0, 3, 2)),
                SurveyCount = 9,
                Title = "Personality Test"
            });
            result.Add(new CassandraService.Entity.SurveyCategory
            {
                Id = Guid.NewGuid().ToString(),
                ListedDateTime = DateTime.Now.Subtract(new TimeSpan(15, 3, 3, 2)),
                SurveyCount = 4,
                Title = "Opinions"
            });
            result.Add(new CassandraService.Entity.SurveyCategory
            {
                Id = Guid.NewGuid().ToString(),
                ListedDateTime = DateTime.Now.Subtract(new TimeSpan(52, 0, 3, 2)),
                SurveyCount = 2,
                Title = "Survey about company policy"
            });
            return result;
        }

        public static CassandraService.Entity.Survey GetSurvey(string id)
        {
            CassandraService.Entity.Survey result = new CassandraService.Entity.Survey();

            if (id.Equals("CreateSurvey", StringComparison.InvariantCultureIgnoreCase))
            {
                result.SurveyTitle = "Sample survey";
                result.SurveyIntroduction = "Some survey intro text";
                result.SurveyClosing = "Some survey closing text";
                result.SurveyCategory = "Opinions";
                result.SurveyStatus = CassandraService.Entity.Survey.RSTopicStatus.Active;
            }
            else
            if (id.Equals("MultipleSampleQuestions", StringComparison.InvariantCultureIgnoreCase))
            {
                result.SurveyTitle = "Sample survey";
                result.SurveyIntroduction = "Some survey intro text";
                result.SurveyClosing = "Some survey closing text";
                result.SurveyCategory = "Opinions";
                result.SurveyStatus = CassandraService.Entity.Survey.RSTopicStatus.Active;

                for (int i = 1; i < 10; i++)
                {
                    result.SurveyQuestionList.Add(new CassandraService.Entity.SurveyQuestion
                    {
                        PageNo = "0",
                        QuestionNo = i.ToString(),
                        QuestionText = "some question " + i.ToString(),
                        QuestionStatus = "Active",
                        QuestionType = CassandraService.Entity.Survey.RSQuestionType.SelectOne,
                        CustomActionList = new List<CassandraService.Entity.SurveyQuestionCustomAction>()
                    });
                }
                
            }
            else
            if (id.Equals("AddQuestion", StringComparison.InvariantCultureIgnoreCase))
            {
                result.SurveyTitle = "Sample survey";
                result.SurveyIntroduction = "Some survey intro text";
                result.SurveyClosing = "Some survey closing text";
                result.SurveyCategory = "Opinions";
                result.SurveyStatus = CassandraService.Entity.Survey.RSTopicStatus.Active;
            }
            else
            if (id.Equals("AddQuestion2", StringComparison.InvariantCultureIgnoreCase))
            {
                result.SurveyTitle = "Sample survey";
                result.SurveyIntroduction = "Some survey intro text";
                result.SurveyClosing = "Some survey closing text";
                result.SurveyCategory = "Opinions";
                result.SurveyStatus = CassandraService.Entity.Survey.RSTopicStatus.Active;

                result.SurveyQuestionList.Add(new CassandraService.Entity.SurveyQuestion
                {
                    PageNo = "0",
                    QuestionNo = "1",
                    QuestionText = "some question",
                    QuestionStatus = "status",
                    QuestionType = CassandraService.Entity.Survey.RSQuestionType.DropList,
                    CustomActionList = new List<CassandraService.Entity.SurveyQuestionCustomAction>()
                });
            }


            return result;
        }

    }
    public partial class SurveyEdit_Bak : System.Web.UI.Page
    {
        private List<String> listIconUrl = null;
        private static ILog Log = LogManager.GetLogger("AdminWebsiteLog");

        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();

        private CassandraService.Entity.Survey survey = null;
        // private List<CassandraService.Entity.SurveyQuestion> surveyQuestionList = null;

        private string formMode = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.formMode = Request.QueryString["mode"];

            this.cbCategory.DataSource = db.GetSurveyCategories();
            this.cbCategory.DataTextField = "Title";
            this.cbCategory.DataValueField = "Title";

            if (!Page.IsPostBack)
            {
                if (formMode.Equals("new", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.survey = new CassandraService.Entity.Survey();
                    this.survey.SurveyTitle = "Sample survey";
                    this.survey.SurveyIntroduction = "Some survey intro text";
                    this.survey.SurveyClosing = "Some survey closing text";
                    this.survey.SurveyCategory = "Opinions";
                    this.survey.SurveyStatus = CassandraService.Entity.Survey.RSTopicStatus.Active;

                    this.tbSurveyTitle.Text = this.survey.SurveyTitle;
                    this.tbSurveyIntroduction.Text = this.survey.SurveyIntroduction;
                    this.tbSurveyClosing.Text = this.survey.SurveyClosing;

                    
                    this.cbCategory.SelectedValue = this.survey.SurveyCategory;
                    this.cbCategory.DataBind();


                    this.cbStatus.SelectedValue = ((int)this.survey.SurveyStatus).ToString();
                    this.cbStatus.DataBind();

                    // 
                    lvQuestion.Visible = false;

                    #region Icon data
                    GetIconData();
                    rtIcon.DataSource = listIconUrl;
                    rtIcon.DataBind();
                    #endregion
                }
                else
                {
                    // Session["current_survey"]
                    // Fetch list of questions 
                    
                    lvQuestion.DataBind();
                    lvQuestion.Visible = true;
                }
            }

            this.survey = Session["current_survey"] as CassandraService.Entity.Survey;
        }

        #region survey header card event handlers
        public void lbCreate_Click(object sender, EventArgs e)
        {
            // TODO:
            // Form input validation

            // Insert into database

            // Rebind

            //lvQuestion.DataSource = db.GetSurvey("CreateSurvey").SurveyQuestionList;

            
            lvQuestion.DataSource = db.GetSurvey("MultipleSampleQuestions").SurveyQuestionList;
            lvQuestion.DataBind();
            lvQuestion.Visible = true;

        }
        #endregion survey header card event handlers

        public void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                mpeAddIcon.Show();
                upAddIcon.Update();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void lbSearch_Click(object sender, EventArgs e)
        {
        }

        public void lbNewQuestionCancel_Click(object sender, EventArgs e)
        {
        }

        public void lbNewQuestionSave_Click(object sender, EventArgs e)
        {
        }

        protected void lvQuestion_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddQuestion"))
            {
                this.survey = db.GetSurvey("AddQuestion2");
                List<CassandraService.Entity.SurveyQuestion> questionList = survey.SurveyQuestionList;

                // Set Page no to question list
                int pageNo = 1;
                foreach (CassandraService.Entity.SurveyQuestion q in questionList)
                {
                    q.PageNo = pageNo.ToString();
                    if (q.PageBreak)
                        pageNo++;
                }

                lvQuestion.DataSource = questionList;
                lvQuestion.DataBind();
                fvQuestion.Visible = true;
                CassandraService.Entity.SurveyQuestion qn = Session["current_question"] as CassandraService.Entity.SurveyQuestion;
                if (qn == null)
                    qn = new CassandraService.Entity.SurveyQuestion();

                System.Web.Script.Serialization.JavaScriptSerializer s = new System.Web.Script.Serialization.JavaScriptSerializer();
                Session["current_question"] = s.Serialize(qn);
                Session["current_survey"] = s.Serialize(survey);
                
                //fvQuestion.DataSource = qn;
                //fvQuestion.DataBind();
            } // end if (e.CommandName.Equals("AddQuestion"))
        }

        protected void fvQuestion_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            System.Web.Script.Serialization.JavaScriptSerializer s = new System.Web.Script.Serialization.JavaScriptSerializer();
            CassandraService.Entity.SurveyQuestion qn = s.Deserialize<CassandraService.Entity.SurveyQuestion>(Session["current_question"].ToString());
            System.Web.UI.WebControls.ListView lv = ((System.Web.UI.WebControls.FormView)sender).FindControl("MultipleAnswerListView") as System.Web.UI.WebControls.ListView;

            TextBox questionTextBox = fvQuestion.FindControl("QuestionTextBox") as TextBox;
            if (questionTextBox != null)
            {
                qn.QuestionText = questionTextBox.Text;
            }

            for (int i = 0; i < lv.Items.Count; i++)
            {
                TextBox tbAnswer = lv.Items[i].FindControl("tbEditAnswer1") as TextBox;
                if (tbAnswer != null)
                    qn.AnswerList[i].Text = tbAnswer.Text;
            }

            // qn.AnswerList

            //e.CommandName == "Cancel"
            if (e.CommandName == "AddAnswer")
            {
                if (qn != null)
                {
                    qn.AnswerList.Add(new CassandraService.Entity.SurveyQuestionAnswer
                    {
                        Id = Guid.NewGuid().ToString(),
                        Text = string.Empty
                    });

                    System.Web.UI.WebControls.Repeater rp =
                        ((System.Web.UI.WebControls.FormView)sender).FindControl("PreviewAnswersRepeater") as System.Web.UI.WebControls.Repeater;

                    lv.DataSource = qn.AnswerList;
                    lv.DataBind();
                    rp.DataSource = qn.AnswerList;
                    rp.DataBind();

                    Session["current_question"] = s.Serialize(qn);
                }
            }

        }

        protected void fvQuestion_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            if (e.CancelingEdit)
            {
                this.fvQuestion.Visible = false;
            }
        }

        protected void fvQuestion_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            // TODO: Validate input values

            // TODO: Insert record into database

        }

        private void SyncQuestionInputSession()
        {
            System.Web.Script.Serialization.JavaScriptSerializer s = new System.Web.Script.Serialization.JavaScriptSerializer();
            CassandraService.Entity.SurveyQuestion qn = s.Deserialize<CassandraService.Entity.SurveyQuestion>(Session["current_question"].ToString());

            if (qn == null)
                qn = new CassandraService.Entity.SurveyQuestion();

            // Get controls on formview and update survey question
            TextBox questionTextBox = fvQuestion.FindControl("QuestionTextBox") as TextBox;
            if (questionTextBox != null)
            {
                qn.QuestionText = questionTextBox.Text;
            }

            // Save question back to session
            Session["current_question"] = s.Serialize(qn);

            // Update preview
            Label questionPreviewLabel = fvQuestion.FindControl("lblEditPreviewQuestion") as Label;
            questionPreviewLabel.Text = qn.QuestionText;
        }

        protected void QuestionTypeRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SyncQuestionInputSession();

            System.Web.UI.WebControls.Panel panel = null;
            System.Web.UI.WebControls.Panel multipleAnswerPanel = fvQuestion.FindControl("MultipleAnswerPanel") as System.Web.UI.WebControls.Panel;
            System.Web.UI.WebControls.Panel numberRangePanel = fvQuestion.FindControl("NumberRangePanel") as System.Web.UI.WebControls.Panel;
            System.Web.UI.WebControls.Panel instructionPanel = fvQuestion.FindControl("InstructionPanel") as System.Web.UI.WebControls.Panel;

            if (multipleAnswerPanel != null)
                multipleAnswerPanel.Visible = false;
            if (numberRangePanel != null)
                numberRangePanel.Visible = false;
            if (instructionPanel != null)
                instructionPanel.Visible = false;

            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "1": // Select one 
                case "2": // Multi choice
                case "5": // Drop List
                    panel = multipleAnswerPanel;
                    System.Web.UI.WebControls.ListView lv = panel.FindControl("MultipleAnswerListView") as System.Web.UI.WebControls.ListView;

                    System.Web.Script.Serialization.JavaScriptSerializer s = new System.Web.Script.Serialization.JavaScriptSerializer();
                    CassandraService.Entity.SurveyQuestion qn = s.Deserialize<CassandraService.Entity.SurveyQuestion>(Session["current_question"].ToString());
                    qn.AnswerList.Add(new CassandraService.Entity.SurveyQuestionAnswer
                    {
                        Id = Guid.NewGuid().ToString(),
                        Text = string.Empty
                    });
                    lv.DataSource = qn.AnswerList;
                    lv.DataBind();
                    Session["current_question"] = s.Serialize(qn);

                    break;
                case "3": // Text
                    // panel = fvQuestion.FindControl("MultipleAnswerPanel") as System.Web.UI.WebControls.Panel;
                    // Do nothing
                    break;
                case "4": //Number range
                    panel = numberRangePanel;
                    break;
                case "6": // Instructions
                    panel = instructionPanel;
                    break;
            }

            // Update items on preview panel

            if (panel != null)
                panel.Visible = true;
            

            //MultipleAnswerPanel
            //NumberRangePanel
            //InstructionPanel

        }

        protected void MultipleAnswerListView_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "AddAnswer")
            {
                System.Web.UI.WebControls.Panel panel = null;
                System.Web.UI.WebControls.Panel multipleAnswerPanel = fvQuestion.FindControl("MultipleAnswerPanel") as System.Web.UI.WebControls.Panel;

                panel = multipleAnswerPanel;
                System.Web.UI.WebControls.ListView lv = panel.FindControl("MultipleAnswerListView") as System.Web.UI.WebControls.ListView;

                List<CassandraService.Entity.SurveyQuestionAnswer> answerList = new List<CassandraService.Entity.SurveyQuestionAnswer>();
                answerList.Add(new CassandraService.Entity.SurveyQuestionAnswer
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "sample answer1"
                });
                answerList.Add(new CassandraService.Entity.SurveyQuestionAnswer
                {
                    Id = Guid.NewGuid().ToString(),
                    Text = "sample answer2"
                });
                lv.DataSource = answerList;
                lv.DataBind();
            }
        }

        protected void AddAnswerButton_Click(object sender, EventArgs e)
        {

        }

        #region dialog event handlers

        /// <summary>
        /// Event handler for select survey icon popup dialog
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    Log.Debug(e.CommandArgument.ToString());
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpeAddIcon.Hide();
                    //upEditTopic.Update();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    GetIconData();
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = listIconUrl[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = listIconUrl[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpeAddIcon.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void GetIconData()
        {
            if (listIconUrl == null)
            {
                managerInfo = Session["admin_info"] as ManagerInfo;
                if (managerInfo != null)
                {
                    CassandraService.ServiceResponses.TopicSelectIconResponse response = asc.SelectAllTopicIcons(
                        managerInfo.UserId,
                        managerInfo.CompanyId);
                    if (response.Success)
                    {
                        listIconUrl = response.TopicIconUrls;
                    }
                }
                
            }
        }
        #endregion dialog event handlers

        protected void lbAddQuestionIcon_Click(object sender, EventArgs e)
        {
            try
            {
                mpeSurveyQuestion.Show();
                popup_addsurveyquestionicon.Visible = true;
                upEditQuestion.Update();
                //upAddIcon.Update();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}