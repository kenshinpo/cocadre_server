﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SurveyAnalyticDemo.aspx.cs" Inherits="AdminWebsite.Survey.SurveyAnalytic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .error_msg { text-align:center; display: none;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><a href="/Survey/List" style="color:#000;">Responsive <span>survey</span></a></div>
        <div class="appbar__meta"><a href="" id="survey-root-link"></a></div>
        <div class="appbar__meta"><a href="" id="simple-survey-link">Overview</a></div>
        <div class="appbar__meta"><a href="" id="responder_report_link" class="hidden">Responders report</a></div>
        <div class="appbar__meta breadcrumb-participant" ></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="survey-wrapper" class="data" style="overflow-y:scroll;">

        <div id="responsive-survey" class="container" style="width:100%;">

            <div style="width:100%; margin:0 auto;">

                <div class="title-section" style="width:80%; margin: 0 auto;">
                    <div class="survey-info">
                        <h1><img id="survey-icon" src="" /><span id="survey-topic"></span></h1>
                        <p class="align-left">Showing data from <span id="survey-start-date"></span> - <span id="survey-end-date"></span></p>
                    </div>
                    <div class="user-info hidden">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span id="user-name" class="list-value"></span>
                                Name:
                            </li>
                            <li class="list-group-item">
                                <span id="user-department" class="list-value"></span>
                                Department:
                            </li>
                            <li class="list-group-item">
                                <span id="user-status" class="list-value"></span>
                                Status:
                            </li>
                            <li class="list-group-item">
                                <span id="user-last-update" class="list-value"></span>
                                Last Update:
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>

                <!-- SELECTED SURVEY RESULT FROM SEE MORE -->
                <div id="single-survey-table-wrapper" class="card animated fadeInUp no-padding">
                </div>

                <!-- START OF OVERVIEW PAGE -->
                <div id="overview-tab" class="tabs tabs--styled">
                    <ul class="tabs__list">
                        <li class="tabs__list__item">
                            <a class="tabs__link" href="#overview">Overview</a>
                        </li>
                        <li class="tabs__list__item">
                            <a class="tabs__link" href="#responders-report">Responders report</a>
                        </li>
                        <li class="tabs__list__item">
                            <a class="tabs__link" href="#responders-feedback">Feedback</a>
                        </li>
                    </ul>
                    <div class="tabs__panels">
                        <!-- OVERVIEW TAB -->
                        <div class="tabs__panels__item overview" id="overview">
                            <div class="card card-7-col animated fadeInUp">
                                <div class="card-header">
                                    <h1 class="title">Responders report</h1>
                                </div>
                                <div class="chart-section" style="width:100%; height:336px; position:relative;">
                                     <div class="legends"></div>
                                    <canvas id="responderChart" width="800" height="335"></canvas>
                                </div>
                                <div class="chart-footer">
                                    <label class="info-label">Starting from <span id="start_date" class="start_date"></span></label>
                                    <div class="chart-info">
                                        <div class="chart-border"></div>
                                        <label id="response_completed" class="response_number">0</label>
                                        <small>responses</small>
                                        <p>Completed since start</p>
                                    </div>
                                    <div class="chart-info">
                                        <div class="chart-border"></div>
                                        <label id="response_bouncerate" class="response_number">0</label>
                                        <small>responses</small>
                                        <p>Bounce rate</p>
                                    </div>
                                    <div class="chart-info">
                                        <label id="response_completionrate" class="response_number">0</label>
                                        <small>Survey completion rate</small>
                                        <p>Since beginning</p>
                                    </div>
                                    <div class="chart-total-responder">
                                        <div class="border-box">
                                            <small>Out of</small>
                                            <label id="response_totalresponder" class="response_number">0</label>
                                            <small class="highlight">Responders</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- SUMMARY -->
                            <div class="card card-3-col animated fadeInUp">
                                <div class="card-header">
                                    <h1 class="title">Summary</h1>
                                </div>
                                <div class="small-card-content">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <span id="total-pages" class="list-value"></span>
                                            Total Pages:
                                        </li>
                                        <li class="list-group-item">
                                            <span id="total-question" class="list-value"></span>
                                            Total Questions:
                                        </li>
                                        <li class="list-group-item">
                                            <span id="total-days" class="list-value"></span>
                                            Days Running:
                                        </li>
                                        <li class="list-group-item">
                                            <span id="average-completion" class="list-value"></span>
                                            Average Completion:
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!-- PERSONNEL COMPLETED -->
                            <div class="card card-3-col animated fadeInUp">
                                <div class="card-header">
                                    <h1 class="title">Personnel completed</h1>
                                </div>
                                <div class="small-card-content">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <span id="completed-value" class="list-value"></span>
                                            <span id="completed-percentage" class="list-percentage color1" ></span>
                                            Completed
                                            <div class="progress">
                                                <div id="completed-bar" class="progress-bar color1"></div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <span id="incomplete-value" class="list-value"></span>
                                            <span id="incomplete-percentage" class="list-percentage color2" ></span>
                                            Incomplete
                                            <div class="progress">
                                                <div id="incompleted-bar" class="progress-bar color2"></div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <span id="absent-value" class="list-value"></span>
                                            <span id="absent-percentage" class="list-percentage color3"></span>
                                            Absent
                                            <div class="progress">
                                                <div id="absent-bar" class="progress-bar color3"></div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!-- ROW FOR BUTTON -->
                            <div class="full-row align-center">
                                <button id="generate" class="btn-default animated fadeIn">Generate Live Data</button>
                            </div>

                            <!-- LIVE DATA SECTION -->
                            <div id="live-data-section"></div>
                        </div>

                        <!-- RESPONDERS REPORT TAB -->
                        <div class="tabs__panels__item responders-report" id="responders-report">

                            <!-- USER REPORT -->
                            <div id="user-report-section"></div>

                            <!-- RESPONDER TABLE -->
                            <div id="responder-table-section">
                                <div class="filter-section">
                                    <label class="select-label">Show</label>
                                    <div class="mdl-selectfield" style="width:250px;float:left;margin-right: 20px; height:40px;">
                                        <%--<label for="status">Show</label>--%>
                                        <select name="status" id="status">
	                                        <option selected="selected" value="">All</option>
	                                        <option value="Completed">Completed</option>
                                            <option value="Incomplete">Incomplete</option>
                                        </select>
                                    </div>
                                    <div class="input-group" style="position:relative;">
                                            <i class="fa fa-search"></i>
                                            <input id="hidden_name" class="search-input" type="text" placeholder="Search Responders" value=""/>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="card animated fadeInUp ">

                                    <!-- LEGENDS -->
                                    <div class="legends">
                                        <div class="legend-item">
                                            <div class="legend-icon">
                                                <div class="legend-color1"></div>
                                            </div>
                                            <label>Completed</label>
                                        </div>
                                        <div class="legend-item">
                                            <div class="legend-icon" >
                                                <div class="legend-color2"></div>
                                            </div>
                                            <label>Incomplete</label>
                                        </div>
                                        <div class="legend-item" >
                                            <div class="legend-icon">
                                                <div class="legend-color3"></div>
                                            </div>
                                            <label>Absent</label>
                                        </div>
                                    </div>

                                    <div class="card-header">
                                        <h1 class="title">Personnel completion</h1>
                                    </div>
                                
                                    <div class="progress stacked">
                                        <div id="progress-bar-completed" class="progress-bar color1">
                                            <span id="personnel-completed-value" class="value">12</span>
                                            <span id="personnel-completed-percentage" class="percentage color1">12%</span>
                                        </div>
                                        <div id="progress-bar-incompleted" class="progress-bar color2">
                                            <span id="personnel-incompleted-value" class="value">12</span>
                                            <span id="personnel-incompleted-percentage" class="percentage color2">12%</span>
                                        </div>
                                        <div id="progress-bar-absent" class="progress-bar color3" >
                                            <span id="personnel-absent-value" class="value">12</span>
                                            <span id="personnel-absent-percentage" class="percentage color3">12%</span>
                                        </div>
                                    </div>
                                </div>

                                <div id="personnel_list" class="card animated fadeInUp no-padding">
                                    <div class="card-header" style="padding:20px 20px 0px 20px;">
                                        <h1 class="title">Personnel list <span id="list-status"> - All</span></h1>
                                    </div>
                                    <div class="table-content">
                                        <p class="error_msg">There are no data at this moment.</p>
                                        <table id="responder-table">
                                            <thead>
                                                <th data-dynatable-column="no" style="width:10%">No</th>
                                                <th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width:30%">Name</th>
                                                <th class="hidden" data-dynatable-column="hidden_name">Hidden name</th>
                                                <th data-dynatable-column="last_updated" data-dynatable-sorts="last_updated" style="width:20%">Last update</th>
                                                <th data-dynatable-column="department" data-dynatable-sorts="department" style="width:20%">Department</th>
                                                <th data-dynatable-column="status" style="width:20%">Status</th>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- FEEDBACK TAB -->
                        <div class="tabs__panels__item responders-report" id="responders-feedback">
                            <div id="feedback_list" class="card animated fadeInUp no-padding">
                                <div class="card-header" style="padding:20px 20px 0px 20px;">
                                    <h1 class="title">Feedback</h1>
                                </div>
                                <p class="error_msg">There are no data at this moment.</p>
                                <div class="table-content">
                                    <table id="feedback-table" class="responder-table">
                                        <thead>
                                            <th data-dynatable-column="date" style="width:20%">Date</th>
                                            <th data-dynatable-column="feedback" style="width:80%">Feedback</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="/js/vendor/moment-with-locales.js"></script>
    <script src="/Js/Chart.js"></script>
    <script>

        $(function () {

            //var CompanyId = "C8b28091502514318bdcf48bec7c69129";
            //var AdminUserId = "Udb0e704ee7244d21aea7930c27709fee";
            //var CategoryId = "RSC884bc782749a4372bbd8732acde72da7";
            //var TopicId = "RSTf54786da3e27434393e075bce3087c29";

            var data = {
                labels: [],
                datasets: [
                    {
                        label: "Completion Number",
                        fillColor: "rgba(254,232,234,0.7)",
                        strokeColor: "rgba(35,118,238,1)",
                        pointColor: "rgba(255,255,255,1)",
                        pointStrokeColor: "rgba(35,118,238,1)",
                        pointHighlightFill: "rgba(255,255,255,1)",
                        pointHighlightStroke: "rgba(35,118,238,1)",
                        data: []
                    },
                    {
                        label: "Bounce Number",
                        fillColor: "rgba(255,191,117,0.7)",
                        strokeColor: "rgba(254,150,1,1)",
                        pointColor: "rgba(255,255,255,1)",
                        pointStrokeColor: "rgba(254,150,1,1)",
                        pointHighlightFill: "rgba(254,150,1,1)",
                        pointHighlightStroke: "rgba(254,150,1,1)",
                        data: []
                    }
                ]
            };

            var options = {

                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: true,

                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.06)",

                //Number - Width of the grid lines
                scaleGridLineWidth: 1,

                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,

                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: false,

                //Boolean - Whether the line is curved between points
                bezierCurve: false,

                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.4,

                //Boolean - Whether to show a dot for each point
                pointDot: true,

                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,

                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,

                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,

                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,

                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,

                //Boolean - Whether to fill the dataset with a colour
                datasetFill: true,
                responsive: true,
                animation: true,

            };

            var ctx = document.getElementById("responderChart").getContext("2d");
            var myLineChart = "";
            var html = "";
            var answer_html = "";
            var chart_data = {};
            var single_table_data = [];
            var single_table_html = "";
            var isAnonymous = false;
            var feedback_data = [];
            var feedback_table = $('#feedback-table');

            //build url params and open new tab
            $(document).on('click', '.viewResponder_link', function (e) {
                e.preventDefault();

                var params = $.param({
                    "OptionId": $(this).data('optionid'),
                    "CardId": $(this).data('cardid'),
                    "TopicId": TopicId,
                    "CategoryId": CategoryId,
                    "AdminUserId": AdminUserId,
                    "CompanyId": CompanyId
                    //"Paging": $(this).data('paging'),
                    //"Ordering": $(this).data('ordering'),
                    //"Question" :,
                });

                var url = window.location.href;
                var hash = location.hash;
                url = window.location.protocol + "//" + window.location.host + "/Survey/SingleQuestionAnalytic.aspx/";

                if (url.indexOf("?") < 0)
                    url += "?" + params;
                else
                    url += "&" + params;

                window.open(url + hash, '_blank');
            });

            function fetchOverview() {
                $('#responsive-survey').addClass('hidden');
                $('#responsive-survey').removeClass('animated fadeInUp');
                ShowProgressBar();

                $.ajax({
                    type: "POST",
                    url: '/adminrs/selectresultoverview',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "CategoryId": CategoryId,
                        "TopicId": TopicId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();

                        if (res.Success) {

                            $('#responsive-survey').removeClass('hidden');
                            $('#responsive-survey').addClass('animated fadeInUp');

                            var summary = res.Overview.Summary;
                            var personnel_completion = res.Overview.PersonnelCompletion;
                            var report = res.Overview.Report;
                            var report_arr = res.Overview.Report.ReportList;
                            var topic = res.Overview.Topic;
                            var starting_month = moment(new Date(report_arr[0].DatestampString)).format("MMM");
                            var ending_month = moment(new Date(report_arr[report_arr.length - 1].DatestampString)).format("MMM");
                            isAnonymous = res.Overview.Topic.IsAnonymous;

                            if (isAnonymous) {
                                $('#responder-table-section .filter-section').css('display', 'none');
                                $('#personnel_list').css('display', 'none');
                            }

                            $('#survey-root-link').html(topic.Title);
                            $('#survey-root-link').attr('href', "/Survey/Edit/" + TopicId + "/" + CategoryId);
                            $('#survey-topic').html(topic.Title);
                            $('#survey-icon').attr('src', topic.IconUrl);

                            $('#total-pages').html(summary.TotalPages);
                            $('#total-question').html(summary.TotalCards);
                            $('#total-days').html(summary.TotalDays);
                            $('#average-completion').html(summary.AverageTimeCompletionInSecond);

                            $('#response_completed').html(report.TotalCompletion);
                            $('#response_bouncerate').html(report.TotalBounces + "%");
                            $('#response_completionrate').html(report.TotalCompletionPercentage + "%");
                            $('#response_totalresponder').html(report.TotalTargetedAudience);

                            $('#completed-value').html(personnel_completion.CompletionNumber);
                            $('#completed-percentage').html(personnel_completion.CompletionPercentage + "%");
                            $('#incomplete-value').html(personnel_completion.IncompletionNumber);
                            $('#incomplete-percentage').html(personnel_completion.IncompletionPercentage + "%");
                            $('#absent-value').html(personnel_completion.AbsentNumber);
                            $('#absent-percentage').html(personnel_completion.AbsentPercentage + "%");

                            setTimeout(function () {
                                $('#completed-bar').css('width', personnel_completion.CompletionPercentage + '%');
                                $('#incompleted-bar').css('width', personnel_completion.IncompletionPercentage + '%');
                                $('#absent-bar').css('width', personnel_completion.AbsentPercentage + '%');
                            }, 1000);

                            if (report_arr.length > 0) {
                                $('#survey-start-date').html(report_arr[0].DatestampString);
                                $('#start_date').html(report_arr[0].DatestampString);
                                $('#survey-end-date').html(report_arr[report_arr.length - 1].DatestampString);
                                $.each(report_arr, function (key, value) {
                                    var date = value.DatestampString.split(' ');
                                    var date_month = date[0];
                                    if (key == 0 || key == report_arr.length - 1) {
                                        date_month = date[0] + " " + date[1];
                                    }
                                    data.labels.push(value.DatestampString);
                                    data.datasets[0].data.push(value.CompletionNumber);
                                    data.datasets[1].data.push(value.BounceNumber);
                                });
                                console.log(JSON.stringify(data));
                                myLineChart = new Chart(ctx).Line(data, options);

                                $('.chart-section').append('<label id="starting_month" style="position: absolute;bottom: 15px; left: 43px;  font-weight: 700;">' + starting_month + '</label><label id="ending_month" style="position:absolute; right: -4px; bottom:15px; font-weight: 700;">' + ending_month + '</label>');
                                $('.chart-section .legends').append('<ul class="line-legend"><li><span class="line-legend-icon" style="background-color:rgba(35,118,238,1)"></span><span class="line-legend-text">Completion Number</span></li><li><span class="line-legend-icon" style="background-color:rgba(254,150,1,1)"></span><span class="line-legend-text">Bounce Number</span></li><li><span class="line-legend-icon" style="background-color:none; border: 1px solid #ff9600;"></span><span class="line-legend-text">Weekends</span></li></ul>');
                            }

                        }

                    }
                });
            }

            function fetchFeedback() {

                ShowProgressBar();

                $.ajax({
                    type: "POST",
                    url: '/adminrs/SelectFeedback',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "CategoryId": CategoryId,
                        "TopicId": TopicId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {

                            if (res.Feedbacks.length > 0) {

                                $('.table-content table').css('display', 'none');
                                $('.error_msg').css('display', 'none');

                                $.each(res.Feedbacks, function (key, value) {
                                    feedback_data.push({
                                        "date": value.CreatedTimestampString,
                                        "feedback": value.Feedback
                                    });
                                });

                                feedback_table.dynatable({
                                    features: {
                                        paginate: false,
                                        search: false,
                                        sorting: false,
                                        recordCount: false
                                    },
                                    dataset: {
                                        records: feedback_data
                                    }
                                });

                                setTimeout(function () {
                                    feedback_table.css('display', 'table');
                                    feedback_table.addClass('animated fadeIn');
                                }, 500);
                            } else {
                                $('.table-content table').css('display', 'none');
                                $('.error_msg').css('display', 'block');


                            }

                        }
                    }
                });
            }

            // RESET OVERVIEW PAGE
            $('#simple-survey-link').on('click', function (e) {
                e.preventDefault();
                $('.breadcrumb-participant').html(' ');
                $('#single-survey-table-wrapper').removeClass('animated fadeInUp')
                $('#single-survey-table-wrapper').addClass('animated fadeOut');

                setTimeout(function () {
                    $('#single-survey-table-wrapper').addClass('hidden');
                }, 500);
                $('#overview-tab').removeClass('hidden animated fadeOut');
                $('#overview-tab').css('display', 'block');
                $('#overview-tab').addClass('animated fadeInUp');
            })

            /***** SEE MORE CLICK EVENT HANDLER *********/
            $(document).on('click', '.see-more-btn-s', function (e) {
                e.preventDefault();
                var cardId = $(this).data('cardid');

                var url = window.location.href;

                url = window.location.protocol + "//" + window.location.host + "/Survey/Analytic/Detail/" + TopicId + "/" + CategoryId + "/" + cardId;

                window.open(url, '_blank');

            });

            function fetchContent() {

                $('#generate').html('Fetching...');

                ShowProgressBar();

                $.ajax({
                    type: "POST",
                    url: '/adminRS/selectcardresult',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "CategoryId": CategoryId,
                        "TopicId": TopicId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        if (res.Success) {
                            HideProgressBar();
                            $('#generate').css('display', 'none');

                            $.each(res.CardResults, function (key, value) {
                                var cardId = value.CardId;
                                var paging = value.Paging;
                                var ordering = value.Ordering;

                                if (value.Type == 3) {
                                    html += '<div class="card card-5-col type' + value.Type + '">';
                                    html += '<div class="card-header">';
                                    html += '<div class="grid-question-number">';
                                    html += '<div style="color:#ddd;">';
                                    html += '<div class="number">P' + value.Paging + ' | Q' + value.Ordering;
                                    html += '</div>';
                                    html += '<div class="code">Code: ' + value.CardId + '</div>';
                                    html += '</div> ';
                                    html += '</div>';
                                    if (value.HasImage) {
                                        html += '<div class="question-image" style="width: 50px; height: auto; float: left;margin-right: 5px;margin-top: 10px;"><img src="' + value.CardImages[0].ImageUrl + '" /></div>';
                                    }
                                    html += '<div class="grid-question">';
                                    html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Question</label>';
                                    html += ' <p class="question">' + value.Content + '</p>';
                                    html += '</div>';
                                    html += '<div class="grid-type align-center">';
                                    html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Type</label>';
                                    html += '<div class="type-image" style="width:25px; height:25px; margin:0 auto;"><img src="/Img/icon_text.png" /></div>';
                                    html += '</div>';
                                    html += '<div class="grid-overall">';
                                    html += '<small>Out of</small>';
                                    html += '<label class="response_number">' + value.TotalResponses + '</label>';
                                    html += '<small>responses</small>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '<hr style="clear:both;"/>';
                                    html += '<div class="small-card-content">';

                                    if (value.Options.length > 0) {

                                        html += '<ul class="list-group type' + value.Type + '">';

                                        value.Options.sort(function (a, b) {

                                            return b.PercentageOfSelection - a.PercentageOfSelection;
                                        });

                                        $.each(value.Options, function (key, value) {
                                            html += '<li class="list-group-item">';
                                            html += '<span class="list-answer-count" >' + value.NumberOfSelection + '</span>';
                                            html += '<p> ' + value.Content + '</p>';
                                            html += '</li>';
                                        });

                                        html += '</ul>';
                                        html += '<a href="" class="align-center see-more-btn-s" data-cardid="' + value.CardId + '">See more</a>'
                                    } else if (value.CustomAnswers.length > 0) {
                                        html += '<ul class="list-group type' + value.Type + '">';

                                        value.Options.sort(function (a, b) {

                                            return b.PercentageOfSelection - a.PercentageOfSelection;
                                        });

                                        $.each(value.CustomAnswers, function (key, value) {
                                            html += '<li class="list-group-item">';
                                            html += '<span class="list-answer-count" >' + value.NumberOfSelection + '</span>';
                                            html += '<p> ' + value.Content + '</p>';
                                            html += '</li>';
                                        });

                                        html += '</ul>';
                                        html += '<a href="" class="align-center see-more-btn-s" data-cardid="' + value.CardId + '">See more</a>'
                                    } else {
                                        html += '<p class="align-center">No data for now.</p>'
                                    }

                                    html += '</div>';
                                    html += '</div>';

                                } else if (value.Type == 6) {
                                    html += '<div class="card card-5-col type' + value.Type + '" style="background: rgba(105,178,221,1); color: #ffffff;">';
                                    html += '<div class="card-header">';
                                    html += '<div class="grid-question-number">';
                                    html += '<div style="color:#fff;">';
                                    html += '<div class="number">P' + value.Paging + ' | Q' + value.Ordering;
                                    html += '</div>';
                                    html += '<div class="code">Code: ' + value.CardId + '</div>';
                                    html += '</div> ';
                                    html += '</div>';
                                    if (value.HasImage) {
                                        html += '<div class="question-image" style="width: 50px; height: auto; float: left;margin-right: 5px;margin-top: 10px;"><img src="' + value.CardImages[0].ImageUrl + '" /></div>';
                                    }
                                    html += '<div class="grid-question">';
                                    html += '<label style="color:#fff; font-size:16px; line-height:16px;">Question</label>';
                                    html += ' <p class="question">' + value.Content + '</p>';
                                    html += '</div>';
                                    html += '<div class="grid-type align-center">';
                                    html += '<label style="color:#fff; font-size:16px; line-height:16px;">Type</label>';
                                    html += '<div class="type-image" style="width:25px; height:25px; margin:0 auto;">';
                                    html += '<img src="/Img/icon_instructions.png" title="Instruction">';
                                    html += '</div></div>';
                                    html += '</div>';

                                    html += '</div>';
                                } else {
                                    html += '<div class="card card-5-col type' + value.Type + '">';
                                    html += '<div class="card-header">';
                                    html += '<div class="grid-question-number">';
                                    html += '<div style="color:#ddd;">';
                                    html += '<div class="number">P' + value.Paging + ' | Q' + value.Ordering + '</div>';
                                    html += '<div class="code">Code: ' + value.CardId + '</div>';
                                    html += '</div></div>';
                                    if (value.HasImage) {
                                        html += '<div class="question-image" style="width: 50px; height: auto; float: left;margin-right: 5px;margin-top: 10px;"><img src="' + value.CardImages[0].ImageUrl + '" /></div>';
                                    }
                                    html += '<div class="grid-question">';
                                    html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Question</label>';
                                    html += '<p class="question">' + value.Content + '</p>';
                                    html += '</div>';
                                    html += '<div class="grid-type align-center">';
                                    html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Type</label>';
                                    html += '<div class="type-image" style="width:25px; height:25px; margin:0 auto;">';

                                    if (value.Type == 1) {
                                        html += '<img src="/Img/icon_selectone.png" />';
                                    } else if (value.Type == 2) {
                                        html += '<img src="/Img/icon_multichoice.png" />';
                                    } else if (value.Type == 4) {
                                        html += '<img src="/Img/icon_numberrange.png" />';
                                    } else if (value.Type == 5) {
                                        html += '<img src="/Img/icon_droplist.png" />';
                                    }

                                    html += '</div>';
                                    html += '</div>';
                                    html += '<div class="grid-overall">';
                                    html += '<small>Out of</small>';
                                    html += '<label class="response_number">' + value.TotalResponses + '</label>';
                                    html += '<small>responses</small>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '<hr style="clear:both;"/>';
                                    html += '<div class="small-card-content">';

                                    if (value.Options.length > 0) {

                                        html += '<ul class="list-group">';

                                        value.Options.sort(function (a, b) {

                                            return b.PercentageOfSelection - a.PercentageOfSelection;
                                        });

                                        $.each(value.Options, function (key, value) {
                                            html += '<li class="list-group-item">';
                                            html += '<span class="list-value">' + value.NumberOfSelection + '</span>';
                                            if (key > 9) {
                                                key = 0;
                                            }
                                            html += '<span class="list-percentage color' + (key + 1) + '">' + value.PercentageOfSelection + '%</span>';

                                            if (value.HasImage) {
                                                html += '<div class="answer-image"><img src="' + value.Images[0].ImageUrl + '" /></div> ';
                                            }
                                            html += '<p>' + value.Content + '</p>';
                                            html += '<div class="progress">';
                                            html += '<div class="progress-bar color' + (key + 1) + '" style="width:' + value.PercentageOfSelection + '%;"></div>';
                                            html += '</div>';

                                            if (!isAnonymous) {
                                                html += '<a href="" class="viewResponder_link" data-ordering="' + ordering + '" data-pagin="' + paging + '" data-cardid="' + cardId + '" data-optionid="' + value.OptionId + '" style="position:absolute; top:10px; width:20px; right:-25%;"><img src="/Img/icon_result.png" /></a>';
                                            }

                                            html += '</li>';
                                        });

                                        html += '</ul>';

                                    } else {
                                        html += '<p class="align-center">No data for now.</p>'
                                    }

                                    if (value.HasCustomAnswer && value.CustomAnswers.length > 0) {
                                        html += '<ul class="list-group type3">';
                                        value.CustomAnswers.sort(function (a, b) {

                                            return b.NumberOfSelection - a.NumberOfSelection;
                                        });

                                        $.each(value.CustomAnswers, function (key, value) {
                                            html += '<li class="list-group-item">';
                                            html += '<span class="list-answer-count" >' + value.NumberOfSelection + '</span>';
                                            html += '<p> ' + value.Content + '</p>';
                                            html += '</li>';
                                        });

                                        html += '</ul>';
                                        html += '<a href="" class="align-right see-more-btn-s" data-cardid="' + value.CardId + '">See more</a>'
                                    }

                                    html += '</div>';
                                    html += '</div>';
                                }

                            });
                            $('#live-data-section').append(html);
                            $("#live-data-section").addClass("animated fadeInUpBig");

                            setTimeout(function () {
                                $('#survey-wrapper').stop().animate({
                                    'scrollTop': 800
                                }, 1000);
                            }, 500);

                        } else {
                            $('#generate').html('Try again');
                        }

                    }
                })
            }

            fetchOverview();

            $('#generate').on('click', function (e) {
                e.preventDefault();
                fetchContent();
            });

            /******************
           **** SECOND TAB ****
           ******************/
            var table_data = [];
            var table = $('#responder-table');

            $('#status').on('change', function () {
                $('#list-status').html($(this).val() == "" ? " - All" : " - " + $(this).val());
            });

            function fetchUserReport(userId) {
                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: '/adminRS/SelectCardResultByUser',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "CategoryId": CategoryId,
                        "TopicId": TopicId,
                        "AnsweredByUserId": userId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {
                            answer_html = "";
                            var user = res.Report.User;
                            $('#user-name').html(user.FirstName + " " + user.LastName);
                            $('#user-department').html(user.Departments[0].Title);
                            $('#user-status').html(res.Report.IsCompleted ? "Completed" : "Incomplete");
                            $('#user-last-update').html(res.Report.LastUpdateTimestampString);
                            //$('#user-profile-pic').attr('src', user.ProfileImageUrl);

                            if ($('.user-info').hasClass('hidden')) {
                                $('.user-info').removeClass('hidden');
                            }

                            if (res.CardResults.length > 0) {
                                $.each(res.CardResults, function (key, value) {
                                    answer_html += '<div class="card animated fadeInUp no-padding">';

                                    if (value.Type == 6) {
                                        answer_html += '<div class="card-header" style="width:100%; float:left; border-right:1px solid #ccc; padding:20px; border-right: 0px;">';
                                        answer_html += '<div class="grid-question-number" style="width:11%">';
                                    } else {
                                        answer_html += '<div class="card-header" style="width:50%; float:left; border-right:1px solid #ccc; padding:20px;">';
                                        answer_html += '<div class="grid-question-number">';
                                    }


                                    answer_html += '<div style="color:#ddd;">';
                                    answer_html += '<div class="number">P' + value.Paging + ' | Q' + value.Ordering + '</div>';
                                    answer_html += '<div class="code">Code: ' + value.CardId + '</div>';
                                    answer_html += '</div>';
                                    answer_html += '</div>';
                                    if (value.HasImage) {
                                        answer_html += '<div class="question-image" style="width: 100px; height: auto; float: left;margin-right: 5px;margin-top: 10px;"><img src="' + value.CardImages[0].ImageUrl + '" /></div>';
                                    }
                                    answer_html += '<div class="grid-question">';

                                    answer_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Question</label>';
                                    answer_html += '<p class="question">' + value.Content + '</p>';
                                    answer_html += '</div>';
                                    answer_html += '</div>';
                                    answer_html += '<div class="card-answer" style="width:50%; float:left;padding:20px;">';

                                    if (value.Type < 6) {
                                        answer_html += '<div class="grid-answer">';
                                        //answer_html += '<div class="question-image" style="width: 50px; height: 50px; float: left;margin-right: 10px;margin-top: -5px;"><img src="' + val.Images[0].ImageUrl + '" /></div>';
                                        answer_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Answer</label>';
                                        if (value.Type == 4) {
                                            answer_html += '<p class="answer">';
                                            answer_html += value.SelectedRange;
                                            answer_html += '</p>';
                                        } else {
                                            if (value.Options.length > 0) {
                                                answer_html += '<p class="answer">';
                                                $.each(value.Options, function (key, val) {

                                                    answer_html += val.Content;
                                                    if (key < value.Options.length - 1) {
                                                        answer_html += ", ";
                                                    }

                                                });
                                                answer_html += '</p>';
                                            }
                                            else if (value.HasCustomAnswer && value.CustomAnswer.Content) {
                                                answer_html += '<p class="answer">' + value.CustomAnswer.Content + '</p>';
                                            }
                                            else {
                                                answer_html += '<p class="answer"><i>The responder doesn\'t answer this question. </i></p>';
                                            }
                                        }

                                        answer_html += '</div>';
                                    } else {
                                        //answer_html += '<div style="background:#ccc; width:100%; height:100%;></div>';
                                    }
                                    answer_html += '</div>';
                                    answer_html += '</div>';
                                });
                            }

                            $('#user-report-section').append(answer_html);
                        }
                    }
                });
            }

            function resetResponderTableSection() {
                $('.breadcrumb-participant').html(' ');
                $('#user-report-section').removeClass('animated fadeInUp');
                $('#user-report-section').addClass('animated fadeOut');

                setTimeout(function () {
                    $('#user-report-section').addClass('hidden');
                    $('#user-report-section').css('display', 'none');
                    $('#responder-table-section').removeClass('hidden animated fadeOut');
                    $('#responder-table-section').addClass('animated fadeInUp');
                }, 500);

                if (!$('.user-info').hasClass('hidden')) {
                    $('.user-info').addClass('hidden');
                }
            }

            function toggleUserSection(user) {
                $('.breadcrumb-participant').html(user.html());
                $('#responder-table-section').addClass('animated fadeOut');

                setTimeout(function () {
                    $('#responder-table-section').addClass('hidden');
                }, 500);
                $('#user-report-section').removeClass('hidden animated fadeOut');
                $('#user-report-section').css('display', 'block');
                $('#user-report-section').addClass('animated fadeInUp');

                answer_html = "";
                $('#user-report-section').empty();

                fetchUserReport(user.data('userid'));
            }

            table.bind('dynatable:afterUpdate', function (e, dynatable) {
                setTimeout(function () {
                    table.css('display', 'table');
                    table.addClass('animated fadeIn');
                }, 500);

                $('.responder-name').on('click', function (e) {
                    e.preventDefault();
                    toggleUserSection($(this));
                });
            });

            // CUSTOM TABLE SEARCH FILTER FUNCTION
            table.bind('dynatable:init', function (e, dynatable) {

                dynatable.queries.functions['hidden_name'] = function (record, queryValue) {

                    return record.hidden_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                };
            });

            // RENDER SALES RESPONDER TAB
            function SelectRespondersReport() {
                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: '/adminRS/SelectRespondersReport',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "CategoryId": CategoryId,
                        "TopicId": TopicId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {
                            $('.table-content table').css('display', 'table');
                            $('.error_msg').css('display', 'none');
                            var personnel_completion = res.PersonnelCompletion;
                            $('#personnel-completed-value').html(personnel_completion.CompletionNumber);
                            $('#personnel-completed-percentage').html(personnel_completion.CompletionPercentage + "%");
                            $('#personnel-incompleted-value').html(personnel_completion.IncompletionNumber);
                            $('#personnel-incompleted-percentage').html(personnel_completion.IncompletionPercentage + "%");
                            $('#personnel-absent-value').html(personnel_completion.AbsentNumber);
                            $('#personnel-absent-percentage').html(personnel_completion.AbsentPercentage + "%");

                            if (res.Reports.length > 0 && !isAnonymous) {
                                $.each(res.Reports, function (key, value) {
                                    table_data.push({
                                        "no": key + 1,
                                        "responder_name": '<a href="" class="responder-name" data-userid="' + value.User.UserId + '">' + value.User.FirstName + " " + value.User.LastName + '</a>',
                                        "hidden_name": value.User.FirstName + " " + value.User.LastName,
                                        "last_updated": value.LastUpdateTimestampString != undefined ? value.LastUpdateTimestampString : " ",
                                        "department": value.User.Departments[0].Title,
                                        "status": (value.IsCompleted ? "Completed" : "Incomplete")
                                    });
                                });

                                table.dynatable({
                                    features: {
                                        paginate: false,
                                        search: false,
                                        sorting: false,
                                        recordCount: false
                                    },
                                    dataset: {
                                        records: table_data
                                    },
                                    inputs: {
                                        queries: $('#status, #hidden_name')
                                    }
                                })

                            }

                            setTimeout(function () {
                                $('#progress-bar-completed').css('width', personnel_completion.CompletionPercentage + "%");
                                $('#progress-bar-incompleted').css('width', personnel_completion.IncompletionPercentage + "%");
                                $('#progress-bar-absent').css('width', personnel_completion.AbsentPercentage + "%");
                                $('#personnel-completed-value').css('display', 'block');
                                $('#personnel-completed-percentage').css('display', 'block');
                                $('#personnel-incompleted-value').css('display', 'block');
                                $('#personnel-incompleted-percentage').css('display', 'block');
                                $('#personnel-absent-value').css('display', 'block');
                                $('#personnel-absent-percentage').css('display', 'block');
                            }, 500);

                        } else {

                            $('.table-content table').css('display', 'none');
                            $('.error_msg').css('display', 'block');

                        }
                    }
                });
            }

            $('#responder_report_link').on('click', function (e) {
                e.preventDefault();
                resetResponderTableSection();
            });

            $('.tabs__list__item').on('click', function () {

                // if clicked on overview report tab
                // reset back to default

                if ($('#overview').is(':visible')) {
                    if (!$('#responder_report_link').hasClass('hidden')) {
                        $('#responder_report_link').addClass('hidden');
                    }
                    resetResponderTableSection();
                }

                // if clicked on responders report tab
                if ($('#responders-report').is(':visible')) {

                    if ($('#responder_report_link').hasClass('hidden')) {
                        $('#responder_report_link').removeClass('hidden');
                    }

                    SelectRespondersReport();
                }

                // if clicked on responders feedback tab
                if ($('#responders-feedback').is(':visible')) {
                    if ($('#responder_report_link').hasClass('hidden')) {
                        $('#responder_report_link').removeClass('hidden');
                    }

                    fetchFeedback();
                }
            });
        });

    </script>
</asp:Content>


