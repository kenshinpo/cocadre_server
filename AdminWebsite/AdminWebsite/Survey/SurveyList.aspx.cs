﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CassandraService.Entity;

namespace AdminWebsite.Survey
{
    public partial class SurveyList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;

        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        private List<CassandraService.Entity.RSTopic> rsTopicList = null;
        private List<CassandraService.Entity.Survey> surveyList = null;
        private CassandraService.Entity.RSTopic rsTopic = new CassandraService.Entity.RSTopic();
        private CassandraService.Entity.RSTopicCategory rsTopicCategory = new CassandraService.Entity.RSTopicCategory();

        private List<string> sortOrder = new List<string>();
        private SurveyListViewModel viewModel = new SurveyListViewModel();

        [Serializable]
        private class SurveyListViewModel
        {
            public string SurveyName { get; set; }
            public string SelectedCategoryId { get; set; }

            public SurveyListViewModel()
            {
                this.SelectedCategoryId = string.Empty;
                this.SurveyName = string.Empty;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                String categoryId = String.Empty;
                if (Page.RouteData.Values["CategoryId"] != null)
                {
                    categoryId = Page.RouteData.Values["CategoryId"].ToString();
                }


                ViewState["currentSortField"] = "lbSortSurveyTitle";
                ViewState["currentSortDirection"] = "asc";

                // Databind filters dropdownlists
                RSCategorySelectAllResponse selectAllRSCategoriesResponse = asc.SelectAllRSCategories(this.adminInfo.UserId, this.adminInfo.CompanyId, 1);
                if (selectAllRSCategoriesResponse.Success)
                {
                    this.ddlFilterCategory.DataSource = selectAllRSCategoriesResponse.Categories;
                    this.ddlFilterCategory.DataTextField = "Title";
                    this.ddlFilterCategory.DataValueField = "CategoryId";
                    this.ddlFilterCategory.DataBind();
                }
                ddlFilterCategory.SelectedValue = categoryId;

                // Load survey list
                RefreshSurveyListView();

            } // if (!Page.IsPostBack)

            tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");

        } // end of protected void Page_Load(object sender, EventArgs e)

        private void RefreshSurveyListView()
        {
            ListView lv = this.lvSurvey;
            LinkButton lb;

            // Fetch data and sort
            CassandraService.ServiceResponses.RSTopicSelectAllBasicResponse response =
                asc.SelectAllRSTopicByCategory(this.adminInfo.UserId, this.adminInfo.CompanyId,
                ddlFilterCategory.SelectedValue,
                tbFilterKeyWord.Text.Trim());

            if (response.Success)
            {
                this.lvSurvey.DataSource = response.Topics;
                this.lvSurvey.DataBind();

                switch (ViewState["currentSortField"].ToString())
                {
                    case "lbSortSurveyTitle":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderBy(r => r.Title).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderByDescending(r => r.Title).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;
                    case "lbSortSurveyCategory":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderBy(r => r.RSCategory.Title).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderByDescending(r => r.RSCategory.Title).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;

                    case "lbSortTotalNumberOfQuestions":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderBy(r => r.NumberOfCards).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderByDescending(r => r.NumberOfCards).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;
                    case "lbSortSurveyStatus":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderBy(r => r.Status).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderByDescending(r => r.Status).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;
                    case "lbSortSurveyProgress":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderBy(r => r.ProgressStatus).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderByDescending(r => r.ProgressStatus).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;

                    case "lbSortParticipants":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderBy(r => r.Status).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderByDescending(r => r.Status).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;
                    case "lbSortCompletionPct":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderBy(r => r.Progress.Percentage).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Topics.OrderByDescending(r => r.Progress.Percentage).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;
                }

            } // end if (response.Success)

            //ListView lv = this.lvSurveyCategory;
            //LinkButton lb;

            // Fetch data and sort
            //CassandraService.ServiceResponses.RSCategorySelectAllResponse response = asc.SelectAllRSCategories(this.adminInfo.UserId, this.adminInfo.CompanyId, 2);
            //if (response.Success)
            //{

            //this.lvSurveyCategory.DataSource = response.Categories;
            //this.lvSurveyCategory.DataBind();

            //switch (ViewState["currentSortField"].ToString())
            //{
            //    case "lbSortCategory":
            //        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
            //        if (lb != null)
            //        {
            //            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
            //            {
            //                this.lvSurveyCategory.DataSource = response.Categories.OrderBy(r => r.Title).ToList();
            //            }
            //            else
            //            {
            //                this.lvSurveyCategory.DataSource = response.Categories.OrderByDescending(r => r.Title).ToList();
            //            }
            //            this.lvSurveyCategory.DataBind();
            //        }
            //        break;
            //    case "lbSortSurveyCount":
            //        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
            //        if (lb != null)
            //        {
            //            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
            //            {
            //                this.lvSurveyCategory.DataSource = response.Categories.OrderBy(r => r.NumberOfRSTopics).ToList();
            //            }
            //            else
            //            {
            //                this.lvSurveyCategory.DataSource = response.Categories.OrderByDescending(r => r.NumberOfRSTopics).ToList();
            //            }
            //            this.lvSurveyCategory.DataBind();
            //        }
            //        break;
            //}
            //}

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }


        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                RSTopicUpdateResponse response = asc.UpdateRSTopicStatus(hfSurveyId.Value, hfCategoryId.Value, adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16
                    (lbAction.CommandArgument));
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    mpePop.Hide();
                    RefreshSurveyListView();

                    if (Convert.ToInt16(lbAction.CommandArgument) == 2)
                    {
                        toastMsg = hfSurveyName.Value + " has been set to active.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == 3)
                    {
                        toastMsg = hfSurveyName.Value + " has been set to hidden.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == -1)
                    {
                        toastMsg = hfSurveyName.Value + " has been deleted.";
                    }
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("RSTopicUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    mpePop.Show();
                    toastMsg = "Failed to change status, please check your internet connection.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbActionCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        protected void ddlFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshSurveyListView();
        }

        protected void ibFilterSurvey_Click(object sender, EventArgs e)
        {
            RefreshSurveyListView();
        }

        protected void lvSurvey_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Survey/Edit/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Analytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Survey/Analytic/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    LinkButton lbSurveyName = e.Item.FindControl("lbSurveyName") as LinkButton;
                    System.Web.UI.WebControls.Image imgSurvey = e.Item.FindControl("imgSurvey") as System.Web.UI.WebControls.Image;

                    hfSurveyId.Value = e.CommandArgument.ToString().Split(',')[0];
                    hfCategoryId.Value = e.CommandArgument.ToString().Split(',')[1];
                    hfSurveyName.Value = lbSurveyName.Text;

                    if (e.CommandName.Equals("Activate", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = "2";
                        ltlActionName.Text = "Activate ";
                        lbAction.Text = "Activate";
                        ltlActionMsg.Text = "Activate <b>" + lbSurveyName.Text + "</b>. Confirm?";

                    }
                    else if (e.CommandName.Equals("Hide", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = "3";
                        ltlActionName.Text = "Hide ";
                        lbAction.Text = "Hide";
                        ltlActionMsg.Text = "Hide <b>" + lbSurveyName.Text + "</b>. Confirm?";
                    }
                    else if (e.CommandName.Equals("DeleteSurvey", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = "-1";
                        ltlActionName.Text = "Delete ";
                        lbAction.Text = "Delete";
                        ltlActionMsg.Text = "All questions within <b>" + lbSurveyName.Text + "</b> will be gone. Confirm?";
                    }

                    imgActionSurvey.ImageUrl = imgSurvey.ImageUrl;
                    ltlActionSurveyTitle.Text = lbSurveyName.Text;
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvSurvey_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            this.RefreshSurveyListView();
        }

        protected void lvSurvey_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListView lv = sender as ListView;
            LinkButton headerLinkButton = null;
            string[] linkButtonControlList = new[] { "lbSortSurveyTitle", "lbSortSurveyCategory", "lbSortTotalNumberOfQuestions", "lbSortSurveyProgress", "lbSortSurveyStatus", "lbSortParticipants", "lbSortCompletionPct" };

            if (lv == null)
                return;

            // Remove the up-down arrows from each header
            foreach (string lbControlId in linkButtonControlList)
            {
                headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                if (headerLinkButton != null)
                {
                    headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                    headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                }
            }

            // Add sort direction back to the field in question
            headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
            if (headerLinkButton != null)
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                {
                    headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                }
                else
                {
                    headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                }
            }

            #region Progress
            LinkButton lbProgress = e.Item.FindControl("lbProgress") as LinkButton;
            if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].ProgressStatus == (int)RSTopic.ProgressStatusEnum.Upcoming)
            {
                lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(254, 204, 0, 1)'></i> Upcoming";
            }
            else if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].ProgressStatus == (int)RSTopic.ProgressStatusEnum.Live)
            {
                lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(148, 207, 0, 1)'></i> Live";
            }
            else if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].ProgressStatus == (int)RSTopic.ProgressStatusEnum.Completed)
            {
                lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(178, 178, 178, 1)'></i> Completed";
            }
            else
            {
                // do nothing
            }
            #endregion

            #region Status
            LinkButton lbStatus = e.Item.FindControl("lbStatus") as LinkButton;
            if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].Status == RSTopic.RSTopicStatus.CODE_UNLISTED)
            {
                lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Unlisted";
            }
            else if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].Status == RSTopic.RSTopicStatus.CODE_ACTIVE)
            {
                lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(0, 117, 254, 1)'></i> Active";
            }
            else if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].Status == RSTopic.RSTopicStatus.CODE_HIDDEN)
            {
                lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Hidden";
            }
            else
            {
                // do nothing
            }
            #endregion

            #region Participants
            LinkButton lbParticipants = e.Item.FindControl("lbParticipants") as LinkButton;
            if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].IsForEveryone)
            {
                lbParticipants.Text = "<i class='fa fa-globe' style='margin-right: 5px;'></i>Everyone";
            }
            else
            {
                if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].IsForDepartment)
                {
                    lbParticipants.Text += "<i class='fa fa-briefcase' style='margin-right: 5px;'></i>";
                }

                if (((List<RSTopic>)lvSurvey.DataSource)[e.Item.DataItemIndex].IsForUser)
                {
                    lbParticipants.Text += "<i class='fa fa-user' style='margin-right: 5px;'></i>";
                }

            }
            #endregion
        }// end protected void lvSurvey_ItemDataBound(object sender, ListViewItemEventArgs e)

        protected void lvSurvey_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.EmptyItem)
                {
                    Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                    if (ddlFilterCategory.SelectedIndex > 0 || !String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                    {
                        if (String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                        {
                            ltlEmptyMsg.Text = @"Filter """ + ddlFilterCategory.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                        }
                        else
                        {
                            ltlEmptyMsg.Text = @"Filter """ + tbFilterKeyWord.Text.Trim() + @""" + """ + ddlFilterCategory.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                        }
                    }
                    else
                    {
                        ltlEmptyMsg.Text = @"Welcome to Responsive Survey Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding a survey by clicking on the green button.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvSurvey_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }
    }
}