﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;
using CassandraService.GlobalResources;
using Amazon.S3;
using AdminWebsite.App_Code.Utilities.AWS;
using System.Globalization;

namespace AdminWebsite.Survey
{
    public partial class SurveyEdit : System.Web.UI.Page
    {
        private const int SURVEY_TITLE_TEXT_MAX = 100;
        private const int SURVEY_INTRODUCTION_TEXT_MAX = 250;
        private const int SURVEY_CLOSING_WORD_TEXT_MAX = 250;

        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private List<String> iconUrlList = null;
        private List<RSTopicCategory> categoryList = null;
        private List<User> searchUserList = null;
        private List<RSCard> cards = null;

        public void GetIconData()
        {
            TopicSelectIconResponse response = asc.SelectAllTopicIcons(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
            if (response.Success)
            {
                iconUrlList = response.TopicIconUrls;
            }
        }

        public void GetCategory()
        {
            RSCategorySelectAllResponse response = asc.SelectAllRSCategories(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), (int)CassandraService.Entity.RSTopicCategory.RSCategoryQueryType.Basic);
            if (response.Success)
            {
                categoryList = response.Categories;
            }
        }

        public void GetSearchUser()
        {
            try
            {
                UserListResponse response;
                if (String.IsNullOrEmpty(tbSearchKey.Text.Trim()))
                {
                    searchUserList = new List<User>();
                }
                else
                {
                    response = asc.SearchUser(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), tbSearchKey.Text.Trim());
                    if (response.Success)
                    {
                        List<User> selectedList = ViewState["selected_user"] as List<User>;
                        searchUserList = new List<User>();

                        for (int i = 0; i < selectedList.Count; i++)
                        {
                            User user = (from a in response.Users where a.UserId == selectedList[i].UserId select a).FirstOrDefault();
                            if (user != null)
                            {
                                response.Users.Remove(user);
                            }
                        }
                        searchUserList = response.Users;
                    }
                    else
                    {
                        Log.Error("SearchUserForPostingSuspend.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (Session["admin_info"] == null)
                {
                    Response.Redirect("/Logout", false);
                    return;
                }
                managerInfo = Session["admin_info"] as ManagerInfo;
                ViewState["manager_user_id"] = managerInfo.UserId;
                ViewState["company_id"] = managerInfo.CompanyId;

                this.Page.Form.DefaultButton = fakeSubmit.UniqueID;

                if (!IsPostBack)
                {
                    plNewQuestion.CssClass = "container animated fadeInDown";

                    tbSurveyTitle.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblSurveyTitleCount.ClientID, SURVEY_TITLE_TEXT_MAX);
                    tbSurveyIntroduction.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblSurveyIntroductionCount.ClientID, SURVEY_INTRODUCTION_TEXT_MAX);
                    tbSurveyClosingWords.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblSurveyClosingWordsCount.ClientID, SURVEY_CLOSING_WORD_TEXT_MAX);

                    #region Get category.
                    GetCategory();
                    cbCategory.Items.Clear();
                    if (categoryList != null)
                    {
                        for (int i = 0; i < categoryList.Count; i++)
                        {
                            cbCategory.Items.Add(new ListItem(categoryList[i].Title, Convert.ToString(categoryList[i].CategoryId)));
                        }
                    }
                    #endregion

                    tbSurveyTitle.MaxLength = SURVEY_TITLE_TEXT_MAX;
                    tbSurveyIntroduction.MaxLength = SURVEY_INTRODUCTION_TEXT_MAX;
                    tbSurveyClosingWords.MaxLength = SURVEY_CLOSING_WORD_TEXT_MAX;


                    this.hfAdminUserId.Value = ViewState["manager_user_id"].ToString();
                    this.hfCompanyId.Value = ViewState["company_id"].ToString();

                    if (Page.RouteData.Values["SurveyId"] != null && Page.RouteData.Values["CategoryId"] != null)
                    {
                        #region Edit Survey
                        this.hfTopicId.Value = Page.RouteData.Values["SurveyId"].ToString();
                        this.hfCategoryId.Value = Page.RouteData.Values["CategoryId"].ToString();

                        ltlActionName.Text = "Edit survey";
                        lbSurvey.Text = "Apply Changes";
                        lbReports.Visible = true;

                        RSTopicSelectResponse response = asc.SelectFullDetailRSTopic(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), Page.RouteData.Values["SurveyId"].ToString(), Page.RouteData.Values["CategoryId"].ToString());
                        if (response.Success)
                        {
                            ViewState["survey_id"] = Page.RouteData.Values["SurveyId"].ToString();
                            ViewState["category_id"] = Page.RouteData.Values["CategoryId"].ToString();

                            imgTopic.ImageUrl = response.Topic.IconUrl;
                            tbSurveyTitle.Text = response.Topic.Title;
                            lblSurveyTitleCount.Text = Convert.ToString(SURVEY_TITLE_TEXT_MAX - response.Topic.Title.Length);
                            tbSurveyIntroduction.Text = response.Topic.Introduction;
                            lblSurveyIntroductionCount.Text = Convert.ToString(SURVEY_INTRODUCTION_TEXT_MAX - response.Topic.Introduction.Length);
                            tbSurveyClosingWords.Text = response.Topic.ClosingWords;
                            lblSurveyClosingWordsCount.Text = Convert.ToString(SURVEY_CLOSING_WORD_TEXT_MAX - response.Topic.ClosingWords.Length);
                            cbParticipantsEveryone.Checked = response.Topic.IsForEveryone;
                            cbParticipantsDepartment.Checked = response.Topic.IsForDepartment;
                            cbParticipantsPersonnel.Checked = response.Topic.IsForUser;

                            #region Anonymous
                            cbIsAnonymous.Checked = response.Topic.IsAnonymous;
                            ddlAnonymityCount.ClearSelection();
                            foreach (ListItem item in ddlAnonymityCount.Items)
                            {
                                if(Convert.ToInt16(item.Value) == response.Topic.AnonymityCount)
                                {
                                    item.Selected = true;
                                }
                            }
                            #endregion

                            #region Start Date
                            ViewState["start_date"] = response.Topic.StartDate;
                            tbScheduleStartDate.Text = response.Topic.StartDate.ToString("dd/MM/yyyy");
                            tbScheduleStartHour.Text = response.Topic.StartDate.ToString("hh");
                            tbScheduleStartMinute.Text = response.Topic.StartDate.ToString("mm");
                            ddlScheduleStartMer.ClearSelection();
                            if (Convert.ToInt16(response.Topic.StartDate.ToString("HH")) > 12)
                            {
                                ddlScheduleStartMer.SelectedIndex = 1;
                            }
                            else
                            {
                                ddlScheduleStartMer.SelectedIndex = 0;
                            }
                            #endregion

                            #region End Date
                            ViewState["end_date"] = response.Topic.EndDate;
                            if (response.Topic.EndDate == null)
                            {
                                rbScheduleEndDateForever.Checked = true;
                            }
                            else
                            {
                                rbScheduleEndDateCustom.Checked = true;
                                tbScheduleEndDate.Text = response.Topic.EndDate.Value.ToString("dd/MM/yyyy");
                                tbScheduleEndHour.Text = response.Topic.EndDate.Value.ToString("hh");
                                tbScheduleEndMinute.Text = response.Topic.EndDate.Value.ToString("mm");
                                ddlScheduleEndMer.ClearSelection();
                                if (Convert.ToInt16(response.Topic.EndDate.Value.ToString("HH")) > 12)
                                {
                                    ddlScheduleEndMer.SelectedIndex = 1;
                                }
                                else
                                {
                                    ddlScheduleEndMer.SelectedIndex = 0;
                                }
                            }
                            #endregion

                            ViewState["selected_department"] = response.Topic.TargetedDepartments;
                            rtParticipantsDepartment.DataSource = response.Topic.TargetedDepartments;
                            rtParticipantsDepartment.DataBind();

                            ViewState["selected_user"] = response.Topic.TargetedUsers;
                            rtParticipantsPersonnel.DataSource = response.Topic.TargetedUsers;
                            rtParticipantsPersonnel.DataBind();

                            for (int i = 0; i < cbCategory.Items.Count; i++)
                            {
                                if (cbCategory.Items[i].Value.Equals(response.Topic.RSCategory.CategoryId))
                                {
                                    cbCategory.SelectedIndex = i;
                                    break;
                                }
                            }

                            if (response.Topic.Status == (int)RSTopic.RSTopicStatus.CODE_UNLISTED)
                            {
                                ddlStatus.Items.Add(new ListItem("Unlisted", "1"));
                                ddlStatus.Items.Add(new ListItem("Active", "2"));
                                ddlStatus.SelectedIndex = 0;
                            }
                            else if (response.Topic.Status == (int)RSTopic.RSTopicStatus.CODE_ACTIVE)
                            {
                                ddlStatus.Items.Add(new ListItem("Active", "2"));
                                ddlStatus.Items.Add(new ListItem("Hidden", "3"));
                                ddlStatus.SelectedIndex = 0;
                            }
                            else if (response.Topic.Status == (int)RSTopic.RSTopicStatus.CODE_HIDDEN)
                            {
                                ddlStatus.Items.Add(new ListItem("Active", "2"));
                                ddlStatus.Items.Add(new ListItem("Hidden", "3"));
                                ddlStatus.SelectedIndex = 1;
                            }
                            ViewState["status"] = response.Topic.Status;

                            plProgressStatus.Visible = true;
                            ViewState["progress_status"] = response.Topic.ProgressStatus;

                            if (response.Topic.ProgressStatus == (int)RSTopic.ProgressStatusEnum.Upcoming) // Upcoming
                            {
                                ltlProgressStatus.Text = "Upcoming";
                            }
                            else if (response.Topic.ProgressStatus == (int)RSTopic.ProgressStatusEnum.Live) // Live
                            {
                                ltlProgressStatus.Text = "Live";
                            }
                            else if (response.Topic.ProgressStatus == (int)RSTopic.ProgressStatusEnum.Completed) // Completed
                            {
                                ltlProgressStatus.Text = "Completed";
                            }
                            else// Other progress
                            {
                                ltlProgressStatus.Text = "Unknow";
                            }

                            // check status of RSTopic first, then check progress of RSTopic.
                            if (response.Topic.Status != RSTopic.RSTopicStatus.CODE_UNLISTED)
                            {
                                if (response.Topic.ProgressStatus == (int)RSTopic.ProgressStatusEnum.Live)
                                {
                                    cbCategory.Enabled = false;
                                    cbIsAnonymous.Enabled = false;
                                    ddlAnonymityCount.Enabled = false;
                                    cbParticipantsEveryone.Enabled = false;
                                    cbParticipantsDepartment.Enabled = false;
                                    cbParticipantsPersonnel.Enabled = false;
                                    lbAddParticipantsDepartment.Visible = false;
                                    tbSearchKey.Visible = false;
                                    tbScheduleStartDate.Enabled = false;
                                    tbScheduleStartHour.Enabled = false;
                                    tbScheduleStartMinute.Enabled = false;
                                    ddlScheduleStartMer.Enabled = false;
                                    lbAdd.Visible = false;
                                }
                                else if (response.Topic.ProgressStatus == (int)RSTopic.ProgressStatusEnum.Completed)
                                {
                                    lbAddIcon.Enabled = false;
                                    tbSurveyTitle.Enabled = false;
                                    tbSurveyIntroduction.Enabled = false;
                                    tbSurveyClosingWords.Enabled = false;
                                    cbCategory.Enabled = false;
                                    ddlStatus.Enabled = false;
                                    cbIsAnonymous.Enabled = false;
                                    ddlAnonymityCount.Enabled = false;
                                    cbParticipantsEveryone.Enabled = false;
                                    cbParticipantsDepartment.Enabled = false;
                                    cbParticipantsPersonnel.Enabled = false;
                                    lbAddParticipantsDepartment.Visible = false;
                                    tbSearchKey.Visible = false;
                                    tbScheduleStartDate.Enabled = false;
                                    tbScheduleStartHour.Enabled = false;
                                    tbScheduleStartMinute.Enabled = false;
                                    ddlScheduleStartMer.Enabled = false;
                                    lbAdd.Visible = false;
                                }
                            }

                            plSearchQuestion.Visible = true;

                            cards = response.Topic.Cards;
                            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                            string resultJson = javascriptSerializer.Serialize(response.Topic);
                            this.hfJsonStore.Value = resultJson;

                            if (response.Topic.ProgressStatus == (int)RSTopic.ProgressStatusEnum.Live && response.Topic.Status != (int)RSTopic.RSTopicStatus.CODE_UNLISTED)
                            {
                                // Show Alert Popup
                                mpePop.PopupControlID = "popup_alertpopup";
                                mpePop.Show();
                            }
                        }
                        else
                        {
                            Log.Warn("The SurveyId arg is invalid.");
                            Response.Redirect("/Survey/List", false);
                        }
                        #endregion
                    }
                    else
                    {
                        #region New Survey
                        ltlActionName.Text = "Create a survey";
                        lbSurvey.Text = "Create";
                        lblSurveyTitleCount.Text = Convert.ToString(SURVEY_TITLE_TEXT_MAX);
                        lblSurveyIntroductionCount.Text = Convert.ToString(SURVEY_INTRODUCTION_TEXT_MAX);
                        lblSurveyClosingWordsCount.Text = Convert.ToString(SURVEY_CLOSING_WORD_TEXT_MAX);
                        lbReports.Visible = false;
                        ViewState["selected_department"] = new List<Department>();
                        ViewState["selected_user"] = new List<User>();
                        imgTopic.ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/default_topic_icon.png";
                        ddlStatus.Items.Add(new ListItem("Unlisted", "1"));
                        ddlStatus.Items.Add(new ListItem("Active", "2"));
                        cbCategory.SelectedIndex = 0;
                        plSearchQuestion.Visible = false;
                        tbScheduleStartDate.Text = DateTime.Now.AddHours(managerInfo.TimeZone).ToString("dd/MM/yyyy");
                        this.hfJsonStore.Value = string.Empty;
                        #endregion
                    }

                }
                else
                {
                    plNewQuestion.CssClass = "container ";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                #region Icon data
                GetIconData();
                rtIcon.DataSource = iconUrlList;
                rtIcon.DataBind();
                #endregion
                mpePop.PopupControlID = "popup_addtopicicon";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = iconUrlList[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = iconUrlList[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpePop.Hide();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lbChooseIcon") as LinkButton;
                if (btn != null)
                {
                    scriptMan.RegisterAsyncPostBackControl(btn);
                }
            }
        }

        protected void lbAddParticipantsDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                DepartmentListResponse response = asc.GetAllDepartment(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
                if (response.Success)
                {
                    List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                    cblDepartment.Items.Clear();
                    for (int i = 0; i < response.Departments.Count; i++)
                    {
                        cblDepartment.Items.Add(new ListItem(response.Departments[i].Title, response.Departments[i].Id));
                        for (int j = 0; j < selectedDepartmentList.Count; j++)
                        {
                            if (response.Departments[i].Id.Equals(selectedDepartmentList[j].Id))
                            {
                                cblDepartment.Items[i].Selected = true;
                            }
                        }
                    }

                    mpePop.PopupControlID = "plSelectDepartment";
                    mpePop.Show();
                }
                else
                {
                    Log.Error("DepartmentListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void cbAllDepartment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbAllDepartment.Checked)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        cblDepartment.Items[i].Selected = true;
                    }
                }
                else
                {
                    cblDepartment.ClearSelection();
                }
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbSelectDepartmentSelect_Click(object sender, EventArgs e)
        {
            try
            {
                List<Department> selectedDepartmentList = new List<Department>();
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (cblDepartment.Items[i].Selected)
                    {
                        Department department = new Department();
                        department.Id = cblDepartment.Items[i].Value;
                        department.Title = cblDepartment.Items[i].Text;
                        selectedDepartmentList.Add(department);
                    }
                }
                ViewState["selected_department"] = selectedDepartmentList;
                rtParticipantsDepartment.DataSource = selectedDepartmentList;
                rtParticipantsDepartment.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void cblDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int selectedCount = 0;
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (cblDepartment.Items[i].Selected)
                    {
                        selectedCount++;
                    }
                }
                if (selectedCount != cblDepartment.Items.Count)
                {
                    cbAllDepartment.Checked = false;
                }
                else
                {
                    cbAllDepartment.Checked = true;
                }
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void rtParticipantsDepartment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                for (int i = 0; i < selectedDepartmentList.Count; i++)
                {
                    if (selectedDepartmentList[i].Id == Convert.ToString(e.CommandArgument))
                    {
                        selectedDepartmentList.RemoveAt(i);
                        break;
                    }
                }

                ViewState["selected_department"] = selectedDepartmentList;
                rtParticipantsDepartment.DataSource = selectedDepartmentList;
                rtParticipantsDepartment.DataBind();
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void tbSearchKey_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetSearchUser();
                rtSearchResult.DataSource = searchUserList;
                rtSearchResult.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddUser"))
                {
                    List<User> selectedList = ViewState["selected_user"] as List<User>;
                    String[] value = Convert.ToString(e.CommandArgument).Split(',');
                    //Dictionary<String, String> dic = new Dictionary<String, String>();
                    //dic.Add("UserId", value[0]);
                    //dic.Add("UserName", value[1]);
                    //selectedList.Add(dic);
                    User user = new CassandraService.Entity.User();
                    user.UserId = value[0];
                    user.FirstName = value[1];
                    user.LastName = value[2];
                    selectedList.Add(user);
                    rtParticipantsPersonnel.DataSource = selectedList;
                    rtParticipantsPersonnel.DataBind();
                    ViewState["SelectedUserId"] = selectedList;
                    tbSearchKey.Text = String.Empty;
                    GetSearchUser();
                    rtSearchResult.DataSource = searchUserList;
                    rtSearchResult.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }



        protected void rtSearchResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetFocus", "SetFocus();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtParticipantsPersonnel_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                List<User> selectedList = ViewState["selected_user"] as List<User>;
                if (selectedList == null || selectedList.Count == 0)
                {
                    Log.Error(@"ViewState[""SelectedUser""] is null.", this.Page);
                }
                else
                {
                    for (int i = 0; i < selectedList.Count; i++)
                    {
                        if (selectedList[i].UserId == Convert.ToString(e.CommandArgument))
                        {
                            selectedList.RemoveAt(i);
                            break;
                        }
                    }

                    ViewState["selected_user"] = selectedList;

                    rtParticipantsPersonnel.DataSource = selectedList;
                    rtParticipantsPersonnel.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSurvey_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Check input data
                #region Step 1.1 Title
                if (String.IsNullOrEmpty(tbSurveyTitle.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the Survey title.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Step 1.2 Introduction
                if (String.IsNullOrEmpty(tbSurveyIntroduction.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the Survey introduction.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Step 1.3 Closing words
                if (String.IsNullOrEmpty(tbSurveyClosingWords.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the Survey Closing words.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Step 1.4 Department of Participants
                List<String> targetedDepartmentIds = new List<String>();
                if (cbParticipantsDepartment.Checked)
                {
                    List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                    if (selectedDepartmentList.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please at least choose a department.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedDepartmentList.Count; i++)
                        {
                            targetedDepartmentIds.Add(selectedDepartmentList[i].Id);
                        }
                    }
                }
                #endregion

                #region Step 1.4 Personnel of Participants
                List<String> targetedUserIds = new List<String>();
                if (cbParticipantsPersonnel.Checked)
                {
                    List<User> selectedUserList = (List<User>)ViewState["selected_user"];
                    if (selectedUserList.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please at least choose a user.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedUserList.Count; i++)
                        {
                            targetedUserIds.Add(selectedUserList[i].UserId);
                        }
                    }
                }
                #endregion

                #region Step 1.5. Category
                GetCategory();
                String categoryID = String.Empty;
                if (cbCategory.SelectedItem == null)
                {
                    ShowToastMsgAndHideProgressBar("Please at least choose a category.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                String categoryTitle = cbCategory.SelectedItem.Text;
                for (int i = 0; i < categoryList.Count; i++)
                {
                    if (categoryList[i].Title.ToLower().Equals(cbCategory.SelectedItem.Text.Trim().ToLower()))
                    {
                        categoryID = categoryList[i].CategoryId;
                        break;
                    }
                }
                #endregion

                #region Step 1.6 Start date and End date, just for Edit mode.
                #region Check survey's status and progress
                if (ViewState["survey_id"] != null) // Edit mode
                {
                    if (Convert.ToInt16((ViewState["status"])) != RSTopic.RSTopicStatus.CODE_UNLISTED)
                    {
                        DateTime dtOriStartDate = Convert.ToDateTime(ViewState["start_date"]).AddHours(-managerInfo.TimeZone);
                      
                        int oriProgressStatus = 1;
                        if (dtOriStartDate.ToUniversalTime() < DateTime.UtcNow)
                        {
                            oriProgressStatus = 2;
                            if (ViewState["end_date"] != null)
                            {
                                DateTime dtOriEndDate = Convert.ToDateTime(ViewState["end_date"]).AddHours(-managerInfo.TimeZone);

                                if (dtOriEndDate.ToUniversalTime() < DateTime.UtcNow)
                                {
                                    oriProgressStatus = 3;
                                }
                            }
                        }

                        if (Convert.ToInt16(ViewState["progress_status"]) != oriProgressStatus) // Progress status has been changed, must reload page!
                        {
                            ShowToastMsgAndHideProgressBar("Progress status has been changed", MessageUtility.TOAST_TYPE_ERROR);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ReloadPage", "RedirectPage('/Survey/Edit/" + ViewState["SurveyId"].ToString() + "/" + ViewState["CategoryId"].ToString() + "', 300);", true);
                            return;
                        }
                    }
                }
                #endregion

                DateTime dtStart = new DateTime();
                try
                {
                    dtStart = DateTime.ParseExact(tbScheduleStartDate.Text + " " + tbScheduleStartHour.Text.PadLeft(2, '0') + ":" + tbScheduleStartMinute.Text.PadLeft(2, '0') + " " + ddlScheduleStartMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).AddHours(-managerInfo.TimeZone).ToUniversalTime();
                }
                catch (Exception ex)
                {
                    ShowToastMsgAndHideProgressBar("Start date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                DateTime? dtEnd = null;
                if (rbScheduleEndDateCustom.Checked)
                {
                    try
                    {
                        dtEnd = DateTime.ParseExact(tbScheduleEndDate.Text + " " + tbScheduleEndHour.Text.PadLeft(2, '0') + ":" + tbScheduleEndMinute.Text.PadLeft(2, '0') + " " + ddlScheduleEndMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).AddHours(-managerInfo.TimeZone).ToUniversalTime();
                        if (dtEnd < DateTime.UtcNow)
                        {
                            ShowToastMsgAndHideProgressBar("End date should not be early now", MessageUtility.TOAST_TYPE_ERROR);
                            return;
                        }

                        if (dtStart > dtEnd)
                        {
                            ShowToastMsgAndHideProgressBar("Your start and end dates does not tally", MessageUtility.TOAST_TYPE_ERROR);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowToastMsgAndHideProgressBar("End date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                }
                else // End date = forever
                {

                }
                #endregion

                #endregion

                #region Step 2. Call api
                if (ViewState["survey_id"] == null || String.IsNullOrEmpty(ViewState["survey_id"].ToString())) // Create
                {
                    RSTopicCreateResponse response = asc.CreateRSTopic(ViewState["manager_user_id"].ToString(),
                                                                        ViewState["company_id"].ToString(),
                                                                        tbSurveyTitle.Text.Trim(),
                                                                        tbSurveyIntroduction.Text.Trim(),
                                                                        tbSurveyClosingWords.Text.Trim(),
                                                                        categoryID, categoryTitle,
                                                                        imgTopic.ImageUrl,
                                                                        Convert.ToInt16(ddlStatus.SelectedValue),
                                                                        targetedDepartmentIds,
                                                                        targetedUserIds,
                                                                        dtStart,
                                                                        dtEnd,
                                                                        Convert.ToInt16(ddlAnonymityCount.SelectedValue)
                                                                        );

                    if (response.Success)
                    {
                        ShowToastMsgAndHideProgressBar("Survey has been created.", MessageUtility.TOAST_TYPE_INFO);
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Survey/Edit/" + response.Topic.TopicId + "/" + response.Topic.RSCategory.CategoryId + "',300);", true);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Survey has not been created.", MessageUtility.TOAST_TYPE_ERROR);
                        Log.Error("RSTopicCreateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
                else // Edit
                {
                    RSTopicUpdateResponse response = asc.UpdateRSTopic(ViewState["survey_id"].ToString(),
                                                                       ViewState["manager_user_id"].ToString(),
                                                                       ViewState["company_id"].ToString(),
                                                                       tbSurveyTitle.Text.Trim(), tbSurveyIntroduction.Text.Trim(),
                                                                       tbSurveyClosingWords.Text.Trim(),
                                                                       categoryID,
                                                                       categoryTitle,
                                                                       imgTopic.ImageUrl,
                                                                       Convert.ToInt16(ddlStatus.SelectedValue),
                                                                       targetedDepartmentIds,
                                                                       targetedUserIds,
                                                                       dtStart,
                                                                       dtEnd,
                                                                       Convert.ToInt16(ddlAnonymityCount.SelectedValue)
                                                                        );
                    if (response.Success)
                    {
                        ShowToastMsgAndHideProgressBar("Survey has been updated.", MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Survey has not been updated.", MessageUtility.TOAST_TYPE_ERROR);
                        Log.Error("RSTopicUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            try
            {
                RSTopicSelectResponse response = asc.SelectFullDetailRSTopic(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), ViewState["survey_id"].ToString(), ViewState["category_id"].ToString());
                if (response.Success)
                {
                    if (response.Topic.Cards.Count == 0)
                    {
                        ltlNewQuestionPaging.Text = "P1";
                        ltlNewCardOrdering.Text = "Q1";
                    }
                    else
                    {
                        if (response.Topic.Cards[response.Topic.Cards.Count - 1].HasPageBreak)
                        {
                            ltlNewQuestionPaging.Text = "P" + (response.Topic.Cards[response.Topic.Cards.Count - 1].Paging + 1);
                        }
                        else
                        {
                            ltlNewQuestionPaging.Text = "P" + response.Topic.Cards[response.Topic.Cards.Count - 1].Paging;
                        }
                        ltlNewCardOrdering.Text = "Q" + (response.Topic.Cards[response.Topic.Cards.Count - 1].Ordering + 1);
                    }
                    plNewQuestion.Visible = true;
                    plNewQuestion.CssClass = "container animated fadeInDown";
                    SetFocus("lbNewQuestionLocation");
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "resetNewCard", "resetNewCard();", true);
                }
                else
                {
                    Log.Error("RSTopicSelectResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbNewQuestionCancel_Click(object sender, EventArgs e)
        {
            try
            {
                plNewQuestion.CssClass = "container animated fadeOut";
                plNewQuestion.Visible = false;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbNewQuestionSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Get input(Json) from fornt-end
                SurveyCard sc = JsonConvert.DeserializeObject<SurveyCard>(hfNewCardJson.Value);
                String cardId = PrefixId.RSCard + Guid.NewGuid().ToString().Replace("-", "");
                #endregion

                #region Step 2. Check type of card
                Boolean hasContentImg = false;
                List<RSImage> contentImgs = new List<RSImage>();
                Boolean isAllowMultiLine = false; // unused
                List<RSOption> options = new List<RSOption>();

                #region Step 2.1. Upload content images of card to S3.(/cocadre-{CompanyId}/surveys/{SurveyId}/{CardId}/{index}.{imgFormat}) // contentImgs of card
                if (sc.contentImgs != null && sc.contentImgs.Count > 0)
                {
                    // for S3
                    String bucketName = "cocadre-" + ViewState["company_id"].ToString().ToLowerInvariant();
                    String folderName = "surveys/" + ViewState["survey_id"].ToString() + "/" + cardId;
                    IAmazonS3 s3Client = S3Utility.GetIAmazonS3(ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString());
                    S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                    String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
                    int imgWidth = 0, imgHeight = 0;

                    using (s3Client)
                    {
                        string filenamePrefix = "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                        for (int i = 0; i < sc.contentImgs.Count; i++)
                        {
                            // Grab image width and height
                            imgWidth = Convert.ToInt16(sc.contentImgs[i].width);
                            imgHeight = Convert.ToInt16(sc.contentImgs[i].height);
                            imgBase64String = sc.contentImgs[i].base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                            imgFormat = sc.contentImgs[i].extension.Replace("image/", "");

                            #region Upload original image
                            // Upload original image
                            Dictionary<String, String> metadatas = new Dictionary<string, string>();
                            byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                            System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                            String fileName = (i + 1) + filenamePrefix + "_original" + "." + imgFormat.ToLower();
                            metadatas.Clear();
                            metadatas.Add("Width", Convert.ToString(imgWidth));
                            metadatas.Add("Height", Convert.ToString(imgHeight));
                            S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            #endregion Upload original image

                            #region Upload large (if original image is 1920 * 1920)
                            // Upload large (if original image is 1920 * 1920)
                            System.Drawing.Image newImage;
                            if (imgWidth > 1920 || imgHeight > 1920)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                                fileName = (i + 1) + filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (i + 1) + filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload large (if original image is 1920 * 1920)

                            #region Upload medium (if original image is 1080 * 1080)
                            // Upload medium (if original image is 1080 * 1080)
                            if (imgWidth > 1080 || imgHeight > 1080)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                                fileName = (i + 1) + filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (i + 1) + filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload medium (if original image is 1080 * 1080)

                            #region Upload small (if original image is 540 * 540)
                            // Upload small (if original image is 540 * 540)
                            if (imgWidth > 540 || imgHeight > 540)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                                fileName = (i + 1) + filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (i + 1) + filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload small (if original image is 540 * 540)

                            // Get profile image url
                            imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + (i + 1) + filenamePrefix + "_original." + imgFormat.ToLower();

                            RSImage s3Image = new RSImage();
                            s3Image.ImageUrl = imageUrl;
                            s3Image.ImageId = string.Empty;
                            s3Image.Ordering = i + 1;
                            contentImgs.Add(s3Image);
                        }
                    }
                    hasContentImg = true;
                }
                #endregion

                #region Step 2.2 CardType
                if (sc.type == 1 || sc.type == 2) // Select one || Multi choice
                {
                    #region 2.3 Options
                    for (int i = 0; i < sc.options.Count; i++)
                    {
                        RSOption option = new RSOption();
                        option.Content = sc.options[i].contentText;
                        option.HasImage = false;
                        option.Ordering = i + 1;
                        option.OptionId = PrefixId.RSOption + Guid.NewGuid().ToString().Replace("-", "");
                        option.Images = new List<RSImage>();

                        #region Step 2.4 (/cocadre-{CompanyId}/surveys/{SurveyId}/{CardId}/{optionId}/{index}_{timestamp}_{original}.{imgFormat}) // contentImg of option
                        if (!String.IsNullOrEmpty(sc.options[i].contentImg.base64))
                        {
                            // 以後要改寫成 multi images of one option !。目前一個 option 只有一張圖。
                            // for S3
                            String bucketName = "cocadre-" + ViewState["company_id"].ToString().ToLowerInvariant();
                            String folderName = "surveys/" + ViewState["survey_id"].ToString() + "/" + cardId + "/" + option.OptionId;
                            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString());
                            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                            String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
                            int imgWidth = 0, imgHeight = 0;

                            using (s3Client)
                            {
                                //for (int k = 0; k < sc.options[i].contentImgs.count ; k++) // 以後要改寫成 multi images of one option !。目前一個 option 只有一張圖。
                                //{
                                //    
                                //}

                                string filenamePrefix = "1_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                                // Grab image width and height
                                imgWidth = Convert.ToInt16(sc.options[i].contentImg.width);
                                imgHeight = Convert.ToInt16(sc.options[i].contentImg.height);
                                imgBase64String = sc.options[i].contentImg.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                                imgFormat = sc.options[i].contentImg.extension.Replace("image/", "");

                                #region Upload original image
                                // Upload original image
                                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                                String fileName = filenamePrefix + "_original" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(imgWidth));
                                metadatas.Add("Height", Convert.ToString(imgHeight));
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                                #endregion Upload original image

                                #region Upload large (if original image is 1920 * 1920)
                                // Upload large (if original image is 1920 * 1920)
                                System.Drawing.Image newImage;
                                if (imgWidth > 1920 || imgHeight > 1920)
                                {
                                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                    metadatas.Clear();
                                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                                }
                                else
                                {
                                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                                }
                                #endregion Upload large (if original image is 1920 * 1920)

                                #region Upload medium (if original image is 1080 * 1080)
                                // Upload medium (if original image is 1080 * 1080)
                                if (imgWidth > 1080 || imgHeight > 1080)
                                {
                                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                    metadatas.Clear();
                                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                                }
                                else
                                {
                                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                                }
                                #endregion Upload medium (if original image is 1080 * 1080)

                                #region Upload small (if original image is 540 * 540)
                                // Upload small (if original image is 540 * 540)
                                if (imgWidth > 540 || imgHeight > 540)
                                {
                                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                    metadatas.Clear();
                                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                                }
                                else
                                {
                                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                                }
                                #endregion Upload small (if original image is 540 * 540)

                                // Get profile image url
                                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + imgFormat.ToLower();

                                RSImage s3Image = new RSImage();
                                s3Image.ImageUrl = imageUrl;
                                s3Image.ImageId = string.Empty;
                                s3Image.Ordering = 1;
                                option.Images.Add(s3Image);
                            }

                            option.HasImage = true;
                        }
                        #endregion
                        options.Add(option);
                    }
                    #endregion
                }
                else if (sc.type == 3 || sc.type == 6) // Text || Instructions
                {
                    if(sc.type == 6)
                    {
                        sc.isAllowSkipped = false;
                    }
                    sc.isTickOtherAnswer = false;
                    sc.customCommand = null;
                    sc.isRandomOption = false;
                    options = null;
                    sc.optionRangeMax = 0;
                    sc.optionRangeMin = 0;
                }
                else if (sc.type == 4) // Number range 
                {
                    sc.isTickOtherAnswer = false;
                    sc.customCommand = null;
                    sc.isRandomOption = false;
                    options = null;

                }
                else if (sc.type == 5) // Drop list 
                {
                    #region Options
                    for (int i = 0; i < sc.options.Count; i++)
                    {
                        RSOption option = new RSOption();
                        option.Content = sc.options[i].contentText;
                        option.HasImage = false;
                        option.Ordering = i + 1;
                        option.OptionId = PrefixId.RSOption + Guid.NewGuid().ToString().Replace("-", "");
                        option.Images = new List<RSImage>();
                        options.Add(option);
                    }
                    #endregion
                }
                else
                {
                    // nothing
                }
                #endregion

                #endregion

                RSCardCreateResponse response = asc.CreateCard(cardId,
                                                               sc.type,
                                                               sc.contentText,
                                                               hasContentImg,
                                                               contentImgs,
                                                               sc.isAllowSkipped,
                                                               sc.isTickOtherAnswer,
                                                               sc.customCommand,
                                                               sc.isRandomOption,
                                                               isAllowMultiLine,
                                                               sc.isPageBreak,
                                                               sc.diplayBg,
                                                               sc.note,
                                                               ViewState["category_id"].ToString(),
                                                               ViewState["survey_id"].ToString(),
                                                               ViewState["manager_user_id"].ToString(),
                                                               ViewState["company_id"].ToString(),
                                                               options,
                                                               sc.minOptions,
                                                               sc.maxOptions,
                                                               sc.optionRangeStartFrom,
                                                               sc.optionRangeMax,
                                                               sc.optionRangeMaxLabel,
                                                               sc.optionRangeMiddle,
                                                               sc.optionRangeMiddleLabel,
                                                               sc.optionRangeMin,
                                                               sc.optionRangeMinLabel);

                if (response.Success)
                {
                    plNewQuestion.CssClass = "container animated fadeInDown";
                    ShowToastMsgAndHideProgressBar("Survey question created", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "resetNewCard", "resetNewCard();", true);

                    RSTopicSelectResponse responseQQ = asc.SelectFullDetailRSTopic(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), ViewState["survey_id"].ToString(), ViewState["category_id"].ToString());
                    if (responseQQ.Success)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        string resultJson = javascriptSerializer.Serialize(responseQQ.Topic);
                        this.hfJsonStore.Value = resultJson;
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "refreshListDisplay", "refreshListDisplay();", true);

                        // Check last card
                        RSCard card = responseQQ.Topic.Cards.LastOrDefault();
                        if (card != null)
                        {
                            int page_number;
                            int question_number;

                            if (card.HasPageBreak)
                            {
                                page_number = card.Paging + 1;

                            }
                            else
                            {
                                page_number = card.Paging;

                            }
                            question_number = card.Ordering + 1;

                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "updateNewCardNumber", "updateNewCardNumber(" + page_number.ToString() + ", " + question_number.ToString() + ");", true);
                        }
                    }
                }
                else
                {
                    Log.Error("RSCardCreateResponse.Success is flase. ErrorMessage: " + response.ErrorMessage);
                    ShowToastMsgAndHideProgressBar("Creation failed", MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtCards_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Literal ltlMinCardPaging = e.Item.FindControl("ltlMinCardPaging") as Literal;
                    Literal ltlMinCardOrdering = e.Item.FindControl("ltlMinCardOrdering") as Literal;
                    LinkButton lbMinShowCard = e.Item.FindControl("lbMinShowCard") as LinkButton;
                    System.Web.UI.WebControls.Image imgMinCardContentImg = e.Item.FindControl("imgMinCardContentImg") as System.Web.UI.WebControls.Image;
                    Literal ltlMinCardContents = e.Item.FindControl("ltlMinCardContents") as Literal;
                    Literal ltlMinCardType = e.Item.FindControl("ltlMinCardType") as Literal;
                    Literal ltlMinCardLogicTo = e.Item.FindControl("ltlMinCardLogicTo") as Literal;
                    Literal ltlMinCardLogicFrom = e.Item.FindControl("ltlMinCardLogicFrom") as Literal;

                    ltlMinCardPaging.Text = "P" + cards[e.Item.ItemIndex].Paging;
                    ltlMinCardOrdering.Text = "Q" + cards[e.Item.ItemIndex].Ordering;
                    lbMinShowCard.CommandArgument = cards[e.Item.ItemIndex].CardId;
                    if (cards[e.Item.ItemIndex].CardImages.Count > 0)
                    {
                        imgMinCardContentImg.ImageUrl = cards[e.Item.ItemIndex].CardImages[0].ImageUrl;
                    }
                    else
                    {
                        imgMinCardContentImg.Visible = false;
                    }
                    ltlMinCardContents.Text = cards[e.Item.ItemIndex].Content;

                    ltlMinCardType.Text = cards[e.Item.ItemIndex].Type + ""; // change image
                    ltlMinCardLogicTo.Text = "To: " + cards[e.Item.ItemIndex].HasToLogic; // change image
                    ltlMinCardLogicFrom.Text = "From: " + cards[e.Item.ItemIndex].HasFromLogic; // change image



                    Literal ltlCardId = e.Item.FindControl("ltlCardId") as Literal;
                    ltlCardId.Text = cards[e.Item.ItemIndex].CardId;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtCards_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ShowCard"))
                {
                    RSCardSelectResponse response = asc.SelectFullDetailCard(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), e.CommandArgument.ToString(), ViewState["survey_id"].ToString());
                    if (response.Success)
                    {
                        Panel plFullContent = e.Item.FindControl("plFullContent") as Panel;
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toggle", "$('#" + plFullContent.ClientID + "').toggle();", true);

                        Literal ltlMaxCardContentText = e.Item.FindControl("ltlMaxCardContentText") as Literal;
                        ltlMaxCardContentText.Text = response.Card.Content;
                    }
                    else
                    {

                    }
                }

                if (e.CommandName.Equals("DeleteQuestion", StringComparison.InvariantCultureIgnoreCase))
                {

                    RSCardUpdateResponse response = asc.DeleteCard(
                        ViewState["survey_id"].ToString(),
                        ViewState["category_id"].ToString(),
                        //"cardId", 
                        e.CommandArgument.ToString(),
                        managerInfo.UserId,
                        managerInfo.CompanyId);

                    if (response.Success)
                    {
                        RSTopicSelectResponse rsTopicSelectResponse = asc.SelectFullDetailRSTopic(
                            ViewState["manager_user_id"].ToString(),
                            ViewState["company_id"].ToString(),
                            Page.RouteData.Values["SurveyId"].ToString(),
                            Page.RouteData.Values["CategoryId"].ToString());

                        if (rsTopicSelectResponse.Success)
                        {
                            //adCards.DataSource = rsTopicSelectResponse.Topic.Cards;
                            //adCards.DataBind();
                        }
                    }
                    else
                    {
                        // TODO: show toast that delete failed

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbPreview_Click(object sender, EventArgs e)
        {
            try
            {
                // TODO: grab data here
                // ViewState["survey_id"]
                // ViewState["category_id"]
                RSCardSelectAllResponse response =
                    asc.Preview(
                        ViewState["survey_id"].ToString(),
                        ViewState["category_id"].ToString(),
                        managerInfo.UserId,
                        managerInfo.CompanyId);

                if (response.Success)
                {
                    System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    //response.Topic.Cards[0].Options.Add(opt);
                    string resultJson = javascriptSerializer.Serialize(response);
                    this.hfLiveSurveyPreviewJson.Value = resultJson;

                    mpePop.PopupControlID = "popup_livesurveypreview";
                    mpePop.Show();
                }
                else
                {
                    // Show toast notification indicating that there is error.
                    MessageUtility.ShowToast(this.Page, "Error retrieving data for live preview. " + response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void lbReports_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(string.Format("/Survey/Analytic/{0}", ViewState["survey_id"].ToString() + "/" + ViewState["category_id"].ToString()));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void fakeSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Log.Debug("hehe");
            }
            catch (Exception ex)
            {

            }
        }
    }
}