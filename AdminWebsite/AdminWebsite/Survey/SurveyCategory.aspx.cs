﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;

namespace AdminWebsite.Survey
{
    public partial class SurveyCategory : Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                ViewState["currentSortField"] = "lbSortCategory";
                ViewState["currentSortDirection"] = "asc";
                RefreshSurveyCategoryListView();
            }
        }

        private void RefreshSurveyCategoryListView()
        {
            ListView lv = this.lvSurveyCategory;
            LinkButton lb;

            // Fetch data and sort
            CassandraService.ServiceResponses.RSCategorySelectAllResponse response = asc.SelectAllRSCategories(this.adminInfo.UserId, this.adminInfo.CompanyId, 2);
            if (response.Success)
            {
                this.lvSurveyCategory.DataSource = response.Categories;
                this.lvSurveyCategory.DataBind();

                switch (ViewState["currentSortField"].ToString())
                {
                    case "lbSortCategory":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurveyCategory.DataSource = response.Categories.OrderBy(r => r.Title).ToList();
                            }
                            else
                            {
                                this.lvSurveyCategory.DataSource = response.Categories.OrderByDescending(r => r.Title).ToList();
                            }
                            this.lvSurveyCategory.DataBind();
                        }
                        break;
                    case "lbSortSurveyCount":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurveyCategory.DataSource = response.Categories.OrderBy(r => r.NumberOfRSTopics).ToList();
                            }
                            else
                            {
                                this.lvSurveyCategory.DataSource = response.Categories.OrderByDescending(r => r.NumberOfRSTopics).ToList();
                            }
                            this.lvSurveyCategory.DataBind();
                        }
                        break;
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        } // end private void RefreshSurveyCategoryListView()

        #region event handlers for all the popups dialogs

        public void lbPopCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        public void lbAddDone_Click(object sender, EventArgs e)
        {
            // User input validation
            if (string.IsNullOrWhiteSpace(tbAddName.Text))
                return;

            try
            {
                CassandraService.ServiceResponses.RSCategoryCreateResponse response =
                    asc.CreateRSCategory(adminInfo.CompanyId, tbAddName.Text.Trim(), adminInfo.UserId);

                if (response.Success)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview
                    // Rebind listview
                    RefreshSurveyCategoryListView();

                    // Display toast
                    MessageUtility.ShowToast(this.Page, tbAddName.Text + " has been created.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void lbRenameSave_Click(object sender, EventArgs e)
        {
            // User input validation
            if (string.IsNullOrWhiteSpace(tbRenameName.Text))
            {
                MessageUtility.ShowToast(this.Page, "Please enter category title. ", MessageUtility.TOAST_TYPE_ERROR);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                return;
            }   

            try
            {
                // This should be update
                CassandraService.ServiceResponses.RSCategoryUpdateResponse response = asc.UpdateRSCategory(adminInfo.UserId, adminInfo.CompanyId, lbRenameSave.CommandArgument, tbRenameName.Text.Trim());

                if (response.Success)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview, Rebind listview
                    RefreshSurveyCategoryListView();

                    // Display toast
                    MessageUtility.ShowToast(this.Page, tbRenameName.Text + " has been renamed.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, tbRenameName.Text + " has not been renamed.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void lbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // This should be delete
                CassandraService.ServiceResponses.RSCategoryUpdateResponse response = asc.DeleteRSCategory(adminInfo.UserId, adminInfo.CompanyId, this.hfCategoryId.Value);

                if (response.Success)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview, Rebind listview
                    RefreshSurveyCategoryListView();

                    // Display toast
                    MessageUtility.ShowToast(this.Page, this.hfCategoryName.Value + " has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, "Delete category failed.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        #endregion event handlers for all the popups dialogs

        #region event handler for green button
        protected void lbOpenAddCategory_Click(object sender, EventArgs e)
        {
            try
            {
                tbAddName.Text = String.Empty;
                mpePop.PopupControlID = "popup_addcategory";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                // Log.Error(ex.ToString(), ex, this.Page);
                Log.Error(ex.ToString(), ex);
            }
        }
        #endregion event handler for green button

        protected void lvSurveyCategory_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            this.hfCategoryId.Value = e.CommandArgument.ToString();

            try
            {
                LinkButton lbSurveyTitle = e.Item.FindControl("lbSurveyTitle") as LinkButton;
                if (e.CommandName.Equals("Rename"))
                {
                    // Configure display 
                    tbRenameName.Text = lbSurveyTitle.Text;

                    lbRenameSave.CommandArgument = e.CommandArgument.ToString();

                    // Display popup
                    mpePop.PopupControlID = "popup_renamecategory";
                    mpePop.Show();
                }
                else if (e.CommandName.Equals("Remove"))
                {
                    HyperLink hlTopicCount = e.Item.FindControl("hlTopicCount") as HyperLink;
                    if (Convert.ToInt16(hlTopicCount.Text) > 0)
                    {
                        // Configure display
                        ltlDeleteMsg.Text = @"<p class='error'>There are still topic(s) left in the category!<br /> For satety reasons we can only allow you to delete empty category.</p><p><img src='/img/tips_icon.png' width='16px' height='20px' /> You can move the topic to other category in the <a href='/Survey/List/" + e.CommandArgument.ToString() + "'>topic console</a> manager.</p>";
                        ltlDeleteMsg.Text = "<p>Are you sure you want to delete this category : <b>" + lbSurveyTitle.Text + "</b> ?</p>";
                        lbDelete.Visible = false;
                        lbDelCancel.Text = "Exit";
                    }
                    else
                    {
                        // Configure display
                        ltlDeleteMsg.Text = "<p>Are you sure you want to delete this category : <b>" + lbSurveyTitle.Text + "</b> ?</p>";
                        lbDelete.Visible = true;
                        lbDelCancel.Text = "Cancel";

                        lbDelete.CommandArgument = e.CommandArgument.ToString();
                        hfCategoryName.Value = lbSurveyTitle.Text;
                    }

                    // Display popup
                    mpePop.PopupControlID = "popup_deletecategory";
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvSurveyCategory_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListView lv = sender as ListView;
            LinkButton headerLinkButton = null;
            string[] linkButtonControlList = new[] { "lbSortCategory", "lbSortSurveyCount" };

            if (lv == null)
                return;

            // Remove the up-down arrows from each header
            foreach (string lbControlId in linkButtonControlList)
            {
                headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                if (headerLinkButton != null)
                {
                    headerLinkButton.Text = headerLinkButton.Text.Replace("<i class=\"fa fa-sort-asc\"></i>", string.Empty);
                    headerLinkButton.Text = headerLinkButton.Text.Replace("<i class=\"fa fa-sort-desc\"></i>", string.Empty);
                }
            }

            // Add sort direction back to the field in question
            headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
            if (headerLinkButton != null)
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                {
                    headerLinkButton.Text = headerLinkButton.Text + "<i class=\"fa fa-sort-asc\"></i>";
                }
                else
                {
                    headerLinkButton.Text = headerLinkButton.Text + "<i class=\"fa fa-sort-desc\"></i>";
                }
            }
        }

        protected void lvSurveyCategory_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            RefreshSurveyCategoryListView();
        }
    }
}
