﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SurveyAnalyticDetail.aspx.cs" Inherits="AdminWebsite.Survey.SurveyAnalyticDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .card-full .grid-overall
        {
            width: 9%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
     <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><a href="/Survey/List" style="color:#000;">Responsive <span>survey</span></a></div>
        <div class="appbar__meta"><a href="" id="survey-root-link"></a></div>
        <div class="appbar__meta"><a href="" id="simple-survey-link">Overview</a></div>
        <div class="appbar__meta"><a href="" id="responder_report_link" class="hidden">Responders report</a></div>
        <div class="appbar__meta breadcrumb-participant" ></div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->


    <div id="survey-wrapper" class="data" style="overflow-y:scroll;">

        <div id="responsive-survey" class="container" style="width:100%;">

            <div style="width:100%; margin:0 auto;">

                <div class="title-section" style="width:80%; margin: 0 auto;">
                    <div class="survey-info">
                        <h1><img id="survey-icon" src="" /><span id="survey-topic" class="ellipsis-title"></span></h1>
                        <p class="align-left">Showing data from <span id="survey-start-date"></span> - <span id="survey-end-date"></span></p>
                    </div>
                    <div class="user-info hidden">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span id="user-name" class="list-value"></span>
                                Name:
                            </li>
                            <li class="list-group-item">
                                <span id="user-department" class="list-value"></span>
                                Department:
                            </li>
                            <li class="list-group-item">
                                <span id="user-status" class="list-value"></span>
                                Status:
                            </li>
                            <li class="list-group-item">
                                <span id="user-last-update" class="list-value"></span>
                                Last Update:
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>

                <!-- SELECTED SURVEY RESULT FROM SEE MORE -->
                <div id="single-survey-table-wrapper" class="card card-full animated fadeInUp no-padding">
                </div>

            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script src="/js/vendor/moment-with-locales.js"></script>
    <script src="/Js/Chart.js"></script>

    <script>
        $(function () {

            //function getUrlParameter(sParam) {
            //    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            //        sURLVariables = sPageURL.split('&'),
            //        sParameterName,
            //        i;

            //    for (i = 0; i < sURLVariables.length; i++) {
            //        sParameterName = sURLVariables[i].split('=');

            //        if (sParameterName[0] === sParam) {
            //            return sParameterName[1] === undefined ? true : sParameterName[1];
            //        }
            //    }
            //};

            //var CompanyId = getUrlParameter('CompanyId');
            //var AdminUserId = getUrlParameter('AdminUserId');
            //var CategoryId = getUrlParameter('CategoryId');
            //var TopicId = getUrlParameter('TopicId');
            //var cardId = getUrlParameter('CardId');

            var single_table_html = [];
            var isAnonymous = false;
            $('#simple-survey-link').attr('href', '/Survey/Analytic/' + TopicId + "/" + CategoryId);


            function fetchOverview() {
                
                $.ajax({
                    type: "POST",
                    url: '/adminrs/selectresultoverview',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "CategoryId": CategoryId,
                        "TopicId": TopicId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {

                        if (res.Success) {

                            var summary = res.Overview.Summary;
                            var personnel_completion = res.Overview.PersonnelCompletion;
                            var report = res.Overview.Report;
                            var report_arr = res.Overview.Report.ReportList;
                            var topic = res.Overview.Topic;
                            var starting_month = moment(new Date(report_arr[report_arr.length - 1].DatestampString)).format("MMM");
                            var ending_month = moment(new Date(report_arr[0].DatestampString)).format("MMM");
                            $('#survey-topic').html(topic.Title);
                            $('#survey-icon').attr('src', topic.IconUrl);

                            if (report_arr.length > 0) {
                                $('#survey-start-date').html(report_arr[0].DatestampString);
                                $('#start_date').html(report_arr[0].DatestampString);
                                $('#survey-end-date').html(report_arr[report_arr.length - 1].DatestampString);
                            }

                        }
                    }
                })
            }

            function fetchData() {

                ShowProgressBar();

                $.ajax({
                    type: "POST",
                    url: '/adminRS/SelectCustomAnswersResult',
                    data: {
                        "CompanyId": CompanyId,
                        "AdminUserId": AdminUserId,
                        "CategoryId": CategoryId,
                        "TopicId": TopicId,
                        "CardId": CardId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {

                            $('.breadcrumb-participant').html(res.Card.CardId);

                            $('#single-survey-table-wrapper').removeClass('hidden animated fadeOut');
                            $('#single-survey-table-wrapper').css('display', 'table');
                            $('#single-survey-table-wrapper').addClass('animated fadeInUp');

                            single_table_data = [];
                            single_table_html = "";

                            single_table_html += '<div class="card-header">';
                            single_table_html += '<div class="grid-question-number">';
                            single_table_html += '<div style="color:#ddd;">';
                            single_table_html += '<div class="number">P' + res.Card.Paging + ' | Q' + res.Card.Ordering;
                            single_table_html += '</div>';
                            single_table_html += '<div class="code">Code: ' + res.Card.CardId + '</div>';
                            single_table_html += '</div> ';
                            single_table_html += '</div>';
                            if (res.Card.HasImage) {
                                single_table_html += '<div class="question-image" style="width: 100px; height: auto; background: #fff; float: left;margin-right: 5px;margin-top: 10px;"><img src="' + res.Card.CardImages[0].ImageUrl + '" /></div>';
                            }
                            single_table_html += '<div class="grid-question">';
                            single_table_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Question</label>';
                            single_table_html += ' <p class="question">' + res.Card.Content + '</p>';
                            single_table_html += '</div>';
                            single_table_html += '<div class="grid-type align-center">';
                            single_table_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Type</label>';
                            single_table_html += '<div class="type-image" style="width:25px; height:25px; margin:0 auto;"><img src="/Img/icon_text.png" /></div>';
                            single_table_html += '</div>';
                            single_table_html += '<div class="grid-overall">';
                            single_table_html += '<small>Out of</small>';
                            single_table_html += '<label class="response_number">' + res.Card.TotalResponses + '</label>';
                            single_table_html += '<small>responses</small>';
                            single_table_html += '</div>';
                            single_table_html += '</div>';
                            single_table_html += ' <div class="table-content">';
                            single_table_html += ' <table id="single-survey-table">';
                            single_table_html += '<thead>';
                            single_table_html += '<th data-dynatable-column="no" style="width:10%">No</th>';
                            single_table_html += '<th data-dynatable-column="responses" data-dynatable-sorts="responses" style="width:50%">Responses</th>';

                            if (!isAnonymous) {
                                single_table_html += '<th data-dynatable-column="name" data-dynatable-sorts="name" style="width:20%">Name</th>';
                                single_table_html += '<th data-dynatable-column="department" data-dynatable-sorts="department" style="width:20%">Department</th>';
                            }

                            single_table_html += '</thead>';
                            single_table_html += '<tbody></tbody>';
                            single_table_html += '</table>';
                            single_table_html += ' </div>';

                            $("#single-survey-table-wrapper").empty();
                            $("#single-survey-table-wrapper").append(single_table_html);

                            if (res.Card.CustomAnswers.length > 0) {

                                if (!isAnonymous) {
                                    $.each(res.Card.CustomAnswers, function (key, value) {
                                        single_table_data.push({
                                            "no": key + 1,
                                            "responses": value.Content,
                                            "name": value.AnsweredByUser.FirstName + " " + value.AnsweredByUser.LastName,
                                            "department": value.AnsweredByUser.Departments[0].Title
                                        });

                                    });
                                } else {
                                    $.each(res.Card.CustomAnswers, function (key, value) {
                                        single_table_data.push({
                                            "no": key + 1,
                                            "responses": value.Content
                                        });
                                    });
                                }

                                var single_survey_table = $('#single-survey-table').dynatable({
                                    features: {
                                        paginate: false,
                                        search: false,
                                        sorting: true,
                                        recordCount: false,
                                        pushState: false,
                                    },
                                    dataset: {
                                        records: single_table_data,
                                        sorts: { 'no': 1 }
                                    }
                                }).data('dynatable');

                                single_survey_table.settings.dataset.originalRecords = single_table_data;
                                single_survey_table.process();                                

                                var dynatable = $('#single-survey-table').data('dynatable');

                                if (typeof dynatable.records !== "undefined") {
                                    dynatable.records.updateFromJson({ records: single_table_data });
                                    dynatable.records.init();
                                }
                                dynatable.paginationPerPage.set(15);
                                dynatable.process();

                                dynatable.dom.update();
                            }
                        }
                    }
                });
            };

            fetchOverview();
            fetchData();
        })
    </script>
</asp:Content>
