﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.Dashboard
{
    public partial class ProfilePhotoApproval : Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();
        private List<ProfileImageApproval> photoList;

        public void GetProfileImageApprovalList()
        {
            try
            {
                DashboardSelectApprovalProfileResponse response = asc.SelectProfileApprovalResponse(adminInfo.UserId, adminInfo.CompanyId);
                if (response.Success)
                {
                    photoList = response.ProfileImageApprovals;
                    if (photoList.Count > 0)
                    {
                        ltlCount.Text = "(" + photoList.Count + ")";
                    }
                    else
                    {
                        ltlCount.Text = String.Empty;
                    }
                }
                else
                {
                    Log.Error("CategorySelectAllResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    GetProfileImageApprovalList();
                    rtPhotos.DataSource = photoList;
                    rtPhotos.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbReject_Click(object sender, EventArgs e)
        {
            try
            {
                DashboardUpdateApprovalProfileResponse response = asc.UpdateProfileApprovalStatus(adminInfo.UserId, adminInfo.CompanyId, hfApprovalId.Value, (int)CassandraService.Entity.Dashboard.ProfileApprovalState.Reject); ;
                if (response.Success)
                {
                    mpePop.Hide();
                    GetProfileImageApprovalList();
                    rtPhotos.DataSource = photoList;
                    rtPhotos.DataBind();
                    MessageUtility.ShowToast(this.Page, ltlUserName.Text + " profile photo has been rejected.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, ltlUserName.Text + " profile photo has not been rejected.", MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtPhotos_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                Literal ltlName = e.Item.FindControl("ltlName") as Literal;

                if (e.CommandName.Equals("Approve"))
                {
                    DashboardUpdateApprovalProfileResponse response = asc.UpdateProfileApprovalStatus(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument), (int)CassandraService.Entity.Dashboard.ProfileApprovalState.Approved); ;
                    if (response.Success)
                    {
                        if (response.ApprovedUser.UserId == adminInfo.UserId)
                        {
                            adminInfo.ProfileImageUrl = response.ApprovedUser.ProfileImageUrl;
                            Session["admin_info"] = adminInfo;
                        }

                        GetProfileImageApprovalList();
                        rtPhotos.DataSource = photoList;
                        rtPhotos.DataBind();
                        MessageUtility.ShowToast(this.Page, ltlName.Text + " profile photo has been approved.", MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        MessageUtility.ShowToast(this.Page, ltlName.Text + " profile photo has not been approved.", MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
                else if (e.CommandName.Equals("Reject"))
                {
                    System.Web.UI.WebControls.Image imgProfilePhotoNew = e.Item.FindControl("imgProfilePhotoNew") as System.Web.UI.WebControls.Image;

                    imgUserPhoto.ImageUrl = imgProfilePhotoNew.ImageUrl;
                    ltlUserName.Text = ltlName.Text;
                    hfApprovalId.Value = Convert.ToString(e.CommandArgument);
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtPhotos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (photoList == null || photoList.Count == 0)
                {
                    if (e.Item.ItemType == ListItemType.Header)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        ltlEmptyMsg.Text = "There are no requests for profile approval at the moment.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}