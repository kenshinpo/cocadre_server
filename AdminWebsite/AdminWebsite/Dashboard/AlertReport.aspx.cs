﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AdminWebsite.Dashboard
{
    public partial class AlertReport : Page
    {
        private ManagerInfo adminInfo;
        private AdminService asc = new AdminService();
        private List<Feed> feedList;
        private List<Comment> commentList;
        private List<Reply> replyList;

        private class ReportUser
        {
            public String UserId { get; set; }
            public String FirstName { get; set; }
            public String LastName { get; set; }
            public String ImageUrl { get; set; }
            public String ReportReason { get; set; }
            public DateTimeOffset ReportTime { get; set; }
        }

        public void GetAllReport()
        {
            try
            {
                DashboardSelectReportedFeedResponse response = asc.SelectReportedFeedPosts(adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16(ddlFilterStatus.SelectedValue));
                if (response.Success)
                {
                    feedList = response.FeedPosts;
                }
                else
                {
                    Log.Error("DashboardSelectReportedFeedResponse.Success is false. ErrorMessage: " + response.ErrorMessage, this.Page);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["admin_info"] == null)
                {
                    Response.Redirect("/Logout");
                    return;
                }
                adminInfo = Session["admin_info"] as ManagerInfo;

                if (!IsPostBack)
                {
                    GetAllReport();
                    rtFeeds.DataSource = feedList;
                    rtFeeds.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtFeeds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (feedList != null && feedList.Count > 0)
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        Panel plPost = e.Item.FindControl("plPost") as Panel;
                        Panel plReportPostPerson = e.Item.FindControl("plReportPostPerson") as Panel;
                        Panel plReportPostAction = e.Item.FindControl("plReportPostAction") as Panel;
                        Panel plImagePost = e.Item.FindControl("plImagePost") as Panel;
                        Panel plVideoPost = e.Item.FindControl("plVideoPost") as Panel;
                        Panel plShareUrlPost = e.Item.FindControl("plShareUrlPost") as Panel;
                        System.Web.UI.WebControls.Image imgReportPostPersonFirst = e.Item.FindControl("imgReportPostPersonFirst") as System.Web.UI.WebControls.Image;
                        //System.Web.UI.WebControls.Image imgCommentPost = e.Item.FindControl("imgCommentPost") as System.Web.UI.WebControls.Image;
                        LinkButton ibReportrPostFirstName = e.Item.FindControl("ibReportrPostFirstName") as LinkButton;
                        LinkButton ibReportrPostOtherName = e.Item.FindControl("ibReportrPostOtherName") as LinkButton;
                        LinkButton lbSuspendAccount = e.Item.FindControl("lbSuspendAccount") as LinkButton;
                        LinkButton lbSuspendPosting = e.Item.FindControl("lbSuspendPosting") as LinkButton;
                        Literal ltlReportrPostFirstReason = e.Item.FindControl("ltlReportrPostFirstReason") as Literal;
                        Literal ltlReportrPostFirstTime = e.Item.FindControl("ltlReportrPostFirstTime") as Literal;
                        Literal ltlPostText = e.Item.FindControl("ltlPostText") as Literal;
                        Repeater rtComment = e.Item.FindControl("rtComment") as Repeater;
                        HtmlGenericControl liAcknowledge = e.Item.FindControl("liAcknowledge") as HtmlGenericControl;
                        HtmlGenericControl liHide = e.Item.FindControl("liHide") as HtmlGenericControl;
                        HtmlGenericControl liDelete = e.Item.FindControl("liDelete") as HtmlGenericControl;
                        HtmlGenericControl liRestore = e.Item.FindControl("liRestore") as HtmlGenericControl;
                        HtmlGenericControl liSuspendAccount = e.Item.FindControl("liSuspendAccount") as HtmlGenericControl;
                        HtmlGenericControl liSuspendPosting = e.Item.FindControl("liSuspendPosting") as HtmlGenericControl;

                        plImagePost.Visible = false;
                        plVideoPost.Visible = false;
                        plShareUrlPost.Visible = false;
                        //imgCommentPost.ImageUrl = adminInfo.CompanyAdminUrl;

                        switch (feedList[e.Item.ItemIndex].FeedType)
                        {
                            case 1: // Text Post
                                #region @user parse
                                if (feedList[e.Item.ItemIndex].TaggedUsers.Count > 0)
                                {
                                    for (int i = 0; i < feedList[e.Item.ItemIndex].TaggedUsers.Count; i++)
                                    {
                                        feedList[e.Item.ItemIndex].FeedText.Content = feedList[e.Item.ItemIndex].FeedText.Content.Replace("@" + feedList[e.Item.ItemIndex].TaggedUsers[i].UserId, feedList[e.Item.ItemIndex].TaggedUsers[i].FirstName + " " + feedList[e.Item.ItemIndex].TaggedUsers[i].LastName);
                                    }
                                }
                                #endregion
                                ltlPostText.Text = feedList[e.Item.ItemIndex].FeedText.Content.Replace("\n", "<br />"); ;
                                break;
                            case 2: // Image Post
                                #region @user parse
                                if (feedList[e.Item.ItemIndex].TaggedUsers.Count > 0)
                                {
                                    for (int i = 0; i < feedList[e.Item.ItemIndex].TaggedUsers.Count; i++)
                                    {
                                        feedList[e.Item.ItemIndex].FeedImage.Caption = feedList[e.Item.ItemIndex].FeedImage.Caption.Replace("@" + feedList[e.Item.ItemIndex].TaggedUsers[i].UserId, feedList[e.Item.ItemIndex].TaggedUsers[i].FirstName + " " + feedList[e.Item.ItemIndex].TaggedUsers[i].LastName);
                                    }
                                }
                                #endregion
                                ltlPostText.Text = feedList[e.Item.ItemIndex].FeedImage.Caption.Replace("\n", "<br />"); ;
                                plImagePost.Visible = true;

                                System.Web.UI.WebControls.Image imgImagePost = e.Item.FindControl("imgImagePost") as System.Web.UI.WebControls.Image;
                                if (feedList[e.Item.ItemIndex].FeedImage.Images.Count > 0)
                                {
                                    imgImagePost.ImageUrl = feedList[e.Item.ItemIndex].FeedImage.Images[0].Url;
                                }
                                else
                                {
                                    imgImagePost.Visible = false;
                                }
                                break;
                            case 3: // Video Post
                                #region @user parse
                                if (feedList[e.Item.ItemIndex].TaggedUsers.Count > 0)
                                {
                                    for (int i = 0; i < feedList[e.Item.ItemIndex].TaggedUsers.Count; i++)
                                    {
                                        feedList[e.Item.ItemIndex].FeedVideo.Caption = feedList[e.Item.ItemIndex].FeedVideo.Caption.Replace("@" + feedList[e.Item.ItemIndex].TaggedUsers[i].UserId, feedList[e.Item.ItemIndex].TaggedUsers[i].FirstName + " " + feedList[e.Item.ItemIndex].TaggedUsers[i].LastName);
                                    }
                                }
                                #endregion
                                ltlPostText.Text = feedList[e.Item.ItemIndex].FeedVideo.Caption.Replace("\n", "<br />"); ;
                                plVideoPost.Visible = true;

                                // for mp4 player
                                HtmlVideo vdPost = plVideoPost.FindControl("vdPost") as HtmlVideo;

                                // for m3u8 player
                                Panel plVideoM3U8 = e.Item.FindControl("plVideoM3U8") as Panel;
                                HtmlGenericControl parmPost = e.Item.FindControl("parmPost") as HtmlGenericControl;

                                if (feedList[e.Item.ItemIndex].FeedVideo.VideoUrl.Contains(".mp4"))
                                {
                                    plVideoM3U8.Visible = false;

                                    vdPost.Src = feedList[e.Item.ItemIndex].FeedVideo.VideoUrl;
                                }
                                else
                                {
                                    vdPost.Visible = false;

                                    parmPost.Attributes["value"] = parmPost.Attributes["value"].Replace("video_souce", feedList[e.Item.ItemIndex].FeedVideo.VideoUrl);
                                }
                                break;
                            case 4: // Share Url Post
                                #region @user parse
                                if (feedList[e.Item.ItemIndex].TaggedUsers.Count > 0)
                                {
                                    for (int i = 0; i < feedList[e.Item.ItemIndex].TaggedUsers.Count; i++)
                                    {
                                        feedList[e.Item.ItemIndex].FeedSharedUrl.Caption = feedList[e.Item.ItemIndex].FeedSharedUrl.Caption.Replace("@" + feedList[e.Item.ItemIndex].TaggedUsers[i].UserId, feedList[e.Item.ItemIndex].TaggedUsers[i].FirstName + " " + feedList[e.Item.ItemIndex].TaggedUsers[i].LastName);
                                    }
                                }
                                #endregion
                                ltlPostText.Text = feedList[e.Item.ItemIndex].FeedSharedUrl.Caption.Replace("\n", "<br />"); ;
                                plShareUrlPost.Visible = true;

                                //HyperLink hlShare = e.Item.FindControl("hlShare") as HyperLink;
                                System.Web.UI.WebControls.Image imgShareThumb = e.Item.FindControl("imgShareThumb") as System.Web.UI.WebControls.Image;
                                Literal ltlShareTitle = e.Item.FindControl("ltlShareTitle") as Literal;
                                Literal ltlShareContent = e.Item.FindControl("ltlShareContent") as Literal;
                                Literal ltlShareUrl = e.Item.FindControl("ltlShareUrl") as Literal;
                                Literal ltlShareSiteName = e.Item.FindControl("ltlShareSiteName") as Literal;


                                //hlShare.NavigateUrl = feedList[e.Item.ItemIndex].FeedSharedUrl.Url;
                                imgShareThumb.ImageUrl = feedList[e.Item.ItemIndex].FeedSharedUrl.ImageUrl;

                                if (feedList[e.Item.ItemIndex].FeedSharedUrl.Title.Length > 30)
                                {
                                    ltlShareTitle.Text = feedList[e.Item.ItemIndex].FeedSharedUrl.Title.Substring(0, 30);
                                }
                                else
                                {
                                    ltlShareTitle.Text = feedList[e.Item.ItemIndex].FeedSharedUrl.Title;

                                }

                                if (feedList[e.Item.ItemIndex].FeedSharedUrl.Description.Length > 170)
                                {
                                    ltlShareContent.Text = feedList[e.Item.ItemIndex].FeedSharedUrl.Description.Substring(0, 170);
                                }
                                else
                                {
                                    ltlShareContent.Text = feedList[e.Item.ItemIndex].FeedSharedUrl.Description;
                                }

                                ltlShareUrl.Text = feedList[e.Item.ItemIndex].FeedSharedUrl.Url.ToLower().Replace("https://", "").Replace("http://", "").Split('/')[0].Replace("www.", "");
                                ltlShareSiteName.Text = feedList[e.Item.ItemIndex].FeedSharedUrl.SiteName;
                                break;

                            default:
                                break;
                        }


                        if (feedList[e.Item.ItemIndex].ReportedFeedPosts.Count > 0)// Post is reported.
                        {
                            plPost.CssClass = "post reported";
                            plReportPostPerson.Visible = true;
                            imgReportPostPersonFirst.ImageUrl = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.ProfileImageUrl;
                            ibReportrPostFirstName.Text = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.FirstName + " " + feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.LastName;

                            if (feedList[e.Item.ItemIndex].ReportedFeedPosts.Count > 1)// more than one person report
                            {
                                ibReportrPostOtherName.Text = @" & " + (feedList[e.Item.ItemIndex].ReportedFeedPosts.Count - 1) + " other reported";
                                List<ReportUser> reportUsers = new List<ReportUser>();
                                for (int i = 0; i < feedList[e.Item.ItemIndex].ReportedFeedPosts.Count; i++)
                                {
                                    ReportUser ru = new ReportUser();
                                    ru.UserId = feedList[e.Item.ItemIndex].ReportedFeedPosts[i].ReportedUser.UserId;
                                    ru.FirstName = feedList[e.Item.ItemIndex].ReportedFeedPosts[i].ReportedUser.FirstName;
                                    ru.LastName = feedList[e.Item.ItemIndex].ReportedFeedPosts[i].ReportedUser.LastName;
                                    ru.ImageUrl = feedList[e.Item.ItemIndex].ReportedFeedPosts[i].ReportedUser.ProfileImageUrl;
                                    ru.ReportReason = feedList[e.Item.ItemIndex].ReportedFeedPosts[i].Reason;
                                    ru.ReportTime = feedList[e.Item.ItemIndex].ReportedFeedPosts[i].ReportedTimestamp;
                                    ru.UserId = feedList[e.Item.ItemIndex].ReportedFeedPosts[i].ReportedUser.UserId;
                                    reportUsers.Add(ru);
                                }

                                ibReportrPostFirstName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                                ibReportrPostOtherName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                            }
                            else // only one person report
                            {
                                ltlReportrPostFirstReason.Text = " : " + feedList[e.Item.ItemIndex].ReportedFeedPosts[0].Reason;

                                List<ReportUser> reportUsers = new List<ReportUser>();
                                ReportUser ru = new ReportUser();
                                ru.UserId = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.UserId;
                                ru.FirstName = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.FirstName;
                                ru.LastName = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.LastName;
                                ru.ImageUrl = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.ProfileImageUrl;
                                ru.ReportReason = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].Reason;
                                ru.ReportTime = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedTimestamp;
                                ru.UserId = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedUser.UserId;
                                reportUsers.Add(ru);
                                ibReportrPostFirstName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                            }

                            ltlReportrPostFirstTime.Text = feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedTimestamp.ToString("MMM dd") + " at " + feedList[e.Item.ItemIndex].ReportedFeedPosts[0].ReportedTimestamp.ToString("hh:mm tt");

                            // admin cannot suspend himselfe
                            if (adminInfo.UserId != feedList[e.Item.ItemIndex].User.UserId)
                            {
                                lbSuspendAccount.Text = "Suspend " + feedList[e.Item.ItemIndex].User.FirstName + " " + feedList[e.Item.ItemIndex].User.LastName + @"'s account";
                                lbSuspendAccount.CommandArgument = feedList[e.Item.ItemIndex].User.UserId;
                                lbSuspendPosting.Text = "Stop " + feedList[e.Item.ItemIndex].User.FirstName + " " + feedList[e.Item.ItemIndex].User.LastName + @"'s from posting";
                                lbSuspendPosting.CommandArgument = feedList[e.Item.ItemIndex].User.UserId;
                                liSuspendAccount.Visible = true;
                                liSuspendPosting.Visible = true;
                            }


                            if (feedList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Active))
                            {
                                plReportPostAction.Visible = true;
                                liAcknowledge.Visible = true;
                                liHide.Visible = true;
                                liDelete.Visible = true;
                            }
                            else if (feedList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Acknowledge))
                            {
                                plReportPostAction.Visible = false;
                            }
                            else if (feedList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Hidden))
                            {
                                plReportPostAction.Visible = true;
                                liRestore.Visible = true;
                            }
                        }
                        else// Post is not reported.
                        {
                            plPost.CssClass = "post";
                            plReportPostPerson.Visible = false;
                            plReportPostAction.Visible = false;
                        }

                        commentList = feedList[e.Item.ItemIndex].Comments;
                        rtComment.DataSource = commentList;
                        rtComment.DataBind();

                        if (e.Item.ItemIndex == feedList.Count - 1)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                        }
                    }
                }
                else
                {
                    if (e.Item.ItemType == ListItemType.Header)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        ltlEmptyMsg.Text = "There are no reports in need of attention at the moment.";
                    }
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtComment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Panel plCommentContent = e.Item.FindControl("plCommentContent") as Panel;
                    Panel plReportCommentPerson = e.Item.FindControl("plReportCommentPerson") as Panel;
                    Panel plReportCommentAction = e.Item.FindControl("plReportCommentAction") as Panel;
                    System.Web.UI.WebControls.Image imgReportCommentPersonFirst = e.Item.FindControl("imgReportCommentPersonFirst") as System.Web.UI.WebControls.Image;
                    LinkButton ibReportrCommentFirstName = e.Item.FindControl("ibReportrCommentFirstName") as LinkButton;
                    LinkButton ibReportrCommentOtherName = e.Item.FindControl("ibReportrCommentOtherName") as LinkButton;
                    LinkButton lbSuspendAccount = e.Item.FindControl("lbSuspendAccount") as LinkButton;
                    LinkButton lbSuspendPosting = e.Item.FindControl("lbSuspendPosting") as LinkButton;
                    Literal ltlCommentContent = e.Item.FindControl("ltlCommentContent") as Literal;
                    Literal ltlReportrCommentFirstReason = e.Item.FindControl("ltlReportrCommentFirstReason") as Literal;
                    Literal ltlReportrCommentFirstTime = e.Item.FindControl("ltlReportrCommentFirstTime") as Literal;
                    HtmlGenericControl liAcknowledge = e.Item.FindControl("liAcknowledge") as HtmlGenericControl;
                    HtmlGenericControl liHide = e.Item.FindControl("liHide") as HtmlGenericControl;
                    HtmlGenericControl liDelete = e.Item.FindControl("liDelete") as HtmlGenericControl;
                    HtmlGenericControl liRestore = e.Item.FindControl("liRestore") as HtmlGenericControl;
                    HtmlGenericControl liSuspendAccount = e.Item.FindControl("liSuspendAccount") as HtmlGenericControl;
                    HtmlGenericControl liSuspendPosting = e.Item.FindControl("liSuspendPosting") as HtmlGenericControl;
                    Repeater rtReply = e.Item.FindControl("rtReply") as Repeater;

                    #region @user parse
                    if (commentList[e.Item.ItemIndex].TaggedUsers.Count > 0)
                    {
                        for (int i = 0; i < commentList[e.Item.ItemIndex].TaggedUsers.Count; i++)
                        {
                            commentList[e.Item.ItemIndex].CommentText.Content = commentList[e.Item.ItemIndex].CommentText.Content.Replace("@" + commentList[e.Item.ItemIndex].TaggedUsers[i].UserId, commentList[e.Item.ItemIndex].TaggedUsers[i].FirstName + " " + commentList[e.Item.ItemIndex].TaggedUsers[i].LastName);
                        }
                    }
                    #endregion
                    ltlCommentContent.Text = commentList[e.Item.ItemIndex].CommentText.Content.Replace("\n","<br />");

                    if (commentList[e.Item.ItemIndex].ReportedCommentPosts.Count > 0)// Comment is reported.
                    {
                        plCommentContent.CssClass = "post__content__article reported";
                        plReportCommentPerson.Visible = true;
                        imgReportCommentPersonFirst.ImageUrl = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.ProfileImageUrl;
                        ibReportrCommentFirstName.Text = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.FirstName + " " + commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.LastName;

                        if (commentList[e.Item.ItemIndex].ReportedCommentPosts.Count > 1) // more than one person report
                        {
                            ibReportrCommentOtherName.Text = @" & " + (commentList[e.Item.ItemIndex].ReportedCommentPosts.Count - 1) + " other reported";
                            List<ReportUser> reportUsers = new List<ReportUser>();
                            for (int i = 0; i < commentList[e.Item.ItemIndex].ReportedCommentPosts.Count; i++)
                            {
                                ReportUser ru = new ReportUser();
                                ru.UserId = commentList[e.Item.ItemIndex].ReportedCommentPosts[i].ReportedUser.UserId;
                                ru.FirstName = commentList[e.Item.ItemIndex].ReportedCommentPosts[i].ReportedUser.FirstName;
                                ru.LastName = commentList[e.Item.ItemIndex].ReportedCommentPosts[i].ReportedUser.LastName;
                                ru.ImageUrl = commentList[e.Item.ItemIndex].ReportedCommentPosts[i].ReportedUser.ProfileImageUrl;
                                ru.ReportReason = commentList[e.Item.ItemIndex].ReportedCommentPosts[i].Reason;
                                ru.ReportTime = commentList[e.Item.ItemIndex].ReportedCommentPosts[i].ReportedTimestamp;
                                ru.UserId = commentList[e.Item.ItemIndex].ReportedCommentPosts[i].ReportedUser.UserId;
                                reportUsers.Add(ru);
                            }

                            ibReportrCommentFirstName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                            ibReportrCommentOtherName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                        }
                        else // only one person report
                        {
                            ltlReportrCommentFirstReason.Text = " : " + commentList[e.Item.ItemIndex].ReportedCommentPosts[0].Reason;

                            List<ReportUser> reportUsers = new List<ReportUser>();
                            ReportUser ru = new ReportUser();
                            ru.UserId = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.UserId;
                            ru.FirstName = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.FirstName;
                            ru.LastName = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.LastName;
                            ru.ImageUrl = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.ProfileImageUrl;
                            ru.ReportReason = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].Reason;
                            ru.ReportTime = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedTimestamp;
                            ru.UserId = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedUser.UserId;
                            reportUsers.Add(ru);
                            ibReportrCommentFirstName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                        }

                        ltlReportrCommentFirstTime.Text = commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedTimestamp.ToString("MMM dd") + " at " + commentList[e.Item.ItemIndex].ReportedCommentPosts[0].ReportedTimestamp.ToString("hh:mm tt");

                        // admin cannot suspend himselfe
                        if (adminInfo.UserId != commentList[e.Item.ItemIndex].User.UserId)
                        {
                            lbSuspendAccount.Text = "Suspend " + commentList[e.Item.ItemIndex].User.FirstName + " " + commentList[e.Item.ItemIndex].User.LastName + @"'s account";
                            lbSuspendAccount.CommandArgument = commentList[e.Item.ItemIndex].User.UserId;
                            lbSuspendPosting.Text = "Stop " + commentList[e.Item.ItemIndex].User.FirstName + " " + commentList[e.Item.ItemIndex].User.LastName + @"'s from posting";
                            lbSuspendPosting.CommandArgument = commentList[e.Item.ItemIndex].User.UserId;
                            liSuspendAccount.Visible = true;
                            liSuspendPosting.Visible = true;
                        }

                        if (commentList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Active))
                        {
                            plReportCommentAction.Visible = true;
                            liAcknowledge.Visible = true;
                            liHide.Visible = true;
                            liDelete.Visible = true;
                        }
                        else if (commentList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Acknowledge))
                        {
                            plReportCommentAction.Visible = false;
                        }
                        else if (commentList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Hidden))
                        {
                            plReportCommentAction.Visible = true;
                            liRestore.Visible = true;
                        }
                    }
                    else// Comment is not reported.
                    {
                        plCommentContent.CssClass = "post__content__article";
                        plReportCommentPerson.Visible = false;
                        plReportCommentAction.Visible = false;
                    }

                    replyList = commentList[e.Item.ItemIndex].Replies;
                    rtReply.DataSource = replyList;
                    rtReply.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtReply_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Panel plReplyContent = e.Item.FindControl("plReplyContent") as Panel;
                    Panel plReportReplyPerson = e.Item.FindControl("plReportReplyPerson") as Panel;
                    Panel plReportReplyAction = e.Item.FindControl("plReportReplyAction") as Panel;
                    System.Web.UI.WebControls.Image imgReportReplyPersonFirst = e.Item.FindControl("imgReportReplyPersonFirst") as System.Web.UI.WebControls.Image;
                    LinkButton ibReportrReplyFirstName = e.Item.FindControl("ibReportrReplyFirstName") as LinkButton;
                    LinkButton ibReportrReplyOtherName = e.Item.FindControl("ibReportrReplyOtherName") as LinkButton;
                    LinkButton lbSuspendAccount = e.Item.FindControl("lbSuspendAccount") as LinkButton;
                    LinkButton lbSuspendPosting = e.Item.FindControl("lbSuspendPosting") as LinkButton;
                    Literal ltlReplyContent = e.Item.FindControl("ltlReplyContent") as Literal;
                    Literal ltlReportrReplyFirstReason = e.Item.FindControl("ltlReportrReplyFirstReason") as Literal;
                    Literal ltlReportrReplyFirstTime = e.Item.FindControl("ltlReportrReplyFirstTime") as Literal;
                    HtmlGenericControl liAcknowledge = e.Item.FindControl("liAcknowledge") as HtmlGenericControl;
                    HtmlGenericControl liHide = e.Item.FindControl("liHide") as HtmlGenericControl;
                    HtmlGenericControl liDelete = e.Item.FindControl("liDelete") as HtmlGenericControl;
                    HtmlGenericControl liRestore = e.Item.FindControl("liRestore") as HtmlGenericControl;
                    HtmlGenericControl liSuspendAccount = e.Item.FindControl("liSuspendAccount") as HtmlGenericControl;
                    HtmlGenericControl liSuspendPosting = e.Item.FindControl("liSuspendPosting") as HtmlGenericControl;

                    #region @user parse
                    if (replyList[e.Item.ItemIndex].TaggedUsers.Count > 0)
                    {
                        for (int i = 0; i < replyList[e.Item.ItemIndex].TaggedUsers.Count; i++)
                        {
                            replyList[e.Item.ItemIndex].ReplyText.Content = replyList[e.Item.ItemIndex].ReplyText.Content.Replace("@" + replyList[e.Item.ItemIndex].TaggedUsers[i].UserId, replyList[e.Item.ItemIndex].TaggedUsers[i].FirstName + " " + replyList[e.Item.ItemIndex].TaggedUsers[i].LastName);
                        }
                    }
                    #endregion
                    ltlReplyContent.Text = replyList[e.Item.ItemIndex].ReplyText.Content.Replace("\n", "<br />"); ;

                    if (replyList[e.Item.ItemIndex].ReportedReplyPosts.Count > 0)// Comment is reported.
                    {
                        plReplyContent.CssClass = "post__content__article reported";
                        plReportReplyPerson.Visible = true;
                        imgReportReplyPersonFirst.ImageUrl = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.ProfileImageUrl;
                        ibReportrReplyFirstName.Text = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.FirstName + " " + replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.LastName;

                        if (replyList[e.Item.ItemIndex].ReportedReplyPosts.Count > 1) // more than one person report
                        {
                            ibReportrReplyOtherName.Text = @" & " + (replyList[e.Item.ItemIndex].ReportedReplyPosts.Count - 1) + " other reported";
                            List<ReportUser> reportUsers = new List<ReportUser>();
                            for (int i = 0; i < replyList[e.Item.ItemIndex].ReportedReplyPosts.Count; i++)
                            {
                                ReportUser ru = new ReportUser();
                                ru.UserId = replyList[e.Item.ItemIndex].ReportedReplyPosts[i].ReportedUser.UserId;
                                ru.FirstName = replyList[e.Item.ItemIndex].ReportedReplyPosts[i].ReportedUser.FirstName;
                                ru.LastName = replyList[e.Item.ItemIndex].ReportedReplyPosts[i].ReportedUser.LastName;
                                ru.ImageUrl = replyList[e.Item.ItemIndex].ReportedReplyPosts[i].ReportedUser.ProfileImageUrl;
                                ru.ReportReason = replyList[e.Item.ItemIndex].ReportedReplyPosts[i].Reason;
                                ru.ReportTime = replyList[e.Item.ItemIndex].ReportedReplyPosts[i].ReportedTimestamp;
                                ru.UserId = replyList[e.Item.ItemIndex].ReportedReplyPosts[i].ReportedUser.UserId;
                                reportUsers.Add(ru);
                            }

                            ibReportrReplyFirstName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                            ibReportrReplyOtherName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                        }
                        else // only one person report
                        {
                            ltlReportrReplyFirstReason.Text = " : " + replyList[e.Item.ItemIndex].ReportedReplyPosts[0].Reason;

                            List<ReportUser> reportUsers = new List<ReportUser>();
                            ReportUser ru = new ReportUser();
                            ru.UserId = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.UserId;
                            ru.FirstName = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.FirstName;
                            ru.LastName = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.LastName;
                            ru.ImageUrl = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.ProfileImageUrl;
                            ru.ReportReason = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].Reason;
                            ru.ReportTime = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedTimestamp;
                            ru.UserId = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedUser.UserId;
                            reportUsers.Add(ru);
                            ibReportrReplyFirstName.CommandArgument = JsonConvert.SerializeObject(reportUsers);
                        }

                        ltlReportrReplyFirstTime.Text = replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedTimestamp.ToString("MMM dd") + " at " + replyList[e.Item.ItemIndex].ReportedReplyPosts[0].ReportedTimestamp.ToString("hh:mm tt");

                        // admin cannot suspend himselfe
                        if (adminInfo.UserId != replyList[e.Item.ItemIndex].User.UserId)
                        {
                            lbSuspendAccount.Text = "Suspend " + replyList[e.Item.ItemIndex].User.FirstName + " " + replyList[e.Item.ItemIndex].User.LastName + @"'s account";
                            lbSuspendAccount.CommandArgument = replyList[e.Item.ItemIndex].User.UserId;
                            lbSuspendPosting.Text = "Stop " + replyList[e.Item.ItemIndex].User.FirstName + " " + replyList[e.Item.ItemIndex].User.LastName + @"'s from posting";
                            lbSuspendAccount.CommandArgument = replyList[e.Item.ItemIndex].User.UserId;
                            liSuspendAccount.Visible = true;
                            liSuspendPosting.Visible = true;
                        }

                        if (replyList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Active))
                        {
                            plReportReplyAction.Visible = true;
                            liAcknowledge.Visible = true;
                            liHide.Visible = true;
                            liDelete.Visible = true;
                        }
                        else if (replyList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Acknowledge))
                        {
                            plReportReplyAction.Visible = false;
                        }
                        else if (replyList[e.Item.ItemIndex].ReportState == Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Hidden))
                        {
                            plReportReplyAction.Visible = true;
                            liRestore.Visible = true;
                        }
                    }
                    else// Comment is not reported.
                    {
                        plReplyContent.CssClass = "post__content__article";
                        plReportReplyPerson.Visible = false;
                        plReportReplyAction.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtFeeds_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                String toastMsg = String.Empty;
                int newStatus = 0;
                String[] commandArguments = Convert.ToString(e.CommandArgument).Split(',');

                if (e.CommandName.Equals("Acknowledge"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Acknowledge);
                }
                else if (e.CommandName.Equals("HidePost"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Hidden);
                }
                else if (e.CommandName.Equals("DeletePost"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Delete);
                }
                else if (e.CommandName.Equals("RestorePost"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Active);
                }
                else if (e.CommandName.Equals("ShowReportPerson"))
                {
                    List<ReportUser> reportUsers = JsonConvert.DeserializeObject<List<ReportUser>>(Convert.ToString(e.CommandArgument));
                    if (reportUsers.Count == 1)
                    {
                        UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, reportUsers[0].UserId);
                        if (response.Success)
                        {
                            ltlReportUserName.Text = response.User.FirstName + " " + response.User.LastName;
                            ltlReportUserEmail.Text = response.User.Email;
                            ltlReportUserDepartmentTitle.Text = response.User.Departments[0].Title;
                            ltlReportUserDepartmentPosition.Text = response.User.Departments[0].Position;
                            imgReportUser.ImageUrl = response.User.ProfileImageUrl;
                            hfReportUserId.Value = response.User.UserId;
                            mpePop.PopupControlID = "plReportUserInfo";
                            mpePop.Show();
                        }
                        else
                        {
                            Log.Error("UserDetailResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        }

                    }
                    else if (reportUsers.Count > 1)
                    {
                        rtReportUser.DataSource = reportUsers;
                        rtReportUser.DataBind();
                        mpePop.PopupControlID = "plReportUserList";
                        mpePop.Show();
                    }
                }
                else if (e.CommandName.Equals("SuspendAccount"))
                {
                    UserUpdateStatusResponse response = asc.UpdateUserStatus(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument), -1);
                    if (response.Success)
                    {
                        toastMsg = "User has been suspended.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserStatusIsIdentical))
                        {
                            toastMsg = "User is already in this status.";
                        }
                        else
                        {
                            toastMsg = "Suspended user failed.";
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                }
                else if (e.CommandName.Equals("SuspendPosting"))
                {
                    PostingPermissionSuspendUserResponse response = asc.PostingPermissionSuspendUser(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument));
                    if (response.Success)
                    {
                        toastMsg = "Users has been suspended";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        Log.Error("PostingPermissionSuspendUserResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserStatusIsIdentical))
                        {
                            toastMsg = "User is already in this status.";
                        }
                        else
                        {
                            toastMsg = "Suspended user failed.";
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                }

                if (newStatus != 0)
                {
                    DashboardUpdateReportStatusResponse response = asc.UpdateReportStatus(adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16(CassandraService.Entity.Dashboard.ReportType.Feed), commandArguments[0], commandArguments[1], newStatus);
                    if (response.Success)
                    {
                        switch (newStatus)
                        {
                            case 1:
                                toastMsg = "Report restored successfully.";
                                break;
                            case 2:
                                toastMsg = "Report acknowledged successfully.";
                                break;
                            case 3:
                                toastMsg = "Report hidden successfully.";
                                break;
                            case 4:
                                toastMsg = "Report deleted successfully.";
                                break;
                            default:
                                break;
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        switch (newStatus)
                        {
                            case 1:
                                toastMsg = "Failed to restore report.";
                                break;
                            case 2:
                                toastMsg = "Failed to acknowledge report.";
                                break;
                            case 3:
                                toastMsg = "Failed to hide report.";
                                break;
                            case 4:
                                toastMsg = "Failed to delete report.";
                                break;
                            default:
                                break;
                        }
                        Log.Error("DashboardUpdateReportStatusResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }

                    GetAllReport();
                    rtFeeds.DataSource = feedList;
                    rtFeeds.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ddlFilterStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAllReport();
                rtFeeds.DataSource = feedList;
                rtFeeds.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtReportUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                mpePop.Hide();
                UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument));
                if (response.Success)
                {
                    ltlReportUserName.Text = response.User.FirstName + " " + response.User.LastName;
                    ltlReportUserEmail.Text = response.User.Email;
                    ltlReportUserDepartmentTitle.Text = response.User.Departments[0].Title;
                    ltlReportUserDepartmentPosition.Text = response.User.Departments[0].Position;
                    imgReportUser.ImageUrl = response.User.ProfileImageUrl;
                    hfReportUserId.Value = response.User.UserId;
                    mpePop.PopupControlID = "plReportUserInfo";
                    mpePop.Show();
                }
                else
                {
                    Log.Error("UserDetailResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbReportUserSuspend_Click(object sender, EventArgs e)
        {
            try
            {
                UserUpdateStatusResponse response = asc.UpdateUserStatus(adminInfo.UserId, adminInfo.CompanyId, hfReportUserId.Value, -1);
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    toastMsg = "User has been suspended.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    toastMsg = "Suspended user failed.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtComment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                String toastMsg = String.Empty;
                int newStatus = 0;
                String[] commandArguments = Convert.ToString(e.CommandArgument).Split(',');

                if (e.CommandName.Equals("Acknowledge"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Acknowledge);
                }
                else if (e.CommandName.Equals("HideComment"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Hidden);
                }
                else if (e.CommandName.Equals("DeleteComment"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Delete);
                }
                else if (e.CommandName.Equals("RestoreComment"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Active);
                }
                else if (e.CommandName.Equals("ShowReportPerson"))
                {
                    List<ReportUser> reportUsers = JsonConvert.DeserializeObject<List<ReportUser>>(Convert.ToString(e.CommandArgument));
                    if (reportUsers.Count == 1)
                    {
                        UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, reportUsers[0].UserId);
                        if (response.Success)
                        {
                            ltlReportUserName.Text = response.User.FirstName + " " + response.User.LastName;
                            ltlReportUserEmail.Text = response.User.Email;
                            ltlReportUserDepartmentTitle.Text = response.User.Departments[0].Title;
                            ltlReportUserDepartmentPosition.Text = response.User.Departments[0].Position;
                            imgReportUser.ImageUrl = response.User.ProfileImageUrl;
                            hfReportUserId.Value = response.User.UserId;
                            mpePop.PopupControlID = "plReportUserInfo";
                            mpePop.Show();
                        }
                        else
                        {
                            Log.Error("UserDetailResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        }

                    }
                    else if (reportUsers.Count > 1)
                    {
                        rtReportUser.DataSource = reportUsers;
                        rtReportUser.DataBind();
                        mpePop.PopupControlID = "plReportUserList";
                        mpePop.Show();
                    }
                }
                else if (e.CommandName.Equals("SuspendAccount"))
                {
                    UserUpdateStatusResponse response = asc.UpdateUserStatus(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument), -1);
                    if (response.Success)
                    {
                        toastMsg = "User has been suspended.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserStatusIsIdentical))
                        {
                            toastMsg = "User is already in this status.";
                        }
                        else
                        {
                            toastMsg = "Suspended user failed.";
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                }
                else if (e.CommandName.Equals("SuspendPosting"))
                {
                    PostingPermissionSuspendUserResponse response = asc.PostingPermissionSuspendUser(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument));
                    if (response.Success)
                    {
                        toastMsg = "Users has been suspended";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        Log.Error("PostingPermissionSuspendUserResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserStatusIsIdentical))
                        {
                            toastMsg = "User is already in this status.";
                        }
                        else
                        {
                            toastMsg = "Suspended user failed.";
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                }

                if (newStatus != 0)
                {
                    DashboardUpdateReportStatusResponse response = asc.UpdateReportStatus(adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16(CassandraService.Entity.Dashboard.ReportType.Comment), commandArguments[0], commandArguments[1], newStatus, commandArguments[2]);
                    if (response.Success)
                    {
                        switch (newStatus)
                        {
                            case 1:
                                toastMsg = "Report restored successfully.";
                                break;
                            case 2:
                                toastMsg = "Report acknowledged successfully.";
                                break;
                            case 3:
                                toastMsg = "Report hidden successfully.";
                                break;
                            case 4:
                                toastMsg = "Report deleted successfully.";
                                break;
                            default:
                                break;
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);

                        GetAllReport();
                        rtFeeds.DataSource = feedList;
                        rtFeeds.DataBind();

                        //Repeater rtComment = e.Item.Parent as Repeater;
                        //rtComment.DataSource = response.UpdatedComments;
                        //rtComment.DataBind();

                        //UpdatePanel upComments = rtComment.Parent.Parent as UpdatePanel;
                        //upComments.Update();
                    }
                    else
                    {
                        switch (newStatus)
                        {
                            case 1:
                                toastMsg = "Failed to restore report.";
                                break;
                            case 2:
                                toastMsg = "Failed to acknowledge report.";
                                break;
                            case 3:
                                toastMsg = "Failed to hide report.";
                                break;
                            case 4:
                                toastMsg = "Failed to delete report.";
                                break;
                            default:
                                break;
                        }
                        Log.Error("DashboardUpdateReportStatusResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtReply_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                String toastMsg = String.Empty;
                int newStatus = 0;
                String[] commandArguments = Convert.ToString(e.CommandArgument).Split(',');

                if (e.CommandName.Equals("Acknowledge"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Acknowledge);
                }
                else if (e.CommandName.Equals("HideReply"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Hidden);
                }
                else if (e.CommandName.Equals("DeleteReply"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Delete);
                }
                else if (e.CommandName.Equals("RestoreReply"))
                {
                    newStatus = Convert.ToInt16(CassandraService.Entity.Dashboard.ReportState.Active);
                }
                else if (e.CommandName.Equals("ShowReportPerson"))
                {
                    List<ReportUser> reportUsers = JsonConvert.DeserializeObject<List<ReportUser>>(Convert.ToString(e.CommandArgument));
                    if (reportUsers.Count == 1)
                    {
                        UserDetailResponse response = asc.GetUserDetail(adminInfo.UserId, adminInfo.CompanyId, reportUsers[0].UserId);
                        if (response.Success)
                        {
                            ltlReportUserName.Text = response.User.FirstName + " " + response.User.LastName;
                            ltlReportUserEmail.Text = response.User.Email;
                            ltlReportUserDepartmentTitle.Text = response.User.Departments[0].Title;
                            ltlReportUserDepartmentPosition.Text = response.User.Departments[0].Position;
                            imgReportUser.ImageUrl = response.User.ProfileImageUrl;
                            hfReportUserId.Value = response.User.UserId;
                            mpePop.PopupControlID = "plReportUserInfo";
                            mpePop.Show();
                        }
                        else
                        {
                            Log.Error("UserDetailResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        }

                    }
                    else if (reportUsers.Count > 1)
                    {
                        rtReportUser.DataSource = reportUsers;
                        rtReportUser.DataBind();
                        mpePop.PopupControlID = "plReportUserList";
                        mpePop.Show();
                    }
                }
                else if (e.CommandName.Equals("SuspendAccount"))
                {
                    UserUpdateStatusResponse response = asc.UpdateUserStatus(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument), -1);
                    if (response.Success)
                    {
                        toastMsg = "User has been suspended.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserStatusIsIdentical))
                        {
                            toastMsg = "User is already in this status.";
                        }
                        else
                        {
                            toastMsg = "Suspended user failed.";
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                }
                else if (e.CommandName.Equals("SuspendPosting"))
                {
                    PostingPermissionSuspendUserResponse response = asc.PostingPermissionSuspendUser(adminInfo.UserId, adminInfo.CompanyId, Convert.ToString(e.CommandArgument));
                    if (response.Success)
                    {
                        toastMsg = "Users has been suspended";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        Log.Error("PostingPermissionSuspendUserResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        if (response.ErrorCode == Convert.ToInt16(ErrorCode.UserStatusIsIdentical))
                        {
                            toastMsg = "User is already in this status.";
                        }
                        else
                        {
                            toastMsg = "Suspended user failed.";
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                    }
                }

                if (newStatus != 0)
                {
                    DashboardUpdateReportStatusResponse response = asc.UpdateReportStatus(adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16(CassandraService.Entity.Dashboard.ReportType.Reply), commandArguments[0], commandArguments[1], newStatus, commandArguments[2], commandArguments[3]);
                    if (response.Success)
                    {
                        switch (newStatus)
                        {
                            case 1:
                                toastMsg = "Report restored successfully.";
                                break;
                            case 2:
                                toastMsg = "Report acknowledged successfully.";
                                break;
                            case 3:
                                toastMsg = "Report hidden successfully.";
                                break;
                            case 4:
                                toastMsg = "Report deleted successfully.";
                                break;

                            default:
                                break;
                        }
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);

                        GetAllReport();
                        rtFeeds.DataSource = feedList;
                        rtFeeds.DataBind();

                        //Repeater rtComment = e.Item.Parent as Repeater;
                        //rtComment.DataSource = response.UpdatedComments;
                        //rtComment.DataBind();

                        //UpdatePanel upComments = rtComment.Parent.Parent as UpdatePanel;
                        //upComments.Update();
                    }
                    else
                    {
                        switch (newStatus)
                        {
                            case 1:
                                toastMsg = "Failed to restore report.";
                                break;
                            case 2:
                                toastMsg = "Failed to acknowledge report.";
                                break;
                            case 3:
                                toastMsg = "Failed to hide report.";
                                break;
                            case 4:
                                toastMsg = "Failed to delete report.";
                                break;
                            default:
                                break;
                        }
                        Log.Error("DashboardUpdateReportStatusResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}