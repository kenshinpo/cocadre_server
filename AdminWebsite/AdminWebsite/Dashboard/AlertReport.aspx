﻿<%@ Page Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AlertReport.aspx.cs" Inherits="AdminWebsite.Dashboard.AlertReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <script type="text/javascript">
        $(function () {
            initChips();
        });

        function initChips() {
            $(".chips--02__button").click(function () {
                var e = window.event || e;
                var $button, $menu;
                $button = $(this);

                $(".chips--02__menu").removeClass("chips--02__menu--show");

                $menu = $button.siblings(".chips--02__menu");
                $menu.toggleClass("chips--02__menu--show");
                e.stopPropagation();
                $menu.children("li").click(function () {
                    $menu.removeClass("chips--02__menu--show");
                });
            });
            $(document).on("click", function (e) {
                $(".chips--02__menu").removeClass("chips--02__menu--show");
            });
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>

            <!-- Report User List -->
            <asp:Panel ID="plReportUserList" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">Reported Personnels</h1>
                <asp:Repeater ID="rtReportUser" runat="server" OnItemCommand="rtReportUser_ItemCommand">
                    <HeaderTemplate>
                        <div class="popup__content popup__content--nominheight">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="post__user">
                            <img class="post__user__figure" src="<%# DataBinder.Eval(Container.DataItem, "ImageUrl") %>" alt="">
                            <div class="post__user__body">
                                <div class="post__user__body__title">
                                    <b><%# DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName") %></b>: <%# DataBinder.Eval(Container.DataItem, "ReportReason") %>
                                </div>
                                <div class="post__user__body__content">
                                    <span><%# DataBinder.Eval(Container.DataItem, "ReportTime","{0:MMM dd}") + " at " + DataBinder.Eval(Container.DataItem, "ReportTime","{0:hh:mm tt}") %></span>
                                </div>
                            </div>
                            <div class="popup__action">
                                <asp:LinkButton ID="lbUserInfo" runat="server" CssClass="popup__action__item popup__action__item--confirm" Text="Suspend" CommandName="UserInfo" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId") %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>

                <div class="popup__action">
                    <asp:LinkButton ID="lbReportUsersCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Report User List -->

            <!-- Report User Info -->
            <asp:Panel ID="plReportUserInfo" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <asp:HiddenField ID="hfReportUserId" runat="server" />
                <h1 class="popup__title">Personnel Info</h1>
                <div class="popup__content">
                    <fieldset class="form material">
                        <div class="container">
                            <div class="main">
                                <div class="form__row">
                                    <asp:Literal ID="ltlReportUserName" runat="server" />
                                    <br />
                                    <asp:Literal ID="ltlReportUserEmail" runat="server" />
                                </div>
                                <div class="label">Department</div>
                                <div class="form__row">
                                    <asp:Literal ID="ltlReportUserDepartmentTitle" runat="server" />
                                    <br />
                                    <asp:Literal ID="ltlReportUserDepartmentPosition" runat="server" />
                                </div>
                                <div class="label">Account Suspension</div>
                                <div class="form__row">
                                    This user will not be able to:
                           
                                    <ul class="normal">
                                        <li>Login to Cocadre account</li>
                                    </ul>
                                </div>
                                <%--<div class="form__row">
                                    <input id="" name="" type="text" placeholder="Reason to User for suspension" required="">
                                </div>--%>
                                <p>
                                    You will be able to activate this user later
                                    <br>
                                    <span class="red">User license fees still apply to suspended users</span>
                                </p>
                            </div>
                            <div class="side">
                                <a href="#" class="">
                                    <asp:Image ID="imgReportUser" runat="server" />
                                    <%--<img src="https://unsplash.it/300" alt="">--%>
                                </a>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <%--<a href="#" class="popup__action__item popup__action__item--confirm">Suspend User Account</a>--%>
                    <asp:LinkButton ID="lbReportUserSuspend" CssClass="popup__action__item popup__action__item--confirm" runat="server" Text="Suspend User Account" OnClick="lbReportUserSuspend_Click" />
                    <asp:LinkButton ID="lbReportUserCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- Report User Info -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="plReportUserList"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Dashboard <span>console</span></div>
        <div class="appbar__meta">Reported Posts <span><%--(21)--%></span></div>
        <div class="appbar__action">
            <!-- <a href=""><i class="fa fa-ellipsis-v"></i></a>
                <a href=""><i class="fa fa-question-circle"></i></a>
                <a class="data-sidebar-toggle" href="#"><i class="fa fa-filter"></i></a> -->
        </div>
    </div>
    <!-- / App Bar -->

    <div class="data">

        <aside class="data__sidebar data__sidebar--size1 filter">
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Alert Report</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Dashboard/ProfilePhotoApproval">Profile Photo Approval</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Dashboard/Feedbacks">Feedback</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <div class="pad">
                <label>By Status</label>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="mdl-selectfield">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlFilterStatus" runat="server" CssClass="cs-select cs-skin-border" OnSelectedIndexChanged="ddlFilterStatus_SelectedIndexChanged" AutoPostBack="true" onchange="ShowProgressBar();">
                            <asp:ListItem Text="Active Report" Value="1" />
                            <asp:ListItem Text="Acknowledged Report" Value="2" />
                            <asp:ListItem Text="Hidden posts & comments Report" Value="3" />
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </aside>

        <asp:UpdatePanel ID="upFeeds" runat="server" class="data__content">
            <ContentTemplate>
                <asp:Repeater ID="rtFeeds" runat="server" OnItemDataBound="rtFeeds_ItemDataBound" OnItemCommand="rtFeeds_ItemCommand">
                    <HeaderTemplate>
                        <div class="posts">
                            <asp:Literal ID="ltlEmptyMsg" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <!-- Post -->
                        <asp:Panel ID="plPost" runat="server">
                            <!-- Post Head -->
                            <div class="post__head">

                                <!-- Report Post -->
                                <asp:Panel ID="plReportPostPerson" runat="server" CssClass="flyout">
                                    <div class="post__user">
                                        <asp:Image ID="imgReportPostPersonFirst" runat="server" CssClass="post__user__figure" />
                                        <div class="post__user__body">
                                            <div class="post__user__body__title">
                                                <asp:LinkButton ID="ibReportrPostFirstName" runat="server" CommandName="ShowReportPerson" />
                                                <asp:Literal ID="ltlReportrPostFirstReason" runat="server" />
                                                <asp:LinkButton ID="ibReportrPostOtherName" runat="server" CommandName="ShowReportPerson" />
                                            </div>
                                            <div class="post__user__body__content">
                                                <span>
                                                    <asp:Literal ID="ltlReportrPostFirstTime" runat="server" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <!-- Report Post -->

                                <div class="post__user">
                                    <img class="post__user__figure" src="<%# DataBinder.Eval(Container.DataItem, "User.ProfileImageUrl") %>" alt="">
                                    <div class="post__user__body">
                                        <div class="post__user__body__title">
                                            <%# DataBinder.Eval(Container.DataItem, "User.FirstName") +" " + DataBinder.Eval(Container.DataItem, "User.LastName") %>
                                        </div>
                                        <div class="post__user__body__content">
                                            <span>
                                                <%# DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:MMM dd}") + " at " + DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:hh:mm tt}") %>
                                            </span>
                                        </div>
                                    </div>

                                    <asp:Panel ID="plReportPostAction" runat="server" CssClass="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li id="liAcknowledge" runat="server" visible="false">
                                                        <asp:LinkButton ID="lbAcknowledge" runat="server" ClientIDMode="AutoID" CommandName="Acknowledge" Text="Acknowledge" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") %>' OnClientClick="ShowProgressBar();" />
                                                    </li>
                                                    <li id="liHide" runat="server" visible="false">
                                                        <asp:LinkButton ID="lbHide" runat="server" ClientIDMode="AutoID" CommandName="HidePost" Text="Hide post" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") %>' OnClientClick="ShowProgressBar();" />
                                                    </li>
                                                    <li id="liDelete" runat="server" visible="false">
                                                        <asp:LinkButton ID="lbDelete" runat="server" ClientIDMode="AutoID" CommandName="DeletePost" Text="Delete post" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") %>' OnClientClick="ShowProgressBar();" />
                                                    </li>
                                                    <li id="liRestore" runat="server" visible="false">
                                                        <asp:LinkButton ID="lbRestore" runat="server" ClientIDMode="AutoID" CommandName="RestorePost" Text="Restore post" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") %>' OnClientClick="ShowProgressBar();" />
                                                    </li>
                                                    <li id="liSuspendAccount" runat="server" visible="false">
                                                        <asp:LinkButton ID="lbSuspendAccount" runat="server" CommandName="SuspendAccount" />
                                                    </li>
                                                    <li id="liSuspendPosting" runat="server" visible="false">
                                                        <asp:LinkButton ID="lbSuspendPosting" runat="server" CommandName="SuspendPosting" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <!-- /Post Head -->

                            <!-- Post Content -->
                            <div class="post__content">
                                <div class="post__content__article">
                                    <p>
                                        <asp:Literal ID="ltlPostText" runat="server" />
                                    </p>
                                </div>

                                <asp:Panel ID="plImagePost" runat="server" CssClass="post__content__media">
                                    <asp:Image ID="imgImagePost" runat="server" />
                                </asp:Panel>

                                <asp:Panel ID="plVideoPost" runat="server" CssClass="post__content__media">

                                    <video id="vdPost" runat="server" preload="auto" width="500" controls="controls">
                                        Your browser does not support the video tag.
                                    </video>

                                    <asp:Panel ID="plVideoM3U8" runat="server">
                                        <object id="player" width="500" data="/Content/Player/GrindPlayer.swf" type="application/x-shockwave-flash">
                                            <param name="allowScriptAccess" value="always">
                                            <param name="wmode" value="opaque">
                                            <param id="parmPost" runat="server" name="flashvars" value="autoPlay=true&amp;src=video_souce%23.m3u8&amp;streamType=live&amp;scaleMode=letterbox&amp;plugin_hls=/Content/Player/FlashlsOSMF.swf&amp;hls_debug=false&amp;hls_debug2=false&amp;hls_minbufferlength=-1&amp;hls_lowbufferlength=2&amp;hls_maxbufferlength=60&amp;hls_startfromlowestlevel=false&amp;hls_seekfromlowestlevel=false&amp;hls_live_flushurlcache=false&amp;hls_seekmode=ACCURATE&amp;hls_capleveltostage=false&amp;hls_maxlevelcappingmode=downscale" />
                                        </object>
                                    </asp:Panel>
                                </asp:Panel>

                                <asp:Panel ID="plShareUrlPost" runat="server" CssClass="post-announcement__preview Media">
                                    <div class="post-announcement__preview__image Media-figure">
                                        <asp:Image ID="imgShareThumb" runat="server" Width="200" Height="200" />
                                    </div>
                                    <div class="post-announcement__preview__body Media-body">
                                        <div class="post-announcement__preview__title">
                                            <asp:Literal ID="ltlShareTitle" runat="server" />
                                        </div>
                                        <div class="post-announcement__preview__snippet">
                                            <asp:Literal ID="ltlShareContent" runat="server" />
                                        </div>
                                        <div class="post-announcement__preview__source">
                                            <span class="url">
                                                <asp:Literal ID="ltlShareUrl" runat="server" />
                                            </span>
                                            <asp:Literal ID="ltlShareSiteName" runat="server" />
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="post__content__meta">
                                    <span><i class="fa fa-arrow-up fa-lg"></i><%# DataBinder.Eval(Container.DataItem, "PositivePoints") %></span>
                                    <span><i class="fa fa-arrow-down fa-lg"></i><%# DataBinder.Eval(Container.DataItem, "NegativePoints") %></span>
                                    <span><i class="fa fa-comment fa-lg"></i><%# DataBinder.Eval(Container.DataItem, "NumberOfComments") %></span>
                                </div>

                                <asp:UpdatePanel ID="upComments" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Repeater ID="rtComment" runat="server" OnItemDataBound="rtComment_ItemDataBound" OnItemCommand="rtComment_ItemCommand">
                                            <HeaderTemplate>
                                                <div class="post__content__comments">
                                                    <!-- Comment -->
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="post__content__comment">
                                                    <!-- Report Comment -->
                                                    <asp:Panel ID="plReportCommentPerson" runat="server" CssClass="flyout">
                                                        <div class="post__user">
                                                            <asp:Image ID="imgReportCommentPersonFirst" runat="server" CssClass="post__user__figure" />
                                                            <div class="post__user__body">
                                                                <div class="post__user__body__title">
                                                                    <asp:LinkButton ID="ibReportrCommentFirstName" runat="server" CommandName="ShowReportPerson" />
                                                                    <asp:Literal ID="ltlReportrCommentFirstReason" runat="server" />
                                                                    <asp:LinkButton ID="ibReportrCommentOtherName" runat="server" CommandName="ShowReportPerson" />
                                                                </div>
                                                                <div class="post__user__body__content">
                                                                    <span>
                                                                        <asp:Literal ID="ltlReportrCommentFirstTime" runat="server" />
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <!-- /Report Comment -->
                                                    <div class="post__user">
                                                        <img class="post__user__figure" src="<%# DataBinder.Eval(Container.DataItem, "User.ProfileImageUrl") %>" alt="">
                                                        <div class="post__user__body">
                                                            <div class="post__user__body__title"><%# DataBinder.Eval(Container.DataItem, "User.FirstName") +" " + DataBinder.Eval(Container.DataItem, "User.LastName") %></div>
                                                            <div class="post__user__body__content">
                                                                <span>
                                                                    <%# DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:MMM dd}") + " at " + DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:hh:mm tt}") %>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <asp:Panel ID="plReportCommentAction" runat="server" CssClass="post__user__action">
                                                            <div class="chips--02">
                                                                <div class="chips--02__container">
                                                                    <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                                    <ul class="chips--02__menu">
                                                                        <li id="liAcknowledge" runat="server" visible="false">
                                                                            <asp:LinkButton ID="lbAcknowledge" runat="server" ClientIDMode="AutoID" CommandName="Acknowledge" Text="Acknowledge" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") %>' OnClientClick="ShowProgressBar();" />
                                                                        </li>
                                                                        <li id="liHide" runat="server" visible="false">
                                                                            <asp:LinkButton ID="lbHide" runat="server" ClientIDMode="AutoID" CommandName="HideComment" Text="Hide comment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") %>' OnClientClick="ShowProgressBar();" />
                                                                        </li>
                                                                        <li id="liDelete" runat="server" visible="false">
                                                                            <asp:LinkButton ID="lbDelete" runat="server" ClientIDMode="AutoID" CommandName="DeleteComment" Text="Delete comment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") %>' OnClientClick="ShowProgressBar();" />
                                                                        </li>
                                                                        <li id="liRestore" runat="server" visible="false">
                                                                            <asp:LinkButton ID="lbRestore" runat="server" ClientIDMode="AutoID" CommandName="RestoreComment" Text="Restore comment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") + "," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") %>' OnClientClick="ShowProgressBar();" />
                                                                        </li>
                                                                        <li id="liSuspendAccount" runat="server" visible="false">
                                                                            <asp:LinkButton ID="lbSuspendAccount" runat="server" CommandName="SuspendAccount" />
                                                                        </li>
                                                                        <li id="liSuspendPosting" runat="server" visible="false">
                                                                            <asp:LinkButton ID="lbSuspendPosting" runat="server" CommandName="SuspendPosting" />
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    <asp:Panel ID="plCommentContent" runat="server">
                                                        <p>
                                                            <asp:Literal ID="ltlCommentContent" runat="server"></asp:Literal>
                                                            <%--<%# DataBinder.Eval(Container.DataItem, "CommentText.Content")%>--%>
                                                        </p>
                                                    </asp:Panel>
                                                    <div class="post__content__meta">
                                                        <span><i class="fa fa-arrow-up"></i><%# DataBinder.Eval(Container.DataItem, "PositivePoints")%></span>
                                                        <span><i class="fa fa-arrow-down"></i><%# DataBinder.Eval(Container.DataItem, "NegativePoints")%></span>
                                                    </div>
                                                    <asp:Repeater ID="rtReply" runat="server" OnItemDataBound="rtReply_ItemDataBound" OnItemCommand="rtReply_ItemCommand">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <!-- Reply -->
                                                            <div class="post__content__comment">
                                                                <asp:Panel ID="plReportReplyPerson" runat="server" CssClass="flyout">
                                                                    <div class="post__user">
                                                                        <asp:Image ID="imgReportReplyPersonFirst" runat="server" CssClass="post__user__figure" />
                                                                        <div class="post__user__body">
                                                                            <div class="post__user__body__title">
                                                                                <asp:LinkButton ID="ibReportrReplyFirstName" runat="server" CommandName="ShowReportPerson" />
                                                                                <asp:Literal ID="ltlReportrReplyFirstReason" runat="server" />
                                                                                <asp:LinkButton ID="ibReportrReplyOtherName" runat="server" CommandName="ShowReportPerson" />
                                                                            </div>
                                                                            <div class="post__user__body__content">
                                                                                <span>
                                                                                    <asp:Literal ID="ltlReportrReplyFirstTime" runat="server" />
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <div class="post__user">
                                                                    <img class="post__user__figure" src="<%# DataBinder.Eval(Container.DataItem, "User.ProfileImageUrl") %>" alt="">
                                                                    <div class="post__user__body">
                                                                        <div class="post__user__body__title">
                                                                            <%# DataBinder.Eval(Container.DataItem, "User.FirstName") +" " + DataBinder.Eval(Container.DataItem, "User.LastName") %>
                                                                        </div>
                                                                        <div class="post__user__body__content">
                                                                            <span>
                                                                                <%# DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:MMM dd}") + " at " + DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:hh:mm tt}") %>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <asp:Panel ID="plReportReplyAction" runat="server" CssClass="post__user__action">
                                                                        <div class="chips--02">
                                                                            <div class="chips--02__container">
                                                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                                                <ul class="chips--02__menu">
                                                                                    <li id="liAcknowledge" runat="server" visible="false">
                                                                                        <asp:LinkButton ID="lbAcknowledge" runat="server" ClientIDMode="AutoID" CommandName="Acknowledge" Text="Acknowledge" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") + "," + DataBinder.Eval(Container.DataItem, "ReplyId") %>' OnClientClick="ShowProgressBar();" />
                                                                                    </li>
                                                                                    <li id="liHide" runat="server" visible="false">
                                                                                        <asp:LinkButton ID="lbHide" runat="server" ClientIDMode="AutoID" CommandName="HideReply" Text="Hide reply" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") + "," + DataBinder.Eval(Container.DataItem, "ReplyId") %>' OnClientClick="ShowProgressBar();" />
                                                                                    </li>
                                                                                    <li id="liDelete" runat="server" visible="false">
                                                                                        <asp:LinkButton ID="lbDelete" runat="server" ClientIDMode="AutoID" CommandName="DeleteReply" Text="Delete reply" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") +"," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") + "," + DataBinder.Eval(Container.DataItem, "ReplyId") %>' OnClientClick="ShowProgressBar();" />
                                                                                    </li>
                                                                                    <li id="liRestore" runat="server" visible="false">
                                                                                        <asp:LinkButton ID="lbRestore" runat="server" ClientIDMode="AutoID" CommandName="RestoreReply" Text="Restore reply" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "FeedId") + "," + DataBinder.Eval(Container.DataItem, "ReportPostId") + "," + DataBinder.Eval(Container.DataItem, "CommentId") + "," + DataBinder.Eval(Container.DataItem, "ReplyId") %>' OnClientClick="ShowProgressBar();" />
                                                                                    </li>
                                                                                    <li id="liSuspendAccount" runat="server" visible="false">
                                                                                        <asp:LinkButton ID="lbSuspendAccount" runat="server" CommandName="SuspendAccount" />
                                                                                    </li>
                                                                                    <li id="liSuspendPosting" runat="server" visible="false">
                                                                                        <asp:LinkButton ID="lbSuspendPosting" runat="server" CommandName="SuspendPosting" />
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                                <asp:Panel ID="plReplyContent" runat="server">
                                                                    <p>
                                                                        <asp:Literal ID="ltlReplyContent" runat="server"></asp:Literal>
                                                                        <%--<%# DataBinder.Eval(Container.DataItem, "ReplyText.Content")%>--%>
                                                                    </p>
                                                                </asp:Panel>
                                                                <div class="post__content__meta">
                                                                    <span><i class="fa fa-arrow-up"></i><%# DataBinder.Eval(Container.DataItem, "PositivePoints")%></span>
                                                                    <span><i class="fa fa-arrow-down"></i><%# DataBinder.Eval(Container.DataItem, "NegativePoints")%></span>
                                                                </div>
                                                            </div>
                                                            <!-- /Reply -->
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <!-- /Comment -->
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <!-- /Post Content -->
                        </asp:Panel>
                        <!-- /Post -->
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%--</div>--%>
    </div>
</asp:Content>
