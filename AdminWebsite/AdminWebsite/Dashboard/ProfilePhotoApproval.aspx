﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ProfilePhotoApproval.aspx.cs" Inherits="AdminWebsite.Dashboard.ProfilePhotoApproval" %>

<asp:Content ID="cttMain" ContentPlaceHolderID="main_content" runat="server">

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfApprovalId" runat="server" />
            <asp:Panel ID="popup_profilephotoreject" runat="server" CssClass="popup popup--profilephotoreject" Width="100%" Style="display: none;">
                <h1 class="popup__title">Profile Photo Reject</h1>
                <div class="popup__content">
                    <p>You have rejected the following personnel's photo</p>
                    <div class="Media">
                        <asp:Image ID="imgUserPhoto" runat="server" CssClass="Media-figure Media-figure--noradius" />
                        <%--<img class="Media-figure Media-figure--noradius" src="https://unsplash.it/100" alt="">--%>
                        <p class="Media-body">
                            <asp:Literal ID="ltlUserName" runat="server" />
                            <%--Manuel Internetiquette--%>
                        </p>
                    </div>
                    <%--<asp:TextBox ID="tbRejectReason" runat="server" placeholder="Reason for photo rejection" />--%>
                    <%--<input type="text" placeholder="Reason for photo rejection">--%>
                    <%--<p class="red">An email to explain the rejection will be sent to the personnel</p>--%>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbReject" runat="server" CssClass="popup__action__item popup__action__item--confirm" Text="Reject" OnClick="lbReject_Click" />
                    <asp:LinkButton ID="lbCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbCancel_Click" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_profilephotoreject"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>


    <!-- App Bar -->

    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Dashboard <span>console</span></div>
        <asp:UpdatePanel ID="upAppbar" runat="server" class="appbar__meta">
            <ContentTemplate>
                <%--<div class="appbar__meta appbar__meta--big">--%>
            Approve personnel profile photos 
            <span>
                <asp:Literal ID="ltlCount" runat="server" />
            </span>
                <%--</div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar data__sidebar--size1 filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Dashboard/AlertReport">Alert Report</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Profile Photo Approval</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Dashboard/Feedbacks">Feedback</a>
                </li>
            </ul>
        </aside>

        <!-- Profile Photos -->
        <asp:UpdatePanel ID="upPhotos" runat="server" class="data__content">
            <ContentTemplate>
                <asp:Repeater ID="rtPhotos" runat="server" OnItemCommand="rtPhotos_ItemCommand" OnItemDataBound="rtPhotos_ItemDataBound">
                    <HeaderTemplate>
                        <div class="posts">
                            <asp:Literal ID="ltlEmptyMsg" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="post">
                            <div class="post__reports">
                                <div class="post__profile-header">
                                    <asp:Image ID="imgProfilePhoto" runat="server" CssClass="post__user__figure" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "RequesterUser.ProfileImageUrl")%>' />
                                    <div class="post__user__body">
                                        <div class="post__profile-header__body__title">
                                            <asp:Literal ID="ltlName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "RequesterUser.FirstName") +" " + DataBinder.Eval(Container.DataItem, "RequesterUser.LastName")  %>' />
                                            :
                                    <span class="post__user__report">requested for profile a photo change</span>
                                        </div>
                                        <div class="post__profile-header__body__content">
                                            <span><%# DataBinder.Eval(Container.DataItem, "SeekApprovalTimestamp","{0:MMM dd}") + " at " + DataBinder.Eval(Container.DataItem, "SeekApprovalTimestamp","{0:hh:mm tt}") %></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="post__content">
                                <div class="photoapproval">
                                    <asp:Image ID="imgProfilePhotoNow" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "RequesterUser.ProfileImageUrl")%>' />
                                    <div class="arrow"><i class="fa fa-caret-right fa-5x"></i></div>
                                    <asp:Image ID="imgProfilePhotoNew" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "NewProfileImageUrl")%>' />
                                </div>
                            </div>
                            <div class="post__action">
                                <asp:LinkButton ID="lbApprove" runat="server" CssClass="primary" Text="Approve" CommandName="Approve" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ApprovalId")%>' />
                                <asp:LinkButton ID="lbReject" runat="server" Text="Reject" CommandName="Reject" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ApprovalId")%>' />
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- /Profile Photos -->
    </div>
</asp:Content>
