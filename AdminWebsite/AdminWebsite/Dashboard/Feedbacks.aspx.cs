﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;

namespace AdminWebsite.Dashboard
{
    public partial class Feedbacks : Page
    {
        private ManagerInfo adminInfor;
        private AdminService asc = new AdminService();

        private List<Feed> feedbackList;

        public void GetFeedbackList()
        {
            try
            {
                DashboardSelectFeedbackResponse response = asc.SelectFeedbackFeedPosts(adminInfor.UserId, adminInfor.CompanyId, Convert.ToInt16(ddlFilterStatus.SelectedValue));
                if (response.Success)
                {
                    feedbackList = response.Feedbacks;
                    ltlFeedbackStatus.Text = ddlFilterStatus.SelectedItem.Text;
                    if (feedbackList.Count > 0)
                    {
                        ltlCount.Text = "(" + feedbackList.Count + ")";
                    }
                    else
                    {
                        ltlCount.Text = String.Empty;
                    }
                }
                else
                {
                    Log.Error("DashboardSelectFeedbackResponse.Success is false. ErrorMessage: " + response.ErrorMessage, this.Page);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            adminInfor = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    GetFeedbackList();
                    rtFeedback.DataSource = feedbackList;
                    rtFeedback.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtFeedback_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (feedbackList != null && feedbackList.Count > 0)
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        #region Find controllers
                        System.Web.UI.WebControls.Image imgAuthorPhoto = e.Item.FindControl("imgAuthorPhoto") as System.Web.UI.WebControls.Image;
                        Literal ltlAuthorName = e.Item.FindControl("ltlAuthorName") as Literal;
                        Literal ltlTimestamp = e.Item.FindControl("ltlTimestamp") as Literal;
                        Literal ltlContent = e.Item.FindControl("ltlContent") as Literal;
                        LinkButton lbAcknowledge = e.Item.FindControl("lbAcknowledge") as LinkButton;
                        LinkButton lbHide = e.Item.FindControl("lbHide") as LinkButton;
                        LinkButton lbRestore = e.Item.FindControl("lbRestore") as LinkButton;
                        Panel plComment = e.Item.FindControl("plComment") as Panel;
                        Button btnComment = e.Item.FindControl("btnComment") as Button;
                        Repeater rtComment = e.Item.FindControl("rtComment") as Repeater;
                        #endregion

                        #region Set controllers value
                        imgAuthorPhoto.ImageUrl = feedbackList[e.Item.ItemIndex].User.ProfileImageUrl;
                        ltlAuthorName.Text = feedbackList[e.Item.ItemIndex].User.FirstName + " " + feedbackList[e.Item.ItemIndex].User.LastName;
                        ltlTimestamp.Text = feedbackList[e.Item.ItemIndex].CreatedOnTimestamp.ToString("MMM dd") + " at " + feedbackList[e.Item.ItemIndex].CreatedOnTimestamp.ToString("hh:mm tt");
                        ltlContent.Text = feedbackList[e.Item.ItemIndex].FeedText.Content;
                        btnComment.CommandArgument = feedbackList[e.Item.ItemIndex].FeedId;
                        lbAcknowledge.CommandArgument = feedbackList[e.Item.ItemIndex].FeedId;
                        lbHide.CommandArgument = feedbackList[e.Item.ItemIndex].FeedId;
                        lbRestore.CommandArgument = feedbackList[e.Item.ItemIndex].FeedId;

                        //Activity
                        if (feedbackList[e.Item.ItemIndex].DashboardFeedbackType == 1)
                        {
                            lbAcknowledge.Visible = true;
                            lbHide.Visible = true;
                            lbRestore.Visible = false;
                            plComment.Visible = true;
                        }
                        //Acknowledge
                        else if (feedbackList[e.Item.ItemIndex].DashboardFeedbackType == 2)
                        {
                            lbAcknowledge.Visible = false;
                            lbHide.Visible = true;
                            lbRestore.Visible = false;
                            plComment.Visible = true;
                        }
                        //Hide
                        else if (feedbackList[e.Item.ItemIndex].DashboardFeedbackType == 3)
                        {
                            lbAcknowledge.Visible = false;
                            lbHide.Visible = false;
                            lbRestore.Visible = true;
                            plComment.Visible = false;
                        }
                        else
                        {

                        }

                        rtComment.DataSource = feedbackList[e.Item.ItemIndex].Comments;
                        rtComment.DataBind();
                        #endregion
                    }
                }
                else
                {
                    if (e.Item.ItemType == ListItemType.Header)
                    {
                        Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                        ltlEmptyMsg.Text = "There are no feedback in need of attention at the moment.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtFeedback_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Post"))
                {
                    TextBox tbComment = e.Item.FindControl("tbComment") as TextBox;
                    DashboardCreateFeedbackCommentResponse response = asc.CreateFeedbackComment(adminInfor.UserId, adminInfor.CompanyId, Convert.ToString(e.CommandArgument), tbComment.Text);
                    String toastMsg = String.Empty;
                    if (response.Success)
                    {
                        UpdatePanel upComments = e.Item.FindControl("upComments") as UpdatePanel;
                        Repeater rtComment = e.Item.FindControl("rtComment") as Repeater;
                        rtComment.DataSource = response.Comments;
                        rtComment.DataBind();
                        tbComment.Text = String.Empty;
                        upComments.Update();
                    }
                    else
                    {
                        Log.Error("CommentCreateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        toastMsg = "Failed to change status, please check your internet connection.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
                else if (e.CommandName.Equals("Acknowledge") || e.CommandName.Equals("Hide") || e.CommandName.Equals("Restore"))
                {
                    String toastMsg = String.Empty;
                    int actionType = 0;
                    if (e.CommandName.Equals("Acknowledge"))
                    {
                        actionType = (int)CassandraService.Entity.Dashboard.FeedbackState.Acknowledged;
                        toastMsg = @"Feedback has been acknowledged.";
                    }
                    else if (e.CommandName.Equals("Hide"))
                    {
                        actionType = (int)CassandraService.Entity.Dashboard.FeedbackState.Hidden;
                        toastMsg = @"Feedback has been hidden.";
                    }
                    else if (e.CommandName.Equals("Restore"))
                    {
                        actionType = (int)CassandraService.Entity.Dashboard.FeedbackState.Active;
                        toastMsg = @"Feedback has been reverted to Active.";
                    }

                    DashboardUpdateFeedbackStatusResponse response = asc.UpdateFeedbackPostStatus(adminInfor.UserId, adminInfor.CompanyId, Convert.ToString(e.CommandArgument), actionType);
                    if (response.Success)
                    {
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                        GetFeedbackList();
                        rtFeedback.DataSource = feedbackList;
                        rtFeedback.DataBind();
                    }
                    else
                    {
                        Log.Error("DashboardUpdateFeedbackStatusResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        toastMsg = "Failed to change status, please try again later.";
                        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void ddlFilterStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetFeedbackList();
                rtFeedback.DataSource = feedbackList;
                rtFeedback.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtFeedback_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (feedbackList == null || feedbackList.Count == 0)
            {
                Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                ltlEmptyMsg.Text = "There are no feedback in need of attention at the moment.";
            }
        }
    }
}