﻿<%@ Page Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Feedbacks.aspx.cs" Inherits="AdminWebsite.Dashboard.Feedbacks" %>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Dashboard <span>console</span></div>
        <asp:UpdatePanel ID="upAppbar" runat="server" class="appbar__meta">
            <ContentTemplate>
                <%--<div class="appbar__meta appbar__meta--big">--%>
                <asp:Literal ID="ltlFeedbackStatus" runat="server" />
                <span>
                    <asp:Literal ID="ltlCount" runat="server" />
                </span>
                <%--</div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar data__sidebar--size1 filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Dashboard/AlertReport">Alert Report</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Dashboard/ProfilePhotoApproval">Profile Photo Approval</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Feedback</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>By Status</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterStatus_SelectedIndexChanged">
                                <asp:ListItem Text="Active Feedback" Value="1" />
                                <asp:ListItem Text="Acknowledged Feedback" Value="2" />
                                <asp:ListItem Text="Hidden Feedback" Value="3" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </aside>

        <!-- Feedbacks -->
        <asp:UpdatePanel ID="upFeedbacks" runat="server" class="data__content">
            <ContentTemplate>
                <asp:Repeater ID="rtFeedback" runat="server" OnItemDataBound="rtFeedback_ItemDataBound" OnItemCommand="rtFeedback_ItemCommand" >
                    <HeaderTemplate>
                        <div class="posts">
                            <asp:Literal ID="ltlEmptyMsg" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="post">
                            <div class="post__content">
                                <div class="post__user">
                                    <asp:Image ID="imgAuthorPhoto" runat="server" CssClass="post__user__figure" />
                                    <div class="post__user__body">
                                        <div class="post__user__body__title">
                                            <asp:Literal ID="ltlAuthorName" runat="server" />
                                        </div>
                                        <div class="post__user__body__content">
                                            <span>
                                                <asp:Literal ID="ltlTimestamp" runat="server" /></span>
                                        </div>
                                    </div>
                                    <div class="post__user__action">
                                        <asp:LinkButton ID="btnChips" runat="server" CssClass="fa fa-ellipsis-v" OnClientClick="return false;" />
                                        <asp:Panel ID="chipsPnl" runat="server" CssClass="chips">
                                            <ul>
                                                <li>
                                                    <asp:LinkButton ID="lbAcknowledge" ClientIDMode="AutoID" runat="server" CommandName="Acknowledge" Text="Acknowledge" Visible="false" />
                                                </li>
                                                <li>
                                                    <asp:LinkButton ID="lbHide" ClientIDMode="AutoID" runat="server" CommandName="Hide" Text="Hide" Visible="false" />
                                                </li>
                                                <li>
                                                    <asp:LinkButton ID="lbRestore" ClientIDMode="AutoID" runat="server" CommandName="Restore" Text="Restore" Visible="false" />
                                                </li>
                                            </ul>
                                        </asp:Panel>
                                        <ajaxToolkit:PopupControlExtender ID="PopEx" runat="server"
                                            TargetControlID="btnChips"
                                            PopupControlID="chipsPnl"
                                            Position="Left"
                                            OffsetX="-175" />
                                        <%--<div class="chips">
                                        <div class="chips__container">
                                            <i class="fa fa-ellipsis-v chips__button"></i>
                                            <ul class="chips__menu">
                                                <li><a href="#">Mark as completed</a></li>
                                                <li><a href="#">Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>--%>
                                    </div>
                                </div>
                                <div class="post__content__article">

                                    <p>
                                        <asp:Literal ID="ltlContent" runat="server" />
                                    </p>
                                </div>
                                <asp:UpdatePanel ID="upComments" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                    <ContentTemplate>

                                        <asp:Repeater ID="rtComment" runat="server">
                                            <HeaderTemplate>
                                                <div class="post__content__replies">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="post__content__reply">
                                                    <div class="post__user">

                                                        <asp:Image ID="imgUser" CssClass="post__user__figure" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "User.ProfileImageUrl")%>' />

                                                        <div class="post__user__body">
                                                            <div class="post__user__body__title">
                                                                <%# DataBinder.Eval(Container.DataItem, "User.FirstName") +" " + DataBinder.Eval(Container.DataItem, "User.LastName")  %>
                                                            </div>
                                                            <div class="post__user__body__content">
                                                                <span><%# DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:MMM dd}") + " at " + DataBinder.Eval(Container.DataItem, "CreatedOnTimestamp","{0:hh:mm tt}") %></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="post__content__article">
                                                        <p><%# DataBinder.Eval(Container.DataItem, "CommentText.Content")%></p>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <asp:Panel ID="plComment" runat="server" CssClass="post__send">
                                <asp:TextBox ID="tbComment" runat="server" TextMode="MultiLine" placeholder="Add a feedback reply" />
                                <asp:Button ID="btnComment" ClientIDMode="AutoID" runat="server" Text="Post" CommandName="Post" />
                            </asp:Panel>

                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- /Feedbacks -->
    </div>
</asp:Content>
