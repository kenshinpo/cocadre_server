﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CassandraService.Entity.User;

namespace AdminWebsite.Setting
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (managerInfo.AccountType.Code == AccountType.CODE_COCADRE_ADMIN || managerInfo.AccountType.Code == AccountType.CODE_SUPER_ADMIN)
                {
                    hfCompanyId.Value = "";
                }
                else
                {
                    hfCompanyId.Value = managerInfo.CompanyId;
                }
                hfAccountType.Value = Convert.ToString(managerInfo.AccountType.Code);
                hfManagerId.Value = managerInfo.UserId;
                hfManagerEmail.Value = managerInfo.Email;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}