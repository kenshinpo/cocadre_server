﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="AdminWebsite.Setting.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link type="text/css" rel="stylesheet" href="/css/jquery-entropizer.min.css" />
    <script type="text/javascript" src="/js/entropizer.js"></script>
    <script type="text/javascript" src="/js/jquery-entropizer.min.js"></script>
    <style>
        input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(145, 145, 145, 1);
        }

        input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(145, 145, 145, 1);
        }

        #strength_meter .entropizer-track {
            background-color: rgba(204, 204, 204, 1);
            border-radius: 2px;
            height: 8px;
        }

        #strength_meter .entropizer-bar {
            height: 8px;
        }
    </style>
    <script type="text/javascript">
        "use strict";

        (function ($) {
            $(document).ready(function () {
                passwordStrength();
            });
        })(jQuery);

        function passwordStrength() {
            $('#strength_meter').entropizer({
                target: '#tbNewPassword',
                buckets: [
                    { max: 20, message: 'Too short', color: 'rgba(204, 204, 204, 1)' },
                    { min: 20, max: 40, message: 'Weak', color: 'rgba(251, 208, 59, 1)' },
                    { min: 40, max: 60, message: 'Fail', color: 'rgba(251, 208, 59, 1)' },
                    { min: 60, max: 80, message: 'Good', color: 'rgba(0, 113, 188, 1)' },
                    { min: 80, message: 'Strong', color: 'rgba(0, 146, 69, 1)' }
                ],
                update: function (data, ui) {
                    ui.bar.css({
                        'background-color': data.color,
                        'width': data.percent + '%'
                    });

                    if (data.percent > 0) {
                        $("#strength_text").html(data.message);
                    }
                    else {
                        $("#strength_text").html("");
                    }
                }
            });
        }

        function isDataValid() {
            // current passowrd
            var currentPassword = $("#tbCurrentPassword").val().trim();
            if (currentPassword == null || currentPassword.length == 0) {
                ShowToast("Please fill in Current Password", 2);
                return false;
            }

            var newPassword = $("#tbNewPassword").val().trim();
            if (newPassword == null || newPassword.length == 0) {
                ShowToast("Please fill in New Password", 2);
                return false;
            }

            var confirmPassword = $("#tbConfirmPassword").val().trim();
            if (confirmPassword == null || confirmPassword.length == 0) {
                ShowToast("Your passwords does not match", 2);
                return false;
            }
            if (newPassword !== confirmPassword) {
                ShowToast("Your passwords does not match", 2);
                return false;
            }

            return true;
        }

        function changePassword() {
            if (!isDataValid()) {
                return;
            }

            var request = {
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'Email': $("#main_content_hfManagerEmail").val(),
                'ManagerId': $("#main_content_hfManagerId").val(),
                'NewPassword': $("#tbConfirmPassword").val().trim(),
                'CurrentPassword': $("#tbCurrentPassword").val().trim(),
                'AccountType': parseInt($("#main_content_hfAccountType").val().trim())
            };

            jQuery.ajax({
                type: "POST",
                url: "/Api/Account/ChangePassword",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(request),
                dataType: "json",
                crossDomain: true,
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        ShowToast("Password updated", 1);
                        RedirectPage("/Setting/ChangePassword", 300);
                    } else {
                        ShowToast(d.ErrorMessage, 2);
                    }
                },
                error: function (xhr, status, error) {
                    ShowToast(error, 2);
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfManagerEmail" />
    <asp:HiddenField runat="server" ID="hfAccountType" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Account Setting <span></span></div>
        <div class="appbar__meta">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Literal ID="ltlCount" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: bolder;">Manage</div>
            <ul class="data__sidebar__list" style="margin-top: 20px;">
                <%--
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Gamification/ExperiencePoints">Experience Points</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Gamification/Leaderboard">Leaderboard</a>
                </li>`
                --%>

                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Change Password</a>
                </li>
            </ul>
        </aside>

        <div class="data__content">
            <div style="padding: 30px; color: rgba(145, 145, 145, 1);">
                <label style="font-weight: 400; font-size: 1.5em; color: rgba(80, 80, 80, 1);">Change Password</label>
                <label style="margin-top: 30px;">Choose a strong password and don't reuse it for other accounts.</label>
                <label style="margin-top: 15px;">Changing your password will sign you out of all your devices, including your phone. You will need to enter your new password on all your devices.</label>

                <input id="tbCurrentPassword" type="password" placeholder="Current Password" style="width: 300px; margin-top: 30px;" />
            </div>
            <hr style="color: rgba(145, 145, 145, 1); margin: 0px;" />
            <div style="padding: 30px; color: rgba(145, 145, 145, 1);">
                <input id="tbNewPassword" type="password" placeholder="New Password" style="width: 300px;" />

                <div style="text-align: left; margin-top: 30px;">
                    <label style="display: inline-block; color: rgba(27, 132, 255, 1);">Password strength</label>
                    <label style="display: inline-block; color: rgba(80, 80, 80, 1);" id="strength_text"></label>
                </div>
                <div id="strength_meter" class="col-md-10 col-md-offset-2" style="width: 300px;"></div>

                <input id="tbConfirmPassword" type="password" placeholder="New Password, again" style="width: 300px; margin-top: 30px;" />
            </div>
            <div style="padding: 30px; width: 360px; color: rgba(145, 145, 145, 1);">
                <span style="float: right; padding: 10px 25px; border-radius: 5px; font-size: 1.2em; color: #FFF; background-color: rgba(0, 118, 255, 1); cursor: pointer;" onclick="changePassword();">SAVE</span>
            </div>
        </div>
    </div>
</asp:Content>
