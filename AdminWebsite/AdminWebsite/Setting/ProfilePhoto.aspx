﻿<%@ Page Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="ProfilePhoto.aspx.cs" Inherits="AdminWebsite.Setting.ProfilePhoto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }

        .invalid-field {
            border-bottom-color: #ff0000 !important;
        }

        .grey-text {
            color: #ccc;
        }

        .chevron:hover {
            color: #839423 !important;
            cursor: pointer;
        }

        .text-colour {
            /*
                selected" style="width: 16px; height: 16px; background-color: #000; border: solid 2px blue; display: inline-block;" onclick="setTextColor('black');"></div>
            */
            width: 16px;
            height: 16px;
            border: solid 2px #ccc;
            display: inline-block;
        }

            .text-colour.selected {
                border: solid 2px blue !important;
            }

        .ios-toggle,
        .ios-toggle:active {
            position: absolute;
            top: -5000px;
            height: 0;
            width: 0;
            opacity: 0;
            border: none;
            outline: none;
        }

        .checkbox-label {
            display: block;
            position: relative;
            padding: 10px;
            margin-bottom: 20px;
            font-size: 12px;
            line-height: 16px;
            width: 100%;
            height: 36px;
            /*border-radius*/
            -webkit-border-radius: 18px;
            -moz-border-radius: 18px;
            border-radius: 18px;
            background: #f8f8f8;
            cursor: pointer;
        }

            .checkbox-label:before {
                content: '';
                display: block;
                position: absolute;
                z-index: 1;
                line-height: 34px;
                text-indent: 40px;
                height: 36px;
                width: 36px;
                /*border-radius*/
                -webkit-border-radius: 100%;
                -moz-border-radius: 100%;
                border-radius: 100%;
                top: 0px;
                left: 0px;
                right: auto;
                background: white;
                /*box-shadow*/
                -webkit-box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
                -moz-box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
                box-shadow: 0 3px 3px rgba(0,0,0,.2), 0 0 0 2px #dddddd;
            }

            .checkbox-label:after {
                content: attr(data-off);
                display: block;
                position: absolute;
                z-index: 0;
                top: 0;
                left: -300px;
                padding: 10px;
                height: 100%;
                width: 300px;
                text-align: right;
                color: #bfbfbf;
                white-space: nowrap;
            }

        .ios-toggle:checked + .checkbox-label {
            /*box-shadow*/
            -webkit-box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
            -moz-box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
            box-shadow: inset 0 0 0 20px rgba(19,191,17,1), 0 0 0 2px rgba(19,191,17,1);
        }

            .ios-toggle:checked + .checkbox-label:before {
                left: calc(100% - 36px);
                /*box-shadow*/
                -webkit-box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
                -moz-box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
                box-shadow: 0 0 0 2px transparent, 0 3px 3px rgba(0,0,0,.3);
            }

            .ios-toggle:checked + .checkbox-label:after {
                content: attr(data-on);
                left: 60px;
                width: 36px;
            }

        .onoffswitch {
            position: relative;
            width: 76px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
        }

        .onoffswitch-checkbox {
            display: none;
        }

        .onoffswitch-label {
            display: block;
            overflow: hidden;
            cursor: pointer;
            border-radius: 20px;
        }

        .onoffswitch-inner {
            display: block;
            width: 200%;
            margin-left: -100%;
            transition: margin 0.3s ease-in 0s;
        }

            .onoffswitch-inner:before, .onoffswitch-inner:after {
                display: block;
                float: left;
                width: 50%;
                height: 30px;
                padding: 0;
                line-height: 30px;
                font-size: 13px;
                color: white;
                font-weight: bold;
                box-sizing: border-box;
            }

            .onoffswitch-inner:before {
                content: "ON";
                padding-left: 10px;
                background-color: #709EEB;
                color: #F2F2F2;
            }

            .onoffswitch-inner:after {
                content: "OFF";
                padding-right: 10px;
                background-color: #4d4d4d;
                color: #F2F2F2;
                text-align: right;
            }

        .onoffswitch-switch {
            display: block;
            width: 26px;
            height: 26px;
            margin: 2px;
            background: #F2F2F2;
            position: absolute;
            bottom: 0px;
            right: 46px;
            border-radius: 20px;
            transition: all 0.3s ease-in 0s;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
            margin-left: 0;
        }

        .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
            right: 0px;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />

    <script>

        function setAutoApproval(input) {
            ShowProgressBar();
            $.ajax({
                type: "POST",
                url: '/Setting/UpdateProfilePhotoSetting',
                data: {
                    "CompanyId": CompanyId,
                    "ManagerId": ManagerId,
                    "IsAutoApproval": input.checked
                },
                crossDomain: true,
                dataType: 'json',
                success: function (res) {
                    if (res.Success) {
                        toastr.info("Update successful");
                    }
                    else {
                        toastr.error("Update faild");
                    }
                    $("#checkbox_autoApproval").prop("checked", res.IsAutoApproval);
                    HideProgressBar();
                }
            });

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Setting <span>Console</span></div>
        <div class="appbar__meta">Profile Photo Setting</div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Profile Photo Setting</a>
                </li>
            </ul>
        </aside>

        <div class="data__content" style="padding-left: 40px;">

            <div style="margin-top:30px; width:500px; ">
                <div style="width: 130px; float:left; text-align:left;">
                    <p style="font-size: 1.2em; font-weight: bolder;">Profile Photo Auto Approval</p>
                </div>
                <div style="width: auto; float:right;">
                    <div class="onoffswitch" style="margin-top:10px;">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="checkbox_autoApproval" style="display: none" onchange="setAutoApproval(this);" />
                        <label class="onoffswitch-label" for="checkbox_autoApproval">
                            <span class="onoffswitch-inner"></span>
                            <span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </div>
            </div>
            
            <div style="clear:both; margin-top:10px;">
                <p style="margin-bottom:0px;">Go to the 'Dashboard Console' to manually approve Personnel's profile photo when the toggle is 'Off'.</p>
                <p style="margin-bottom:0px;">Turn 'On' to auto approve Personnel's profile photo for the Client and Admin Tool.</p>
            </div>

        </div>
    </div>
</asp:Content>
