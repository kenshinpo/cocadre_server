﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace AdminWebsite.Setting
{
    public partial class ProfilePhoto : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout", false);
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    AutoApprovalProfilePhotoResponse response = asc.GetAutoApprovalProfilePhoto(managerInfo.CompanyId, managerInfo.UserId);
                    if (response.Success)
                    {
                        String jsCommand = @"
                            var CompanyId = '" + managerInfo.CompanyId + @"';
                            var ManagerId = '" + managerInfo.UserId + @"';
                        ";

                        if (response.IsAutoApproval)
                        {
                            jsCommand += "document.getElementById('checkbox_autoApproval').checked = true;";
                        }
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "defineVar", jsCommand, true);
                    }
                    else
                    {
                        // data error
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
    }
}