﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="AdminWebsite.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <title>Sign In - Cocadre</title>
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/scss/app.css" />
    <link rel="stylesheet" href="/css/toastr.min.css" />
    <style>
        html,
        body {
            background: white;
        }

        .companies a {
            height: 130px !important;
        }

        .index-company-name {
            width: 100% !important;
            height: auto !important;
            padding: 3px !important;
            display: block;
            color: #000;
            font-size: 16px;
            text-align: center;
            line-height: 18px;
            position: relative !important;
            margin-bottom: 30px;
        }

        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(211, 211, 211, 1);
        }

        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(212, 212, 212, 1);
            opacity: 1;
        }

        ::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(212, 212, 212, 1);
            opacity: 1;
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(212, 212, 212, 1);
        }
    </style>
    <script src="/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/apo-min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-ui-1.11.4.js"></script>
    <script src="/js/toastr.min.js"></script>

    <script type="text/javascript">
        function forgotPassword() {
            try {
                var request = {
                    'Email': $("#email").val()
                };

                jQuery.ajax({
                    type: "POST",
                    url: "/Api/Account/ForgotPassword",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(request),
                    dataType: "json",
                    crossDomain: true,
                    beforeSend: function (xhr, settings) {
                        ShowProgressBar();
                    },
                    success: function (d, status, xhr) {
                        if (d.Success) {
                            $(".forgot").css("display", "none");
                            $(".sent").css("display", "block");
                            $(".show_email").html($("#email").val());
                        } else {
                            ShowToast(d.ErrorMessage, 2);
                        }
                    },
                    error: function (xhr, status, error) {
                        ShowToast(error, 2);
                    },
                    complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                        HideProgressBar();
                    }
                });
            } catch (e) {
                console.error(e.toString());
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hfAction" runat="server" />
        <asp:HiddenField ID="hfEmail" runat="server" />

        <div id="divProgress" style="text-align: center; display: none; position: fixed; top: 50%; left: 50%;">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
            <div id="ProgressText" style="font-size: 15px; color: #1B3563;">Processing...</div>
        </div>
        <div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;"></div>


        <div class="login forgot">
            <div class="login__header">
                <a href="http://www.cocadre.com">
                    <div class="login__header__logo">Cocadre</div>
                </a>
                <div class="login__header__title">Forgot password</div>
            </div>
            <div class="login__card card">
                <label>Enter your email address to retrieve</label>
                <input name="email" id="email" type="text" placeholder="Email Address" />
                <div>
                    <a href="/Login" class="login__card__submit btn brandcolor" style="border-color: rgba(230, 230, 230, 1); width: 100px; color: black; background-color: rgba(230, 230, 230, 1);">Cancel</a>
                    <input name="btnNext" class="login__card__submit btn brandcolor" id="btnNext" type="button" value="Next" style="width: 100px;" onclick="forgotPassword();" />
                </div>
            </div>
        </div>

        <div class="login sent" style="display: none;">
            <div class="login__header">
                <a href="http://www.cocadre.com">
                    <div class="login__header__logo">Cocadre</div>
                </a>
            </div>
            <div class="login__card card" style="width: 600px; color: rgba(77, 77, 77, 1); font-size: 1.2em; padding: 2.5em;">
                <label style="font-weight: bold;">Instructions to reset your password has been sent to your mail box</label>
                <label style="font-weight: bold;">Please check your inbox</label>
                <img src="/Img/icon_forgot_password_sent.png" style="margin-top: 20px;" />
                <label>We have sent the verification email to:</label>
                <label style="font-weight: bold; font-size: 1.5em;" class="show_email"></label>

                <label style="margin: 20px 0px;">Wrong email? <a href="/ForgotPassword">Change address</a></label>

                <label style="margin-bottom: -2px;">Did you mean to sign in?</label>

                <a href="/Login">Sign in to an existing account</a>
            </div>
        </div>

    </form>
</body>
</html>
