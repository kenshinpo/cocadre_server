﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="PostNotification.aspx.cs" Inherits="AdminWebsite.Announcement.PostNotification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <!-- Floating Action Button -->
    <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
        <li class="mfb-component__wrap">
            <a class="mfb-component__button--main" data-mfb-label="Add PostNotification" style="background: rgba(255, 150, 0, 1);">
                <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
            </a>
        </li>
    </ul>
    <!-- /Floating Action Button -->

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Annoucement <span>Post notification</span></div>
        <div class="appbar__meta">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Literal ID="ltlCount" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: bolder;">Manage</div>
            <ul class="data__sidebar__list" style="margin-top: 20px;">

                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Post notification</a>
                </li>
            </ul>

            <div class="data__sidebar__title" style="margin-top: 200px;">Filters</div>

            <div class="pad">
                <label>By Status</label>
            </div>

        </aside>

        <div class="data__content">
        </div>
    </div>
</asp:Content>
