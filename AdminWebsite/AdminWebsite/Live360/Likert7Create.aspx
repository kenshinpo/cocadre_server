﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Likert7Create.aspx.cs" Inherits="AdminWebsite.Live360.Likert7Create" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }

        input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(145, 145, 145, 1);
        }

        input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(145, 145, 145, 1);
        }
    </style>
    <script src="/js/Live360/Liker7Detail.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>
    <div id="dvSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Select Department</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <input id="selectAllDepartment" type="checkbox" onclick="selectAllDepartments(this);" style="margin: 0px;" />
                        <label for="selectAllDepartment" style="margin: 0px;">Select All</label>
                    </div>
                    <hr />
                    <div class="accessrights departments">
                        <span id="department_checkboxlist"></span>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:setLikert7Value(9, this, null);">Select</a>
            <a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup(1)">Cancel</a>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTimezone" />
    <asp:HiddenField runat="server" ID="hfLikert7Id" />

    <div class="data__content">

        <!-- App Bar -->
        <div class="appbar">
            <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
            <div class="appbar__title"><a href="/Live360/Likert7List" style="color: #000;">Live 360 <span>Likert 7</span></a></div>
            <div class="appbar__meta">
                <label>Add 'Likert7'</label>
            </div>
            <div class="appbar__action">
            </div>
        </div>
        <!-- / App Bar -->

        <div class="responsive-pulse-root">
            <div class="container" style="padding: 25px;">

                <div class="card card-header grid">
                    <div class="grid__inner" style="background-color: transparent;">
                        <!-- Logo -->
                        <div class="grid__span--2">
                            <div style="text-align: center; width: 120px; height: 120px;">
                            </div>
                        </div>
                        <!-- Logo -->

                        <div class="grid__span grid__span--10 grid__span--last" style="background-color: transparent;">
                            <!-- CARD HEADER (ROW HEADER) START -->
                            <div class="grid__inner" style="background-color: transparent;">

                                <div style="float: right; width: 50%;">
                                    <div class="grid__span--11">
                                        <button id="lblSave" type="button" class="survey-bar__search__button" onclick="createLikert7();">Create</button>
                                        <a class="survey-bar__search__button" href="/Live360/Likert7List">Cancel</a>
                                    </div>
                                </div>

                                <div class="tabs tabs--styled">
                                    <ul class="tabs__list">
                                        <li class="tabs__list__item">
                                            <a class="tabs__link" href="#pulse-deck-info">Appraisal Info</a>
                                        </li>
                                        <li class="tabs__list__item">
                                            <a class="tabs__link" href="#pulse-deck-privacy">Privacy</a>
                                        </li>
                                    </ul>
                                    <div class="tabs__panels">
                                        <!-- PULSE DECK INFO (CONTENT) START -->
                                        <div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-info">
                                            <div class="grid__inner" style="background-color: transparent;">
                                                <div class="grid__span--8">

                                                    <div>
                                                        <div class="data" style="margin: 0 0 1.5em;">
                                                            <label for="tbTitle" style="width: 80px; font-size: 1.2em; font-weight: bold; margin-right: 20px; vertical-align: middle; display: inline-block;">Title</label>
                                                            <div class="text_container" style="width: 80%; vertical-align: middle; display: inline-block; position: relative;">
                                                                <input id="tbTitle" style="border-image: none; width: 95%; margin: 0px;" type="text" placeholder="Title of template" onblur="setLikert7Value(1, this, null);" onkeyup="letterCounter(this, COUNT_MAX.TITLE, 'red', '#DDD');" />
                                                                <span class="letterCount" style="right: 5px; bottom: 10px; color: #DDD; position: absolute;"></span>
                                                            </div>
                                                        </div>

                                                        <div class="data" style="margin: 0 0 1.5em;">
                                                            <label for="tbContext" style="width: 80px; font-size: 1.2em; font-weight: bold; margin-right: 20px; vertical-align: middle; display: inline-block;">Context</label>
                                                            <div class="text_container" style="width: 80%; vertical-align: middle; display: inline-block; position: relative;">
                                                                <textarea id="tbContext" style="width: 95%; margin: 0px;" placeholder="Context of the feedback" onblur="setLikert7Value(2, this, null);" onkeyup="letterCounter(this, COUNT_MAX.CONTEXT, 'red', '#DDD');"></textarea>
                                                                <span class="letterCount" style="right: 2px; bottom: 10px; color: #DDD; position: absolute;"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form__row">
                                                            <div class="grid">
                                                                <div class="grid__inner" style="background-color: transparent;">
                                                                    <div class="grid__span--8">
                                                                        <h3>Publish method</h3>
                                                                        <h5>
                                                                            <label style="display: inline; margin-left: 10px;">
                                                                                <input type="radio" name="publish_method" class="publish_method publish_method-schedule" value="1" onclick="setLikert7Value(3, this, null);" />
                                                                                <span>Schedule</span>
                                                                            </label>
                                                                            <label style="display: inline; margin-left: 10px;">
                                                                                <input type="radio" name="publish_method" class="publish_method publish_method-perpetual" value="2" onclick="setLikert7Value(3, this, null);" />
                                                                                <span>Perpetual</span>
                                                                            </label>
                                                                        </h5>

                                                                        <div class="data">
                                                                            <span style="display: inline;">
                                                                                <span style="width: 70px; text-align: right; display: inline-block;">Start date</span>
                                                                                <input type="text" id="startDate" style="border-width: 0px 0px 1px; width: 100px; display: inline;" placeholder="dd/mm/yyyy" class="input-field" onchange="setLikert7Value(4, this, null);" />
                                                                                <span>at</span>
                                                                                <input id="startDateHH" style="border-width: 0px 0px 1px; border-style: none none solid; border-color: currentColor currentColor rgb(221, 221, 221); border-image: none; width: 4em; display: inline;" onchange="setLikert7Value(4, this, null);" type="text" maxlength="2" placeholder="hh" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                                <span>:</span>
                                                                                <input type="text" id="startDateMM" maxlength="2" style="border-width: 0px 0px 1px; width: 4em; display: inline;" placeholder="mm" onchange="setLikert7Value(4, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                                <select id="startDateMR" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 0.65em; margin-bottom: 0.75em;" onchange="setLikert7Value(4, this, null);">
                                                                                    <option value="am">am</option>
                                                                                    <option value="pm">pm</option>
                                                                                </select>
                                                                            </span>
                                                                        </div>

                                                                        <div id="dvEndDate" class="data" style="margin-top: 20px;">
                                                                            <span style="display: inline;">
                                                                                <span style="width: 70px; text-align: right; display: inline-block;">End date</span>
                                                                                <%--<label style="display: inline;">
                                                                                <span style="display: inline;">--%>
                                                                                <input type="text" id="endDate" style="border-width: 0px 0px 1px; width: 100px; display: inline;" placeholder="dd/mm/yyyy" class="input-field" onchange="setLikert7Value(5, this, null);" />
                                                                                <span>at</span>
                                                                                <input type="text" id="endDateHH" maxlength="2" style="border-width: 0px 0px 1px; width: 4em; display: inline;" placeholder="hh" onchange="setLikert7Value(5, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                                <span>:</span>
                                                                                <input type="text" id="endDateMM" maxlength="2" style="border-width: 0px 0px 1px; width: 4em; display: inline;" placeholder="mm" onchange="setLikert7Value(5, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                                <select id="endDateMR" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 0.65em; margin-bottom: 0.75em;" onchange="setLikert7Value(5, this, null);">
                                                                                    <option value="am">am</option>
                                                                                    <option value="pm">pm</option>
                                                                                </select>
                                                                                <%--</span>
                                                                            </label>--%>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="grid__span--4">
                                                    <div>
                                                        <div class="form__row">
                                                            <label style="font-weight: 700; float: left">
                                                                <i class="fa fa-star" style="color: #FFC700"></i>
                                                                <span>Priority Overrule </span>
                                                            </label>
                                                            <div class="mdl-selectfield">
                                                                <select id="ddlPriorityOverrule" style="display: inline-block; padding-bottom: 4px;" onchange="setLikert7Value(12, this, null);">
                                                                    <option value="false" selected="selected">No</option>
                                                                    <option value="true">Yes</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form__row anonymous__tip">
                                                            <label style="font-weight: 700; display: inline-block;">Anonymity</label>
                                                            <i class="fa fa-info-circle tooltip-icon" style="color: rgb(230, 230, 230); font-size: 1.4em; margin-left: 5px;"></i>
                                                            <div class="anonymous_tip" style="display: none; top: 20px;">
                                                                <img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />
                                                                <label style="font-weight: 700;">Anonymous</label>
                                                                Participant's detail will not be disclosed in the Final Report.
                                                            </div>
                                                        </div>
                                                        <div class="mdl-selectfield" style="width: 100%; margin-top: 1.5em;">
                                                            <select id="ddlAnonymous" style="display: inline-block; padding-bottom: 4px;">
                                                                <option value="1" selected="selected">User define</option>
                                                            </select>
                                                        </div>

                                                        <div class="form__row">
                                                            <label style="font-weight: 700; float: left">
                                                                <span>Customize Question</span>
                                                            </label>
                                                            <div class="mdl-selectfield">
                                                                <select id="ddlCustomizeQuestion" style="display: inline-block; padding-bottom: 4px;" onchange="setLikert7Value(13, this, null);">
                                                                    <option value="true" selected="selected">Yes</option>
                                                                    <option value="false">No</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form__row" style="display:none;">
                                                            <label style="font-weight: 700; float: left">
                                                                <span>Team Type</span>
                                                            </label>
                                                            <div class="mdl-selectfield">
                                                                <select id="ddlTeamType" style="display: inline-block; padding-bottom: 4px;" onchange="setLikert7Value(15, this, null);">
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form__row">
                                                            <label style="font-weight: 700; float: left">
                                                                <span>Question Limit</span>
                                                            </label>
                                                            <div class="mdl-selectfield">
                                                                <select id="ddlQuestionLimit" style="display: inline-block; padding-bottom: 4px;" onchange="setLikert7Value(16, this, null);">
                                                                    <option value="1" selected="selected">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>

                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- PULSE DECK INFO (CONTENT) END -->

                                        <!-- PULSE DECK PRIVACY (CONTENT) START -->
                                        <div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-privacy">
                                            <div id="tab-privacy">
                                                <div>
                                                    <label for="cbTargetEveryone">
                                                        <input type="checkbox" id="cbTargetEveryone" onchange="setLikert7Value(6, this, null);" checked="checked" />
                                                        <i class="fa fa-users" style="color: rgb(153, 153, 153);"></i>
                                                        <span>&nbsp;Everyone</span>
                                                    </label>
                                                </div>
                                                <hr />
                                                <div>
                                                    <label for="cbTargetDepartment">
                                                        <input type="checkbox" id="cbTargetDepartment" onchange="setLikert7Value(7, this, null);" />
                                                        <i class="fa fa-briefcase" style="color: rgb(153, 153, 153);"></i>
                                                        <span>&nbsp;Selected departments</span>
                                                    </label>
                                                    <div class="department tags">
                                                    </div>
                                                    <a id="lblAddDepartment" style="color: rgba(0, 118, 255, 1); margin-top: 10px;" href="javascript:showPopup(1);">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                        <span>Add more departments</span>
                                                    </a>
                                                </div>
                                                <div>
                                                    <label for="cbTargetUser">
                                                        <input type="checkbox" id="cbTargetUser" onchange="setLikert7Value(8, this, null);" />
                                                        <i class="fa fa-user" style="color: rgb(153, 153, 153);"></i>
                                                        <span>&nbsp;Selected personnel</span>
                                                    </label>
                                                    <div class="user tags" style="color: #000;"></div>
                                                    <input id="tbSearchUser" type="text" placeholder="User name" style="width: 300px; border-width: 0 0 1px;" onkeypress="if( event.keyCode == 13 ) { return false; };" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- PULSE DECK PRIVACY (CONTENT) END -->
                                </div>
                            </div>
                        </div>
                        <!-- CARD HEADER (ROW HEADER) END -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
