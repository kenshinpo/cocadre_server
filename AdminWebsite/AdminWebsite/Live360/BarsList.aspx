﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="BarsList.aspx.cs" Inherits="AdminWebsite.Live360.BarsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>

            <asp:HiddenField ID="hfId" runat="server" />
            <asp:HiddenField ID="hfTitle" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/Live360/BarsCreate" class="mfb-component__button--main" data-mfb-label="Add BARS">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Acitve, Delete -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClientClick="ShowProgressBar();" OnClick="lbAction_Click" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Acitve, Delete -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">Live360 <span>BARS</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlCount" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->
    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: bolder;">Manage</div>
            <ul class="data__sidebar__list" style="margin-top: 20px;">
                <li class="data__sidebar__list__item">
                    <a href="/Live360/Likert7List" class="data__sidebar__link">Likert7</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a href="/Live360/BarsList" class="data__sidebar__link data__sidebar__link--active">BARS</a>
                </li>
            </ul>


            <%--<div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Education</label>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Type the education's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" OnClick="ibFilterKeyWord_Click" runat="server" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                    <div class="pad">
                        <label>By Category</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterCategory" runat="server" AutoPostBack="true" onchange="ShowProgressBar('Filtering');" OnSelectedIndexChanged="ddlFilterCategory_SelectedIndexChanged" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>--%>
        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvList" runat="server" OnItemDataBound="lvList_ItemDataBound" OnItemCreated="lvList_ItemCreated" OnSorting="lvList_Sorting" OnItemCommand="lvList_ItemCommand" OnItemEditing="lvList_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 60px; padding: 15px 20px; display: none;">
                                            <!--Icon-->
                                        </th>
                                        <th style="width: 35%; padding: 15px 25px;">
                                            <!--Title-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByTitle" ID="SortByTitle" OnClientClick="ShowProgressBar('Filtering');" Style="min-width: 80px;" Text="Title" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Type-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByType" ID="SortByType" OnClientClick="ShowProgressBar('Filtering');" Style="min-width: 80px;" Text="Type" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Questions-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByQuestions" ID="SortByQuestions" OnClientClick="ShowProgressBar('Filtering');" Style="min-width: 80px;" Text="Questions" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Progress-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByProgress" ID="SortByProgress" OnClientClick="ShowProgressBar('Filtering');" Style="min-width: 80px;" Text="Progress" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Status-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByStatus" ID="SortByStatus" OnClientClick="ShowProgressBar('Filtering');" Style="min-width: 80px;" Text="Status" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Participants-->
                                            <asp:Label runat="server" Style="min-width: 75px;" Text="Participants" />
                                        </th>
                                        <th class="no-sort" style="width: 40px; padding: 15px 5px;">
                                            <!--Analytic Icon-->
                                        </th>
                                        <th class="no-sort" style="width: auto; padding: 15px 15px;">
                                            <!--Action Menu-->
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="padding: 15px 20px; text-align: center; display: none;">
                                    <!--Icon-->
                                    <asp:HiddenField runat="server" ID="hfId" Value='<%# DataBinder.Eval(Container.DataItem, "AppraisalId") %>' />
                                    <%--
                                    <div class="constrained" style="display: inline-block;">
                                        <asp:LinkButton ID="lbIcon" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "EducationId") + "/" + DataBinder.Eval(Container.DataItem, "Category.CategoryId") %>' Style="color: #999;">
                                            <asp:Image ID="imgMLearning" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "IconUrl") %>' runat="server" />
                                        </asp:LinkButton>
                                    </div>
                                    --%>
                                </td>
                                <td style="padding: 15px 25px">
                                    <!--Title-->
                                    <asp:LinkButton ID="lbTitle" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Type-->
                                    <asp:LinkButton ID="lbType" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Questions-->
                                    <asp:LinkButton ID="lbQuestions" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfCards") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Progress-->
                                    <asp:LinkButton ID="lbProgress" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Status-->
                                    <asp:LinkButton ID="lbStatus" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Participants-->
                                    <asp:LinkButton ID="lbParticipants" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px; text-align: center;">
                                    <!--Analytic Icon-->
                                    <asp:LinkButton ID="lbReport" runat="server" CommandName="Analytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' Visible="false">
                                        <asp:Image ID="imgReport" runat="server" ImageUrl="~/Img/icon_report.png" Width="35px" />
                                    </asp:LinkButton>
                                </td>
                                <td style="padding: 15px 15px;">
                                    <!--Action Menu-->
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")  %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbActivate" ClientIDMode="AutoID" runat="server" CommandName="Activate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId")%>' Text="Activate" Visible='<%# ((DataBinder.Eval(Container.DataItem, "Status").ToString() == "1") || (DataBinder.Eval(Container.DataItem, "Status").ToString() == "3")) %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbHide" ClientIDMode="AutoID" runat="server" CommandName="Hide" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId") %>' Text="Hide" Visible='<%# DataBinder.Eval(Container.DataItem, "Status").ToString() == "2" %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Del" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AppraisalId") %>' Text="Delete" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>


