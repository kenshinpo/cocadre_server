﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="Likert7Detail.aspx.cs" Inherits="AdminWebsite.Live360.Likert7Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }

        input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(145, 145, 145, 1);
        }

        input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(145, 145, 145, 1);
        }
    </style>
    <script src="/js/Live360/Liker7Detail.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none; background: #0b0b0b; opacity: 0.8;"></div>
    <div id="dvSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Select Department</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <input id="selectAllDepartment" type="checkbox" onclick="selectAllDepartments(this);" style="margin: 0px;" />
                        <label for="selectAllDepartment" style="margin: 0px;">Select All</label>
                    </div>
                    <hr />
                    <div class="accessrights departments">
                        <span id="department_checkboxlist"></span>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:setLikert7Value(9, this, null);">Select</a>
            <a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup(1)">Cancel</a>
        </div>
    </div>

    <div id="dvDeleteCategory" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Delete Category</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <label id="delCategoryMsg"></label>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="hlDelCategory" class="popup__action__item popup__action__item--cta" style="color: red;" href="javascript:deleteCategory();">Delete</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup(2)">Cancel</a>
        </div>
    </div>

    <div id="dvDeleteCard" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Delete Card</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <label id="delCardMsg"></label>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="hlDelCard" class="popup__action__item popup__action__item--cta" style="color: red;" href="javascript:deleteCard();">Delete</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup(3)">Cancel</a>
        </div>
    </div>

    <div id="dvAlert" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Edit Likert7</h1>
        <div class="topicicons" style="text-align: center; border-top-style: none;">
            <label style="background: rgba(0, 117, 254, 1); margin: -15px auto 0px; padding: 5px; border-radius: 24px; width: 200px; color: white;">Likert7 is Live and Active/Hidden</label><br />
            <label style="color: red;">
                Editing will affect the Analytics,
                        <br />
                therefore some features will be locked.</label>
        </div>
        <div class="popup__action">
            <a class="ppopup__action__item popup__action__item" href="javascript:hidePopup(4)">OK</a>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTimezone" />
    <asp:HiddenField runat="server" ID="hfLikert7Id" />
    <div class="data__content" style="background: rgba(244, 244, 244, 1);">

        <!-- App Bar -->
        <div class="appbar">
            <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
            <div class="appbar__title"><a href="/Live360/Likert7List" style="color: #000;">Live 360 <span>Likert 7</span></a></div>
            <div class="appbar__meta">
                <label>Edit 'Likert 7'</label>
            </div>
            <div class="appbar__action">
            </div>
        </div>
        <!-- / App Bar -->

        <div class="responsive-pulse-root">
            <div class="container">

                <div class="card card-header grid" style="border: none; box-shadow: none; margin: 1.5em 10px 0px 10px; padding: 0px; background: rgba(244, 244, 244, 1);">
                    <div class="grid__inner" style="background-color: transparent;">
                        <!-- Logo -->
                        <div class="grid__span--2">
                            <div style="text-align: center; width: 120px; height: 120px;">
                            </div>
                        </div>
                        <!-- Logo -->

                        <div class="grid__span grid__span--10 grid__span--last" style="background-color: transparent;">
                            <!-- CARD HEADER (ROW HEADER) START -->
                            <div class="grid__inner" style="background-color: transparent;">

                                <div style="float: right; width: 50%;">
                                    <div class="grid__span--11">
                                        <button id="lblSave" type="button" class="survey-bar__search__button" onclick="updateLikert7();">Apply Changes</button>
                                        <a class="survey-bar__search__button" href="/Live360/Likert7List">Cancel</a>
                                        <%--<a class="survey-bar__search__button analytics" style="margin: 0px 10px 0px 0px;">Analytics</a>--%>
                                    </div>
                                </div>

                                <div class="tabs tabs--styled">
                                    <ul class="tabs__list">
                                        <li class="tabs__list__item">
                                            <a class="tabs__link" href="#pulse-deck-info">Appraisal Info</a>
                                        </li>
                                        <li class="tabs__list__item">
                                            <a class="tabs__link" href="#pulse-deck-privacy">Privacy</a>
                                        </li>
                                    </ul>
                                    <div class="tabs__panels">
                                        <!-- PULSE DECK INFO (CONTENT) START -->
                                        <div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-info">
                                            <div class="grid__inner" style="background-color: transparent;">
                                                <div class="grid__span--8" style="width: calc(100% - 460px); text-align: right; margin: 0px;">
                                                    <div style="margin: 10px;">
                                                        <div class="data" style="margin: 0 0 1.5em;">
                                                            <div class="text_container" style="width: 90%; vertical-align: middle; display: inline-block; position: relative;">
                                                                <input id="tbTitle" style="border-image: none; width: 95%; margin: 0px; font-weight: bolder;" type="text" placeholder="Title of template" onkeyup="letterCounter(this, COUNT_MAX.TITLE, 'red', '#DDD');" onblur="setLikert7Value(1, this, null);" />
                                                                <span class="letterCount" style="right: 5px; bottom: 10px; color: #DDD; position: absolute;"></span>
                                                            </div>
                                                        </div>

                                                        <div class="data" style="margin: 0 0 1.5em;">
                                                            <div class="text_container" style="width: 90%; vertical-align: middle; display: inline-block; position: relative; text-align: left;">
                                                                <textarea id="tbContext" style="width: 95%; margin: 0px; font-weight: bolder;" placeholder="Context of the feedback" onkeyup="letterCounter(this, COUNT_MAX.CONTEXT, 'red', '#DDD');" onblur="setLikert7Value(2, this, null);"></textarea>
                                                                <span class="letterCount" style="right: 2px; bottom: 10px; color: #DDD; position: absolute;"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form__row">
                                                            <div class="grid">
                                                                <div class="grid__inner" style="background-color: transparent;">
                                                                    <div class="grid__span--8" style="width: 90%; margin-top: 10px;">
                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: 30%; color: rgba(162, 162, 162, 1);">
                                                                                <span>Status </span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 25%; font-weight: bolder;">
                                                                                <select id="ddlStatus" style="display: inline-block; padding-bottom: 2px;" onclick="setLikert7Value(14, this, null);">
                                                                                    <option value="1">Unlisted</option>
                                                                                    <option value="2">Active</option>
                                                                                    <option value="3">Hide</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: 30%; color: rgba(162, 162, 162, 1);">
                                                                                <i class="fa fa-star" style="color: #FFC700"></i>
                                                                                <span>Priority Overrule </span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 25%; font-weight: bolder;">
                                                                                <select id="ddlPriorityOverrule" style="display: inline-block; padding-bottom: 2px;" onchange="setLikert7Value(12, this, null);">
                                                                                    <option value="false">No</option>
                                                                                    <option value="true">Yes</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: 30%; color: rgba(162, 162, 162, 1);">
                                                                                <i class="fa fa-info-circle tooltip-icon" style="color: rgb(230, 230, 230);"></i>
                                                                                <span>Anonymity </span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 25%; font-weight: bolder;">
                                                                                <select id="ddlAnonymous" style="display: inline-block; padding-bottom: 2px;">
                                                                                    <option value="1" selected="selected">User define</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: 30%; color: rgba(162, 162, 162, 1);">
                                                                                <span>Customize Question</span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 25%; font-weight: bolder;">
                                                                                <select id="ddlCustomizeQuestion" style="display: inline-block; padding-bottom: 2px;" onchange="setLikert7Value(13, this, null);">
                                                                                    <option value="true">Yes</option>
                                                                                    <option value="false">No</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form__row" style="margin: 0px; display: none;">
                                                                            <label style="font-weight: 700; display: inline-block; width: 30%; color: rgba(162, 162, 162, 1);">
                                                                                <span>Team Type</span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 60%; font-weight: bolder;">
                                                                                <select id="ddlTeamType" style="display: inline-block; padding-bottom: 4px;" onchange="setLikert7Value(15, this, null);">
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: 30%; color: rgba(162, 162, 162, 1);">
                                                                                <span>Question Limit</span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 25%; font-weight: bolder;">
                                                                                <select id="ddlQuestionLimit" style="display: inline-block; padding-bottom: 4px;" onchange="setLikert7Value(16, this, null);">
                                                                                    <option value="1" selected="selected">1</option>
                                                                                    <option value="2">2</option>
                                                                                    <option value="3">3</option>
                                                                                    <option value="4">4</option>
                                                                                    <option value="5">5</option>
                                                                                    <option value="6">6</option>
                                                                                    <option value="7">7</option>
                                                                                    <option value="8">8</option>
                                                                                    <option value="9">9</option>
                                                                                    <option value="10">10</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid__span--4" style="width: 460px;">
                                                    <div style="margin: 10px;">
                                                        <div>
                                                            <label style="font-weight: 700; display: inline-block; width: 135px; color: rgba(162, 162, 162, 1); text-align: right; padding: 0 10px 0 0;">
                                                                <span>Publish method</span>
                                                            </label>
                                                            <div class="mdl-selectfield" style="display: inline-block; width: calc(100% - 150px); font-weight: bolder;">
                                                                <select id="ddlPublishMethod" style="display: inline-block; padding-bottom: 2px;" onchange="setLikert7Value(3, this, null);">
                                                                    <option value="1">Schedule</option>
                                                                    <option value="2">Perpetual</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="data">
                                                            <div style="font-weight: 700; display: inline-block; width: 135px; color: rgba(162, 162, 162, 1); text-align: right; padding: 0 10px 0 0;">
                                                                <span>Start date</span>
                                                            </div>
                                                            <div style="display: inline-block; width: calc(100% - 150px); font-weight: bolder; vertical-align: middle;">
                                                                <input type="text" id="startDate" style="border-width: 0px 0px 1px; width: 100px; display: inline;" placeholder="dd/mm/yyyy" class="input-field" onchange="setLikert7Value(4, this, null);" />
                                                                <span>at</span>
                                                                <input id="startDateHH" style="border-width: 0px 0px 1px; border-style: none none solid; border-color: currentColor currentColor rgb(221, 221, 221); border-image: none; width: 35px; display: inline;" onchange="setLikert7Value(4, this, null);" type="text" maxlength="2" placeholder="hh" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <span>:</span>
                                                                <input type="text" id="startDateMM" maxlength="2" style="border-width: 0px 0px 1px; width: 35px; display: inline;" placeholder="mm" onchange="setLikert7Value(4, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <div class="mdl-selectfield" style="display: inline-block;">
                                                                    <select id="startDateMR" style="width: 80px; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 0.65em; margin-bottom: 0.75em;" onchange="setLikert7Value(4, this, null);">
                                                                        <option value="am">am</option>
                                                                        <option value="pm">pm</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="dvEndDate" class="data" style="margin-top: 20px;">
                                                            <div style="font-weight: 700; display: inline-block; width: 135px; color: rgba(162, 162, 162, 1); text-align: right; padding: 0 10px 0 0;">
                                                                <span>End date</span>
                                                            </div>
                                                            <div style="display: inline-block; width: calc(100% - 150px); font-weight: bolder; vertical-align: middle;">
                                                                <input type="text" id="endDate" style="border-width: 0px 0px 1px; width: 100px; display: inline;" placeholder="dd/mm/yyyy" class="input-field" onchange="setLikert7Value(5, this, null);" />
                                                                <span>at</span>
                                                                <input type="text" id="endDateHH" maxlength="2" style="border-width: 0px 0px 1px; width: 35px; display: inline;" placeholder="hh" onchange="setLikert7Value(5, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <span>:</span>
                                                                <input type="text" id="endDateMM" maxlength="2" style="border-width: 0px 0px 1px; width: 35px; display: inline;" placeholder="mm" onchange="setLikert7Value(5, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <div class="mdl-selectfield" style="display: inline-block;">
                                                                    <select id="endDateMR" style="width: 80px; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 0.65em; margin-bottom: 0.75em;" onchange="setLikert7Value(5, this, null);">
                                                                        <option value="am">am</option>
                                                                        <option value="pm">pm</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <!-- PULSE DECK INFO (CONTENT) END -->

                                    <!-- PULSE DECK PRIVACY (CONTENT) START -->
                                    <div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-privacy">
                                        <div id="tab-privacy">
                                            <div>
                                                <label for="cbTargetEveryone">
                                                    <input type="checkbox" id="cbTargetEveryone" onchange="setLikert7Value(6, this, null);" checked="checked" />
                                                    <i class="fa fa-users" style="color: rgb(153, 153, 153);"></i>
                                                    <span>&nbsp;Everyone</span>
                                                </label>
                                            </div>
                                            <hr />
                                            <div>
                                                <label for="cbTargetDepartment">
                                                    <input type="checkbox" id="cbTargetDepartment" onchange="setLikert7Value(7, this, null);" />
                                                    <i class="fa fa-briefcase" style="color: rgb(153, 153, 153);"></i>
                                                    <span>&nbsp;Selected departments</span>
                                                </label>
                                                <div class="department tags"></div>
                                                <a id="lblAddDepartment" style="color: rgba(0, 118, 255, 1); margin-top: 10px;" href="javascript:showPopup(1);">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    <span>Add more departments</span>
                                                </a>
                                            </div>
                                            <div>
                                                <label for="cbTargetUser">
                                                    <input type="checkbox" id="cbTargetUser" onchange="setLikert7Value(8, this, null);" />
                                                    <i class="fa fa-user" style="color: rgb(153, 153, 153);"></i>
                                                    <span>&nbsp;Selected personnel</span>
                                                </label>
                                                <div class="user tags" style="color: #000;"></div>
                                                <input id="tbSearchUser" type="text" placeholder="User name" style="width: 300px; border-width: 0 0 1px; background-color: rgba(244, 244, 244, 1);" onkeypress="if( event.keyCode == 13 ) { return false; };" />
                                            </div>

                                        </div>
                                    </div>
                                    <!-- PULSE DECK PRIVACY (CONTENT) END -->
                                </div>
                            </div>
                        </div>
                        <!-- CARD HEADER (ROW HEADER) END -->
                    </div>
                </div>

                <!-- SEARCH PULSE START -->
                <div>
                    <div id="sticky_anchor"></div>
                    <div class="survey-bar sticky">
                        <div class="container" style="padding: 10px 30px;">
                            <div class="survey-bar__search">
                                <div class="grid__inner" style="background-color: transparent;">
                                    <div style="width: 50%; display: inline-block; float: left; position: relative;">
                                        <i class="fa fa-search" style="position: absolute; left: 2px; top: 14px; color: #aeaeae; visibility: hidden;"></i>
                                        <input class="survey-bar__search__input input-field" type="text" placeholder="Search question" style="background-color: rgba(244, 244, 244, 1); margin-left: 5px; border: solid #ddd; border-width: 0 0 1px; visibility: hidden;" />
                                    </div>
                                    <div style="width: 50%; display: inline-block">
                                        <button type="button" class="add-category survey-bar__search__button" style="margin: 0px; visibility: visible" onclick="addCategory();">+ Add Category</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SEARCH PULSE END -->

                <div class="dvNewCategory" style="padding: 10px 30px;"></div>

                <div class="dvCategoryList" style="padding: 10px 30px;">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
