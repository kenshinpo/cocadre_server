﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="BarsDetail.aspx.cs" Inherits="AdminWebsite.Live360.BarsDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }

        input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(145, 145, 145, 1);
        }

        input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(145, 145, 145, 1);
        }
    </style>
    <script src="/js/Live360/AppraisalDetail.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none; background: #0b0b0b; opacity: 0.8;"></div>
    <div id="dvSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Select Department</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <input id="selectAllDepartment" type="checkbox" onclick="selectAllDepartments(this);" style="margin: 0px;" />
                        <label for="selectAllDepartment" style="margin: 0px;">Select All</label>
                    </div>
                    <hr />
                    <div class="accessrights departments">
                        <span id="department_checkboxlist"></span>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:setAppraisalValue(9, this, null);">Select</a>
            <a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup(1)">Cancel</a>
        </div>
    </div>

    <div id="dvDeleteCategory" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Delete Category</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <label id="delCategoryMsg"></label>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="hlDelCategory" class="popup__action__item popup__action__item--cta" style="color: red;" href="javascript:deleteCategory();">Delete</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup(2)">Cancel</a>
        </div>
    </div>

    <div id="dvDeleteCard" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Delete Card</h1>
        <div class="popup__content">
            <fieldset class="form">
                <div class="container">
                    <label id="delCardMsg"></label>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a id="hlDelCard" class="popup__action__item popup__action__item--cta" style="color: red;" href="javascript:deleteCard();">Delete</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup(3)">Cancel</a>
        </div>
    </div>

    <div id="dvAlert" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Edit <span class="title_bar_action"></span></h1>
        <div class="topicicons" style="text-align: center; border-top-style: none;">
            <label style="background: rgba(0, 117, 254, 1); margin: -15px auto 0px; padding: 5px; border-radius: 24px; width: 200px; color: white;">This appraisal is Live and Active/Hidden</label><br />
            <label style="color: red;">
                Editing will affect the Analytics,
                        <br />
                therefore some features will be locked.</label>
        </div>
        <div class="popup__action">
            <a class="ppopup__action__item popup__action__item" href="javascript:hidePopup(4)">OK</a>
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTimezone" />
    <asp:HiddenField runat="server" ID="hfAppraisalId" />
    <asp:HiddenField runat="server" ID="hfAppraisalType" />

    <div class="data__content" style="background: rgba(244, 244, 244, 1);">
        <!-- App Bar -->
        <div class="appbar">
            <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
            <div class="appbar__title"><a class="appraisal_type_action" href="" style="color: #000;">Live 360 <span></span></a></div>
            <div class="appbar__meta">
                <label class="title_bar_action"></label>
            </div>
            <div class="appbar__action">
            </div>
        </div>
        <!-- / App Bar -->

        <div class="responsive-pulse-root">
            <div class="container">

                <div class="card card-header grid" style="border: none; box-shadow: none; margin: 1.5em 10px 0px 10px; padding: 0px; background: rgba(244, 244, 244, 1);">
                    <div class="grid__inner" style="background-color: transparent;">
                        <!-- Logo -->
                        <div class="grid__span--2">
                            <div style="text-align: center; width: 120px; height: 120px;">
                            </div>
                        </div>
                        <!-- Logo -->

                        <div class="grid__span grid__span--10 grid__span--last" style="background-color: transparent;">
                            <!-- CARD HEADER (ROW HEADER) START -->
                            <div class="grid__inner" style="background-color: transparent;">

                                <div style="float: right; width: 50%;">
                                    <div class="grid__span--11">
                                        <button id="lblSave" type="button" class="survey-bar__search__button" onclick="updateAppraisal();">Apply Changes</button>
                                        <a class="survey-bar__search__button">Cancel</a>
                                        <%--<a class="survey-bar__search__button analytics" style="margin: 0px 10px 0px 0px;">Analytics</a>--%>
                                    </div>
                                </div>

                                <div class="tabs tabs--styled">
                                    <ul class="tabs__list">
                                        <li class="tabs__list__item">
                                            <a class="tabs__link" href="#pulse-deck-info">Appraisal Info</a>
                                        </li>
                                        <li class="tabs__list__item">
                                            <a class="tabs__link" href="#pulse-deck-privacy">Privacy</a>
                                        </li>
                                    </ul>
                                    <div class="tabs__panels">
                                        <!-- PULSE DECK INFO (CONTENT) START -->
                                        <div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-info">
                                            <div class="grid__inner" style="background-color: transparent;">
                                                <div class="grid__span--8" style="width: calc(100% - 460px); text-align: right; margin: 0px;">
                                                    <div style="margin: 10px;">
                                                        <div class="data" style="margin: 0 0 1.5em;">
                                                            <div class="text_container" style="width: 90%; vertical-align: middle; display: inline-block; position: relative;">
                                                                <input id="tbTitle" style="border-image: none; width: 95%; margin: 0px; font-weight: bolder;" type="text" placeholder="Title of template" onkeyup="letterCounter(this, COUNT_MAX.TITLE, 'red', '#DDD');" onblur="setAppraisalValue(1, this, null);" />
                                                                <span class="letterCount" style="right: 5px; bottom: 10px; color: #DDD; position: absolute;"></span>
                                                            </div>
                                                        </div>

                                                        <div class="data" style="margin: 0 0 1.5em;">
                                                            <div class="text_container" style="width: 90%; vertical-align: middle; display: inline-block; position: relative; text-align: left;">
                                                                <textarea id="tbContext" style="width: calc(100% - 25px); margin: 0px; font-weight: bolder;" placeholder="Context of the feedback" onkeyup="letterCounter(this, COUNT_MAX.CONTEXT, 'red', '#DDD');" onblur="setAppraisalValue(2, this, null);"></textarea>
                                                                <span class="letterCount" style="right: 2px; bottom: 10px; color: #DDD; position: absolute;"></span>
                                                            </div>
                                                        </div>

                                                        <div class="form__row">
                                                            <div class="grid">
                                                                <div class="grid__inner" style="background-color: transparent;">
                                                                    <div class="grid__span--8" style="width: 90%; margin-top: 10px;">
                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: auto; color: rgba(162, 162, 162, 1);">
                                                                                <span>Status </span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 150px; font-weight: bolder;">
                                                                                <select id="ddlStatus" style="display: inline-block; padding-bottom: 2px;" onclick="setAppraisalValue(14, this, null);">
                                                                                    <option value="1">Unlisted</option>
                                                                                    <option value="2">Active</option>
                                                                                    <option value="3">Hide</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: auto; color: rgba(162, 162, 162, 1);">
                                                                                <i class="fa fa-star" style="color: #FFC700"></i>
                                                                                <span>Priority Overrule </span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 150px; font-weight: bolder;">
                                                                                <select id="ddlPriorityOverrule" style="display: inline-block; padding-bottom: 2px;" onchange="setAppraisalValue(12, this, null);">
                                                                                    <option value="false">No</option>
                                                                                    <option value="true">Yes</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: auto; color: rgba(162, 162, 162, 1);">
                                                                                <i class="fa fa-info-circle tooltip-icon" style="color: rgb(230, 230, 230);"></i>
                                                                                <span>Anonymity </span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 150px; font-weight: bolder;">
                                                                                <select id="ddlAnonymous" style="display: inline-block; padding-bottom: 2px;">
                                                                                    <option value="1" selected="selected">User define</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form__row" style="margin: 0px;">
                                                                            <label style="font-weight: 700; display: inline-block; width: auto; color: rgba(162, 162, 162, 1);">
                                                                                <span>Customize Question</span>
                                                                            </label>
                                                                            <div class="mdl-selectfield" style="display: inline-block; width: 150px; font-weight: bolder;">
                                                                                <select id="ddlCustomizeQuestion" style="display: inline-block; padding-bottom: 2px;" onchange="setAppraisalValue(13, this, null);">
                                                                                    <option value="true">Yes</option>
                                                                                    <option value="false">No</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid__span--4" style="width: 460px;">
                                                    <div style="margin: 10px;">
                                                        <div>
                                                            <label style="font-weight: 700; display: inline-block; width: 135px; color: rgba(162, 162, 162, 1); text-align: right; padding: 0 10px 0 0;">
                                                                <span>Publish method</span>
                                                            </label>
                                                            <div class="mdl-selectfield" style="display: inline-block; width: calc(100% - 150px); font-weight: bolder;">
                                                                <select id="ddlPublishMethod" style="display: inline-block; padding-bottom: 2px;" onchange="setAppraisalValue(3, this, null);">
                                                                    <option value="1">Schedule</option>
                                                                    <option value="2">Perpetual</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="data">
                                                            <div style="font-weight: 700; display: inline-block; width: 135px; color: rgba(162, 162, 162, 1); text-align: right; padding: 0 10px 0 0;">
                                                                <span>Start date</span>
                                                            </div>
                                                            <div style="display: inline-block; width: calc(100% - 150px); font-weight: bolder; vertical-align: middle;">
                                                                <input type="text" id="startDate" style="border-width: 0px 0px 1px; width: 100px; display: inline;" placeholder="dd/mm/yyyy" class="input-field" onchange="setAppraisalValue(4, this, null);" />
                                                                <span>at</span>
                                                                <input id="startDateHH" style="border-width: 0px 0px 1px; border-style: none none solid; border-color: currentColor currentColor rgb(221, 221, 221); border-image: none; width: 35px; display: inline;" onchange="setLikert7Value(4, this, null);" type="text" maxlength="2" placeholder="hh" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <span>:</span>
                                                                <input type="text" id="startDateMM" maxlength="2" style="border-width: 0px 0px 1px; width: 35px; display: inline;" placeholder="mm" onchange="setAppraisalValue(4, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <div class="mdl-selectfield" style="display: inline-block;">
                                                                    <select id="startDateMR" style="width: 80px; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 0.65em; margin-bottom: 0.75em;" onchange="setAppraisalValue(4, this, null);">
                                                                        <option value="am">am</option>
                                                                        <option value="pm">pm</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div id="dvEndDate" class="data" style="margin-top: 20px;">
                                                            <div style="font-weight: 700; display: inline-block; width: 135px; color: rgba(162, 162, 162, 1); text-align: right; padding: 0 10px 0 0;">
                                                                <span>End date</span>
                                                            </div>
                                                            <div style="display: inline-block; width: calc(100% - 150px); font-weight: bolder; vertical-align: middle;">
                                                                <input type="text" id="endDate" style="border-width: 0px 0px 1px; width: 100px; display: inline;" placeholder="dd/mm/yyyy" class="input-field" onchange="setAppraisalValue(5, this, null);" />
                                                                <span>at</span>
                                                                <input type="text" id="endDateHH" maxlength="2" style="border-width: 0px 0px 1px; width: 35px; display: inline;" placeholder="hh" onchange="setAppraisalValue(5, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <span>:</span>
                                                                <input type="text" id="endDateMM" maxlength="2" style="border-width: 0px 0px 1px; width: 35px; display: inline;" placeholder="mm" onchange="setAppraisalValue(5, this, null);" class="input-field" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                                                                <div class="mdl-selectfield" style="display: inline-block;">
                                                                    <select id="endDateMR" style="width: 80px; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 0.65em; margin-bottom: 0.75em;" onchange="setAppraisalValue(5, this, null);">
                                                                        <option value="am">am</option>
                                                                        <option value="pm">pm</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <!-- PULSE DECK INFO (CONTENT) END -->

                                    <!-- PULSE DECK PRIVACY (CONTENT) START -->
                                    <div class="tabs__panels__item add-topic__info__details--basicinfo" id="pulse-deck-privacy">
                                        <div id="tab-privacy">
                                            <p>
                                                <label for="cbTargetEveryone">
                                                    <input type="checkbox" id="cbTargetEveryone" onchange="setAppraisalValue(6, this, null);" checked="checked" />
                                                    <i class="fa fa-users" style="color: rgb(153, 153, 153);"></i>
                                                    <span>&nbsp;Everyone</span>
                                                </label>
                                            </p>
                                            <hr />
                                            <p>
                                                <label for="cbTargetDepartment">
                                                    <input type="checkbox" id="cbTargetDepartment" onchange="setAppraisalValue(7, this, null);" />
                                                    <i class="fa fa-briefcase" style="color: rgb(153, 153, 153);"></i>
                                                    <span>&nbsp;Selected departments</span>
                                                </label>
                                                <div class="department tags">
                                                </div>
                                                <a id="lblAddDepartment" style="color: rgba(0, 118, 255, 1); margin-top: 10px;" href="javascript:showPopup(1);">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                    <span>Add more departments</span>
                                                </a>
                                            </p>
                                            <p>
                                                <label for="cbTargetUser">
                                                    <input type="checkbox" id="cbTargetUser" onchange="setAppraisalValue(8, this, null);" />
                                                    <i class="fa fa-user" style="color: rgb(153, 153, 153);"></i>
                                                    <span>&nbsp;Selected personnel</span>
                                                </label>
                                                <div class="user tags" style="color: #000;"></div>
                                                <input id="tbSearchUser" type="text" placeholder="User name" style="width: 300px; border-width: 0 0 1px; background-color: rgba(244, 244, 244, 1);" onkeypress="if( event.keyCode == 13 ) { return false; };" />
                                            </p>

                                        </div>
                                    </div>
                                    <!-- PULSE DECK PRIVACY (CONTENT) END -->
                                </div>
                            </div>
                        </div>
                        <!-- CARD HEADER (ROW HEADER) END -->
                    </div>
                </div>

                <!-- SEARCH PULSE START -->
                <div>
                    <div id="sticky_anchor"></div>
                    <div class="survey-bar sticky">
                        <div class="container" style="padding: 10px 30px;">
                            <div class="survey-bar__search">
                                <div class="grid__inner" style="background-color: transparent;">
                                    <div style="width: 50%; display: inline-block; float: left; position: relative;">
                                        <i class="fa fa-search" style="position: absolute; left: 2px; top: 14px; color: #aeaeae; visibility: hidden;"></i>
                                        <input class="survey-bar__search__input input-field" type="text" placeholder="Search question" style="background-color: rgba(244, 244, 244, 1); margin-left: 5px; border: solid #ddd; border-width: 0 0 1px; visibility: hidden;" />
                                    </div>
                                    <div style="width: 50%; display: inline-block">
                                        <button type="button" class="add-category survey-bar__search__button" style="margin: 0px; visibility: visible" onclick="addCategory();">+ Add Category</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- SEARCH PULSE END -->

                <div class="dvNewCategory" style="padding: 10px 30px;"></div>

                <div class="dvCategoryList" style="padding: 10px 30px;">
                    <div class="category_layout APPC98748ef37f084c38bfc03368db11890a" style="margin-top: 20px;" data-ordering="1" data-categoryid="APPC98748ef37f084c38bfc03368db11890a">
                        <div class="category_detail">
                            <div class="grid-question-number" style="margin: 0px; width: calc(100% - 150px); float: left; display: inline;">
                                <label style="font-size: 1.2em; font-weight: bolder;">001-Category</label>
                            </div>
                            <div class="grid-question-icon" style="width: 150px; float: left;"><span style="padding: 10px; text-align: right; color: rgba(57, 137, 242, 1); font-weight: bold; cursor: pointer;" onclick="switchCategoryMode('APPC98748ef37f084c38bfc03368db11890a', true); ">Edit</span>                <span style="padding: 10px; text-align: right; color: rgba(57, 137, 242, 1); font-weight: bold; cursor: pointer;" onclick="showPopup(2, 'APPC98748ef37f084c38bfc03368db11890a'); ">Delete</span>            </div>
                        </div>
                        <hr style="width: 100%;">
                        <div style="width: 100%; display: inline-block;">
                            <button class="add-card survey-bar__search__button" style="margin: 0px; visibility: visible;" onclick="addCard('APPC98748ef37f084c38bfc03368db11890a');" type="button">+ Add Question</button>
                        </div>
                        <div class="dvNewCard" style="width: 100%; display: inline-block;">
                            <!--************-->
                            <div class="card add-question " id="add-question-card">
                                <div class="add-question__questionnumber">
                                    <div class="number">
                                        P1
                                    </div>
                                    <hr>
                                    <div class="number">
                                        Q6
                                    </div>
                                </div>
                                <div class="add-question__info">
                                    <div class="add-question__info__header">
                                        <div class="add-question__info__header__action">
                                            <a class="btn secondary" id="main_content_lbNewQuestionCancel" href="javascript:__doPostBack('ctl00$main_content$lbNewQuestionCancel','')">Cancel</a>
                                            <a class="btn" id="main_content_lbNewQuestionSave" onclick="ShowProgressBar();  return checkNewCardInputData();" href="javascript:__doPostBack('ctl00$main_content$lbNewQuestionSave','')">Create</a>
                                        </div>
                                        <div class="add-question__info__header__title">Question Type</div>
                                    </div>
                                    <div class="add-question__info__setup">
                                        <div class="add-question__info__setup__questiontype">
                                            <div class="tabs--questionformat">
                                                <ul class="tabs__list">
                                                    <div class="tabs__list__item" id="divNewCardType1" style="background: rgb(0, 117, 255); margin: 2px; color: rgb(242, 244, 247); cursor: pointer;" onclick="setNewCardType(1);">
                                                        Select one
                                                    </div>
                                                    <div class="tabs__list__item" id="divNewCardType2" style="margin: 2px; color: rgb(204, 204, 204); cursor: pointer;" onclick="setNewCardType(2);">
                                                        Multi choice
                                                    </div>
                                                    <div class="tabs__list__item" id="divNewCardType3" style="margin: 2px; color: rgb(204, 204, 204); cursor: pointer;" onclick="setNewCardType(3);">
                                                        Text
                                                    </div>
                                                    <div class="tabs__list__item" id="divNewCardType4" style="margin: 2px; color: rgb(204, 204, 204); cursor: pointer;" onclick="setNewCardType(4);">
                                                        Number range
                                                    </div>
                                                    <div class="tabs__list__item" id="divNewCardType5" style="margin: 2px; color: rgb(204, 204, 204); cursor: pointer;" onclick="setNewCardType(5);">
                                                        Drop list
                                                    </div>
                                                    <div class="tabs__list__item" id="divNewCardType6" style="margin: 2px; color: rgb(204, 204, 204); cursor: pointer;" onclick="setNewCardType(6);">
                                                        Instructions
                                                    </div>
                                                </ul>
                                                <div class="tabs__panels">
                                                    <div class="tabs__panels__item tabs__panels__item">
                                                        <div class="form__row form__row--question">
                                                            <div>
                                                                <textarea id="tbNewCardContent" style="padding-left: 40px;" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblContentCount', 250); setNewCardValue(1, this, null);" onblur="setNewCardValue(1, this, null);" placeholder="Question" rows="2" cols="20">Question</textarea>
                                                                <span class="survey-lettercount" id="lblContentCount">250</span>
                                                                <label class="upload-card-image" for="fuNewCardContentImg">
                                                                    <input title="Add image" id="fuNewCardContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,1, 1);" type="file" accept="image/*">
                                                                </label>
                                                                <div class="question-image-content" id="divNewCardContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(1, 1);">
                                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                                    </div>
                                                                    <img id="imgNewCardContentImgPreview" src="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form__row form__row--question" id="divNewCardType1Layout" style="overflow: auto; display: block; visibility: visible;">
                                                            <div class="options-container" id="divNewCardOptions">
                                                                <div id="divNewCardOption1" style="margin-bottom: 10px;"><a class="remove-option-link" onclick="removeNewCardOptionField(0);" href="javascript:void(0);">- Remove option</a><textarea name="tbNewCardOption1" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption1ContentCount', 150); setNewCardValue(2, this, 1);" onblur="setNewCardValue(2, this, 1);" placeholder="Option 1" rows="2" cols="20">Option 1</textarea><span class="survey-lettercount" id="lblNewCardOption1ContentCount">150</span><label class="upload-option-image" for="fuNewCardOption1ContentImg"><input title="Add image" id="fuNewCardOption1ContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,2, 1);" type="file" accept="image/*"></label><div class="option-image-content" id="divNewCardOption1ContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(2, 1);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                    <img id="imgNewOption1ContentImgPreview"></div>
                                                                </div>
                                                                <div id="divNewCardOption2" style="margin-bottom: 10px;"><a class="remove-option-link" onclick="removeNewCardOptionField(1);" href="javascript:void(0);">- Remove option</a><textarea name="tbNewCardOption2" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption2ContentCount', 150); setNewCardValue(2, this, 2);" onblur="setNewCardValue(2, this, 2);" placeholder="Option 2" rows="2" cols="20">Option 2</textarea><span class="survey-lettercount" id="lblNewCardOption2ContentCount">150</span><label class="upload-option-image" for="fuNewCardOption2ContentImg"><input title="Add image" id="fuNewCardOption2ContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,2, 2);" type="file" accept="image/*"></label><div class="option-image-content" id="divNewCardOption2ContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(2, 2);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                    <img id="imgNewOption2ContentImgPreview"></div>
                                                                </div>
                                                                <div id="divNewCardOption3" style="margin-bottom: 10px;"><a class="remove-option-link" onclick="removeNewCardOptionField(2);" href="javascript:void(0);">- Remove option</a><textarea name="tbNewCardOption3" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption3ContentCount', 150); setNewCardValue(2, this, 3);" onblur="setNewCardValue(2, this, 3);" placeholder="Option 3" rows="2" cols="20">Option 3</textarea><span class="survey-lettercount" id="lblNewCardOption3ContentCount">150</span><label class="upload-option-image" for="fuNewCardOption3ContentImg"><input title="Add image" id="fuNewCardOption3ContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,2, 3);" type="file" accept="image/*"></label><div class="option-image-content" id="divNewCardOption3ContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(2, 3);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                    <img id="imgNewOption3ContentImgPreview"></div>
                                                                </div>
                                                                <div id="divNewCardOption4" style="margin-bottom: 10px;"><a class="remove-option-link" onclick="removeNewCardOptionField(3);" href="javascript:void(0);">- Remove option</a><textarea name="tbNewCardOption4" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption4ContentCount', 150); setNewCardValue(2, this, 4);" onblur="setNewCardValue(2, this, 4);" placeholder="Option 4" rows="2" cols="20">Option 4</textarea><span class="survey-lettercount" id="lblNewCardOption4ContentCount">150</span><label class="upload-option-image" for="fuNewCardOption4ContentImg"><input title="Add image" id="fuNewCardOption4ContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,2, 4);" type="file" accept="image/*"></label><div class="option-image-content" id="divNewCardOption4ContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(2, 4);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                    <img id="imgNewOption4ContentImgPreview"></div>
                                                                </div>
                                                                <div id="divNewCardOption5" style="margin-bottom: 10px;"><a class="remove-option-link" onclick="removeNewCardOptionField(4);" href="javascript:void(0);">- Remove option</a><textarea name="tbNewCardOption5" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption5ContentCount', 150); setNewCardValue(2, this, 5);" onblur="setNewCardValue(2, this, 5);" placeholder="Option 5" rows="2" cols="20">Option 5</textarea><span class="survey-lettercount" id="lblNewCardOption5ContentCount">150</span><label class="upload-option-image" for="fuNewCardOption5ContentImg"><input title="Add image" id="fuNewCardOption5ContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,2, 5);" type="file" accept="image/*"></label><div class="option-image-content" id="divNewCardOption5ContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(2, 5);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                    <img id="imgNewOption5ContentImgPreview"></div>
                                                                </div>
                                                                <div id="divNewCardOption6" style="margin-bottom: 10px;"><a class="remove-option-link" onclick="removeNewCardOptionField(5);" href="javascript:void(0);">- Remove option</a><textarea name="tbNewCardOption6" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption6ContentCount', 150); setNewCardValue(2, this, 6);" onblur="setNewCardValue(2, this, 6);" placeholder="Option 6" rows="2" cols="20">Option 6</textarea><span class="survey-lettercount" id="lblNewCardOption6ContentCount">150</span><label class="upload-option-image" for="fuNewCardOption6ContentImg"><input title="Add image" id="fuNewCardOption6ContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,2, 6);" type="file" accept="image/*"></label><div class="option-image-content" id="divNewCardOption6ContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(2, 6);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                    <img id="imgNewOption6ContentImgPreview"></div>
                                                                </div>
                                                                <div id="divNewCardOption7" style="margin-bottom: 10px;"><a class="remove-option-link" onclick="removeNewCardOptionField(6);" href="javascript:void(0);">- Remove option</a><textarea name="tbNewCardOption7" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption7ContentCount', 150); setNewCardValue(2, this, 7);" onblur="setNewCardValue(2, this, 7);" placeholder="Option 7" rows="2" cols="20">Option 7</textarea><span class="survey-lettercount" id="lblNewCardOption7ContentCount">150</span><label class="upload-option-image" for="fuNewCardOption7ContentImg"><input title="Add image" id="fuNewCardOption7ContentImg" style="display: none;" onchange="previewImage(this, 2560, 2560 ,2, 7);" type="file" accept="image/*"></label><div class="option-image-content" id="divNewCardOption7ContentImg" style="height: 0px; overflow: auto; visibility: hidden;">
                                                                    <div class="survey-cross" onclick="removeContentImg(2, 7);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                    <img id="imgNewOption7ContentImgPreview"></div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <a class="options-container add-option-link" style="float: right;" onclick="addOptionField()" href="javascript:void(0);">+ Add option</a>
                                                        </div>
                                                        <div class="form__row form__row--question" id="divNewCardType2Layout" style="overflow: auto; display: none; visibility: hidden;">Type 2 Layout</div>
                                                        <div class="form__row form__row--question" id="divNewCardType3Layout" style="overflow: auto; display: none; visibility: hidden;"></div>
                                                        <div class="form__row form__row--question" id="divNewCardType4Layout" style="overflow: auto; display: none; visibility: hidden;">
                                                            <label style="margin-bottom: 15px;">
                                                                Number range
                                                                <br>
                                                                (Minimum: -20 to Maximum: 20)
                                                            </label>
                                                            <div class="option_section">
                                                                Slider starts from<br>
                                                                <label class="inline-label radio">
                                                                    <input name="numberRangeStartFrom" onchange="resetRangeType(1); setNewCardValue(15, this, null);" type="radio" checked="checked" value="1">Minimum/Maximum</label>
                                                                <label class="inline-label radio">
                                                                    <input name="numberRangeStartFrom" onchange="resetRangeType(2); setNewCardValue(15, this, null);" type="radio" value="2">Middle</label>
                                                            </div>
                                                            <div id="divNewCardRangeNonmiddle" style="display: block;">
                                                                <select id="sltRangeNonmiddle" onchange="setNewCardValue(15, this, null);">
                                                                    <option value="1">Start from Minimum</option>
                                                                    <option value="3">Start from Maximum</option>
                                                                </select>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Minimum value</label>
                                                                    <input name="tbNewCardOptionRangeMin" class="tbNewCardOptionRangeMin nonmiddle" onblur="setNewCardValue(11, this, null);" type="number"><input name="tbNewCardOptionRangeMinLabel" class="tbNewCardOptionRangeMinLabel" onkeyup="textCounter(this,'lblNewCardRangeNonmiddleMinCount', 25); setNewCardValue(19, this, null);" type="text" placeholder="Label(Optional)"><span class="survey-lettercount" id="lblNewCardRangeNonmiddleMinCount">25</span><label class="tip">Eg. Poor, Bad, Need improvement, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Maximum value</label>
                                                                    <input name="tbNewCardOptionRangeMax" class="tbNewCardOptionRangeMax nonmiddle" onblur="setNewCardValue(12, this, null);" type="number"><input name="tbNewCardOptionRangeMaxLabel" class="tbNewCardOptionRangeMaxLabel" onkeyup="textCounter(this,'lblNewCardRangeNonmiddleMaxCount', 25); setNewCardValue(18, this, null);" type="text" placeholder="Label(Optional)"><span class="survey-lettercount" id="lblNewCardRangeNonmiddleMaxCount">25</span><label class="tip">Eg. Excellent, Great, Very good, Amazing, etc.</label>
                                                                </div>
                                                            </div>
                                                            <div id="divNewCardRangeMiddle" style="display: none;">
                                                                <div class="option_section">
                                                                    <label class="inline-label">Middle value</label>
                                                                    <input name="tbNewCardOptionRangeMiddle" class="tbNewCardOptionRangeMiddle middle" onblur="setNewCardValue(16, this, null);" type="number"><input name="tbNewCardOptionRangeMiddleLabel" class="tbNewCardOptionRangeMiddleLabel" onkeyup="textCounter(this,'lblNewCardRangeMiddleMiddleCount', 25); setNewCardValue(17, this, null);" type="text" placeholder="Label"><span class="survey-lettercount" id="lblNewCardRangeMiddleMiddleCount">25</span><label class="tip">Eg. Average, Neutral, So-so, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Maximum value</label>
                                                                    <input name="tbNewCardOptionRangeMax" class="tbNewCardOptionRangeMax middle" onblur="setNewCardValue(20, this, null);" type="number"><input name="tbNewCardOptionRangeMaxLabel" class="tbNewCardOptionRangeMaxLabel" onkeyup="textCounter(this,'lblNewCardRangeMiddleMaxCount', 25); setNewCardValue(18, this, null);" type="text" placeholder="Label"><span class="survey-lettercount" id="lblNewCardRangeMiddleMaxCount">25</span><label class="tip">Eg. Excellent, Great, Very good, Amazing, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Minimum value</label>
                                                                    <label id="lblNewCardOptionRangeMiddleMin">0</label>
                                                                    <input name="tbNewCardOptionRangeMinLabel" class="tbNewCardOptionRangeMinLabel" onkeyup="textCounter(this,'lblNewCardRangeMiddleMinCount', 25); setNewCardValue(19, this, null);" type="text" placeholder="Label"><span class="survey-lettercount" id="lblNewCardRangeMiddleMinCount">25</span><label class="tip">Eg. Poor, Bad, Need improvement, etc.</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form__row form__row--question" id="divNewCardType5Layout" style="overflow: auto; display: none; visibility: hidden;">
                                                            <textarea class="droplist_options" id="txareaNewCard" style="-ms-overflow-y: auto;" onkeyup="updateSelectPreview(this);setNewCardValue(14, this, null);" placeholder="Please type in your options here, subsequent options separated by 'Enter' key." rows="16">Please type in your options here, subsequent options separated by 'Enter' key.</textarea>
                                                            <label>Please type in your options here, subsequent options separated by 'Enter' key.</label>
                                                        </div>
                                                        <div class="form__row form__row--question" id="divNewCardType6Layout" style="overflow: auto; display: none; visibility: hidden;"></div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add-question__info__setup__preview">
                                            <!-- Card Preview Format -->
                                            <div class="survey-preview" id="divNewCardPreview" style='position: relative; background-image: url("/Img/bg_survey_1a_big.png");'>
                                                <div>
                                                    <!-- For Note -->
                                                    <img class="right" id="imgNewCardPreviewNote" src="/Img/icon_note_small.png">
                                                </div>
                                                <div class="preview-question">
                                                    <!-- For CardContentText, CardContentImgs  -->
                                                    <span class="preview-question-content" id="lblNewCardPreviewContentText" style="color: black;"></span>
                                                    <img class="preview-question-image" id="imgNewCardPreviewContentImg">
                                                </div>
                                                <div id="divNewCardPreviewTypes">
                                                    <div id="divNewCardType1PreviewLayout">
                                                        <div class="preview-option">
                                                            <div class="preview-option-content"></div>
                                                            <div class="clear-float"></div>
                                                        </div>
                                                        <div class="preview-option">
                                                            <div class="preview-option-content"></div>
                                                            <div class="clear-float"></div>
                                                        </div>
                                                        <div class="preview-option">
                                                            <div class="preview-option-content"></div>
                                                            <div class="clear-float"></div>
                                                        </div>
                                                        <div class="preview-option">
                                                            <div class="preview-option-content"></div>
                                                            <div class="clear-float"></div>
                                                        </div>
                                                        <div class="preview-option">
                                                            <div class="preview-option-content"></div>
                                                            <div class="clear-float"></div>
                                                        </div>
                                                        <div class="preview-option">
                                                            <div class="preview-option-content"></div>
                                                            <div class="clear-float"></div>
                                                        </div>
                                                        <div class="preview-option">
                                                            <div class="preview-option-content"></div>
                                                            <div class="clear-float"></div>
                                                        </div>
                                                    </div>

                                                    <div id="divNewCardType2PreviewLayout" style="height: 0px; display: none;"></div>
                                                    <div id="divNewCardType3PreviewLayout" style="height: 0px; display: none;">
                                                        <div class="preview-textarea">
                                                            <div class="preview-textarea-content">Answer area</div>
                                                        </div>
                                                    </div>
                                                    <div id="divNewCardType4PreviewLayout" style="height: 0px; overflow: auto; display: none;">
                                                        <div class="preview-numberrange">
                                                            <div>
                                                                <input name="range" id="range" type="text" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divNewCardType5PreviewLayout" style="height: 0px; display: none;">
                                                        <select class="preview-droplist">
                                                            <option>Please Select</option>
                                                        </select>
                                                    </div>
                                                    <div id="divNewCardType6PreviewLayout" style="height: 0px; display: none;">
                                                    </div>
                                                </div>
                                                <!-- For PageBreak, IsAllowSkipped -->
                                                <div class="preview-footer-content" id="divNewCardPeviewNoPageBreak" style="background: rgba(0, 153, 253, 1); bottom: 0px; color: white; position: absolute;"><span class="center">No page break</span></div>
                                                <div class="preview-footer-content" id="divNewCardPeviewPageBreak" style="background: rgba(0, 153, 253, 1); bottom: 0px; color: white; display: none; position: absolute;"><span class="center">Next</span></div>
                                                <div class="preview-footer-content" id="divNewCardPeviewSkip" style="bottom: 0px; color: rgba(150, 176, 216, 1); display: none; position: absolute;"><span class="right">Skip</span></div>
                                            </div>
                                            <!-- / Card Preview Format -->
                                        </div>
                                        <div class="add-question__info__setup__choice">
                                            <div id="divNewCardBehaviour">
                                                <label style="font-weight: bold;">Behaviour</label>
                                                <span class="setting-checkbox">
                                                    <input name="cbNewCardAllowSkip" id="cbNewCardAllowSkip" onclick="setNewCardValue(3, this, null);" type="checkbox"><label for="cbNewCardAllowSkip">Allow question to be skipped</label>
                                                </span>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardTickOther">
                                                    <span class="setting-checkbox">
                                                        <input name="cbNewCardTickOther" id="cbNewCardTickOther" onclick="setNewCardValue(4, this, null);" type="checkbox">
                                                        <label for="cbNewCardTickOther">Tick 'other' and type your own answer</label>
                                                    </span>
                                                </div>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardCustomCommand" style="height: 0px; overflow: auto; visibility: hidden;">
                                                    Custom command
                                                    <input id="tbNewCardCustomCommand" onkeyup="setNewCardValue(6, this, null);" onblur="setNewCardValue(6, this, null);" type="text" placeholder="Please specify (Default)">
                                                </div>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardRandomOption" style="height: 0px; overflow: auto; visibility: hidden;">
                                                    <span class="setting-checkbox">
                                                        <input name="cbNewCardRandomOption" id="cbNewCardRandomOption" onclick="setNewCardValue(5, this, null);" type="checkbox">
                                                        <label for="cbNewCardRandomOption">Randomize option order</label>
                                                    </span>
                                                </div>
                                                <div class="clear-float"></div>

                                            </div>
                                            <br>
                                            <div id="divNewCardMulti" style="height: 0px; visibility: hidden;">
                                                <label style="font-weight: bold;">No of options to be selected</label>
                                                Minimum
                                                <input name="tbNewCardMultiMin" id="tbNewCardMultiMin" onblur="setNewCardValue(7, this, null);" type="number" value="1"><br>
                                                Maximum
                                                <input name="tbNewCardMultiMax" id="tbNewCardMultiMax" onblur="setNewCardValue(8, this, null);" type="number" value="1"><br>
                                            </div>
                                            <br>
                                            <div id="divNewCardDisplay">
                                                <label style="font-weight: bold;">Display</label>
                                                <div>
                                                    Background
                                                    <!-- Background -->
                                                    <div class="imageslider" style="margin: 10px auto auto; width: 85%;">
                                                        <div id="slider">
                                                            <!-- Slider Setup -->
                                                            <input name="slider" id="slide1" onchange="setNewCardValue(13, this, 1);" type="radio" checked="" selected="false">
                                                            <input name="slider" id="slide2" onchange="setNewCardValue(13, this, 2);" type="radio" selected="false">
                                                            <input name="slider" id="slide3" onchange="setNewCardValue(13, this, 3);" type="radio" selected="false">
                                                            <!-- / Slider Setup -->
                                                            <!-- The Slider -->
                                                            <div id="slides" style="margin-top: 0px;">
                                                                <div id="overflow">
                                                                    <div class="inner">
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_1a_small.png">
                                                                        </article>
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_2a_small.png">
                                                                        </article>
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_3a_small.png">
                                                                        </article>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- / The Slider -->
                                                            <!-- Controls and Active Slide Display -->
                                                            <div class="controls" id="controls" style="top: 38%; display: none;">
                                                                <label for="slide1"></label>
                                                                <label for="slide2"></label>
                                                                <label for="slide3"></label>
                                                            </div>
                                                            <!-- / Controls and Active Slide Display -->
                                                        </div>
                                                    </div>
                                                    <!-- Background -->

                                                </div>
                                                <br>
                                            </div>
                                            <br>
                                            <div id="divNewCardNotes">
                                                <label style="font-weight: bold;">Notes</label>
                                                <input name="tbNewCardNotes" id="tbNewCardNotes" onkeydown="return (event.keyCode!=13);" onblur="setNewCardValue(9, this, null);" type="text" maxlength="150" placeholder="Enter notes">
                                                <span style="color: rgb(151, 151, 151);">Notes will be seen by user during the survey via the
                                                    <img src="/Img/icon_note_small.png">icon</span>
                                            </div>
                                            <br>
                                            <div id="divNewCardPageBreak">
                                                <span class="setting-checkbox">
                                                    <input name="cbNewCardPageBreak" id="cbNewCardPageBreak" onclick="setNewCardValue(10, this, null);" type="checkbox">
                                                    <label for="cbNewCardPageBreak">Page Break</label>
                                                </span>
                                                <div class="clear-float"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--************-->
                        </div>
                        <div class="dvCardList APPC98748ef37f084c38bfc03368db11890a" style="margin-top: 10px;"></div>
                    </div>
                    <hr class="category_hr APPC98748ef37f084c38bfc03368db11890a" style="border-width: 5px; width: 100%; margin-bottom: 3em;">
                    <div class="category_layout APPC46e658cb2c484c67b540ac9d11508be4" style="margin-top: 20px;" data-ordering="1" data-categoryid="APPC46e658cb2c484c67b540ac9d11508be4">
                        <div class="category_detail">
                            <div class="grid-question-number" style="margin: 0px; width: calc(100% - 150px); float: left; display: inline;">
                                <label style="font-size: 1.2em; font-weight: bolder;">002-Category</label>
                            </div>
                            <div class="grid-question-icon" style="width: 150px; float: left;"><span style="padding: 10px; text-align: right; color: rgba(57, 137, 242, 1); font-weight: bold; cursor: pointer;" onclick="switchCategoryMode('APPC46e658cb2c484c67b540ac9d11508be4', true); ">Edit</span>                <span style="padding: 10px; text-align: right; color: rgba(57, 137, 242, 1); font-weight: bold; cursor: pointer;" onclick="showPopup(2, 'APPC46e658cb2c484c67b540ac9d11508be4'); ">Delete</span>            </div>
                        </div>
                        <hr style="width: 100%;">
                        <div style="width: 100%; display: inline-block;">
                            <button class="add-card survey-bar__search__button" style="margin: 0px; visibility: visible;" onclick="addCard('APPC46e658cb2c484c67b540ac9d11508be4');" type="button">+ Add Question</button>
                        </div>
                        <div class="dvNewCard" style="width: 100%; display: inline-block;"></div>
                        <div class="dvCardList APPC46e658cb2c484c67b540ac9d11508be4" style="margin-top: 10px;"></div>
                    </div>
                    <hr class="category_hr APPC46e658cb2c484c67b540ac9d11508be4" style="border-width: 5px; width: 100%; margin-bottom: 3em;">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
