﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Live360
{
    public partial class BarsList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo managerInfo = null;
        private List<Appraisal> appraisals = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    ViewState["currentSortField"] = "SortByTitle";
                    ViewState["currentSortDirection"] = "asc";


                    RefreshListView();
                }

                //tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RefreshListView()
        {
            GetAllAppraisals();
            if (appraisals != null)
            {
                switch (ViewState["currentSortField"].ToString())
                {
                    case "SortByTitle":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = appraisals.OrderBy(a => a.Title).ToList();
                        }
                        else
                        {
                            lvList.DataSource = appraisals.OrderByDescending(a => a.Title).ToList();
                        }
                        break;

                    case "SortByType":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = appraisals.OrderBy(a => a.AppraisalType).ToList();
                        }
                        else
                        {
                            lvList.DataSource = appraisals.OrderByDescending(a => a.AppraisalType).ToList();
                        }
                        break;

                    case "SortByQuestions":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = appraisals.OrderBy(a => a.NumberOfCards).ToList();
                        }
                        else
                        {
                            lvList.DataSource = appraisals.OrderByDescending(a => a.NumberOfCards).ToList();
                        }
                        break;

                    case "SortByProgress":

                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = appraisals.OrderBy(a => a.Progress).ToList();
                        }
                        else
                        {
                            lvList.DataSource = appraisals.OrderByDescending(a => a.Progress).ToList();
                        }
                        break;

                    case "SortByStatus":

                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = appraisals.OrderBy(a => a.Status).ToList();
                        }
                        else
                        {
                            lvList.DataSource = appraisals.OrderByDescending(a => a.Status).ToList();
                        }
                        break;



                    default:
                        lvList.DataSource = appraisals;
                        break;
                }

                lvList.DataBind();
                if (appraisals != null && appraisals.Count > 1)
                {
                    ltlCount.Text = appraisals.Count + " templates";
                }
                else if (appraisals != null && appraisals.Count == 1)
                {
                    ltlCount.Text = "1 template";
                }
                else
                {
                    ltlCount.Text = "0 template";
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        public void GetAllAppraisals()
        {
            AppraisalSelectAllTemplatesResponse response = asc.SelectAppraisalTemplates(managerInfo.UserId, managerInfo.CompanyId, (int)Appraisal.AppraisalTypeEnum.Bars);
            if (response.Success)
            {
                appraisals = response.Templates;
            }
            else
            {
                Log.Error("AppraisalSelectAllTemplatesResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    #region Sort 
                    ListView lv = sender as ListView;
                    LinkButton headerLinkButton = null;
                    string[] linkButtonControlList = new[] { "SortByTitle", "SortByType", "SortByQuestions", "SortByProgress", "SortByStatus" };


                    if (lv == null)
                        return;

                    // Remove the up-down arrows from each header
                    foreach (string lbControlId in linkButtonControlList)
                    {
                        headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                        if (headerLinkButton != null)
                        {
                            headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                            headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                        }
                    }

                    // Add sort direction back to the field
                    headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                    if (headerLinkButton != null)
                    {
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                        }
                        else
                        {
                            headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                        }
                    }
                    #endregion

                    #region Type
                    LinkButton lbType = e.Item.FindControl("lbType") as LinkButton;
                    switch (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].AppraisalType)
                    {
                        case (int)Appraisal.AppraisalTypeEnum.Likert7:
                            lbType.Text = "Likert 7";

                            break;

                        case (int)Appraisal.AppraisalTypeEnum.Bars:
                            lbType.Text = "BARS";

                            break;

                        default:
                            break;
                    }
                    #endregion

                    #region Progress
                    LinkButton lbProgress = e.Item.FindControl("lbProgress") as LinkButton;
                    switch (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Progress)
                    {
                        case (int)Appraisal.AppraisalProgressEnum.Upcoming:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(254, 204, 0, 1)\"></i> Upcoming";

                            break;

                        case (int)Appraisal.AppraisalProgressEnum.Active:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(148, 207, 0, 1)\"></i> Live";

                            break;

                        case (int)Appraisal.AppraisalProgressEnum.Completed:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(178, 178, 178, 1)\"></i> Completed";

                            break;

                        default:
                            break;
                    }
                    #endregion

                    #region Status
                    LinkButton lbStatus = e.Item.FindControl("lbStatus") as LinkButton;
                    if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Status == (int)Appraisal.AppraisalStatusEnum.Unlisted)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Unlisted";
                    }
                    else if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Status == (int)Appraisal.AppraisalStatusEnum.Active)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(0, 117, 254, 1)'></i> Active";
                    }
                    else if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Status == (int)Appraisal.AppraisalStatusEnum.Hidden)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Hidden";
                    }
                    else
                    {
                        // do nothing
                    }
                    #endregion

                    #region Participants
                    LinkButton lbParticipants = e.Item.FindControl("lbParticipants") as LinkButton;
                    if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Privacy.IsForEveryone)
                    {
                        lbParticipants.Text = "<i class='fa fa-globe' style='margin-right: 5px;'></i>Everyone";
                    }
                    else
                    {
                        if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Privacy.IsForDepartment)
                        {
                            lbParticipants.Text += "<i class='fa fa-briefcase' style='margin-right: 5px;'></i>";
                        }

                        if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Privacy.IsForUser)
                        {
                            lbParticipants.Text += "<i class='fa fa-user' style='margin-right: 5px;'></i>";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
        protected void lvList_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.EmptyItem)
                {
                    Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                    ltlEmptyMsg.Text = @"Welcome to Live360 Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding a BARS by clicking on the green button.";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvList_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            RefreshListView();
        }

        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Live360/BarsDetail/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Analytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Live360/BarsAnalytic/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    LinkButton lbTitle = e.Item.FindControl("lbTitle") as LinkButton;
                    hfId.Value = e.CommandArgument.ToString();
                    hfTitle.Value = lbTitle.Text;

                    if (e.CommandName.Equals("Activate", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = Convert.ToString((int)Appraisal.AppraisalStatusEnum.Active);
                        ltlActionName.Text = "Activate ";
                        lbAction.Text = "Activate";
                        ltlActionMsg.Text = "Activate <b>" + lbTitle.Text + "</b>. Confirm?";

                    }
                    else if (e.CommandName.Equals("Hide", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = Convert.ToString((int)Appraisal.AppraisalStatusEnum.Hidden);
                        ltlActionName.Text = "Hide ";
                        lbAction.Text = "Hide";
                        ltlActionMsg.Text = "Hide <b>" + lbTitle.Text + "</b>. Confirm?";
                    }
                    else if (e.CommandName.Equals("Del", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = Convert.ToString((int)Appraisal.AppraisalStatusEnum.Deleted);
                        ltlActionName.Text = "Delete ";
                        lbAction.Text = "Delete";
                        ltlActionMsg.Text = "All questions within <b>" + lbTitle.Text + "</b> will be gone. Confirm?";
                    }

                    ltlActionTitle.Text = lbTitle.Text;
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                AppraisalUpdateResponse response = asc.UpdateAppraisalStatus(managerInfo.UserId, managerInfo.CompanyId, hfId.Value, Convert.ToInt16(lbAction.CommandArgument));
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    mpePop.Hide();
                    RefreshListView();

                    if (Convert.ToInt16(lbAction.CommandArgument) == 2)
                    {
                        toastMsg = hfTitle.Value + " has been set to active.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == 3)
                    {
                        toastMsg = hfTitle.Value + " has been set to hidden.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == -1)
                    {
                        toastMsg = hfTitle.Value + " has been deleted.";
                    }
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("AppraisalUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    if (response.ErrorCode == Convert.ToInt32(CassandraService.GlobalResources.ErrorCode.AppraisalCannotActive))
                    {
                        toastMsg = response.ErrorMessage;
                    }
                    else
                    {
                        toastMsg = "Failed to change status, please check your internet connection.";
                    }
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }
    }
}