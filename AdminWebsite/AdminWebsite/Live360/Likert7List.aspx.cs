﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Live360
{
    public partial class Likert7List : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo managerInfo = null;
        private List<Appraisal> Likert7s = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    ViewState["currentSortField"] = "SortByTitle";
                    ViewState["currentSortDirection"] = "asc";


                    RefreshListView();
                }

                //tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RefreshListView()
        {
            GetAllLikert7s();
            if (Likert7s != null)
            {
                switch (ViewState["currentSortField"].ToString())
                {
                    case "SortByTitle":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = Likert7s.OrderBy(a => a.Title).ToList();
                        }
                        else
                        {
                            lvList.DataSource = Likert7s.OrderByDescending(a => a.Title).ToList();
                        }
                        break;

                    case "SortByType":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = Likert7s.OrderBy(a => a.AppraisalType).ToList();
                        }
                        else
                        {
                            lvList.DataSource = Likert7s.OrderByDescending(a => a.AppraisalType).ToList();
                        }
                        break;

                    case "SortByQuestions":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = Likert7s.OrderBy(a => a.NumberOfCards).ToList();
                        }
                        else
                        {
                            lvList.DataSource = Likert7s.OrderByDescending(a => a.NumberOfCards).ToList();
                        }
                        break;

                    case "SortByProgress":

                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = Likert7s.OrderBy(a => a.Progress).ToList();
                        }
                        else
                        {
                            lvList.DataSource = Likert7s.OrderByDescending(a => a.Progress).ToList();
                        }
                        break;

                    case "SortByStatus":

                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = Likert7s.OrderBy(a => a.Status).ToList();
                        }
                        else
                        {
                            lvList.DataSource = Likert7s.OrderByDescending(a => a.Status).ToList();
                        }
                        break;



                    default:
                        lvList.DataSource = Likert7s;
                        break;
                }

                lvList.DataBind();
                if (Likert7s != null && Likert7s.Count > 1)
                {
                    ltlCount.Text = Likert7s.Count + " templates";
                }
                else if (Likert7s != null && Likert7s.Count == 1)
                {
                    ltlCount.Text = "1 template";
                }
                else
                {
                    ltlCount.Text = "0 template";
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        public void GetAllLikert7s()
        {
            AppraisalSelectAllTemplatesResponse response = asc.SelectAppraisalTemplates(managerInfo.UserId, managerInfo.CompanyId, (int)Appraisal.AppraisalTypeEnum.Likert7);
            if (response.Success)
            {
                Likert7s = response.Templates;
            }
            else
            {
                Log.Error("AppraisalSelectAllTemplatesResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    #region Sort 
                    ListView lv = sender as ListView;
                    LinkButton headerLinkButton = null;
                    string[] linkButtonControlList = new[] { "SortByTitle", "SortByType", "SortByQuestions", "SortByProgress", "SortByStatus" };


                    if (lv == null)
                        return;

                    // Remove the up-down arrows from each header
                    foreach (string lbControlId in linkButtonControlList)
                    {
                        headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                        if (headerLinkButton != null)
                        {
                            headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                            headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                        }
                    }

                    // Add sort direction back to the field
                    headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                    if (headerLinkButton != null)
                    {
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                        }
                        else
                        {
                            headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                        }
                    }
                    #endregion

                    #region Type
                    LinkButton lbType = e.Item.FindControl("lbType") as LinkButton;
                    switch (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].AppraisalType)
                    {
                        case (int)Appraisal.AppraisalTypeEnum.Likert7:
                            lbType.Text = "Likert 7";

                            break;

                        case (int)Appraisal.AppraisalTypeEnum.Bars:
                            lbType.Text = "BARS";

                            break;

                        default:
                            break;
                    }
                    #endregion

                    #region Progress
                    LinkButton lbProgress = e.Item.FindControl("lbProgress") as LinkButton;
                    switch (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Progress)
                    {
                        case (int)Appraisal.AppraisalProgressEnum.Upcoming:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(254, 204, 0, 1)\"></i> Upcoming";

                            break;

                        case (int)Appraisal.AppraisalProgressEnum.Active:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(148, 207, 0, 1)\"></i> Live";

                            break;

                        case (int)Appraisal.AppraisalProgressEnum.Completed:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(178, 178, 178, 1)\"></i> Completed";

                            break;

                        default:
                            break;
                    }
                    #endregion

                    #region Status
                    LinkButton lbStatus = e.Item.FindControl("lbStatus") as LinkButton;
                    if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Status == (int)Appraisal.AppraisalStatusEnum.Unlisted)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Unlisted";
                    }
                    else if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Status == (int)Appraisal.AppraisalStatusEnum.Active)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(0, 117, 254, 1)'></i> Active";
                    }
                    else if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Status == (int)Appraisal.AppraisalStatusEnum.Hidden)
                    {
                        lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Hidden";
                    }
                    else
                    {
                        // do nothing
                    }
                    #endregion

                    #region Participants
                    LinkButton lbParticipants = e.Item.FindControl("lbParticipants") as LinkButton;
                    if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Privacy.IsForEveryone)
                    {
                        lbParticipants.Text = "<i class='fa fa-globe' style='margin-right: 5px;'></i>Everyone";
                    }
                    else
                    {
                        if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Privacy.IsForDepartment)
                        {
                            lbParticipants.Text += "<i class='fa fa-briefcase' style='margin-right: 5px;'></i>";
                        }

                        if (((List<Appraisal>)lvList.DataSource)[e.Item.DataItemIndex].Privacy.IsForUser)
                        {
                            lbParticipants.Text += "<i class='fa fa-user' style='margin-right: 5px;'></i>";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvList_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.EmptyItem)
                {
                    Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                    ltlEmptyMsg.Text = @"Welcome to Live360 Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding a Likert7 by clicking on the green button.";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvList_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            RefreshListView();
        }

        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Live360/Likert7Detail/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Analytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Live360/Likert7Analytic/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    LinkButton lbTitle = e.Item.FindControl("lbTitle") as LinkButton;
                    hfId.Value = e.CommandArgument.ToString();
                    hfTitle.Value = lbTitle.Text;

                    if (e.CommandName.Equals("Activate", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = Convert.ToString((int)Appraisal.AppraisalStatusEnum.Active);
                        ltlActionName.Text = "Activate ";
                        lbAction.Text = "Activate";
                        ltlActionMsg.Text = "Activate <b>" + lbTitle.Text + "</b>. Confirm?";

                    }
                    else if (e.CommandName.Equals("Hide", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = Convert.ToString((int)Appraisal.AppraisalStatusEnum.Hidden);
                        ltlActionName.Text = "Hide ";
                        lbAction.Text = "Hide";
                        ltlActionMsg.Text = "Hide <b>" + lbTitle.Text + "</b>. Confirm?";
                    }
                    else if (e.CommandName.Equals("Del", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = Convert.ToString((int)Appraisal.AppraisalStatusEnum.Deleted);
                        ltlActionName.Text = "Delete ";
                        lbAction.Text = "Delete";
                        ltlActionMsg.Text = "All questions within <b>" + lbTitle.Text + "</b> will be gone. Confirm?";
                    }

                    ltlActionTitle.Text = lbTitle.Text;
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                AppraisalUpdateResponse response = asc.UpdateAppraisalStatus(managerInfo.UserId, managerInfo.CompanyId, hfId.Value, Convert.ToInt16(lbAction.CommandArgument));
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    mpePop.Hide();
                    RefreshListView();

                    if (Convert.ToInt16(lbAction.CommandArgument) == 2)
                    {
                        toastMsg = hfTitle.Value + " has been set to active.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == 3)
                    {
                        toastMsg = hfTitle.Value + " has been set to hidden.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == -1)
                    {
                        toastMsg = hfTitle.Value + " has been deleted.";
                    }
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("AppraisalUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    if (response.ErrorCode == Convert.ToInt32(CassandraService.GlobalResources.ErrorCode.AppraisalCannotActive))
                    {
                        toastMsg = response.ErrorMessage;
                    }
                    else
                    {
                        toastMsg = "Failed to change status, please check your internet connection.";
                    }
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }

        protected void xxxbtnDownloadCsv_Click(object sender, EventArgs e)
        {
            AppraisalSelectOverallReportsResponse response = asc.SelectAppraisalReportsByAdmin(managerInfo.UserId, managerInfo.CompanyId);
            if (response.Success)
            {
                System.Web.Script.Serialization.JavaScriptSerializer jsSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = jsSerializer.Serialize(response.TemplateAppraisals);

                System.IO.MemoryStream jsonMemoryStream = null;
                using (jsonMemoryStream = new System.IO.MemoryStream())
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(jsonMemoryStream))
                {
                    sw.AutoFlush = true;
                    sw.Write(jsonString);

                }
                byte[] jsonFileBytes = jsonMemoryStream.ToArray(); // simpler way of converting to array

                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=template-appraisals-jsonData.json");
                Response.BinaryWrite(jsonFileBytes);
                Response.End();
            }

            return;

            System.IO.MemoryStream memoryStream = null;
            using (memoryStream = new System.IO.MemoryStream())
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(memoryStream))
            {
                sw.AutoFlush = true;
                sw.WriteLine("csv1-1, csv1-2, csv1-3");
                sw.WriteLine("csv2-1, csv2-2, csv2-3");
            }
            byte[] fileBytes = memoryStream.ToArray(); // simpler way of converting to array


            System.IO.MemoryStream individualDataMemoryStream = null;
            using (individualDataMemoryStream = new System.IO.MemoryStream())
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(individualDataMemoryStream))
            {
                sw.AutoFlush = true;
                sw.WriteLine("icsv1-1, icsv1-2, icsv1-3");
                sw.WriteLine("icsv2-1, icsv2-2, icsv2-3");
            }
            byte[] individualDataFileBytes = individualDataMemoryStream.ToArray(); // simpler way of converting to array

            System.IO.MemoryStream zipMemoryStream = null;
            using (zipMemoryStream = new System.IO.MemoryStream())
            using (var archive = new System.IO.Compression.ZipArchive(zipMemoryStream, System.IO.Compression.ZipArchiveMode.Create, true))
            {
                System.IO.Compression.ZipArchiveEntry surveyArchiveEntry = archive.CreateEntry("survey.csv");
                using (System.IO.Stream entryStream = surveyArchiveEntry.Open())
                {
                    entryStream.Write(fileBytes, 0, fileBytes.Length);
                }

                System.IO.Compression.ZipArchiveEntry individualArchiveEntry = archive.CreateEntry("individual.csv");
                using (System.IO.Stream entryStream = individualArchiveEntry.Open())
                {
                    entryStream.Write(individualDataFileBytes, 0, individualDataFileBytes.Length);
                }
            }

            byte[] zipFileBytes = zipMemoryStream.ToArray();

            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("content-disposition", "attachment; filename=likert7-data.zip");
            Response.BinaryWrite(zipFileBytes);
            Response.End();

        }

        #region helper functions
        static string GetCommentAsCsvString(List<CassandraService.Entity.AppraisalCard.AppraisalComment> comments)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            foreach (CassandraService.Entity.AppraisalCard.AppraisalComment comment in comments)
            {
                if (!string.IsNullOrWhiteSpace(comment.Content))
                {
                    sb.Append(comment.Content);
                    sb.Append(",");
                }
            }

            return sb.ToString().Trim(',');
        }

        static string GetCardContent(CassandraService.Entity.Appraisal appraisal, string cardId)
        {
            string content = string.Empty;
            bool found = false;

            foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
            {
                foreach (CassandraService.Entity.AppraisalCard card in category.Cards)
                {
                    if (card.CardId == cardId)
                    {
                        content = card.Content;
                        found = true;
                        break;
                    }
                }

                if (found)
                {
                    break;
                }
            }

            return content;
        }

        #endregion helper functions

        static byte[] GenerateSurveyCsv(List<CassandraService.Entity.Appraisal> appraisalList, List<CassandraService.Entity.Appraisal> templateAppraisalList)
        {
            Dictionary<string, int> cardColumnPosition = new Dictionary<string, int>();
            if (appraisalList == null)
                return null;

            int tempPos = 0;
            int headerFieldCount = 0;
            int categoryItemNumber = 1;
            int feedbackItemNumber = 1;
            int customisedFeedbackItemNumber = 1;

            List<string> surveyHeaders = new List<string>();

            #region define headers
            ////////////////////////////////////////
            // Fixed headers
            ////////////////////////////////////////
            #region define surveyHeaders 
            surveyHeaders.Add("Survey ID"); headerFieldCount++;
            surveyHeaders.Add("Feedback Creator (Last Name)"); headerFieldCount++;
            surveyHeaders.Add("Feedback Creator (First Name)"); headerFieldCount++;
            surveyHeaders.Add("Email Address"); headerFieldCount++;
            surveyHeaders.Add("Agency"); headerFieldCount++;
            surveyHeaders.Add("Age Band"); headerFieldCount++;
            surveyHeaders.Add("No. of respondents invited"); headerFieldCount++;
            surveyHeaders.Add("No. of respondents who completed session"); headerFieldCount++;
            surveyHeaders.Add("Session Start Date"); headerFieldCount++;
            surveyHeaders.Add("Session Start Time"); headerFieldCount++;
            surveyHeaders.Add("Session End Date"); headerFieldCount++;
            surveyHeaders.Add("Session End Time"); headerFieldCount++;
            surveyHeaders.Add("Duration (in seconds)"); headerFieldCount++;
            surveyHeaders.Add("Avg Time Taken to Complete Session (in seconds)"); headerFieldCount++;
            surveyHeaders.Add("Title of Session"); headerFieldCount++;
            surveyHeaders.Add("Context of Session"); headerFieldCount++;
            surveyHeaders.Add("Job Level"); headerFieldCount++;
            //surveyHeaders.Add("Team Type"); headerFieldCount++;
            surveyHeaders.Add("Anonymous/ Identifiable"); headerFieldCount++;
            #endregion

            ////////////////////////////////////////
            // Set of 3 columns for each card (Feedback item, Self-Assessment, Remarks)
            ////////////////////////////////////////
            foreach (CassandraService.Entity.Appraisal appraisal in templateAppraisalList)
            {
                foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
                {
                    foreach (CassandraService.Entity.AppraisalCard card in category.Cards)
                    {
                        cardColumnPosition.Add("card-" + card.CardId, headerFieldCount);
                        surveyHeaders.Add(card.Content); headerFieldCount++;
                        surveyHeaders.Add(string.Format("Feedback item {0} Remarks", feedbackItemNumber)); headerFieldCount++;
                        surveyHeaders.Add(string.Format("Feedback item {0} self-assessment", feedbackItemNumber)); headerFieldCount++;
                        feedbackItemNumber++;
                    }
                }
            }

            ////////////////////////////////////////
            // Total No. of Feedback Items from Category n
            ////////////////////////////////////////
            foreach (CassandraService.Entity.Appraisal appraisal in templateAppraisalList)
            {
                foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
                {
                    cardColumnPosition.Add("category-" + category.CategoryId, headerFieldCount);
                    surveyHeaders.Add(string.Format("Total No. Feedback Items from Category {0}", categoryItemNumber++)); headerFieldCount++;
                }
            }

            // Total No. of Feedback Items Chosen
            cardColumnPosition.Add("total-feedback-items", headerFieldCount);
            surveyHeaders.Add("Total No. of Feedback Items Chosen"); headerFieldCount++;

            ////////////////////////////////////////
            // Set of 3 columns for each card (Customised Feedback item, Customised Self-Assessment, Customised Remarks)
            ////////////////////////////////////////
            // Find number of customised card

            int maxCustomisedFeedbackItemNumber = 0;
            foreach (CassandraService.Entity.Appraisal appraisal in appraisalList)
            {
                customisedFeedbackItemNumber = 0;

                foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
                {
                    foreach (CassandraService.Entity.AppraisalCard card in category.Cards)
                    {
                        if (!card.IsReferencedFromAdmin)
                        {
                            cardColumnPosition.Add("customised-" + card.CardId, headerFieldCount + customisedFeedbackItemNumber);
                            //customisedFeedbackItemNumber++;
                            customisedFeedbackItemNumber = customisedFeedbackItemNumber + 4;
                        }
                    }
                }

                if (maxCustomisedFeedbackItemNumber <= customisedFeedbackItemNumber)
                {   // Set new max
                    maxCustomisedFeedbackItemNumber = customisedFeedbackItemNumber;
                }
            }

            maxCustomisedFeedbackItemNumber = maxCustomisedFeedbackItemNumber / 4;

            for (int feedBackIdx = 0; feedBackIdx < maxCustomisedFeedbackItemNumber; feedBackIdx++)
            {
                surveyHeaders.Add(string.Format("Customised Feedback item {0}", (feedBackIdx + 1))); headerFieldCount++;
                surveyHeaders.Add(string.Format("Customised Feedback item {0} score ", (feedBackIdx + 1))); headerFieldCount++;
                surveyHeaders.Add(string.Format("Customised Feedback item {0} self-assessment", (feedBackIdx + 1))); headerFieldCount++;
                surveyHeaders.Add(string.Format("Customised Feedback item {0} remarks", (feedBackIdx + 1))); headerFieldCount++;
            }

            #endregion define headers

            List<string[]> records = new List<string[]>();

            foreach (CassandraService.Entity.Appraisal appraisal in appraisalList)
            {
                string[] record = new string[surveyHeaders.Count()];
                record[0] = appraisal.AppraisalId;
                record[1] = appraisal.CreatedByUser.LastName;
                record[2] = appraisal.CreatedByUser.FirstName;
                record[3] = appraisal.CreatedByUser.Email;
                record[4] = appraisal.CreatedByUser.Departments.FirstOrDefault() == null ? string.Empty : appraisal.CreatedByUser.Departments.FirstOrDefault().Title;
                record[5] = appraisal.CreatedByUser.AgeBand;
                record[6] = appraisal.NumberOfParticipants.ToString();
                record[7] = appraisal.NumberOfParticipantsCompleted.ToString();
                record[8] = appraisal.StartDateString;
                record[9] = appraisal.StartTimeString;
                record[10] = appraisal.EndDateString;
                record[11] = appraisal.EndTimeString;
                record[12] = appraisal.Duration;
                record[13] = appraisal.AverageTimeTaken;
                record[14] = appraisal.Title;
                record[15] = appraisal.Description;
                record[16] = appraisal.CreatedByUser.Job.Title;
                //record[17] = appraisal.SelectedTeamType.Label;
                record[17] = appraisal.IsAnonymous == true ? "Anonymous" : "Identifiable";

                //string[] record = new string[]
                //{
                //    appraisal.AppraisalId,
                //    appraisal.CreatedByUser.LastName,
                //    appraisal.CreatedByUser.FirstName,
                //    appraisal.CreatedByUser.Email,
                //    appraisal.CreatedByUser.Departments.FirstOrDefault() == null ? string.Empty : appraisal.CreatedByUser.Departments.FirstOrDefault().Title,
                //    "Age Band(missing)",
                //    appraisal.NumberOfParticipants.ToString(),
                //    appraisal.NumberOfParticipantsCompleted.ToString(),
                //    appraisal.StartDateString,
                //    appraisal.EndDateString,
                //    appraisal.Duration,
                //    appraisal.AverageTimeTaken,
                //    appraisal.Title,
                //    appraisal.Description,
                //    "Job Level(missing)",
                //    appraisal.SelectedTeamType.Label,
                //    appraisal.IsAnonymous == true? "Anonymous" : "Identifiable"
                //};

                // Set of 3 columns for each card (Feedback item, Self-Assessment, Remarks)
                // Set of 3 columns for each card (Customised Feedback item, Customised Self-Assessment, Customised Remarks)
                foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
                {
                    foreach (CassandraService.Entity.AppraisalCard card in category.Cards)
                    {
                        //cardColumnPosition.Add("customised_" + card.CardId, headerFieldCount);
                        //// surveyHeaders.Add(card.Content); headerFieldCount++;
                        //surveyHeaders.Add(string.Format("Customised Feedback item {0}", feedbackItemNumber)); headerFieldCount++;
                        //surveyHeaders.Add(string.Format("Customised Feedback item self-assessment{0}", feedbackItemNumber)); headerFieldCount++;
                        //surveyHeaders.Add(string.Format("Customised Feedback item {0} remarks", feedbackItemNumber)); headerFieldCount++;
                        //feedbackItemNumber++;
                        int pos = 0;
                        if (card.IsReferencedFromAdmin)
                        {
                            pos = cardColumnPosition["card-" + card.ReferencedCardId];
                            record[pos] = card.AverageRating.ToString();
                            record[pos + 1] = GetCommentAsCsvString(card.Comments);
                            record[pos + 2] = card.SelfRating.ToString();
                        }
                        else // is customised
                        {
                            pos = cardColumnPosition["customised-" + card.CardId];
                            record[pos] = card.Content;
                            record[pos + 1] = card.AverageRating.ToString();
                            record[pos + 2] = card.SelfRating.ToString();
                            record[pos + 3] = GetCommentAsCsvString(card.Comments);
                        }

                    }
                }

                // Total No. of Feedback Items from Category n
                int totalCards = 0;
                foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
                {
                    if (category.IsReferencedFromAdmin)
                    {
                        tempPos = cardColumnPosition["category-" + category.ReferencedCategoryId];
                        record[tempPos] = category.Cards.Count().ToString();
                        totalCards = totalCards + category.Cards.Count();
                    }
                    // ZX: We don't count for customised
                }

                // Total No. of Feedback Items Chosen
                tempPos = cardColumnPosition["total-feedback-items"];
                record[tempPos] = totalCards.ToString();

                records.Add(record);
            }

            #region Finally write to file

            // Finally write to file
            int idx; int recordIdx;
            System.IO.MemoryStream memoryStream = null;
            using (memoryStream = new System.IO.MemoryStream())
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(memoryStream, Encoding.UTF8))
            {
                sw.AutoFlush = true;

                // Write header
                for (idx = 0; idx < surveyHeaders.Count; idx++)
                {
                    sw.Write(string.Format("\"{0}\"", surveyHeaders[idx]));
                    if ((idx + 1) < surveyHeaders.Count)
                    {
                        sw.Write(",");
                    }
                    else
                    {
                        sw.WriteLine();
                    }
                }

                // Write records
                for (recordIdx = 0; recordIdx < records.Count; recordIdx++)
                {
                    string[] record = records[recordIdx];
                    for (idx = 0; idx < record.Length; idx++)
                    {
                        sw.Write(string.Format("\"{0}\"", record[idx]));
                        if ((idx + 1) < record.Length)
                        {
                            sw.Write(",");
                        }
                        else
                        {
                            sw.WriteLine();
                        }
                    }
                }
            }
            byte[] fileBytes = memoryStream.ToArray(); // simpler way of converting to array


            return fileBytes;

            #endregion Finally write to file
        }

        static byte[] GenerateIndividualCsv(List<CassandraService.Entity.Appraisal> appraisalList, List<CassandraService.Entity.Appraisal> templateAppraisalList)
        {
            Dictionary<string, int> csvColumnPosition = new Dictionary<string, int>();

            if (appraisalList == null)
                return null;

            int headerFieldCount = 0;
            int feedbackItemNumber = 1;

            List<string> headers = new List<string>();
            #region define headers 
            headers.Add("Survey ID"); headerFieldCount++;
            headers.Add("Feedback Creator (Last Name)"); headerFieldCount++;
            headers.Add("Feedback Creator (First Name)"); headerFieldCount++;
            headers.Add("Email Address"); headerFieldCount++;
            headers.Add("Age Band"); headerFieldCount++;
            headers.Add("Job Level (Feedback Creator)"); headerFieldCount++;
            headers.Add("Agency (Feedback Creator)"); headerFieldCount++;
            headers.Add("Respondent (Last Name)"); headerFieldCount++;
            headers.Add("Respondent (First Name)"); headerFieldCount++;
            headers.Add("Email Address"); headerFieldCount++;
            headers.Add("Age Band"); headerFieldCount++;
            headers.Add("Job Level (Respondent)"); headerFieldCount++;
            headers.Add("Agency (Respondent)"); headerFieldCount++;
            headers.Add("Time Taken to Complete Feedback (in seconds)"); headerFieldCount++;
            #endregion

            ////////////////////////////////////////
            // Set of 2 columns for each card (Feedback item, Remarks)
            ////////////////////////////////////////
            foreach (CassandraService.Entity.Appraisal appraisal in templateAppraisalList)
            {
                foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
                {
                    foreach (CassandraService.Entity.AppraisalCard card in category.Cards)
                    {
                        csvColumnPosition.Add("card-" + card.CardId, headerFieldCount);
                        headers.Add(string.Format("Feedback item {0} (Raw score)", feedbackItemNumber)); headerFieldCount++;
                        headers.Add(string.Format("Feedback item {0} (Comments)", feedbackItemNumber)); headerFieldCount++;
                        feedbackItemNumber++;
                    }
                }
            }

            ////////////////////////////////////////
            // Set of 3 columns for each card (Feedback item, Raw Score, Comments)
            ////////////////////////////////////////
            //feedbackItemNumber = 1;
            int maxCustomisedFeedbackItemNumber = 0;
            int customisedFeedbackItemNumber = 0;
            foreach (CassandraService.Entity.Appraisal appraisal in appraisalList)
            {
                //customisedFeedbackItemNumber = 0;

                foreach (CassandraService.Entity.AppraisalCategory category in appraisal.Categories)
                {
                    foreach (CassandraService.Entity.AppraisalCard card in category.Cards)
                    {
                        if (!card.IsReferencedFromAdmin)
                        {
                            csvColumnPosition.Add("customised-" + card.CardId, headerFieldCount + customisedFeedbackItemNumber);
                            //customisedFeedbackItemNumber++; // 應該要+3
                            customisedFeedbackItemNumber = customisedFeedbackItemNumber + 3;
                        }
                    }
                }

                if (maxCustomisedFeedbackItemNumber <= customisedFeedbackItemNumber)
                {   // Set new max
                    maxCustomisedFeedbackItemNumber = customisedFeedbackItemNumber;
                }
            }

            maxCustomisedFeedbackItemNumber = maxCustomisedFeedbackItemNumber / 3;

            for (int feedBackIdx = 0; feedBackIdx < maxCustomisedFeedbackItemNumber; feedBackIdx++)
            {
                headers.Add(string.Format("Customised Feedback item {0}", (feedBackIdx + 1))); headerFieldCount++;
                headers.Add(string.Format("Customised Feedback item {0} score ", (feedBackIdx + 1))); headerFieldCount++;
                headers.Add(string.Format("Customised Feedback item {0} remarks", (feedBackIdx + 1))); headerFieldCount++;
            }

            List<string[]> records = new List<string[]>();

            // 
            foreach (CassandraService.Entity.Appraisal appraisal in appraisalList)
            {
                foreach (CassandraService.Entity.Appraisal.ParticipantBreakdown participant in appraisal.ParticipantsBreakdown)
                {
                    string[] record = new string[headers.Count()];

                    record[0] = appraisal.AppraisalId;
                    record[1] = appraisal.CreatedByUser.LastName;
                    record[2] = appraisal.CreatedByUser.FirstName;
                    record[3] = appraisal.CreatedByUser.Email;
                    record[4] = appraisal.CreatedByUser.AgeBand;
                    record[5] = appraisal.CreatedByUser.Job.Title;
                    record[6] = appraisal.CreatedByUser.Departments.FirstOrDefault() == null ? string.Empty : appraisal.CreatedByUser.Departments.FirstOrDefault().Title;
                    record[7] = participant.Participant.LastName;
                    record[8] = participant.Participant.FirstName;
                    record[9] = participant.Participant.Email;
                    record[10] = participant.Participant.AgeBand;
                    record[11] = participant.Participant.Job.Title;
                    record[12] = participant.Participant.Departments.FirstOrDefault() == null ? string.Empty : participant.Participant.Departments.FirstOrDefault().Title;
                    record[13] = participant.TimeTakenString;

                    //records.Add(new string[] {
                    //    appraisal.AppraisalId,
                    //    appraisal.CreatedByUser.LastName,
                    //    appraisal.CreatedByUser.FirstName,
                    //    appraisal.CreatedByUser.Email,
                    //    "Age Band(missing)",
                    //    "Job Level(missing)",
                    //    appraisal.CreatedByUser.Departments.FirstOrDefault() == null ? string.Empty : appraisal.CreatedByUser.Departments.FirstOrDefault().Title,
                    //    participant.Participant.LastName,
                    //    participant.Participant.FirstName,
                    //    participant.Participant.Email,
                    //    "Age Band(missing)",
                    //    "Job Level(missing)",
                    //    participant.Participant.Departments.FirstOrDefault() == null ? string.Empty : participant.Participant.Departments.FirstOrDefault().Title,
                    //    participant.TimeTakenString
                    //});

                    foreach (CassandraService.Entity.AppraisalCard.ParticipantAnswerBreakdown answerBreakdown in participant.AnswersBreakdown)
                    {
                        if (answerBreakdown.IsReferencedFromAdmin)
                        {
                            // Set of 2 columns for each card (Feedback item Score, Remarks)
                            int pos = csvColumnPosition["card-" + answerBreakdown.ReferencedCardId];
                            record[pos] = answerBreakdown.SelectedScore.ToString();
                            record[pos + 1] = answerBreakdown.Comment;
                        }
                        else
                        {
                            // Set of 3 columns for each card (Feedback item,Feedback item Score, Remarks)
                            int pos = csvColumnPosition["customised-" + answerBreakdown.CardId];
                            record[pos] = GetCardContent(appraisal, answerBreakdown.CardId);
                            record[pos + 1] = answerBreakdown.SelectedScore.ToString();
                            record[pos + 2] = answerBreakdown.Comment;
                        }
                    }

                    records.Add(record);

                }

            }


            // Finally write to file
            int idx; int recordIdx;
            System.IO.MemoryStream memoryStream = null;
            using (memoryStream = new System.IO.MemoryStream())
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(memoryStream, Encoding.UTF8))
            {
                sw.AutoFlush = true;

                // Write header
                for (idx = 0; idx < headers.Count; idx++)
                {
                    sw.Write(string.Format("\"{0}\"", headers[idx]));
                    if ((idx + 1) < headers.Count)
                    {
                        sw.Write(",");
                    }
                    else
                    {
                        sw.WriteLine();
                    }
                }

                // Write records
                for (recordIdx = 0; recordIdx < records.Count; recordIdx++)
                {
                    string[] record = records[recordIdx];
                    for (idx = 0; idx < record.Length; idx++)
                    {
                        sw.Write(string.Format("\"{0}\"", record[idx]));
                        if ((idx + 1) < record.Length)
                        {
                            sw.Write(",");
                        }
                        else
                        {
                            sw.WriteLine();
                        }
                    }
                }
            }
            byte[] fileBytes = memoryStream.ToArray(); // simpler way of converting to array


            return fileBytes;





        }

        protected void btnDownloadCsv_Click(object sender, EventArgs e)
        {
            AppraisalSelectOverallReportsResponse response = asc.SelectAppraisalReportsByAdmin(managerInfo.UserId, managerInfo.CompanyId);

            if (response.Success)
            {
                byte[] surveyFileBytes = GenerateSurveyCsv(response.Appraisals, response.TemplateAppraisals);
                byte[] individualFileBytes = GenerateIndividualCsv(response.Appraisals, response.TemplateAppraisals);
                System.IO.MemoryStream zipMemoryStream = null;
                using (zipMemoryStream = new System.IO.MemoryStream())
                using (var archive = new System.IO.Compression.ZipArchive(zipMemoryStream, System.IO.Compression.ZipArchiveMode.Create, true))
                {
                    System.IO.Compression.ZipArchiveEntry surveyArchiveEntry = archive.CreateEntry("survey.csv");
                    using (System.IO.Stream entryStream = surveyArchiveEntry.Open())
                    {
                        entryStream.Write(surveyFileBytes, 0, surveyFileBytes.Length);
                    }

                    System.IO.Compression.ZipArchiveEntry individualArchiveEntry = archive.CreateEntry("individual.csv");
                    using (System.IO.Stream entryStream = individualArchiveEntry.Open())
                    {
                        entryStream.Write(individualFileBytes, 0, individualFileBytes.Length);
                    }
                }

                byte[] zipFileBytes = zipMemoryStream.ToArray();

                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AddHeader("content-disposition", "attachment; filename=likert7-data.zip");
                Response.BinaryWrite(zipFileBytes);
                Response.End();
            }

        }
    }
}