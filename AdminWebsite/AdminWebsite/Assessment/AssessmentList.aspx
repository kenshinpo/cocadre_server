﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AssessmentList.aspx.cs" Inherits="AdminWebsite.Assessment.AssessmentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfSurveyId" runat="server" />
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfSurveyName" runat="server" />

            <asp:HiddenField ID="hfAssessmentId" runat="server" />
            <asp:HiddenField ID="hfAssessmentName" runat="server" />


            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/Assessment/Create" class="mfb-component__button--main" data-mfb-label="Add Assessment">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Delete Survey -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Assessment
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgActionSurvey" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionSurveyTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbAction_Click" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>

            <!-- Rename Assessment -->
            <asp:Panel ID="popup_renamecategory" runat="server" CssClass="popup popup--renamecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Rename Assessment</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Assessment Title</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbRenameName" runat="server" placeholder="Title of your assessment" MaxLength="20" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbRenameSave" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbRenameSave_Click" OnClientClick="ShowProgressBar();" Text="Save" />
                    <asp:LinkButton ID="lbRenameCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Rename Assessment  -->

            <!-- Delete Assessment  -->
            <asp:Panel ID="popup_deletecategory" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Delete Assessment</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <asp:Literal ID="ltlDeleteMsg" runat="server" />
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbDelete_Click" Text="Delete" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbDelCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Delete Assessment  -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Delete Survey -->


        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/" style="color: #000;">Assessment <span>console</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="surveyCountLiteral" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
            <div style="font-size: small;">
                <span style="color: black">Status</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #FECA02"></i>Draft&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #94CF00"></i>Live&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Assessments</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/Coach">Coach</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/Subscription">Subscription</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Assessment</label>
                        <asp:TextBox ID="tbFilterSurvey" runat="server" placeholder="Type the assessment's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterSurvey" runat="server" OnClick="ibFilterSurvey_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvSurvey" runat="server" OnItemCommand="lvSurvey_ItemCommand" OnSorting="lvSurvey_Sorting" OnItemDataBound="lvSurvey_ItemDataBound" OnItemCreated="lvSurvey_ItemCreated" OnItemEditing="lvSurvey_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 40px;"></th>
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentTitle" ID="lbSortAssessmentTitle" OnClientClick="ShowProgressBar('Filtering');">Assessment</asp:LinkButton></th>
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentCoach" ID="lbSortAssessmentCoach" OnClientClick="ShowProgressBar('Filtering');">Coach</asp:LinkButton></th>
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessmentStatus" ID="lbSortAssessmentStatus" OnClientClick="ShowProgressBar('Filtering');">Status</asp:LinkButton></th>
                                        <th class="no-sort"></th>
                                        <th class="no-sort" style="width: 5%;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField runat="server" ID="SurveyIdHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") %>' />
                                    <div class="constrained" style="display: inline-block;">
                                        <asp:Image ID="imgSurvey" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "IconUrl") %>' runat="server" />
                                    </div>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbAssessmentName" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") + "/"   %>' Style="color: #999;" />
                                </td>
                                <td>
                                    <asp:Literal ID="CategoryLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Coach.FirstName") %>'></asp:Literal>
                                </td>
                                <td>
                                    <asp:Literal Mode="PassThrough" Text="<i class='fa fa-circle' style='color:#94CF00'></i> Active" runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "Status").ToString() == "2" %>' />
                                    <asp:Literal Mode="PassThrough" Text="<i class='fa fa-circle' style='color:#FECA02'></i> Unlisted/Hidden" runat="server" Visible='<%# ((DataBinder.Eval(Container.DataItem, "Status").ToString() == "1") || (DataBinder.Eval(Container.DataItem, "Status").ToString() == "3")) %>' />
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Edit assessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") + "/"  %>' />
                                                    </li>
                                                    <%--<li>
                                                        <asp:LinkButton ID="lbActivate" ClientIDMode="AutoID" runat="server" CommandName="Activate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") + "," %>' Text="Activate survey" Visible='<%# ((DataBinder.Eval(Container.DataItem, "Status").ToString() == "1") || (DataBinder.Eval(Container.DataItem, "Status").ToString() == "3")) %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbHide" ClientIDMode="AutoID" runat="server" CommandName="Hide" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") + "," %>' Text="Hide survey" Visible='<%# DataBinder.Eval(Container.DataItem, "Status").ToString() == "2" %>' />
                                                    </li>--%>

                                                    <li>
                                                        <asp:LinkButton ID="lbRename" ClientIDMode="AutoID" runat="server" CommandName="RenameAssessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") %>' Text="Rename assessment" />
                                                    </li>

                                                    <li>
                                                        <asp:LinkButton ID="lbDuplicate" ClientIDMode="AutoID" runat="server" CommandName="DuplicateAssessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") %>' Text="Duplicate assessment" />
                                                    </li>

                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="DeleteAssessment" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssessmentId") %>' Text="Delete assessment" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
