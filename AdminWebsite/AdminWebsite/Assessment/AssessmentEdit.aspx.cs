﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using AdminWebsite.App_Code.Utilities.AWS;
using Amazon.S3;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Assessment
{
    public partial class AssessmentUpload
    {
        public CassandraService.Entity.Base64Content videoUpload { get; set; }

        public CassandraService.Entity.Base64Content videoThumbnailUpload { get; set; }

        public AssessmentUpload()
        {
            this.videoUpload = new Base64Content();
            this.videoThumbnailUpload = new Base64Content();
        }
    }

    [Serializable]
    public class AssessmentCardJson
    {
        public class ContentImage
        {
            public string base64 { get; set; }
            public string extension { get; set; }
            public int width { get; set; }
            public int height { get; set; }
        }

        public class TabulationAllocation
        {
            public int multiplier { get; set; }
            public string tabulationId { get; set; }
        }

        public class Option
        {
            public string contentText { get; set; }
            public ContentImage contentImg { get; set; }
            public bool hasLogic { get; set; }
            public string logicCardId { get; set; }
            public List<TabulationAllocation> Allocations { get; set; }

            public Option()
            {
                this.Allocations = new List<TabulationAllocation>();
            }
        }

        public int type { get; set; }
        public String contentText { get; set; }
        public List<ContentImage> contentImgs { get; set; }
        public List<Option> options { get; set; }
        public int optionRangeStartFrom { get; set; }
        public int optionRangeMiddle { get; set; }
        public int optionRangeMin { get; set; }
        public int optionRangeMax { get; set; }
        public string optionRangeMiddleLabel { get; set; }
        public string optionRangeMaxLabel { get; set; }
        public string optionRangeMinLabel { get; set; }
        public bool isAllowSkipped { get; set; }
        public bool isTickOtherAnswer { get; set; }
        public string customCommand { get; set; }
        public bool isRandomOption { get; set; }
        public int minOptions { get; set; }
        public int maxOptions { get; set; }
        public int diplayBg { get; set; }
        public string note { get; set; }
        public bool isPageBreak { get; set; }
        public int totalPointsAllocation { get; set; }

        public AssessmentCardJson()
        {

        }

    }

    public partial class AssessmentEdit : System.Web.UI.Page
    {
        private const int SURVEY_TITLE_TEXT_MAX = 100;
        private const int SURVEY_INTRODUCTION_TEXT_MAX = 250;
        private const int SURVEY_CLOSING_WORD_TEXT_MAX = 250;

        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private List<String> iconUrlList = null;
        private List<RSTopicCategory> categoryList = null;
        private List<User> searchUserList = null;
        private List<RSCard> cards = null;

        public void GetIconData()
        {
            TopicSelectIconResponse response = asc.SelectAllTopicIcons(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
            if (response.Success)
            {
                iconUrlList = response.TopicIconUrls;
            }
        }

        public void GetCategory()
        {
            RSCategorySelectAllResponse response = asc.SelectAllRSCategories(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), (int)CassandraService.Entity.RSTopicCategory.RSCategoryQueryType.Basic);
            if (response.Success)
            {
                categoryList = response.Categories;
            }
        }

        

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            try
            {
                if (Session["admin_info"] == null)
                {
                    Response.Redirect("/Logout", false);
                    return;
                }
                managerInfo = Session["admin_info"] as ManagerInfo;
                ViewState["manager_user_id"] = managerInfo.UserId;
                ViewState["company_id"] = managerInfo.CompanyId;
                ViewState["assessment_id"] = Page.RouteData.Values["AssessmentId"];

                this.Page.Form.DefaultButton = fakeSubmit.UniqueID;

                if (!IsPostBack)
                {
                    this.hfAdminUserId.Value = managerInfo.UserId;
                    this.hfCompanyId.Value = managerInfo.CompanyId;

                    imgTopic.ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/default_topic_icon.png";

                    if (Page.RouteData.Values["AssessmentId"] != null)
                    {
                        // Edit Assessment
                        AssessmentSelectResponse response = asc.SelectFullDetailAssessment(
                            ViewState["manager_user_id"].ToString(), 
                            Page.RouteData.Values["AssessmentId"].ToString());

                        this.imgTopic.ImageUrl = response.Assessment.IconUrl;

                        

                        // Populate header right column
                        this.assessmentTitle.Text = response.Assessment.Title;
                        this.assessmentIntro.Text = response.Assessment.Introduction;
                        this.assessmentVideoTitle.Text = response.Assessment.VideoTitle;
                        this.assessmentVideoDescription.Text = response.Assessment.VideoDescription;
                        this.assessmentClosing.Text = response.Assessment.ClosingWords;

                        this.videoDisplay.Src = response.Assessment.VideoUrl;

                        // Populate header left column 
                        //this.ddlCoach.Items.Add(new ListItem("Kevin Ng", "CHe3b8f93c2f3f4498ad01a697b9dc3be7"));
                        this.ddlCoach.DataSource = asc.SelectAllCoaches(ViewState["manager_user_id"].ToString()).Coaches;
                        this.ddlCoach.DataTextField = "FirstName";
                        this.ddlCoach.DataValueField = "UserId";
                        this.ddlCoach.SelectedValue = response.Assessment.Coach.UserId;
                        this.ddlCoach.DataBind();
                        this.ddlDisplayOnClient.SelectedValue = response.Assessment.IsDisplayCoachName.ToString().ToLowerInvariant();
                        this.ddlDisplayOnClient.DataBind();
                        this.ddlStatus.SelectedValue = response.Assessment.Status.ToString();
                        this.ddlStatus.DataBind();
                        this.ddlAnonymousPulse.SelectedValue = response.Assessment.IsAnonymous.ToString().ToLowerInvariant();
                        this.ddlAnonymousPulse.DataBind();
                        this.textareaCssArea.Text = response.Assessment.CssArea;

                        this.hfAssessment.Value = javascriptSerializer.Serialize(response.Assessment);
                        this.hfJsonStore.Value = this.hfAssessment.Value;
                        this.hfAssessmentUpload.Value = javascriptSerializer.Serialize(new AssessmentUpload());

                        plSearchQuestion.Visible = true;
                    }
                    else
                    {
                        // Redirect to assessment list
                        Response.Redirect("~/Assessment/List");
                    }
                }
                else
                {
                    plNewQuestion.CssClass = "container ";
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                #region Icon data
                GetIconData();
                rtIcon.DataSource = iconUrlList;
                rtIcon.DataBind();
                #endregion
                mpePop.PopupControlID = "popup_addtopicicon";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = iconUrlList[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = iconUrlList[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpePop.Hide();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lbChooseIcon") as LinkButton;
                if (btn != null)
                {
                    scriptMan.RegisterAsyncPostBackControl(btn);
                }
            }
        }


        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void rtSearchResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetFocus", "SetFocus();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtParticipantsPersonnel_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                List<User> selectedList = ViewState["selected_user"] as List<User>;
                if (selectedList == null || selectedList.Count == 0)
                {
                    Log.Error(@"ViewState[""SelectedUser""] is null.", this.Page);
                }
                else
                {
                    for (int i = 0; i < selectedList.Count; i++)
                    {
                        if (selectedList[i].UserId == Convert.ToString(e.CommandArgument))
                        {
                            selectedList.RemoveAt(i);
                            break;
                        }
                    }

                    ViewState["selected_user"] = selectedList;

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbSurvey_Click(object sender, EventArgs e)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            javascriptSerializer.MaxJsonLength = int.MaxValue;
            CassandraService.Entity.Assessment assessment = javascriptSerializer.Deserialize<CassandraService.Entity.Assessment>(hfAssessment.Value);
            AssessmentUpload assessmentUpload = javascriptSerializer.Deserialize<AssessmentUpload>(hfAssessmentUpload.Value);
            int idx = 0;

            if (assessment.VideoUrl.StartsWith("data:"))
            {
                // Upload to S3
                assessment.VideoUrl = DumpToS3(
                    ViewState["company_id"].ToString(),
                    ViewState["manager_user_id"].ToString(),
                    assessmentUpload.videoUpload,
                    ViewState["assessment_id"].ToString());

                DumpToTranscoderS3(
                    ViewState["company_id"].ToString(),
                    ViewState["manager_user_id"].ToString(),
                    assessmentUpload.videoUpload,
                    ViewState["assessment_id"].ToString());
            }

            if (assessment.VideoThumbnailUrl.StartsWith("data:"))
            {
                // Upload to S3
                assessment.VideoThumbnailUrl = DumpToS3(
                    ViewState["company_id"].ToString(),
                    ViewState["manager_user_id"].ToString(),
                    assessmentUpload.videoThumbnailUpload,
                    ViewState["assessment_id"].ToString());
            }

            string tx = ""; // hfAssessment
            try
            {
                #region Step 1. Check input data
                #region Step 1.1 Title
                //if (String.IsNullOrEmpty(tbSurveyTitle.Text.Trim()))
                //{
                //    ShowToastMsgAndHideProgressBar("You are missing the Survey title.", MessageUtility.TOAST_TYPE_ERROR);
                //    return;
                //}
                #endregion

                #region Step 1.2 Introduction
                //if (String.IsNullOrEmpty(tbSurveyIntroduction.Text.Trim()))
                //{
                //    ShowToastMsgAndHideProgressBar("You are missing the Survey introduction.", MessageUtility.TOAST_TYPE_ERROR);
                //    return;
                //}
                #endregion

                #region Step 1.3 Closing words
                //if (String.IsNullOrEmpty(tbSurveyClosingWords.Text.Trim()))
                //{
                //    ShowToastMsgAndHideProgressBar("You are missing the Survey Closing words.", MessageUtility.TOAST_TYPE_ERROR);
                //    return;
                //}
                #endregion

                #region Step 1.5. Category
                //GetCategory();
                //String categoryID = String.Empty;
                //if (cbCategory.SelectedItem == null)
                //{
                //    ShowToastMsgAndHideProgressBar("Please at least choose a category.", MessageUtility.TOAST_TYPE_ERROR);
                //    return;
                //}

                //String categoryTitle = cbCategory.SelectedItem.Text;
                //for (int i = 0; i < categoryList.Count; i++)
                //{
                //    if (categoryList[i].Title.ToLower().Equals(cbCategory.SelectedItem.Text.Trim().ToLower()))
                //    {
                //        categoryID = categoryList[i].CategoryId;
                //        break;
                //    }
                //}
                #endregion

                #region Step 1.6 Start date and End date, just for Edit mode.
                Log.Debug("managerInfo.TimeZone: " + managerInfo.TimeZone);
                #region Check survey's status and progress
                if (ViewState["survey_id"] != null) // Edit mode
                {
                    if (Convert.ToInt16((ViewState["status"])) != RSTopic.RSTopicStatus.CODE_UNLISTED)
                    {
                        DateTime dtOriStartDate = Convert.ToDateTime(ViewState["start_date"]).AddHours(-managerInfo.TimeZone);

                        Log.Debug("Check survey's status and progress || Start time:" + dtOriStartDate.ToString("yyyy/MM/dd HH:mm:ss"));
                        Log.Debug("Check survey's status and progress || Start utc time:" + dtOriStartDate.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss"));

                        int oriProgressStatus = 1;
                        if (dtOriStartDate.ToUniversalTime() < DateTime.UtcNow)
                        {
                            oriProgressStatus = 2;
                            if (ViewState["end_date"] != null)
                            {
                                DateTime dtOriEndDate = Convert.ToDateTime(ViewState["end_date"]).AddHours(-managerInfo.TimeZone);

                                Log.Debug("Check survey's status and progress || End time:" + dtOriEndDate.ToString("yyyy/MM/dd HH:mm:ss"));
                                Log.Debug("Check survey's status and progress || End utc time:" + dtOriEndDate.ToUniversalTime().ToString("yyyy/MM/dd HH:mm:ss"));

                                if (dtOriEndDate.ToUniversalTime() < DateTime.UtcNow)
                                {
                                    oriProgressStatus = 3;
                                }
                            }
                        }

                        if (Convert.ToInt16(ViewState["progress_status"]) != oriProgressStatus) // Progress status has been changed, must reload page!
                        {
                            ShowToastMsgAndHideProgressBar("Progress status has been changed", MessageUtility.TOAST_TYPE_ERROR);
                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ReloadPage", "RedirectPage('/Survey/Edit/" + ViewState["SurveyId"].ToString() + "/" + ViewState["CategoryId"].ToString() + "', 300);", true);
                            return;
                        }
                    }
                }
                #endregion

                //DateTime dtStart = new DateTime();
                //try
                //{
                //    dtStart = DateTime.ParseExact(tbScheduleStartDate.Text + " " + tbScheduleStartHour.Text.PadLeft(2, '0') + ":" + tbScheduleStartMinute.Text.PadLeft(2, '0') + " " + ddlScheduleStartMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).AddHours(-managerInfo.TimeZone).ToUniversalTime();
                //}
                //catch (Exception ex)
                //{
                //    ShowToastMsgAndHideProgressBar("Start date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                //    return;
                //}

                //DateTime? dtEnd = null;
                //if (rbScheduleEndDateCustom.Checked)
                //{
                //    try
                //    {
                //        dtEnd = DateTime.ParseExact(tbScheduleEndDate.Text + " " + tbScheduleEndHour.Text.PadLeft(2, '0') + ":" + tbScheduleEndMinute.Text.PadLeft(2, '0') + " " + ddlScheduleEndMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture).AddHours(-managerInfo.TimeZone).ToUniversalTime();
                //        if (dtEnd < DateTime.UtcNow)
                //        {
                //            ShowToastMsgAndHideProgressBar("End date should not be early now", MessageUtility.TOAST_TYPE_ERROR);
                //            return;
                //        }

                //        if (dtStart > dtEnd)
                //        {
                //            ShowToastMsgAndHideProgressBar("Your start and end dates does not tally", MessageUtility.TOAST_TYPE_ERROR);
                //            return;
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        ShowToastMsgAndHideProgressBar("End date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                //        return;
                //    }
                //}
                //else // End date = forever
                //{

                //}
                #endregion

                #endregion

                

                #region Step 2. Call api

                List<AssessmentImage> newImageBanners = new List<AssessmentImage>();
                List<AssessmentTabulation> newTabulations = new List<AssessmentTabulation>();
                List<AssessmentAnswer> newAnswers = new List<AssessmentAnswer>();

                //ViewState["assessment_id"].ToString(),
                //ViewState["manager_user_id"].ToString()
                for (idx = 0; idx < assessment.Banners.Count; idx++)
                {
                    if (assessment.Banners[idx].ImageId == null)
                    {
                        // 
                        string bannerUrl = AddToS3(ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString(), assessment.Banners[idx], ViewState["assessment_id"].ToString());
                        assessment.Banners[idx].ImageUrl = bannerUrl;
                        assessment.Banners[idx].Ordering = idx + 1;
                        //req.imageBanners.Add(new AssessmentImage
                        //{
                        //    ImageUrl = bannerUrl,
                        //    Ordering = imageBannerOrder++
                        //});
                    }
                }

                newImageBanners = assessment.Banners;
                newTabulations = assessment.Tabulations;
                newAnswers = assessment.Answers;

                if ((ddlStatus.SelectedValue == "2") && ((assessment.Tabulations.Count() == 0) || (assessment.Answers.Count() == 0) || (assessment.Cards.Count() == 0)))
                {
                    ShowToastMsgAndHideProgressBar("Assessment cannot go live if there are no tabulations, results or cards.", MessageUtility.TOAST_TYPE_ERROR);
                    Log.Error("Assessment cannot go live if there are no tabulations, results or cards.");
                    return;
                }

                string newVideoUrl = "";
                string newVideoThumbnailUrl = "";
                string oldTabulationId = "";
                Dictionary<string, AssessmentTabulation> tabulationMap = new Dictionary<string, AssessmentTabulation>();

                foreach (AssessmentTabulation tab in assessment.Tabulations)
                {
                    oldTabulationId = tab.TabulationId;
                    if (tab.TabulationId.StartsWith("tabulation_", StringComparison.Ordinal))
                    {
                        tab.TabulationId = UUIDGenerator.GenerateUniqueIDForAssessmentTabulation();
                    }
                    tabulationMap.Add(oldTabulationId, tab);

                    foreach (CassandraService.Entity.ContentImage img in tab.ImagesUpload)
                    {
                        string bannerUrl = AddToS3(ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString(), img, ViewState["assessment_id"].ToString());
                        tab.Images.Add(new AssessmentImage
                        {
                            ImageId = UUIDGenerator.GenerateUniqueIDForRSImage(),
                            ImageUrl = bannerUrl
                        });
                    }

                    // Clean "fake" images 
                    tab.Images = tab.Images.Where(r => r.ImageId != null).ToList();
                }

                
                foreach (AssessmentAnswer ans in assessment.Answers)
                {
                    if (ans.AnswerId.StartsWith("result_", StringComparison.InvariantCultureIgnoreCase))
                    {
                        ans.AnswerId = UUIDGenerator.GenerateUniqueIDForAssessmentAnswer();
                    }

                    //foreach (string tabulationId in ans.TabulationIdList)
                    //{
                    //    ans.Tabulations.Add(tabulationMap[tabulationId]);
                    //}

                    for (int tabIdx = 0; tabIdx < ans.Tabulations.Count(); tabIdx++)
                    {
                        if (ans.Tabulations[tabIdx].TabulationId.StartsWith("tabulation_", StringComparison.InvariantCultureIgnoreCase))
                        {
                            ans.Tabulations[tabIdx] = tabulationMap[ans.Tabulations[tabIdx].TabulationId];
                        }
                    }
                    
                }

                AssessmentUpdateResponse response = asc.UpdateAssessment(
                    ViewState["assessment_id"].ToString(),
                    ViewState["manager_user_id"].ToString(),
                    assessmentTitle.Text,
                    assessmentIntro.Text,
                    assessmentClosing.Text,
                    imgTopic.ImageUrl,
                    ddlCoach.SelectedValue,
                    ddlCoach.SelectedItem.Text,
                    Convert.ToBoolean(ddlDisplayOnClient.SelectedValue),
                    Convert.ToBoolean(ddlAnonymousPulse.SelectedValue),
                    Convert.ToInt16(ddlStatus.SelectedValue),
                    newImageBanners,
                    newTabulations,
                    newAnswers,
                    assessment.VideoUrl,
                    assessmentVideoTitle.Text,
                    assessmentVideoDescription.Text,
                    assessment.VideoThumbnailUrl,
                    textareaCssArea.Text);

                if (response.Success)
                {
                    ShowToastMsgAndHideProgressBar("Assessment have been updated.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("Assessment have not been updated.", MessageUtility.TOAST_TYPE_ERROR);
                    Log.Error("AssessmentUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }

                #region comment out
                if (ViewState["survey_id"] == null || String.IsNullOrEmpty(ViewState["survey_id"].ToString())) // Create
                {
                    //RSTopicCreateResponse response = asc.CreateRSTopic(ViewState["manager_user_id"].ToString(),
                    //                                                    ViewState["company_id"].ToString(),
                    //                                                    tbSurveyTitle.Text.Trim(),
                    //                                                    tbSurveyIntroduction.Text.Trim(),
                    //                                                    tbSurveyClosingWords.Text.Trim(),
                    //                                                    categoryID, categoryTitle,
                    //                                                    imgTopic.ImageUrl,
                    //                                                    Convert.ToInt16(ddlStatus.SelectedValue),
                    //                                                    targetedDepartmentIds,
                    //                                                    targetedUserIds,
                    //                                                    dtStart,
                    //                                                    dtEnd,
                    //                                                    false
                    //                                                    );

                    //if (response.Success)
                    //{
                    //    ShowToastMsgAndHideProgressBar("Survey have been created.", MessageUtility.TOAST_TYPE_INFO);
                    //    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/Survey/Edit/" + response.Topic.TopicId + "/" + response.Topic.RSCategory.CategoryId + "',300);", true);
                    //}
                    //else
                    //{
                    //    ShowToastMsgAndHideProgressBar("Survey have not been created.", MessageUtility.TOAST_TYPE_ERROR);
                    //    Log.Error("RSTopicCreateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    //}
                }
                else // Edit
                {
                    //ViewState["manager_user_id"] = managerInfo.UserId;
                    //ViewState["company_id"] = managerInfo.CompanyId;

                    //RSTopicUpdateResponse response = asc.UpdateRSTopic(ViewState["survey_id"].ToString(),
                    //                                                   ViewState["manager_user_id"].ToString(),
                    //                                                   ViewState["company_id"].ToString(),
                    //                                                   tbSurveyTitle.Text.Trim(), tbSurveyIntroduction.Text.Trim(),
                    //                                                   tbSurveyClosingWords.Text.Trim(),
                    //                                                   categoryID,
                    //                                                   categoryTitle,
                    //                                                   imgTopic.ImageUrl,
                    //                                                   Convert.ToInt16(ddlStatus.SelectedValue),
                    //                                                   targetedDepartmentIds,
                    //                                                   targetedUserIds,
                    //                                                   dtStart,
                    //                                                   dtEnd,
                    //                                                   false
                    //                                                    );
                    //if (response.Success)
                    //{
                    //    ShowToastMsgAndHideProgressBar("Survey have been updated.", MessageUtility.TOAST_TYPE_INFO);
                    //}
                    //else
                    //{
                    //    ShowToastMsgAndHideProgressBar("Survey have not been updated.", MessageUtility.TOAST_TYPE_ERROR);
                    //    Log.Error("RSTopicUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    //}
                }
                #endregion comment out
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //RSTopicSelectResponse response = asc.SelectFullDetailRSTopic(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), ViewState["survey_id"].ToString(), ViewState["category_id"].ToString());
                //if (response.Success)
                //{
                //    if (response.Topic.Cards.Count == 0)
                //    {
                //        ltlNewQuestionPaging.Text = "P1";
                //        ltlNewCardOrdering.Text = "Q1";
                //    }
                //    else
                //    {
                //        if (response.Topic.Cards[response.Topic.Cards.Count - 1].HasPageBreak)
                //        {
                //            ltlNewQuestionPaging.Text = "P" + (response.Topic.Cards[response.Topic.Cards.Count - 1].Paging + 1);
                //        }
                //        else
                //        {
                //            ltlNewQuestionPaging.Text = "P" + response.Topic.Cards[response.Topic.Cards.Count - 1].Paging;
                //        }
                //        ltlNewCardOrdering.Text = "Q" + (response.Topic.Cards[response.Topic.Cards.Count - 1].Ordering + 1);
                //    }
                plNewQuestion.Visible = true;
                plNewQuestion.CssClass = "container animated fadeInDown";
                SetFocus("lbNewQuestionLocation");
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "resetNewCard", "resetNewCard();", true);
                //}
                //else
                //{
                //    Log.Error("RSTopicSelectResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                //}
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbNewQuestionCancel_Click(object sender, EventArgs e)
        {
            try
            {
                plNewQuestion.CssClass = "container animated fadeOut";
                plNewQuestion.Visible = false;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbNewQuestionSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. Get input(Json) from fornt-end
                string assessmentId = string.Empty;
                AssessmentCardJson sc = JsonConvert.DeserializeObject<AssessmentCardJson>(hfNewCardJson.Value);
                //String cardId = PrefixId.AssessmentCard+ Guid.NewGuid().ToString().Replace("-", "");
                string cardId = UUIDGenerator.GenerateUniqueIDForAssessmentCard();
                #endregion

                #region Step 2. Check type of card
                Boolean hasContentImg = false;
                List<AssessmentImage> contentImgs = new List<AssessmentImage>();
                Boolean isAllowMultiLine = false; // unused
                List<RSOption> options = new List<RSOption>();


                if (Page.RouteData.Values["AssessmentId"] != null)
                {
                    assessmentId = Page.RouteData.Values["AssessmentId"].ToString();
                }

                if (sc.contentImgs.Count > 0)
                {
                    // TODO: Convert contentImgs into List<AssessmentImage> assessmentImages
                }

                //assessmentCardOption.Allocations.Add(new OptionTabulationAllocation)
                //AssessmentCardJson.Option op = new AssessmentCardJson.Option();
                //op.contentImg = null;
                //op.contentText = sc.contentText;
                //sc.options.Add(new AssessmentCardJson.Option { })

                List<AssessmentImage> assessmentImages = new List<AssessmentImage>();
                List<AssessmentCardOption> assessmentCardOptions = new List<AssessmentCardOption>();


                #region Step 2.1. Upload content (question) images of card to S3.(/cocadre-{CompanyId}/surveys/{SurveyId}/{CardId}/{index}.{imgFormat}) // contentImgs of card
                if (sc.contentImgs != null && sc.contentImgs.Count > 0)
                {
                    // for S3
                    String bucketName = "cocadre-marketplace";
                    String folderName = "assessment/" + assessmentId + "/" + cardId;
                    IAmazonS3 s3Client = S3Utility.GetIAmazonS3(ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString());
                    S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                    String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
                    int imgWidth = 0, imgHeight = 0;

                    using (s3Client)
                    {
                        string filenamePrefix = "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                        for (int i = 0; i < sc.contentImgs.Count; i++)
                        {
                            // Grab image width and height
                            imgWidth = Convert.ToInt16(sc.contentImgs[i].width);
                            imgHeight = Convert.ToInt16(sc.contentImgs[i].height);
                            imgBase64String = sc.contentImgs[i].base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                            imgFormat = sc.contentImgs[i].extension.Replace("image/", "");

                            #region Upload original image
                            // Upload original image
                            Dictionary<String, String> metadatas = new Dictionary<string, string>();
                            byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                            System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                            String fileName = (i + 1) + filenamePrefix + "_original" + "." + imgFormat.ToLower();
                            metadatas.Clear();
                            metadatas.Add("Width", Convert.ToString(imgWidth));
                            metadatas.Add("Height", Convert.ToString(imgHeight));
                            S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            #endregion Upload original image

                            #region Upload large (if original image is 1920 * 1920)
                            // Upload large (if original image is 1920 * 1920)
                            System.Drawing.Image newImage;
                            if (imgWidth > 1920 || imgHeight > 1920)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                                fileName = (i + 1) + filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (i + 1) + filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload large (if original image is 1920 * 1920)

                            #region Upload medium (if original image is 1080 * 1080)
                            // Upload medium (if original image is 1080 * 1080)
                            if (imgWidth > 1080 || imgHeight > 1080)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                                fileName = (i + 1) + filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (i + 1) + filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload medium (if original image is 1080 * 1080)

                            #region Upload small (if original image is 540 * 540)
                            // Upload small (if original image is 540 * 540)
                            if (imgWidth > 540 || imgHeight > 540)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                                fileName = (i + 1) + filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (i + 1) + filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload small (if original image is 540 * 540)

                            // Get profile image url
                            imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + (i + 1) + filenamePrefix + "_original." + imgFormat.ToLower();

                            AssessmentImage s3Image = new AssessmentImage();
                            s3Image.ImageUrl = imageUrl;
                            //s3Image.ImageId = string.Empty;
                            s3Image.ImageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                            s3Image.Ordering = i + 1;
                            contentImgs.Add(s3Image);
                            assessmentImages.Add(s3Image);
                        }
                    }
                    hasContentImg = true;
                }
                #endregion

                #region Step 2.2 CardType (comment out)
                //if (sc.type == 1 || sc.type == 2) // Select one || Multi choice
                //{
                //    #region 2.3 Options
                //    for (int i = 0; i < sc.options.Count; i++)
                //    {
                //        RSOption option = new RSOption();
                //        option.Content = sc.options[i].contentText;
                //        option.HasImage = false;
                //        option.Ordering = i + 1;
                //        option.OptionId = PrefixId.RSOption + Guid.NewGuid().ToString().Replace("-", "");
                //        option.Images = new List<RSImage>();

                //        #region Step 2.4 (/cocadre-{CompanyId}/surveys/{SurveyId}/{CardId}/{optionId}/{index}_{timestamp}_{original}.{imgFormat}) // contentImg of option
                //        if (!String.IsNullOrEmpty(sc.options[i].contentImg.base64))
                //        {
                //            // 以後要改寫成 multi images of one option !。目前一個 option 只有一張圖。
                //            // for S3
                //            String bucketName = "cocadre-" + ViewState["company_id"].ToString().ToLowerInvariant();
                //            String folderName = "surveys/" + ViewState["survey_id"].ToString() + "/" + cardId + "/" + option.OptionId;
                //            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString());
                //            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                //            String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
                //            int imgWidth = 0, imgHeight = 0;

                //            using (s3Client)
                //            {
                //                //for (int k = 0; k < sc.options[i].contentImgs.count ; k++) // 以後要改寫成 multi images of one option !。目前一個 option 只有一張圖。
                //                //{
                //                //    
                //                //}

                //                string filenamePrefix = "1_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                //                // Grab image width and height
                //                imgWidth = Convert.ToInt16(sc.options[i].contentImg.width);
                //                imgHeight = Convert.ToInt16(sc.options[i].contentImg.height);
                //                imgBase64String = sc.options[i].contentImg.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                //                imgFormat = sc.options[i].contentImg.extension.Replace("image/", "");

                //                #region Upload original image
                //                // Upload original image
                //                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                //                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                //                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                //                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                //                String fileName = filenamePrefix + "_original" + "." + imgFormat.ToLower();
                //                metadatas.Clear();
                //                metadatas.Add("Width", Convert.ToString(imgWidth));
                //                metadatas.Add("Height", Convert.ToString(imgHeight));
                //                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                //                #endregion Upload original image

                //                #region Upload large (if original image is 1920 * 1920)
                //                // Upload large (if original image is 1920 * 1920)
                //                System.Drawing.Image newImage;
                //                if (imgWidth > 1920 || imgHeight > 1920)
                //                {
                //                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                //                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                //                    metadatas.Clear();
                //                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                //                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                //                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                //                }
                //                else
                //                {
                //                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                //                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                //                }
                //                #endregion Upload large (if original image is 1920 * 1920)

                //                #region Upload medium (if original image is 1080 * 1080)
                //                // Upload medium (if original image is 1080 * 1080)
                //                if (imgWidth > 1080 || imgHeight > 1080)
                //                {
                //                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                //                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                //                    metadatas.Clear();
                //                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                //                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                //                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                //                }
                //                else
                //                {
                //                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                //                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                //                }
                //                #endregion Upload medium (if original image is 1080 * 1080)

                //                #region Upload small (if original image is 540 * 540)
                //                // Upload small (if original image is 540 * 540)
                //                if (imgWidth > 540 || imgHeight > 540)
                //                {
                //                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                //                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                //                    metadatas.Clear();
                //                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                //                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                //                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                //                }
                //                else
                //                {
                //                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                //                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                //                }
                //                #endregion Upload small (if original image is 540 * 540)

                //                // Get profile image url
                //                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + imgFormat.ToLower();

                //                RSImage s3Image = new RSImage();
                //                s3Image.ImageUrl = imageUrl;
                //                s3Image.ImageId = string.Empty;
                //                s3Image.Ordering = 1;
                //                option.Images.Add(s3Image);
                //            }

                //            option.HasImage = true;
                //        }
                //        #endregion
                //        options.Add(option);
                //    }
                //    #endregion
                //}
                //else if (sc.type == 3 || sc.type == 6) // Text || Instructions
                //{
                //    sc.isAllowSkipped = false;
                //    sc.isTickOtherAnswer = false;
                //    sc.customCommand = null;
                //    sc.isRandomOption = false;
                //    options = null;
                //    sc.optionRangeMax = 0;
                //    sc.optionRangeMin = 0;
                //}
                //else if (sc.type == 4) // Number range 
                //{
                //    sc.isTickOtherAnswer = false;
                //    sc.customCommand = null;
                //    sc.isRandomOption = false;
                //    options = null;

                //}
                //else if (sc.type == 5) // Drop list 
                //{
                //    #region Options
                //    for (int i = 0; i < sc.options.Count; i++)
                //    {
                //        RSOption option = new RSOption();
                //        option.Content = sc.options[i].contentText;
                //        option.HasImage = false;
                //        option.Ordering = i + 1;
                //        option.OptionId = PrefixId.RSOption + Guid.NewGuid().ToString().Replace("-", "");
                //        option.Images = new List<RSImage>();
                //        options.Add(option);
                //    }
                //    #endregion
                //}
                //else
                //{
                //    // nothing
                //}
                #endregion

                #endregion

                int optionIdx = 0;
                foreach (AssessmentCardJson.Option jsonOption in sc.options)
                {
                    AssessmentCardOption assessmentCardOption = new AssessmentCardOption();
                    assessmentCardOption.OptionId = UUIDGenerator.GenerateUniqueIDForAssessmentCardOption();
                    assessmentCardOption.HasImage = false;
                    assessmentCardOption.Images = new List<AssessmentImage>();
                    assessmentCardOption.Allocations = new List<OptionTabulationAllocation>();
                    assessmentCardOption.Ordering = optionIdx;

                    if ((jsonOption.contentImg != null) && (jsonOption.contentImg.base64.Length > 0))
                    {
                        #region Upload content (option) images of card to S3.(/cocadre-{CompanyId}/assessment/{AssessmentId}/{CardId}/{index}.{imgFormat}) // contentImgs of card
                        // for S3
                        String bucketName = "cocadre-marketplace";
                        String folderName = "assessment/" + assessmentId + "/" + cardId;
                        IAmazonS3 s3Client = S3Utility.GetIAmazonS3(ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString());
                        S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

                        String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
                        int imgWidth = 0, imgHeight = 0;

                        using (s3Client)
                        {
                            string filenamePrefix = "_" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");

                            // Grab image width and height
                            imgWidth = Convert.ToInt16(jsonOption.contentImg.width);
                            imgHeight = Convert.ToInt16(jsonOption.contentImg.height);
                            imgBase64String = jsonOption.contentImg.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "");
                            imgFormat = jsonOption.contentImg.extension.Replace("image/", "");

                            #region Upload original image
                            // Upload original image
                            Dictionary<String, String> metadatas = new Dictionary<string, string>();
                            byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                            System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                            System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                            String fileName = (optionIdx + 1) + filenamePrefix + "_original" + "." + imgFormat.ToLower();
                            metadatas.Clear();
                            metadatas.Add("Width", Convert.ToString(imgWidth));
                            metadatas.Add("Height", Convert.ToString(imgHeight));
                            S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            #endregion Upload original image

                            #region Upload large (if original image is 1920 * 1920)
                            // Upload large (if original image is 1920 * 1920)
                            System.Drawing.Image newImage;
                            if (imgWidth > 1920 || imgHeight > 1920)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                                fileName = (optionIdx + 1) + filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (optionIdx + 1) + filenamePrefix + "_large" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload large (if original image is 1920 * 1920)

                            #region Upload medium (if original image is 1080 * 1080)
                            // Upload medium (if original image is 1080 * 1080)
                            if (imgWidth > 1080 || imgHeight > 1080)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                                fileName = (optionIdx + 1) + filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (optionIdx + 1) + filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload medium (if original image is 1080 * 1080)

                            #region Upload small (if original image is 540 * 540)
                            // Upload small (if original image is 540 * 540)
                            if (imgWidth > 540 || imgHeight > 540)
                            {
                                newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                                fileName = (optionIdx + 1) + filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                metadatas.Clear();
                                metadatas.Add("Width", Convert.ToString(newImage.Width));
                                metadatas.Add("Height", Convert.ToString(newImage.Height));
                                S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                            }
                            else
                            {
                                fileName = (optionIdx + 1) + filenamePrefix + "_small" + "." + imgFormat.ToLower();
                                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                            }
                            #endregion Upload small (if original image is 540 * 540)

                            // Get profile image url
                            imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + (optionIdx + 1) + filenamePrefix + "_original." + imgFormat.ToLower();

                            AssessmentImage s3Image = new AssessmentImage();
                            s3Image.ImageUrl = imageUrl;
                            s3Image.ImageId = string.Empty;
                            s3Image.ImageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                            s3Image.Ordering = optionIdx + 1;
                            contentImgs.Add(s3Image);
                            assessmentCardOption.Images.Add(s3Image);

                        }
                        //hasContentImg = true;
                        assessmentCardOption.HasImage = true;
                            
                        #endregion
                    }

                    foreach (AssessmentCardJson.TabulationAllocation tabAllocation in jsonOption.Allocations)
                    {
                        OptionTabulationAllocation alloc = new OptionTabulationAllocation();
                        alloc.CardId = cardId;
                        alloc.OptionId = assessmentCardOption.OptionId;
                        //alloc.Ordering
                        //alloc.Tabulation
                        alloc.TabulationId = tabAllocation.tabulationId;
                        alloc.Value = tabAllocation.multiplier;
                        alloc.ValueOperator = 1;
                        assessmentCardOption.Allocations.Add(alloc);
                    }

                    assessmentCardOption.Content = jsonOption.contentText;
                    assessmentCardOption.IsSelected = false;
                    assessmentCardOption.Ordering = (optionIdx + 1);
                    //sc.totalPointsAllocation = 10;// TODO: To discuss with Kevin when question type is non-accumulation
                    assessmentCardOption.PointAllocated = sc.totalPointsAllocation;
                    assessmentCardOptions.Add(assessmentCardOption);
                    optionIdx++;
                    
                }
                
                

                #region comment out
                //if (sc.options.Count > 0)
                //{
                //    // TODO: Convert contentImgs into List<AssessmentCardOption> assessmentCardOptions
                //    foreach (AssessmentCardJson.Option op in sc.options)
                //    {
                //        assCardOption.HasImage = false; //op.contentImg
                //        assCardOption.Content = op.contentText;
                //        foreach (AssessmentCardJson.TabulationAllocation alloc in op.tabAllocation)
                //        {
                //            OptionTabulationAllocation tabAlloc = new OptionTabulationAllocation();
                //            tabAlloc.CardId = cardId;
                //            //tabAlloc.OptionId 
                //            //tabAlloc.Ordering
                //            tabAlloc.Tabulation = new AssessmentTabulation
                //            {
                //            };
                //            tabAlloc.TabulationId = UUIDGenerator.GenerateUniqueIDForAssessmentTabulation();
                //            tabAlloc.TabulationOperator = 1;
                //            tabAlloc.Value = 1;
                //            tabAlloc.ValueOperator = 1;
                //            assCardOption.Allocations.Add(tabAlloc);
                //        }
                //        //assCardOption.Allocations = op.tabAllocation;
                //        // op.hasLogic
                //        // op.logicCardId
                //        assessmentCardOptions.Add(new AssessmentCardOption
                //        {
                //        });
                //    }
                //}
                #endregion comment out

                AssessmentCreateCardResponse response = asc.CreateAssessmentCard(
                    cardId,
                    sc.type,
                    sc.contentText,
                    assessmentImages,
                    sc.isAllowSkipped,
                    sc.isRandomOption,
                    sc.isPageBreak,
                    sc.diplayBg,
                    sc.note,
                    assessmentId,
                    ViewState["manager_user_id"].ToString(),
                    assessmentCardOptions,
                    sc.totalPointsAllocation,
                    sc.minOptions,
                    sc.maxOptions,
                    sc.optionRangeStartFrom,
                    sc.optionRangeMax,
                    sc.optionRangeMaxLabel,
                    sc.optionRangeMiddle,
                    sc.optionRangeMiddleLabel,
                    sc.optionRangeMin,
                    sc.optionRangeMinLabel);


                if (response.Success)
                {
                    plNewQuestion.CssClass = "container animated fadeInDown";
                    ShowToastMsgAndHideProgressBar("Assessment question created", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "resetNewCard", "resetNewCard();", true);

                    AssessmentSelectResponse responseQQ = asc.SelectFullDetailAssessment(
                            ViewState["manager_user_id"].ToString(),
                            Page.RouteData.Values["AssessmentId"].ToString());

                    if (responseQQ.Success)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        string resultJson = javascriptSerializer.Serialize(responseQQ.Assessment);
                        this.hfJsonStore.Value = resultJson;
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "refreshListDisplay", "fetchAndRefreshListDisplay();", true);

                        // Check last card
                        AssessmentCard card = responseQQ.Assessment.Cards.LastOrDefault();
                        if (card != null)
                        {
                            int page_number;
                            int question_number;

                            if (card.HasPageBreak)
                            {
                                page_number = card.Paging + 1;

                            }
                            else
                            {
                                page_number = card.Paging;

                            }
                            question_number = card.Ordering + 1;

                            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "updateNewCardNumber", "updateNewCardNumber(" + page_number.ToString() + ", " + question_number.ToString() + ");", true);
                        }
                    }
                }
                else
                {
                    Log.Error("RSCardCreateResponse.Success is flase. ErrorMessage: " + response.ErrorMessage);
                    ShowToastMsgAndHideProgressBar("Creation failed", MessageUtility.TOAST_TYPE_ERROR);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtCards_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Literal ltlMinCardPaging = e.Item.FindControl("ltlMinCardPaging") as Literal;
                    Literal ltlMinCardOrdering = e.Item.FindControl("ltlMinCardOrdering") as Literal;
                    LinkButton lbMinShowCard = e.Item.FindControl("lbMinShowCard") as LinkButton;
                    System.Web.UI.WebControls.Image imgMinCardContentImg = e.Item.FindControl("imgMinCardContentImg") as System.Web.UI.WebControls.Image;
                    Literal ltlMinCardContents = e.Item.FindControl("ltlMinCardContents") as Literal;
                    Literal ltlMinCardType = e.Item.FindControl("ltlMinCardType") as Literal;
                    Literal ltlMinCardLogicTo = e.Item.FindControl("ltlMinCardLogicTo") as Literal;
                    Literal ltlMinCardLogicFrom = e.Item.FindControl("ltlMinCardLogicFrom") as Literal;

                    ltlMinCardPaging.Text = "P" + cards[e.Item.ItemIndex].Paging;
                    ltlMinCardOrdering.Text = "Q" + cards[e.Item.ItemIndex].Ordering;
                    lbMinShowCard.CommandArgument = cards[e.Item.ItemIndex].CardId;
                    if (cards[e.Item.ItemIndex].CardImages.Count > 0)
                    {
                        imgMinCardContentImg.ImageUrl = cards[e.Item.ItemIndex].CardImages[0].ImageUrl;
                    }
                    else
                    {
                        imgMinCardContentImg.Visible = false;
                    }
                    ltlMinCardContents.Text = cards[e.Item.ItemIndex].Content;

                    ltlMinCardType.Text = cards[e.Item.ItemIndex].Type + ""; // change image
                    ltlMinCardLogicTo.Text = "To: " + cards[e.Item.ItemIndex].HasToLogic; // change image
                    ltlMinCardLogicFrom.Text = "From: " + cards[e.Item.ItemIndex].HasFromLogic; // change image



                    Literal ltlCardId = e.Item.FindControl("ltlCardId") as Literal;
                    ltlCardId.Text = cards[e.Item.ItemIndex].CardId;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtCards_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("ShowCard"))
                {
                    RSCardSelectResponse response = asc.SelectFullDetailCard(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), e.CommandArgument.ToString(), ViewState["survey_id"].ToString());
                    if (response.Success)
                    {
                        Panel plFullContent = e.Item.FindControl("plFullContent") as Panel;
                        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "toggle", "$('#" + plFullContent.ClientID + "').toggle();", true);

                        Literal ltlMaxCardContentText = e.Item.FindControl("ltlMaxCardContentText") as Literal;
                        ltlMaxCardContentText.Text = response.Card.Content;
                    }
                    else
                    {

                    }
                }

                if (e.CommandName.Equals("DeleteQuestion", StringComparison.InvariantCultureIgnoreCase))
                {

                    RSCardUpdateResponse response = asc.DeleteCard(
                        ViewState["survey_id"].ToString(),
                        ViewState["category_id"].ToString(),
                        //"cardId", 
                        e.CommandArgument.ToString(),
                        managerInfo.UserId,
                        managerInfo.CompanyId);

                    if (response.Success)
                    {
                        RSTopicSelectResponse rsTopicSelectResponse = asc.SelectFullDetailRSTopic(
                            ViewState["manager_user_id"].ToString(),
                            ViewState["company_id"].ToString(),
                            Page.RouteData.Values["SurveyId"].ToString(),
                            Page.RouteData.Values["CategoryId"].ToString());

                        if (rsTopicSelectResponse.Success)
                        {
                            //adCards.DataSource = rsTopicSelectResponse.Topic.Cards;
                            //adCards.DataBind();
                        }
                    }
                    else
                    {
                        // TODO: show toast that delete failed

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbPreview_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.RouteData.Values["AssessmentId"] != null)
                {
                    // Edit Assessment
                    AssessmentSelectResponse response = asc.PreviewAssessment(
                        ViewState["manager_user_id"].ToString(),
                        Page.RouteData.Values["AssessmentId"].ToString());

                    if (response.Success)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                        string resultJson = javascriptSerializer.Serialize(response);
                        this.hfLiveSurveyPreviewJson.Value = resultJson;

                        mpePop.PopupControlID = "popup_livesurveypreview";
                        mpePop.Show();
                    }
                    else
                    {
                        // Show toast notification indicating that there is error.
                        MessageUtility.ShowToast(this.Page, "Error retrieving data for live preview. " + response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                    }
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "display_preview_intro", "display_preview_intro();", true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void lbReports_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(string.Format("/Survey/Analytic/{0}", ViewState["survey_id"].ToString() + "/" + ViewState["category_id"].ToString()));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void fakeSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                Log.Debug("hehe");
            }
            catch (Exception ex)
            {

            }
        }

        private string AddToS3(string company_id, string manager_user_id, CassandraService.Entity.ContentImage cardImage, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            // for S3
            String bucketName = "cocadre-marketplace";
            String folderName = string.Format("assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            String imgBase64String = String.Empty, imgFormat = String.Empty, imageUrl = String.Empty;
            int imgWidth = 0, imgHeight = 0;
            string filenamePrefix = string.Empty;

            using (s3Client)
            {
                // Grab image width and height

                imgWidth = Convert.ToInt16(cardImage.width);
                imgHeight = Convert.ToInt16(cardImage.height);
                imgBase64String = cardImage.base64.Replace("data:image/png;base64,", "").Replace("data:image/jpg;base64,", "").Replace("data:image/jpeg;base64,", "").Replace("data:image/bmp;base64,", "").Replace("data:image/gif;base64,", "");
                imgFormat = cardImage.extension.Replace("image/", "");
                filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] imgBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(imgBytes);
                System.Drawing.Image originImage = System.Drawing.Image.FromStream(ms);
                String fileName = filenamePrefix + "_original" + "." + imgFormat.ToLower();
                metadatas.Clear();
                metadatas.Add("Width", Convert.ToString(imgWidth));
                metadatas.Add("Height", Convert.ToString(imgHeight));
                S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                #region Upload large (if original image is 1920 * 1920)
                // Upload large (if original image is 1920 * 1920)
                System.Drawing.Image newImage;
                if (imgWidth > 1920 || imgHeight > 1920)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1920, 1920), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_large" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload large (if original image is 1920 * 1920)

                #region Upload medium (if original image is 1080 * 1080)
                // Upload medium (if original image is 1080 * 1080)
                if (imgWidth > 1080 || imgHeight > 1080)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(1080, 1080), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_medium" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload medium (if original image is 1080 * 1080)

                #region Upload small (if original image is 540 * 540)
                // Upload small (if original image is 540 * 540)
                if (imgWidth > 540 || imgHeight > 540)
                {
                    newImage = ImageUtility.ResizeImage(originImage, new System.Drawing.Size(540, 540), ImageUtility.GetImageFormat(originImage));
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    metadatas.Clear();
                    metadatas.Add("Width", Convert.ToString(newImage.Width));
                    metadatas.Add("Height", Convert.ToString(newImage.Height));
                    S3Utility.UploadImageToS3(s3Client, ImageUtility.ImageToByteArray(newImage, ImageUtility.GetImageFormat(originImage)), bucketName, folderName, fileName, metadatas);
                }
                else
                {
                    fileName = filenamePrefix + "_small" + "." + imgFormat.ToLower();
                    S3Utility.UploadImageToS3(s3Client, imgBytes, bucketName, folderName, fileName, metadatas);
                }
                #endregion Upload small (if original image is 540 * 540)

                imageUrl = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + imgFormat.ToLower();

                return imageUrl;

            }

        }

        private string DumpToS3(string company_id, string manager_user_id, CassandraService.Entity.Base64Content content, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            // for S3
            String bucketName = "cocadre-marketplace";
            String folderName = string.Format("assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            using (s3Client)
            {
                string imgBase64String = content.base64.Remove(0, content.base64.IndexOf(",") + 1);
                string filename = System.IO.Path.GetFileName(content.filename);
                string extension = System.IO.Path.GetExtension(content.filename).Remove(0, 1);
                string filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] contentBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                String fileName = filenamePrefix + "_original" + "." + extension.ToLower();
                S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                string s3url = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + extension.ToLower();

                return s3url;

            }
        }

        private string DumpToTranscoderS3(string company_id, string manager_user_id, CassandraService.Entity.Base64Content content, string assessmentId)
        {
            System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            // cocadre-elastic-transcoder-input/dev/cocadre-marketplace/assessment/{AssessmentID}/{video}.mp4

            // for S3
            String bucketName = "cocadre-elastic-transcoder-input";
            String folderName = string.Format("dev/cocadre-marketplace/assessment/{0}", assessmentId);

            IAmazonS3 s3Client = S3Utility.GetIAmazonS3(company_id, manager_user_id);
            S3Utility.CheckFolderOnS3(s3Client, bucketName, folderName);

            using (s3Client)
            {
                string imgBase64String = content.base64.Remove(0, content.base64.IndexOf(",") + 1);
                string filename = System.IO.Path.GetFileName(content.filename);
                string extension = System.IO.Path.GetExtension(content.filename).Remove(0, 1);
                string filenamePrefix = string.Format("{0}_{1}", manager_user_id, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff"));

                #region Upload original image
                // Upload original image
                Dictionary<String, String> metadatas = new Dictionary<string, string>();
                byte[] contentBytes = Convert.FromBase64String(imgBase64String);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(contentBytes);
                String fileName = filenamePrefix + "_original" + "." + extension.ToLower();
                S3Utility.UploadImageToS3(s3Client, contentBytes, bucketName, folderName, fileName, metadatas);
                #endregion Upload original image

                string s3url = "https://s3-" + s3Client.GetBucketLocation("cocadre/" + folderName).Location.Value + ".amazonaws.com/" + bucketName + "/" + folderName + "/" + filenamePrefix + "_original." + extension.ToLower();

                return s3url;

            }
        }
    }
}