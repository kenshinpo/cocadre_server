﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AssessmentCreate.aspx.cs" Inherits="AdminWebsite.Assessment.AssessmentCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <style type="text/css">
        .wrapper,
        .mainContent,
        .sideBar {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .wrapper {
            font-size: 1em;
            padding: 1.5em;
            width: 100%;
        }

        .mainContent,
        .sideBar {
            display: inline-block;
            vertical-align: top;
            width: 100%;
        }

        .duration_tip, .anonymous_tip {
            position: absolute;
            /*top: 0px;*/
            /*right: 0px;*/
            left:90px;
            opacity: 0;
            display: block !important;
            background: #ffffff;
            width: 275px;
            border: 1px solid #ccc;
            padding: 10px;
            z-index: 1000;
            box-shadow: 0px 0px 10px #cccccc;
            transition: all 0.3s ease;
            -webkit-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
        }

        .form__row.duration__tip .icon-img:hover + .duration_tip {
            opacity: 1;
        }

        .form__row.anonymous__tip .icon-img:hover + .anonymous_tip {
            opacity: 1;
        }

        .img-container {
            display: inline-block;
            max-width: 100px;
            max-height: 100px;
            vertical-align: top;
        }

        .remove-img-banner {
            background: rgba(0, 0, 0, 0.6);
            padding-top: 5px;
            padding-left: 10px;
            width: 30px;
            height: 30px;
            color: white;
            position: absolute;
            cursor: pointer;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-align-content: center;
            align-content: center;

        }

        @media (min-width: 700px) {

            .mainContent {
                /*margin-right: 5%;*/
                width: 75%;
            }

            .sideBar {
                width: 20%;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfAssessmentJson" />

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/Assessment/List" style="color: #000;">Assessment <span>console</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlActionName" runat="server" Text="Create an assessment" />
        </div>
        <div class="appbar__action" style="width: 500px;">
            <%--<asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn primary" >Create Assessment</asp:LinkButton>
            <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn secondary" >Cancel</asp:LinkButton>
            <asp:LinkButton ID="lbPreview" runat="server" CssClass="btn secondary" OnClick="lbPreview_Click">Live Survey Preview</asp:LinkButton>--%>
        </div>
    </div>
    <!-- / App Bar -->

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>

            <!-- Reveal: Add Survey Icon -->
            <asp:Panel ID="popup_addtopicicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Survey Icon</h1>
                <div class="topicicons">
                    <p>Select from list</p>
                    <asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand" OnItemCreated="rtIcon_ItemCreated">
                        <HeaderTemplate>
                            <div class="topicicons__list">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <label class="topicicons__icon">
                                <asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
                                    <asp:Image ID="imgChooseIcon" runat="server" />
                                </asp:LinkButton>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbIconCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Add Survey Icon -->

            <!-- Active: Hide, Delete Survey -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="Literal1" runat="server" />Assessment
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgActionSurvey" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionSurveyTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" />
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpePop" BehaviorID="mpe" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_addtopicicon"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="data">
        <div class="data__content" onscroll="sticky_div();">

            <div class="container">
                <div class="card add-assessment">
                    <asp:UpdatePanel ID="upEditSurvey" runat="server" class="add-topic__icon">
                        <ContentTemplate>
                            <asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
                                <asp:Image ID="imgTopic" ImageUrl="~/img/icon-topicicon-default.png" runat="server" Width="120" Height="120" />
                                <label><small>Choose a survey icon</small></label>
                            </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="add-topic__info">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" class="add-topic__info__action" style="width: 40%;">
                            <ContentTemplate>
                                <%--<asp:LinkButton ID="lbCreateAssessment" runat="server" CssClass="btn" Text="Create Assessment" OnClientClick="ShowProgressBar();" OnClick="lbCreateAssessment_Click"  />--%>
                                <button type="button" class="btn" id="button_create_assessment">Create Assessment</button>
                                <asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/Assessment/List" Text="Cancel" CssClass="btn secondary" />
                                <%--<asp:LinkButton ID="lbPreviewIntro" runat="server" CssClass="btn secondary"  OnClick="lbPreview_Click" Text="Preview Intro" />--%>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="add-topic__info__details">
                            <div class="tabs tabs--styled">
                                <ul class="tabs__list">
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#assessment-intro">Intro</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#assessment-tabulation">Tabulation</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#assessment-results">Answers</a>
                                    </li>
                                </ul>

                                <div class="tabs__panels">
                                    <div class="tabs__panels__item add-topic__info__details--assessment-intro" id="assessment-intro">


                                        <div class="wrapper">
                                            <div class="mainContent">

                                                <div class="form__row" style="width: 100%;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Title</label>
                                                    <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <input name="input_title_of_assessment" type="text" id="input_title_of_assessment" class="topic-detail" placeholder="Name your exam" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'input_title_of_assessmentCount','50');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;" />
                                                        <span id="input_title_of_assessmentCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;">50</span>
                                                    </div>
                                                </div>

                                                <%--<div style="text-align:right;">
                                                    <label style="font-weight: 700; display:inline-block;color:#888; font-size:14pt;width:165px">Title</label>
                                                    <input id="input_title_of_assessment" type="text" style="border:1px solid #ccc;display:inline-block; width: 560px; margin-right:28px;" placeholder="Title of assessment"/>
                                                </div>--%>


                                                <div class="form__row" style="width: 100%;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Introduction</label>
                                                    <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <textarea name="input_introduction" rows="2" cols="20" id="input_introduction" class="topic-detail" placeholder="Welcome message / purpose of this assessment" onkeyup="textCounter(this,'input_introductionCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>
                                                        <span id="input_introductionCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;">100</span>
                                                    </div>
                                                </div>

                                                <%--<div style="text-align:right;">
                                                    <label style="font-weight: 700; display:inline-block;color:#888; font-size:14pt; width:165px">Introduction</label>
                                                    <input type="text" id="input_introduction" style="border:1px solid #ccc;display:inline-block; width: 560px;margin-right:28px;" 
                                                        placeholder="Welcome message / brief introduction about this assessment" />
                                                </div>--%>

                                                <div class="form__row" style="width: 100%;margin-bottom:0;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: top; display: inline-block;">Image<br />Banners</label>
                                                    
                                                    <div style="border:0px dotted #ccc;display:inline-block; width: 560px;margin-right:28px;min-height:50px; margin-bottom: 0; vertical-align: middle; text-align:left;">

                                                        <div style="width:166px; height:166px; border: dashed 2px #ccc;display:inline-block;">
                                                            <label style="display:inline-block; position:inherit; bottom:auto; left:auto; cursor:pointer; height:100%; width:100%; text-align:center; vertical-align:middle;" onclick="showAddImagePopup();">
                                                                <span style="margin-top:15px; display:inline-block;color: rgba(203, 203, 203, 1); ">
                                                                    <span style="font-size:48pt;">&#43;</span>
                                                                    <br />
                                                                    <span style="font-size:14pt;">Upload Photo</span>
                                                                </span>
                                                                <%--<input type="file" accept="image/*" multiple="multiple" title="Add image" style="display: none" onchange="setPreviewImage(this, 'divPreviewImageList');" />--%>
                                                            </label>
                                                        </div>
                                                        &nbsp;
                                                        <br />
                                                        <p class="divPreviewImageList" style="display:inline"></p>
                                                        
                                                        <p style="color: rgba(203, 203, 203, 1); margin: .75em 0 0;">
                                                            A maximum of 5 photos are allowed.<br />
                                                            Uploaded photo up to 750 x 400 pixels and no bigger than 60KB.
                                                        </p>

                                                        &nbsp;
                                                    </div>
                                                </div>
                                                

                                                <div class="form__row video__input" style="width: 100%;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Introduction Video</label>
                                                    <div style="display:inline-block; width: 560px;margin-right:28px; text-align:left; vertical-align:top;">
                                                        
                                                        <label style="display:inline">
                                                            <span style="display:inline-block;" class="btn">+ Upload Video</span>
                                                            <input type="file" id="videoFileInput" accept="video/*" style="display:none;" class="" />
                                                        </label>
                                                        <br />
                                                        <span style="color: rgba(127, 127, 127, 1);">Uploaded video should be in .mp4 format with length no more than 30 seconds.</span>
                                                    </div>
                                                </div>

                                                <div class="form__row has_input_video" style="width: 100%;margin-left:22%;">
<div id="videoPlaceholder" style="width:320px; height:176px; border:1px solid #876389; position:relative;display:none;z-index:1;">
    <video id="videoDisplay" style="position:absolute;top:0;left:0;z-index:-1"></video>
    <div id="removeVideoButton" class="remove-video" onclick="removeVideo(this);" style="vertical-align:top; text-align:right; margin: 0 .5em;z-index:-200"><i class="fa fa-times" aria-hidden="true"></i></div>
    <img id="playVideoButton" style="display:block;margin:auto;position:relative;top:calc(100% - 60%);" src="/img/Media Play1-WF.gif" onclick="playVideo();" />
</div>
                                                </div>

                                                <div class="form__row has_input_video" style="width: 100%; display:none;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Video Title</label>
                                                    <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <input name="input_video_title" type="text" id="input_video_title" class="topic-detail" placeholder="Video Title" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'input_video_titleCount','50');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;" />
                                                        <span id="input_video_titleCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;">50</span>
                                                    </div>
                                                </div>

                                                <div class="form__row has_input_video" style="width: 100%; display:none;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Video Description</label>
                                                    <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <textarea name="input_video_description" rows="2" cols="20" id="input_video_description" class="topic-detail" placeholder="Brief description about the video" onkeyup="textCounter(this,'input_video_descriptionCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>
                                                        <span id="input_video_descriptionCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;">100</span>
                                                    </div>
                                                </div>

                                                <%--<div class="form__row" style="width: 100%; display:none;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">CSS Area</label>
                                                    <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <textarea name="input_css_area" rows="2" cols="20" id="input_css_area" class="topic-detail" placeholder="Any additional dynamic info" onkeyup="textCounter(this,'input_css_areaCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>
                                                        <span id="input_css_areaCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;">100</span>
                                                    </div>
                                                </div>--%>

                                                <div class="form__row" style="width: 100%;">
                                                    <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Closing Words</label>
                                                    <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <textarea name="input_closing_words" rows="2" cols="20" id="input_closing_words" class="topic-detail" placeholder="Thank you for participation / How can they follow up" onkeyup="textCounter(this,'input_closing_wordsCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>
                                                        <span id="input_closing_wordsCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;">100</span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="sideBar">
                                                <%--<p>
                                                    <label style="font-weight: 700">Coach</label>
                                                    <input id="input_coach_name" type="text" value="Arthur Carmazzi" />
                                                </p>--%>

                                                <div class="form__row">
                                                    <label style="font-weight: 700;">Coach</label>
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlCoach" runat="server" style="height:46px;" />
                                                    </div>
                                                </div>

                                                <div class="form__row anonymous__tip">
                                                    <asp:CheckBox ID="chk_display_coach" runat="server" Text="Display on client" CssClass="setting-checkbox" Style="float: left; width: 150px;" />
                                                </div>
                                                
                                                <p>&nbsp;</p>

                                                <div class="form__row">
                                                    <label style="font-weight: 700;">Status</label>
                                                    <div class="mdl-selectfield" style="height:46px;">
                                                        <asp:DropDownList ID="ddlStatus" runat="server" />
                                                    </div>
                                                </div>

                                                <div class="form__row anonymous__tip">
                                                    <asp:CheckBox ID="cbIsAnonymous" runat="server" Text="Anonymous Survey" CssClass="setting-checkbox" Style="float: left; width: 150px;" />
                                                    <img class="icon-img" src="/Img/icon_note_small.png" title="Participant's name will not be display in the Survey Report" />
                                                    <div class="anonymous_tip" style="display: none; top: 20px;">
                                                        <img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />
                                                        <label style="font-weight: 700;">Anonymous Survey</label>
                                                        Participant's detail will not be disclosed in the Final Report.
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--assessment-tabulation" id="assessment-tabulation">
                                        
                                        <div class="form__row anonymous__tip" style="display:inline-block;">
                                            <span style="float:left;width:65px;">Tabulation</span>
                                            <img class="icon-img" src="/Img/icon_note_small.png" title="Participant's name will not be display in the Survey Report" />
                                            <div class="anonymous_tip" style="display: none; top: -20px;">
                                                <%--<img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />--%>
                                                <label style="font-weight: 700;">Tabulation</label>
                                                Selected option points will be allocated to the any of the table here.
                                            </div>
                                        </div>


                                        <div class="grid__inner tabulation-list" style="background-color: transparent;">

                                        </div>

                                        &nbsp;
                                        <div class="grid__inner" style="background-color: transparent;">
                                            <a href="javascript:addTabulation();">+ Add a table</a>
                                        </div>

                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--assessment-result-list" id="assessment-results">
                                        
                                        <div class="assessment-result-list"></div>

                                        <div class="grid__inner" style="background-color: transparent;">
                                            <span style="color:#4683ea; cursor:pointer;" onclick="addResult(this);">+ Add a result</span>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="cardList" class="container">
            </div>
        </div>
    </div>

    <div class="popup__section">
        <div id="popup_background" class="mfp-bg" style="display:none; position: fixed; left: 0px; top: 0px; z-index: 9000; width: calc(100%); height: calc(100%);"></div>

        <div id="popup_addimage" class="popup popup--addimage" style="display:none; width: 100%; position: fixed; margin: 0px; z-index: 9001; max-height: 368px; left: 563px; top: 292.5px; height: 368px; max-width:1078px;">
            <div><h1 class="popup__title">Add Image</h1></div>
            <div class="popup__content sortable" id="foo" style="width:1076px; max-height:226px; vertical-align:top;">

                <div style="width:166px; height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top;" class="image0">
                    <label style="display:inline-block; position:inherit; bottom:auto; left:auto; cursor:pointer; height:100%; width:100%; text-align:center; vertical-align:middle;">
                        <span style="margin-top:15px; display:inline-block;color: rgba(203, 203, 203, 1); ">
                            <span style="font-size:48pt;">&#43;</span>
                            <br />
                            <span style="font-size:14pt;">Upload Photo</span>
                        </span>
                        <input type="file" accept="image/*" multiple="multiple" title="Add image" style="display: none" onchange="setPreviewImage(this, 'divPreviewImageList');" />
                    </label>
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;" class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle; text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;" class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle; text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;"  class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle;text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;"  class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle;text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;"  class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle;text-align:center;" />
                </div>


            </div>
            <div class="popup__action">
                <a id="lbClosepopup_addimage" class="popup__action__item popup__action__item--cancel" href="javascript:closeAddImagePopup();">Close</a>
            </div>
        </div>
    </div>

<script type="text/javascript" src="/Js/Sortable.js"></script>
<script type="text/javascript" src="/Js/cropbox.js"></script>
<script type="text/javascript">

    var model = {
        assessment : new assessment()
    };

    $(document).on("change", ".fileSelector", function (e) {
        var tabulationItem, tabulationItemIndex, current_tabulation;
        tabulationItem = $(e.target).parents(".tabulation-item");
        tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
        var inputFile = e.target.files[0];
        var inputFileType = inputFile.type;

        if (tabulationItemIndex >= 0) {
            current_tabulation = model.assessment.tabulation_list[tabulationItemIndex];

            //// add image to tabulation item'a list of table images;
            //var options = {
            //    imageBox: '.imageBox',
            //    thumbBox: '.thumbBox',
            //    spinner: '.spinner',
            //    imgSrc: 'avatar.png'
            //};

            //var cropper = new cropbox(options);
            var reader = new FileReader();
            reader.onload = function (e) {
                var image = new Image();
                image.src = e.target.result;
                image.onload = function () {
                    var w = this.width;
                    var h = this.height;
                    var s = inputFile.width
                    current_tabulation.table_images.push(e.target.result);
                    //options.imgSrc = e.target.result;
                    //cropper = new cropbox(options);
                    tabulationItem.find(".currentImage")[0].src = e.target.result;
                    tabulationItem.find(".remove-tabulation").show();

                    var contentImg = getContentImageObj();
                    contentImg.base64 = this.src;
                    contentImg.extension = inputFileType;
                    contentImg.width = w;
                    contentImg.height = h;
                    //current_tabulation.
                    current_tabulation.ImagesUpload.push(contentImg);

                }
                
            }

            reader.readAsDataURL(this.files[0]);
        }
    });

    //$(".sortable").sortable({
    //    items: "> .preview-image",
    //    cancel: ".opencard",
    //    axis: "x",
    //    containment: ".popup__content.sortable.ui-sortable"
    //});

    var foo = document.getElementById("foo");
    Sortable.create(foo, {
        group: "omega",
        draggable: ".preview-image",
        onEnd: function (e) {
            var tempOld = model.assessment.imageBannersUpload[e.oldIndex];
            model.assessment.imageBannersUpload[e.oldIndex] = model.assessment.imageBannersUpload[e.newIndex];
            model.assessment.imageBannersUpload[e.newIndex] = tempOld;
        }
    });

    $(".divPreviewImageList").sortable({
        items: "> .img-container",
        cancel: ".opencard",
        axis: "x",
        containment: "parent"
    });

    $(document).on("click", "#button_create_assessment", function () {
        // Check input
        if ($("#input_introduction").val().length <= 0) {
            ShowToast("Cannot create assessment without introduction.", 2);
            return;
        }

        if ($("#input_closing_words").val().length <= 0) {
            ShowToast("Cannot create assessment without closing words.", 2);
            return;
        }

        if (($("#main_content_ddlStatus").val() == "2") && ((model.assessment.tabulation_list.length <= 0) || (model.assessment.result_list.length <= 0))) {
            ShowToast("Cannot create assessment without tabulation and answers.", 2);
            return;
        }

        // Sync 
        model.assessment.adminUserId = $("#main_content_hfAdminUserId").val();
        model.assessment.companyId = $("#main_content_hfCompanyId").val();
        model.assessment.closingWords = $("#input_closing_words").val();
        model.assessment.coachId = $("#main_content_ddlCoach :selected").val();
        model.assessment.coachName = $("#main_content_ddlCoach :selected").text();
        model.assessment.cssArea = $("#input_css_area").val();
        model.assessment.iconUrl = $("#main_content_imgTopic").prop("src");
        model.assessment.introduction = $("#input_introduction").val();
        model.assessment.isAnonymous = $("#main_content_cbIsAnonymous").prop("checked");
        model.assessment.isDisplayCoachName = $("#main_content_chk_display_coach").prop("checked");
        model.assessment.title = $("#input_title_of_assessment").val();
        model.assessment.videoTitle = $("#input_video_title").val();;
        model.assessment.videoDescription = $("#input_video_description").val();;

        var idx, tabIdx;
        var tabulationElement, resultPageElement;
        var resultId;
        for (idx = 0; idx < model.assessment.tabulation_list.length; idx++) {
            tabIdx = (idx + 1).toString();
            model.assessment.tabulation_list[idx].Label = $(".tabulation-item.tabulation_" + tabIdx + " .tabulation-label").val();
            model.assessment.tabulation_list[idx].InfoTitle = $(".tabulation-item.tabulation_" + tabIdx + " .tabulation-title").val();
            model.assessment.tabulation_list[idx].InfoDescription = $(".tabulation-item.tabulation_" + tabIdx + " .tabulation-description").val();
        }

        for (idx = 0; idx < model.assessment.result_list.length; idx++) {
            resultId = model.assessment.result_list[idx].id;
            // get result page answer
            resultPageElement = $(".assessment-result-list ." + resultId);
            model.assessment.result_list[idx].Title = $(".assessment-result." + resultId + " .result-title").val();
            model.assessment.result_list[idx].Condition = $(".assessment-result." + resultId + " .result-condition").val();
            model.assessment.result_list[idx].ShowInAssessment = $(".assessment-result." + resultId + " .result-showinassessment").prop("checked");
            model.assessment.result_list[idx].VisibleToSelf = $(".assessment-result." + resultId + " .result-visibletoself").prop("checked");
            model.assessment.result_list[idx].VisibleToOthers = $(".assessment-result." + resultId + " .result-visibletoothers").prop("checked");
            model.assessment.result_list[idx].VisibleToOthersForCompletedUser = $(".assessment-result." + resultId + " .result-visibletoothersforcompleteduser").prop("checked");
            model.assessment.result_list[idx].VisibleToOthersForIncompletedUser = $(".assessment-result." + resultId + " .result-visibletoothersforincompleteduser").prop("checked");
        }

        // Make ajax call 
        jQuery.ajax({
            type: "PUT",
            url: "/api/Assessment",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(model.assessment),
            dataType: "json",
            beforeSend: function (xhr, settings) {
                ShowProgressBar();
            },
            success: function (d, status, xhr) {
                if (d.Success) {
                    ShowToast("Assessment created.", 1);
                    RedirectPage('/Assessment/Edit/' + d.AssessmentId, 300);
                } else {
                    ShowToast(d.ErrorMessage, 2);
                }
            },
            error: function (xhr, status, error) {
                ShowToast(error, 2);
            },
            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                HideProgressBar();
            }
        });
    });


    function updateTableLabel(e) {
        var $e;
        // find the tabulation of text box.
        
        var tabulationItem, tabulationItemIndex, tabulation, idx, optionElement;
        $e = $(e);
        tabulationItem = $e.parents(".tabulation-item");
        tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);

        if (tabulationItemIndex >= 0) {
            tabulation = model.assessment.tabulation_list[tabulationItemIndex];
            // update the model label text
            
            if ($e.hasClass("tabulation-label")) {
                //tabulation.Label = $(e).val();
                tabulation.Label = $e.val();
            }
            if ($e.hasClass("tabulation-title")) {
                tabulation.InfoTitle = $e.val();
            }
            if ($e.hasClass("tabulation-description")) {
                tabulation.InfoDescription = $e.val();
            }

            // for each result_page
            for (idx = 0; idx < model.assessment.result_list.length; idx++) { 
                // update affected table dropdown list
                optionElement = $(".assessment-result-list ." + model.assessment.result_list[idx].id + " .affected-tabulation-list option[value='" + tabulation.TabulationId + "']");
                optionElement.text(tabulation.Label);
                // update selected affected table drop down list
                $(".assessment-result-list ." + model.assessment.result_list[idx].id + " .affected-tabulations-panel ." + tabulation.TabulationId).text(tabulation.Label);

            }
        }
        
    }

    function removeAffectedTabulation(e) {
        var assessmentResultElement, assessmentResultIndex, assessmentResult, html;
        var affectedTabulationSelect;
        assessmentResultElement = $(e).parents(".assessment-result");
        assessmentResultIndex = $(".assessment-result-list").children().index(assessmentResultElement);
        assessmentResult = model.assessment.result_list[assessmentResultIndex];

        var toRemoveIndex;
        toRemoveIndex = assessmentResult.TabulationIdList.indexOf($(e).parent().data("value"));
        if (toRemoveIndex >= 0) {
            assessmentResult.TabulationIdList.splice(toRemoveIndex, 1);
        }
        
        // Remove element markup.
        $(e).parent().remove();
    }

    function addAffectedTable(e) {
        var assessmentResultElement, assessmentResultIndex, assessmentResult, html;
        var affectedTabulationSelect;
        assessmentResultElement = $(e).parents(".assessment-result");
        assessmentResultIndex = $(".assessment-result-list").children().index(assessmentResultElement);
        assessmentResult = model.assessment.result_list[assessmentResultIndex];
        
        affectedTabulationSelected = assessmentResultElement.find(".affected-tabulation-list :selected");

        if (affectedTabulationSelected.val() == "") {
            return;
        }

        if (assessmentResult.TabulationIdList.indexOf(affectedTabulationSelected.val()) >= 0) {
            return;
        }

        assessmentResult.TabulationIdList.push(affectedTabulationSelected.val());

        html = "";
        html = html + "<div class=\"affected-tabulation " + affectedTabulationSelected.val() + "\" data-value=\"" + affectedTabulationSelected.val() + "\">";
        html = html + affectedTabulationSelected.text();
        html = html + "    <i class=\"fa fa-times\" aria-hidden=\"true\" onclick=\"removeAffectedTabulation(this);\"></i>";
        html = html + "</div>";

        // affected-tabulations-panel
        assessmentResultElement.find(".affected-tabulations-panel").append(html);
    }

    function removeResult(e) {
        var assessmentResultElement, assessmentResultIndex, assessmentResult;
        assessmentResultElement = $(e).parents(".assessment-result");
        assessmentResultIndex = $(".assessment-result-list").children().index(assessmentResultElement);
        //assessmentResult = model.assessment.result_list[assessmentResultIndex];

        if (assessmentResultIndex >= 0) { // remove result page from model
            model.assessment.result_list.splice(assessmentResultIndex, 1);
        }

        // remove result page markup
        $(e).parents(".assessment-result").remove();
    }

    function addResult(e) {
        var idx, html, r = new result();
        r.id = "result_" + (model.assessment.result_list_lastcount + 1).toString();
        model.assessment.result_list.push(r);
        model.assessment.result_list_lastcount = model.assessment.result_list_lastcount + 1;

        html = "";
        html = html + "<div class=\"assessment-result " + r.id + "\">";
        html = html + "    <div style=\"float:right;\">";
        html = html + "        <i class=\"fa fa-arrows fa-3x\" style=\"background-color: #eee; padding: 6px; border: 1px solid #ccc;\" aria-hidden=\"true\"></i>";
        html = html + "    </div>";
        html = html + "    <label style=\"font-weight:bold;font-size:14pt; clear:both;\">Model Answers</label>";
        html = html + "    <div class=\"grid__inner\" style=\"background-color: transparent;\">";
        html = html + "        <div class=\"grid__span--6 \">";
        html = html + "            <label style=\"font-weight:bold; font-size:12pt;\">Results Page " + model.assessment.result_list_lastcount.toString() + "</label>";
        html = html + "            <div class=\"answer-div " + r.id + "\" style=\"border-image: none; width: 76%; color: rgba(209, 209, 209, 1); vertical-align: middle; display: inline-block; position: relative;\">";
        html = html + "            <input class=\"result-title\" style=\"\" type=\"text\" onkeyup=\"updateTextCounter(this,'.answer-div ." + r.id + " .lettercount',60);\" />";
        html = html + "            <span class=\"lettercount\" style=\"bottom: 12px; right: 5px; position: absolute;\">60</span>";
        html = html + "            </div>";
        html = html + "            <label style=\"font-weight:bold;font-size:14pt;\">Affected table</label>";
        html = html + "            <div class=\"mdl-selectfield\" style=\"display:inline-block;width:80%;\">";

        // get a list of option value
        html = html + "                <select class=\"affected-tabulation-list\">";
        html = html + "                    <option>Select the table</option>";
        for (idx = 0; idx < model.assessment.tabulation_list.length; idx++) {
            html = html + "                    <option value=\"" + model.assessment.tabulation_list[idx].id + "\">" + model.assessment.tabulation_list[idx].Label + "</option>";
        }
        html = html + "                </select>";
        html = html + "            </div>";
        html = html + "            <button type=\"button\" onclick=\"addAffectedTable(this);\">Add</button>";
        html = html + "            <div class=\"affected-tabulations-panel\"></div>";
        html = html + "        </div>";
        html = html + "        <div class=\"grid__span--6 grid__span--last\">";
        html = html + "            <label style=\"font-weight:bold;font-size:14pt;\">Condition</label>";
        html = html + "            <div class=\"mdl-selectfield\" >";
        html = html + "                <select class=\"result-condition\" style=\"height:36px;\">";
        html = html + "                    <option value=\"1\">Table with highest points</option>";
        html = html + "                    <option value=\"2\">Table with lowest points</option>";
        html = html + "                </select>";
        html = html + "            </div>";
        html = html + "            <label style=\"font-weight:bold;font-size:14pt;\">Show</label>";
        html = html + "            <ul>";
        html = html + "                <li>";
        html = html + "                    <span class=\"setting-checkbox\">";
        html = html + "                        <input class=\"result-showinassessment\" type=\"checkbox\">";
        html = html + "                        <label>In Responsive Assessment</label>";
        html = html + "                    </span>";
        html = html + "                </li>";
        html = html + "                <li>";
        html = html + "                    <span class=\"setting-checkbox\">";
        html = html + "                        <input class=\"result-visibletoself\" type=\"checkbox\">";
        html = html + "                        <label>Visible to self in Profile Page</label>";
        html = html + "                    </span>";
        html = html + "                </li>";
        html = html + "                <li>";
        html = html + "                    <span class=\"setting-checkbox\">";
        html = html + "                        <input class=\"result-visibletoothers\" type=\"checkbox\">";
        html = html + "                        <label>Visible to others in Profile Page</label>";
        html = html + "                    </span>";
        html = html + "                </li>";
        html = html + "                <li style=\"margin-left:2em;\">";
        html = html + "                    <label style=\"font-weight:bold;font-size:14pt;\">Display</label>";
        html = html + "                    <ul>";
        html = html + "                        <li>";
        html = html + "                            <span class=\"setting-checkbox\">";
        html = html + "                                <input class=\"result-visibletoothersforcompleteduser\" type=\"checkbox\">";
        html = html + "                                <label>To those who have completed the Assessment</label>";
        html = html + "                            </span>";
        html = html + "                        </li>";
        html = html + "                        <li>";
        html = html + "                            <span class=\"setting-checkbox\">";
        html = html + "                                <input class=\"result-visibletoothersforincompleteduser\" type=\"checkbox\">";
        html = html + "                                <label>To those who have not started or incomplete</label>";
        html = html + "                            </span>";
        html = html + "                        </li>";
        html = html + "                    </ul>";
        html = html + "                </li>";
        html = html + "            </ul>";
        html = html + "        </div>";
        html = html + "    </div>";
        html = html + "    <div style=\"width:100%;text-align:right;\">";
        html = html + "        <span style=\"color:red;cursor:pointer;\" onclick=\"removeResult(this);\">Delete Result Page</span>";
        html = html + "    </div>";
        html = html + "    <hr />";
        html = html + "</div>";

        $(".assessment-result-list").append(html);
    }

    function selectImage(e) {
        $(e).parents(".tabulation-item").find(".fileSelector").click();
    }
    
    function result() {
        this.Title = "";
        this.Ordering = 0;
        this.TabulationIdList = [];
        this.Condition = 0;
        this.ShowInAssessment = false;
        this.VisibleToSelf = false;
        // this.VisibleToOthers = false;
        this.VisibleToOthersForCompletedUser = false;
        this.VisibleToOthersForIncompletedUser = false;
        this.id = "";
    }

    function tabulation() {
        // Tabulation properties
        this.TabulationId = "";
        this.Label = "";
        this.InfoTitle = "";
        this.InfoDescription = "";
        this.current_table_image_index = 0;
        this.table_images = [];
        this.questions = [];
        this.id = "";
        this.ImagesUpload = [];

        // Tabulation functions
        this.getPrevTableImage = function() {
            if (this.table_images.length > 0) {
                if (this.current_table_image_index == 0) {
                    this.current_table_image_index = this.table_images.length - 1;
                } else { // tabulation.current_table_image_index >= tabulation.table_images.length
                    this.current_table_image_index = this.current_table_image_index - 1;
                }

                return this.table_images[this.current_table_image_index];
            }

        };

        this.getNextTableImage = function() {
            if (this.table_images.length > 0) {
                if (this.current_table_image_index < (this.table_images.length - 1)) {
                    this.current_table_image_index = this.current_table_image_index + 1;

                } else { // tabulation.current_table_image_index >= tabulation.table_images.length
                    this.current_table_image_index = 0; 
                }

                return this.table_images[this.current_table_image_index];
            }
        };
    }

    function assessment() {
        this.adminUserId = "";
        this.result_list = [];
        this.closingWords = "";
        this.coachId = "";
        this.coachName = "";
        this.cssArea = "";
        this.iconUrl = "";
        this.imageBanners = [];
        this.introduction= "";
        this.isAnonymous = false;
        this.isDisplayCoachName = false;
        this.tabulation_list = [];
        this.title = "";
        this.videoDescription = "";
        this.videoThumbnailUrl = "";
        this.videoTitle = "";
        this.videoUrl = "";
        this.tabulation_list_lastcount = 0;
        this.result_list_lastcount = 0;
        this.imageBannersUpload = [];
        this.videoThumbnailUpload = {};
        this.videoUpload = {};

        //this.title = "";
        //this.introduction = "";
        //this.image_banners = [];
        //this.video = null;
        //this.video_title = "";
        //this.video_description = "";
        //this.css_area = "";
        //this.closing_words = "";
        //this.coach = "";
        //this.display_on_client = false;
        //this.status = 0;
        //this.anonymous_survey = false;

        //this.tabulation_list = [];
        //this.result_list = [];
    }

    function prevImage(e) {
        // Find tabulation from model.assessment.tabulation_list
        var tabulationItem, tabulationItemIndex, tabulation;

        tabulationItem = $(e).parents(".tabulation-item");
        tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
        if (tabulationItemIndex >= 0) {
            tabulation = model.assessment.tabulation_list[tabulationItemIndex];

            if (tabulation.current_table_image_index == 0) {
                tabulation.current_table_image_index = tabulation.table_images.length - 1;
            } else { 
                tabulation.current_table_image_index = tabulation.current_table_image_index - 1;
            }

            $(e).siblings(".currentImage")[0].src = tabulation.table_images[tabulation.current_table_image_index];
        }
    }

    function removeTabulationImage(e) {
        // Find tabulation from model.assessment.tabulation_list
        var tabulationItem, tabulationItemIndex, current_tabulation;

        tabulationItem = $(e).parents(".tabulation-item");
        tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
        current_tabulation = model.assessment.tabulation_list[tabulationItemIndex];

        current_tabulation.table_images.splice(current_tabulation.current_table_image_index, 1);
        current_tabulation.ImagesUpload.splice(current_tabulation.current_table_image_index, 1);

        if (current_tabulation.current_table_image_index < current_tabulation.table_images.length) {
            tabulationItem.find(".currentImage")[0].src = current_tabulation.table_images[current_tabulation.current_table_image_index];
        } else {
            current_tabulation.current_table_image_index = current_tabulation.current_table_image_index - 1;
            if (current_tabulation.current_table_image_index >= 0) {
                tabulationItem.find(".currentImage")[0].src = current_tabulation.table_images[current_tabulation.current_table_image_index];
            } else {
                tabulationItem.find(".currentImage")[0].src = "/Img/icon-upload-photo.png";
                tabulationItem.find(".remove-tabulation").hide();
            }
        }

    }

    function nextImage(e) {
        // Find tabulation from model.assessment.tabulation_list
        var tabulationItem, tabulationItemIndex, tabulation;

        tabulationItem = $(e).parents(".tabulation-item");
        tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
        if (tabulationItemIndex >= 0) {
            tabulation = model.assessment.tabulation_list[tabulationItemIndex];
            $(e).siblings(".currentImage")[0].src = tabulation.getNextTableImage();
        }
    }

    function removeVideo(e) {
        $("#videoPlaceholder").hide();
        $("#playVideoButton").hide();
        $("#removeVideoButton").hide();
        $("#videoFileInput").val("");
        var videoNode = $("#videoDisplay")[0];
        videoNode.src = "";

        $(".form__row.video__input").show();
        $(".form__row.has_input_video").hide();
    }

    function playVideo() {
        var videoNode = $("#videoDisplay")[0];
        $("#playVideoButton").hide();
        videoNode.play();
    }


    function getContentImageObj() {
        return {
            'base64': '',
            'extension': '',
            'width': 0,
            'height': 0
        };
    }

    function removeContentImg(type, arg) {
        switch (type) {
            case 1: // NewCard.contentImgs[arg]
                document.getElementById('imgNewCardContentImgPreview').src = "";
                document.getElementById('divNewCardContentImg').style.visibility = "hidden";
                document.getElementById('divNewCardContentImg').style.height = "0px";
                //document.getElementById('imgNewCardPreviewContentImg').src = "";

                //NewCard.contentImgs.splice(arg - 1, 1);
                break;
            case 2: // NewCard.options[arg].contentImg
                document.getElementById('imgNewOption' + arg + 'ContentImgPreview').src = "";
                document.getElementById('divNewCardOption' + arg + 'ContentImg').style.visibility = "hidden";
                document.getElementById('divNewCardOption' + arg + 'ContentImg').style.height = "0px";

                NewCard.options[arg - 1].contentImg.base64 = "";
                NewCard.options[arg - 1].contentImg.extension = "";
                NewCard.options[arg - 1].contentImg.width = 0;
                NewCard.options[arg - 1].contentImg.height = 0;

                rebuildNewCardPreviewOptions();
                break;
            default:
                break;
        }
    }


    function removePopupImgBanner(element) {
        var html, uploadBannerIdx;
        uploadBannerIdx = $(element).parent().parent().children().index($(element).parent());

        //remove 
        $($("#popup_addimage .popup__content .preview-image")[uploadBannerIdx-1]).remove();
        // compensate
        html = "";
        html = html + "<div style=\"width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;\" class=\"preview-image\">";
        html = html + "    <div class=\"remove-img-banner\" style=\"display:none;\" onclick=\"removePopupImgBanner(this);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
        html = html + "    <img style=\"max-height:100%; max-height:100%; vertical-align:middle;text-align:center;\" />";
        html = html + "</div> ";

        $("#popup_addimage .popup__content").append(html);

        model.assessment.imageBannersUpload.splice(uploadBannerIdx-1, 1);
        $(element).parent().remove();
    }

    function removeImgBanner(element) {
        var html, uploadBannerIdx;
        uploadBannerIdx = $(element).parent().parent().children().index($(element).parent());
        //remove 
        $($("#popup_addimage .popup__content .preview-image")[uploadBannerIdx]).remove();
        // compensate
        html = "";
        html = html + "<div style=\"width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;\" class=\"preview-image\">";
        html = html + "    <div class=\"remove-img-banner\" style=\"display:none;\" onclick=\"removePopupImgBanner(this);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
        html = html + "    <img style=\"max-height:100%; max-height:100%; vertical-align:middle;text-align:center;\" />";
        html = html + "</div> ";

        $("#popup_addimage .popup__content").append(html);

        model.assessment.imageBannersUpload.splice(uploadBannerIdx, 1);
        $(element).parent().remove();
    }

    function setPreviewImage(input, elementId) {
        var idx, inputFile;
        if (input.files.length > 0) {
            for (idx = 0; idx < input.files.length; idx++) {

                inputFile = input.files[idx];
                var inputFileType = inputFile.type;

                if (inputFileType.toLowerCase() == "image/png" ||
                    inputFileType.toLowerCase() == "image/jpeg" ||
                    inputFileType.toLowerCase() == "image/jpg") {

                    var fileReader = new FileReader();

                    fileReader.onload = function (e) {
                        var image = new Image();
                        image.src = e.target.result;
                        image.onload = function (a, b, c) {
                            var w = this.width;
                            var h = this.height;
                            var html = "";

                            if (model.assessment.imageBannersUpload.length < 5) {
                                html = html + "<div class=\"img-container\" >";
                                html = html + "    <div class=\"remove-img-banner\" onclick=\"removeImgBanner(this);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                                html = html + "    <img class=\"img-banner\" style=\"max-height:100px;\" />";
                                html = html + "</div>";
                                $(".divPreviewImageList").append(html);

                                $(".divPreviewImageList img:last")[0].src = this.src;
                                $(".divPreviewImageList img:last")[0].style.visibility = "visible";
                                $(".divPreviewImageList img:last")[0].style.height = "auto";

                                var contentImg = getContentImageObj();
                                contentImg.base64 = this.src;
                                contentImg.extension = inputFileType;
                                contentImg.width = w;
                                contentImg.height = h;
                            
                                $($("#popup_addimage .popup__content").children(".preview-image")[model.assessment.imageBannersUpload.length]).children("img")[0].src = this.src;
                                $($("#popup_addimage .popup__content").children(".preview-image")[model.assessment.imageBannersUpload.length]).children(".remove-img-banner").show();
                                model.assessment.imageBannersUpload.push(contentImg);
                            }
                            
                        }
                    }

                    fileReader.readAsDataURL(input.files[idx]);
                } else {
                    toastr.error('File extension is invalid.');
                }
            }
        }
    }

    function previewImage(input, maxWidth, maxHeight, type, arg) // arg: "index of contentImgs", "index of options".
    {
        if (input.files && input.files[0]) {
            var t = input.files[0].type;
            var s = input.files[0].size / (1024 * 1024);
            if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
                if (s > 5) {
                    toastr.error('Uploaded photo exceeded 5MB.');
                } else {
                    var fileReader = new FileReader();
                    var image = new Image();
                    fileReader.onload = function (e) {
                        image.src = e.target.result;
                        image.onload = function () {
                            var w = this.width,
                                h = this.height;
                            if ((w > maxWidth || h > maxHeight)) {
                                toastr.error('Uploaded photo exceeded ' + maxWidth + ' x ' + maxHeight + '.');
                            }
                            else {
                                switch (type) {
                                    case 1: // New Card > Content Image
                                        document.getElementById('imgNewCardContentImgPreview').src = this.src;
                                        document.getElementById('divNewCardContentImg').style.visibility = "visible";
                                        document.getElementById('divNewCardContentImg').style.height = "auto";
                                        //document.getElementById('imgNewCardPreviewContentImg').src = this.src;

                                        var contentImg = getContentImageObj();
                                        contentImg.base64 = this.src;
                                        contentImg.extension = t;
                                        contentImg.width = w;
                                        contentImg.height = h;
                                        //NewCard.contentImgs[arg - 1] = contentImg;

                                        break;
                                    case 2: // New Card > Option Image
                                        var id = "imgNewOptionContent" + arg + "ImgPreview";
                                        document.getElementById('imgNewOption' + arg + 'ContentImgPreview').src = this.src;
                                        document.getElementById('divNewCardOption' + arg + 'ContentImg').style.visibility = "visible";
                                        document.getElementById('divNewCardOption' + arg + 'ContentImg').style.height = "auto";

                                        NewCard.options[arg - 1].contentImg.base64 = this.src;
                                        NewCard.options[arg - 1].contentImg.extension = t;
                                        NewCard.options[arg - 1].contentImg.width = w;
                                        NewCard.options[arg - 1].contentImg.height = h;

                                        rebuildNewCardPreviewOptions();

                                        break;
                                    default:
                                        break;
                                }
                            }
                        };
                    }
                }
            }
            else {
                toastr.error('File extension is invalid.');
            }
            fileReader.readAsDataURL(input.files[0]);
        }
    }

    function displayCardPreviewImage(input, maxWidth, maxHeight, cardId, optionId) {
        var image = new Image();

        if (input.files && input.files[0]) {
            var t = input.files[0].type;
            var s = input.files[0].size / (1024 * 1024);
            if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
                if (s > 5) {
                    toastr.error('Uploaded photo exceeded 5MB.');
                } else {
                    var fileReader = new FileReader();
                    fileReader.addEventListener("load", function () {
                        var image = new Image();
                        image.src = this.result;

                        model.UseCustomImage = true;
                        model.CustomImageDataUrl = {
                            base64: this.result,
                            extension: $("#selected_background").val().substr($("#selected_background").val().lastIndexOf(".") + 1),
                            width: image.width,
                            height: image.height
                        };


                        $("#pulseCard").css("backgroundImage", "url(" + this.result + ")");

                        // display mini-x
                        $(".remove-image-icon").show();

                        // hide attach image icon
                        $(".upload-option-image").hide();

                    }, false);
                    fileReader.readAsDataURL(input.files[0]);
                }
            } else {
                toastr.error('File extension is invalid.');
            }
        }
    } // end function displayCardPreviewImage(input, maxWidth, maxHeight) {

    function deleteTable(e) {
        // Find tabulation from model.assessment.tabulation_list
        var tabulationItem, tabulationItemIndex, tabulation;

        tabulationItem = $(e).parents(".tabulation-item");
        tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
        if (tabulationItemIndex >= 0) {
            //tabulation = model.assessment.tabulation_list[tabulationItemIndex];
            //$(e).siblings(".currentImage")[0].src = tabulation.getNextTableImage();
            model.assessment.tabulation_list.splice(tabulationItemIndex, 1);
            tabulationItem.remove();
        }
    }

    function addTabulation() {

        var t1 = new tabulation();
        t1.Label = "";
        t1.InfoDescription = "";
        t1.InfoTitle = "";
        t1.current_table_image_index = 0;
        t1.id = "tabulation_" + (model.assessment.tabulation_list_lastcount + 1).toString();
        t1.TabulationId = "tabulation_" + (model.assessment.tabulation_list_lastcount + 1).toString();
        
        model.assessment.tabulation_list.push(t1);
        model.assessment.tabulation_list_lastcount = model.assessment.tabulation_list_lastcount + 1;

        var html = "";
        html = html + "<div class=\"grid__span--6 grid__span--last tabulation-item " + t1.TabulationId + "\">";
        html = html + "    <fieldset style=\"border:1px solid #ccc;padding:1em;width:98%;margin-top:8px;\">";
        html = html + "        <legend style=\"background-color:#FF9800;color:#FFF9F0; padding:0 1em;\">TABLE " + model.assessment.tabulation_list_lastcount.toString() + "</legend>";
        html = html + "        <div class=\"tabulation-label-div\" style=\"border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
        html = html + "            <input class=\"tabulation-label\" type=\"text\" value=\"\" placeholder=\"Enter table name\" onkeyup=\"updateTableLabel(this);updateTextCounter(this,'.tabulation-label-div .lettercount',60);\" />";
        html = html + "            <span class=\"lettercount\" style=\"bottom: 12px; right: 5px; position: absolute;\">60</span>";
        html = html + "        </div>";
        html = html + "        <div class=\"form__row anonymous__tip\" style=\"display:inline-block;width:100%;margin:0;\">";
        html = html + "            <span style=\"float:left;width:30px;\">Info</span>";
        html = html + "            <img class=\"icon-img\" src=\"/Img/icon_note_small.png\" title=\"Participant's name will not be display in the Survey Report\" />";
        html = html + "        </div>";
        html = html + "        <div style=\"width:100%; display:inline-block;\">";
        html = html + "            <div style=\"width:49%;border:dashed 1px #666;display:inline-flex;vertical-align:top;height:160px;\">";
        html = html + "                <img style=\"margin:auto;\" src=\"/Img/prev.png\" onclick=\"prevImage(this);\" />";
        html = html + "                <div style=\"z-index:1;display:none;\" class=\"remove-tabulation image-button\">";
        html = html + "                    <i class=\"fa fa-times fa-2x\" aria-hidden=\"true\" style=\"width:0;\" onclick=\"removeTabulationImage(this);\"></i>";
        html = html + "                </div>";
        html = html + "";
        html = html + "                <img style=\"margin:auto; max-width:180px;max-height:155px;\" class=\"currentImage\" src=\"/Img/icon-upload-photo.png\" onclick=\"selectImage(this);\" />";
        html = html + "                <label style=\"width:0;\">";
        html = html + "                    <input type=\"file\" class=\"fileSelector\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" />";
        html = html + "                </label>";
        html = html + "                <img style=\"margin:auto;\" src=\"/Img/next.png\" onclick=\"nextImage(this);\" />";
        html = html + "            </div>";
        html = html + "            <div style=\"width:49%;color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
        html = html + "                <div class=\"tabulation-title-div\" style=\"width:100%;color: rgba(209, 209, 209, 1); font-size: .9em; vertical-align: middle; display: inline-block; position: relative;\">";
        html = html + "                    <input class=\"tabulation-title\" type=\"text\" value=\"\" placeholder=\"Enter title\" onkeyup=\"updateTableLabel(this);updateTextCounter(this,'.tabulation-title-div .lettercount',60);\" />";
        html = html + "                    <span class=\"lettercount\" style=\"bottom: 12px; right: 5px; position: absolute;\">60</span>";
        html = html + "                </div>";
        html = html + "                <div class=\"tabulation-description-div\"  style=\"width:100%;color: rgba(209, 209, 209, 1); font-size: .9em; vertical-align: middle; display: inline-block; position: relative;\">";
        html = html + "                    <textarea class=\"tabulation-description\" style=\"height:115px\" placeholder=\"Description\" onkeyup=\"updateTableLabel(this);updateTextCounter(this,'.tabulation-description-div .lettercount',250);\"></textarea>";
        html = html + "                    <span class=\"lettercount\" style=\"bottom: 15px; right: 10px; position: absolute;\">250</span>";
        html = html + "                </div>";
        html = html + "            </div>";
        html = html + "        </div>";
        html = html + "        <div class=\"table-links\">";
        html = html + "            <div style=\"display:inline-block;width:100%;\">";
        html = html + "                <a href=\"#\" style=\"color:#ccc\">Edit image</a>";
        html = html + "            </div>";
        html = html + "        </div>";
        html = html + "        <div class=\"table-links\">";
        html = html + "            <div style=\"display:inline-block;width:49%;\">";
        html = html + "                <a href=\"#\" style=\"color:#ccc\">Questions affected</a>";
        html = html + "            </div>";
        html = html + "            <div style=\"display:inline-block;width:49%;text-align:right;\">";
        html = html + "                <span onclick=\"deleteTable(this);\" style=\"color:red;cursor:pointer;\">Delete table</span>";
        html = html + "            </div>";
        html = html + "        </div>";
        html = html + "    </fieldset>";
        html = html + "</div>";

        $(".grid__inner.tabulation-list").append(html);

        
        //return html;
    }

    function hideAddImagePopup() {
        $("#popup_background").hide();
        $("#popup_addimage").hide();
    }

    function showAddImagePopup() {
        var windowWidth, windowHeight,
            dialogWidth, dialogHeight,
            centerX, centerY, 
                dialogX, dialogY;

        dialogWidth = $("#popup_addimage").width();
        dialogHeight= $("#popup_addimage").height();

        windowWidth = $(window).width();
        windowHeight = $(window).height();
        centerX = windowWidth / 2;
        centerY = windowHeight / 2;

        dialogX = centerX - (dialogWidth / 2);
        dialogY = centerY - (dialogHeight / 2);

        $("#popup_background").show();
        $("#popup_addimage").show();

        $("#popup_addimage").offset({
            top: dialogY,
            left: dialogX
        });
    }

    function closeAddImagePopup() {
        $("#popup_background").hide();
        $("#popup_addimage").hide();
    }

    function updateTextCounter(element, counterSelector, maxCount) {
        var letterLeft = maxCount - $(element).val().length;
        $(element).siblings(".lettercount").text(letterLeft);
        if ($(element).val().length > maxCount) {
            $(element).siblings(".lettercount").text(0);
            $(element).val($(element).val().slice(0, maxCount));
        }
    }

    (function ($) {

        $("#videoDisplay").on("ended", function (e) {
            $("#playVideoButton").show();
            $("#removeVideoButton").show();
        });

        $("#videoDisplay").on("loadedmetadata", function (e) {
            if (this.videoWidth < 590) {
                $("#videoPlaceholder").width(this.videoWidth);
                $("#videoPlaceholder").height(this.videoHeight);
            } else {
                var whRatio = this.videoWidth / this.videoHeight;
                var widthRatio = this.videoWidth / 590
                $("#videoPlaceholder").width(590);
                $("#videoPlaceholder").height(this.videoHeight / widthRatio);
                $("#videoDisplay").width(590);
                $("#videoDisplay").height(this.videoHeight / widthRatio);
            }
            
            // Set position for playVideoButton
            var top = (($("#videoPlaceholder").height() / 2) - $("#removeVideoButton").height() - 24) + "px";
            $("#playVideoButton").css("top", top);
        });

        $("#videoDisplay").on("loadeddata", function (e) {
            // Generate thumbnail
            var scale = 1;
            var canvas = document.createElement("canvas");
            canvas.width = this.videoWidth * scale;
            canvas.height = this.videoHeight * scale;
            canvas.getContext('2d').drawImage(this, 0, 0, canvas.width, canvas.height);
            var thumbnailUrl = canvas.toDataURL();
            model.assessment.videoThumbnailUrl = thumbnailUrl;
            model.assessment.videoThumbnailUpload = {
                'base64': thumbnailUrl,
                'filename': "video_thumbnail.png"
            };
        });

        $("#videoFileInput").change(function (e) {
            var file,
                fileType,
                fileReader,
                fileUrl,
                fileName,
                videoNode;

            fileReader = new FileReader();
            fileReader.onload = function (e) {
                model.assessment.videoUrl = e.target.result;
                model.assessment.videoUpload = {
                    'base64': e.target.result,
                    'filename': fileName
                };
            }
            file = e.target.files[0];
            fileType = file.type;
            fileName = file.name;
            videoNode = $("#videoDisplay")[0];
            fileUrl = window.URL.createObjectURL(file);
            videoNode.src = fileUrl;

            $("#videoPlaceholder").show();
            $("#playVideoButton").show();
            $("#removeVideoButton").show();
            $(".form__row.video__input").hide();
            $(".form__row.has_input_video").show();

            fileReader.readAsDataURL(file);
        });

    }(jQuery));
</script>
</asp:Content>
