﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Assessment
{
    public partial class CoachList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            System.Configuration.Configuration configuration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            System.Web.Configuration.HttpRuntimeSection httpruntinesec =
                (System.Web.Configuration.HttpRuntimeSection)configuration.GetSection("system.web/httpRuntime");


            //System.Configuration.ConfigurationManager.
            //System.Web.Configuration.
            //System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            //System.Configuration.ConfigurationSection requestFilteringSection = configuration.GetSection("system.webServer/security");
            //System.Configuration.ConfigurationElement requestLimitsElement = requestFilteringSection.GetChildElement("requestLimits");

            //object maxAllowedContentLength = requestLimitsElement.GetAttributeValue("maxAllowedContentLength");
            //if (null != maxAllowedContentLength)
            //{
            //    uint.TryParse(maxAllowedContentLength.ToString(), out uiMaxAllowedContentLength);
            //}


            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                String categoryId = String.Empty;
                if (Page.RouteData.Values["CategoryId"] != null)
                {
                    categoryId = Page.RouteData.Values["CategoryId"].ToString();
                }

                ViewState["currentSortField"] = "SortByCoach";
                ViewState["currentSortDirection"] = "asc";


            } // if (!Page.IsPostBack)

            // Load listview
            RefreshListView();
        }

        private void RefreshListView()
        {
            // Fetch data and sort
            CoachSelectAllResponse response = asc.SelectAllCoaches(this.adminInfo.UserId);
            if (response.Success)
            {
                List<Coach> coaches = response.Coaches;

                if (!string.IsNullOrWhiteSpace(tbFilterKeyWord.Text))
                {
                    coaches = coaches.Where(r => r.FirstName.ToLowerInvariant().Contains(tbFilterKeyWord.Text.ToLowerInvariant())).ToList();
                }

                switch (ViewState["currentSortField"].ToString())
                {
                    case "SortByCoach":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            coaches = response.Coaches.OrderBy(r => r.FirstName).ToList();
                        }
                        else
                        {
                            coaches = response.Coaches.OrderByDescending(r => r.FirstName).ToList();
                        }
                        break;

                    case "SortByCount":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            coaches = response.Coaches.OrderBy(r => r.NumberOfAssessments).ToList();
                        }
                        else
                        {
                            coaches = response.Coaches.OrderByDescending(r => r.NumberOfAssessments).ToList();
                        }
                        break;

                    default:
                        // do nothing
                        break;
                }

                this.lvCoach.DataSource = coaches;
                this.lvCoach.DataBind();

                if (coaches != null && coaches.Count > 0)
                {
                    ltlCount.Text = coaches.Count + " coaches";
                }
                else if (coaches != null && coaches.Count == 1)
                {
                    ltlCount.Text = "1 coach";
                }
                else
                {
                    ltlCount.Text = "0 coach";
                }

            } // end if (response.Success)

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    RSTopicUpdateResponse response = asc.UpdateRSTopicStatus(hfSurveyId.Value, hfCategoryId.Value, adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16
            //        (lbAction.CommandArgument));
            //    String toastMsg = String.Empty;
            //    if (response.Success)
            //    {
            //        mpePop.Hide();
            //        RefreshSurveyListView();

            //        if (Convert.ToInt16(lbAction.CommandArgument) == 2)
            //        {
            //            toastMsg = hfSurveyName.Value + " has been set to active.";
            //        }
            //        else if (Convert.ToInt16(lbAction.CommandArgument) == 3)
            //        {
            //            toastMsg = hfSurveyName.Value + " has been set to hidden.";
            //        }
            //        else if (Convert.ToInt16(lbAction.CommandArgument) == -1)
            //        {
            //            toastMsg = hfSurveyName.Value + " has been deleted.";
            //        }
            //        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
            //    }
            //    else
            //    {
            //        Log.Error("RSTopicUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            //        mpePop.Show();
            //        toastMsg = "Failed to change status, please check your internet connection.";
            //        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
            //    }
            //    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            //}
            //catch (Exception ex)
            //{
            //    Log.Error(ex.ToString(), ex, this.Page);
            //}
        }

        protected void lbActionCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        protected void ibFilterKeyWord_Click(object sender, EventArgs e)
        {
            RefreshListView();
        }

        protected void lvCoach_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                Literal ltlFirstName = e.Item.FindControl("ltlFirstName") as Literal;

                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Assessment/Edit/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("RenameCoach", StringComparison.InvariantCultureIgnoreCase))
                {
                    tbRenameName.Text = ltlFirstName.Text;

                    lbRenameSave.CommandArgument = e.CommandArgument.ToString();
                    hfCoachId.Value = e.CommandArgument.ToString();
                    mpePop.PopupControlID = "popup_renamecategory";
                    mpePop.Show();
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("DeleteCoach", StringComparison.InvariantCultureIgnoreCase))
                {
                    ltlDeleteMsg.Text = "<p>Are you sure you want to delete this coach : <b>" + ltlFirstName.Text + "</b> ?</p>";
                    lbDelete.Visible = true;
                    lbDelCancel.Text = "Cancel";

                    lbDelete.CommandArgument = e.CommandArgument.ToString();
                    hfCoachId.Value = e.CommandArgument.ToString();
                    mpePop.PopupControlID = "popup_deletecategory";
                    mpePop.Show();
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("DuplicateAssessment", StringComparison.InvariantCultureIgnoreCase))
                {
                    // not yet
                    Context.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvCoach_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            RefreshListView();
        }
        protected void lvCoach_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                ListView lv = sender as ListView;
                LinkButton headerLinkButton = null;
                string[] linkButtonControlList = new[] { "SortByCoach", "SortByCount" };

                if (lv == null)
                    return;

                // Remove the up-down arrows from each header
                foreach (string lbControlId in linkButtonControlList)
                {
                    headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                    if (headerLinkButton != null)
                    {
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                    }
                }

                // Add sort direction back to the field in question
                headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                if (headerLinkButton != null)
                {
                    if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                    }
                    else
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
        protected void lvCoach_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.EmptyItem)
                {
                    Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                    if (!String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                    {
                        ltlEmptyMsg.Text = @"Filter """ + tbFilterKeyWord.Text.Trim() + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                    }
                    else
                    {
                        ltlEmptyMsg.Text = @"Welcome to Assessment Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start by adding a Coach with the green plus button.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }
        protected void lvCoach_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }


        public void lbPopCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }
        public void lbRenameSave_Click(object sender, EventArgs e)
        {
            // User input validation
            if (string.IsNullOrWhiteSpace(tbRenameName.Text))
            {
                MessageUtility.ShowToast(this.Page, "Please enter category title. ", MessageUtility.TOAST_TYPE_ERROR);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                return;
            }

            try
            {
                // This should be update
                //TODO: waiting for API to rename assessment
                //CassandraService.ServiceResponses.RSCategoryUpdateResponse response = asc.UpdateRSCategory(adminInfo.UserId, adminInfo.CompanyId, lbRenameSave.CommandArgument, tbRenameName.Text.Trim());
                CoachUpdateResponse response = asc.UpdateCoach(adminInfo.UserId, hfCoachId.Value, tbRenameName.Text);

                if (response.Success)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview, Rebind listview
                    //RefreshSurveyCategoryListView();
                    RefreshListView();

                    // Display toast
                    MessageUtility.ShowToast(this.Page, tbRenameName.Text + " has been renamed.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, tbRenameName.Text + " has not been renamed.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
        public void lbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CoachUpdateResponse response = asc.DeleteCoach(adminInfo.UserId, hfCoachId.Value);
                if (response.Success)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview, Rebind listview
                    RefreshListView();

                    // Display toast
                    //MessageUtility.ShowToast(this.Page, this.hfCategoryName.Value + " has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                    MessageUtility.ShowToast(this.Page, "Coach has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, "Delete coach failed.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
        public void lbAddDone_Click(object sender, EventArgs e)
        {
            // User input validation
            if (string.IsNullOrWhiteSpace(tbAddName.Text))
                return;

            try
            {
                CoachCreateResponse response = asc.CreateCoach(tbAddName.Text, this.adminInfo.UserId);

                if (response.Success)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview
                    // Rebind listview
                    RefreshListView();

                    // Display toast
                    MessageUtility.ShowToast(this.Page, tbAddName.Text + " has been created.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    //MessageUtility.ShowToast(this.Page, response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddCoach_Click(object sender, EventArgs e)
        {
            // Configure display 
            tbRenameName.Text = "Add coach";

            //lbRenameSave.CommandArgument = e.CommandArgument.ToString();
            //hfAssessmentId.Value = e.CommandArgument.ToString();
            //hfAssessmentName.Value = lbAssessmentName.Text;

            // Display popup
            mpePop.PopupControlID = "popup_addcategory";
            mpePop.Show();
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}