﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Assessment
{
    public partial class AssessmentCreate : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private List<String> iconUrlList = null;
        private ManagerInfo managerInfo;

        public void GetIconData()
        {
            TopicSelectIconResponse response = asc.SelectAllTopicIcons(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
            if (response.Success)
            {
                iconUrlList = response.TopicIconUrls;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout", false);
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;
            ViewState["manager_user_id"] = managerInfo.UserId;
            ViewState["company_id"] = managerInfo.CompanyId;
            this.hfAdminUserId.Value = ViewState["manager_user_id"].ToString();
            this.hfCompanyId.Value = ViewState["company_id"].ToString();

            if (!Page.IsPostBack)
            {
                // Set default icon for assessment logo
                imgTopic.ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/default_topic_icon.png";

                ddlStatus.Items.Add(new ListItem("Draft", "1"));
                ddlStatus.Items.Add(new ListItem("Live", "2"));
                ddlStatus.SelectedIndex = 0;


                //ddlCoach
                CoachSelectAllResponse coachResponse = asc.SelectAllCoaches(managerInfo.UserId);
                if (coachResponse.Success)
                {
                    ddlCoach.DataSource = coachResponse.Coaches;
                    ddlCoach.DataMember = "UserId";
                    ddlCoach.DataTextField = "FirstName";
                    ddlCoach.DataValueField = "UserId";
                    ddlCoach.SelectedIndex = 0;
                    ddlCoach.DataBind();
                }
                
            }
        }

        #region event handlers for picking icon

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = iconUrlList[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = iconUrlList[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpePop.Hide();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lbChooseIcon") as LinkButton;
                if (btn != null)
                {
                    scriptMan.RegisterAsyncPostBackControl(btn);
                }
            }
        }

        protected void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                #region Icon data
                GetIconData();
                rtIcon.DataSource = iconUrlList;
                rtIcon.DataBind();
                #endregion
                mpePop.PopupControlID = "popup_addtopicicon";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        #endregion event handlers for picking icon

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbCreateAssessment_Click(object sender, EventArgs e)
        {
            // Creation is done at the Web API side of things.
        }

        protected void lbPreview_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.RouteData.Values["AssessmentId"] != null)
                {
                    // Edit Assessment
                    AssessmentSelectResponse response = asc.PreviewAssessment(
                        ViewState["manager_user_id"].ToString(),
                        Page.RouteData.Values["AssessmentId"].ToString());

                    if (response.Success)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                        string resultJson = javascriptSerializer.Serialize(response);
                        //this.hfLiveSurveyPreviewJson.Value = resultJson;

                        mpePop.PopupControlID = "popup_livesurveypreview";
                        mpePop.Show();
                    }
                    else
                    {
                        // Show toast notification indicating that there is error.
                        MessageUtility.ShowToast(this.Page, "Error retrieving data for live preview. " + response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }
    }
}