﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Assessment
{
    public partial class SubscriptionList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                String categoryId = String.Empty;
                if (Page.RouteData.Values["CategoryId"] != null)
                {
                    categoryId = Page.RouteData.Values["CategoryId"].ToString();
                }

                ViewState["currentSortField"] = "lbSortAssessmentTitle";
                ViewState["currentSortDirection"] = "asc";


            } // if (!Page.IsPostBack)

            // Load survey list
            RefreshListView();
        }

        private void RefreshListView()
        {
            ListView lv = this.lvSurvey;
            LinkButton lb;

            // Fetch data and sort

            SubscriptionSelectAllResponse response = asc.SelectAllAssessmentSubscription(this.adminInfo.UserId);

            if (response.Success)
            {
                if (!string.IsNullOrWhiteSpace(tbFilterSurvey.Text))
                {
                    response.Subscriptions = response.Subscriptions.Where(r => r.Company.CompanyTitle.ToLowerInvariant().Contains(tbFilterSurvey.Text.ToLowerInvariant())).ToList();
                }

                this.lvSurvey.DataSource = response.Subscriptions;
                this.lvSurvey.DataBind();

                switch (ViewState["currentSortField"].ToString())
                {
                    case "lbSortCompany":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Subscriptions.OrderBy(r => r.Company.CompanyTitle).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Subscriptions.OrderByDescending(r => r.Company.CompanyTitle).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;
                    case "lbSortAssessment":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Subscriptions.OrderBy(r => r.Assessment.Title).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Subscriptions.OrderByDescending(r => r.Assessment.Title).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;

                    case "lbSortAssessmentStatus":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Subscriptions.OrderBy(r => r.Status).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Subscriptions.OrderByDescending(r => r.Status).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;

                }

            } // end if (response.Success)

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                String toastMsg = String.Empty;
                // TODO: pending Kevin implementation of DeleteAssessmentSubscription
                //SubscriptionDeleteResponse response = asc.DeleteSubscription(this.adminInfo.UserId, hfCompanyId.Value, hfAssessmentId.Value);

                //if (response.Success)
                //{
                //    mpePop.Hide();
                //    RefreshListView();

                //    toastMsg = "Subscription deleted.";
                //    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                //}
                //else
                //{
                //    Log.Error("DeleteSubscription.Success is false. ErrorMessage: " + response.ErrorMessage);
                //    mpePop.Show();
                //    toastMsg = "Failed to change status, please check your internet connection.";
                //    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                //}
                //ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lbActionCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        protected void ibFilterSurvey_Click(object sender, EventArgs e)
        {
            RefreshListView();
        }

        protected void lvSurvey_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    string[] args = e.CommandArgument.ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    string assessment_id = args[0]; // assessment id
                    string company_id = args[1]; // company id

                    Response.Redirect(string.Format("/Assessment/SubscriptionEdit/{0}/{1}", company_id, assessment_id), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Analytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Survey/Analytic/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                //else if (e.CommandName.Equals("DuplicateSubscription", StringComparison.InvariantCultureIgnoreCase))
                //{
                //    //Response.Redirect(string.Format("/Survey/Analytic/{0}", e.CommandArgument), false);
                //    Context.ApplicationInstance.CompleteRequest();
                //}
                else if (e.CommandName.Equals("DeleteSubscription", StringComparison.InvariantCultureIgnoreCase))
                {
                    string[] args = e.CommandArgument.ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    LinkButton lbSurveyName = e.Item.FindControl("lbSurveyName") as LinkButton;
                    Literal CategoryLiteral = e.Item.FindControl("CategoryLiteral") as Literal;

                    hfAssessmentId.Value = args[0];
                    hfCompanyId.Value = args[1];

                    ltlActionMsg.Text = "Do you want to delete this subscription: <br/><br/> ";
                    ltlActionMsg.Text = ltlActionMsg.Text + "<strong>Company</strong> : " + lbSurveyName.Text + "<br />";
                    ltlActionMsg.Text = ltlActionMsg.Text + "<strong>Assessment</strong> : " + CategoryLiteral.Text + "<br />";

                    mpePop.PopupControlID = "popup_actiontopic";
                    mpePop.Show();
                    Context.ApplicationInstance.CompleteRequest();
                }

                #region comment out
                //else
                //{
                //    LinkButton lbSurveyName = e.Item.FindControl("lbSurveyName") as LinkButton;
                //    System.Web.UI.WebControls.Image imgSurvey = e.Item.FindControl("imgSurvey") as System.Web.UI.WebControls.Image;

                //    hfSurveyId.Value = e.CommandArgument.ToString().Split(',')[0];
                //    hfCategoryId.Value = e.CommandArgument.ToString().Split(',')[1];
                //    hfSurveyName.Value = lbSurveyName.Text;

                //    if (e.CommandName.Equals("Activate", StringComparison.InvariantCultureIgnoreCase))
                //    {
                //        lbAction.CommandArgument = "2";
                //        ltlActionName.Text = "Activate ";
                //        lbAction.Text = "Activate";
                //        ltlActionMsg.Text = "Activate <b>" + lbSurveyName.Text + "</b>. Confirm?";

                //    }
                //    else if (e.CommandName.Equals("Hide", StringComparison.InvariantCultureIgnoreCase))
                //    {
                //        lbAction.CommandArgument = "3";
                //        ltlActionName.Text = "Hide ";
                //        lbAction.Text = "Hide";
                //        ltlActionMsg.Text = "Hide <b>" + lbSurveyName.Text + "</b>. Confirm?";
                //    }
                //    else if (e.CommandName.Equals("DeleteSurvey", StringComparison.InvariantCultureIgnoreCase))
                //    {
                //        lbAction.CommandArgument = "-1";
                //        ltlActionName.Text = "Delete ";
                //        lbAction.Text = "Delete";
                //        ltlActionMsg.Text = "All questions within <b>" + lbSurveyName.Text + "</b> will be gone. Confirm?";
                //    }

                //    imgActionSurvey.ImageUrl = imgSurvey.ImageUrl;
                //    ltlActionSurveyTitle.Text = lbSurveyName.Text;
                //    mpePop.Show();
                //}
                #endregion comment out
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        protected void lvSurvey_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            this.RefreshListView();
        }
        protected void lvSurvey_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

        }
        protected void lvSurvey_ItemCreated(object sender, ListViewItemEventArgs e)
        {

        }
        protected void lvSurvey_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }

        protected void lbAddSubscription_Click(object sender, EventArgs e)
        {
            // Configure display 

            ddlCompany.DataSource = this.adminInfo.Companies;
            ddlCompany.DataTextField = "CompanyTitle";
            ddlCompany.DataValueField = "CompanyId";
            ddlCompany.DataBind();

            AssessmentSelectAllResponse response = asc.SelectAllAssessments(this.adminInfo.UserId);
            if (response.Success)
            {
                ddlAssessment.DataSource = response.Assessments;
                ddlAssessment.DataTextField = "Title";
                ddlAssessment.DataValueField = "AssessmentId";
            }
            else
            {
                ddlAssessment.DataSource = null;
            }
            ddlAssessment.DataBind();

            // Display popup
            mpePop.PopupControlID = "popup_addcategory";
            mpePop.Show();
            Context.ApplicationInstance.CompleteRequest();
        }

        public void lbAddDone_Click(object sender, EventArgs e)
        {
            // User input validation
            if (string.IsNullOrWhiteSpace(ddlCompany.SelectedValue) || string.IsNullOrWhiteSpace(ddlAssessment.SelectedValue))
            {
                mpePop.Show();
                MessageUtility.ShowToast(this.Page, "Company and Assessment cannot be empty.", MessageUtility.TOAST_TYPE_ERROR);
                return;
            }

            try
            {
                // Redirect user to Subscription page
                Response.Redirect(string.Format("~/Assessment/SubscriptionCreate/{0}/{1}", ddlCompany.SelectedValue, ddlAssessment.SelectedValue));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void lbPopCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

    }
}