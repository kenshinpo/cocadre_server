﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SubscriptionList.aspx.cs" Inherits="AdminWebsite.Assessment.SubscriptionList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
        <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfSurveyId" runat="server" />
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfSurveyName" runat="server" />
            <asp:HiddenField ID="hfAssessmentId" runat="server" />
            <asp:HiddenField ID="hfCompanyId" runat="server" />


            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <asp:LinkButton ID="lbAddSubscription" ClientIDMode="AutoID" runat="server" CommandName="AddSubscription" Text="Add subscription" CssClass="mfb-component__button--main" data-mfb-label="Add subscription" OnClick="lbAddSubscription_Click">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </asp:LinkButton>
                </li>
            </ul>
            <!-- /Floating Action Button -->


            <!-- Active: Hide, Delete Survey -->
            <!-- Add Subscription -->
            <asp:Panel ID="popup_addcategory" runat="server" CssClass="popup popup--addcategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Add subscription</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Company</div>
                                <div class="form__row">
                                    <div class="mdl-selectfield">
                                        <asp:DropDownList runat="server" ID="ddlCompany" AppendDataBoundItems="true">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="label">Assessment</div>
                                <div class="form__row">
                                    <div class="mdl-selectfield">
                                        <asp:DropDownList runat="server" ID="ddlAssessment" AppendDataBoundItems="true">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAddDone" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbAddDone_Click" OnClientClick="ShowProgressBar();" Text="Subscribe" />
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Add Subscription -->

            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Assessment
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbAction_Click" OnClientClick="ShowProgressBar();" Text="Delete" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Delete Survey -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/" style="color: #000;">Assessment <span>console</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="surveyCountLiteral" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
            <div style="font-size: small;">
                <span style="color: black">Status</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #FECA02"></i>Draft&nbsp;&nbsp;&nbsp;&nbsp;
                <i class="fa fa-circle" style="color: #94CF00"></i>Live&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/List">Assessments</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/Coach">Coach</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Assessment/Subscription">Subscription</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Company</label>
                        <asp:TextBox ID="tbFilterSurvey" runat="server" placeholder="Type the company's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterSurvey" runat="server" OnClick="ibFilterSurvey_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvSurvey" runat="server" OnItemCommand="lvSurvey_ItemCommand" OnSorting="lvSurvey_Sorting" OnItemDataBound="lvSurvey_ItemDataBound" OnItemCreated="lvSurvey_ItemCreated" OnItemEditing="lvSurvey_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 40px;"></th>
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortCompany" ID="lbSortCompany" OnClientClick="ShowProgressBar('Filtering');">Company</asp:LinkButton></th>
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="lbSortAssessment" ID="lbSortAssessment" OnClientClick="ShowProgressBar('Filtering');">Assessment</asp:LinkButton></th>
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" ID="lbSortReportButton"></asp:LinkButton></th>
                                        <th class="no-sort"></th>
                                        <th class="no-sort" style="width: 5%;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField runat="server" ID="SurveyIdHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "Company.CompanyTitle") %>' />
                                    <div class="constrained" style="display: inline-block;">
                                        <asp:Image ID="imgSurvey" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Company.CompanyLogoUrl") %>' runat="server" />
                                    </div>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbSurveyName" Text='<%# DataBinder.Eval(Container.DataItem, "Company.CompanyTitle") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Company.CompanyTitle") + "/"   %>' Style="color: #999;" />
                                </td>
                                <td>
                                    <asp:Literal ID="CategoryLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Assessment.Title") %>'></asp:Literal>
                                </td>
                                <td>
                                    <%--<asp:Literal Mode="PassThrough" Text="<i class='fa fa-circle' style='color:#94CF00'></i> Active" runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "Status").ToString() == "2" %>' />
                                    <asp:Literal Mode="PassThrough" Text="<i class='fa fa-circle' style='color:#FECA02'></i> Unlisted/Hidden" runat="server" Visible='<%# ((DataBinder.Eval(Container.DataItem, "Status").ToString() == "1") || (DataBinder.Eval(Container.DataItem, "Status").ToString() == "3")) %>' />--%>
                                    <asp:Button Text="Report" runat="server" />
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Assessment.AssessmentId") + "," + DataBinder.Eval(Container.DataItem, "Company.CompanyId") %>' Text="Edit subscription"  />
                                                    </li>
                                                    <%--<li>
                                                        <asp:LinkButton ID="lbDuplicate" ClientIDMode="AutoID" runat="server" CommandName="DuplicateSubscription" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Assessment.Title") %>' Text="Duplicate subscription" />
                                                    </li>--%>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="DeleteSubscription" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Assessment.AssessmentId") + "," + DataBinder.Eval(Container.DataItem, "Company.CompanyId") %>' Text="Delete subscription" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
