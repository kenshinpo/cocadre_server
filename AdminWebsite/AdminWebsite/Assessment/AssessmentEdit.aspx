﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AssessmentEdit.aspx.cs" Inherits="AdminWebsite.Assessment.AssessmentEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="/Css/Calendar.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <style>
        #main_content_UpdatePanel6 {
            margin-bottom: 20px;
            margin-top: 20px;
        }

            #main_content_UpdatePanel5 input, #main_content_UpdatePanel5 label, #main_content_UpdatePanel6 input, #main_content_UpdatePanel6 label {
                display: inline-block;
                width: auto;
            }

                #main_content_UpdatePanel5 label:first-child, #main_content_UpdatePanel6 label:first-child {
                    display: block;
                }


        .field-label {
            width: 115px;
            display:inline-block;
        }
        .no-bottom-margin {
            margin-bottom: 0 !important;
        }
        .mdl-selectfield {
            width: 170px;
            display: inline-block;
        }

            .mdl-selectfield select {
                padding: 0.65em;
                margin-bottom: 0px;
            }

        #main_content_UpdatePanel6 input.time, #main_content_UpdatePanel5 input.time {
            width: 80px;
            text-align: center;
        }

        .duration_tip, .anonymous_tip {
            position: absolute;
            /*top: 0px;*/
            /*right: 0px;*/
            left: 90px;
            opacity: 0;
            display: block !important;
            background: #ffffff;
            width: 275px;
            border: 1px solid #ccc;
            padding: 10px;
            z-index: 1000;
            box-shadow: 0px 0px 10px #cccccc;
            transition: all 0.3s ease;
            -webkit-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
        }

        .form__row.duration__tip:hover .duration_tip {
            opacity: 1;
        }

        .form__row.anonymous__tip:hover .anonymous_tip {
            opacity: 1;
        }

        .allocate-weight {
            display: inline-block !important;
            width: 70px !important;
        }

        .clickable {
            cursor: pointer;
        }
        .clickable:hover {
            color:red;
        }

        .img-container {
            display: inline-block;
            max-width: 100px;
            max-height: 100px;
            vertical-align: top;
        }

        .remove-img-banner {
            background: rgba(0, 0, 0, 0.6);
            padding-top: 5px;
            padding-left: 10px;
            width: 30px;
            height: 30px;
            color: white;
            position: absolute;
            cursor: pointer;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-align-content: center;
            align-content: center;

        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <script type="text/javascript">
        $(function () {
            TabFunction();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            TabFunction();
        })

        /* For search personnel */
        var delayTimer;
        function RefreshUpdatePanel(event) {
            if (event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 91 || event.keyCode == 93 || event.keyCode == 144 || (event.keyCode > 15 && event.keyCode < 21) || (event.keyCode > 32 && event.keyCode < 41) || (event.keyCode > 111 && event.keyCode < 124)) {
                return false;
            }
            clearTimeout(delayTimer);
            delayTimer = setTimeout(function () {

                <%--__doPostBack('<%= tbSearchKey.ClientID %>', '');--%>
            }, 1000);

            var progress = $('#imgLoading');
            progress.fadeToggle();
        }

        $(document).on("keydown", function (e) {
            if (e.which === 8 && !$(e.target).is("input, textarea")) {
                e.preventDefault();
            }
        });

        function showLoading() {
            var progress = $('#imgLoading');

            progress.fadeToggle();
        }

        function SetFocus() {
            <%--var textBox = document.getElementById('<%= tbSearchKey.ClientID %>');--%>
            var elemLen = textBox.value.length;
            if (document.selection) {
                // Set focus
                // Use IE Ranges
                var oSel = document.selection.createRange();
                // Reset position to 0 & then set at end
                oSel.moveStart('character', -elemLen);
                oSel.moveStart('character', elemLen);
                oSel.moveEnd('character', 0);
                oSel.select();
            }
            else if (textBox.selectionStart || textBox.selectionStart == '0') {
                // Firefox/Chrome
                textBox.selectionStart = elemLen;
                textBox.selectionEnd = elemLen;
            }
            textBox.focus();
        }
        /* For search personnel */

        function cardObjectToJson(type) {
            switch (type) {
                case 1: // NewCard
                    document.getElementById('<%= hfNewCardJson.ClientID%>').value = JSON.stringify(NewCard);
                    break;
                default:
                    break
            }
        }

    </script>
    <script src="/js/Assessment/AssessmentEdit.js"></script>

    <asp:HiddenField ID="hfNewCardJson" runat="server" />
    <asp:HiddenField ID="hfTopicId" runat="server" />
    <asp:HiddenField ID="hfCategoryId" runat="server" />
    <asp:HiddenField ID="hfAdminUserId" runat="server" />
    <asp:HiddenField ID="hfCompanyId" runat="server" />
    <asp:HiddenField ID="hfAssessment" runat="server" />
    <asp:HiddenField ID="hfAssessmentUpload" runat="server" />

    <script src="/js/hammer.min.js"></script>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/Assessment/List" style="color: #000;">Assessment <span>console</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlActionName" runat="server" Text="Edit assessment" />
        </div>
        <div class="appbar__action">
            <%--<asp:LinkButton ID="lbPreview" runat="server" CssClass="btn secondary" OnClick="lbPreview_Click">Assessment Preview</asp:LinkButton>--%>
        </div>
    </div>
    <!-- / App Bar -->

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfJsonStore" runat="server" />
            <asp:HiddenField ID="hfLiveSurveyPreviewJson" runat="server" />

            <!-- Reveal: Add Survey Icon -->
            <asp:Panel ID="popup_addtopicicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Survey Icon</h1>
                <div class="topicicons">
                    <p>Select from list</p>
                    <asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand" OnItemCreated="rtIcon_ItemCreated">
                        <HeaderTemplate>
                            <div class="topicicons__list">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <label class="topicicons__icon">
                                <asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
                                    <asp:Image ID="imgChooseIcon" runat="server" />
                                </asp:LinkButton>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbIconCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Add Survey Icon -->

            <!-- Reveal: Alert Popup -->
            <asp:Panel ID="popup_alertpopup" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Edit Survey</h1>
                <div class="topicicons" style="text-align: center; border-top-style: none;">
                    <label style="background: rgba(0, 117, 254, 1); margin: -15px auto 0px; padding: 5px; border-radius: 24px; width: 200px; color: white;">Survey is Active and Live/Hidden</label><br />
                    <label style="color: red;">
                        Editing will affect the Analytics,
                        <br />
                        therefore some features will be locked.</label>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAlertPopupCancel" CssClass="popup__action__item popup__action__item" runat="server" Text="OK" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Alert Popup -->

            <!--  Live Survey Preview -->
            <link href="/css/ion.rangeSlider/ion.rangeSlider.css" rel="stylesheet" />
            <link href="/css/ion.rangeSlider/ion.rangeSlider.skinNice.css" rel="stylesheet" />
            <asp:Panel ID="popup_livesurveypreview" runat="server" CssClass="popup " Width="100%" Style="display: none;">
                <h1 class="popup__title">Live Survey Preview</h1>
                <style>
                    .popup_phonepreview .question,
                    .popup_phonepreview .answer,
                    .popup_phonepreview .mcq-answer {
                        margin: 6px;
                    }

                    .popup_phonepreview .content {
                        background-color: #eee;
                        border: solid #eee 4px;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .popup_phonepreview .answer .content:hover {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .popup_phonepreview .mcq-answer .content:hover {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    table.mcq tbody tr td.text div.answer,
                    .preview-body .question .content {
                        background-color: #eee;
                        border: solid #eee 4px;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .preview-body .question .plain-content {
                        background-color: transparent;
                        border: solid transparent 4px;
                        padding: 4px;
                        text-align: center;
                    }

                    .preview-body .answer .content {
                        background-color: #eee;
                        border: solid #eee 4px;
                        border-radius: 8px;
                        padding: 4px;
                    }

                    .preview-body .content-selected {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        text-align: left;
                    }

                    .preview-body .content-instruction {
                        background-color: #ffd800 !important;
                        border: solid #ffd800 4px !important;
                        text-align: center;
                    }

                    table.mcq td {
                        margin: 0;
                        padding: 0;
                    }

                        table.mcq td.checkbox {
                            width: 20px;
                        }

                        table.mcq td.text {
                            padding-left: 10px;
                        }

                    .mcq-instruction {
                        font-size: smaller;
                        background-color: #eee;
                        padding: 0 1em;
                    }

                    .tbNewCardOptionRangeMin, .tbNewCardOptionRangeMax, .tbNewCardOptionRangeMiddle, #lblNewCardOptionRangeMiddleMin {
                        display: inline-block;
                        width: 100px !important;
                        vertical-align: middle;
                        margin-left: 10px;
                    }

                    .tbNewCardOptionRangeMinLabel, .tbNewCardOptionRangeMaxLabel, .tbNewCardOptionRangeMiddleLabel {
                        display: inline-block;
                        margin-bottom: 5px !important;
                        padding: 10px 0px !important;
                        width: 92% !important;
                    }

                    #sltRangeNonmiddle, .maxminSelect {
                        border: 0px;
                        border-bottom: #ccc 1px solid;
                        width: 100%;
                        padding: 5px 0px;
                    }

                    label.tip {
                        color: #ccc;
                        font-size: 12px;
                    }

                    .inline-label {
                        display: inline-block;
                    }

                    label.inline-label.radio {
                        width: 195px;
                    }

                    .option_section {
                        margin-bottom: 30px;
                    }

                        .option_section .survey-lettercount {
                            float: none;
                        }

                    .remove-option-link {
                        float: right;
                        color: #fff;
                        background: #ed4444;
                        border: 1px solid #ed4444;
                        padding: 2px 10px;
                        font-size: 10px;
                        line-height: 16px;
                        transition: all 0.3s ease;
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                    }

                        .remove-option-link:hover {
                            background: #ffffff;
                            color: #ed4444;
                            border: 1px solid #ed4444;
                        }

                    .add-option-link {
                        float: right;
                        color: #fff;
                        background: #74d257;
                        padding: 5px 15px;
                        border: 1px solid #74d257;
                        font-size: 12px;
                        line-height: 16px;
                        transition: all 0.3s ease;
                        -webkit-transition: all 0.3s ease;
                        -moz-transition: all 0.3s ease;
                        -ms-transition: all 0.3s ease;
                    }

                        .add-option-link:hover {
                            background: #ffffff;
                            color: #74d257;
                            border: 1px solid #74d257;
                        }

                    .question-image-content, .option-image-content, .cardContentImageWrapper {
                        visibility: visible;
                        height: auto;
                        overflow: auto;
                        border: 1px solid #ccc;
                        text-align: center;
                        background: url('/Img/grid.png') repeat;
                        /*display: flex;
                        display: -webkit-box;
                        display: -ms-flexbox;
                        align-content: center;
                        justify-content: center;*/
                    }

                        .question-image-content img, .option-image-content img, .cardContentImageWrapper img {
                            /*padding: 30px;*/
                            width: auto;
                            height: auto;
                            margin: 0 auto;
                        }

                    .survey-cross {
                        padding-top: 0px;
                        padding-left: 0px;
                        align-items: center;
                        justify-content: center;
                    }

                        .survey-cross i {
                            color: #fff;
                        }
                    /*
                    table.mcq tbody tr{
                        border: 2px dashed transparent;
                    }
                    table.mcq tbody tr:hover {
                        border: 2px dashed red;
                    }
                    */
                </style>
                <script type="text/javascript" src="/Js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
                <script type="text/javascript">
                    var data = [], currentQuestionIndex, topicJson, currentTabulationIndex;

                    function chooseOne(div) {
                        var q = model.preview.assessment.Cards[currentQuestionIndex];

                        if ($(div).hasClass("content-selected")) {
                            $(div).removeClass("content-selected");
                            if ($(div).parent().parent().find("div.answer div.content-selected").length <= 0)
                                setPreviewCardFooterDisplay(q);
                        } else {

                            $(div).parent().parent().find("div.answer div.content-selected").removeClass("content-selected");
                            $(div).addClass("content-selected");

                        }
                    }
                    function checkChange(checkbox) {
                        var q = model.preview.assessment.Cards[currentQuestionIndex];

                        if ($(checkbox).prop("checked") == false) {
                            $(checkbox).parent().parent().removeClass("content-selected");
                            if ($(checkbox).parent().parent().parent().parent().find(":checked").length <= 0)
                                setPreviewCardFooterDisplay(q);
                        } else {
                            $(".popup_phonepreview .footer-button").hide();
                            if (q.HasPageBreak) {
                                $(".popup_phonepreview .next-page-button").show();
                            } else {
                                $(".popup_phonepreview .next-question-button").show();
                            }
                        }
                    }
                    function hidePreviewOverlay(div) {
                        $(".popup_phonepreview div.popup-overlay").hide();
                    }
                    function displayPreviewImage(div) {
                        $(".popup_phonepreview div.popup-overlay div.message").html($(div).parent().children("span").text());
                        $(".popup_phonepreview div.popup-overlay div.message").show();
                        $(".popup_phonepreview div.popup-overlay img")[0].src = div.src;
                        $(".popup_phonepreview div.popup-overlay img").show();
                        $(".popup_phonepreview div.popup-overlay").show();
                    }
                    function displayInfo(fa) {
                        $(".popup_phonepreview div.popup-overlay img").hide();
                        $(".popup_phonepreview div.popup-overlay div.message").html("<h1>Info</h1>" + $(fa).data("note"));
                        $(".popup_phonepreview div.popup-overlay div.message").show();
                        $(".popup_phonepreview div.popup-overlay").show();
                    }

                    function nextButtonAction() {
                        var idx, qIdx,
                            ansid, q = model.preview.assessment.Cards[currentQuestionIndex];

                        if (q.ToBeSkipped == true) {
                            currentQuestionIndex++;
                            if (model.preview.assessment.Cards.length == currentQuestionIndex) {
                                currentTabulationIndex = 0;
                                displayClosing(model.preview.Assessment.Tabulations[currentTabulationIndex], model.preview.Assessment.Tabulations);
                            } else {
                                displayQuestion(model.preview.assessment.Cards[currentQuestionIndex]);
                            }
                                
                            return;
                        }

                        // Validate question
                        if (q.Type == 1) { // selectone
                            if ($("div.answer div.content-selected").length <= 0) {
                                return; // don't know to display error message or do nothing (ie. return)
                            }

                            if (q.HasToLogic) {
                                ansid = $("div.answer div.content-selected").data("answer");

                                // search for card and display that card
                                for (idx = 0; idx < model.preview.assessment.Cards.length; idx++) {
                                    if (data[idx].CardId === ansid) {
                                        currentQuestionIndex = idx;
                                        displayQuestion(model.preview.assessment.Cards[currentQuestionIndex]);
                                        break;
                                    }
                                }
                                return;
                            }

                        } else if (q.Type == 2) { // multi-choice
                            var checkedItemCount = $(".mcq-answer input[type=checkbox]:checked").length;

                            if ((checkedItemCount < q.MiniOptionToSelect) || (checkedItemCount > q.MaxOptionToSelect)) {
                                return; // don't know to display error message or do nothing (ie. return)
                            }

                            //if (q.HasToLogic) { // This logic is flawed for multi-choice because of ambiguity of how logic is defined for question
                            //    ansid = $("div.mcq-answer div.content-selected").data("answer");

                            //    // search for card and display that card
                            //    for (idx = 0; idx < data.length; idx++) {
                            //        if (data[idx].CardId === ansid) {
                            //            currentQuestionIndex = idx;
                            //            displayQuestion(data[currentQuestionIndex]);
                            //            break;
                            //        }
                            //    }
                            //    return;
                            //}

                        } else if (q.Type == 5) { // drop list

                            if ($("div.answer select option:selected").val() == "") {
                                return; // don't know to display error message or do nothing (ie. return)
                            }

                            // No logic for drop list
                            //if (q.logic.length > 0) {
                            //    ansid = $("div.answer select option:selected").val();
                            //    for (idx = 0; idx < q.logic.length; idx++) {
                            //        if (q.logic[idx].when == ansid) { // Then need to find the index of goto question here
                            //            for (qIdx = currentQuestionIndex; qIdx < data.length; qIdx++) {
                            //                if (q.logic[idx].goto == data[qIdx].CardId) {
                            //                    currentQuestionIndex = qIdx;
                            //                    displayQuestion(data[currentQuestionIndex]);
                            //                    break;
                            //                }
                            //            }
                            //            break;
                            //        }
                            //    }
                            //    return;
                            //}
                        }

                        // Assume validation cleared; proceed to the next question
                        currentQuestionIndex++;
                        if (model.preview.assessment.Cards.length == currentQuestionIndex) {
                            currentTabulationIndex = 0;
                            var resultTabulation = model.preview.Assessment.Tabulations[currentTabulationIndex];
                            displayClosing(resultTabulation, model.preview.Assessment.Tabulations);
                        } else {
                            displayQuestion(model.preview.assessment.Cards[currentQuestionIndex]);
                        }
                    }

                    function displaySelectQuestion(q) {
                        var html, idx;

                        // Remove all question/options
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display question
                        html = "<div class=\"question\">" +
                                "	<div class=\"plain-content\">" + q.Content + "</div>";
                        for (idx = 0; idx < q.CardImages.length; idx++) {
                            html = html + "    <div style=\"width:100%; text-align:center;\" class=\"image\">" +
                                            "        <img style=\"width:250px;vertical-align:middle\" src=\"" + q.CardImages[idx].ImageUrl + "\" onclick=\"displayPreviewImage(this);\">" +
                                            "    </div>";
                        }
                        html = html + "</div>";
                        $(".popup_phonepreview .preview-body").append(html);

                        // Display answer(s)
                        for (idx = 0; idx < q.Options.length; idx++) {
                            html = "<div class=\"answer\">" +
                                    "	<div class=\"content\" onclick=\"chooseOne(this);\"  data-answer=\"" + q.Options[idx].NextCardId + "\" >";

                            if ((q.Options[idx].hasOwnProperty("Images")) && (q.Options[idx].Images.length > 0)) {
                                html = html + "        <img style=\"width:50px;vertical-align:middle\" src=\"" + q.Options[idx].Images[0].ImageUrl + "\" onclick=\"displayPreviewImage(this);\">";
                            }

                            html = html + "        <span>" + q.Options[idx].Content + "</span>" +
                                    "	</div>" +
                                    "</div>";
                            $(".popup_phonepreview .preview-body").append(html);
                        }
                    }
                    function displayMcqQuestion(q) {
                        var html, idx;

                        // Remove all question/options
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display question
                        html = "<div class=\"question\">" +
                                "	<div class=\"plain-content\">" + q.Content + "</div>";
                        for (idx = 0; idx < q.CardImages.length; idx++) {
                            html = html + "    <div style=\"width:100%; text-align:center;\" class=\"image\">" +
                                            "        <img style=\"width:250px;vertical-align:middle\" src=\"" + q.CardImages[idx].ImageUrl + "\">" +
                                            "    </div>";
                        }
                        html = html + "</div>";
                        $(".popup_phonepreview .preview-body").append(html);


                        // Decide what to display for multi-choice instructions
                        $(".popup_phonepreview .preview-body").append("<div class=\"mcq-instruction\">Select all that apply</div>");
                        //if ((q.MiniOptionToSelect == 0) && (q.MaxOptionToSelect == q.Options.length))
                        //    $(".popup_phonepreview .preview-body").append("<div class=\"mcq-instruction\">Select all that apply</div>");
                        //else
                        //    $(".popup_phonepreview .preview-body").append("<div class=\"mcq-instruction\">Selection: Min: " + q.MiniOptionToSelect + "&nbsp;&nbsp;&nbsp;&nbsp;Max: " + q.MaxOptionToSelect + "</div>");

                        // Display answer(s)
                        for (idx = 0; idx < q.Options.length; idx++) {
                            html = "<div class=\"mcq-answer\">" +
                                    "	<label class=\"content\" data-answer=\"" + q.Options[idx].NextCardId + "\" >" +
                                    "        <div style=\"float:right;\"><input type=\"checkbox\" onchange=\"checkChange(this);\" /></div> ";

                            if ((q.Options[idx].hasOwnProperty("img")) && (q.Options[idx].img.length > 0)) {
                                html = html + "        <img style=\"width:50px;vertical-align:middle\" src=\"" + q.Options[idx].img + "\">";
                            }

                            html = html + "        <span>" + q.Options[idx].Content + "</span>" +
                                        "	</label>" +
                                    "</div>";
                            $(".popup_phonepreview .preview-body").append(html);

                            //$(".popup_phonepreview .preview-body").append(
                            //    "<div class=\"mcq-answer\">" +
                            //    "	<label class=\"content\">" +
                            //    "        <div style=\"float:right;\"><input type=\"checkbox\" /></div>" + 
                            //    "		<div class=\"mcq-text\">" + q.Options[idx] + "</div>" +
                            //    "	</label>" +
                            //    "</div>");
                        }
                    }
                    function displayListQuestion(q) {

                        // Display question text in yellow div
                        $(".popup_phonepreview .preview-body").children().remove();
                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"question\">" +
                            "	<div class=\"content\">" + q.Content +
                            "	</div>" +
                            "</div>");

                        var html, idx;
                        html = "<div class=\"answer\">" +
                        "	<div class=\"content mdl-selectfield\">" +
                        "		<select>";
                        html = html + "			<option value=\"\">Please select</option>";
                        for (idx = 0; idx < q.Options.length; idx++) {
                            html = html + "			<option value=\"" + q.Options[idx].OptionId + "\">" + q.Options[idx].Content + "</option>";
                        }
                        html = html + "		</select>" +
                        "	</div>" +
                        "</div>";

                        $(".popup_phonepreview .preview-body").append(html);

                    }
                    function displayRangeQuestion(q) {

                        $(".popup_phonepreview .preview-body").children().remove();

                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"question\">" +
                            "	<div class=\"content\">" + q.Content +
                            "	</div>" +
                            "</div>");

                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"answer\">" +
                            "   <div class=\"content\">" +
                            "       <input type=\"text\" id=\"inputRange\" name=\"inputRange\" value=\"\" />" +
                            "   </div>" +
                            "</div>");

                        var rangeIdx, rangeValues;
                        rangeValues = [];
                        for (rangeIdx = q.MinRange; rangeIdx <= q.MaxRange; rangeIdx++) {
                            rangeValues.push(rangeIdx);
                        }

                        //$("#inputRange").ionRangeSlider({
                        //    min: q.MinRange,
                        //    max: q.MaxRange,
                        //    from: q.MinRange
                        //});

                        $("#inputRange").ionRangeSlider({
                            min: q.MinRange,
                            max: q.MaxRange,
                            from: q.MinRange,
                            values: rangeValues,
                            onFinish: function (data) {
                                console.log("onFinish");

                                if ((q.MinRangeLabel) && (q.MinRangeLabel.length > 0)) {
                                    $(".preview-body .content .irs-min").text(q.MinRangeLabel);
                                } else {
                                    $(".preview-body .content .irs-min").text(q.MinRange.toString());
                                }

                                if ((q.MaxRangeLabel) && (q.MaxRangeLabel.length > 0)) {
                                    $(".preview-body .content .irs-max").text(q.MaxRangeLabel);
                                } else {
                                    $(".preview-body .content .irs-max").text(q.MaxRange.toString());
                                }
                            }
                        });

                        if ((q.MinRangeLabel) && (q.MinRangeLabel.length > 0)) {
                            $(".preview-body .content .irs-min").text(q.MinRangeLabel);
                        } else {
                            $(".preview-body .content .irs-min").text(q.MinRange.toString());
                        }

                        if ((q.MaxRangeLabel) && (q.MaxRangeLabel.length > 0)) {
                            $(".preview-body .content .irs-max").text(q.MaxRangeLabel);
                        } else {
                            $(".preview-body .content .irs-max").text(q.MaxRange.toString());
                        }
                    }
                    function displayTextQuestion(q) {

                        // Display question text in yellow div
                        $(".popup_phonepreview .preview-body").children().remove();
                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"question\">" +
                            "	<div class=\"content\">" + q.Content +
                            "	</div>" +
                            "</div>");
                        $(".popup_phonepreview .preview-body").append(
                            "<div class=\"answer\">" +
                            "   <div class=\"content\">" +
                            "       <textarea rows=\"8\" placeholder=\"Write here\"></textarea>" +
                            "   </div>" +
                            "</div>");
                    }
                    function displayInstructionQuestion(q) {

                        var idx, html = "";
                        // Display question text in yellow div
                        $(".popup_phonepreview .preview-body").children().remove();

                        html = "<div class=\"question\">" +
                            "	<div class=\"content content-instruction\" style=\"margin-bottom:1em;\" >" + q.Content +
                            "	</div>";

                        if (q.CardImages.length > 0) {
                            for (idx = 0; idx < q.CardImages.length; idx++) {
                                html = html + "<div style=\"text-align:center\"><img style=\"width:50px;vertical-align:middle\" src=\"" + q.CardImages[0].ImageUrl + "\" onclick=\"displayPreviewImage(this);\"></div>";
                            }
                        }

                        html = html + "</div>";

                        $(".popup_phonepreview .preview-body").append(html);

                        //$(".popup_phonepreview .preview-body").append(
                        //    "<div class=\"question\">" +
                        //    "	<div class=\"content content-instruction\">" + q.Content +
                        //    "	</div>" +
                        //    "</div>");

                    }

                    function displayIntroduction(topic) {
                        var html;

                        $(".popup_phonepreview .progress-bar-indication").hide();

                        // Remove all question/options
                        $(".popup_phonepreview .preview-info-bar").children().remove();
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display
                        html = "";
                        html = html + "    <div class=\"preview-title\" style=\"display:flex; align-items:center; background-color:#FF9800;height:36px; color:#fff;\">";
                        html = html + "        <div class=\"flex-cell\" style=\"flex:1; margin-left:.5em;\">";
                        html = html + "            <i class=\"fa fa-angle-left fa-2x\" aria-hidden=\"true\"></i>";
                        html = html + "        </div>";
                        html = html + "        <div class=\"flex-cell\" style=\"flex:1; text-align:center;\">" + topic.Assessment.Title + "</div>";
                        html = html + "        <div class=\"flex-cell\" style=\"flex:1;\">&nbsp;</div>";
                        html = html + "    </div>";
                        html = html + "    <div class=\"preview-banner-list\" style=\"display:flex; flex-direction:column; align-items:center; text-align:center; background-color:#ccc;\">";
                        html = html + "        <img src=\"http://placehold.it/280x100/ccc/fff/?text=IMAGE\" style=\"flex:1;\" /> ";
                        html = html + "        <div style=\"color:#fff;background-color: #ccc;flex:1; width:100%;\">";
                        html = html + "            <i class=\"fa fa-circle\" aria-hidden=\"true\"></i>";
                        html = html + "            <i class=\"fa fa-circle\" aria-hidden=\"true\"></i>";
                        html = html + "            <i class=\"fa fa-circle\" aria-hidden=\"true\"></i>";
                        html = html + "            <i class=\"fa fa-circle\" aria-hidden=\"true\"></i>";
                        html = html + "            <i class=\"fa fa-circle\" aria-hidden=\"true\"></i>";
                        html = html + "            </div>";
                        html = html + "    </div>";
                        html = html + "    &nbsp;";
                        html = html + "    <div class=\"preview-assessment-intro\" style=\"display:flex; align-items:center; margin: 0 5px 0; padding: 0 4px 0; color:#ccc;\">";
                        html = html + "        <div> ";
                        html = html + "            <img align=\"left\" src=\"http://placehold.it/50x50/ccc/fff/?text=ICON\" style=\"width:50px;vertical-align:top;margin:6px;\">";
                        html = html + "            <div style=\"font-size:smaller;margin-left: .5em;display:inline;\">";
                        html = html + "                <span>" + topic.Assessment.Title + "</span><br/>";
                        html = html + "                <span>" + topic.Assessment.Coach.FirstName + "</span><br/>";
                        html = html + "                <span style=\"clear:both\">" + topic.Assessment.Introduction + "</span>";
                        html = html + "            </div>";
                        html = html + "        </div>";
                        html = html + "    </div>";
                        html = html + "    &nbsp;";
                        html = html + "    <div class=\"preview-video\" style=\"display:flex; flex-direction:column; align-items:center; text-align:center;\" >";
                        html = html + "        <div style=\"font-size:smaller;color:#ccc; flex:1; width:100%; text-align:left;\">" + topic.Assessment.VideoTitle + "</div>";
                        html = html + "        <img src=\"http://placehold.it/280x100/ccc/fff/?text=VIDEO\" style=\"flex:1;\" /> ";
                        html = html + "        <div style=\"font-size:smaller;color:#ccc; flex:1; width:100%; text-align:left;\">" + topic.Assessment.VideoDescription + "</div>";
                        html = html + "    </div>";
                        html = html + "    &nbsp;";

                        $(".popup_phonepreview .preview-body").append(html);

                        // Remove all buttons except for Start button
                        $(".popup_phonepreview .footer-button").hide();
                        $(".popup_phonepreview .start-survey-button").show();
                    }
                    function displayClosing(topic, tabs) {
                        var tabIdx, imgIdx;
                        $(".popup_phonepreview .progress-bar-indication").hide();

                        // Remove all question/options
                        $(".popup_phonepreview .preview-info-bar").children().remove();
                        $(".popup_phonepreview .preview-body").children().remove();

                        // Display
                        //html = "<div style=\"position: absolute;top: 40%;text-align: center;width: 100%;\">" +
                        //        "	<h2>" + topic.ClosingWords + "</h2>" +
                        //        "</div>"
                        html = "";
                        html = html + "<div class=\"preview-title\" style=\"display:flex; align-items:center; background-color:#FF9800;height:36px; color:#fff;\">";
                        html = html + "    <div class=\"flex-cell\" style=\"flex:100; text-align:center;\">Preview Results</div>";
                        html = html + "</div>";
                        html = html + "<div class=\"preview-results-radio\" style=\"display:flex; flex-direction:column; align-items:center; text-align:center;margin-top:4px;\">";
                        html = html + "    <div style=\"display:inline-flex;\">";
                        html = html + "        <label>";
                        html = html + "            <input type=\"radio\" name=\"results_radio\" checked=\"checked\" style=\"margin:0;\" /> Results Page</label>&nbsp;";
                        html = html + "        <label>";
                        html = html + "            <input type=\"radio\" name=\"results_radio\" style=\"margin:0;\" /> Profile Page</label>";
                        html = html + "    </div>";
                        html = html + "</div>";
                        html = html + "<div class=\"preview-results-tabulation\" style=\"text-align:left;margin-left:.5em;\">";
                        html = html + "    <label><strong>Table</strong></label>";
                        html = html + "    <select style=\"width:90%; margin-left: .5em;border:0; border-bottom:solid 1px #ccc;margin:0;padding:0;background-color:transparent;\">";
                        for (tabIdx = 0; tabIdx < tabs.length; tabIdx++) {
                            html = html + "        <option value=\"" + tabs[tabIdx].TabulationId + "\">" + (tabIdx +1).toString() + ". " + tabs[tabIdx].InfoTitle + "</option>";
                        }
                        html = html + "    </select>";
                        html = html + "</div>";
                        html = html + "&nbsp;";
                        html = html + "<div class=\"preview-results-answer\" style=\"margin-left:.5em;\">";
                        html = html + "    <label>" + topic.InfoTitle + "</label>";
                        html = html + "    <div>" + topic.InfoDescription + "</div>";
                        html = html + "    &nbsp;";
                        html = html + "</div>";
                        html = html + "<div style=\"max-width:290px; text-align:center;\">";
                        for (imgIdx = 0; imgIdx < topic.Images.length; imgIdx++) {
                            html = html + "    <img src=\"" + topic.Images[imgIdx].ImageUrl + "\">";
                        }
                        html = html + "</div>";
                        html = html + "<div>";
                        html = html + "    <button type=\"button\" class=\"next-question-button footer-button\" ";
                        html = html + "            style=\"text-align: center; width: 100%; border-radius: 0;position:absolute;bottom:0;\" onclick=\"nextButtonAction();\"><i class=\"fa fa-chevron-down\"></i></button>";
                        html = html + "</div>";
                        $(".popup_phonepreview .preview-body").html("");
                        $(".popup_phonepreview .preview-body").append(html);

                        // Remove all buttons except for Done button
                        $(".popup_phonepreview .footer-button").hide();
                        $(".popup_phonepreview .done-survey-button").show();
                    }

                    function setPreviewCardCustomAnswerDisplay(q) {
                        if (q.HasCustomAnswer) {
                            $(".popup_phonepreview .preview-body").append(
                                "<div class=\"answer\">" +
                                "    <div class=\"plain-content\">" +
                                "        <div class=\"content\" onclick=\"chooseOne(this);\" style=\"display:inline-block;padding:1px;cursor:default;\" >Others:</div>" +
                                "        <input type=\"text\" placeholder=\"Please specify\" autocomplete=\"off\" style=\"display:inline-block;width:initial;padding:9px;\">" +
                                "    </div>" +
                                "</div>"
                            );
                        }
                    }
                    function setPreviewCardHeaderDisplay(q) {
                        // Check whether to display info icon
                        $(".popup_phonepreview .preview-info-bar").children().remove();
                        if (q.Note.length > 0) {
                            $(".popup_phonepreview .preview-info-bar").append("<i class=\"fa fa-info-circle\" data-note=\"" + q.Note + "\" onclick=\"displayInfo(this);\"></i>");
                        }
                    }
                    function setPreviewCardFooterDisplay(q) {
                        $(".popup_phonepreview .footer-button").hide();
                        if (q.HasPageBreak) {
                            $(".popup_phonepreview .next-page-button").show();
                        } else if (q.ToBeSkipped) {
                            $(".popup_phonepreview .skip-question-button").show();
                        } else {
                            $(".popup_phonepreview .next-question-button").show();
                        }
                    }

                    function startSurvey() {
                        model.preview = JSON.parse($("#main_content_hfLiveSurveyPreviewJson").val());
                        model.preview.assessment = model.preview.Assessment;
                        if (model.preview.assessment.Cards.length > 0) {
                            currentQuestionIndex = 0;
                            displayQuestion(model.preview.assessment.Cards[currentQuestionIndex]);
                        }
                    }
                    function closeSurvey() {
                        $find("mpe").hide();
                    }

                    function displayQuestion(q) {   // q should be a question (aka Topic)
                        // All question share a common order of display
                        $(".popup_phonepreview").css("background-image", "url(/Img/bg_survey_" + q.BackgroundType + "a_big.png)");

                        setPreviewCardHeaderDisplay(q);

                        switch (q.Type) {
                            case 1: // "select"
                                displaySelectQuestion(q);
                                break;
                            case 2: // "mcq"
                                displayMcqQuestion(q);
                                break;
                            case 5: // "list"
                                displayListQuestion(q);
                                break;
                            case 4: // "range"
                                displayRangeQuestion(q);
                                break;
                            case 3: // "text"
                                displayTextQuestion(q);
                                break;
                            case 6: // "instruction"
                                displayInstructionQuestion(q);
                                break;
                            case 7: // "accumulation"
                                displaySelectQuestion(q);
                                break;
                        }

                        setPreviewCardCustomAnswerDisplay(q);

                        setPreviewCardFooterDisplay(q);
                    } // end function displayQuestion(q) 

                    function display_preview_intro() {
                                                if ($("#<%=hfLiveSurveyPreviewJson.ClientID %>").val().length > 0) {
                            topicJson = JSON.parse($("#<%=hfLiveSurveyPreviewJson.ClientID %>").val());
                            // comment out for now; use the handcrafted json sample survey
                            data = topicJson.Cards;

                            displayIntroduction(topicJson);


                            // Define touch events (using hammerjs)
                            var myElement = $(".popup_phonepreview")[0];

                            // Create Hammer Manager
                            // A hammer manager is a container of all the recognizer instances for your element. 
                            // It sets up the input event listeners, and sets the touch-action property for you on the element.
                            var mc = new Hammer.Manager(myElement);

                            // Create Recognizers
                            var swipeRecognizer = new Hammer.Swipe({
                                event: "swipe",
                                threshold: 10,
                                velocity: 0.3,
                                direction: 24,
                                pointers: 1
                            });

                            mc.add([swipeRecognizer]);

                            mc.on("swipeup swipedown tap press", function (ev) { // listen to events...
                                var currentCard = model.preview.assessment.Cards[currentQuestionIndex];

                                if ((currentCard === undefined) ||
                                    (currentCard.HasPageBreak) ||
                                    (currentCard.ToBeSkipped)
                                    ) {   // do nothing
                                    return;
                                }

                                // Else advance to next question
                                currentQuestionIndex++;
                                if (data.length == currentQuestionIndex)
                                    displayClosing(topicJson);
                                else
                                    displayQuestion(model.preview.assessment.Cards[currentQuestionIndex]);
                                return;
                                // check question
                            });
                        }
                    }

                    $(document).ready(function () {
                        // read data from main_content_hfJsonStore
                        if ($("#<%=hfLiveSurveyPreviewJson.ClientID %>").val().length > 0) {
                            topicJson = JSON.parse($("#<%=hfLiveSurveyPreviewJson.ClientID %>").val());
                            // comment out for now; use the handcrafted json sample survey
                            data = topicJson.Cards;

                            displayIntroduction(topicJson);


                            // Define touch events (using hammerjs)
                            var myElement = $(".popup_phonepreview")[0];

                            // Create Hammer Manager
                            // A hammer manager is a container of all the recognizer instances for your element. 
                            // It sets up the input event listeners, and sets the touch-action property for you on the element.
                            var mc = new Hammer.Manager(myElement);

                            // Create Recognizers
                            var swipeRecognizer = new Hammer.Swipe({
                                event: "swipe",
                                threshold: 10,
                                velocity: 0.3,
                                direction: 24,
                                pointers: 1
                            });

                            mc.add([swipeRecognizer]);

                            mc.on("swipeup swipedown tap press", function (ev) { // listen to events...
                                var currentCard = model.preview.assessment.Cards[currentQuestionIndex];

                                if ((currentCard === undefined) ||
                                    (currentCard.HasPageBreak) ||
                                    (currentCard.ToBeSkipped)
                                    ) {   // do nothing
                                    return;
                                }

                                // Else advance to next question
                                currentQuestionIndex++;
                                if (data.length == currentQuestionIndex)
                                    displayClosing(topicJson);
                                else
                                    displayQuestion(model.preview.assessment.Cards[currentQuestionIndex]);
                                return;
                                // check question
                            });
                        }

                    });

                </script>
                
                <asp:Button ID="fakeSubmit" runat="server" Style="display: none;" OnClick="fakeSubmit_Click" />
                <div class="popup_phonepreview" style="width: 300px; height: 480px; border: solid 5px #999; margin: 0 auto; position: relative;">
                    <div class="popup-overlay" style="background-color: #fcfcfc; width: 100%; height: 100%; z-index: 10; position: absolute; display: none; text-align: center;" onclick="hidePreviewOverlay(this);">
                        <div class="message">
                            <h1>Info</h1>
                            hello world
                        </div>
                        <img src="" />
                    </div>
                    <div class="progress-bar-indication" style="border: 0; background-color: #ccc;">
                        <span class="meter" style="width: 60%; height: 4px; border: 0; background-color: #ffd800;"></span>
                    </div>

                    <div class="preview-info-bar" style="text-align: right; padding: 0 .5em;">
                        <i class="fa fa-info-circle"></i>
                    </div>

                    <div class="preview-body" style="height: 420px; display: inline-block; overflow-y: auto; overflow-x: hidden; width: 100%; position: relative;">
                    </div>

                    <footer style="width: 100%; position: absolute; bottom: 0; border-top: 1px solid #aaa;">
                        <button type="button" class="next-page-button footer-button" style="text-align: center; width: 100%; border-radius: 0; display: none;" onclick="nextButtonAction();">Next</button>
                        <button type="button" class="next-question-button footer-button" style="text-align: center; width: 100%; border-radius: 0; display: none;" onclick="nextButtonAction();"><i class="fa fa-chevron-down"></i></button>
                        <div class="skip-question-button footer-button" style="color: #222; text-align: right; margin-right: 4px; display: inline; float: right; cursor: pointer;" onclick="nextButtonAction();">Skip</div>

                        <button type="button" class="start-survey-button footer-button" style="text-align: center; width: 100%; border-radius: 0; background-color: #FFC700; display: none;" onclick="startSurvey();">Start</button>
                        <button type="button" class="done-survey-button footer-button" style="text-align: center; width: 100%; border-radius: 0; background-color: #FFC700; display: none;" onclick="closeSurvey();">Done</button>
                    </footer>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="LinkButton3" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- / Live Survey Preview -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" BehaviorID="mpe" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_addtopicicon"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="data">
        <div class="data__content" onscroll="sticky_div();">
            <!-- Edit  Survey -->
            <div class="container">
                <div class="card add-topic">
                    <asp:UpdatePanel ID="upEditSurvey" runat="server" class="add-topic__icon">
                        <ContentTemplate>
                            <asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
                                <asp:Image ID="imgTopic" ImageUrl="~/img/icon-topicicon-default.png" runat="server" Width="120" Height="120" />
                                <label><small>Choose a survey icon</small></label>
                            </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="add-topic__info">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" class="add-topic__info__action" style="width: 35%;">
                            <ContentTemplate>
                                <asp:LinkButton ID="lbPreviewIntro" runat="server" CssClass="btn secondary" Text="Preview Intro" Style="float: none; width: 120px;" OnClick="lbPreview_Click" />
                                <asp:LinkButton ID="lbSurvey" runat="server" CssClass="btn" Text="Apply Changes" OnClick="lbSurvey_Click" OnClientClick="ShowProgressBar();updateAssessmentHeader();" />
                                <asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/Assessment/List" Text="Cancel" CssClass="btn secondary" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="add-topic__info__details">
                            <div class="tabs tabs--styled">
                                <ul class="tabs__list">
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#assessment-intro">Intro</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#assessment-tabulation">Tabulation</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#assessment-results">Answers</a>
                                    </li>
                                </ul>
                                <div class="tabs__panels">
                                    <div class="tabs__panels__item add-topic__info__details--basicinfo" id="assessment-intro">
                                        <div class="column--input">

                                            <!-- Intro -->
                                            <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                <%--<input name="input_title_of_assessment" type="text" id="input_title_of_assessment" class="topic-detail" placeholder="Name your exam" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'main_content_assessmentTitle','50');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;" />--%>
                                                <%--<label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Title</label>--%>
                                                <asp:TextBox runat="server" ID="assessmentTitle" placeholder="Name of exam" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;" onkeyup="textCounter(this,'input_title_of_assessmentCount','50');" />
                                                <span id="input_title_of_assessmentCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;"><%= (50 - assessmentTitle.Text.Length).ToString() %></span>
                                            </div>
                                            <p>&nbsp;</p>
                                            <div class="form__row" style="width: 100%;">
                                                <%--<label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Introduction</label>--%>
                                                <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                    <%--<textarea name="input_introduction" rows="2" cols="20" id="input_introduction" class="topic-detail" placeholder="Welcome message / purpose of this assessment" onkeyup="textCounter(this,'input_introductionCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>--%>
                                                    <asp:TextBox runat="server" TextMode="MultiLine" Rows="4" ID="assessmentIntro" placeholder="Welcome message / purpose of this assessment" onkeyup="textCounter(this,'input_introductionCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;" />
                                                    <span id="input_introductionCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;"><%= (100 - assessmentIntro.Text.Length).ToString() %></span>
                                                </div>
                                            </div>


                                                <div class="form__row" style="width: 100%;margin-bottom:0;">
                                                    <label style="width: 100%; text-align: left; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: top; display: inline-block;">Image Banners</label>
                                                    
                                                    <div style="border:0px dotted #ccc;display:inline-block; width: 560px;margin-right:28px;min-height:50px; margin-bottom: 0; vertical-align: middle; text-align:left;">

                                                        <div style="width:166px; height:166px; border: dashed 2px #ccc;display:inline-block;">
                                                            <label style="display:inline-block; position:inherit; bottom:auto; left:auto; cursor:pointer; height:100%; width:100%; text-align:center; vertical-align:middle;" onclick="showAddImagePopup();">
                                                                <span style="margin-top:15px; display:inline-block;color: rgba(203, 203, 203, 1); ">
                                                                    <span style="font-size:48pt;">&#43;</span>
                                                                    <br />
                                                                    <span style="font-size:14pt;">Upload Photo</span>
                                                                </span>
                                                                <%--<input type="file" accept="image/*" multiple="multiple" title="Add image" style="display: none" onchange="setPreviewImage(this, 'divPreviewImageList');" />--%>
                                                            </label>
                                                        </div>
                                                        &nbsp;
                                                        <br />
                                                        <p class="divPreviewImageList" style="display:inline"></p>
                                                        
                                                        <p style="color: rgba(203, 203, 203, 1); margin: .75em 0 0;">
                                                            A maximum of 5 photos are allowed.<br />
                                                            Uploaded photo up to 750 x 400 pixels and no bigger than 60KB.
                                                        </p>

                                                        &nbsp;
                                                    </div>
                                                </div>

                                            
<label id="uploadVideoButton" style="display:block;">
    <span style="margin-top:40px; display:inline-block;" class="btn">+ Upload Video</span>
    <input type="file" id="videoFileInput" accept="video/*" style="display:none;" class="" />
</label>

                                            <div id="video_section">
                                                <div id="videoPlaceholder" style="width:200px; border:1px solid #876389; position:relative;z-index:1;display:inline-block;">
                                                    <video id="videoDisplay" style="width:200px;position:absolute;top:0;left:0;z-index:-1" runat="server" onclick=""></video>
                                                    <div id="removeVideoButton" class="remove-video" onclick="videoController.removeVideo('#<%= videoDisplay.ClientID %>');" style="vertical-align:top; text-align:right; margin: 0 .5em;z-index:-200"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                    <img id="playVideoButton" style="display:block;margin:auto;position:relative;top:calc(100% - 60%);" src="/img/Media Play1-WF.gif" onclick="videoController.play('#main_content_videoDisplay');" />
                                                </div>

                                                <div style="display:inline-block; vertical-align:top; width:320px;">

                                                    
                                                    <div style="border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <%--<input name="input_video_title" type="text" id="input_video_title" class="topic-detail" placeholder="Video Title" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'input_video_titleCount','50');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;" />--%>
                                                        <asp:TextBox runat="server" ID="assessmentVideoTitle" placeholder="Video Title" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'input_video_titleCount','50');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;" />
                                                        <span id="input_video_titleCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;"><%= (50 - assessmentVideoTitle.Text.Length).ToString() %></span>
                                                    </div>
                                                    
                                                    <p>&nbsp;</p>
                                                    
                                                    <div style="border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                        <%--<textarea name="input_video_description" rows="2" cols="20" id="input_video_description" class="topic-detail" placeholder="Brief description about the video" onkeyup="textCounter(this,'input_video_descriptionCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>--%>
                                                        <asp:TextBox runat="server" TextMode="MultiLine" Rows="3" ID="assessmentVideoDescription" placeholder="Brief description about the video" onkeyup="textCounter(this,'input_video_descriptionCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;" />
                                                        <span id="input_video_descriptionCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;"><%= (100 - assessmentVideoDescription.Text.Length).ToString() %></span>
                                                    </div>

                                                </div>
                                            </div>
                                            &nbsp;
                                            <div style="border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                <%--<textarea name="input_closing_words" rows="2" cols="20" id="input_closing_words" class="topic-detail" placeholder="Thank you for participation / How can they follow up" onkeyup="textCounter(this,'input_closing_wordsCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>--%>
                                                <asp:TextBox runat="server" ID="assessmentClosing" placeholder="Thank you for participation / How can they follow up" onkeyup="textCounter(this,'input_closing_wordsCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; overflow: hidden;" />
                                                <span id="input_closing_wordsCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;"><%= (100 - assessmentClosing.Text.Length).ToString() %></span>
                                            </div>

                                        </div>

                                        <div class="column--choice">
                                            <div class="form__row no-bottom-margin">
                                                <asp:Label Text="Coach:" runat="server" CssClass="field-label"  />
                                                <div class="mdl-selectfield">
                                                    <asp:DropDownList runat="server" ID="ddlCoach"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form__row no-bottom-margin">
                                                <asp:Label Text="Display on Client:" runat="server" CssClass="field-label"  />
                                                <div class="mdl-selectfield">
                                                    <asp:DropDownList runat="server" ID="ddlDisplayOnClient">
                                                        <asp:ListItem Value="false" Text="No" />
                                                        <asp:ListItem Value="true" Text="Yes" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form__row no-bottom-margin">
                                                <asp:Label Text="Status:" runat="server" CssClass="field-label"  />
                                                <div class="mdl-selectfield">
                                                    <asp:DropDownList runat="server" ID="ddlStatus">
                                                        <asp:ListItem Value="1" Text="Draft" />
                                                        <asp:ListItem Value="2" Text="Live" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form__row no-bottom-margin">
                                                <asp:Label Text="Anonymous Pulse:" runat="server" CssClass="field-label" />
                                                <div class="mdl-selectfield">
                                                    <asp:DropDownList runat="server" ID="ddlAnonymousPulse">
                                                        <asp:ListItem Value="false" Text="No" />
                                                        <asp:ListItem Value="true" Text="Yes" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form__row">
                                                <label style="font-weight: 700;">CSS Area</label>
                                                

                                                <div style="border-image: none; width: 100%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                    <%--<textarea name="input_css_area" rows="2" cols="20" id="input_css_area" class="topic-detail" placeholder="Any additional dynamic info" onkeyup="textCounter(this,'input_css_areaCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;"></textarea>--%>
                                                    <asp:TextBox TextMode="MultiLine" Rows="4" runat="server" id="textareaCssArea" placeholder="Any additional dynamic info" onkeyup="textCounter(this,'input_css_areaCount','100');" style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;" />
                                                    <span id="input_css_areaCount" class="topic-lettercount" style="bottom: 5px; right: 5px; position: absolute;"><%= (100 - textareaCssArea.Text.Length).ToString() %></span>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--department" id="assessment-tabulation">
                                        <div class="form__row anonymous__tip" style="display:inline-block;">
                                            <span style="float:left;width:65px;">Tabulation</span>
                                            <img class="icon-img" src="/Img/icon_note_small.png" title="Tip" />
                                            <div class="anonymous_tip" style="display: none; top: -20px;">
                                                <%--<img src="/Img/icon_anonymous.png" title="Anonymous" style="width: 45px; display: block; float: left; vertical-align: middle; margin-right: 10px; margin-top: 5px;" />--%>
                                                <label style="font-weight: 700;">Tabulation</label>
                                                Selected option points will be allocated to the any of the table here.
                                            </div>
                                        </div>


                                        <div class="grid__inner tabulation-list" style="background-color: transparent;">

                                        </div>

                                        &nbsp;
                                        <div class="grid__inner" style="background-color: transparent;">
                                            <a href="javascript:addTabulation();">+ Add a table</a>
                                        </div>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--settings" id="assessment-results">
                                        
                                        <div class="assessment-result-list"></div>

                                        <div class="grid__inner" style="background-color: transparent;">
                                            <span style="color:#4683ea; cursor:pointer;" onclick="addResult(null);">+ Add a result</span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / Edit Survey -->

            <!-- / Questions  Bar -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <script>
                        function sticky_div() {
                            var window_top = $(window).scrollTop();

                            var divTopId = $('#sticky_anchor');
                            if (!divTopId.length) {
                                return;
                            }
                            var div_top = divTopId.offset().top; // get the offset of the div from the top of page

                            // var div_top = $('#sticky-anchor').offset().top;
                            if (window_top + 160 > div_top) {
                                $('.sticky').addClass('stick');
                                $('#sticky_anchor').addClass('stick');

                            } else {
                                $('.sticky').removeClass('stick');
                                $('#sticky_anchor').removeClass('stick');

                            }
                        }
                    </script>
                    <asp:Panel ID="plSearchQuestion" runat="server" Visible="false">
                        <div id="sticky_anchor"></div>
                        <div class="survey-bar sticky">
                            <div class="container">
                                <div class="survey-bar__search">
                                    <asp:TextBox ID="tbSearchQuestion" runat="server" placeholder="Search question" CssClass="survey-bar__search__input" />
                                    <asp:LinkButton ID="lbAdd" runat="server" CssClass="survey-bar__search__button" Text="+ Add a Question" OnClick="lbAdd_Click" />
                                    <a onclick="ShowProgressBar(); return fetchAndRefreshListDisplay();" class="survey-bar__search__button" href="javascript:void(0);"><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <!-- / Questions  Bar -->

            <!-- New Card -->
            <div id="animateCard">
                <asp:UpdatePanel ID="upNewQuestion" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="plNewQuestion" runat="server" CssClass="container" Visible="false">
                            <div id="add-question-card" class="card add-question ">
                                <div class="add-question__questionnumber">
                                    <div class="number">
                                        <asp:Literal ID="ltlNewQuestionPaging" runat="server" />
                                    </div>
                                    <hr />
                                    <div class="number">
                                        <asp:Literal ID="ltlNewCardOrdering" runat="server" />
                                    </div>
                                </div>
                                <div class="add-question__info">
                                    <div class="add-question__info__header">
                                        <div class="add-question__info__header__action">
                                            <asp:LinkButton ID="lbNewQuestionCancel" runat="server" CssClass="btn secondary" Text="Cancel" OnClick="lbNewQuestionCancel_Click" />
                                            <asp:LinkButton ID="lbNewQuestionSave" runat="server" CssClass="btn" Text="Create" OnClientClick="ShowProgressBar();  return checkNewCardInputData();" OnClick="lbNewQuestionSave_Click" />
                                        </div>
                                        <div class="add-question__info__header__title">Question Type</div>
                                    </div>
                                    <div class="add-question__info__setup">
                                        <div class="add-question__info__setup__questiontype">
                                            <div class="tabs--questionformat">
                                                <ul class="tabs__list">
                                                    <div id="divNewCardType1" class="tabs__list__item" style="margin: 2px; background: #0075FF; color: #F2F4F7; cursor: pointer;" onclick="setNewCardType(1);">
                                                        Select one
                                                    </div>
                                                    <div id="divNewCardType2" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(2);">
                                                        Multi choice
                                                    </div>
                                                    <%--
                                                    <div id="divNewCardType3" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(3);">
                                                        Text
                                                    </div>
                                                    <div id="divNewCardType4" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(4);">
                                                        Number range
                                                    </div>
                                                    <div id="divNewCardType5" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(5);">
                                                        Drop list
                                                    </div>--%>
                                                    <div id="divNewCardType6" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(6);">
                                                        Instructions
                                                    </div>
                                                    <div id="divNewCardType7" class="tabs__list__item" style="margin: 2px; color: #CCCCCC; cursor: pointer;" onclick="setNewCardType(7);">
                                                        Accumulation
                                                    </div>
                                                </ul>
                                                <div class="tabs__panels">
                                                    <div class="tabs__panels__item tabs__panels__item">
                                                        <div class="form__row form__row--question">
                                                            <div>
                                                                <textarea id="tbNewCardContent" style="padding-left: 40px;" onkeydown="return (event.keyCode!=13);" onblur="setNewCardValue(1, this, null);" placeholder="Question" rows="2" cols="20" onkeyup="textCounter(this,'lblContentCount', 250); setNewCardValue(1, this, null);"></textarea>
                                                                <span id="lblContentCount" class="survey-lettercount"></span>
                                                                <label class="upload-card-image" for="fuNewCardContentImg">
                                                                    <input type="file" id="fuNewCardContentImg" accept="image/*" title="Add image" style="display: none" onchange="previewImage(this, 2560, 2560 ,1, 1);" /></label><div id="divNewCardContentImg" style="visibility: hidden; height: 0px; overflow: auto;" class="question-image-content">
                                                                        <div class="survey-cross" onclick="removeContentImg(1, 1);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                        <img id="imgNewCardContentImgPreview" />
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <div id="divNewCardType1Layout" style="visibility: visible; display: block; overflow: auto;" class="form__row form__row--question">
                                                            <div id="divNewCardOptions" class="options-container">
                                                                <div style="margin-bottom: 10px" id="divNewCardOption1" class="card-option">
                                                                    <textarea name="tbNewCardOption1" class="option-content" onkeydown="return (event.keyCode!=13);" onkeyup="textCounter(this,'lblNewCardOption1ContentCount', 150); setNewCardValue(2, this, 1);" onblur="setNewCardValue(2, this, 1);" placeholder="Option 1" rows="2" cols="20"></textarea>
                                                                    <span id="lblNewCardOption1ContentCount" class="survey-lettercount"></span>
                                                                    <label class="upload-option-image" for="fuNewCardOption1ContentImg">
                                                                        <input type="file" id="fuNewCardOption1ContentImg" accept="image/*" title="Add image" style="display: none" onchange="previewImage(this, 2560, 2560 ,2, 1);" />
                                                                    </label>
                                                                    <div id="divNewCardOption1ContentImg" style="visibility: hidden; height: 0px; overflow: auto;" class="option-image-content">
                                                                        <div class="survey-cross" onclick="removeContentImg(2, 1);"><i class="fa fa-times" aria-hidden="true"></i></div>
                                                                        <img id="imgNewOption1ContentImgPreview" />
                                                                    </div>
                                                                    <div class="option-tabulation-allocation"></div>
                                                                    <div>
                                                                        <span class="clickable" onclick="addOptionAllocation(this);">Add option allocation</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br />
                                                            <a href="javascript:void(0);" class="options-container add-option-link" onclick="addOptionField()" style="float: right;">+ Add option</a>
                                                            <br />
                                                            <div id="assignTotalPoints" class="assign-total-points" style="clear:both;display:none;">
                                                                <label style="font-weight: bold">Total Points <img class="icon-tooltip" src="/Img/icon_note_small.png" title="Total points to allocate for question." /></span></label>
                                                                <span>Points add up value <input onkeydown="return (event.keyCode!=13);" type="number" style="width:133px;display:inline-block;" onkeyup="setNewCardValue(21, this, null);" onblur="setNewCardValue(21, this, null);" /></span><br />
                                                            </div>
                                                        </div>
                                                        <div id="divNewCardType2Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question">Type 2 Layout</div>
                                                        <div id="divNewCardType3Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question"></div>
                                                        <div id="divNewCardType4Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question">
                                                            <label style="margin-bottom: 15px;">
                                                                Number range
                                                                <br />
                                                                (Minimum: -20 to Maximum: 20)
                                                            </label>
                                                            <div class="option_section">
                                                                Slider starts from<br />
                                                                <label class="inline-label radio">
                                                                    <input type="radio" name="numberRangeStartFrom" value="1" checked="checked" onchange="resetRangeType(1); setNewCardValue(15, this, null);">Minimum/Maximum</label>
                                                                <label class="inline-label radio">
                                                                    <input type="radio" name="numberRangeStartFrom" value="2" onchange="resetRangeType(2); setNewCardValue(15, this, null);">Middle</label>
                                                            </div>
                                                            <div id="divNewCardRangeNonmiddle">
                                                                <select id="sltRangeNonmiddle" onchange="setNewCardValue(15, this, null);">
                                                                    <option value="1">Start from Minimum</option>
                                                                    <option value="3">Start from Maximum</option>
                                                                </select>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Minimum value</label>
                                                                    <input name="tbNewCardOptionRangeMin" class="tbNewCardOptionRangeMin nonmiddle" onblur="setNewCardValue(11, this, null);" type="number" /><input name="tbNewCardOptionRangeMinLabel" class="tbNewCardOptionRangeMinLabel" type="text" placeholder="Label(Optional)" onkeyup="textCounter(this,'lblNewCardRangeNonmiddleMinCount', 25); setNewCardValue(19, this, null);" /><span id="lblNewCardRangeNonmiddleMinCount" class="survey-lettercount"></span><label class="tip">Eg. Poor, Bad, Need improvement, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Maximum value</label>
                                                                    <input name="tbNewCardOptionRangeMax" class="tbNewCardOptionRangeMax nonmiddle" onblur="setNewCardValue(12, this, null);" type="number" /><input type="text" name="tbNewCardOptionRangeMaxLabel" class="tbNewCardOptionRangeMaxLabel" placeholder="Label(Optional)" onkeyup="textCounter(this,'lblNewCardRangeNonmiddleMaxCount', 25); setNewCardValue(18, this, null);" /><span id="lblNewCardRangeNonmiddleMaxCount" class="survey-lettercount"></span><label class="tip">Eg. Excellent, Great, Very good, Amazing, etc.</label>
                                                                </div>
                                                            </div>
                                                            <div id="divNewCardRangeMiddle" style="display: none;">
                                                                <div class="option_section">
                                                                    <label class="inline-label">Middle value</label>
                                                                    <input name="tbNewCardOptionRangeMiddle" class="tbNewCardOptionRangeMiddle middle" onblur="setNewCardValue(16, this, null);" type="number" /><input type="text" name="tbNewCardOptionRangeMiddleLabel" class="tbNewCardOptionRangeMiddleLabel" placeholder="Label" onkeyup="textCounter(this,'lblNewCardRangeMiddleMiddleCount', 25); setNewCardValue(17, this, null);" /><span id="lblNewCardRangeMiddleMiddleCount" class="survey-lettercount"></span><label class="tip">Eg. Average, Neutral, So-so, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Maximum value</label>
                                                                    <input name="tbNewCardOptionRangeMax" class="tbNewCardOptionRangeMax middle" onblur="setNewCardValue(20, this, null);" type="number" /><input type="text" name="tbNewCardOptionRangeMaxLabel" class="tbNewCardOptionRangeMaxLabel" placeholder="Label" onkeyup="textCounter(this,'lblNewCardRangeMiddleMaxCount', 25); setNewCardValue(18, this, null);" /><span id="lblNewCardRangeMiddleMaxCount" class="survey-lettercount"></span><label class="tip">Eg. Excellent, Great, Very good, Amazing, etc.</label>
                                                                </div>
                                                                <div class="option_section">
                                                                    <label class="inline-label">Minimum value</label>
                                                                    <label id="lblNewCardOptionRangeMiddleMin"></label>
                                                                    <input type="text" name="tbNewCardOptionRangeMinLabel" class="tbNewCardOptionRangeMinLabel" placeholder="Label" onkeyup="textCounter(this,'lblNewCardRangeMiddleMinCount', 25); setNewCardValue(19, this, null);" /><span id="lblNewCardRangeMiddleMinCount" class="survey-lettercount"></span><label class="tip">Eg. Poor, Bad, Need improvement, etc.</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="divNewCardType5Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question">
                                                            <textarea rows="16" id="txareaNewCard" class="droplist_options" placeholder="Please type in your options here, subsequent options separated by 'Enter' key." onkeyup="updateSelectPreview(this);setNewCardValue(14, this, null);" style="overflow-y: auto;"></textarea>
                                                            <label>Please type in your options here, subsequent options separated by 'Enter' key.</label>
                                                        </div>
                                                        <div id="divNewCardType6Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question"></div>

                                                        <div id="divNewCardType7Layout" style="visibility: hidden; display: none; overflow: auto;" class="form__row form__row--question"></div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add-question__info__setup__preview">
                                            <!-- Card Preview Format -->
                                            <div id="divNewCardPreview" class="survey-preview" style="background-image: url(/Img/bg_survey_1a_big.png); position: relative;">
                                                <div>
                                                    <!-- For Note -->
                                                    <img class="right" id="imgNewCardPreviewNote" src="/Img/icon_note_small.png" />
                                                </div>
                                                <div class="preview-question">
                                                    <!-- For CardContentText, CardContentImgs  -->
                                                    <span class="preview-question-content" id="lblNewCardPreviewContentText" style="color: black;"></span>
                                                    <img class="preview-question-image" id="imgNewCardPreviewContentImg" />
                                                </div>
                                                <div id="divNewCardPreviewTypes">
                                                    <div id="divNewCardType1PreviewLayout"></div>

                                                    <div id="divNewCardType2PreviewLayout" style="display: none; height: 0px;"></div>
                                                    <div id="divNewCardType3PreviewLayout" style="display: none; height: 0px;">
                                                        <div class="preview-textarea">
                                                            <div class="preview-textarea-content">Answer area</div>
                                                        </div>
                                                    </div>
                                                    <div id="divNewCardType4PreviewLayout" style="display: none; height: 0px; overflow: auto;">
                                                        <div class="preview-numberrange">
                                                            <div>
                                                                <input type="text" id="range" value="" name="range" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="divNewCardType5PreviewLayout" style="display: none; height: 0px;">
                                                        <select class="preview-droplist">
                                                            <option>Please Select</option>
                                                        </select>
                                                    </div>
                                                    <div id="divNewCardType6PreviewLayout" style="display: none; height: 0px;"></div>

                                                    <div id="divNewCardType7PreviewLayout" style="display: none; height: 0px;"></div>
                                                </div>
                                                <!-- For PageBreak, IsAllowSkipped -->
                                                <div class="preview-footer-content" id="divNewCardPeviewNoPageBreak" style="background: rgba(0, 153, 253, 1); color: white; position: absolute; bottom: 0;"><span class="center">No page break</span></div>
                                                <div class="preview-footer-content" id="divNewCardPeviewPageBreak" style="background: rgba(0, 153, 253, 1); color: white; position: absolute; bottom: 0; display: none;"><span class="center">Next</span></div>
                                                <div class="preview-footer-content" id="divNewCardPeviewSkip" style="color: rgba(150, 176, 216, 1); position: absolute; bottom: 0; display: none;"><span class="right">Skip</span></div>
                                            </div>
                                            <!-- / Card Preview Format -->
                                        </div>
                                        <div class="add-question__info__setup__choice">
                                            <div id="divNewCardBehaviour">
                                                <label style="font-weight: bold">Behaviour</label>
                                                <span class="setting-checkbox">
                                                    <input name="cbNewCardAllowSkip" id="cbNewCardAllowSkip" onclick="setNewCardValue(3, this, null);" type="checkbox" /><label for="cbNewCardAllowSkip">Allow question to be skipped</label>
                                                </span>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardTickOther">
                                                    <span class="setting-checkbox">
                                                        <input name="cbNewCardTickOther" id="cbNewCardTickOther" onclick="setNewCardValue(4, this, null);" type="checkbox" />
                                                        <label for="cbNewCardTickOther">Tick 'other' and type your own answer</label>
                                                    </span>
                                                </div>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardCustomCommand" style="visibility: hidden; height: 0px; overflow: auto;">
                                                    Custom command
                                                    <input id="tbNewCardCustomCommand" onkeyup="setNewCardValue(6, this, null);" onblur="setNewCardValue(6, this, null);" type="text" placeholder="Please specify (Default)" />
                                                </div>
                                                <div class="clear-float"></div>
                                                <div id="divNewCardRandomOption" style="visibility: hidden; height: 0px; overflow: auto;">
                                                    <span class="setting-checkbox">
                                                        <input name="cbNewCardRandomOption" id="cbNewCardRandomOption" onclick="setNewCardValue(5, this, null);" type="checkbox" />
                                                        <label for="cbNewCardRandomOption">Randomize option order</label>
                                                    </span>
                                                </div>
                                                <div class="clear-float"></div>

                                            </div>
                                            <br />
                                            <div id="divNewCardMulti" style="visibility: hidden; height: 0px;">
                                                <label style="font-weight: bold">No of options to be selected</label>
                                                Minimum
                                                <input name="tbNewCardMultiMin" id="tbNewCardMultiMin" onblur="setNewCardValue(7, this, null);" type="number" value="1" /><br />
                                                Maximum
                                                <input name="tbNewCardMultiMax" id="tbNewCardMultiMax" onblur="setNewCardValue(8, this, null);" type="number" value="1" /><br />
                                            </div>
                                            <br />
                                            <div id="divNewCardDisplay">
                                                <label style="font-weight: bold">Display</label>
                                                <div>
                                                    Background
                                                    <!-- Background -->
                                                    <div class="imageslider" style="margin: 10px auto auto; width: 85%;">
                                                        <div id="slider">
                                                            <!-- Slider Setup -->
                                                            <input name="slider" id="slide1" onchange="setNewCardValue(13, this, 1);" type="radio" selected="false" checked="">
                                                            <input name="slider" id="slide2" onchange="setNewCardValue(13, this, 2);" type="radio" selected="false">
                                                            <input name="slider" id="slide3" onchange="setNewCardValue(13, this, 3);" type="radio" selected="false">
                                                            <!-- / Slider Setup -->
                                                            <!-- The Slider -->
                                                            <div id="slides" style="margin-top: 0px;">
                                                                <div id="overflow">
                                                                    <div class="inner">
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_1a_small.png">
                                                                        </article>
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_2a_small.png">
                                                                        </article>
                                                                        <article>
                                                                            <div class="info">
                                                                            </div>
                                                                            <img src="/Img/bg_survey_3a_small.png">
                                                                        </article>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- / The Slider -->
                                                            <!-- Controls and Active Slide Display -->
                                                            <div class="controls" id="controls" style="top: 38%; display: none;">
                                                                <label for="slide1"></label>
                                                                <label for="slide2"></label>
                                                                <label for="slide3"></label>
                                                            </div>
                                                            <!-- / Controls and Active Slide Display -->
                                                        </div>
                                                    </div>
                                                    <!-- Background -->

                                                </div>
                                                <br />
                                            </div>
                                            <br />
                                            <div id="divNewCardNotes" class="divNewCardNotes" style="width:100%;color: rgba(209, 209, 209, 1); font-size: .9em; vertical-align: middle; display: inline-block; position: relative;">
                                                <label style="font-weight: bold">Notes</label>
                                                <input name="tbNewCardNotes" style="width:226px;" id="tbNewCardNotes" onkeydown="return (event.keyCode!=13);" onblur="setNewCardValue(9, this, null);" type="text" maxlength="60" placeholder="Enter notes" onkeyup="updateTextCounter(this,'.divNewCardNotes .lettercount',60);">
                                                <span class="lettercount" style="bottom: 60px; right: 10px; position: absolute;">60</span>
                                                <span style="color: #979797">Notes will be seen by user during the survey via the
                                                    <img src="/Img/icon_note_small.png" />icon</span>
                                            </div>
                                            <br />
                                            <div id="divNewCardPageBreak">
                                                <span class="setting-checkbox">
                                                    <input name="cbNewCardPageBreak" id="cbNewCardPageBreak" onclick="setNewCardValue(10, this, null);" type="checkbox" />
                                                    <label for="cbNewCardPageBreak">Page Break</label>
                                                </span>
                                                <div class="clear-float"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr style="border-bottom: 2px solid #cccccc; margin-top: 35px;" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <asp:LinkButton ID="lbNewQuestionLocation" runat="server" />
            <!-- / New Card -->

            <style>
                .column {
                    width: 100%;
                    margin-right: .5%;
                    min-height: 300px;
                    background: #fff;
                    float: left;
                }

                    .column .dragbox {
                        margin: 5px 2px 20px;
                        background: #fff;
                        position: relative;
                        border: 1px solid #ddd;
                        -moz-border-radius: 5px;
                        -webkit-border-radius: 5px;
                    }

                        .column .dragbox h2 {
                            margin: 0;
                            font-size: 12px;
                            padding: 5px;
                            background: #f0f0f0;
                            color: #000;
                            border-bottom: 1px solid #eee;
                            font-family: Verdana;
                            cursor: move;
                        }

                .dragbox-content {
                    background: #fff;
                    min-height: 100px;
                    margin: 5px;
                    font-family: 'Lucida Grande', Verdana;
                    font-size: 0.8em;
                    line-height: 1.5em;
                    /*display: none;*/
                }

                .column .placeholder {
                    background: #f0f0f0;
                    border: 1px dashed #ddd;
                }

                .dragbox h2.collapse {
                    background: #f0f0f0 url('collapse.png') no-repeat top right;
                }

                .dragbox h2 .configure {
                    font-size: 11px;
                    font-weight: normal;
                    margin-right: 30px;
                    float: right;
                }

                .selected-card-type {
                    background: #0075FF !important;
                    color: #F2F4F7 !important;
                }
            </style>

            <div id="cardList" class="container">
            </div>
        </div>
    </div>

        <div class="popup__section">
        <div id="popup_background" class="mfp-bg" style="display:none; position: fixed; left: 0px; top: 0px; z-index: 9000; width: calc(100%); height: calc(100%);"></div>

        <div id="popup_addimage" class="popup popup--addimage" style="display:none; width: 100%; position: fixed; margin: 0px; z-index: 9001; max-height: 368px; left: 563px; top: 292.5px; height: 368px; max-width:1078px;">
            <div><h1 class="popup__title">Add Image</h1></div>
            <div class="popup__content sortable" id="foo" style="width:1076px; max-height:226px; vertical-align:top;">

                <div style="width:166px; height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top;" class="image0">
                    <label style="display:inline-block; position:inherit; bottom:auto; left:auto; cursor:pointer; height:100%; width:100%; text-align:center; vertical-align:middle;">
                        <span style="margin-top:15px; display:inline-block;color: rgba(203, 203, 203, 1); ">
                            <span style="font-size:48pt;">&#43;</span>
                            <br />
                            <span style="font-size:14pt;">Upload Photo</span>
                        </span>
                        <input type="file" accept="image/*" multiple="multiple" title="Add image" style="display: none" onchange="setPreviewImage(this, 'divPreviewImageList');" />
                    </label>
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;" class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle; text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;" class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle; text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;"  class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle;text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;"  class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle;text-align:center;" />
                </div>

                <div style="width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;"  class="preview-image">
                    <div class="remove-img-banner" style="display:none;" onclick="removePopupImgBanner(this);"><i class="fa fa-times" aria-hidden="true"></i></div>
                    <img style="max-height:100%; max-height:100%; vertical-align:middle;text-align:center;" />
                </div>


            </div>
            <div class="popup__action">
                <a id="lbClosepopup_addimage" class="popup__action__item popup__action__item--cancel" href="javascript:closeAddImagePopup();">Close</a>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var model = {};
        var videoController = new videoController();


        function removeImgBanner(element) {
            var html, uploadBannerIdx;
            uploadBannerIdx = $(element).parent().parent().children().index($(element).parent());
            //remove 
            $($("#popup_addimage .popup__content .preview-image")[uploadBannerIdx]).remove();
            // compensate
            html = "";
            html = html + "<div style=\"width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;\" class=\"preview-image\">";
            html = html + "    <div class=\"remove-img-banner\" style=\"display:none;\" onclick=\"removePopupImgBanner(this);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
            html = html + "    <img style=\"max-height:100%; max-height:100%; vertical-align:middle;text-align:center;\" />";
            html = html + "</div> ";
            $("#popup_addimage .popup__content").append(html);

            model.assessment.imageBannersUpload.splice(uploadBannerIdx, 1);
            $(element).parent().remove();
        }


        function setPreviewImage(input, elementId) {
            var idx, inputFile;
            if (input.files.length > 0) {
                for (idx = 0; idx < input.files.length; idx++) {

                    inputFile = input.files[idx];
                    var inputFileType = inputFile.type;

                    if (inputFileType.toLowerCase() == "image/png" ||
                        inputFileType.toLowerCase() == "image/jpeg" ||
                        inputFileType.toLowerCase() == "image/jpg") {

                        var fileReader = new FileReader();

                        fileReader.onload = function (e) {
                            var image = new Image();
                            image.src = e.target.result;
                            image.onload = function (a, b, c) {
                                var w = this.width;
                                var h = this.height;
                                var html = "";

                                if (model.assessment.imageBannersUpload.length < 5) {
                                    html = html + "<div class=\"img-container\" >";
                                    html = html + "    <div class=\"remove-img-banner\" onclick=\"removeImgBanner(this);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                                    html = html + "    <img class=\"img-banner\" style=\"max-height:100px;\" />";
                                    html = html + "</div>";
                                    $(".divPreviewImageList").append(html);

                                    $(".divPreviewImageList img:last")[0].src = this.src;
                                    $(".divPreviewImageList img:last")[0].style.visibility = "visible";
                                    $(".divPreviewImageList img:last")[0].style.height = "auto";

                                    var contentImg = getContentImageObj();
                                    contentImg.base64 = this.src;
                                    contentImg.extension = inputFileType;
                                    contentImg.width = w;
                                    contentImg.height = h;

                                    $($("#popup_addimage .popup__content").children(".preview-image")[model.assessment.imageBannersUpload.length]).children("img")[0].src = this.src;
                                    $($("#popup_addimage .popup__content").children(".preview-image")[model.assessment.imageBannersUpload.length]).children(".remove-img-banner").show();
                                    model.assessment.imageBannersUpload.push(contentImg);
                                }

                            }
                        }

                        fileReader.readAsDataURL(input.files[idx]);
                    } else {
                        toastr.error('File extension is invalid.');
                    }
                }
            }
        }

        /* Functions for banners start **/

        function hideAddImagePopup() {
            $("#popup_background").hide();
            $("#popup_addimage").hide();
        }

        function showAddImagePopup() {
            var windowWidth, windowHeight,
                dialogWidth, dialogHeight,
                centerX, centerY,
                    dialogX, dialogY;

            dialogWidth = $("#popup_addimage").width();
            dialogHeight = $("#popup_addimage").height();

            windowWidth = $(window).width();
            windowHeight = $(window).height();
            centerX = windowWidth / 2;
            centerY = windowHeight / 2;

            dialogX = centerX - (dialogWidth / 2);
            dialogY = centerY - (dialogHeight / 2);

            $("#popup_background").show();
            $("#popup_addimage").show();

            $("#popup_addimage").offset({
                top: dialogY,
                left: dialogX
            });
        }

        function closeAddImagePopup() {
            $("#popup_background").hide();
            $("#popup_addimage").hide();
        }

        function removePopupImgBanner(element) {
            var html, uploadBannerIdx;
            uploadBannerIdx = $(element).parent().parent().children().index($(element).parent());

            //remove 
            $($("#popup_addimage .popup__content .preview-image")[uploadBannerIdx - 1]).remove();
            // compensate
            html = "";
            html = html + "<div style=\"width:166px; height:166px; line-height:166px; border: dashed 2px #ccc; display:inline-block; vertical-align:top; text-align:center;\" class=\"preview-image\">";
            html = html + "    <div class=\"remove-img-banner\" style=\"display:none;\" onclick=\"removePopupImgBanner(this);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
            html = html + "    <img style=\"max-height:100%; max-height:100%; vertical-align:middle;text-align:center;\" />";
            html = html + "</div> ";

            $("#popup_addimage .popup__content").append(html);

            model.assessment.imageBannersUpload.splice(uploadBannerIdx - 1, 1);
            $(element).parent().remove();
        }

        /* Functions for banners end **/

        function updateLocalJSON() {
            $("#main_content_hfAssessment").val(JSON.stringify(model.assessment));
            $("#main_content_hfAssessmentUpload").val(JSON.stringify(model.assessmentUpload));
        }

        function updateTopicHeader(topic) {
            if (topic) { // if parameter is not null or undefined

            }
        }

        function displayTopicCardList(cards) {
            if (cards) {// if parameter is not null or undefined
                if (cards.length <= 0) // no cards; do nothing
                    return;
                // display card 
            }
        }

        function selectSkipOrPagebreak(cardId) {
            var cardDivSelector;
            cardDivSelector = "div.card." + cardId;

            $(cardDivSelector + " .preview-footer-content.no-page-break").hide();
            $(cardDivSelector + " .preview-footer-content.page-break").hide();
            $(cardDivSelector + " .preview-footer-content.skip").hide();

            if ($(cardDivSelector + " .setting-ToBeSkipped").is(":checked")) {
                $(cardDivSelector + " .preview-footer-content.skip").show();
            }
            else {
                if ($(cardDivSelector + " .setting-PageBreak").is(":checked")) {
                    $(cardDivSelector + " .preview-footer-content.page-break").show();
                }
                else {
                    $(cardDivSelector + " .preview-footer-content.no-page-break").show();
                }
            }
        }

        function updateListCard(card) {
            var cardDivSelector, html, disabledAttr;

            cardDivSelector = "div.card." + card.CardId;

            $(cardDivSelector).addClass("opencard");

            html = "" +
"    <div class=\"add-question__questionnumber\">" +
"        <div class=\"number paging\">" +
"            P" + card.Paging +
"        </div>" +
"        <hr>" +
"        <div class=\"number ordering\">" +
"            Q" + card.Ordering +
"        </div>" +
"    </div>" +
"    <div class=\"add-question__info\">" +
"        <div class=\"add-question__info__header\">" +
"            <div class=\"add-question__info__header__action\">" +
"                <a id=\"main_content_lbNewQuestionCancel\" class=\"btn secondary\" href=\"javascript:displayMiniCard('" + card.CardId + "')\">Cancel</a>" +
"                <a onclick=\"ShowProgressBar(); return updateCard('" + card.CardId + "');\" class=\"btn updateCard\" href=\"javascript:void(0);\">Save</a>" +
"            </div>" +
"            <div class=\"add-question__info__header__title\">Question Type</div>" +
"        </div>" +
"        <div class=\"add-question__info__setup\">" +
"            <div class=\"add-question__info__setup__questiontype\">" +
"                <div class=\"tabs--questionformat\">";

            if (canChangeCardFormat()) {
                disabledAttr = "";
                html = html +
            "                    <ul class=\"tabs__list\">" +
            "                        <div class=\"cardtype1 tabs__list__item selected-card-type\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(1, '" + card.CardId + "');\">Select one</div>" +
            "                        <div class=\"cardtype2 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(2, '" + card.CardId + "');\">Multi choice</div>" +
            //"                        <div class=\"cardtype3 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(3, '" + card.CardId + "');\">Text</div>" +
            //"                        <div class=\"cardtype4 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(4, '" + card.CardId + "');\">Number range</div>" +
            //"                        <div class=\"cardtype5 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(5, '" + card.CardId + "');\">Drop list</div>" +
            "                        <div class=\"cardtype6 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(6, '" + card.CardId + "');\">Instructions</div>" +
            "                        <div class=\"cardtype7 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; cursor: pointer; \" onclick=\"setCardDisplay(7, '" + card.CardId + "');\">Accumulation</div>" +
            "                    </ul>";
            } else {
                disabledAttr = "disabled";
                html = html +
            "                    <ul class=\"tabs__list\">" +
            "                        <div class=\"cardtype1 tabs__list__item selected-card-type\" style=\"margin: 2px; color: #CCCCCC; \" >Select one</div>" +
            "                        <div class=\"cardtype2 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Multi choice</div>" +
            //"                        <div class=\"cardtype3 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Text</div>" +
            //"                        <div class=\"cardtype4 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Number range</div>" +
            //"                        <div class=\"cardtype5 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Drop list</div>" +
            "                        <div class=\"cardtype6 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Instructions</div>" +
            "                        <div class=\"cardtype7 tabs__list__item\" style=\"margin: 2px; color: #CCCCCC; \" >Accumulation</div>" +
            "                    </ul>";
            }


            html = html +
            "                    <div class=\"tabs__panels\">" +
            "                        <div class=\"tabs__panels__item tabs__panels__item\">" +
            "                            <div class=\"form__row form__row--question\">" +
            "                                <div>";
            html = html +
            "<div>" +
            "    <textarea class=\"tbNewCardContent\" data-optionid=\"" + card.CardId + "\"  style=\"padding-left: 40px;\" onkeydown=\"return (event.keyCode!=13);\" placeholder=\"Question\" rows=\"2\" cols=\"20\" onkeyup=\"updateOptionTextCount(this, SURVEY_CARD_CONTENT_MAX); $('div.card." + card.CardId + " .preview-question-content').text(this.value); \">" + card.Content + "</textarea>" +
            "    <span class=\"survey-lettercount " + card.CardId + "\">" + (SURVEY_CARD_CONTENT_MAX - card.Content.length) + "</span>" +
            "    <label class=\"upload-card-image\" for=\"fileUploadForQuestion_" + card.CardId + "\">" +
            "        <input type=\"file\" id=\"fileUploadForQuestion_" + card.CardId + "\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"displayCardPreviewImage(this, 2560, 2560 , '" + card.CardId + "');\">" +
            "    </label>";
            if (card.CardImages.length > 0) {
                html = html +
                "    <div class=\"cardContentImage cardContentImageWrapper " + card.CardId + "\" style=\"visibility: visible; height: auto; overflow: auto; margin-top:-23px;\">" +
                "        <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.CardId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>" +
                "        <img class=\"questionimage cardContentImage cardimage " + card.CardId + "\" src=\"" + card.CardImages[0].ImageUrl + "\">" +
                "    </div>";
            } else {
                html = html +
                "    <div class=\"cardContentImage cardContentImageWrapper " + card.CardId + "\" style=\"visibility: hidden; height: 0px; overflow: auto; margin-top:-23px;\">" +
                    "   <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.CardId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>" +
                    "   <img class=\"questionimage cardContentImage cardimage " + card.CardId + "\" src=\"\">" +
                    "</div>";
            }

            html = html + "</div>" +
            "                 </div>" +
            "                            </div>" +
            "                            <div class=\"form__row form__row--option options-container\"></div>" +
            "                        </div>" +
            "                    </div>" +
            "                </div>" +
            "            </div>" +
            "            <div class=\"add-question__info__setup__preview\">" +

            "               <!-- Card Preview Format -->" +
            "               <div class=\"survey-preview\" style=\"background-image: url(/Img/bg_survey_1a_big.png); position:relative; \">" +
            "                   <div>" +
            "                       <!-- For Note -->" +
            "                       <img class=\"right\" src=\"/Img/icon_note_small.png\" />" +
            "                   </div>" +
            "                   <div class=\"preview-question\"> " +
            "                       <!-- For CardContentText, CardContentImgs  -->" +
            "                       <span class=\"preview-question-content\" style=\"color: black;\">" + card.Content + "</span>" +
            "                       <img class=\"preview-question-image " + card.CardId + "\" src=\"" + ((card.CardImages.length < 1 || card.CardImages[0].ImageUrl.length < 1) ? '' : card.CardImages[0].ImageUrl) + "\" />" +
            "                   </div>" +
            "                   <div>" +
            "                       <div class=\"divEditCardType1PreviewLayout\">Type 1</div>" +
            "                       <div class=\"divEditCardType2PreviewLayout\" style=\"height: 0px;\">Type 2</div>" +
            "                       <div class=\"divEditCardType3PreviewLayout\" style=\"height: 0px;\">" +
            "                           <div class=\"preview-textarea\">" +
            "                               <div class=\"preview-textarea-content\">Answer area</div>" +
            "                           </div>" +
            "                       </div>" +
            "                       <div class=\"divEditCardType4PreviewLayout\" style=\"height: 0px; overflow: auto;\">" +
            "                           <div class=\"preview-numberrange\">" +
            "                               <div>" +
            "                                   <input type=\"text\" id=\"range\" value=\"\" name=\"range\" />" +
            "                               </div>" +
            "                           </div>" +
            "                       </div>" +
            "                       <div class=\"divEditCardType5PreviewLayout\" style=\"height: 0px;\ \">" +
            "                           <select class=\"preview-droplist\">" +
            "                               <option>Please Select</option>" +
            "                           </select>" +
            "                       </div>" +
            "                       <div class=\"divEditCardType6PreviewLayout\" style=\"height: 0px;\"></div>" +
            "                       <div class=\"divEditCardType7PreviewLayout\" style=\"height: 0px;\"></div>" +
            "                   </div>" +
            "                   <!-- For PageBreak, IsAllowSkipped -->";
            if (card.ToBeSkipped) {
                html = html + " <div class=\"preview-footer-content no-page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none;\"><span class=\"center\">No page break</span></div>" +
            "                   <div class=\"preview-footer-content page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none;\"><span class=\"center\">Next</span></div>" +
            "                   <div class=\"preview-footer-content skip\" style=\"color: rgba(150, 176, 216, 1); position:absolute; bottom: 0;\"><span class=\"right\">Skip</span></div>";
            }
            else {
                if (card.HasPageBreak) {
                    html = html + "                   <div class=\"preview-footer-content no-page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none; \"><span class=\"center\">No page break</span></div>" +
            "                   <div class=\"preview-footer-content page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0;\"><span class=\"center\">Next</span></div>" +
            "                   <div class=\"preview-footer-content skip\" style=\"color: rgba(150, 176, 216, 1); position:absolute; bottom: 0; display: none;\"><span class=\"right\">Skip</span></div>";
                }
                else {
                    html = html + "                   <div class=\"preview-footer-content no-page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0;\"><span class=\"center\">No page break</span></div>" +
                    "                   <div class=\"preview-footer-content page-break\" style=\"background: rgba(0, 153, 253, 1); color: white; position:absolute; bottom: 0; display: none;\"><span class=\"center\">Next</span></div>" +
                    "                   <div class=\"preview-footer-content skip\" style=\"color: rgba(150, 176, 216, 1); position:absolute; bottom: 0; display: none;\"><span class=\"right\">Skip</span></div>";
                }
            }
            html = html + "               </div>" +
            "               <!-- / Card Preview Format -->" +

            "            </div>" +
            "            <div class=\"cardSettingSection add-question__info__setup__choice\">" +
            "                <div class=\"cardSetting cardBehaviour\">" +
            "                    <label style=\"font-weight: bold\">Behaviour</label>" +
            "                    <span class=\"setting-checkbox\"><input class=\"setting-ToBeSkipped\" type=\"checkbox\" onclick=\"selectSkipOrPagebreak('" + card.CardId + "');\" " + disabledAttr + " ><label>Allow question to be skipped</label></span>" +
            "                    <div class=\"clear-float\"></div>" +
            "                    <div>" +
            "                        <span class=\"setting-checkbox\"><input class=\"setting-HasCustomAnswer\" type=\"checkbox\" onchange=\"updatePreview('" + card.CardId + "');\" onclick=\" this.checked? $(\'div.card." + card.CardId + " .cardSettingSection .cardSetting div.setting-CustomAnswer').show() : $(\'div.card." + card.CardId + " .cardSettingSection .cardSetting div.setting-CustomAnswer').hide() \" " + disabledAttr + " /><label>Tick 'other' and type your own answer</label></span>" +
            "                    </div>" +
            "                    <div class=\"clear-float\"></div>" +
            "                    <div class=\"setting-CustomAnswer\" style=\"height: auto; overflow: auto;\">" +
            "                        Custom answer" +
            "                        <input class=\"setting-CustomAnswer\" type=\"text\" onkeyup=\"updatePreview('" + card.CardId + "');\" placeholder=\"Please specify (Default)\" " + disabledAttr + ">" +
            "                    </div>" +
            "                    <div class=\"clear-float\"></div>" +
            "                    <div style=\"height: 0px; overflow: auto; visibility: hidden;\">" +
            "                        <span class=\"setting-checkbox\"><input class=\"setting-IsOptionRandomized\" type=\"checkbox\" " + disabledAttr + " /><label>Randomize option order</label></span>" +
            "                    </div>" +
            "                    <div class=\"clear-float\"></div>" +
            "" +
            "                </div><br />" +
            "                <div class=\"cardSetting cardMultiChoice\">" +
            "                    <label style=\"font-weight: bold\">No of options to be selected</label>" +
            "                    Minimum" +
            "                    <input type=\"number\" class=\"setting-MinimumSelection\" value=\"" + card.MiniOptionToSelect + "\" onkeyup=\"setNoOptionsSelect(\'" + card.CardId + "\', \'MiniOptionToSelect\', this);\" /><br>" +
            "                    Maximum" +
            "                    <input type=\"number\" class=\"setting-MaximumSelection\" value=\"" + card.MaxOptionToSelect + "\" onkeyup=\"setNoOptionsSelect(\'" + card.CardId + "\', \'MaxOptionToSelect\', this);\" />" +
            "                    <br>" +
            "                </div><br />" +
            "                <div class=\"cardSetting displaySection title\">" +
            "                    <label style=\"font-weight: bold\">Display</label>" +
            "                    <div>" +
            "                        Background" +
            "                        <!-- Background -->" +
            "                        <input class=\"setting-BackgroundType\" type=\"hidden\">" +
            "                        <div class=\"imageslider\" style=\"margin: 10px auto auto; width: 85%;\">" +
            "                           <div id=\"slider\">" +
            "                           <!-- Slider Setup -->" +
            "                                <input id=\"bgSlider1" + card.CardId + "\" name=\"bgSlider" + card.CardId + "\" onchange=\"$('div.card." + card.CardId + " .setting-BackgroundType').val(1); $('div.card." + card.CardId + " .survey-preview').css('background-image', 'url(/Img/bg_survey_1a_big.png)'); \" type=\"radio\" class=\"setting-BackgroundType1\" >" +
            "                                <input id=\"bgSlider2" + card.CardId + "\" name=\"bgSlider" + card.CardId + "\" onchange=\"$('div.card." + card.CardId + " .setting-BackgroundType').val(2); $('div.card." + card.CardId + " .survey-preview').css('background-image', 'url(/Img/bg_survey_2a_big.png)'); \" type=\"radio\" class=\"setting-BackgroundType2\" >" +
            "                                <input id=\"bgSlider3" + card.CardId + "\" name=\"bgSlider" + card.CardId + "\" onchange=\"$('div.card." + card.CardId + " .setting-BackgroundType').val(3); $('div.card." + card.CardId + " .survey-preview').css('background-image', 'url(/Img/bg_survey_3a_big.png)'); \" type=\"radio\" class=\"setting-BackgroundType3\">" +
            "                                <!-- / Slider Setup -->" +
            "" +
            "                                <!-- The Slider -->" +
            "                                <div id=\"slides\" class=\"slides\" style=\"margin-top: 0px;\">" +
            "                                    <div id=\"overflow\">" +
            "                                        <div class=\"inner\">" +
            "                                            <article>" +
            "                                                <div class=\"info\"></div>" +
            "                                                <img src=\"/Img/bg_survey_1a_small.png\">" +
            "                                           </article>" +
            "                                            <article>" +
            "                                                <div class=\"info\"></div>" +
            "                                               <img src=\"/Img/bg_survey_2a_small.png\">" +
            "                                            </article>" +
            "                                            <article>" +
            "                                                <div class=\"info\"></div>" +
            "                                                <img src=\"/Img/bg_survey_3a_small.png\">" +
            "                                            </article>" +
            "                                        </div>" +
            "                                   </div>" +
            "                                </div>" +
            "                                <!-- / The Slider -->" +
            "" +
            "                               <!-- Controls and Active Slide Display -->" +
            "                                <div class=\"controls\" id=\"controls\" style=\"top: 38%; display: none;\">" +
            "                                    <label for=\"bgSlider1" + card.CardId + "\"></label>" +
            "                                    <label for=\"bgSlider2" + card.CardId + "\"></label>" +
            "                                    <label for=\"bgSlider3" + card.CardId + "\"></label>" +
            "                                </div>" +
            "                                <!-- / Controls and Active Slide Display -->" +
            "                          </div>" +
            "                        </div>" +
            "                        <!-- Background -->" +
            "                    </div>" +
            "                    <br>" +
            "                </div><br />" +
            "                <div class=\"cardSetting displaySection notes divNewCardNotes\" style=\"width:100%;color: rgba(209, 209, 209, 1); font-size: .9em; vertical-align: middle; display: inline-block; position: relative;\">" +
            "                    <label style=\"font-weight: bold\">Notes</label>" +
            "                    <input class=\"setting-Notes\" style=\"width:226px;\" type=\"text\" placeholder=\"Enter notes\" onkeydown=\"return (event.keyCode!=13);\" value=\"" + card.Note + "\" onkeyup=\"$('div.card." + card.CardId + " .survey-preview img.right').prop('title', this.value); updateTextCounter(this,'.divNewCardNotes .lettercount',60);\" >" +
            "                    <span class=\"lettercount\" style=\"bottom: 60px; right: 10px; position: absolute;\">" + (60 - card.Note.length).toString() + "</span>" +
            "                    <span style=\"color: #979797\">Notes will be seen by user during the survey via the <img src=\"/Img/icon_note_small.png\"> icon</span>" +
            "                </div><br />" +
            "                <div class=\"cardSetting displaySection pagebreak\">" +
            "                    <span class=\"setting-checkbox\"><input class=\"setting-PageBreak\" type=\"checkbox\" " + disabledAttr + " onclick=\"selectSkipOrPagebreak('" + card.CardId + "');\"><label>Page Break</label></span>" +
            "                    <div class=\"clear-float\"></div>" +
            "                </div>" +
            "                <div class=\"cardSetting displaySection deleteButton\">" +
            "                    <a class=\"btn confirm right\" href=\"javascript:deleteCard('" + card.CardId + "');\">Delete Card</a>" +
            "                </div>" +
            "            </div>" +
            "        </div>" +
            "    </div>";

            $(cardDivSelector).html(""); // Clear contents of the card
            $(cardDivSelector).append(html); // Change the card to 

            // Find card and update the local Json to new card
            var rsTopic;

            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === card.CardId) {
                    rsTopic.Cards[idx] = card;
                    $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));
                    model.assessment = rsTopic;
                    break;
                }
            }
            setCardDisplay(card.Type, card.CardId);
        }

        function setNoOptionsSelect(cardId, key, element) {
            var rsTopic, card;
            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    if (key == "MaxOptionToSelect") {
                        card.MaxOptionToSelect = element.value;
                    }
                    else if (key == "MiniOptionToSelect") {
                        card.MiniOptionToSelect = element.value;
                    }
                    else {
                        // other 
                    }
                    rsTopic.Cards[idx] = card;
                    $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));
                    break;
                }
            }
        }

        function removeAttachedImg(cardId, optionId) {
            var imgElement, imgDivElement, imgPreviewElement;

            if (optionId == null) // Remove image of question.
            {
                imgDivElement = $(".cardContentImage." + cardId)[0];
                imgElement = $(".cardimage." + cardId)[0];
                if (imgElement === undefined)
                    return;

                imgElement.src = "";
                imgDivElement.style.visibility = "hidden";
                imgDivElement.style.height = "0px";

                imgPreviewElement = $(".preview-question-image." + cardId)[0];
                imgPreviewElement.src = "";
            }
            else // Remove image of options.
            {

            }
        }

        function displayCardPreviewImage(input, maxWidth, maxHeight, cardId, optionId) {
            var image = new Image();
            if (input.files && input.files[0]) {
                var t = input.files[0].type;
                var s = input.files[0].size / (1024 * 1024);
                if (t.toLowerCase() == "image/png" || t.toLowerCase() == "image/jpeg" || t.toLowerCase() == "image/jpg") {
                    if (s > 5) {
                        toastr.error('Uploaded photo exceeded 5MB.');
                    } else {

                        var previewElementImgPlaceHolder, previewElementImg, imgPreviewElement;

                        if (optionId === undefined) {
                            previewElementImgPlaceHolder = $("div.card." + cardId + " div.cardContentImage")[0];
                            previewElementImg = $("div.card." + cardId + " img.cardContentImage")[0];
                            imgPreviewElement = $(".preview-question-image." + cardId)[0];
                        } else {
                            previewElementImgPlaceHolder = $("div.card." + cardId + " div.cardContentImage." + optionId)[0];
                            previewElementImg = $("div.card." + cardId + " img.cardContentImage." + optionId)[0];
                            //remove option img
                            imgPreviewElement = $(".preview-option-image." + optionId)[0];
                        }

                        $(previewElementImg).data('extension', t);

                        var fileReader = new FileReader();
                        fileReader.addEventListener("load", function () {
                            previewElementImg.src = fileReader.result;
                            previewElementImgPlaceHolder.style.visibility = "visible";
                            previewElementImgPlaceHolder.style.height = "auto";
                            //previewElementImgPlaceHolder.style.

                            if (imgPreviewElement)
                                imgPreviewElement.src = fileReader.result;

                            var image = new Image();
                            image.src = this.result;
                            $(previewElementImg).data('width', image.width);
                            $(previewElementImg).data('height', image.height);

                        }, false);
                        fileReader.readAsDataURL(input.files[0]);
                    }
                } else {
                    toastr.error('File extension is invalid.');
                }
            }
        } // end function displayCardPreviewImage(input, maxWidth, maxHeight) {

        function fetchCardFromWebAPI(cardId) {
            var ajaxData,
                cardPromise, cardLogicPromise;

            ajaxData = { // data format to select specific card
                //'Action': 'GET',
                //'TopicId': $("#main_content_hfTopicId").val(),
                //'CategoryId': $("#main_content_hfCategoryId").val(),
                //
                //'AdminUserId': $("#main_content_hfAdminUserId").val(),
                //'CompanyId': $("#main_content_hfCompanyId").val()
                
                'assessmentId': JSON.parse($("#main_content_hfAssessment").val()).AssessmentId,
                'CardId': cardId,
                'adminUserId': $("#main_content_hfAdminUserId").val()
            };

            cardPromise = jQuery.ajax({
                type: "GET",
                url: "/api/AssessmentCard",
                data: ajaxData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        updateListCard(d.Card);
                    } else {
                        ReloadErrorToast();
                        toastr.error("Operation failed. " + d.ErrorMessage);
                        return false;
                    }
                },
                error: function (xhr, status, error) {
                    ReloadErrorToast();
                    toastr.error("Error connecting to server.");
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete

                }
            });

            jQuery.when(cardPromise).done(function (cardResult) {
                if (cardResult.Success) { // Update card if both result is successful
                    var card = cardResult.Card;

                    // Replace card in model.assessment.Cards
                    var itm = model.assessment.Cards.findIndex(function (currVal, idx, arr) {
                        if (currVal.CardId === this.toString()) {
                            return true;
                        }
                    }, cardResult.Card.CardId);
                    if (itm >= 0) {
                        model.assessment.Cards[itm] = cardResult.Card;
                    }

                    //$("#main_content_hfJsonStore").val()
                    $("#main_content_hfAssessment").val(JSON.stringify(model.assessment));
                    updateListCard(card);

                } else { // Else display error message
                    if (cardResult.Success == false) {
                        ReloadErrorToast();
                        toastr.error("Error fetching card details. " + cardResult.ErrorMessage);
                    } else if (cardLogicResult[0].Success == false) {
                        ReloadErrorToast();
                        toastr.error("Error fetching card logic. " + cardLogicResult.ErrorMessage);
                    }
                }

                HideProgressBar();
            });
        }

        function updateAssessmentHeader() {
            var answerIdx, answer, answerSelector;

            // Sync Assessment Header Answers
            for (answerIdx = 0; answerIdx < model.assessment.Answers.length; answerIdx++) {
                answer = model.assessment.Answers[answerIdx];

                answerSelector = ".assessment-result-list .assessment-result." + answer.AnswerId;
                //$(".assessment-result-list .assessment-result.AA8b3978c9131346a989c4282fc9bcfa30")

                answer.Title = $(answerSelector + " .result-title").val();

                // left panel
                answer.Condition = $(answerSelector + " .result-condition").val();

                answer.ShowInAssessment = $(answerSelector + " .result-showinassessment").prop("checked");
                answer.VisibleToSelf = $(answerSelector + " .result-visibletoself").prop("checked");
                answer.VisibleToOthers = $(answerSelector + " .result-visibletoothers").prop("checked");
                answer.VisibleToOthersForCompletedUser = $(answerSelector + " .result-visibletoothersforcompleteduser").prop("checked");
                answer.VisibleToOthersForIncompletedUser = $(answerSelector + " .result-visibletoothersforincompleteduser").prop("checked");
            }
            
            updateLocalJSON();
        }

        function updateCard(cardId) {
            var ajaxData, cardDivSelector,
                imgSelector, $img, imgJson,
                card, idx, optionIdx, tempOptions,
                hasError;
            hasError = false;
            cardDivSelector = "div.card." + cardId;

            // Find and update card
            var rsTopic;
            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    break;
                }
            }

            var cardSelectedMode, $cardOptions, cardOptionIdx, tempIdx;
            var $selectedCardType = $(cardDivSelector + " ul.tabs__list .tabs__list__item.selected-card-type");
            /***********************/
            // Do update of card depending on ui. 
            // validation and set new value.
            if (card == null) {
                errorToReload("Invalid Question, refreshing page...", true);
                return false;
            }

            /*** 1. Card type ***/
            //  Figure which mode of editing is this
            if ($selectedCardType.hasClass("cardtype1")) card.Type = 1;
            if ($selectedCardType.hasClass("cardtype2")) card.Type = 2;
            if ($selectedCardType.hasClass("cardtype3")) card.Type = 3;
            if ($selectedCardType.hasClass("cardtype4")) card.Type = 4;
            if ($selectedCardType.hasClass("cardtype5")) card.Type = 5;
            if ($selectedCardType.hasClass("cardtype6")) card.Type = 6;
            if ($selectedCardType.hasClass("cardtype7")) card.Type = 7;

            if (card.Type == null || card.Type < 1 || card.Type > 7) {
                errorToReload("Invalid Question, refreshing page...", true);
                return false;
            }

            /*** 2. Card content text & images ***/
            //  Update question text to card
            card.Content = $(cardDivSelector + " textarea.tbNewCardContent").val().trim();
            if (card.Content.length == 0) {
                errorToReload("Please type in a question.", false);
                return false;
            }
            if (card.Content.length > 250) {
                errorToReload("Question length is too long.", false);
                return false;
            }

            // reset all the images on the card's question and options
            card.HasImage = false;
            card.CardImages = [];
            // Update Card (question) images
            $img = $(cardDivSelector + " img.questionimage.cardContentImage:visible");
            if (($img.length > 0) && ($img.attr("src").length > 0)) {
                card.HasImage = true;
                card.CardImages.push({
                    width: $img.data("width"),
                    height: $img.data("height"),
                    base64: $img.prop("src"),
                    extension: $img.data("extension")
                });
            }

            /*** 3. Background, Note and PageBreak ***/
            card.BackgroundType = parseInt($(cardDivSelector + " .setting-BackgroundType").val(), 10);
            card.Note = $(cardDivSelector + " .setting-Notes").val();
            card.HasPageBreak = $(cardDivSelector + " .setting-PageBreak").prop("checked");

            /*** 4. Options & Behaviour ***/
            if (card.Type == 1 || card.Type == 2 || card.Type == 7) // 1 = select-one, 2 = multi-choice
            {
                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = $(cardDivSelector + " .setting-HasCustomAnswer").prop("checked");
                card.CustomAnswerInstruction = $(cardDivSelector + " input.setting-CustomAnswer").val().trim();
                if (card.CustomAnswerInstruction.length > 20) {
                    errorToReload("Other's custom answer is too long.", false);
                    return false;
                }
                card.IsOptionRandomized = $(cardDivSelector + " .setting-IsOptionRandomized").prop("checked");

                // No of options to be selected
                if ((card.Type == 1) || (card.Type == 7)) {
                    card.MaxOptionToSelect = 0;
                    card.MiniOptionToSelect = 0;
                }
                else {
                    card.MaxOptionToSelect = $(cardDivSelector + " .setting-MaximumSelection").val();
                    card.MiniOptionToSelect = $(cardDivSelector + " .setting-MinimumSelection").val();

                    //if (card.MaxOptionToSelect < 1) {
                    //    errorToReload("Minimum selection should be at least 1.", false);
                    //    return false;
                    //}

                    if (card.MiniOptionToSelect < 1) {
                        errorToReload("Minimum selection should be at least 1.", false);
                        return false;
                    }

                    if (card.MaxOptionToSelect < card.MiniOptionToSelect) {
                        errorToReload("Maximum selection value should not be smaller than Minimum selection value.", false);
                        return false;
                    }
                }

                // Options
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";

                var optionId, option = new Object();
                $cardOptions = $(cardDivSelector + " textarea.option-content");
                if ($cardOptions.length == 0) {
                    errorToReload("Please add an option.", false);
                    return false;
                }
                else {

                    for (optionIdx = 0; optionIdx < $cardOptions.length; optionIdx++) {
                        option = null;
                        optionId = $($cardOptions[optionIdx]).data("optionid");

                        // find the option on card
                        for (idx = 0; idx < card.Options.length; idx++) {
                            if (card.Options[idx].OptionId == optionId) {
                                option = card.Options[idx];
                                break;
                            }
                        }

                        if (optionId.startsWith("__")) {
                            option.OptionId = "";
                        }

                        // Update text
                        option.Content = $($cardOptions[optionIdx]).val();
                        if (option.Content.length == 0) {
                            errorToReload("Please fill in your option.", false);
                            return false;
                        }
                        if (option.Content.length > 150) {
                            errorToReload("Option length is too long.", false);
                            return false;
                        }

                        // Update image
                        $img = $(cardDivSelector + " img.optionimage.cardContentImage." + optionId + ":visible");

                        if (($img.length > 0) && ($img.prop("src").length > 0)) {
                            option.HasImage = true;
                            option.Images = []; // Zero image array first
                            option.Images.push({
                                ImageUrl: JSON.stringify({
                                    width: $img.data("width"),
                                    height: $img.data("height"),
                                    base64: $img.prop("src"),
                                    extension: $img.data("extension")
                                })
                            });
                        } else {
                            option.HasImage = false;
                        }

                        // Update logic
                        if (card.Type == 1) {
                            if (($("input#cbCardOptionLogic_" + optionId).prop("checked")) &&
                                ($(cardDivSelector + " .logic-content." + optionId + " .selectedLogic").length > 0)) {
                                option.NextCardId = $(cardDivSelector + " .logic-content." + optionId + " .selectedLogic").data("optionid");
                            } else {
                                option.NextCardId = "";
                            }
                        }
                        else {
                            option.NextCardId = "";
                        }

                        option.Ordering = optionIdx + 1;
                        card.Options[optionIdx] = option;
                    }

                    if (card.Type == 2) {
                        if (card.MiniOptionToSelect == null || card.MiniOptionToSelect == 0) {
                            errorToReload("Your minimum selectable value is invaild.", false);
                            return false;
                        }

                        if (card.MiniOptionToSelect > card.Options.length) {
                            errorToReload("Your minimum selectable value is invaild.", false);
                            return false;
                        }

                        if (card.MaxOptionToSelect > card.Options.length) {
                            errorToReload("Your maximum selectable value is invaild.", false);
                            return false;
                        }

                        if ((card.MaxOptionToSelect <= card.MinOptionToSelect)) {
                            errorToReload("Maximum selectable value must be bigger than minimum selectable value.", false);
                            return false;
                        }
                    }
                }
            }
            else if (card.Type == 3) // 3 = Text
            {
                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.Options = [];
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";
            }
            else if (card.Type == 4) // 4 = number-range
            {
                var numberRangeSelection = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .option_section :checked").val()
                if (numberRangeSelection == 2) // Slider start from Middle (Middle:2)
                {
                    card.StartRangePosition = 2;
                    card.MidRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMiddle").val();
                    card.MidRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMiddleLabel").val();
                    if (card.MidRangeLabel.length == 0) {
                        errorToReload("Please give the middle value a label", false);
                        return false;
                    }
                    if (card.MidRangeLabel.length > 25) {
                        errorToReload("The Middle label is too long", false);
                        return false;
                    }
                    card.MaxRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMax").val();
                    card.MaxRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMaxLabel").val();
                    if (card.MaxRangeLabel.length == 0) {
                        errorToReload("Please give the maximum value a label", false);
                        return false;
                    }
                    if (card.MaxRangeLabel.length > 25) {
                        errorToReload("Maximum label is too long", false);
                        return false;
                    }
                    card.MinRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .lblMinimun").text();
                    card.MinRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMinLabel").val();
                    if (card.MinRangeLabel.length == 0) {
                        errorToReload("Please give the minimum value a label", false);
                        return false;
                    }
                    if (card.MinRangeLabel.length > 25) {
                        errorToReload("Minimum label is too long", false);
                        return false;
                    }
                }
                else  // Slider start from Minimum/Maximum (Minimum:1, Maximum:3)
                {
                    numberRangeSelection = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .maxminSelect").val();
                    card.StartRangePosition = numberRangeSelection;
                    card.MidRange = 5;
                    card.MidRangeLabel = "";
                    card.MaxRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMax").val();
                    card.MaxRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMaxLabel").val();
                    if (card.MaxRangeLabel.length > 25) {
                        errorToReload("Maximum label is too long", false);
                        return false;
                    }
                    card.MinRange = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMin").val();
                    card.MinRangeLabel = $("div.card." + card.CardId + " .cardSetting.cardNumberRange .tbNewCardOptionRangeMinLabel").val();
                    if (card.MinRangeLabel.length > 25) {
                        errorToReload("Minimum label is too long", false);
                        return false;
                    }
                }

                if ((numberRangeSelection == 1) || (numberRangeSelection == 3)) {
                    if (isNaN(parseInt(card.MaxRange, 10))) {
                        errorToReload("Please enter an integer for Maximum value.", false);
                        return false;
                    }

                    if (isNaN(parseInt(card.MinRange, 10))) {
                        errorToReload("Please enter an integer for Minimum value.", false);
                        return false;
                    }

                    if (parseInt(card.MinRange, 10) >= parseInt(card.MaxRange, 10)) {
                        errorToReload("Your Minimum value and Maximum value does not tally", false);
                        return false;
                    }
                }
                if (numberRangeSelection == 2) {
                    if (isNaN(parseInt(card.MidRange, 10))) {
                        errorToReload("Please enter an integer for Middle value.", false);
                        return false;
                    }

                    if (isNaN(parseInt(card.MaxRange, 10))) {
                        errorToReload("Please enter an integer for Maximum value.", false);
                        return false;
                    }

                    if ((parseInt(card.MinRange, 10) >= parseInt(card.MaxRange, 10)) || (parseInt(card.MinRange, 10) >= parseInt(card.MidRange, 10))) {
                        errorToReload("Your Minimum value and Maximum value does not tally", false);
                        return false;
                    }
                }

                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.Options = [];
            }
            else if (card.Type == 5) // 5 = drop-list
            {
                // Behaviour
                card.ToBeSkipped = $(cardDivSelector + " .setting-ToBeSkipped").prop("checked");
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";

                if (card.Options == null) {
                    card.Options = [];
                }

                tempOptions = $(cardDivSelector + " textarea.droplist_options").val().trim().replace(/\n\n/g, "\n").split(/\n/);
                for (idx = 0; idx < tempOptions.length; idx++) {
                    if (tempOptions[idx] == "" || tempOptions[idx].length == 0) {
                        errorToReload("Please fill in your option", false);
                        return false;
                    }
                    else if (tempOptions[idx].length > 150) {
                        errorToReload("Option length is too long", false);
                        return false;
                    }
                    else {
                        var option = new Object();
                        option.Content = tempOptions[idx];
                        option.HasImage = false;
                        option.Ordering = idx + 1;
                        card.Options[idx] = option;
                    }
                }
            }
            else if (card.Type == 6) // 6 = Instructions
            {
                // Behaviour
                card.ToBeSkipped = false;
                card.HasCustomAnswer = false;
                card.CustomAnswerInstruction = "";
                card.IsOptionRandomized = false;
                // No of options to be selected
                card.MaxOptionToSelect = 0;
                card.MiniOptionToSelect = 0;
                // Option
                card.Options = [];
                card.StartRangePosition = 0;
                card.MinRange = 0;
                card.MinRangeLabel = "";
                card.MidRange = 0;
                card.MidRangeLabel = "";
                card.MaxRange = 0;
                card.MaxRangeLabel = "";
            }
            else if (card.Type == 7)
            {
                card.TotalPointsAllocated = $(cardDivSelector + " .assign-total-points input").val();
            }
            else // Other mode 
            {

            }
            /***********************/

            // Update local Json store
            rsTopic.Cards[idx] = card;
            $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));
            $("div#cardList div.card." + cardId).removeClass("opencard");
            ajaxData = { // data format to select specific card
                'Action': 'UPDATE',
                'TopicId': $("#main_content_hfTopicId").val(),
                'CategoryId': $("#main_content_hfCategoryId").val(),
                'CardId': cardId,
                'AdminUserId': $("#main_content_hfAdminUserId").val(),
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'UserData': JSON.stringify(card)
            };

            card.adminUserId = $("#main_content_hfAdminUserId").val();
            card.assessmentId = model.assessment.AssessmentId;

            jQuery.ajax({
                type: "POST",
                url: "/api/AssessmentCard",
                data: JSON.stringify(card),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        toastr.info("Update Successful");
                        fetchAndRefreshListDisplay();
                        if ((card.CardImages !== undefined) && (card.CardImages.length > 0) && card.CardImages[0].ImageUrl !== undefined) {
                            if ((rsTopic.Cards[idx].CardImages.length > 0) && (card.CardImages[0].ImageUrl.slice(0, 4).toLowerCase() !== "http")) {
                                rsTopic.Cards[idx].CardImages[0].ImageUrl = JSON.parse(rsTopic.Cards[idx].CardImages[0].ImageUrl).base64;
                            }
                        }
                        $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));
                    } else {
                        errorToReload("Update unsuccessful. " + d.ErrorMessage, true);
                        return false;
                    }
                },
                error: function (xhr, status, error) {
                    errorToReload("Connection Error, Refreshing page...", true);
                    return false;
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });


        }

        function getMiniCardMarkup(card, mode) {
            var html = "";
            html = html + "    <div class=\"grid-question-number\">";
            html = html + "        <div>";
            html = html + "            <div style=\"color:#ddd;\">";
            html = html + "                <div class=\"number\" style=\"font-size:1.8rem;font-weight:bold;margin-bottom:5px;\">P" + card.Paging + " | Q" + card.Ordering + "</div>";
            html = html + "                <div class=\"\">Code: " + card.CardId + "</div>";
            html = html + "            </div>";
            html = html + "        </div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question\">";
            html = html + "        <label style=\"color:#ddd;\"></label>";
            html = html + "        <div class=\"image-placeholder\">";
            if ((card.HasImage) && (card.CardImages.length > 0))
                html = html + "        <img src=\"" + card.CardImages[0].ImageUrl + "\"/>";
            html = html + "        </div>";
            html = html + "        <div class=\"grid__span--6\">";
            html = html + "             <label style=\"color:#ccc;font-weight:bold;\">Question</label>";
            html = html + "             <div>" + card.Content + "</div>";
            html = html + "        </div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question-icon\">";
            html = html + "        <label style=\"color:#ccc;font-weight:bold;\">Type</label><div>";

            //html = html + "        <img src=\"http://placehold.it/60x60\" title=\"Type\"/>";

            if (card.Type == 1) { // select one icon
                html = html + "<img src=\"/Img/icon_selectone.png\" title=\"Select one\"/>";
            } else if (card.Type == 2) { // multi=choice icon
                html = html + "<img src=\"/Img/icon_multichoice.png\" title=\"Multi choice\"/>";
            } else if (card.Type == 3) { // text icon
                html = html + "<img src=\"/Img/icon_text.png\" title=\"Text\"/>";
            } else if (card.Type == 4) { // range icon
                html = html + "<img src=\"/Img/icon_numberrange.png\" title=\"Number range\"/>";
            } else if (card.Type == 5) { // drop list icon
                html = html + "<img src=\"/Img/icon_droplist.png\" title=\"Dropdown list\"/>";
            } else if (card.Type == 6) { // instruction icon
                html = html + "<img src=\"/Img/icon_instructions.png\" title=\"Instruction\"/>";
            }

            html = html + "    </div></div>";
            html = html + "    <div class=\"grid-question-icon\">";
            html = html + "        <label style=\"color:#ccc;font-weight:bold;\"></label>";
            html = html + "        <div>";
            if (card.HasFromLogic)
                html = html + "<img src=\"/Img/icon_logic_destination.png\" title=\"Destination\"/>";
            if (card.HasToLogic)
                html = html + "<img src=\"/Img/icon_logic_redirect.png\" title=\"Source\"/>";
            else
                html = html + "<span></span>";
            html = html + "        </div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question-icon\" style=\"visibility: hidden;\">";
            html = html + "        <label style=\"color:#ccc;font-weight:bold;visibility:hidden;\">Analytics</label>";
            html = html + "        <div><a href=\"/Survey/Analytic/" + $("#main_content_hfTopicId").val() + "/" + $("#main_content_hfCategoryId").val() + "\"><img src=\"/Img/icon_result.png\" title=\"Analytics\"/></a></div>";
            html = html + "    </div>";
            html = html + "    <div class=\"grid-question-expandable\">";
            html = html + "        <a href=\"javascript:fetchCardFromWebAPI('" + card.CardId + "');\"><i class=\"fa fa-caret-square-o-down fa-lg icon\"></i></a>";
            html = html + "    </div>";
            //html = html + "</div>";

            if (mode === "contentOnly")
                return html;

            //return "<div class=\"card cansort " + card.CardId + "\" data-cardid=\"" + card.CardId + "\">" + html + "</div>";

            if (card.HasFromLogic || card.HasToLogic || card.HasPageBreak) {
                if (card.HasPageBreak)
                    return "<div class=\"card " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div> <div class=\"pagebreak " + card.CardId + "\"><hr style=\"clear:both;border-top:#9C3E1E 4px solid;\" /></div>";
                else
                    return "<div class=\"card " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div>";
            } else {
                if (card.HasPageBreak)
                    return "<div class=\"card cansort " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div> <div class=\"pagebreak " + card.CardId + "\"><hr style=\"clear:both;border-top:#9C3E1E 4px solid;\" /></div>";
                else
                    return "<div class=\"card cansort " + card.CardId + "\" data-cardid=\"" + card.CardId + "\" data-paging=\"" + card.Paging + "\" data-ordering=\"" + card.Ordering + "\">" + html + "</div>";
            }

        }

        function displayMiniCard(cardId) {
            // Find the card with cardId
            var rsTopic, idx, cardDivSelector, html;

            cardDivSelector = "div.card." + cardId;

            if ($(cardDivSelector).hasClass("opencard"))
                $(cardDivSelector).removeClass("opencard");

            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    html = getMiniCardMarkup(rsTopic.Cards[idx], "contentOnly");
                    $(cardDivSelector).html(""); // Clear contents of the card
                    $(cardDivSelector).append(html); // Change the card to 
                    break;
                }
            }
        }

        function updateNumberRangeCounter(textarea, cardId, maxCount) {
            $(textarea).siblings(".survey-lettercount").text((maxCount - textarea.value.length).toString());

            if ($(textarea).hasClass("tbNewCardOptionRangeMinLabel")) {
                if ($(textarea).val().trim().length > 0) {
                    $(".preview-numberrange .irs-min").text($(textarea).val());
                } else {
                    $(".preview-numberrange .irs-min").text($(textarea).siblings(".tbNewCardOptionRangeMin.nonmiddle").val());
                }
            }

            if ($(textarea).hasClass("tbNewCardOptionRangeMaxLabel")) {
                if ($(textarea).val().trim().length > 0) {
                    $(".preview-numberrange .irs-max").text($(textarea).val());
                } else {
                    $(".preview-numberrange .irs-max").text($(textarea).siblings(".tbNewCardOptionRangeMax.nonmiddle").val());
                }
            }
        }

        function updateOptionTextCount(textarea, maxCount) {
            // Find label
            var $textarea = $(textarea);
            var optionid = $textarea.data("optionid");
            var cardid = $textarea.data("cardid");
            var label;// = $(".survey-lettercount." + optionid)[0];

            if (optionid === "")
                label = $textarea.siblings(".survey-lettercount")[0];
            else
                label = $(".survey-lettercount." + optionid)[0];

            var countLength = maxCount - $textarea.val().length;

            if (countLength < 0) {
                textarea.style.borderColor = "red";
            } else {
                textarea.style.borderColor = "";
            }

            label.innerHTML = countLength;

            // find card and update option
            var rsTopic, idx, found, cardId, card;
            cardId = $(textarea).data("cardid");
            card = null;
            found = false;
            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            if (rsTopic.Cards !== undefined) {
                for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                    if (rsTopic.Cards[idx].CardId == cardId) {
                        card = rsTopic.Cards[idx];
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                for (idx = 0; idx < card.Options.length; idx++) {
                    if (card.Options[idx].OptionId === optionid) {
                        card.Options[idx].Content = textarea.value;
                        break;
                    }
                }
                $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));
            }

            updatePreview(cardid);
        }

        function canChangeCardFormat() {
            var rsTopic;
            var isChangable = true;
            // TODO: Read 3 items: survey start-datetime, end-datetime and survey status
            // Base on the 3 items, determine if this card should be changable.

            rsTopic = JSON.parse($("#main_content_hfAssessment").val());

            if ((rsTopic.Status !== 1) && (rsTopic.ProgressStatus === 2)) {
                isChangable = false;
            }
            else {
                isChangable = true;
            }


            return isChangable;
        }

        function getNumberRangeMarkup(startType, card) {
            var html = "";

            if (startType == 2) {
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Middle value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMiddle\" class=\"tbNewCardOptionRangeMiddle middle\" type=\"number\" value=\"" + (card.MidRange || card.MidRange === 0 ? card.MidRange : "") + "\" onblur=\"setNumberRangeMinValue('" + card.CardId + "');\" >";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMiddleLabel\" class=\"tbNewCardOptionRangeMiddleLabel\" placeholder=\"Label\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MidRangeLabel ? card.MidRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeMiddleMiddleCount survey-lettercount\">" + (25 - (card.MidRangeLabel ? card.MidRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Average, Neutral, So-so, etc.</label>";
                html = html + "    </div>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Maximum value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMax\" class=\"tbNewCardOptionRangeMax middle\" type=\"number\" value=\"" + (card.MaxRange || card.MaxRange === 0 ? card.MaxRange : "") + "\" onblur=\"setNumberRangeMinValue('" + card.CardId + "');\">";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMaxLabel\" class=\"tbNewCardOptionRangeMaxLabel\" placeholder=\"Label\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MaxRangeLabel ? card.MaxRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeMiddleMaxCount survey-lettercount\">" + (25 - (card.MaxRangeLabel ? card.MaxRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Excellent, Great, Very good, Amazing, etc.</label>";
                html = html + "    </div>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Minimum value</label>";
                html = html + "        <label class=\"lblMinimun\">" + (card.MinRange || card.MinRange === 0 ? card.MinRange : "0") + "</label>";
                html = html + "        ";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMinLabel\" class=\"tbNewCardOptionRangeMinLabel\" placeholder=\"Label\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MinRangeLabel ? card.MinRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeMiddleMinCount survey-lettercount\">" + (25 - (card.MinRangeLabel ? card.MinRangeLabel : '').length) + "</span>";
                html = html + "              ";
                html = html + "        <label class=\"tip\">Eg. Poor, Bad, Need improvement, etc.</label>";
                html = html + "    </div>";
            } else {
                html = html + "    <select class=\"maxminSelect\">";
                html = html + "        <option value=\"1\">Start from Minimum</option>";
                html = html + "        <option value=\"3\">Start from Maximum</option>";
                html = html + "    </select>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Minimum value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMin\" class=\"tbNewCardOptionRangeMin nonmiddle\" type=\"number\" value=\"" + (card.MinRange || card.MinRange === 0 ? card.MinRange : "") + "\">";
                html = html + "        <input name=\"tbNewCardOptionRangeMinLabel\" class=\"tbNewCardOptionRangeMinLabel\" type=\"text\" placeholder=\"Label(Optional)\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "',25);\" value=\"" + (card.MinRangeLabel ? card.MinRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeNonmiddleMinCount survey-lettercount\">" + (25 - (card.MinRangeLabel ? card.MinRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Poor, Bad, Need improvement, etc.</label>";
                html = html + "    </div>";
                html = html + "    <div class=\"option_section\">";
                html = html + "        <label class=\"inline-label\">Maximum value</label>";
                html = html + "        <input name=\"tbNewCardOptionRangeMax\" class=\"tbNewCardOptionRangeMax nonmiddle\" type=\"number\" value=\"" + (card.MaxRange || card.MaxRange === 0 ? card.MaxRange : "") + "\">";
                html = html + "        <input type=\"text\" name=\"tbNewCardOptionRangeMaxLabel\" class=\"tbNewCardOptionRangeMaxLabel\" placeholder=\"Label(Optional)\" onkeyup=\"updateNumberRangeCounter(this, '" + card.CardId + "', 25);\" value=\"" + (card.MaxRangeLabel ? card.MaxRangeLabel : '') + "\">";
                html = html + "        <span class=\"lblNewCardRangeNonmiddleMaxCount survey-lettercount\">" + (25 - (card.MaxRangeLabel ? card.MaxRangeLabel : '').length) + "</span>";
                html = html + "        <label class=\"tip\">Eg. Excellent, Great, Very good, Amazing, etc.</label>";
                html = html + "    </div>";
            }

            return html;
        }

        function setNumberRangeMinValue(cardId) {
            ReloadErrorToast();
            var mid, max, min;
            mid = $("div.card." + cardId + " .tbNewCardOptionRangeMiddle.middle").val();
            max = $("div.card." + cardId + " .tbNewCardOptionRangeMax.middle").val();

            if (mid == "" || isNaN(mid) || parseFloat(mid).toString().indexOf(".") >= 0) {
                toastr.error("Please enter an integer for Middle value.");
                return false;
            }

            mid = parseInt(mid);
            if (mid > 19 || mid < -19) {
                toastr.error("Middle value must be within -19 and 19.");
                return false;
            }

            if (max == "" || isNaN(max) || parseFloat(max).toString().indexOf(".") >= 0) {
                toastr.error("Please enter an integer for Maximum value.");
                return false;
            }

            max = parseInt(max);
            if (max <= mid || max > 20 || max < -18) {
                toastr.error("Maximum value must be within -18 and 20 and must be bigger than Middle value.");
                return false;
            }

            min = mid * 2 - max;
            $("div.card." + cardId + " .lblMinimun").html(min);
        }

        function changeCardRangeType(cardId, newRangeType) {
            var idx, rsTopic, found, html = "";

            found = false;
            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    found = true;
                }
            }

            html = getNumberRangeMarkup(newRangeType, card);

            $("div.card." + cardId + " .divCardRangeContainer").html(html);

            if (newRangeType == 2) {
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMiddle").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMiddleLabel").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMax").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMaxLabel").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblMinimun").text("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMinLabel").val("");

                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblNewCardRangeMiddleMiddleCount.survey-lettercount").text("25");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblNewCardRangeMiddleMaxCount.survey-lettercount").text("25");

                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .lblNewCardRangeMiddleMinCount.survey-lettercount").text("25");
            } else {
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMin").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMinLabel").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMax").val("");
                $("div.card." + card.CardId + " .cardSetting.cardNumberRange .divCardRangeContainer .tbNewCardOptionRangeMaxLabel").val("");
            }
        }

        function getCardDisplayMarkup(mode, card) {
            var idx, logicCardIdx, html = "", textarea_val, disabledAttr, allocationIdx, tabulationIdx, selectedProp;
            if (canChangeCardFormat()) {
                disabledAttr = "";
            } else {
                disabledAttr = "disabled";
            }

            /* for preview layout */
            var cardDivSelector;
            cardDivSelector = "div.card." + card.CardId;
            for (var i = 1; i < 7; i++) {
                $(cardDivSelector + " .divEditCardType" + i + "PreviewLayout").css("display", "none");
                $(cardDivSelector + " .divEditCardType" + i + "PreviewLayout").css("height", "0px");
            }

            $(cardDivSelector + " .divEditCardType" + mode + "PreviewLayout").css("display", "block");
            $(cardDivSelector + " .divEditCardType" + mode + "PreviewLayout").css("height", "0px");
            /* for preview layout */

            
            if ((mode == 1) || (mode == 2) || (mode == 7)) {  // select one or multi choice or aggregation
                html = "<span class=\"cardOptionList options-container\">";

                var previewOptionsHtml = "";

                // foreach option add input tag
                if (card.Options.length > 0) {
                    for (idx = 0; idx < card.Options.length; idx++) {
                        html = html + "<div style=\"margin-bottom: 10px\" class=\"card-option\" data-optionId=\"" + card.Options[idx].OptionId + "\">";

                        if (canChangeCardFormat()) {
                            html = html + "    <a href=\"javascript:void(0);\" class=\"remove-option-link\" onclick=\"removeOptionField('" + card.CardId + "', '" + card.Options[idx].OptionId + "');\">- Remove option</a>";
                        }
                        html = html + "    <textarea class=\"option-content " + card.Options[idx].OptionId + "\" data-optionid=\"" + card.Options[idx].OptionId + "\" data-cardid=\"" + card.CardId + "\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"updateOptionTextCount(this, 150);\" placeholder=\"Option\" rows=\"2\" cols=\"20\">" + card.Options[idx].Content + "</textarea>";
                        html = html + "    <span class=\"survey-lettercount " + card.Options[idx].OptionId + "\">" + (150 - card.Options[idx].Content.length) + "</span>";
                        html = html + "    <label class=\"upload-option-image\" for=\"fuCardOptionContentImg_" + card.Options[idx].OptionId + "\">";
                        html = html + "        <input id=\"fuCardOptionContentImg_" + card.Options[idx].OptionId + "\" type=\"file\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"displayCardPreviewImage(this, 2560, 2560, '" + card.CardId + "', '" + card.Options[idx].OptionId + "');\">";
                        html = html + "    </label>";
                        if (card.Options[idx].Images.length > 0) {
                            html = html + "    <div class=\"divNewCardOption1ContentImg cardContentImage cardContentImageWrapper " + card.Options[idx].OptionId + "\" style=\"visibility: visible; height: auto; overflow: auto; margin-top:-23px;\" class=\"option-image-content\">";
                            html = html + "        <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.Options[idx].OptionId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                            html = html + "        <img class=\"optionimage cardContentImage cardimage " + card.Options[idx].OptionId + "\" src=\"" + card.Options[idx].Images[0].ImageUrl + "\">";
                            html = html + "    </div>";
                        } else {
                            html = html + "    <div class=\"divNewCardOption1ContentImg cardContentImage cardContentImageWrapper " + card.Options[idx].OptionId + "\" style=\"visibility: hidden; height: 0px; overflow: auto; margin-top:-23px;\" class=\"option-image-content\">";
                            html = html + "        <div class=\"survey-cross\" onclick=\"removeAttachedImg('" + card.Options[idx].OptionId + "');\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                            html = html + "        <img class=\"optionimage cardContentImage cardimage " + card.Options[idx].OptionId + "\">";
                            html = html + "    </div>";
                        }

                        // Restore allocation
                        html = html + "    <div class=\"option-tabulation-allocation\">";
                        for (allocationIdx = 0; allocationIdx < card.Options[idx].Allocations.length; allocationIdx++) {
                            html = html + "<div class=\"option-allocation\">Each point allocated to the answer, system will multiply the value of ";
                            html = html + "    <input type=\"number\" class=\"allocate-weight\" onkeyup=\"cardOptionAllocationValue(this);\" value=\"" + card.Options[idx].Allocations[allocationIdx].Value + "\" /> and add to ";
                            html = html + "    <div class=\"mdl-selectfield\">";
                            html = html + "        <select class=\"allocate-tabulation\" onchange=\"cardOptionAllocationValue(this);\">";
                            html = html + "            <option value=\"\"></option>";
                            for (tabulationIdx = 0; tabulationIdx < model.assessment.Tabulations.length; tabulationIdx++) {
                                if ((card.Options[idx].Allocations[allocationIdx].Tabulation) && (model.assessment.Tabulations[tabulationIdx].TabulationId === card.Options[idx].Allocations[allocationIdx].Tabulation.TabulationId)) {
                                    selectedProp = "selected='selected'";
                                } else {
                                    selectedProp = "";
                                }
                                
                                html = html + "        <option value=\"" + model.assessment.Tabulations[tabulationIdx].TabulationId + "\" " + selectedProp + ">" + model.assessment.Tabulations[tabulationIdx].Label + "</option>";
                            }
                            html = html + "        </select>";
                            html = html + "    </div>";
                            html = html + "</div>";
                        }
                        html = html + "    </div>"; // close option-tabulation-allocation
                        html = html + "    <div>";
                        html = html + "    <span class=\"clickable\" onclick=\"addOptionAllocation(this);\">Add option allocation</span>";
                        html = html + "    </div>";
                        html = html + "</div>";

                        

                        /* for preview layout */
                        if (card.Options != null && card.Options.length > 0) {
                            //for (var i = 0; i < card.Options.length; i++) {
                            if ((mode == 1) || (mode == 7)) {
                                previewOptionsHtml += "<div class=\"preview-option\">";
                                if (card.Options[idx].Images != null && card.Options[idx].Images.length > 0 && card.Options[idx].Images[0].ImageUrl.length > 0) {
                                    previewOptionsHtml += "    <img class=\"preview-option-image " + card.Options[idx].OptionId + "\" src=\"" + card.Options[idx].Images[0].ImageUrl + "\" />";
                                }
                                previewOptionsHtml += "   <div class=\"preview-option-content\">" + card.Options[idx].Content + "</div>";
                                previewOptionsHtml += "   <div class=\"clear-float\"></div>";
                                previewOptionsHtml += "</div>";
                            }
                            else if (mode == 2) {
                                previewOptionsHtml += "<div class=\"preview-option-multi\">";
                                previewOptionsHtml += "   <div class=\"preview-option\" style=\"width: 70%; float: left;\">";
                                if (card.Options[idx].Images != null && card.Options[idx].Images.length > 0 && card.Options[idx].Images[0].ImageUrl.length > 0) {
                                    previewOptionsHtml += "       <img class=\"preview-option-image " + card.Options[idx].OptionId + "\" src=\"" + card.Options[idx].Images[0].ImageUrl + "\" />";
                                }
                                previewOptionsHtml += "           <div class=\"preview-option-content\">" + card.Options[idx].Content + "</div>";
                                previewOptionsHtml += "           <div class=\"clear-float\"></div>";
                                previewOptionsHtml += "   </div>";
                                previewOptionsHtml += "   <img class=\"preview-option-tick\" src=\"/Img/icon_multichoice_tick.png\"";
                                if ((card.Options[idx].Images != null && card.Options[idx].Images.length > 0 && card.Options[idx].Images[0].ImageUrl.length > 0) || card.Options[idx].Content.length > 0) {
                                    previewOptionsHtml += " />";
                                }
                                else {
                                    previewOptionsHtml += " style=\"visibility: hidden;\" />";
                                }
                                previewOptionsHtml += "   <div class=\"clear-float\"></div>";
                                previewOptionsHtml += "</div>";
                            }
                            //}
                        }
                        /* for preview layout */
                    }
                }

                if (mode == 1 && card.HasCustomAnswer) {
                    previewOptionsHtml += "<div class=\"preview-others\">";
                    previewOptionsHtml += "   <div class=\"preview-others-content\">Others:</div>";
                    previewOptionsHtml += "   <div class=\"preview-command-content\">" + card.CustomAnswerInstruction + "</div>";
                    previewOptionsHtml += "   <div class=\"clear-float\"></div>";
                    previewOptionsHtml += "</div>";
                }

                $(cardDivSelector + " .divEditCardType" + mode + "PreviewLayout").html(previewOptionsHtml);

                html = html + "</span>";

                if (canChangeCardFormat()) {
                    html = html + "<a style=\"float:right;\" href=\"javascript:\" class=\"add-option-link\" onclick=\"addNewOptionField('" + card.CardId + "')\">+ Add option</a>";
                    if (mode == 7) {
                        html = html + "<br />";
                        html = html + "<div class=\"assign-total-points\" style=\"clear:both;\">";
                        html = html + "    <label style=\"font-weight: bold\">Total Points <img src=\"/Img/icon_note_small.png\" title=\"Total points to allocate for question.\" /></span></label>";
                        html = html + "    <span>Points add up value <input value=\"" + card.TotalPointsAllocated + "\" onkeydown=\"return (event.keyCode!=13);\" type=\"number\" style=\"width:133px;display:inline-block;\" onkeyup=\"setNewCardValue(22, this, null);\" onblur=\"setNewCardValue(22, this, null);\" /></span><br />";
                        html = html + "</div>";
                    }
                }
            }
            else if (mode == 3) {
                /* for preview layout */
            }
            else if (mode == 4) {
                /* for preview layout */
                html = "";
                html = html + "<div class=\"cardSetting cardNumberRange\" style=\"visibility: visible; overflow: auto; height: auto;\" class=\"form__row form__row--question\">";
                html = html + "     <label style=\"margin-bottom:15px;\">Number range <br />(Minimum: -20 to Maximum: 20)</label>";
                html = html + "     <div class=\"option_section\">";
                html = html + "         Slider starts from<br />";
                html = html + "         <label class=\"inline-label radio\">";
                html = html + "             <input type=\"radio\" name=\"numberRangeStartFrom_" + card.CardId + "\" value=\"1\"";
                if (!(card.StartRangePosition === 2)) {
                    html = html + "             checked=\"checked\"";
                }
                html = html + "                 onchange=\"changeCardRangeType('" + card.CardId + "', 1); \">Minimum/Maximum";
                html = html + "         </label>";
                html = html + "         <label class=\"inline-label radio\">";
                html = html + "             <input type=\"radio\" name=\"numberRangeStartFrom_" + card.CardId + "\" value=\"2\"";
                if ((card.StartRangePosition === 2)) {
                    html = html + "             checked=\"checked\"";
                }
                html = html + "                 onchange=\"changeCardRangeType('" + card.CardId + "', 2); \">Middle";
                html = html + "         </label>"; html = html + "     </div>";
                html = html + "    <div class=\"divCardRangeContainer\" style=\"display: block;\">";
                html = html + getNumberRangeMarkup(card.StartRangePosition, card);
                html = html + "    </div>";

                html = html + "</div>";

                var rangeIdx, rangeValues;
                rangeValues = [];
                for (rangeIdx = card.MinRange; rangeIdx <= card.MaxRange; rangeIdx++) {
                    rangeValues.push(rangeIdx);
                }

                $(".preview-numberrange #range").ionRangeSlider({
                    min: card.MinRange,
                    max: card.MaxRange,
                    from: card.MinRange,
                    values: rangeValues,
                    onFinish: function (data) {
                        console.log("onFinish");
                        if ((card.MinRangeLabel) && (card.MinRangeLabel.length > 0)) {
                            $(".preview-numberrange .irs-min").text(card.MinRangeLabel);
                        } else {
                            $(".preview-numberrange .irs-min").text(card.MinRange.toString());
                        }

                        if ((card.MaxRangeLabel) && (card.MaxRangeLabel.length > 0)) {
                            $(".preview-numberrange .irs-max").text(card.MaxRangeLabel);
                        } else {
                            $(".preview-numberrange .irs-max").text(card.MaxRange.toString());
                        }
                    }
                });
                if ((card.MinRangeLabel) && (card.MinRangeLabel.length > 0)) {
                    $(".preview-numberrange .irs-min").text(card.MinRangeLabel);
                } else {
                    $(".preview-numberrange .irs-min").text(card.MinRange.toString());
                }

                if ((card.MaxRangeLabel) && (card.MaxRangeLabel.length > 0)) {
                    $(".preview-numberrange .irs-max").text(card.MaxRangeLabel);
                } else {
                    $(".preview-numberrange .irs-max").text(card.MaxRange.toString());
                }

            }
            else if (mode == 5) {
                /* for preview layout */
                textarea_val = "";
                for (idx = 0; idx < card.Options.length; idx++) {
                    if (textarea_val.length > 0)
                        textarea_val = textarea_val + "\n";
                    textarea_val = textarea_val + card.Options[idx].Content;
                }

                html = "";
                html = html + "    <textarea rows=\"16\" class=\"droplist_options\" placeholder=\"Please type in your options here, subsequent options separated by 'Enter' key.\" onkeyup=\"updateSelectPreview(this);\" style=\"overflow-y:auto;\" >" + textarea_val + "</textarea>";
                html = html + "    <label>Please type in your options here, subsequent options separated by 'Enter' key.</label>";

                updateSelectPreview($(html)[0]);

            }
            else if (mode == 6) {
                html = "";

                /* for preview layout */
            }
            else {
                html = "";
            }

            return html;
        }

        function displayLogicContent(btn) {
            $(btn).siblings(".logic-content").toggle();
        }

        function selectLogic(option) {
            $(option).siblings().removeClass("selectedLogic");
            $(option).addClass("selectedLogic");
            $(option).parent().siblings(".logic-button").text($(option).text());
            $(option).parent().hide();
        }

        function truncateString(string) {
            console.log(string.length);
            if (string.length >= 50) {
                return string.substring(0, 40) + "...";
            } else {
                return string;
            }
        }

        function setCardSettingDisplay(mode, card) {
            // hide all
            var cardDivSelector, cardSettingSelector, idx, html;

            // Hide all card settings first
            cardDivSelector = "div.card." + card.CardId;
            cardSettingSelector = cardDivSelector + " .cardSettingSection .cardSetting";
            $(cardSettingSelector).hide();

            // Then display the card settings for the mode
            $(cardSettingSelector + ".cardBehaviour").show();   // display behaviour
            $(".setting-HasCustomAnswer").parent().hide();
            $(".setting-CustomAnswer").hide();

            if ((mode == 1) || (mode == 7)) { // select one
                $(".setting-HasCustomAnswer").parent().show();
                $(".setting-CustomAnswer").show();

                if (card.hasOwnProperty("HasCustomAnswer") && card.HasCustomAnswer) {
                    $(cardSettingSelector + " .setting-HasCustomAnswer").prop("checked", card.HasCustomAnswer);
                    if (card.hasOwnProperty("CustomAnswerInstruction"))
                        $(cardSettingSelector + " input.setting-CustomAnswer").val(card.CustomAnswerInstruction);
                    $(cardSettingSelector + " div.setting-CustomAnswer").show();

                } else {
                    $(cardSettingSelector + " .setting-HasCustomAnswer").prop("checked", false);
                    $(cardSettingSelector + " div.setting-CustomAnswer").hide();
                }

                if (card.hasOwnProperty("IsOptionRandomized") && card.IsOptionRandomized)
                    $(cardSettingSelector + " .setting-IsOptionRandomized").prop("checked", card.IsOptionRandomized);
                else
                    $(cardSettingSelector + " .setting-IsOptionRandomized").prop("checked", false);


            } else if (mode == 2) { // multi-choice
                $(cardSettingSelector + ".cardMultiChoice").show(); // display the minimum and maximum
                var minimumSelection, maximumSelection;
                if (card.Options.length > 0) {
                    minimumSelection = 1;
                    maximumSelection = card.Options.length;
                } else {
                    minimumSelection = 0;
                    maximumSelection = 0;
                }
                $(cardSettingSelector + ".cardMultiChoice .setting-MinimumSelection").val(minimumSelection);
                $(cardSettingSelector + ".cardMultiChoice .setting-MaximumSelection").val(maximumSelection);
            }
            else if (mode == 6) {
                $(cardSettingSelector + ".cardBehaviour").hide();
            }

            // All must show background, notes and pagebreak
            if (card.hasOwnProperty("BackgroundType")) {
                $(cardSettingSelector + " .setting-BackgroundType").val(card.BackgroundType);

                if (card.BackgroundType == 1) {
                    $(cardSettingSelector + " .setting-BackgroundType1").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_1a_big.png)");
                }
                else if (card.BackgroundType == 2) {
                    $(cardSettingSelector + " .setting-BackgroundType2").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_2a_big.png)");
                }
                else if (card.BackgroundType == 3) {
                    $(cardSettingSelector + " .setting-BackgroundType3").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_3a_big.png)");
                }
                else {
                    // Other situation
                    $(cardSettingSelector + " .setting-BackgroundType1").prop("checked", true);
                    $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_1a_big.png)");
                }
            }
            else {
                $(cardSettingSelector + " .setting-BackgroundType1").prop("checked", true);
                $(cardDivSelector + " .survey-preview").css("background-image", "url(/Img/bg_survey_1a_big.png)");
            }

            if (card.hasOwnProperty("ToBeSkipped") && card.ToBeSkipped)
                $(cardSettingSelector + " .setting-ToBeSkipped").prop("checked", card.ToBeSkipped);
            else
                $(cardSettingSelector + " .setting-ToBeSkipped").prop("checked", false);

            $(cardSettingSelector + " .setting-Notes").val(card.Note);
            $(cardDivSelector + " .survey-preview img.right").prop("title", card.Note);

            if (card.hasOwnProperty("HasPageBreak") && card.HasPageBreak)
                $(cardSettingSelector + " .setting-PageBreak").prop("checked", card.HasPageBreak);
            $(cardSettingSelector + ".displaySection").show(); // display notes and page break
        }

        function deleteCard(cardId) {
            ReloadErrorToast();
            var ajaxData;

            ajaxData = { // data format to select specific card
                'cardId': cardId,
                'adminUserId': $("#main_content_hfAdminUserId").val(),
                'assessmentId': model.assessment.AssessmentId
            };

            jQuery.ajax({
                type: "DELETE",
                url: "/api/AssessmentCard?cardId=" + ajaxData.cardId + "&adminUserId=" + ajaxData.adminUserId + "&assessmentId=" + ajaxData.assessmentId,
                data: ajaxData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    if (d.Success) {
                        //updateListCard(d.Card);
                        // Update the card in local JSON
                        //      Find the card in local JSON
                        //      Remove the card
                        //      Save to local JSON
                        // Remove the card from display
                        var cardIdx, rsTopic, $cards, displayedCardIdx, html, cardDivSelector, found;
                        rsTopic = JSON.parse($("#main_content_hfAssessment").val());
                        $cards = $("#cardList div.card");
                        if (rsTopic !== null) {
                            for (cardIdx = 0; cardIdx < rsTopic.Cards.length; cardIdx++) {
                                if (rsTopic.Cards[cardIdx].CardId == cardId) {
                                    rsTopic.Cards.splice(cardIdx, 1);
                                    $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));
                                    $("#cardList div.card." + cardId).remove();
                                    fetchAndRefreshListDisplay();
                                    break;
                                }
                            }
                        }
                    } else {
                        toastr.error("Operation failed. " + d.ErrorMessage);
                    }
                },
                error: function (xhr, status, error) {
                    toastr.error("Error connecting to server.");
                },
                complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });
        }

        function setCardDisplay(mode, cardId) {
            var rsTopic, idx, cardDivSelector, html, card;

            cardDivSelector = "div.card." + cardId;

            // search and find card om json store
            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                if (rsTopic.Cards[idx].CardId === cardId) {
                    card = rsTopic.Cards[idx];
                    card.Type = mode;
                    $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));

                    // Get and set html markup for question/answer section
                    html = getCardDisplayMarkup(mode, card);
                    $(cardDivSelector + " div.form__row.form__row--option").children().remove();
                    $(cardDivSelector + " div.form__row.form__row--option").append(html);

                    // Get and set html markup for preview section // comment out for now

                    // Get and set html markup for question settings 
                    setCardSettingDisplay(mode, card);

                    // Clear other buttons and set selected button
                    $(cardDivSelector + " ul.tabs__list .tabs__list__item").removeClass("selected-card-type");
                    $(cardDivSelector + " ul.tabs__list .tabs__list__item.cardtype" + mode).addClass("selected-card-type");
                    break;
                } // 
            } // end-for search card
        }

        function fetchAndRefreshListDisplay() {
            ReloadErrorToast();
            var ajaxData;

            ajaxData = { // data format to select specific card
                'Action': 'REFRESH',
                'TopicId': $("#main_content_hfTopicId").val(),
                'CategoryId': $("#main_content_hfCategoryId").val(),
                'AdminUserId': $("#main_content_hfAdminUserId").val(),
                'CompanyId': $("#main_content_hfCompanyId").val(),
                'CardName': $("#main_content_tbSearchQuestion").val().trim().length <= 0 ? null : $("#main_content_tbSearchQuestion").val().trim()
                //,'UserData': newCardPostion
            };

            ajaxData = {
                'userId': $("#main_content_hfAdminUserId").val(),
                'assessmentId': model.assessment.AssessmentId
            };

            jQuery.ajax({
                type: "GET",
                url: "/api/Assessment",
                data: ajaxData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function (xhr, settings) {
                    ShowProgressBar();
                },
                success: function (d, status, xhr) {
                    // Merge fetch data with what is Json store
                    var localRSTopic, remoteCardIdx, localCardIdx, cardFound;
                    localRSTopic = JSON.parse($("#main_content_hfAssessment").val());

                    // For each card returned by server
                    // Find the corresponding card in hfJsonStore // $("#cardList div.card")
                    // if the card is currently open, update P and Q numbers only
                    // else replace then entire card
                    for (remoteCardIdx = 0; remoteCardIdx < d.Assessment.Cards.length; remoteCardIdx++) {
                        cardFound = false;
                        for (localCardIdx = 0; localCardIdx < localRSTopic.Cards.length; localCardIdx++) {
                            if (localRSTopic.Cards[localCardIdx].CardId == d.Assessment.Cards[remoteCardIdx].CardId) {
                                // Card found
                                // Check if card is open
                                if ($("div.card." + d.Assessment.Cards[remoteCardIdx].CardId).hasClass("opencard")) {
                                    // update card P&Q numbers
                                    localRSTopic.Cards[localCardIdx].Paging = d.Assessment.Cards[remoteCardIdx].Paging;
                                    localRSTopic.Cards[localCardIdx].Ordering = d.Assessment.Cards[remoteCardIdx].Ordering;
                                } else {
                                    // replace minicard details
                                    localRSTopic.Cards[localCardIdx] = d.Assessment.Cards[remoteCardIdx];
                                }
                                break;
                            }
                        }

                        if (cardFound === false) {
                            // new remote card, add to store
                            localRSTopic.Cards.push(d.Assessment.Cards[remoteCardIdx]);
                        }
                    }

                    // Store back to hfJsonStore, then refresh list display
                    //$("#main_content_hfAssessment").val(JSON.stringify(d.Topic));
                    $("#main_content_hfAssessment").val(JSON.stringify(localRSTopic));
                    refreshListDisplay(d.Assessment.Cards);
                },
                error: function (xhr, status, error) {
                    toastr.error("Error connecting to server.");
                },
                complete: function (xhr, status) {
                    // Do any task that you want to do after the ajax call is complete
                    HideProgressBar();
                }
            });
        }

        function refreshListDisplay(cardsToDisplay) {
            var cardIdx, rsTopic, $cards, displayedCardIdx, html, cardDivSelector, found, card;

            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            $cards = $("#cardList div.card");

            if (cardsToDisplay) {
                $("#cardList").children().remove();
                for (cardIdx = 0; cardIdx < cardsToDisplay.length; cardIdx++) {
                    card = cardsToDisplay[cardIdx];
                    html = getMiniCardMarkup(card, card.Type);
                    $("#cardList").append(html);
                }
                HideProgressBar();
                return;
            }

            if (rsTopic !== null) {
                //$("#cardList").children().remove();
                for (cardIdx = 0; cardIdx < rsTopic.Cards.length; cardIdx++) {
                    card = rsTopic.Cards[cardIdx];
                    found = false;
                    // find the card in displayed card list
                    for (displayedCardIdx = 0; displayedCardIdx < $cards.length; displayedCardIdx++) {
                        if ($($cards[displayedCardIdx]).data("cardid") == rsTopic.Cards[cardIdx].CardId) {
                            // Update the contents
                            found = true;
                            html = getMiniCardMarkup(rsTopic.Cards[cardIdx], "contentOnly");
                            cardDivSelector = "div.card." + rsTopic.Cards[cardIdx].CardId;

                            // Check pagebreak
                            if (rsTopic.Cards[cardIdx].HasPageBreak) {
                                if ($("div.pagebreak." + rsTopic.Cards[cardIdx].CardId).length <= 0) {
                                    $("div.card." + rsTopic.Cards[cardIdx].CardId).after("<div class=\"pagebreak " + rsTopic.Cards[cardIdx].CardId + "\"><hr style=\"clear:both;border-top:#9C3E1E 4px solid;\" /></div>");
                                }
                            } else {
                                $("div.pagebreak." + rsTopic.Cards[cardIdx].CardId).remove();
                            }

                            $(cardDivSelector).html(""); // Clear contents of the card
                            $(cardDivSelector).append(html); // Change the card to 
                            break;
                        }
                    }

                    if (!found) {
                        html = getMiniCardMarkup(card, card.Type);
                        $("#cardList").append(html);
                    }
                }
            }

            // Filter #cardList based on cardsToDisplay
            //if (cardsToDisplay) {
            //    var cardListChildren = $("#cardList").children();
            //    var cardListChildrenCount = $("#cardList").children().length;
            //    var childrenIdx, displayCardIdx, displayCardFound;
            //    for (childrenIdx = 0; childrenIdx < cardListChildrenCount; childrenIdx++) {

            //        displayCardFound = false;

            //        for (displayCardIdx = 0; displayCardIdx < cardsToDisplay.length; displayCardIdx++) {
            //            cardsToDisplay[displayCardIdx] == "";
            //        }

            //        if ($("#cardList .card." + cardsToDisplay[0].CardId).length) {

            //        }

            //    }
            //}

            HideProgressBar();
        }

        function updateNewCardNumber(page_number, question_number) {
            $(".add-question__questionnumber .number")[0].innerText = "P" + page_number;
            $(".add-question__questionnumber .number")[1].innerText = "Q" + question_number;
        }

        function createNewOption() {
            var option = {
                AnsweredByUser: null,
                Content: "Yes",
                HasImage: false,
                Images: [],
                IsCustom: false,
                IsSelected: false,
                NextCard: null,
                NextCardId: null,
                NumberOfSelection: 0,
                OptionId: null,
                Ordering: 0,
                PercentageOfSelection: 0,
            };

            return option;
        }

        function addNewOptionField(cardId) {
            var rsTopic, idx, found, card, html;

            card = null;
            found = false;
            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            if (rsTopic.Cards !== undefined) {
                for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                    if (rsTopic.Cards[idx].CardId == cardId) {
                        card = rsTopic.Cards[idx];
                        found = true;
                        break;
                    }
                }
            }

            if (found) {

                // Update card options
                var optionIdx, tempOption, $image, imageSrc, $logic, $logicText;
                for (optionIdx in card.Options) { // foreach option in card
                    tempOption = card.Options[optionIdx];

                    // Update card content
                    tempOption.Content = $("div.card." + card.CardId + " .option-content." + tempOption.OptionId).val();

                    // Update card image
                    $image = $(".optionimage.cardContentImage.cardimage." + tempOption.OptionId);
                    imageSrc = $image.attr("src");

                    console.log(imageSrc);
                    if (imageSrc) { // has attached image
                        if (imageSrc.startsWith("data:")) { // handles cases where new image are attached
                            if (tempOption.Images.length <= 0) { // no image in state; add image to state
                                tempOption.Images.push({
                                    ImageUrl: imageSrc
                                });
                                // update card
                            } else { // has image state; update image state
                                tempOption.Images[0].ImageId = "";
                                tempOption.Images[0].ImageUrl = imageSrc;
                            }
                        } // else implies original image since last post
                    } else { // no or removed attached image
                        if (tempOption.Images.length > 0) {
                            tempOption.Images.splice(0, 1);
                        }
                    }

                    // Update card logic
                    $logic = $("div.card." + card.CardId + " .cbNewCardOption1Logic." + tempOption.OptionId);

                    if ($logic.prop("checked")) { // logic is checked
                        tempOption.NextCardId = $("div.card." + card.CardId + " .logic-content." + tempOption.OptionId + " .selectedLogic").data("optionid");
                        var logicCardIdx;
                        for (logicCardIdx = 0; logicCardIdx < card.LogicCards.length; logicCardIdx++) {
                            if (card.LogicCards[logicCardIdx].CardId == tempOption.NextCardId) {
                                tempOption.NextCard = card.LogicCards[logicCardIdx];
                                break;
                            }
                        }
                    } else { // logic is not checked or is unchecked
                        tempOption.NextCardId = "";
                        tempOption.NextCard = null;
                    }
                }
                var newOptionObj = getOptionObj();
                newOptionObj.OptionId = "undefined_" + (card.Options.length + 1).toString();

                card.Options.push(newOptionObj);

                $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));

                setCardDisplay(card.Type, card.CardId);
                updateListCard(card);

            } else {
                // Handling for new card
                var nextOptionNum = ($("#divNewCardOptions").children().length + 1).toString();

                html = "";
                html = html + "<div style=\"margin-bottom: 10px\">";
                html = html + "    <textarea name=\"tbNewCardOption" + nextOptionNum + "\" class=\"option-content\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"textCounter(this,'lblNewCardOption" + nextOptionNum + "ContentCount', 150); setNewCardValue(2, this, " + nextOptionNum + ");\" onblur=\"setNewCardValue(2, this, " + nextOptionNum + ");\" placeholder=\"Option " + nextOptionNum + "\" rows=\"2\" cols=\"20\"></textarea>";
                html = html + "    <span id=\"lblNewCardOption" + nextOptionNum + "ContentCount\" class=\"survey-lettercount\">150</span>";
                html = html + "    <label class=\"upload-option-image\" for=\"fuNewCardOption" + nextOptionNum + "ContentImg\">";
                html = html + "        <input type=\"file\" id=\"fuNewCardOption" + nextOptionNum + "ContentImg\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"previewImage(this, 2560, 2560 ,2, " + nextOptionNum + ");\">";
                html = html + "    </label>";
                html = html + "    <div id=\"divNewCardOption" + nextOptionNum + "ContentImg\" style=\"visibility: hidden; height: 0px; overflow: auto;\" class=\"option-image-content\">";
                html = html + "        <div class=\"survey-cross\" onclick=\"removeContentImg(2, " + nextOptionNum + ");\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                html = html + "        <img id=\"imgNewOption" + nextOptionNum + "ContentImgPreview\">";
                html = html + "    </div>";
                html = html + "</div>";

                $("#divNewCardOptions").append(html);

                if (NewCard) {
                    //NewCard.options[NewCard.options.length + 1] = getOptionObj();
                    NewCard.options.push(getOptionObj());
                }
            }
        }

        function removeOptionField(cardId, optionId) {
            var rsTopic, idx, found, card, html;

            card = null;
            found = false;
            rsTopic = JSON.parse($("#main_content_hfAssessment").val());
            if (rsTopic.Cards !== undefined) {
                for (idx = 0; idx < rsTopic.Cards.length; idx++) {
                    if (rsTopic.Cards[idx].CardId == cardId) {
                        card = rsTopic.Cards[idx];
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                // Find and splice option 
                for (idx = 0; idx < card.Options.length; idx++) {
                    if (card.Options[idx].OptionId === optionId) {
                        card.Options.splice(idx, 1);
                        break;
                    }
                }

                // Update card options
                var optionIdx, tempOption, $image, imageSrc, $logic, $logicText;
                for (optionIdx in card.Options) { // foreach option in card
                    tempOption = card.Options[optionIdx];

                    // Update card content
                    tempOption.Content = $("div.card." + card.CardId + " .option-content." + tempOption.OptionId).val();

                    // Update card image
                    $image = $(".optionimage.cardContentImage.cardimage." + tempOption.OptionId);
                    imageSrc = $image.attr("src");

                    console.log(imageSrc);
                    if (imageSrc) { // has attached image
                        if (imageSrc.startsWith("data:")) { // handles cases where new image are attached
                            if (tempOption.Images.length <= 0) { // no image in state; add image to state
                                tempOption.Images.push({
                                    ImageUrl: imageSrc
                                });
                                // update card
                            } else { // has image state; update image state
                                tempOption.Images[0].ImageId = "";
                                tempOption.Images[0].ImageUrl = imageSrc;
                            }
                        } // else implies original image since last post
                    } else { // no or removed attached image
                        if (tempOption.Images.length > 0) {
                            tempOption.Images.splice(0, 1);
                        }
                    }

                    // Update card logic
                    $logic = $("div.card." + card.CardId + " .cbNewCardOption1Logic." + tempOption.OptionId);

                    if ($logic.prop("checked")) { // logic is checked
                        tempOption.NextCardId = $("div.card." + card.CardId + " .logic-content." + tempOption.OptionId + " .selectedLogic").data("optionid");
                        var logicCardIdx;
                        for (logicCardIdx = 0; logicCardIdx < card.LogicCards.length; logicCardIdx++) {
                            if (card.LogicCards[logicCardIdx].CardId == tempOption.NextCardId) {
                                tempOption.NextCard = card.LogicCards[logicCardIdx];
                                break;
                            }
                        }
                    } else { // logic is not checked or is unchecked
                        tempOption.NextCardId = "";
                        tempOption.NextCard = null;
                    }
                }


                $("#main_content_hfAssessment").val(JSON.stringify(rsTopic));

                setCardDisplay(card.Type, card.CardId);
                updateListCard(card);

            } else {
                // Handling for new card

                //var nextOptionNum = ($("#spanNewCardOptions").children().length + 1).toString();

                //html = "";
                //html = html + "<div style=\"margin-bottom: 10px\">";
                //html = html + "    <textarea name=\"tbNewCardOption" + nextOptionNum + "\" class=\"option-content\" onkeydown=\"return (event.keyCode!=13);\" onkeyup=\"textCounter(this,'lblNewCardOption" + nextOptionNum + "ContentCount', 150); setNewCardValue(2, this, " + nextOptionNum + ");\" onblur=\"setNewCardValue(2, this, " + nextOptionNum + ");\" placeholder=\"Option " + nextOptionNum + "\" rows=\"2\" cols=\"20\"></textarea>";
                //html = html + "    <span id=\"lblNewCardOption" + nextOptionNum + "ContentCount\" class=\"survey-lettercount\">150</span>";
                //html = html + "    <label class=\"upload-option-image\" for=\"fuNewCardOption" + nextOptionNum + "ContentImg\">";
                //html = html + "        <input type=\"file\" id=\"fuNewCardOption" + nextOptionNum + "ContentImg\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" onchange=\"previewImage(this, 2560, 2560 ,2, " + nextOptionNum + ");\">";
                //html = html + "    </label>";
                //html = html + "    <div id=\"divNewCardOption" + nextOptionNum + "ContentImg\" style=\"visibility: hidden; height: 0px; overflow: auto;\" class=\"option-image-content\">";
                //html = html + "        <div class=\"survey-cross\" onclick=\"removeContentImg(2, " + nextOptionNum + ");\">X</div>";
                //html = html + "        <img id=\"imgNewOption" + nextOptionNum + "ContentImgPreview\">";
                //html = html + "    </div>";
                //html = html + "</div>";

                //$("#spanNewCardOptions").append(html);

                //if (NewCard) {
                //    //NewCard.options[NewCard.options.length + 1] = getOptionObj();
                //    NewCard.options.push(getOptionObj());
                //}
            }
        }

        function updatePreview(cardId) {
            var card;
            var rsTopic, idx, html, found, card, cardImage;

            $("div.card." + cardId + " .divEditCardType1PreviewLayout").children().remove();
            $("div.card." + cardId + " .divEditCardType2PreviewLayout").children().remove();
            $("div.card." + cardId + " .divEditCardType7PreviewLayout").children().remove();

            var $optionContent = $("div.card." + cardId + " .cardOptionList.options-container .option-content");
            for (idx = 0; idx < $optionContent.length; idx++) {
                html = "";
                html = html + "<div class=\"preview-option\">   ";
                cardImage = $($optionContent[idx]).siblings(".cardContentImage").children("img.cardContentImage")[0];
                if (cardImage) {
                    html = html + "<img class=\"preview-option-image " + $($optionContent[idx]).data("optionid") + "\" src=\"" + cardImage.src + "\">";
                }

                html = html + "    <div class=\"preview-option-content\">" + $optionContent[idx].value + "</div>   ";
                html = html + "    <div class=\"clear-float\"></div>";
                html = html + "</div>";
                $("div.card." + cardId + " .divEditCardType1PreviewLayout").append(html);
                $("div.card." + cardId + " .divEditCardType2PreviewLayout").append(html);
                $("div.card." + cardId + " .divEditCardType7PreviewLayout").append(html);
            }

            var isTickOtherAnswer = $("div.card." + cardId + " .setting-HasCustomAnswer").prop('checked');
            if (isTickOtherAnswer) {
                html = "";
                html += "<div class=\"preview-others\">";
                html += "   <div class=\"preview-others-content\">Others:</div>";
                html += "   <div class=\"preview-command-content\">" + $("div.card." + cardId + " input.setting-CustomAnswer").val() + "</div>";
                html += "   <div class=\"clear-float\"></div>";
                html += "</div>";
                $("div.card." + cardId + " .divEditCardType1PreviewLayout").append(html);
                $("div.card." + cardId + " .divEditCardType7PreviewLayout").append(html);
            }
        }

        /**********
        functions related to assessment
        */
        function getContentImageObj() {
            return {
                'base64': '',
                'extension': '',
                'width': 0,
                'height': 0
            };
        }

        function prevImage(e) {
            // Find tabulation from model.assessment.tabulation_list
            var tabulationItem, tabulationItemIndex, tabulation;

            tabulationItem = $(e).parents(".tabulation-item");
            tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
            if (tabulationItemIndex >= 0) {
                tabulation = model.assessment.Tabulations[tabulationItemIndex];

                if (tabulation.current_table_image_index == 0) {
                    tabulation.current_table_image_index = tabulation.Images.length - 1;
                } else {
                    tabulation.current_table_image_index = tabulation.current_table_image_index - 1;
                }

                $(e).siblings(".currentImage")[0].src = tabulation.Images[tabulation.current_table_image_index].ImageUrl;
            }
        }

        function removeTabulationImage(e) {
            // Find tabulation from model.assessment.tabulation_list
            var tabulationItem, tabulationItemIndex, current_tabulation;

            tabulationItem = $(e).parents(".tabulation-item");
            tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
            //current_tabulation = model.assessment.tabulation_list[tabulationItemIndex];
            current_tabulation = model.assessment.Tabulations[tabulationItemIndex];

            if (!current_tabulation.table_images) {
                current_tabulation.table_images = current_tabulation.Images;
            }

            if (current_tabulation.table_images && current_tabulation.table_images.length > 0)
            {
                current_tabulation.table_images.splice(current_tabulation.current_table_image_index, 1);

                if (current_tabulation.current_table_image_index < current_tabulation.table_images.length) {
                    tabulationItem.find(".currentImage")[0].src = current_tabulation.table_images[current_tabulation.current_table_image_index];
                } else {
                    current_tabulation.current_table_image_index = current_tabulation.current_table_image_index - 1;
                    if (current_tabulation.current_table_image_index >= 0) {
                        tabulationItem.find(".currentImage")[0].src = current_tabulation.table_images[current_tabulation.current_table_image_index];
                    } else {
                        tabulationItem.find(".currentImage")[0].src = "/Img/icon-upload-photo.png";
                        tabulationItem.find(".remove-tabulation").hide();
                    }
                }
            }
            

        }

        function nextImage(e) {
            // Find tabulation from model.assessment.tabulation_list
            var tabulationItem, tabulationItemIndex, tabulation;

            tabulationItem = $(e).parents(".tabulation-item");
            tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
            if (tabulationItemIndex >= 0) {
                tabulation = model.assessment.Tabulations[tabulationItemIndex];

                if (!tabulation.getNextTableImage) {
                    tabulation.getNextTableImage = function () {
                        if (this.table_images.length > 0) {
                            if (this.current_table_image_index < (this.table_images.length - 1)) {
                                this.current_table_image_index = this.current_table_image_index + 1;
                            } else { // tabulation.current_table_image_index >= tabulation.table_images.length
                                this.current_table_image_index = 0;
                            }

                            return this.table_images[this.current_table_image_index];
                        }
                    };
                }
                $(e).siblings(".currentImage")[0].src = tabulation.getNextTableImage();
            }
        }

        function newResult() {
            this.Title = "";
            this.Ordering = 0;
            this.TabulationIdList = [];
            this.Condition = 0;
            this.ShowInAssessment = false;
            this.VisibleToSelf = false;
            // this.VisibleToOthers = false;
            this.VisibleToOthersForCompletedUser = false;
            this.VisibleToOthersForIncompletedUser = false;
            this.id = "";
            this.Tabulations = [];
        }

        function tabulation() {
            // Tabulation properties
            this.TabulationId = "";
            this.Label = "";
            this.InfoTitle = "";
            this.InfoDescription = "";
            this.current_table_image_index = 0;
            this.table_images = [];
            this.questions = [];
            this.id = "";
            this.Images = [];
            this.ImagesUpload = [];

            // Tabulation functions
            this.getPrevTableImage = function () {
                if (this.table_images.length > 0) {
                    if (this.current_table_image_index == 0) {
                        this.current_table_image_index = this.table_images.length - 1;
                    } else { // tabulation.current_table_image_index >= tabulation.table_images.length
                        this.current_table_image_index = this.current_table_image_index - 1;
                    }

                    return this.table_images[this.current_table_image_index];
                }
            };

            this.getNextTableImage = function () {
                if (this.table_images.length > 0) {
                    if (this.current_table_image_index < (this.table_images.length - 1)) {
                        this.current_table_image_index = this.current_table_image_index + 1;

                    } else { // tabulation.current_table_image_index >= tabulation.table_images.length
                        this.current_table_image_index = 0;
                    }

                    return this.table_images[this.current_table_image_index];
                }
            };

        }

        function addTabulation(tab, tabCount) {

            if (tab === undefined)
            {
                tab = new tabulation();
                tab.Label = "";
                tab.InfoDescription = "";
                tab.InfoTitle = "";
                tab.current_table_image_index = 0;
                if (!model.assessment.tabulation_list_lastcount) {
                    model.assessment.tabulation_list_lastcount = 0;
                }
                tab.id = "tabulation_" + (model.assessment.tabulation_list_lastcount + 1).toString();
                tab.TabulationId = "tabulation_" + (model.assessment.tabulation_list_lastcount + 1).toString();
                
                model.assessment.Tabulations.push(tab);
                model.assessment.tabulation_list_lastcount = model.assessment.tabulation_list_lastcount + 1;
                tabCount = model.assessment.tabulation_list_lastcount;
                updateLocalJSON();
                
            }

            if (!tabCount) {
                tabCount = 1;
            }
            
            var html = "", imageSrc = "/Img/icon-upload-photo.png";
            html = html + "<div class=\"grid__span--6 grid__span--last tabulation-item " + tab.TabulationId + "\">";
            html = html + "    <fieldset style=\"border:1px solid #ccc;padding:1em;width:98%;margin-top:8px;\">";
            html = html + "        <legend style=\"background-color:#FF9800;color:#FFF9F0; padding:0 1em;\">TABLE " + tabCount + "</legend>";
            html = html + "        <div class=\"tabulation-label-div\" style=\"border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
            html = html + "            <input class=\"tabulation-label\" type=\"text\" value=\"" + tab.Label + "\" placeholder=\"Enter table name\" onkeyup=\"updateTableLabel(this);updateTextCounter(this,'.tabulation-label-div .lettercount',60);\" />";
            html = html + "            <span class=\"lettercount\" style=\"bottom: 12px; right: 5px; position: absolute;\">" + (60 - tab.Label.length).toString() + "</span>";
            html = html + "        </div>";
            html = html + "        <div class=\"form__row anonymous__tip\" style=\"display:inline-block;width:100%;margin:0;\">";
            html = html + "            <span style=\"float:left;width:30px;\">Info</span>";
            html = html + "            <img class=\"icon-img\" src=\"/Img/icon_note_small.png\" title=\"Participant's name will not be display in the Survey Report\" />";
            html = html + "        </div>";
            html = html + "        <div style=\"width:100%; display:inline-block;\">";
            html = html + "            <div style=\"width:49%;border:dashed 1px #666;display:inline-flex;vertical-align:top;height:160px;\">";
            html = html + "                <img style=\"margin:auto;\" src=\"/Img/prev.png\" onclick=\"prevImage(this);\" />";
            html = html + "                <div style=\"z-index:1;" + ((tab.Images.length > 0)? "" : "display:none;") + "\" class=\"remove-tabulation image-button\">";
            html = html + "                    <i class=\"fa fa-times fa-2x\" aria-hidden=\"true\" style=\"width:0;\" onclick=\"removeTabulationImage(this);\"></i>";
            html = html + "                </div>";
            html = html + "";
            if (tab.Images.length > 0) {
                imageSrc = tab.Images[0].ImageUrl;
            }
            html = html + "                <img style=\"margin:auto; max-width:180px;max-height:155px;\" class=\"currentImage\" src=\"" + imageSrc + "\" onclick=\"selectImage(this);\" />";
            html = html + "                <label style=\"width:0;\">";
            html = html + "                    <input type=\"file\" class=\"fileSelector\" accept=\"image/*\" title=\"Add image\" style=\"display: none\" />";
            html = html + "                </label>";
            html = html + "                <img style=\"margin:auto;\" src=\"/Img/next.png\" onclick=\"nextImage(this);\" />";
            html = html + "            </div>";
            html = html + "            <div style=\"width:49%;color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;\">";
            html = html + "                <div class=\"tabulation-title-div\" style=\"width:100%;color: rgba(209, 209, 209, 1); font-size: .9em; vertical-align: middle; display: inline-block; position: relative;\">";
            html = html + "                    <input class=\"tabulation-title\" type=\"text\" value=\"" + tab.InfoTitle + "\" placeholder=\"Enter title\" onkeyup=\"updateTableLabel(this);updateTextCounter(this,'.tabulation-title-div .lettercount',60);\" />";
            html = html + "                    <span class=\"lettercount\" style=\"bottom: 12px; right: 5px; position: absolute;\">" + (60 - tab.InfoTitle.length).toString() + "</span>";
            html = html + "                </div>";
            html = html + "                <div class=\"tabulation-description-div\"  style=\"width:100%;color: rgba(209, 209, 209, 1); font-size: .9em; vertical-align: middle; display: inline-block; position: relative;\">";
            html = html + "                    <textarea class=\"tabulation-description\" style=\"height:115px\" placeholder=\"Description\" onkeyup=\"updateTableLabel(this);updateTextCounter(this,'.tabulation-description-div .lettercount',9999);\">" + tab.InfoDescription + "</textarea>";
            html = html + "                    <span class=\"lettercount\" style=\"bottom: 15px; right: 10px; position: absolute;\">" + (9999 - tab.InfoDescription.length).toString() + "</span>";
            html = html + "                </div>";
            html = html + "            </div>";
            html = html + "        </div>";
            html = html + "        <div class=\"table-links\">";
            html = html + "            <div style=\"display:inline-block;width:100%;\">";
            html = html + "                <a href=\"#\" style=\"color:#ccc\">Edit image</a>";
            html = html + "            </div>";
            html = html + "        </div>";
            html = html + "        <div class=\"table-links\">";
            html = html + "            <div style=\"display:inline-block;width:49%;\">";
            html = html + "                <a href=\"#\" style=\"color:#ccc\">Questions affected</a>";
            html = html + "            </div>";
            html = html + "            <div style=\"display:inline-block;width:49%;text-align:right;\">";
            html = html + "                <span onclick=\"deleteTable(this);\" style=\"color:red;cursor:pointer;\">Delete table</span>";
            html = html + "            </div>";
            html = html + "        </div>";
            html = html + "    </fieldset>";
            html = html + "</div>";

            $(".grid__inner.tabulation-list").append(html);

            //return html;
        }

        function selectImage(e) {
            $(e).parents(".tabulation-item").find(".fileSelector").click();
        }

        function updateTableLabel(e) {
            // find the tabulation of text box.

            var $e, tabulationItem, tabulationItemIndex, tabulation, idx, optionElement;
            $e = $(e);

            tabulationItem = $e.parents(".tabulation-item");
            tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);

            if (tabulationItemIndex >= 0) {
                tabulation = model.assessment.Tabulations[tabulationItemIndex];
                // update the model label text
                if ($e.hasClass("tabulation-label")) {
                    tabulation.Label = $e.val();
                }
                if ($e.hasClass("tabulation-title")) {
                    tabulation.InfoTitle = $e.val();
                }
                if ($e.hasClass("tabulation-description")) {
                    tabulation.InfoDescription = $e.val();
                }

                if ($(".assessment-result-list option[value='" + tabulation.TabulationId + "']").length <= 0) {
                    $(".assessment-result-list .affected-tabulation-list").append("<option value=\"" + tabulation.TabulationId + "\">" + tabulation.Label + "</option");
                } else {
                    $(".assessment-result-list option[value='" + tabulation.TabulationId + "']").text(tabulation.Label);
                }

                // for each result_page, update option text
                for (idx = 0; idx < model.assessment.Answers.length; idx++) {
                    // update affected table dropdown list
                    optionElement = $(".assessment-result-list ." + model.assessment.Answers[idx].id + " .affected-tabulation-list option[value='" + tabulation.TabulationId + "']");
                    
                    if (optionElement.length > 0) {
                        optionElement.text(tabulation.Label);
                        // update selected affected table drop down list
                        $(".assessment-result-list ." + model.assessment.Answers[idx].id + " .affected-tabulations-panel ." + tabulation.TabulationId).text(tabulation.Label);
                    }

                }
            }

            updateLocalJSON();
        }

        function removeAffectedTabulation(e) {
            var assessmentResultElement, assessmentResultIndex, assessmentResult, html, resultTabIdx;
            var affectedTabulationSelect;
            assessmentResultElement = $(e).parents(".assessment-result");
            assessmentResultIndex = $(".assessment-result-list").children().index(assessmentResultElement);
            assessmentResult = model.assessment.Answers[assessmentResultIndex];

            for (resultTabIdx = 0; assessmentResult.Tabulations.length; resultTabIdx++) {
                if (assessmentResult.Tabulations[resultTabIdx].TabulationId == $(e).parent().data("value")) {
                    assessmentResult.Tabulations.splice(resultTabIdx, 1);
                    break;
                }
            }
            var toRemoveIndex;
            toRemoveIndex = assessmentResult.TabulationIdList.indexOf($(e).parent().data("value"));
            if (toRemoveIndex >= 0) {
                assessmentResult.TabulationIdList.splice(toRemoveIndex, 1);
            }

            updateLocalJSON();

            // Remove element markup.
            $(e).parent().remove();
        }

        function addAffectedTable(e) {
            var assessmentResultElement, assessmentResultIndex, assessmentResult, html, idx;
            var affectedTabulationSelect, isInList;
            assessmentResultElement = $(e).parents(".assessment-result");
            assessmentResultIndex = $(".assessment-result-list").children().index(assessmentResultElement);
            assessmentResult = model.assessment.Answers[assessmentResultIndex];
            isInList = false;

            affectedTabulationSelected = assessmentResultElement.find(".affected-tabulation-list :selected");
            for (idx = 0; idx < assessmentResult.Tabulations.length; idx++) {
                if (assessmentResult.Tabulations[idx].TabulationId == affectedTabulationSelected.val()) {
                    isInList = true;
                    break;
                }
            }

            if (!isInList) {
                
                assessmentResult.Tabulations.push({
                    'TabulationId': affectedTabulationSelected.val()
                });

                if (affectedTabulationSelected.val() == "") {
                    return;
                }

                html = "";
                html = html + "<div class=\"tag affected-tabulation " + affectedTabulationSelected.val() + "\" data-value=\"" + affectedTabulationSelected.val() + "\">";
                html = html + "    <span class=\"tag__label\">" + affectedTabulationSelected.text() + "</span>";
                html = html + "    <i class=\"fa fa-times\" aria-hidden=\"true\" onclick=\"removeAffectedTabulation(this);\"></i>";
                html = html + "</div>";

                // affected-tabulations-panel
                assessmentResultElement.find(".affected-tabulations-panel").append(html);
            }
        }

        function removeResult(e) {
            var assessmentResultElement, assessmentResultIndex, assessmentResult;
            assessmentResultElement = $(e).parents(".assessment-result");
            assessmentResultIndex = $(".assessment-result-list").children().index(assessmentResultElement);

            if (assessmentResultIndex >= 0) { // remove result page from model
                model.assessment.Answers.splice(assessmentResultIndex, 1);
            }

            // remove result page markup
            $(e).parents(".assessment-result").remove();
        }

        function addResult(result, resultCount) {
            var checkboxProp = "";
            var idx, html, r, tabulationIdx;

            if (!result) {
                result = new newResult();
                result.AnswerId = "result_" + (model.assessment.result_list_lastcount + 1).toString();
                model.assessment.Answers.push(result);
                model.assessment.result_list_lastcount = model.assessment.result_list_lastcount + 1;
                resultCount = model.assessment.result_list_lastcount;
            }

            if (!resultCount) {
                resultCount = 1;
            }
            
            html = "";
            html = html + "<div class=\"assessment-result " + result.AnswerId + "\">";
            html = html + "    <div style=\"float:right;\">";
            html = html + "        <i class=\"fa fa-arrows fa-3x\" style=\"background-color: #eee; padding: 6px; border: 1px solid #ccc;\" aria-hidden=\"true\"></i>";
            html = html + "    </div>";
            html = html + "    <label style=\"font-weight:bold;font-size:14pt; clear:both;\">Model Answers</label>";
            html = html + "    <div class=\"grid__inner\" style=\"background-color: transparent;\">";
            html = html + "        <div class=\"grid__span--6\">";
            html = html + "            <label style=\"font-weight:bold; font-size:12pt;\">Results Page " + resultCount + "</label>";
            html = html + "            <div class=\"answer-div " + result.AnswerId + "\" style=\"border-image: none; width: 76%; color: rgba(209, 209, 209, 1); vertical-align: middle; display: inline-block; position: relative;\">";
            html = html + "            <input class=\"result-title\" style=\"font-size:12pt;\" type=\"text\" value=\"" + result.Title + "\" onkeyup=\"updateTextCounter(this,'.answer-div ." + result.AnswerId + " .lettercount',60);\" />";
            html = html + "            <span class=\"lettercount\" style=\"bottom: 12px; right: 5px; position: absolute;\">" + (60 - result.Title.length).toString() + "</span>";
            html = html + "            </div>";
            html = html + "            <label style=\"font-weight:bold;font-size:14pt;\">Affected table</label>";
            html = html + "            <div class=\"mdl-selectfield\" style=\"display:inline-block;width:80%;\">";

            // get a list of option value
            html = html + "                <select class=\"affected-tabulation-list\">";
            html = html + "                    <option>Select the table</option>";
            for (idx = 0; idx < model.assessment.Tabulations.length; idx++) {
                html = html + "                    <option value=\"" + model.assessment.Tabulations[idx].TabulationId + "\">" + model.assessment.Tabulations[idx].Label + "</option>";
            }
            html = html + "                </select>";
            html = html + "            </div>";
            html = html + "            <button type=\"button\" onclick=\"addAffectedTable(this);\">Add</button>";
            html = html + "            <div class=\"affected-tabulations-panel\">";
            for (tabulationIdx = 0; tabulationIdx < result.Tabulations.length; tabulationIdx++) {
                if (result.Tabulations[tabulationIdx] == null) {
                    continue;
                }
                html = html + "<div class=\"affected-tabulation " + result.Tabulations[tabulationIdx].TabulationId + "\" data-value=\"" + result.Tabulations[tabulationIdx].TabulationId + "\">";
                html = html + result.Tabulations[tabulationIdx].Label;
                html = html + "    <i class=\"fa fa-times\" aria-hidden=\"true\" onclick=\"removeAffectedTabulation(this);\"></i>";
                html = html + "</div>";
            }
            html = html + "            </div>";
            html = html + "        </div>";
            html = html + "        <div class=\"grid__span--6 grid__span--last\">";
            html = html + "            <label style=\"font-weight:bold;font-size:14pt;\">Condition</label>";
            html = html + "            <div class=\"mdl-selectfield\">";
            html = html + "                <select class=\"result-condition\" style=\"height:36px;\">";
            html = html + "                    <option value=\"1\">Table with highest points</option>";
            html = html + "                    <option value=\"2\">Table with lowest points</option>";
            html = html + "                </select>";
            html = html + "            </div>";
            html = html + "            <label style=\"font-weight:bold;font-size:14pt;\">Show</label>";
            html = html + "            <ul>";
            html = html + "                <li>";
            html = html + "                    <span class=\"setting-checkbox\">";
            if (result.ShowInAssessment) {
                checkboxProp = "checked=checked";
            } else {
                checkboxProp = "";
            }
            html = html + "                        <input class=\"result-showinassessment\" type=\"checkbox\" " + checkboxProp + ">";
            html = html + "                        <label>In Responsive Assessment</label>";
            html = html + "                    </span>";
            html = html + "                </li>";
            html = html + "                <li>";
            html = html + "                    <span class=\"setting-checkbox\">";
            if (result.VisibleToSelf) {
                checkboxProp = "checked=checked";
            } else {
                checkboxProp = "";
            }
            html = html + "                        <input class=\"result-visibletoself\" type=\"checkbox\" " + checkboxProp + ">";
            html = html + "                        <label>Visible to self in Profile Page</label>";
            html = html + "                    </span>";
            html = html + "                </li>";
            html = html + "                <li>";
            if (result.VisibleToOthers) {
                checkboxProp = "checked=checked";
            } else {
                checkboxProp = "";
            }
            html = html + "                    <span class=\"setting-checkbox\">";
            html = html + "                        <input class=\"result-visibletoothers\" type=\"checkbox\" " + checkboxProp + ">";
            html = html + "                        <label>Visible to others in Profile Page</label>";
            html = html + "                    </span>";
            html = html + "                </li>";
            html = html + "                <li style=\"margin-left:2em;\">";
            html = html + "                    <label style=\"font-weight:bold;font-size:14pt;\">Display</label>";
            html = html + "                    <ul>";
            html = html + "                        <li>";
            html = html + "                            <span class=\"setting-checkbox\">";
            if (result.VisibleToOthersForCompletedUser) {
                checkboxProp = "checked=checked";
            } else {
                checkboxProp = "";
            }
            html = html + "                                <input class=\"result-visibletoothersforcompleteduser\" type=\"checkbox\" " + checkboxProp + ">";
            html = html + "                                <label>To those who have completed the Assessment</label>";
            html = html + "                            </span>";
            html = html + "                        </li>";
            html = html + "                        <li>";
            html = html + "                            <span class=\"setting-checkbox\">";
            if (result.VisibleToOthersForIncompletedUser) {
                checkboxProp = "checked=checked";
            } else {
                checkboxProp = "";
            }
            html = html + "                                <input class=\"result-visibletoothersforincompleteduser\" type=\"checkbox\" " + checkboxProp + ">";
            html = html + "                                <label>To those who have not started or incomplete</label>";
            html = html + "                            </span>";
            html = html + "                        </li>";
            html = html + "                    </ul>";
            html = html + "                </li>";
            html = html + "            </ul>";
            html = html + "        </div>";
            html = html + "    </div>";
            html = html + "    <div style=\"width:100%;text-align:right;\">";
            html = html + "        <span style=\"color:red;cursor:pointer;\" onclick=\"removeResult(this);\">Delete Result Page</span>";
            html = html + "    </div>";
            html = html + "    <hr />";
            html = html + "</div>";

            $(".assessment-result-list").append(html);
        }

        $(document).on("change", ".fileSelector", function (e) {
            var tabulationItem, tabulationItemIndex, current_tabulation;
            tabulationItem = $(e.target).parents(".tabulation-item");
            tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
            var inputFile = e.target.files[0];
            var inputFileType = inputFile.type;

            if (tabulationItemIndex >= 0) {
                current_tabulation = model.assessment.Tabulations[tabulationItemIndex];

                //var cropper = new cropbox(options);
                var reader = new FileReader();
                reader.onload = function (e) {
                    var image = new Image();
                    image.src = e.target.result;
                    image.onload = function () {
                        var w = this.width;
                        var h = this.height;
                        var s = inputFile.width;

                        if (current_tabulation.table_images === undefined) {
                            current_tabulation.table_images = [];
                        }
                        current_tabulation.table_images.push(e.target.result);
                        //options.imgSrc = e.target.result;
                        //cropper = new cropbox(options);
                        tabulationItem.find(".currentImage")[0].src = e.target.result;
                        tabulationItem.find(".remove-tabulation").show();

                        var contentImg = getContentImageObj();
                        contentImg.base64 = this.src;
                        contentImg.extension = inputFileType;
                        contentImg.width = w;
                        contentImg.height = h;

                        current_tabulation.ImagesUpload.push(contentImg);
                        current_tabulation.Images.push(contentImg);
                        // should add to Images[] in tabulation as well
                        updateLocalJSON();
                    }

                }

                reader.readAsDataURL(this.files[0]);
            }
        });
        

        $("#main_content_videoDisplay").on("ended", function (e) {
            $("#playVideoButton").show();
            $("#removeVideoButton").show();
        });

        $("#main_content_videoDisplay").on("loadedmetadata", function (e) {
            // ZX: Comment out. Don't resize video placeholder
            //$("#videoPlaceholder").width(this.videoWidth);
            //$("#videoPlaceholder").height(this.videoHeight);

            // Set position for playVideoButton
            //var top = (($("#videoPlaceholder").height() / 2) - $("#removeVideoButton").height() - 24) + "px";
            //$("#playVideoButton").css("top", top);
        });

        $("#main_content_videoDisplay").on("loadeddata", function (e) {
            // Generate thumbnail
            if (this.src.startsWith("blob"))
            {
                var scale = 1;
                var canvas = document.createElement("canvas");
                canvas.width = this.videoWidth * scale;
                canvas.height = this.videoHeight * scale;
                canvas.getContext('2d').drawImage(this, 0, 0, canvas.width, canvas.height);
                var thumbnailUrl = canvas.toDataURL();
                model.assessment.VideoThumbnailUrl = thumbnailUrl;
                model.assessmentUpload.videoThumbnailUpload = {
                    'base64': thumbnailUrl,
                    'filename': "video_thumbnail.png"
                };
                
                updateLocalJSON();
            }
        });

        $("#videoFileInput").change(function (e) {
            var file,
                fileType,
                fileReader,
                fileUrl,
                fileName,
                videoNode;

            fileReader = new FileReader();
            fileReader.onload = function (e) {
                model.assessment.VideoUrl = e.target.result;
                model.assessmentUpload.videoUpload = {
                    'base64': e.target.result,
                    'filename': fileName
                };
                if (model.assessment.VideoUrl.length <= 0) {
                    $("#video_section").hide();
                } else {
                    $("#video_section").show();
                }
                updateLocalJSON();
            }
            file = e.target.files[0];
            fileType = file.type;
            fileName = file.name;
            videoNode = $("#main_content_videoDisplay")[0];
            fileUrl = window.URL.createObjectURL(file);
            videoNode.src = fileUrl;

            $("#videoPlaceholder").show();
            $("#playVideoButton").show();
            $("#removeVideoButton").show();

            fileReader.readAsDataURL(file);
        });

        function updateTextCounter(element, counterSelector, maxCount) {
            var letterLeft = maxCount - $(element).val().length;
            $(element).siblings(".lettercount").text(letterLeft);
            if ($(element).val().length > maxCount) {
                $(element).siblings(".lettercount").text(0);
                $(element).val($(element).val().slice(0, maxCount));
            }
        }

        $(document).ready(function () {
            "use strict";
            var idx, html;

            // Read JSON from hfAssessment hiddenfield variable 
            model.assessmentUpload = JSON.parse($("#main_content_hfAssessmentUpload").val());
            model.assessment = JSON.parse($("#main_content_hfAssessment").val());
            if (model.assessment != null) {

                //if (model.assessment.imageBannersUpload === undefined) {
                //    model.assessment.imageBannersUpload = [];
                //}
                model.assessment.imageBannersUpload = model.assessment.Banners;
                
                if (model.assessment.imageBannersUpload.length > 0) {
                    var $preview_img_list = $("#popup_addimage div.preview-image img");
                    for (idx = 0; idx < model.assessment.imageBannersUpload.length; idx++) {
                        html = "";
                        html = html + "<div class=\"img-container\" >";
                        html = html + "    <div class=\"remove-img-banner\" onclick=\"removeImgBanner(this);\"><i class=\"fa fa-times\" aria-hidden=\"true\"></i></div>";
                        html = html + "    <img class=\"img-banner\" style=\"max-height:100px;\" src=\"" + model.assessment.imageBannersUpload[idx].ImageUrl + "\" />";
                        html = html + "</div>";
                        $(".divPreviewImageList").append(html);

                        // Update popup_addimage
                        $preview_img_list[idx].src = model.assessment.imageBannersUpload[idx].ImageUrl;
                        $($preview_img_list[idx]).siblings().show();
                    }
                }
                
                model.assessment.StartDate = moment(model.assessment.StartDate).format("YYYY-MM-DD");
                if (model.assessment.Coach) {
                    model.assessment.Coach.LastActiveTimestamp = moment.utc(model.assessment.Coach.LastActiveTimestamp).format();
                    model.assessment.Coach.LastModifiedProfileTimestamp = moment.utc(model.assessment.Coach.LastModifiedProfileTimestamp).format();
                    model.assessment.Coach.LastModifiedStatusTimestamp = moment.utc(model.assessment.Coach.LastModifiedStatusTimestamp).format();
                    model.assessment.Coach.LastLoginDateTime = moment.utc(model.assessment.Coach.LastLoginDateTime).format();
                }

                if (model.assessment.VideoUrl.length <= 0) {
                    $("#video_section").hide();
                } else {
                    $("#video_section").show();
                }

                ////////////////////////////////////////////////////////////////////////////////
                // Display all the assessment tabulations
                if (model.assessment.Tabulations.length > 0) {
                    
                    model.assessment.tabulation_list_lastcount = model.assessment.Tabulations.length;

                    var orderedTabulations = model.assessment.Tabulations.sort(function (a, b) { return a.Ordering > b.Ordering });
                    for (idx = 0; idx < orderedTabulations.length; idx++) {
                        // Render each tabulation
                        orderedTabulations[idx].current_table_image_index = 0;
                        orderedTabulations[idx].getNextTableImage = function () {
                            if (this.Images.length > 0) {
                                if (this.current_table_image_index < (this.Images.length - 1)) {
                                    this.current_table_image_index = this.current_table_image_index + 1;

                                } else { // tabulation.current_table_image_index >= tabulation.table_images.length
                                    this.current_table_image_index = 0;
                                }
                                
                                if (this.Images[this.current_table_image_index].ImageUrl !== undefined) {
                                    return this.Images[this.current_table_image_index].ImageUrl;
                                } else {
                                    return this.Images[this.current_table_image_index].base64;
                                }
                                
                            }
                        };
                        orderedTabulations[idx].getPrevTableImage = function () {
                            if (this.Images.length > 0) {
                                if (this.current_table_image_index == 0) {
                                    this.current_table_image_index = this.Images.length - 1;
                                } else { // tabulation.current_table_image_index >= tabulation.table_images.length
                                    this.current_table_image_index = this.current_table_image_index - 1;
                                }

                                //return this.Images[this.current_table_image_index].ImageUrl;
                                if (this.Images[this.current_table_image_index].ImageUrl !== undefined) {
                                    return this.Images[this.current_table_image_index].ImageUrl;
                                } else {
                                    return this.Images[this.current_table_image_index].base64;
                                }
                            }
                        };
                        addTabulation(orderedTabulations[idx], (idx+1));
                    }
                } // end Display all the assessment tabulations

                ////////////////////////////////////////////////////////////////////////////////
                // Display all the result pages
                if (model.assessment.Answers.length > 0) {
                    
                    model.assessment.result_list_lastcount = model.assessment.Answers.length;
                    for (idx = 0; idx < model.assessment.Answers.length; idx++) {
                        addResult(model.assessment.Answers[idx], (idx+1));
                    }
                }
            }


            // Read JSON from hiddenfield variable 
            // Render each item in json list 
            var cardIdx;
            if ($("#main_content_hfAssessment").val() == "")
                return;

            var rsTopic = JSON.parse($("#main_content_hfAssessment").val());

            if (rsTopic !== null) {
                // foreach card in rsTopic.Cards
                // Display rsTopic.Cards[0]
                for (cardIdx = 0; cardIdx < rsTopic.Cards.length; cardIdx++) {
                    $("#cardList").append(getMiniCardMarkup(rsTopic.Cards[cardIdx]));
                }
            }

            // $($("#cardList div.card.cansort")[13]).removeClass("cansort");

            // ZX disable sorting for now.
            //$("#cardList").sortable({
            //    items: "> .card.cansort",
            //    cancel: ".opencard",
            //    axis: "y",
            //    stop: function (e, ui) {
            //        // on stop send signal, send ajax request updating of card's new position

            //        // first find the new position
            //        var ajaxData, tempCardIdx, targetCardId, $cardDivs, newCardPostion, prevCardDiv, nextCardDiv;

            //        targetCardId = ui.item.data("cardid");
            //        $cardDivs = $("#cardList div.card");

            //        newCardPostion = 1; // default to 1
            //        for (tempCardIdx = 0; $cardDivs.length; tempCardIdx++) {
            //            if ($($cardDivs[tempCardIdx]).data("cardid") == targetCardId) {
            //                newCardPostion = tempCardIdx + 1;
            //                prevCardDiv = $cardDivs[tempCardIdx - 1];
            //                nextCardDiv = $cardDivs[tempCardIdx + 1];
            //                break;
            //            }
            //        }

            //        // If same position, do nothing
            //        if (newCardPostion === $("div.card." + targetCardId).data("ordering"))
            //            return;

            //        // Do checking; card cannot move outside of page
            //        if (prevCardDiv !== undefined) {
            //            if ($("div.card." + targetCardId).data("paging") !== $(prevCardDiv).data("paging")) {
            //                $("div.card." + $(prevCardDiv).data("cardid"))
            //                if ($("div.pagebreak." + $(prevCardDiv).data("cardid")).length <= 0) {
            //                    ReloadErrorToast();
            //                    toastr.error("Cannot move to another page.");
            //                    $("#cardList").sortable("cancel");
            //                    return;
            //                }
            //            }
            //        }

            //        // Send ajax call for re-ordering of cards
            //        ajaxData = { // data format to select specific card
            //            'Action': 'REORDER',
            //            'TopicId': $("#main_content_hfTopicId").val(),
            //            'CategoryId': $("#main_content_hfCategoryId").val(),
            //            'CardId': targetCardId,
            //            'AdminUserId': $("#main_content_hfAdminUserId").val(),
            //            'CompanyId': $("#main_content_hfCompanyId").val(),
            //            'UserData': newCardPostion
            //        };

            //        jQuery.ajax({
            //            type: "POST",
            //            url: "/api/RSCardRearrange",
            //            data: JSON.stringify(ajaxData),
            //            contentType: "application/json; charset=utf-8",
            //            dataType: "json",
            //            beforeSend: function (xhr, settings) {
            //                ShowProgressBar();
            //            },
            //            success: function (d, status, xhr) {
            //                if (d.Success) {
            //                    // if success make another ajax call to fetch list
            //                    fetchAndRefreshListDisplay();
            //                } else {
            //                    ReloadErrorToast();
            //                    toastr.error("Operation failed. " + d.ErrorMessage + " Sorting will be reverted.");
            //                    $("#cardList").sortable("cancel");
            //                }
            //            },
            //            error: function (xhr, status, error) {
            //                HideProgressBar();
            //                ReloadErrorToast();
            //                toastr.error("Error connecting to server.");
            //            },
            //            complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
            //            }
            //        });

            //    }
            //});
        });

        function errorToReload(errorMsg, isNeedReload) {
            ReloadErrorToast();
            toastr.error(errorMsg);
            HideProgressBar();
            if (isNeedReload) {
                setTimeout(function () {
                    window.location.reload(1);
                }, 2000);
            }
        }

        function videoController() {
            this.play = function (elementSelector) {
                var videoElement;
                if ($(elementSelector).length <= 0)
                    return;
                videoElement = $(elementSelector)[0];
                videoElement.play();
                $("#playVideoButton").hide();
                $("#removeVideoButton").hide();
            }
            this.removeVideo = function (elementSelector) {
                $("#videoPlaceholder").hide();
                $("#playVideoButton").hide();
                $("#removeVideoButton").hide();
                $("#videoFileInput").val("");
                var videoNode = $(elementSelector)[0];
                videoNode.src = "";
                $("#video_section").hide();
            }

        }

        function addOptionAllocation(e) {
            var idx, html = "", changeFunction;

            // Find NewCard option index
            var $cardList, $card, cardIdx, card, cardOption, cardOptionIdx, option;
            cardOption = $(e).parents(".card-option");
            cardOptionIdx = $(e).parents(".options-container").children().index(cardOption);

            if ($(e).parent().parents("#add-question-card").length > 0) {
                option = NewCard.options[cardOptionIdx];
                option.Allocations.push({
                    'multiplier': '',
                    'tabulationId': ''
                });
                changeFunction = "setNewCardOptionAllocationValue";
            } else {
                // find
                $cardList = $(e).parents("#cardList");
                $card = $(e).parents(".card");
                cardIdx = $cardList.children().index($card);
                card = model.assessment.Cards[cardIdx];
                option = card.Options[cardOptionIdx];
                if (option.Allocations === undefined) {
                    option.Allocations = [];
                }
                option.Allocations.push({
                    'multiplier': '',
                    'tabulationId': ''
                });
                updateLocalJSON();
                changeFunction = "cardOptionAllocationValue";
            }

            html = html + "<div class=\"option-allocation\">Each point allocated to the answer, system will multiply the value of ";
            html = html + "    <input type=\"number\" class=\"allocate-weight\" onkeyup=\"" + changeFunction + "(this);\" /> and add to ";
            html = html + "    <div class=\"mdl-selectfield\">";
            html = html + "        <select class=\"allocate-tabulation\" onchange=\"" + changeFunction + "(this);\">";
            html = html + "            <option value=\"\"></option>";
            for (idx = 0; idx < model.assessment.Tabulations.length; idx++) {
                html = html + "        <option value=\"" + model.assessment.Tabulations[idx].TabulationId + "\">" + model.assessment.Tabulations[idx].Label + "</option>";
            }
            html = html + "        </select>";
            html = html + "    </div>";
            html = html + "</div>";

            $(e).parents(".card-option").find(".option-tabulation-allocation").append(html);
        }

        function deleteTable(e) {
            // Find tabulation from model.assessment.tabulation_list
            var tabulationItem, tabulationItemIndex, tabulation;

            tabulationItem = $(e).parents(".tabulation-item");
            tabulationItemIndex = $(".grid__inner.tabulation-list").children().index(tabulationItem);
            if (tabulationItemIndex >= 0) {
                //tabulation = model.assessment.tabulation_list[tabulationItemIndex];
                //$(e).siblings(".currentImage")[0].src = tabulation.getNextTableImage();
                model.assessment.Tabulations.splice(tabulationItemIndex, 1);
                tabulationItem.remove();
            }
        }
    </script>



</asp:Content>

