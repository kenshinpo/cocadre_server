﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Assessment
{
    public partial class AssessmentList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;
        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            if (!Page.IsPostBack)
            {
                String categoryId = String.Empty;
                if (Page.RouteData.Values["CategoryId"] != null)
                {
                    categoryId = Page.RouteData.Values["CategoryId"].ToString();
                }

                ViewState["currentSortField"] = "lbSortAssessmentTitle";
                ViewState["currentSortDirection"] = "asc";


            } // if (!Page.IsPostBack)

            // Load survey list
            RefreshSurveyListView();
        }

        private void RefreshSurveyListView()
        {
            ListView lv = this.lvSurvey;
            LinkButton lb;

            // Fetch data and sort
            AssessmentSelectAllResponse response = asc.SelectAllAssessments(this.adminInfo.UserId);
            if (response.Success)
            {
                if (!string.IsNullOrWhiteSpace(tbFilterSurvey.Text))
                {
                    response.Assessments = response.Assessments.Where(r => r.Title.ToLowerInvariant().Contains(tbFilterSurvey.Text.ToLowerInvariant())).ToList();
                }
                
                this.lvSurvey.DataSource = response.Assessments;
                this.lvSurvey.DataBind();

                switch (ViewState["currentSortField"].ToString())
                {
                    case "lbSortAssessmentTitle":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Assessments.OrderBy(r => r.Title).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Assessments.OrderByDescending(r => r.Title).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;
                    case "lbSortAssessmentCoach":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Assessments.OrderBy(r => r.Coach.FirstName).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Assessments.OrderByDescending(r => r.Coach.FirstName).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;

                    case "lbSortAssessmentStatus":
                        lb = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                        if (lb != null)
                        {
                            if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                            {
                                this.lvSurvey.DataSource = response.Assessments.OrderBy(r => r.Status).ToList();
                            }
                            else
                            {
                                this.lvSurvey.DataSource = response.Assessments.OrderByDescending(r => r.Status).ToList();
                            }
                            this.lvSurvey.DataBind();
                        }
                        break;

                }

            } // end if (response.Success)

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    RSTopicUpdateResponse response = asc.UpdateRSTopicStatus(hfSurveyId.Value, hfCategoryId.Value, adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16
            //        (lbAction.CommandArgument));
            //    String toastMsg = String.Empty;
            //    if (response.Success)
            //    {
            //        mpePop.Hide();
            //        RefreshSurveyListView();

            //        if (Convert.ToInt16(lbAction.CommandArgument) == 2)
            //        {
            //            toastMsg = hfSurveyName.Value + " has been set to active.";
            //        }
            //        else if (Convert.ToInt16(lbAction.CommandArgument) == 3)
            //        {
            //            toastMsg = hfSurveyName.Value + " has been set to hidden.";
            //        }
            //        else if (Convert.ToInt16(lbAction.CommandArgument) == -1)
            //        {
            //            toastMsg = hfSurveyName.Value + " has been deleted.";
            //        }
            //        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
            //    }
            //    else
            //    {
            //        Log.Error("RSTopicUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            //        mpePop.Show();
            //        toastMsg = "Failed to change status, please check your internet connection.";
            //        MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
            //    }
            //    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            //}
            //catch (Exception ex)
            //{
            //    Log.Error(ex.ToString(), ex, this.Page);
            //}
        }

        protected void lbActionCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        protected void ibFilterSurvey_Click(object sender, EventArgs e)
        {
            RefreshSurveyListView();
        }

        protected void lvSurvey_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                LinkButton lbAssessmentName = e.Item.FindControl("lbAssessmentName") as LinkButton;

                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/Assessment/Edit/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("RenameAssessment", StringComparison.InvariantCultureIgnoreCase))
                {
                    // Configure display 
                    tbRenameName.Text = "Rename assessment";

                    lbRenameSave.CommandArgument = e.CommandArgument.ToString();
                    hfAssessmentId.Value = e.CommandArgument.ToString();
                    hfAssessmentName.Value = lbAssessmentName.Text;

                    // Display popup
                    mpePop.PopupControlID = "popup_renamecategory";
                    mpePop.Show();
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("DeleteAssessment", StringComparison.InvariantCultureIgnoreCase))
                {
                    ltlDeleteMsg.Text = "<p>Are you sure you want to delete this category : <b>" + lbAssessmentName.Text + "</b> ?</p>";
                    lbDelete.Visible = true;
                    lbDelCancel.Text = "Cancel";

                    lbDelete.CommandArgument = e.CommandArgument.ToString();
                    hfAssessmentId.Value = e.CommandArgument.ToString();
                    hfAssessmentName.Value = lbAssessmentName.Text;

                    // Display popup
                    mpePop.PopupControlID = "popup_deletecategory";
                    mpePop.Show();
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("DuplicateAssessment", StringComparison.InvariantCultureIgnoreCase))
                {
                    Context.ApplicationInstance.CompleteRequest();
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        protected void lvSurvey_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            this.RefreshSurveyListView();
        }
        protected void lvSurvey_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

        }
        protected void lvSurvey_ItemCreated(object sender, ListViewItemEventArgs e)
        {

        }
        protected void lvSurvey_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }


        public void lbPopCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }
        public void lbRenameSave_Click(object sender, EventArgs e)
        {
            // User input validation
            if (string.IsNullOrWhiteSpace(tbRenameName.Text))
            {
                MessageUtility.ShowToast(this.Page, "Please enter category title. ", MessageUtility.TOAST_TYPE_ERROR);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                return;
            }

            try
            {
                // This should be update
                //TODO: waiting for API to rename assessment
                //CassandraService.ServiceResponses.RSCategoryUpdateResponse response = asc.UpdateRSCategory(adminInfo.UserId, adminInfo.CompanyId, lbRenameSave.CommandArgument, tbRenameName.Text.Trim());

                //if (response.Success)
                if (false)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview, Rebind listview
                    //RefreshSurveyCategoryListView();
                    RefreshSurveyListView();

                    // Display toast
                    MessageUtility.ShowToast(this.Page, tbRenameName.Text + " has been renamed.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, tbRenameName.Text + " has not been renamed.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
        public void lbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                CassandraService.ServiceResponses.AssessmentUpdateResponse response = asc.DeleteAssessment(adminInfo.UserId, adminInfo.CompanyId, this.hfAssessmentId.Value);

                if (response.Success)
                {
                    // Close dialog
                    mpePop.Hide();

                    // Get data for listview, Rebind listview
                    RefreshSurveyListView();

                    // Display toast
                    //MessageUtility.ShowToast(this.Page, this.hfCategoryName.Value + " has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                    MessageUtility.ShowToast(this.Page, "Assessment has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    // Display error
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, "Delete assessment failed. " + response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}