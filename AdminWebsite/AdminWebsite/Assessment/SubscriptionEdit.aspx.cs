﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.Assessment
{
    public partial class SubscriptionEdit : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private List<String> iconUrlList = null;
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            managerInfo = Session["admin_info"] as ManagerInfo;

            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout", false);
                return;
            }

            if (!Page.IsPostBack)
            {

                // Set default icon for assessment logo
                //imgTopic.ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/default_topic_icon.png";

                if (!string.IsNullOrWhiteSpace(Page.RouteData.Values["CompanyId"].ToString()))
                {
                    ViewState["company_id"] = Page.RouteData.Values["CompanyId"].ToString();
                }

                if (!string.IsNullOrWhiteSpace(Page.RouteData.Values["AssessmentId"].ToString()))
                {
                    ViewState["AssessmentId"] = Page.RouteData.Values["AssessmentId"].ToString();
                }

                this.hfCompanyId.Value = ViewState["company_id"].ToString();
                this.hfAdminUserId.Value = managerInfo.UserId;

                CompanyResponse response = asc.GetCompany(ViewState["company_id"].ToString());
                if (response.Success)
                {
                    imgTopic.ImageUrl = response.Company.CompanyLogoUrl;
                    CompanyTitleLabel.Text = response.Company.CompanyTitle;
                }

                AssessmentSelectResponse assessmentSelectResponse = asc.SelectFullDetailAssessment(this.hfAdminUserId.Value, ViewState["AssessmentId"].ToString());
                if (assessmentSelectResponse.Success)
                {
                    if (assessmentSelectResponse.Assessment.Banners.Count > 0)
                    {
                        assessmentLogo.ImageUrl = assessmentSelectResponse.Assessment.Banners[0].ImageUrl;
                    }

                    TotalPersonnelTextBox.Text = assessmentSelectResponse.Assessment.Title;
                }

                SubscriptionSelectResponse subscriptionResponse = asc.SelectFullDetailAssessmentSubscription(this.hfAdminUserId.Value, ViewState["company_id"].ToString(), ViewState["AssessmentId"].ToString());
                if (subscriptionResponse.Success)
                {
                    NoOfLicensesTextBox.Text = subscriptionResponse.Subscription.NumberOfLicences.ToString();
                    VisibilityTextBox.Text = subscriptionResponse.Subscription.VisibleDaysAfterCompletion.ToString();

                    ddlNoOfRetakes.SelectedValue = subscriptionResponse.Subscription.NumberOfRetakes.ToString();
                    ddlNoOfRetakes.DataBind();
                    Status.SelectedValue = subscriptionResponse.Subscription.Status.ToString();
                    Status.DataBind();
                }

            }
        }

        #region event handlers for picking icon

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = iconUrlList[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = iconUrlList[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lbChooseIcon") as LinkButton;
                if (btn != null)
                {
                    scriptMan.RegisterAsyncPostBackControl(btn);
                }
            }
        }
        #endregion event handlers for picking icon

        protected void button_create_assessment_Click(object sender, EventArgs e)
        {
            List<string> targetedDepartmentIds = new List<string>();
            List<string> targetedUserIds = new List<string>();

            targetedDepartmentIds = hfTargetedDepartmentIds.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
            targetedUserIds = hfTargetedUserIds.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();

            DateTime parsedDateTime;
            DateTime? startDate = new DateTime();
            DateTime? endDate = new DateTime();

            if (DateTime.TryParseExact(hfScheduleStartDateTime.Value, "dd/MM/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDateTime))
            {
                startDate = parsedDateTime;
            }
            else
            {
                startDate = null;
            }

            if (DateTime.TryParseExact(hfScheduleEndDateTime.Value, "dd/MM/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out parsedDateTime))
            {
                endDate = parsedDateTime;
            }
            else
            {
                endDate = null;
            }

            // Do subscription
            SubscriptionCreateResponse response = asc.CreateAssessmentSubscription(
                this.managerInfo.UserId,
                ViewState["company_id"].ToString(),
                ViewState["AssessmentId"].ToString(),
                Convert.ToInt32(NoOfLicensesTextBox.Text),
                Convert.ToInt32(hfDuration.Value),
                Convert.ToInt32(VisibilityTextBox.Text),
                Convert.ToInt32(ddlNoOfRetakes.Text),
                Convert.ToInt32(Status.SelectedValue),
                startDate,
                endDate,
                targetedDepartmentIds,
                targetedUserIds);

            if (response.Success)
            {
                ShowToastMsgAndHideProgressBar("Subscription has been updated.", MessageUtility.TOAST_TYPE_INFO);
                Response.Redirect("~/Assessment/Subscription");
            }
            else
            {
                ShowToastMsgAndHideProgressBar("Subscription cannot be added.", MessageUtility.TOAST_TYPE_ERROR);
                Log.Error("CreateAssessmentSubscription.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        protected void rblDuration_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }
    }
}