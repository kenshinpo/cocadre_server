﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="SubscriptionCreate.aspx.cs" Inherits="AdminWebsite.Assessment.SubscriptionCreate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
	<style type="text/css">
		.ui-autocomplete-loading {
			background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
		}

		.wrapper,
		.mainContent,
		.sideBar {
			-webkit-box-sizing: border-box;
			-moz-box-sizing: border-box;
			box-sizing: border-box;
		}

		.wrapper {
			font-size: 1em;
			padding: 1.5em;
			width: 100%;
		}

		.mainContent,
		.sideBar {
			display: inline-block;
			vertical-align: top;
			width: 100%;
		}

		.duration_tip, .anonymous_tip {
			position: absolute;
			/*top: 0px;*/
			right: 0px;
			opacity: 0;
			display: block !important;
			background: #ffffff;
			width: 275px;
			border: 1px solid #ccc;
			padding: 10px;
			z-index: 1000;
			box-shadow: 0px 0px 10px #cccccc;
			transition: all 0.3s ease;
			-webkit-transition: all 0.3s ease;
			-ms-transition: all 0.3s ease;
			-moz-transition: all 0.3s ease;
		}

		.form__row.duration__tip .icon-img:hover + .duration_tip {
			opacity: 1;
		}

		.form__row.anonymous__tip .icon-img:hover + .anonymous_tip {
			opacity: 1;
		}

		.img-container {
			display: inline-block;
		}

		.remove-img-banner {
			background: rgba(0, 0, 0, 0.6);
			padding-top: 5px;
			padding-left: 10px;
			width: 30px;
			height: 30px;
			color: white;
			position: absolute;
			cursor: pointer;
			display: -webkit-flex;
			display: flex;
			-webkit-flex-wrap: wrap;
			flex-wrap: wrap;
			-webkit-align-content: center;
			align-content: center;

		}

		@media (min-width: 700px) {

			.mainContent {
				/*margin-right: 5%;*/
				width: 75%;
			}

			.sideBar {
				width: 20%;
			}
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
	<asp:HiddenField runat="server" ID="hfAdminUserId" />
	<asp:HiddenField runat="server" ID="hfCompanyId" />
	<asp:HiddenField runat="server" ID="hfAssessmentJson" />
	<asp:HiddenField runat="server" ID="hfDuration" Value="1" />
	<asp:HiddenField runat="server" ID="hfScheduleStartDateTime" />
	<asp:HiddenField runat="server" ID="hfScheduleEndDateTime" />
	<asp:HiddenField runat="server" ID="hfTargetedUserIds" />
	<asp:HiddenField runat="server" ID="hfTargetedDepartmentIds" />

	<div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

	<div id="plSelectDepartment" class="popup popup--addtopicicon" style="width: 500px; margin: 0px auto; z-index: 9001; left: 50%; top: 50%; display: none; object-fit: contain !important; transform: translate(-50%, -50%); -o-transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%); -moz-transform: translate(-50%, -50%); max-width: 600px; max-height: 80%; position: absolute; overflow: auto;">
		<h1 class="popup__title">Select Department</h1>
		<div class="popup__content">
			<fieldset class="form">
				<div class="container">
					<div class="accessrights">
						<input id="selectAllDepartment" type="checkbox" onclick="selectAllDepartments(this);" />
						<label for="selectAllDepartment">Select All</label>
					</div>
					<hr />
					<div class="accessrights departments">
						<span id="department_checkboxlist">
							<label>
								<input type="checkbox" value="D04461505c52d44d8be0ff0d80c6a2408" />Human Resource</label><br />
						</span>
					</div>
				</div>
			</fieldset>
		</div>
		<div class="popup__action">
			<a id="main_content_lbSelectDepartmentSelect" class="popup__action__item popup__action__item--cta" href="javascript:saveDepartmentSelection()">Select</a>
			<a id="main_content_lbSelectDepartmentCancel" class="popup__action__item popup__action__item--cancel" href="javascript:hideDepartmentSelector()">Cancel</a>
		</div>
	</div>

	<!-- App Bar -->
	<div class="appbar">
		<div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
		<div class="appbar__title"><a href="/Assessment/List" style="color: #000;">Assessment <span>console</span></a></div>
		<div class="appbar__meta">
			<asp:Literal ID="ltlActionName" runat="server" Text="Create an subscription" />
		</div>
		<div class="appbar__action" style="width: 500px;">
			<%--<asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn primary" >Create Assessment</asp:LinkButton>
			<asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn secondary" >Cancel</asp:LinkButton>
			<asp:LinkButton ID="lbPreview" runat="server" CssClass="btn secondary" OnClick="lbPreview_Click">Live Survey Preview</asp:LinkButton>--%>
		</div>
	</div>
	<!-- / App Bar -->


	<div class="data">
		<div class="data__content" onscroll="sticky_div();">

			<div class="container">
				<div class="card add-assessment">
					<div>
						<div class="right-column">
							<asp:UpdatePanel ID="UpdatePanel4" runat="server" class="add-topic__info__action" style="width: 40%;">
								<ContentTemplate>
									<asp:Button Text="Submit" runat="server" id="button_create_assessment" CssClass="btn" OnClick="button_create_assessment_Click" />
									<asp:LinkButton ID="lbReport" runat="server" CssClass="btn secondary" Text="Report" />
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>

						<div class="left-column">
							<asp:Image ID="imgTopic" ImageUrl="~/img/icon-topicicon-default.png" runat="server" Width="120" Height="120" style="vertical-align:top" />
							<div style="display:inline-block;margin:40px 0 10px 1em; border-bottom: 1px solid #ccc; width:500px;">
								<h3><asp:Label runat="server" ID="CompanyTitleLabel" Text="Monetary Authority of Singapore" style="color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;"></asp:Label></h3>
							</div>
						</div>
					</div>
					<hr />

					<div class="assessment-section">
						<div class="no-of-licenses">
							<div class="" style="display:inline-block;">
								<asp:Image ImageUrl="imageurl" runat="server" ID="assessmentLogo" style="vertical-align:middle" Width="50px" Height="50px" />
								<asp:Label runat="server" ID="AssessmentTitle" Text="Coloured Brain"></asp:Label>
							</div>

							<div style="display:inline-block;">
								<label>Total Personnel</label><br />
								<asp:TextBox runat="server" ID="TotalPersonnelTextBox" />
							</div>

							<div class="subscription-top-section" style="border-image: none; width: 140px; font-size: 1.1em; display: inline-block; position: relative;">
								<label>No of Licenses</label><br />
								<asp:TextBox runat="server" ID="NoOfLicensesTextBox" placeholder="Enter no. of licenses" MaxLength="5" onkeyup="updateTextCounter(this,'.subscription-top-section .lettercount',5);" />
								<span class="lettercount" style="bottom: 18px; right: 5px; position: absolute; color: rgba(209, 209, 209, 1);"><%= (5 - NoOfLicensesTextBox.Text.Length).ToString() %></span>
							</div>

							<div style="display:inline-block;">
								<label>Status</label><br />
								<div class="mdl-selectfield">
									<asp:DropDownList runat="server" ID="Status" style="width:100px;padding-bottom:12px;">
										<asp:ListItem Text="Action" Value="1" />
										<asp:ListItem Text="Hide" Value="0"  />
									</asp:DropDownList>
								</div>

							</div>
							
						</div>
					</div>
						

					<!-- PULSE DECK PRIVACY (CONTENT) START -->
					<div class="tabs__panels__item add-topic__info__details--basicinfo" id="subscription_privacy">
						<h3>Subscription</h3>

						<p>
							<label for="checkbox_privacy">
								<input type="checkbox" id="checkbox_privacy" onchange="checkbox_selected_everyone();" checked="checked" />
								<i class="fa fa-users" style="color: #999999"></i>Everyone
							</label>
						</p>
							<hr />

							<p>
								<label for="checkbox_selected_departments">
									<input type="checkbox" id="checkbox_selected_departments" onchange="checkbox_selected_departments_change();" />
									<i class="fa fa-briefcase" style="color: #999999"></i>Selected departments
								</label>
								<div class="department tags"></div>
								(<span id="selectDepartmentLinkButton" style="color: #C1C1C1; cursor: default;">+ Add more departments</span>)
							</p>

							<p>
								<label for="checkbox_selected_personnel">
									<input type="checkbox" id="checkbox_selected_personnel" onchange="checkbox_selected_personnel_change();" />
									<i class="fa fa-user" style="color: #999999"></i>Selected personnel
								</label>
								<div class="personnel tags"></div>

								<input type="text" id="text_selected_personnel" style="width: 300px; border: none; border-bottom: 1px solid #ccc;" />
							</p>
					</div>
					<!-- PULSE DECK PRIVACY (CONTENT) END -->

					<div class="tabs__panels__item add-topic__info__details--basicinfo" id="subscription_duration">
						<h3>Duration</h3>

						<asp:UpdatePanel runat="server">
							<ContentTemplate>
								<div class="duration-section">
									<label><input type="radio" value="1" name="durationType" class="durationType" checked="checked" />Perpetual</label>
									<label><input type="radio" value="2" name="durationType" class="durationType" />Schedule</label>
								</div>

								<div id="durationSchedule" style="display:none;">

									<div id="durationScheduleDate">
										<span style="">
											<span style="width: 70px; text-align: right; display: inline-block;">Start date</span>
											<input type="text" id="deckStartDate" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" class="input-field" />
											<span>at</span>
											<input id="deckStartHH" style="border-width: 0px 0px 1px; border-style: none none solid; border-color: currentColor currentColor rgb(221, 221, 221); border-image: none; width: 4em; display: inline;" onchange="validateHour(this);" type="text" maxlength="2" placeholder="hh" value="12" />
											<span>:</span>
											<input type="text" id="deckStartMM" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" class="input-field" value="00" />
											<select id="deckStartMer" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 12px;">
												<option value="am">am</option>
												<option value="pm">pm</option>
											</select>
										</span>
									</div>

									<div id="durationScheduleTime" class="publish_method schedule-settings">
										<span style="width: 70px; text-align: right; display: inline-block;">End date</span>
										<label style="display: inline;">
											<span style="display: inline;">
												<input type="text" id="deckEndDate" style="width: 100px; display: inline;" placeholder="dd/mm/yyyy" onchange="validateDate(this);" class="input-field" />
												<span>at</span>
												<input type="text" id="deckEndHH" maxlength="2" style="width: 4em; display: inline;" placeholder="hh" onchange="validateHour(this);" class="input-field" value="11" />
												<span>:</span>
												<input type="text" id="deckEndMM" maxlength="2" style="width: 4em; display: inline;" placeholder="mm" onchange="validateMinute(this);" class="input-field" value="59" />
												<select id="deckEndMer" style="width: 4em; display: inline; border: none; border-bottom: 1px solid #ddd; padding-bottom: 12px;">
													<option value="am">am</option>
													<option value="pm" selected="selected">pm</option>
												</select>
											</span>
										</label>
									</div>
								</div>

							</ContentTemplate>
						</asp:UpdatePanel>
						
					</div>

					<div class="subscription-visibility tabs__panels__item add-topic__info__details--basicinfo" id="subscription_visibility" style="border-image: none; font-size: 1.1em; display: inline-block; position: relative;">
						<h3>Visibility</h3>
						<asp:TextBox runat="server" ID="VisibilityTextBox" style="display:inline-block;width:100px;" MaxLength="4" onkeyup="updateTextCounter(this,'.subscription-visibility .lettercount',4);" /> days after User completed the Assessment
						<span class="lettercount" style="bottom: 18px; right: 280px; position: absolute; color: rgba(209, 209, 209, 1);"><%= (4 - VisibilityTextBox.Text.Length).ToString() %></span>
					</div>

					<div class="tabs__panels__item add-topic__info__details--basicinfo" id="subscription_retakes">
						<h3>No of retake allowed</h3>
						<div class="mdl-selectfield" style="width:150px;">
							<asp:DropDownList runat="server" ID="ddlNoOfRetakes" >
								<asp:ListItem Text="0" Value="0" />
								<asp:ListItem Text="1" Value="1" />
								<asp:ListItem Text="2" Value="2" />
								<asp:ListItem Text="3" Value="3" />
								<asp:ListItem Text="4" Value="4" />
							</asp:DropDownList>
						</div>
					</div>


					
				</div>
			</div>

			<div id="cardList" class="container"></div>
		</div>
	</div>

	<script type="text/javascript">
		var departmentList;
		var model = {
			deck: {
				//if (model.deck.targetedDepartmentIds.indexOf(departmentList[idx].Id) >= 0) {
				targetedUserIds: [],
				targetedDepartmentIds : [],
				privacy : new Privacy()
			}
		};

		function Privacy() {
			this.everyone = true;
			this.selectd_department = false;
			this.selected_personnel = false;
		}

		function updateTextCounter(element, counterSelector, maxCount) {
			var letterLeft = maxCount - $(element).val().length;
			$(element).siblings(".lettercount").text(letterLeft);
			if ($(element).val().length > maxCount) {
				$(element).siblings(".lettercount").text(0);
				$(element).val($(element).val().slice(0, maxCount));
			}
		}

		function removeDepartment(departmentId) {
			var foundIndex = model.deck.targetedDepartmentIds.indexOf(departmentId);

			if (foundIndex >= 0) {
				model.deck.targetedDepartmentIds.splice(foundIndex, 1);

				// Update UI
				$(".department.tags .tag." + departmentId).remove();
			}

			$("#checkbox_selected_departments").prop("checked", model.deck.targetedDepartmentIds.length > 0);
			$("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
		}

		////////////////////////////////////////////////////////////////////////////////
		// Privacy related javascript functions -- START
		function checkbox_selected_everyone() {
			// Update UI
			$("#checkbox_selected_departments").prop("checked", false);
			$("#checkbox_selected_personnel").prop("checked", false);
		}

		function checkbox_selected_departments_change() {
			if ($("#checkbox_selected_departments").is(":checked")) // Departments is included in Privacy.
			{
				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selectd_department = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
				$("#selectDepartmentLinkButton").css("color", "blue");
				$("#selectDepartmentLinkButton").css("cursor", "pointer");
				$("#selectDepartmentLinkButton").click(displayDepartmentSelector);
			}
			else // Departments is not included in Privacy.
			{
				// Update Model
				model.deck.privacy.selectd_department = false;

				// Update UI
				$("#selectDepartmentLinkButton").css("color", "#C1C1C1");
				$("#selectDepartmentLinkButton").css("cursor", "default");
				$("#selectDepartmentLinkButton").prop('onclick', null).off('click');
			}
		}

		function displayDepartmentSelector() {
			var companyId, adminUserId, ajaxPromise;

			companyId = $("#main_content_hfCompanyId").val();
			adminUserId = $("#main_content_hfAdminUserId").val();

			// Ajax call to fetch 
			ajaxPromise = jQuery.ajax({
				type: "GET",
				url: "/api/Company?userid=" + adminUserId + "&companyid=" + companyId,
				contentType: "application/json; charset=utf-8",
				data: {},
				dataType: "json",
				beforeSend: function (xhr, settings) {
					ShowProgressBar();
				},
				success: function (d, status, xhr) {
					if (d.Success) {
						departmentList = d.Departments;
					}
				},
				error: function (xhr, status, error) {
				},
				complete: function (xhr, status) { // Do any task that you want to do after the ajax call is complete
					// Render department

					var idx, departmentObj, html, checked, isAllChecked = true;

					$("#department_checkboxlist").children().remove();
					for (idx = 0; idx < departmentList.length; idx++) {
						checked = "";
						if (model.deck.targetedDepartmentIds.indexOf(departmentList[idx].Id) >= 0) {
							checked = "checked=\"checked\"";
						}
						else {
							isAllChecked = false;
						}

						html = "<label><input class=\"cbDepartment " + departmentList[idx].Id + "\" type=\"checkbox\" " + checked + " value=\"" + departmentList[idx].Id + "\" onclick=\"checkbox_selectd_department_click('" + departmentList[idx].Id + "');\" />" + departmentList[idx].Title + "</label>";

						$("#department_checkboxlist").append(html);

						if (isAllChecked) {
							$("#selectAllDepartment").attr("checked", true);
						}
						else {
							$("#selectAllDepartment").removeAttr("checked");;
						}
					}

					HideProgressBar();
					$("#mpe_backgroundElement").show();
					$("#plSelectDepartment").show();
				}
			});
		}

		function checkbox_selectd_department_click(departmentId) {
			if ($(".cbDepartment." + departmentId).is(':checked')) {
				var cbDepartments = $("#department_checkboxlist").children();
				var isAllDepartmentChecked = true;
				for (var i = 0; i < cbDepartments.length; i++) {
					console.debug($(".cbDepartment." + $($(cbDepartments[i]).children()[0]).val()).is(':checked'));
					if (!$(".cbDepartment." + $($(cbDepartments[i]).children()[0]).val()).is(':checked')) {
						isAllDepartmentChecked = false;
						break;
					}
				}
				$("#selectAllDepartment").prop("checked", isAllDepartmentChecked);;
			}
			else {
				$("#selectAllDepartment").prop("checked", false);;
			}
		}

		function checkbox_selected_personnel_change() {
			if ($("#checkbox_selected_personnel").is(":checked")) // Personnels is included in Privacy.
			{
				// Update Model
				model.deck.privacy.everyone = false;
				model.deck.privacy.selected_personnel = true;

				// Update UI
				$("#checkbox_privacy").prop("checked", false);
			}
			else  // Personnels is not included in Privacy.
			{
				// Update Model
				model.deck.privacy.selected_personnel = false;
			}
		}

		function hideDepartmentSelector() {
			$("#mpe_backgroundElement").hide();
			$("#plSelectDepartment").hide();
		}

		function saveDepartmentSelection() {
			var idx, $checkedItems = [], html;

			// Clear targetedDepartmentIds 
			model.deck.targetedDepartmentIds = [];
			$(".department.tags").children().remove();

			// Re-populate targetedDepartmentIds with selected department id
			$checkedItems = $("#plSelectDepartment .departments :checked");
			for (idx = 0; idx < $checkedItems.length; idx++) {
				model.deck.targetedDepartmentIds.push($checkedItems[idx].value);
				html = "";
				html = html + "<div class=\"tag " + $checkedItems[idx].value + "\">";
				html = html + "    <span class=\"department-tag\" style=\"padding-left:10px;\">" + $($checkedItems[idx]).parent().text() + "</span>";
				html = html + "    <a class=\"tag__icon\" href=\"javascript:removeDepartment('" + $checkedItems[idx].value + "');\">x</a>";
				html = html + "</div>";
				$(".department.tags").append(html);
			}

			hideDepartmentSelector();

			$("#checkbox_selected_departments").prop("checked", model.deck.targetedDepartmentIds.length > 0);
			$("#checkbox_selected_everyone").prop("checked", !$("#checkbox_selected_departments").prop("checked"));
		}

		////////////////////////////////////////////////////////////////////////////////

		// Validate publish date time -- START

		function validateDate(input) {
			input.className = input.className.replace("invalid-field", "");
			if (!moment(input.value, "DD/MM/YYYY", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function validateHour(input) {
			input.className = input.className.replace("invalid-field", "");

			if (!moment(input.value, "h", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function validateMinute(input) {
			input.className = input.className.replace("invalid-field", "");

			if (!moment(input.value, "m", true).isValid()) {
				input.className += " invalid-field";
			}
		}

		function validateForm()
		{
			// do validation of the user input on the form
			var parsedInt, isValidForm;

			isValidForm = true;

			// Check No of Licences
			parsedInt = parseInt($("#main_content_NoOfLicensesTextBox").val(), 10);
			if (!parsedInt) {
				// no of licenses is invalid
				isValidForm = false;
			}

			// Check startDate and endDate (if Duration is Schedule)
			if ($(".durationType:checked").val() == "1") {
				// Check startDate and endDate
				if (!moment($("#deckStartDate").val(), "DD/MM/YYYY", true).isValid()) {
					// display invalid
					isValidForm = false;
				}
				if (!moment($("#deckStartHH").val(), "h", true).isValid()) {
					// display invalid
					isValidForm = false;
				}
				if (!moment($("#deckStartMM").val(), "m", true).isValid()) {
					// display invalid
					isValidForm = false;
				}

				if (!moment($("#deckEndDate").val(), "DD/MM/YYYY", true).isValid()) {
					// display invalid
					isValidForm = false;
				}
				if (!moment($("#deckEndHH").val(), "h", true).isValid()) {
					// display invalid
					isValidForm = false;
				}
				if (!moment($("#deckEndMM").val(), "m", true).isValid()) {
					// display invalid
					isValidForm = false;
				}

				$("#deckEndMer").val();

				$("#main_content_hfScheduleStartDateTime").val(
					$("#deckStartDate").val() + " " + $("#deckStartHH").val() + ":" + $("#deckStartMM").val() + " " + $("#deckStartMer").val().toUpperCase());
				$("#main_content_hfScheduleEndDateTime").val(
					$("#deckEndDate").val() + " " + $("#deckEndHH").val() + ":" + $("#deckEndMM").val() + " " + $("#deckEndMer").val().toUpperCase());
			} else {
				$("#main_content_hfScheduleStartDateTime").val("");
				$("#main_content_hfScheduleEndDateTime").val("");
			}

			$("#main_content_hfDuration").val($(".durationType:checked").val());

			// Check Visibility
			parsedInt = parseInt($("#main_content_VisibilityTextBox").val(), 10);
			if (!parsedInt) {
				// no of licenses is invalid
				isValidForm = false;
				toastr.error("Invalid no of licenses");
			}

			// Check No of Licences
			parsedInt = parseInt($("#main_content_VisibilityTextBox").val(), 10);
			if (!parsedInt) { // no of days visible
				isValidForm = false;
				toastr.error("Invalid no of days of visibility");
			}

			var idx, str, catStr;

			catStr = "";
			for (idx = 0; idx < model.deck.targetedUserIds.length; idx++) {
				catStr = catStr + str;
				if ((idx+1) < model.deck.targetedUserIds.length) {
					catStr = catStr + ",";
				}
			}
			$("#main_content_hfTargetedUserIds").val(catStr);

			for (idx = 0; idx < model.deck.targetedDepartmentIds.length; idx++) {
				catStr = catStr + str;
				if ((idx + 1) < model.deck.targetedDepartmentIds.length) {
					catStr = catStr + ",";
				}
			}
			$("#main_content_hfTargetedDepartmentIds").val(catStr);
			
			return isValidForm;
		}

		// Validate publish date time -- END
		(function ($) {

			$(document).on('submit', '#form1', function () { // code
				return validateForm();
			});

			$(document).ready(function () {
				$(".durationType").change(function (a, c) {
					if (a.target.value == "2") {
						$("#durationSchedule").show();
					} else {
						$("#durationSchedule").hide();
					}
				});
			});

			$("#text_selected_personnel").autocomplete({
				source: "/api/personnel?companyid=" + $("#main_content_hfCompanyId").val() + "&userid=" + $("#main_content_hfAdminUserId").val(),
				minLength: 1,
				response: function (event, ui) {
					for (var i = 0; i < ui.content.length; i++) {
						if (model.deck.targetedUserIds.indexOf(ui.content[i].value) != -1) {
							ui.content.splice(i, 1);
							if (i != ui.content.length - 1) {
								i--;
							}
						}
					}
				},
				select: function (event, ui) {
					var html;
					if (model.deck.targetedUserIds.indexOf(ui.item.value) == -1) {
						model.deck.targetedUserIds.push(ui.item.value);

						html = "";
						html = html + "<div class=\"tag " + ui.item.value + "\">";
						html = html + "    <span class=\"personnel-tag\" style=\"padding-left:10px;\">" + ui.item.label + "</span>";
						html = html + "    <a class=\"tag__icon\" href=\"javascript:removePersonnel('" + ui.item.value + "');\">x</a>";
						html = html + "</div>";
						$(".personnel.tags").append(html);

						$("#checkbox_selected_personnel").prop("checked", model.deck.targetedUserIds.length > 0);
						var everyoneChecked = !($("#checkbox_selected_departments").prop("checked") || $("#checkbox_selected_personnel").prop("checked"));
						$("#checkbox_privacy").prop("checked", everyoneChecked);
					}
					return false;
				},
				focus: function (event, ui) {
					event.preventDefault();
				},
				close: function (event, ui) {
					$("#text_selected_personnel").val("");
				}
			});

		}(jQuery));
	</script>
</asp:Content>
