﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="CoachList.aspx.cs" Inherits="AdminWebsite.Assessment.CoachList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfSurveyId" runat="server" />
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfSurveyName" runat="server" />
            <asp:HiddenField ID="hfCoachId" runat="server" />


            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <asp:LinkButton ID="lbAddCoach" ClientIDMode="AutoID" runat="server" CommandName="AddCoach" Text="Add coach" CssClass="mfb-component__button--main" data-mfb-label="Add coach" OnClick="lbAddCoach_Click">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </asp:LinkButton>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Delete Survey -->
            <!-- Add Coach -->
            <asp:Panel ID="popup_addcategory" runat="server" CssClass="popup popup--addcategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Add a Coach</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Coach</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbAddName" runat="server" placeholder="Enter Coach's name" MaxLength="20" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAddDone" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbAddDone_Click" OnClientClick="ShowProgressBar();" Text="Done" />
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Add Coach -->

            <!-- Rename Coach -->
            <asp:Panel ID="popup_renamecategory" runat="server" CssClass="popup popup--renamecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Rename Coach</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Assessment Coach</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbRenameName" runat="server" placeholder="Enter Coach's name" MaxLength="20" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbRenameSave" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbRenameSave_Click" OnClientClick="ShowProgressBar();" Text="Save" />
                    <asp:LinkButton ID="lbRenameCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Rename Coach  -->

            <!-- Delete Coach  -->
            <asp:Panel ID="popup_deletecategory" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Delete Coach</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <asp:Literal ID="ltlDeleteMsg" runat="server" />
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbDelete_Click" Text="Delete" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbDelCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Delete Coach  -->

            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Assessment
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgActionSurvey" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionSurveyTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClick="lbAction_Click" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbActionCancel_Click" />
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Delete Survey -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/" style="color: #000;">Assessment <span>console</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlCount" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/List">Assessments</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active" href="/Assessment/Coach">Coach</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/Assessment/Subscription">Subscription</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Coach</label>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Type the coach's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" runat="server" OnClick="ibFilterKeyWord_Click" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvCoach" runat="server" OnItemCommand="lvCoach_ItemCommand" OnSorting="lvCoach_Sorting" OnItemDataBound="lvCoach_ItemDataBound" OnItemCreated="lvCoach_ItemCreated" OnItemEditing="lvCoach_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th style="width: 40%;">
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByCoach" ID="SortByCoach" OnClientClick="ShowProgressBar('Filtering');" Text="Coach" />
                                        </th>
                                        <th style="width: 40%;">
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByCount" ID="SortByCount" OnClientClick="ShowProgressBar('Filtering');" Text="No of Assessment" />
                                        </th>
                                        <th class="no-sort" style="width: auto;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField runat="server" ID="SurveyIdHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "UserId") %>' />
                                    <asp:Literal ID="FirstNameLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") %>'></asp:Literal>
                                </td>
                                <td>
                                    <asp:Literal ID="NumberOfAssessmentsLiteral" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfAssessments") %>'></asp:Literal>
                                </td>

                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbRename" ClientIDMode="AutoID" runat="server" CommandName="RenameCoach" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId") %>' Text="Rename coach" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="DeleteCoach" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId") %>' Text="Delete coach" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>
