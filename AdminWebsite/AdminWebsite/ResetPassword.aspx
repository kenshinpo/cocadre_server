﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="AdminWebsite.ResetPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <title>Sign In - Cocadre</title>
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/scss/app.css" />
    <link rel="stylesheet" href="/css/toastr.min.css" />

    <link type="text/css" rel="stylesheet" href="/css/jquery-entropizer.min.css" />

    <style type="text/css">
        html,
        body {
            background: white;
        }

        .companies a {
            height: 130px !important;
        }

        .index-company-name {
            width: 100% !important;
            height: auto !important;
            padding: 3px !important;
            display: block;
            color: #000;
            font-size: 16px;
            text-align: center;
            line-height: 18px;
            position: relative !important;
            margin-bottom: 30px;
        }

        #strength_meter .entropizer-track {
            background-color: rgba(204, 204, 204, 1);
            border-radius: 2px;
            height: 8px;
        }

        #strength_meter .entropizer-bar {
            height: 8px;
        }

        input {
            width: 50% !important;
            border-top: 0px !important;
            border-right: 0px !important;
            border-left: 0px !important;
            border-bottom: 2px solid rgba(204, 204, 204, 1) !important;
            font-size: 1.15em !important;
        }

        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(204, 204, 204, 1) !important;
            font-size: 1.15em !important;
        }

        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(204, 204, 204, 1) !important;
            opacity: 1;
            font-size: 1.15em !important;
        }

        ::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(204, 204, 204, 1) !important;
            opacity: 1;
            font-size: 1.15em !important;
        }

        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(204, 204, 204, 1) !important;
            font-size: 1.15em !important;
        }
    </style>
    <script src="/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/apo-min.js"></script>
    <script type="text/javascript" src="/Scripts/jquery-ui-1.11.4.js"></script>
    <script src="/js/toastr.min.js"></script>

    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/entropizer.js"></script>
    <script type="text/javascript" src="/js/jquery-entropizer.min.js"></script>

    <script type="text/javascript">
        "use strict";

        (function ($) {
            $(document).ready(function () {
                passwordStrength();
            });
        })(jQuery);

        function passwordStrength() {
            $('#strength_meter').entropizer({
                target: '#tbNewPassword',
                buckets: [
                    { max: 20, message: 'Too short', color: 'rgba(204, 204, 204, 1)' },
                    { min: 20, max: 40, message: 'Weak', color: 'rgba(251, 208, 59, 1)' },
                    { min: 40, max: 60, message: 'Fail', color: 'rgba(251, 208, 59, 1)' },
                    { min: 60, max: 80, message: 'Good', color: 'rgba(0, 113, 188, 1)' },
                    { min: 80, message: 'Strong', color: 'rgba(0, 146, 69, 1)' }
                ],
                update: function (data, ui) {
                    ui.bar.css({
                        'background-color': data.color,
                        'width': data.percent + '%'
                    });

                    if (data.percent > 0) {
                        $("#strength_text").html(data.message);
                    }
                    else {
                        $("#strength_text").html("");
                    }
                }
            });
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="login forgot">
            <div class="login__header">
                <a href="http://www.cocadre.com">
                    <div class="login__header__logo">Cocadre</div>
                </a>
                <div class="login__header__title" style="color: rgba(134, 134, 134, 1); width: 45%; text-align: left; margin: 0 auto;">
                    <label style="margin-top: 120px;">Choose a strong password and don't reuse it for other accounts.</label>

                    <label style="margin-top: 20px;">Changing your password will sign you out of all your devices, including your phone. You will need to enter your new password on all your devices.</label>
                </div>
            </div>
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="login__card card" style="border: 1px solid rgba(0, 0, 0, 0.2); border-image: none; width: 50%; margin-top: 50px; background-color: white;">
                        <div>
                            <asp:TextBox TextMode="Password" ID="tbNewPassword" runat="server" placeholder="New Password" />
                        </div>

                        <div style="text-align: left; font-size: 1.2em; margin-top: 30px;">
                            <label style="display: inline-block; color: rgba(27, 132, 255, 1);">Password strength</label>
                            <label style="display: inline-block;" id="strength_text"></label>
                        </div>
                        <div id="strength_meter" class="col-md-10 col-md-offset-2" style="width: 50%;"></div>

                        <div style="margin-top: 30px;">
                            <asp:TextBox TextMode="Password" ID="tbConfirmPassword" runat="server" placeholder="Confirm New Password" />
                        </div>

                        <label style="text-align: left; color: rgba(120, 120, 120, 1); font-size: 1.2em;">
                            *Password are case-sensitive
                        </label>
                        <label style="text-align: left;">
                            <asp:LinkButton ID="lbChangePassword" runat="server" Text="CHANGE PASSWORD" Style="width: 200px; margin-top: 50px;" CssClass="btn" OnClick="lbChangePassword_Click" OnClientClick="ShowProgressBar();" />
                        </label>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            passwordStrength();
        })
    </script>
</body>
</html>
