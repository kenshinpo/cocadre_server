﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.MLearning
{
    public partial class AssignmentDetail : System.Web.UI.Page
    {
        private ManagerInfo managerInfo;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                hfManagerId.Value = managerInfo.UserId;
                hfCompanyId.Value = managerInfo.CompanyId;
                hfTimezone.Value = managerInfo.TimeZone.ToString();
                hfAssignmentId.Value = "";

                if (Page.RouteData.Values["AssignmentId"] != null)
                {
                    hfAssignmentId.Value = Page.RouteData.Values["AssignmentId"].ToString();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}