﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EvaluationAnalyticUser.aspx.cs" Inherits="AdminWebsite.MLearning.EvaluationAnalyticUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/Css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        #analytic-wrapper .data__content {
            background: #F5F5F5;
        }

        #education-analytic {
            margin-top: 30px;
        }

        .filter-section {
            margin: 0 auto;
        }

            .filter-section .search-input {
                font-size: 18px;
                padding-left: 50px !important;
            }

            .filter-section .fa {
                font-size: 25px;
            }

        .invisible {
            visibility: hidden;
        }

        .mdl-selectfield {
            width: 220px !important;
        }

        .dynatable-per-page {
            display: none;
        }

        .dynatable-pagination-links {
            float: none;
            text-align: center;
            padding: 20px;
        }

        .dynatable-active-page {
            background: #cccccc;
        }
    
        .topic-img {
            width: 50px;
            height: 50px;
            display: inline-block;
            vertical-align:middle;
            margin-right: 15px;
        }
        .card-title {
            margin-bottom: 0px;
        }
        .disclaimer {
            font-size: 16px;
        }
        .inline-card-col {
            display:inline-block;
            vertical-align: top;
            padding:20px;
        }
        .divider-line {
            border: 0px;
            height: 3px;
            background: #fff;
            box-shadow: 1px 1px 5px rgba(0,0,0,0.1);
        }
        .card-type {
            font-size:16px;
            color:#ccc;
            font-weight:500;
        }
        .card-label {
            color:#6e6e6e;
            font-size:16px;
            font-weight:500;
        }
        .type-indicator {
            position: absolute;
            left:0px;
            top:0px;
            width: 8px;
            height: 100%;
            background:#ccc;
        }
        .correct-indicator {
            background:#95D000;
        }
        .wrong-indicator {
            background:#FF2851;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTopicId" />
    <asp:HiddenField runat="server" ID="hfCategoryId" />
    <asp:HiddenField runat="server" ID="hfUserId" />

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>M</strong> <span>Learning</span></div>
        <div class="appbar__meta"><a href="/MLearning/EvaluationList" id="root-link">Evaluation Analytics</a></div>
        <div class="appbar__meta">
            <!--<a id="second-link"></a>-->
            <span style="color: black;" id="second-link"></span>
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->
    
    <div id="analytic-wrapper" class="data">

        <div class="data__content">

            <div id="education-analytic" class="container" style="width: 100%;">

                <div style="width: 100%; margin: 0 auto;">
                    <!-- TITLE -->
                    <div class="title-section" style="width: 100%; margin: 0 auto;">
                        <div class="analytic-info">
                            <h2 id="analytic-title" class="ellipsis-title"></h2>
                        </div>
                        <div class="header-left" style="margin-bottom:10px;">
                            <ul class="header-list">
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Name:</span>
                                    <span class="name-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Department:</span>
                                    <span class="department-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Department:</span>
                                    <span class="department-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Status:</span>
                                    <span class="status-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                            </ul>
                        </div>
                        <div class="header-right">
                             <ul class="header-list">
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Category:</span>
                                    <span class="category-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Passing Grade:</span>
                                    <span class="grade-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                            </ul>
                            <p class="align-left" style="margin-top:50px;"><span id="data-date-string" style="font-size: 16px;color: #727272;"></span></p>
                        </div>
                    </div>

                    <div id="overview-tab" class="tabs tabs--styled" style="clear:both; ">
                        <ul class="tabs__list">
                            <li class="tabs__list__item">
                                <a class="tabs__link" id="no_attempt" href="#attempt">Attempts</a>
                            </li>
                        </ul>
                        <div class="tabs__panels">
                            <!-- OVERVIEW TAB -->
                            <div class="tabs__panels__item overview" id="attempt">

                                <div id="left-card" class="card half-col animated fadeInUp" style="height:250px; padding:25px 35px;">
                                    <h2 class="score-title card-title">Score</h2>
                                    <div class="score-section" style="text-align: right;height: auto;display: inline-block;width: 60%;vertical-align: middle;">
                                        <div style="position:relative;width: 170px;height: 170px;width: 170px;display: inline-block;text-align: right;vertical-align: middle;">
                                            <canvas id="doughnut-chart" width="170" height="170"></canvas>
                                            <h1 class="chart-percentage" style="position:absolute; color: #95D000; font-weight:500; width:100%; text-align:center; top:42%;">87%</h1>
                                        </div>
                                    </div>
                                    <div class="chart-info" style="display: inline-block;width: 35%;float: none;vertical-align: middle;text-align: left; padding-left:35px;">
                                        <label style="color: #95D000; font-size:20px; margin:0px; font-weight:500;">Passed</label>
                                        <span class="passed_count" style="font-size:22px;">44</span>
                                        <span style="font-size:22px;">/</span>
                                        <span class="total_count" style="font-size:22px;">50</span>
                                    </div>
                                </div>

                                <div id="right-card" class="card half-col animated fadeInUp" style="height:250px; padding:25px 35px;">
                                    <h2 class="rank-title card-title">Rank</h2>
                                    <div class="rank-section" style="text-align:center; height: auto;">
                                        <span style="font-size:10em; font-weight:500; line-height:1.1em;" class="no_candidates">3</span> 
                                        <span> / </span>
                                        <span class="total_candidates">1232 </span>
                                        <span>Candidates</span>
                                    </div>
                                    <label class="disclaimer">Ranking is sort by attempt, then by score follow by time</label>
                                </div>

                                <div class="filter-section text-center" style="clear:both; margin-top: 50px; padding-left: 20px; padding-top: 20px;">
                                    <div class="option-filter">
                                        <div class="mdl-selectfield" style="width: 250px; float: left; margin-right: 20px; height: 40px;">
                                            <select name="status" id="status">
                                                <option selected="selected" value="0">All Questions</option>
                                                <option value="1">Correct answers</option>
                                                <option value="-1">Wrong answers</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="search-filter">
                                        <div class="input-group" style="position: relative; width:65%;">
                                            <i class="fa fa-search"></i>
                                            <input name="search" id="hidden_name" class="search-input" type="text" placeholder="Search Questions" value="" />
                                        </div>
                                    </div>
                                </div>

                                <div id="questions-section" class="animated fadeInUp" style="display:block;">
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
        <script src="/Js/Chart.js"></script>
        <script>
            $(function () {
                var CompanyId = $('#main_content_hfCompanyId').val();
                var ManagerId = $('#main_content_hfAdminUserId').val();
                var TopicId = $('#main_content_hfTopicId').val();
                var CategoryId = $('#main_content_hfCategoryId').val();   
                var AnsweredUserId = $('#main_content_hfUserId').val();
                var cardsArr = [];
                
                function ordinal_suffix_of(i) {
                    var j = i % 10,
                        k = i % 100;
                    if (j == 1 && k != 11) {
                        return i + "st";
                    }
                    if (j == 2 && k != 12) {
                        return i + "nd";
                    }
                    if (j == 3 && k != 13) {
                        return i + "rd";
                    }
                    return i + "th";
                }

                function getCardType(type) {
                    var typeStr = "";
                    if (type == 1 || type == 2) {
                        typeStr = "Question";
                    } else if (type == 3) {
                        typeStr = "Instruction";
                    } else if (type == 4) {
                        typeStr = "Article";
                    } else if (type == 5) {
                        typeStr = "Video";
                    } else if (type == 6) {
                        typeStr = "PDF";
                    }
                    return typeStr;
                }

                function generateChart(data) {
                    var ctx = document.getElementById("doughnut-chart").getContext("2d");
                    var chart_data = [
                        { value: data.Stats[0].Count, color: "#95D000" },
                        { value: data.MaxCount - data.Stats[0].Count, color: "#6e6e6e" }
                    ];

                    var doughnutChart = new Chart(ctx).Doughnut(chart_data, {
                        percentageInnerCutout: 75,
                        animationEasing: "linear",
                        animateScale: false,
                        animationSteps: 50,
                        segmentShowStroke: false
                    });

                    $('.score-title').html(data.Title);
                    $('.passed_count').html(data.Stats[0].Count);
                    $('.total_count').html(data.MaxCount);
                    $('.chart-percentage').html(data.Stats[0].Percentage+"%");
                }

                function generateQuestions(cards) {
                    var questionsHTML = "";
                    $('#questions-section').empty();

                    if (cards.length > 0) {
                        $.each(cards, function (k, v) {
                            questionsHTML += '<div class="card animated fadeInUp no-padding" style="margin:1%; position:relative;">';
                            if (v.Type == 1 || v.Type == 2) {
                                if (v.isCorrectAttempt) {
                                    questionsHTML += '<div class="type-indicator correct-indicator"></div>';
                                } else {
                                    questionsHTML += '<div class="type-indicator wrong-indicator"></div>';
                                }
                            } else {
                                questionsHTML += '<div class="type-indicator"></div>';
                            }
                            questionsHTML += '<div class="inline-card-col card-label" style="width:15%; min-height: 135px;">';
                            
                            questionsHTML += '<div style="color:#ddd; word-break: break-word;">';
                            questionsHTML += '<div class="number" style="font-size:28px;">P' + v.Paging + " | Q" + v.Ordering + '</div>';
                            questionsHTML += '<div class="code">Code ' + v.CardId + '</div>';
                            questionsHTML += '</div>';
                            questionsHTML += '</div>';

                            if (v.Type == 1 || v.Type == 2) {
                                questionsHTML += '<div class="inline-card-col card-question" style="width:38%; min-height: 135px; border-right: 1px solid #ccc;">';
                                if (v.HasUploadedContent ) {
                                    questionsHTML += '<div style="width:70px; height: 70px; display:inline-block;vertical-align:top; margin-right: 15px;"><img src="' + v.UploadedContent[0].Url + '" style="width:100%;" /></div>'
                                }
                                questionsHTML += '<div style="display:inline-block;vertical-align:top;">';
                                questionsHTML += '<label class="card-type">' + getCardType(v.Type) + '</label>';
                                questionsHTML += '<p class="card-label">' + v.Content + '</p>';
                                questionsHTML += '</div>';
                                questionsHTML += '</div>';

                                questionsHTML += '<div class="inline-card-col card-answer" style="width:38%; min-height: 135px; border-right: 1px solid #ccc;">';
                                questionsHTML += '<div style="width:70px; height: 70px; display:inline-block;vertical-align:top; margin-right: 15px;">';
                                questionsHTML += '</div>';
                                questionsHTML += '<div style="display:inline-block;vertical-align:top; width: calc(100% - 90px)">';
                                questionsHTML += '<label class="card-type">Answer</label>';
                                questionsHTML += '<p class="card-label">';

                                var score = 0;
                                var answerStr = "";
                                $.each(v.Options, function (key, val) {
                                    if (val.IsSelected) {
                                        if (answerStr) {
                                            answerStr += ", ";
                                        }
                                        score += val.Score;
                                        answerStr += val.Score > 0 ? '<span style="color:#95D000;">' + val.Content + '</span>' : val.Content;
                                    }
                                });
                                questionsHTML += answerStr;
                                questionsHTML += '</p>';
                                questionsHTML += '</div>';
                                questionsHTML += '</div>';
                                questionsHTML += '<div class="inline-card-col card-score" style="width:7%; min-height: 135px; ">';
                                questionsHTML += '<label class="card-type">Score</label>';
                                questionsHTML += '<p class="card-label">' + score + '</p>';
                                questionsHTML += '</div>';

                            } else {

                                questionsHTML += '<div class="inline-card-col card-question" style="width:85%; min-height: 135px; border-right: 1px solid #ccc;">';
                                if (v.HasUploadedContent && v.Type < 5) {
                                    questionsHTML += '<div style="width:70px; height: 70px; display:inline-block;vertical-align:top; margin-right: 15px;"><img src="' + v.UploadedContent[0].Url + '" /></div>'
                                }

                                questionsHTML += '<div style="display:inline-block;vertical-align:top;">';
                                questionsHTML += '<label class="card-type">' + getCardType(v.Type) + '</label>';

                                if (v.Type == 4 && v.PagesContent.length > 0) {
                                    questionsHTML += '<p class="card-label">';
                                    var titleStr = "";
                                    $.each(v.PagesContent, function (ka, va) {
                                        if (titleStr) {
                                            titleStr += ", ";
                                        }
                                        titleStr += va.Title;
                                    });
                                    questionsHTML += titleStr;
                                    questionsHTML += '</p>';
                                } else {
                                    questionsHTML += '<p class="card-label">' + v.Content + '</p>';
                                }
                                
                                questionsHTML += '</div>';
                                questionsHTML += '</div>';
                            }

                            questionsHTML += '</div>';

                            if (v.HasPageBreak && k < cards.length -1) {
                                questionsHTML += '<hr class="divider-line"/>';
                            }
                        });
                    } else {
                        $('#questions-section').append('<p style="text-align:center; font-size:18px; padding:20px;">No result!</p>');
                    }

                    $('#questions-section').append(questionsHTML);
                }

                function fetchData() {
                    $('#education-analytic').addClass('hidden');
                    $('#education-analytic').removeClass('animated fadeInUp');

                    ShowProgressBar();
                    $.ajax({
                        type: "POST",
                        url: '/MLEvaluation/AnalytictUserAttempt',
                        data:{
                            "CompanyId": CompanyId,
                            "ManagerId": ManagerId,
                            "CategoryId": CategoryId,
                            "TopicId": TopicId,
                            "AnsweredUserId": AnsweredUserId
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            HideProgressBar();
                            if (res.Success) {
                                $('#education-analytic').removeClass('hidden');
                                $('#education-analytic').addClass('animated fadeInUp');

                                var topic = res.Topic;
                                var answeredUser = res.AnsweredByUser;
                                var scoreData = res.ScoreChart;
                                var rankData = res.RankChart;
                                var toggleType = res.CardToggleType;
                                var attempt = res.Attempt;

                                var titleHTML = topic.Title;
                                $('#second-link').html(topic.Title);

                                if (topic.IconUrl) {
                                    titleHTML = '<img src="' + topic.IconUrl + '" class="topic-img" />' + titleHTML;
                                }

                                $('#analytic-title').html(titleHTML);

                                $('.name-label').html(answeredUser.FirstName + " " + answeredUser.LastName);
                                $('.department-label').html(answeredUser.Departments[0].Title);
                                $('.status-label').html(attempt.ResultMessage);
                                $('.date-label').html(attempt.CompletedDateString);
                                $('.category-label').html(topic.MLTopicCategory.Title);
                                $('.grade-label').html(topic.PassingGrade);
                                $('#data-date-string').html();
                                $('#no_attempt').html(ordinal_suffix_of(attempt.Attempt) + " attempt");
                                generateChart(scoreData);

                                $('.rank-title').html(rankData.Title);
                                $('.no_candidates').html(rankData.Stats[0].Count);
                                $('.total_candidates').html(rankData.MaxCount);
                                $('.disclaimer').html(rankData.Disclaimer);

                                cardsArr = topic.Cards;
                                
                                $.each(cardsArr, function (k, v) {
                                    var totalPoint = 0;
                                    var userScore = 0;

                                    if (v.Type == 1 || v.Type == 2) {
                                        $.each(v.Options, function (key, val) {
                                            totalPoint += val.Score;
                                            if (val.IsSelected) {
                                                userScore += val.Score
                                            }
                                        }); 

                                        if (userScore >= totalPoint) {
                                            v.isCorrectAttempt = true;
                                        } else {
                                            v.isCorrectAttempt = false;
                                        }
                                    }
                                });
                                
                                generateQuestions(cardsArr);
                            }
                        }
                    })
                   
                }

                fetchData();

                var timer;
                var delay = 300;

                $('input[name=search]').bind('input', function () {
                    window.clearTimeout(timer);
                    var this_element = $(this);
                    timer = window.setTimeout(function () {

                        var searchText = this_element.val().toLowerCase();

                        if (searchText) {
                            var filteredArr = _.filter(cardsArr, function (elem) {
                                return elem.Content.toLowerCase().indexOf(searchText) > 0;
                            });
                            generateQuestions(filteredArr);
                        } else {
                            generateQuestions(cardsArr);
                        }

                    }, delay);

                });

                $('#status').on('change', function (e) {
                    var selected_val = $(this).val();
                    if (selected_val == 0) {
                        generateQuestions(cardsArr);
                    } else {
                        var filteredArr = [];
                        if (selected_val == 1) {
                            filteredArr = _.filter(cardsArr, function (elem) {
                                return elem.isCorrectAttempt == true;
                            });
                        } else {
                            filteredArr = _.filter(cardsArr, function (elem) {
                                return elem.isCorrectAttempt == false;
                            });
                        }
                        generateQuestions(filteredArr);
                    }
                });

            })

        </script>
</asp:Content>
