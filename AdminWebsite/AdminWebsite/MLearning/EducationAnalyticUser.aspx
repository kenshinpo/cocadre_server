﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EducationAnalyticUser.aspx.cs" Inherits="AdminWebsite.MLearning.EducationAnalyticUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/Css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        #analytic-wrapper .data__content {
            background: #F5F5F5;
        }

        #education-analytic {
            margin-top: 30px;
        }
        .topic-img {
            width: 50px;
            height: 50px;
            display: inline-block;
            vertical-align:middle;
            margin-right: 15px;
        }
        .divider-line {
            border: 0px;
            height: 3px;
            background: #fff;
            box-shadow: 1px 1px 5px rgba(0,0,0,0.1);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfEducationId" />
    <asp:HiddenField runat="server" ID="hfCategoryId" />
    <asp:HiddenField runat="server" ID="hfUserId" />

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <div class="appbar__title"><strong>M</strong> <span>Learning</span></div>
        <div class="appbar__meta"><a href="/MLearning/EducationList" id="root-link">Education Analytics</a></div>
        <div class="appbar__meta">
            <!--<a id="second-link"></a>-->
            <span style="color: black;" id="second-link"></span>
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="analytic-wrapper" class="data">

        <div class="data__content">

            <div id="education-analytic" class="container" style="width: 100%;">

                <div style="width: 100%; margin: 0 auto;">
                    <!-- TITLE -->
                    <div class="title-section" style="width: 100%; margin: 0 auto;">
                        <div class="analytic-info">
                            <h2 id="analytic-title" class="ellipsis-title"><img src="https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/2_Orange_45.png" class="topic-img">Apo Test 2</h2>
                        </div>
                        <div class="header-left" style="margin-bottom:10px;">
                            <ul class="header-list">
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Name:</span>
                                    <span class="name-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Department:</span>
                                    <span class="department-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Status:</span>
                                    <span class="status-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Last Update:</span>
                                    <span class="date-label" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                            </ul>
                        </div>
                        <div class="header-right">
                            <ul class="header-list">
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Category:</span>
                                    <span class="category-type" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                                <li>
                                    <span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Answer:</span>
                                    <span class="answer-type" style="display:inline-block; font-size: 16px;color: #000;"></span>
                                </li>
                            </ul>
                            <p class="align-left"><span id="data-date-string" style="font-size: 16px;color: #727272;"></span></p>
                        </div>
                    </div>

                    <div id="overview-tab" class="tabs tabs--styled" style="clear:both; ">
                        <ul class="tabs__list">
                            <li class="tabs__list__item">
                                <a class="tabs__link" href="#details">Details</a>
                            </li>
                        </ul>
                        <div class="tabs__panels">
                            <!-- OVERVIEW TAB -->
                            <div class="tabs__panels__item overview" id="details">

                                <div id="user-report-section" class="animated fadeInUp" style="display:block;"></div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/vendor/moment-with-locales.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sugar/1.4.1/sugar.min.js"></script>
    <script>
        $(function () {

            var CompanyId = $('#main_content_hfCompanyId').val();
            var ManagerId = $('#main_content_hfAdminUserId').val();
            var CategoryId = $('#main_content_hfCategoryId').val();
            var EducationId = $('#main_content_hfEducationId').val();
            var AnsweredUserId = $('#main_content_hfUserId').val();
            
            var answer_html = "";

            function getProgressStatus(statusCode) {
                var status = "";

                if (statusCode == 1) {
                    status = "<span style='color:#ccc'>Absent</span>";
                } else if (statusCode == 2) {
                    status = "<span style='color:#ccc'>Incomplete</span>";
                } else if (statusCode == 3) {
                    status = "Completed";
                }

                return status;
            }

            function getCardType(typeCode) {
                var type = "";

                if (typeCode == 1) {
                    type = "Question";
                } else if (typeCode == 2) {
                    type = "Question";
                } else if (typeCode == 3) {
                    type = "Instruction";
                } else if (typeCode == 4) {
                    type = "Article";
                } else if (typeCode == 5) {
                    type = "Video";
                } else if (typeCode == 6) {
                    type = "PDF";
                }

                return type;
            }

            function fetchData() {
                $('#education-analytic').addClass('hidden');
                $('#education-analytic').removeClass('animated fadeInUp');

                ShowProgressBar();
                $.ajax({
                    type: "POST",
                    url: '/Api/MLEducation/AnalyticUser',
                    data: {
                        "CompanyId": CompanyId,
                        "ManagerId": ManagerId,
                        "CategoryId": CategoryId,
                        "EducationId": EducationId,
                        "AnsweredUserId": AnsweredUserId
                    },
                    crossDomain: true,
                    dataType: 'json',
                    success: function (res) {
                        HideProgressBar();
                        if (res.Success) {

                            $('#education-analytic').removeClass('hidden');
                            $('#education-analytic').addClass('animated fadeInUp');

                            var education = res.Education;
                            var answeredUser = res.AnsweredUser;
                            var chapters = res.Education.Chapters;

                            var titleHTML = education.Title;

                            $('.category-type').html(education.Category.Title);
                            $('.answer-type').html(education.DisplayAnswerType == 1 ? 'Hidden' : 'Show');
                            $('#data-date-string').html("Showing data from " + education.AnalyseDurationString);
                            $('.name-label').html(answeredUser.FirstName +" "+ answeredUser.LastName);
                            $('.department-label').html(answeredUser.Departments[0].Title);
                            $('.status-label').html(getProgressStatus(answeredUser.EducationAnalytic.ProgressStatus));
                            $('.date-label').html(answeredUser.EducationAnalytic.LastProgressDateTimeString ? answeredUser.EducationAnalytic.LastProgressDateTimeString : 'NA');

                            if (education.IconUrl) {
                                titleHTML = '<img src="' + education.IconUrl + '" class="topic-img" />' + titleHTML;
                            }

                            $('#analytic-title').html(titleHTML);

                            if (chapters.length > 0) {
                                $.each(chapters, function (vKey, vValue) {

                                    $.each(vValue.Cards, function (key, value) {

                                        answer_html += '<div class="card animated fadeInUp no-padding">';

                                        if (value.Type == 1 || value.Type == 2) {
                                            answer_html += '<div class="card-header" style="width:50%; float:left; border-right:1px solid #ccc; padding:20px;">';
                                            answer_html += '<div class="grid-question-number">';
                                        } else {
                                            answer_html += '<div class="card-header" style="width:100%; float:left; border-right:1px solid #ccc; padding:20px; border-right: 0px;">';
                                            answer_html += '<div class="grid-question-number" style="width:11%">';
                                        }
                                        
                                        answer_html += '<div style="color:#ddd;">';
                                        answer_html += '<div class="number">P' + value.Paging + ' | Q' + value.Ordering + '</div>';
                                        answer_html += '<div class="code">Code: ' + value.CardId + '</div>';
                                        answer_html += '</div>';
                                        answer_html += '</div>';
                                        if (value.HasUploadedContent && value.Type <= 3) {
                                            answer_html += '<div class="question-image" style="width: 70px; height: auto; float: left;margin-right: 5px;margin-top: 10px;"><img src="' + value.UploadedContent[0].Url + '" /></div>';
                                        }

                                        if (value.Type >= 3) {
                                            answer_html += '<div class="grid-question" style="width:60%;">';
                                        } else {
                                            answer_html += '<div class="grid-question">';
                                        }
                                        

                                        answer_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">' + getCardType(value.Type) + '</label>';

                                        if (value.Type == 4) {
                                            answer_html += '<p class="question">' + value.PagesContent[0].Title + '</p>';
                                            answer_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Description</label>';
                                            answer_html += '<p class="description">' + value.PagesContent[0].Content.truncate(450) + '</p>';
                                        } else {
                                            answer_html += '<p class="question">' + value.Content + '</p>';
                                            if (value.Description != null) {
                                                answer_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Description</label>';
                                                answer_html += '<p class="description">' + value.Description + '</p>';
                                            }
                                        }

                                        answer_html += '<a href="/MLearning/EducationEdit/'+EducationId+'/'+CategoryId+'" target="_blank"><p>See more</p></a>';
                                        
                                        answer_html += '</div>';
                                        answer_html += '</div>';
                                        answer_html += '<div class="card-answer" style="width:50%; float:left;padding:20px;">';

                                        if (value.Type == 1 || value.Type == 2) {
                                            answer_html += '<div class="grid-answer">';
                                        
                                            if (value.Options.length > 0) {
                                                answer_html += '<p class="answer">';
                                                $.each(value.Options, function (key, val) {
                                                    
                                                    if (val.Score > 0) {

                                                        if (val.HasUploadedContent) {
                                                            answer_html += '<div class="question-image" style="width: 70px; height: auto; float: left;margin-right: 25px;"><img src="' + val.UploadedContent[0].Url + '" /></div>';
                                                        } 
                                                        answer_html += '<label style="color:#ddd; font-size:16px; line-height:16px;">Answer</label>';
                                                        answer_html += val.Content;
                                                    }

                                                });
                                                answer_html += '</p>';
                                            }
                                            else {
                                                answer_html += '<p class="answer"><i>The responder doesn\'t answer this question. </i></p>';
                                            }
                                        
                                            answer_html += '</div>';
                                        } else {
                                            //answer_html += '<div style="background:#ccc; width:100%; height:100%;></div>';
                                        }
                                        answer_html += '</div>';
                                        answer_html += '</div>';
                                    });

                                    if (vKey < chapters.length - 1) {
                                        answer_html += '<hr class="divider-line"/>';
                                    }
                                        
                                });
                            }

                            $('#user-report-section').append(answer_html);

                            
                        }
                        
                    }
                });
            }


            fetchData();
        });
    </script>
</asp:Content>
