﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EvaluationList.aspx.cs" Inherits="AdminWebsite.MLearning.EvaluationList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <script src="/Js/grayscale.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfMLearningId" runat="server" />
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfMLearningTitle" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/MLearning/EvaluationCreate" class="mfb-component__button--main" data-mfb-label="Add evaluation">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Hide, Acitve, Delete -->
            <asp:Panel ID="popup_actiontopic" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Evaluation
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgAction" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClientClick="ShowProgressBar();" OnClick="lbAction_Click" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" />
                </div>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_actiontopic"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Acitve, Delete -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">M Learning <span>Evaluation</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlCount" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Evaluation</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EvaluationCategory">Evaluation Category</a>
                </li>
            </ul>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationList">Education</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationCategory">Education Category</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/AssignmentList">Assignment</a>
                </li>--%>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Evaluation</label>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Type the evaluation's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" OnClick="ibFilterKeyWord_Click" runat="server" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" />
                    </div>
                    <div class="pad">
                        <label>By Category</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterCategory" runat="server" AutoPostBack="true" onchange="ShowProgressBar('Filtering');" OnSelectedIndexChanged="ddlFilterCategory_SelectedIndexChanged" />
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvMLearing" runat="server" OnItemCreated="lvMLearing_ItemCreated" OnSorting="lvMLearing_Sorting" OnItemDataBound="lvMLearing_ItemDataBound" OnItemCommand="lvMLearing_ItemCommand" OnItemEditing="lvMLearing_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th class="no-sort" style="width: 60px; padding: 15px 20px;">
                                            <!--Icon-->
                                        </th>
                                        <th style="width: 25%; padding: 15px 5px;">
                                            <!--Title-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByTitle" ID="SortByTitle" OnClientClick="ShowProgressBar('Filtering');" Text="Evaluations" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 20%; padding: 15px 5px;">
                                            <!--Category-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByCategory" ID="SortByCategory" OnClientClick="ShowProgressBar('Filtering');" Text="Category" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Progress-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByProgress" ID="SortByProgress" OnClientClick="ShowProgressBar('Filtering');" Text="Progress" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Status-->
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByStatus" ID="SortByStatus" OnClientClick="ShowProgressBar('Filtering');" Text="Status" Style="min-width: 80px;" />
                                        </th>
                                        <th style="width: 10%; padding: 15px 5px;">
                                            <!--Participants-->
                                            <asp:Label runat="server" Style="min-width: 75px;" Text="Participants" />
                                        </th>
                                        <th class="no-sort" style="width: 40px; padding: 15px 5px;">
                                            <!--Analytic Icon-->
                                        </th>
                                        <th class="no-sort" style="width: auto; padding: 15px 15px;">
                                            <!--Action Menu-->
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="padding: 15px 20px; text-align: center;">
                                    <!--Icon-->
                                    <asp:HiddenField runat="server" ID="SurveyIdHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "TopicId") %>' />
                                    <div class="constrained" style="display: inline-block;">
                                        <asp:LinkButton ID="lbIcon" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>' Style="color: #999;">
                                            <asp:Image ID="imgMLearning" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "IconUrl") %>' runat="server" />
                                        </asp:LinkButton>
                                    </div>
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Title-->
                                    <asp:LinkButton ID="lbMLearningTitle" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Category-->
                                    <asp:LinkButton ID="lbCategory" Text='<%# DataBinder.Eval(Container.DataItem, "MLTopicCategory.Title") %>' ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>' Style="color: #999;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Progress-->
                                    <asp:LinkButton ID="lbProgress" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Status-->
                                    <asp:LinkButton ID="lbStatus" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px;">
                                    <!--Participants-->
                                    <asp:LinkButton ID="lbParticipants" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>' Style="color: #999; min-width: 80px;" />
                                </td>
                                <td style="padding: 15px 5px; text-align: center;">
                                    <!--Analytic Icon-->
                                    <asp:LinkButton ID="lbReport" runat="server" CommandName="Analytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>'>
                                        <asp:Image ID="imgReport" runat="server" ImageUrl="~/Img/icon_report.png" Width="35px" />
                                    </asp:LinkButton>
                                </td>
                                <td style="padding: 15px 15px;">
                                    <!--Action Menu-->
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Edit evaluation" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "/" + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId") %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbActivate" ClientIDMode="AutoID" runat="server" CommandName="Activate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "," + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId")%>' Text="Activate evaluation" Visible='<%# ((DataBinder.Eval(Container.DataItem, "Status").ToString() == "1") || (DataBinder.Eval(Container.DataItem, "Status").ToString() == "3")) %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbHide" ClientIDMode="AutoID" runat="server" CommandName="Hide" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "," + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId")%>' Text="Hide evaluation" Visible='<%# DataBinder.Eval(Container.DataItem, "Status").ToString() == "2" %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Del" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TopicId") + "," + DataBinder.Eval(Container.DataItem, "MLTopicCategory.CategoryId")%>' Text="Delete evaluation" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>

