﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AssignmentList.aspx.cs" Inherits="AdminWebsite.MLearning.AssignmentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <script type="text/javascript">
        $(function () {
            initChips();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfAssignmentId" runat="server" />
            <asp:HiddenField ID="hfEducationTitle" runat="server" />
            <asp:HiddenField ID="hfEducationUrl" runat="server" />

            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <a href="/MLearning/AssignmentDetail" class="mfb-component__button--main" data-mfb-label="Add assignment">
                        <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                        <i class="mfb-component__main-icon--active fa fa-lightbulb-o"></i>
                    </a>
                </li>
            </ul>
            <!-- /Floating Action Button -->


            <!-- Active: Hide, Acitve, Delete -->
            <asp:Panel ID="popup_action" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Assignment
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p style="margin-top: 30px;">
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <asp:Image ID="imgAction" ImageUrl="~/img/image-01.jpg" runat="server" CssClass="Media-figure" Visible="false" />
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action" style="margin-top: 50px;">
                    <asp:LinkButton ID="lbAction" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClientClick="ShowProgressBar();" OnClick="lbAction_Click" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" />
                </div>
            </asp:Panel>

            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_action"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
            <!-- /Active: Hide, Acitve, Delete -->
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">M Learning <span>Assignment</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlCount" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: bolder;">Manage</div>
            <ul class="data__sidebar__list" style="margin-top: 20px;">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EvaluationList">Evaluation</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EvaluationCategory">Evaluation Category</a>
                </li>
            </ul>
            <ul class="data__sidebar__list" style="margin-top: 20px;">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationList">Education</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationCategory">Education Category</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Assignment</a>
                </li>
            </ul>

            <div class="data__sidebar__title">Filters</div>
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                    <div class="pad">
                        <label>Search Education</label>
                        <asp:TextBox ID="tbFilterKeyWord" runat="server" placeholder="Type the education's name" MaxLength="120" Style="width: 80%; display: inline-block;" />
                        <asp:ImageButton ID="ibFilterKeyWord" runat="server" ImageUrl="~/Img/search_button.png" Style="width: 30px; vertical-align: middle; display: inline-block;" OnClientClick="ShowProgressBar('Filtering');" OnClick="ibFilterKeyWord_Click" />
                    </div>
                    <div class="pad">
                        <label>By Status</label>
                        <div class="mdl-selectfield">
                            <asp:DropDownList ID="ddlFilterProgress" runat="server" AutoPostBack="true" onchange="ShowProgressBar('Filtering');" OnSelectedIndexChanged="ddlFilterProgress_SelectedIndexChanged">
                                <asp:ListItem Text="All" Value="0" />
                                <asp:ListItem Text="Upcoming" Value="1" />
                                <asp:ListItem Text="Live" Value="2" />
                                <asp:ListItem Text="Completed" Value="3" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </aside>
        <div class="data__content">
            <asp:UpdatePanel ID="upList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvList" runat="server" OnItemDataBound="lvList_ItemDataBound" OnItemCreated="lvList_ItemCreated" OnSorting="lvList_Sorting" OnItemCommand="lvList_ItemCommand" OnItemEditing="lvList_ItemEditing">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th style="width: 100px;">
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByProgress" ID="SortByProgress" OnClientClick="ShowProgressBar('Filtering');">Status</asp:LinkButton>
                                        </th>
                                        <th style="width: auto;">
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByEducation" ID="SortByEducation" OnClientClick="ShowProgressBar('Filtering');">Education</asp:LinkButton>
                                        </th>
                                        <th style="width: 90px;">
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByStartDate" ID="SortByStartDate" OnClientClick="ShowProgressBar('Filtering');">Start</asp:LinkButton>
                                        </th>
                                        <th style="width: 90px;">
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByEndDate" ID="SortByEndDate" OnClientClick="ShowProgressBar('Filtering');">End</asp:LinkButton>
                                        </th>
                                        <th style="width: 100px;">
                                            <%--<asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByParticipants" ID="SortByParticipants" OnClientClick="ShowProgressBar('Filtering');">Participants</asp:LinkButton>--%>
                                            <asp:Literal runat="server">Participants</asp:Literal>
                                        </th>
                                        <th class="no-sort" style="width: 40px;"></th>
                                        <th class="no-sort" style="width: 40px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField runat="server" ID="hfId" Value='<%# DataBinder.Eval(Container.DataItem, "AssignmentId") %>' />
                                    <asp:LinkButton ID="lbProgress" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId") %>' Text="Live" Style="color: rgba(92, 92, 92, 1);" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbEducation" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId") %>'>
                                        <asp:Image ID="imgEducationIcon" runat="server" ImageUrl='<%# DataBinder.Eval(Container.DataItem, "Education.IconUrl") %>' Style="display: inline-block; width: 40px; height: 40px; vertical-align: middle;" />
                                        <asp:Label ID="lblEducationTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Education.Title") %>' Style="color: rgba(92, 92, 92, 1); display: inline-block; vertical-align: middle; margin-left: 10px;" />
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbStartDate" Text="31/01/2017 11:05PM" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId") %>' Style="color: rgba(92, 92, 92, 1);" />
                                </td>

                                <td>
                                    <asp:LinkButton ID="lbEndtDate" Text="28/02/2017 11:05PM" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId") %>' Style="color: rgba(92, 92, 92, 1);" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbParticipants" ClientIDMode="AutoID" runat="server" CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId")  %>' Style="color: rgba(92, 92, 92, 1);" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbReport" runat="server" CommandName="Analytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId") %>' Visible="false">
                                        <asp:Image ID="imgReport" runat="server" ImageUrl="~/Img/icon_report.png" Width="30px" />
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbEdit" ClientIDMode="AutoID" runat="server" CommandName="Edit" Text="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId")  %>' />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbAnalytics" ClientIDMode="AutoID" runat="server" CommandName="Analytics" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId")%>' Text="View Result" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Del" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AssignmentId") %>' Text="Delete" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
