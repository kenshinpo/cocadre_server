﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CassandraService.Entity.MLEducationCategory;

namespace AdminWebsite.MLearning
{
    public partial class EvaluationCreate : System.Web.UI.Page
    {
        private const int MLEARNING_TITLE_TEXT_MAX = 50;
        private const int MLEARNING_INTRODUCTION_TEXT_MAX = 100;
        private const int MLEARNING_INSTRUCTIONS_TEXT_MAX = 100;
        private const int MLEARNING_CLOSINGWORD_TEXT_MAX = 100;

        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private List<String> iconUrlList = null;
        private List<User> searchUserList = null;
        private List<MLTopicCategory> categoryList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.managerInfo = Session["admin_info"] as ManagerInfo;
            ViewState["manager_user_id"] = managerInfo.UserId;
            ViewState["company_id"] = managerInfo.CompanyId;

            try
            {
                if (!IsPostBack)
                {
                    ViewState["selected_department"] = new List<Department>();
                    ViewState["selected_user"] = new List<User>();

                    tbMLearningTitle.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblMLearningTitleCount.ClientID, MLEARNING_TITLE_TEXT_MAX);
                    tbMLearningIntroduction.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblMLearningIntroductionCount.ClientID, MLEARNING_INTRODUCTION_TEXT_MAX);
                    tbMLearningInstructions.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblMLearningInstructionsCount.ClientID, MLEARNING_INSTRUCTIONS_TEXT_MAX);
                    tbMLearningClosingWords.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblMLearningClosingWordsCount.ClientID, MLEARNING_CLOSINGWORD_TEXT_MAX);

                    lblMLearningTitleCount.Text = Convert.ToString(MLEARNING_TITLE_TEXT_MAX);
                    lblMLearningIntroductionCount.Text = Convert.ToString(MLEARNING_INTRODUCTION_TEXT_MAX);
                    lblMLearningInstructionsCount.Text = Convert.ToString(MLEARNING_INSTRUCTIONS_TEXT_MAX);
                    lblMLearningClosingWordsCount.Text = Convert.ToString(MLEARNING_CLOSINGWORD_TEXT_MAX);

                    lblAuthor.Text = managerInfo.FirstName + " " + managerInfo.LastName;
                    tbStartDate.Text = DateTime.UtcNow.AddHours(managerInfo.TimeZone).AddDays(1).ToString("dd/MM/yyyy");
                    tbEndDate.Text = DateTime.UtcNow.AddHours(managerInfo.TimeZone).AddMonths(1).ToString("dd/MM/yyyy");

                    #region Step 1. cbCategory data bind.
                    GetCategoryData();
                    if (categoryList != null)
                    {
                        for (int i = 0; i < categoryList.Count; i++)
                        {
                            cbCategory.Items.Add(new ListItem(categoryList[i].Title, categoryList[i].CategoryId));
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpePop.Hide();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lbChooseIcon") as LinkButton;
                if (btn != null)
                {
                    scriptMan.RegisterAsyncPostBackControl(btn);
                }
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = iconUrlList[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = iconUrlList[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void cbAllDepartment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbAllDepartment.Checked)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        cblDepartment.Items[i].Selected = true;
                    }
                }
                else
                {
                    cblDepartment.ClearSelection();
                }
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void cblDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int selectedCount = 0;
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (cblDepartment.Items[i].Selected)
                    {
                        selectedCount++;
                    }
                }
                if (selectedCount != cblDepartment.Items.Count)
                {
                    cbAllDepartment.Checked = false;
                }
                else
                {
                    cbAllDepartment.Checked = true;
                }
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbSelectDepartmentSelect_Click(object sender, EventArgs e)
        {
            try
            {
                List<Department> selectedDepartmentList = new List<Department>();
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (cblDepartment.Items[i].Selected)
                    {
                        Department department = new Department();
                        department.Id = cblDepartment.Items[i].Value;
                        department.Title = cblDepartment.Items[i].Text;
                        selectedDepartmentList.Add(department);
                    }
                }
                ViewState["selected_department"] = selectedDepartmentList;
                rtParticipantsDepartment.DataSource = selectedDepartmentList;
                rtParticipantsDepartment.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                #region Icon data
                GetIconData();
                rtIcon.DataSource = iconUrlList;
                rtIcon.DataBind();
                #endregion
                mpePop.PopupControlID = "popup_addtopicicon";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbReports_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(string.Format("/Survey/Analytic/{0}", ViewState["survey_id"].ToString() + "/" + ViewState["category_id"].ToString()));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtParticipantsDepartment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                for (int i = 0; i < selectedDepartmentList.Count; i++)
                {
                    if (selectedDepartmentList[i].Id == Convert.ToString(e.CommandArgument))
                    {
                        selectedDepartmentList.RemoveAt(i);
                        break;
                    }
                }

                ViewState["selected_department"] = selectedDepartmentList;
                rtParticipantsDepartment.DataSource = selectedDepartmentList;
                rtParticipantsDepartment.DataBind();
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbAddParticipantsDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                DepartmentListResponse response = asc.GetAllDepartment(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
                if (response.Success)
                {
                    List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                    cblDepartment.Items.Clear();
                    for (int i = 0; i < response.Departments.Count; i++)
                    {
                        cblDepartment.Items.Add(new ListItem(response.Departments[i].Title, response.Departments[i].Id));
                        for (int j = 0; j < selectedDepartmentList.Count; j++)
                        {
                            if (response.Departments[i].Id.Equals(selectedDepartmentList[j].Id))
                            {
                                cblDepartment.Items[i].Selected = true;
                            }
                        }
                    }

                    mpePop.PopupControlID = "plSelectDepartment";
                    mpePop.Show();
                }
                else
                {
                    Log.Error("DepartmentListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void rtParticipantsPersonnel_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                List<User> selectedList = ViewState["selected_user"] as List<User>;
                if (selectedList == null || selectedList.Count == 0)
                {
                    Log.Error(@"ViewState[""SelectedUser""] is null.", this.Page);
                }
                else
                {
                    for (int i = 0; i < selectedList.Count; i++)
                    {
                        if (selectedList[i].UserId == Convert.ToString(e.CommandArgument))
                        {
                            selectedList.RemoveAt(i);
                            break;
                        }
                    }

                    ViewState["selected_user"] = selectedList;

                    rtParticipantsPersonnel.DataSource = selectedList;
                    rtParticipantsPersonnel.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void tbSearchKey_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetSearchUser();
                rtSearchResult.DataSource = searchUserList;
                rtSearchResult.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddUser"))
                {
                    List<User> selectedList = ViewState["selected_user"] as List<User>;
                    String[] value = Convert.ToString(e.CommandArgument).Split(',');
                    User user = new CassandraService.Entity.User();
                    user.UserId = value[0];
                    user.FirstName = value[1];
                    user.LastName = value[2];
                    selectedList.Add(user);
                    rtParticipantsPersonnel.DataSource = selectedList;
                    rtParticipantsPersonnel.DataBind();
                    ViewState["SelectedUserId"] = selectedList;
                    tbSearchKey.Text = String.Empty;
                    GetSearchUser();
                    rtSearchResult.DataSource = searchUserList;
                    rtSearchResult.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetFocus", "SetFocus();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        public void GetCategoryData()
        {
            MLCategorySelectAllResponse response = asc.SelectAllMLCategories(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), (int)MLCategoryQueryType.Basic);
            if (response.Success)
            {
                categoryList = response.Categories;
            }
            else
            {
                Log.Error("MLCategorySelectAllResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }


        public void GetIconData()
        {
            TopicSelectIconResponse response = asc.SelectAllTopicIcons(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
            if (response.Success)
            {
                iconUrlList = response.TopicIconUrls;
            }
            else
            {
                Log.Error("TopicSelectIconResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        public void GetSearchUser()
        {
            try
            {
                if (String.IsNullOrEmpty(tbSearchKey.Text.Trim()))
                {
                    searchUserList = new List<User>();
                }
                else
                {
                    UserListResponse response = asc.SearchUser(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), tbSearchKey.Text.Trim());
                    if (response.Success)
                    {
                        List<User> selectedList = ViewState["selected_user"] as List<User>;
                        searchUserList = new List<User>();

                        for (int i = 0; i < selectedList.Count; i++)
                        {
                            User user = (from a in response.Users where a.UserId == selectedList[i].UserId select a).FirstOrDefault();
                            if (user != null)
                            {
                                response.Users.Remove(user);
                            }
                        }
                        searchUserList = response.Users;
                    }
                    else
                    {
                        Log.Error("SearchUserForPostingSuspend.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        protected void cbParticipantsEveryone_CheckedChanged(object sender, EventArgs e)
        {
            if (cbParticipantsEveryone.Checked)
            {
                cbParticipantsDepartment.Checked = false;
                cbParticipantsPersonnel.Checked = false;
            }
        }

        protected void cbParticipantsPersonnel_CheckedChanged(object sender, EventArgs e)
        {
            if (cbParticipantsPersonnel.Checked)
            {
                cbParticipantsEveryone.Checked = false;
            }
        }

        protected void cbParticipantsDepartment_CheckedChanged(object sender, EventArgs e)
        {
            if (cbParticipantsDepartment.Checked)
            {
                cbParticipantsEveryone.Checked = false;
            }
        }

        protected void lbCreate_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. 驗證資料
                #region Title
                if (String.IsNullOrEmpty(tbMLearningTitle.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the title.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (tbMLearningTitle.Text.Length > MLEARNING_TITLE_TEXT_MAX)
                {
                    ShowToastMsgAndHideProgressBar("Title length can not be bigger than " + MLEARNING_TITLE_TEXT_MAX, MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Introduction
                if (String.IsNullOrEmpty(tbMLearningIntroduction.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the introduction.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (tbMLearningIntroduction.Text.Length > MLEARNING_INTRODUCTION_TEXT_MAX)
                {
                    ShowToastMsgAndHideProgressBar("Introduction length can not be bigger than " + MLEARNING_INTRODUCTION_TEXT_MAX, MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Instructions
                if (String.IsNullOrEmpty(tbMLearningInstructions.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the instructions.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (tbMLearningInstructions.Text.Length > MLEARNING_INSTRUCTIONS_TEXT_MAX)
                {
                    ShowToastMsgAndHideProgressBar("Instructions length can not be bigger than " + MLEARNING_INSTRUCTIONS_TEXT_MAX, MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Closing Words
                if (String.IsNullOrEmpty(tbMLearningClosingWords.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the closing words.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (tbMLearningClosingWords.Text.Length > MLEARNING_CLOSINGWORD_TEXT_MAX)
                {
                    ShowToastMsgAndHideProgressBar("Closing words length can not be bigger than " + MLEARNING_CLOSINGWORD_TEXT_MAX, MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Category
                GetCategoryData();
                String categoryID = String.Empty;
                if (cbCategory.SelectedItem == null)
                {
                    ShowToastMsgAndHideProgressBar("Please at least choose a category.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                String categoryTitle = cbCategory.SelectedItem.Text;
                for (int i = 0; i < categoryList.Count; i++)
                {
                    if (categoryList[i].Title.ToLower().Equals(cbCategory.SelectedItem.Text.Trim().ToLower()))
                    {
                        categoryID = categoryList[i].CategoryId;
                        break;
                    }
                }
                #endregion

                #region Passing Grade
                string passingGradeString = string.Empty;
                if (String.IsNullOrEmpty(tbPassingGradeInt.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the closing words.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                int passingGrade;
                try
                {
                    passingGrade = Int16.Parse(tbPassingGradeInt.Text.Trim());
                }
                catch (Exception ex)
                {
                    ShowToastMsgAndHideProgressBar("Passing grade should not be negative", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (cbPassingGradePer.Checked)  // only allow 0 ~ 100
                {
                    if (passingGrade < 0 || passingGrade > 100)
                    {
                        ShowToastMsgAndHideProgressBar("Please keep passing grade between 0% to 100%", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        passingGradeString = passingGrade + "%";
                    }
                }
                else
                {
                    if (passingGrade < 0 || passingGrade > 1000)
                    {
                        ShowToastMsgAndHideProgressBar("Please keep passing grade between 0 to 1000", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }

                    passingGradeString = Convert.ToString(passingGrade);
                }
                #endregion

                #region  Department of Participants
                List<String> targetedDepartmentIds = new List<String>();
                if (cbParticipantsDepartment.Checked)
                {
                    List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                    if (selectedDepartmentList.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please at least choose a department.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedDepartmentList.Count; i++)
                        {
                            targetedDepartmentIds.Add(selectedDepartmentList[i].Id);
                        }
                    }
                }
                #endregion

                #region Personnel of Participants
                List<String> targetedUserIds = new List<String>();
                if (cbParticipantsPersonnel.Checked)
                {
                    List<User> selectedUserList = (List<User>)ViewState["selected_user"];
                    if (selectedUserList.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please at least choose a user.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedUserList.Count; i++)
                        {
                            targetedUserIds.Add(selectedUserList[i].UserId);
                        }
                    }
                }
                #endregion

                #region Start Date
                DateTime dtStart = new DateTime();
                try
                {
                    DateTime dtTemp = DateTime.ParseExact(tbStartDate.Text + " " + tbStartHour.Text.PadLeft(2, '0') + ":" + tbStartMinute.Text.PadLeft(2, '0') + " " + ddlStartMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture);
                    dtStart = new DateTime(dtTemp.Year, dtTemp.Month, dtTemp.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second, DateTimeKind.Utc).AddHours(-managerInfo.TimeZone);
                }
                catch (Exception ex)
                {
                    ShowToastMsgAndHideProgressBar("Start date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region End Date
                DateTime? dtEnd = null;
                if (rblDuration.SelectedIndex != 0)
                {
                    try
                    {
                        DateTime dtTemp = DateTime.ParseExact(tbEndDate.Text + " " + tbEndHour.Text.PadLeft(2, '0') + ":" + tbEndMinute.Text.PadLeft(2, '0') + " " + ddlEndMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture);
                        dtEnd = new DateTime(dtTemp.Year, dtTemp.Month, dtTemp.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second, DateTimeKind.Utc).AddHours(-managerInfo.TimeZone);
                        if (dtEnd < DateTime.UtcNow)
                        {
                            ShowToastMsgAndHideProgressBar("End date should not be early now", MessageUtility.TOAST_TYPE_ERROR);
                            return;
                        }

                        if (dtStart > dtEnd)
                        {
                            ShowToastMsgAndHideProgressBar("Your start and end dates does not tally", MessageUtility.TOAST_TYPE_ERROR);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowToastMsgAndHideProgressBar("End date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                }
                #endregion

                #endregion

                #region Step 2. Call API
                MLTopicCreateResponse response = asc.CreateMLTopic(ViewState["manager_user_id"].ToString(),
                    ViewState["company_id"].ToString(),
                    tbMLearningTitle.Text.Trim(),
                    tbMLearningIntroduction.Text.Trim(),
                    tbMLearningInstructions.Text.Trim(),
                    tbMLearningClosingWords.Text.Trim(),
                    imgTopic.ImageUrl,
                    passingGradeString,
                    categoryID,
                    categoryTitle,
                    Convert.ToInt16(rblDuration.SelectedValue),
                    targetedDepartmentIds,
                    targetedUserIds,
                    dtStart,
                    dtEnd,
                    cbAuthor.Checked,
                    Convert.ToBoolean(rblQuestionRandom.SelectedValue),
                    Convert.ToBoolean(rblAllowRetestForFailures.SelectedValue),
                    Convert.ToBoolean(rblAllowRetestForPassers.SelectedValue),
                    Convert.ToInt16(rblAllowReview.SelectedValue),
                    Convert.ToInt16(rblDisplayAnswer.SelectedValue)
                        );

                if (response.Success)
                {
                    ShowToastMsgAndHideProgressBar("M-Learning have been created.", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/MLearning/EvaluationEdit/" + response.Topic.TopicId + "/" + response.Topic.MLTopicCategory.CategoryId + "',300);", true);
                }
                else
                {
                    ShowToastMsgAndHideProgressBar("M-Learning have not been created.", MessageUtility.TOAST_TYPE_ERROR);
                    Log.Error("RSTopicCreateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void rblDuration_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rblDuration.SelectedIndex == 0)
                {
                    plEndDate.Visible = false;
                }
                else
                {
                    plEndDate.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }
    }
}