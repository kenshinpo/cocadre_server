﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EvaluationCreate.aspx.cs" Inherits="AdminWebsite.MLearning.EvaluationCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <script src="/js/MLearning/EvaluationCreate.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <style>
        ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
            color: rgba(127, 127, 127, 1) !important;
        }

        ::-moz-placeholder { /* Firefox 19+ */
            color: rgba(127, 127, 127, 1) !important;
        }

        :-ms-input-placeholder { /* IE 10+ */
            color: rgba(127, 127, 127, 1) !important;
        }

        :-moz-placeholder { /* Firefox 18- */
            color: rgba(127, 127, 127, 1) !important;
        }

        #main_content_UpdatePanel6 {
            margin-bottom: 20px;
            margin-top: 20px;
        }

            #main_content_plEndDate input, #main_content_plEndDate label, #main_content_UpdatePanel6 input, #main_content_UpdatePanel6 label {
                display: inline-block;
                width: auto;
            }

                #main_content_plEndDate label:first-child, #main_content_UpdatePanel6 label:first-child {
                    display: block;
                }

        .mdl-selectfield {
            width: 90px;
            display: inline-block;
        }

            .mdl-selectfield select {
                padding: 0.65em;
                margin-bottom: 0px;
            }

        #main_content_UpdatePanel6 input.time, #main_content_plEndDate input.time {
            width: 80px;
            text-align: center;
        }

        .duration_tip, .anonymous_tip {
            position: absolute;
            right: 0px;
            opacity: 0;
            display: none;
            background: #ffffff;
            width: 275px;
            border: 1px solid #ccc;
            padding: 10px;
            z-index: 1000;
            box-shadow: 0px 0px 10px #cccccc;
            transition: all 0.3s ease;
            -webkit-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            -moz-transition: all 0.3s ease;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            TabFunction();
            rebindJs();

            selectedIcon = new Icon();
            selectedIcon.Id = "";
            selectedIcon.Url = "https://s3-ap-southeast-1.amazonaws.com/cocadre/icons/Banking_1.png";
            selectedIcon.ColorCode = "FF0000";
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            TabFunction();
            rebindJs();
        })

        /* For search personnel */
        var delayTimer;
        function RefreshUpdatePanel(event) {
            if (event.keyCode == 9 || event.keyCode == 13 || event.keyCode == 45 || event.keyCode == 46 || event.keyCode == 91 || event.keyCode == 93 || event.keyCode == 144 || (event.keyCode > 15 && event.keyCode < 21) || (event.keyCode > 32 && event.keyCode < 41) || (event.keyCode > 111 && event.keyCode < 124)) {
                return false;
            }
            clearTimeout(delayTimer);
            delayTimer = setTimeout(function () {

                __doPostBack('<%= tbSearchKey.ClientID %>', '');
            }, 1000);

            var progress = $('#imgLoading');
            progress.fadeToggle();
        }

        $(document).on("keydown", function (e) {
            if (e.which === 8 && !$(e.target).is("input, textarea")) {
                e.preventDefault();
            }
        });

        function showLoading() {
            var progress = $('#imgLoading');

            progress.fadeToggle();
        }

        function SetFocus() {
            var textBox = document.getElementById('<%= tbSearchKey.ClientID %>');
            var elemLen = textBox.value.length;
            if (document.selection) {
                // Set focus
                // Use IE Ranges
                var oSel = document.selection.createRange();
                // Reset position to 0 & then set at end
                oSel.moveStart('character', -elemLen);
                oSel.moveStart('character', elemLen);
                oSel.moveEnd('character', 0);
                oSel.select();
            }
            else if (textBox.selectionStart || textBox.selectionStart == '0') {
                // Firefox/Chrome
                textBox.selectionStart = elemLen;
                textBox.selectionEnd = elemLen;
            }
            textBox.focus();
        }
        /* For search personnel */


    </script>
    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

    <div id="dvEditIcon" class="popup" style="min-width: 50%; min-height: 50%; margin: 0px auto; z-index: 9001; bottom: 50%; right: 50%; display: none; object-fit: contain !important; transform: translate(50%, 51%); -o-transform: translate(50%, 51%); -webkit-transform: translate(50%, 51%); -moz-transform: translate(50%, 51%); max-width: 80%; max-height: 60%; position: absolute; overflow: auto;">


        <h1 class="popup__title">Select icon</h1>
        <div class="popup__content" style="min-height: 150px;">
            <fieldset class="form">
                <div class="container">
                    <div class="accessrights">
                        <div>
                            <label style="display: inline-block; width: auto;">Choose your color</label>
                            <input type='text' id="colorPicker" style="display: inline-block;" />
                        </div>


                        <div class="badgeStyleList" style="margin: 20px; text-align: center;">
                            Value:
                            <input id="valueInput" value="ff6699" />
                            <br />
                            Style:
                            <input id="styleInput" />
                        </div>

                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a class="popup__action__item popup__action__item--cta" style="color: rgba(254, 30, 38, 1);" href="javascript:confirmBadge();">SELECT</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hideChoiceBadgePopup();">CANCEL</a>
        </div>
    </div>




    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title"><a href="/MLearning/EvaluationList" style="color: #000;">M <span>Learning</span></a></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlActionName" runat="server" Text="Create an evaluation" />
        </div>
    </div>
    <!-- / App Bar -->

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>

            <!-- Reveal: Add Icon -->
            <asp:Panel ID="popup_addtopicicon" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Evaluation Icon</h1>
                <div class="topicicons">
                    <p>Select from list</p>
                    <asp:Repeater ID="rtIcon" runat="server" OnItemDataBound="rtIcon_ItemDataBound" OnItemCommand="rtIcon_ItemCommand" OnItemCreated="rtIcon_ItemCreated">
                        <HeaderTemplate>
                            <div class="topicicons__list">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <label class="topicicons__icon">
                                <asp:LinkButton ID="lbChooseIcon" runat="server" CommandName="Choose">
                                    <asp:Image ID="imgChooseIcon" runat="server" />
                                </asp:LinkButton>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbIconCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Add Icon -->

            <!-- Reveal: Custom Icon -->
            <asp:Panel ID="popup_customicon" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">Choose Icon</h1>
                <div class="topicicons">
                    <p>Custom Icon</p>

                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbCustomIconCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Custom Icon -->


            <!-- Reveal: Alert Popup -->
            <asp:Panel ID="popup_alertpopup" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Edit Survey</h1>
                <div class="topicicons" style="text-align: center; border-top-style: none;">
                    <label style="background: rgba(0, 117, 254, 1); margin: -15px auto 0px; padding: 5px; border-radius: 24px; width: 200px; color: white;">Survey is Active and Live/Hidden</label><br />
                    <label style="color: red;">
                        Editing will affect the Analytics,
                        <br />
                        therefore some features will be locked.</label>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAlertPopupCancel" CssClass="popup__action__item popup__action__item" runat="server" Text="OK" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Reveal: Alert Popup -->

            <!-- Select Department -->
            <asp:Panel ID="plSelectDepartment" runat="server" CssClass="popup popup--addtopicicon" Width="100%" Style="display: none;">
                <h1 class="popup__title">Select Department</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="accessrights">
                                <asp:CheckBox ID="cbAllDepartment" runat="server" Text="Select All" AutoPostBack="true" OnCheckedChanged="cbAllDepartment_CheckedChanged" />
                            </div>
                            <hr />
                            <div class="accessrights">
                                <asp:CheckBoxList ID="cblDepartment" runat="server" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="cblDepartment_SelectedIndexChanged" />
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbSelectDepartmentSelect" CssClass="popup__action__item popup__action__item--cta" runat="server" Text="Select" OnClick="lbSelectDepartmentSelect_Click" />
                    <asp:LinkButton ID="lbSelectDepartmentCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- / Select Department -->

            <ajaxToolkit:ModalPopupExtender ID="mpePop" BehaviorID="mpe" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_addtopicicon"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="data">
        <div class="data__content" onscroll="sticky_div();">
            <div class="container">
                <div class="card add-topic">
                    <asp:UpdatePanel ID="upEditIcon" runat="server" class="add-topic__icon">
                        <ContentTemplate>

                            <asp:LinkButton ID="lbAddIcon" runat="server" OnClick="lbAddIcon_Click">
                                <%--<a href="javascript:showPopup(1);">--%>
                                <asp:Image ID="imgTopic" ImageUrl="https://s3-ap-southeast-1.amazonaws.com/cocadre/topic-icons/default_topic_icon.png" runat="server" Width="120" Height="120" />
                                <label><small>Choose a evaluation icon</small></label>
                                <%--</a>--%>
                            </asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="add-topic__info">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" class="add-topic__info__action" style="width: 35%;">
                            <ContentTemplate>
                                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn" Text="Create" OnClick="lbCreate_Click" OnClientClick="ShowProgressBar();" />
                                <asp:HyperLink ID="hlCancel" runat="server" NavigateUrl="/MLearning/EvaluationList" Text="Cancel" CssClass="btn secondary" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="add-topic__info__details">
                            <div class="tabs tabs--styled">
                                <ul class="tabs__list">
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#tab-basic-info">Basic Info</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#tab-privacy">Privacy</a>
                                    </li>
                                    <li class="tabs__list__item">
                                        <a class="tabs__link" href="#tab-settings">Settings</a>
                                    </li>
                                </ul>
                                <div class="tabs__panels">
                                    <div class="tabs__panels__item add-topic__info__details--basicinfo" id="tab-basic-info">
                                        <div class="column--input" style="width: 60%; margin: 0px;">
                                            <div class="form__row" style="width: 100%;">
                                                <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Title</label>
                                                <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                    <asp:TextBox ID="tbMLearningTitle" runat="server" CssClass="topic-detail" placeholder="Name your evaluation" onkeydown="return (event.keyCode!=13);" Style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle;" />
                                                    <asp:Label ID="lblMLearningTitleCount" runat="server" CssClass="topic-lettercount" Style="bottom: 5px; right: 5px; position: absolute;" />
                                                </div>
                                            </div>
                                            <div class="form__row" style="width: 100%;">
                                                <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Introduction</label>
                                                <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                    <asp:TextBox ID="tbMLearningIntroduction" runat="server" CssClass="topic-detail" TextMode="MultiLine" placeholder="Welcome message / purpose of this evaluation" Style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;" />
                                                    <asp:Label ID="lblMLearningIntroductionCount" runat="server" CssClass="topic-lettercount" Style="bottom: 5px; right: 5px; position: absolute;" />
                                                </div>
                                            </div>
                                            <div class="form__row" style="width: 100%;">
                                                <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Instructions</label>
                                                <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                    <asp:TextBox ID="tbMLearningInstructions" runat="server" CssClass="topic-detail" TextMode="MultiLine" placeholder="Enter the rules and instructions for the participants.   Eg. 1. Please read the question carefully" Style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;" />
                                                    <asp:Label ID="lblMLearningInstructionsCount" runat="server" CssClass="topic-lettercount" Style="bottom: 5px; right: 5px; position: absolute;" />
                                                </div>
                                            </div>
                                            <div class="form__row" style="width: 100%;">
                                                <label style="width: 22%; text-align: right; color: rgba(127, 127, 127, 1); font-size: 1.5em; margin-right: 5px; vertical-align: middle; display: inline-block;">Closing Words</label>
                                                <div style="border-image: none; width: 76%; color: rgba(209, 209, 209, 1); font-size: 1.1em; vertical-align: middle; display: inline-block; position: relative;">
                                                    <asp:TextBox ID="tbMLearningClosingWords" runat="server" CssClass="topic-detail" TextMode="MultiLine" placeholder="Thank you for participation / how can they follow up" Style="margin: 0px; padding: 15px 40px 15px 15px; border-radius: 3px; border: 1px solid rgba(209, 209, 209, 1); border-image: none; width: 100% !important; vertical-align: middle; height: 130px; overflow: hidden;" />
                                                    <asp:Label ID="lblMLearningClosingWordsCount" runat="server" CssClass="topic-lettercount" Style="bottom: 5px; right: 5px; position: absolute;" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column--choice" style="width: 25%; margin-left: 15%;">
                                            <div class="form__row" style="display: none;">
                                                <label style="font-weight: bold; font-size: 1.2em;">Author</label>
                                                <asp:Label ID="lblAuthor" runat="server" Font-Size="1.0em" /><br />
                                                <div style="padding: 5px;">
                                                    <asp:CheckBox ID="cbAuthor" runat="server" Style="vertical-align: middle;" />
                                                    <label style="display: inline-block; vertical-align: middle; font-weight: bold;" for="main_content_cbAuthor">Display on client</label>
                                                </div>
                                            </div>

                                            <div class="form__row">
                                                <label style="font-weight: bold; font-size: 1.2em;">Category</label>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <ajaxToolkit:ComboBox ID="cbCategory" runat="server" AutoCompleteMode="Suggest" CssClass="combos" Width="100%" onkeydown="return (event.keyCode!=13);" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form__row">
                                                <label style="font-weight: bold; font-size: 1.2em;">Passing Grade</label>
                                                <div>
                                                    <asp:TextBox ID="tbPassingGradeInt" runat="server" Style="margin: 0px; width: 30%; display: inline-block;" TextMode="Number" MaxLength="5" Text="60"
                                                        onkeypress="return allowOnlyNumber(event);" />
                                                    <asp:CheckBox ID="cbPassingGradePer" runat="server" Checked="true" /><label style="display: inline-block;">%</label>
                                                </div>
                                                <label style="color: rgba(203, 203, 203, 1);">Type number for score or add % for percentage</label>
                                            </div>
                                            <div class="form__row">
                                                <div>
                                                    <label style="font-weight: bold; font-size: 1.2em; display: inline-block;">Evaluation time and duration</label>
                                                    <img onmouseover="showTip(1);" onmouseout="hideTip(1);" src="/Img/icon_note_small.png" title="Go to 'Settings' to set the evaluation time and duration" style="display: inline-block;" />
                                                </div>

                                                <div class="duration_tip" style="top: 11px;">
                                                    <label style="font-weight: 700;">Evaluation time and druation</label>
                                                    <span>Go to 'Settings' to set the evaluation time and duration.</span>
                                                </div>
                                                <a style="margin-top: 20px; display: block; cursor: pointer;" onclick="$('.tabs.tabs--styled').easytabs('select', '#tab-settings');">Edit</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--department" id="tab-privacy">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div style="font-size: 1.1em;">
                                                    <asp:CheckBox ID="cbParticipantsEveryone" runat="server" Checked="true" AutoPostBack="true" OnCheckedChanged="cbParticipantsEveryone_CheckedChanged" /><i class="fa fa-users" style="color: #999999"></i> Everyone
                                                </div>

                                                <hr />
                                                <div style="font-size: 1.1em;">
                                                    <asp:CheckBox ID="cbParticipantsDepartment" AutoPostBack="true" OnCheckedChanged="cbParticipantsDepartment_CheckedChanged" runat="server" /><i class="fa fa-briefcase" style="color: #999999"></i> Selected departments
                                                </div>
                                                <div>

                                                    <asp:Repeater ID="rtParticipantsDepartment" runat="server" OnItemCommand="rtParticipantsDepartment_ItemCommand">
                                                        <HeaderTemplate>
                                                            <div class="tags">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <div class="tag">
                                                                <asp:Label ID="lblTagName" runat="server" CssClass="tag__label" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' />
                                                                <asp:LinkButton ID="lbTagRemove" runat="server" CssClass="tag__icon" Text="x" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Id")%>' CommandName="Remove" />
                                                            </div>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <br />
                                                    <asp:LinkButton ID="lbAddParticipantsDepartment" runat="server" Text="+ Add more department" OnClick="lbAddParticipantsDepartment_Click" />

                                                </div>
                                                <br />
                                                <div style="font-size: 1.1em;">
                                                    <asp:CheckBox ID="cbParticipantsPersonnel" runat="server" AutoPostBack="true" OnCheckedChanged="cbParticipantsPersonnel_CheckedChanged" /><i class="fa fa-user" style="color: #999999"></i> Selected personnel
                                                </div>
                                                <div>
                                                    <asp:Repeater ID="rtParticipantsPersonnel" runat="server" OnItemCommand="rtParticipantsPersonnel_ItemCommand">
                                                        <HeaderTemplate>
                                                            <div class="tags">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <div class="tag">
                                                                <asp:Label ID="lblTagName" runat="server" CssClass="tag__label" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName")%>' />
                                                                <asp:LinkButton ID="lbTagRemove" runat="server" CssClass="tag__icon" Text="x" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId")%>' CommandName="Remove" />
                                                            </div>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                    <br />
                                                    <div class="suspendedsearch" style="width: 60%;">
                                                        <asp:TextBox ID="tbSearchKey" runat="server" placeholder="Search Personnel" onkeydown="return (event.keyCode!=13);showLoading();" onkeyup="RefreshUpdatePanel(event);" MaxLength="50" OnTextChanged="tbSearchKey_TextChanged" autocomplete="off" />
                                                        <img id="imgLoading" src="/Img/circle_loading.gif" style="display: none;" />
                                                        <div class="suggestions">
                                                            <asp:Repeater ID="rtSearchResult" runat="server" OnItemCommand="rtSearchResult_ItemCommand" OnItemDataBound="rtSearchResult_ItemDataBound">
                                                                <HeaderTemplate>
                                                                    <ul class="suggestions__list">
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <li class="suggestions__list__item">
                                                                        <asp:LinkButton ID="lbAddUser" runat="server" CssClass="suggestions__link" ClientIDMode="AutoID" CommandName="AddUser" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserId")+ "," + DataBinder.Eval(Container.DataItem, "FirstName") +"," + DataBinder.Eval(Container.DataItem, "LastName") %>'>
                                                                            <span class="suggestions__name">
                                                                                <asp:Literal ID="ltlUserName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FirstName") +" " + DataBinder.Eval(Container.DataItem, "LastName")  %>' />
                                                                            </span>
                                                                            <span class="suggestions__email"><%# DataBinder.Eval(Container.DataItem, "Email") %></span>
                                                                        </asp:LinkButton></li>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </ul>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="tabs__panels__item add-topic__info__details--settings" id="tab-settings">

                                        <style>
                                            .rbl li label {
                                                display: inline-block;
                                            }
                                        </style>

                                        <div style="width: 100%; display: inline-block; margin-top: 20px;">
                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                <ContentTemplate>
                                                    <label style="font-size: 1.2em; font-weight: bold;">Start date</label>
                                                    <asp:TextBox ID="tbStartDate" runat="server" placeholder="dd/mm/yyyy" CssClass="date StartDate" onkeydown="return false;" />
                                                    <span class="text-vertical-center">at</span>
                                                    <asp:TextBox ID="tbStartHour" runat="server" placeholder="hh" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="12" />
                                                    <span class="text-vertical-center">:</span>
                                                    <asp:TextBox ID="tbStartMinute" runat="server" placeholder="mm" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="00" />
                                                    <div class="mdl-selectfield">
                                                        <asp:DropDownList ID="ddlStartMer" runat="server" Style="padding: 0.7em; display: inline;">
                                                            <asp:ListItem Text="am" Value="am" />
                                                            <asp:ListItem Text="pm" Value="pm" />
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:Label ID="lblScheduleStartDate" runat="server" Visible="false" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                <ContentTemplate>
                                                    <asp:Panel ID="plEndDate" runat="server" Visible="false">
                                                        <label style="font-size: 1.2em; font-weight: bold;">End date</label>
                                                        <asp:TextBox ID="tbEndDate" runat="server" placeholder="dd/mm/yyyy" CssClass="date EndtDate" onkeydown="return false;" />
                                                        <span class="text-vertical-center">at</span>
                                                        <asp:TextBox ID="tbEndHour" runat="server" placeholder="hh" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="12" />
                                                        <span class="text-vertical-center">:</span>
                                                        <asp:TextBox ID="tbEndMinute" runat="server" placeholder="mm" CssClass="time" MaxLength="2" onkeypress="return allowOnlyNumber(event);" Text="00" />
                                                        <div class="mdl-selectfield">
                                                            <asp:DropDownList ID="ddlEndMer" runat="server" Style="padding: 0.7em; display: inline;">
                                                                <asp:ListItem Text="am" Value="am" />
                                                                <asp:ListItem Text="pm" Value="pm" />
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:Label ID="lblScheduleEndDate" runat="server" Visible="false" />
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>

                                        <div style="width: 33%; display: block; margin-top: 20px;">
                                            <div class="form__row" style="width: 100%;">
                                                <label style="font-weight: bold; font-size: 1.2em;">Duration</label>
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <asp:RadioButtonList ID="rblDuration" runat="server" Style="border: 0px;" RepeatLayout="OrderedList" CssClass="rbl" AutoPostBack="true" OnSelectedIndexChanged="rblDuration_SelectedIndexChanged">
                                                            <asp:ListItem Text="Perpetual" Value="1" Selected="True" />
                                                            <asp:ListItem Text="Schedule" Value="2" />
                                                        </asp:RadioButtonList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>

                                            <div class="form__row" style="width: 100%;">
                                                <label style="font-weight: bold; font-size: 1.2em;">Allow retest for failures</label>
                                                <asp:RadioButtonList ID="rblAllowRetestForFailures" runat="server" Style="border: 0px;" RepeatLayout="OrderedList" CssClass="rbl">
                                                    <asp:ListItem Text="Yes" Value="true" Selected="True" />
                                                    <asp:ListItem Text="No" Value="false" />
                                                </asp:RadioButtonList>
                                            </div>

                                            <div class="form__row" style="width: 100%; display: none;">
                                                <label style="font-weight: bold; font-size: 1.2em;">Allow review answer</label>
                                                <asp:RadioButtonList ID="rblAllowReview" runat="server" Style="border: 0px;" RepeatLayout="OrderedList" CssClass="rbl">
                                                    <asp:ListItem Text="No" Value="3" />
                                                    <asp:ListItem Text="Yes, all answers" Value="2" Selected="True" />
                                                    <asp:ListItem Text="Yes, those answered incorrectly only" Value="1" />
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div style="width: 33%; display: inline-block;">

                                            <div class="form__row" style="width: 100%; display: none;">
                                                <label style="font-weight: bold; font-size: 1.2em;">Display answer</label>
                                                <asp:RadioButtonList ID="rblDisplayAnswer" runat="server" Style="border: 0px;" RepeatLayout="OrderedList" CssClass="rbl">
                                                    <asp:ListItem Text="End of test only" Value="1" />
                                                    <asp:ListItem Text="End of test or anytime after" Value="2" Selected="True" />
                                                </asp:RadioButtonList>
                                            </div>


                                            <div class="form__row" style="width: 100%; display: none;">
                                                <label style="font-weight: bold; font-size: 1.2em;">Questions random</label>
                                                <asp:RadioButtonList ID="rblQuestionRandom" runat="server" Style="border: 0px;" RepeatLayout="OrderedList" CssClass="rbl">
                                                    <asp:ListItem Text="Yes" Value="true" Selected="True" />
                                                    <asp:ListItem Text="no" Value="false" />
                                                </asp:RadioButtonList>
                                            </div>

                                            <div class="form__row" style="width: 100%; display: none;">
                                                <label style="font-weight: bold; font-size: 1.2em;">Allow retest for passers</label>
                                                <asp:RadioButtonList ID="rblAllowRetestForPassers" runat="server" Style="border: 0px;" RepeatLayout="OrderedList" CssClass="rbl">
                                                    <asp:ListItem Text="Yes" Value="true" Selected="True" />
                                                    <asp:ListItem Text="No" Value="false" />
                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                        <div style="width: 33%; display: inline-block;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
