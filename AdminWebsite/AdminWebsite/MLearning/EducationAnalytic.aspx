﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EducationAnalytic.aspx.cs" Inherits="AdminWebsite.MLearning.EducationAnalytic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="/Css/ca-style.css" />
	<link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
	<style>
		#analytic-wrapper .data__content {
			background: #F5F5F5;
		}

		#education-analytic {
			margin-top: 30px;
		}

		.filter-section {
			margin: 0 auto;
		}

			.filter-section .search-input {
				font-size: 18px;
				padding-left: 50px !important;
			}

			.filter-section .fa {
				font-size: 25px;
			}

		.invisible {
			visibility: hidden;
		}

		.mdl-selectfield {
			width: 220px !important;
		}

		.dynatable-per-page {
			display: none;
		}

		.dynatable-pagination-links {
			float: none;
			text-align: center;
			padding: 20px;
		}

		.dynatable-active-page {
			background: #cccccc;
		}
	
		.topic-img {
			width: 50px;
			height: 50px;
			display: inline-block;
			vertical-align:middle;
			margin-right: 15px;
		}
        .legend-color1 {
            background: rgba(254, 149, 0, 1) !important;
        }

        .legend-color2 {
            background: #ccc !important;
        }
        .legend-color3 {
            background: #fff !important;
            border: 1px solid #ccc !important;
        }
        .progress-bar.color1 {
            background-color: rgba(254, 149, 0, 1) !important;
        }
        .progress-bar.color2 {
            background-color: #ccc !important;
        }
        .progress-bar.color3 {
            background-color: #fff !important;
            border: 1px solid #ccc;
        }
        .color1 {
            color: rgba(254, 149, 0, 1) !important;
        }
        .color2 {
            color: #ccc !important;
        }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

	<asp:HiddenField runat="server" ID="hfAdminUserId" />
	<asp:HiddenField runat="server" ID="hfCompanyId" />
	<asp:HiddenField runat="server" ID="hfEducationId" />
	<asp:HiddenField runat="server" ID="hfCategoryId" />

	<!-- App Bar -->
	<div id="st-trigger-effects" class="appbar">
		<a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
		<div class="appbar__title"><strong>M</strong> <span>Learning</span></div>
		<div class="appbar__meta"><a href="/MLearning/EducationList" id="root-link">Education Analytics</a></div>
		<div class="appbar__meta">
			<!--<a id="second-link"></a>-->
			<span style="color: black;" id="second-link"></span>
		</div>
		<div class="appbar__action">
			<a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
		</div>
	</div>
	<!-- /App Bar -->
	
	<div id="analytic-wrapper" class="data">

		<div class="data__content">

			<div id="education-analytic" class="container" style="width: 100%;">

				<div style="width: 100%; margin: 0 auto;">
					<!-- TITLE -->
					<div class="title-section" style="width: 100%; margin: 0 auto;">
						<div class="analytic-info">
							<h2 id="analytic-title" class="ellipsis-title"></h2>
						</div>
						<div class="header-left" style="margin-bottom:10px;">
							<ul class="header-list">
								<li>
									<span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Category:</span>
									<span class="category-type" style="display:inline-block; font-size: 16px;color: #000;"></span>
								</li>
								<li>
									<span style="width:150px; display:inline-block; font-size: 16px;color: #727272;">Answer:</span>
									<span class="answer-type" style="display:inline-block; font-size: 16px;color: #000;"></span>
								</li>
							</ul>
							<p class="align-left"><span id="start-date-string" style="font-size: 16px;color: #727272;"></span></p>
						</div>
						<div class="header-right">
							<p id="progress-status" class="status" style="font-size:16px;"><span class="rounded-indicator"></span>Completed</p>
							<p class="align-left" style="margin-top:50px;"><span id="data-date-string" style="font-size: 16px;color: #727272;"></span></p>
						</div>
					</div>

					<div id="overview-tab" class="tabs tabs--styled" style="clear:both; ">
						<ul class="tabs__list">
							<li class="tabs__list__item">
								<a class="tabs__link" href="#candidates">Candidates</a>
							</li>
						</ul>
						<div class="tabs__panels">
							<!-- OVERVIEW TAB -->
							<div class="tabs__panels__item overview" id="candidates">

								<div id="top-card" class="card full-row animated fadeInUp">
									<!-- DYNAMICALLY GENERATE CONTENT FOR THIS CARD -->
								</div>

								<div class="filter-section text-center" style="clear:both; margin-top: 30px; padding-left: 20px;">
									<div class="option-filter">
										<div class="mdl-selectfield" style="width: 250px; float: left; margin-right: 20px; height: 40px;">
											<select name="status" id="status">
												<option selected="selected" value="">All Departments</option>
											</select>
										</div>
									</div>
									<div class="search-filter">
										<div class="input-group" style="position: relative;">
											<i class="fa fa-search"></i>
											<input id="hidden_name" class="search-input" type="text" placeholder="Search Responders" value="" />
										</div>
									</div>
								</div>

								<div id="personnel-card" class="card full-row animated fadeInUp no-padding">
									<div class="table-content" style="position:relative;">
										<div class="table-progress" style="width:100%; height:100%; position:absolute; text-align:center; top:0; left:0; background:rgba(255,255,255,0.8); z-index: 2; display:none;">
											<div style="position:absolute; top:45%; width:100%;">
												<div class="spinner">
													<div class="rect1"></div>
													<div class="rect2"></div>
													<div class="rect3"></div>
													<div class="rect4"></div>
													<div class="rect5"></div>
												</div>
												<div id="ProgressText" style="font-size: 15px; color: #1B3563;">Processing...</div>
											</div>
											
										</div>
										<table id="responder-table" class="responder-table" style="margin:0px;"></table>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>

			</div>

		</div>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
		<script src="/Js/Chart.js"></script>
	<script>
			$(function () {

				var CompanyId = $('#main_content_hfCompanyId').val();
				var ManagerId = $('#main_content_hfAdminUserId').val();
				var CategoryId = $('#main_content_hfCategoryId').val();
				var EducationId = $('#main_content_hfEducationId').val();

				var card_right_html = "";
				var card_left_html = "";
				var legend_html = "";
				var progress_html = "";
				var table_html = "";

				var responder_table = $('#responder-table');
				var responder_data = [];
				
				function getProgressStatus(statusCode) {
					var status = "";

					if (statusCode == 1) {
						status = "<span style='color:#ccc'>Absent</span>";
					} else if (statusCode == 2) {
						status = "<span style='color:#ccc'>Incomplete</span>";
					} else if (statusCode == 3) {
						status = "Completed";
					}

					return status;
				}

				function generateTable(userList) {
					responder_data = [];

					table_html = "";
					table_html += '<thead>';
					table_html += '<th data-dynatable-column="no" style="width:10%">No</th>';
					table_html += '<th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width:30%">Name</th>';
					table_html += '<th class="hidden" data-dynatable-column="hidden_name">Hidden name</th>';
					table_html += '<th data-dynatable-column="date" data-dynatable-sorts="date" style="width:20%">Date</th>';
					table_html += '<th data-dynatable-column="department" data-dynatable-sorts="department" style="width:20%">Department</th>';
					table_html += '<th data-dynatable-column="status" style="width:20%">Status</th>';
					table_html += '</thead><tbody></tbody>';

					$.each(userList, function (k, v) {

						var nameHTML = "";

						if (v.EducationAnalytic.ProgressStatus == 3) {
							nameHTML = '<a href="/MLearning/EducationAnalyticUser/' + EducationId + '/' + CategoryId + '/' + v.UserId + '" target="_blank">' + v.FirstName + " " + v.LastName + '</a>';
						} else {
							nameHTML = v.FirstName + " " + v.LastName;
						}

						var status = getProgressStatus(v.EducationAnalytic.ProgressStatus);

						responder_data.push({
							"no": k + 1,
							"responder_name": nameHTML,
							"hidden_name": v.FirstName + " " + v.LastName,
							"date": v.EducationAnalytic.LastProgressDateTimeString == null ? 'NA' : v.EducationAnalytic.LastProgressDateTimeString,
							"department": v.Departments[0].Title,
							"status": status
						})
					});

					$('#responder-table').append(table_html);

					responder_table.dynatable({
						features: {
							paginate: true,
							search: false,
							sorting: true,
							recordCount: false,
							pushState: false,
						},
						dataset: {
							records: responder_data,
							sorts: { 'no': 1 }
						},
						inputs: {
							queries: $(' #hidden_name')
						}
					});


					var dynatable = responder_table.data('dynatable');

					if (typeof dynatable.records !== "undefined") {
						dynatable.records.updateFromJson({ records: responder_data });
						dynatable.records.init();
					}
					dynatable.paginationPerPage.set(15);
					dynatable.process();


					responder_table.data('dynatable').paginationPerPage.set(15);
					responder_table.data('dynatable').process();

					setTimeout(function () {
						responder_table.css('display', 'table');
						responder_table.addClass('animated fadeIn');
					}, 500);
				}

				function fetchData() {
					$('#education-analytic').addClass('hidden');
					$('#education-analytic').removeClass('animated fadeInUp');

					ShowProgressBar();
					$.ajax({
						type: "POST",
						url: '/Api/MLEducation/AnalyticAttendance',
						data: {
							"CompanyId": CompanyId,
							"ManagerId": ManagerId,
							"CategoryId": CategoryId,
							"EducationId": EducationId
						},
						crossDomain: true,
						dataType: 'json',
						success: function (res) {
							HideProgressBar();
							if (res.Success) {
								$('#education-analytic').removeClass('hidden');
								$('#education-analytic').addClass('animated fadeInUp');

								var education = res.Education;
								var attendant = res.Attendants;
								var userList = res.Users;
								var departments = res.Departments;

								var titleHTML = education.Title;

								$('.category-type').html(education.Category.Title);
								$('.answer-type').html(education.DisplayAnswerType == 1 ? 'Hidden' : 'Show');

								if (departments.length > 0) {
									$.each(departments, function (k, v) {
										$('select').append('<option value="' + v.Id + '">' + v.Title + '</option>');
									});
								}

								if (education.IconUrl) {
									titleHTML = '<img src="' + education.IconUrl + '" class="topic-img" />' + titleHTML;
								} 

								$('#analytic-title').html(titleHTML);
								$('#second-link').html(education.Title);

								//progressStatus
								//Upcoming = 1,
								//Live = 2,
								//Completed = 3

								if (education.ProgressStatus == 1) {
									$('#progress-status').html('<span class="rounded-indicator color1"></span> Upcoming');
								} else if (education.ProgressStatus == 2) {
									$('#progress-status').html('<span class="rounded-indicator color2"></span> Live');
								} else if (education.ProgressStatus == 3) {
									$('#progress-status').html('<span class="rounded-indicator color3"></span> Completed');
								}

								$('#start-date-string').html("Showing data from "+education.AnalyseDurationString);

								if (education.EndDateString) {
									$('#data-date-string').html('End date ' + education.EndDateString);
								} 

								$.each(attendant, function (k, v) {
									legend_html += '<div class="legend-item"><div class="legend-icon"><div class="legend-color' + (k + 1) + '"></div></div>';
									legend_html += '<label>' + v.Name + '</label></div>';

									var percentage = v.Percentage;

									if (v.Percentage > 0) {
										percentage = v.Percentage - 0.1;
									}
									progress_html += '<div class="progress-bar color' + (k + 1) + '"  style="width:' + percentage + '%;">';

                                    if (k == 0 && v.Percentage < 8) {
										progress_html += '<span class="value" style="left:-25px;">' + v.Count + '</span>';
										progress_html += '<span class="percentage color' + (k + 1) + '" style="color:rgba(254, 149, 0, 1); width:' + (v.Percentage - 0.1) + '%; left:-27px;">' + v.Percentage + '%</span></div>';
                                    } else if (k == 1 && v.Percentage < 8) {
										progress_html += '<span class="value" style="left:15px;">' + v.Count + '</span>';
										progress_html += '<span class="percentage color' + (k + 1) + '" style="color:rgba(254, 149, 0, 1); width:' + (v.Percentage - 0.1) + '%; left:10px;">' + v.Percentage + '%</span></div>';
									} else {
										progress_html += '<span class="value">' + v.Count + '</span>';
										progress_html += '<span class="percentage color' + (k + 1) + '" style="color:#000; width:' + (v.Percentage - 0.1) + '%;">' + v.Percentage + '%</span></div>';
									}

								});

								card_left_html += '<div class="card-header">';
								card_left_html += '<h1 class="title">Attendance</h1>';
								card_left_html += '<div class="legends">';
								card_left_html += legend_html;
								card_left_html += '</div></div>';
								card_left_html += '<div class="type-content"><div class="progress stacked" style="padding-left: 30px;padding-right: 30px;">';
								card_left_html += progress_html + '</div>';

								$('#top-card').empty();
								$('#top-card').append(card_left_html);

								generateTable(userList);

								
							}
							else { // ERROR RESPONSE
								$('#education-analytic').empty();
								$('#education-analytic').html('<p class="align-center">' + res.ErrorMessage + '</p>');
							}
						}
					});
				}

				responder_table.bind('dynatable:afterUpdate', function (e, dynatable) {
					setTimeout(function () {
						responder_table.css('display', 'table');
						responder_table.addClass('animated fadeIn');
					}, 1500);
				});

				// CUSTOM TABLE SEARCH FILTER FUNCTION
				responder_table.bind('dynatable:init', function (e, dynatable) {

					dynatable.queries.functions['hidden_name'] = function (record, queryValue) {
						return record.hidden_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
					};
				});

				fetchData();

				function refreshTable(id) {
					$.ajax({
						type: "POST",
						url: '/Api/MLEducation/AnalyticUsers',
						data: {
							"CompanyId": CompanyId,
							"ManagerId": ManagerId,
							"EducationId": EducationId,
							"DepartmentId": id
						},
						crossDomain: true,
						dataType: 'json',
						success: function (res) {

							if (res.Success) {
								$('.table-progress').css('display', 'none');
								var userList = res.Users;
								$('#responder-table').empty();
								generateTable(userList);
							}

						}

					});
				}

				$('#status').on('change', function (e) {
					var id = $(this).val();
					$('.table-progress').css('display', 'block');
					refreshTable(id);
				});
			});
		</script>
</asp:Content>
