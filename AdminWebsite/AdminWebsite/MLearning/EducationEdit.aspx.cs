﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CassandraService.Entity.MLEducation;
using static CassandraService.Entity.MLEducationCategory;

namespace AdminWebsite.MLearning
{
    public partial class EducationEdit : System.Web.UI.Page
    {
        private const int MLEARNING_TITLE_TEXT_MAX = 50;
        private const int MLEARNING_DESCRIPTION_TEXT_MAX = 26;

        private System.Web.Script.Serialization.JavaScriptSerializer javascriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        private ManagerInfo managerInfo;
        private AdminService asc = new AdminService();
        private List<String> iconUrlList = null;
        private List<User> searchUserList = null;
        private List<MLEducationCategory> categoryList = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.managerInfo = Session["admin_info"] as ManagerInfo;
            ViewState["manager_user_id"] = managerInfo.UserId;
            ViewState["company_id"] = managerInfo.CompanyId;

            if (Page.RouteData.Values["EducationId"] == null || Page.RouteData.Values["CategoryId"] == null)
            {
                Response.Redirect("/MLearning/EducationList");
                return;
            }

            try
            {
                hfCompanyId.Value = managerInfo.CompanyId;
                hfManagerUserId.Value = managerInfo.UserId;
                hfEducationId.Value = Page.RouteData.Values["EducationId"].ToString();
                hfCategoryId.Value = Page.RouteData.Values["CategoryId"].ToString();

                if (!IsPostBack)
                {
                    ViewState["selected_department"] = new List<Department>();
                    ViewState["selected_user"] = new List<User>();

                    MLEducationDetailResponse response = asc.SelectFullDetailMLEducation(
                        ViewState["company_id"].ToString(), ViewState["manager_user_id"].ToString(), hfCategoryId.Value, hfEducationId.Value);

                    if (response.Success)
                    {
                        #region Step 1. Data binding
                        // Icon url
                        imgTopic.ImageUrl = response.Education.IconUrl;

                        // Title

                        tbEducationTitle.Text = response.Education.Title;
                        lblEducationTitleCount.Text = Convert.ToString(MLEducation.TITLE_TEXT_MAX - response.Education.Title.Length);
                        tbEducationTitle.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", lblEducationTitleCount.ClientID, MLEducation.TITLE_TEXT_MAX);

                        // Introduction
                        this.tbDescription.Text = response.Education.Description;
                        this.lblDescriptionCount.Text = Convert.ToString(MLEducation.DESCRIPTION_TEXT_MAX - response.Education.Description.Length);
                        this.tbDescription.Attributes["onkeyup"] = String.Format("textCounter(this,'{0}','{1}');", this.lblDescriptionCount.ClientID, MLEducation.DESCRIPTION_TEXT_MAX);


                        lblAuthor.Text = response.Education.Author.FirstName + " " + response.Education.Author.LastName;
                        cbAuthor.Checked = response.Education.IsDisplayAuthorOnClient;

                        GetCategoryData();
                        if (categoryList != null)
                        {
                            for (int i = 0; i < categoryList.Count; i++)
                            {
                                cbCategory.Items.Add(new ListItem(categoryList[i].Title, categoryList[i].CategoryId));
                            }
                        }
                        cbCategory.SelectedValue = response.Education.Category.CategoryId;

                        ddlStatus.Items.Clear();
                        if (response.Education.Status == (int)MLTopic.MLTopicStatusEnum.Unlisted)
                        {
                            ddlStatus.Items.Add(new ListItem("Unlisted", "1"));
                            ddlStatus.Items.Add(new ListItem("Active", "2"));
                        }
                        else
                        {
                            ddlStatus.Items.Add(new ListItem("Active", "2"));
                            ddlStatus.Items.Add(new ListItem("Hidden", "3"));
                        }

                        ddlStatus.SelectedValue = Convert.ToString(response.Education.Status);

                        // Privacy
                        if (response.Education.IsForEveryone)
                        {
                            cbParticipantsEveryone.Checked = true;
                            cbParticipantsDepartment.Checked = false;
                            cbParticipantsPersonnel.Checked = false;
                        }
                        else
                        {
                            cbParticipantsEveryone.Checked = false;

                            if (response.Education.IsForDepartment)
                            {
                                cbParticipantsDepartment.Checked = true;
                                rtParticipantsDepartment.DataSource = response.Education.TargetedDepartments;
                                rtParticipantsDepartment.DataBind();

                                ViewState["selected_department"] = response.Education.TargetedDepartments;
                            }
                            else
                            {
                                cbParticipantsDepartment.Checked = false;
                            }

                            if (response.Education.IsForUser)
                            {
                                cbParticipantsPersonnel.Checked = true;
                                rtParticipantsPersonnel.DataSource = response.Education.TargetedUsers;
                                rtParticipantsPersonnel.DataBind();

                                ViewState["selected_user"] = response.Education.TargetedUsers;
                            }
                            else
                            {
                                cbParticipantsPersonnel.Checked = false;
                            }
                        }

                        // StartDateTime
                        DateTime startDateTime = response.Education.StartDate.AddHours(managerInfo.TimeZone);
                        tbStartDate.Text = startDateTime.ToString("dd/MM/yyyy");
                        tbStartHour.Text = startDateTime.ToString("hh");
                        tbStartMinute.Text = startDateTime.ToString("mm");
                        ddlStartMer.ClearSelection();
                        ddlStartMer.SelectedIndex = Convert.ToInt16(startDateTime.ToString("HH")) > 12 ? 1 : 0;

                        // Duration and EndDateTime
                        if (response.Education.DurationType == (int)DurationTypeEnum.Perpetual)
                        {
                            rblDuration.SelectedIndex = 0;

                            plEndDate.Visible = false;
                            tbEndDate.Text = startDateTime.AddMonths(1).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            rblDuration.SelectedIndex = 1;

                            plEndDate.Visible = true;
                            DateTime endDateTime = response.Education.EndDate.Value.AddHours(managerInfo.TimeZone);
                            tbEndDate.Text = endDateTime.ToString("dd/MM/yyyy");
                            tbEndHour.Text = endDateTime.ToString("hh");
                            tbEndMinute.Text = endDateTime.ToString("mm");

                            ddlEndMer.SelectedIndex = Convert.ToInt16(endDateTime.ToString("HH")) > 12 ? 1 : 0;
                        }

                        if (response.Education.DisplayAnswerType == (int)DisplayAnswerTypeEnum.ShowAnswer)
                        {
                            rbShowAnswerTypeShow.Checked = true;
                        }
                        else
                        {
                            rbShowAnswerTypeShow.Checked = false;
                        }
                        rbShowAnswerTypeHide.Checked = !rbShowAnswerTypeShow.Checked;

                        chkAutoBreak.Checked = response.Education.IsAutoBreak;

                        if (response.Education.ProgressStatus == (int)MLEducation.ProgressStatusEnum.Upcoming || response.Education.Status == (int)MLEducation.MLEducationStatusEnum.Unlisted)
                        {
                            hlAnalytics.Visible = false;
                        }
                        else
                        {
                            hlAnalytics.NavigateUrl = "/MLearning/EducationAnalytic/" + response.Education.EducationId + "/" + response.Education.Category.CategoryId;
                        }


                        if (response.Education.Status > (int)MLEducation.MLEducationStatusEnum.Unlisted && response.Education.ProgressStatus != (int)MLEducation.ProgressStatusEnum.Upcoming)
                        {
                            cbParticipantsEveryone.Enabled = false;
                            cbParticipantsDepartment.Enabled = false;
                            cbParticipantsPersonnel.Enabled = false;
                            lbAddParticipantsDepartment.Visible = false;
                            tbSearchKey.Visible = false;
                            tbStartDate.Enabled = false;
                            tbStartHour.Enabled = false;
                            tbStartMinute.Enabled = false;
                            ddlStartMer.Enabled = false;
                            tbEndDate.Enabled = false;
                            tbEndHour.Enabled = false;
                            tbEndMinute.Enabled = false;
                            ddlEndMer.Enabled = false;
                            rblDuration.Enabled = false;
                            rblDisplayAnswer.Enabled = false;
                            chkAutoBreak.Enabled = false;
                            rbShowAnswerTypeHide.Enabled = false;
                            rbShowAnswerTypeShow.Enabled = false;

                        }
                        #endregion

                        hfEducation.Value = javascriptSerializer.Serialize(response.Education);


                        if (response.Education.ProgressStatus == (int)MLEducation.ProgressStatusEnum.Live && response.Education.Status != (int)MLEducation.MLEducationStatusEnum.Unlisted)
                        {
                            // Show Alert Popup
                            mpePop.PopupControlID = "popup_alertpopup";
                            mpePop.Show();
                        }
                    }
                    else
                    {
                        Log.Error("MLEducationDetailResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                        Response.Redirect("/MLearning/EducationList");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            //ltlShow.Text = "EducationId: " + Page.RouteData.Values["EducationId"].ToString() + " || CategoryId: " + Page.RouteData.Values["CategoryId"].ToString();
        }



        protected void lbAddIcon_Click(object sender, EventArgs e)
        {
            try
            {
                #region Icon data
                GetIconData();
                rtIcon.DataSource = iconUrlList;
                rtIcon.DataBind();
                #endregion
                mpePop.PopupControlID = "popup_addtopicicon";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbPopCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void cbAllDepartment_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbAllDepartment.Checked)
                {
                    for (int i = 0; i < cblDepartment.Items.Count; i++)
                    {
                        cblDepartment.Items[i].Selected = true;
                    }
                }
                else
                {
                    cblDepartment.ClearSelection();
                }
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void cblDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int selectedCount = 0;
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (cblDepartment.Items[i].Selected)
                    {
                        selectedCount++;
                    }
                }
                if (selectedCount != cblDepartment.Items.Count)
                {
                    cbAllDepartment.Checked = false;
                }
                else
                {
                    cbAllDepartment.Checked = true;
                }
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbSelectDepartmentSelect_Click(object sender, EventArgs e)
        {
            try
            {
                List<Department> selectedDepartmentList = new List<Department>();
                for (int i = 0; i < cblDepartment.Items.Count; i++)
                {
                    if (cblDepartment.Items[i].Selected)
                    {
                        Department department = new Department();
                        department.Id = cblDepartment.Items[i].Value;
                        department.Title = cblDepartment.Items[i].Text;
                        selectedDepartmentList.Add(department);
                    }
                }
                ViewState["selected_department"] = selectedDepartmentList;
                rtParticipantsDepartment.DataSource = selectedDepartmentList;
                rtParticipantsDepartment.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void rtParticipantsDepartment_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                for (int i = 0; i < selectedDepartmentList.Count; i++)
                {
                    if (selectedDepartmentList[i].Id == Convert.ToString(e.CommandArgument))
                    {
                        selectedDepartmentList.RemoveAt(i);
                        break;
                    }
                }

                ViewState["selected_department"] = selectedDepartmentList;
                rtParticipantsDepartment.DataSource = selectedDepartmentList;
                rtParticipantsDepartment.DataBind();
                mpePop.Hide();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void lbAddParticipantsDepartment_Click(object sender, EventArgs e)
        {
            try
            {
                DepartmentListResponse response = asc.GetAllDepartment(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
                if (response.Success)
                {
                    List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                    cblDepartment.Items.Clear();
                    for (int i = 0; i < response.Departments.Count; i++)
                    {
                        cblDepartment.Items.Add(new ListItem(response.Departments[i].Title, response.Departments[i].Id));
                        for (int j = 0; j < selectedDepartmentList.Count; j++)
                        {
                            if (response.Departments[i].Id.Equals(selectedDepartmentList[j].Id))
                            {
                                cblDepartment.Items[i].Selected = true;
                            }
                        }
                    }

                    mpePop.PopupControlID = "plSelectDepartment";
                    mpePop.Show();
                }
                else
                {
                    Log.Error("DepartmentListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        protected void cbParticipantsPersonnel_CheckedChanged(object sender, EventArgs e)
        {
            if (cbParticipantsPersonnel.Checked)
            {
                cbParticipantsEveryone.Checked = false;
            }
        }

        protected void rtParticipantsPersonnel_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                List<User> selectedList = ViewState["selected_user"] as List<User>;
                if (selectedList == null || selectedList.Count == 0)
                {
                    Log.Error(@"ViewState[""SelectedUser""] is null.", this.Page);
                }
                else
                {
                    for (int i = 0; i < selectedList.Count; i++)
                    {
                        if (selectedList[i].UserId == Convert.ToString(e.CommandArgument))
                        {
                            selectedList.RemoveAt(i);
                            break;
                        }
                    }

                    ViewState["selected_user"] = selectedList;

                    rtParticipantsPersonnel.DataSource = selectedList;
                    rtParticipantsPersonnel.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void tbSearchKey_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GetSearchUser();
                rtSearchResult.DataSource = searchUserList;
                rtSearchResult.DataBind();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void cbParticipantsDepartment_CheckedChanged(object sender, EventArgs e)
        {
            if (cbParticipantsDepartment.Checked)
            {
                cbParticipantsEveryone.Checked = false;
            }
        }

        protected void cbParticipantsEveryone_CheckedChanged(object sender, EventArgs e)
        {
            if (cbParticipantsEveryone.Checked)
            {
                cbParticipantsDepartment.Checked = false;
                cbParticipantsPersonnel.Checked = false;
            }
        }

        protected void rtSearchResult_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("AddUser"))
                {
                    List<User> selectedList = ViewState["selected_user"] as List<User>;
                    String[] value = Convert.ToString(e.CommandArgument).Split(',');
                    User user = new CassandraService.Entity.User();
                    user.UserId = value[0];
                    user.FirstName = value[1];
                    user.LastName = value[2];
                    selectedList.Add(user);
                    rtParticipantsPersonnel.DataSource = selectedList;
                    rtParticipantsPersonnel.DataBind();
                    ViewState["SelectedUserId"] = selectedList;
                    tbSearchKey.Text = String.Empty;
                    GetSearchUser();
                    rtSearchResult.DataSource = searchUserList;
                    rtSearchResult.DataBind();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rtSearchResult_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetFocus", "SetFocus();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void rblDuration_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (rblDuration.SelectedIndex == 0)
                {
                    plEndDate.Visible = false;
                }
                else
                {
                    plEndDate.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        #region rtIcon event-handlers

        protected void rtIcon_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Choose"))
                {
                    imgTopic.ImageUrl = e.CommandArgument.ToString();
                    mpePop.Hide();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void rtIcon_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lbChooseIcon") as LinkButton;
                if (btn != null)
                {
                    scriptMan.RegisterAsyncPostBackControl(btn);
                }
            }
        }

        protected void rtIcon_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    System.Web.UI.WebControls.Image imgChooseIcon = e.Item.FindControl("imgChooseIcon") as System.Web.UI.WebControls.Image;
                    LinkButton lbChooseIcon = e.Item.FindControl("lbChooseIcon") as LinkButton;
                    imgChooseIcon.ImageUrl = iconUrlList[e.Item.ItemIndex];
                    lbChooseIcon.CommandArgument = iconUrlList[e.Item.ItemIndex];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        #endregion rtIcon event-handlers

        public void GetIconData()
        {
            TopicSelectIconResponse response = asc.SelectAllTopicIcons(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString());
            if (response.Success)
            {
                iconUrlList = response.TopicIconUrls;
            }
            else
            {
                Log.Error("TopicSelectIconResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        public void GetSearchUser()
        {
            try
            {
                if (String.IsNullOrEmpty(tbSearchKey.Text.Trim()))
                {
                    searchUserList = new List<User>();
                }
                else
                {
                    UserListResponse response = asc.SearchUser(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), tbSearchKey.Text.Trim());
                    if (response.Success)
                    {
                        List<User> selectedList = ViewState["selected_user"] as List<User>;
                        searchUserList = new List<User>();

                        for (int i = 0; i < selectedList.Count; i++)
                        {
                            User user = (from a in response.Users where a.UserId == selectedList[i].UserId select a).FirstOrDefault();
                            if (user != null)
                            {
                                response.Users.Remove(user);
                            }
                        }
                        searchUserList = response.Users;
                    }
                    else
                    {
                        Log.Error("SearchUserForPostingSuspend.Success is false. ErrorMessage: " + response.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        public void GetCategoryData()
        {
            MLEducationCategoryListResponse response = asc.SelectAllMLEducationCategories(ViewState["manager_user_id"].ToString(), ViewState["company_id"].ToString(), (int)MLCategoryQueryType.Basic);
            if (response.Success)
            {
                categoryList = response.Categories;
            }
            else
            {
                Log.Error("MLEducationCategoryListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        protected void lbEdit_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. 驗證資料
                #region Title
                if (String.IsNullOrEmpty(tbEducationTitle.Text.Trim()))
                {
                    ShowToastMsgAndHideProgressBar("You are missing the title.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                if (tbEducationTitle.Text.Length > MLEARNING_TITLE_TEXT_MAX)
                {
                    ShowToastMsgAndHideProgressBar("Title length can not be bigger than " + MLEARNING_TITLE_TEXT_MAX, MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Description
                // description is not compulsory. 2017/02/10
                //if (String.IsNullOrEmpty(tbDescription.Text.Trim()))
                //{
                //    ShowToastMsgAndHideProgressBar("You are missing the description.", MessageUtility.TOAST_TYPE_ERROR);
                //    return;
                //}

                if (tbDescription.Text.Length > MLEARNING_DESCRIPTION_TEXT_MAX)
                {
                    ShowToastMsgAndHideProgressBar("Description length can not be bigger than " + MLEARNING_DESCRIPTION_TEXT_MAX, MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region Category
                GetCategoryData();
                String categoryID = String.Empty;
                if (cbCategory.SelectedItem == null)
                {
                    ShowToastMsgAndHideProgressBar("Please at least choose a category.", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }

                String categoryTitle = cbCategory.SelectedItem.Text;
                for (int i = 0; i < categoryList.Count; i++)
                {
                    if (categoryList[i].Title.ToLower().Equals(cbCategory.SelectedItem.Text.Trim().ToLower()))
                    {
                        categoryID = categoryList[i].CategoryId;
                        break;
                    }
                }
                #endregion

                #region  Department of Participants
                List<String> targetedDepartmentIds = new List<String>();
                if (cbParticipantsDepartment.Checked)
                {
                    List<Department> selectedDepartmentList = (List<Department>)ViewState["selected_department"];
                    if (selectedDepartmentList.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please at least choose a department.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedDepartmentList.Count; i++)
                        {
                            targetedDepartmentIds.Add(selectedDepartmentList[i].Id);
                        }
                    }
                }
                #endregion

                #region Personnel of Participants
                List<String> targetedUserIds = new List<String>();
                if (cbParticipantsPersonnel.Checked)
                {
                    List<User> selectedUserList = (List<User>)ViewState["selected_user"];
                    if (selectedUserList.Count == 0)
                    {
                        ShowToastMsgAndHideProgressBar("Please at least choose a user.", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                    else
                    {
                        for (int i = 0; i < selectedUserList.Count; i++)
                        {
                            targetedUserIds.Add(selectedUserList[i].UserId);
                        }
                    }
                }
                #endregion

                #region Start Date
                DateTime dtStart = new DateTime();
                try
                {
                    DateTime dtTemp = DateTime.ParseExact(tbStartDate.Text + " " + tbStartHour.Text.PadLeft(2, '0') + ":" + tbStartMinute.Text.PadLeft(2, '0') + " " + ddlStartMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture);
                    dtStart = new DateTime(dtTemp.Year, dtTemp.Month, dtTemp.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second, DateTimeKind.Utc).AddHours(-managerInfo.TimeZone);
                }
                catch (Exception ex)
                {
                    ShowToastMsgAndHideProgressBar("Start date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                    return;
                }
                #endregion

                #region End Date
                DateTime? dtEnd = null;
                if (rblDuration.SelectedIndex != 0)
                {
                    try
                    {
                        DateTime dtTemp = DateTime.ParseExact(tbEndDate.Text + " " + tbEndHour.Text.PadLeft(2, '0') + ":" + tbEndMinute.Text.PadLeft(2, '0') + " " + ddlEndMer.SelectedValue, "dd/MM/yyyy hh:mm ttt", CultureInfo.InvariantCulture);
                        dtEnd = new DateTime(dtTemp.Year, dtTemp.Month, dtTemp.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second, DateTimeKind.Utc).AddHours(-managerInfo.TimeZone);
                        if (dtEnd < DateTime.UtcNow)
                        {
                            ShowToastMsgAndHideProgressBar("End date should not be early now", MessageUtility.TOAST_TYPE_ERROR);
                            return;
                        }

                        if (dtStart > dtEnd)
                        {
                            ShowToastMsgAndHideProgressBar("Your start and end dates does not tally", MessageUtility.TOAST_TYPE_ERROR);
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        ShowToastMsgAndHideProgressBar("End date is invalid", MessageUtility.TOAST_TYPE_ERROR);
                        return;
                    }
                }
                #endregion

                #endregion

                #region Step 2. Call API
                MLEducationUpdateResponse response = asc.UpdateMLEducation(
                    hfEducationId.Value,
                    ViewState["manager_user_id"].ToString(),
                    ViewState["company_id"].ToString(),
                    tbEducationTitle.Text.Trim(),
                    tbDescription.Text.Trim(),
                    imgTopic.ImageUrl,
                    categoryID,
                    categoryTitle,
                    Convert.ToInt16(rblDuration.SelectedValue),
                    Convert.ToInt16(ddlStatus.SelectedValue),
                    targetedDepartmentIds,
                    targetedUserIds,
                    dtStart,
                    dtEnd,
                    (rbShowAnswerTypeHide.Checked) ? (int)DisplayAnswerTypeEnum.DoNotShowAnswer : (int)DisplayAnswerTypeEnum.ShowAnswer,
                    cbAuthor.Checked,
                    chkAutoBreak.Checked
                    );

                if (response.Success)
                {
                    ShowToastMsgAndHideProgressBar("Education have been edited.", MessageUtility.TOAST_TYPE_INFO);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "RedirectPage", "RedirectPage('/MLearning/EducationEdit/" + response.Education.EducationId + "/" + response.Education.Category.CategoryId + "',300);", true);
                }
                else
                {
                    Log.Error("MLEducationUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    if (response.ErrorCode == Convert.ToInt32(CassandraService.GlobalResources.ErrorCode.MLEducationCardCountMustMoreThenZero))
                    {
                        ShowToastMsgAndHideProgressBar("Active Education needs at least one card", MessageUtility.TOAST_TYPE_ERROR);
                    }
                    else
                    {
                        ShowToastMsgAndHideProgressBar("Education have not been edited.", MessageUtility.TOAST_TYPE_ERROR);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }
        }

        private void ShowToastMsgAndHideProgressBar(String toastMsg, int toastMsgType)
        {
            MessageUtility.ShowToast(this.Page, toastMsg, toastMsgType);
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }
    }
}