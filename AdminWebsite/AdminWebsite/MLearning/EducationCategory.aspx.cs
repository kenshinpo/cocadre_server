﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CassandraService.Entity.MLTopicCategory;

namespace AdminWebsite.MLearning
{
    public partial class EducationCategory : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!Page.IsPostBack)
                {
                    ViewState["currentSortField"] = "SortByTitle";
                    ViewState["currentSortDirection"] = "asc";
                    RefreshCategoryListView();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void lbPopCancel_Click(object sender, EventArgs e)
        {
            mpePop.Hide();
        }

        protected void lbAddCategory_Click(object sender, EventArgs e)
        {
            try
            {
                tbAddName.Text = String.Empty;
                mpePop.PopupControlID = "popup_addcategory";
                mpePop.Show();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAddDone_Click(object sender, EventArgs e)
        {
            #region Step 1. User input validation
            if (string.IsNullOrWhiteSpace(tbAddName.Text.Trim()))
            {
                mpePop.Show();
                MessageUtility.ShowToast(this.Page, "Please enter your title of category", MessageUtility.TOAST_TYPE_ERROR);
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                return;
            }
            #endregion

            #region Step 2. Call API
            try
            {
                MLEducationCategoryCreateResponse response = asc.CreateMLEducationCategory(adminInfo.CompanyId, tbAddName.Text.Trim(), adminInfo.UserId);

                if (response.Success)
                {
                    mpePop.Hide();
                    RefreshCategoryListView();
                    MessageUtility.ShowToast(this.Page, tbAddName.Text.Trim() + " has been created.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                }

                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);

            }
            #endregion
        }

        public void RefreshCategoryListView()
        {
            try
            {
                #region Step 1. Get Data
                MLEducationCategoryListResponse response = asc.SelectAllMLEducationCategories(adminInfo.UserId, adminInfo.CompanyId, (int)MLCategoryQueryType.FullDetail);
                #endregion

                #region Step 2. Data binding
                if (response.Success)
                {
                    if (ViewState["currentSortField"].ToString().Equals("SortByTitle"))
                    {
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.lvCategory.DataSource = response.Categories.OrderBy(r => r.Title).ToList();
                        }
                        else
                        {
                            this.lvCategory.DataSource = response.Categories.OrderByDescending(r => r.Title).ToList();
                        }
                    }
                    else if (ViewState["currentSortField"].ToString().Equals("SortByCount"))
                    {
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.lvCategory.DataSource = response.Categories.OrderBy(r => r.NumberOfMLEducation).ToList();
                        }
                        else
                        {
                            this.lvCategory.DataSource = response.Categories.OrderByDescending(r => r.NumberOfMLEducation).ToList();
                        }
                    }
                    else
                    {
                        this.lvCategory.DataSource = response.Categories;
                    }

                    this.lvCategory.DataBind();
                }
                else
                {
                    Log.Error("MLEducationCategoryListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvCategory_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                this.hfCategoryId.Value = e.CommandArgument.ToString();

                LinkButton lbCategoryTitle = e.Item.FindControl("lbCategoryTitle") as LinkButton;
                if (e.CommandName.Equals("Rename"))
                {
                    tbRenameName.Text = lbCategoryTitle.Text;
                    lbRenameSave.CommandArgument = e.CommandArgument.ToString();
                    mpePop.PopupControlID = "popup_renamecategory";
                    mpePop.Show();
                }
                else if (e.CommandName.Equals("Remove"))
                {
                    HyperLink hlTopicCount = e.Item.FindControl("hlCount") as HyperLink;
                    if (Convert.ToInt16(hlTopicCount.Text) > 0)
                    {
                        ltlDeleteMsg.Text = @"<p class='error'>There are still education(s) left in the category!<br /> For satety reasons we can only allow you to delete empty category.</p><p><img src='/img/tips_icon.png' width='16px' height='20px' /> You can move the education to other category in the <a href='/MLearning/EducationList/" + e.CommandArgument.ToString() + "'>M Learning console</a> manager.</p>";
                        lbDelete.Visible = false;
                        lbDelCancel.Text = "Exit";
                    }
                    else
                    {
                        ltlDeleteMsg.Text = "<p>Are you sure you want to delete this category : <b>" + lbCategoryTitle.Text + "</b> ?</p>";
                        lbDelete.CommandArgument = e.CommandArgument.ToString();
                        lbDelete.Visible = true;
                        lbDelCancel.Text = "Cancel";
                        hfCategoryName.Value = lbCategoryTitle.Text;
                    }

                    mpePop.PopupControlID = "popup_deletecategory";
                    mpePop.Show();
                }
                else
                {
                    // do nothing
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbRenameSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region Step 1. User input validation
                if (string.IsNullOrWhiteSpace(tbRenameName.Text.Trim()))
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, "Please enter your title of category", MessageUtility.TOAST_TYPE_ERROR);
                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                    return;
                }
                #endregion

                #region Step 2. Call API
                try
                {
                    MLEducationCategoryUpdaateResponse response = asc.UpdateMLEducationCategory(adminInfo.UserId, adminInfo.CompanyId, lbRenameSave.CommandArgument, tbRenameName.Text.Trim());

                    if (response.Success)
                    {
                        mpePop.Hide();
                        RefreshCategoryListView();
                        MessageUtility.ShowToast(this.Page, tbAddName.Text.Trim() + " has been renamed.", MessageUtility.TOAST_TYPE_INFO);
                    }
                    else
                    {
                        mpePop.Show();
                        MessageUtility.ShowToast(this.Page, response.ErrorMessage, MessageUtility.TOAST_TYPE_ERROR);
                    }

                    ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.ToString(), ex);

                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbDelete_Click(object sender, EventArgs e)
        {
            try
            {
                MLEducationCategoryDeleteResponse response = asc.DeleteMLEducationCategory(adminInfo.UserId, adminInfo.CompanyId, lbDelete.CommandArgument);

                if (response.Success)
                {
                    mpePop.Hide();
                    RefreshCategoryListView();
                    MessageUtility.ShowToast(this.Page, hfCategoryName.Value + " has been deleted.", MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, "Delete category failed.", MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);

            }
        }

        protected void lvCategory_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            RefreshCategoryListView();
        }

        protected void lvCategory_DataBound(object sender, EventArgs e)
        {
            try
            {
                ListView lv = sender as ListView;
                LinkButton headerLinkButton = null;
                string[] linkButtonControlList = new[] { "SortByTitle", "SortByCount" };

                if (lv == null)
                    return;

                // Remove the up-down arrows from each header
                foreach (string lbControlId in linkButtonControlList)
                {
                    headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                    if (headerLinkButton != null)
                    {
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                    }
                }

                // Add sort direction back to the field in question
                headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                if (headerLinkButton != null)
                {
                    if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                    }
                    else
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}