﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdminWebsite.MLearning
{
    public partial class AssignmentList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo managerInfo = null;
        private List<MLAssignment> assignments = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.managerInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    ViewState["currentSortField"] = "SortByEducation";
                    ViewState["currentSortDirection"] = "asc";


                    RefreshListView();
                }

                tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RefreshListView()
        {
            GetAllAssignments();
            if (assignments != null)
            {
                switch (ViewState["currentSortField"].ToString())
                {
                    case "SortByProgress":

                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = assignments.OrderBy(a => a.Progress).ToList();
                        }
                        else
                        {
                            lvList.DataSource = assignments.OrderByDescending(a => a.Progress).ToList();
                        }
                        break;

                    case "SortByEducation":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = assignments.OrderBy(a => a.Education.Title).ToList();
                        }
                        else
                        {
                            lvList.DataSource = assignments.OrderByDescending(a => a.Education.Title).ToList();
                        }
                        break;

                    case "SortByStartDate":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = assignments.OrderBy(a => a.StartDate).ToList();
                        }
                        else
                        {
                            lvList.DataSource = assignments.OrderByDescending(a => a.StartDate).ToList();
                        }
                        break;

                    case "SortByEndDate":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvList.DataSource = assignments.OrderBy(a => a.EndDate).ToList();
                        }
                        else
                        {
                            lvList.DataSource = assignments.OrderByDescending(a => a.EndDate).ToList();
                        }
                        break;



                    default:
                        lvList.DataSource = assignments;
                        break;
                }

                lvList.DataBind();
                if (assignments != null && assignments.Count > 1)
                {
                    ltlCount.Text = assignments.Count + " assignments";
                }
                else if (assignments != null && assignments.Count == 1)
                {
                    ltlCount.Text = "1 assignment";
                }
                else
                {
                    ltlCount.Text = "0 assignment";
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        public void GetAllAssignments()
        {
            MLAssignmentListResponse response = asc.SelectAllMLAssignments(managerInfo.UserId, managerInfo.CompanyId, Convert.ToInt16(ddlFilterProgress.SelectedValue), tbFilterKeyWord.Text.Trim().ToLower());
            if (response.Success)
            {
                assignments = response.Assignments;
            }
            else
            {
                Log.Error("MLAssignmentListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        protected void lvList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.DataItem)
                {
                    #region Sort 
                    ListView lv = sender as ListView;
                    LinkButton headerLinkButton = null;
                    string[] linkButtonControlList = new[] { "SortByProgress", "SortByEducation", "SortByStartDate", "SortByEndDate" };

                    if (lv == null)
                        return;

                    // Remove the up-down arrows from each header
                    foreach (string lbControlId in linkButtonControlList)
                    {
                        headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                        if (headerLinkButton != null)
                        {
                            headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                            headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                        }
                    }

                    // Add sort direction back to the field
                    headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                    if (headerLinkButton != null)
                    {
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                        }
                        else
                        {
                            headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                        }
                    }
                    #endregion

                    #region Progress
                    LinkButton lbProgress = e.Item.FindControl("lbProgress") as LinkButton;
                    switch (((List<MLAssignment>)lvList.DataSource)[e.Item.DataItemIndex].Progress)
                    {
                        case (int)MLAssignment.AssignmentProgressType.Upcoming:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(254, 204, 0, 1)\"></i> Upcoming";

                            break;

                        case (int)MLAssignment.AssignmentProgressType.Live:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(148, 207, 0, 1)\"></i> Live";

                            break;

                        case (int)MLAssignment.AssignmentProgressType.Completed:
                            lbProgress.Text = "<i class=\"fa fa-circle\" style=\"color: rgba(178, 178, 178, 1)\"></i> Completed";

                            break;

                        default:
                            break;
                    }
                    #endregion

                    #region StartDate
                    LinkButton lbStartDate = e.Item.FindControl("lbStartDate") as LinkButton;
                    lbStartDate.Text = ((List<MLAssignment>)lvList.DataSource)[e.Item.DataItemIndex].StartDate.AddHours(managerInfo.TimeZone).ToString("dd/MM/yyyy hh:mm tt", CultureInfo.CreateSpecificCulture("en-US"));
                    #endregion

                    #region EndDate
                    LinkButton lbEndtDate = e.Item.FindControl("lbEndtDate") as LinkButton;
                    if (((List<MLAssignment>)lvList.DataSource)[e.Item.DataItemIndex].EndDate.HasValue)
                    {
                        lbEndtDate.Text = ((List<MLAssignment>)lvList.DataSource)[e.Item.DataItemIndex].EndDate.Value.AddHours(managerInfo.TimeZone).ToString("dd/MM/yyyy hh:mm tt", CultureInfo.CreateSpecificCulture("en-US"));
                    }
                    else
                    {
                        lbEndtDate.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    }
                    #endregion

                    #region Participants
                    LinkButton lbParticipants = e.Item.FindControl("lbParticipants") as LinkButton;
                    if (((List<MLAssignment>)lvList.DataSource)[e.Item.DataItemIndex].PrivacySetting.IsForEveryone)
                    {
                        lbParticipants.Text = "<i class='fa fa-globe' style='margin-right: 5px;'></i>Everyone";
                    }
                    else
                    {
                        if (((List<MLAssignment>)lvList.DataSource)[e.Item.DataItemIndex].PrivacySetting.IsForDepartment)
                        {
                            lbParticipants.Text += "<i class='fa fa-briefcase' style='margin-right: 5px;'></i>";
                        }

                        if (((List<MLAssignment>)lvList.DataSource)[e.Item.DataItemIndex].PrivacySetting.IsForUser)
                        {
                            lbParticipants.Text += "<i class='fa fa-user' style='margin-right: 5px;'></i>";
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvList_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.EmptyItem)
                {
                    Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                    if (ddlFilterProgress.SelectedIndex > 0 || !String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                    {
                        if (String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                        {
                            ltlEmptyMsg.Text = @"Filter """ + ddlFilterProgress.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                        }
                        else
                        {
                            ltlEmptyMsg.Text = @"Filter """ + tbFilterKeyWord.Text.Trim() + @""" + """ + ddlFilterProgress.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                        }
                    }
                    else
                    {
                        ltlEmptyMsg.Text = @"Welcome to M Learning Assignment Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding a Assignment by clicking on the green button.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvList_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            RefreshListView();
        }

        protected void lvList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/MLearning/AssignmentDetail/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Analytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    //Response.Redirect(string.Format("/MLearning/EducationAnalytic/{0}", e.CommandArgument), false);
                    //Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    Label lblEducationTitle = e.Item.FindControl("lblEducationTitle") as Label;
                    System.Web.UI.WebControls.Image imgEducationIcon = e.Item.FindControl("imgEducationIcon") as System.Web.UI.WebControls.Image;

                    hfAssignmentId.Value = e.CommandArgument.ToString();

                    if (e.CommandName.Equals("Del", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = "-1";
                        ltlActionName.Text = "Delete ";
                        lbAction.Text = "Delete";
                        ltlActionMsg.Text = "Confirm deletion of the assignment <b>" + lblEducationTitle.Text + "</b>";
                    }
                    else
                    {
                        // do nothing
                    }

                    //imgAction.ImageUrl = imgEducationIcon.ImageUrl;
                    //ltlActionTitle.Text = lblEducationTitle.Text;
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvList_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }

        protected void ddlFilterProgress_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshListView();
        }

        protected void ibFilterKeyWord_Click(object sender, ImageClickEventArgs e)
        {
            RefreshListView();
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                MLAssignmentDeleteResponse response =  asc.DeleteMLAssignement(managerInfo.UserId, managerInfo.CompanyId, hfAssignmentId.Value);
                String toastMsg = String.Empty; mpePop.Hide();

                if (response.Success)
                {   
                    RefreshListView();
                    toastMsg = "Assignment has been deleted.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("MLAssignmentDeleteResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    mpePop.Show();
                    toastMsg = "Failed to delete assignment, please check your internet connection.";
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}