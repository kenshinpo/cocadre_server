﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EvaluationCategory.aspx.cs" Inherits="AdminWebsite.MLearning.EvaluationCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">
    <script type="text/javascript">
        $(function () {
            initChips();
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            initChips();
        })
    </script>

    <asp:UpdatePanel ID="upPop" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hfCategoryId" runat="server" />
            <asp:HiddenField ID="hfCategoryName" runat="server" />

            <%--<asp:HiddenField ID="hfSurveyId" runat="server" />--%>


            <!-- Floating Action Button -->
            <ul id="menu" class="mfb-component--br mfb-zoomin" data-mfb-toggle="hover">
                <li class="mfb-component__wrap">
                    <asp:LinkButton ID="lbAddCategory" runat="server" CssClass="mfb-component__button--main" data-mfb-label="Add Category" OnClick="lbAddCategory_Click">
                    <i class="mfb-component__main-icon--resting fa fa-plus"></i>
                    <i class="mfb-component__main-icon--active fa fa-user-plus"></i>
                    </asp:LinkButton>
                </li>
            </ul>
            <!-- /Floating Action Button -->

            <!-- Active: Add Category -->
            <asp:Panel ID="popup_addcategory" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">Add a Category</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Category</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbAddName" runat="server" placeholder="Title of your category" MaxLength="20" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbAddDone" CssClass="popup__action__item popup__action__item--cta" runat="server" OnClientClick="ShowProgressBar();" Text="Done" OnClick="lbAddDone_Click" />
                    <asp:LinkButton ID="lbAddCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Active: Add Category -->

            <!-- Active: Rename Category -->
            <asp:Panel ID="popup_renamecategory" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">Rename Category</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <div class="main">
                                <div class="label">Category</div>
                                <div class="form__row">
                                    <asp:TextBox ID="tbRenameName" runat="server" placeholder="Title of your category" MaxLength="20" onkeydown="return (event.keyCode!=13);" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbRenameSave" CssClass="popup__action__item popup__action__item--cancel popup__action__item--cta" runat="server" OnClick="lbRenameSave_Click" OnClientClick="ShowProgressBar();" Text="Save" />
                    <asp:LinkButton ID="lbRenameCancel" CssClass="popup__action__item popup__action__item--cancel" runat="server" OnClick="lbPopCancel_Click" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Active: Rename Category -->

            <!-- Active: Delete Category -->
            <asp:Panel ID="popup_deletecategory" runat="server" CssClass="popup popup--deletecategory" Width="100%" Style="display: none;">
                <h1 class="popup__title">Delete Category</h1>
                <div class="popup__content">
                    <fieldset class="form">
                        <div class="container">
                            <asp:Literal ID="ltlDeleteMsg" runat="server" />
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbDelete" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" Text="Delete" OnClientClick="ShowProgressBar();" OnClick="lbDelete_Click" />
                    <asp:LinkButton ID="lbDelCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" OnClick="lbPopCancel_Click" />
                </div>
            </asp:Panel>
            <!-- /Active: Delete Category -->

            <!-- Active: Rename, Delete -->
            <asp:Panel ID="popup_action" runat="server" CssClass="popup" Width="100%" Style="display: none;">
                <h1 class="popup__title">
                    <asp:Literal ID="ltlActionName" runat="server" />Category
                </h1>
                <div class="popup__content popup__content--nominheight">
                    <fieldset class="form">
                        <div class="container">
                            <p>
                                <asp:Literal ID="ltlActionMsg" runat="server" />
                            </p>
                            <div class="Media">
                                <div class="Media-body">
                                    <asp:Literal ID="ltlActionSurveyTitle" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="popup__action">
                    <asp:LinkButton ID="lbActionConfirm" runat="server" CssClass="popup__action__item popup__action__item--cancel popup__action__item--confirm" OnClientClick="ShowProgressBar();" />
                    <asp:LinkButton ID="lbActionCancel" runat="server" CssClass="popup__action__item popup__action__item--cancel" Text="Cancel" />
                </div>
            </asp:Panel>
            <!-- /Active: Rename, Delete -->
            <ajaxToolkit:ModalPopupExtender ID="mpePop" runat="server"
                TargetControlID="lbPop"
                PopupControlID="popup_action"
                BackgroundCssClass="mfp-bg"
                DropShadow="false" />
            <asp:LinkButton ID="lbPop" runat="server" />

        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">M Learning <span>Evaluation Category</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="surveyCountLiteral" runat="server" />
        </div>
        <div class="appbar__action" style="width: auto;">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">

        <aside class="data__sidebar filter">
            <div class="data__sidebar__title" style="font-weight: 600;">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EvaluationList">Evaluation</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Evaluation Category</a>
                </li>
            </ul>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationList">Education</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationCategory">Education Category</a>
                </li>
                <%--<li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/AssignmentList">Assignment</a>
                </li>--%>
            </ul>
        </aside>

        <div class="data__content">
            <asp:UpdatePanel ID="upSurveyList" runat="server">
                <ContentTemplate>
                    <asp:ListView ID="lvCategory" runat="server" OnDataBound="lvCategory_DataBound" OnItemCommand="lvCategory_ItemCommand" OnSorting="lvCategory_Sorting">
                        <EmptyDataTemplate>
                            <br />
                            <br />
                            <p style="color: #999; text-align: center;">
                                <asp:Literal ID="ltlEmptyMsg" runat="server" />
                            </p>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <table runat="server" class="dataTable hover">
                                <thead>
                                    <tr runat="server">
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByTitle" ID="SortByTitle" OnClientClick="ShowProgressBar('Filtering');" Text="Category" />
                                        </th>
                                        <th>
                                            <asp:LinkButton runat="server" CommandName="sort" CommandArgument="SortByCount" ID="SortByCount" OnClientClick="ShowProgressBar('Filtering');" Text="Evaluation" />
                                        </th>
                                        <th class="no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="ItemPlaceholder" runat="server"></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr runat="server">
                                <td>
                                    <asp:LinkButton ID="lbCategoryTitle" Text='<%# DataBinder.Eval(Container.DataItem, "Title") %>' runat="server" CommandName="Rename" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>' Style="color: #999;" />
                                </td>
                                <td>
                                    <asp:HyperLink ID="hlCount" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NumberOfMLTopics") %>' NavigateUrl='<%# "/MLearning/EvaluationList/"+ DataBinder.Eval(Container.DataItem, "CategoryId")%>' />
                                </td>
                                <td class="no-sort">
                                    <div class="post__user__action">
                                        <div class="chips--02">
                                            <div class="chips--02__container">
                                                <i class="fa fa-ellipsis-v chips--02__button"></i>
                                                <ul class="chips--02__menu">
                                                    <li>
                                                        <asp:LinkButton ID="lbRename" ClientIDMode="AutoID" runat="server" CommandName="Rename" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>' Text="Rename Category" />
                                                    </li>
                                                    <li>
                                                        <asp:LinkButton ID="lbDelete" ClientIDMode="AutoID" runat="server" CommandName="Remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryId") %>' Text="Delete Category" />
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

