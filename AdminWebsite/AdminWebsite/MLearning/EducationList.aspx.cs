﻿using AdminWebsite.App_Code.Entity;
using AdminWebsite.App_Code.Utilities;
using CassandraService.Entity;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CassandraService.Entity.MLEducationCategory;

namespace AdminWebsite.MLearning
{
    public partial class EducationList : System.Web.UI.Page
    {
        private AdminService asc = new AdminService();
        private ManagerInfo adminInfo = null;
        private List<MLEducationCategory> categories = null;
        private List<MLEducation> educations = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin_info"] == null)
            {
                Response.Redirect("/Logout");
                return;
            }
            this.adminInfo = Session["admin_info"] as ManagerInfo;

            try
            {
                if (!IsPostBack)
                {
                    String categoryId = String.Empty;
                    if (Page.RouteData.Values["CategoryId"] != null)
                    {
                        categoryId = Page.RouteData.Values["CategoryId"].ToString();
                    }

                    ViewState["currentSortField"] = "SortByTitle";
                    ViewState["currentSortDirection"] = "asc";

                    #region Step 1. Get categories
                    GetAllCategories();
                    if (categories != null)
                    {
                        ddlFilterCategory.Items.Clear();
                        ddlFilterCategory.Items.Add(new ListItem("All", ""));

                        for (int i = 0; i < categories.Count; i++)
                        {
                            ddlFilterCategory.Items.Add(new ListItem(categories[i].Title, categories[i].CategoryId));
                        }
                        ddlFilterCategory.SelectedValue = categoryId;

                    }
                    #endregion

                    #region Step 2. Get M-Learnings
                    RefreshListView();
                    #endregion
                }

                tbFilterKeyWord.Attributes.Add("onkeydown", "if (event.keyCode==13){document.getElementById('" + ibFilterKeyWord.ClientID + "').focus();return true;}");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RefreshListView()
        {
            GetAllEducation();
            if (educations != null)
            {
                switch (ViewState["currentSortField"].ToString())
                {
                    case "SortByTitle":

                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvMLearing.DataSource = educations.OrderBy(r => r.Title).ToList();
                        }
                        else
                        {
                            lvMLearing.DataSource = educations.OrderByDescending(r => r.Title).ToList();
                        }
                        break;

                    case "SortByCategory":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvMLearing.DataSource = educations.OrderBy(r => r.Category.Title).ToList();
                        }
                        else
                        {
                            lvMLearing.DataSource = educations.OrderByDescending(r => r.Category.Title).ToList();
                        }
                        break;

                    case "SortByProgress":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvMLearing.DataSource = educations.OrderBy(r => r.ProgressStatus).ToList();
                        }
                        else
                        {
                            lvMLearing.DataSource = educations.OrderByDescending(r => r.ProgressStatus).ToList();
                        }
                        break;

                    case "SortByStatus":
                        if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                        {
                            lvMLearing.DataSource = educations.OrderBy(r => r.Status).ToList();
                        }
                        else
                        {
                            lvMLearing.DataSource = educations.OrderByDescending(r => r.Status).ToList();
                        }
                        break;

                    default:
                        lvMLearing.DataSource = educations;
                        break;
                }

                lvMLearing.DataBind();

                if (educations != null && educations.Count > 1)
                {
                    ltlCount.Text = educations.Count + " educations";
                }
                else if (educations != null && educations.Count == 1)
                {
                    ltlCount.Text = "1 education";
                }
                else
                {
                    ltlCount.Text = "0 education";
                }
            }

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
        }

        public void GetAllEducation()
        {
            MLEducationListResponse response = asc.SelectAllMLEducaation(adminInfo.UserId, adminInfo.CompanyId, ddlFilterCategory.SelectedValue, tbFilterKeyWord.Text.Trim());
            if (response.Success)
            {
                educations = response.Educations;
            }
            else
            {
                Log.Error("MLEducationListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
            }
        }

        public void GetAllCategories()
        {
            try
            {
                MLEducationCategoryListResponse response = asc.SelectAllMLEducationCategories(adminInfo.UserId, adminInfo.CompanyId, (int)MLCategoryQueryType.Basic);
                if (response.Success)
                {
                    categories = response.Categories;
                }
                else
                {
                    Log.Error("MLEducationCategoryListResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ddlFilterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshListView();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void ibFilterKeyWord_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                RefreshListView();
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lvMLearing_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListViewItemType.EmptyItem)
                {
                    Literal ltlEmptyMsg = e.Item.FindControl("ltlEmptyMsg") as Literal;
                    if (ddlFilterCategory.SelectedIndex > 0 || !String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                    {
                        if (String.IsNullOrEmpty(tbFilterKeyWord.Text.Trim()))
                        {
                            ltlEmptyMsg.Text = @"Filter """ + ddlFilterCategory.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                        }
                        else
                        {
                            ltlEmptyMsg.Text = @"Filter """ + tbFilterKeyWord.Text.Trim() + @""" + """ + ddlFilterCategory.SelectedItem.Text + @""" 's search found no result.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Try different filters.";
                        }
                    }
                    else
                    {
                        ltlEmptyMsg.Text = @"Welcome to M Learning Education Console.<br /><img src='/img/tips_icon.png' width='16' height='20' /> Start adding a Education by clicking on the green button.";
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }

        }

        protected void lvMLearing_Sorting(object sender, ListViewSortEventArgs e)
        {
            // if same sort field, just change sort direction
            if (ViewState["currentSortField"].ToString().Equals(e.SortExpression, StringComparison.InvariantCultureIgnoreCase))
            {
                if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    ViewState["currentSortDirection"] = "desc";
                else
                    ViewState["currentSortDirection"] = "asc";
            }
            else
            {
                ViewState["currentSortField"] = e.SortExpression;
                ViewState["currentSortDirection"] = "asc";
            }

            RefreshListView();
        }

        protected void lvMLearing_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            try
            {
                ListView lv = sender as ListView;
                LinkButton headerLinkButton = null;
                string[] linkButtonControlList = new[] { "SortByTitle", "SortByCategory", "SortByProgress", "SortByStatus", "SortByParticipants", "SortByAttendance" };

                if (lv == null)
                    return;

                // Remove the up-down arrows from each header
                foreach (string lbControlId in linkButtonControlList)
                {
                    headerLinkButton = lv.FindControl(lbControlId) as LinkButton;
                    if (headerLinkButton != null)
                    {
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-asc\"></i>", string.Empty);
                        headerLinkButton.Text = headerLinkButton.Text.Replace(" <i class=\"fa fa-sort-desc\"></i>", string.Empty);
                    }
                }

                // Add sort direction back to the field in question
                headerLinkButton = lv.FindControl(ViewState["currentSortField"].ToString()) as LinkButton;
                if (headerLinkButton != null)
                {
                    if (ViewState["currentSortDirection"].ToString().Equals("asc", StringComparison.InvariantCultureIgnoreCase))
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-asc\"></i>";
                    }
                    else
                    {
                        headerLinkButton.Text = headerLinkButton.Text + " <i class=\"fa fa-sort-desc\"></i>";
                    }
                }

                #region Progress
                LinkButton lbProgress = e.Item.FindControl("lbProgress") as LinkButton;
                if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].ProgressStatus == (int)MLEducation.ProgressStatusEnum.Upcoming)
                {
                    lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(254, 204, 0, 1)'></i> Upcoming";
                }
                else if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].ProgressStatus == (int)MLEducation.ProgressStatusEnum.Live)
                {
                    lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(148, 207, 0, 1)'></i> Live";
                }
                else if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].ProgressStatus == (int)MLEducation.ProgressStatusEnum.Completed)
                {
                    lbProgress.Text = "<i class='fa fa-circle' style='color:rgba(178, 178, 178, 1)'></i> Completed";
                }
                else
                {
                    // do nothing
                }
                #endregion
                
                #region Status
                LinkButton lbStatus = e.Item.FindControl("lbStatus") as LinkButton;
                if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].Status == (int)MLEducation.MLEducationStatusEnum.Unlisted)
                {
                    lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Unlisted";
                }
                else if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].Status == (int)MLEducation.MLEducationStatusEnum.Active)
                {
                    lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(0, 117, 254, 1)'></i> Active";
                }
                else if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].Status == (int)MLEducation.MLEducationStatusEnum.Hidden)
                {
                    lbStatus.Text = "<i class='fa fa-circle' style='color:rgba(254, 149, 0, 1)'></i> Hidden";
                }
                else
                {
                    // do nothing
                }
                #endregion

                #region Participants
                LinkButton lbParticipants = e.Item.FindControl("lbParticipants") as LinkButton;
                if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].IsForEveryone)
                {
                    lbParticipants.Text = "<i class='fa fa-globe' style='margin-right: 5px;'></i>Everyone";
                }
                else
                {
                    if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].IsForDepartment)
                    {
                        lbParticipants.Text += "<i class='fa fa-briefcase' style='margin-right: 5px;'></i>";
                    }

                    if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].IsForUser)
                    {
                        lbParticipants.Text += "<i class='fa fa-user' style='margin-right: 5px;'></i>";
                    }

                }
                #endregion

                #region Report
                if (((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].ProgressStatus == (int)MLEducation.ProgressStatusEnum.Upcoming || ((List<MLEducation>)lvMLearing.DataSource)[e.Item.DataItemIndex].Status == (int)MLEducation.MLEducationStatusEnum.Unlisted)
                {
                    LinkButton lbReport = e.Item.FindControl("lbReport") as LinkButton;
                    System.Web.UI.WebControls.Image imgReport = e.Item.FindControl("imgReport") as System.Web.UI.WebControls.Image;

                    lbReport.Enabled = false;
                    imgReport.ImageUrl = "/Img/icon_report_gray.png";
                }
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvMLearing_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Edit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/MLearning/EducationEdit/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else if (e.CommandName.Equals("Analytics", StringComparison.InvariantCultureIgnoreCase))
                {
                    Response.Redirect(string.Format("/MLearning/EducationAnalytic/{0}", e.CommandArgument), false);
                    Context.ApplicationInstance.CompleteRequest();
                }
                else
                {
                    LinkButton lbMLearningTitle = e.Item.FindControl("lbMLearningTitle") as LinkButton;
                    System.Web.UI.WebControls.Image imgMLearning = e.Item.FindControl("imgMLearning") as System.Web.UI.WebControls.Image;

                    hfEducationId.Value = e.CommandArgument.ToString().Split(',')[0];
                    hfCategoryId.Value = e.CommandArgument.ToString().Split(',')[1];
                    hfEducationTitle.Value = lbMLearningTitle.Text;

                    if (e.CommandName.Equals("Activate", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = "2";
                        ltlActionName.Text = "Activate ";
                        lbAction.Text = "Activate";
                        ltlActionMsg.Text = "Activate <b>" + lbMLearningTitle.Text + "</b>. Confirm?";

                    }
                    else if (e.CommandName.Equals("Hide", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = "3";
                        ltlActionName.Text = "Hide ";
                        lbAction.Text = "Hide";
                        ltlActionMsg.Text = "Hide <b>" + lbMLearningTitle.Text + "</b>. Confirm?";
                    }
                    else if (e.CommandName.Equals("Del", StringComparison.InvariantCultureIgnoreCase))
                    {
                        lbAction.CommandArgument = "-1";
                        ltlActionName.Text = "Delete ";
                        lbAction.Text = "Delete";
                        ltlActionMsg.Text = "All questions within <b>" + lbMLearningTitle.Text + "</b> will be gone. Confirm?";
                    }

                    imgAction.ImageUrl = imgMLearning.ImageUrl;
                    ltlActionTitle.Text = lbMLearningTitle.Text;
                    mpePop.Show();
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        protected void lbAction_Click(object sender, EventArgs e)
        {
            try
            {
                MLEducationUpdateResponse response = asc.UpdateMLEducationStatus(hfEducationId.Value, hfCategoryId.Value, adminInfo.UserId, adminInfo.CompanyId, Convert.ToInt16(lbAction.CommandArgument));
                String toastMsg = String.Empty;
                if (response.Success)
                {
                    mpePop.Hide();
                    RefreshListView();

                    if (Convert.ToInt16(lbAction.CommandArgument) == 2)
                    {
                        toastMsg = hfEducationTitle.Value + " has been set to active.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == 3)
                    {
                        toastMsg = hfEducationTitle.Value + " has been set to hidden.";
                    }
                    else if (Convert.ToInt16(lbAction.CommandArgument) == -1)
                    {
                        toastMsg = hfEducationTitle.Value + " has been deleted.";
                    }
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_INFO);
                }
                else
                {
                    Log.Error("MLEducationUpdateResponse.Success is false. ErrorMessage: " + response.ErrorMessage);
                    if (response.ErrorCode == Convert.ToInt32(CassandraService.GlobalResources.ErrorCode.MLEducationCardCountMustMoreThenZero))
                    {
                        toastMsg = "Active Education needs at least one card";
                    }
                    else
                    {
                        toastMsg = "Failed to change status, please check your internet connection.";
                    }
                    mpePop.Show();
                    MessageUtility.ShowToast(this.Page, toastMsg, MessageUtility.TOAST_TYPE_ERROR);
                }
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "HideProgressBar", "HideProgressBar();", true);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex, this.Page);
            }
        }

        protected void lvMLearing_ItemEditing(object sender, ListViewEditEventArgs e)
        {

        }

    }
}