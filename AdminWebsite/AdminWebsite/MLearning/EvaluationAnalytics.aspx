﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="EvaluationAnalytics.aspx.cs" Inherits="AdminWebsite.MLearning.EvaluationAnalytics" %>
<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/Css/ca-style.css" />
    <link href="/Css/SurveyAnalytic.css" rel="stylesheet" type="text/css" />
    <style>
        .header-padding {
            padding: 20px 30px;
        }

        .header-border {
            border-bottom: 1px solid #ccc;
        }

        .secondary-label {
            color: #ccc;
            margin: 0px;
        }

        #analytic-wrapper .data__content {
            background: #F5F5F5;
        }

        #ml-analytic {
            margin-top: 30px;
        }

        .filter-section {
            margin: 0 auto;
        }

            .filter-section .search-input {
                font-size: 18px;
                padding-left: 50px !important;
            }

            .filter-section .fa {
                font-size: 25px;
            }

        .invisible {
            visibility: hidden;
        }

        .mdl-selectfield {
            width: 220px !important;
        }

        .progress.stacked .progress-bar {
            border-top: 1px solid #cccccc;
            border-bottom: 1px solid #cccccc;
        }

            .progress.stacked .progress-bar:first-child {
                border-left: 1px solid #cccccc;
            }

            .progress.stacked .progress-bar:last-child {
                border-right: 1px solid #cccccc;
            }

        #saved-count {
            font-size: 65px;
            padding-left: 10px;
            vertical-align: middle;
        }

        .small-note {
            text-align: left;
            font-size: 10px;
            width: 65%;
            margin: 0 auto;
            line-height: 12px;
            padding-bottom: 13px;
            padding-top: 5px;
        }

        .dynatable-per-page {
            display: none;
        }

        .dynatable-pagination-links {
            float: none;
            text-align: center;
            padding: 20px;
        }

        .dynatable-active-page {
            background: #cccccc;
        }

        #doughnut-chart {
            vertical-align: middle;
        }

        .chart-container {
            display: inline-block;
            vertical-align: middle;
        }

        .doughnutchart-info {
            vertical-align: middle;
            display: inline-block;
            padding-left: 12px;
            padding-right: 12px;
            margin: 0px;
            padding: 0px;
            padding-left: 10px;
        }

        .type-content {
            padding: 10px;
        }

        .chart-title {
            font-weight: 700;
            font-size: 14px;
        }

        #chart-percentage {
            color: #95D000;
            position: absolute;
            top: 40px;
            left: 50px;
            width: 50px;
            height: 50px;
            text-align: center;
            vertical-align: middle;
            font-size: 26px;
            margin: 0px;
            line-height: 50px;
        }


        #chart-label {
            color: #95D000;
            position: absolute;
            top: 65px;
            left: 50px;
            width: 50px;
            height: 50px;
            text-align: center;
            vertical-align: middle;
            font-size: 15px;
            margin: 0px;
            line-height: 50px;
        }

        .progress-bar.color1 {
            background-color: #95D000;
        }

        .progress-bar.color2 {
            background-color: #FF3333;
        }

        .progress-bar.color3 {
            background-color: #D6D6D6;
        }

        .progress-bar.color4 {
            background-color: #FFFFFF;
        }

        .legend-icon > .legend-color3 {
            background: #ffffff;
            border: 1px solid #cccccc;
        }

        .big-progress-group {
            padding-bottom: 80px;
        }

        /*DoNothing = 0,
        Next = 1,
        Skip = 2,
        SaveToInbox = 3,
        Delete = 4,
        Submit = 5,
        Acknowledge = 6,
        PlayTopic = 7,
        StartEvent = 8,
        StartSurvey = 9,
        AcceptChallenge = 10,*/

        .color1 {
            color: #95D000;
        }

        .color2 {
            color: #FF3333;
        }

        .color3 {
            color: #4D4D4D;
        }

        .color4 {
            color: #4D4D4D;
        }

        .rounded-indicator.color1 {
            background: #69b2dd;
        }

        .rounded-indicator.color2 {
            background: #aad933;
        }

        .rounded-indicator.color3 {
            background: #cccccc;
        }

        .legend-icon > .legend-color1 {
            width: 10px;
            height: 10px;
            background: #95D000;
        }

        .legend-icon > .legend-color2 {
            width: 10px;
            height: 10px;
            background: #FF3333;
        }

        .legend-icon > .legend-color3 {
            width: 10px;
            height: 10px;
            background: #D6D6D6;
        }

        .legend-icon > .legend-color4 {
            width: 10px;
            height: 10px;
            border: 1px solid #BFBFBF;
            background: #FFFFFF;
        }

        .clickable-row {
            cursor: pointer;
            transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -webkit-transition: all 0.5s ease;
            -ms-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
        }

            .clickable-row:hover {
                background: #f1f1f1;
            }
            .topic-img {
            width: 50px;
            height: 50px;
            display: inline-block;
            vertical-align:middle;
            margin-right: 15px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfAdminUserId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTopicId" />
    <asp:HiddenField runat="server" ID="hfCategoryId" />

    <!-- App Bar -->
    <div id="st-trigger-effects" class="appbar">
        <a id="st-trigger-effects-button" class="appbar__hamburger js-menu-trigger sliding-panel-button" data-effect="st-effect-11"><i class="fa fa-bars"></i></a>
        <a href="/MLearning/EvaluationList">
            <div class="appbar__title" style="color: black;"><strong>M</strong> <span>Learning</span></div>
        </a>
        <div class="appbar__meta">
            <a id="second-link" style="color: black;"></a>
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div id="analytic-wrapper" class="data">

        <div class="data__content">

            <div id="ml-analytic" class="container" style="width: 100%;">

                <div style="width: 100%; margin: 0 auto;">
                    <!-- TITLE -->
                    <div class="title-section" style="width: 100%; margin: 0 auto;">
                        <div class="analytic-info" style="width: 100%;">
                            <h2 id="analytic-title">Topic Title</h2>
                            <div style="float: left; width: 10%;">
                                <p>Author:</p>
                                <p>Category:</p>
                                <p>Passing Grade:</p>
                            </div>
                            <div style="float: left; width: 70%;">
                                <p id="analytic-topic-author">Author name</p>
                                <p id="analytic-topic-category">Category title</p>
                                <p id="analytic-topic-pgrade">Passing Grade</p>
                            </div>
                            <div style="float: right; width: 10%;">
                                <p id="progress-status" class="status"><span class="rounded-indicator"></span>Completed</p>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div class="header-left">
                            <p class="align-left"><span id="start-date-string"></span></p>
                        </div>
                        <div class="header-right">
                            <p class="align-right"><span id="data-date-string"></span></p>
                        </div>
                        <div style="clear: both;"></div>
                    </div>

                    <div id="overview-tab" class="tabs tabs--styled">
                        <ul class="tabs__list">
                            <li class="tabs__list__item">
                                <a class="tabs__link" href="#ml-candidate">Candidates</a>
                            </li>
                            <li class="tabs__list__item">
                                <a class="tabs__link" href="#ml-question">Questions</a>
                            </li>
                        </ul>
                        <div class="tabs__panels">
                            <!-- CANDIDATE TAB -->
                            <div class="tabs__panels__item ml-candidate" id="ml-candidate">
                                <div class="filter-section text-center">
                                    <div id="ml-card-left" class="card card-7-col animated fadeInUp">
                                        <!-- DYNAMICALLY GENERATE CONTENT FOR THIS CARD -->
                                    </div>

                                    <div id="ml-card-right" class="card card-3-col animated fadeInUp">
                                        <!-- DYNAMICALLY GENERATE CONTENT FOR THIS CARD -->
                                    </div>
                                    <div style="width: 90%; margin-left: auto; margin-right: auto; clear: both; padding-top: 20px; padding-bottom: 20px;">
                                        <div class="option-filter" style="display: inline;">
                                            <label class="select-label">Filter</label>
                                            <div class="mdl-selectfield" style="width: 250px; float: left; margin-right: 20px; height: 40px;">
                                                <select name="candidate-filter" id="candidate-filter">
                                                    <option selected="selected" value="">All</option>
                                                    <option value="Completed">Completed</option>
                                                    <option value="Incomplete">Incomplete</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="search-filter">
                                            <div class="input-group" style="position: relative; width: 70%">
                                                <i class="fa fa-search"></i>
                                                <input id="hidden_name" class="search-input" type="text" placeholder="Search Responders" value="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="personnel-card" class="card full-row animated fadeInUp no-padding">
                                    <div class="card-header" style="padding: 20px 20px 0px 20px;">
                                        <h1 class="title">Responders list</h1>
                                    </div>
                                    <div class="table-content">
                                        <table id="ml-candidate-table" class="responder-table">
                                            <thead>
                                                <th data-dynatable-column="no" data-dynatable-sorts="no" data-dynatable-default-sort="true" style="width: 10%">No</th>
                                                <th data-dynatable-column="responder_name" data-dynatable-sorts="responder_name" style="width: 30%">Name</th>
                                                <th class="hidden" data-dynatable-column="hidden_name">Hidden name</th>
                                                <th data-dynatable-column="last_updated" data-dynatable-sorts="last_updated" style="width: 15%">Date</th>
                                                <th data-dynatable-column="department" data-dynatable-sorts="department" style="width: 25%">Department</th>
                                                <th data-dynatable-column="grade" data-dynatable-sorts="grade" style="width: 10%">Grade</th>
                                                <th data-dynatable-column="status" data-dynatable-sorts="status" style="width: 10%">Status</th>
                                                <th data-dynatable-column="attempt" data-dynatable-sorts="attempt" style="width: 10%">Attempt</th>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- Question TAB -->
                            <div class="tabs__panels__item ml-question" id="ml-question">
                                <div id="card-stat" class="card full-row animated fadeInUp no-padding" style="align-items: stretch; min-height: 250px; position: relative; display: none;">
                                    <div class="additional-header header-padding header-border hidden">
                                        <div class="source inline-block-top" style="width: 49%;">
                                            <label style="font-weight: 700;">Source:</label>
                                            <p>RP3: Do you prefer eating out or eating in?</p>
                                        </div>
                                        <div class="option inline-block-top" style="width: 49%;">
                                            <label style="font-weight: 700;">Option:</label>
                                            <p>I prefer eating in....</p>
                                        </div>
                                    </div>
                                    <div class="card-header header-padding header-border" style="padding-top: 20px;">
                                        <div class="inline-block-top" style="width: 19%; padding-right: 15px;">
                                            <h1 class="stat-title secondary-label" style="font-size: 50px;"></h1>
                                            <small class="stat-code secondary-label"></small>
                                        </div>
                                        <div class="inline-block-top" style="width: 49%;">
                                            <p class="question-label secondary-label">Question</p>
                                            <p class="question">Types of food you like?</p>
                                            <p class="desc-label secondary-label">Description</p>
                                            <p class="desc">Types of food you like?</p>
                                        </div>
                                        <%--                                        <div class="align-right inline-block-top" style="width: 29%;">
                                            <p class="filterBy" class="secondary-label">All Department</p>
                                        </div>--%>
                                    </div>
                                    <div class="iProgress" style="text-align: center; display: none; top: 50%; left: 50%;">
                                        <div class="spinner">
                                            <div class="rect1"></div>
                                            <div class="rect2"></div>
                                            <div class="rect3"></div>
                                            <div class="rect4"></div>
                                            <div class="rect5"></div>
                                        </div>
                                    </div>
                                    <div class="iMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px; top: 0px;"></div>
                                    <div class="card-content">
                                        <p class="error-msg">No information.</p>
                                        <div class="card-stat">
                                            <div class="question-type align-center inline-block-top" style="width: 15%;">
                                                <label style="color: #ddd; font-size: 16px; line-height: 16px;">Type</label>
                                                <div class="preview-type-image" style="width: 25px; height: 25px; margin: 0 auto;">
                                                    <img src="/Img/icon_text.png" />
                                                </div>
                                            </div>
                                            <div class="result-table inline-block-top" style="width: 70%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 90%; margin-left: auto; margin-right: auto; clear: both; padding-top: 20px; padding-bottom: 20px;">
                                    <div class="search-filter">
                                        <div class="input-group" style="position: relative; width: 100%;">
                                            <i class="fa fa-search" style="font-size: 18px;"></i>
                                            <input id="hidden_question_content" class="search-input" style="font-size: 18px;" type="text" placeholder="Search or Click on the other Questions below to display report" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div id="question-card" class="card full-row animated fadeInUp no-padding">
                                    <div class="table-content">
                                        <div class="card-header" style="padding: 20px;">
                                            <div class="filter-section">
                                                <div class="mdl-selectfield" style="width: 20%; float: left; margin-right: 20px; height: 40px;">
                                                    <select name="question-filter" id="question-filter">
                                                        <option value="1" selected="selected">All Questions Published</option>
                                                        <option value="2">Hardest Questions</option>
                                                        <option value="3">Easiest Questions</option>
                                                    </select>
                                                </div>
                                                <div class="table-legends">
                                                    <div class="attempt-legends" style="float: right;">
                                                        <label>Attempts</label>
                                                        <ul class="inline-ul">
                                                            <li>
                                                                <div class="indicator" style="background-color: #95D000"></div>
                                                                Correct</li>
                                                            <li>
                                                                <div class="indicator" style="background-color: #FF3333"></div>
                                                                Wrong</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <table id="ml-question-table" class="responder-table">
                                            <thead>
                                                <th data-dynatable-column="card_number" data-dynatable-sort="card_number" data-dynatable-default-sort="true" style="width: 8%">Card</th>
                                                <th data-dynatable-column="question_content" style="width: 40%">Question</th>
                                                <th class="hidden" data-dynatable-column="hidden_question_content">Hidden name</th>
                                                <th data-dynatable-column="question_type" data-dynatable-sorts="question_type" style="width: 5%">Type</th>
                                                <th data-dynatable-column="accum_attempt" data-dynatable-sorts="accum_attempt" style="width: 15%">Accumlated Attempts</th>
                                                <th class="hidden" data-dynatable-column="hidden_card_id">Card Id</th>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
        <script src="/Js/Chart.js"></script>
        <script>
            $(function () {
                var CompanyId = $('#main_content_hfCompanyId').val();
                var AdminUserId = $('#main_content_hfAdminUserId').val();
                var TopicId = $('#main_content_hfTopicId').val();
                var CategoryId = $('#main_content_hfCategoryId').val();

                var chart_passing_html = "";
                var chart_attendance_html = "";
                var legend_html = "";
                var progress_html = "";
                var table_html = "";

                //----------------- CANDIDATE -----------------
                function fetchCandidate(searchId) {
                    var responder_table = $('#ml-candidate-table');
                    responder_table.find("tbody").empty();

                    var responder_data = [];

                    $('#ml-analytic').addClass('hidden');
                    $('#ml-analytic').removeClass('animated fadeInUp');
                    ShowProgressBar();
                    $.ajax({
                        type: "POST",
                        url: "/MLEvaluation/AnalyticCandidate",
                        data: {
                            "CompanyId": CompanyId,
                            "ManagerId": AdminUserId,
                            "TopicId": TopicId,
                            "CategoryId": CategoryId,
                            "SearchId": searchId
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            if (res.Success) {
                                $('#ml-analytic').removeClass('hidden');
                                $('#ml-analytic').addClass('animated fadeInUp');

                                var topic = res.Topic;
                                var attendanceChart = res.CandidateResult.Attendance;
                                var passingChart = res.CandidateResult.PassingRate;
                                var personnelList = res.CandidateResult.Candidates;
                                var searchTerms = res.SearchTerms;

                                var titleHTML = topic.Title;

                                if (topic.IconUrl) {
                                    titleHTML = '<img src="' + topic.IconUrl + '" class="topic-img" />' + titleHTML;
                                }

                                $('#analytic-title').html(titleHTML);

                                $('#second-link').html(topic.Title);
                                $('#second-link').attr("href", "/MLearning/EvaluationEdit/" + TopicId + "/" + CategoryId);

                                // Set up filter info
                                setFilterOption(searchTerms, 'candidate-filter');

                                // Set up topic info
                                $('#analytic-topic-author').html(topic.Author.FirstName + " " + topic.Author.LastName);
                                $('#analytic-topic-category').html(topic.MLTopicCategory.Title);
                                $('#analytic-topic-pgrade').html(topic.PassingGrade);

                                //progressStatus
                                //Upcoming = 1,
                                //Live = 2,
                                //Completed = 3

                                if (topic.Status == 1) {
                                    $('#progress-status').html('<span class="rounded-indicator color1"></span> Upcoming');
                                } else if (topic.Status == 2) {
                                    $('#progress-status').html('<span class="rounded-indicator color2"></span> Live');
                                } else if (topic.Status == 3) {
                                    $('#progress-status').html('<span class="rounded-indicator color3"></span> Completed');
                                }

                                if (topic.EndDateString) {
                                    $('#start-date-string').html('Showing data from ' + topic.StartDateString + " - " + topic.EndDateString);
                                    $('#data-date-string').html('End date ' + topic.EndDateString);
                                } else {
                                    $('#start-date-string').html('Showing data since ' + topic.StartDateString);
                                    //$('#data-date-string').html('Showing data since ' + topic.StartDateString);
                                }

                                //Chart
                                //Bar = 1,
                                //Pie = 2,
                                //OneToOne = 3,
                                //Doughnut = 4,
                                //HorizontalHistogram = 5

                                // ATTENDANCE CHART
                                if (attendanceChart.Type == 1) {
                                    if (attendanceChart.Stats.length > 1) {
                                        $.each(attendanceChart.Stats, function (k, v) {
                                            legend_html += '<div class="legend-item"><div class="legend-icon"><div class="legend-color' + (k + 1) + '"></div></div>';
                                            legend_html += '<label>' + v.Name + '</label></div>';

                                            if (v.Percentage > 0) {
                                                // Overall bar
                                                progress_html += '<div class="progress-bar color' + (k + 1) + '"  style="width:' + v.Percentage + '%;">';

                                                // Label
                                                if (k == 0 && v.Percentage < 10) {
                                                    progress_html += '<span class="value" style="left:-5px;">' + v.Count + '</span>';
                                                    progress_html += '<span class="percentage color' + (k + 1) + '" style="width:' + v.Percentage + '%; left:-17px;">' + v.Percentage + '%</span></div>';
                                                } else {
                                                    progress_html += '<span class="value">' + v.Count + '</span>';
                                                    progress_html += '<span class="percentage color' + (k + 1) + '" style="width:' + v.Percentage + '%;">' + v.Percentage + '%</span></div>';
                                                }
                                            }

                                        });

                                        chart_attendance_html += '<div class="card-header">';
                                        chart_attendance_html += '<h1 class="title">' + attendanceChart.Title + '</h1>';
                                        chart_attendance_html += '<div class="legends">';
                                        chart_attendance_html += legend_html;
                                        chart_attendance_html += '</div></div>';
                                        chart_attendance_html += '<div class="type-content"><div class="progress stacked" style="padding-left: 17px;padding-right: 10px;">';
                                        chart_attendance_html += progress_html;
                                        chart_attendance_html += '</div>';
                                        chart_attendance_html += '<div>' + attendanceChart.Disclaimer + '</div>';

                                        $('#ml-card-left').empty();
                                        $('#ml-card-left').append(chart_attendance_html);
                                    } else {
                                        chart_attendance_html += '<div class="card-header">';
                                        chart_attendance_html += '<h1 class="title">' + attendanceChart.Title + '</h1>';
                                        chart_attendance_html += '</div>';
                                        chart_attendance_html += '<div class="type-content">';
                                        chart_attendance_html += ' <ul class="list-group big-progress-group" >';
                                        chart_attendance_html += ' <li class="list-group-item"> ';
                                        chart_attendance_html += '<span id="completed-percentage" class="list-percentage">330</span>';
                                        chart_attendance_html += '<div class="progress">';
                                        chart_attendance_html += '<div id="completed-bar" class="progress-bar color2" style="width:100%;"></div>';
                                        chart_attendance_html += '</div></li></ul>';
                                        chart_attendance_html += '</div>';
                                        $('#ml-card-left').empty();
                                        $('#ml-card-left').append(chart_attendance_html);
                                    }

                                    legend_html = "";
                                    progress_html = "";
                                    chart_attendance_html = "";
                                }

                                // PASSING CHART
                                if (passingChart.Type == 4) {
                                    chart_passing_html += '<div class="card-header">';
                                    chart_passing_html += '<h1 class="title">' + passingChart.Title + '</h1>';
                                    chart_passing_html += '</div>';
                                    //chart_passing_html += '<div class="legends" style="top:48px;">';
                                    //chart_passing_html += '<div class="legend-item">';
                                    //chart_passing_html += ' <div class="legend-icon"><div class="legend-color1" style="background:#FF8300;"></div></div>';
                                    //chart_passing_html += '<label>Viewed</label></div>';
                                    //chart_passing_html += '<div class="legend-item">';
                                    //chart_passing_html += ' <div class="legend-icon"><div class="legend-color1" style="background:#6e6e6e;"></div></div>';
                                    //chart_passing_html += '<label>Have not view</label></div></div>';
                                    chart_passing_html += '<div class="type-content">';
                                    chart_passing_html += ' <div class="chart-container" style="position:relative;">';
                                    chart_passing_html += ' <canvas id="doughnut-chart" width="150" height="150" ></canvas>';
                                    chart_passing_html += '<label id="chart-percentage">' + passingChart.Stats[0].Percentage + '%</label>';
                                    chart_passing_html += '<label id="chart-label">Pass</label>';
                                    chart_passing_html += '</div>';
                                    chart_passing_html += '<div class="doughnutchart-info">';
                                    //chart_passing_html += '<label class="chart-title">' + passingChart.Stats[0].Name + '</label>';
                                    chart_passing_html += '<p class="chart-data"><span>' + passingChart.MaxCount + '</span></p>';
                                    chart_passing_html += '<p class="chart-data" style="margin-top:-10px;">Active Participant</p>';
                                    chart_passing_html += '</div>';
                                    chart_passing_html += '</div>';
                                    chart_passing_html += '</div>';
                                    chart_passing_html += '<p class="note">' + passingChart.Disclaimer + '</p>';

                                    $('#ml-card-right').empty();
                                    $('#ml-card-right').append(chart_passing_html);

                                    var ctx = document.getElementById("doughnut-chart").getContext("2d");
                                    var basedCount = passingChart.MaxCount <= 0 ? 1 : passingChart.MaxCount - passingChart.Stats[0].Count
                                    var chart_data = [
                                        { value: passingChart.Stats[0].Count, color: "#95D000" },
                                        { value: basedCount, color: "#6e6e6e" }
                                    ];

                                    var doughnutChart = new Chart(ctx).Doughnut(chart_data, {
                                        percentageInnerCutout: 75,
                                        animationEasing: "linear",
                                        animateScale: false,
                                        animationSteps: 50,
                                        segmentShowStroke: false
                                    });

                                } else if (passingChart.Type == 3) {
                                    chart_passing_html += '<div class="card-header">';
                                    chart_passing_html += '<h1 class="title">' + passingChart.Title + '</h1>';
                                    chart_passing_html += '</div>';
                                    chart_passing_html += '<div class="type-content align-center">';
                                    chart_passing_html += '<h1 class="text-center" style="margin-bottom:0px;">';
                                    chart_passing_html += '<img src="/Img/type3-icon.png" style="vertical-align:middle;"/>';
                                    chart_passing_html += '<span id="saved-count" style="vertical-align:middle;">' + passingChart.Stats[0].Count + '</span>';
                                    chart_passing_html += '</h1>';
                                    chart_passing_html += '<p class="small-note">' + passingChart.Disclaimer + '</p>';
                                    chart_passing_html += '</div>';
                                    $('#ml-card-right').empty();
                                    $('#ml-card-right').append(chart_passing_html);
                                }

                                chart_passing_html = "";

                                //CANDIDATE TABLE
                                $.each(personnelList, function (k, v) {

                                    var nameHTML = "";
                                    nameHTML = v.Candidate.FirstName + " " + v.Candidate.LastName;
                                    if (v.Attempt.ResultCode == 1 || v.Attempt.ResultCode == -1) {
                                        nameHTML = '<a href="/MLearning/EvaluationAnalyticUser/' + TopicId + '/' + CategoryId + '/' + v.Candidate.UserId + '/" target="_blank">' + v.Candidate.FirstName + " " + v.Candidate.LastName + '</a>';
                                    }

                                    responder_data.push({
                                        "no": k + 1,
                                        "responder_name": nameHTML,
                                        "hidden_name": v.Candidate.FirstName + " " + v.Candidate.LastName,
                                        "last_updated": v.Attempt.CompletedDateString,
                                        "department": v.Candidate.Departments[0].Title,
                                        "grade": v.Attempt.Grade,
                                        "status": v.Attempt.ResultMessage,
                                        "attempt": v.Attempt.Attempt,
                                    })
                                });
                                

                                responder_table.dynatable({
                                    features: {
                                        paginate: true,
                                        search: false,
                                        sorting: true,
                                        recordCount: false,
                                        pushState: false
                                    },
                                    dataset: {
                                        records: responder_data,
                                        sorts: { 'no': 1 }
                                    },
                                    inputs: {
                                        queries: $('#hidden_name'),
                                    }
                                });

                                var dynatable = responder_table.data('dynatable');
                                if (typeof dynatable.records !== "undefined") {
                                    dynatable.records.updateFromJson({ records: responder_data });
                                    dynatable.records.init();
                                }
                                dynatable.paginationPerPage.set(15);
                                dynatable.process();

                                setTimeout(function () {
                                    responder_table.css('display', 'table');
                                    responder_table.addClass('animated fadeIn');
                                }, 100);
                            }
                            else { // ERROR RESPONSE
                                $('#ml-analytic').empty();
                                $('#ml-analytic').html('<p class="align-center">' + res.ErrorMessage + '</p>');
                            }

                            HideProgressBar();
                        }

                    });

                    responder_table.bind('dynatable:afterUpdate', function (e, dynatable) {
                        setTimeout(function () {
                            responder_table.css('display', 'table');
                            responder_table.addClass('animated fadeIn');
                        }, 100);
                    });

                    // CUSTOM TABLE SEARCH FILTER FUNCTION
                    responder_table.bind('dynatable:init', function (e, dynatable) {
                        dynatable.queries.functions['hidden_name'] = function (record, queryValue) {
                            return record.hidden_name.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                        };
                    });
                }

                //----------------- CANDIDATE -----------------

                // First load
                fetchCandidate('');

                //----------------- QUESTION -----------------
                function fetchQuestion(questionToggleType) {
                    var responder_table = $('#ml-question-table');
                    var responder_data = [];

                    ShowProgressBar();
                    $.ajax({
                        type: "POST",
                        url: "/MLEvaluation/AnalyticQuestion",
                        data: {
                            "CompanyId": CompanyId,
                            "ManagerId": AdminUserId,
                            "TopicId": TopicId,
                            "CategoryId": CategoryId,
                            "QuestionToggleType": questionToggleType,
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            if (res.Success) {
                                var topic = res.Topic;
                                var questionList = res.Topic.Cards;
                                var previewCard = res.PreviewCard;
                                var cardToggleType = res.CardToggleType;

                                //QUESTION CARD
                                var cardId = previewCard.CardId;
                                var options = previewCard.Options;

                                if (!$('.additional-header').hasClass('hidden')) {
                                    $('.additional-header').addClass('hidden');
                                }

                                // Set up filter info
                                setFilterOption(cardToggleType, 'question-filter');

                                $('#card-stat .stat-title').html('P' + previewCard.Paging + 'Q' + previewCard.Ordering);
                                //$('#card-stat .stat-code').html('Code ' + previewCard.CardId);
                                $('#card-stat .stat-code').html('');

                                if (previewCard.Type == 4)
                                {
                                    $('.question-label').html("Article");
                                    $('.question').html(previewCard.PagesContent[0].Title);
                                }
                                else if (previewCard.Type == 5)
                                {
                                    $('.question-label').html("Video");
                                }
                                else
                                {
                                    $('.question-label').html("Question");
                                    $('.question').html(previewCard.Content);
                                }

                                if (previewCard.Description && previewCard.Description != "") {
                                    $('.desc-label .desc').css('display', 'block');
                                    $('.desc').html(previewCard.Description);
                                } else {
                                    $('.desc').css('display', 'none');
                                    $('.desc-label').css('display', 'none');
                                }

                                if ((options && options.length > 0)) {
                                    $('#card-stat .error-msg').css('display', 'none');
                                    $('#card-stat .card-stat').css('display', 'block');
                                    generateStatResult(options, previewCard.Type, cardId);
                                } else {
                                    $('#card-stat .error-msg').css('display', 'block');
                                    $('#card-stat .card-stat').css('display', 'none');
                                }

                                //QUESTION TABLE
                                $.each(questionList, function (k, v) {
                                    var mlQuestion = questionList[k];
                                    var content = "";
                                    if (mlQuestion.Type == 4)
                                    {
                                        content = mlQuestion.PagesContent[0].Title;
                                    }
                                    else
                                    {
                                        content = mlQuestion.Content;
                                    }

                                    responder_data.push({
                                        "card_number": "P" + mlQuestion.Paging + "Q" + mlQuestion.Ordering,
                                        "question_content": content,
                                        "hidden_question_content": content,
                                        "question_type": '<div class="type-image" style="width: 25px; height: 25px; margin: 0 auto;">' + generateCardTypeIcon(mlQuestion.Type) + '</div>',
                                        "accum_attempt": '<ul class="list-group"><li class="list-group-item"><span id="completed-percentage" class="list-percentage" style="float:left;">' + mlQuestion.CorrectAttempt + '</span><div class="progress"><div id="completed-bar" class="progress-bar color1" style="width:' + ((mlQuestion.CorrectAttempt / (mlQuestion.CorrectAttempt + mlQuestion.IncorrectAttempt)) * 100) + '%;"></div></div></li><li class="list-group-item"><span id="incomplete-percentage" class="list-percentage" style="float:left;">' + mlQuestion.IncorrectAttempt + '</span><div class="progress"><div id="incompleted-bar" class="progress-bar color2" style="width:' + ((mlQuestion.IncorrectAttempt / (mlQuestion.CorrectAttempt + mlQuestion.IncorrectAttempt)) * 100) + '%"></div></div></li></ul>',
                                        "hidden_card_id": mlQuestion.CardId
                                    })
                                });

                                responder_table.dynatable({
                                    features: {
                                        paginate: true,
                                        search: false,
                                        sorting: true,
                                        recordCount: false
                                    },
                                    dataset: {
                                        records: responder_data
                                    },
                                    inputs: {
                                        queries: $('#hidden_question_content'),
                                        sorts: { 'card_number': -1 }
                                    }
                                });

                                var dynatable = responder_table.data('dynatable');
                                if (typeof dynatable.records !== "undefined") {
                                    dynatable.records.updateFromJson({ records: responder_data });
                                    dynatable.records.init();
                                }

                                responder_table.dynatable().bind("dynatable:afterUpdate", function (e, rows) {
                                    $(e.target).children("tbody").children().each(function (i) {
                                        $(this).addClass('clickable-row')
                                        $(this).click(function () {
                                            var cardId = $(this).children().last().html();
                                            fetchPreviewQuestion(cardId);
                                        });
                                    });
                                });

                                dynatable.paginationPerPage.set(15);
                                dynatable.process();

                                setTimeout(function () {
                                    responder_table.css('display', 'table');
                                    responder_table.addClass('animated fadeIn');
                                }, 100);
                            }
                            else { // ERROR RESPONSE
                                $('#ml-analytic').empty();
                                $('#ml-analytic').html('<p class="align-center">' + res.ErrorMessage + '</p>');
                            }

                            $("#card-stat").css("display", "block");

                            HideProgressBar();
                        }

                    });

                    responder_table.bind('dynatable:afterUpdate', function (e, dynatable) {
                        setTimeout(function () {
                            responder_table.css('display', 'table');
                            responder_table.addClass('animated fadeIn');
                        }, 100);
                    });

                    // CUSTOM TABLE SEARCH FILTER FUNCTION
                    responder_table.bind('dynatable:init', function (e, dynatable) {
                        dynatable.queries.functions['hidden_question_content'] = function (record, queryValue) {
                            return record.hidden_question_content.toLowerCase().indexOf(queryValue.toLowerCase()) > -1;
                        };
                    });
                }

                function fetchPreviewQuestion(cardId) {
                    ShowInternalProgress("#card-stat");
                    $.ajax({
                        type: "POST",
                        url: "/MLEvaluation/AnalyticPreviewQuestion",
                        data: {
                            "CompanyId": CompanyId,
                            "ManagerId": AdminUserId,
                            "TopicId": TopicId,
                            "CategoryId": CategoryId,
                            "CardId": cardId,
                        },
                        crossDomain: true,
                        dataType: 'json',
                        success: function (res) {
                            if (res.Success) {
                                var previewCard = res.PreviewCard;
                                //QUESTION CARD
                                var cardId = previewCard.CardId;
                                var options = previewCard.Options;

                                if (!$('.additional-header').hasClass('hidden')) {
                                    $('.additional-header').addClass('hidden');
                                }

                                $('#card-stat .stat-title').html('P' + previewCard.Paging + 'Q' + previewCard.Ordering);
                                //$('#card-stat .stat-code').html('Code ' + previewCard.CardId);
                                $('#card-stat .stat-code').html('');

                                if (previewCard.Type == 4) {
                                    $('.question-label').html("Article");
                                    $('.question').html(previewCard.PagesContent[0].Title);
                                }
                                else if (previewCard.Type == 5) {
                                    $('.question-label').html("Video");
                                }
                                else {
                                    $('.question-label').html("Question");
                                    $('.question').html(previewCard.Content);
                                }

                                if (previewCard.Description && previewCard.Description != "") {
                                    $('.desc-label .desc').css('display', 'block');
                                    $('.desc').html(previewCard.Description);
                                } else {
                                    $('.desc-label .desc').css('display', 'none');
                                }

                                $('#card-stat .error-msg').css('display', 'none');
                                $('#card-stat .card-stat').css('display', 'block');
                                generateStatResult(options, previewCard.Type, cardId);
                            }
                            else { // ERROR RESPONSE
                                $('#ml-analytic').empty();
                                $('#ml-analytic').html('<p class="align-center">' + res.ErrorMessage + '</p>');
                            }

                            HideInternalProgress("#card-stat");
                        }

                    });
                }

                function generateStatResult(options, cardType, cardId) {

                    $('.preview-type-image').html(generateCardTypeIcon(cardType));

                    result_list_html = "";

                    result_list_html += '<ul class="list-group type' + cardType + ' style="width:85%;">';

                    if (cardType != 3) {
                        // For now there is only Select 1, Multi-choice and Instruction 
                        $.each(options, function (key, value) {
                            var correctColor = '#95D000';
                            if (parseInt(value.Score, 0) <= 0) {
                                correctColor = '#FF3333';
                            }

                            result_list_html += '<li class="list-group-item">';
                            result_list_html += '<span class="list-value" style="float: none; position: absolute; right: 47px;">' + value.NumberOfSelection + '</span>';
                            result_list_html += '<span class="list-percentage" style="color:' + correctColor + '">' + value.PercentageOfSelection + '%</span>';
                            result_list_html += '<p>' + value.Content + '</p>';
                            result_list_html += '<div class="progress">';
                            result_list_html += '<div class="progress-bar" style="background-color:' + correctColor + '; width: ' + value.PercentageOfSelection + '%;"></div>';
                            result_list_html += '</div>';
                            //result_list_html += '<a href="/DynamicPulse/RespondersReportList/' + deckId + '/' + cardId + '/' + value.OptionId + '" class="viewResponder_link" target="_blank" data-optionid="' + value.OptionId + '" style="position:absolute; top:10px; width:20px; right:-50px;">';
                            //result_list_html += '<img src="/Img/icon_result.png">';
                            //result_list_html += '</a>';
                            result_list_html += '</li>';
                        });

                    }

                    result_list_html += '</ul>';

                    $('#card-stat .result-table').empty();
                    $('#card-stat .result-table').html(result_list_html);
                }

                function generateCardTypeIcon(cardType) {
                    var html = "";

                    if (cardType == 1) {
                        html += '<img src="/Img/icon_selectone.png" style="margin:0; max-width:100%; width:20px; height:20px;"/>';
                    } else if (cardType == 2) {
                        html += '<img src="/Img/icon_multichoice.png" style="margin:0; max-width:100%; width:20px; height:20px;"/>';
                    } else if (cardType == 3) {
                        html += '<img src="/Img/icon_instructions.png" style="margin:0; max-width:100%; width:20px; height:20px;"/>';
                    } else if (cardType == 4) {
                        html += '<img src="/Img/icon_article.png" style="margin:0; max-width:100%; width:18px; height:21px;"/>';
                    } else if (cardType == 5) {
                        html += '<img src="/Img/icon_video.png" style="margin:0; max-width:100%; width:20px; height:20px;"/>';
                    } else if (cardType == 6) {
                        html += '<img src="/Img/icon-slides.png" style="margin:0; max-width:100%; width:20px; height:20px;"/>';
                    }

                    return html;
                }

                //----------------- QUESTION -----------------

                //----------------- GENERAL FUNCTIONS -----------------

                function setFilterOption(searchTerms, id) {
                    var option = "";
                    $.each(searchTerms, function (key, value) {
                        option += '<option value="' + value.Id + '" data-name="' + value.Content + '">' + truncateStr(value.Content, 25, '...') + '</option>';
                    });
                    $('select[name=' + id + ']').empty();
                    $('select[name=' + id + ']').append(option);
                }

                function truncateStr(str, length, ending) {
                    if (length == null) {
                        length = 100;
                    }
                    if (ending == null) {
                        ending = '...';
                    }
                    if (str.length > length) {
                        return str.substring(0, length - ending.length) + ending;
                    } else {
                        return str;
                    }
                }

                $('select[name="candidate-filter"]').on('change', function () {
                    var selected_filter = $(this).find(':selected').data('name');
                    searchId = $(this).val();

                    //$('.filterBy').html(truncateStr(selected_filter, 25, '...'));
                    fetchCandidate(searchId);
                });

                $('select[name="question-filter"]').on('change', function () {
                    var selected_filter = $(this).find(':selected').data('name');
                    questionToggleType = $(this).val();

                    //$('.filterBy').html(truncateStr(selected_filter, 25, '...'));
                    fetchQuestion(questionToggleType);
                });

                //----------------- GENERAL FUNCTIONS -----------------

                $('.tabs__list__item').on('click', function () {

                    // if clicked on candidate tab
                    if ($('#ml-candidate').is(':visible')) {
                        fetchCandidate('');
                    }

                    // if clicked on responders report tab
                    if ($('#ml-question').is(':visible')) {
                        fetchQuestion(1);
                    }
                });

                //----------------- GENERAL FUNCTIONS -----------------

                //----------------- LOADER -----------------
                function ShowInternalProgress(divId) {
                    var height = $(divId).height();
                    var width = $(divId).width();

                    var progress = $(divId).find('.iProgress');
                    var bg = $(divId).find('.iMaskFrame');
                    progress.css({ 'z-index': 999999, 'left': (width / 2) - ($('#iProgress').width() / 2), 'top': (height / 2) - ($('#iProgress').height() / 2), 'position': 'absolute', 'display': 'block' });
                    bg.css({ 'width': width, 'height': height, 'position': 'absolute', 'display': 'block', 'top': 0, 'z-index': 999998, 'opacity': 0.7 });
                }

                function HideInternalProgress(divId) {
                    var progress = $(divId).find('.iProgress');
                    var bg = $(divId).find('.iMaskFrame');
                    progress.css({ 'display': 'none' });
                    bg.css({ 'display': 'none' });
                }

            });
        </script>
    </div>
</asp:Content>

