﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Navi.Master" AutoEventWireup="true" CodeBehind="AssignmentDetail.aspx.cs" Inherits="AdminWebsite.MLearning.AssignmentDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="navihead" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
    <style>
        .ui-autocomplete-loading {
            background: white url("/img/ui-anim_basic_16x16.gif") right center no-repeat;
        }

        input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color: rgba(145, 145, 145, 1);
        }

        input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: rgba(145, 145, 145, 1);
            opacity: 1;
        }

        input:-ms-input-placeholder { /* Internet Explorer 10-11 */
            color: rgba(145, 145, 145, 1);
        }
    </style>

    <script src="/js/MLearning/AssignmentDetail.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main_content" runat="server">

    <asp:HiddenField runat="server" ID="hfManagerId" />
    <asp:HiddenField runat="server" ID="hfCompanyId" />
    <asp:HiddenField runat="server" ID="hfTimezone" />
    <asp:HiddenField runat="server" ID="hfAssignmentId" />

    <div id="mpe_backgroundElement" class="mfp-bg" style="position: fixed; left: 0px; top: 0px; z-index: 9000; width: 100%; height: 100%; display: none;"></div>

    <div id="dvSelectDepartment" class="popup" style="min-height: 50%; margin: 0px auto; z-index: 9001; bottom: 50%; right: 50%; display: none; object-fit: contain !important; transform: translate(50%, 51%); -o-transform: translate(50%, 51%); -webkit-transform: translate(50%, 51%); -moz-transform: translate(50%, 51%); max-width: 50%; max-height: 60%; position: absolute; overflow: auto;">
        <h1 class="popup__title">Select department</h1>
        <div class="popup__content" style="min-height: 150px;">
            <fieldset class="form">
                <div class="container">
                    <p style="font-weight: bold;">Available</p>
                    <div id="dvAvailableDepartments">
                    </div>
                    <hr />
                    <p style="font-weight: bold;">Unavailable</p>
                    <div id="dvUnavailableDepartments">
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="popup__action">
            <a class="popup__action__item popup__action__item--cta" style="color: rgba(254, 30, 38, 1);" href="javascript:setAssignmentValue(6, this, null);">SELECT</a>
            <a class="popup__action__item popup__action__item--cancel" href="javascript:hidePopup();">CANCEL</a>
        </div>
    </div>

    <!-- App Bar -->
    <div class="appbar">
        <div class="appbar__hamburger js-menu-trigger sliding-panel-button"><i class="fa fa-bars"></i></div>
        <div class="appbar__title">M Learning <span>Assignment</span></div>
        <div class="appbar__meta">
            <asp:Literal ID="ltlCount" runat="server" />
        </div>
        <div class="appbar__action">
            <a class="data-sidebar-toggle" onmouseover="this.style.cursor='pointer'"><i class="fa fa-filter"></i></a>
        </div>
    </div>
    <!-- /App Bar -->

    <div class="data">
        <aside class="data__sidebar filter">
            <div class="data__sidebar__title">Manage</div>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EvaluationList">Evaluation</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EvaluationCategory">Evaluation Category</a>
                </li>
            </ul>
            <ul class="data__sidebar__list">
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationList">Education</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link" href="/MLearning/EducationCategory">Education Category</a>
                </li>
                <li class="data__sidebar__list__item">
                    <a class="data__sidebar__link data__sidebar__link--active">Assignment</a>
                </li>
            </ul>
        </aside>
        <div class="data__content">

            <div style="padding: 20px 50px;">
                <label id="lblAction" style="font-size: 1.8em; color: rgba(93, 93, 93, 1);">Add Assignment</label>
                <div style="padding: 30px 10px; width: 80%;">
                    <div style="font-weight: 700; font-size: 1.15em;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;">Duration</label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle;">
                            <label style="display: inline-block; margin-right: 20px;">
                                <input type="radio" value="1" name="durationType" style="vertical-align: middle;" onclick="setAssignmentValue(1, this, null);" /><span style="vertical-align: middle;">Prerpetual</span>
                            </label>
                            <label style="display: inline-block;">
                                <input type="radio" value="2" name="durationType" style="vertical-align: middle;" onclick="setAssignmentValue(1, this, null);" /><span style="vertical-align: middle;">Schedule</span>
                            </label>
                        </div>
                    </div>
                    <div style="color: rgba(143, 143, 143, 1); margin-top: 20px;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;">Start date</label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle;">
                            <input type="text" id="startDate" style="width: 150px; display: inline-block; vertical-align: middle;" placeholder="dd/mm/yyyy" onchange="setAssignmentValue(2, this, null);" />
                            <label style="display: inline-block; vertical-align: middle;">at</label>
                            <input type="text" id="startDateHH" style="width: 40px; display: inline-block; vertical-align: middle;" placeholder="hh" onchange="setAssignmentValue(2, this, null);" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);"  />
                            <label style="display: inline-block; vertical-align: middle;">:</label>
                            <input type="text" id="startDateMM" style="width: 40px; display: inline-block; vertical-align: middle;" placeholder="mm" onchange="setAssignmentValue(2, this, null);" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                            <div class="mdl-selectfield" style="margin-left: 10px; width: 80px; display: inline-block; vertical-align: middle; padding-bottom: 0.75em;">
                                <select id="startDateMR" class="achievement_type" style="margin-bottom: 0px; color: #000;" onchange="setAssignmentValue(2, this, null);">
                                    <option value="am">am</option>
                                    <option value="pm">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="color: rgba(143, 143, 143, 1); margin-top: 20px; display: none;" id="dvEndDate">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;">End date</label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle;">
                            <input type="text" id="endDate" style="width: 150px; display: inline-block; vertical-align: middle;" placeholder="dd/mm/yyyy" onchange="setAssignmentValue(3, this, null);" />
                            <label style="display: inline-block; vertical-align: middle;">at</label>
                            <input type="text" id="endDateHH" style="width: 40px; display: inline-block; vertical-align: middle;" placeholder="hh" onchange="setAssignmentValue(3, this, null);" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                            <label style="display: inline-block; vertical-align: middle;">:</label>
                            <input type="text" id="endDateMM" style="width: 40px; display: inline-block; vertical-align: middle;" placeholder="mm" onchange="setAssignmentValue(3, this, null);" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" />
                            <div class="mdl-selectfield" style="margin-left: 10px; width: 80px; display: inline-block; vertical-align: middle; padding-bottom: 0.75em;">
                                <select id="endDateMR" class="achievement_type" style="margin-bottom: 0px; color: #000;" onchange="setAssignmentValue(3, this, null);">
                                    <option value="am">am</option>
                                    <option value="pm">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="color: rgba(143, 143, 143, 1); margin-top: 20px;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: top; margin-right: 20px; margin-top: 10px;">Education</label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle; width: 85%;">
                            <div id="dvCreateModeEducation">
                                <div class="education tags" style="color: #000;">
                                </div>
                                <input type="text" id="tbSearchEducation" placeholder="Education Title" style="width: 300px;" onkeypress="if( event.keyCode == 13 ) { return false; };" />
                            </div>
                            <div id="dvEditModeEducation" style="display: block;">
                                <img id="imgEducationIcon" style="width: 34px; vertical-align: middle;" />
                                <span id="spEducationTitle" style="margin-left: 10px;"></span>
                            </div>
                        </div>
                    </div>

                    <div style="color: rgba(143, 143, 143, 1); margin-top: 20px;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;">Participants</label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle;">
                            <div class="mdl-selectfield" style="width: 360px; display: inline-block; vertical-align: middle; padding-bottom: 0.75em;">
                                <select id="ddlParticipantsType" style="margin-bottom: 0px; color: #000;" onchange="setAssignmentValue(5, this, null);">
                                    <option value="1">Everyone except those who do not have rights access</option>
                                    <option value="2">Custom</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div id="dvTargetDapartments" style="color: rgba(143, 143, 143, 1); margin-top: 10px;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;"></label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle; width: 85%;">
                            <label style="color: rgba(104, 104, 104, 1); font-size: 1.1em;">
                                <input id="cbTargetDepartment" type="checkbox" onclick="setAssignmentValue(12, this, null);" />
                                <i class="fa fa-briefcase" style="color: rgb(153, 153, 153);"></i><span>&nbsp;Selected departments</span>
                            </label>
                            <div class="department tags" style="color: #000;">
                            </div>
                            <label id="lblAddDepartment" style="color: rgba(0, 118, 255, 1); margin-top: 10px; cursor: pointer;" onclick="showPopup(1);">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <span>Add more departments</span>
                            </label>
                            <div class="department error" style="color: red; visibility: hidden;">
                                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                <span>Department contains Personnel that has been blocked from using the selected Education</span>
                            </div>
                        </div>
                    </div>

                    <div id="dvTargetUsers" style="color: rgba(143, 143, 143, 1); margin-top: 20px;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;"></label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle; width: 85%;">
                            <label style="color: rgba(104, 104, 104, 1); font-size: 1.1em;">
                                <input id="cbTargetUser" type="checkbox" onclick="setAssignmentValue(13, this, null);" />
                                <i class="fa fa-user" style="color: rgb(153, 153, 153);"></i><span>&nbsp;Selected personnels</span>
                            </label>
                            <div class="user tags" style="color: #000;">
                            </div>
                            <input id="tbSearchUser" type="text" placeholder="User name" style="width: 300px;" onkeypress="if( event.keyCode == 13 ) { return false; };" />
                            <div class="user error" style="color: red; visibility: hidden;">
                                <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
                                <span>Personnel has been blocked from using the selected Education</span>
                            </div>
                        </div>
                    </div>

                    <div style="color: rgba(143, 143, 143, 1); margin-top: 20px;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: top; margin-right: 20px; margin-top: 15px;">Reminder</label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle;">
                            <div class="mdl-selectfield" style="width: 360px; display: inline-block; vertical-align: middle; padding-bottom: 0.75em;">
                                <select id="ddlReminder" style="margin-bottom: 0px; color: #000;" onchange="setAssignmentValue(10, this, null);">
                                    <option value="off">OFF</option>
                                    <option value="on">ON</option>
                                </select>
                            </div>
                            <br />
                            <label style="width: 360px;">Will only remind via notification during the scheduled period only</label>
                        </div>
                    </div>

                    <div id="dvFrequency" style="color: rgba(143, 143, 143, 1); margin-top: 20px; display: none;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;">Frequency</label>
                        <div style="display: inline-block; text-align: left; vertical-align: middle;">
                            <label style="display: inline-block; color: #000;">Once every</label>
                            <input type="number" id="tbFrequency" style="width: 50px; display: inline-block;" onkeypress="if( event.keyCode == 13 ) { return false; }; return allowOnlyNumber(event);" onblur="setAssignmentValue(11, this, null);" />
                            <label style="display: inline-block; color: #000;">days</label>
                        </div>
                    </div>

                    <div style="color: rgba(143, 143, 143, 1); margin-top: 20px;">
                        <label style="display: inline-block; width: 100px; text-align: right; vertical-align: middle; margin-right: 20px;"></label>
                        <div style="display: inline-block; text-align: right; vertical-align: middle; width: 370px;">
                            <label style="padding: 10px; color: #000; display: inline-block; font-weight: 700; cursor: pointer; margin-right: 20px;" onclick="RedirectPage('/MLearning/AssignmentList', 300);">CANCEL</label>
                            <label id="lblSave" style="padding: 10px 15px; display: inline-block; background-color: rgba(0, 118, 255, 1); color: #FFF; font-weight: 700; cursor: pointer; border-radius: 3px;" onclick="createAssignment();">PUBLISH</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
