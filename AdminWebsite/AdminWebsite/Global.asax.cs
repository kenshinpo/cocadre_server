﻿using System;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Routing;
using System.Web.UI;
using log4net.Config;
using System.Web.Http;

namespace AdminWebsite
{
    /// <summary>
    /// Class to configure WebApi routing
    /// </summary>
    public class WebApiConfig
    {
        public WebApiConfig()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void Register(System.Web.Http.HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = System.Web.Http.RouteParameter.Optional }
            );
        }
    }

    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);

            XmlConfigurator.ConfigureAndWatch(new FileInfo(Server.MapPath("~/log4net.config")));
            System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);

            string JQueryVer = "3.1.1";
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
            {
                Path = "~/Scripts/jquery-" + JQueryVer + ".min.js",
                DebugPath = "~/Scripts/jquery-" + JQueryVer + ".js",
                CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".min.js",
                CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-" + JQueryVer + ".js",
                CdnSupportsSecureConnection = true,
                LoadSuccessExpression = "window.jQuery"
            });
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            //HttpRuntimeSection runTime = (HttpRuntimeSection)WebConfigurationManager.GetSection("system.web/httpRuntime");
            //int maxRequestLength = (runTime.MaxRequestLength - 100) * 1024;
            //HttpContext context = ((HttpApplication)sender).Context;
            //if (context.Request.ContentLength > maxRequestLength)
            //{
            //    IServiceProvider provider = (IServiceProvider)context;
            //    HttpWorkerRequest workerRequest = (HttpWorkerRequest)provider.GetService(typeof(HttpWorkerRequest));

            //    // Check if body contains data
            //    if (workerRequest.HasEntityBody())
            //    {
            //        // get the total body length
            //        int requestLength = workerRequest.GetTotalEntityBodyLength();
            //        // Get the initial bytes loaded
            //        int initialBytes = 0;

            //        if (workerRequest.GetPreloadedEntityBody() != null)
            //            initialBytes = workerRequest.GetPreloadedEntityBody().Length;

            //        if (!workerRequest.IsEntireEntityBodyIsPreloaded())
            //        {
            //            byte[] buffer = new byte[512000];
            //            // Set the received bytes to initial bytes before start reading
            //            int receivedBytes = initialBytes;
            //            while (requestLength - receivedBytes >= initialBytes)
            //            {
            //                // Read another set of bytes
            //                initialBytes = workerRequest.ReadEntityBody(buffer, buffer.Length);

            //                // Update the received bytes
            //                receivedBytes += initialBytes;
            //            }

            //            initialBytes = workerRequest.ReadEntityBody(buffer, requestLength - receivedBytes);
            //        }
            //    }
            //    // Redirect the user to the same page with querystring action=exception.
            //    context.Response.Redirect(this.Request.Url.LocalPath + "?action=exception");
            //}
        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
        protected void RegisterRoutes(RouteCollection routes)
        {
            #region Common
            routes.MapPageRoute("Login", "Login", "~/index.aspx");
            routes.MapPageRoute("Home", "Home", "~/Console.aspx");
            routes.MapPageRoute("Logout", "Logout", "~/Logout.aspx");
            routes.MapPageRoute("ErrorPage", "ErrorPage", "~/ErrorPage.aspx");
            routes.MapPageRoute("Health", "Health", "~/Health.ashx");
            routes.MapPageRoute("ForgotPassword", "ForgotPassword", "~/ForgotPassword.aspx");
            routes.MapPageRoute("ResetPassword", "ResetPassword", "~/ResetPassword.aspx");
            #endregion

            #region Personnel
            routes.MapPageRoute("Personnel", "Personnel/Personnel", "~/Personnels/Personnel.aspx");
            routes.MapPageRoute("AddMultiUsersReport", "Personnel/AddMultiUsersReport", "~/Personnels/AddMultiUsersReport.aspx");
            routes.MapPageRoute("PersonnelArg1Dept", "Personnel/Personnel/{DepartmentId}", "~/Personnels/Personnel.aspx");
            routes.MapPageRoute("Department", "Personnel/Department", "~/Personnels/Department.aspx");
            routes.MapPageRoute("JobLevel", "Personnel/JobLevel", "~/Personnels/JobLevel.aspx");
            #endregion

            #region MatchUp
            routes.MapPageRoute("TopicList", "MatchUp/TopicList", "~/MatchUp/TopicList.aspx");
            routes.MapPageRoute("TopicListArg1Category", "MatchUp/TopicList/{CategoryId}", "~/MatchUp/TopicList.aspx");
            routes.MapPageRoute("TopicCreate", "MatchUp/TopicCreate", "~/MatchUp/TopicCreate.aspx");
            routes.MapPageRoute("QuizEdit", "MatchUp/QuizEdit/{TopicId}", "~/MatchUp/QuizEdit.aspx");
            routes.MapPageRoute("Category", "MatchUp/Category", "~/MatchUp/Category.aspx");
            #endregion

            #region Survey
            routes.MapPageRoute("SurveyList", "Survey/List", "~/Survey/SurveyList.aspx");
            routes.MapPageRoute("SurveyListWithCategoryid", "Survey/List/{CategoryId}", "~/Survey/SurveyList.aspx");
            routes.MapPageRoute("SurveyCreate", "Survey/Create", "~/Survey/SurveyCreate.aspx");
            routes.MapPageRoute("SurveyCategory", "Survey/Category", "~/Survey/SurveyCategory.aspx");
            routes.MapPageRoute("SurveyEdit", "Survey/Edit", "~/Survey/SurveyEdit.aspx");
            routes.MapPageRoute("SurveyEditWithSurveyidCategoryid", "Survey/Edit/{SurveyId}/{CategoryId}", "~/Survey/SurveyEdit.aspx");
            routes.MapPageRoute("SurveyAnalytic", "Survey/Analytic/{SurveyId}/{CategoryId}", "~/Survey/SurveyAnalytic.aspx");
            routes.MapPageRoute("SurveyAnalyticDetail", "Survey/Analytic/Detail/{TopicId}/{CategoryId}/{CardId}", "~/Survey/SurveyAnalyticDetail.aspx");

            routes.MapPageRoute("SurveyAnalyticDemo", "Survey/AnalyticDemo/{SurveyId}/{CategoryId}", "~/Survey/SurveyAnalyticDemo.aspx");
            #endregion

            #region Feed
            routes.MapPageRoute("Permission", "Feed/Permission", "~/Feeds/Permission.aspx");
            routes.MapPageRoute("Hashtags", "Feed/Hashtags", "~/Feeds/Hashtags.aspx");
            routes.MapPageRoute("SuspendedUser", "Feed/SuspendedUser", "~/Feeds/SuspendedUser.aspx");
            #endregion

            #region Dashboard
            routes.MapPageRoute("AlertReport", "Dashboard/AlertReport", "~/Dashboard/AlertReport.aspx");
            routes.MapPageRoute("ProfilePhotoApproval", "Dashboard/ProfilePhotoApproval", "~/Dashboard/ProfilePhotoApproval.aspx");
            routes.MapPageRoute("Feedbacks", "Dashboard/Feedbacks", "~/Dashboard/Feedbacks.aspx");
            #endregion

            #region Analytics
            routes.MapPageRoute("Summary", "Analytics/Summary", "~/Analytics/Summary.aspx");
            routes.MapPageRoute("DailyActiveUsers", "Analytics/DailyActiveUsers", "~/Analytics/DailyActiveUsers.aspx");
            routes.MapPageRoute("MatchUpImprovement", "Analytics/MatchUpImprovement", "~/Analytics/MatchUpImprovement.aspx");
            routes.MapPageRoute("SingleTopicAnalytic", "Analytics/SingleTopicAnalytic/{TopicId}", "~/Analytics/SingleTopicAnalytic.aspx");
            routes.MapPageRoute("QuestionDetail", "Analytics/QuestionDetail/{TopicId}/{QuestionId}", "~/Analytics/QuestionDetail.aspx");
            routes.MapPageRoute("AllQuestions", "Analytics/AllQuestions/{TopicId}", "~/Analytics/AllQuestions.aspx");
            routes.MapPageRoute("DailyTopicActivities", "Analytics/DailyTopicActivities/{Date}", "~/Analytics/DailyTopicActivities.aspx");
            routes.MapPageRoute("AllTopics", "Analytics/AllTopics", "~/Analytics/AllTopics.aspx");
            #endregion

            #region CompanyProfile
            routes.MapPageRoute("CompanyProfile", "CompanyProfile/Profile", "~/CompanyProfile/Profile.aspx");

            routes.MapPageRoute("CompanyAdminPhoto", "CompanyProfile/AdminPhoto", "~/CompanyProfile/AdminPhoto.aspx");
            routes.MapPageRoute("CompanyAnimationLogo", "CompanyProfile/AnimationLogo", "~/CompanyProfile/AnimationLogo.aspx");
            routes.MapPageRoute("CompanyCompanyBanner", "CompanyProfile/CompanyBanner", "~/CompanyProfile/CompanyBanner.aspx");
            routes.MapPageRoute("CompanyCoverPhoto", "CompanyProfile/CoverPhoto", "~/CompanyProfile/CoverPhoto.aspx");
            routes.MapPageRoute("CompanyMatchupBanner", "CompanyProfile/MatchupBanner", "~/CompanyProfile/MatchupBanner.aspx");
            routes.MapPageRoute("CompanyPhoneBanner", "CompanyProfile/PhoneBanner", "~/CompanyProfile/PhoneBanner.aspx");
            routes.MapPageRoute("CompanyEmail", "CompanyProfile/Email", "~/CompanyProfile/Email.aspx");
            routes.MapPageRoute("CompanyWebsiteHeader", "CompanyProfile/WebsiteHeader", "~/CompanyProfile/WebsiteHeader.aspx");
            /**********/
            routes.MapPageRoute("AlbumAdminPhoto", "CompanyProfile/AlbumAdminPhoto", "~/CompanyProfile/AlbumAdminPhoto.aspx");
            routes.MapPageRoute("AlbumEmail", "CompanyProfile/AlbumEmail", "~/CompanyProfile/AlbumEmail.aspx");
            routes.MapPageRoute("AlbumWebsiteHeader", "CompanyProfile/AlbumWebsiteHeader", "~/CompanyProfile/AlbumWebsiteHeader.aspx");
            routes.MapPageRoute("AlbumCompanySelection", "CompanyProfile/AlbumCompanySelection", "~/CompanyProfile/AlbumCompanySelection.aspx");
            routes.MapPageRoute("AlbumPhoneBanner", "CompanyProfile/AlbumPhoneBanner", "~/CompanyProfile/AlbumPhoneBanner.aspx");
            routes.MapPageRoute("AlbumAnimationLogo", "CompanyProfile/AlbumAnimationLogo", "~/CompanyProfile/AlbumAnimationLogo.aspx");
            routes.MapPageRoute("AlbumMatchupBanner", "CompanyProfile/AlbumMatchupBanner", "~/CompanyProfile/AlbumMatchupBanner.aspx");
            routes.MapPageRoute("AlbumCoverPhoto", "CompanyProfile/AlbumCoverPhoto", "~/CompanyProfile/AlbumCoverPhoto.aspx");
            #endregion

            #region Event
            routes.MapPageRoute("Event", "Event/Event", "~/Event/Event.aspx");
            routes.MapPageRoute("EventCreate", "Event/EventCreate", "~/Event/EventCreate.aspx");
            routes.MapPageRoute("EventEdit", "Event/EventEdit/{EventId}", "~/Event/EventEdit.aspx");
            #endregion

            #region Dynamic Pulse
            routes.MapPageRoute("AnnouncementPulseFeed_List", "DynamicPulse/AnnouncementPulseFeedList", "~/DynamicPulse/AnnouncementPulseFeedList.aspx");
            routes.MapPageRoute("AnnouncementPulseFeed_Create", "DynamicPulse/AnnouncementPulse/", "~/DynamicPulse/AnnouncementPulse.aspx");
            routes.MapPageRoute("AnnouncementPulseFeed_Edit", "DynamicPulse/AnnouncementPulse/{PulseId}", "~/DynamicPulse/AnnouncementPulse.aspx");

            routes.MapPageRoute("ClarityPulseFeed_List", "DynamicPulse/ClarityPulseFeedList", "~/DynamicPulse/ClarityPulseFeedList.aspx");
            routes.MapPageRoute("ClarityPulseFeed_Create", "DynamicPulse/ClarityPulse/", "~/DynamicPulse/ClarityPulse.aspx");
            routes.MapPageRoute("ClarityPulseFeed_Edit", "DynamicPulse/ClarityPulse/{PulseId}", "~/DynamicPulse/ClarityPulse.aspx");

            routes.MapPageRoute("ResponsivePulseFeed_List", "DynamicPulse/ResponsivePulseFeedList", "~/DynamicPulse/ResponsivePulseFeedList.aspx");
            routes.MapPageRoute("ResponsivePulseFeed_Create", "DynamicPulse/ResponsivePulseCreate/", "~/DynamicPulse/ResponsivePulseCreate.aspx");
            routes.MapPageRoute("ResponsivePulseFeed_Edit", "DynamicPulse/ResponsivePulse/{DeckId}", "~/DynamicPulse/ResponsivePulse.aspx");

            routes.MapPageRoute("DynamicPulse_PulseAnalytic", "DynamicPulse/PulseAnalytic/{PulseId}", "~/DynamicPulse/PulseAnalytic.aspx");
            routes.MapPageRoute("DynamicPulse_ResponsivePulseAnalytic", "DynamicPulse/ResponsivePulseAnalytic/{DeckId}/", "~/DynamicPulse/ResponsivePulseAnalytic.aspx");
            routes.MapPageRoute("DynamicPulse_ResponsivePulseAnalytic_2", "DynamicPulse/ResponsivePulseAnalytic/{DeckId}/{PulseId}", "~/DynamicPulse/ResponsivePulseAnalytic.aspx");
            routes.MapPageRoute("DynamicPulse_RespondersReportList", "DynamicPulse/RespondersReportList/{DeckId}/{PulseId}/{OptionId}", "~/DynamicPulse/RespondersReportList.aspx");
            routes.MapPageRoute("DynamicPulse_RespondersReportList_2", "DynamicPulse/RespondersReportList/{DeckId}/{PulseId}/{OptionId}/{DepartmentName}", "~/DynamicPulse/RespondersReportList.aspx");
            routes.MapPageRoute("DynamicPulse_ResponderReportDetail", "DynamicPulse/ResponderReportDetail/{DeckId}/{UserId}", "~/DynamicPulse/ResponderReportDetail.aspx");
            routes.MapPageRoute("DynamicPulse_RespondersAnswerList", "DynamicPulse/RespondersAnswerList/{DeckId}/{PulseId}", "~/DynamicPulse/RespondersAnswerList.aspx");

            #endregion Dynamic Pulse

            #region Setting
            routes.MapPageRoute("Setting_ProfilePhoto", "Setting/ProfilePhoto", "~/Setting/ProfilePhoto.aspx");
            routes.MapPageRoute("Setting_ChangePassword", "Setting/ChangePassword", "~/Setting/ChangePassword.aspx");
            #endregion

            #region Assessment
            routes.MapPageRoute("AssessmentList", "Assessment/List", "~/Assessment/AssessmentList.aspx");
            routes.MapPageRoute("AssessmentCreate", "Assessment/Create", "~/Assessment/AssessmentCreate.aspx");
            routes.MapPageRoute("AssessmentEdit", "Assessment/Edit/{AssessmentId}", "~/Assessment/AssessmentEdit.aspx");
            routes.MapPageRoute("AssessmentCoach", "Assessment/Coach", "~/Assessment/CoachList.aspx");
            routes.MapPageRoute("AssessmentSubscription", "Assessment/Subscription", "~/Assessment/SubscriptionList.aspx");
            routes.MapPageRoute("SubscriptionCreate", "Assessment/SubscriptionCreate/{CompanyId}/{AssessmentId}", "~/Assessment/SubscriptionCreate.aspx");
            routes.MapPageRoute("SubscriptionEdit", "Assessment/SubscriptionEdit/{CompanyId}/{AssessmentId}", "~/Assessment/SubscriptionEdit.aspx");


            //routes.MapPageRoute("SurveyListWithCategoryid", "Survey/List/{CategoryId}", "~/Survey/SurveyList.aspx");
            //routes.MapPageRoute("SurveyCategory", "Survey/Category", "~/Survey/SurveyCategory.aspx");
            //routes.MapPageRoute("SurveyEditWithSurveyidCategoryid", "Survey/Edit/{SurveyId}/{CategoryId}", "~/Survey/SurveyEdit.aspx");
            //routes.MapPageRoute("SurveyAnalytic", "Survey/Analytic/{SurveyId}/{CategoryId}", "~/Survey/SurveyAnalytic.aspx");
            //routes.MapPageRoute("SurveyAnalyticDetail", "Survey/Analytic/Detail/{TopicId}/{CategoryId}/{CardId}", "~/Survey/SurveyAnalyticDetail.aspx");

            #endregion

            #region Module
            routes.MapPageRoute("ModuleCompanyList", "Module/Companies", "~/Module/CompanyList.aspx");
            routes.MapPageRoute("ModuleList", "Module/List/{CompanyId}", "~/Module/ModuleList.aspx");
            #endregion

            #region MLearning
            routes.MapPageRoute("EvaluationCategory", "MLearning/EvaluationCategory", "~/MLearning/EvaluationCategory.aspx");
            routes.MapPageRoute("EvaluationList", "MLearning/EvaluationList", "~/MLearning/EvaluationList.aspx");
            routes.MapPageRoute("EvaluationListWithCategoryid", "MLearning/EvaluationList/{CategoryId}", "~/MLearning/EvaluationList.aspx");
            routes.MapPageRoute("EvaluationCreate", "MLearning/EvaluationCreate", "~/MLearning/EvaluationCreate.aspx");
            routes.MapPageRoute("EvaluationEdit", "MLearning/EvaluationEdit/{ExamId}/{CategoryId}", "~/MLearning/EvaluationEdit.aspx");
            routes.MapPageRoute("EvaluationAnalytics", "MLearning/EvaluationAnalytics/{ExamId}/{CategoryId}", "~/MLearning/EvaluationAnalytics.aspx");
            routes.MapPageRoute("EvaluationAnalyticUser", "MLearning/EvaluationAnalyticUser/{ExamId}/{CategoryId}/{UserId}", "~/MLearning/EvaluationAnalyticUser.aspx");
            routes.MapPageRoute("EducationCategory", "MLearning/EducationCategory", "~/MLearning/EducationCategory.aspx");
            routes.MapPageRoute("EducationList", "MLearning/EducationList", "~/MLearning/EducationList.aspx");
            routes.MapPageRoute("EducationListWithCategoryid", "MLearning/EducationList/{CategoryId}", "~/MLearning/EducationList.aspx");
            routes.MapPageRoute("EducationCreate", "MLearning/EducationCreate", "~/MLearning/EducationCreate.aspx");
            routes.MapPageRoute("EducationEdit", "MLearning/EducationEdit/{EducationId}/{CategoryId}", "~/MLearning/EducationEdit.aspx");
            routes.MapPageRoute("EducationAnalytic", "MLearning/EducationAnalytic/{EducationId}/{CategoryId}", "~/MLearning/EducationAnalytic.aspx");
            routes.MapPageRoute("EducationAnalyticUser", "MLearning/EducationAnalyticUser/{EducationId}/{CategoryId}/{UserId}", "~/MLearning/EducationAnalyticUser.aspx");
            routes.MapPageRoute("AssignmentList", "MLearning/AssignmentList", "~/MLearning/AssignmentList.aspx");
            routes.MapPageRoute("AssignmentDetail", "MLearning/AssignmentDetail", "~/MLearning/AssignmentDetail.aspx");
            routes.MapPageRoute("AssignmentDetailWithAssignmentId", "MLearning/AssignmentDetail/{AssignmentId}", "~/MLearning/AssignmentDetail.aspx");
            #endregion

            #region Gamification
            routes.MapPageRoute("ExperiencePoints", "Gamification/ExperiencePoints", "~/Gamification/ExperiencePoints.aspx");
            routes.MapPageRoute("Leaderboard", "Gamification/Leaderboard", "~/Gamification/Leaderboard.aspx");
            routes.MapPageRoute("AchievementList", "Gamification/AchievementList", "~/Gamification/AchievementList.aspx");
            routes.MapPageRoute("AchievementDetail", "Gamification/AchievementDetail", "~/Gamification/AchievementDetail.aspx");
            routes.MapPageRoute("AchievementDetailWithAchievementd", "Gamification/AchievementDetail/{AchievementId}", "~/Gamification/AchievementDetail.aspx");
            routes.MapPageRoute("AchievementCreate", "Gamification/AchievementCreate", "~/Gamification/AchievementCreate.aspx");
            routes.MapPageRoute("GamificationAnalytic", "Gamification/GamificationAnalytic/{AchievementId}", "~/Gamification/GamificationAnalytic.aspx");
            routes.MapPageRoute("UserAchievements", "Gamification/UserAchievements/{AchieverId}", "~/Gamification/UserAchievements.aspx");
            #endregion

            #region Announcement
            routes.MapPageRoute("PostNotification", "Announcement/PostNotification", "~/Announcement/PostNotification.aspx");
            #endregion

            #region Live360
            routes.MapPageRoute("Likert7List", "Live360/Likert7List", "~/Live360/Likert7List.aspx");
            routes.MapPageRoute("Likert7Create", "Live360/Likert7Create", "~/Live360/Likert7Create.aspx");
            routes.MapPageRoute("Likert7Detail", "Live360/Likert7Detail/{Likert7Id}", "~/Live360/Likert7Detail.aspx");
            routes.MapPageRoute("BarsList", "Live360/BarsList", "~/Live360/BarsList.aspx");
            routes.MapPageRoute("BarsCreate", "Live360/BarsCreate", "~/Live360/BarsCreate.aspx");
            routes.MapPageRoute("BarsDetail", "Live360/BarsDetail/{AppraisalId}", "~/Live360/BarsDetail.aspx");
            #endregion
        }

        public static bool IsMaxRequestExceededException(Exception e)
        {
            // unhandled errors = caught at global.ascx level
            // http exception = caught at page level
            int TimedOutExceptionCode = -2147467259;
            Exception main;
            var unhandled = e as HttpUnhandledException;

            if (unhandled != null && unhandled.ErrorCode == TimedOutExceptionCode)
            {
                main = unhandled.InnerException;
            }
            else
            {
                main = e;
            }


            var http = main as HttpException;

            if (http != null && http.ErrorCode == TimedOutExceptionCode)
            {
                // hack: no real method of identifying if the error is max request exceeded as 
                // it is treated as a timeout exception
                if (http.StackTrace.Contains("GetEntireRawContent"))
                {
                    // MAX REQUEST HAS BEEN EXCEEDED
                    return true;
                }
            }

            return false;
        }
    }
}