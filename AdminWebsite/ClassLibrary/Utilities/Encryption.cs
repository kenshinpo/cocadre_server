﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CoCadre.Utilities
{
    public class Encryption
    {
        public static string SHA256_Hash(string text, string salt)
        {
            SHA256 sha256 = new SHA256Managed();
            string combined = string.Concat(text, salt);

            byte[] combinedByte = Encoding.UTF8.GetBytes(combined);
            byte[] hashByte = sha256.ComputeHash(combinedByte);
            string newText = "";
            for (int i = 0; i < hashByte.Length; i++)
            {
                newText += hashByte[i].ToString("X2");
            }

            return newText;

        }
    }
}
