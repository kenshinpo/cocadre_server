﻿namespace UploadQuestionForm
{
    partial class fUploadQuestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gvQuestion = new System.Windows.Forms.DataGridView();
            this.btnUpload = new System.Windows.Forms.Button();
            this.tbCompanyId = new System.Windows.Forms.TextBox();
            this.tbTopicCategoryId = new System.Windows.Forms.TextBox();
            this.tbTopicId = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbAdminId = new System.Windows.Forms.TextBox();
            this.btnActive = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.gbServer = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.gvQuestion)).BeginInit();
            this.gbServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // gvQuestion
            // 
            this.gvQuestion.AllowUserToAddRows = false;
            this.gvQuestion.AllowUserToDeleteRows = false;
            this.gvQuestion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvQuestion.Location = new System.Drawing.Point(12, 12);
            this.gvQuestion.Name = "gvQuestion";
            this.gvQuestion.ReadOnly = true;
            this.gvQuestion.Size = new System.Drawing.Size(1320, 477);
            this.gvQuestion.TabIndex = 0;
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(1338, 12);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(173, 23);
            this.btnUpload.TabIndex = 1;
            this.btnUpload.Text = "Upload CSV";
            this.btnUpload.UseVisualStyleBackColor = true;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // tbCompanyId
            // 
            this.tbCompanyId.Location = new System.Drawing.Point(1338, 358);
            this.tbCompanyId.Name = "tbCompanyId";
            this.tbCompanyId.Size = new System.Drawing.Size(173, 20);
            this.tbCompanyId.TabIndex = 2;
            this.tbCompanyId.Text = "Company ID";
            this.tbCompanyId.Enter += new System.EventHandler(this.tbCompanyId_Enter);
            this.tbCompanyId.Leave += new System.EventHandler(this.tbCompanyId_Leave);
            // 
            // tbTopicCategoryId
            // 
            this.tbTopicCategoryId.Location = new System.Drawing.Point(1338, 384);
            this.tbTopicCategoryId.Name = "tbTopicCategoryId";
            this.tbTopicCategoryId.Size = new System.Drawing.Size(173, 20);
            this.tbTopicCategoryId.TabIndex = 3;
            this.tbTopicCategoryId.Text = "Topic Category ID";
            this.tbTopicCategoryId.Enter += new System.EventHandler(this.tbTopicCategoryId_Enter);
            this.tbTopicCategoryId.Leave += new System.EventHandler(this.tbTopicCategoryId_Leave);
            // 
            // tbTopicId
            // 
            this.tbTopicId.Location = new System.Drawing.Point(1338, 410);
            this.tbTopicId.Name = "tbTopicId";
            this.tbTopicId.Size = new System.Drawing.Size(173, 20);
            this.tbTopicId.TabIndex = 4;
            this.tbTopicId.Text = "Topic ID";
            this.tbTopicId.Enter += new System.EventHandler(this.tbTopicId_Enter);
            this.tbTopicId.Leave += new System.EventHandler(this.tbTopicId_Leave);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(1338, 436);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(173, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Create Questions";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbAdminId
            // 
            this.tbAdminId.Location = new System.Drawing.Point(1338, 332);
            this.tbAdminId.Name = "tbAdminId";
            this.tbAdminId.Size = new System.Drawing.Size(173, 20);
            this.tbAdminId.TabIndex = 6;
            this.tbAdminId.Text = "Admin ID";
            this.tbAdminId.Enter += new System.EventHandler(this.tbAdminId_Enter);
            this.tbAdminId.Leave += new System.EventHandler(this.tbAdminId_Leave);
            // 
            // btnActive
            // 
            this.btnActive.Location = new System.Drawing.Point(1339, 466);
            this.btnActive.Name = "btnActive";
            this.btnActive.Size = new System.Drawing.Size(172, 23);
            this.btnActive.TabIndex = 7;
            this.btnActive.Text = "Set Current Topic to Active";
            this.btnActive.UseVisualStyleBackColor = true;
            this.btnActive.Click += new System.EventHandler(this.btnActive_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(12, 499);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(101, 13);
            this.lblStatus.TabIndex = 8;
            this.lblStatus.Text = "Uploader status text";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(88, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Development";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 65);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(76, 17);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.Text = "Production";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 42);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(61, 17);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.Text = "Staging";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // gbServer
            // 
            this.gbServer.Controls.Add(this.radioButton1);
            this.gbServer.Controls.Add(this.radioButton3);
            this.gbServer.Controls.Add(this.radioButton2);
            this.gbServer.Location = new System.Drawing.Point(1338, 226);
            this.gbServer.Name = "gbServer";
            this.gbServer.Size = new System.Drawing.Size(173, 100);
            this.gbServer.TabIndex = 9;
            this.gbServer.TabStop = false;
            this.gbServer.Text = "Servers";
            // 
            // fUploadQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1523, 521);
            this.Controls.Add(this.gbServer);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnActive);
            this.Controls.Add(this.tbAdminId);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbTopicId);
            this.Controls.Add(this.tbTopicCategoryId);
            this.Controls.Add(this.tbCompanyId);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.gvQuestion);
            this.Name = "fUploadQuestion";
            this.Text = "Uploader";
            ((System.ComponentModel.ISupportInitialize)(this.gvQuestion)).EndInit();
            this.gbServer.ResumeLayout(false);
            this.gbServer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gvQuestion;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.TextBox tbCompanyId;
        private System.Windows.Forms.TextBox tbTopicCategoryId;
        private System.Windows.Forms.TextBox tbTopicId;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbAdminId;
        private System.Windows.Forms.Button btnActive;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.GroupBox gbServer;
    }
}

