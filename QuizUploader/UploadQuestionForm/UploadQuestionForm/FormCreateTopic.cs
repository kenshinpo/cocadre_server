﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UploadQuestionForm
{
    public partial class FormCreateTopic : Form
    {
        private Action<string, string> UpdateInitialFormId;

        public FormCreateTopic(Action<string, string> update_action)
        {
            UpdateInitialFormId = update_action;
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void tbTopicTitle_Enter(object sender, EventArgs e)
        {
            if (tbTopicTitle.Text.Equals("Topic Title"))
            {
                tbTopicTitle.Text = string.Empty;
            }
        }

        private void tbTopicTitle_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbTopicTitle.Text))
            {
                tbTopicTitle.Text = "Topic Title";
            }

            tbTopicTitle.Select(0, 0);
        }

        private void tbTopicCategoryId_Enter(object sender, EventArgs e)
        {
            if (tbTopicCategoryId.Text.Equals("Topic Category ID"))
            {
                tbTopicCategoryId.Text = string.Empty;
            }
        }

        private void tbTopicCategoryId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbTopicCategoryId.Text))
            {
                tbTopicCategoryId.Text = "Topic Category ID";
            }

            tbTopicCategoryId.Select(0, 0);

        }

        private void tbTopicCategoryTitle_Enter(object sender, EventArgs e)
        {
            if (tbTopicCategoryTitle.Text.Equals("Topic Category Title"))
            {
                tbTopicCategoryTitle.Text = string.Empty;
            }
        }

        private void tbTopicCategoryTitle_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbTopicCategoryTitle.Text))
            {
                tbTopicCategoryTitle.Text = "Topic Category Title";
            }

            tbTopicCategoryTitle.Select(0, 0);
        }

        private void btnCreateTopic_Click(object sender, EventArgs e)
        {
            if (IsValidated())
            {
                string topic_id = string.Empty;
                string topic_category_id = tbTopicCategoryId.Text;

                //AdminQuizServiceClient client = new AdminQuizServiceClient();
                UpdateInitialFormId(topic_id, topic_category_id);
                this.Close();
            }
        }

        private bool IsValidated()
        {
            if (tbTopicTitle.Text.Equals("Topic Title"))
            {
                MessageBox.Show("Check Topic Title", "Invalid Topic Title");
            }
            else if (tbTopicCategoryId.Text.Equals("Topic Category ID") && (tbTopicCategoryTitle.Text.Equals("Topic Category Title")))
            {
                MessageBox.Show("Check Topic Category Title", "Invalid Topic Category Title");
            }
            else
            {
                return true;
            }

            return false;
        }
    }
}
