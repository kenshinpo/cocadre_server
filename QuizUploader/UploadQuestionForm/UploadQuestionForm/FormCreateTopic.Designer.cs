﻿namespace UploadQuestionForm
{
    partial class FormCreateTopic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTopicTitle = new System.Windows.Forms.TextBox();
            this.tbTopicCategoryId = new System.Windows.Forms.TextBox();
            this.tbTopicCategoryTitle = new System.Windows.Forms.TextBox();
            this.btnCreateTopic = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbTopicTitle
            // 
            this.tbTopicTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbTopicTitle.Location = new System.Drawing.Point(33, 20);
            this.tbTopicTitle.Name = "tbTopicTitle";
            this.tbTopicTitle.Size = new System.Drawing.Size(178, 20);
            this.tbTopicTitle.TabIndex = 0;
            this.tbTopicTitle.Text = "Topic Title";
            this.tbTopicTitle.Enter += new System.EventHandler(this.tbTopicTitle_Enter);
            this.tbTopicTitle.Leave += new System.EventHandler(this.tbTopicTitle_Leave);
            // 
            // tbTopicCategoryId
            // 
            this.tbTopicCategoryId.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbTopicCategoryId.Location = new System.Drawing.Point(33, 46);
            this.tbTopicCategoryId.Name = "tbTopicCategoryId";
            this.tbTopicCategoryId.Size = new System.Drawing.Size(178, 20);
            this.tbTopicCategoryId.TabIndex = 1;
            this.tbTopicCategoryId.Text = "Topic Category ID";
            this.tbTopicCategoryId.Enter += new System.EventHandler(this.tbTopicCategoryId_Enter);
            this.tbTopicCategoryId.Leave += new System.EventHandler(this.tbTopicCategoryId_Leave);
            // 
            // tbTopicCategoryTitle
            // 
            this.tbTopicCategoryTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbTopicCategoryTitle.Location = new System.Drawing.Point(33, 72);
            this.tbTopicCategoryTitle.Name = "tbTopicCategoryTitle";
            this.tbTopicCategoryTitle.Size = new System.Drawing.Size(178, 20);
            this.tbTopicCategoryTitle.TabIndex = 2;
            this.tbTopicCategoryTitle.Text = "Topic Category Title";
            this.tbTopicCategoryTitle.Enter += new System.EventHandler(this.tbTopicCategoryTitle_Enter);
            this.tbTopicCategoryTitle.Leave += new System.EventHandler(this.tbTopicCategoryTitle_Leave);
            // 
            // btnCreateTopic
            // 
            this.btnCreateTopic.Location = new System.Drawing.Point(33, 157);
            this.btnCreateTopic.Name = "btnCreateTopic";
            this.btnCreateTopic.Size = new System.Drawing.Size(178, 23);
            this.btnCreateTopic.TabIndex = 3;
            this.btnCreateTopic.Text = "Create New Topic";
            this.btnCreateTopic.UseVisualStyleBackColor = true;
            this.btnCreateTopic.Click += new System.EventHandler(this.btnCreateTopic_Click);
            // 
            // FormCreateTopic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 192);
            this.Controls.Add(this.btnCreateTopic);
            this.Controls.Add(this.tbTopicCategoryTitle);
            this.Controls.Add(this.tbTopicCategoryId);
            this.Controls.Add(this.tbTopicTitle);
            this.Name = "FormCreateTopic";
            this.Text = "Create New Topic";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTopicTitle;
        private System.Windows.Forms.TextBox tbTopicCategoryId;
        private System.Windows.Forms.TextBox tbTopicCategoryTitle;
        private System.Windows.Forms.Button btnCreateTopic;
    }
}