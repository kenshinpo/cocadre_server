﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CassandraService.ServiceInterface;
using CassandraService.ServiceResponses;
using CassandraService.CassandraUtilities;

namespace UploadQuestionForm
{
    public partial class fUploadQuestion : Form
    {
        public delegate void ChangeLabelEventHandler(string status);

        private static List<Dictionary<string, string>> questionList = new List<Dictionary<string, string>>();
        private static string[] columns = null;
        private static bool hasErrorReadingQuestion = false;
        private static AdminService client = new AdminService();
 
        private string initialFilePath =  @"D:\Dropbox (AP Origin)\AP Origin Team Folder\CoCadre\Documents\CocadreQuestions";

        OpenFileDialog fDialog = new OpenFileDialog();

        public int mode = 1;

        public enum ServerMode
        {
            Development = 1,
            Staging = 2,
            Production = 3
        }

        public fUploadQuestion()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();

            fDialog = new OpenFileDialog();
            fDialog.InitialDirectory = initialFilePath;
            fDialog.Filter = "CSV Files|*.csv";
            fDialog.Title = "Open CSV File";
        }

        private void DisplayCsvContent()
        {
            DataTable dt = new DataTable();
            List<DataRow> dataRows = new List<DataRow>();

            foreach (string column in columns)
            {
                dt.Columns.Add(new DataColumn(column));
            }

            foreach (Dictionary<string, string> questionDict in questionList)
            {
                DataRow row = dt.NewRow();
                foreach (string column in columns)
                {
                    row[column] = questionDict[column];
                }
                dt.Rows.Add(row);
            }

            gvQuestion.DataSource = dt;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (fDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = fDialog.FileName;
                questionList = new List<Dictionary<string, string>>();

                LoadCsv(fileName);

                DisplayCsvContent();

                this.Invoke(new ChangeLabelEventHandler(UpdateStatus), string.Format("Uploaded {0} questions", questionList.Count()));

                fDialog.InitialDirectory = Path.GetDirectoryName(fDialog.FileName);
            }
        }

        private void LoadCsv(string fileName)
        {
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding();
                TextFieldParser parser = new TextFieldParser(fileName, utf8);

                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");

                columns = parser.ReadFields();

                int indexOfChoiceType = Array.FindIndex(columns, column => column.Contains("choice_type"));
                int indexOfQuestionType = Array.FindIndex(columns, column => column.Contains("question_type"));

                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();

                    Dictionary<string, string> questionDict = new Dictionary<string, string>();

                    int questionType = Int16.Parse(fields[indexOfQuestionType]);
                    questionDict.Add("question_type", questionType.ToString());

                    int choiceType = Int16.Parse(fields[indexOfChoiceType]);
                    questionDict.Add("choice_type", choiceType.ToString());

                    string questionContent = string.Empty;

                    string firstChoiceContent = string.Empty;
                    string secondChoiceContent = string.Empty;
                    string thirdChoiceContent = string.Empty;
                    string fourthChoiceContent = string.Empty;

                    float timeReading = 0.0f;
                    float timeAnswering = 0.0f;

                    int difficultyLevel = 1;
                    int scoreMultiplier = 1;
                    double frequency = 0.0;

                    // Text question
                    if (questionType == 1 || questionType == 2)
                    {
                        questionContent = fields[Array.FindIndex(columns, column => column.Contains("question_content"))];
                        CheckForNull(questionContent);
                        questionDict.Add("question_content", questionContent);
                    }

                    if (choiceType == 1)
                    {
                        firstChoiceContent = fields[Array.FindIndex(columns, column => column.Contains("first_choice_content"))];
                        CheckForNull(firstChoiceContent);
                        questionDict.Add("first_choice_content", firstChoiceContent);

                        secondChoiceContent = fields[Array.FindIndex(columns, column => column.Contains("second_choice_content"))];
                        CheckForNull(secondChoiceContent);
                        questionDict.Add("second_choice_content", secondChoiceContent);

                        thirdChoiceContent = fields[Array.FindIndex(columns, column => column.Contains("third_choice_content"))];
                        CheckForNull(thirdChoiceContent);
                        questionDict.Add("third_choice_content", thirdChoiceContent);

                        fourthChoiceContent = fields[Array.FindIndex(columns, column => column.Contains("fourth_choice_content"))];
                        CheckForNull(fourthChoiceContent);
                        questionDict.Add("fourth_choice_content", fourthChoiceContent);
                    }

                    timeReading = float.Parse(fields[Array.FindIndex(columns, column => column.Contains("time_assigned_for_reading"))]);
                    CheckForNull(timeReading.ToString());
                    questionDict.Add("time_assigned_for_reading", timeReading.ToString());

                    timeAnswering = float.Parse(fields[Array.FindIndex(columns, column => column.Contains("time_assigned_for_answering"))]);
                    CheckForNull(timeAnswering.ToString());
                    questionDict.Add("time_assigned_for_answering", timeAnswering.ToString());

                    difficultyLevel = Int16.Parse(fields[Array.FindIndex(columns, column => column.Contains("difficulty_level"))]);
                    CheckForNull(difficultyLevel.ToString());
                    questionDict.Add("difficulty_level", difficultyLevel.ToString());

                    scoreMultiplier = Int16.Parse(fields[Array.FindIndex(columns, column => column.Contains("score_multiplier"))]);
                    CheckForNull(scoreMultiplier.ToString());
                    questionDict.Add("score_multiplier", scoreMultiplier.ToString());

                    frequency = double.Parse(fields[Array.FindIndex(columns, column => column.Contains("frequency"))]);
                    CheckForNull(frequency.ToString());
                    questionDict.Add("frequency", frequency.ToString());

                    if(!hasErrorReadingQuestion)
                    {
                        questionList.Add(questionDict);
                    }
                    else
                    {
                        MessageBox.Show("Question created wrongly", "GG");
                    }

                }
                parser.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void CheckForNull(string content)
        {
            if(string.IsNullOrEmpty(content))
            {
                hasErrorReadingQuestion = true;
            }
        }

        private void tbAdminId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbAdminId.Text))
            {
                tbAdminId.Text = "Admin ID";
            }

            tbAdminId.Select(0, 0);
        }

        private void tbAdminId_Enter(object sender, EventArgs e)
        {
            if (tbAdminId.Text.Equals("Admin ID"))
            {
                tbAdminId.Text = string.Empty;
            }
        }

        private void tbCompanyId_Enter(object sender, EventArgs e)
        {
            if (tbCompanyId.Text.Equals("Company ID"))
            {
                tbCompanyId.Text = string.Empty;
            }
        }

        private void tbCompanyId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbCompanyId.Text))
            {
                tbCompanyId.Text = "Company ID";
            }

            tbCompanyId.Select(0, 0);
        }

        private void tbTopicCategoryId_Enter(object sender, EventArgs e)
        {
            if (tbTopicCategoryId.Text.Equals("Topic Category ID"))
            {
                tbTopicCategoryId.Text = string.Empty;
            }
        }

        private void tbTopicCategoryId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbTopicCategoryId.Text))
            {
                tbTopicCategoryId.Text = "Topic Category ID";
            }

            tbTopicCategoryId.Select(0, 0);
        }

        private void tbTopicId_Enter(object sender, EventArgs e)
        {
            if (tbTopicId.Text.Equals("Topic ID"))
            {
                tbTopicId.Text = string.Empty;
            }
        }

        private void tbTopicId_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbTopicId.Text))
            {
                tbTopicId.Text = "Topic ID";
            }

            tbTopicId.Select(0, 0);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidated(false))
                {
                    string adminUserId = tbAdminId.Text;
                    string companyId = tbCompanyId.Text;
                    string topicCategoryId = tbTopicCategoryId.Text;
                    string topicId = tbTopicId.Text;

                    bool isSuccessfulCreated = true;

                    foreach (Dictionary<string, string> questionDict in questionList)
                    {
                        if (Int16.Parse(questionDict["question_type"]) == 1 || Int16.Parse(questionDict["question_type"]) == 2)
                        {
                            if (Int16.Parse(questionDict["choice_type"]) == 1)
                            {
                                QuestionCreateResponse response = client.CreateNewQuestion(adminUserId,
                                                                                             companyId,
                                                                                             null,
                                                                                             topicId,
                                                                                             topicCategoryId,
                                                                                             Int16.Parse(questionDict["question_type"]),
                                                                                             questionDict["question_content"],
                                                                                             null,
                                                                                             null,
                                                                                             "#000000",
                                                                                             Int16.Parse(questionDict["choice_type"]),
                                                                                             questionDict["first_choice_content"],
                                                                                             null,
                                                                                             null,
                                                                                             questionDict["second_choice_content"],
                                                                                             null,
                                                                                             null,
                                                                                             questionDict["third_choice_content"],
                                                                                             null,
                                                                                             null,
                                                                                             questionDict["fourth_choice_content"],
                                                                                             null,
                                                                                             null,
                                                                                             float.Parse(questionDict["time_assigned_for_reading"]),
                                                                                             float.Parse(questionDict["time_assigned_for_answering"]),
                                                                                             Int16.Parse(questionDict["difficulty_level"]),
                                                                                             Int16.Parse(questionDict["score_multiplier"]),
                                                                                             double.Parse(questionDict["frequency"]),
                                                                                             1,
                                                                                             mode);

                                if (!response.Success)
                                {
                                    Console.WriteLine("Error: " + response.ErrorMessage);
                                    isSuccessfulCreated = false;
                                    break;
                                }
                            }
                        }

                    }
                    if (isSuccessfulCreated)
                    {
                        this.Invoke(new ChangeLabelEventHandler(UpdateStatus), string.Format("Created {0} questions", questionList.Count()));
                        MessageBox.Show("All questions created successfully", "Success");
                    }
                    else
                    {
                        MessageBox.Show("Question created wrongly", "GG");
                    }
                    new ConnectionManager().Close(); 
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
            
        }

        private bool IsValidated(bool is_set_active)
        {
            if (!is_set_active && questionList.Count == 0)
            {
                MessageBox.Show("Fail to load question from CSV", "No Question");
            }
            else if (tbAdminId.Text.Equals("Admin ID"))
            {
                MessageBox.Show("Check Admin ID.", "Invalid Admin ID");
            }
            else if (tbCompanyId.Text.Equals("Company ID"))
            {
                MessageBox.Show("Check Company ID.", "Invalid Company ID");
            }
            else if (tbTopicCategoryId.Text.Equals("Topic Category ID"))
            {
                MessageBox.Show("Check Topic Category ID.", "Invalid Topic Category ID");
            }
            else if (tbTopicId.Text.Equals("Topic ID"))
            {
                MessageBox.Show("Check Topic ID.", "Invalid Topic ID");
            }
            //else if (tbTopicCategoryId.Text.Equals("Topic Category ID") || (tbTopicId.Text.Equals("Topic ID")))
            //{
            //    FormCreateTopic form_topic = new FormCreateTopic(UpdateId);
            //    form_topic.Show();
            //}
            else
            {
                return true;
            }

            return false;
        }

        public void UpdateId(string topic_id, string topic_category_id)
        {
            tbTopicId.Text = topic_id;
            tbTopicCategoryId.Text = topic_category_id;
        }

        private void btnActive_Click(object sender, EventArgs e)
        {
            if (IsValidated(true))
            {
                string adminUserId = tbAdminId.Text;
                string companyId = tbCompanyId.Text;
                string topicCategoryId = tbTopicCategoryId.Text;
                string topicId = tbTopicId.Text;

                TopicUpdateResponse response = client.UpdateTopicStatus(adminUserId, companyId, topicId, topicCategoryId, 2);

                if (response.Success)
                {
                    MessageBox.Show("Topic updated to Active", "Success");
                }
                else
                {
                    MessageBox.Show(response.ErrorMessage, "GG");
                }
            }

        }

        private void UpdateStatus(string status)
        {
            lblStatus.Text = status;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control control in this.gbServer.Controls)
            {
                if (control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;
                    if (radio.Checked)
                    {
                        if (radio.Text.Equals("Development"))
                        {
                            mode = (int)ServerMode.Development;
                            this.Invoke(new ChangeLabelEventHandler(UpdateStatus), string.Format("Development Server"));
                        }
                        else if (radio.Text.Equals("Staging"))
                        {
                            mode = (int)ServerMode.Staging;
                            this.Invoke(new ChangeLabelEventHandler(UpdateStatus), string.Format("Staging Server"));
                        }
                        else
                        {
                            mode = (int)ServerMode.Production;
                            this.Invoke(new ChangeLabelEventHandler(UpdateStatus), string.Format("Production Server"));
                        }
                        break;
                    }
                }
            }
        }
    }
}
