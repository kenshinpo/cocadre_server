﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;
using CassandraService.ServiceResponses;

namespace CassandraService.ServiceInterface
{
    public class AdminService
    {
        Authenticator authenticator = new Authenticator();
        Department department = new Department();
        User user = new User();
        Country country = new Country();
        AccountKind accountKind = new AccountKind();

        #region Authenticator
        public AuthenticationUpdateResponse UpdatePasswordForUser(string adminUserId, string email, string companyId, string newPassword)
        {
            return new Authenticator().UpdatePasswordForUser(adminUserId, email, companyId, newPassword);
        }

        public AuthenticationUpdateResponse ResetPasswordForEmail(string email)
        {
            return new Authenticator().ResetPasswordForEmail(email);
        }

        public AuthenticationCheckEmailForCompanyResponse CheckEmailExistsForCompany(string adminUserId, string companyId, string email)
        {
            return new Authenticator().CheckEmailExistsForCompany(adminUserId, companyId, email);
        }

        public AuthenticationUpdateResponse ResetPasswordForUser(string adminUserId, string companyId, string userId)
        {
            return new Authenticator().ResetPasswordForUser(adminUserId, companyId, userId);
        }

        public AuthenticationUpdateResponse ReinviteUser(string adminUserId, string companyId, string userId)
        {
            return new Authenticator().ReinviteUser(adminUserId, companyId, userId);
        }

        public AuthenticationUpdateResponse ReinviteUsers(string adminUserId, string companyId)
        {
            return new Authenticator().ReinviteUsers(adminUserId, companyId);
        }
        #endregion

        #region For Company
        public GetCompanyPermissionResponse GetCompanyPermission(string companyId, string adminUserId)
        {
            return new Company().GetCompanyPermission(companyId, adminUserId);
        }

        public CompanyUpdatePermissionResponse UpdateCompanyPermisson(string companyId, string adminUserId, int permisssionType, List<string> allowDepartmentId)
        {
            return new Company().UpdateCompanyPermisson(companyId, adminUserId, permisssionType, allowDepartmentId);
        }

        public CompanyUpdateProfileResponse UpdateProfile(string adminUserId, string companyId, string newTitle, string newPrimaryAdminUserId, string newPrimaryAdminSecondaryEmail, string supportMessage, string zoneName, double timezoneOffset)
        {
            return new Company().UpdateProfile(adminUserId, companyId, newTitle, newPrimaryAdminUserId, newPrimaryAdminSecondaryEmail, supportMessage, zoneName, timezoneOffset);
        }

        public CompanySelectProfileResponse SelectProfile(string adminUserId, string companyId)
        {
            return new Company().SelectProfile(adminUserId, companyId);
        }

        public CompanySelectImageResponse SelectImage(string adminUserId, string companyId, int imageType)
        {
            return new Company().SelectEmailLogo(adminUserId, companyId, imageType);
        }

        public CompanyUpdateProfileResponse UpdateImage(string adminUserId, string companyId, string imageUrl, int imageType)
        {
            return new Company().UpdateImage(adminUserId, companyId, imageUrl, imageType);
        }

        public CompanySelectEmailTemplateResponse SelectEmailTemplate(string adminUserId, string companyId)
        {
            return new Company().SelectEmailTemplate(adminUserId, companyId);
        }

        public CompanyUpdateProfileResponse UpdateEmailTemplate(string adminUserId, string companyId, int updateType, string emailTitle, string emailDescription, string emailSupportInfo)
        {
            return new Company().UpdateEmailTemplate(adminUserId, companyId, updateType, emailTitle, emailDescription, emailSupportInfo);
        }

        public CompanyResponse GetCompany(String companyId)
        {
            return new Company().GetCompany(companyId);
        }

        #endregion

        #region Preload Data
        public void WriteExpAToTable()
        {
            Analytic analytic = new Analytic();
            analytic.WriteExpAToTable();
        }

        public void AddCountryToTable()
        {
            new Country().AddCountryToTable();
        }

        public void AddCountryIpToTable()
        {
            new Country().AddCountryIpToTable();
        }

        public void AddTimezoneToTable()
        {
            new Country().AddTimezoneToTable();
        }
        #endregion

        #region Authenticator
        public LoginByManagerResponse LoginByManager(String email, String password)
        {
            return authenticator.LoginByManager(email, password);
        }

        public GetManagerInfoResponse GetManagerInfo(String managerId, String companyId)
        {
            return new Authenticator().GetManagerInfo(managerId, companyId);
        }

        public AccountKindListResponse GetAllAccountKind(String adminUserId, String companyId)
        {
            return accountKind.GetAllAccountKind(adminUserId, companyId);
        }

        public AccountKindListResponse GetAllAccountKind(String adminUserId, String companyId, Cassandra.ISession session)
        {
            return accountKind.GetAllAccountKind(adminUserId, companyId, session);
        }

        #endregion

        #region For Department
        public DepartmentDetailResponse GetDepartmentDetail(String adminUserId, String companyId, String departmentId)
        {
            return department.GetDepartmentDetail(adminUserId, companyId, departmentId, Department.QUERY_TYPE_DETAIL);
        }

        public DepartmentListResponse GetAllDepartment(String adminUserId, String companyId)
        {
            return department.GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_DETAIL);
        }

        public DepartmentListResponse GetAllDepartment(String adminUserId, String companyId, Cassandra.ISession session)
        {
            return department.GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_DETAIL, session);
        }

        public DepartmentCreateResponse CreateDepartment(String adminUserId, String companyId, String departmentTitle)
        {
            return department.Create(adminUserId, companyId, departmentTitle);
        }

        public DepartmentUpdateResponse UpdateDepartment(String adminUserId, String companyId, String departmentId, String departmentTitle)
        {
            return department.Update(adminUserId, companyId, departmentId, departmentTitle);
        }

        public DepartmentDeleteResponse DeleteDepartment(String adminUserId, String companyId, String departmentId)
        {
            return department.Delete(adminUserId, companyId, departmentId);
        }
        #endregion

        #region For User
        public UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode)
        {
            return new User().CreateAdmin(companyId, companyTitle, companyLogoUrl, adminUserId, plainPassword, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode);
        }
        public UserListResponse GetAllUser(String adminUserId, String companyId, String departmentId, int userTypeCode, int userStatusCode, List<String> searchKeys, bool isPendingInvite, bool isPendingLogin)
        {
            return user.GetAllUserForAdmin(adminUserId, companyId, departmentId, userTypeCode, userStatusCode, searchKeys, isPendingInvite, isPendingLogin);
        }

        public UserListResponse GetAdmin(String adminUserId, String companyId)
        {
            return user.GetAdmin(adminUserId, companyId);
        }

        public UserDetailResponse GetUserDetail(String adminUserId, String companyId, String userId)
        {
            return user.GetUserDetail(adminUserId, companyId, userId);
        }

        public UserCreateResponse CreateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, int gender, DateTime? birthday, bool isSendEmail = true)
        {
            return user.Create(adminUserId, companyId, userId, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentTitle, gender, birthday, isSendEmail);
        }

        public UserUpdateResponse UpdateUser(String adminUserId, String companyId, String userId, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode, String departmentTitle, int gender, DateTime? birthday)
        {
            return user.Update(adminUserId, companyId, userId, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode, departmentTitle, gender, birthday);
        }

        public UserUpdateStatusResponse UpdateUserStatus(String adminUserId, String companyId, String userId, int statusCode)
        {
            return user.UpdateStatus(adminUserId, companyId, userId, statusCode);
        }

        public UserUpdateTypeResponse UpdateUserType(String adminUserId, String companyId, String userId, int typeCode, List<int> accessModulesKey, DateTime? expiryDate)
        {
            return user.UpdateType(adminUserId, companyId, userId, typeCode, accessModulesKey, expiryDate);
        }

        public UserDeleteResponse Delete(string adminUserId, string companyId, string userId)
        {
            return user.Delete(adminUserId, companyId, userId);
        }

        public PostingSuspendedUserListResponse GetAllPostingSuspendedUser(String adminUserId, String companyId)
        {
            return new User().GetAllPostingSuspendedUser(adminUserId, companyId);
        }

        public UserListResponse SearchUserForPostingSuspend(String adminUserId, String companyId, String searchKey)
        {
            return new User().SearchUserForPostingSuspend(adminUserId, companyId, searchKey);
        }

        public PostingPermissionSuspendUserResponse PostingPermissionSuspendUsers(String adminUserId, String companyId, List<String> userIds)
        {
            return new User().PostingPermissionSuspendUsers(adminUserId, companyId, userIds);
        }

        public PostingPermissionSuspendUserResponse PostingPermissionSuspendUser(String adminUserId, String companyId, String userId)
        {
            return new User().PostingPermissionSuspendUser(adminUserId, companyId, userId);
        }

        public PostingPermissionRestoreUserResponse PostingPermissionResotreUser(String adminUserId, String companyId, String userId)
        {
            return new User().PostingPermissionRestoreUser(adminUserId, companyId, userId);
        }

        public ModeratorChangeRightsResponse ModeratorChangeRights(String adminUserId, String companyId, String userId, List<int> accessModulesKey, DateTime? expiryDate)
        {
            return new User().ModeratorChangeRights(adminUserId, companyId, userId, accessModulesKey, expiryDate);
        }

        public UserIdResponse GetUserId(string email, string company_id)
        {
            return user.GetUserId(email, company_id);
        }

        public BasicUserInfoResponse GetBasicUserInfo(string userId)
        {
            return user.GetBasicUserInfo(userId);
        }

        public UserAccountTypeResponse GetUserAccountType(string userId)
        {
            return user.GetUserAccountType(userId);
        }

        public ModeratorAccessRightsResponse GetModeratorAccessRights(String userId)
        {
            return user.GetModeratorAccessRights(userId);
        }

        #endregion

        #region Country
        public CountryListResponse GetAllCountry(String adminUserId, String companyId)
        {
            return country.GetAllCountry(adminUserId, companyId);
        }
        #endregion

        #region Feed

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId, string adminUserId)
        {
            return new Feed().DeleteFeed(feedId, companyId, ownerUserId, adminUserId);
        }
        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId, string adminUserId)
        {
            return new Feed().DeleteComment(feedId, commentId, companyId, commentorUserId, adminUserId);
        }
        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId, string adminUserId)
        {
            return new Feed().DeleteReply(feedId, commentId, replyId, companyId, commentorUserId, adminUserId);
        }

        #endregion

        #region Challenge

        public CategorySelectResponse GetCategoryDetail(string adminUserId, string companyId, string categoryId)
        {
            return new TopicCategory().SelectCategory(categoryId, companyId, adminUserId);
        }

        public TopicSelectIconResponse SelectAllTopicIcons(string adminUserId,
                                                           string companyId)
        {
            return new Topic().SelectAllTopicIcons(adminUserId, companyId);
        }

        public CategoryCreateResponse CreateCategory(String adminUserId, String companyId, String categoryTitle)
        {
            return new TopicCategory().Create(adminUserId, companyId, categoryTitle);
        }

        public CategoryUpdateResponse UpdateCategory(String adminUserId, String companyId, String categoryId, String categoryTitle)
        {
            return new TopicCategory().Update(adminUserId, companyId, categoryId, categoryTitle);
        }

        public CategoryDeleteResponse DeleteCategory(String adminUserId, String companyId, String categoryId)
        {
            return new TopicCategory().Delete(adminUserId, companyId, categoryId);
        }

        public List<Topic.TopicStatus> SelectTopicStatusForDropdown()
        {
            return new Topic().SelectTopicStatusForDropdown();
        }

        public List<ChallengeQuestion.QuestionStatus> SelectQuestionStatusForDropdown()
        {
            return new ChallengeQuestion().SelectQuestionStatusForDropdown();
        }

        public List<Topic.DropdownNumberOfQuestions> SelectNumberOfQuestionsForDropdown()
        {
            return new Topic().SelectNumberOfQuestionsForDropdown();
        }

        public CategorySelectAllResponse SelectAllCategories(string adminUserId, string companyId)
        {
            TopicCategory category = new TopicCategory();

            return category.SelectAllCategories(adminUserId, companyId);
        }

        public CategorySelectAllResponse SelectAllCategoriesForDropdown(string adminUserId, string companyId)
        {
            TopicCategory category = new TopicCategory();

            return category.SelectAllCategoriesForDropdown(adminUserId, companyId);
        }

        public TopicCreateResponse CreateTopic(string adminUserId,
                                               string companyId,
                                               string topicTitle,
                                               string topicLogoUrl,
                                               string topicDescription,
                                               string categoryId,
                                               string categoryTitle,
                                               List<string> targetedDepartmentIds,
                                               int numberOfSelectedQuestions,
                                               bool isForEveryone)
        {
            Topic topic = new Topic();
            return topic.CreateTopic(adminUserId, companyId, topicTitle, topicLogoUrl, topicDescription, categoryId, categoryTitle, targetedDepartmentIds, numberOfSelectedQuestions, isForEveryone);
        }

        public QuestionCreateResponse CreateNewQuestion(string adminUserId,
                                                        string companyId,
                                                        string questionId,
                                                        string topicId,
                                                        string categoryId,
                                                        int questionType,
                                                        string questionContent,
                                                        string questionContentImageUrl,
                                                        string questionContentImageMd5,
                                                        string questionContentImageBackgroundColorCode,
                                                        int choiceType,
                                                        string firstChoiceContent,
                                                        string firstChoiceImageUrl,
                                                        string firstChoiceImageMd5,
                                                        string secondChoiceContent,
                                                        string secondChoiceImageUrl,
                                                        string secondChoiceImageMd5,
                                                        string thirdChoiceContent,
                                                        string thirdChoiceImageUrl,
                                                        string thirdChoiceImageMd5,
                                                        string fourthChoiceContent,
                                                        string fourthChoiceImageUrl,
                                                        string fourthChoiceImageMd5,
                                                        float timeAssignedForReading,
                                                        float timeAssignedForAnswering,
                                                        int difficultyLevel,
                                                        int scoreMultiplier,
                                                        double frequency,
                                                        int questionStatus,
                                                        int serverMode)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.CreateNewQuestion(adminUserId,
                                             companyId,
                                             questionId,
                                             topicId,
                                             categoryId,
                                             questionType,
                                             questionContent,
                                             questionContentImageUrl,
                                             questionContentImageMd5,
                                             questionContentImageBackgroundColorCode,
                                             choiceType,
                                             firstChoiceContent,
                                             firstChoiceImageUrl,
                                             firstChoiceImageMd5,
                                             secondChoiceContent,
                                             secondChoiceImageUrl,
                                             secondChoiceImageMd5,
                                             thirdChoiceContent,
                                             thirdChoiceImageUrl,
                                             thirdChoiceImageMd5,
                                             fourthChoiceContent,
                                             fourthChoiceImageUrl,
                                             fourthChoiceImageMd5,
                                             timeAssignedForReading,
                                             timeAssignedForAnswering,
                                             difficultyLevel,
                                             scoreMultiplier,
                                             frequency,
                                             questionStatus,
                                             serverMode);
        }

        public TopicSelectAllBasicResponse SelectTopicBasicByCategoryAndDepartment(string adminUserId,
                                                                                   string companyId,
                                                                                   string selectedTopicCategoryId,
                                                                                   string selectedDepartmentId,
                                                                                   string containsName = null)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, selectedTopicCategoryId, selectedDepartmentId, containsName);
        }


        public TopicSelectResponse SelectTopicDetail(string topicId,
                                                     string adminUserId,
                                                     string companyId,
                                                     string searchQuestionContent = null)
        {
            Topic topic = new Topic();
            return topic.SelectTopicDetail(topicId,
                                           adminUserId,
                                           companyId,
                                           searchQuestionContent);
        }

        public QuestionSelectResponse SelectQuestion(string questionId, string topicId, string companyId, string adminUserId)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.SelectQuestion(questionId,
                                           topicId,
                                           companyId,
                                           adminUserId);
        }

        public TopicUpdateResponse UpdateTopicStatus(string adminUserId,
                                                     string companyId,
                                                     string topicId,
                                                     string categoryId,
                                                     int status)
        {
            Topic topic = new Topic();
            return topic.UpdateTopicStatus(adminUserId,
                                           companyId,
                                           topicId,
                                           categoryId,
                                           status);
        }

        public QuestionUpdateResponse UpdateQuestionStatus(string adminUserId,
                                                           string companyId,
                                                           string topicId,
                                                           string categoryId,
                                                           string questionId,
                                                           int status)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.UpdateQuestionStatus(adminUserId, companyId, topicId, categoryId, questionId, status);
        }

        public TopicUpdateResponse UpdateTopic(string adminUserId,
                                               string companyId,
                                               string topicId,
                                               string newTitle,
                                               string newLogoUrl,
                                               string newDescription,
                                               string newCategoryId,
                                               string newCategoryTitle,
                                               int newStatus,
                                               List<string> newTargetedDepartmentIds,
                                               int newNumberOfSelectedQuestions,
                                               bool isForEveryone)
        {
            Topic topic = new Topic();
            return topic.UpdateTopic(adminUserId, companyId, topicId, newTitle, newLogoUrl, newDescription, newCategoryId, newCategoryTitle, newStatus, newTargetedDepartmentIds, newNumberOfSelectedQuestions, isForEveryone);
        }

        public QuestionUpdateResponse UpdateQuestion(string adminUserId,
                                                    string companyId,
                                                    string questionId,
                                                    string topicId,
                                                    string categoryId,
                                                    int questionType,
                                                    string questionContent,
                                                    string questionContentImageUrl,
                                                    string questionContentImageMd5,
                                                    string questionContentImageBackgroundColorCode,
                                                    int choiceType,
                                                    string firstChoiceContent,
                                                    string firstChoiceImageUrl,
                                                    string firstChoiceImageMd5,
                                                    string secondChoiceContent,
                                                    string secondChoiceImageUrl,
                                                    string secondChoiceImageMd5,
                                                    string thirdChoiceContent,
                                                    string thirdChoiceImageUrl,
                                                    string thirdChoiceImageMd5,
                                                    string fourthChoiceContent,
                                                    string fourthChoiceImageUrl,
                                                    string fourthChoiceImageMd5,
                                                    float timeAssignedForReading,
                                                    float timeAssignedForAnswering,
                                                    int difficultyLevel,
                                                    int scoreMultiplier,
                                                    double frequency,
                                                    int questionStatus)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.UpdateQuestion(adminUserId, companyId, questionId, topicId, categoryId, questionType, questionContent, questionContentImageUrl, questionContentImageMd5, questionContentImageBackgroundColorCode, choiceType, firstChoiceContent, firstChoiceImageUrl, firstChoiceImageMd5, secondChoiceContent, secondChoiceImageUrl, secondChoiceImageMd5, thirdChoiceContent, thirdChoiceImageUrl, thirdChoiceImageMd5, fourthChoiceContent, fourthChoiceImageUrl, fourthChoiceImageMd5, timeAssignedForReading, timeAssignedForAnswering, difficultyLevel, scoreMultiplier, frequency, questionStatus);
        }
        #endregion

        #region For Module
        public GetModeratorModulesResponse GetModeratorModules()
        {
            GetModeratorModulesResponse response = new GetModeratorModulesResponse();
            response.Success = true;
            response.Modules = Module.GetModules(User.AccountType.CODE_MODERATER);
            return response;
        }
        #endregion

        #region Dashboard
        public DashboardSelectFeedbackResponse SelectFeedbackFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectFeedbackFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardSelectReportedFeedResponse SelectReportedFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectReportedFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardCreateFeedbackCommentResponse CreateFeedbackComment(string adminUserId,
                                                                            string companyId,
                                                                            string feedId,
                                                                            string content)
        {
            return new Dashboard().CreateFeedbackComment(adminUserId, companyId, feedId, content);
        }

        public DashboardUpdateFeedbackStatusResponse UpdateFeedbackPostStatus(string adminUserId, string companyId, string feedId, int status)
        {
            return new Dashboard().UpdateFeedbackPostStatus(adminUserId, companyId, feedId, status);
        }

        public DashboardSelectApprovalProfileResponse SelectProfileApprovalResponse(string adminUserId, string companyId)
        {
            return new Dashboard().SelectProfileApprovalResponse(adminUserId, companyId);
        }

        public DashboardUpdateApprovalProfileResponse UpdateProfileApprovalStatus(string adminUserId, string companyId, string approvalId, int newApprovalState)
        {
            return new Dashboard().UpdateProfileApprovalStatus(adminUserId, companyId, approvalId, newApprovalState);
        }

        public DashboardUpdateReportStatusResponse UpdateReportStatus(string adminUserId, string companyId, int reportType, string feedId, string reportPostId, int newStatus, string commentId = null, string replyId = null)
        {
            return new Dashboard().UpdateReportStatus(adminUserId, companyId, reportType, feedId, reportPostId, newStatus, commentId, replyId);
        }
        #endregion

        #region DAU
        public AnalyticsSelectBasicResponse SelectBasicReport(string adminUserId, string companyId)
        {
            return new Analytic().SelectBasicReport(adminUserId, companyId);
        }

        public AnalyticsSelectDetailDauResponse SelectDau(string adminUserId, string companyId, int dauType, int timeActivityFrameType)
        {
            return new Analytic().SelectDau(adminUserId, companyId, dauType, timeActivityFrameType);
        }
        #endregion

        #region Event
        public EventCreateResponse CreateEvent(string adminUserId,
                                               string companyId,
                                               string title,
                                               string description,
                                               DateTime startTimestamp,
                                               DateTime endTimestamp,
                                               int eventType,
                                               int participantType,
                                               int bannerPrivacyType,
                                               DateTime bannerPrivacyDate,
                                               int scoringType,
                                               int calculationType,
                                               bool isDisplayResult,
                                               int extensionDaysForResult,
                                               int numberOfLeaderboard,
                                               string backgroundImageUrl,
                                               List<string> topicIds,
                                               List<string> departmentIds = null,
                                               List<string> userIds = null)
        {
            return new Event().Create(adminUserId, companyId, title, description, startTimestamp, endTimestamp, eventType, participantType, bannerPrivacyType, bannerPrivacyDate, scoringType, calculationType, isDisplayResult, extensionDaysForResult, numberOfLeaderboard, backgroundImageUrl, topicIds, departmentIds, userIds);
        }

        public EventSelectAllResponse SelectAllEvents(string adminUserId, string companyId, int eventStatus)
        {
            return new Event().SelectAllBasic(adminUserId, companyId, eventStatus);
        }

        public EventSelectResponse SelectEvent(string adminUserId,
                                               string eventId,
                                               string companyId)
        {
            return new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL);
        }

        public EventSelectTopicResponse SelectTopicsForEvent(string adminUserId, string companyId, int eventParticipantType, List<string> selectedUserIds, List<string> selectedDepartmentIds)
        {
            return new Event().SelectTopicsForEvent(adminUserId, companyId, eventParticipantType, selectedUserIds, selectedDepartmentIds);
        }

        public EventSelectDepartmentResponse SelectDepartmentsForEvent(string adminUserId, string companyId, List<string> selectedTopicIds)
        {
            return new Event().SelectDepartmentsForEvent(adminUserId, companyId, selectedTopicIds);
        }

        public EventSelectUserResponse SelectUsersForEvent(string adminUserId, string companyId, List<string> selectedTopicIds)
        {
            return new Event().SelectUsersForEvent(adminUserId, companyId, selectedTopicIds);
        }

        public EventUpdateResponse UpdateEvent(string eventId,
                                               string adminUserId,
                                               string companyId,
                                               string newTitle,
                                               string newDescription,
                                               DateTime newStartTimestamp,
                                               DateTime newEndTimestamp,
                                               int newEventType,
                                               int newParticipantType,
                                               int newBannerPrivacyType,
                                               DateTime newBannerVisibleDate,
                                               int newScoringType,
                                               int newCalculationType,
                                               bool newIsDisplayResult,
                                               int newExtensionDaysForResult,
                                               int newNumberOfLeaderboard,
                                               string newBackgroundImageUrl,
                                               List<string> newTopicIds,
                                               List<string> newDepartmentIds = null,
                                               List<string> newUserIds = null)
        {
            return new Event().Update(eventId, adminUserId, companyId, newTitle, newDescription, newStartTimestamp, newEndTimestamp, newEventType, newParticipantType, newBannerPrivacyType, newBannerVisibleDate, newScoringType, newCalculationType, newIsDisplayResult, newExtensionDaysForResult, newNumberOfLeaderboard, newBackgroundImageUrl, newTopicIds, newDepartmentIds, newUserIds);
        }

        public EventUpdateResponse SuspendEvent(string adminUserId, string companyId, string eventId)
        {
            return new Event().SuspendEvent(adminUserId, companyId, eventId);
        }

        public EventUpdateResponse RestoreEvent(string adminUserId, string companyId, string eventId)
        {
            return new Event().RestoreEvent(adminUserId, companyId, eventId);
        }

        public EventUpdateResponse DeleteEvent(string adminUserId, string companyId, string eventId)
        {
            return new Event().DeleteEvent(adminUserId, companyId, eventId);
        }
        #endregion


        #region Responsive Survey
        public RSCategoryCreateResponse CreateRSCategory(string companyId, string title, string adminUserId)
        {
            return new RSTopicCategory().CreateRSCategory(companyId, title, adminUserId);
        }
        public RSCategoryUpdateResponse UpdateRSCategory(string adminUserId, string companyId, string categoryId, string newTitle)
        {
            return new RSTopicCategory().UpdateRSCategory(adminUserId, companyId, categoryId, newTitle);
        }

        public RSCategoryUpdateResponse DeleteRSCategory(string adminUserId, string companyId, string categoryId)
        {
            return new RSTopicCategory().DeleteRSCategory(adminUserId, companyId, categoryId);
        }
        public RSTopicCreateResponse CreateRSTopic(string adminUserId,
                                                   string companyId,
                                                   string title,
                                                   string introduction,
                                                   string closingWords,
                                                   string categoryId,
                                                   string categoryTitle,
                                                   string topicIconUrl,
                                                   int status,
                                                   List<string> targetedDepartmentIds,
                                                   List<string> targetedUserIds,
                                                   bool isRandomizedAllQuestion,
                                                   bool isShowProgressBar,
                                                   bool isAllowReturnPrevious,
                                                   bool isAllowFeedback,
                                                   bool isAllowViewHistory)
        {
            return new RSTopic().CreateRSTopic(adminUserId, companyId, title, introduction, closingWords, categoryId, categoryTitle, topicIconUrl, status, targetedDepartmentIds, targetedUserIds, isRandomizedAllQuestion, isShowProgressBar, isAllowReturnPrevious, isAllowFeedback, isAllowViewHistory);
        }

        public RSTopicUpdateResponse UpdateRSTopic(string topicId,
                                                 string adminUserId,
                                                 string companyId,
                                                 string newTitle,
                                                 string newIntroduction,
                                                 string newClosingWords,
                                                 string newCategoryId,
                                                 string newCategoryTitle,
                                                 string newTopicIconUrl,
                                                 int newStatus,
                                                 List<string> newTargetedDepartmentIds,
                                                 List<string> newTargetedUserIds,
                                                 bool newIsRandomizedAllQuestion,
                                                 bool newIsShowProgressBar,
                                                 bool newIsAllowReturnPrevious,
                                                 bool newIsAllowFeedback,
                                                 bool newIsAllowViewHistory)
        {
            return new RSTopic().UpdateTopic(topicId, adminUserId, companyId, newTitle, newIntroduction, newClosingWords, newCategoryId, newCategoryTitle, newTopicIconUrl, newStatus, newTargetedDepartmentIds, newTargetedUserIds, newIsRandomizedAllQuestion, newIsShowProgressBar, newIsAllowReturnPrevious, newIsAllowFeedback, newIsAllowViewHistory);
        }

        public RSTopicUpdateResponse UpdateRSTopicStatus(string topicId, string categoryId, string adminUserId, string companyId, int updatedStatus)
        {
            return new RSTopic().UpdateTopicStatus(topicId, categoryId, adminUserId, companyId, updatedStatus);
        }

        public RSCategorySelectAllResponse SelectAllRSCategories(string adminUserId, string companyId, int queryType)
        {
            return new RSTopicCategory().SelectAllRSCategories(adminUserId, companyId, queryType);
        }

        public RSTopicSelectAllBasicResponse SelectAllRSTopicByCategory(string adminUserId, string companyId, string selectedCategoryId, string containsName)
        {
            return new RSTopic().SelectAllBasicByCategory(adminUserId, companyId, selectedCategoryId, containsName);
        }

        public RSCardCreateResponse CreateCard(string cardId,
                                               int type,
                                               string content,
                                               bool hasImage,
                                               List<RSImage> images,
                                               bool toBeSkipped,
                                               bool hasCustomAnswer,
                                               string customAnswerInstruction,
                                               bool isOptionRandomized,
                                               bool allowMultipleLines,
                                               bool hasPageBreak,
                                               int backgroundType,
                                               string note,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               List<RSOption> options,
                                               int miniOptionToSelect,
                                               int maxOptionToSelect,
                                               int maxRange,
                                               int minRange)
        {
            return new RSCard().CreateCard(cardId, type, content, hasImage, images, toBeSkipped, hasCustomAnswer, customAnswerInstruction, isOptionRandomized, allowMultipleLines, hasPageBreak, backgroundType, note, categoryId, topicId, adminUserId, companyId, options, miniOptionToSelect, maxOptionToSelect, maxRange, minRange);
        }

        public RSCardUpdateResponse UpdateCard(string cardId,
                                               string content,
                                               bool hasImage,
                                               List<RSImage> images,
                                               string note,
                                               bool hasPageBreak,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               List<RSOption> options,
                                               int miniOptionToSelect,
                                               int maxOptionToSelect)
        {
            return new RSCard().UpdateCard(cardId, content, hasImage, images, note, hasPageBreak, options, categoryId, topicId, adminUserId, companyId, miniOptionToSelect, maxOptionToSelect);
        }

        public RSCardUpdateResponse DeleteCard(string topicId, string categoryId, string cardId, string adminUserId, string companyId)
        {
            return new RSCard().DeleteCard(topicId, categoryId, cardId, adminUserId, companyId);
        }

        public RSTopicSelectResponse SelectFullDetailRSTopic(string adminUserId, string companyId, string topicId, string categoryId, string containsCardName = null)
        {
            return new RSTopic().SelectFullDetailRSTopic(adminUserId, companyId, topicId, categoryId, containsCardName);
        }

        public RSCardSelectResponse SelectFullDetailCard(string adminUserId, string companyId, string cardId, string topicId)
        {
            return new RSCard().SelectCard(adminUserId, companyId, cardId, topicId, (int)RSCard.RSCardQueryType.FullDetail);
        }

        public RSCardSelectAllResponse Preview(string topicId, string categoryId, string adminUserId, string companyId)
        {
            return new RSCard().SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)RSCard.RSCardQueryType.FullDetail);
        }

        public RSCardReorderResponse ReorderCard(string cardId, string topicId, string categoryId, string adminUserId, string companyId, int insertAtOrder)
        {
            return new RSCard().ReorderCard(cardId, topicId, categoryId, adminUserId, companyId, insertAtOrder);
        }

        public RSOptionSelectLogicResponse SelectLogicToNextCard(string topicId, string categoryId, string currentCardId, string adminUserId, string companyId)
        {
            return new RSOption().SelectLogicToNextCard(topicId, categoryId, currentCardId, adminUserId, companyId);
        }

        public AnalyticSelectRSResultOverviewResponse SelectResultOverview(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().SelectResultOverview(adminUserId, companyId, topicId, categoryId);
        }

        public AnalyticSelectRSCardResultResponse SelectCardResult(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSCard().SelectResult(adminUserId, companyId, topicId, categoryId);
        }

        public AnalyticSelectRSCustomAnswersResponse SelectCustomAnswersFromCard(string adminUserId, string companyId, string topicId, string categoryId, string cardId)
        {
            return new RSCard().SelectCustomAnswersFromCard(adminUserId, companyId, topicId, categoryId, cardId);
        }

        public AnalyticSelectRSResponderReportResponse SelectRespondersReport(string adminUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().SelectRespondersReport(adminUserId, companyId, topicId, categoryId);
        }

        public AnalyticSelectRSCardResultByUserResponse SelectCardResultByUser(string adminUserId, string companyId, string topicId, string categoryId, string answeredByUserId)
        {
            return new RSCard().SelectResultByUser(adminUserId, companyId, topicId, categoryId, answeredByUserId);
        }

        #endregion

        #region Update Script
        public UpdateScriptResponse InsertValidStatusColumnToFeed(string adminUserId, string companyId)
        {
            return new UpdateScript().InsertValidStatusColumnToFeed(adminUserId, companyId);
        }

        public UpdateScriptResponse InsertNewColumnsAndTablesToChallenge()
        {
            return new UpdateScript().InsertNewColumnsAndTablesToChallenge();
        }

        public UpdateScriptResponse InsertTableChallengeByTopic()
        {
            return new UpdateScript().InsertTableChallengeByTopic();
        }

        public UpdateScriptResponse CreateNewTablesForUserToken()
        {
            return new UpdateScript().CreateNewTablesForUserToken();
        }

        public UpdateScriptResponse CreateNewTablesForNotification()
        {
            return new UpdateScript().CreateNewTablesForNotification();
        }

        public UpdateScriptResponse CreateNewColumnsForUser()
        {
            return new UpdateScript().CreateNewColumnsForUser();
        }

        public UpdateScriptResponse CreateNewColumnsForCompany()
        {
            return new UpdateScript().CreateNewColumnsForCompany();
        }
        #endregion

        #region CleanUp
        public UserCleanUpResponse RemoveRecentlyDeletedUsers()
        {
            return new User().RemoveRecentlyDeletedUsers();
        }
        #endregion
    }
}
