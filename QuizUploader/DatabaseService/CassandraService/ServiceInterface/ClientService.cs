﻿using CassandraService.Entity;
using CassandraService.ServiceResponses;
using System;
using System.Collections.Generic;

namespace CassandraService.ServiceInterface
{
    public class ClientService
    {
        #region Admin
        public UserCreateResponse CreateAdmin(String companyId, String companyTitle, String companyLogoUrl, String adminUserId, String plainPassword, String firstName, String lastName, String email, String profileImageUrl, String position, String phoneNumber, String phoneCountryCode, String phoneCountryName, String address, String addressCountryName, String postalCode)
        {
            return new User().CreateAdmin(companyId, companyTitle, companyLogoUrl, adminUserId, plainPassword, firstName, lastName, email, profileImageUrl, position, phoneNumber, phoneCountryCode, phoneCountryName, address, addressCountryName, postalCode);
        }
        #endregion

        #region Preload Data
        public void WriteExpAToTable()
        {
            Analytic analytic = new Analytic();
            analytic.WriteExpAToTable();
        }

        public void AddCountryToTable()
        {
            new Country().AddCountryToTable();
        }

        public void AddCountryIpToTable()
        {
            new Country().AddCountryIpToTable();
        }

        public void AddTimezoneToTable()
        {
            new Country().AddTimezoneToTable();
        }
        #endregion

        #region Device Token
        public UserUpdateDeviceTokenResponse UpdateUserDeviceToken(string userId,
                                                                   string companyId,
                                                                   string deviceToken,
                                                                   int deviceType)
        {
            User user = new User();
            return user.UpdateUserDeviceToken(userId, companyId, deviceToken, deviceType);
        }
        #endregion

        #region Authentication
#warning Deprecated code in AuthenticateUserForLogin, replaced by AuthenticateEmailForLogin
        public AuthenticationSelectUserResponse AuthenticateUserForLogin(string email,
                                                                         string encryptedPassword)
        {
            Authenticator authenticator = new Authenticator();
            return authenticator.SelectAuthenticatedUser(email, encryptedPassword);
        }

        public AuthenticationSelectUserResponse AuthenticateEmailForLogin(string email,
                                                                          string encryptedPassword)
        {
            return new Authenticator().SelectAuthenticatedEmailWithAllCompanies(email, encryptedPassword);
        }

        public AuthenticationSelectUserByCompanyResponse AuthenticateLoginByCompany(string loginCompanyId,
                                                                           string loginUserId,
                                                                           string currentAuthenticationToken,
                                                                           string encryptedPassword)
        {
            return new Authenticator().AuthenticateLoginByCompany(loginCompanyId, loginUserId, currentAuthenticationToken, encryptedPassword);
        }

#warning Deprecated code in SelectUserWithTokenAndCompany, replaced by SelectUserWithCompany
        public UserSelectTokenWithCompanyResponse SelectUserTokenWithCompany(string userId,
                                                                             string companyId)
        {
            User user = new User();
            return user.SelectUserWithTokenAndCompany(userId, companyId);
        }

        public UserSelectWithCompanyResponse SelectUserWithCompany(string userId, string companyId)
        {
            return new User().SelectUserWithCompany(userId, companyId);
        }

        public AuthenticationUpdateResponse UpdatePasswordForUser(string requesterUserId, string email, string companyId, string newPassword)
        {
            return new Authenticator().UpdatePasswordForUser(requesterUserId, email, companyId, newPassword);
        }

        public AuthenticationUpdateResponse ResetPasswordForEmail(string email)
        {
            return new Authenticator().ResetPasswordForEmail(email);
        }

        public UserTokenValidityResponse CheckUserTokenValidity(string userToken, string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return new Authenticator().CheckUserTokenValidity(userToken);
            }
            return new Authenticator().CheckUserTokenValidity(userToken, userId);
        }

        public AuthenticationUpdateResponse ResetPasswordWithToken(string token)
        {
            return new Authenticator().ResetPasswordWithToken(token);
        }

        public AuthenticationSelectCompanyResponse SwitchCompanyByUser(string requesterUserId, string fromCompanyId)
        {
            return new Authenticator().SwitchCompanyByUser(requesterUserId, fromCompanyId);
        }

        #endregion

        #region Colleagues
        public UserSelectAllByDepartmentResponse SelectAllUsersByDepartment(string requesterUserId, string companyId)
        {
            User user = new User();
            return user.SelectAllUsersSortedByDepartment(requesterUserId, companyId);
        }
        #endregion

        #region DAU
        public void UpdateUserActivity(string userId,
                                       string companyId,
                                       bool isActive)
        {
            Analytic analytic = new Analytic();
            analytic.UpdateUserActivity(userId, companyId, isActive);
        }

        public UserUpdateLoginResponse UpdateUserLogin(string userId,
                                                       string companyId,
                                                       bool isLogin)
        {
            Analytic analytic = new Analytic();
            return analytic.UpdateUserLogin(userId, companyId, isLogin);
        }

        public AnalyticsUpdateDailyActiveUserResponse UpdateDailyActiveUser(DateTime today)
        {
            return new Analytic().UpdateDailyActiveUser(today);
        }

        public AnalyticsSelectBasicResponse SelectBasicReport(string adminUserId, string companyId)
        {
            return new Analytic().SelectBasicReport(adminUserId, companyId);
        }

        public AnalyticsSelectDetailDauResponse SelectDau(string adminUserId, string companyId, int dauType, int timeActivityFrameType)
        {
            return new Analytic().SelectDau(adminUserId, companyId, dauType, timeActivityFrameType);
        }
        #endregion

        #region Brain
        public BrainSelectResponse SelectBrain(string requesterUserId, string companyId)
        {
            return new Brain().SelectBrain(requesterUserId, companyId);
        }

        public UserSelectStatsResponse SelectStatsForUser(string requesterUserId, string companyId)
        {
            return new Analytic().SelectStatsForUser(requesterUserId, companyId);
        }
        #endregion

        #region Feed
        public FeedAuthenticationResponse AuthenticateUserForPostingFeed(string companyId,
                                                                         string userId)
        {
            Feed feed = new Feed();
            FeedAuthenticationResponse response = feed.AuthenticateUserForPostingFeed(companyId,
                                                                                      userId);


            return response;
        }

        public FeedCreateResponse CreateFeedTextPost(string feedId,
                                                     string creatorUserId,
                                                     string companyId,
                                                     string content,
                                                     List<string> targetedDepartmentIds,
                                                     List<string> targetedUserIds,
                                                     bool isForAdmin)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedTextPost(feedId,
                                                                  creatorUserId,
                                                                  companyId,
                                                                  content,
                                                                  targetedDepartmentIds,
                                                                  targetedUserIds,
                                                                  isForAdmin);


            return response;
        }

        public FeedCreateResponse CreateFeedImagePost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      List<string> imageUrls,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      bool isForAdmin)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedImagePost(feedId,
                                                                    creatorUserId,
                                                                    companyId,
                                                                    caption,
                                                                    imageUrls,
                                                                    targetedDepartmentIds,
                                                                    targetedUserIds,
                                                                    isForAdmin);


            return response;
        }

        public FeedCreateResponse CreateFeedVideoPost(string feedId,
                                                      string creatorUserId,
                                                      string companyId,
                                                      string caption,
                                                      string videoUrl,
                                                      string videoThumbnailUrl,
                                                      List<string> targetedDepartmentIds,
                                                      List<string> targetedUserIds,
                                                      bool isForAdmin)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedVideoPost(feedId,
                                                                    creatorUserId,
                                                                    companyId,
                                                                    caption,
                                                                    videoUrl,
                                                                    videoThumbnailUrl,
                                                                    targetedDepartmentIds,
                                                                    targetedUserIds,
                                                                    isForAdmin);

            return response;
        }

        public FeedCreateResponse CreateFeedSharedUrlPost(string feedId,
                                                          string creatorUserId,
                                                          string companyId,
                                                          string caption,
                                                          string url,
                                                          string urlTitle,
                                                          string urlDescription,
                                                          string urlSiteName,
                                                          string urlImageUrl,
                                                          List<string> targetedDepartmentIds,
                                                          List<string> targetedUserIds,
                                                          bool isForAdmin)
        {
            Feed feed = new Feed();
            FeedCreateResponse response = feed.CreateFeedSharedUrlPost(feedId,
                                                                       creatorUserId,
                                                                       companyId,
                                                                       caption,
                                                                       url,
                                                                       urlTitle,
                                                                       urlDescription,
                                                                       urlSiteName,
                                                                       urlImageUrl,
                                                                       targetedDepartmentIds,
                                                                       targetedUserIds,
                                                                       isForAdmin);

            return response;
        }

        public CommentCreateResponse CreateCommentText(string creatorUserId,
                                                       string companyId,
                                                       string feedId,
                                                       string content)
        {
            Feed feed = new Feed();
            CommentCreateResponse response = feed.CreateCommentText(creatorUserId,
                                                                     companyId,
                                                                     feedId,
                                                                     content);

            return response;
        }

        public ReplyCreateResponse CreateReplyText(string creatorUserId,
                                                   string companyId,
                                                   string feedId,
                                                   string commentId,
                                                   string content)
        {
            Feed feed = new Feed();
            ReplyCreateResponse response = feed.CreateReplyText(creatorUserId,
                                                                companyId,
                                                                feedId,
                                                                commentId,
                                                                content);

            return response;
        }


        public FeedSelectResponse SelectCompanyFeedPost(string requesterUserId,
                                                        string companyId,
                                                        string searchContent,
                                                        int numberOfPostsLoaded,
                                                        DateTime? newestTimestamp,
                                                        DateTime? oldestTimestamp)
        {
            Feed feed = new Feed();
            FeedSelectResponse response = feed.SelectCompanyFeedPost(requesterUserId,
                                                                      companyId,
                                                                      searchContent,
                                                                      numberOfPostsLoaded,
                                                                      newestTimestamp,
                                                                      oldestTimestamp);

            return response;
        }

        public FeedSelectResponse SelectPersonnelFeedPost(string requesterUserId,
                                                          string ownerUserId,
                                                          string companyId,
                                                          string searchContent,
                                                          DateTime? newestTimestamp,
                                                          DateTime? oldestTimestamp)
        {
            Feed feed = new Feed();
            FeedSelectResponse response = feed.SelectPersonnelFeedPost(requesterUserId,
                                                                        ownerUserId,
                                                                        companyId,
                                                                        searchContent,
                                                                        newestTimestamp,
                                                                        oldestTimestamp);

            return response;
        }

        public CommentSelectResponse SelectFeedComment(string requesterUserId,
                                                       string feedId,
                                                       string companyId)
        {
            Feed feed = new Feed();
            CommentSelectResponse response = feed.SelectFeedComment(requesterUserId,
                                                                     feedId,
                                                                     companyId);

            return response;
        }

        public ReplySelectResponse SelectCommentReply(string requesterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId)
        {
            Feed feed = new Feed();
            ReplySelectResponse response = feed.SelectCommentReply(requesterUserId,
                                                                    feedId,
                                                                    commentId,
                                                                    companyId);

            return response;
        }

        public PointUpdateResponse UpdateFeedPoint(string voterUserId,
                                                   string feedId,
                                                   string companyId,
                                                   bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateFeedPoint(voterUserId,
                                                                feedId,
                                                                companyId,
                                                                isUpVote);

            return response;

        }

        public PointUpdateResponse UpdateCommentPoint(string voterUserId,
                                                      string feedId,
                                                      string commentId,
                                                      string companyId,
                                                      bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateCommentPoint(voterUserId,
                                                                   feedId,
                                                                   commentId,
                                                                   companyId,
                                                                   isUpVote);

            return response;
        }

        public PointUpdateResponse UpdateReplyPoint(string voterUserId,
                                                    string feedId,
                                                    string commentId,
                                                    string replyId,
                                                    string companyId,
                                                    bool isUpVote)
        {
            Feed feed = new Feed();
            PointUpdateResponse response = feed.UpdateReplyPoint(voterUserId,
                                                                  feedId,
                                                                  commentId,
                                                                  replyId,
                                                                  companyId,
                                                                  isUpVote);

            return response;
        }

        public FeedDeleteResponse DeleteFeed(string feedId, string companyId, string ownerUserId)
        {
            return new Feed().DeleteFeed(feedId, companyId, ownerUserId, null);
        }

        public FeedDeleteResponse DeleteComment(string feedId, string commentId, string companyId, string commentorUserId)
        {
            return new Feed().DeleteComment(feedId, commentId, companyId, commentorUserId, null);
        }

        public FeedDeleteResponse DeleteReply(string feedId, string commentId, string replyId, string companyId, string commentorUserId)
        {
            return new Feed().DeleteReply(feedId, commentId, replyId, companyId, commentorUserId, null);
        }

        public FeedSelectPrivacyResponse SelectFeedPrivacy(string requesterUserId,
                                                           string companyId,
                                                           string feedId)
        {
            return new Feed().SelectFeedPrivacy(requesterUserId, companyId, feedId);
        }

        public FeedUpdateResponse UpdateToFeedTextPost(string ownerUserId,
                                                       string companyId,
                                                       string currentFeedId,
                                                       string content,
                                                       List<string> targetedDepartmentIds,
                                                       List<string> targetedUserIds)
        {
            return new Feed().UpdateToFeedTextPost(ownerUserId, companyId, currentFeedId, content, targetedDepartmentIds, targetedUserIds);
        }

        public FeedUpdateResponse UpdateToFeedImagePost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> updatedImageUrls,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds)
        {

            return new Feed().UpdateToFeedImagePost(ownerUserId, companyId, currentFeedId, caption, updatedImageUrls, targetedDepartmentIds, targetedUserIds);
        }

        public FeedUpdateResponse UpdateToFeedVideoPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds)
        {
            return new Feed().UpdateToFeedVideoPost(ownerUserId, companyId, currentFeedId, caption, targetedDepartmentIds, targetedUserIds);
        }

        public FeedUpdateResponse UpdateToSharedUrlPost(string ownerUserId,
                                                        string companyId,
                                                        string currentFeedId,
                                                        string caption,
                                                        List<string> targetedDepartmentIds,
                                                        List<string> targetedUserIds)
        {
            return new Feed().UpdateToSharedUrlPost(ownerUserId, companyId, currentFeedId, caption, targetedUserIds, targetedUserIds);
        }
        #endregion

        #region Challenge
        public ChallengeInvalidateResponse InvalidateChallenge(string userId, string companyId, string challengeId, int reason)
        {
            return new Topic().InvalidateChallenge(userId, companyId, challengeId, reason);
        }

        public CategorySelectAllWithTopicResponse SelectAllTopicBasicByUserAndCategory(string requesterUserId,
                                                                                       string companyId)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByUserAndCategory(requesterUserId, companyId);
        }

        public TopicSelectAllBasicResponse SelectAllTopicBasicByOpponent(string initiatorUserId,
                                                                         string challengedUserId,
                                                                         string companyId,
                                                                         string topicStartsWithName)
        {
            Topic topic = new Topic();
            return topic.SelectAllTopicBasicByOpponent(initiatorUserId, challengedUserId, companyId, topicStartsWithName);
        }

        public UserSelectAllBasicResponse SelectAllUsersByTopicId(string topicId, string categoryId, string requesterUserId, string companyId, string startsWithName)
        {
            User user = new User();
            return user.SelectAllUsersByTopicId(topicId, categoryId, requesterUserId, companyId, startsWithName);
        }

        public ChallengeCreateResponse CreateChallengeWithTopicId(string companyId,
                                                                  string initiatorUserId,
                                                                  string challengedUserId,
                                                                  string topicId,
                                                                  string categoryId)
        {
            Topic topic = new Topic();
            return topic.CreateChallengeWithTopicId(companyId, initiatorUserId, challengedUserId, topicId, categoryId);
        }

        public QuestionSelectAllResponse SelectAllQuestionsForChallenge(string requesterUserId,
                                                                        string companyId,
                                                                        string challengeId)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.SelectAllQuestionsForChallenge(requesterUserId, companyId, challengeId);
        }

        public QuestionAnswerResponse AnswerChallengeQuestion(string challengeId,
                                                              string companyId,
                                                              string userId,
                                                              string answer,
                                                              float timeTaken,
                                                              string questionId,
                                                              string topicId,
                                                              int round)
        {
            ChallengeQuestion question = new ChallengeQuestion();
            return question.AnswerChallengeQuestion(challengeId,
                                                    companyId,
                                                    userId,
                                                    answer,
                                                    timeTaken,
                                                    questionId,
                                                    topicId,
                                                    round);
        }

        public ChallengeSelectResponse SelectChallengeWithChallengeId(string companyId,
                                                                      string challengeId,
                                                                      string requesterUserId)
        {
            Topic topic = new Topic();
            return topic.SelectChallengeWithChallengeId(companyId, challengeId, requesterUserId);
        }

        public ChallengeStartWithoutOpponentResponse StartChallengeWithoutOpponent(string challengeId, string requesterUserId, string companyId)
        {
            Topic topic = new Topic();
            return topic.StartChallengeWithoutOpponent(challengeId, requesterUserId, companyId);
        }

        public ChallengeOfflineSelectOpponentAnswerResponse SelectOpponentAnswerForOfflineGame(string challengeId, string requesterUserId, string companyId, int round)
        {
            Topic topic = new Topic();
            return topic.SelectOpponentAnswerForOfflineGame(challengeId, requesterUserId, companyId, round);
        }

        public ChallengeIsReadyResponse SetPlayerReadyForChallenge(string requesterUserId,
                                                                   string companyId,
                                                                   string challengeId)
        {
            Topic topic = new Topic();
            return topic.SetPlayerReadyForChallenge(requesterUserId, companyId, challengeId);
        }
        #endregion

        #region Responsive Survey
        public RSTopicSelectResponse SelectRSTopicByUser(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().SelectRSTopicByUser(requesterUserId, companyId, topicId, categoryId);
        }


        public RSCardAnswerResponse AnswerCard(string answeredByUserId, string companyId, string topicId, string topicCategoryId, string cardId, int rangeSelected, List<string> optionIds, RSOption customAnswer)
        {
            return new RSCard().AnswerCard(answeredByUserId, companyId, topicId, topicCategoryId, cardId, rangeSelected, optionIds, customAnswer);
        }

        public RSUpdateActivityResponse UpdateBounceActivity(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().UpdateBounceActivity(requesterUserId, companyId, topicId, categoryId);
        }

        public RSUpdateActivityResponse UpdateCompletionActivity(string requesterUserId, string companyId, string topicId, string categoryId)
        {
            return new RSTopic().UpdateCompletionActivity(requesterUserId, companyId, topicId, categoryId);
        }

        #endregion

        #region Leaderboard
        public AnalyticsSelectLeaderboardByCompanyResponse SelectLeaderboardByCompany(string requesterUserId, string companyId)
        {
            Analytic analytic = new Analytic();
            return analytic.SelectLeaderboardByCompany(requesterUserId, companyId);
        }

        public AnalyticsSelectLeaderboardByDepartmentResponse SelectLeaderboardByDepartment(string requesterUserId, string companyId)
        {
            Analytic analytic = new Analytic();
            return analytic.SelectLeaderboardByDepartment(requesterUserId, companyId);
        }
        #endregion

        #region Challenge stats
        public ChallengeSelectStatsResponse SelectStatsForChallenge(string topicId, string requesterUserId, string opponentUserId, string companyId)
        {
            Analytic analytic = new Analytic();
            return analytic.SelectStatsForChallenge(topicId, requesterUserId, opponentUserId, companyId);
        }

        public AnalyticsSelectAllGameHistoriesResponse SelectAllGameHistoriesForUser(string requesterUserId,
                                                                                string companyId,
                                                                                int numberOfHistoryLoaded,
                                                                                DateTime? newestTimestamp,
                                                                                DateTime? oldestTimestamp)
        {
            Analytic analytic = new Analytic();
            return analytic.SelectAllGameHistoriesForUser(requesterUserId, companyId, numberOfHistoryLoaded, newestTimestamp, oldestTimestamp);
        }

        public AnalyticsSelectGameResultResponse SelectGameResultForChallenge(string requesterUserId,
                                                                              string companyId,
                                                                              string challengeId)
        {
            Analytic analytic = new Analytic();
            return analytic.SelectGameResultForChallenge(requesterUserId, companyId, challengeId);
        }
        #endregion

        #region Notification
        public NotificationSelectResponse SelectNotificationByUser(string requesterUserId,
                                                                   string companyId,
                                                                   int numberOfPostsLoaded,
                                                                   DateTime? newestTimestamp,
                                                                   DateTime? oldestTimestamp)
        {
            return new Notification().SelectNotificationByUser(requesterUserId, companyId, numberOfPostsLoaded, newestTimestamp, oldestTimestamp);
        }

        public NotificationSelectNumberResponse SelectNotificationNumberByUser(string currentUserId,
                                                                               string companyId)
        {
            return new Notification().SelectNotificationNumberByUser(currentUserId, companyId, null);
        }

        public NotificationUpdateSeenResponse UpdateSeenNotification(string currentUserId,
                                                                     string companyId,
                                                                     int notificationType,
                                                                     int notificationSubType,
                                                                     string challengeId,
                                                                     string feedId,
                                                                     string commentId,
                                                                     string replyId,
                                                                     string notificationId)
        {
            return new Notification().UpdateSeenNotification(currentUserId, companyId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId);
        }
        #endregion

        #region Dashboard
        public DashboardReportResponse ReportFeed(string reportedUserId, string reason, string companyId, string feedId, string commentId, string replyId, int reportType)
        {
            return new Dashboard().ReportFeed(reportedUserId, reason, companyId, feedId, commentId, replyId, reportType);
        }

        public DashboardSelectFeedbackResponse SelectFeedbackFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectFeedbackFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardSelectReportedFeedResponse SelectReportedFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            return new Dashboard().SelectReportedFeedPosts(adminUserId, companyId, reportedState);
        }

        public DashboardCreateFeedbackCommentResponse CreateFeedbackComment(string adminUserId,
                                                                            string companyId,
                                                                            string feedId,
                                                                            string content)
        {
            return new Dashboard().CreateFeedbackComment(adminUserId, companyId, feedId, content);
        }

        public UserUploadProfileImageResponse UploadProfileImage(string uploaderUserId, string companyId, string uploadedImageUrl, int approvalState)
        {
            return new User().UploadProfileImage(uploaderUserId, null, companyId, uploadedImageUrl, approvalState);
        }
        #endregion

        #region Event
        public EventSelectResponse SelectEventDetail(string requesterUserId,
                                                     string eventId,
                                                     string companyId)
        {
            return new Event().SelectEventByClient(requesterUserId, eventId, companyId, null, false);
        }
        #endregion

        #region App Setting
        public SelectAppSettingResponse SelectAppSetting(string requesterUserId, string companyId)
        {
            return new Setting().SelectAppSetting(requesterUserId, companyId);
        }

        public SelectAppNotificationResponse SelectNotificationSetting(string requesterUserId, string companyId)
        {
            return new Setting().SelectNotificationSetting(requesterUserId, companyId);
        }

        public UpdateAppSettingResponse UpdateAppSoundEffect(string requesterUserId, string companyId, bool isOn)
        {
            return new Setting().UpdateAppSoundEffect(requesterUserId, companyId, isOn);
        }

        public UpdateAppSettingResponse UpdateInGameMusic(string requesterUserId, string companyId, bool isOn)
        {
            return new Setting().UpdateInGameMusic(requesterUserId, companyId, isOn);
        }

        public UpdateAppSettingResponse UpdateNotification(string requesterUserId, string companyId, int gameNotification, int eventNotification, int feedNotification)
        {
            return new Setting().UpdateNotification(requesterUserId, companyId, gameNotification, eventNotification, feedNotification);
        }
        #endregion

        #region TestLoad
        public UserSelectWithCompanyResponse TestWithoutLoop(string userId, string companyId)
        {
            return new TestLoad().TestWithoutLoop(userId, companyId);
        }

        public CategorySelectAllWithTopicResponse TestWithLoop(string requesterUserId,
                                                               string companyId)
        {
            return new TestLoad().TestWithLoop(requesterUserId, companyId);
        }

        public BrainSelectResponse TestWithMultipleKeyspaces(string requesterUserId, string companyId)
        {
            return new TestLoad().TestWithMultipleKeyspaces(requesterUserId, companyId);
        }
        #endregion
    }
}
