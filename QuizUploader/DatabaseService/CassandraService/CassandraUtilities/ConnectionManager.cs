﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Web.Hosting;
using Cassandra;
using CassandraService.GlobalResources;
using log4net;

namespace CassandraService.CassandraUtilities
{
    public class ConnectionManager
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum ServerMode
        {
            Development = 1,
            Staging = 2,
            Production = 3
        }

        private static readonly string mainKeyspace = ConfigurationManager.AppSettings["cassandra_main_keyspace"];
        private static readonly string analyticKeyspace = ConfigurationManager.AppSettings["cassandra_analytic_keyspace"];

        private string serverIp = ConfigurationManager.AppSettings["cassandra_server_ip"];
        private static readonly string username = ConfigurationManager.AppSettings["cassandra_username"];
        private static readonly string password = ConfigurationManager.AppSettings["cassandra_password"];
        private static readonly string certFileName = ConfigurationManager.AppSettings["cassandra_cert_filename"];
        private static readonly string certKey = ConfigurationManager.AppSettings["cassandra_cert_key"];

        private static Cluster cluster;
        private static ISession mainSession = null, analyticSession = null;

        public ConnectionManager()
        {
        }

        public ConnectionManager(int serverMode)
        {
            switch(serverMode)
            {
                case (int)ServerMode.Development:
                    serverIp = ConfigurationManager.AppSettings["dev_cassandra_server_ip"];
                    break;

                case (int)ServerMode.Staging:
                    serverIp = ConfigurationManager.AppSettings["staging_cassandra_server_ip"];
                    break;

                case (int)ServerMode.Production:
                    serverIp = ConfigurationManager.AppSettings["prod_cassandra_server_ip"];
                    break;
            }
        }

        public ISession getMainSession()
        {
            if (mainSession == null)
            {
                ConnectToKeyspace(mainKeyspace);
            }
            return mainSession;
        }

        public ISession getAnalyticSession()
        {
            if (analyticSession == null)
            {
                ConnectToKeyspace(analyticKeyspace);
            }
            return analyticSession;
        }

        private void ConnectToKeyspace(string keyspace)
        {
            try
            {
                string[] addresses = serverIp.Split(',');

                if (cluster == null)
                {
                    // Basic configuration
                    Builder builder = Cluster.Builder();
                    builder.AddContactPoints(addresses);

                    cluster = Cluster.Builder()
                        .WithCredentials(username, password)
                        .AddContactPoints(addresses).Build();
                }


                if (keyspace.Equals(mainKeyspace))
                {
                    mainSession = cluster.Connect(keyspace);
                }
                else
                {
                    analyticSession = cluster.Connect(keyspace);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }
        }

        public bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            try
            {
                #region for file name
                //string cert_path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", certFileName);
                #endregion
                X509Certificate2 cert2 = new X509Certificate2(Files.Cluster_Ca_Certificate, certKey);

                X509Certificate2Collection xc = new X509Certificate2Collection(cert2);

                if (certificate.Equals(cert2))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }
            return false;
        }

        public void Close()
        {
            if (mainSession != null)
            {
                mainSession.Dispose();
                mainSession = null;
            }

            if (analyticSession != null)
            {
                analyticSession.Dispose();
                analyticSession = null;
            }

            if(cluster != null)
            {
                cluster.Shutdown();
                cluster = null;
            }
        }
    }
}
