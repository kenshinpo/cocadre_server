﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using CassandraService.Entity;
using CassandraService.GlobalResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.CassandraUtilities
{
    public static class EmailManager
    {
        public class EmailType
        {
            public const int CREATED_USER = 1;
            public const int CREATED_ADMIN = 2;
            public const int PASSWORD_RESET = 3;
            public const int FORGET_PASSWORD_BY_COMPANY = 4;
        }

        public static void SendEmail(Dictionary<String, object> args, int type, string source, Destination destination)
        {
            EmailTemplate emailTemplate = new EmailTemplate(type, args);
            Message message = new Message();
            message.Subject = new Content(emailTemplate.Title);
            Body mBody = new Body();
            if (emailTemplate.IsHtmlFormat)
            {
                mBody.Html = new Content(emailTemplate.Body);
            }
            else
            {
                mBody.Text = new Content(emailTemplate.Body);
            }
            message.Body = mBody;
            SendEmailRequest request = new SendEmailRequest(source, destination, message);
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient("AKIAIRFRL6Z2OYHO4R3Q", "HfL9OECxbpWqEH7KjHtl8T5/dJM6UiVQ66ywHXzY", RegionEndpoint.USWest2);
            client.SendEmail(request);
        }

    }

    public class EmailTemplate
    {
        public String Title { get; set; }
        public Boolean IsHtmlFormat { get; set; }
        public String Body { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="args">CompanyName, CompanyLogoUrl, UserEmail, UserPassword, AdminWebSiteUrl</param>
        public EmailTemplate(int type, Dictionary<String, object> args)
        {
            string emailLogo = args["EmailLogo"].ToString();
            string emailTitle = args["EmailTitle"].ToString();
            string emailDescription = args["EmailDescription"].ToString();
            string emailSupportInfo = args["EmailSupportInfo"].ToString();

            #region Switch mode
            switch (type)
            {
                #region CREATED_USER
                case CassandraService.CassandraUtilities.EmailManager.EmailType.CREATED_USER:
                    Title = emailTitle;
                    IsHtmlFormat = true;
                    Body = @"
<html>
<head>
	<title>"+ emailTitle + @"</title>
	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
</head>
<body bgcolor=""#FFFFFF"" leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">
	<table width=""800""  border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin:0 auto;"">
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/header.png"" width=""800"" height=""63"" alt=""Cocadre"" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<table width=""768"" border=""0"" cellpadding=""0"" cellspacing=""0""  bgcolor=""#f9f9f9"" style=""background-color:#f9f9f9;"">
					<tr>
						<td width=""217"" style=""max-width:217;"">
						</td>
						<td style=""padding:15px 0;"" align=""right"">
							<table width=""250"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td valign=""center"" height=""40"" style=""max-width:16px;""><img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/titlebar.jpg"" width=""13"" height=""40""></td>
									<td valign=""center""><span style=""font-family:sans-serif; font-size:18px; margin:0; padding:0;"">" + emailTitle + @"</span></td>
								</tr>
								<tr>
									<td colspan=""2"" valign=""bottom"">
									<div style=""font-family:sans-serif; font-size:12px; margin-top:5px;"">" + emailDescription + @"</div>
									</td>
								</tr>
							</table>
						</td>
						<td style=""padding:15px 0 15px 15px;"" align=""left"" width=""83"" style=""max-width:83px;"">
							<div style=""padding:9px; border:1px solid #ccc; max-width:65px;"">
								<img src=""" + emailLogo + @""" width=""65"" height=""65"" alt=""Welcome to Cocadre"">
							</div>
						</td>
						<td width=""217"" style=""max-width:217;"">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/topshadow.png"" width=""800"" height=""33"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td>
				<table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""26"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" height=""52"" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
						<td  width=""746"" height=""52"">
							<table width=""746"" height=""52"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td width=""100""></td>
									<td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200"">Your Login ID</td>
									<td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">
										<a href=""#"" style=""color:#ffab33; text-decoration:none; cursor:text;"">" + args["UserEmail"].ToString() + @"</a>
									</td>
									<td width=""100""></td>
								</tr>
								<tr>
									<td width=""100""></td>
									<td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200"">Your Password</td>
									<td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">" + args["UserPassword"].ToString() + @"</td>
									<td width=""100""></td>
								</tr>
							</table>
						</td>
						<td width=""28"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" height=""52"" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/botshadow.png"" width=""800"" height=""34"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<div style=""background:#f9f9f9; text-align:center;"">
						<p style=""font-family:sans-serif; font-size:12px; padding:0px 180px; margin:0; color:#ccc;"">
						" + emailSupportInfo + @"
						</p>
				</div>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""150"" align=""left"">
				<table width=""800"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""text-align:right;"">
								<tr>
									<td width=""277"">
									</td>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
								<tr>
									<td width=""277"">
									</td>
									<td>
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
							</table>
						</td>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""1"" cellspacing=""0"" style=""text-align:left;"">
								<tr>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
								<tr>
									<td width=""123"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""62"">
			<p style=""font-family:sans-serif; font-size:12px; padding:0 80px; margin:0; color:#ccc; text-align:center;"">
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/coicon.jpg"" width=""17"" height=""16"" alt=""Cocadre"" style=""position:relative; top:3px; padding-right:3px;""> 
				This e-mail is generated from CoCadre automated system. If you think this is spam or you did not request for this please report to 
				<a href=""mailto:admin@cocadre.com"" style=""text-decoration:none; color:#0076ff;"">admin@cocadre.com</a> 
				for immediate action.
			</p>
			</td>
		</tr>
	</table>
</body>
</html>";

                    break;
                #endregion

                #region CREATED_ADMIN
                case CassandraService.CassandraUtilities.EmailManager.EmailType.CREATED_ADMIN:
                    Title = emailTitle;
                    IsHtmlFormat = true;
                    Body = @"
<html>
<head>
	<title>" + emailTitle + @"/title>
	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
</head>
<body bgcolor=""#FFFFFF"" leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">
	<table width=""800""  border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin:0 auto;"">
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/header.png"" width=""800"" height=""63"" alt=""Cocadre"" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<table width=""768"" border=""0"" cellpadding=""0"" cellspacing=""0""  bgcolor=""#f9f9f9"" style=""background-color:#f9f9f9;"">
					<tr>
						<td width=""217"" style=""max-width:217;"">
						</td>
						<td style=""padding:15px 0;"" align=""right"">
							<table width=""250"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td valign=""center"" height=""40"" style=""max-width:16px;""><img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/titlebar.jpg"" width=""13"" height=""40""></td>
									<td valign=""center""><span style=""font-family:sans-serif; font-size:18px; margin:0; padding:0;"">" + emailTitle + @"</span></td>
								</tr>
								<tr>
									<td colspan=""2"" valign=""bottom"">
									<div style=""font-family:sans-serif; font-size:12px; margin-top:5px;"">" + emailDescription + @"</div>
									</td>
								</tr>
							</table>
						</td>
						<td style=""padding:15px 0 15px 15px;"" align=""left"" width=""83"" style=""max-width:83px;"">
							<div style=""padding:9px; border:1px solid #ccc; max-width:65px;"">
								<img src="""+ emailLogo + @""" width=""65"" height=""65"" alt=""Welcome to Cocadre"">
							</div>
						</td>
						<td width=""217"" style=""max-width:217;"">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/topshadow.png"" width=""800"" height=""33"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td>
				<table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""26"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" height=""52"" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
						<td  width=""746"" height=""52"">
							<table width=""746"" height=""52"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td width=""100""></td>
									<td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200"">Your Login ID</td>
									<td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">
										<a href=""#"" style=""color:#ffab33; text-decoration:none; cursor:text;"">" + args["UserEmail"].ToString() + @"</a>
									</td>
									<td width=""100""></td>
								</tr>
								<tr>
									<td width=""100""></td>
									<td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200"">Your Password</td>
									<td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">" + args["UserPassword"].ToString() + @"</td>
									<td width=""100""></td>
								</tr>
							</table>
						</td>
						<td width=""28"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" height=""52"" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/botshadow.png"" width=""800"" height=""34"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<div style=""background:#f9f9f9; text-align:center;"">
						<p style=""font-family:sans-serif; font-size:12px; padding:0px 180px; margin:0; color:#ccc;"">
						" + emailSupportInfo + @"
						</p>
				</div>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""150"" align=""left"">
				<table width=""800"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""text-align:right;"">
								<tr>
									<td width=""277"">
									</td>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
								<tr>
									<td width=""277"">
									</td>
									<td>
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
							</table>
						</td>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""1"" cellspacing=""0"" style=""text-align:left;"">
								<tr>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
								<tr>
									<td width=""123"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""62"">
			<p style=""font-family:sans-serif; font-size:12px; padding:0 80px; margin:0; color:#ccc; text-align:center;"">
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/coicon.jpg"" width=""17"" height=""16"" alt=""Cocadre"" style=""position:relative; top:3px; padding-right:3px;""> 
				This e-mail is generated from CoCadre automated system. If you think this is spam or you did not request for this please report to 
				<a href=""mailto:admin@cocadre.com"" style=""text-decoration:none; color:#0076ff;"">admin@cocadre.com</a> 
				for immediate action.
			</p>
			</td>
		</tr>
	</table>
</body>
</html>
        ";


                    break;
                #endregion

                #region PASSWORD_RESET
                case CassandraService.CassandraUtilities.EmailManager.EmailType.PASSWORD_RESET:

                    Title = emailTitle;
                    IsHtmlFormat = true;
                    Body = @"
<html>
<head>
	<title>" + emailTitle + @"</title>
	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
</head>
<body bgcolor=""#FFFFFF"" leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">
	<table width=""800""  border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin:0 auto;"">
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/header.png"" width=""800"" height=""63"" alt=""Cocadre"" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<table width=""768"" border=""0"" cellpadding=""0"" cellspacing=""0""  bgcolor=""#f9f9f9"" style=""background-color:#f9f9f9;"">
					<tr>
						<td width=""217"" style=""max-width:217;"">
						</td>
						<td style=""padding:15px 0;"" align=""right"">
							<table width=""250"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td valign=""center"" width=""16"" height=""40"" style=""max-width:16px;""><img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/titlebar.jpg"" width=""13"" height=""40""></td>
									<td valign=""center""><span style=""font-family:sans-serif; font-size:18px; margin:0; padding:0;"">" + emailTitle + @"</span></td>
								</tr>
								<tr>
									<td colspan=""2"" valign=""bottom"">
									<div style=""font-family:sans-serif; font-size:12px; margin-top:5px;"">" + emailDescription + @"</div>
									</td>
								</tr>
							</table>
						</td>
						<td style=""padding:15px 0 15px 15px;"" align=""left"" width=""83"" style=""max-width:83px;"">
							<div style=""padding:9px; border:1px solid #ccc; max-width:65px;"">
								<img src="""+ emailLogo + @""" width=""65"" height=""65"" alt=""Welcome to Cocadre"">
							</div>
						</td>
						<td width=""217"" style=""max-width:217;"">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/topshadow.png"" width=""800"" height=""33"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td>
				<table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""26"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" height=""52"" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
						<td width=""746"" height=""52"">
							<table width=""746"" height=""52"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td width=""100""></td>
									<td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200"">Your Login ID</td>
									<td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">
										<a href=""#"" style=""color:#ffab33; text-decoration:none; cursor:text;"">" + args["UserEmail"].ToString() + @"</a>
									</td>
									<td width=""100""></td>
								</tr>
								<tr>
									<td width=""100""></td>
									<td style=""text-align:right; font-weight:bold; font-family:sans-serif; color:#444444; font-size:14px;"" width=""200"">Your Password</td>
									<td style=""text-align:left;  font-family:sans-serif; color:#ffab33; font-size:14px; padding-left:10px;"" width=""346"">" + args["UserPassword"].ToString() + @"</td>
									<td width=""100""></td>
								</tr>
							</table>
						</td>
						<td width=""28"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" height=""52"" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td >
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/botshadow.png"" width=""800"" height=""34"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<div style=""background:#f9f9f9; text-align:center;"">
						<p style=""font-family:sans-serif; font-size:12px; padding:0px 180px; margin:0; color:#ccc;"">
						"+ emailSupportInfo + @"
						</p>
				</div>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""150"" align=""left"">
				<table width=""800"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""text-align:right;"">
								<tr>
									<td width=""277"">
									</td>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
								<tr>
									<td width=""277"">
									</td>
									<td>
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
							</table>
						</td>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""1"" cellspacing=""0"" style=""text-align:left;"">
								<tr>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
								<tr>
									<td width=""123"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""62"">
			<p style=""font-family:sans-serif; font-size:12px; padding:0 80px; margin:0; color:#ccc; text-align:center;"">
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/coicon.jpg"" width=""17"" height=""16"" alt=""Cocadre"" style=""position:relative; top:3px; padding-right:3px;""> 
				This e-mail is generated from CoCadre automated system. If you think this is spam or you did not request for this please report to 
				<a href=""mailto:admin@cocadre.com"" style=""text-decoration:none; color:#0076ff;"">admin@cocadre.com</a> 
				for immediate action.
			</p>
			</td>
		</tr>
	</table>
</body>
</html>
        ";
                    break;
                #endregion

                #region FORGET_PASSWORD_BY_COMPANY
                case CassandraService.CassandraUtilities.EmailManager.EmailType.FORGET_PASSWORD_BY_COMPANY:
                    string companyLogos = string.Empty;
                    foreach (Company company in (List<Company>)args["Companies"])
                    {
                        companyLogos += @"
                            <tr width=""270"" style=""max-width:270px;"" align=""center"">
								<td width=""270"" style=""max-width:270px;"">
									<a href=""" + company.ResetPasswordUrl + @""" style=""height:55px; width:270px; height:55px; background:white; text-decoration:none"">
										<div width=""270"" height=""55"" style=""border-radius:15px; border:1px solid #ccc;"">
											<img src=""" + company.CompanyLogoUrl + @""" width=""270"" style=""height:55px; width:270px; border-radius:15px;"">
										</div>
										<p style=""font-family:sans-serif; font-size:12px; color:#ffab33; margin-bottom:10px;"">
											" + company.CompanyTitle + @"
										</p>
									</a>
								</td>
							</tr>";
                    }

                    Title = emailTitle;
                    IsHtmlFormat = true;
                    Body = @"
<html>
<head>
	<title>"+ emailTitle + @"</title>
	<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">
</head>
<body bgcolor=""#FFFFFF"" leftmargin=""0"" topmargin=""0"" marginwidth=""0"" marginheight=""0"">
	<table width=""800""  border=""0"" cellpadding=""0"" cellspacing=""0"" style=""margin:0 auto;"">
		<tr>
			<td>
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/header.png"" width=""800"" height=""63"" alt=""Cocadre"" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<table width=""768"" border=""0"" cellpadding=""0"" cellspacing=""0""  bgcolor=""#f9f9f9"" style=""background-color:#f9f9f9;"">
					<tr>
						<td width=""217"" style=""max-width:217;"">
						</td>
						<td style=""padding:15px 0;"" align=""right"">
							<table width=""250"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td valign=""center"" height=""40"" style=""max-width:16px;""><img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/titlebar.jpg"" width=""13"" height=""40""></td>
									<td valign=""center""><span style=""font-family:sans-serif; font-size:18px; margin:0; padding:0;"">"+ emailTitle + @"</span></td>
								</tr>
								<tr>
									<td colspan=""2"" valign=""bottom"">
									<div style=""font-family:sans-serif; font-size:12px; margin-top:5px;"">"+ emailDescription + @"</div>
									</td>
								</tr>
							</table>
						</td>
						<td style=""padding:15px 0 15px 15px;"" align=""left"" width=""83"" style=""max-width:83px;"">
							<div style=""padding:9px; border:1px solid #ccc; max-width:65px;"">
								<img src="""+ emailLogo + @""" width=""65"" height=""65"" alt=""Welcome to Cocadre"">
							</div>
						</td>
						<td width=""217"" style=""max-width:217;"">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/topshadow.png"" width=""800"" height=""33"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td>
				<table width=""800"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""26"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/leftshadow.png"" width=""26"" height=""" + ((List<Company>)args["Companies"]).Count() * 160 + @""" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
						<td width=""746"" style=""max-width:746px;"">
							<table width=""746"" style=""margin:auto; max-width:746px;"" border=""0"" cellpadding=""0"" cellspacing=""0"">
								<tr>
									<td width=""223"" style=""max-width:223px;""></td>
									<td width=""300"" style=""max-width:300px;"">
										<table width=""300"" style=""margin:auto; max-width:300px;"" border=""0"" cellpadding=""0"" cellspacing=""0"">
											<tr align=""center"">
												<div height=""10"" style=""height:30px; text-align:center;"">
													<p style=""font-family:sans-serif; font-size:12px; color:#ccc;"">
													Select the account you want to reset:
													</p>
												</div>
											</tr>" + companyLogos + @"
										</table>
									</td>
									<td width=""223"" style=""max-width:223px;""></td>
								</tr>
							</table>
						</td>		
						<td width=""28"" style=""max-width:26px; color:#444444;"">
							<!--[if !mso]><!-->
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" alt="""" style=""display:block;height:100% !important;"">
							<!--<![endif]-->
							<!--[if gte mso 9]>
							<img class=""shadow"" src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/rightshadow.png"" width=""28"" height=""" + ((List<Company>)args["Companies"]).Count() * 160 + @""" alt="""" style=""display:block;"">
							<![endif]-->
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/botshadow.png"" width=""800"" alt="""" style=""display:block;"">
			</td>
		</tr>
		<tr>
			<td width=""800"" style=""padding:0 16px;"">
				<div style=""background:#f9f9f9; text-align:center;"">
						<p style=""font-family:sans-serif; font-size:12px; padding:0px 180px; margin:0; color:#ccc;"">
						"+ emailSupportInfo + @"
						</p>
				</div>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""150"" align=""left"">
				<table width=""800"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"">
					<tr>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""0"" cellspacing=""0"" style=""text-align:right;"">
								<tr>
									<td width=""277"">
									</td>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
								<tr>
									<td width=""277"">
									</td>
									<td>
										<a href=""https://appsto.re/sg/S0I--.i"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/applestore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
								</tr>
							</table>
						</td>
						<td width=""400"" height=""150"">
							<table width=""400"" height=""150"" border=""0"" cellpadding=""1"" cellspacing=""0"" style=""text-align:left;"">
								<tr>
									<td width=""123"" style=""text-align:center"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:20px; padding-bottom:5px;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_qr.jpg"" width=""80"" height=""80"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
								<tr>
									<td width=""123"">
										<a href=""https://play.google.com/store/apps/details?id=com.aporigin.cocadre"" style=""border:0px; display:block; margin:0; padding:0;"">
											<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/androidstore_btn.jpg"" width=""123"" height=""46"" alt=""Apple Store"">
										</a>
									</td>
									<td width=""277"">
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width=""800"" height=""62"">
			<p style=""font-family:sans-serif; font-size:12px; padding:0 80px; margin:0; color:#ccc; text-align:center;"">
				<img src=""https://s3-ap-southeast-1.amazonaws.com/cocadre/email-images/coicon.jpg"" width=""17"" height=""16"" alt=""Cocadre"" style=""position:relative; top:3px; padding-right:3px;""> 
				This e-mail is generated from CoCadre automated system. If you think this is spam or you did not request for this please report to 
				<a href=""mailto:admin@cocadre.com"" style=""text-decoration:none; color:#0076ff;"">admin@cocadre.com</a> 
				for immediate action.
			</p>
			</td>
		</tr>
	</table>
</body>
</html>
";

                    break;
                #endregion

                default:
                    break;
            }
            #endregion
        }

    }


}
