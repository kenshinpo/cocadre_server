﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace CassandraService
{
    public class AssemblySettings
    {
        private KeyValueConfigurationCollection _settings;

        public AssemblySettings(Assembly asmb, String sectionName = null)
        {
            LoadSettings(asmb, sectionName);
        }

        private void LoadSettings(Assembly asmb, string sectionName)
        {
            ExeConfigurationFileMap cfgFileMap = new ExeConfigurationFileMap();
            Uri codeBaseUri = new Uri(asmb.CodeBase);
            cfgFileMap.ExeConfigFilename = codeBaseUri.AbsolutePath + ".config";

            if (!File.Exists(cfgFileMap.ExeConfigFilename))
            {
                throw new Exception("Configuration file not found: " + cfgFileMap.ExeConfigFilename);
            }

            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(cfgFileMap, ConfigurationUserLevel.None);

            if (String.IsNullOrEmpty(sectionName))
            {
                sectionName = "appSettings";
            }

            AppSettingsSection section = (config.GetSection(sectionName) as AppSettingsSection);
            _settings = section.Settings;
        }

        public string this[string key]
        {
            get
            {
                return _settings[key].Value;
            }
        }
    }
}
