﻿using System;
using CassandraService.GlobalResources;

namespace CassandraService.Utilities
{
    /// <summary> 
    /// Generate unique IDs for DB
    /// </summary> 
    public class UUIDGenerator
    {
        #region Account
        public static String GenerateNumberPasswordForUser()
        {
            Random random = new Random();
            string id = random.Next(10000, 99999).ToString();
            return id;
        }

        public static String GeneratePasswordForUser()
        {
            string id = Guid.NewGuid().ToString();
            return id.Replace("-", "").Substring(0, 6);
        }

        public static String GenerateResetPasswordTokenForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ResetPasswordToken + id.Replace("-", "");
        }

        public static String GenerateUniqueIDForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.User + id.Replace("-", "");
        }

        public static String GenerateUniqueIDForCompany()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Company + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForDepartment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Department + id.Replace("-", "");
        }
        #endregion

        #region Feed
        public static string GenerateUniqueIDForFeedPrivacy()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedPrivacy + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeed()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Feed + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedComment()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedComment + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedCommentReply()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedCommentReply + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedMedia()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedMedia + id.Replace("-", "");
        }
        #endregion

        #region Challenge
        public static string GenerateUniqueIDForTopicCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeTopicCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForTopic()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeTopic + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuestion()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeQuestion + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuizChallenge()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Challenge + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForQuizChallengeChoice()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeQuestionChoice + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForExpAllocation()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeExpAllocation + id.Replace("-", "");
        }
        #endregion

        #region Log
        public static string GenerateUniqueIDForLog()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Log + id.Replace("-", "");
        }
        #endregion

        #region User Token
        public static string GenerateUniqueTokenForUser()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.AuthenticationToken + id.Replace("-", "");
        }
        #endregion

        #region Notification
        public static string GenerateUniqueIDForFeedNewPostNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedNewPostNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedNewCommentNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedNewCommentNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedNewReplyNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedNewReplyNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedUpvotePostNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedUpvotePostNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedUpvoteCommentNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedUpvoteCommentNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForFeedUpvoteReplyNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.FeedUpvoteReplyNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForChallengeCompletedNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeCompletedNotification + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForChallengeRequestNotification()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ChallengeRequestNotification + id.Replace("-", "");
        }
        #endregion

        #region Dashboard
        public static string GenerateUniqueIDForReport()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Report + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForProfileApproval()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.ProfileApproval + id.Replace("-", "");
        }
        #endregion

        #region Event
        public static string GenerateUniqueIDForEvent()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.Event + id.Replace("-", "");
        }
        #endregion

        #region Responsive Survey
        public static string GenerateUniqueIDForRSTopic()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSTopic + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSCategory()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSCategory + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSCard()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSCard + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSOption(bool isCustom)
        {
            string id = Guid.NewGuid().ToString();

            if(isCustom)
            {
                return PrefixId.RSOptionCustom + id.Replace("-", "");
            }

            return PrefixId.RSOption + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSImage()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSImage + id.Replace("-", "");
        }

        public static string GenerateUniqueIDForRSSimilar()
        {
            string id = Guid.NewGuid().ToString();
            return PrefixId.RSSimilar + id.Replace("-", "");
        }
        #endregion

    }
}
