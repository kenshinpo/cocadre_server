﻿using System.Collections.Generic;
using CassandraService.Entity;

namespace CassandraService.ServiceResponses
{
    public class CountryListResponse : ServiceResponse
    {
        public List<Country> Countries { get; set; }
    }
}