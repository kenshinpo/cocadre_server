﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class NotificationSelectNumberResponse : ServiceResponse
    {
        public int NumberOfNotification { get; set; }
    }

    public class NotificationSelectResponse : ServiceResponse
    {
        public List<Notification> Notifications { get; set; }
    }

    public class NotificationUpdateSeenResponse : ServiceResponse
    {
    }
}
