﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class AnalyticsSelectAllGameHistoriesResponse : ServiceResponse
    {
        public List<Analytic.GameHistory> GameHistories { get; set; }
    }

    public class AnalyticsSelectBasicResponse : ServiceResponse
    {
        public CassandraService.Entity.Analytic.BasicDau BasicDau { get; set; }
    }

    public class AnalyticsSelectDetailDauResponse : ServiceResponse
    {
        public CassandraService.Entity.Analytic.DetailDau DetailDau { get; set; }
        public CassandraService.Entity.Analytic.DauChart DauChart { get; set; }
        public List<CassandraService.Entity.Analytic.TimeActivity> TimeActivities { get; set; }
    }

    public class AnalyticsSelectGameResultResponse : ServiceResponse
    {
        public Topic Topic { get; set; }
        public List<User> Players { get; set; }
        public List<ChallengeQuestion> GameResult { get; set; }
        public int Outcome { get; set; }
    }

    public class AnalyticsSelectLeaderboardByCompanyResponse : ServiceResponse
    {
        public List<User> TopPersonnels { get; set; }
    }

    public class AnalyticsSelectLeaderboardByDepartmentResponse : ServiceResponse
    {
        public List<Department> TopDepartments { get; set; }
    }

    public class AnalyticsSelectLeaderboardByEventResponse : ServiceResponse
    {
        public List<CassandraService.Entity.Analytic.EventLeaderboard> TopEventLeaderboard { get; set; }
        public List<CassandraService.Entity.Analytic.EventLeaderboard> OutOfRankingLeaderboard { get; set; }
    }

    public class AnalyticsUpdateDailyActiveUserResponse : ServiceResponse
    {
    }

    public class AnalyticSelectRSResultOverviewResponse : ServiceResponse
    {
        public Analytic.RSAnalyticOverview Overview { get; set; }
    }

    public class AnalyticSelectRSCardResultResponse : ServiceResponse
    {
        public List<RSCard> CardResults { get; set; }
    }

    public class AnalyticsSelectRSCardsResponse : ServiceResponse
    {

    }

    public class AnalyticSelectRSCustomAnswersResponse : ServiceResponse
    {
        public RSCard Card { get; set; }
    }

    public class AnalyticSelectRSResponderReportResponse : ServiceResponse
    {
        public Analytic.RSPersonnelCompletion PersonnelCompletion { get; set; }
        public List<Analytic.RSReportPerUser> Reports { get; set; }
    }

    public class AnalyticSelectRSCardResultByUserResponse : ServiceResponse
    {
        public Analytic.RSReportPerUser Report { get; set; }
        public List<RSCard> CardResults { get; set; }
    }
}
