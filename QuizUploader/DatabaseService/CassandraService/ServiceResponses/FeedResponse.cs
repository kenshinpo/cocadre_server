﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class FeedAuthenticationResponse : ServiceResponse
    {
        public bool IsAuthenticatedToPost { get; set; }

        public string FeedId { get; set; }
    }

    public class FeedCreateResponse : ServiceResponse
    {
        public List<Dictionary<string, object>> TargetedUsers { get; set; }
        public string FeedId { get; set; }
    }

    public class FeedDeleteResponse : ServiceResponse
    {
    }

    public class FeedVisibilityResponse : ServiceResponse
    {
    }

    public class FeedSelectResponse : ServiceResponse
    {
        public List<Feed> Feeds { get; set; }
        public int NumberOfNewNotification { get; set; }
    }

    public class CommentCreateResponse : ServiceResponse
    {
        public int NumberOfNotificationForTargetedUser { get; set; }
        public string NotificationText { get; set; }
        public string TargetedUserId { get; set; }

        public string FeedId { get; set; }
        public string CommentId { get; set; }

        public int NotificationType { get; set; }
        public int NotificationSubType { get; set; }
    }

    public class CommentSelectResponse : ServiceResponse
    {
        public Feed FeedPost { get; set; }
        public List<Comment> Comments { get; set; }
    }

    public class PointUpdateResponse : ServiceResponse
    {
        public int NegativePoints { get; set; }
        public int PositivePoints { get; set; }
        public bool HasVoted { get; set; }
        public bool IsUpVoted { get; set; }
        public string NotificationText { get; set; }
        public string TargetedUserId { get; set; }
        public int NumberOfNotificationForTargetedUser { get; set; }

        public string FeedId { get; set; }
        public string CommentId { get; set; }
        public string ReplyId { get; set; }

        public int NotificationType { get; set; }
        public int NotificationSubType { get; set; }
    }

    public class ReplyCreateResponse : ServiceResponse
    {
        public int NumberOfNotificationForTargetedUser { get; set; }
        public string NotificationText { get; set; }
        public string TargetedUserId { get; set; }

        public string FeedId { get; set; }
        public string CommentId { get; set; }
        public string ReplyId { get; set; }

        public int NotificationType { get; set; }
        public int NotificationSubType { get; set; }
    }

    public class ReplySelectResponse : ServiceResponse
    {
        public Comment Comment { get; set; }
        public List<Reply> Replies { get; set; }
    }

    public class FeedUpdateResponse : ServiceResponse
    {
        public List<Dictionary<string, object>> TargetedUsers { get; set; }
        public string FeedId { get; set; }
    }

    public class FeedSelectPrivacyResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public List<User> Users { get; set; }
        [DataMember(EmitDefaultValue = false)]
        public List<Department> Departments { get; set; }
    }
}
