﻿using System.Collections.Generic;
using CassandraService.Entity;

namespace CassandraService.ServiceResponses
{
    public class GetModeratorModulesResponse : ServiceResponse
    {
        public List<Module> Modules { get; set; }
    }
}
