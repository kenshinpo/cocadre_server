﻿using System;
using System.Runtime.Serialization;

namespace CassandraService.ServiceResponses
{
    [Serializable]
    public class ServiceResponse
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ErrorCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ErrorMessage { get; set; }
    }
}