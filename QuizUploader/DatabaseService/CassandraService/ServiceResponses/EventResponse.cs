﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;

namespace CassandraService.ServiceResponses
{
    public class EventCreateResponse : ServiceResponse
    {

    }

    public class EventSelectAllResponse : ServiceResponse
    {
        public List<Event> Events { get; set; }
    }

    public class EventSelectResponse : ServiceResponse
    {
        public Event Event { get; set; }
    }

    public class EventSelectTopicResponse : ServiceResponse
    {
        public List<Topic> Topics { get; set; }
    }

    public class EventSelectUserResponse : ServiceResponse
    {
        public List<User> Users { get; set; }
    }

    public class EventSelectDepartmentResponse : ServiceResponse
    {
        public List<Department> Departments { get; set; }
    }

    public class EventUpdateResponse : ServiceResponse
    {
    }

    public class CheckEventInProgressResponse : ServiceResponse
    {
        public bool isEventInProgress { get; set; }
    }
}
