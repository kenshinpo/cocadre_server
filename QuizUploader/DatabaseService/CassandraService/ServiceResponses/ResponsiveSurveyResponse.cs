﻿using Cassandra;
using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    #region Category
    public class RSCategorySelectAllResponse : ServiceResponse
    {
        public List<RSTopicCategory> Categories { get; set; }
    }

    public class RSCategorySelectAllWithTopicResponse : ServiceResponse
    {
    }

    public class RSCategorySelectResponse : ServiceResponse
    {
        public RSTopicCategory Category { get; set; }
    }

    public class RSCategoryUpdateResponse : ServiceResponse
    {
    }

    public class RSCategoryDeleteResponse : ServiceResponse
    {
    }

    public class RSCategoryCreateResponse : ServiceResponse
    {
    }
    #endregion

    #region Topic
    public class RSTopicSelectAllByCategoryResponse : ServiceResponse
    {
        public List<RSTopicCategory> RSCategories { get; set; }
    }

    public class RSTopicSelectAllBasicResponse : ServiceResponse
    {
        public List<RSTopic> Topics { get; set; }
    }

    public class RSTopicSelectIconResponse : ServiceResponse
    {
    }

    public class RSTopicUpdateResponse : ServiceResponse
    {
    }

    public class RSTopicCreateResponse : ServiceResponse
    {
        public RSTopic Topic { get; set; }
    }

    public class RSTopicSelectResponse : ServiceResponse
    {
        public RSTopic Topic { get; set; }
    }

    public class RSTopicStartResponse : ServiceResponse
    {
        public bool IsStart { get; set; }
        public string Introduction { get; set; }
        public bool IsEnd { get; set; }
        public string ClosingWords { get; set; }
        public RSCard NextCard { get; set; }
    }
    #endregion

    #region Card
    public class RSCardCreateResponse : ServiceResponse
    {
    }

    public class RSCardSelectAllResponse : ServiceResponse
    {
        public string TopicTitle { get; set; }
        public string TopicIconUrl { get; set; }
        public string Introduction { get; set; }
        public string ClosingWords { get; set; }
        public List<RSCard> Cards { get; set; }
    }

    public class RSCardAnswerResponse : ServiceResponse
    {
        public bool IsEnd { get; set; }
        public string ClosingWords { get; set; }
        public List<RSCard> NextCards { get; set; }
    }

    public class RSCardSelectResponse : ServiceResponse
    {
        public RSCard Card { get; set; }
    }

    public class RSCardUpdateResponse : ServiceResponse
    {
    }

    public class RSCardReorderResponse : ServiceResponse
    {
    }
    #endregion

    #region Option
    public class RSOptionCreateResponse : ServiceResponse
    {
        public string previousCustomAnswer { get; set; }
        public List<BoundStatement> boundStatements { get; set; }
    }

    public class RSOptionUpdateResponse : ServiceResponse
    {
        public List<BoundStatement> boundStatements { get; set; }
    }

    public class RSOptionCheckLogicResponse : ServiceResponse
    {
    }

    public class RSOptionSelectLogicResponse : ServiceResponse
    {
        public List<RSCard> Cards { get; set; }
    }

    public class RSOptionRemovePreviouslyCustomAnswerResponse : ServiceResponse
    {
        public List<BoundStatement> deleteStatements { get; set; }
        public List<BoundStatement> updateStatements { get; set; }
    }

    public class RSOptionRemovePreviouslyOptionAnswerResponse : ServiceResponse
    {
        public List<BoundStatement> deleteStatements { get; set; }
        public List<BoundStatement> updateStatements { get; set; }
    }
    #endregion

    #region Activity
    public class RSUpdateActivityResponse : ServiceResponse
    {
    }
    #endregion
}
