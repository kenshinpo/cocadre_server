﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;
using System.Runtime.Serialization;

namespace CassandraService.ServiceResponses
{
    public class AuthenticationSelectUserResponse : ServiceResponse
    {
        [DataMember]
        public List<Company> Companies { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UserToken { get; set; }
    }

    public class AuthenticationSelectUserByCompanyResponse : ServiceResponse
    {
        [DataMember(EmitDefaultValue = false)]
        public string UserToken { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsPasswordDefault { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User User { get; set; }
    }

    public class AuthenticationSelectCompanyResponse : ServiceResponse
    {
        [DataMember]
        public List<Company> Companies { get; set; }
    }

    public class AuthenticationUpdateResponse : ServiceResponse
    {
    }

    public class AuthenticationCheckEmailForCompanyResponse : ServiceResponse
    {
        public bool IsEmailExists { get; set; }
    }

    public class GetManagerInfoResponse : ServiceResponse
    {
        public String ManagerUserId { get; set; }
        public String CompanyId { get; set; }
        public String CompanyName { get; set; }
        public String CompanyLogoUrl { get; set; }
        public String CompanyAdminUrl { get; set; }
        public String PrimaryManagerId { get; set; }
        public String ManagerFirstName { get; set; }
        public String ManagerLastName { get; set; }
        public String ManagerEmail { get; set; }
        public String ManagerProfileImageUrl { get; set; }
        public User.AccountType ManagerAccountType { get; set; }
        public List<Module> ManagerAccessModules { get; set; }
    }

    public class UserTokenValidityResponse : ServiceResponse
    {
        public bool isTokenExists { get; set; }
        public bool isTokenValid { get; set; }
    }

    public class LoginByManagerResponse : ServiceResponse
    {
        public List<Company> Companies { get; set; }
    }

    public class AccountKindListResponse : ServiceResponse
    {
        public List<AccountKind> AccountKinds { get; set; }
    }
}
