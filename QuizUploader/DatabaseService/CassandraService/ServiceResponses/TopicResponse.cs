﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class TopicCreateResponse : ServiceResponse
    {
        public string TopicId { get; set; }
    }

    public class TopicSelectAllBasicResponse : ServiceResponse
    {
        public List<Topic> Topics;
    }

    public class TopicSelectIconResponse : ServiceResponse
    {
        public List<string> TopicIconUrls;
    }

    public class TopicSelectResponse : ServiceResponse
    {
        public Topic Topic;
    }

    public class TopicUpdateResponse : ServiceResponse
    {
        public string NewTopicCategoryId { get; set; }
    }
}
