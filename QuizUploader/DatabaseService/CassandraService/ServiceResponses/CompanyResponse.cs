﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CassandraService.Entity;

namespace CassandraService.ServiceResponses
{
    public class GetCompanyPermissionResponse : ServiceResponse
    {
        public Company Company { get; set; }
    }

    public class CompanySelectProfileResponse : ServiceResponse
    {
        public CompanyProfile CompanyProfile { get; set; }
        public List<Timezone> Timezones { get; set; }
    }

    public class CompanySelectEmailTemplateResponse : ServiceResponse
    {
        public string EmailSquareLogoUrl { get; set; }

        public string EmailPersonnelInvitationTitle { get; set; }

        public string EmailPersonnelInvitationDescription { get; set; }

        public string EmailPersonnelInvitationSupportInfo { get; set; }

        public string EmailAdminInvitationTitle { get; set; }

        public string EmailAdminInvitationDescription { get; set; }

        public string EmailAdminInvitationSupportInfo { get; set; }

        public string EmailForgotPasswordTitle { get; set; }

        public string EmailForgotPasswordDescription { get; set; }

        public string EmailForgotPasswordSupportInfo { get; set; }

        public string EmailResetPasswordTitle { get; set; }

        public string EmailResetPasswordDescription { get; set; }

        public string EmailResetPasswordSupportInfo { get; set; }
    }

    public class CompanyUpdatePermissionResponse : ServiceResponse
    {

    }

    public class CompanyUpdateProfileResponse : ServiceResponse
    {

    }

    public class CompanySelectImageResponse : ServiceResponse
    {
        public string ImageUrl { get; set; }
    }

    public class CompanySelectEmailDetailResponse : ServiceResponse
    {
        public string EmailLogoUrl { get; set; }
        public string EmailTitle { get; set; }
        public string EmailDescription { get; set; }
        public string EmailSupportInfo { get; set; }
    }


    public class CompanyResponse : ServiceResponse
    {
        //managerInfo.CompanyId = company.CompanyId;
        //managerInfo.CompanyLogoUrl = company.CompanyLogoUrl;
        //managerInfo.CompanyName = company.CompanyTitle;
        //managerInfo.CompanyAdminImageUrl = company.AdminImageUrl;
        //managerInfo.PrimaryManagerId = company.PrimaryManagerId; // ?

        public CassandraService.Entity.Company Company;
        public string CompanyId;
        public string CompanyLogoUrl;
        public string CompanyTitle;
        public string AdminImageUrl;
        public string PrimaryManagerId;

    }
}
