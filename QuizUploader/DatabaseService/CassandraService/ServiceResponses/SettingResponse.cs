﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class SelectAppSettingResponse : ServiceResponse
    {
        public bool RequireSoundEffects { get; set; }
        public bool RequireInGameMusic { get; set; }
    }

    public class SelectAppNotificationResponse : ServiceResponse
    {
        public int GameNotification { get; set; }
        public int EventNotification { get; set; }
        public int FeedNotification { get; set; }
    }

    public class UpdateAppSettingResponse : ServiceResponse
    {
    }
}
