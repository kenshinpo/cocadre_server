﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class DepartmentCreateResponse : ServiceResponse
    {
        public Department Department;
    }

    public class DepartmentDeleteResponse : ServiceResponse
    {
    }

    public class DepartmentDetailResponse : ServiceResponse
    {
        public Department Department { get; set; }
    }

    public class DepartmentListResponse : ServiceResponse
    {
        public List<Department> Departments { get; set; }
    }

    public class DepartmentUpdateResponse : ServiceResponse
    {
    }
}
