﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class ChallengeCreateResponse : ServiceResponse
    {
        public Topic Topic;
        public List<User> Players;
        public string ChallengeId;
        public int NotificationBadgeForTargetedUser;
    }

    public class ChallengeInvalidateResponse : ServiceResponse
    {
        public User InitiatedUser { get; set; }
        public User ChallengedUser { get; set; }
    }

    public class ChallengeIsReadyResponse : ServiceResponse
    {
        public bool HaveBothPlayersAccepted { get; set; }
        public bool IsLive { get; set; }
        public List<string> PlayerIds { get; set; }
    }

    public class ChallengeOfflineSelectOpponentAnswerResponse : ServiceResponse
    {
        public string ChallengeId { get; set; }
        public string OpponentUserId { get; set; }
        public bool HasOpponentAnswered { get; set; }
        public bool IsOpponentAnswerCorrect { get; set; }
        public float TimeTakenByOpponent { get; set; }
    }

    public class ChallengeSelectResponse : ServiceResponse
    {
        public Topic Topic;
        public List<User> Players;
        public string ChallengeId;
    }

    public class ChallengeSelectStatsResponse : ServiceResponse
    {
        public Analytic.Exp Exp;
        public int OpponentLevel;
        public List<User> TopPlayers;
    }

    public class ChallengeStartWithoutOpponentResponse : ServiceResponse
    {
    }
}
