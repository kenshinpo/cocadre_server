﻿using CassandraService.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.ServiceResponses
{
    public class BrainSelectResponse : ServiceResponse
    {
        public Analytic.Exp UserExp { get; set; }
        public List<Topic> RecentlyPlayedTopics { get; set; }
        public List<User> RecentOpponents { get; set; }
        public List<Challenge> LatestChallenges { get; set; }
        public List<Event> Events { get; set; }
        public List<RSTopicCategory> Surveys { get; set; }
        public int ChallengeCount { get; set; }
    }
}
