﻿using Cassandra;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Web;
using CassandraService.Utilities;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Validation;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    public class Notification
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [DataMember(EmitDefaultValue = false)]
        public string Id { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedFeedId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedCommentId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedReplyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TaggedChallengeId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NotificationText { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Type { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int SubType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User TaggedUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsSeen { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime CreatedOnTimestamp { get; set; }

        [Serializable]
        public enum NotificationType
        {
            [EnumMember]
            Feed = 1,

            [EnumMember]
            Game = 2,
        }

        [Serializable]
        public enum NotificationFeedSubType
        {
            [EnumMember]
            NewPost = 1,

            [EnumMember]
            NewComment = 2,

            [EnumMember]
            NewReply = 3,

            [EnumMember]
            UpvotePost = 4,

            [EnumMember]
            UpvoteComment = 5,

            [EnumMember]
            UpvoteReply = 6,
        }

        [Serializable]
        public enum NotificationGameSubType
        {
            [EnumMember]
            NewChallenge = 1,

            [EnumMember]
            ChallengedUserCompletedChallenge = 2,
        }

        public string CreateFeedNotification(string fromUserId,
                                             string companyId,
                                             string targetedUserId,
                                             int subType,
                                             int feedType,
                                             string feedId,
                                             string commentId,
                                             string replyId,
                                             long createdTimeLong,
                                             ISession session)
        {
            string notificationText = string.Empty;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement preparedStatement = null;
                BatchStatement batchStatement = new BatchStatement();
                Row notificationRow = null;

                string notificationId = string.Empty;
                string challengeId = "NA";

                if (string.IsNullOrEmpty(replyId))
                {
                    replyId = "NA";
                }

                if (string.IsNullOrEmpty(commentId))
                {
                    commentId = "NA";
                }

                if (subType == (int)NotificationFeedSubType.NewComment ||
                    subType == (int)NotificationFeedSubType.NewReply ||
                    subType == (int)NotificationFeedSubType.UpvotePost ||
                    subType == (int)NotificationFeedSubType.UpvoteComment ||
                    subType == (int)NotificationFeedSubType.UpvoteReply)
                {

                    preparedStatement = session.Prepare(CQLGenerator.SelectStatement("notification_privacy",
                        new List<string>(), new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype" }));
                    notificationRow = session.Execute(preparedStatement.Bind(challengeId, feedId, commentId, replyId, targetedUserId, (int)NotificationType.Feed, subType)).FirstOrDefault();

                    if (notificationRow != null)
                    {
                        DateTimeOffset createdOnTimestamp = notificationRow.GetValue<DateTimeOffset>("notification_created_on_timestamp");
                        notificationId = notificationRow.GetValue<string>("notification_id");

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy",
                            new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "notification_created_on_timestamp", "is_seen", "is_fetched" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(createdTimeLong, false, false, challengeId, feedId, commentId, replyId, targetedUserId, (int)NotificationType.Feed, subType, notificationId));

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_challenge",
                            new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "notification_created_on_timestamp", "is_seen", "is_fetched" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(createdTimeLong, false, false, challengeId, feedId, commentId, replyId, targetedUserId, (int)NotificationType.Feed, subType, notificationId));

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_feed",
                            new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "notification_created_on_timestamp", "is_seen", "is_fetched" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(createdTimeLong, false, false, challengeId, feedId, commentId, replyId, targetedUserId, (int)NotificationType.Feed, subType, notificationId));

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_comment",
                            new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "notification_created_on_timestamp", "is_seen", "is_fetched" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(createdTimeLong, false, false, challengeId, feedId, commentId, replyId, targetedUserId, (int)NotificationType.Feed, subType, notificationId));

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_reply",
                            new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "notification_created_on_timestamp", "is_seen", "is_fetched" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(createdTimeLong, false, false, challengeId, feedId, commentId, replyId, targetedUserId, (int)NotificationType.Feed, subType, notificationId));

                        preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("notification_privacy_desc",
                            new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                        batchStatement.Add(preparedStatement.Bind(targetedUserId, createdOnTimestamp, notificationId));

                        preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("notification_privacy_asc",
                            new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                        batchStatement.Add(preparedStatement.Bind(targetedUserId, createdOnTimestamp, notificationId));
                    }
                    else
                    {
                        if (subType == (int)NotificationFeedSubType.NewComment)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedNewCommentNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.NewReply)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedNewReplyNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.UpvotePost)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedUpvotePostNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.UpvoteComment)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedUpvoteCommentNotification();
                        }
                        else if (subType == (int)NotificationFeedSubType.UpvoteReply)
                        {
                            notificationId = UUIDGenerator.GenerateUniqueIDForFeedUpvoteReplyNotification();
                        }

                        preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                        preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_challenge",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                        preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_feed",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                        preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_comment",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                        preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_reply",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                        batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));
                    }
                }
                else if (subType == (int)NotificationFeedSubType.NewPost)
                {
                    notificationId = UUIDGenerator.GenerateUniqueIDForFeedNewPostNotification();

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_challenge",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_feed",
                            new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_comment",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_reply",
                        new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));
                }

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_desc",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_asc",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Feed, subType, createdTimeLong));

                session.Execute(batchStatement);

                User fromUser = new User().SelectUserBasic(fromUserId, companyId, false, session).User;
                string fromUserFullName = string.Format("{0} {1}", fromUser.FirstName, fromUser.LastName);

                if (subType == (int)NotificationFeedSubType.NewPost)
                {
                    notificationText = string.Format("{0} tagged you in a post.", fromUserFullName);
                }
                if (subType == (int)NotificationFeedSubType.NewComment)
                {
                    notificationText = string.Format("{0} has commented on your post.", fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.NewReply)
                {
                    notificationText = string.Format("{0} has replied to your comment.", fromUserFullName);
                }
                else if (subType == (int)NotificationFeedSubType.UpvotePost)
                {
                    notificationText = string.Format("Someone has upvoted your post.");
                }
                else if (subType == (int)NotificationFeedSubType.UpvoteComment)
                {
                    notificationText = string.Format("Someone has upvoted your comment.");
                }
                else if (subType == (int)NotificationFeedSubType.UpvoteReply)
                {
                    notificationText = string.Format("Someone has upvoted your reply.");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

            return notificationText;
        }

        public bool CreateGameNotification(string targetedUserId,
                                           int subType,
                                           int challengeType,
                                           string challengeId,
                                           long createdTimeLong,
                                           ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement preparedStatement = null;
                BatchStatement batchStatement = new BatchStatement();

                string notificationId = string.Empty;
                string feedId = "NA";
                string commentId = "NA";
                string replyId = "NA";
                int feedType = 0;

                if (subType == (int)Notification.NotificationGameSubType.NewChallenge)
                {
                    notificationId = UUIDGenerator.GenerateUniqueIDForChallengeRequestNotification();
                }
                else if (subType == (int)Notification.NotificationGameSubType.ChallengedUserCompletedChallenge)
                {
                    notificationId = UUIDGenerator.GenerateUniqueIDForChallengeCompletedNotification();
                }

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Game, subType, createdTimeLong));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_challenge",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Game, subType, createdTimeLong));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_feed",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Game, subType, createdTimeLong));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_comment",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Game, subType, createdTimeLong));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_by_reply",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Game, subType, createdTimeLong));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_desc",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Game, subType, createdTimeLong));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("notification_privacy_asc",
                    new List<string> { "notification_id", "targeted_user_id", "feed_type", "feed_id", "comment_id", "reply_id", "challenge_id", "is_seen", "is_fetched", "notification_type", "notification_subtype", "notification_created_on_timestamp" }));
                batchStatement.Add(preparedStatement.Bind(notificationId, targetedUserId, feedType, feedId, commentId, replyId, challengeId, false, false, (int)NotificationType.Game, subType, createdTimeLong));

                session.Execute(batchStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());

                return false;
            }

            return true;
        }

        public NotificationUpdateSeenResponse UpdateSeenNotification(string currentUserId,
                                                                     string companyId,
                                                                     int notificationType,
                                                                     int notificationSubType,
                                                                     string challengeId,
                                                                     string feedId,
                                                                     string commentId,
                                                                     string replyId,
                                                                     string notificationId)
        {
            NotificationUpdateSeenResponse response = new NotificationUpdateSeenResponse();
            response.Success = false;

            try
            {

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(currentUserId, companyId, session);

                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                if (string.IsNullOrEmpty(notificationId))
                {
                    notificationId = string.Empty;
                }

                if (notificationType == (int)NotificationType.Feed)
                {
                    challengeId = "NA";

                    if (notificationSubType == (int)NotificationFeedSubType.NewPost || notificationSubType == (int)NotificationFeedSubType.UpvotePost || notificationSubType == (int)NotificationFeedSubType.NewComment)
                    {
                        commentId = "NA";
                        replyId = "NA";
                    }
                    else if (notificationSubType == (int)NotificationFeedSubType.UpvoteComment || notificationSubType == (int)NotificationFeedSubType.NewReply)
                    {
                        replyId = "NA";
                    }
                }
                else if (notificationType == (int)NotificationType.Game)
                {
                    feedId = "NA";
                    commentId = "NA";
                    replyId = "NA";
                }

                PreparedStatement psNotification = session.Prepare(CQLGenerator.SelectStatement("notification_privacy",
                    new List<string>(), new List<string> { "targeted_user_id", "notification_type", "notification_subtype", "challenge_id", "feed_id", "comment_id", "reply_id", "notification_id" }));
                BoundStatement bsNotification = psNotification.Bind(currentUserId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId);

                Row notificationRow = session.Execute(bsNotification).FirstOrDefault();

                if (notificationRow != null)
                {
                    BatchStatement batchStatement = new BatchStatement();

                    psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy",
                        new List<string> { "targeted_user_id", "notification_type", "notification_subtype", "challenge_id", "feed_id", "comment_id", "reply_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                    batchStatement.Add(psNotification.Bind(true, currentUserId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId));

                    psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_feed",
                        new List<string> { "targeted_user_id", "notification_type", "notification_subtype", "challenge_id", "feed_id", "comment_id", "reply_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                    batchStatement.Add(psNotification.Bind(true, currentUserId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId));

                    psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_comment",
                        new List<string> { "targeted_user_id", "notification_type", "notification_subtype", "challenge_id", "feed_id", "comment_id", "reply_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                    batchStatement.Add(psNotification.Bind(true, currentUserId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId));

                    psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_reply",
                        new List<string> { "targeted_user_id", "notification_type", "notification_subtype", "challenge_id", "feed_id", "comment_id", "reply_id", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                    batchStatement.Add(psNotification.Bind(true, currentUserId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId));

                    // For cleaning up 
                    if (notificationRow["notification_created_on_timestamp"] == null)
                    {
                        psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_desc",
                            new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(psNotification.Bind(true, currentUserId, null, notificationId));

                        psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_asc",
                            new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(psNotification.Bind(true, currentUserId, null, notificationId));
                    }
                    else
                    {
                        DateTimeOffset createdTimestamp = notificationRow.GetValue<DateTimeOffset>("notification_created_on_timestamp");

                        psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_desc",
                            new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(psNotification.Bind(true, currentUserId, createdTimestamp, notificationId));

                        psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_asc",
                            new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_seen" }, new List<string>()));
                        batchStatement.Add(psNotification.Bind(true, currentUserId, createdTimestamp, notificationId));
                    }

                    session.Execute(batchStatement);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public NotificationSelectNumberResponse SelectNotificationNumberByUser(string currentUserId,
                                                                               string companyId,
                                                                               ISession session = null)
        {
            NotificationSelectNumberResponse response = new NotificationSelectNumberResponse();
            response.Success = false;
            response.NumberOfNotification = 0;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.CountStatement("notification_privacy_desc",
                    new List<string> { "targeted_user_id", "is_fetched" }));
                long number = session.Execute(preparedStatement.Bind(currentUserId, false)).FirstOrDefault().GetValue<long>("count");
                response.NumberOfNotification = (int)number;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public List<string> SelectTargetedUserIdForNotification(string feedId,
                                                                string challengeId,
                                                                string commentId,
                                                                string replyId,
                                                                int notificationType,
                                                                int notificationSubType,
                                                                ISession session)
        {
            List<string> targetedUserIds = new List<string>();

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("notification_privacy_by_feed",
                  new List<string>(), new List<string> { "feed_id", "notification_type", "notification_subtype", "challenge_id", "comment_id", "reply_id" }));
                RowSet notificationRowset = session.Execute(preparedStatement.Bind(feedId, notificationType, notificationSubType, challengeId, commentId, replyId));

                foreach(Row notificationRow in notificationRowset)
                {
                    string userId = notificationRow.GetValue<string>("targeted_user_id");
                    targetedUserIds.Add(userId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return targetedUserIds;
        }

        public NotificationSelectResponse SelectNotificationByUser(string requesterUserId,
                                                                   string companyId,
                                                                   int numberOfPostsLoaded,
                                                                   DateTime? newestTimestamp,
                                                                   DateTime? oldestTimestamp)
        {
            NotificationSelectResponse response = new NotificationSelectResponse();
            response.Notifications = new List<Notification>();
            response.Success = false;

            try
            {
                int limit = Int16.Parse(WebConfigurationManager.AppSettings["notification_limit"]);

                if (numberOfPostsLoaded > 0)
                {
                    limit += numberOfPostsLoaded;
                }


                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                PreparedStatement psNotification = null;
                BoundStatement bsNotification = null;

                bool isSelectCompleted = false;

                Feed feed = new Feed();
                Department department = new Department();

                while (isSelectCompleted == false)
                {
                    if (oldestTimestamp != null)
                    {
                        DateTime currentDateTime = oldestTimestamp.Value;
                        psNotification = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("notification_privacy_desc",
                            null, new List<string> { "targeted_user_id" }, "notification_created_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        bsNotification = psNotification.Bind(requesterUserId, DateHelper.ConvertDateToLong(currentDateTime));
                    }
                    else
                    {
                        psNotification = session.Prepare(CQLGenerator.SelectStatementWithLimit("notification_privacy_desc",
                            new List<string>(), new List<string> { "targeted_user_id" }, limit));
                        bsNotification = psNotification.Bind(requesterUserId);
                    }

                    RowSet allNotificationRowSet = session.Execute(bsNotification);
                    List<Row> allNotificationRowList = new List<Row>();
                    allNotificationRowList = allNotificationRowSet.GetRows().ToList();

                    foreach (Row allNotificationRow in allNotificationRowList)
                    {
                        Notification notification = null;

                        string notificationId = allNotificationRow.GetValue<string>("notification_id");
                        string challengeId = allNotificationRow.GetValue<string>("challenge_id").Equals("NA") ? null : allNotificationRow.GetValue<string>("challenge_id");
                        string feedId = allNotificationRow.GetValue<string>("feed_id").Equals("NA") ? null : allNotificationRow.GetValue<string>("feed_id");
                        string commentId = allNotificationRow.GetValue<string>("comment_id").Equals("NA") ? null : allNotificationRow.GetValue<string>("comment_id");
                        string replyId = allNotificationRow.GetValue<string>("reply_id").Equals("NA") ? null : allNotificationRow.GetValue<string>("reply_id");
                        int feedType = allNotificationRow.GetValue<int>("feed_type");
                        bool isSeen = allNotificationRow.GetValue<bool>("is_seen");
                        int notificationSubType = allNotificationRow.GetValue<int>("notification_subtype");
                        int notificationType = allNotificationRow.GetValue<int>("notification_type");
                        DateTimeOffset createdOnTimestampOffset = allNotificationRow.GetValue<DateTimeOffset>("notification_created_on_timestamp");
                        DateTime createdOnTimestamp = allNotificationRow.GetValue<DateTime>("notification_created_on_timestamp");

                        string content = string.Empty;

                        oldestTimestamp = createdOnTimestamp;

                        // Clean up
                        if (allNotificationRow["is_fetched"] == null || (allNotificationRow["is_fetched"] != null && !allNotificationRow.GetValue<bool>("is_fetched")))
                        {
                            BatchStatement batchStatement = new BatchStatement();
                            if (string.IsNullOrEmpty(challengeId))
                            {
                                challengeId = "NA";
                            }

                            if (string.IsNullOrEmpty(feedId))
                            {
                                feedId = "NA";
                            }

                            if (string.IsNullOrEmpty(commentId))
                            {
                                commentId = "NA";
                            }

                            if (string.IsNullOrEmpty(replyId))
                            {
                                replyId = "NA";
                            }

                            psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                            batchStatement.Add(psNotification.Bind(true, challengeId, feedId, commentId, replyId, requesterUserId, notificationType, notificationSubType, notificationId));

                            psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_challenge",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                            batchStatement.Add(psNotification.Bind(true, challengeId, feedId, commentId, replyId, requesterUserId, notificationType, notificationSubType, notificationId));

                            psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_feed",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                            batchStatement.Add(psNotification.Bind(true, challengeId, feedId, commentId, replyId, requesterUserId, notificationType, notificationSubType, notificationId));

                            psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_comment",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                            batchStatement.Add(psNotification.Bind(true, challengeId, feedId, commentId, replyId, requesterUserId, notificationType, notificationSubType, notificationId));

                            psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_by_reply",
                                new List<string> { "challenge_id", "feed_id", "comment_id", "reply_id", "targeted_user_id", "notification_type", "notification_subtype", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                            batchStatement.Add(psNotification.Bind(true, challengeId, feedId, commentId, replyId, requesterUserId, notificationType, notificationSubType, notificationId));

                            psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_desc",
                               new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                            batchStatement.Add(psNotification.Bind(true, requesterUserId, createdOnTimestampOffset, notificationId));

                            psNotification = session.Prepare(CQLGenerator.UpdateStatement("notification_privacy_asc",
                               new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }, new List<string> { "is_fetched" }, new List<string>()));
                            batchStatement.Add(psNotification.Bind(true, requesterUserId, createdOnTimestampOffset, notificationId));

                            session.Execute(batchStatement);
                        }

                        if (notificationType == (int)NotificationType.Feed)
                        {
                            Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);

                            if (feedRow == null)
                            {
                                continue;
                            }

                            string ownerUserId = feedRow.GetValue<string>("owner_user_id");

                            User ownerUser = null;

                            bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                            bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                            bool isForUser = feedRow.GetValue<bool>("is_for_user");
                            bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                            List<Department> departments = department.GetAllDepartmentByUserId(requesterUserId, companyId, session).Departments;
                            List<string> departmentIds = new List<string>();

                            foreach (Department depart in departments)
                            {
                                departmentIds.Add(depart.Id);
                            }

                            if (!ownerUserId.Equals(requesterUserId))
                            {
                                if (!feed.CheckPostForCurrentUser(requesterUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session))
                                {
                                    continue;
                                }
                            }

                            if (notificationSubType == (int)NotificationFeedSubType.NewPost)
                            {
                                ownerUser = new User().SelectUserBasic(ownerUserId, companyId, false, session).User;
                                string ownerFullName = string.Format("{0} {1}", ownerUser.FirstName, ownerUser.LastName);

                                if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                                {
                                    content = string.Format(NotificationMessage.NotificationText, ownerFullName);
                                }
                                else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                                {
                                    PreparedStatement psFeedImageUrl = session.Prepare(CQLGenerator.CountStatement("feed_image_url",
                                        new List<string> { "feed_id", "is_valid" }));
                                    int countOfImages = (int)session.Execute(psFeedImageUrl.Bind(feedId, true)).FirstOrDefault().GetValue<long>("count");

                                    if (countOfImages > 1)
                                    {
                                        content = string.Format(NotificationMessage.NotificationImageMany, ownerFullName, countOfImages);
                                    }
                                    else
                                    {
                                        content = string.Format(NotificationMessage.NotificationImageOne, ownerFullName);
                                    }
                                }
                                else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                                {
                                    PreparedStatement psFeedSharedUrl = session.Prepare(CQLGenerator.SelectStatement("feed_shared_url",
                                        new List<string> { "caption" }, new List<string> { "id", "is_valid" }));
                                    Row feedSharedUrlRow = session.Execute(psFeedSharedUrl.Bind(feedId, true)).FirstOrDefault();
                                    string title = feedSharedUrlRow["caption"] == null ? string.Empty : feedSharedUrlRow.GetValue<string>("caption");

                                    if (title.Length > 100)
                                    {
                                        title = title.Substring(0, 100);
                                    }

                                    if (string.IsNullOrEmpty(title))
                                    {
                                        content = string.Format(NotificationMessage.NotificationSharedUrlNoTitle, ownerFullName);
                                    }
                                    else
                                    {
                                        content = string.Format(NotificationMessage.NotificationSharedUrlWithTitle, ownerFullName, title);
                                    }
                                }
                                else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                                {
                                    content = string.Format(NotificationMessage.NotificationVideo, ownerFullName);
                                }
                            }
                            else if (notificationSubType == (int)NotificationFeedSubType.NewComment || notificationSubType == (int)NotificationFeedSubType.NewReply)
                            {
                                PreparedStatement psCommentByTimestamp = null;
                                RowSet commentByTimestampRowSet = null;

                                if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                {
                                    psCommentByTimestamp = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc",
                                        new List<string>(), new List<string> { "feed_id", "is_comment_valid" }));
                                    commentByTimestampRowSet = session.Execute(psCommentByTimestamp.Bind(feedId, true));
                                }
                                else
                                {
                                    psCommentByTimestamp = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc",
                                        new List<string>(), new List<string> { "comment_id", "is_reply_valid" }));
                                    commentByTimestampRowSet = session.Execute(psCommentByTimestamp.Bind(commentId, true));
                                }


                                int numberOfCommentors = 0;
                                string lastCommentorUserId = string.Empty;

                                List<string> commentorUserIds = new List<string>();
                                foreach (Row commentByTimestampRow in commentByTimestampRowSet)
                                {
                                    string currentUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                                    if (!currentUserId.Equals(ownerUserId))
                                    {
                                        lastCommentorUserId = currentUserId;

                                        if (!commentorUserIds.Contains(currentUserId))
                                        {
                                            commentorUserIds.Add(currentUserId);
                                            numberOfCommentors++;
                                        }

                                    }
                                }

                                numberOfCommentors--;

                                if (numberOfCommentors < 0)
                                {
                                    continue;
                                }

                                ownerUser = new User().SelectUserBasic(lastCommentorUserId, companyId, false, session).User;
                                string creatorFullName = string.Format("{0} {1}", ownerUser.FirstName, ownerUser.LastName);

                                if (isPostedByAdmin)
                                {
                                    if (numberOfCommentors >= 1)
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentManyOnAdminPost, creatorFullName, numberOfCommentors);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyManyOnAdminComment, creatorFullName, numberOfCommentors);
                                        }

                                    }
                                    else
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentOneOnAdminPost, creatorFullName);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyOneOnAdminComment, creatorFullName);
                                        }

                                    }
                                }
                                else
                                {
                                    if (numberOfCommentors >= 1)
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentManyOnMyPost, creatorFullName, numberOfCommentors);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyManyOnMyComment, creatorFullName, numberOfCommentors);
                                        }

                                    }
                                    else
                                    {
                                        if (notificationSubType == (int)NotificationFeedSubType.NewComment)
                                        {
                                            content = string.Format(NotificationMessage.NotificationCommentOneOnMyPost, creatorFullName);
                                        }
                                        else
                                        {
                                            content = string.Format(NotificationMessage.NotificationReplyOneOnMyComment, creatorFullName);
                                        }

                                    }
                                }

                            }
                            else if (notificationSubType == (int)NotificationFeedSubType.UpvotePost || notificationSubType == (int)NotificationFeedSubType.UpvoteComment || notificationSubType == (int)NotificationFeedSubType.UpvoteReply)
                            {
                                Dictionary<string, int> pointDict = new Dictionary<string, int>();
                                int positivePoint = 0;

                                if (notificationSubType == (int)NotificationFeedSubType.UpvotePost)
                                {
                                    string feedText = string.Empty;
                                    string textVariable = string.Empty;
                                    string tableName = string.Empty;

                                    pointDict = feed.GetPointsForFeed(feedId, session);

                                    positivePoint = pointDict["positivePoint"];

                                    if (positivePoint <= 0)
                                    {
                                        continue;
                                    }

                                    if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                                    {
                                        textVariable = "content";
                                        tableName = "feed_text";
                                    }
                                    else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                                    {
                                        textVariable = "caption";
                                        tableName = "feed_image";
                                    }
                                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                                    {
                                        textVariable = "caption";
                                        tableName = "feed_video";
                                    }
                                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                                    {
                                        textVariable = "caption";
                                        tableName = "feed_shared_url";
                                    }

                                    PreparedStatement psFeedPost = session.Prepare(CQLGenerator.SelectStatement(tableName,
                                        new List<string>(), new List<string> { "id", "is_valid" }));
                                    Row feedPostRow = session.Execute(psFeedPost.Bind(feedId, true)).FirstOrDefault();

                                    if (feedPostRow == null)
                                    {
                                        continue;
                                    }

                                    feedText = feedPostRow[textVariable] != null ? feedPostRow.GetValue<string>(textVariable) : string.Empty;

                                    if (feedText.Length > 100)
                                    {
                                        feedText = feedText.Substring(0, 100);
                                    }

                                    if (!string.IsNullOrEmpty(feedText))
                                    {
                                        content = string.Format(NotificationMessage.NotificationUpvotedPostWithTitle, positivePoint, feedText);
                                    }
                                    else
                                    {
                                        content = string.Format(NotificationMessage.NotificationUpvotedPostNoTitle, positivePoint);
                                    }

                                }
                                else if (notificationSubType == (int)NotificationFeedSubType.UpvoteComment)
                                {
                                    pointDict = feed.GetPointsForComment(feedId, commentId, session);

                                    positivePoint = pointDict["positivePoint"];

                                    if (positivePoint <= 0)
                                    {
                                        continue;
                                    }

                                    PreparedStatement psCommentText = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                                        new List<string>(), new List<string> { "feed_id", "id", "is_valid" }));
                                    Row commentTextRow = session.Execute(psCommentText.Bind(feedId, commentId, true)).FirstOrDefault();

                                    if (commentTextRow == null)
                                    {
                                        continue;
                                    }

                                    string commentText = commentTextRow["content"] != null ? commentTextRow.GetValue<string>("content") : string.Empty;
                                    if (commentText.Length > 100)
                                    {
                                        commentText = commentText.Substring(0, 100);
                                    }

                                    content = string.Format(NotificationMessage.NotificationUpvotedComment, positivePoint, commentText);
                                }
                                else if (notificationSubType == (int)NotificationFeedSubType.UpvoteReply)
                                {
                                    pointDict = feed.GetPointsForReply(commentId, replyId, session);

                                    positivePoint = pointDict["positivePoint"];

                                    if (positivePoint <= 0)
                                    {
                                        continue;
                                    }

                                    PreparedStatement psReplyText = session.Prepare(CQLGenerator.SelectStatement("feed_reply_text",
                                       new List<string>(), new List<string> { "comment_id", "id", "is_valid" }));
                                    Row replyTextRow = session.Execute(psReplyText.Bind(commentId, replyId, true)).FirstOrDefault();

                                    if (replyTextRow == null)
                                    {
                                        continue;
                                    }

                                    string replyText = replyTextRow["content"] != null ? replyTextRow.GetValue<string>("content") : string.Empty;
                                    if (replyText.Length > 100)
                                    {
                                        replyText = replyText.Substring(0, 100);
                                    }

                                    content = string.Format(NotificationMessage.NotificationUpvotedReply, positivePoint, replyText);
                                }
                            }

                            notification = new Notification
                            {
                                Id = notificationId,
                                TaggedFeedId = feedId,
                                TaggedCommentId = commentId,
                                TaggedReplyId = replyId,
                                TaggedUser = ownerUser,
                                Type = notificationType,
                                SubType = notificationSubType,
                                NotificationText = content,
                                IsSeen = isSeen,
                                CreatedOnTimestamp = createdOnTimestamp
                            };
                        }
                        // Game
                        else
                        {
                            feedId = "NA";
                            commentId = "NA";
                            replyId = "NA";

                            Row challengeRow = vh.ValidateChallenge(challengeId, companyId, session);

                            if (challengeRow == null)
                            {
                                DeleteNotificationByUser(requesterUserId, notificationType, notificationSubType, challengeId, feedId, commentId, replyId, notificationId, createdOnTimestampOffset, session);
                                continue;
                            }

                            List<string> playerIds = challengeRow.GetValue<List<string>>("players_ids");
                            string initiatorUserId = playerIds[0];
                            string challengedUserId = playerIds[1];

                            string topicId = challengeRow.GetValue<string>("topic_id");
                            string topicCategoryId = challengeRow.GetValue<string>("topic_category_id");

                            User player = null;
                            Topic topic = new Topic().SelectTopicBasic(topicId, null, companyId, topicCategoryId, null, session).Topic;

                            if (topic != null)
                            {
                                if (notificationSubType == (int)NotificationGameSubType.NewChallenge)
                                {
                                    if (challengedUserId.Equals(requesterUserId))
                                    {
                                        player = new User().SelectUserBasic(initiatorUserId, companyId, false, session).User;
                                        string playerFullName = string.Format("{0} {1}", player.FirstName, player.LastName);
                                        content = string.Format(NotificationMessage.NotificationChallengeNew, playerFullName, topic.TopicTitle);
                                    }
                                }
                                else if (notificationSubType == (int)NotificationGameSubType.ChallengedUserCompletedChallenge)
                                {
                                    if (initiatorUserId.Equals(requesterUserId))
                                    {
                                        player = new User().SelectUserBasic(challengedUserId, companyId, false, session).User;
                                        string playerFullName = string.Format("{0} {1}", player.FirstName, player.LastName);
                                        content = string.Format(NotificationMessage.NotificationChallengeCompleted, playerFullName, topic.TopicTitle);
                                    }
                                }

                                notification = new Notification
                                {
                                    Id = notificationId,
                                    TaggedChallengeId = challengeId,
                                    TaggedUser = player,
                                    Type = notificationType,
                                    SubType = notificationSubType,
                                    NotificationText = content,
                                    IsSeen = isSeen,
                                    CreatedOnTimestamp = createdOnTimestamp
                                };
                            }

                        }

                        if (notification != null)
                        {
                            response.Notifications.Add(notification);

                            if (response.Notifications.Count() == limit)
                            {
                                isSelectCompleted = true;
                                break;
                            }

                        }
                    }// for

                    if (allNotificationRowList.Count == 0)
                    {
                        isSelectCompleted = true;
                    }

                }// while

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool DeleteNotificationByUser(string targetedUserId, int type, int subType, string challengeId, string feedId, string commentId, string replyId, string notificationId, DateTimeOffset createdTimestamp, ISession session)
        {
            try
            {
                BatchStatement batchStatement = new BatchStatement();

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("notification_privacy",
                    new List<string> { "targeted_user_id", "notification_type", "notification_subtype", "challenge_id", "feed_id", "comment_id", "reply_id", "notification_id" }));
                batchStatement.Add(preparedStatement.Bind(targetedUserId, type, subType, challengeId, feedId, commentId, replyId, notificationId));

                preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("notification_privacy_desc",
                    new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                batchStatement.Add(preparedStatement.Bind(targetedUserId, createdTimestamp, notificationId));

                preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("notification_privacy_asc",
                    new List<string> { "targeted_user_id", "notification_created_on_timestamp", "notification_id" }));
                batchStatement.Add(preparedStatement.Bind(targetedUserId, createdTimestamp, notificationId));

                session.Execute(batchStatement);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }

        public bool DeleteNotificationByFeed(string feedId, ISession session)
        {
            try
            {
                BatchStatement batchStatement = new BatchStatement();

                // Use feed to get targeted audience
                // Remove from user

                // Feed.cs includes select targeted audience

                session.Execute(batchStatement);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }
    }
}