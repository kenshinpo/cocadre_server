﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class RSCard
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum RSCardQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        public enum RSCardType
        {
            SelectOne = 1,
            MultiChoice = 2,
            Text = 3,
            NumberRange = 4,
            DropList = 5,
            Instructional = 6
        }

        public enum RSCardStatus
        {
            Deleted = -1,
            Active = 1,
            Hidden = 2
        }

        [DataMember]
        public string CardId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public bool HasToLogic { get; set; }

        [DataMember]
        public bool HasFromLogic { get; set; }

        [DataMember]
        public bool HasImage { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public List<RSImage> CardImages { get; set; }

        [DataMember]
        public bool ToBeSkipped { get; set; }

        [DataMember]
        public bool HasCustomAnswer { get; set; }

        [DataMember]
        public string CustomAnswerInstruction { get; set; }

        [DataMember]
        public bool IsOptionRandomized { get; set; }

        [DataMember]
        public int MiniOptionToSelect { get; set; }

        [DataMember]
        public int MaxOptionToSelect { get; set; }

        [DataMember]
        public bool AllowMultipleLines { get; set; }

        [DataMember]
        public bool HasPageBreak { get; set; }

        [DataMember]
        public int Paging { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public int BackgroundType { get; set; }

        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public List<RSOption> Options { get; set; }

        [DataMember]
        public List<RSOption> CustomAnswers { get; set; }

        [DataMember]
        public int MaxRange { get; set; }

        [DataMember]
        public int MinRange { get; set; }

        [DataMember]
        public int SelectedRange { get; set; }

        [DataMember]
        public RSOption CustomAnswer { get; set; }

        [DataMember]
        public int TotalResponses { get; set; }

        public string GenerateCardID()
        {
            return UUIDGenerator.GenerateUniqueIDForRSCard();
        }

        public RSCardCreateResponse CreateCard(string cardId,
                                               int type,
                                               string content,
                                               bool hasImage,
                                               List<RSImage> images,
                                               bool toBeSkipped,
                                               bool hasCustomAnswer,
                                               string customAnswerInstruction,
                                               bool isOptionRandomized,
                                               bool allowMultipleLines,
                                               bool hasPageBreak,
                                               int backgoundType,
                                               string note,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               List<RSOption> options,
                                               int miniOptionToSelect = 0,
                                               int maxOptionToSelect = 0,
                                               int maxRange = 0,
                                               int minRange = 0)
        {
            RSCardCreateResponse response = new RSCardCreateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                // Check content
                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Invalid content");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardContentEmpty);
                    response.ErrorMessage = ErrorMessage.RSCardContentEmpty;
                    return response;
                }

                if (string.IsNullOrEmpty(cardId))
                {
                    cardId = UUIDGenerator.GenerateUniqueIDForRSCard();
                }

                RSOption optionManagement = new RSOption();
                RSOptionCreateResponse optionResponse = new RSOptionCreateResponse();
                PreparedStatement ps = null;
                BatchStatement batchStatement = new BatchStatement();

                // Setting to default
                switch (type)
                {
                    case (int)RSCardType.SelectOne:
                        minRange = 0;
                        maxRange = 0;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)RSCardType.Text:
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        isOptionRandomized = false;
                        minRange = 0;
                        maxRange = 0;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)RSCardType.NumberRange:
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        isOptionRandomized = false;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)RSCardType.DropList:
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        minRange = 0;
                        maxRange = 0;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                    case (int)RSCardType.Instructional:
                        toBeSkipped = false;
                        hasCustomAnswer = false;
                        customAnswerInstruction = null;
                        isOptionRandomized = false;
                        minRange = 0;
                        maxRange = 0;
                        miniOptionToSelect = 0;
                        maxOptionToSelect = 0;
                        break;
                }

                if (type == (int)RSCardType.SelectOne || type == (int)RSCardType.MultiChoice || type == (int)RSCardType.DropList)
                {
                    if (type == (int)RSCardType.MultiChoice)
                    {
                        if (miniOptionToSelect < 0 || maxOptionToSelect == 0 || maxOptionToSelect < miniOptionToSelect)
                        {
                            Log.Error("Mismatch in option selection values");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSOptionSelectionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionSelectionRangeIsInvalid;
                            return response;
                        }
                    }
                    optionResponse = optionManagement.CreateOptions(adminUserId, cardId, topicId, options, session);

                    if (!optionResponse.Success)
                    {
                        Log.Error(optionResponse.ErrorMessage);
                        response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                        response.ErrorMessage = optionResponse.ErrorMessage;
                        return response;
                    }

                    foreach (BoundStatement bs in optionResponse.boundStatements)
                    {
                        batchStatement.Add(bs);
                    }
                }

                if (type == (int)RSCardType.NumberRange)
                {
                    if (maxRange <= minRange)
                    {
                        Log.Error("Mismatch in range values");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSOptionRangeIsInvalid);
                        response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                        return response;
                    }
                }

                ps = session.Prepare(CQLGenerator.CountStatement("rs_card_order", new List<string> { "rs_topic_id" }));
                int ordering = (int)session.Execute(ps.Bind(topicId)).FirstOrDefault().GetValue<long>("count") + 1;

                ps = session.Prepare(CQLGenerator.InsertStatement("rs_card",
                    new List<string> { "rs_topic_id", "id", "type", "has_image", "content", "to_be_skipped", "has_custom_answer", "custom_answer_instruction", "is_option_randomized", "mini_option_to_select", "max_option_to_select", "allow_multiple_lines", "has_page_break", "ordering", "background_image_type", "note", "max_range", "min_range", "status", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                batchStatement.Add(ps.Bind(topicId, cardId, type, hasImage, content, toBeSkipped, hasCustomAnswer, customAnswerInstruction, isOptionRandomized, miniOptionToSelect, maxOptionToSelect, allowMultipleLines, hasPageBreak, ordering, backgoundType, note, maxRange, minRange, (int)RSCardStatus.Active, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));

                if (hasImage)
                {
                    foreach (RSImage image in images)
                    {
                        string imageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                        string imageUrl = image.ImageUrl;

                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            Log.Error("Missing card image url");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSCardImageUrlMissing);
                            response.ErrorMessage = ErrorMessage.RSCardImageUrlMissing;
                            return response;
                        }

                        int order = image.Ordering;

                        ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_image",
                            new List<string> { "card_id", "image_id", "image_url", "ordering" }));
                        batchStatement.Add(ps.Bind(cardId, imageId, imageUrl, order));
                    }
                }

                ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order",
                    new List<string> { "rs_topic_id", "card_id", "ordering" }));
                batchStatement.Add(ps.Bind(topicId, cardId, ordering));

                session.Execute(batchStatement);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardUpdateResponse UpdateCard(string cardId,
                                               string content,
                                               bool hasImage,
                                               List<RSImage> images,
                                               string note,
                                               bool hasPageBreak,
                                               List<RSOption> options,
                                               string categoryId,
                                               string topicId,
                                               string adminUserId,
                                               string companyId,
                                               int miniOptionToSelect = 0,
                                               int maxOptionToSelect = 0)
        {
            RSCardUpdateResponse response = new RSCardUpdateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                // Check card row
                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                // Check content
                if (string.IsNullOrEmpty(content))
                {
                    Log.Error("Invalid content");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardContentEmpty);
                    response.ErrorMessage = ErrorMessage.RSCardContentEmpty;
                    return response;
                }

                int type = cardRow.GetValue<int>("type");

                if (type == (int)RSCardType.MultiChoice)
                {
                    if (miniOptionToSelect < 0 || maxOptionToSelect == 0 || maxOptionToSelect < miniOptionToSelect)
                    {
                        Log.Error("Mismatch in option selection values");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSOptionSelectionRangeIsInvalid);
                        response.ErrorMessage = ErrorMessage.RSOptionSelectionRangeIsInvalid;
                        return response;
                    }
                }

                BatchStatement deleteBatch = new BatchStatement();
                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "has_image", "content", "note", "has_page_break", "mini_option_to_select", "max_option_to_select", "last_modified_by_admin_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(hasImage, content, note, hasPageBreak, miniOptionToSelect, maxOptionToSelect, adminUserId, DateTime.UtcNow, topicId, cardId));

                // Update option
                int currentCardOrdering = cardRow.GetValue<int>("ordering");
                RSOptionUpdateResponse optionResponse = new RSOption().UpdateOptions(cardId, topicId, currentCardOrdering, options, companyId, session);

                if (!optionResponse.Success)
                {
                    Log.Error(optionResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                    response.ErrorMessage = optionResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in optionResponse.boundStatements)
                {
                    updateBatch.Add(bs);
                }

                // Delete from S3
                bool currentHasImage = cardRow.GetValue<bool>("has_image");
                List<string> currentImageUrls = new List<string>();
                if (currentHasImage)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_image", new List<string>(), new List<string> { "card_id" }));
                    RowSet currentImageRowset = session.Execute(ps.Bind(cardId));

                    foreach (Row currentImageRow in currentImageRowset)
                    {
                        string imageUrl = currentImageRow.GetValue<string>("image_url");
                        currentImageUrls.Add(imageUrl);
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_image", new List<string> { "card_id" }));
                    deleteBatch.Add(ps.Bind(cardId));
                }

                if (hasImage && images != null && images.Count > 0)
                {
                    int order = 1;
                    foreach (RSImage image in images)
                    {
                        string imageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                        string imageUrl = image.ImageUrl;

                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            Log.Error("Missing card image url");
                            response.ErrorCode = Int16.Parse(ErrorCode.RSCardImageUrlMissing);
                            response.ErrorMessage = ErrorMessage.RSCardImageUrlMissing;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_image",
                            new List<string> { "card_id", "image_id", "image_url", "ordering" }));
                        updateBatch.Add(ps.Bind(cardId, imageId, imageUrl, order));

                        order++;

                        if (currentImageUrls.Contains(imageUrl))
                        {
                            currentImageUrls.Remove(imageUrl);
                        }
                    }
                }

                if (currentImageUrls.Count > 0)
                {
                    String bucketName = "cocadre-" + companyId.ToLower();
                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        // Remaining image url to be remove
                        foreach (string currentImageUrl in currentImageUrls)
                        {
                            string path = currentImageUrl.Replace("https://", "");
                            string[] splitPath = path.Split('/');
                            string imageName = splitPath[splitPath.Count() - 1];

                            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                            {
                                BucketName = bucketName,
                                Key = "surveys/" + topicId + "/" + cardId + "/" + imageName
                            };

                            s3Client.DeleteObject(deleteObjectRequest);
                        }
                    }
                }

                session.Execute(deleteBatch);
                session.Execute(updateBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardUpdateResponse DeleteAllCards(string topicId, string companyId, ISession session)
        {
            RSCardUpdateResponse response = new RSCardUpdateResponse();
            response.Success = false;
            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardRowset = session.Execute(ps.Bind(topicId));

                String bucketName = "cocadre-" + companyId.ToLower();
                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    foreach (Row cardRow in cardRowset)
                    {
                        deleteBatch = new BatchStatement();
                        string cardId = cardRow.GetValue<string>("id");
                        bool hasImage = cardRow.GetValue<bool>("has_image");
                        int order = cardRow.GetValue<int>("ordering");
                        if (hasImage)
                        {
                            // Remove from S3
                            ListObjectsRequest listRequest = new ListObjectsRequest();
                            listRequest.BucketName = bucketName;
                            listRequest.Prefix = "surveys/" + topicId + "/" + cardId;

                            ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                            foreach (S3Object imageObject in listResponse.S3Objects)
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = imageObject.Key;
                                s3Client.DeleteObject(deleteRequest);
                            }

                            DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                            deleteFolderRequest.BucketName = bucketName;
                            deleteFolderRequest.Key = "surveys/" + topicId + "/" + cardId;
                            s3Client.DeleteObject(deleteFolderRequest);

                            ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_image", new List<string> { "card_id" }));
                            deleteBatch.Add(ps.Bind(cardId));
                        }

                        RSOptionUpdateResponse optionResponse = new RSOption().DeleteAllOptions(cardId, companyId, session);
                        if (!optionResponse.Success)
                        {
                            Log.Error(optionResponse.ErrorMessage);
                            response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                            response.ErrorMessage = optionResponse.ErrorMessage;
                            return response;
                        }

                        foreach (BoundStatement bs in optionResponse.boundStatements)
                        {
                            deleteBatch.Add(bs);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card", new List<string> { "rs_topic_id", "id" }));
                        deleteBatch.Add(ps.Bind(topicId, cardId));

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                        deleteBatch.Add(ps.Bind(topicId, order, cardId));

                        session.Execute(deleteBatch);
                    }
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardUpdateResponse DeleteCard(string topicId, string categoryId, string cardId, string adminUserId, string companyId)
        {
            RSCardUpdateResponse response = new RSCardUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                // Check topic category
                Row topicCategoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);

                if (topicCategoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CategoryInvalid);
                    response.ErrorMessage = ErrorMessage.CategoryInvalid;
                    return response;
                }

                // Check topic row
                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                // Check card row
                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Card already been deleted: " + cardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardAlreadyBeenDeleted);
                    response.ErrorMessage = ErrorMessage.RSCardAlreadyBeenDeleted;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                int currentOrder = cardRow.GetValue<int>("ordering");

                if (CheckCardForFromLogic(cardId, session))
                {
                    Log.Error("Card has from logic");
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardHasLogic);
                    response.ErrorMessage = ErrorMessage.RSCardHasLogic;
                    return response;
                }

                if (!IsAbleToDelete(topicId, currentOrder, session))
                {
                    Log.Error("Previous card does not have page break");
                    response.ErrorCode = Int16.Parse(ErrorCode.RSNoPageBreakForDeleteLogic);
                    response.ErrorMessage = ErrorMessage.RSNoPageBreakForDeleteLogic;
                    return response;
                }

                // Remove from S3
                String bucketName = "cocadre-" + companyId.ToLower();

                using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                {
                    ListObjectsRequest listRequest = new ListObjectsRequest();
                    listRequest.BucketName = bucketName;
                    listRequest.Prefix = "surveys/" + topicId + "/" + cardId;

                    ListObjectsResponse listResponse = s3Client.ListObjects(listRequest);
                    foreach (S3Object imageObject in listResponse.S3Objects)
                    {
                        DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                        deleteRequest.BucketName = bucketName;
                        deleteRequest.Key = imageObject.Key;
                        s3Client.DeleteObject(deleteRequest);
                    }

                    DeleteObjectRequest deleteFolderRequest = new DeleteObjectRequest();
                    deleteFolderRequest.BucketName = bucketName;
                    deleteFolderRequest.Key = "surveys/" + topicId + "/" + cardId;
                    s3Client.DeleteObject(deleteFolderRequest);
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_image", new List<string> { "card_id" }));
                deleteBatch.Add(ps.Bind(cardId));

                RSOptionUpdateResponse optionResponse = new RSOption().DeleteAllOptions(cardId, companyId, session);
                if (!optionResponse.Success)
                {
                    Log.Error(optionResponse.ErrorMessage);
                    response.ErrorCode = Convert.ToInt16(optionResponse.ErrorCode);
                    response.ErrorMessage = optionResponse.ErrorMessage;
                    return response;
                }

                foreach (BoundStatement bs in optionResponse.boundStatements)
                {
                    deleteBatch.Add(bs);
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card", new List<string> { "rs_topic_id", "id" }));
                deleteBatch.Add(ps.Bind(topicId, cardId));

                // Reorder card
                ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                RowSet orderRowset = session.Execute(ps.Bind(topicId, currentOrder));

                foreach (Row orderRow in orderRowset)
                {
                    int order = orderRow.GetValue<int>("ordering");
                    string nextCardId = orderRow.GetValue<string>("card_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(topicId, order, nextCardId));

                    ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                    updateBatch.Add(ps.Bind(order - 1, topicId, nextCardId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                    updateBatch.Add(ps.Bind(topicId, order - 1, nextCardId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                deleteBatch.Add(ps.Bind(topicId, currentOrder, cardId));

                session.Execute(deleteBatch);
                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public int SelectNumberOfCards(string topicId, ISession session)
        {
            int number = 0;

            try
            {
                PreparedStatement psCard = session.Prepare(CQLGenerator.CountStatement("rs_card",
                    new List<string> { "rs_topic_id", "status" }));
                Row cardRow = session.Execute(psCard.Bind(topicId, (int)RSCardStatus.Active)).FirstOrDefault();

                if (cardRow != null)
                {
                    number = (int)cardRow.GetValue<long>("count");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return number;
        }

        public RSCardSelectAllResponse SelectAllCardsByUser(string requesterUserId, string topicId, ISession mainSession, ISession analyticSession)
        {
            RSCardSelectAllResponse response = new RSCardSelectAllResponse();
            response.Cards = new List<RSCard>();
            response.Success = false;
            try
            {
                // Select card details using rs_card_order
                PreparedStatement ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardRowset = mainSession.Execute(ps.Bind(topicId));

                int page = 1;
                foreach (Row cardRow in cardRowset)
                {
                    string cardId = cardRow.GetValue<string>("card_id");
                    int ordering = cardRow.GetValue<int>("ordering");

                    RSCard selectedCard = SelectCard(requesterUserId, null, cardId, topicId, (int)RSCardQueryType.FullDetail, null, true, mainSession, analyticSession).Card;

                    if (selectedCard != null)
                    {
                        selectedCard.Ordering = ordering;
                        selectedCard.Paging = page;
                        if (selectedCard.HasPageBreak)
                        {
                            page++;
                        }
                        response.Cards.Add(selectedCard);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardSelectAllResponse SelectAllCards(string topicId, string categoryId, string adminUserId, string companyId, int queryType, string containsName = null, ISession session = null)
        {
            RSCardSelectAllResponse response = new RSCardSelectAllResponse();
            response.Cards = new List<RSCard>();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, session);
                    if (categoryRow == null)
                    {
                        Log.Error("Invalid categoryId: " + categoryId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                        response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                        return response;
                    }

                    Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);
                    if (topicRow == null)
                    {
                        Log.Error("Invalid topicId: " + topicId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                        response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                        return response;
                    }

                    response.Introduction = topicRow.GetValue<string>("introduction");
                    response.ClosingWords = topicRow.GetValue<string>("closing_words");
                    response.TopicTitle = topicRow.GetValue<string>("title");
                    response.TopicIconUrl = topicRow.GetValue<string>("icon_url");
                }


                // Select card details using rs_card_order
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }));
                RowSet cardRowset = session.Execute(ps.Bind(topicId));

                int page = 1;
                foreach (Row cardRow in cardRowset)
                {
                    string cardId = cardRow.GetValue<string>("card_id");
                    int ordering = cardRow.GetValue<int>("ordering");

                    RSCard selectedCard = SelectCard(adminUserId, companyId, cardId, topicId, queryType, null, false, session, null).Card;

                    if (selectedCard != null)
                    {
                        selectedCard.Ordering = ordering;
                        selectedCard.Paging = page;

                        if (selectedCard.HasPageBreak)
                        {
                            page++;
                        }

                        if (!string.IsNullOrEmpty(containsName))
                        {
                            if (selectedCard.Content.Contains(containsName))
                            {
                                response.Cards.Add(selectedCard);
                            }
                        }
                        else
                        {
                            response.Cards.Add(selectedCard);
                        }

                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSCardSelectResponse SelectCard(string requesterUserId, string companyId, string cardId, string topicId, int queryType, Row cardRow = null, bool getAnswer = false, ISession session = null, ISession analyticSession = null)
        {
            RSCardSelectResponse response = new RSCardSelectResponse();
            response.Card = null;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (session == null)
                {
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(requesterUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                // cardRow can be already fetched from other methods
                PreparedStatement ps = null;
                if (cardRow == null)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id", "id", "status" }));
                    cardRow = session.Execute(ps.Bind(topicId, cardId, (int)RSCardStatus.Active)).FirstOrDefault();

                    if (cardRow == null)
                    {
                        Log.Error("Invalid cardId: " + cardId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardInvalid);
                        response.ErrorMessage = ErrorMessage.RSCardInvalid;
                        return response;
                    }
                }

                // Get Basic Details
                int ordering = cardRow.GetValue<int>("ordering");
                string content = cardRow.GetValue<string>("content");
                bool hasImage = cardRow.GetValue<bool>("has_image");
                int type = cardRow.GetValue<int>("type");
                bool hasPageBreak = cardRow.GetValue<bool>("has_page_break");
                bool hasToLogic = false;
                bool hasFromLogic = false;
                List<RSImage> cardImages = new List<RSImage>();
                List<RSOption> options = new List<RSOption>();


                bool isAllowToBeSkipped = false;
                bool hasCustomAnswer = false;
                string customAnswerInstruction = string.Empty;
                bool isOptionRandomized = false;
                int miniOption = 0;
                int maxOption = 0;
                bool isAllowMultipleLines = false;
                int backgroundType = 1;
                string note = string.Empty;
                int page = 1;
                int maxRange = 0;
                int minRange = 0;

                int selectedRange = -1;
                RSOption customAnswer = null;

                if (hasImage)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_image", new List<string>(), new List<string> { "card_id" }));
                    RowSet cardImageRowset = session.Execute(ps.Bind(cardId));

                    foreach (Row cardImageRow in cardImageRowset)
                    {
                        string imageId = cardImageRow.GetValue<string>("image_id");
                        string imageUrl = cardImageRow.GetValue<string>("image_url");
                        int order = cardImageRow.GetValue<int>("ordering");

                        RSImage image = new RSImage
                        {
                            ImageId = imageId,
                            ImageUrl = imageUrl,
                            Ordering = order
                        };

                        cardImages.Add(image);
                    }

                    cardImages = cardImages.OrderBy(o => o.Ordering).ToList();
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id" }));
                RowSet cardLogicRowset = session.Execute(ps.Bind(cardId));
                List<Row> cardToLogicList = cardLogicRowset.ToList();

                if (cardToLogicList.Count() > 0)
                {
                    hasToLogic = true;
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_from_logic", new List<string>(), new List<string> { "card_id" }));
                cardLogicRowset = session.Execute(ps.Bind(cardId));
                List<Row> cardFromLogicList = cardLogicRowset.ToList();

                if (cardFromLogicList.Count() > 0)
                {
                    hasFromLogic = true;
                }

                RSOption optionManager = new RSOption();
                Analytic analyticManager = new Analytic();

                // Get Full Details
                if (queryType == (int)RSCardQueryType.FullDetail)
                {
                    isAllowToBeSkipped = cardRow.GetValue<bool>("to_be_skipped");
                    hasCustomAnswer = cardRow.GetValue<bool>("has_custom_answer");
                    customAnswerInstruction = cardRow.GetValue<string>("custom_answer_instruction");
                    isOptionRandomized = cardRow.GetValue<bool>("is_option_randomized");
                    miniOption = cardRow.GetValue<int>("mini_option_to_select");
                    maxOption = cardRow.GetValue<int>("max_option_to_select");
                    isAllowMultipleLines = cardRow.GetValue<bool>("allow_multiple_lines");
                    backgroundType = cardRow.GetValue<int>("background_image_type");
                    note = cardRow.GetValue<string>("note");
                    page = FindPageForCard(topicId, ordering, session);

                    if (type != (int)RSCardType.Instructional && type != (int)RSCardType.Text && type != (int)RSCardType.NumberRange)
                    {
                        options = optionManager.SelectOptions(topicId, companyId, cardId, session, analyticSession, requesterUserId, getAnswer);

                        if (hasCustomAnswer && getAnswer)
                        {
                            customAnswer = analyticManager.SelectCustomAnswerFromCardByUser(topicId, cardId, requesterUserId, session, analyticSession);
                        }
                    }
                    else
                    {
                        if (type == (int)RSCardType.NumberRange)
                        {
                            maxRange = cardRow.GetValue<int>("max_range");
                            minRange = cardRow.GetValue<int>("min_range");

                            // Get answer
                            if (getAnswer)
                            {
                                selectedRange = analyticManager.SelectRangeFromCard(cardId, requesterUserId, analyticSession);
                            }
                        }
                        else if (type == (int)RSCardType.Text)
                        {
                            // Get answer
                            if (getAnswer)
                            {
                                customAnswer = analyticManager.SelectCustomAnswerFromCardByUser(topicId, cardId, requesterUserId, session, analyticSession);
                            }

                        }
                    }

                }

                response.Card = new RSCard
                {
                    CardId = cardId,
                    Type = type,
                    HasImage = hasImage,
                    Content = content,
                    HasToLogic = hasToLogic,
                    HasFromLogic = hasFromLogic,
                    CardImages = cardImages,
                    Options = options,
                    ToBeSkipped = isAllowToBeSkipped,
                    HasCustomAnswer = hasCustomAnswer,
                    CustomAnswerInstruction = customAnswerInstruction,
                    IsOptionRandomized = isOptionRandomized,
                    MiniOptionToSelect = miniOption,
                    MaxOptionToSelect = maxOption,
                    AllowMultipleLines = isAllowMultipleLines,
                    HasPageBreak = hasPageBreak,
                    Ordering = ordering,
                    Paging = page,
                    BackgroundType = backgroundType,
                    Note = note,
                    MaxRange = maxRange,
                    MinRange = minRange,

                    SelectedRange = selectedRange,
                    CustomAnswer = customAnswer
                };

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSCardAnswerResponse AnswerCard(string answeredByUserId, string companyId, string topicId, string topicCategoryId, string cardId, int rangeSelected, List<string> optionIds, RSOption customAnswer)
        {
            RSCardAnswerResponse response = new RSCardAnswerResponse();
            response.NextCards = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticsSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(answeredByUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, topicCategoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, mainSession);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                bool hasCustomAnswer = cardRow.GetValue<bool>("has_custom_answer");
                int currentOrder = cardRow.GetValue<int>("ordering");
                int cardType = cardRow.GetValue<int>("type");

                BatchStatement updateBatch = new BatchStatement();

                bool isCustom = false;
                RSOption newCustomAnswer = new RSOption();
                RSOption previousCustomAnswer = new RSOption();

                // Text, custom answer, range
                if (optionIds == null || optionIds != null && optionIds.Count == 0)
                {
                    if (cardType == (int)RSCard.RSCardType.NumberRange)
                    {
                        int maxRange = cardRow.GetValue<int>("max_range");
                        int minRange = cardRow.GetValue<int>("min_range");

                        if (rangeSelected < minRange || rangeSelected > maxRange)
                        {
                            Log.Error(ErrorMessage.RSOptionRangeIsInvalid);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionRangeIsInvalid);
                            response.ErrorMessage = ErrorMessage.RSOptionRangeIsInvalid;
                            return response;
                        }
                    }
                    else
                    {
                        isCustom = true;

                        if(customAnswer == null || customAnswer != null && string.IsNullOrEmpty(customAnswer.Content))
                        {
                            Log.Error(ErrorMessage.RSCustomAnswerNotFilled);
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSCustomAnswerNotFilled);
                            response.ErrorMessage = ErrorMessage.RSCustomAnswerNotFilled;
                            return response;
                        }

                        // Previously was not answered
                        if (string.IsNullOrEmpty(customAnswer.OptionId))
                        {
                            customAnswer.OptionId = UUIDGenerator.GenerateUniqueIDForRSOption(true);
                        }

                        RSOptionCreateResponse optionResponse = new RSOption().CreateCustomOption(answeredByUserId, cardId, customAnswer, mainSession);

                        if (!optionResponse.Success)
                        {
                            Log.Error(optionResponse.ErrorMessage);
                            response.ErrorCode = optionResponse.ErrorCode;
                            response.ErrorMessage = optionResponse.ErrorMessage;
                            return response;
                        }

                        foreach (BoundStatement bs in optionResponse.boundStatements)
                        {
                            updateBatch.Add(bs);
                        }

                        previousCustomAnswer.OptionId = customAnswer.OptionId;
                        previousCustomAnswer.Content = optionResponse.previousCustomAnswer;

                        mainSession.Execute(updateBatch);
                    }
                }
                else
                {
                    // Only take first option for select one choice 
                    if (cardType == (int)RSCard.RSCardType.SelectOne)
                    {
                        optionIds = optionIds.Take(1).ToList();
                    }

                    // Still have to check for previous custom answer
                    if (hasCustomAnswer)
                    {
                        PreparedStatement ps = analyticsSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_user",
                            new List<string>(), new List<string> { "created_by_user_id", "card_id" }));
                        Row customOptionRow = analyticsSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                        if (customOptionRow != null)
                        {
                            string customOptionId = customOptionRow.GetValue<string>("option_id");
                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option",
                              new List<string>(), new List<string> { "card_id", "id" }));
                            Row customAnswerRow = mainSession.Execute(ps.Bind(cardId, customOptionId)).FirstOrDefault();

                            if (customAnswerRow != null)
                            {
                                string content = customAnswerRow.GetValue<string>("content");
                                previousCustomAnswer.OptionId = customOptionId;
                                previousCustomAnswer.Content = content;

                                // Remove from custom option
                                ps = mainSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option", new List<string> { "card_id", "id" }));
                                mainSession.Execute(ps.Bind(cardId, customOptionId));
                            }
                        }
                    }

                }

                // Update analytics 
                Analytic analytic = new Analytic();
                analytic.AnswerRSCard(answeredByUserId, topicId, cardId, cardType, hasCustomAnswer, isCustom, optionIds, customAnswer, previousCustomAnswer, rangeSelected, DateTime.UtcNow, analyticsSession);

                //// Fetch nextCard
                //ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id", "option_id" }));
                //Row nextLogicRow = mainSession.Execute(ps.Bind(cardId, optionId)).FirstOrDefault();

                //// Follow flow, next question inline
                //if (nextLogicRow != null)
                //{
                //    string nextCardId = nextLogicRow.GetValue<string>("next_card_id");

                //    if (nextCardId.Equals(DefaultResource.RSEndCardId))
                //    {
                //        response.IsEnd = true;
                //        response.ClosingWords = topicRow.GetValue<string>("closing_words");
                //    }
                //    else
                //    {
                //        RSCard nextCard = SelectCard(null, companyId, nextCardId, topicId, (int)RSCardQueryType.FullDetail, null, mainSession).Card;
                //        response.NextCards.Add(nextCard);

                //        currentOrder = nextCard.Ordering;
                //    }
                //}


                //int nextOrder = currentOrder + 1;
                //ps = mainSession.Prepare(CQLGenerator.SelectStatementWithComparison("rs_card", new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                //RowSet nextCardRowset = mainSession.Execute(ps.Bind(topicId, nextOrder));
                //List<Row> nextCardRowList = nextCardRowset.ToList();

                //// Go to the ending text
                //if (nextCardRowList.Count == 0)
                //{
                //    response.IsEnd = true;
                //    response.ClosingWords = topicRow.GetValue<string>("closing_words");
                //}
                //else
                //{
                //    foreach (Row nextCardRow in nextCardRowList)
                //    {
                //        string nextCardId = nextCardRow.GetValue<string>("id");
                //        RSCard nextCard = SelectCard(null, companyId, nextCardId, topicId, (int)RSCardQueryType.FullDetail, nextCardRow, mainSession).Card;
                //        response.NextCards.Add(nextCard);
                //        if (nextCard.HasPageBreak)
                //        {
                //            break;
                //        }
                //    }
                //}

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSCardReorderResponse ReorderCard(string cardId, string topicId, string categoryId, string adminUserId, string companyId, int insertAtOrder)
        {
            RSCardReorderResponse response = new RSCardReorderResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                bool currentHasPageBreak = cardRow.GetValue<bool>("has_page_break");
                int currentOrder = cardRow.GetValue<int>("ordering");

                if (currentOrder != insertAtOrder)
                {
                    if (vh.ValidateLogicForCard(cardId, topicId, session))
                    {
                        Log.Error("Card has logic");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSCardHasLogic);
                        response.ErrorMessage = ErrorMessage.RSCardHasLogic;
                        return response;
                    }

                    if (!IsAbleToReorder(topicId, currentHasPageBreak, insertAtOrder, session))
                    {
                        Log.Error("The next card has logic but modified card does not have a page break");
                        response.ErrorCode = Int16.Parse(ErrorCode.RSNoPageBreakForOrderLogic);
                        response.ErrorMessage = ErrorMessage.RSNoPageBreakForOrderLogic;
                        return response;
                    }

                    BatchStatement updateBatch = new BatchStatement();
                    BatchStatement deleteBatch = new BatchStatement();

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                    updateBatch.Add(ps.Bind(insertAtOrder, topicId, cardId));

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                    deleteBatch.Add(ps.Bind(topicId, currentOrder, cardId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                    updateBatch.Add(ps.Bind(topicId, insertAtOrder, cardId));

                    // Ignore the top range
                    if (currentOrder > insertAtOrder)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatementWithRangeComparison("rs_card_order",
                            new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThanOrEquals, "ordering", CQLGenerator.Comparison.LessThan, 0));
                        RowSet orderRowset = session.Execute(ps.Bind(topicId, insertAtOrder, currentOrder));

                        foreach (Row orderRow in orderRowset)
                        {
                            string updateCardId = orderRow.GetValue<string>("card_id");
                            int updateCardOrder = orderRow.GetValue<int>("ordering");

                            ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            deleteBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));

                            updateCardOrder++;

                            ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                            updateBatch.Add(ps.Bind(updateCardOrder, topicId, updateCardId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            updateBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));
                        }
                    }
                    // Ignore the bottom range
                    else
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatementWithRangeComparison("rs_card_order",
                            new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, "ordering", CQLGenerator.Comparison.LessThanOrEquals, 0));
                        RowSet orderRowset = session.Execute(ps.Bind(topicId, currentOrder, insertAtOrder));

                        foreach (Row orderRow in orderRowset)
                        {
                            string updateCardId = orderRow.GetValue<string>("card_id");
                            int updateCardOrder = orderRow.GetValue<int>("ordering");

                            ps = session.Prepare(CQLGenerator.DeleteStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            deleteBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));

                            updateCardOrder--;

                            ps = session.Prepare(CQLGenerator.UpdateStatement("rs_card", new List<string> { "rs_topic_id", "id" }, new List<string> { "ordering" }, new List<string>()));
                            updateBatch.Add(ps.Bind(updateCardOrder, topicId, updateCardId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_card_order", new List<string> { "rs_topic_id", "ordering", "card_id" }));
                            updateBatch.Add(ps.Bind(topicId, updateCardOrder, updateCardId));
                        }
                    }

                    session.Execute(deleteBatch);
                    session.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public int FindPageForCard(string topicId, int cardOrder, ISession session)
        {
            int page = 1;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("rs_card_order", new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.LessThan, 0));
                RowSet orderRowset = session.Execute(ps.Bind(topicId, cardOrder));

                foreach (Row orderRow in orderRowset)
                {
                    string cardId = orderRow.GetValue<string>("card_id");
                    RSCard card = SelectCard(null, null, cardId, topicId, (int)RSCardQueryType.Basic, null, false, session, null).Card;

                    if (card != null && card.HasPageBreak)
                    {
                        page++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return page;
        }

        public bool IsAbleToReorder(string topicId, bool modifiedCardHasPageBreak, int insertAtOrder, ISession session)
        {
            bool result = true;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id", "ordering" }));
                Row cardOrderRow = session.Execute(ps.Bind(topicId, insertAtOrder)).FirstOrDefault();

                if (cardOrderRow != null)
                {
                    string currentCardId = cardOrderRow.GetValue<string>("card_id");
                    bool currentCardHasFromLogic = CheckCardForFromLogic(currentCardId, session);

                    if (currentCardHasFromLogic && !modifiedCardHasPageBreak)
                    {
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }

        public bool IsAbleToDelete(string topicId, int deleteOrder, ISession session)
        {
            bool result = true;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id", "ordering" }));
                Row nextCardOrderRow = session.Execute(ps.Bind(topicId, deleteOrder + 1)).FirstOrDefault();

                if (nextCardOrderRow != null)
                {
                    string nextCardId = nextCardOrderRow.GetValue<string>("card_id");
                    bool nextCardHasFromLogic = CheckCardForFromLogic(nextCardId, session);

                    if (nextCardHasFromLogic)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id", "ordering" }));
                        Row previousCardOrderRow = session.Execute(ps.Bind(topicId, deleteOrder - 1)).FirstOrDefault();
                        if (previousCardOrderRow != null)
                        {
                            string previousCardId = previousCardOrderRow.GetValue<string>("card_id");
                            ps = session.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id", "id" }));
                            Row previousCardRow = session.Execute(ps.Bind(topicId, previousCardId)).FirstOrDefault();
                            if (previousCardRow != null)
                            {
                                if (!previousCardRow.GetValue<bool>("has_page_break"))
                                {
                                    result = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }


        public bool CheckCardForFromLogic(string cardId, ISession session)
        {
            bool result = false;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_from_logic", new List<string>(), new List<string> { "card_id" }));

                if (session.Execute(ps.Bind(cardId)).Count() > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }

        public bool CheckCardForTologic(string cardId, ISession session)
        {
            bool result = false;
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id" }));

                if (session.Execute(ps.Bind(cardId)).Count() > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return result;
        }

        public AnalyticSelectRSCardResultResponse SelectResult(string adminUserId, string companyId, string topicId, string categoryId)
        {
            AnalyticSelectRSCardResultResponse response = new AnalyticSelectRSCardResultResponse();
            response.CardResults = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                List<RSCard> cards = SelectAllCards(topicId, categoryId, adminUserId, companyId, (int)RSCard.RSCardQueryType.FullDetail, null, mainSession).Cards;

                // Fetch result per card
                Analytic analyticManager = new Analytic();
                foreach(RSCard card in cards)
                {
                    analyticManager.SelectRSCardResult(card, analyticSession, mainSession);
                }

                response.CardResults = cards;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectRSCardResultByUserResponse SelectResultByUser(string adminUserId, string companyId, string topicId, string categoryId, string answeredByUserId)
        {
            AnalyticSelectRSCardResultByUserResponse response = new AnalyticSelectRSCardResultByUserResponse();
            response.CardResults = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                User answeredByUser = new User().SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User;
                if(answeredByUser == null)
                {
                    Log.Error("Invalid userId: " + answeredByUser);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                List<RSCard> cards = SelectAllCardsByUser(answeredByUserId, topicId, mainSession, analyticSession).Cards;

                // Clean up
                foreach(RSCard card in cards)
                {
                    if(card.Type == (int)RSCard.RSCardType.MultiChoice || card.Type == (int)RSCard.RSCardType.SelectOne || card.Type == (int)RSCard.RSCardType.DropList)
                    {
                        for (int index = 0; index < card.Options.Count; index++)
                        {
                            if (!card.Options[index].IsSelected)
                            {
                                card.Options.Remove(card.Options[index]);
                                index--;
                            }
                        }
                    }
                }

                response.Report = new Analytic().SelectRSRespondersReport(topicId, new List<User> { answeredByUser }, DateHelper.SelectTimeOffsetForCompany(companyId, mainSession), mainSession, analyticSession).Reports[0];
                response.CardResults = cards;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticSelectRSCustomAnswersResponse SelectCustomAnswersFromCard(string adminUserId, string companyId, string topicId, string categoryId, string cardId)
        {
            AnalyticSelectRSCustomAnswersResponse response = new AnalyticSelectRSCustomAnswersResponse();
            response.Card = new RSCard();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row categoryRow = vh.ValidateRSTopicCategory(companyId, categoryId, mainSession);
                if (categoryRow == null)
                {
                    Log.Error("Invalid categoryId: " + categoryId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicCategoryInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicCategoryInvalid;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, mainSession);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSTopicInvalid);
                    response.ErrorMessage = ErrorMessage.RSTopicInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateRSTopicCard(cardId, topicId, mainSession);

                if(cardRow == null)
                {
                    Log.Error("Invalid cardId: " + cardId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                RSCard card = SelectCard(adminUserId, companyId, cardId, topicId, (int)RSCard.RSCardQueryType.FullDetail, cardRow, false, mainSession, analyticSession).Card;

                // Fetch result
                Analytic analyticManager = new Analytic();
                analyticManager.SelectRSCustomAnswersResultFromCard(card, companyId, analyticSession, mainSession);

                response.Card = card;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }

    [Serializable]
    [DataContract]
    public class RSImage
    {
        [DataMember]
        public string ImageId { get; set; }

        [DataMember]
        public string ImageUrl { get; set; }

        [DataMember]
        public int Ordering { get; set; }
    }
}
