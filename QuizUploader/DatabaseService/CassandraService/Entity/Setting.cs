﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    public class Setting
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public void CreateDefaultSettingPermisson(string companyId, ISession session)
        {
            try
            {
                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("setting",
                    new List<string> { "company_id", "setting_key", "setting_value" }));
                session.Execute(preparedStatement.Bind(companyId, DefaultResource.SettingFeedPostingPermissionKey, DefaultResource.SettingFeedPostingPermissionValue));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public SelectAppSettingResponse SelectAppSetting(string requesterUserId, string companyId)
        {
            SelectAppSettingResponse response = new SelectAppSettingResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);
                
                if(userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("app_setting", new List<string>(), new List<string> { "user_id" }));
                RowSet settingRowset = session.Execute(ps.Bind(requesterUserId));
                List<Row> settingRowList = settingRowset.ToList();

                if(settingRowList.Count == 0)
                {
                    response.RequireSoundEffects = true;
                    response.RequireInGameMusic = true;

                    // Update all to default
                    ps = session.Prepare(CQLGenerator.InsertStatement("app_setting", new List<string> { "user_id", "company_id", "setting_key", "setting_value" }));
                    updateBatch.Add(ps.Bind(requesterUserId, companyId, SettingKey.ClientSoundEffects, DefaultResource.ClientSoundEffectsValue));

                    ps = session.Prepare(CQLGenerator.InsertStatement("app_setting", new List<string> { "user_id", "company_id", "setting_key", "setting_value" }));
                    updateBatch.Add(ps.Bind(requesterUserId, companyId, SettingKey.ClientInGameMusic, DefaultResource.ClientInGameMusicValue));

                    ps = session.Prepare(CQLGenerator.InsertStatement("app_setting", new List<string> { "user_id", "company_id", "setting_key", "setting_value" }));
                    updateBatch.Add(ps.Bind(requesterUserId, companyId, SettingKey.ClientPushNotificationForGames, DefaultResource.ClientPushNotificationForGameValue));

                    ps = session.Prepare(CQLGenerator.InsertStatement("app_setting", new List<string> { "user_id", "company_id", "setting_key", "setting_value" }));
                    updateBatch.Add(ps.Bind(requesterUserId, companyId, SettingKey.ClientPushNotificationForEvent, DefaultResource.ClientPushNotificationForEventValue));

                    ps = session.Prepare(CQLGenerator.InsertStatement("app_setting", new List<string> { "user_id", "company_id", "setting_key", "setting_value" }));
                    updateBatch.Add(ps.Bind(requesterUserId, companyId, SettingKey.ClientPushNotificationForFeed, DefaultResource.ClientPushNotificationForFeedValue));

                    session.Execute(updateBatch);
                }
                else
                {
                    foreach (Row settingRow in settingRowList)
                    {
                        if(settingRow.GetValue<string>("setting_key").Equals(SettingKey.ClientSoundEffects))
                        {
                            response.RequireSoundEffects = Convert.ToBoolean(settingRow.GetValue<string>("setting_value"));
                        }
                        else if (settingRow.GetValue<string>("setting_key").Equals(SettingKey.ClientInGameMusic))
                        {
                            response.RequireInGameMusic = Convert.ToBoolean(settingRow.GetValue<string>("setting_value"));
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public SelectAppNotificationResponse SelectNotificationSetting(string requesterUserId, string companyId, ISession session = null)
        {
            SelectAppNotificationResponse response = new SelectAppNotificationResponse();
            response.GameNotification = Convert.ToInt16(DefaultResource.ClientPushNotificationForGameValue);
            response.EventNotification = Convert.ToInt16(DefaultResource.ClientPushNotificationForEventValue);
            response.FeedNotification = Convert.ToInt16(DefaultResource.ClientPushNotificationForFeedValue);
            response.Success = false;
            try
            {
                if(session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();

                    Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                    if (userRow == null)
                    {
                        Log.Error("Invalid user");
                        response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                        response.ErrorMessage = ErrorMessage.UserInvalid;
                        return response;
                    }
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("app_setting", new List<string>(), new List<string> { "user_id" }));
                RowSet settingRowset = session.Execute(ps.Bind(requesterUserId));

                foreach (Row settingRow in settingRowset)
                {
                    if (settingRow.GetValue<string>("setting_key").Equals(SettingKey.ClientPushNotificationForGames))
                    {
                        response.GameNotification = Convert.ToInt16(settingRow.GetValue<string>("setting_value"));
                    }
                    else if (settingRow.GetValue<string>("setting_key").Equals(SettingKey.ClientPushNotificationForEvent))
                    {
                        response.EventNotification = Convert.ToInt16(settingRow.GetValue<string>("setting_value"));
                    }
                    else if (settingRow.GetValue<string>("setting_key").Equals(SettingKey.ClientPushNotificationForFeed))
                    {
                        response.FeedNotification = Convert.ToInt16(settingRow.GetValue<string>("setting_value"));
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateAppSettingResponse UpdateAppSoundEffect(string requesterUserId, string companyId, bool isOn)
        {
            UpdateAppSettingResponse response = new UpdateAppSettingResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("app_setting",
                    new List<string> { "user_id", "setting_key" }, new List<string> { "setting_value" }, new List<string>()));

                session.Execute(ps.Bind(isOn.ToString().ToLower(), requesterUserId, SettingKey.ClientSoundEffects));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateAppSettingResponse UpdateInGameMusic(string requesterUserId, string companyId, bool isOn)
        {
            UpdateAppSettingResponse response = new UpdateAppSettingResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("app_setting",
                    new List<string> { "user_id", "setting_key" }, new List<string> { "setting_value" }, new List<string>()));

                session.Execute(ps.Bind(isOn.ToString().ToLower(), requesterUserId, SettingKey.ClientInGameMusic));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UpdateAppSettingResponse UpdateNotification(string requesterUserId, string companyId, int gameNotification, int eventNotification, int feedNotification)
        {
            UpdateAppSettingResponse response = new UpdateAppSettingResponse();
            response.Success = false;

            try
            {
                BatchStatement updateBatch = new BatchStatement();

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                Row userRow = vh.ValidateUser(requesterUserId, companyId, session);

                if (userRow == null)
                {
                    Log.Error("Invalid user");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserInvalid);
                    response.ErrorMessage = ErrorMessage.UserInvalid;
                    return response;
                }

                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("app_setting",
                    new List<string> { "user_id", "setting_key" }, new List<string> { "setting_value" }, new List<string>()));
                updateBatch.Add(ps.Bind(gameNotification.ToString(), requesterUserId, SettingKey.ClientPushNotificationForGames));

                ps = session.Prepare(CQLGenerator.UpdateStatement("app_setting",
                    new List<string> { "user_id", "setting_key" }, new List<string> { "setting_value" }, new List<string>()));
                updateBatch.Add(ps.Bind(eventNotification.ToString(), requesterUserId, SettingKey.ClientPushNotificationForEvent));

                ps = session.Prepare(CQLGenerator.UpdateStatement("app_setting",
                    new List<string> { "user_id", "setting_key" }, new List<string> { "setting_value" }, new List<string>()));
                updateBatch.Add(ps.Bind(feedNotification.ToString(), requesterUserId, SettingKey.ClientPushNotificationForFeed));

                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }
}
