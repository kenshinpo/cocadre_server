﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Web.Configuration;
using System.Web.Hosting;

namespace CassandraService.Entity
{
    public class Analytic
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum ChallengeStatType
        {
            Draw = 1,
            Lost = 2,
            NetworkError = 3,
            Surrender = 4,
            Win = 5,
        }

        public enum ResultOutcome
        {
            Lost = -1,
            Win = 1,
            Draw = 2,
            Incompleted = 3
        }

        public enum DauType
        {
            LastThirtyDays = 1,
            Weekly = 2,
            Monthly = 3
        }

        public enum DauComparableTimeFrame
        {
            Yesterday = 1,
            LastSevenDays = 2,
            LastThirtyDays = 3
        }

        public enum TimeActivityBaseType
        {
            OneMinute = 1,
            FifteenMinute = 2,
            ThiryMinute = 3,
            OneHour = 4
        }

        public void WriteExpAToTable()
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();

                string expAFileName = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data", "exp-a.csv");
                TextFieldParser parser = new TextFieldParser(expAFileName);
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                parser.ReadLine();

                BatchStatement batchStatement = new BatchStatement();

                List<Dictionary<string, int>> expList = new List<Dictionary<string, int>>();
                while (!parser.EndOfData)
                {
                    Dictionary<string, int> expDict = new Dictionary<string, int>();
                    string[] fields = parser.ReadFields();

                    int level = Int32.Parse(fields[0].Trim());
                    int exp = Int32.Parse(fields[1].Trim());

                    expDict.Add("level", level);
                    expDict.Add("exp", exp);
                    expList.Add(expDict);
                }

                for (int index = 0; index < expList.Count; index++)
                {
                    PreparedStatement psExp = session.Prepare(CQLGenerator.InsertStatement("experience_scheme_a",
                        new List<string> { "level", "from_exp", "to_exp" }));

                    if (index + 1 < expList.Count)
                    {
                        batchStatement = batchStatement.Add(psExp.Bind(expList[index]["level"], expList[index]["exp"], expList[index + 1]["exp"] - 1));
                    }
                    else
                    {
                        batchStatement = batchStatement.Add(psExp.Bind(expList[index]["level"], expList[index]["exp"], expList[index]["exp"]));
                    }
                }

                session.Execute(batchStatement);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }
        }

        public void UpdateExpOfUser(string userId, string topicId, string questionId, int score, DateTimeOffset timeAllocated, ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                BatchStatement bs = new BatchStatement();
                string allocationId = UUIDGenerator.GenerateUniqueIDForExpAllocation();

                PreparedStatement psExp = session.Prepare(CQLGenerator.UpdateCounterStatement("experience_by_user",
                    new List<string> { "user_id" }, new List<string> { "exp" }, new List<int> { score }));
                session.Execute(psExp.Bind(userId));

                psExp = session.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(psExp.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                psExp = session.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_user_timestamp",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(psExp.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                psExp = session.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(psExp.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                psExp = session.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_topic_timestamp",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(psExp.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                psExp = session.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(psExp.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                psExp = session.Prepare(CQLGenerator.InsertStatement("experience_allocation_by_question_timestamp",
                    new List<string> { "allocation_id", "topic_id", "question_id", "exp_allocated", "user_id", "allocated_on_timestamp" }));
                bs.Add(psExp.Bind(allocationId, topicId, questionId, score, userId, timeAllocated));

                session.Execute(bs);
            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

        }

        public ChallengeSelectStatsResponse SelectStatsForChallenge(string topicId, string requesterUserId, string opponentUserId, string companyId)
        {
            ChallengeSelectStatsResponse response = new ChallengeSelectStatsResponse();
            response.Exp = new Exp();
            response.TopPlayers = new List<User>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                Exp currentUserExp = SelectLevelOfUser(requesterUserId, session);

                Exp opponentExp = SelectLevelOfUser(opponentUserId, session);
                response.OpponentLevel = opponentExp.CurrentLevel;

                int limit = Int16.Parse(WebConfigurationManager.AppSettings["leaderboard_by_topic"]);

                PreparedStatement psLeaderboard = session.Prepare(CQLGenerator.SelectStatementWithLimit("user_leaderboard_by_topic_sorted", new List<string> { "user_id" }, new List<string> { "topic_id", "is_visible" }, limit));
                BoundStatement bsLeaderboard = psLeaderboard.Bind(topicId, true);
                RowSet leaderboardRowset = session.Execute(bsLeaderboard);

                foreach (Row leaderboardRow in leaderboardRowset)
                {
                    string userId = leaderboardRow.GetValue<string>("user_id");
                    Exp leaderboardUserExp = SelectLevelOfUser(userId, session);

                    int level = leaderboardUserExp.CurrentLevel;

                    User player = new User().SelectUserBasic(userId, companyId, false, mainSession).User;
                    player.Level = level;
                    response.TopPlayers.Add(player);
                }

                response.Exp = currentUserExp;
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public UserSelectStatsResponse SelectStatsForUser(string requesterUserId, string companyId)
        {
            UserSelectStatsResponse response = new UserSelectStatsResponse();
            response.UserStats = new UserStats();
            response.UserStats.Topics = new List<TopicStats>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("challenge_total_stats_counter_by_user",
                    new List<string>(), new List<string> { "company_id", "user_id" }));
                Row totalStatsRow = session.Execute(preparedStatement.Bind(companyId, requesterUserId)).FirstOrDefault();

                if (totalStatsRow != null)
                {
                    long draws = totalStatsRow["total_draws"] != null ? totalStatsRow.GetValue<long>("total_draws") : 0;
                    long losses = totalStatsRow["total_losses"] != null ? totalStatsRow.GetValue<long>("total_losses") : 0;
                    long wins = totalStatsRow["total_win"] != null ? totalStatsRow.GetValue<long>("total_win") : 0;
                    long totalGamesPlayed = draws + losses + wins;

                    decimal winPercentage = ((decimal)wins / totalGamesPlayed);
                    decimal losePercentage = ((decimal)losses / totalGamesPlayed);

                    response.UserStats.WinningPercentage = (int)(winPercentage * 100);
                    response.UserStats.LosingPercentage = (int)(losePercentage * 100);
                    response.UserStats.DrawPercentage = 100 - response.UserStats.WinningPercentage - response.UserStats.LosingPercentage;
                }

                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                    new List<string>(), new List<string> { "user_id", "is_visible" }));
                RowSet topicStatsRowSet = session.Execute(preparedStatement.Bind(requesterUserId, true));

                Topic topic = new Topic();
                foreach (Row topicStatsRow in topicStatsRowSet)
                {
                    string topicId = topicStatsRow.GetValue<string>("topic_id");
                    int numberAnsweredCorrectly = topicStatsRow.GetValue<int>("number_answered_correctly");

                    Topic selectedTopic = topic.SelectTopicBasic(topicId, null, companyId, null, null, mainSession).Topic;

                    if (selectedTopic != null)
                    {
                        TopicStats topicStats = new TopicStats();
                        topicStats.Topic = selectedTopic;
                        decimal percentage = ((decimal)numberAnsweredCorrectly / selectedTopic.TotalNumberOfQuestions);
                        topicStats.CompletionPercentage = (int)(percentage * 100);

                        response.UserStats.Topics.Add(topicStats);
                    }
                }

                int limit = Convert.ToInt16(WebConfigurationManager.AppSettings["challenge_topic_stats_limit"]);
                if (response.UserStats.Topics.Count > 5)
                {
                    response.UserStats.Topics = response.UserStats.Topics.OrderByDescending(stats => stats.CompletionPercentage).ToList().GetRange(0, limit);
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public Exp SelectLevelOfUser(string userId, ISession session)
        {
            Exp levelExp = new Exp();

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                PreparedStatement psExpByUser = session.Prepare(CQLGenerator.SelectStatement("experience_by_user", new List<string> { "exp" }, new List<string> { "user_id" }));
                BoundStatement bsExpByUser = psExpByUser.Bind(userId);
                Row expByUserRow = session.Execute(bsExpByUser).FirstOrDefault();

                int expPoint = 0;

                if (expByUserRow != null)
                {
                    expPoint = (int)expByUserRow.GetValue<long>("exp");
                }

                PreparedStatement psExp = session.Prepare("SELECT * FROM experience_scheme_a WHERE from_exp <= ? ALLOW FILTERING");
                BoundStatement bsExp = psExp.Bind(expPoint);
                RowSet expRowset = session.Execute(bsExp);

                List<Dictionary<string, int>> expList = new List<Dictionary<string, int>>();

                foreach (Row expRow in expRowset)
                {
                    Dictionary<string, int> expDict = new Dictionary<string, int>();
                    expDict.Add("level", expRow.GetValue<int>("level"));
                    expDict.Add("fromExp", expRow.GetValue<int>("from_exp"));
                    expDict.Add("toExp", expRow.GetValue<int>("to_exp"));

                    expList.Add(expDict);
                }

                int currentLevel = 0;
                int currentExp = 0;
                int nextLevel = 0;
                int nextExpToReach = 0;

                expList = expList.OrderByDescending(expDict => expDict["fromExp"]).ToList();
                currentLevel = expList[0]["level"];
                currentExp = expPoint;
                nextLevel = currentLevel + 1;
                nextExpToReach = expList[0]["toExp"] + 1;

                levelExp.CurrentLevel = currentLevel;
                levelExp.CurrentExp = currentExp;
                levelExp.NextLevel = nextLevel;
                levelExp.NextExpToReach = nextExpToReach;

            }
            catch (Exception ex)
            {
                Log.Error(ex.StackTrace.ToString());
            }

            return levelExp;
        }

        public void UpdateUserActivity(string userId,
                                       string companyId,
                                       bool isActive)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                string logId = UUIDGenerator.GenerateUniqueIDForLog();
                DateTime currentDatetime = DateTime.UtcNow;

                // The date will have some problem due to zone time 
                PreparedStatement psUserActivityLog = session.Prepare(CQLGenerator.InsertStatement("user_activity_log",
                    new List<string> { "id", "company_id", "user_id", "is_active", "activity_datestamp", "activity_date_timestamp" }));
                BoundStatement bsUserActivityLog = psUserActivityLog.Bind(logId, companyId, userId, isActive, DateHelper.ConvertDateToLong(currentDatetime.Date), DateHelper.ConvertDateToLong(currentDatetime));

                session.Execute(bsUserActivityLog);

                //Log.Debug("Updated activity successfully");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public UserUpdateLoginResponse UpdateUserLogin(string userId,
                                                       string companyId,
                                                       bool isLogin)
        {
            UserUpdateLoginResponse response = new UserUpdateLoginResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getAnalyticSession();
                ValidationHandler vh = new ValidationHandler();

                string logId = UUIDGenerator.GenerateUniqueIDForLog();
                DateTime currentDatetime = DateTime.UtcNow;

                PreparedStatement psUserLoginLog = session.Prepare(CQLGenerator.InsertStatement("user_login_log",
                    new List<string> { "id", "company_id", "user_id", "is_login", "login_datestamp", "login_date_timestamp" }));
                BoundStatement bsUserLoginLog = psUserLoginLog.Bind(logId, companyId, userId, isLogin, DateHelper.ConvertDateToLong(currentDatetime.Date), DateHelper.ConvertDateToLong(currentDatetime));

                session.Execute(bsUserLoginLog);

                if (!isLogin)
                {
                    UserSelectDeviceTokenResponse userResponse = new User().SelectUserDeviceToken(userId);
                    response.DeviceToken = userResponse.DeviceToken;
                    response.DeviceType = userResponse.DeviceType;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        //public void UpdateLeaderboardStatsByQuestion(string userId, string topicId, string questionId, string challengeId, string mainDepartmentId, string companyId, bool isCorrect, ISession session)
        //{
        //    try
        //    {
        //        if (session == null)
        //        {
        //            ConnectionManager cm = new ConnectionManager();
        //            session = cm.getAnalyticSession();
        //        }

        //        DateTimeOffset currentTime = DateTime.UtcNow;

        //        PreparedStatement psChallengeStats = session.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
        //            new List<string>(), new List<string> { "user_id", "question_id" }, 1));
        //        BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
        //        Row challengeStatsRow = session.Execute(bsChallengeStats).FirstOrDefault();

        //        BatchStatement deleteStatements = new BatchStatement();
        //        BatchStatement updateStatements = new BatchStatement();

        //        // Question is answered before by this current user
        //        if (challengeStatsRow != null)
        //        {
        //            bool isAnsweredCorrectlyPreviously = challengeStatsRow.GetValue<bool>("is_correct");
        //            // Now correct
        //            if (isCorrect)
        //            {
        //                // Previously answer wrongly, but now correct
        //                if (!isAnsweredCorrectlyPreviously)
        //                {
        //                    //------------------------- Topic leaderboard by user ------------------------
        //                    PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
        //                           new List<string>(), new List<string> { "user_id", "topic_id" }));
        //                    BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
        //                    Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByUser = 0;
        //                    DateTimeOffset lastUpdatedTimestampByUser = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    //------------------------- Company leaderboard by user ------------------------
        //                    psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
        //                           new List<string>(), new List<string> { "user_id" }));
        //                    bsLeaderBoard = psLeaderBoard.Bind(userId);
        //                    leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByCompany = 0;
        //                    DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    //------------------------- Company leaderboard by department ------------------------
        //                    psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
        //                        new List<string>(), new List<string> { "department_id" }));
        //                    bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
        //                    leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByDepartment = 0;
        //                    DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    //------------------------- Department leaderboard by user ------------------------
        //                    psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
        //                        new List<string>(), new List<string> { "user_id" }));
        //                    bsLeaderBoard = psLeaderBoard.Bind(userId);
        //                    leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByDepartmentUser = 0;
        //                    DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    // Delete
        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
        //                        new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));


        //                    // Update
        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
        //                        new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser + 1, currentTime, userId, topicId));

        //                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
        //                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));

        //                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
        //                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
        //                        new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + 1, currentTime, userId));

        //                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
        //                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + 1, currentTime, companyId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
        //                        new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));

        //                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
        //                        new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
        //                        new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, userId));

        //                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, mainDepartmentId));
        //                }
        //            }
        //            // Now wrong
        //            else
        //            {
        //                // Previously answer correctly, but now wrong
        //                if (isAnsweredCorrectlyPreviously)
        //                {
        //                    //------------------------- Topic leaderboard by user ------------------------
        //                    PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
        //                           new List<string>(), new List<string> { "user_id", "topic_id" }));
        //                    BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
        //                    Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByUser = 0;
        //                    DateTimeOffset lastUpdatedTimestampByUser = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    //------------------------- Company leaderboard by user ------------------------
        //                    psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
        //                           new List<string>(), new List<string> { "user_id" }));
        //                    bsLeaderBoard = psLeaderBoard.Bind(userId);
        //                    leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByCompany = 0;
        //                    DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    //------------------------- Company leaderboard by department ------------------------
        //                    psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
        //                        new List<string>(), new List<string> { "department_id" }));
        //                    bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
        //                    leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByDepartment = 0;
        //                    DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    //------------------------- Department leaderboard by user ------------------------
        //                    psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
        //                        new List<string>(), new List<string> { "user_id" }));
        //                    bsLeaderBoard = psLeaderBoard.Bind(userId);
        //                    leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                    int numberAnswerCorrectlyByDepartmentUser = 0;
        //                    DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

        //                    if (leaderBoardRow != null)
        //                    {
        //                        numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                        lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                    }

        //                    // Delete
        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
        //                        new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));


        //                    psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
        //                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                    deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));


        //                    // Update
        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
        //                        new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser - 1, currentTime, userId, topicId));

        //                    if (numberAnswerCorrectlyByUser - 1 > 0)
        //                    {
        //                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
        //                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
        //                        updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser - 1, currentTime, topicId, true));

        //                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
        //                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
        //                        updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser - 1, currentTime, topicId, true));
        //                    }

        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
        //                        new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - 1, currentTime, userId));

        //                    if (numberAnswerCorrectlyByCompany - 1 > 0)
        //                    {
        //                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
        //                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
        //                        updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - 1, currentTime, companyId));
        //                    }


        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
        //                        new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - 1, currentTime, mainDepartmentId));

        //                    if (numberAnswerCorrectlyByDepartment - 1 > 0)
        //                    {
        //                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
        //                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                        updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - 1, currentTime, mainDepartmentId));
        //                    }


        //                    psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
        //                        new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                    updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - 1, currentTime, userId));

        //                    if (numberAnswerCorrectlyByDepartmentUser - 1 > 0)
        //                    {
        //                        psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
        //                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                        updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - 1, currentTime, mainDepartmentId));
        //                    }


        //                }

        //            }
        //        }
        //        // Question is not answered before by this current user
        //        else
        //        {
        //            if (isCorrect)
        //            {
        //                //------------------------- Topic leaderboard by user ------------------------
        //                PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
        //                       new List<string>(), new List<string> { "user_id", "topic_id" }));
        //                BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
        //                Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                int numberAnswerCorrectlyByUser = 0;
        //                DateTimeOffset lastUpdatedTimestampByUser = currentTime;

        //                if (leaderBoardRow != null)
        //                {
        //                    numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                    lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                }

        //                //------------------------- Company leaderboard by user ------------------------
        //                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
        //                       new List<string>(), new List<string> { "user_id" }));
        //                bsLeaderBoard = psLeaderBoard.Bind(userId);
        //                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                int numberAnswerCorrectlyByCompany = 0;
        //                DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

        //                if (leaderBoardRow != null)
        //                {
        //                    numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                    lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                }

        //                //------------------------- Company leaderboard by department ------------------------
        //                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
        //                    new List<string>(), new List<string> { "department_id" }));
        //                bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
        //                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                int numberAnswerCorrectlyByDepartment = 0;
        //                DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

        //                if (leaderBoardRow != null)
        //                {
        //                    numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                    lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                }

        //                //------------------------- Department leaderboard by user ------------------------
        //                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
        //                    new List<string>(), new List<string> { "user_id" }));
        //                bsLeaderBoard = psLeaderBoard.Bind(userId);
        //                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

        //                int numberAnswerCorrectlyByDepartmentUser = 0;
        //                DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

        //                if (leaderBoardRow != null)
        //                {
        //                    numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
        //                    lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
        //                }

        //                // Delete
        //                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
        //                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

        //                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
        //                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));


        //                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
        //                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


        //                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
        //                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));


        //                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));


        //                // Update
        //                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
        //                    new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser + 1, currentTime, userId, topicId));

        //                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
        //                updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));

        //                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
        //                updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + 1, currentTime, topicId, true));


        //                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
        //                    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + 1, currentTime, userId));

        //                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
        //                updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + 1, currentTime, companyId));


        //                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
        //                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));

        //                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
        //                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + 1, currentTime, mainDepartmentId));


        //                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
        //                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
        //                updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, userId));

        //                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
        //                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
        //                updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + 1, currentTime, mainDepartmentId));

        //            }
        //        }

        //        if (!string.IsNullOrEmpty(challengeId))
        //        {
        //            psChallengeStats = session.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_user",
        //                new List<string> { "user_id", "topic_id", "challenge_id", "question_id", "is_correct", "answered_timestamp" }));
        //            bsChallengeStats = psChallengeStats.Bind(userId, topicId, challengeId, questionId, isCorrect, currentTime);
        //            updateStatements.Add(bsChallengeStats);
        //        }


        //        session.Execute(deleteStatements);
        //        session.Execute(updateStatements);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(ex.ToString(), ex);
        //    }
        //}

        public void UpdateLeaderboardStatsByQuestion(string userId, string topicId, string questionId, string challengeId, string mainDepartmentId, string companyId, bool isCorrect, DateTimeOffset currentTime, ISession analyticsSession, ISession mainSession)
        {
            try
            {
                if (analyticsSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticsSession = cm.getAnalyticSession();
                }

                BatchStatement bs = new BatchStatement();
                PreparedStatement psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
                    new List<string>(), new List<string> { "user_id", "question_id" }, 1));
                BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
                Row challengeStatsRow = analyticsSession.Execute(bsChallengeStats).FirstOrDefault();

                psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_user",
                    new List<string> { "user_id", "topic_id", "challenge_id", "question_id", "is_correct", "answered_timestamp" }));
                bsChallengeStats = psChallengeStats.Bind(userId, topicId, challengeId, questionId, isCorrect, currentTime);
                bs.Add(bsChallengeStats);

                psChallengeStats = analyticsSession.Prepare(CQLGenerator.InsertStatement("challenge_question_stats_by_topic",
                    new List<string> { "user_id", "topic_id", "challenge_id", "question_id", "is_correct", "answered_timestamp" }));
                bsChallengeStats = psChallengeStats.Bind(userId, topicId, challengeId, questionId, isCorrect, currentTime);
                bs.Add(bsChallengeStats);
                analyticsSession.Execute(bs);

                // Get events associated to user
                Event eventManager = new Event();
                List<Event> selectedEvents = eventManager.SelectLiveEventsByTopicIdAndUserId(userId, mainDepartmentId, companyId, topicId, mainSession);

                // Question is answered before by this current user
                if (challengeStatsRow != null)
                {
                    bool isAnsweredCorrectlyPreviously = challengeStatsRow.GetValue<bool>("is_correct");
                    // Now correct
                    if (isCorrect)
                    {
                        // Previously answer wrongly, but now correct
                        if (!isAnsweredCorrectlyPreviously)
                        {
                            UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, 1, analyticsSession);
                        }

                        //UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession);

                        Thread updateEventLeaderboardThread = new Thread(() => UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession));
                        updateEventLeaderboardThread.Start();
                    }
                    // Now wrong
                    else
                    {
                        // Previously answer correctly, but now wrong
                        if (isAnsweredCorrectlyPreviously)
                        {
                            UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, -1, analyticsSession);

                            //UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, -1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession);

                            Thread updateEventLeaderboardThread = new Thread(() => UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, -1, isAnsweredCorrectlyPreviously, analyticsSession, mainSession));
                            updateEventLeaderboardThread.Start();
                        }

                    }
                }
                // Question is not answered before by this current user
                else
                {
                    if (isCorrect)
                    {

                        UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, 1, analyticsSession);

                        //UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, false, analyticsSession, mainSession);

                        Thread updateEventLeaderboardThread = new Thread(() => UpdateEventLeaderboard(userId, selectedEvents, mainDepartmentId, companyId, 1, false, analyticsSession, mainSession));
                        updateEventLeaderboardThread.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveLeaderboardStatsByQuestion(string userId, string topicId, string questionId, string challengeId, string mainDepartmentId, string companyId, bool isCorrect, DateTimeOffset currentTime, ISession analyticsSession, ISession mainSession)
        {
            try
            {
                if (analyticsSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticsSession = cm.getAnalyticSession();
                }

                PreparedStatement psChallengeStats = analyticsSession.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
                    new List<string>(), new List<string> { "user_id", "question_id" }, 1));
                BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
                Row challengeStatsRow = analyticsSession.Execute(bsChallengeStats).FirstOrDefault();

                // Get events associated to user
                Event eventManager = new Event();
                List<Event> selectedEvents = eventManager.SelectLiveEventsByTopicIdAndUserId(userId, mainDepartmentId, companyId, topicId, mainSession);

                // Question is answered before by this current user
                if (challengeStatsRow != null)
                {
                    bool isAnsweredCorrectlyPreviously = challengeStatsRow.GetValue<bool>("is_correct");

                    // Previously answer correctly, but now wrong
                    if (isAnsweredCorrectlyPreviously)
                    {
                        UpdateLeaderboard(userId, topicId, companyId, mainDepartmentId, -1, analyticsSession);
                    }

                    // Update event
                    RemoveEventLeaderboardByQuestion(userId, selectedEvents, mainDepartmentId, companyId, topicId, questionId, isAnsweredCorrectlyPreviously, analyticsSession, mainSession);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }


        private void UpdateLeaderboard(string userId, string topicId, string companyId, string mainDepartmentId, int value, ISession session)
        {
            try
            {
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                DateTimeOffset currentTime = DateTime.UtcNow;

                //------------------------- Topic leaderboard by user ------------------------
                PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id", "topic_id" }));
                BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, topicId);
                Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByUser = 0;
                DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                // Update
                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
                   new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser + value, currentTime, userId, topicId));

                if (numberAnswerCorrectlyByUser + value > 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + value, currentTime, topicId, true));

                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + value, currentTime, topicId, true));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();

                //------------------------- Company leaderboard by user ------------------------
                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                bsLeaderBoard = psLeaderBoard.Bind(userId);
                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByCompany = 0;
                DateTimeOffset lastUpdatedTimestampByCompany = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                  new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


                // Update
                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                  new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + value, currentTime, userId));

                if (numberAnswerCorrectlyByCompany + value > 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + value, currentTime, companyId));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();

                //------------------------- Company leaderboard by department ------------------------
                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                    new List<string>(), new List<string> { "department_id" }));
                bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByDepartment = 0;
                DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                // Update
                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + value, currentTime, mainDepartmentId));

                if (numberAnswerCorrectlyByDepartment + value > 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                        new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + value, currentTime, mainDepartmentId));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();


                //------------------------- Department leaderboard by user ------------------------
                psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                    new List<string>(), new List<string> { "user_id" }));
                bsLeaderBoard = psLeaderBoard.Bind(userId);
                leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                int numberAnswerCorrectlyByDepartmentUser = 0;
                DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                if (leaderBoardRow != null)
                {
                    numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                    lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                }

                // Delete
                psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                // Update
                psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, userId));

                if (numberAnswerCorrectlyByDepartmentUser + value > 0)
                {
                    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId));
                }

                session.Execute(deleteStatements);
                session.Execute(updateStatements);

                deleteStatements = new BatchStatement();
                updateStatements = new BatchStatement();

                #region Old
                // Delete
                //psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                //    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                //deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                //psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                //    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                //deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));


                //psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                //    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                //deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));


                //psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                //    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                //deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));


                //psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                //    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                //deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));


                // Update
                //psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user",
                //    new List<string> { "user_id", "topic_id" }, new List<string> { "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                //updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByUser + value, currentTime, userId, topicId));

                //if (numberAnswerCorrectlyByUser + value > 0)
                //{
                //    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("topic_leaderboard_by_user_sorted",
                //        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                //    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + value, currentTime, topicId, true));

                //    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_topic_sorted",
                //        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id", "is_visible" }));
                //    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser + value, currentTime, topicId, true));
                //}

                //psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                //    new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                //updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + value, currentTime, userId));

                //if (numberAnswerCorrectlyByCompany + value > 0)
                //{
                //    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                //        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                //    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + value, currentTime, companyId));
                //}


                //psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                //    new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                //updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + value, currentTime, mainDepartmentId));

                //if (numberAnswerCorrectlyByDepartment + value > 0)
                //{
                //    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                //        new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                //    updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + value, currentTime, mainDepartmentId));
                //}


                //psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                //    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                //updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, userId));

                //if (numberAnswerCorrectlyByDepartmentUser + value > 0)
                //{
                //    psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                //        new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                //    updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId));
                //}

                //session.Execute(deleteStatements);
                //session.Execute(updateStatements);
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        // COMBINE WITH UPDATE EVENTLEADERBOARD!!!!!
        private void RemoveEventLeaderboardByQuestion(string userId, List<Event> events, string mainDepartmentId, string companyId, string topicid, string questionId, bool isAnswerCorrectlyPreviously, ISession analyticSession, ISession mainSession)
        {
            try
            {
                foreach (Event selectedEvent in events)
                {
                    string eventId = selectedEvent.EventId;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;
                    DateTimeOffset startTimeOffset = selectedEvent.StartTimestamp;
                    DateTimeOffset endTimeOffset = selectedEvent.EndTimestamp;

                    // Check scoring and calculation
                    // Scoring
                    // Top scorer vs Top department

                    // Calculation type
                    // Accumulative
                    // Unique
                    // Total Accumulative vs Total unique
                    // Avg accumulative vs avg unique

                    BatchStatement deleteStatements = new BatchStatement();
                    BatchStatement updateStatements = new BatchStatement();

                    DateTimeOffset currentTime = DateTime.UtcNow;

                    int scoreToDeduct = 0;
                    if (scoringType == (int)Event.ScoringTypeEnum.TopScorer)
                    {
                        Log.Debug("Top Scorer");

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.Accumulative)
                        {
                            // Get the total number of correct times from start to end date
                            PreparedStatement psScoreboard = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("challenge_question_stats_by_user",
                                new List<string>(), new List<string> { "user_id", "question_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "answered_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                            BoundStatement bsScoreboard = psScoreboard.Bind(userId, questionId, true, startTimeOffset, endTimeOffset);
                            RowSet scoreboardRowset = analyticSession.Execute(bsScoreboard);

                            scoreToDeduct = -scoreboardRowset.ToList().Count();

                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.Unique)
                        {
                            // Unique does not take in previously correct answer
                            if (!isAnswerCorrectlyPreviously)
                            {
                                Log.Debug("Unique -> Continue: answer wrongly previously");
                                continue;
                            }

                            scoreToDeduct = -1;
                        }

                        //------------------------- Event leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id", "event_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, eventId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByUser = 0;
                        DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        // Update
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user",
                           new List<string> { "user_id", "event_id" }, new List<string> { "department_id", "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByUser + scoreToDeduct, currentTime, userId, eventId));

                        if (numberAnswerCorrectlyByUser + scoreToDeduct > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_leaderboard_by_user_sorted",
                                new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + scoreToDeduct, currentTime, eventId, true));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_event_sorted",
                                new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + scoreToDeduct, currentTime, eventId, true));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        Log.Debug("Update eventleaderboard completed");
                    }
                    else if (scoringType == (int)Event.ScoringTypeEnum.TopDepartment)
                    {

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative)
                        {
                            // Get the total number of correct times from start to end date
                            PreparedStatement psScoreboard = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("challenge_question_stats_by_user",
                                new List<string>(), new List<string> { "user_id", "question_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "answered_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                            BoundStatement bsScoreboard = psScoreboard.Bind(userId, questionId, true, startTimeOffset, endTimeOffset);
                            RowSet scoreboardRowset = analyticSession.Execute(bsScoreboard);

                            scoreToDeduct = -scoreboardRowset.ToList().Count();
                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalUnique || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            // Unique does not take in previously correct answer
                            if (!isAnswerCorrectlyPreviously)
                            {
                                Log.Debug("Unique -> Continue: answer wrongly previously");
                                continue;
                            }

                            scoreToDeduct = -1;
                        }

                        //------------------------- Department user leaderboard by event ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id", "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId, userId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByDepartmentUser = 0;
                        DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                            new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                        // Update
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_user_leaderboard_by_department",
                            new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + scoreToDeduct, currentTime, mainDepartmentId, eventId, userId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_department_leaderboard_by_user",
                            new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + scoreToDeduct, currentTime, mainDepartmentId, eventId, userId));

                        if (numberAnswerCorrectlyByDepartmentUser + scoreToDeduct > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                                new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser + scoreToDeduct, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();

                        //------------------------- Event leaderboard by department ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                           new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        float numberAnswerCorrectlyByDepartment = 0f;
                        DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<float>("score");
                            lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                        // Update
                        float newScore = 0f;

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        RowSet leaderBoardRowset = analyticSession.Execute(bsLeaderBoard);

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, mainDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(newScore, currentTime, mainDepartmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, newScore, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                    }


                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        private void UpdateEventLeaderboard(string userId, List<Event> events, string mainDepartmentId, string companyId, int value, bool isAnswerCorrectlyPreviously, ISession analyticSession, ISession mainSession)
        {
            try
            {
                foreach (Event selectedEvent in events)
                {
                    string eventId = selectedEvent.EventId;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Check scoring and calculation
                    // Scoring
                    // Top scorer vs Top department

                    // Calculation type
                    // Accumulative
                    // Unique
                    // Total Accumulative vs Total unique
                    // Avg accumulative vs avg unique

                    BatchStatement deleteStatements = new BatchStatement();
                    BatchStatement updateStatements = new BatchStatement();

                    DateTimeOffset currentTime = DateTime.UtcNow;

                    if (scoringType == (int)Event.ScoringTypeEnum.TopScorer)
                    {
                        Log.Debug("Top Scorer");

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.Accumulative)
                        {
                            // Accumulative does not consider previously wronged question
                            if (value < 0)
                            {
                                Log.Debug("Accumlative -> Continue: answer wrongly");
                                continue;
                            }
                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.Unique)
                        {
                            // Unique does not take in previously correct answer
                            if (value > 0 && isAnswerCorrectlyPreviously)
                            {
                                Log.Debug("Unique -> Continue: answer correctly");
                                continue;
                            }
                        }

                        //------------------------- Event leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id", "event_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, eventId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByUser = 0;
                        DateTimeOffset lastUpdatedTimestampByUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, eventId));

                        // Update
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user",
                           new List<string> { "user_id", "event_id" }, new List<string> { "department_id", "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByUser + value, currentTime, userId, eventId));

                        if (numberAnswerCorrectlyByUser + value > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_leaderboard_by_user_sorted",
                                new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + value, currentTime, eventId, true));

                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_event_sorted",
                                new List<string> { "user_id", "department_id", "score", "last_updated_timestamp", "event_id", "is_visible" }));
                            updateStatements.Add(psLeaderBoard.Bind(userId, mainDepartmentId, numberAnswerCorrectlyByUser + value, currentTime, eventId, true));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        Log.Debug("Update eventleaderboard completed");
                    }
                    else if (scoringType == (int)Event.ScoringTypeEnum.TopDepartment)
                    {

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative)
                        {
                            // Accumulative does not consider previously wronged question
                            if (value < 0)
                            {
                                continue;
                            }
                        }
                        else if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalUnique || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            // Unique does not take in previously correct answer
                            if (value > 0 && isAnswerCorrectlyPreviously)
                            {
                                continue;
                            }
                        }

                        //------------------------- Department user leaderboard by event ------------------------
                        PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id", "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId, userId);
                        Row leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyByDepartmentUser = 0;
                        DateTimeOffset lastUpdatedTimestampByDepartmentUser = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("score");
                            lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                            new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                        // Update
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_user_leaderboard_by_department",
                            new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId, eventId, userId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_department_leaderboard_by_user",
                            new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId, eventId, userId));

                        if (numberAnswerCorrectlyByDepartmentUser + value > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                                new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, userId, numberAnswerCorrectlyByDepartmentUser + value, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();

                        //------------------------- Event leaderboard by department ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                           new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        leaderBoardRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        float numberAnswerCorrectlyByDepartment = 0f;
                        DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                        if (leaderBoardRow != null)
                        {
                            numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<float>("score");
                            lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        // Delete
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderBoard.Bind(eventId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                        // Update
                        float newScore = 0f;

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId, eventId);
                        RowSet leaderBoardRowset = analyticSession.Execute(bsLeaderBoard);

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, mainDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoard.Bind(newScore, currentTime, mainDepartmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoard.Bind(eventId, newScore, currentTime, mainDepartmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                    }


                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public void InsertLeaderboardForEvent(string eventId, int scoringType, int scoreCalculationType, List<string> topicIds, string adminUserId, string companyId, List<string> participantUserIds, DateTime startTime, ISession analyticSession, ISession mainSession)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                Event selectedEvent = new Event
                {
                    EventId = eventId,
                    ScoringType = scoringType,
                    CalculationType = scoreCalculationType
                };

                foreach (string topicId in topicIds)
                {
                    // Get all question ids
                    PreparedStatement psQuestion = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_question",
                        new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsQuestion = psQuestion.Bind(topicId);
                    RowSet questionRowset = mainSession.Execute(bsQuestion);

                    List<string> selectedQuestionIds = new List<string>();

                    foreach (Row questionRow in questionRowset)
                    {
                        int status = questionRow.GetValue<int>("status");

                        if (status == ChallengeQuestion.QuestionStatus.CODE_ACTIVE)
                        {
                            string questionId = questionRow.GetValue<string>("id");

                            foreach (string participantId in participantUserIds)
                            {
                                if (scoreCalculationType == (int)Event.CalculationTypeEnum.Accumulative ||
                                    scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative ||
                                    scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative)
                                {
                                    PreparedStatement psQuestionbyTopic = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_question_stats_by_topic",
                                        new List<string>(), new List<string> { "topic_id", "question_id", "user_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, 0));
                                    BoundStatement bsQuestionbyTopic = psQuestionbyTopic.Bind(topicId, questionId, participantId, true, startTime);
                                    RowSet questionByTopicRowset = analyticSession.Execute(bsQuestionbyTopic);

                                    List<Department> userDepartments = new Department().GetAllDepartmentByUserId(participantId, companyId, mainSession).Departments;
                                    string mainDepartmentId = userDepartments[0].Id;

                                    foreach (Row questionByTopicRow in questionByTopicRowset)
                                    {
                                        UpdateEventLeaderboard(participantId, new List<Event> { selectedEvent }, mainDepartmentId, companyId, 1, true, analyticSession, mainSession);
                                    }
                                }
                                else
                                {
                                    PreparedStatement psQuestionbyTopic = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_question_stats_by_topic",
                                        new List<string>(), new List<string> { "topic_id", "question_id", "user_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, 1));
                                    BoundStatement bsQuestionbyTopic = psQuestion.Bind(topicId, questionId, participantId, true, startTime);
                                    RowSet questionByTopicRowset = mainSession.Execute(bsQuestionbyTopic);

                                    List<Department> userDepartments = new Department().GetAllDepartmentByUserId(participantId, companyId, mainSession).Departments;
                                    string mainDepartmentId = userDepartments[0].Id;

                                    foreach (Row questionByTopicRow in questionByTopicRowset)
                                    {
                                        UpdateEventLeaderboard(participantId, new List<Event> { selectedEvent }, mainDepartmentId, companyId, 1, false, analyticSession, mainSession);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromEventLeaderboard(string adminUserId, string companyId, string userId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                // This method only involves when delete user
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Event leaderboard by user ------------------------
                PreparedStatement psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                RowSet leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(userId, score, updatedTimestamp, eventId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(userId, score, updatedTimestamp, eventId));
                }

                analyticSession.Execute(deleteStatements);
                deleteStatements = new BatchStatement();

                //------------------------- Event department leaderboard by user ------------------------
                psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    string departmentId = leaderBoardByUserRow.GetValue<string>("department_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    Event selectedEvent = new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL, (int)Event.EventStatusEnum.Ignored, mainSession).Event;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Need to delete this first
                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(departmentId, eventId, score, updatedTimestamp, userId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department",
                        new List<string> { "department_id", "event_id", "user_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(departmentId, eventId, userId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_department_leaderboard_by_user",
                        new List<string> { "department_id", "event_id", "user_id", }));
                    deleteStatements.Add(psLeaderBoardByUser.Bind(departmentId, eventId, userId));

                    analyticSession.Execute(deleteStatements);
                    deleteStatements = new BatchStatement();

                    //------------------------- Department leaderboard by event ------------------------
                    PreparedStatement psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                      new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardByDepartmentRow = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId)).FirstOrDefault();

                    if (leaderBoardByDepartmentRow != null)
                    {
                        float departmentScore = leaderBoardByDepartmentRow.GetValue<float>("score");
                        DateTimeOffset updatedDepartmentTimestamp = leaderBoardByDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "department_id", "event_id", "score", "last_updated_timestamp" }));
                        deleteStatements.Add(psLeaderBoardByDepartment.Bind(departmentId, eventId, score, updatedTimestamp));

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoardByDepartment.Bind(newScore, currentTime, departmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoardByDepartment.Bind(eventId, newScore, currentTime, departmentId));
                        }

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                        deleteStatements = new BatchStatement();
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void RemoveFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is removed, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                            new List<string> { "user_id", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                            new List<string> { "user_id", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "topic_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser, topicId));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

#warning Delete event analytics if event is deleted
        public void DeleteEventLeaderboard(string eventId, ISession analyticSession = null)
        {

        }

        public void HideFromEventLeaderboard(string adminUserId, string companyId, string userId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                // This method only involves when suspending user
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Event leaderboard by user ------------------------
                PreparedStatement psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                RowSet leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(false, userId, score, updatedTimestamp, eventId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_event_sorted",
                        new List<string> { "event_id", "score", "last_updated_timestamp", "user_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(false, eventId, score, updatedTimestamp, userId));
                }

                analyticSession.Execute(updateStatements);
                updateStatements = new BatchStatement();

                //------------------------- Event department leaderboard by user ------------------------
                psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    string departmentId = leaderBoardByUserRow.GetValue<string>("department_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    Event selectedEvent = new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL, (int)Event.EventStatusEnum.Ignored, mainSession).Event;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Need to delete this first
                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id", }));
                    analyticSession.Execute(psLeaderBoardByUser.Bind(departmentId, eventId, score, updatedTimestamp, userId));

                    //------------------------- Department leaderboard by event ------------------------
                    PreparedStatement psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                      new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardByDepartmentRow = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId)).FirstOrDefault();

                    if (leaderBoardByDepartmentRow != null)
                    {
                        float departmentScore = leaderBoardByDepartmentRow.GetValue<float>("score");
                        DateTimeOffset updatedDepartmentTimestamp = leaderBoardByDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "department_id", "event_id", "score", "last_updated_timestamp" }));
                        deleteStatements.Add(psLeaderBoardByDepartment.Bind(departmentId, eventId, score, updatedTimestamp));

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoardByDepartment.Bind(newScore, currentTime, departmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoardByDepartment.Bind(eventId, newScore, currentTime, departmentId));
                        }

                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
        public void HideFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is hidden, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(false, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            if (numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany - numberAnswerCorrectlyByUser, currentTime, companyId));
                            }

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                    new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            if (numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser > 0)
                            {
                                psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                    new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                                updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser - numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                            }
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

        }

        public void UnhideFromEventLeaderboard(string adminUserId, string companyId, string userId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                // This method only involves when suspending user
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }
                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Event leaderboard by user ------------------------
                PreparedStatement psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                RowSet leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user_sorted",
                        new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(true, userId, score, updatedTimestamp, eventId));

                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_event_sorted",
                        new List<string> { "event_id", "score", "last_updated_timestamp", "user_id", }, new List<string> { "is_visible" }, new List<string>()));
                    updateStatements.Add(psLeaderBoardByUser.Bind(true, eventId, score, updatedTimestamp, userId));
                }

                analyticSession.Execute(updateStatements);
                updateStatements = new BatchStatement();

                //------------------------- Event department leaderboard by user ------------------------
                psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id" }));
                leaderBoardByUserRowset = analyticSession.Execute(psLeaderBoardByUser.Bind(userId));

                foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                {
                    string eventId = leaderBoardByUserRow.GetValue<string>("event_id");
                    string departmentId = leaderBoardByUserRow.GetValue<string>("department_id");
                    int score = leaderBoardByUserRow.GetValue<int>("score");
                    DateTimeOffset updatedTimestamp = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    Event selectedEvent = new Event().SelectEventByAdmin(adminUserId, eventId, companyId, Event.QUERY_TYPE_DETAIL, (int)Event.EventStatusEnum.Ignored, mainSession).Event;
                    int scoringType = selectedEvent.ScoringType;
                    int scoreCalculationType = selectedEvent.CalculationType;

                    // Need to insert this first
                    psLeaderBoardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id", }));
                    analyticSession.Execute(psLeaderBoardByUser.Bind(departmentId, eventId, score, updatedTimestamp, userId));

                    //------------------------- Department leaderboard by event ------------------------
                    PreparedStatement psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                      new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardByDepartmentRow = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId)).FirstOrDefault();

                    if (leaderBoardByDepartmentRow != null)
                    {
                        float departmentScore = leaderBoardByDepartmentRow.GetValue<float>("score");
                        DateTimeOffset updatedDepartmentTimestamp = leaderBoardByDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "department_id", "event_id", "score", "last_updated_timestamp" }));
                        deleteStatements.Add(psLeaderBoardByDepartment.Bind(departmentId, eventId, score, updatedTimestamp));

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoardByDepartment.Bind(departmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderBoardByDepartment.Bind(newScore, currentTime, departmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderBoardByDepartment = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderBoardByDepartment.Bind(eventId, newScore, currentTime, departmentId));
                        }

                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UnhideFromLeaderboard(bool forUser, bool forTopic, string companyId, ISession session, string userId = null, string mainDepartmentId = null, string topicId = null)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                // Topic is hidden, so leaderboard need to be updated
                if (forTopic && !string.IsNullOrEmpty(topicId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByTopic = session.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_topic_sorted",
                           new List<string>(), new List<string> { "topic_id" }));
                    BoundStatement bsLeaderBoardByTopic = psLeaderBoardByTopic.Bind(topicId);
                    RowSet leaderBoardByTopicRowset = session.Execute(bsLeaderBoardByTopic);


                    foreach (Row leaderBoardByTopicRow in leaderBoardByTopicRowset)
                    {
                        userId = leaderBoardByTopicRow.GetValue<string>("user_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByTopicRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByTopicRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));


                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, companyId));

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
                else if (forUser && !string.IsNullOrEmpty(userId))
                {
                    //------------------------- Topic leaderboard by user ------------------------
                    PreparedStatement psLeaderBoardByUser = session.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user_sorted",
                           new List<string>(), new List<string> { "user_id" }));
                    BoundStatement bsLeaderBoardByUser = psLeaderBoardByUser.Bind(userId);
                    RowSet leaderBoardByUserRowset = session.Execute(bsLeaderBoardByUser);


                    foreach (Row leaderBoardByUserRow in leaderBoardByUserRowset)
                    {
                        topicId = leaderBoardByUserRow.GetValue<string>("topic_id");
                        int numberAnswerCorrectlyByUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        //------------------------- Topic leaderboard by user ------------------------
                        //PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("topic_leaderboard_by_user",
                        //    new List<string> { "user_id", "topic_id" }));
                        //deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, topicId));

                        PreparedStatement psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("topic_leaderboard_by_user_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("user_leaderboard_by_topic_sorted",
                            new List<string> { "user_id", "topic_id", "number_answered_correctly", "last_updated_timestamp", }, new List<string> { "is_visible" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(true, userId, topicId, numberAnswerCorrectlyByUser, lastUpdatedTimestampByUser));

                        //------------------------- Company leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_user",
                               new List<string>(), new List<string> { "user_id" }));
                        BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId);
                        Row leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByCompany = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByCompany = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_user",
                            new List<string> { "user_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany, lastUpdatedTimestampByCompany, companyId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_company_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "company_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByCompany + numberAnswerCorrectlyByUser, currentTime, companyId));

                        }

                        //------------------------- Company leaderboard by department ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(mainDepartmentId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyByDepartment + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }


                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = session.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                            new List<string>(), new List<string> { "user_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(userId);
                        leaderBoardRow = session.Execute(bsLeaderBoard).FirstOrDefault();

                        if (leaderBoardRow != null)
                        {
                            int numberAnswerCorrectlyByDepartmentUser = leaderBoardRow.GetValue<int>("number_answered_correctly");
                            DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderBoard = session.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(mainDepartmentId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, userId));

                            psLeaderBoard = session.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, mainDepartmentId));

                            psLeaderBoard = session.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser + numberAnswerCorrectlyByUser, currentTime, mainDepartmentId));
                        }

                        session.Execute(deleteStatements);
                        session.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SwitchLeaderboardForDepartment(string userId, string companyId, string currentDepartmentId, string newDepartmentId, ISession analyticSession = null)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }


                DateTimeOffset currentTime = DateTime.UtcNow;

                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                //------------------------- Department leaderboard by user ------------------------
                PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_user",
                       new List<string>(), new List<string> { "user_id", "department_id" }));
                BoundStatement bsLeaderBoard = psLeaderBoard.Bind(userId, currentDepartmentId);
                Row leaderBoardByUserRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                // Only if the user has contributed some points to that department
                if (leaderBoardByUserRow != null)
                {
                    int numberAnswerCorrectlyByDepartmentUser = leaderBoardByUserRow.GetValue<int>("number_answered_correctly");
                    DateTimeOffset lastUpdatedTimestampByDepartmentUser = leaderBoardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    if (numberAnswerCorrectlyByDepartmentUser > 0)
                    {
                        // Current department points
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                               new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(currentDepartmentId);

                        Row currentDepartmentRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyForCurrentDepartment = currentDepartmentRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestampByCurrentDepartment = currentDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        // New department points
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("company_leaderboard_by_department",
                               new List<string>(), new List<string> { "department_id" }));
                        bsLeaderBoard = psLeaderBoard.Bind(newDepartmentId);

                        Row newDepartmentRow = analyticSession.Execute(bsLeaderBoard).FirstOrDefault();

                        int numberAnswerCorrectlyForNewDepartment = 0;
                        DateTimeOffset lastUpdatedTimestampByNewDepartment = currentTime;

                        if (newDepartmentRow != null)
                        {
                            numberAnswerCorrectlyForNewDepartment = newDepartmentRow.GetValue<int>("number_answered_correctly");
                            lastUpdatedTimestampByNewDepartment = newDepartmentRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        }

                        //------------------------- Company leaderboard by department (Current Department) ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                                   new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser, currentTime, currentDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment, lastUpdatedTimestampByCurrentDepartment, currentDepartmentId));

                        if (numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser > 0)
                        {
                            psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                            updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForCurrentDepartment - numberAnswerCorrectlyByDepartmentUser, currentTime, currentDepartmentId));
                        }

                        //------------------------- Company leaderboard by department (New Department) ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("company_leaderboard_by_department",
                                                   new List<string> { "department_id" }, new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment + numberAnswerCorrectlyByDepartmentUser, currentTime, newDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment, lastUpdatedTimestampByNewDepartment, newDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                                                   new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(companyId, numberAnswerCorrectlyForNewDepartment + numberAnswerCorrectlyByDepartmentUser, currentTime, newDepartmentId));

                        //------------------------- Department leaderboard by user ------------------------
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("department_leaderboard_by_user",
                                    new List<string> { "user_id" }, new List<string> { "department_id", "number_answered_correctly" }, new List<string>()));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(newDepartmentId, numberAnswerCorrectlyByDepartmentUser, userId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                   new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        deleteStatements = deleteStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, currentDepartmentId));

                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                            new List<string> { "user_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        updateStatements = updateStatements.Add(psLeaderBoard.Bind(userId, numberAnswerCorrectlyByDepartmentUser, lastUpdatedTimestampByDepartmentUser, newDepartmentId));


                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SwitchEventLeaderboardForDepartment(string userId, string companyId, string currentDepartmentId, string newDepartmentId, ISession mainSession = null, ISession analyticSession = null)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                Event eventManager = new Event();

                DateTimeOffset currentTime = DateTime.UtcNow;

                // Update everyone/personnel targeted events
                PreparedStatement psLeaderboardByUser = null;
                BatchStatement deleteStatements = new BatchStatement();
                BatchStatement updateStatements = new BatchStatement();

                #region Personnel Events
                List<Event> personnelEvents = eventManager.SelectLivePersonnelEventsByUserId(userId, companyId, mainSession).Events;
                foreach (Event personnelEvent in personnelEvents)
                {
                    string eventId = personnelEvent.EventId;
                    psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_user",
                        new List<string>(), new List<string> { "user_id", "event_id" }));
                    Row leaderboardByUserRow = analyticSession.Execute(psLeaderboardByUser.Bind(userId, eventId)).FirstOrDefault();

                    if (leaderboardByUserRow != null)
                    {
                        int score = leaderboardByUserRow.GetValue<int>("score");
                        bool isVisible = leaderboardByUserRow.GetValue<bool>("is_visible");
                        DateTimeOffset updatedTimestamp = leaderboardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_user_sorted",
                           new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_event_sorted",
                            new List<string> { "user_id", "score", "last_updated_timestamp", "event_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_user",
                            new List<string> { "user_id", "event_id" }, new List<string> { "department_id" }, new List<string>()));
                        updateStatements.Add(psLeaderboardByUser.Bind(newDepartmentId, userId, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("event_leaderboard_by_user_sorted",
                          new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", "department_id", "is_visible" }));
                        updateStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId, newDepartmentId, isVisible));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_event_sorted",
                          new List<string> { "user_id", "score", "last_updated_timestamp", "event_id", "department_id", "is_visible" }));
                        updateStatements.Add(psLeaderboardByUser.Bind(userId, score, updatedTimestamp, eventId, newDepartmentId, isVisible));

                        analyticSession.Execute(deleteStatements);
                        analyticSession.Execute(updateStatements);

                        deleteStatements = new BatchStatement();
                        updateStatements = new BatchStatement();
                    }
                }
                #endregion

                #region Department events
                // Update department targeted events
                List<Event> currentDepartmentEvents = eventManager.SelectLiveDepartmentEventsByDepartmentId(null, currentDepartmentId, companyId, mainSession).Events;
                List<Event> newDepartmentEvents = eventManager.SelectLiveDepartmentEventsByDepartmentId(null, newDepartmentId, companyId, mainSession).Events;

                #region Remove from current department events
                //Remove from current department events
                foreach (Event currentDepartmentEvent in currentDepartmentEvents)
                {
                    string eventId = currentDepartmentEvent.EventId;
                    DateTimeOffset startTimeOffset = currentDepartmentEvent.StartTimestamp;
                    DateTimeOffset endTimeOffset = currentDepartmentEvent.EndTimestamp;

                    int scoringType = currentDepartmentEvent.ScoringType;
                    int scoreCalculationType = currentDepartmentEvent.CalculationType;

                    psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                        new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderboardByUserRow = analyticSession.Execute(psLeaderboardByUser.Bind(currentDepartmentId, eventId)).FirstOrDefault();

                    if (leaderboardByUserRow != null)
                    {
                        float score = leaderboardByUserRow.GetValue<float>("score");
                        DateTimeOffset updatedTimestamp = leaderboardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_leaderboard_by_department",
                           new List<string> { "department_id", "event_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(eventId, score, updatedTimestamp, currentDepartmentId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department",
                            new List<string> { "department_id", "event_id", "user_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId, userId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_department_leaderboard_by_user",
                            new List<string> { "department_id", "event_id", "user_id" }));
                        deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId, userId));

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                            new List<string>(), new List<string> { "department_id", "event_id", "user_id" }));
                        leaderboardByUserRow = analyticSession.Execute(psLeaderboardByUser.Bind(currentDepartmentId, eventId, userId)).FirstOrDefault();

                        if (leaderboardByUserRow != null)
                        {
                            score = leaderboardByUserRow.GetValue<float>("score");
                            updatedTimestamp = leaderboardByUserRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                            psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("event_user_leaderboard_by_department_sorted",
                                new List<string> { "department_id", "event_id", "score", "last_updated_timestamp", "user_id" }));
                            deleteStatements.Add(psLeaderboardByUser.Bind(currentDepartmentId, eventId, score, updatedTimestamp, userId));
                        }

                        analyticSession.Execute(deleteStatements);
                        deleteStatements = new BatchStatement();

                        // Recalculate
                        // Update
                        float newScore = 0f;

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department_sorted",
                            new List<string>(), new List<string> { "department_id", "event_id" }));
                        RowSet leaderBoardRowset = analyticSession.Execute(psLeaderboardByUser.Bind(currentDepartmentId, eventId));

                        foreach (Row leaderboardRow in leaderBoardRowset)
                        {
                            int scoreByUser = leaderboardRow.GetValue<int>("score");
                            newScore += scoreByUser;
                        }

                        if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                        {
                            int numberOfPersonnel = 0;
                            Department departmentManager = new Department();
                            Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, currentDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                            numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                            newScore = newScore / numberOfPersonnel;
                        }

                        psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                            new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                        updateStatements.Add(psLeaderboardByUser.Bind(newScore, currentTime, currentDepartmentId, eventId));

                        if (newScore > 0)
                        {
                            psLeaderboardByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                                new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                            updateStatements.Add(psLeaderboardByUser.Bind(eventId, newScore, currentTime, currentDepartmentId));
                        }

                        analyticSession.Execute(updateStatements);
                        updateStatements = new BatchStatement();
                    }
                }
                #endregion

                #region Insert to new department events
                // Add to new department events
                foreach (Event newDepartmentEvent in newDepartmentEvents)
                {
                    string eventId = newDepartmentEvent.EventId;
                    DateTimeOffset startTimeOffset = newDepartmentEvent.StartTimestamp;
                    DateTimeOffset endTimeOffset = newDepartmentEvent.EndTimestamp;

                    int scoringType = newDepartmentEvent.ScoringType;
                    int scoreCalculationType = newDepartmentEvent.CalculationType;

                    int scoreToAdd = 0;
                    if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative)
                    {
                        foreach (Topic topicInEvent in newDepartmentEvent.Topics)
                        {
                            foreach (ChallengeQuestion question in topicInEvent.Questions)
                            {
                                string questionId = question.Id;

                                // Get the total number of correct times from start to end date
                                PreparedStatement psScoreboard = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("challenge_question_stats_by_user",
                                    new List<string>(), new List<string> { "user_id", "question_id", "is_correct" }, "answered_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "answered_timestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                                BoundStatement bsScoreboard = psScoreboard.Bind(userId, questionId, true, startTimeOffset, endTimeOffset);
                                RowSet scoreboardRowset = analyticSession.Execute(bsScoreboard);

                                scoreToAdd += scoreboardRowset.ToList().Count();
                            }

                        }

                    }
                    else if (scoreCalculationType == (int)Event.CalculationTypeEnum.TotalUnique || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                    {
                        foreach (Topic topicInEvent in newDepartmentEvent.Topics)
                        {
                            foreach (ChallengeQuestion question in topicInEvent.Questions)
                            {
                                string questionId = question.Id;

                                // Unique does not take in previously correct answer
                                PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_question_stats_by_user",
                                    new List<string>(), new List<string> { "user_id", "question_id" }, 1));
                                BoundStatement bsChallengeStats = psChallengeStats.Bind(userId, questionId);
                                Row challengeStatsRow = analyticSession.Execute(bsChallengeStats).FirstOrDefault();

                                if (challengeStatsRow != null & challengeStatsRow.GetValue<bool>("is_correct"))
                                {
                                    scoreToAdd += 1;
                                }
                            }
                        }
                    }

                    //------------------------- Event user leaderboard by department ------------------------
                    // Update
                    PreparedStatement psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_user_leaderboard_by_department",
                        new List<string> { "department_id", "event_id", "user_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(scoreToAdd, currentTime, newDepartmentId, eventId, userId));

                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("event_user_leaderboard_by_department_sorted",
                        new List<string> { "event_id", "user_id", "score", "last_updated_timestamp", "department_id" }));
                    updateStatements.Add(psLeaderBoard.Bind(eventId, userId, scoreToAdd, currentTime, newDepartmentId));

                    analyticSession.Execute(updateStatements);

                    updateStatements = new BatchStatement();

                    //------------------------- Event leaderboard by department ------------------------
                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_leaderboard_by_department",
                       new List<string>(), new List<string> { "department_id", "event_id" }));
                    Row leaderBoardRow = analyticSession.Execute(psLeaderBoard.Bind(newDepartmentId, eventId)).FirstOrDefault();

                    float numberAnswerCorrectlyByDepartment = 0f;
                    DateTimeOffset lastUpdatedTimestampByDepartment = currentTime;

                    if (leaderBoardRow != null)
                    {
                        numberAnswerCorrectlyByDepartment = leaderBoardRow.GetValue<float>("score");
                        lastUpdatedTimestampByDepartment = leaderBoardRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                    }

                    // Delete
                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_event_sorted",
                        new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                    deleteStatements.Add(psLeaderBoard.Bind(eventId, numberAnswerCorrectlyByDepartment, lastUpdatedTimestampByDepartment, newDepartmentId));

                    // Update
                    float newScore = 0f;

                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.SelectStatement("event_user_leaderboard_by_department",
                        new List<string>(), new List<string> { "department_id", "event_id" }));
                    RowSet leaderBoardRowset = analyticSession.Execute(psLeaderBoard.Bind(newDepartmentId, eventId));

                    foreach (Row leaderboardRow in leaderBoardRowset)
                    {
                        int scoreByUser = leaderboardRow.GetValue<int>("score");
                        newScore += scoreByUser;
                    }

                    if (scoreCalculationType == (int)Event.CalculationTypeEnum.AverageAccumulative || scoreCalculationType == (int)Event.CalculationTypeEnum.AverageUnique)
                    {
                        int numberOfPersonnel = 0;
                        Department departmentManager = new Department();
                        Department currentDepartment = departmentManager.GetDepartmentDetail(null, companyId, newDepartmentId, Department.QUERY_TYPE_DETAIL, mainSession).Department;
                        numberOfPersonnel = (int)currentDepartment.CountOfUsers;

                        newScore = newScore / numberOfPersonnel;
                    }

                    psLeaderBoard = analyticSession.Prepare(CQLGenerator.UpdateStatement("event_leaderboard_by_department",
                        new List<string> { "department_id", "event_id" }, new List<string> { "score", "last_updated_timestamp" }, new List<string>()));
                    updateStatements.Add(psLeaderBoard.Bind(newScore, currentTime, newDepartmentId, eventId));

                    if (newScore > 0)
                    {
                        psLeaderBoard = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_event_sorted",
                            new List<string> { "event_id", "score", "last_updated_timestamp", "department_id" }));
                        updateStatements.Add(psLeaderBoard.Bind(eventId, newScore, currentTime, newDepartmentId));
                    }

                    analyticSession.Execute(deleteStatements);
                    analyticSession.Execute(updateStatements);

                }
                #endregion

                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void UpdateChallengeStats(string companyId, string userId, ChallengeStatType stat, ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getAnalyticSession();
                }

                string columnStatName = string.Empty;

                switch (stat)
                {
                    case ChallengeStatType.Draw:
                        columnStatName = "total_draws";
                        break;
                    case ChallengeStatType.Lost:
                        columnStatName = "total_losses";
                        break;
                    case ChallengeStatType.NetworkError:
                        columnStatName = "total_network_errors";
                        break;
                    case ChallengeStatType.Surrender:
                        columnStatName = "total_surrenders";
                        break;
                    case ChallengeStatType.Win:
                        columnStatName = "total_win";
                        break;

                }

                PreparedStatement psChallengeStats = session.Prepare(CQLGenerator.UpdateCounterStatement("challenge_total_stats_counter_by_user",
                    new List<string> { "company_id", "user_id" }, new List<string> { columnStatName, "total_games_played" }, new List<int> { 1, 1 }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(companyId, userId);

                session.Execute(bChallengeStats);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AnalyticsSelectLeaderboardByCompanyResponse SelectLeaderboardByCompany(string requesterUserId, string companyId)
        {
            AnalyticsSelectLeaderboardByCompanyResponse response = new AnalyticsSelectLeaderboardByCompanyResponse();
            response.TopPersonnels = new List<User>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                if (vh.ValidateCompany(companyId, mainSession) == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_company_sorted",
                    new List<string>(), new List<string> { "company_id" }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(companyId);

                RowSet challengeStatsRowSet = analyticSession.Execute(bChallengeStats);

                User user = new User();
                List<string> cleanUpList = new List<string>();
                foreach (Row challengeStatsRow in challengeStatsRowSet)
                {
                    string userId = challengeStatsRow.GetValue<string>("user_id");

                    if (!cleanUpList.Contains(userId))
                    {
                        User rankedUser = user.SelectUserBasic(userId, companyId, true, mainSession).User;

                        if (rankedUser != null)
                        {
                            response.TopPersonnels.Add(rankedUser);
                        }

                        cleanUpList.Add(userId);
                    }
                    else
                    {
                        Log.Error(string.Format("UserId: {0} duplicated for user leaderboard by company.", userId));
                        int numberAnsweredCorrectly = challengeStatsRow.GetValue<int>("number_answered_correctly");
                        DateTimeOffset lastUpdatedTimestamp = challengeStatsRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                        psChallengeStats = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                        analyticSession.Execute(psChallengeStats.Bind(companyId, numberAnsweredCorrectly, lastUpdatedTimestamp, userId));
                    }

                }

                int limit = Convert.ToInt16(WebConfigurationManager.AppSettings["user_leaderboard_by_company"]);

                if (response.TopPersonnels.Count > limit)
                {
                    response.TopPersonnels = response.TopPersonnels.Take(limit).ToList();
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectLeaderboardByDepartmentResponse SelectLeaderboardByDepartment(string requesterUserId, string companyId)
        {
            AnalyticsSelectLeaderboardByDepartmentResponse response = new AnalyticsSelectLeaderboardByDepartmentResponse();
            response.TopDepartments = new List<Department>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                if (vh.ValidateCompany(companyId, mainSession) == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                bool reload = true;

                while (reload)
                {
                    PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_company_sorted",
                    new List<string>(), new List<string> { "company_id" }));
                    BoundStatement bChallengeStats = psChallengeStats.Bind(companyId);

                    RowSet challengeStatsRowSet = analyticSession.Execute(bChallengeStats);
                    List<Row> challengeStatsRowList = challengeStatsRowSet.ToList();

                    if (challengeStatsRowList.Count == 0)
                    {
                        break;
                    }

                    User user = new User();
                    Department department = new Department();

                    List<string> cleanUpDepartmentList = new List<string>();

                    foreach (Row challengeStatsRow in challengeStatsRowList)
                    {
                        string departmentId = challengeStatsRow.GetValue<string>("department_id");

                        if (!cleanUpDepartmentList.Contains(departmentId))
                        {
                            Department departmentLeaderboard = department.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;

                            if (departmentLeaderboard != null)
                            {
                                departmentLeaderboard.Users = new List<User>();

                                int limit = Int16.Parse(WebConfigurationManager.AppSettings["user_leaderboard_by_department"]);

                                psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("user_leaderboard_by_department_sorted",
                                    new List<string>(), new List<string> { "department_id" }, limit));
                                bChallengeStats = psChallengeStats.Bind(departmentId);
                                RowSet departmentStatsRowSet = analyticSession.Execute(bChallengeStats);

                                List<string> cleanUpUserList = new List<string>();
                                bool isUserInDepartmentDuplicated = false;
                                foreach (Row departmentStatsRow in departmentStatsRowSet)
                                {
                                    string userId = departmentStatsRow.GetValue<string>("user_id");

                                    if (!cleanUpUserList.Contains(userId))
                                    {
                                        User rankedUser = user.SelectUserBasic(userId, companyId, true, mainSession).User;

                                        if (rankedUser != null)
                                        {
                                            departmentLeaderboard.Users.Add(rankedUser);
                                        }

                                        cleanUpUserList.Add(userId);
                                    }
                                    // Clean up
                                    else
                                    {
                                        isUserInDepartmentDuplicated = true;

                                        Log.Error(string.Format("UserId: {0} duplicated for user leaderboard by department: {1}.", userId, departmentId));

                                        int totalNumberAnsweredCorrectly = 0;

                                        // Looping through records
                                        PreparedStatement psUserLeaderboard = analyticSession.Prepare(CQLGenerator.SelectStatement("topic_leaderboard_by_user",
                                            new List<string>(), new List<string> { "user_id" }));
                                        RowSet userLeaderboardRowSet = analyticSession.Execute(psUserLeaderboard.Bind(userId));

                                        DateTimeOffset? latestTimestamp = null;
                                        foreach (Row userLeaderboardRow in userLeaderboardRowSet)
                                        {
                                            DateTimeOffset lastUpdatedTimestamp = userLeaderboardRow.GetValue<DateTime>("last_updated_timestamp");
                                            int numberAnsweredCorrectly = userLeaderboardRow.GetValue<int>("number_answered_correctly");
                                            if (latestTimestamp == null || (latestTimestamp != null && latestTimestamp.Value < latestTimestamp))
                                            {
                                                latestTimestamp = lastUpdatedTimestamp;
                                            }

                                            totalNumberAnsweredCorrectly += numberAnsweredCorrectly;
                                        }

                                        PreparedStatement psDepartmentByUser = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_department_sorted",
                                            new List<string>(), new List<string> { "department_id" }));
                                        RowSet departmentByUserRowSet = analyticSession.Execute(psDepartmentByUser.Bind(departmentLeaderboard.Id));

                                        BatchStatement batchStatement = new BatchStatement();
                                        foreach (Row departmentByUserRow in departmentByUserRowSet)
                                        {
                                            string departmentUserId = departmentByUserRow.GetValue<string>("user_id");

                                            if (userId.Equals(departmentUserId))
                                            {
                                                DateTimeOffset lastUpdatedTimestamp = departmentByUserRow.GetValue<DateTime>("last_updated_timestamp");
                                                int numberAnsweredCorrectly = departmentByUserRow.GetValue<int>("number_answered_correctly");

                                                psDepartmentByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("user_leaderboard_by_department_sorted",
                                                    new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                                batchStatement.Add(psDepartmentByUser.Bind(departmentLeaderboard.Id, numberAnsweredCorrectly, lastUpdatedTimestamp, userId));
                                            }
                                        }

                                        psDepartmentByUser = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_user",
                                                    new List<string> { "user_id" }));
                                        batchStatement.Add(psDepartmentByUser.Bind(userId));

                                        // Remove 
                                        analyticSession.Execute(batchStatement);

                                        batchStatement = new BatchStatement();

                                        psDepartmentByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("user_leaderboard_by_department_sorted",
                                                   new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                        batchStatement.Add(psDepartmentByUser.Bind(departmentLeaderboard.Id, totalNumberAnsweredCorrectly, latestTimestamp.Value, userId));

                                        psDepartmentByUser = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_user",
                                            new List<string> { "department_id", "number_answered_correctly", "last_updated_timestamp", "user_id" }));
                                        batchStatement.Add(psDepartmentByUser.Bind(departmentLeaderboard.Id, totalNumberAnsweredCorrectly, latestTimestamp.Value, userId));

                                        // Add back
                                        analyticSession.Execute(batchStatement);

                                        // Recalcuate
                                        RecalculateDepartmentScore(analyticSession, companyId, departmentId);
                                        break;
                                    }


                                }

                                if (isUserInDepartmentDuplicated)
                                {
                                    reload = true;
                                    cleanUpUserList = new List<string>();
                                    response.TopDepartments = new List<Department>();
                                    cleanUpDepartmentList = new List<string>();
                                    break;
                                }

                                response.TopDepartments.Add(departmentLeaderboard);
                                cleanUpDepartmentList.Add(departmentId);
                            }

                            reload = false;
                        }
                        else
                        {
                            reload = true;
                            response.TopDepartments = new List<Department>();

                            Log.Error(string.Format("DepartmentId: {0} duplicated for user leaderboard by department.", departmentId));

                            RecalculateDepartmentScore(analyticSession, companyId, departmentId);

                            break;
                        }

                    }
                }


                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectLeaderboardByEventResponse SelectLeaderboardByEvent(string requesterUserId, string companyId, string eventId, int numberOfLeaderboard, int participantType, int scoringType, int calculationType, ISession mainSession, ISession analyticSession)
        {
            AnalyticsSelectLeaderboardByEventResponse response = new AnalyticsSelectLeaderboardByEventResponse();
            response.TopEventLeaderboard = new List<EventLeaderboard>();
            response.OutOfRankingLeaderboard = new List<EventLeaderboard>();
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                ConnectionManager cm = new ConnectionManager();

                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                User userManager = new User();
                Department departmentManager = new Department();

                if (scoringType == (int)Event.ScoringTypeEnum.TopScorer)
                {
                    PreparedStatement psUserByEvent = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_event_sorted",
                       new List<string>(), new List<string> { "event_id", "is_visible" }));
                    RowSet userByEventRowset = analyticSession.Execute(psUserByEvent.Bind(eventId, true));
                    List<Row> userByEventList = userByEventRowset.ToList();

                    int rank = 1;
                    bool isUserFoundWithinLeaderboard = false;

                    int countDownLimit = userByEventList.Count;

                    for (int index = 0; index < countDownLimit; index++)
                    {
                        Row userByEventRow = userByEventList[index];

                        string userId = userByEventRow.GetValue<string>("user_id");
                        string departmentId = userByEventRow.GetValue<string>("department_id");
                        int score = userByEventRow.GetValue<int>("score");

                        User selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                        Department selectedDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;

                        if (selectedUser != null && selectedDepartment != null)
                        {
                            selectedUser.Departments = new List<Department> { selectedDepartment };
                            EventLeaderboard eventLeaderboard = new EventLeaderboard
                            {
                                User = selectedUser,
                                Score = score,
                                Rank = rank
                            };

                            if (response.TopEventLeaderboard.Count < numberOfLeaderboard)
                            {
                                if (userId.Equals(requesterUserId))
                                {
                                    isUserFoundWithinLeaderboard = true;
                                }

                                response.TopEventLeaderboard.Add(eventLeaderboard);
                            }
                            else
                            {
                                if (isUserFoundWithinLeaderboard)
                                {
                                    break;
                                }

                                // Out of ranking
                                if (userId.Equals(requesterUserId))
                                {
                                    int limitForOutOfRank = Convert.ToInt16(WebConfigurationManager.AppSettings["event_leaderboard_out_of_ranking_limit"]);

                                    int rankForCurrentUser = rank;

                                    int lowerLimit = 0;
                                    int upperLimit = 0;

                                    if (rankForCurrentUser - limitForOutOfRank > numberOfLeaderboard)
                                    {
                                        lowerLimit = rankForCurrentUser - limitForOutOfRank - 1;
                                    }
                                    else
                                    {
                                        lowerLimit = numberOfLeaderboard;
                                    }

                                    rank = lowerLimit + 1;

                                    upperLimit = rankForCurrentUser + limitForOutOfRank < countDownLimit ? rankForCurrentUser + limitForOutOfRank : countDownLimit;

                                    for (int lowerRankIndex = lowerLimit; lowerRankIndex < upperLimit; lowerRankIndex++)
                                    {
                                        userByEventRow = userByEventList[lowerRankIndex];

                                        userId = userByEventRow.GetValue<string>("user_id");
                                        departmentId = userByEventRow.GetValue<string>("department_id");
                                        score = userByEventRow.GetValue<int>("score");

                                        selectedUser = userManager.SelectUserBasic(userId, companyId, true, mainSession).User;
                                        selectedDepartment = departmentManager.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, mainSession).Department;

                                        if (selectedUser != null && selectedDepartment != null)
                                        {
                                            eventLeaderboard = new EventLeaderboard
                                            {
                                                User = selectedUser,
                                                Score = score,
                                                Rank = lowerRankIndex + 1
                                            };

                                            response.OutOfRankingLeaderboard.Add(eventLeaderboard);
                                            rank++;
                                        }

                                    }
                                }
                            }

                            rank++;
                        }
                    }



                }
                else if (scoringType == (int)Event.ScoringTypeEnum.TopDepartment)
                {

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private void RecalculateDepartmentScore(ISession analyticSession, string companyId, string departmentId)
        {
            try
            {
                PreparedStatement psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_department_sorted",
                                   new List<string>(), new List<string> { "department_id" }));
                BoundStatement bChallengeStats = psChallengeStats.Bind(departmentId);
                RowSet departmentStatsRowSet = analyticSession.Execute(bChallengeStats);

                DateTimeOffset? latestDatetime = null;
                int totalNumberOfCorrectAnswers = 0;

                foreach (Row departmentStatsRow in departmentStatsRowSet)
                {
                    string userId = departmentStatsRow.GetValue<string>("user_id");
                    int numberAnswered = departmentStatsRow.GetValue<int>("number_answered_correctly");
                    DateTimeOffset dateTimestamp = departmentStatsRow.GetValue<DateTimeOffset>("last_updated_timestamp");

                    totalNumberOfCorrectAnswers += numberAnswered;

                    if (latestDatetime == null)
                    {
                        latestDatetime = dateTimestamp;
                    }
                    else
                    {
                        if (dateTimestamp > latestDatetime.Value)
                        {
                            latestDatetime = dateTimestamp;
                        }
                    }
                }

                psChallengeStats = analyticSession.Prepare(CQLGenerator.SelectStatement("department_leaderboard_by_company_sorted",
                    new List<string>(), new List<string> { "company_id" }));
                bChallengeStats = psChallengeStats.Bind(companyId);

                RowSet cleanUpRowSet = analyticSession.Execute(bChallengeStats);

                PreparedStatement psUpdate = null;
                BatchStatement bsDelete = new BatchStatement();
                BatchStatement bsInsert = new BatchStatement();

                foreach (Row cleanUpRow in cleanUpRowSet)
                {
                    string cleanUpDepartmentId = cleanUpRow.GetValue<string>("department_id");
                    if (cleanUpDepartmentId.Equals(departmentId))
                    {
                        DateTimeOffset lastUpdatedTimestamp = cleanUpRow.GetValue<DateTimeOffset>("last_updated_timestamp");
                        int totalNumberOfCorrect = cleanUpRow.GetValue<int>("number_answered_correctly");
                        psUpdate = analyticSession.Prepare(CQLGenerator.DeleteStatement("department_leaderboard_by_company_sorted",
                            new List<string> { "company_id", "number_answered_correctly", "last_updated_timestamp", "department_id" }));
                        bsDelete.Add(psUpdate.Bind(companyId, totalNumberOfCorrect, lastUpdatedTimestamp, departmentId));
                    }
                }

                psUpdate = analyticSession.Prepare(CQLGenerator.DeleteStatement("company_leaderboard_by_department",
                            new List<string> { "department_id" }));
                bsDelete.Add(psUpdate.Bind(departmentId));

                analyticSession.Execute(bsDelete);

                psUpdate = analyticSession.Prepare(CQLGenerator.InsertStatement("company_leaderboard_by_department",
                            new List<string> { "department_id", "company_id", "last_updated_timestamp", "number_answered_correctly" }));
                bsInsert.Add(psUpdate.Bind(departmentId, companyId, latestDatetime, totalNumberOfCorrectAnswers));

                psUpdate = analyticSession.Prepare(CQLGenerator.InsertStatement("department_leaderboard_by_company_sorted",
                    new List<string> { "department_id", "company_id", "last_updated_timestamp", "number_answered_correctly" }));
                bsInsert.Add(psUpdate.Bind(departmentId, companyId, latestDatetime, totalNumberOfCorrectAnswers));

                analyticSession.Execute(bsInsert);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AnalyticsSelectAllGameHistoriesResponse SelectAllGameHistoriesForUser(string requesterUserId,
                                                                                    string companyId,
                                                                                    int numberOfHistoryLoaded,
                                                                                    DateTime? newestTimestamp,
                                                                                    DateTime? oldestTimestamp)
        {
            AnalyticsSelectAllGameHistoriesResponse response = new AnalyticsSelectAllGameHistoriesResponse();
            response.GameHistories = new List<GameHistory>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                int limit = Int16.Parse(WebConfigurationManager.AppSettings["game_history_limit"]);
                //int limit = 0;

                if (numberOfHistoryLoaded > 0)
                {
                    limit += numberOfHistoryLoaded;
                }

                bool isSelectCompleted = false;

                Feed feed = new Feed();
                User user = new User();

                PreparedStatement psHistory = null;
                BoundStatement bsHistory = null;

                List<GameHistory> gameHistories = new List<GameHistory>();

                while (isSelectCompleted == false)
                {
                    if (oldestTimestamp != null)
                    {
                        DateTime currentDateTime = oldestTimestamp.Value;
                        psHistory = session.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_history_by_user_desc",
                            null, new List<string> { "user_id" }, "completed_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        bsHistory = psHistory.Bind(requesterUserId, DateHelper.ConvertDateToLong(currentDateTime));
                    }
                    else
                    {
                        psHistory = session.Prepare(CQLGenerator.SelectStatementWithLimit("challenge_history_by_user_desc",
                            new List<string>(), new List<string> { "user_id" }, limit));
                        bsHistory = psHistory.Bind(requesterUserId);
                    }

                    RowSet completedHistoryRowSet = session.Execute(bsHistory);
                    List<Row> completedHistoryRowList = new List<Row>();
                    completedHistoryRowList = completedHistoryRowSet.GetRows().ToList();

                    foreach (Row completedHistoryRow in completedHistoryRowList)
                    {
                        string challengeId = completedHistoryRow.GetValue<string>("challenge_id");

                        Row challengeRow = vh.ValidateChallenge(challengeId, companyId, session);

                        DateTime? completedOnTimestamp = completedHistoryRow.GetValue<DateTime?>("completed_on_timestamp");
                        oldestTimestamp = completedOnTimestamp;

                        if (challengeRow == null)
                        {
                            continue;
                        }


                        string opponentUserId = completedHistoryRow.GetValue<string>("opponent_user_id");

                        User opponentUser = user.SelectUserBasic(opponentUserId, companyId, false, session).User;

                        psHistory = session.Prepare(CQLGenerator.SelectStatement("challenge_history",
                            new List<string>(), new List<string> { "company_id", "id", "is_valid" }));
                        bsHistory = psHistory.Bind(companyId, challengeId, true);
                        Row historyRow = session.Execute(bsHistory).FirstOrDefault();

                        if (historyRow == null)
                        {
                            continue;
                        }

                        string challengeTopicId = historyRow.GetValue<string>("topic_id");
                        string challengeTopicCategoryId = historyRow.GetValue<string>("topic_category_id");

                        Topic challengeTopic = new Topic().SelectTopicBasic(challengeTopicId, null, companyId, challengeTopicCategoryId, null, session).Topic;
                        if (challengeTopic == null)
                        {
                            continue;
                        }

                        string winnerId = historyRow.GetValue<string>("winner_id");
                        int outcome = (int)ResultOutcome.Draw;

                        if (!string.IsNullOrEmpty(winnerId))
                        {
                            if (winnerId.Equals(requesterUserId))
                            {
                                outcome = (int)ResultOutcome.Win;
                            }
                            else
                            {
                                outcome = (int)ResultOutcome.Lost;
                            }
                        }
                        else
                        {
                            if (historyRow["completed_on_timestamp"] == null)
                            {
                                outcome = (int)ResultOutcome.Incompleted;
                            }
                        }

                        GameHistory gameHistory = new GameHistory
                        {
                            Opponent = opponentUser,
                            CompletedOnTimestamp = completedOnTimestamp.Value,
                            ChallengeId = challengeId,
                            Topic = challengeTopic,
                            Outcome = outcome
                        };

                        response.GameHistories.Add(gameHistory);

                        if (gameHistories.Count == limit)
                        {
                            isSelectCompleted = true;
                            break;
                        }
                    }// for

                    if (completedHistoryRowList.Count == 0)
                    {
                        isSelectCompleted = true;
                    }

                }// while

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectGameResultResponse SelectGameResultForChallenge(string requesterUserId,
                                                                              string companyId,
                                                                              string challengeId)
        {
            AnalyticsSelectGameResultResponse response = new AnalyticsSelectGameResultResponse();
            response.Players = new List<User>();
            response.GameResult = new List<ChallengeQuestion>();
            response.Outcome = (int)ResultOutcome.Draw;
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row challengeRow = vh.ValidateChallenge(challengeId, companyId, session);
                if (challengeRow == null)
                {
                    Log.Error("Invalid challengeId: " + challengeId);
                    response.ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalid);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalid;
                    return response;
                }

                List<string> playerIds = challengeRow.GetValue<List<string>>("players_ids");
                if (!playerIds.Contains(requesterUserId))
                {
                    Log.Error("Invalid player for challenge");
                    response.ErrorCode = Int16.Parse(ErrorCode.ChallengeInvalidPlayer);
                    response.ErrorMessage = ErrorMessage.ChallengeInvalidPlayer;
                    return response;
                }

                User user = new User();
                foreach (string playerId in playerIds)
                {
                    User player = user.SelectUserBasic(playerId, companyId, false, session).User;
                    response.Players.Add(player);
                }

                string winnerId = challengeRow["winner_id"] == null ? string.Empty : challengeRow.GetValue<string>("winner_id");
                if (!string.IsNullOrEmpty(winnerId))
                {
                    if (winnerId.Equals(requesterUserId))
                    {
                        response.Outcome = (int)ResultOutcome.Win;
                    }
                    else
                    {
                        response.Outcome = (int)ResultOutcome.Lost;
                    }
                }
                else
                {
                    if (challengeRow["completed_on_timestamp"] == null)
                    {
                        response.Outcome = (int)ResultOutcome.Incompleted;
                    }
                }

                string topicId = challengeRow.GetValue<string>("topic_id");
                string topicCategoryId = challengeRow.GetValue<string>("topic_category_id");

                response.Topic = new Topic().SelectTopicBasic(topicId, null, companyId, topicCategoryId, null, session).Topic;

                PreparedStatement psFullChallengeHistory = session.Prepare(CQLGenerator.SelectStatement("full_challenge_history",
                    new List<string>(), new List<string> { "company_id", "challenge_id" }));
                RowSet fullChallengeHistoryRowSet = session.Execute(psFullChallengeHistory.Bind(companyId, challengeId));

                foreach (Row fullChallengeHistoryRow in fullChallengeHistoryRowSet)
                {
                    ChallengeQuestion question = new ChallengeQuestion();

                    string questionId = fullChallengeHistoryRow.GetValue<string>("question_id");
                    int questionType = fullChallengeHistoryRow.GetValue<int>("type");
                    int choiceType = fullChallengeHistoryRow.GetValue<int>("choice_type");

                    string content = fullChallengeHistoryRow.GetValue<string>("content");
                    string contentImageUrl = fullChallengeHistoryRow.GetValue<string>("content_image_url");
                    string contentImageMd5 = fullChallengeHistoryRow.GetValue<string>("content_image_md5");
                    string contentImageBackgroundColorCode = fullChallengeHistoryRow["content_image_background_color_code"] != null ? fullChallengeHistoryRow.GetValue<string>("content_image_background_color_code") : "#000000";

                    string firstChoiceId = fullChallengeHistoryRow.GetValue<string>("first_choice_id");
                    string firstChoice = fullChallengeHistoryRow.GetValue<string>("first_choice");
                    string firstChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("first_choice_image_url");
                    string firstChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("first_choice_image_md5");

                    string secondChoiceId = fullChallengeHistoryRow.GetValue<string>("second_choice_id");
                    string secondChoice = fullChallengeHistoryRow.GetValue<string>("second_choice");
                    string secondChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("second_choice_image_url");
                    string secondChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("second_choice_image_md5");

                    string thirdChoiceId = fullChallengeHistoryRow.GetValue<string>("third_choice_id");
                    string thirdChoice = fullChallengeHistoryRow.GetValue<string>("third_choice");
                    string thirdChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("third_choice_image_url");
                    string thirdChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("third_choice_image_md5");

                    string fourthChoiceId = fullChallengeHistoryRow.GetValue<string>("fourth_choice_id");
                    string fourthChoice = fullChallengeHistoryRow.GetValue<string>("fourth_choice");
                    string fourthChoiceImageUrl = fullChallengeHistoryRow.GetValue<string>("fourth_choice_image_url");
                    string fourthChoiceImageMd5 = fullChallengeHistoryRow.GetValue<string>("fourth_choice_image_md5");

                    float timeAnswering = fullChallengeHistoryRow.GetValue<float>("time_answering");
                    float timeReading = fullChallengeHistoryRow.GetValue<float>("time_reading");

                    int scoreMultiplier = fullChallengeHistoryRow.GetValue<int>("score_multiplier");

                    string correctAnswer = fullChallengeHistoryRow.GetValue<string>("answer");

                    string initiatedUserAnswer = fullChallengeHistoryRow["answer_of_initiated_user"] == null ? string.Empty : fullChallengeHistoryRow.GetValue<string>("answer_of_initiated_user");
                    string challengedUserAnswer = fullChallengeHistoryRow["answer_of_initiated_user"] == null ? string.Empty : fullChallengeHistoryRow.GetValue<string>("answer_of_challenged_user");

                    float timeTakenByInitiatedUser = fullChallengeHistoryRow["time_taken_by_initiated_user"] == null ? 0 : fullChallengeHistoryRow.GetValue<float>("time_taken_by_initiated_user");
                    float timeTakenByChallengedUser = fullChallengeHistoryRow["time_taken_by_challenged_user"] == null ? 0 : fullChallengeHistoryRow.GetValue<float>("time_taken_by_challenged_user");

                    if (questionType == ChallengeQuestion.QUESTION_TYPE_LONG || questionType == ChallengeQuestion.QUESTION_TYPE_NORMAL)
                    {

                        if (choiceType == ChallengeQuestion.CHOICE_TYPE_TEXT)
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoice = firstChoice,

                                SecondChoiceId = secondChoiceId,
                                SecondChoice = secondChoice,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoice = thirdChoice,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoice = fourthChoice,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                        else
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoiceContentImageUrl = firstChoiceImageUrl,
                                FirstChoiceContentImageMd5 = firstChoiceImageMd5,

                                SecondChoiceId = secondChoiceId,
                                SecondChoiceContentImageUrl = secondChoiceImageUrl,
                                SecondChoiceContentImageMd5 = secondChoiceImageMd5,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoiceContentImageUrl = thirdChoiceImageUrl,
                                ThirdChoiceContentImageMd5 = thirdChoiceImageMd5,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoiceContentImageUrl = fourthChoiceImageUrl,
                                FourthChoiceContentImageMd5 = fourthChoiceImageMd5,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                    }
                    else
                    {
                        if (choiceType == ChallengeQuestion.CHOICE_TYPE_TEXT)
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ContentImageUrl = contentImageUrl,
                                ContentImageMd5 = contentImageMd5,
                                ContentImageBackgroundColorCode = contentImageBackgroundColorCode,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoice = firstChoice,

                                SecondChoiceId = secondChoiceId,
                                SecondChoice = secondChoice,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoice = thirdChoice,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoice = fourthChoice,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                        else
                        {
                            question = new ChallengeQuestion
                            {
                                Id = questionId,
                                QuestionType = questionType,
                                Content = content,
                                ContentImageUrl = contentImageUrl,
                                ContentImageMd5 = contentImageMd5,
                                ContentImageBackgroundColorCode = contentImageBackgroundColorCode,
                                ChoiceType = choiceType,

                                FirstChoiceId = firstChoiceId,
                                FirstChoiceContentImageUrl = firstChoiceImageUrl,
                                FirstChoiceContentImageMd5 = firstChoiceImageMd5,

                                SecondChoiceId = secondChoiceId,
                                SecondChoiceContentImageUrl = secondChoiceImageUrl,
                                SecondChoiceContentImageMd5 = secondChoiceImageMd5,

                                ThirdChoiceId = thirdChoiceId,
                                ThirdChoiceContentImageUrl = thirdChoiceImageUrl,
                                ThirdChoiceContentImageMd5 = thirdChoiceImageMd5,

                                FourthChoiceId = fourthChoiceId,
                                FourthChoiceContentImageUrl = fourthChoiceImageUrl,
                                FourthChoiceContentImageMd5 = fourthChoiceImageMd5,

                                ScoreMultiplier = scoreMultiplier,

                                CorrectAnswer = correctAnswer,

                                TimeAssignedForAnswering = timeAnswering,
                                TimeAssignedForReading = timeReading,

                                InitiatedUserAnswer = initiatedUserAnswer,
                                ChallengedUserAnswer = challengedUserAnswer,

                                TimeTakenByInitiatedUser = timeTakenByInitiatedUser,
                                TimeTakenByChallengedUser = timeTakenByChallengedUser
                            };
                        }
                    }

                    response.GameResult.Add(question);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsUpdateDailyActiveUserResponse UpdateDailyActiveUser(DateTime today)
        {
            AnalyticsUpdateDailyActiveUserResponse response = new AnalyticsUpdateDailyActiveUserResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticsSession = cm.getAnalyticSession();

                PreparedStatement preparedStatement = mainSession.Prepare(CQLGenerator.SelectStatement("company",
                    new List<string>(), new List<string> { "is_valid" }));
                RowSet companyRowSet = mainSession.Execute(preparedStatement.Bind(true));

                // UTC (lack of one day)
                if (today == DateTime.MinValue)
                {
                    today = DateTime.UtcNow.Date.AddDays(1);
                }

                DateTime yesterday = today.Date.AddDays(-1);

                foreach (Row companyRow in companyRowSet)
                {
                    string companyId = companyRow.GetValue<string>("id");
                    preparedStatement = analyticsSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("user_activity_log",
                        null, new List<string> { "company_id" }, "activity_date_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "activity_date_timestamp", CQLGenerator.Comparison.LessThan, -1));
                    RowSet activityRowSet = analyticsSession.Execute(preparedStatement.Bind(companyId, yesterday, today));

                    // Total number of users
                    int totalNumberOfUsers = 0;
                    List<Department> departments = new Department().GetAllDepartment(null, companyId, Department.QUERY_TYPE_DETAIL, mainSession).Departments;

                    foreach (Department department in departments)
                    {
                        totalNumberOfUsers += (int)department.CountOfUsers;
                    }

                    List<Dictionary<string, object>> userList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> timeFrameList = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> userWithLastActiveList = new List<Dictionary<string, object>>();
                    double totalTimeTakenInMin = 0.0;

                    int numberOfActiveTimeFrameUsers = 0;

                    int numberOfActiveUsers = 0;
                    int numberOfIOS = 0;
                    int numberOfAndroid = 0;

                    foreach (Row activityRow in activityRowSet)
                    {
                        string userId = activityRow.GetValue<string>("user_id");
                        DateTime dateTimestamp = activityRow.GetValue<DateTime>("activity_date_timestamp");
                        dateTimestamp = dateTimestamp.AddMilliseconds(-dateTimestamp.Millisecond);
                        dateTimestamp = dateTimestamp.AddSeconds(-dateTimestamp.Second);

                        bool isActive = activityRow.GetValue<bool>("is_active");

                        if (isActive)
                        {
                            if (!userList.Any(user => user["userId"].Equals(userId)))
                            {
                                preparedStatement = mainSession.Prepare(CQLGenerator.SelectStatement("user_device_token",
                                    new List<string>(), new List<string> { "user_id" }));
                                Row deviceRow = mainSession.Execute(preparedStatement.Bind(userId)).FirstOrDefault();

                                if (deviceRow != null)
                                {
                                    int deviceType = deviceRow.GetValue<int>("device_type");

                                    if (deviceType == (int)User.DeviceTokenType.iOS)
                                    {
                                        numberOfIOS++;
                                    }
                                    else
                                    {
                                        numberOfAndroid++;
                                    }

                                    numberOfActiveUsers++;

                                    Dictionary<string, object> userDict = new Dictionary<string, object>();
                                    userDict.Add("userId", userId);
                                    userDict.Add("deviceType", deviceType);
                                    userList.Add(userDict);
                                }
                            }

                            if (!userWithLastActiveList.Any(active => active["userId"].Equals(userId)))
                            {
                                Dictionary<string, object> userWithLastActive = new Dictionary<string, object>();
                                userWithLastActive.Add("userId", userId);
                                userWithLastActive.Add("lastActive", dateTimestamp);
                                userWithLastActiveList.Add(userWithLastActive);
                            }
                            else
                            {
                                Dictionary<string, object> userWithLastActive = userWithLastActiveList.First(active => ((string)active["userId"]).Equals(userId));
                                userWithLastActive["lastActive"] = dateTimestamp;
                            }


                            numberOfActiveTimeFrameUsers++;

                            if (!timeFrameList.Any(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp))
                            {
                                Dictionary<string, object> timeFrame = new Dictionary<string, object>();

                                timeFrame.Add("dateTimestamp", dateTimestamp);
                                timeFrame.Add("active", numberOfActiveTimeFrameUsers);

                                timeFrameList.Add(timeFrame);
                            }
                            else
                            {
                                Dictionary<string, object> timeFrameDict = timeFrameList.First(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp);
                                timeFrameDict["active"] = numberOfActiveTimeFrameUsers;
                            }
                        }
                        else
                        {
                            if (numberOfActiveTimeFrameUsers - 1 >= 0)
                            {
                                numberOfActiveTimeFrameUsers--;
                            }

                            if (!timeFrameList.Any(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp))
                            {
                                Dictionary<string, object> timeFrame = new Dictionary<string, object>();

                                timeFrame.Add("dateTimestamp", dateTimestamp);
                                timeFrame.Add("active", numberOfActiveTimeFrameUsers);

                                timeFrameList.Add(timeFrame);
                            }
                            else
                            {
                                Dictionary<string, object> timeFrameDict = timeFrameList.First(timeFrame => (DateTime)timeFrame["dateTimestamp"] == dateTimestamp);
                                timeFrameDict["active"] = numberOfActiveTimeFrameUsers;
                            }

                            if (userWithLastActiveList.Any(active => ((string)active["userId"]).Equals(userId)))
                            {
                                // Calculate time taken
                                Dictionary<string, object> userWithLastActive = userWithLastActiveList.First(active => ((string)active["userId"]).Equals(userId));
                                double minTaken = dateTimestamp.Subtract((DateTime)userWithLastActive["lastActive"]).TotalMinutes;
                                totalTimeTakenInMin += minTaken;
                            }

                        }
                    }

                    //Write to db
                    BatchStatement batchStatement = new BatchStatement();

                    foreach (Dictionary<string, object> userDict in userList)
                    {
                        string userId = userDict["userId"].ToString();

                        preparedStatement = analyticsSession.Prepare(CQLGenerator.InsertStatement("daily_active_user_by_user",
                            new List<string> { "company_id", "datestamp", "user_id" }));
                        batchStatement.Add(preparedStatement.Bind(companyId, yesterday, userId));
                    }
                    analyticsSession.Execute(batchStatement);

                    preparedStatement = analyticsSession.Prepare(CQLGenerator.InsertStatement("daily_active_user",
                        new List<string> { "company_id", "datestamp", "number_active_users", "number_total_users", "number_os_ios", "number_os_android", "total_time_taken_min" }));
                    batchStatement.Add(preparedStatement.Bind(companyId, yesterday, numberOfActiveUsers, totalNumberOfUsers, numberOfIOS, numberOfAndroid, totalTimeTakenInMin));

                    foreach (Dictionary<string, object> timeFrame in timeFrameList)
                    {
                        DateTime timestamp = (DateTime)timeFrame["dateTimestamp"];
                        int numberOfActiveUserForTimeframe = (int)timeFrame["active"];

                        preparedStatement = analyticsSession.Prepare(CQLGenerator.InsertStatement("daily_time_period",
                            new List<string> { "company_id", "datestamp", "timestamp", "number_active_user" }));
                        batchStatement.Add(preparedStatement.Bind(companyId, yesterday, timestamp, numberOfActiveUserForTimeframe));
                    }

                    analyticsSession.Execute(batchStatement);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public AnalyticsSelectBasicResponse SelectBasicReport(string adminUserId, string companyId)
        {
            AnalyticsSelectBasicResponse response = new AnalyticsSelectBasicResponse();
            response.BasicDau = new BasicDau();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                response.BasicDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.LastThirtyDays, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private BasicDau SelectBasicDau(string companyId, int dauComparableTimeFrame, ISession analyticSession, Row firstRow = null)
        {
            BasicDau basicDau = new BasicDau();

            try
            {
                PreparedStatement preparedStatement = null;

                if (firstRow == null)
                {
                    preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("daily_active_user",
                        new List<string>(), new List<string> { "company_id" }, 1));
                    firstRow = analyticSession.Execute(preparedStatement.Bind(companyId)).FirstOrDefault();
                }

                if (firstRow != null)
                {
                    DateTime currentDate = firstRow.GetValue<DateTime>("datestamp");
                    currentDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                    currentDate = currentDate.AddSeconds(-currentDate.Second);

                    int totalNumberOfUsers = firstRow.GetValue<int>("number_total_users");
                    int numberOfDaysDifference = 0;

                    if (dauComparableTimeFrame == (int)DauComparableTimeFrame.Yesterday)
                    {
                        numberOfDaysDifference = 1;
                    }
                    else if (dauComparableTimeFrame == (int)DauComparableTimeFrame.LastSevenDays)
                    {
                        numberOfDaysDifference = 7;
                    }
                    else if (dauComparableTimeFrame == (int)DauComparableTimeFrame.LastThirtyDays)
                    {
                        numberOfDaysDifference = 30;
                    }

                    // Last x days
                    DateTime previousDate = currentDate.AddDays(-numberOfDaysDifference);
                    preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("daily_active_user_by_user",
                        null, new List<string> { "company_id" }, "datestamp", CQLGenerator.Comparison.GreaterThan, "datestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                    RowSet dauByUserRowSet = analyticSession.Execute(preparedStatement.Bind(companyId, previousDate, currentDate));

                    int previousActiveSum = 0;
                    List<string> userIdList = new List<string>();

                    foreach (Row dauByUserRow in dauByUserRowSet)
                    {
                        string userId = dauByUserRow.GetValue<string>("user_id");
                        if (!userIdList.Contains(userId))
                        {
                            userIdList.Add(userId);
                            previousActiveSum++;
                        }
                    }

                    // Last 2x days
                    DateTime lastPreviousDate = previousDate.AddDays(-numberOfDaysDifference);
                    preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("daily_active_user_by_user",
                        null, new List<string> { "company_id" }, "datestamp", CQLGenerator.Comparison.GreaterThan, "datestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                    dauByUserRowSet = analyticSession.Execute(preparedStatement.Bind(companyId, lastPreviousDate, previousDate));

                    int lastPreviousActiveSum = 0;
                    userIdList = new List<string>();

                    foreach (Row dauByUserRow in dauByUserRowSet)
                    {
                        string userId = dauByUserRow.GetValue<string>("user_id");
                        if (!userIdList.Contains(userId))
                        {
                            userIdList.Add(userId);
                            lastPreviousActiveSum++;
                        }
                    }

                    basicDau.CurrentDate = currentDate;
                    basicDau.TotalCurrentUsers = totalNumberOfUsers;

                    double difference = 0;
                    double percentage = 0;

                    difference = previousActiveSum - lastPreviousActiveSum;

                    if (Math.Abs(difference) > 0 && (previousActiveSum > 0 || lastPreviousActiveSum > 0))
                    {
                        if (previousActiveSum > lastPreviousActiveSum)
                        {
                            percentage = (difference / previousActiveSum) * 100;
                        }
                        else
                        {
                            percentage = (difference / lastPreviousActiveSum) * 100;
                        }

                    }


                    basicDau.DifferenceInNumber = (int)difference;
                    basicDau.DifferenceInPercentage = percentage;
                    basicDau.TotalUniqueActiveUsers = previousActiveSum;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return basicDau;
        }

        public AnalyticsSelectDetailDauResponse SelectDau(string adminUserId, string companyId, int dauType, int timeActivityFrameType)
        {
            AnalyticsSelectDetailDauResponse response = new AnalyticsSelectDetailDauResponse();
            response.DetailDau = new DetailDau();
            response.DauChart = new DauChart();
            response.TimeActivities = new List<TimeActivity>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession analyticSession = cm.getAnalyticSession();
                ISession mainSession = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(adminUserId, companyId, mainSession);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (dauType == (int)DauType.LastThirtyDays)
                {
                    response.DetailDau = SelectDetailDauForThirtyDays(companyId, analyticSession);
                    response.DauChart.Date = "Last 30 days";
                    response.DauChart.AverageTimeSpentPerUser = response.DetailDau.AverageTimeSpentPerUser;
                    response.DauChart.TotalCurrentUsers = response.DetailDau.LastThirtyDaysDau.TotalCurrentUsers;
                    response.DauChart.TotalUniqueActiveUsers = response.DetailDau.LastThirtyDaysDau.TotalUniqueActiveUsers;
                    response.DauChart.TotalHours = response.DetailDau.TotalTimeSpent;
                    response.DauChart.AttendanceRatePercentage = (int)(response.DauChart.TotalUniqueActiveUsers * 100.00 / (response.DauChart.TotalUniqueActiveUsers + response.DauChart.TotalCurrentUsers));
                }

                //response.TimeActivities = SelectTimeActivityForOneWeek(companyId, timeActivityFrameType, analyticSession);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private List<TimeActivity> SelectTimeActivityForOneWeek(string companyId, int timeActivityFrame, ISession analyticSession)
        {
            List<TimeActivity> timeActivities = new List<TimeActivity>();

            try
            {
                PreparedStatement preparedStatement = null;
                preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("daily_time_period",
                    new List<string>(), new List<string> { "company_id" }, 1));
                Row firstRow = analyticSession.Execute(preparedStatement.Bind(companyId)).FirstOrDefault();

                if (firstRow != null)
                {
                    int numberOfDays = 7;

                    DateTime currentDate = firstRow.GetValue<DateTime>("datestamp");
                    currentDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                    currentDate = currentDate.AddSeconds(-currentDate.Second);

                    int incremental = 0;
                    int totalMinutes = 60 * 24;
                    if (timeActivityFrame == (int)TimeActivityBaseType.OneMinute)
                    {
                        incremental = 1;
                    }
                    else if (timeActivityFrame == (int)TimeActivityBaseType.FifteenMinute)
                    {
                        incremental = 15;
                    }
                    else if (timeActivityFrame == (int)TimeActivityBaseType.ThiryMinute)
                    {
                        incremental = 30;
                    }
                    else if (timeActivityFrame == (int)TimeActivityBaseType.OneHour)
                    {
                        incremental = 60;
                    }

                    for (int addedMinutes = 0; addedMinutes <= totalMinutes - 1; addedMinutes += incremental)
                    {
                        int totalUsers = 0;
                        for (int day = 7; day >= 0; day--)
                        {
                            DateTime pastDate = currentDate.AddDays(-day);
                            pastDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                            pastDate = currentDate.AddSeconds(-currentDate.Second);

                            DateTime pastDateTime = pastDate.AddMinutes(addedMinutes);

                            preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("daily_time_period",
                                null, new List<string> { "company_id", "datestamp" }, "timestamp", CQLGenerator.Comparison.LessThanOrEquals, 1));
                            Row timeFrameRow = analyticSession.Execute(preparedStatement.Bind(companyId, pastDate, pastDateTime)).FirstOrDefault();

                            if (timeFrameRow != null)
                            {
                                int numberOfActiveUsersPerDay = timeFrameRow.GetValue<int>("number_active_user");
                                totalUsers += numberOfActiveUsersPerDay;
                            }
                        }

                        TimeSpan span = TimeSpan.FromMinutes(addedMinutes);

                        TimeActivity timeActivity = new TimeActivity
                        {
                            Time = string.Format("{0}:{1}", (int)span.TotalHours, span.Minutes),
                            Minutes = addedMinutes,
                            TotalActiveUsers = totalUsers / numberOfDays
                        };


                        timeActivities.Add(timeActivity);

                        // To include 23:59
                        if (timeActivityFrame != (int)TimeActivityBaseType.OneMinute && addedMinutes + incremental > totalMinutes - 1)
                        {
                            totalUsers = 0;
                            for (int day = 7; day >= 0; day--)
                            {
                                DateTime pastDate = currentDate.AddDays(-day);
                                pastDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                                pastDate = currentDate.AddSeconds(-currentDate.Second);

                                DateTime pastDateTime = pastDate.AddMinutes(totalMinutes - 1);

                                preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("daily_time_period",
                                    null, new List<string> { "company_id", "datestamp" }, "timestamp", CQLGenerator.Comparison.LessThanOrEquals, 1));
                                Row timeFrameRow = analyticSession.Execute(preparedStatement.Bind(companyId, pastDate, pastDateTime)).FirstOrDefault();

                                if (timeFrameRow != null)
                                {
                                    int numberOfActiveUsersPerDay = timeFrameRow.GetValue<int>("number_active_user");
                                    totalUsers += numberOfActiveUsersPerDay;
                                }
                            }

                            span = TimeSpan.FromMinutes(totalMinutes - 1);

                            timeActivity = new TimeActivity
                            {
                                Time = string.Format("{0}:{1}", (int)span.TotalHours, span.Minutes),
                                Minutes = totalMinutes - 1,
                                TotalActiveUsers = totalUsers / numberOfDays
                            };

                            timeActivities.Add(timeActivity);
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return timeActivities;
        }

        private DetailDau SelectDetailDauForThirtyDays(string companyId, ISession analyticSession)
        {
            DetailDau detailDau = new DetailDau();
            detailDau.DauColumns = new List<DauColumn>();
            detailDau.CurrentDateDau = new BasicDau();
            detailDau.LastSevenDaysDau = new BasicDau();
            detailDau.LastThirtyDaysDau = new BasicDau();
            try
            {
                PreparedStatement preparedStatement = null;
                preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("daily_active_user",
                    new List<string>(), new List<string> { "company_id" }, 1));
                Row firstRow = analyticSession.Execute(preparedStatement.Bind(companyId)).FirstOrDefault();

                if (firstRow != null)
                {
                    int numberOfDaysDifference = 30;
                    DateTime currentDate = firstRow.GetValue<DateTime>("datestamp");
                    currentDate = currentDate.AddMilliseconds(-currentDate.Millisecond);
                    currentDate = currentDate.AddSeconds(-currentDate.Second);

                    // Group detail
                    DateTime previousDate = currentDate.AddDays(-numberOfDaysDifference);
                    preparedStatement = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("daily_active_user",
                        null, new List<string> { "company_id" }, "datestamp", CQLGenerator.Comparison.GreaterThan, "datestamp", CQLGenerator.Comparison.LessThanOrEquals, 0));
                    RowSet dauRowSet = analyticSession.Execute(preparedStatement.Bind(companyId, previousDate, currentDate));

                    double totalTimeTakenMin = 0;
                    double totalAverageTimeTakenMin = 0;

                    DateTime endDatestamp = DateTime.UtcNow;
                    bool isFetchDevice = false;

                    foreach (Row dauRow in dauRowSet)
                    {
                        double timeTakenMin = dauRow.GetValue<double>("total_time_taken_min");
                        totalTimeTakenMin += timeTakenMin;

                        DateTime datestamp = dauRow.GetValue<DateTime>("datestamp");
                        datestamp = datestamp.AddMilliseconds(-datestamp.Millisecond);
                        datestamp = datestamp.AddSeconds(-datestamp.Second);

                        DauColumn column = new DauColumn();
                        int numberOfActiveUsers = dauRow.GetValue<int>("number_active_users");

                        if (!isFetchDevice)
                        {
                            detailDau.NumberOfIOS = dauRow.GetValue<int>("number_os_ios");
                            detailDau.NumberOfAndroid = dauRow.GetValue<int>("number_os_android");

                            detailDau.PercentageOfIOS = 0;
                            detailDau.PercentageOfAndroid = 0;

                            if (detailDau.NumberOfIOS + detailDau.NumberOfAndroid > 0)
                            {
                                detailDau.PercentageOfIOS = (int)(detailDau.NumberOfIOS * 100.00 / (detailDau.NumberOfIOS + detailDau.NumberOfAndroid));
                                detailDau.PercentageOfAndroid = 100 - detailDau.PercentageOfIOS;
                            }

                            isFetchDevice = true;
                            column.XAxisTitle = datestamp.ToString("MMMM");
                        }

                        double averageTimeTakenMin = 0.0;
                        column.AverageHourSpent = 0;
                        if (timeTakenMin > 0 && numberOfActiveUsers > 0)
                        {
                            averageTimeTakenMin = timeTakenMin / numberOfActiveUsers;
                            column.AverageHourSpent = (int)Math.Ceiling(averageTimeTakenMin / 60);
                        }

                        totalAverageTimeTakenMin += averageTimeTakenMin;

                        column.XAxisDate = datestamp.Date.ToString();
                        column.XAxisDay = datestamp.Day.ToString();
                        column.XAxisMonth = datestamp.Month.ToString();
                        column.XAxisYear = datestamp.Year.ToString();
                        column.NumberOfActiveUsers = numberOfActiveUsers;

                        if (column.XAxisDay.Equals("1"))
                        {
                            column.XAxisTitle = datestamp.ToString("MMMM");
                        }

                        detailDau.DauColumns.Add(column);
                        endDatestamp = datestamp;
                        numberOfDaysDifference--;
                    }

                    for (int index = 1; index < numberOfDaysDifference; index++)
                    {
                        endDatestamp = endDatestamp.AddDays(-1);
                        DauColumn column = new DauColumn();
                        column.XAxisDate = endDatestamp.Date.ToString();
                        column.XAxisDay = endDatestamp.Day.ToString();
                        column.XAxisMonth = endDatestamp.Month.ToString();
                        column.XAxisYear = endDatestamp.Year.ToString();
                        column.NumberOfActiveUsers = 0;
                        column.AverageHourSpent = 0;

                        detailDau.DauColumns.Add(column);
                    }


                    // Non graph detail
                    detailDau.CurrentDateDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.Yesterday, analyticSession, firstRow);
                    detailDau.LastSevenDaysDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.LastSevenDays, analyticSession, firstRow);
                    detailDau.LastThirtyDaysDau = SelectBasicDau(companyId, (int)DauComparableTimeFrame.LastThirtyDays, analyticSession, firstRow);

                    // Fomulating average time spent per user
                    //double totalAverageTimeTakenMinPerDay = totalAverageTimeTakenMin / 30;
                    double totalAverageTimeTakenMinPerDay = totalTimeTakenMin / detailDau.LastThirtyDaysDau.TotalUniqueActiveUsers;
                    detailDau.AverageTimeSpentPerUser = string.Format("{0}hrs {1}mins", (int)totalAverageTimeTakenMinPerDay / 60, (int)totalAverageTimeTakenMinPerDay % 60);

                    detailDau.TotalTimeSpent = string.Format("{0} man-hours", (int)totalTimeTakenMin / 60);

                    detailDau.CurrentDate = detailDau.CurrentDateDau.CurrentDate;

                    // Current number of users
                    detailDau.TotalCurrentUsers = detailDau.CurrentDateDau.TotalCurrentUsers;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return detailDau;
        }

        public int SelectCompletedNumberOfUsersForRSTopic(string topicId, ISession analyticSession)
        {
            int numberOfUsers = 0;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_completion_by_topic_timestamp", new List<string> { "topic_id" }));
                numberOfUsers = (int)analyticSession.Execute(ps.Bind(topicId)).FirstOrDefault().GetValue<long>("count");
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return numberOfUsers;
        }

        public bool CheckCompleted(string topicId, string requesterUserId, ISession analyticSession)
        {
            bool isCompleted = false;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_completion_by_user_timestamp", new List<string>(), new List<string> { "completed_by_user_id", "topic_id" }));
                Row completedRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId)).FirstOrDefault();

                if (completedRow != null)
                {
                    isCompleted = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isCompleted;
        }

        public void AnswerRSCard(string answeredByUserId,
                                 string topicId,
                                 string cardId,
                                 int cardType,
                                 bool hasCustomAnswer,
                                 bool isCustom,
                                 List<string> optionIds,
                                 RSOption newCustomAnswer,
                                 RSOption previousCustomAnswer,
                                 int rangeSelected,
                                 DateTime currentTime,
                                 ISession analyticSession)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                BatchStatement updateBatch = new BatchStatement();
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                // Check if number range
                if (cardType == (int)RSCard.RSCardType.NumberRange)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_range_answered_by_user",
                       new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                    Row rangeRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                    if (rangeRow != null)
                    {
                        int previousRangeSelected = rangeRow.GetValue<int>("range_value");

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_range_answered_by_user", new List<string> { "card_id", "range_value", "answered_by_user_id" }));
                        deleteBatch.Add(ps.Bind(cardId, previousRangeSelected, answeredByUserId));

                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_range_answered_by_card", new List<string> { "card_id", "range_value", "answered_by_user_id" }));
                        deleteBatch.Add(ps.Bind(cardId, previousRangeSelected, answeredByUserId));
                    }

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_range_answered_by_user", new List<string> { "card_id", "range_value", "answered_by_user_id", "answered_on_timestamp" }));
                    updateBatch.Add(ps.Bind(cardId, rangeSelected, answeredByUserId, currentTime));
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_range_answered_by_card", new List<string> { "card_id", "range_value", "answered_by_user_id", "answered_on_timestamp" }));
                    updateBatch.Add(ps.Bind(cardId, rangeSelected, answeredByUserId, currentTime));
                }
                else
                {
                    // Text, Single/Mutiple option, Single/Mutiple option with custom answer
                    // isCustom = text, Single/Mutiple option with custom answer
                    if (isCustom)
                    {
                        if (hasCustomAnswer)
                        {
                            // Only Single/Mutiple option has custom answer
                            // Remove previously answered options
                            RSOptionRemovePreviouslyOptionAnswerResponse optionAnswerResponse = RemoveOptionAnswer(analyticSession, answeredByUserId, cardId);
                            if (optionAnswerResponse.Success)
                            {
                                foreach (BoundStatement bs in optionAnswerResponse.deleteStatements)
                                {
                                    deleteBatch.Add(bs);
                                }

                                foreach (BoundStatement bs in optionAnswerResponse.updateStatements)
                                {
                                    updateBatch.Add(bs);
                                }
                            }
                        }

                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_card",
                            new List<string>(), new List<string> { "card_id", "option_id", "created_by_user_id" }));
                        Row customOptionRow = analyticSession.Execute(ps.Bind(cardId, newCustomAnswer.OptionId, answeredByUserId)).FirstOrDefault();

                        if (customOptionRow != null)
                        {
                            if (!string.IsNullOrEmpty(previousCustomAnswer.Content))
                            {
                                // Remove previously answer custom answer
                                RSOptionRemovePreviouslyCustomAnswerResponse customAnswerResponse = RemoveCustomAnswer(analyticSession, answeredByUserId, cardId, previousCustomAnswer.Content);
                                if (customAnswerResponse.Success)
                                {
                                    foreach (BoundStatement bs in customAnswerResponse.deleteStatements)
                                    {
                                        deleteBatch.Add(bs);
                                    }

                                    foreach (BoundStatement bs in customAnswerResponse.updateStatements)
                                    {
                                        updateBatch.Add(bs);
                                    }
                                }
                            }

                        }
                        else
                        {
                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_created_by_user", new List<string> { "card_id", "option_id", "created_by_user_id", "created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, newCustomAnswer.OptionId, answeredByUserId, currentTime));
                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_created_by_card", new List<string> { "card_id", "option_id", "created_by_user_id", "created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, newCustomAnswer.OptionId, answeredByUserId, currentTime));
                        }

                        string lowerCustomAnswer = newCustomAnswer.Content.ToLower().Trim();
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer", new List<string>(), new List<string> { "custom_answer", "card_id" }));
                        Row similarRow = analyticSession.Execute(ps.Bind(lowerCustomAnswer, cardId)).FirstOrDefault();

                        string similarId = string.Empty;
                        int number = 0;
                        if (similarRow != null)
                        {
                            similarId = similarRow.GetValue<string>("similar_id");
                            DateTimeOffset lastUpdatedTime = similarRow.GetValue<DateTimeOffset>("created_on_timestamp");
                            number = similarRow.GetValue<int>("number");

                            ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                            deleteBatch.Add(ps.Bind(cardId, number, lastUpdatedTime, lowerCustomAnswer));

                            ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_custom_option_by_answer", new List<string> { "custom_answer", "card_id" }, new List<string> { "number", "created_on_timestamp" }, new List<string>()));
                            updateBatch.Add(ps.Bind(number + 1, currentTime, lowerCustomAnswer, cardId));
                        }
                        else
                        {
                            similarId = UUIDGenerator.GenerateUniqueIDForRSSimilar();

                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_by_answer", new List<string> { "similar_id", "card_id", "custom_answer", "number", "created_on_timestamp" }));
                            updateBatch.Add(ps.Bind(similarId, cardId, lowerCustomAnswer, number + 1, currentTime));
                        }

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                        updateBatch.Add(ps.Bind(cardId, number + 1, currentTime, lowerCustomAnswer));

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_by_similar", new List<string> { "similar_id", "option_id", "created_by_user_id", "created_on_timestamp" }));
                        updateBatch.Add(ps.Bind(similarId, newCustomAnswer.OptionId, answeredByUserId, currentTime));
                    }

                    //Single/Mutiple option
                    else
                    {
                        // Delete previously selected options
                        RSOptionRemovePreviouslyOptionAnswerResponse optionAnswerResponse = RemoveOptionAnswer(analyticSession, answeredByUserId, cardId);
                        if (optionAnswerResponse.Success)
                        {
                            foreach (BoundStatement bs in optionAnswerResponse.deleteStatements)
                            {
                                deleteBatch.Add(bs);
                            }

                            foreach (BoundStatement bs in optionAnswerResponse.updateStatements)
                            {
                                updateBatch.Add(bs);
                            }
                        }

                        // Delete previously custom answer
                        if (!string.IsNullOrEmpty(previousCustomAnswer.Content))
                        {
                            RSOptionRemovePreviouslyCustomAnswerResponse customAnswerResponse = RemoveCustomAnswer(analyticSession, answeredByUserId, cardId, previousCustomAnswer.Content);
                            if (customAnswerResponse.Success)
                            {
                                foreach (BoundStatement bs in customAnswerResponse.deleteStatements)
                                {
                                    deleteBatch.Add(bs);
                                }

                                foreach (BoundStatement bs in customAnswerResponse.updateStatements)
                                {
                                    updateBatch.Add(bs);
                                }

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_user", new List<string> { "created_by_user_id", "card_id" }));
                                deleteBatch.Add(ps.Bind(answeredByUserId, cardId));

                                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_created_by_card", new List<string> { "created_by_user_id", "card_id", "option_id" }));
                                deleteBatch.Add(ps.Bind(answeredByUserId, cardId, previousCustomAnswer.OptionId));
                            }
                        }

                        foreach (string optionId in optionIds)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_option_answered_by_user", new List<string> { "card_id", "option_id", "answered_by_user_id", "answered_on_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, optionId, answeredByUserId, currentTime));
                            ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_option_answered_by_card", new List<string> { "card_id", "option_id", "answered_by_user_id", "answered_on_timestamp" }));
                            updateBatch.Add(ps.Bind(cardId, optionId, answeredByUserId, currentTime));
                        }
                    }
                }

                // Update last updated time
                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id" }, new List<string> { "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(currentTime, answeredByUserId, topicId));
                ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id" }, new List<string> { "last_updated_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(currentTime, answeredByUserId, topicId));

                analyticSession.Execute(deleteBatch);
                analyticSession.Execute(updateBatch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public RSOptionRemovePreviouslyOptionAnswerResponse RemoveOptionAnswer(ISession analyticSession, string answeredByUserId, string cardId)
        {
            RSOptionRemovePreviouslyOptionAnswerResponse response = new RSOptionRemovePreviouslyOptionAnswerResponse();
            response.deleteStatements = new List<BoundStatement>();
            response.updateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                // Delete previously selected options
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                RowSet optionAnsweredRowset = analyticSession.Execute(ps.Bind(answeredByUserId, cardId));

                foreach (Row optionAnsweredRow in optionAnsweredRowset)
                {
                    string optionId = optionAnsweredRow.GetValue<string>("option_id");
                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_option_answered_by_card", new List<string> { "answered_by_user_id", "card_id", "option_id" }));
                    response.deleteStatements.Add(ps.Bind(answeredByUserId, cardId, optionId));
                }

                ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_option_answered_by_user", new List<string> { "answered_by_user_id", "card_id" }));
                response.deleteStatements.Add(ps.Bind(answeredByUserId, cardId));
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public RSOptionRemovePreviouslyCustomAnswerResponse RemoveCustomAnswer(ISession analyticSession, string answeredByUserId, string cardId, string previousCustomAnswer)
        {
            RSOptionRemovePreviouslyCustomAnswerResponse response = new RSOptionRemovePreviouslyCustomAnswerResponse();
            response.deleteStatements = new List<BoundStatement>();
            response.updateStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                // Delete custom answer
                previousCustomAnswer = previousCustomAnswer.ToLower().Trim();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer",
                    new List<string>(), new List<string> { "custom_answer", "card_id" }));
                Row answerRow = analyticSession.Execute(ps.Bind(previousCustomAnswer, cardId)).FirstOrDefault();

                BatchStatement deleteBatch = new BatchStatement();

                if (answerRow != null)
                {
                    string similiarId = answerRow.GetValue<string>("similar_id");
                    int numberOfSimiliar = answerRow.GetValue<int>("number");
                    DateTimeOffset createdDate = answerRow.GetValue<DateTimeOffset>("created_on_timestamp");

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                    deleteBatch.Add(ps.Bind(cardId, numberOfSimiliar, createdDate, previousCustomAnswer));

                    ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_by_similar", new List<string> { "similar_id", "created_on_timestamp", "created_by_user_id" }));
                    deleteBatch.Add(ps.Bind(similiarId, createdDate, answeredByUserId));

                    if (numberOfSimiliar > 1)
                    {
                        numberOfSimiliar--;
                        ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_custom_option_by_answer",
                            new List<string> { "custom_answer", "card_id" }, new List<string> { "number", "created_on_timestamp" }, new List<string>()));
                        response.updateStatements.Add(ps.Bind(numberOfSimiliar, createdDate, previousCustomAnswer, cardId));

                        ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_custom_option_sort_by_number_timestamp", new List<string> { "card_id", "number", "created_on_timestamp", "custom_answer" }));
                        response.updateStatements.Add(ps.Bind(cardId, numberOfSimiliar, createdDate, previousCustomAnswer));
                    }
                    else if (numberOfSimiliar == 1)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.DeleteStatement("rs_custom_option_by_answer", new List<string> { "card_id", "custom_answer" }));
                        deleteBatch.Add(ps.Bind(cardId, previousCustomAnswer));
                    }

                    // Need to execute delete first
                    analyticSession.Execute(deleteBatch);

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateRSBounceActivity(string requesterUserId, string topicId, ISession analyticSession)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                DateTime bouncedDatestamp = DateTime.UtcNow.Date;

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_bounced_by_user_datestamp", new List<string>(), new List<string> { "bounced_by_user_id", "topic_id", "bounced_on_datestamp" }));
                Row bounceRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId, bouncedDatestamp)).FirstOrDefault();

                if (bounceRow == null)
                {
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_bounced_by_user_datestamp", new List<string> { "bounced_by_user_id", "topic_id", "bounced_on_datestamp" }));
                    analyticSession.Execute(ps.Bind(requesterUserId, topicId, bouncedDatestamp));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_bounced_by_topic_datestamp", new List<string> { "bounced_by_user_id", "topic_id", "bounced_on_datestamp" }));
                    analyticSession.Execute(ps.Bind(requesterUserId, topicId, bouncedDatestamp));
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public RSUpdateActivityResponse UpdateRSCompletionActivity(string requesterUserId, string topicId, ISession analyticSession)
        {
            RSUpdateActivityResponse response = new RSUpdateActivityResponse();
            response.Success = false;
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                DateTime currentTime = DateTime.UtcNow;
                BatchStatement updateBatch = new BatchStatement();

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_completion_by_user_timestamp", new List<string>(), new List<string> { "completed_by_user_id", "topic_id" }));
                Row completionRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId)).FirstOrDefault();

                if (completionRow == null)
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id" }, new List<string> { "completed_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, currentTime, requesterUserId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id" }, new List<string> { "completed_on_timestamp", "last_updated_timestamp" }, new List<string>()));
                    updateBatch.Add(ps.Bind(currentTime, currentTime, requesterUserId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_completion_by_topic_timestamp", new List<string> { "topic_id", "completed_on_timestamp", "completed_by_user_id", "completed_on_datestamp" }));
                    updateBatch.Add(ps.Bind(topicId, currentTime, requesterUserId, currentTime.Date));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_completion_by_user_timestamp", new List<string> { "completed_by_user_id", "completed_on_timestamp", "topic_id", "completed_on_datestamp" }));
                    updateBatch.Add(ps.Bind(requesterUserId, currentTime, topicId, currentTime.Date));

                    analyticSession.Execute(updateBatch);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public void UpdateRSStartProgressByUser(string requesterUserId, string topicId, ISession analyticSession)
        {
            try
            {
                BatchStatement batch = new BatchStatement();

                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_progress_by_user", new List<string>(), new List<string> { "user_id", "topic_id" }));
                Row completionRow = analyticSession.Execute(ps.Bind(requesterUserId, topicId)).FirstOrDefault();

                if (completionRow != null)
                {
                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id" }, new List<string> { "is_omitted" }, new List<string>()));
                    batch.Add(ps.Bind(true, requesterUserId, topicId));

                    ps = analyticSession.Prepare(CQLGenerator.UpdateStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id" }, new List<string> { "is_omitted" }, new List<string>()));
                    batch.Add(ps.Bind(true, requesterUserId, topicId));
                }
                else
                {
                    DateTime currentTime = DateTime.UtcNow;
                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_progress_by_user", new List<string> { "user_id", "topic_id", "start_on_timestamp", "is_omitted" }));
                    batch.Add(ps.Bind(requesterUserId, topicId, currentTime, false));

                    ps = analyticSession.Prepare(CQLGenerator.InsertStatement("rs_progress_by_topic", new List<string> { "user_id", "topic_id", "start_on_timestamp", "is_omitted" }));
                    batch.Add(ps.Bind(requesterUserId, topicId, currentTime, false));
                }

                analyticSession.Execute(batch);
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public int SelectRangeFromCard(string cardId, string answeredByUserId, ISession analyticSession)
        {
            int selectedRange = 0;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_range_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id" }));
                Row rangeRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                if (rangeRow != null)
                {
                    selectedRange = rangeRow.GetValue<int>("range_value");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return selectedRange;
        }

        public RSOption SelectCustomAnswerFromCardByUser(string topicId, string cardId, string answeredByUserId, ISession mainSession, ISession analyticSession)
        {
            RSOption customAnswer = new RSOption();

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_created_by_user", new List<string>(), new List<string> { "created_by_user_id", "card_id" }));
                Row customRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId)).FirstOrDefault();

                if (customRow != null)
                {
                    string optionId = customRow.GetValue<string>("option_id");
                    customAnswer = new RSOption().SelectCustomOption(topicId, cardId, optionId, mainSession);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return customAnswer;
        }

        public bool CheckForSelectedOption(string cardId, string optionId, string answeredByUserId, ISession analyticSession)
        {
            bool isSelected = false;

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_option_answered_by_user", new List<string>(), new List<string> { "answered_by_user_id", "card_id", "option_id" }));
                Row selectedRow = analyticSession.Execute(ps.Bind(answeredByUserId, cardId, optionId)).FirstOrDefault();

                if (selectedRow != null)
                {
                    isSelected = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return isSelected;
        }

        public RSAnalyticOverview SelectRSResultOverview(string topicId, int totalPages, int totalCards, int totalTargetedAudience, double timezoneOffset, ISession analyticSession)
        {
            RSAnalyticOverview overview = new RSAnalyticOverview();
            overview.Summary = new RSSummary();
            overview.PersonnelCompletion = new RSPersonnelCompletion();
            overview.Report = new RSResponderReport();
            overview.Report.ReportList = new List<RSReportPerDay>();

            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                PreparedStatement ps = null;
                int limit = 14;
                DateTime baseTimestamp = DateTime.UtcNow.AddDays(-(limit - 1)).Date;

                // Get number of completion, bounce per day
                for (int day = 0; day < limit; day++)
                {
                    ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("rs_completion_by_topic_timestamp", new List<string>(), new List<string> { "topic_id" }, "completed_on_timestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "completed_on_timestamp", CQLGenerator.Comparison.LessThan, 0));
                    RowSet completionRowset = analyticSession.Execute(ps.Bind(topicId, baseTimestamp, baseTimestamp.AddDays(1)));

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithDateRangeComparison("rs_bounced_by_topic_datestamp", new List<string>(), new List<string> { "topic_id" }, "bounced_on_datestamp", CQLGenerator.Comparison.GreaterThanOrEquals, "bounced_on_datestamp", CQLGenerator.Comparison.LessThan, 0));
                    RowSet bounceRowset = analyticSession.Execute(ps.Bind(topicId, baseTimestamp, baseTimestamp.AddDays(1)));

                    int bounceNumber = bounceRowset.Count();
                    int completionNumber = completionRowset.Count();

                    RSReportPerDay report = new RSReportPerDay
                    {
                        BounceNumber = bounceNumber,
                        CompletionNumber = completionNumber,
                        Datestamp = baseTimestamp.AddHours(timezoneOffset),
                        DatestampString = baseTimestamp.AddHours(timezoneOffset).ToString("dd MMM yyyy")
                    };

                    overview.Report.ReportList.Add(report);
                    baseTimestamp = baseTimestamp.AddDays(1);
                }

                // Get Personnel completion
                overview.PersonnelCompletion = SelectPersonnelCompletion(topicId, totalTargetedAudience, analyticSession);

                // Get data for summary
                overview.Summary = new RSSummary
                {
                    TotalCards = totalCards,
                    TotalPages = totalPages,
                    AverageTimeCompletionInSecond = overview.PersonnelCompletion.CompletionNumber == 0 ? 0 : overview.PersonnelCompletion.TotalTimeTakenInSecond / overview.PersonnelCompletion.CompletionNumber
                };

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return overview;
        }

        private RSPersonnelCompletion SelectPersonnelCompletion(string topicId, int totalTargetedAudience, ISession analyticSession)
        {
            RSPersonnelCompletion personnelCompletion = new RSPersonnelCompletion();

            try
            {
                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_completion_by_topic_timestamp", new List<string> { "topic_id" }));
                Row totalCompletionRow = analyticSession.Execute(ps.Bind(topicId)).FirstOrDefault();

                ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_progress_by_topic", new List<string>(), new List<string> { "topic_id" }));
                RowSet progressRowset = analyticSession.Execute(ps.Bind(topicId));

                int totalIncompletion = 0;
                int totalTimeTaken = 0;

                foreach (Row progressRow in progressRowset)
                {
                    if (progressRow["completed_on_timestamp"] == null)
                    {
                        totalIncompletion++;
                    }
                    else
                    {
                        bool isOmitted = progressRow.GetValue<bool>("is_omitted");

                        if (!isOmitted)
                        {
                            DateTimeOffset completedTimestamp = progressRow.GetValue<DateTimeOffset>("completed_on_timestamp");
                            DateTimeOffset startTimestamp = progressRow.GetValue<DateTimeOffset>("start_on_timestamp");

                            totalTimeTaken += (int)completedTimestamp.Subtract(startTimestamp).TotalSeconds;
                        }

                    }
                }

                int totalCompletion = (int)totalCompletionRow.GetValue<long>("count");
                int totalAbsent = totalTargetedAudience - totalCompletion - totalIncompletion;

                int percentageCompletion = (int)((double)totalCompletion / totalTargetedAudience * 100);
                int percentageIncompletion = (int)((double)totalIncompletion / totalTargetedAudience * 100);
                int percentageAbsent = 100 - percentageCompletion - percentageIncompletion;

                personnelCompletion = new RSPersonnelCompletion
                {
                    CompletionNumber = totalCompletion,
                    IncompletionNumber = totalIncompletion,
                    AbsentNumber = totalAbsent,

                    CompletionPercentage = percentageCompletion,
                    IncompletionPercentage = percentageIncompletion,
                    AbsentPercentage = percentageAbsent,

                    TotalTimeTakenInSecond = totalTimeTaken
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return personnelCompletion;
        }

        public void SelectRSCardResult(RSCard card, ISession analyticSession, ISession mainSession)
        {
            try
            {
                if (analyticSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = null;

                int totalResponses = 0;

                if (card.Type == (int)RSCard.RSCardType.NumberRange)
                {
                    List<RSOption> rangeOptions = new List<RSOption>();

                    // Get total responses
                    ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_range_answered_by_card",
                            new List<string> { "card_id" }));
                    Row totalCountRow = analyticSession.Execute(ps.Bind(card.CardId)).FirstOrDefault();
                    totalResponses = (int)totalCountRow.GetValue<long>("count");

                    // Convert range to option
                    // Get count per range
                    for (int index = card.MaxRange; index >= card.MinRange; index--)
                    {
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_range_answered_by_card",
                            new List<string> { "card_id", "range_value" }));
                        Row countRow = analyticSession.Execute(ps.Bind(card.CardId, index)).FirstOrDefault();
                        int count = (int)countRow.GetValue<long>("count");
                        RSOption option = new RSOption
                        {
                            OptionId = index.ToString(),
                            Content = index.ToString(),
                            NumberOfSelection = count,
                            PercentageOfSelection = totalResponses == 0 ? 0 : (int)((double)count / totalResponses * 100)
                        };

                        rangeOptions.Add(option);
                    }

                    card.TotalResponses = totalResponses;
                    card.Options = rangeOptions;
                }
                else
                {
                    if (card.Type == (int)RSCard.RSCardType.MultiChoice || card.Type == (int)RSCard.RSCardType.SelectOne || card.Type == (int)RSCard.RSCardType.DropList)
                    {
                        // Get total responses
                        ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_option_answered_by_card",
                                new List<string> { "card_id" }));
                        Row totalCountRow = analyticSession.Execute(ps.Bind(card.CardId)).FirstOrDefault();
                        totalResponses = (int)totalCountRow.GetValue<long>("count");

                        // Get count per option
                        foreach (RSOption option in card.Options)
                        {
                            ps = analyticSession.Prepare(CQLGenerator.CountStatement("rs_option_answered_by_card",
                                new List<string> { "card_id", "option_id" }));
                            Row countRow = analyticSession.Execute(ps.Bind(card.CardId, option.OptionId)).FirstOrDefault();
                            int count = (int)countRow.GetValue<long>("count");
                            option.NumberOfSelection = count;
                            option.PercentageOfSelection = totalResponses == 0 ? 0 : (int)((double)count / totalResponses * 100);
                        }

                        card.TotalResponses = totalResponses;
                        card.Options = card.Options.OrderByDescending(option => option.NumberOfSelection).ToList();

                        if (card.HasCustomAnswer)
                        {
                            // Get count of custom answers
                            ps = mainSession.Prepare(CQLGenerator.CountStatement("rs_custom_option",
                                new List<string> { "card_id" }));
                            card.TotalResponses += (int)mainSession.Execute(ps.Bind(card.CardId)).FirstOrDefault().GetValue<long>("count");

                            // Take TOP 6 smiliar and then by timestamp
                            SelectSortedCustomAnswer(card, analyticSession);
                        }
                    }
                    else if (card.Type == (int)RSCard.RSCardType.Text)
                    {
                        // Get count of custom answers
                        ps = mainSession.Prepare(CQLGenerator.CountStatement("rs_custom_option",
                                new List<string> { "card_id" }));
                        card.TotalResponses = (int)mainSession.Execute(ps.Bind(card.CardId)).FirstOrDefault().GetValue<long>("count");

                        // Take TOP 6 smiliar and then by timestamp
                        SelectSortedCustomAnswer(card, analyticSession);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        private void SelectSortedCustomAnswer(RSCard card, ISession analyticSession)
        {
            try
            {
                int limit = 6;

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatementWithLimit("rs_custom_option_sort_by_number_timestamp",
                            new List<string>(), new List<string> { "card_id" }, limit));
                RowSet customOptionSortRowset = analyticSession.Execute(ps.Bind(card.CardId));

                card.CustomAnswers = new List<RSOption>();

                int index = 1;
                foreach (Row customOptionSortRow in customOptionSortRowset)
                {
                    int numberOfSelection = customOptionSortRow.GetValue<int>("number");
                    string customAnswer = customOptionSortRow.GetValue<string>("custom_answer");
                    customAnswer = char.ToUpper(customAnswer[0]) + customAnswer.Substring(1);

                    RSOption option = new RSOption
                    {
                        OptionId = index.ToString(),
                        Content = customAnswer,
                        NumberOfSelection = numberOfSelection
                    };

                    card.CustomAnswers.Add(option);
                    index++;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public void SelectRSCustomAnswersResultFromCard(RSCard card, string companyId, ISession analyticSession, ISession mainSession)
        {
            try
            {
                ConnectionManager cm = new ConnectionManager();
                if (analyticSession == null)
                {
                    analyticSession = cm.getAnalyticSession();
                }

                if (mainSession == null)
                {
                    mainSession = cm.getMainSession();
                }

                PreparedStatement ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_sort_by_number_timestamp",
                            new List<string>(), new List<string> { "card_id" }));
                RowSet customOptionSortRowset = analyticSession.Execute(ps.Bind(card.CardId));

                card.CustomAnswers = new List<RSOption>();

                User userManager = new User();

                int numberOfResponses = 0;
                foreach (Row customOptionSortRow in customOptionSortRowset)
                {
                    string lowerCustomAnswer = customOptionSortRow.GetValue<string>("custom_answer");

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_answer",
                            new List<string>(), new List<string> { "custom_answer", "card_id" }));
                    Row similarRow = analyticSession.Execute(ps.Bind(lowerCustomAnswer, card.CardId)).FirstOrDefault();

                    if (similarRow != null)
                    {
                        string similarId = similarRow.GetValue<string>("similar_id");
                        ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option_by_similar",
                           new List<string>(), new List<string> { "similar_id" }));
                        RowSet similarOptionRowset = analyticSession.Execute(ps.Bind(similarId));

                        foreach (Row similarOptionRow in similarOptionRowset)
                        {
                            string optionId = similarOptionRow.GetValue<string>("option_id");
                            string answeredByUserId = similarOptionRow.GetValue<string>("created_by_user_id");

                            ps = mainSession.Prepare(CQLGenerator.SelectStatement("rs_custom_option",
                                new List<string>(), new List<string> { "card_id", "id" }));
                            Row customOptionRow = mainSession.Execute(ps.Bind(card.CardId, optionId)).FirstOrDefault();
                            string customAnswer = customOptionRow.GetValue<string>("content");

                            RSOption option = new RSOption
                            {
                                OptionId = optionId,
                                Content = customAnswer,
                                NumberOfSelection = 0,
                                AnsweredByUser = userManager.SelectUserBasic(answeredByUserId, companyId, false, mainSession, null, true).User
                            };

                            card.CustomAnswers.Add(option);
                            numberOfResponses++;
                        }
                    }
                }

                card.TotalResponses = numberOfResponses;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }

        public AnalyticSelectRSResponderReportResponse SelectRSRespondersReport(string topicId, List<User> targetedAudience, double timezoneOffset, ISession mainSession, ISession analyticSession)
        {
            AnalyticSelectRSResponderReportResponse response = new AnalyticSelectRSResponderReportResponse();
            response.PersonnelCompletion = new RSPersonnelCompletion();
            response.Reports = new List<RSReportPerUser>();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;

                response.PersonnelCompletion = SelectPersonnelCompletion(topicId, targetedAudience.Count(), analyticSession);

                foreach (User targetedUser in targetedAudience)
                {
                    bool isCompleted = false;

                    string userId = targetedUser.UserId;

                    ps = analyticSession.Prepare(CQLGenerator.SelectStatement("rs_progress_by_user",
                          new List<string>(), new List<string> { "user_id", "topic_id" }));
                    Row progressRow = analyticSession.Execute(ps.Bind(userId, topicId)).FirstOrDefault();

                    RSReportPerUser report = new RSReportPerUser();
                    report.User = targetedUser;

                    if (progressRow != null)
                    {
                        DateTime? lastUpdatedTimestamp = progressRow.GetValue<DateTime?>("last_updated_timestamp");
                        string lastUpdatedTimestampString = string.Empty;

                        if(lastUpdatedTimestamp != null)
                        {
                            lastUpdatedTimestamp = lastUpdatedTimestamp.Value.AddHours(timezoneOffset);
                            lastUpdatedTimestampString = lastUpdatedTimestamp.Value.ToString("dd MMM yyyy");
                        }

                        if (progressRow["completed_on_timestamp"] != null)
                        {
                            isCompleted = true;
                        }

                        report = new RSReportPerUser
                        {
                            IsCompleted = isCompleted,
                            LastUpdateTimestamp = lastUpdatedTimestamp,
                            LastUpdateTimestampString = lastUpdatedTimestampString,
                            User = targetedUser
                        };
                    }

                    response.Reports.Add(report);
                }


                response.Reports = response.Reports.OrderByDescending(report => report.IsCompleted).ThenBy(report => report.LastUpdateTimestamp).ToList();
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        [Serializable]
        public class DetailDau
        {
            [DataMember]
            public int DauMetricType { get; set; }
            [DataMember]
            public List<DauColumn> DauColumns { get; set; }
            [DataMember]
            public DateTime CurrentDate { get; set; }
            [DataMember]
            public BasicDau CurrentDateDau { get; set; }
            [DataMember]
            public BasicDau LastSevenDaysDau { get; set; }
            [DataMember]
            public BasicDau LastThirtyDaysDau { get; set; }
            [DataMember]
            public int TotalCurrentUsers { get; set; }
            [DataMember]
            public string AverageTimeSpentPerUser { get; set; }
            [DataMember]
            public string TotalTimeSpent { get; set; }
            [DataMember]
            public int NumberOfIOS { get; set; }
            [DataMember]
            public int PercentageOfIOS { get; set; }
            [DataMember]
            public int NumberOfAndroid { get; set; }
            [DataMember]
            public int PercentageOfAndroid { get; set; }
        }

        [Serializable]
        public class DauColumn
        {
            [DataMember]
            public string XAxisDate { get; set; }
            [DataMember]
            public string XAxisDay { get; set; }
            [DataMember]
            public string XAxisMonth { get; set; }
            [DataMember]
            public string XAxisYear { get; set; }
            [DataMember]
            public string XAxisTitle { get; set; }
            [DataMember]
            public int NumberOfActiveUsers { get; set; }
            [DataMember]
            public int AverageHourSpent { get; set; }
        }

        [Serializable]
        public class DauChart
        {
            [DataMember]
            public string Date { get; set; }
            [DataMember]
            public int TotalCurrentUsers { get; set; }
            [DataMember]
            public int TotalUniqueActiveUsers { get; set; }
            [DataMember]
            public int AttendanceRatePercentage { get; set; }
            [DataMember]
            public string TotalHours { get; set; }
            [DataMember]
            public string AverageTimeSpentPerUser { get; set; }
        }

        [Serializable]
        public class TimeActivity
        {
            [DataMember]
            public string Time { get; set; }
            [DataMember]
            public int Minutes { get; set; }
            [DataMember]
            public int TotalActiveUsers { get; set; }
        }

        [Serializable]
        public class BasicDau
        {
            [DataMember]
            public DateTime CurrentDate { get; set; }
            [DataMember]
            public int TotalCurrentUsers { get; set; }
            [DataMember]
            public int TotalUniqueActiveUsers { get; set; }
            [DataMember]
            public int DifferenceInNumber { get; set; }
            [DataMember]
            public double DifferenceInPercentage { get; set; }
        }

        [Serializable]
        public class Exp
        {
            //[DataMember]
            //public int PreviousLevel { get; set; }
            //[DataMember]
            //public int PreviousExp { get; set; }
            [DataMember]
            public int CurrentLevel { get; set; }
            [DataMember]
            public int CurrentExp { get; set; }
            [DataMember]
            public int NextLevel { get; set; }
            [DataMember]
            public int NextExpToReach { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public string Rank { get; set; }
        }

        [Serializable]
        public class GameHistory
        {
            [DataMember]
            public string ChallengeId { get; set; }
            [DataMember]
            public User Opponent { get; set; }
            [DataMember]
            public Topic Topic { get; set; }
            [DataMember]
            public int Outcome { get; set; }
            [DataMember]
            public DateTime CompletedOnTimestamp { get; set; }
        }

        [Serializable]
        public class UserStats
        {
            [DataMember]
            public int WinningPercentage { get; set; }
            [DataMember]
            public int DrawPercentage { get; set; }
            [DataMember]
            public int LosingPercentage { get; set; }
            [DataMember]
            public List<TopicStats> Topics { get; set; }
        }

        [Serializable]
        public class TopicStats
        {
            [DataMember]
            public Topic Topic { get; set; }
            [DataMember]
            public int CompletionPercentage { get; set; }
        }

        [Serializable]
        public class EventLeaderboard
        {
            [DataMember(EmitDefaultValue = false)]
            public User User { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public Department Department { get; set; }

            [DataMember]
            public int Score { get; set; }

            [DataMember]
            public int Rank { get; set; }
        }

        [Serializable]
        public class RSAnalyticOverview
        {
            [DataMember(EmitDefaultValue = false)]
            public RSTopic Topic { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public RSSummary Summary { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public RSPersonnelCompletion PersonnelCompletion { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public RSResponderReport Report { get; set; }
        }

        [Serializable]
        public class RSSummary
        {
            [DataMember(EmitDefaultValue = false)]
            public int TotalPages;

            [DataMember(EmitDefaultValue = false)]
            public int TotalCards { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int TotalDays { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int AverageTimeCompletionInSecond { get; set; }
        }

        [Serializable]
        public class RSPersonnelCompletion
        {
            [DataMember(EmitDefaultValue = false)]
            public int CompletionNumber;

            [DataMember(EmitDefaultValue = false)]
            public int CompletionPercentage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int IncompletionNumber;

            [DataMember(EmitDefaultValue = false)]
            public int IncompletionPercentage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int AbsentNumber;

            [DataMember(EmitDefaultValue = false)]
            public int AbsentPercentage { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int TotalTimeTakenInSecond { get; set; }
        }

        [Serializable]
        public class RSResponderReport
        {
            [DataMember(EmitDefaultValue = false)]
            public List<RSReportPerDay> ReportList;
        }

        [Serializable]
        public class RSReportPerDay
        {
            [DataMember(EmitDefaultValue = false)]
            public DateTime Datestamp;

            [DataMember(EmitDefaultValue = false)]
            public string DatestampString;

            [DataMember(EmitDefaultValue = false)]
            public int CompletionNumber { get; set; }

            [DataMember(EmitDefaultValue = false)]
            public int BounceNumber;
        }

        [Serializable]
        public class RSReportPerUser
        {
            [DataMember(EmitDefaultValue = false)]
            public User User;

            [DataMember(EmitDefaultValue = false)]
            public DateTime? LastUpdateTimestamp;

            [DataMember(EmitDefaultValue = false)]
            public string LastUpdateTimestampString;

            [DataMember(EmitDefaultValue = false)]
            public bool IsCompleted;
        }
    }
}
