﻿using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    [Serializable]
    public class Event
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public const int QUERY_TYPE_BASIC = 1;

        public const int QUERY_TYPE_DETAIL = 2;

        [DataMember(EmitDefaultValue = false)]
        public String EventId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public String Description { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset StartTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset EndTimestamp { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int EventType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NumberOfTopics { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Topic> Topics { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<Department> TargetedDepartments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<User> TargetedUsers { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ParticipantType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int BannerPrivacyType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset BannerVisibleDate { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ScoringType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int CalculationType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int Status { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsDisplayResult { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int NumberOfDaysToDisplayResult { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LeaderboardNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string BackgroundImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<CassandraService.Entity.Analytic.EventLeaderboard> TopEventLeaderboard { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<CassandraService.Entity.Analytic.EventLeaderboard> OutOfRankingLeaderboard { get; set; }

        public enum EventTypeEnum
        {
            OneTopic = 1,
            MultipleTopics = 2,
            AllTopics = 3
        }

        public enum EventStatusEnum
        {
            All = 0,
            Live = 1,
            Completed = 2,
            Upcoming = 3,
            Suspended = -1,
            Ignored = -999
        }

        public enum CalculationTypeEnum
        {
            Accumulative = 1,
            Unique = 2,

            TotalAccumulative = 3,
            TotalUnique = 4,
            AverageAccumulative = 5,
            AverageUnique = 6
        }

        public enum ParticipantTypeEnum
        {
            Everyone = 1,
            Department = 2,
            Personnel = 3
        }

        public enum BannerPrivacyTypeEnum
        {
            Everyone = 1,
            Participant = 2
        }

        public enum ScoringTypeEnum
        {
            TopScorer = 1,
            TopDepartment = 2
        }

        public enum EventValidStatusEnum
        {
            Valid = 1,
            Hidden = -1,
            Suspended = -2,
            Deleted = -999
        }


        public EventCreateResponse Create(string adminUserId,
                                          string companyId,
                                          string title,
                                          string description,
                                          DateTime startTimestamp,
                                          DateTime endTimestamp,
                                          int eventType,
                                          int participantType,
                                          int bannerPrivacyType,
                                          DateTime bannerVisibleDate,
                                          int scoringType,
                                          int calculationType,
                                          bool isDisplayResult,
                                          int extensionDaysForResult,
                                          int numberOfLeaderboard,
                                          string backgroundImageUrl,
                                          List<string> topicIds,
                                          List<string> departmentIds = null,
                                          List<string> userIds = null,
                                          string eventId = null,
                                          ISession session = null)
        {
            EventCreateResponse response = new EventCreateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                BatchStatement insertBatch = new BatchStatement();
                PreparedStatement ps = null;

                if (session == null)
                {
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                }

                DateTime currentTime = DateTime.UtcNow;
                startTimestamp = startTimestamp.ToUniversalTime();
                endTimestamp = endTimestamp.ToUniversalTime();
                bannerVisibleDate = bannerVisibleDate.ToUniversalTime();

                if (startTimestamp >= endTimestamp)
                {
                    Log.Error(ErrorMessage.EventEndDateEarlierThanStartDate);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventEndDateEarlierThanStartDate);
                    response.ErrorMessage = ErrorMessage.EventEndDateEarlierThanStartDate;
                    return response;
                }

                if (currentTime >= endTimestamp)
                {
                    Log.Error(ErrorMessage.EventEndDateEarlierThanToday);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventEndDateEarlierThanToday);
                    response.ErrorMessage = ErrorMessage.EventEndDateEarlierThanToday;
                    return response;
                }

                if (startTimestamp < bannerVisibleDate)
                {
                    Log.Error(ErrorMessage.EventBannerDateLaterThanStartDate);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventBannerDateLaterThanStartDate);
                    response.ErrorMessage = ErrorMessage.EventBannerDateLaterThanStartDate;
                    return response;
                }

                if (string.IsNullOrEmpty(title))
                {
                    Log.Error(ErrorMessage.EventTitleMissing);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventTitleMissing);
                    response.ErrorMessage = ErrorMessage.EventTitleMissing;
                    return response;
                }

                bool isUpdate = false;

                if (string.IsNullOrEmpty(eventId))
                {
                    eventId = UUIDGenerator.GenerateUniqueIDForEvent();
                }
                else
                {
                    isUpdate = true;
                }

                // Create topic
                if (eventType != (int)EventTypeEnum.AllTopics)
                {
                    if (topicIds == null || topicIds != null && topicIds.Count == 0)
                    {
                        Log.Error(ErrorMessage.EventTopicMissing);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventTopicMissing);
                        response.ErrorMessage = ErrorMessage.EventTopicMissing;
                        return response;
                    }

                    if (eventType == (int)EventTypeEnum.OneTopic && topicIds.Count > 1)
                    {
                        topicIds.RemoveRange(1, topicIds.Count - 1);
                    }

                    List<string> selectedValidTopicIds = new List<string>();
                    foreach (string topicId in topicIds)
                    {
                        Topic selectedTopic = new Topic().SelectTopicBasic(topicId, adminUserId, companyId, null, null, session).Topic;

                        if (selectedTopic != null)
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("event_by_topic",
                                new List<string> { "topic_id", "event_id" }));
                            insertBatch.Add(ps.Bind(topicId, eventId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("topic_by_event",
                                new List<string> { "topic_id", "event_id" }));
                            insertBatch.Add(ps.Bind(topicId, eventId));

                            selectedValidTopicIds.Add(topicId);
                        }
                    }

                    if (selectedValidTopicIds.Count == 0)
                    {
                        Log.Error(ErrorMessage.EventTopicMissing);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventTopicMissing);
                        response.ErrorMessage = ErrorMessage.EventTopicMissing;
                        return response;
                    }
                }

                // Create participants
                List<string> participantIds = new List<string>();
                User userManager = new User();

                if (participantType == (int)ParticipantTypeEnum.Department)
                {
                    if (departmentIds == null || departmentIds != null && departmentIds.Count == 0)
                    {
                        Log.Error(string.Format(ErrorMessage.EventMissingDepartment));
                        response.ErrorCode = Int16.Parse(ErrorCode.EventMissingDepartment);
                        response.ErrorMessage = ErrorMessage.EventMissingDepartment;
                        return response;
                    }

                    foreach (string departmentId in departmentIds)
                    {
                        ps = session.Prepare(CQLGenerator.InsertStatement("event_by_department",
                            new List<string> { "department_id", "event_id" }));
                        insertBatch.Add(ps.Bind(departmentId, eventId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("department_by_event",
                            new List<string> { "department_id", "event_id" }));
                        insertBatch.Add(ps.Bind(departmentId, eventId));
                    }

                    participantIds = userManager.SelectAllUserIdsByDepartmentIds(departmentIds, adminUserId, companyId, session);
                }
                else if (participantType == (int)ParticipantTypeEnum.Personnel)
                {
                    if (userIds == null || userIds != null && userIds.Count == 0)
                    {
                        Log.Error(string.Format(ErrorMessage.EventMissingUser));
                        response.ErrorCode = Int16.Parse(ErrorCode.EventMissingUser);
                        response.ErrorMessage = ErrorMessage.EventMissingUser;
                        return response;
                    }

                    foreach (string userId in userIds)
                    {
                        ps = session.Prepare(CQLGenerator.InsertStatement("event_by_user",
                            new List<string> { "user_id", "event_id" }));
                        insertBatch.Add(ps.Bind(userId, eventId));

                        ps = session.Prepare(CQLGenerator.InsertStatement("user_by_event",
                            new List<string> { "user_id", "event_id" }));
                        insertBatch.Add(ps.Bind(userId, eventId));

                        participantIds.Add(userId);
                    }
                }
                else if (participantType == (int)ParticipantTypeEnum.Everyone)
                {
                    List<Department> allDepartments = userManager.SelectAllUsersSortedByDepartment(adminUserId, companyId, session).Departments;

                    foreach (Department selectedDepartment in allDepartments)
                    {
                        foreach (User userInDepartment in selectedDepartment.Users)
                        {
                            if (!participantIds.Contains(userInDepartment.UserId))
                            {
                                participantIds.Add(userInDepartment.UserId);
                            }
                        }
                    }
                }

                if (!isUpdate)
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("event",
                        new List<string> { "company_id", "event_id", "title", "description", "start_timestamp", "end_timestamp", "event_type", "participant_type", "banner_privacy_type", "banner_visible_date", "scoring_type", "calculation_type", "extended_display_days", "is_display_result", "leaderboard_number", "background_image_url", "is_suspended_by_admin", "valid_status", "created_by_user_id", "created_on_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }));
                    insertBatch.Add(ps.Bind(companyId, eventId, title, description, startTimestamp.ToUniversalTime(), endTimestamp.ToUniversalTime(), eventType, participantType, bannerPrivacyType, bannerVisibleDate, scoringType, calculationType, extensionDaysForResult, isDisplayResult, numberOfLeaderboard, backgroundImageUrl, false, (int)EventValidStatusEnum.Valid, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));

                    ps = session.Prepare(CQLGenerator.InsertStatement("event_by_start_timestamp",
                        new List<string> { "company_id", "event_id", "start_timestamp", "is_suspended_by_admin", "valid_status" }));
                    insertBatch.Add(ps.Bind(companyId, eventId, startTimestamp.ToUniversalTime(), false, (int)EventValidStatusEnum.Valid));
                }
                else
                {
                    BatchStatement deleteBatch = new BatchStatement();

                    ps = session.Prepare(CQLGenerator.SelectStatement("event",
                       new List<string>(), new List<string> { "company_id", "event_id" }));
                    Row eventRow = session.Execute(ps.Bind(companyId, eventId)).FirstOrDefault();

                    DateTimeOffset currentStartTime = eventRow.GetValue<DateTimeOffset>("start_timestamp");

                    // Event by topic
                    ps = session.Prepare(CQLGenerator.SelectStatement("topic_by_event",
                        new List<string>(), new List<string> { "event_id" }));
                    RowSet topicEventRowset = session.Execute(ps.Bind(eventId));

                    foreach (Row topicEventRow in topicEventRowset)
                    {
                        string topicId = topicEventRow.GetValue<string>("topic_id");
                        ps = session.Prepare(CQLGenerator.DeleteStatement("event_by_topic",
                            new List<string> { "topic_id", "event_id" }));
                        deleteBatch.Add(ps.Bind(topicId, eventId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("topic_by_event",
                            new List<string> { "event_id" }));
                    deleteBatch.Add(ps.Bind(eventId));

                    // Event by department 
                    ps = session.Prepare(CQLGenerator.SelectStatement("department_by_event",
                        new List<string>(), new List<string> { "event_id" }));
                    topicEventRowset = session.Execute(ps.Bind(eventId));

                    foreach (Row topicEventRow in topicEventRowset)
                    {
                        string departmentId = topicEventRow.GetValue<string>("department_id");
                        ps = session.Prepare(CQLGenerator.DeleteStatement("event_by_department",
                            new List<string> { "department_id", "event_id" }));
                        deleteBatch.Add(ps.Bind(departmentId, eventId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("department_by_event",
                            new List<string> { "event_id" }));
                    deleteBatch.Add(ps.Bind(eventId));

                    // Event by user 
                    ps = session.Prepare(CQLGenerator.SelectStatement("user_by_event",
                        new List<string>(), new List<string> { "event_id" }));
                    topicEventRowset = session.Execute(ps.Bind(eventId));

                    foreach (Row topicEventRow in topicEventRowset)
                    {
                        string userId = topicEventRow.GetValue<string>("user_id");
                        ps = session.Prepare(CQLGenerator.DeleteStatement("event_by_user",
                            new List<string> { "user_id", "event_id" }));
                        deleteBatch.Add(ps.Bind(userId, eventId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("user_by_event",
                            new List<string> { "event_id" }));
                    deleteBatch.Add(ps.Bind(eventId));

                    // Event by start timestamp
                    ps = session.Prepare(CQLGenerator.DeleteStatement("event_by_start_timestamp",
                           new List<string> { "company_id", "start_timestamp", "event_id" }));
                    deleteBatch.Add(ps.Bind(companyId, currentStartTime, eventId));

                    session.Execute(deleteBatch);

                    ps = session.Prepare(CQLGenerator.UpdateStatement("event",
                       new List<string> { "company_id", "event_id" }, new List<string> { "title", "description", "start_timestamp", "end_timestamp", "event_type", "participant_type", "banner_privacy_type", "banner_visible_date", "scoring_type", "calculation_type", "extended_display_days", "is_display_result", "leaderboard_number", "background_image_url", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    insertBatch.Add(ps.Bind(title, description, startTimestamp.ToUniversalTime(), endTimestamp.ToUniversalTime(), eventType, participantType, bannerPrivacyType, bannerVisibleDate, scoringType, calculationType, extensionDaysForResult, isDisplayResult, numberOfLeaderboard, backgroundImageUrl, adminUserId, DateTime.UtcNow, companyId, eventId));

                    ps = session.Prepare(CQLGenerator.InsertStatement("event_by_start_timestamp",
                       new List<string> { "company_id", "event_id", "start_timestamp", "is_suspended_by_admin", "valid_status" }));
                    insertBatch.Add(ps.Bind(companyId, eventId, startTimestamp.ToUniversalTime(), false, (int)EventValidStatusEnum.Valid));
                }

                session.Execute(insertBatch);

                if (startTimestamp < currentTime)
                {
                    Analytic analytics = new Analytic();
                    ISession analyticsSession = cm.getAnalyticSession();
                    analytics.InsertLeaderboardForEvent(eventId, scoringType, calculationType, topicIds, adminUserId, companyId, participantIds, startTimestamp, analyticsSession, session);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectAllResponse SelectAllBasic(string adminUserId, string companyId, int eventStatusType)
        {
            EventSelectAllResponse response = new EventSelectAllResponse();
            response.Events = new List<Event>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psEvent = session.Prepare(CQLGenerator.SelectStatement("event_by_start_timestamp",
                    new List<string>(), new List<string> { "company_id" }));
                RowSet eventRowset = session.Execute(psEvent.Bind(companyId));

                foreach (Row eventRow in eventRowset)
                {
                    int validStatus = eventRow.GetValue<int>("valid_status");

                    if (validStatus > (int)EventValidStatusEnum.Deleted)
                    {
                        string eventId = eventRow.GetValue<string>("event_id");
                        Event selectedEvent = SelectEventByAdmin(adminUserId, eventId, companyId, QUERY_TYPE_BASIC, eventStatusType, session).Event;
                        if (selectedEvent != null)
                        {
                            response.Events.Add(selectedEvent);
                        }
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectAllResponse SelectAllByUser(string requesterUserId, string companyId, ISession session)
        {
            EventSelectAllResponse response = new EventSelectAllResponse();
            response.Events = new List<Event>();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }


                PreparedStatement psEvent = session.Prepare(CQLGenerator.SelectStatement("event_by_start_timestamp",
                    new List<string>(), new List<string> { "company_id", "valid_status" }));
                RowSet eventRowset = session.Execute(psEvent.Bind(companyId, (int)EventValidStatusEnum.Valid));

                foreach (Row eventRow in eventRowset)
                {
                    bool isSuspendedByAdmin = eventRow.GetValue<bool>("is_suspended_by_admin");
                    string eventId = eventRow.GetValue<string>("event_id");

                    if (!isSuspendedByAdmin)
                    {
                        Event selectedEvent = SelectEventByClient(requesterUserId, eventId, companyId, session).Event;
                        if (selectedEvent != null)
                        {
                            response.Events.Add(selectedEvent);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectResponse SelectEventByAdmin(string adminUserId,
                                                      string eventId,
                                                      string companyId,
                                                      int queryType,
                                                      int eventStatus = (int)EventStatusEnum.Ignored,
                                                      ISession session = null)
        {
            EventSelectResponse response = new EventSelectResponse();
            response.Event = null;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                Row eventRow = vh.ValidateEvent(eventId, companyId, session, false);

                if (eventRow != null)
                {
                    DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp");
                    DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp");
                    string title = eventRow.GetValue<string>("title");
                    string description = eventRow.GetValue<string>("description");
                    int participantType = eventRow.GetValue<int>("participant_type");
                    int eventType = eventRow.GetValue<int>("event_type");

                    bool isSuspended = eventRow.GetValue<bool>("is_suspended_by_admin");

                    int status = 0;

                    if (isSuspended)
                    {
                        status = (int)EventStatusEnum.Suspended;
                    }
                    else
                    {
                        DateTime currentDate = DateTime.UtcNow;

                        if (currentDate >= startTime && currentDate <= endTime)
                        {
                            status = (int)EventStatusEnum.Live;
                        }
                        else if (currentDate > endTime)
                        {
                            status = (int)EventStatusEnum.Completed;
                        }
                        else if (currentDate < startTime)
                        {
                            status = (int)EventStatusEnum.Upcoming;
                        }
                    }

                    if (eventStatus != (int)EventStatusEnum.Ignored && eventStatus != (int)EventStatusEnum.All)
                    {
                        if (status != eventStatus)
                        {
                            return response;
                        }
                    }


                    List<Topic> topics = new List<Topic>();
                    List<Department> targetedDepartments = new List<Department>();
                    List<User> targetedUsers = new List<User>();

                    int bannerPrivacyType = 0;
                    int scoringType = 0;
                    int calculationType = 0;
                    bool isDisplayResult = false;
                    int numberOfDaysToDisplayResult = 0;
                    int leaderboardNumber = 0;
                    string backgroundImageUrl = string.Empty;
                    DateTimeOffset bannerVisibleDate = new DateTimeOffset(DateTime.UtcNow);

                    int numberOfTopics = 0;
                    PreparedStatement psEvent = session.Prepare(CQLGenerator.CountStatement("topic_by_event", new List<string> { "event_id" }));
                    BoundStatement bsEvent = psEvent.Bind(eventId);
                    numberOfTopics = (int)session.Execute(bsEvent).FirstOrDefault().GetValue<long>("count");

                    if (queryType == QUERY_TYPE_DETAIL)
                    {
                        backgroundImageUrl = eventRow.GetValue<string>("background_image_url");
                        bannerVisibleDate = eventRow.GetValue<DateTimeOffset>("banner_visible_date");

                        psEvent = session.Prepare(CQLGenerator.SelectStatement("topic_by_event",
                            new List<string>(), new List<string> { "event_id" }));
                        bsEvent = psEvent.Bind(eventId);
                        RowSet topicRowSet = session.Execute(bsEvent);

                        Topic topic = new Topic();
                        foreach (Row topicRow in topicRowSet)
                        {
                            string topicId = topicRow.GetValue<string>("topic_id");
                            Topic selectedTopic = topic.SelectTopicBasic(topicId, null, companyId, null, null, session).Topic;

                            if (topic != null)
                            {
                                topics.Add(selectedTopic);
                            }
                        }

                        if (participantType != (int)ParticipantTypeEnum.Everyone)
                        {
                            if (participantType == (int)ParticipantTypeEnum.Department)
                            {
                                psEvent = session.Prepare(CQLGenerator.SelectStatement("department_by_event",
                                    new List<string>(), new List<string> { "event_id" }));
                                bsEvent = psEvent.Bind(eventId);
                                RowSet departmentRowSet = session.Execute(bsEvent);

                                Department department = new Department();
                                foreach (Row departmentRow in departmentRowSet)
                                {
                                    string departmentId = departmentRow.GetValue<string>("department_id");
                                    Department selectedDepartment = department.GetDepartmentDetail(null, companyId, departmentId, Department.QUERY_TYPE_BASIC, session).Department;

                                    if (selectedDepartment != null)
                                    {
                                        targetedDepartments.Add(selectedDepartment);
                                    }
                                }
                            }
                            else if (participantType == (int)ParticipantTypeEnum.Personnel)
                            {
                                psEvent = session.Prepare(CQLGenerator.SelectStatement("event_by_user",
                                    new List<string>(), new List<string> { "event_id" }));
                                bsEvent = psEvent.Bind(eventId);
                                RowSet userRowSet = session.Execute(bsEvent);

                                User user = new User();
                                foreach (Row userRow in userRowSet)
                                {
                                    string userId = userRow.GetValue<string>("user_id");
                                    User selectedUser = user.SelectUserBasic(userId, companyId, true, session).User;

                                    if (selectedUser != null)
                                    {
                                        targetedUsers.Add(selectedUser);
                                    }
                                }
                            }
                        }

                        bannerPrivacyType = eventRow.GetValue<int>("banner_privacy_type");
                        scoringType = eventRow.GetValue<int>("scoring_type");
                        calculationType = eventRow.GetValue<int>("calculation_type");
                        numberOfDaysToDisplayResult = eventRow.GetValue<int>("extended_display_days");
                        isDisplayResult = eventRow.GetValue<bool>("is_display_result");
                        leaderboardNumber = eventRow.GetValue<int>("leaderboard_number");
                    }

                    Event selectedEvent = new Event
                    {
                        EventId = eventId,
                        Title = title,
                        Description = description,
                        StartTimestamp = DateHelper.ConvertDateToTimezoneSpecific(startTime, companyId, session),
                        EndTimestamp = DateHelper.ConvertDateToTimezoneSpecific(endTime, companyId, session),
                        EventType = eventType,
                        NumberOfTopics = numberOfTopics,
                        Topics = topics,
                        ParticipantType = participantType,
                        TargetedDepartments = targetedDepartments,
                        TargetedUsers = targetedUsers,
                        BannerPrivacyType = bannerPrivacyType,
                        BannerVisibleDate = DateHelper.ConvertDateToTimezoneSpecific<DateTimeOffset>(bannerVisibleDate, companyId, session),
                        ScoringType = scoringType,
                        CalculationType = calculationType,
                        IsDisplayResult = isDisplayResult,
                        NumberOfDaysToDisplayResult = numberOfDaysToDisplayResult,
                        LeaderboardNumber = leaderboardNumber,
                        BackgroundImageUrl = backgroundImageUrl,
                        Status = status
                    };

                    response.Event = selectedEvent;

                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectResponse SelectEventByClient(string requesterUserId,
                                                       string eventId,
                                                       string companyId,
                                                       ISession mainSession = null,
                                                       bool isBasicDetail = true)
        {
            EventSelectResponse response = new EventSelectResponse();
            response.Event = null;

            try
            {
                ConnectionManager cm = new ConnectionManager();

                if (mainSession == null)
                {
                    ValidationHandler vh = new ValidationHandler();
                    mainSession = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement psEvent = mainSession.Prepare(CQLGenerator.SelectStatement("event",
                    new List<string>(), new List<string> { "company_id", "event_id", "is_suspended_by_admin" }));
                BoundStatement bsEvent = psEvent.Bind(companyId, eventId, false);

                Row eventRow = mainSession.Execute(bsEvent).FirstOrDefault();

                if (eventRow != null)
                {
                    DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                    DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();
                    int extendedDaysToShowResult = eventRow.GetValue<int>("extended_display_days");

                    string title = eventRow.GetValue<string>("title");
                    string description = eventRow.GetValue<string>("description");
                    int participantType = eventRow.GetValue<int>("participant_type");
                    int eventType = eventRow.GetValue<int>("event_type");
                    int bannerPrivacyType = eventRow.GetValue<int>("banner_privacy_type");
                    DateTime bannerVisibleDate = eventRow.GetValue<DateTime>("banner_visible_date").ToUniversalTime();
                    bool isDisplayResult = eventRow.GetValue<bool>("is_display_result");
                    int leaderBoardNumber = eventRow.GetValue<int>("leaderboard_number");
                    string backgroundImageUrl = eventRow.GetValue<string>("background_image_url");

                    int scoringType = eventRow.GetValue<int>("scoring_type");
                    int calculationType = eventRow.GetValue<int>("calculation_type");

                    int eventStatus = (int)EventStatusEnum.Upcoming;

                    // Check time
                    DateTime currentTime = DateTime.UtcNow;
                    if (currentTime < bannerVisibleDate)
                    {
                        return response;
                    }

                    if (currentTime >= startTime)
                    {
                        if (currentTime < endTime)
                        {
                            eventStatus = (int)EventStatusEnum.Live;
                        }
                        else if (currentTime >= endTime)
                        {
                            if (currentTime > endTime.AddDays(extendedDaysToShowResult))
                            {
                                return response;
                            }

                            eventStatus = (int)EventStatusEnum.Completed;
                        }
                    }

                    // Check banner privacy
                    if (bannerPrivacyType == (int)BannerPrivacyTypeEnum.Participant)
                    {
                        if (participantType == (int)ParticipantTypeEnum.Personnel)
                        {
                            psEvent = mainSession.Prepare(CQLGenerator.SelectStatement("event_by_user",
                                   new List<string>(), new List<string> { "event_id", "user_id" }));
                            bsEvent = psEvent.Bind(eventId, requesterUserId);
                            Row userRow = mainSession.Execute(bsEvent).FirstOrDefault();

                            if (userRow == null)
                            {
                                return response;
                            }
                        }
                        else if (participantType == (int)ParticipantTypeEnum.Department)
                        {

                            List<Department> departments = new Department().GetAllDepartmentByUserId(requesterUserId, companyId, mainSession).Departments;

                            bool isTargetedForDepartment = false;

                            foreach (Department department in departments)
                            {
                                string departmentId = department.Id;
                                psEvent = mainSession.Prepare(CQLGenerator.SelectStatement("department_by_event",
                                  new List<string>(), new List<string> { "event_id", "department_id" }));
                                bsEvent = psEvent.Bind(eventId, departmentId);
                                Row departmentRow = mainSession.Execute(bsEvent).FirstOrDefault();

                                if (departmentRow != null)
                                {
                                    isTargetedForDepartment = true;
                                    break;
                                }
                            }

                            if (!isTargetedForDepartment)
                            {
                                return response;
                            }
                        }
                    }

                    // Fetch topics
                    List<Topic> topics = new List<Topic>();

                    psEvent = mainSession.Prepare(CQLGenerator.SelectStatement("topic_by_event",
                           new List<string>(), new List<string> { "event_id" }));
                    bsEvent = psEvent.Bind(eventId);
                    RowSet topicRowSet = mainSession.Execute(bsEvent);

                    Topic topic = new Topic();
                    foreach (Row topicRow in topicRowSet)
                    {
                        string topicId = topicRow.GetValue<string>("topic_id");
                        Topic selectedTopic = topic.SelectTopicBasic(topicId, null, companyId, null, null, mainSession).Topic;

                        if (topic != null)
                        {
                            topics.Add(selectedTopic);
                        }
                    }

                    // Fetch results
                    List<CassandraService.Entity.Analytic.EventLeaderboard> topEventLeaderboard = new List<CassandraService.Entity.Analytic.EventLeaderboard>();
                    List<CassandraService.Entity.Analytic.EventLeaderboard> outOfRankingLeaderboard = new List<CassandraService.Entity.Analytic.EventLeaderboard>();
                    if (!isBasicDetail)
                    {
                        if (isDisplayResult)
                        {
                            Analytic analytic = new Analytic();
                            ISession analyticSession = cm.getAnalyticSession();
                            AnalyticsSelectLeaderboardByEventResponse leaderboardResponse = analytic.SelectLeaderboardByEvent(requesterUserId, companyId, eventId, leaderBoardNumber, participantType, scoringType, calculationType, mainSession, analyticSession);
                            if (leaderboardResponse.Success)
                            {
                                topEventLeaderboard = leaderboardResponse.TopEventLeaderboard;
                                outOfRankingLeaderboard = leaderboardResponse.OutOfRankingLeaderboard;
                            }
                        }

                    }

                    Event selectedEvent = new Event
                    {
                        EventId = eventId,
                        Title = title,
                        Description = description,
                        StartTimestamp = startTime,
                        EndTimestamp = endTime,
                        EventType = eventType,
                        Topics = topics,
                        ParticipantType = participantType,
                        BannerPrivacyType = bannerPrivacyType,
                        ScoringType = scoringType,
                        CalculationType = calculationType,
                        TopEventLeaderboard = topEventLeaderboard,
                        OutOfRankingLeaderboard = outOfRankingLeaderboard,
                        BackgroundImageUrl = backgroundImageUrl,
                        Status = eventStatus,
                        IsDisplayResult = isDisplayResult
                    };

                    response.Event = selectedEvent;
                    response.Success = true;
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectTopicResponse SelectTopicsForEvent(string adminUserId, string companyId, int eventParticipantType, List<string> selectedUserIds, List<string> selectedDepartmentIds)
        {
            EventSelectTopicResponse response = new EventSelectTopicResponse();
            response.Topics = new List<Topic>();

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Topic topicManager = new Topic();
                List<Topic> selectedTopics = new List<Topic>();

                if (eventParticipantType == (int)ParticipantTypeEnum.Department)
                {
                    if (selectedDepartmentIds.Count == 0)
                    {
                        Log.Error(string.Format(ErrorMessage.EventMissingDepartment));
                        response.ErrorCode = Int16.Parse(ErrorCode.EventMissingDepartment);
                        response.ErrorMessage = ErrorMessage.EventMissingDepartment;
                        return response;
                    }


                    foreach (string departmentId in selectedDepartmentIds)
                    {
                        List<Topic> departmentTopics = topicManager.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, string.Empty, departmentId).Topics;

                        if (selectedTopics.Count == 0)
                        {
                            selectedTopics.AddRange(departmentTopics);
                        }

                        if (departmentTopics.Count > 0)
                        {
                            selectedTopics.Select(a => a.TopicId).Intersect(departmentTopics.Select(b => b.TopicId));
                        }

                    }

                }
                else if (eventParticipantType == (int)ParticipantTypeEnum.Personnel)
                {
                    if (selectedUserIds.Count == 0)
                    {
                        Log.Error(string.Format(ErrorMessage.EventMissingUser));
                        response.ErrorCode = Int16.Parse(ErrorCode.EventMissingUser);
                        response.ErrorMessage = ErrorMessage.EventMissingUser;
                        return response;
                    }

                    Department departmentManager = new Department();

                    foreach (string userId in selectedUserIds)
                    {
                        List<Department> departments = departmentManager.GetAllDepartmentByUserId(userId, companyId, session).Departments;

                        foreach (string departmentId in selectedDepartmentIds)
                        {
                            List<Topic> departmentTopics = topicManager.SelectAllTopicBasicByCategoryAndDepartment(adminUserId, companyId, string.Empty, departmentId).Topics;

                            if (selectedTopics.Count == 0 && departmentTopics.Count > 0)
                            {
                                selectedTopics.AddRange(departmentTopics);
                            }

                            if (departmentTopics.Count > 0)
                            {
                                selectedTopics.Select(a => a.TopicId).Intersect(departmentTopics.Select(b => b.TopicId));
                            }

                        }
                    }
                }
                else
                {
                    selectedTopics = topicManager.SelectAllTopicBasicByEveryone(adminUserId, companyId, session).Topics;
                }

                response.Topics = selectedTopics;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectAllResponse SelectLiveDepartmentEventsByDepartmentId(string adminUserId, string departmentId, string companyId, ISession session = null)
        {
            EventSelectAllResponse response = new EventSelectAllResponse();
            response.Events = new List<Event>();
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                PreparedStatement psEventByDepartment = session.Prepare(CQLGenerator.SelectStatement("event_by_department",
                    new List<string>(), new List<string> { "department_id" }));
                RowSet eventByDepartmentRowset = session.Execute(psEventByDepartment.Bind(departmentId));

                foreach (Row eventByDepartmentRow in eventByDepartmentRowset)
                {
                    string eventId = eventByDepartmentRow.GetValue<string>("event_id");

                    Row eventRow = vh.ValidateEvent(eventId, companyId, session);

                    if (eventRow != null)
                    {
                        // Check if this is a live event
                        DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                        DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();

                        DateTime currentDate = DateTime.UtcNow;

                        if (currentDate >= startTime && currentDate <= endTime)
                        {
                            int participantType = eventRow.GetValue<int>("participant_type");
                            int scoringType = eventRow.GetValue<int>("scoring_type");
                            int calculationType = eventRow.GetValue<int>("calculation_type");

                            PreparedStatement psTopicByEvent = session.Prepare(CQLGenerator.SelectStatement("topic_by_event",
                                new List<string>(), new List<string> { "event_id" }));
                            RowSet topicByEventRowset = session.Execute(psTopicByEvent.Bind(eventId));

                            List<Topic> selectedTopics = new List<Topic>();

                            Topic topicManager = new Topic();
                            foreach(Row topicByEventRow in topicByEventRowset)
                            {
                                string topicId = topicByEventRow.GetValue<string>("topic_id");
                                Topic topic = topicManager.SelectTopicDetail(topicId, adminUserId, companyId, null, session).Topic;
                                if(topic != null)
                                {
                                    selectedTopics.Add(topic);
                                }
                            }

                            Event selectedEvent = new Event
                            {
                                EventId = eventId,
                                ScoringType = scoringType,
                                CalculationType = calculationType,
                                StartTimestamp = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime(),
                                EndTimestamp = eventRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime(),
                                Topics = selectedTopics
                            };

                            response.Events.Add(selectedEvent);
                        }
                    }
                }


                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectAllResponse SelectLivePersonnelEventsByUserId(string userId, string companyId, ISession session = null)
        {
            EventSelectAllResponse response = new EventSelectAllResponse();
            response.Events = new List<Event>();
            response.Success = false;

            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsUser(userId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }

                // Get all events targeted for everyone and for this current user
                PreparedStatement psEventByUser = session.Prepare(CQLGenerator.SelectStatement("event_by_user",
                    new List<string>(), new List<string> { "user_id" }));
                RowSet eventByUserRowset = session.Execute(psEventByUser.Bind(userId));

                foreach (Row eventByUserRow in eventByUserRowset)
                {
                    string eventId = eventByUserRow.GetValue<string>("event_id");

                    Row eventRow = vh.ValidateEvent(eventId, companyId, session);

                    if (eventRow != null)
                    {
                        // Check if this is a live event
                        DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                        DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();

                        DateTime currentDate = DateTime.UtcNow;

                        if (currentDate >= startTime && currentDate <= endTime)
                        {
                            int participantType = eventRow.GetValue<int>("participant_type");
                            int scoringType = eventRow.GetValue<int>("scoring_type");
                            int calculationType = eventRow.GetValue<int>("calculation_type");

                            Event selectedEvent = new Event
                            {
                                EventId = eventId,
                                ScoringType = scoringType,
                                CalculationType = calculationType,
                                StartTimestamp = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime(),
                                EndTimestamp = eventRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime()
                            };

                            response.Events.Add(selectedEvent);
                        }
                    }
                }

                // For everyone
                psEventByUser = session.Prepare(CQLGenerator.SelectStatement("event",
                    new List<string>(), new List<string> { "company_id", "participant_type" }));
                eventByUserRowset = session.Execute(psEventByUser.Bind(companyId, (int)ParticipantTypeEnum.Everyone));

                foreach (Row eventByUserRow in eventByUserRowset)
                {
                    string eventId = eventByUserRow.GetValue<string>("event_id");

                    Row eventRow = vh.ValidateEvent(eventId, companyId, session);

                    if (eventRow != null)
                    {
                        // Check if this is a live event
                        DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                        DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();

                        DateTime currentDate = DateTime.UtcNow;

                        if (currentDate >= startTime && currentDate <= endTime)
                        {
                            int participantType = eventRow.GetValue<int>("participant_type");
                            int scoringType = eventRow.GetValue<int>("scoring_type");
                            int calculationType = eventRow.GetValue<int>("calculation_type");

                            Event selectedEvent = new Event
                            {
                                EventId = eventId,
                                ScoringType = scoringType,
                                CalculationType = calculationType,
                                StartTimestamp = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime(),
                                EndTimestamp = eventRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime()
                            };

                            response.Events.Add(selectedEvent);
                        }
                    }
                }

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectDepartmentResponse SelectDepartmentsForEvent(string adminUserId, string companyId, List<string> selectedTopicIds, ISession session = null)
        {
            EventSelectDepartmentResponse response = new EventSelectDepartmentResponse();
            response.Departments = new List<Department>();
            response.Success = false;

            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }


                Department departmentManager = new Department();
                List<Department> selectedDepartments = new List<Department>();

                if (selectedTopicIds.Count > 0)
                {
                    foreach (string topicId in selectedTopicIds)
                    {
                        List<Department> topicDepartments = departmentManager.GetAllDepartmentByTopicId(topicId, companyId, session).Departments;
                        if (selectedDepartments.Count == 0 && topicDepartments.Count > 0)
                        {
                            selectedDepartments.AddRange(topicDepartments);
                        }

                        if (topicDepartments.Count > 0)
                        {
                            selectedDepartments.Select(a => a.Id).Intersect(topicDepartments.Select(b => b.Id));
                        }
                    }
                }
                else
                {
                    selectedDepartments = departmentManager.GetAllDepartment(adminUserId, companyId, Department.QUERY_TYPE_BASIC, session).Departments;
                }


                response.Departments = selectedDepartments;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventSelectUserResponse SelectUsersForEvent(string adminUserId, string companyId, List<string> selectedTopicIds)
        {
            EventSelectUserResponse response = new EventSelectUserResponse();
            response.Users = new List<User>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                List<Department> selectedDepartments = SelectDepartmentsForEvent(adminUserId, companyId, selectedTopicIds, session).Departments;
                response.Users = new User().SelectAllUsersByDepartments(selectedDepartments, adminUserId, companyId, session, null, false).Users;
                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventUpdateResponse Update(string eventId,
                                          string adminUserId,
                                          string companyId,
                                          string newTitle,
                                          string newDescription,
                                          DateTime newStartTimestamp,
                                          DateTime newEndTimestamp,
                                          int newEventType,
                                          int newParticipantType,
                                          int newBannerPrivacyType,
                                          DateTime newBannerVisibleDate,
                                          int newScoringType,
                                          int newCalculationType,
                                          bool newIsDisplayResult,
                                          int newExtensionDaysForResult,
                                          int newNumberOfLeaderboard,
                                          string newBackgroundImageUrl,
                                          List<string> newTopicIds,
                                          List<string> newDepartmentIds = null,
                                          List<string> newUserIds = null)
        {
            EventUpdateResponse response = new EventUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row eventRow = vh.ValidateEvent(eventId, companyId, session, false);

                if (eventRow == null)
                {
                    Log.Error("Event is invalid: " + eventId);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventInvalid);
                    response.ErrorMessage = ErrorMessage.EventInvalid;
                    return response;
                }

                newStartTimestamp = newStartTimestamp.ToUniversalTime();
                newEndTimestamp = newEndTimestamp.ToUniversalTime();

                bool isSuspendedByAdmin = eventRow.GetValue<bool>("is_suspended_by_admin");
                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement preparedStatement = new PreparedStatement();

                if (!isSuspendedByAdmin)
                {
                    // Check if this is a live event
                    DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                    DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();

                    DateTime currentDate = DateTime.UtcNow;

                    if (currentDate >= startTime && currentDate >= endTime)
                    {
                        Log.Error(ErrorMessage.EventLiveUpdateFailed);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventLiveUpdateFailed);
                        response.ErrorMessage = ErrorMessage.EventLiveUpdateFailed;
                        return response;
                    }

                    // Check for completed event
                    if (currentDate > endTime)
                    {
                        Log.Error(ErrorMessage.EventAlreadyCompleted);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventAlreadyCompleted);
                        response.ErrorMessage = ErrorMessage.EventAlreadyCompleted;
                        return response;
                    }

                    // Upcoming event
                    if (newStartTimestamp >= newEndTimestamp)
                    {
                        Log.Error(ErrorMessage.EventEndDateEarlierThanStartDate);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.EventEndDateEarlierThanStartDate;
                        return response;
                    }

                    if (currentDate >= newEndTimestamp)
                    {
                        Log.Error(ErrorMessage.EventEndDateEarlierThanToday);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventEndDateEarlierThanToday);
                        response.ErrorMessage = ErrorMessage.EventEndDateEarlierThanToday;
                        return response;
                    }

                    if (newStartTimestamp < newBannerVisibleDate)
                    {
                        Log.Error(ErrorMessage.EventBannerDateLaterThanStartDate);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventBannerDateLaterThanStartDate);
                        response.ErrorMessage = ErrorMessage.EventBannerDateLaterThanStartDate;
                        return response;
                    }

                    if (string.IsNullOrEmpty(newTitle))
                    {
                        Log.Error(ErrorMessage.EventTitleMissing);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventTitleMissing);
                        response.ErrorMessage = ErrorMessage.EventTitleMissing;
                        return response;
                    }

                    EventCreateResponse updateResponse = Create(adminUserId,
                                                                companyId,
                                                                newTitle,
                                                                newDescription,
                                                                newStartTimestamp,
                                                                newEndTimestamp,
                                                                newEventType,
                                                                newParticipantType,
                                                                newBannerPrivacyType,
                                                                newBannerVisibleDate,
                                                                newScoringType,
                                                                newCalculationType,
                                                                newIsDisplayResult,
                                                                newExtensionDaysForResult,
                                                                newNumberOfLeaderboard,
                                                                newBackgroundImageUrl,
                                                                newTopicIds,
                                                                newDepartmentIds,
                                                                newUserIds,
                                                                eventId,
                                                                session);
                    if (updateResponse.Success)
                    {
                        response.Success = true;
                    }
                    else
                    {
                        response.ErrorCode = updateResponse.ErrorCode;
                        response.ErrorMessage = updateResponse.ErrorMessage;
                    }
                }
                else
                {
                    // Suspended event
                    // Allows update on end date, title and description
                    DateTimeOffset startDateTime = eventRow.GetValue<DateTimeOffset>("start_timestamp");
                    if (startDateTime >= newEndTimestamp)
                    {
                        Log.Error(ErrorMessage.EventEndDateEarlierThanStartDate);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventEndDateEarlierThanStartDate);
                        response.ErrorMessage = ErrorMessage.EventEndDateEarlierThanStartDate;
                        return response;
                    }

                    if (string.IsNullOrEmpty(newTitle))
                    {
                        Log.Error(ErrorMessage.EventTitleMissing);
                        response.ErrorCode = Int16.Parse(ErrorCode.EventTitleMissing);
                        response.ErrorMessage = ErrorMessage.EventTitleMissing;
                        return response;
                    }

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("event",
                        new List<string> { "company_id", "event_id" }, new List<string> { "title", "description", "end_timestamp", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    batchStatement.Add(preparedStatement.Bind(newTitle, newDescription, newEndTimestamp, adminUserId, DateTime.UtcNow, companyId, eventId));

                    session.Execute(batchStatement);

                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventUpdateResponse SuspendEvent(string adminUserId, string companyId, string eventId)
        {
            EventUpdateResponse response = new EventUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row eventRow = vh.ValidateEvent(eventId, companyId, session, false);

                if (eventRow == null)
                {
                    Log.Error("Event is invalid: " + eventId);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventInvalid);
                    response.ErrorMessage = ErrorMessage.EventInvalid;
                    return response;
                }

                bool isSuspendedByAdmin = eventRow.GetValue<bool>("is_suspended_by_admin");

                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                if (isSuspendedByAdmin)
                {
                    Log.Error("Event already been suspended: " + eventId);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventAlreadySuspended);
                    response.ErrorMessage = ErrorMessage.EventAlreadySuspended;
                    return response;
                }

                // Check if this is a live event
                DateTimeOffset startTime = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime();
                DateTimeOffset endTime = eventRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime();

                DateTime currentDate = DateTime.UtcNow;

                if (currentDate < startTime || currentDate > endTime)
                {
                    Log.Error(ErrorMessage.EventNoLongerLive);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventNoLongerLive);
                    response.ErrorMessage = ErrorMessage.EventNoLongerLive;
                    return response;
                }

                ps = session.Prepare(CQLGenerator.UpdateStatement("event",
                    new List<string> { "company_id", "event_id" }, new List<string> { "is_suspended_by_admin", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(true, (int)EventValidStatusEnum.Suspended, adminUserId, DateTime.Now, companyId, eventId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("event_by_start_timestamp",
                    new List<string> { "company_id", "start_timestamp", "event_id" }));
                session.Execute(ps.Bind(companyId, startTime, eventId));

                ps = session.Prepare(CQLGenerator.InsertStatement("event_by_start_timestamp",
                                        new List<string> { "company_id", "event_id", "start_timestamp", "is_suspended_by_admin", "valid_status" }));
                updateBatch.Add(ps.Bind(companyId, eventId, startTime, true, (int)EventValidStatusEnum.Suspended));

                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventUpdateResponse RestoreEvent(string adminUserId, string companyId, string eventId)
        {
            EventUpdateResponse response = new EventUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row eventRow = vh.ValidateEvent(eventId, companyId, session, false);

                if (eventRow == null)
                {
                    Log.Error("Event is invalid: " + eventId);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventInvalid);
                    response.ErrorMessage = ErrorMessage.EventInvalid;
                    return response;
                }

                bool isSuspendedByAdmin = eventRow.GetValue<bool>("is_suspended_by_admin");
                int validStatus = eventRow.GetValue<int>("valid_status");

                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                if (!isSuspendedByAdmin && validStatus == (int)EventValidStatusEnum.Valid)
                {
                    Log.Error("Event has already been restored " + eventId);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventAlreadyRestored);
                    response.ErrorMessage = ErrorMessage.EventAlreadyRestored;
                    return response;
                }

                DateTimeOffset startTime = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime();

                // No need check if event has completed
                //DateTimeOffset endTime = eventRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime();

                //DateTime currentDate = DateTime.UtcNow;

                //if (currentDate >= endTime)
                //{
                //    Log.Error(ErrorMessage.EventAlreadyCompleted);
                //    response.ErrorCode = Int16.Parse(ErrorCode.EventAlreadyCompleted);
                //    response.ErrorMessage = ErrorMessage.EventAlreadyCompleted;
                //    return response;
                //}

                ps = session.Prepare(CQLGenerator.UpdateStatement("event",
                    new List<string> { "company_id", "event_id" }, new List<string> { "is_suspended_by_admin", "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind(false, (int)EventValidStatusEnum.Valid, adminUserId, DateTime.Now, companyId, eventId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("event_by_start_timestamp",
                    new List<string> { "company_id", "start_timestamp", "event_id" }));
                session.Execute(ps.Bind(companyId, startTime, eventId));

                ps = session.Prepare(CQLGenerator.InsertStatement("event_by_start_timestamp",
                                        new List<string> { "company_id", "event_id", "start_timestamp", "is_suspended_by_admin", "valid_status" }));
                updateBatch.Add(ps.Bind(companyId, eventId, startTime, false, (int)EventValidStatusEnum.Valid));

                session.Execute(updateBatch);

                response.Success = true;

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public EventUpdateResponse DeleteEvent(string adminUserId, string companyId, string eventId)
        {
            EventUpdateResponse response = new EventUpdateResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row eventRow = vh.ValidateEvent(eventId, companyId, session, false);

                if (eventRow == null)
                {
                    Log.Error("Event is invalid: " + eventId);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventInvalid);
                    response.ErrorMessage = ErrorMessage.EventInvalid;
                    return response;
                }

                int validStatus = eventRow.GetValue<int>("valid_status");

                if (validStatus == (int)EventValidStatusEnum.Deleted)
                {
                    Log.Error("Event has been deleted: " + eventId);
                    response.ErrorCode = Int16.Parse(ErrorCode.EventAlreadyDeleted);
                    response.ErrorMessage = ErrorMessage.EventAlreadyDeleted;
                    return response;
                }

                BatchStatement updateBatch = new BatchStatement();
                PreparedStatement ps = null;

                DateTimeOffset startTime = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime();
                bool isSuspendedByAdmin = eventRow.GetValue<bool>("is_suspended_by_admin");

                ps = session.Prepare(CQLGenerator.UpdateStatement("event",
                    new List<string> { "company_id", "event_id" }, new List<string> { "valid_status", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                updateBatch.Add(ps.Bind((int)EventValidStatusEnum.Deleted, adminUserId, DateTime.Now, companyId, eventId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("event_by_start_timestamp",
                    new List<string> { "company_id", "start_timestamp", "event_id" }));
                session.Execute(ps.Bind(companyId, startTime, eventId));

                //ps = session.Prepare(CQLGenerator.InsertStatement("event_by_start_timestamp",
                //    new List<string> { "company_id", "event_id", "start_timestamp", "is_suspended_by_admin", "valid_status" }));
                //updateBatch.Add(ps.Bind(companyId, eventId, startTime, isSuspendedByAdmin, (int)EventValidStatusEnum.Deleted));

                session.Execute(updateBatch);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CheckEventInProgressResponse CheckEventInProgress(string adminUserId, string companyId, string topicId, ISession session)
        {
            CheckEventInProgressResponse response = new CheckEventInProgressResponse();
            response.isEventInProgress = false;
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }
                }


                PreparedStatement psEventByTopic = session.Prepare(CQLGenerator.SelectStatement("event_by_topic",
                    new List<string>(), new List<string> { "topic_id" }));
                RowSet eventByTopicRowset = session.Execute(psEventByTopic.Bind(topicId));

                foreach (Row eventByTopicRow in eventByTopicRowset)
                {
                    string eventId = eventByTopicRow.GetValue<string>("event_id");
                    Row eventRow = vh.ValidateEvent(eventId, companyId, session, true);

                    if (eventRow != null)
                    {
                        // Check if this is a live event
                        DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                        DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();

                        DateTime currentDate = DateTime.UtcNow;

                        if (currentDate >= startTime && currentDate <= endTime)
                        {
                            response.isEventInProgress = true;
                            break;
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }


            return response;

        }

        public List<Event> SelectLiveEventsByTopicIdAndUserId(string userId, string mainDepartmentId, string companyId, string topicId, ISession mainSession)
        {
            List<Event> events = new List<Event>();
            try
            {
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psEventByTopic = mainSession.Prepare(CQLGenerator.SelectStatement("event_by_topic",
                    new List<string>(), new List<string> { "topic_id" }));
                RowSet eventByTopicRowset = mainSession.Execute(psEventByTopic.Bind(topicId));

                foreach (Row eventByTopicRow in eventByTopicRowset)
                {
                    string eventId = eventByTopicRow.GetValue<string>("event_id");

                    Row eventRow = vh.ValidateEvent(eventId, companyId, mainSession, true);

                    if (eventRow != null)
                    {
                        // Check if this is a live event
                        DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                        DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();

                        DateTime currentDate = DateTime.UtcNow;

                        if (currentDate >= startTime && currentDate <= endTime)
                        {
                            int participantType = eventRow.GetValue<int>("participant_type");
                            int scoringType = eventRow.GetValue<int>("scoring_type");
                            int calculationType = eventRow.GetValue<int>("calculation_type");

                            if (participantType == (int)ParticipantTypeEnum.Everyone)
                            {
                                Log.Debug("Live event with participant type: everyone");

                                Event selectedEvent = new Event
                                {
                                    EventId = eventId,
                                    ScoringType = scoringType,
                                    CalculationType = calculationType,
                                    StartTimestamp = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime(),
                                    EndTimestamp = eventRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime()
                                };

                                events.Add(selectedEvent);
                            }
                            else
                            {
                                PreparedStatement psParticipant = null;
                                Row participantRow = null;

                                if (participantType == (int)ParticipantTypeEnum.Personnel)
                                {
                                    psParticipant = mainSession.Prepare(CQLGenerator.SelectStatement("user_by_event",
                                        new List<string>(), new List<string> { "event_id", "user_id" }));
                                    participantRow = mainSession.Execute(psParticipant.Bind(eventId, userId)).FirstOrDefault();
                                }
                                else if (participantType == (int)ParticipantTypeEnum.Department)
                                {
                                    psParticipant = mainSession.Prepare(CQLGenerator.SelectStatement("department_by_event",
                                        new List<string>(), new List<string> { "event_id", "department_id" }));
                                    participantRow = mainSession.Execute(psParticipant.Bind(eventId, mainDepartmentId)).FirstOrDefault();
                                }

                                if (participantRow != null)
                                {
                                    Event selectedEvent = new Event
                                    {
                                        EventId = eventId,
                                        ScoringType = scoringType,
                                        CalculationType = calculationType,
                                        StartTimestamp = eventRow.GetValue<DateTimeOffset>("start_timestamp").ToUniversalTime(),
                                        EndTimestamp = eventRow.GetValue<DateTimeOffset>("end_timestamp").ToUniversalTime()
                                    };

                                    events.Add(selectedEvent);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                return new List<Event>();
            }

            return events;
        }

        public List<Event> SelectLiveEventsByTopicId(string companyId, string topicId, ISession mainSession)
        {
            List<Event> events = new List<Event>();
            try
            {
                ValidationHandler vh = new ValidationHandler();

                PreparedStatement psEventByTopic = mainSession.Prepare(CQLGenerator.SelectStatement("event_by_topic",
                    new List<string>(), new List<string> { "topic_id" }));
                RowSet eventByTopicRowset = mainSession.Execute(psEventByTopic.Bind(topicId));

                foreach (Row eventByTopicRow in eventByTopicRowset)
                {
                    string eventId = eventByTopicRow.GetValue<string>("event_id");

                    Row eventRow = vh.ValidateEvent(eventId, companyId, mainSession, true);

                    if (eventRow != null)
                    {
                        // Check if this is a live event
                        DateTime startTime = eventRow.GetValue<DateTime>("start_timestamp").ToUniversalTime();
                        DateTime endTime = eventRow.GetValue<DateTime>("end_timestamp").ToUniversalTime();

                        DateTime currentDate = DateTime.UtcNow;

                        if (currentDate >= startTime && currentDate <= endTime)
                        {
                            int participantType = eventRow.GetValue<int>("participant_type");
                            int scoringType = eventRow.GetValue<int>("scoring_type");
                            int calculationType = eventRow.GetValue<int>("calculation_type");

                            Event selectedEvent = new Event
                            {
                                EventId = eventId,
                                ScoringType = scoringType,
                                CalculationType = calculationType
                            };

                            events.Add(selectedEvent);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                return new List<Event>();
            }

            return events;
        }
    }


}
