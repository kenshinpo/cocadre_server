﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CassandraService.Entity
{
    /// <summary>
    /// Hard Code mode.
    /// </summary>
    [Serializable]
    public class Module
    {
        public const int MODULE_PERSONNEL = 1;  // Only for admin
        public const int MODULE_MATCHUP = 2; // Admin and Moderator
        public const int MODULE_FEED = 3; // Only for admin
        public const int MODULE_DASHBOARD = 4; // Admin and Moderator
        public const int MODULE_ANALYTICS = 5; // Admin and Moderator
        public const int MODULE_COMPANY_PROFILE = 6;  // Only for admin
        
        public const int MODULE_SUPPORT = 7; // Only for admin
        public const int MODULE_ANNOUNCEMENT = 8; // Admin and Moderator
        public const int MODULE_BILLING = 9;  // Only for admin
        public const int MODULE_MARTET_PLACE = 10;  // Only for admin
        public const int MODULE_EVENT = 11; // Admin and Moderator
        public const int MODULE_ADMIN_LOG = 12;  // Only for admin
        public const int MODULE_GAMIFICATION = 13; // Admin and Moderator
        public const int MODULE_SURVEY = 14; // Admin and Moderator

        public int Key { get; set; }
        public bool IsUsing { get; set; }
        public String Title { get; set; }

        public String IconUrl { get; set; }

        public String Description { get; set; }

        public String RightsDescription { get; set; }

        public Module(int key)
        {
            Key = key;
            switch (key)
            {
                case MODULE_PERSONNEL:
                    IsUsing = true;
                    Title = "Personnel";
                    IconUrl = "icon-personnel.png";
                    Description = @"Add rename, and manage users";
                    RightsDescription = @"";
                    break;

                case MODULE_MATCHUP:
                    IsUsing = true;
                    Title = "Match Up";
                    IconUrl = "icon-quiz.png";
                    Description = @"Create Topic and Questions";
                    RightsDescription = @"Add, modify and delete Quiz category, topic and question";
                    break;

                case MODULE_FEED:
                    IsUsing = true;
                    Title = "Feed";
                    IconUrl = "icon-feed.png";
                    Description = @"Enable users to post on Feed";
                    RightsDescription = @"";
                    break;

                case MODULE_DASHBOARD:
                    IsUsing = true;
                    Title = "Dashboard";
                    IconUrl = "icon-dashboard.png";
                    Description = @"Manager notifications and dashboard";
                    RightsDescription = @"View the alert section of the ""Reported Post"" (including private post), respond to and delete comments or posts. Approve profile photos and reply feedbacks";
                    break;

                case MODULE_ANALYTICS:
                    IsUsing = true;
                    Title = "Analytics";
                    IconUrl = "icon-analytics.png";
                    Description = @"Track usage of services";
                    RightsDescription = @"Access to Analytic reports";
                    break;

                case MODULE_COMPANY_PROFILE:
                    IsUsing = true;
                    Title = "Company Profile";
                    IconUrl = "icon-companyprofile.png";
                    Description = @"Update information about your company";
                    RightsDescription = @"";
                    break;

                case MODULE_ADMIN_LOG:
                    IsUsing = false;
                    Title = "Admin Log";
                    IconUrl = "icon-adminlog.png";
                    Description = @"History";
                    RightsDescription = @"";
                    break;

                case MODULE_SUPPORT:
                    IsUsing = false;
                    Title = "Support";
                    IconUrl = "icon-support.png";
                    Description = @"Learn more and get help";
                    RightsDescription = @"";
                    break;

                case MODULE_ANNOUNCEMENT:
                    IsUsing = false;
                    Title = "Announcement";
                    IconUrl = "icon-announcement.png";
                    Description = @"Post announcements and notifications";
                    RightsDescription = @"Post or modify announcements and notifications as a moderator";
                    break;

                case MODULE_BILLING:
                    IsUsing = false;
                    Title = "Billing";
                    IconUrl = "icon-billing.png";
                    Description = @"View charges and manage licenses";
                    RightsDescription = @"";
                    break;

                case MODULE_MARTET_PLACE:
                    IsUsing = false;
                    Title = "Marketplace";
                    IconUrl = "icon-marketplace.png";
                    Description = @"Cocadre settings and support, help";
                    RightsDescription = @"";
                    break;

                case MODULE_EVENT:
                    IsUsing = true;
                    Title = "Event";
                    IconUrl = "icon-event.png";
                    Description = @"Manage all events";
                    RightsDescription = @"Create Quiz Event and view ongoing result before the end of the event";
                    break;

                case MODULE_GAMIFICATION:
                    IsUsing = false;
                    Title = "Gamification";
                    IconUrl = "icon-gamification.png";
                    Description = @"Gamification settings";
                    RightsDescription = @"Change, reset and backup game-related components of CoCadre";
                    break;

                case MODULE_SURVEY:
                    IsUsing = true;
                    Title = "Survey";
                    IconUrl = "icon-survey.png";
                    Description = @"Responsive Survey";
                    RightsDescription = @"Survey creation and report";
                    break;
                default:
                    break;
            }
        }

        public static List<Module> GetModules(int accountTypeCode)
        {
            List<Module> modules = new List<Module>();
            if (accountTypeCode == User.AccountType.CODE_ADMIN || accountTypeCode == User.AccountType.CODE_SUPER_ADMIN)
            {
                modules.Add(new Module(MODULE_PERSONNEL));
                modules.Add(new Module(MODULE_MATCHUP));
                modules.Add(new Module(MODULE_FEED));
                modules.Add(new Module(MODULE_DASHBOARD));
                modules.Add(new Module(MODULE_ANALYTICS));
                modules.Add(new Module(MODULE_COMPANY_PROFILE));

                modules.Add(new Module(MODULE_ADMIN_LOG));
                modules.Add(new Module(MODULE_SUPPORT));
                modules.Add(new Module(MODULE_ANNOUNCEMENT));
                modules.Add(new Module(MODULE_BILLING));
                modules.Add(new Module(MODULE_MARTET_PLACE));
                modules.Add(new Module(MODULE_EVENT));
                modules.Add(new Module(MODULE_GAMIFICATION));

                modules.Add(new Module(MODULE_SURVEY));
            }
            else if (accountTypeCode == User.AccountType.CODE_MODERATER)
            {
                modules.Add(new Module(MODULE_MATCHUP));
                modules.Add(new Module(MODULE_DASHBOARD));
                modules.Add(new Module(MODULE_ANALYTICS));
                modules.Add(new Module(MODULE_ANNOUNCEMENT));
                modules.Add(new Module(MODULE_EVENT));
                modules.Add(new Module(MODULE_GAMIFICATION));

                modules.Add(new Module(MODULE_SURVEY));
            }
            return modules;
        }
    }
}
