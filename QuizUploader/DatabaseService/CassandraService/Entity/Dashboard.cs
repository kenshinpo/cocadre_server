﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    public class Dashboard
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [Serializable]
        public enum ReportState
        {
            [EnumMember]
            Normal = 0,

            [EnumMember]
            Active = 1,

            [EnumMember]
            Acknowledge = 2,

            [EnumMember]
            Hidden = 3,

            [EnumMember]
            Delete = 4
        }

        [Serializable]
        public enum ReportType
        {
            [EnumMember]
            Feed = 1,

            [EnumMember]
            Comment = 2,

            [EnumMember]
            Reply = 3
        }

        [Serializable]
        public enum FeedbackState
        {
            [EnumMember]
            Active = 1,

            [EnumMember]
            Acknowledged = 2,

            [EnumMember]
            Hidden = 3
        }

        [Serializable]
        public enum ProfileApprovalState
        {
            [EnumMember]
            Pending = 2,

            [EnumMember]
            Approved = 1,

            [EnumMember]
            Reject = -1
        }

        public DashboardReportResponse ReportFeed(string reportedUserId, string reason, string companyId, string feedId, string commentId, string replyId, int reportType)
        {
            DashboardReportResponse response = new DashboardReportResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();

                ErrorStatus es = vh.isValidatedAsUser(reportedUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(reason))
                {
                    Log.Error("Empty reason");
                    response.ErrorCode = Int16.Parse(ErrorCode.DashboardReportInvalidReason);
                    response.ErrorMessage = ErrorMessage.DashboardReportInvalidReason;
                    return response;
                }

                Feed feed = new Feed();
                Row feedRow = vh.ValidateFeedPost(feedId, companyId, session);

                if (feedRow == null)
                {
                    Log.Error("Invalid feedId: " + feedId);
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                Department department = new Department();
                bool isForEveryone = feedRow.GetValue<bool>("is_for_everyone");
                bool isForDepartment = feedRow.GetValue<bool>("is_for_department");
                bool isForUser = feedRow.GetValue<bool>("is_for_user");
                bool isPostedByAdmin = feedRow.GetValue<bool>("is_posted_by_admin");

                List<Department> departments = department.GetAllDepartmentByUserId(reportedUserId, companyId, session).Departments;
                List<string> departmentIds = new List<string>();

                foreach (Department depart in departments)
                {
                    departmentIds.Add(depart.Id);
                }

                if (!feed.CheckPostForCurrentUser(reportedUserId, feedId, isForEveryone, isForDepartment, isForUser, departmentIds, session))
                {
                    Log.Error(string.Format("User {0} not allowed to view feed post: {1}", reportedUserId, feedId));
                    response.ErrorCode = Int16.Parse(ErrorCode.FeedPermissionNotGranted);
                    response.ErrorMessage = ErrorMessage.FeedPermissionNotGranted;
                    return response;
                }


                if (reportType == (int)ReportType.Feed)
                {
                    commentId = "NA";
                    replyId = "NA";
                }
                else if (reportType == (int)ReportType.Comment || reportType == (int)ReportType.Reply)
                {
                    Row commentRow = vh.ValidateFeedComment(feedId, commentId, session);
                    if (commentRow == null)
                    {
                        Log.Error("Invalid commentId: " + commentId);
                        response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        return response;
                    }

                    if (reportType == (int)ReportType.Comment)
                    {
                        replyId = "NA";
                    }
                    else
                    {
                        Row replyRow = vh.ValidateCommentReply(commentId, replyId, session);

                        if (replyRow == null)
                        {
                            Log.Error("Invalid replyId: " + replyId);
                            response.ErrorCode = Int16.Parse(ErrorCode.FeedInvalidReply);
                            response.ErrorMessage = ErrorMessage.FeedInvalidReply;
                            return response;
                        }
                    }
                }


                BatchStatement updateBatchStatement = new BatchStatement();

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("dashboard_feed_report",
                    new List<string>(), new List<string> { "company_id", "feed_id", "report_state" }));
                Row reportRow = session.Execute(preparedStatement.Bind(companyId, feedId, (int)ReportState.Active)).FirstOrDefault();

                string reportId = UUIDGenerator.GenerateUniqueIDForReport();
                long currentTime = DateHelper.ConvertDateToLong(DateTime.UtcNow);
                if (reportRow == null)
                {
                    string postReportId = UUIDGenerator.GenerateUniqueIDForReport();
                    DateTime feedCreatedTimestamp = feedRow.GetValue<DateTime>("feed_created_on_timestamp");

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report",
                        new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                    updateBatchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, (int)ReportState.Active, feedCreatedTimestamp, currentTime));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_asc",
                        new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                    updateBatchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, (int)ReportState.Active, feedCreatedTimestamp, currentTime));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_desc",
                        new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                    updateBatchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, (int)ReportState.Active, feedCreatedTimestamp, currentTime));
                }


                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_user",
                    new List<string> { "report_id", "feed_id", "report_state", "comment_id", "reply_id", "reported_user_id", "reported_reason", "reported_on_timestamp" }));
                updateBatchStatement.Add(preparedStatement.Bind(reportId, feedId, (int)ReportState.Active, commentId, replyId, reportedUserId, reason, currentTime));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_user_timestamp_asc",
                     new List<string> { "report_id", "feed_id", "report_state", "comment_id", "reply_id", "reported_user_id", "reported_reason", "reported_on_timestamp" }));
                updateBatchStatement.Add(preparedStatement.Bind(reportId, feedId, (int)ReportState.Active, commentId, replyId, reportedUserId, reason, currentTime));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_user_timestamp_desc",
                     new List<string> { "report_id", "feed_id", "report_state", "comment_id", "reply_id", "reported_user_id", "reported_reason", "reported_on_timestamp" }));
                updateBatchStatement.Add(preparedStatement.Bind(reportId, feedId, (int)ReportState.Active, commentId, replyId, reportedUserId, reason, currentTime));

                session.Execute(updateBatchStatement);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public DashboardSelectReportedFeedResponse SelectReportedFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            DashboardSelectReportedFeedResponse response = new DashboardSelectReportedFeedResponse();
            response.FeedPosts = new List<Feed>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psDashboardFeedReport = session.Prepare(CQLGenerator.SelectStatement("dashboard_feed_report_by_timestamp_desc",
                    new List<string>(), new List<string> { "company_id", "report_state" }));
                BoundStatement bsDashboardFeedReport = psDashboardFeedReport.Bind(companyId, reportedState);
                RowSet dashboardFeedRowSet = session.Execute(bsDashboardFeedReport);

                Feed reportedFeed = null;
                User user = new User();

                foreach (Row dashboardFeedRow in dashboardFeedRowSet)
                {
                    reportedFeed = SelectFeedForReport(dashboardFeedRow, companyId, adminUserId, reportedState, session, false);
                    if (reportedFeed != null)
                    {
                        response.FeedPosts.Add(reportedFeed);
                    }

                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        private Feed SelectFeedForReport(Row dashboardFeedRow, string companyId, string adminUserId, int reportedState, ISession session, bool checkForValid = true)
        {
            Feed feed = new Feed();
            Feed reportedFeed = null;
            try
            {
                string reportPostId = string.Empty;
                if(dashboardFeedRow.GetColumn("report_post_id") != null)
                {
                    reportPostId = dashboardFeedRow.GetValue<string>("report_post_id");
                }
                
                string feedId = dashboardFeedRow.GetValue<string>("feed_id");
                int reportPostState = dashboardFeedRow.GetValue<int>("report_state");

                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                PreparedStatement preparedStatement = null;

                Row feedPrivacyRow = null;

                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("feed_privacy",
                new List<string>(), new List<string> { "company_id", "feed_id" }));
                feedPrivacyRow = session.Execute(preparedStatement.Bind(companyId, feedId)).FirstOrDefault();

                if (feedPrivacyRow == null)
                {
                    Log.Debug("Feed post is already invalidated: " + feedId);
                    return reportedFeed;
                }

                int feedType = feedPrivacyRow.GetValue<int>("feed_type");
                string ownerUserId = feedPrivacyRow.GetValue<string>("owner_user_id");
                DateTime feedCreatedOnTimestamp = feedPrivacyRow.GetValue<DateTime>("feed_created_on_timestamp");

                reportedFeed = feed.GetFeed(feedId, feedType, feedCreatedOnTimestamp, adminUserId, new User().SelectUserBasic(ownerUserId, companyId, false, session).User, session, checkForValid, companyId, true);

                if (reportedFeed != null)
                {
                    reportedFeed.Comments = new List<Comment>();

                    #region Get reported post on feed
                    reportedFeed.ReportPostState = reportPostState;
                    reportedFeed.ReportPostId = reportPostId;
                    reportedFeed.ReportedFeedPosts = SelectReportedPost(feedId, "NA", "NA", companyId, reportedState, session, true);

                    if (reportedFeed.ReportedFeedPosts.Count == 0)
                    {
                        reportedFeed.ReportState = (int)ReportState.Normal;
                    }
                    else
                    {
                        reportedFeed.ReportState = reportPostState;
                    }
                    #endregion


                    #region Comments with replies
                    List<Comment> comments = SelectCommentForReportedFeed(feedId, reportPostId, companyId, reportedState, session, false, true);
                    reportedFeed.Comments = comments;

                    #endregion Comments with replies
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return reportedFeed;
        }

        private List<Comment> SelectCommentForReportedFeed(string feedId, string reportPostId, string companyId, int reportedState, ISession session, bool checkForValid = true, bool isConvertToTimezone = false)
        {
            Feed feed = new Feed();
            List<Comment> comments = new List<Comment>();

            try
            {
                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc",
                    new List<string> { "comment_id", "comment_type", "commentor_user_id" }, new List<string> { "feed_id" }));
                RowSet commentByTimestampRowset = session.Execute(preparedStatement.Bind(feedId));

                foreach (Row commentByTimestampRow in commentByTimestampRowset)
                {
                    string commentId = commentByTimestampRow.GetValue<string>("comment_id");
                    int commentType = commentByTimestampRow.GetValue<int>("comment_type");

                    Comment comment = null;

                    // Get counter
                    int numberOfReplies = feed.GetNumberOfRepliesForComment(feedId, commentId, session);

                    // Get pointers
                    Dictionary<string, int> pointDict = feed.GetPointsForComment(feedId, commentId, session);
                    int negativePoints = !pointDict.ContainsKey("negativePoint") ? 0 : pointDict["negativePoint"];
                    int positivePoints = !pointDict.ContainsKey("positivePoint") ? 0 : pointDict["positivePoint"];

                    if (commentType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        PreparedStatement psCommentText = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                            new List<string>(), new List<string> { "id", "feed_id" }));
                        BoundStatement bsCommentText = psCommentText.Bind(commentId, feedId);

                        Row commentTextRow = session.Execute(bsCommentText).FirstOrDefault();

                        if (commentTextRow != null)
                        {
                            int validStatus = commentTextRow.GetValue<int>("valid_status");
                            if (validStatus >= (int)Feed.FeedValidStatus.Hidden)
                            {
                                string commentContent = commentTextRow.GetValue<string>("content");
                                string commentorUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                                User commentorUser = new User().SelectUserBasic(commentorUserId, companyId, false, session).User;

                                DateTime createdOnTimestamp = commentTextRow.GetValue<DateTime>("created_on_timestamp");

                                #region Replies
                                List<Reply> replies = SelectReplyForReportedComment(feedId, reportPostId, commentId, companyId, reportedState, session, false, true);
                                #endregion

                                comment = new Comment
                                {
                                    FeedId = feedId,
                                    CommentId = commentId,
                                    CommentType = commentType,
                                    CommentText = new CommentText
                                    {
                                        Content = commentContent
                                    },
                                    Replies = replies,
                                    NegativePoints = negativePoints,
                                    PositivePoints = positivePoints,
                                    NumberOfReplies = numberOfReplies,
                                    HasVoted = false,
                                    IsUpVoted = false,
                                    User = commentorUser,
                                    CreatedOnTimestamp = !isConvertToTimezone ? createdOnTimestamp : DateHelper.ConvertDateToTimezoneSpecific(createdOnTimestamp, companyId, session),
                                    ReportPostId = reportPostId,
                                    ReportState = reportedState,
                                    ReportedCommentPosts = SelectReportedPost(feedId, commentId, "NA", companyId, reportedState, session, true)
                                };

                                comments.Add(comment);
                            }

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return comments;
        }

        private List<Reply> SelectReplyForReportedComment(string feedId, string reportPostId, string commentId, string companyId, int reportedState, ISession session, bool checkForValid = true, bool isConvertToTimezone = false)
        {
            Feed feed = new Feed();

            List<Reply> replies = new List<Reply>();

            try
            {
                PreparedStatement psReplyByTimestamp = session.Prepare(CQLGenerator.SelectStatement("reply_by_comment_timestamp_asc",
                                                 new List<string> { "reply_id", "reply_type", "commentor_user_id" }, new List<string> { "comment_id" }));
                BoundStatement bsReplyByTimestamp = psReplyByTimestamp.Bind(commentId);

                RowSet replyByTimestampRowset = session.Execute(bsReplyByTimestamp);

                foreach (Row replyByTimestampRow in replyByTimestampRowset)
                {
                    string replyId = replyByTimestampRow.GetValue<string>("reply_id");
                    int replyType = replyByTimestampRow.GetValue<int>("reply_type");

                    // Get pointers
                    Dictionary<string, int> replyPointDict = feed.GetPointsForReply(commentId, replyId, session);
                    int replyNegativePoints = !replyPointDict.ContainsKey("negativePoint") ? 0 : replyPointDict["negativePoint"];
                    int replyPositivePoints = !replyPointDict.ContainsKey("positivePoint") ? 0 : replyPointDict["positivePoint"];

                    if (replyType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        PreparedStatement psReplyText = session.Prepare(CQLGenerator.SelectStatement("feed_reply_text",
                            new List<string>(), new List<string> { "id", "comment_id" }));
                        BoundStatement bsReplyText = psReplyText.Bind(replyId, commentId);

                        Row replyTextRow = session.Execute(bsReplyText).FirstOrDefault();

                        if (replyTextRow != null)
                        {
                            int validStatus = replyTextRow.GetValue<int>("valid_status");
                            if (validStatus >= (int)Feed.FeedValidStatus.Hidden)
                            {
                                string repliedContent = replyTextRow.GetValue<string>("content");
                                string repliedUserId = replyTextRow.GetValue<string>("user_id");
                                User repliedUser = new User().SelectUserBasic(repliedUserId, companyId, false, session).User;
                                DateTime repliedCreatedOnTimestamp = replyTextRow.GetValue<DateTime>("created_on_timestamp");

                                Reply reply = new Reply
                                {
                                    FeedId = feedId,
                                    CommentId = commentId,
                                    ReplyId = replyId,
                                    ReplyType = replyType,
                                    ReplyText = new ReplyText
                                    {
                                        Content = repliedContent
                                    },
                                    NegativePoints = replyNegativePoints,
                                    PositivePoints = replyPositivePoints,
                                    HasVoted = false,
                                    IsUpVoted = false,
                                    User = repliedUser,
                                    CreatedOnTimestamp = !isConvertToTimezone ? repliedCreatedOnTimestamp : DateHelper.ConvertDateToTimezoneSpecific(repliedCreatedOnTimestamp, companyId, session),
                                    ReportPostId = reportPostId,
                                    ReportState = reportedState,
                                    ReportedReplyPosts = SelectReportedPost(feedId, commentId, replyId, companyId, reportedState, session, true)
                                };

                                replies.Add(reply);
                            }

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return replies;
        }

        private List<ReportedPost> SelectReportedPost(string feedId, string commentId, string replyId, string companyId, int reportedState, ISession session, bool isConvertToTimezone = false)
        {
            List<ReportedPost> reportedPosts = new List<ReportedPost>();

            try
            {
                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("dashboard_feed_report_by_user_timestamp_asc",
                                 new List<string>(), new List<string> { "feed_id", "report_state", "comment_id", "reply_id" }));
                RowSet reportedFeedRowSet = session.Execute(preparedStatement.Bind(feedId, reportedState, commentId, replyId));

                foreach (Row reportedFeedRow in reportedFeedRowSet)
                {
                    string reportId = reportedFeedRow.GetValue<string>("report_id");
                    string reportedUserId = reportedFeedRow.GetValue<string>("reported_user_id");
                    string reportedReason = reportedFeedRow.GetValue<string>("reported_reason");
                    int reportState = reportedFeedRow.GetValue<int>("report_state");
                    DateTimeOffset reportedTimestamp = reportedFeedRow.GetValue<DateTimeOffset>("reported_on_timestamp");

                    ReportedPost post = new ReportedPost
                    {
                        ReportId = reportId,
                        Reason = reportedReason,
                        ReportState = reportState,
                        ReportedTimestamp = !isConvertToTimezone ? reportedTimestamp : DateHelper.ConvertDateToTimezoneSpecific<DateTimeOffset>(reportedTimestamp, companyId, session),
                        ReportedUser = new User().SelectUserBasic(reportedUserId, companyId, false, session).User
                    };

                    reportedPosts.Add(post);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return reportedPosts;
        }

        public bool CreateFeedbackToAdmin(string reportedUserId, string reason, string companyId, string feedbackFeedId, long feedbackCreatedOnTimestamp, ISession session)
        {
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                BatchStatement updateBatchStatement = new BatchStatement();

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feedback",
                        new List<string> { "company_id", "feed_id", "report_state", "feedback_created_on_timestamp", "updated_timestamp" }));
                updateBatchStatement.Add(preparedStatement.Bind(companyId, feedbackFeedId, (int)FeedbackState.Active, feedbackCreatedOnTimestamp, feedbackCreatedOnTimestamp));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feedback_by_timestamp_desc",
                    new List<string> { "company_id", "feed_id", "report_state", "feedback_created_on_timestamp", "updated_timestamp" }));
                updateBatchStatement.Add(preparedStatement.Bind(companyId, feedbackFeedId, (int)FeedbackState.Active, feedbackCreatedOnTimestamp, feedbackCreatedOnTimestamp));

                preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feedback_by_timestamp_asc",
                    new List<string> { "company_id", "feed_id", "report_state", "feedback_created_on_timestamp", "updated_timestamp" }));
                updateBatchStatement.Add(preparedStatement.Bind(companyId, feedbackFeedId, (int)FeedbackState.Active, feedbackCreatedOnTimestamp, feedbackCreatedOnTimestamp));

                session.Execute(updateBatchStatement);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }

        public DashboardSelectFeedbackResponse SelectFeedbackFeedPosts(string adminUserId, string companyId, int reportedState)
        {
            DashboardSelectFeedbackResponse response = new DashboardSelectFeedbackResponse();
            response.Feedbacks = new List<Feed>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psDashboardFeedbackReport = session.Prepare(CQLGenerator.SelectStatement("dashboard_feedback_by_timestamp_desc",
                    new List<string>(), new List<string> { "company_id", "report_state" }));
                BoundStatement bsDashboardFeedbackReport = psDashboardFeedbackReport.Bind(companyId, reportedState);
                RowSet dashboardFeedbackRowSet = session.Execute(bsDashboardFeedbackReport);

                Feed reportedFeed = null;
                User user = new User();
                foreach (Row dashboardFeedbackRow in dashboardFeedbackRowSet)
                {
                    reportedFeed = SelectFeedForReport(dashboardFeedbackRow, companyId, adminUserId, reportedState, session, false);
                    if (reportedFeed != null)
                    {
                        reportedFeed.ReportedFeedPosts = null;
                        reportedFeed.DashboardFeedbackType = reportedState;
                        response.Feedbacks.Add(reportedFeed);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public DashboardUpdateReportStatusResponse UpdateReportStatus(string adminUserId, string companyId, int reportType, string feedId, string reportPostId, int newStatus, string commentId = null, string replyId = null)
        {
            DashboardUpdateReportStatusResponse response = new DashboardUpdateReportStatusResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(feedId))
                {
                    Log.Error("FeedId is null");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                Row feedPrivacyRow = vh.ValidateFeedPost(feedId, companyId, session, false);
                Row commentRow = null;
                Row replyRow = null;

                if (feedPrivacyRow == null)
                {
                    Log.Error("FeedId does not exists: " + feedId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                if (reportType == (int)Dashboard.ReportType.Feed)
                {
                    commentId = "NA";
                    replyId = "NA";
                }
                else if (reportType == (int)Dashboard.ReportType.Comment)
                {
                    if (string.IsNullOrEmpty(commentId))
                    {
                        Log.Error("CommentId is null");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalidComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        return response;
                    }

                    commentRow = vh.ValidateFeedComment(feedId, commentId, session, false);

                    if (commentRow == null)
                    {
                        Log.Error("CommentId does not exists: " + commentId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalidComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        return response;
                    }

                    replyId = "NA";
                }
                else if (reportType == (int)Dashboard.ReportType.Reply)
                {
                    if (string.IsNullOrEmpty(commentId))
                    {
                        Log.Error("CommentId is null");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalidComment);
                        response.ErrorMessage = ErrorMessage.FeedInvalidComment;
                        return response;
                    }

                    if (string.IsNullOrEmpty(replyId))
                    {
                        Log.Error("ReplyId is null");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalidReply);
                        response.ErrorMessage = ErrorMessage.FeedInvalidReply;
                        return response;
                    }

                    replyRow = vh.ValidateCommentReply(commentId, replyId, session, false);

                    if (replyRow == null)
                    {
                        Log.Error("ReplyId does not exists: " + replyId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalidReply);
                        response.ErrorMessage = ErrorMessage.FeedInvalidReply;
                        return response;
                    }
                }

                BatchStatement batchStatement = new BatchStatement();
                PreparedStatement preparedStatement = null;

                int currentStatus = (int)Dashboard.ReportState.Active;

                if (newStatus == (int)Dashboard.ReportState.Active)
                {
                    currentStatus = (int)Dashboard.ReportState.Hidden;
                }

                preparedStatement = session.Prepare(CQLGenerator.SelectStatementWithLimit("dashboard_feed_report_by_user",
                    new List<string>(), new List<string> { "feed_id", "comment_id", "reply_id", "report_state" }, 1));
                Row dashboardReportByUserRow = session.Execute(preparedStatement.Bind(feedId, commentId, replyId, currentStatus)).FirstOrDefault();

                if (dashboardReportByUserRow == null)
                {
                    Log.Error("Report post not found");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DashboardReportNotAvailable);
                    response.ErrorMessage = ErrorMessage.DashboardReportNotAvailable;
                    return response;
                }

                // Acknowledge -> Clear (Feed post still available, dashboard cannot see)
                // Hidden -> Restore
                if ((currentStatus == (int)Dashboard.ReportState.Hidden && newStatus != (int)Dashboard.ReportState.Active) || (currentStatus == (int)Dashboard.ReportState.Acknowledge))
                {
                    Log.Error("Report status already updated by other user");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DashboardReportStatusUpdated);
                    response.ErrorMessage = ErrorMessage.DashboardReportStatusUpdated;
                    return response;
                }

                Feed feed = new Feed();
                DateTimeOffset feedCreatedTimestamp = feedPrivacyRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                // Hidden/Acknowledge/Active
                if (newStatus != (int)Dashboard.ReportState.Delete)
                {
                    preparedStatement = session.Prepare(CQLGenerator.SelectStatement("dashboard_feed_report_by_user",
                        new List<string>(), new List<string> { "feed_id", "comment_id", "reply_id" }));
                    RowSet reportByUserRowset = session.Execute(preparedStatement.Bind(feedId, commentId, replyId));

                    foreach (Row reportByUserRow in reportByUserRowset)
                    {
                        DateTimeOffset reportedTimestamp = reportByUserRow.GetValue<DateTimeOffset>("reported_on_timestamp");
                        string reportId = reportByUserRow.GetValue<string>("report_id");
                        string reportedUserId = reportByUserRow.GetValue<string>("reported_user_id");

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feed_report_by_user",
                            new List<string> { "feed_id", "comment_id", "reply_id", "report_id", "reported_user_id" }, new List<string> { "report_state" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(newStatus, feedId, commentId, replyId, reportId, reportedUserId));

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feed_report_by_user_timestamp_asc",
                            new List<string> { "feed_id", "comment_id", "reply_id", "reported_on_timestamp", "report_id", "reported_user_id" }, new List<string> { "report_state" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(newStatus, feedId, commentId, replyId, reportedTimestamp, reportId, reportedUserId));

                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feed_report_by_user_timestamp_desc",
                            new List<string> { "feed_id", "comment_id", "reply_id", "reported_on_timestamp", "report_id", "reported_user_id" }, new List<string> { "report_state" }, new List<string>()));
                        batchStatement.Add(preparedStatement.Bind(newStatus, feedId, commentId, replyId, reportedTimestamp, reportId, reportedUserId));

                        session.Execute(batchStatement);
                        batchStatement = new BatchStatement();
                    }

                    switch (newStatus)
                    {
                        case (int)ReportState.Hidden:
                            if (reportType == (int)Dashboard.ReportType.Feed)
                            {
                                feed.ToggleFeedVisibilty(feedId, companyId, adminUserId, feedPrivacyRow, true, session);
                            }
                            else if (reportType == (int)Dashboard.ReportType.Comment)
                            {
                                feed.ToggleCommentVisibilty(feedId, commentId, adminUserId, commentRow, true, session);
                            }
                            else if (reportType == (int)Dashboard.ReportType.Reply)
                            {
                                feed.ToggleReplyVisibilty(feedId, commentId, replyId, adminUserId, replyRow, true, session);
                            }
                            break;
                        case (int)ReportState.Active:
                            if (reportType == (int)Dashboard.ReportType.Feed)
                            {
                                feed.ToggleFeedVisibilty(feedId, companyId, adminUserId, feedPrivacyRow, false, session);
                            }
                            else if (reportType == (int)Dashboard.ReportType.Comment)
                            {
                                feed.ToggleCommentVisibilty(feedId, commentId, adminUserId, commentRow, false, session);
                            }
                            else if (reportType == (int)Dashboard.ReportType.Reply)
                            {
                                feed.ToggleReplyVisibilty(feedId, commentId, replyId, adminUserId, replyRow, false, session);
                            }
                            break;
                    }
                }
                // Delete
                else
                {
                    //DeleteReport(session, reportType, feedId, commentId, replyId, companyId, feedCreatedTimestamp);

                    if (reportType == (int)Dashboard.ReportType.Feed)
                    {
                        feed.DeleteFeed(feedId, companyId, null, adminUserId, session);
                    }
                    else if (reportType == (int)Dashboard.ReportType.Comment)
                    {
                        feed.DeleteComment(feedId, commentId, companyId, null, adminUserId, session);
                    }
                    else if (reportType == (int)Dashboard.ReportType.Reply)
                    {
                        feed.DeleteReply(feedId, commentId, replyId, companyId, null, adminUserId, session);
                    }
                }

                // Check overall status
                preparedStatement = session.Prepare(CQLGenerator.SelectStatementWithLimit("dashboard_feed_report_by_user",
                    new List<string>(), new List<string> { "feed_id", "report_state" }, 1));
                dashboardReportByUserRow = session.Execute(preparedStatement.Bind(feedId, currentStatus)).FirstOrDefault();

                if (dashboardReportByUserRow == null)
                {
                    // Check for new status
                    preparedStatement = session.Prepare(CQLGenerator.SelectStatementWithLimit("dashboard_feed_report",
                       new List<string>(), new List<string> { "company_id", "feed_id", "report_state" }, 1));
                    Row reportRow = session.Execute(preparedStatement.Bind(companyId, feedId, newStatus)).FirstOrDefault();

                    if (reportRow == null)
                    {
                        // New status does not exists
                        string postReportId = UUIDGenerator.GenerateUniqueIDForReport();

                        preparedStatement = session.Prepare(CQLGenerator.SelectStatementWithLimit("dashboard_feed_report",
                            new List<string>(), new List<string> { "company_id", "feed_id" }, 1));
                        reportRow = session.Execute(preparedStatement.Bind(companyId, feedId)).FirstOrDefault();

                        if (reportRow != null)
                        {
                            DateTimeOffset reportedOnTimestamp = reportRow.GetValue<DateTimeOffset>("first_reported_on_timestamp");

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report",
                                new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                            batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_asc",
                                new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                            batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_desc",
                                new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                            batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                            session.Execute(batchStatement);
                            batchStatement = new BatchStatement();
                        }

                    }

                    // Delete
                    preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report",
                        new List<string> { "company_id", "feed_id", "report_post_id" }));
                    batchStatement.Add(preparedStatement.Bind(companyId, feedId, reportPostId));

                    preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_timestamp_desc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id", "report_post_id" }));
                    batchStatement.Add(preparedStatement.Bind(companyId, feedCreatedTimestamp, feedId, reportPostId));

                    preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_timestamp_asc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id", "report_post_id" }));
                    batchStatement.Add(preparedStatement.Bind(companyId, feedCreatedTimestamp, feedId, reportPostId));

                    session.Execute(batchStatement);
                    batchStatement = new BatchStatement();
                }
                else
                {
                    preparedStatement = session.Prepare(CQLGenerator.SelectStatementWithLimit("dashboard_feed_report",
                        new List<string>(), new List<string> { "company_id", "feed_id", "report_state" }, 1));
                    Row reportRow = session.Execute(preparedStatement.Bind(companyId, feedId, newStatus)).FirstOrDefault();

                    if (reportRow == null)
                    {
                        string postReportId = UUIDGenerator.GenerateUniqueIDForReport();

                        preparedStatement = session.Prepare(CQLGenerator.SelectStatementWithLimit("dashboard_feed_report",
                            new List<string>(), new List<string> { "company_id", "feed_id" }, 1));
                        reportRow = session.Execute(preparedStatement.Bind(companyId, feedId)).FirstOrDefault();

                        if (reportRow != null)
                        {
                            DateTimeOffset reportedOnTimestamp = reportRow.GetValue<DateTimeOffset>("first_reported_on_timestamp");

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report",
                                new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                            batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_asc",
                                new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                            batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                            preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_desc",
                                new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                            batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                            session.Execute(batchStatement);
                            batchStatement = new BatchStatement();
                        }

                    }
                }

                // Return to admin console
                if (reportType == (int)ReportType.Comment)
                {
                    response.UpdatedComments = SelectCommentForReportedFeed(feedId, reportPostId, companyId, newStatus, session, false, true);
                }
                else if (reportType == (int)ReportType.Reply)
                {
                    response.UpdatedReplies = SelectReplyForReportedComment(feedId, reportPostId, commentId, companyId, newStatus, session, false, true);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool DeleteReport(ISession session, int reportType, string feedId, string commentId, string replyId, string companyId, DateTimeOffset? feedCreatedTimestamp)
        {
            try
            {
                PreparedStatement preparedStatement = null;
                BatchStatement batchStatement = new BatchStatement();

                int newStatus = (int)Dashboard.ReportState.Delete;

                if (reportType == (int)Dashboard.ReportType.Feed)
                {
                    preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report",
                        new List<string> { "company_id", "feed_id" }));
                    batchStatement.Add(preparedStatement.Bind(companyId, feedId));

                    preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_timestamp_desc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }));
                    batchStatement.Add(preparedStatement.Bind(companyId, feedCreatedTimestamp.Value, feedId));

                    preparedStatement = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_timestamp_asc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }));
                    batchStatement.Add(preparedStatement.Bind(companyId, feedCreatedTimestamp.Value, feedId));

                    session.Execute(batchStatement);
                    batchStatement = new BatchStatement();

                    // Delete status does not exists
                    string postReportId = UUIDGenerator.GenerateUniqueIDForReport();

                    DateTimeOffset reportedOnTimestamp = DateTime.UtcNow;

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report",
                        new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_asc",
                        new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_feed_report_by_timestamp_desc",
                        new List<string> { "report_post_id", "company_id", "feed_id", "report_state", "feed_created_on_timestamp", "first_reported_on_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(postReportId, companyId, feedId, newStatus, feedCreatedTimestamp, reportedOnTimestamp));

                    session.Execute(batchStatement);
                    batchStatement = new BatchStatement();

                }

                if (string.IsNullOrEmpty(commentId))
                {
                    commentId = "NA";
                }

                if (string.IsNullOrEmpty(replyId))
                {
                    replyId = "NA";
                }

                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("dashboard_feed_report_by_user",
                       new List<string>(), new List<string> { "feed_id", "comment_id", "reply_id" }));
                RowSet reportByUserRowset = session.Execute(preparedStatement.Bind(feedId, commentId, replyId));

                foreach (Row reportByUserRow in reportByUserRowset)
                {
                    DateTimeOffset reportedTimestamp = reportByUserRow.GetValue<DateTimeOffset>("reported_on_timestamp");
                    string reportId = reportByUserRow.GetValue<string>("report_id");
                    string reportedUserId = reportByUserRow.GetValue<string>("reported_user_id");

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feed_report_by_user",
                        new List<string> { "feed_id", "comment_id", "reply_id", "report_id", "reported_user_id" }, new List<string> { "report_state" }, new List<string>()));
                    batchStatement.Add(preparedStatement.Bind(newStatus, feedId, commentId, replyId, reportId, reportedUserId));

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feed_report_by_user_timestamp_asc",
                        new List<string> { "feed_id", "comment_id", "reply_id", "reported_on_timestamp", "report_id", "reported_user_id" }, new List<string> { "report_state" }, new List<string>()));
                    batchStatement.Add(preparedStatement.Bind(newStatus, feedId, commentId, replyId, reportedTimestamp, reportId, reportedUserId));

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feed_report_by_user_timestamp_desc",
                        new List<string> { "feed_id", "comment_id", "reply_id", "reported_on_timestamp", "report_id", "reported_user_id" }, new List<string> { "report_state" }, new List<string>()));
                    batchStatement.Add(preparedStatement.Bind(newStatus, feedId, commentId, replyId, reportedTimestamp, reportId, reportedUserId));

                    session.Execute(batchStatement);
                    batchStatement = new BatchStatement();
                }



                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }

        public DashboardUpdateFeedbackStatusResponse UpdateFeedbackPostStatus(string adminUserId, string companyId, string feedId, int status)
        {
            DashboardUpdateFeedbackStatusResponse response = new DashboardUpdateFeedbackStatusResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement preparedStatement = null;
                BatchStatement batchStatement = new BatchStatement();

                long currentTimeLong = DateHelper.ConvertDateToLong(DateTime.UtcNow);

                Row feedPrivacyRow = vh.ValidateFeedPost(feedId, companyId, session);

                if (feedPrivacyRow == null)
                {
                    Log.Error("Feedback post is not valid: " + feedId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.FeedInvalid);
                    response.ErrorMessage = ErrorMessage.FeedInvalid;
                    return response;
                }

                preparedStatement = session.Prepare(CQLGenerator.SelectStatement("dashboard_feedback",
                    new List<string>(), new List<string> { "company_id", "feed_id" }));
                Row feedbackRow = session.Execute(preparedStatement.Bind(companyId, feedId)).FirstOrDefault();

                if (feedbackRow == null)
                {
                    Log.Error("Feedback not available");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DashboardFeedbackNotAvailable);
                    response.ErrorMessage = ErrorMessage.DashboardFeedbackNotAvailable;
                    return response;
                }

                DateTimeOffset feedbackCreatedTimestamp = feedbackRow.GetValue<DateTimeOffset>("feedback_created_on_timestamp");

                if (status == (int)Dashboard.FeedbackState.Hidden || status == (int)Dashboard.FeedbackState.Active)
                {
                    bool isFeedValid = false;
                    string tableName = string.Empty;
                    if (status == (int)Dashboard.FeedbackState.Active)
                    {
                        isFeedValid = true;
                    }

                    int feedType = feedPrivacyRow.GetValue<int>("feed_type");
                    if (feedType == Int16.Parse(FeedTypeCode.TextPost))
                    {
                        tableName = "feed_text";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.ImagePost))
                    {
                        tableName = "feed_image";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.VideoPost))
                    {
                        tableName = "feed_video";
                    }
                    else if (feedType == Int16.Parse(FeedTypeCode.SharedUrlPost))
                    {
                        tableName = "feed_shared_url";
                    }

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement(tableName,
                        new List<string> { "id" }, new List<string> { "is_valid", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                    batchStatement = batchStatement.Add(preparedStatement.Bind(isFeedValid, adminUserId, DateTime.UtcNow, feedId));

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy",
                        new List<string> { "company_id", "feed_id" }, new List<string> { "is_feed_valid" }, new List<string>()));
                    batchStatement.Add(preparedStatement.Bind(isFeedValid, companyId, feedId));

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_desc",
                      new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid" }, new List<string>()));
                    batchStatement.Add(preparedStatement.Bind(isFeedValid, companyId, feedbackCreatedTimestamp, feedId));

                    preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("feed_privacy_by_company_timestamp_asc",
                        new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }, new List<string> { "is_feed_valid" }, new List<string>()));
                    batchStatement.Add(preparedStatement.Bind(isFeedValid, companyId, feedbackCreatedTimestamp, feedId));
                }

                preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feedback",
                    new List<string> { "company_id", "feed_id" }, new List<string> { "report_state", "updated_by_admin_user_id", "updated_timestamp" }, new List<string>()));
                batchStatement.Add(preparedStatement.Bind(status, adminUserId, currentTimeLong, companyId, feedId));

                preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feedback_by_timestamp_desc",
                    new List<string> { "company_id", "feedback_created_on_timestamp", "feed_id" }, new List<string> { "report_state", "updated_by_admin_user_id", "updated_timestamp" }, new List<string>()));
                batchStatement.Add(preparedStatement.Bind(status, adminUserId, currentTimeLong, companyId, feedbackCreatedTimestamp, feedId));

                preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_feedback_by_timestamp_asc",
                    new List<string> { "company_id", "feedback_created_on_timestamp", "feed_id" }, new List<string> { "report_state", "updated_by_admin_user_id", "updated_timestamp" }, new List<string>()));
                batchStatement.Add(preparedStatement.Bind(status, adminUserId, currentTimeLong, companyId, feedbackCreatedTimestamp, feedId));

                session.Execute(batchStatement);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public DashboardCreateFeedbackCommentResponse CreateFeedbackComment(string adminUserId,
                                                                            string companyId,
                                                                            string feedId,
                                                                            string content)
        {
            DashboardCreateFeedbackCommentResponse response = new DashboardCreateFeedbackCommentResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Feed feed = new Feed();
                CommentCreateResponse commentResponse = feed.CreateCommentText(adminUserId,
                                                                               companyId,
                                                                               feedId,
                                                                               content,
                                                                               true,
                                                                               session);

                // Select all comments
                if (commentResponse.Success)
                {
                    PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("comment_by_feed_timestamp_asc",
                          new List<string> { "comment_id", "comment_type", "commentor_user_id" }, new List<string> { "feed_id", "is_comment_valid" }));
                    RowSet commentByTimestampRowset = session.Execute(preparedStatement.Bind(feedId, true));

                    List<Comment> comments = new List<Comment>();

                    foreach (Row commentByTimestampRow in commentByTimestampRowset)
                    {
                        string commentId = commentByTimestampRow.GetValue<string>("comment_id");
                        int commentType = commentByTimestampRow.GetValue<int>("comment_type");

                        Comment comment = null;

                        if (commentType == Int16.Parse(FeedTypeCode.TextPost))
                        {
                            PreparedStatement psCommentText = session.Prepare(CQLGenerator.SelectStatement("feed_comment_text",
                                new List<string> { "user_id", "content", "created_on_timestamp" }, new List<string> { "id", "feed_id", "is_valid" }));
                            BoundStatement bsCommentText = psCommentText.Bind(commentId, feedId, true);

                            Row commentTextRow = session.Execute(bsCommentText).FirstOrDefault();

                            if (commentTextRow != null)
                            {
                                string commentContent = commentTextRow.GetValue<string>("content");
                                string commentorUserId = commentByTimestampRow.GetValue<string>("commentor_user_id");

                                User commentorUser = new User().SelectUserBasic(commentorUserId, companyId, false, session).User;

                                DateTime createdOnTimestamp = commentTextRow.GetValue<DateTime>("created_on_timestamp");

                                comment = new Comment
                                {
                                    CommentId = commentId,
                                    CommentType = commentType,
                                    CommentText = new CommentText
                                    {
                                        Content = commentContent
                                    },
                                    User = commentorUser,
                                    CreatedOnTimestamp = createdOnTimestamp
                                };

                                comments.Add(comment);
                            }

                        }
                    }

                    response.Comments = comments;
                    response.Success = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public bool DeleteFeedbackPost(string companyId, string feedId, ISession session)
        {
            try
            {
                BatchStatement batchStatement = new BatchStatement();

                PreparedStatement psDashboardFeedback = session.Prepare(CQLGenerator.SelectStatement("dashboard_feedback",
                    new List<string>(), new List<string> { "company_id", "feed_id" }));
                BoundStatement bsDashboardFeedback = psDashboardFeedback.Bind(companyId, feedId);

                Row dashboardFeedbackRow = session.Execute(bsDashboardFeedback).FirstOrDefault();

                if (dashboardFeedbackRow != null)
                {
                    DateTimeOffset feedbackCreatedTimestamp = dashboardFeedbackRow.GetValue<DateTimeOffset>("dashboard_feedback");
                    psDashboardFeedback = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feedback",
                        new List<string> { "company_id", "feed_id" }));
                    batchStatement.Add(psDashboardFeedback.Bind(companyId, feedId));

                    psDashboardFeedback = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feedback_by_timestamp_asc",
                       new List<string> { "company_id", "feedback_created_on_timestamp", "feed_id" }));
                    batchStatement.Add(psDashboardFeedback.Bind(companyId, feedbackCreatedTimestamp, feedId));

                    psDashboardFeedback = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feedback_by_timestamp_desc",
                        new List<string> { "company_id", "feedback_created_on_timestamp", "feed_id" }));
                    batchStatement.Add(psDashboardFeedback.Bind(companyId, feedbackCreatedTimestamp, feedId));

                    session.Execute(batchStatement);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }

        public bool DeleteReportedFeedPost(string companyId, string feedId, ISession session)
        {
            try
            {

                BatchStatement batchStatement = new BatchStatement();

                if (!string.IsNullOrEmpty(feedId))
                {
                    PreparedStatement psDashboardReport = session.Prepare(CQLGenerator.SelectStatement("dashboard_feed_report",
                        new List<string>(), new List<string> { "company_id", "feed_id" }));
                    BoundStatement bsDashboardReport = psDashboardReport.Bind(companyId, feedId);

                    Row dashboardReportRow = session.Execute(bsDashboardReport).FirstOrDefault();

                    if (dashboardReportRow != null)
                    {
                        DateTimeOffset feedCreatedTimestamp = dashboardReportRow.GetValue<DateTimeOffset>("feed_created_on_timestamp");

                        psDashboardReport = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_timestamp_desc",
                            new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }));
                        batchStatement.Add(psDashboardReport.Bind(companyId, feedCreatedTimestamp, feedId));

                        psDashboardReport = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_timestamp_asc",
                           new List<string> { "company_id", "feed_created_on_timestamp", "feed_id" }));
                        batchStatement.Add(psDashboardReport.Bind(companyId, feedCreatedTimestamp, feedId));

                        psDashboardReport = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_user",
                            new List<string> { "feed_id" }));
                        batchStatement.Add(psDashboardReport.Bind(feedId));

                        psDashboardReport = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_user_timestamp_asc",
                            new List<string> { "feed_id" }));
                        batchStatement.Add(psDashboardReport.Bind(feedId));

                        psDashboardReport = session.Prepare(CQLGenerator.DeleteStatement("dashboard_feed_report_by_user_timestamp_desc",
                            new List<string> { "feed_id" }));
                        batchStatement.Add(psDashboardReport.Bind(feedId));

                        session.Execute(batchStatement);

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }

        public bool CreateApprovalForProfileImage(string approvalId, string userId, string adminUserId, string profileImageUrl, string companyId, DateTimeOffset currentTime, int approvalState, ISession session)
        {
            try
            {
                BatchStatement batchStatement = new BatchStatement();

                if (!string.IsNullOrEmpty(profileImageUrl))
                {
                    PreparedStatement preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_profile_approval",
                        new List<string> { "approval_id", "company_id", "user_id", "profile_image_url", "approval_state", "seek_approval_on_timestamp", "updated_by_admin_user_id", "updated_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(approvalId, companyId, userId, profileImageUrl, approvalState, currentTime, adminUserId, currentTime));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_profile_approval_by_timestamp_asc",
                        new List<string> { "approval_id", "company_id", "user_id", "profile_image_url", "approval_state", "seek_approval_on_timestamp", "updated_by_admin_user_id", "updated_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(approvalId, companyId, userId, profileImageUrl, approvalState, currentTime, adminUserId, currentTime));

                    preparedStatement = session.Prepare(CQLGenerator.InsertStatement("dashboard_profile_approval_by_timestamp_desc",
                        new List<string> { "approval_id", "company_id", "user_id", "profile_image_url", "approval_state", "seek_approval_on_timestamp", "updated_by_admin_user_id", "updated_timestamp" }));
                    batchStatement.Add(preparedStatement.Bind(approvalId, companyId, userId, profileImageUrl, approvalState, currentTime, adminUserId, currentTime));

                    session.Execute(batchStatement);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return false;
        }

        public DashboardSelectApprovalProfileResponse SelectProfileApprovalResponse(string adminUserId, string companyId)
        {
            DashboardSelectApprovalProfileResponse response = new DashboardSelectApprovalProfileResponse();
            response.ProfileImageApprovals = new List<ProfileImageApproval>();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                PreparedStatement psDashboardProfileApproval = session.Prepare(CQLGenerator.SelectStatement("dashboard_profile_approval_by_timestamp_desc",
                    new List<string>(), new List<string> { "company_id", "approval_state" }));
                BoundStatement bsDashboardProfileAppoval = psDashboardProfileApproval.Bind(companyId, (int)Dashboard.ProfileApprovalState.Pending);
                RowSet dashboardProfileApprovalRowSet = session.Execute(bsDashboardProfileAppoval);

                User user = new User();
                foreach (Row dashboardProfileApprovalRow in dashboardProfileApprovalRowSet)
                {
                    string approvalId = dashboardProfileApprovalRow.GetValue<string>("approval_id");
                    string userId = dashboardProfileApprovalRow.GetValue<string>("user_id");
                    string newProfileImageUrl = dashboardProfileApprovalRow.GetValue<string>("profile_image_url");
                    DateTimeOffset seekApprovalTimestamp = dashboardProfileApprovalRow.GetValue<DateTimeOffset>("seek_approval_on_timestamp");

                    User requesterUser = user.SelectUserBasic(userId, companyId, true, session).User;

                    if (requesterUser != null)
                    {
                        ProfileImageApproval approval = new ProfileImageApproval
                        {
                            ApprovalId = approvalId,
                            RequesterUser = requesterUser,
                            NewProfileImageUrl = newProfileImageUrl,
                            SeekApprovalTimestamp = DateHelper.ConvertDateToTimezoneSpecific<DateTimeOffset>(seekApprovalTimestamp, companyId, session)
                        };

                        response.ProfileImageApprovals.Add(approval);
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }

        public DashboardUpdateApprovalProfileResponse UpdateProfileApprovalStatus(string adminUserId, string companyId, string approvalId, int newApprovalState)
        {
            DashboardUpdateApprovalProfileResponse response = new DashboardUpdateApprovalProfileResponse();
            response.Success = false;

            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error("Invalid admin: " + adminUserId);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }


                BatchStatement updatedBatch = new BatchStatement();

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.SelectStatement("dashboard_profile_approval",
                    new List<string>(), new List<string> { "company_id", "approval_id" }));
                Row dashboardProfileApprovalRow = session.Execute(preparedStatement.Bind(companyId, approvalId)).FirstOrDefault();

                if (dashboardProfileApprovalRow == null)
                {
                    Log.Error("No such approval application found: " + approvalId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DashboardApprovalNotAvailable);
                    response.ErrorMessage = ErrorMessage.DashboardApprovalNotAvailable;
                    return response;
                }

                int approvalState = dashboardProfileApprovalRow.GetValue<int>("approval_state");

                if (approvalState != (int)Dashboard.ProfileApprovalState.Pending)
                {
                    Log.Error("Approval application is no longer in pending state: " + approvalId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DashboardApprovalNotPending);
                    response.ErrorMessage = ErrorMessage.DashboardApprovalNotPending;
                    return response;
                }

                DateTimeOffset seekApprovalTimestamp = dashboardProfileApprovalRow.GetValue<DateTimeOffset>("seek_approval_on_timestamp");
                DateTimeOffset currentTimestamp = DateTimeOffset.UtcNow;

                string userId = dashboardProfileApprovalRow.GetValue<string>("user_id");

                es = vh.isValidatedAsUser(userId, companyId, session);

                if (es != null)
                {
                    Log.Error("Invalid user: " + userId);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                string newProfileImageUrl = dashboardProfileApprovalRow.GetValue<string>("profile_image_url");

                preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_profile_approval",
                    new List<string> { "company_id", "approval_id" }, new List<string> { "approval_state", "updated_by_admin_user_id", "updated_timestamp" }, new List<string>()));
                updatedBatch.Add(preparedStatement.Bind(newApprovalState, adminUserId, currentTimestamp, companyId, approvalId));

                preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_profile_approval_by_timestamp_asc",
                    new List<string> { "company_id", "seek_approval_on_timestamp", "approval_id" }, new List<string> { "approval_state", "updated_by_admin_user_id", "updated_timestamp" }, new List<string>()));
                updatedBatch.Add(preparedStatement.Bind(newApprovalState, adminUserId, currentTimestamp, companyId, seekApprovalTimestamp, approvalId));

                preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("dashboard_profile_approval_by_timestamp_desc",
                    new List<string> { "company_id", "seek_approval_on_timestamp", "approval_id" }, new List<string> { "approval_state", "updated_by_admin_user_id", "updated_timestamp" }, new List<string>()));
                updatedBatch.Add(preparedStatement.Bind(newApprovalState, adminUserId, currentTimestamp, companyId, seekApprovalTimestamp, approvalId));

                session.Execute(updatedBatch);

                User user = new User();
                user.UpdateUserProfileImageApprovalInAlbum(userId, adminUserId, approvalId, newApprovalState, currentTimestamp, seekApprovalTimestamp, session);

                if (newApprovalState == (int)Dashboard.ProfileApprovalState.Reject)
                {
                    try
                    {
                        String bucketName = "cocadre-" + companyId.ToLower() + "/users/" + userId;
                        String[] array = newProfileImageUrl.Split('/');
                        newProfileImageUrl = array[array.Length - 1];

                        List<String> keys = new List<string>();
                        keys.Add(newProfileImageUrl);
                        keys.Add(newProfileImageUrl.Replace("_original", "_large"));
                        keys.Add(newProfileImageUrl.Replace("_original", "_medium"));
                        keys.Add(newProfileImageUrl.Replace("_original", "_small"));

                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            for (int i = 0; i < keys.Count; i++)
                            {
                                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = keys[i]
                                };
                                try
                                {
                                    s3Client.DeleteObject(deleteObjectRequest);
                                    Log.Debug("Delete file: " + bucketName + "/" + keys[i]);
                                }
                                catch (AmazonS3Exception s3Exception)
                                {
                                    Log.Error(s3Exception.ToString(), s3Exception);
                                }
                            }
                        }
                    }
                    catch (AmazonS3Exception s3Exception)
                    {
                        Log.Error(s3Exception.Message, s3Exception.InnerException);
                    }
                }
                else if (newApprovalState == (int)Dashboard.ProfileApprovalState.Approved)
                {
                    // Check timestamp of the latest approval list
                    preparedStatement = session.Prepare(CQLGenerator.SelectStatementWithLimit("profile_image_approval_by_user_timestamp_desc",
                    new List<string>(), new List<string> { "user_id", "approval_state" }, 1));

                    Row userApprovalImageRow = session.Execute(preparedStatement.Bind(userId, (int)Dashboard.ProfileApprovalState.Approved)).FirstOrDefault();

                    bool isUpdateUserProfileImage = true;

                    if (userApprovalImageRow != null)
                    {
                        DateTimeOffset approvedTimestamp = userApprovalImageRow.GetValue<DateTimeOffset>("seek_approval_on_timestamp");
                        if (seekApprovalTimestamp < approvedTimestamp)
                        {
                            isUpdateUserProfileImage = false;
                        }
                    }

                    if (isUpdateUserProfileImage)
                    {
                        preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("user_basic",
                            new List<string> { "id" }, new List<string> { "profile_image_url", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                        session.Execute(preparedStatement.Bind(newProfileImageUrl, adminUserId, currentTimestamp, userId));
                    }

                    User approvedUser = new User().SelectUserBasic(userId, companyId, true, session).User;

                    if (approvedUser != null)
                    {
                        response.ApprovedUser = approvedUser;
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }

    public class ProfileImageApproval
    {
        [DataMember(EmitDefaultValue = false)]
        public User RequesterUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string NewProfileImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ApprovalId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int ApprovalState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTimeOffset SeekApprovalTimestamp { get; set; }
    }
}
