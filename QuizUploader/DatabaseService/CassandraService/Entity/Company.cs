﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using Amazon.S3;
using Amazon;
using System.Web.Configuration;
using Amazon.S3.Model;

namespace CassandraService.Entity
{
    [Serializable]
    public class Company
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        [Serializable]
        public class PermissionType
        {
            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ADMIN_ONLY = 1;

            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ADMIN_AND_MODERATER = 2;

            [DataMember(EmitDefaultValue = false)]
            public const int CODE_ALL_PERSONNEL = 3;

            [DataMember(EmitDefaultValue = false)]
            public const int CODE_DEPARTMENT = 4;

            [DataMember(EmitDefaultValue = false)]
            public int Code { get; private set; }

            [DataMember(EmitDefaultValue = false)]
            public string Title { get; private set; }

            public PermissionType(int code)
            {
                switch (code)
                {
                    case CODE_ADMIN_ONLY:
                        Code = CODE_ADMIN_ONLY;
                        Title = "Admin Only";
                        break;
                    case CODE_ADMIN_AND_MODERATER:
                        Code = CODE_ADMIN_AND_MODERATER;
                        Title = "Admin & Moderater";
                        break;
                    case CODE_ALL_PERSONNEL:
                        Code = CODE_ALL_PERSONNEL;
                        Title = "All Personnel";
                        break;
                    case CODE_DEPARTMENT:
                        Code = CODE_DEPARTMENT;
                        Title = "Selected Department Only";
                        break;

                    default:
                        break;
                }
            }
        }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyTitle { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyLogoUrl { get; set; }

        // Website
        [DataMember(EmitDefaultValue = false)]
        public string AdminImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PrimaryManagerId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AdminProfileImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SecondaryEmail { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SupportMessage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CountryTimeZone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ZoneName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public double TimeZoneOffset { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AdminWebsiteHeaderLogoUrl { get; set; }


        // Client
        [DataMember(Name = "CompanyBannerUrl", EmitDefaultValue = false)]
        public string CompanyBannerUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyPullRefreshBannerUrl { get; set; }

        [DataMember(Name = "MatchupBannerUrl", EmitDefaultValue = false)]
        public string MatchupBannerUrl { get; set; }

        [DataMember(Name = "ProfilePopupBannerUrl", EmitDefaultValue = false)]
        public string ProfilePopupBannerUrl { get; set; }

        [DataMember]
        public bool IsPasswordDefault { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User Manager { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public PermissionType CompanyPermissionType { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<string> CompanyPermissionDepartments { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ResetPasswordUrl { get; set; }

        #region ForLogin
        [DataMember]
        public int NumberOfNotifications { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string UserId { get; set; }
        #endregion

        [Serializable]
        public enum CompanyImageType
        {

            [EnumMember]
            AdminProfile = 1,

            [EnumMember]
            EmailSquare = 2,

            [EnumMember]
            WebsiteHeader = 3,

            [EnumMember]
            LoginSelection = 4,

            [EnumMember]
            ClientBanner = 5,

            [EnumMember]
            ClientPullRefresh = 6,

            [EnumMember]
            ClientMatchUp = 7,

            [EnumMember]
            ClientPopUp = 8,
        }

        [Serializable]
        public enum CompanyEmailTemplate
        {

            [EnumMember]
            PersonnelInvite = 1,

            [EnumMember]
            AdminInvite = 2,

            [EnumMember]
            ForgotPassword = 3,

            [EnumMember]
            ResettedPassword = 4
        }

        public GetCompanyPermissionResponse GetCompanyPermission(string companyId, string adminUserId)
        {
            GetCompanyPermissionResponse response = new GetCompanyPermissionResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. Read database.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("setting", new List<string> { }, new List<string> { "company_id", "setting_key" }));
                BoundStatement bs = ps.Bind(companyId, "feed_permission_type");
                Row row = session.Execute(bs).FirstOrDefault();
                if (row != null)
                {
                    Company company = new Company();
                    List<string> listDepartmentId = new List<string>();
                    company.CompanyPermissionType = new PermissionType(Convert.ToInt16(row.GetValue<string>("setting_value")));

                    if (Convert.ToInt16(row.GetValue<string>("setting_value")) == Company.PermissionType.CODE_DEPARTMENT)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_department_allowed", new List<string> { }, new List<string> { "company_id" }));
                        bs = ps.Bind(companyId);
                        RowSet rowSet = session.Execute(bs);
                        foreach (Row item in rowSet)
                        {
                            listDepartmentId.Add(item.GetValue<string>("department_id"));
                        }
                        company.CompanyPermissionDepartments = listDepartmentId;
                    }

                    response.Company = company;
                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.CompanyPermissionError);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyPermissionError);
                    response.ErrorMessage = ErrorMessage.CompanyPermissionError;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdatePermissionResponse UpdateCompanyPermisson(string companyId, string adminUserId, int permisssionType, List<string> allowDepartmentId)
        {
            CompanyUpdatePermissionResponse response = new CompanyUpdatePermissionResponse();
            response.Success = false;
            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }
                #endregion
                #endregion

                #region Step 2. write data.
                PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("setting",
                              new List<string> { "company_id", "setting_key" }, new List<string> { "setting_value" }, new List<string>()));
                BoundStatement bs = ps.Bind(Convert.ToString(permisssionType), companyId, "feed_permission_type");
                session.Execute(bs);

                // Delete > Insert permission_feed_department_allowed
                BatchStatement batchStatement = new BatchStatement();
                ps = session.Prepare(CQLGenerator.SelectStatement("permission_feed_department_allowed", new List<string> { }, new List<string> { "company_id" }));
                bs = ps.Bind(companyId);
                RowSet rowSet = session.Execute(bs);
                if (rowSet != null)
                {
                    foreach (Row item in rowSet)
                    {
                        PreparedStatement ps_permission_feed_department_allowed = session.Prepare(CQLGenerator.DeleteStatement("permission_feed_department_allowed", new List<string> { "company_id", "department_id" }));
                        BoundStatement bs_permission_feed_department_allowed = ps_permission_feed_department_allowed.Bind(companyId, item.GetValue<string>("department_id"));
                        batchStatement.Add(bs_permission_feed_department_allowed);
                    }
                    session.Execute(batchStatement);
                }

                if (allowDepartmentId.Count > 0)
                {
                    batchStatement = new BatchStatement();
                    foreach (string departmentId in allowDepartmentId)
                    {
                        PreparedStatement ps_permission_feed_department_allowed = session.Prepare(CQLGenerator.InsertStatement("permission_feed_department_allowed", new List<string> { "company_id", "department_id", "last_assigned_by_user_id", "last_assigned_on_timestamp" }));
                        batchStatement.Add(ps_permission_feed_department_allowed.Bind(companyId, departmentId, adminUserId, DateTime.UtcNow));
                    }
                }

                session.Execute(batchStatement);
                response.Success = true;
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdateProfileResponse UpdateProfile(string adminUserId, string companyId, string newTitle, string newPrimaryAdminUserId, string newPrimaryAdminSecondaryEmail, string supportMessage, string zoneName, double timezoneOffset)
        {
            CompanyUpdateProfileResponse response = new CompanyUpdateProfileResponse();
            response.Success = false;
            try
            {
                if (string.IsNullOrEmpty(newPrimaryAdminUserId))
                {
                    Log.Error("Primary admin is not valid");
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyMissingPrimaryAdmin);
                    response.ErrorMessage = ErrorMessage.CompanyMissingPrimaryAdmin;
                    return response;
                }

                if (string.IsNullOrEmpty(supportMessage))
                {
                    Log.Error("Support message is empty");
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyMissingSupportMessage);
                    response.ErrorMessage = ErrorMessage.CompanyMissingSupportMessage;
                    return response;
                }

                if (string.IsNullOrEmpty(zoneName))
                {
                    Log.Error("Zone name is invalid");
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyMissingTimezoneCountryName);
                    response.ErrorMessage = ErrorMessage.CompanyMissingTimezoneCountryName;
                    return response;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                string currentPrimaryAdminUserId = companyRow.GetValue<string>("created_by_user_id");
                string currentPrimaryAdminSecondaryEmail = companyRow.GetValue<string>("secondary_email");

                if (!currentPrimaryAdminUserId.Equals(adminUserId) && !currentPrimaryAdminUserId.Equals(newPrimaryAdminUserId)) //&& !currentPrimaryAdminSecondaryEmail.Equals(newPrimaryAdminSecondaryEmail))
                {
                    Log.Error("Admin not authorized: " + adminUserId);
                    response.ErrorCode = Int16.Parse(ErrorCode.CompanyAdminUserNotPrimaryAdmin);
                    response.ErrorMessage = ErrorMessage.CompanyAdminUserNotPrimaryAdmin;
                    return response;
                }

                #region Check new primary admin account status.
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("user_account_type", new List<string> { }, new List<string> { "user_id" }));
                BoundStatement bs = ps.Bind(newPrimaryAdminUserId);
                Row row = session.Execute(bs).FirstOrDefault();
                if (row != null)
                {
                    if (row.GetValue<int>("account_status") < 0)
                    {
                        Log.Error("Admin account is not valid");
                        response.ErrorCode = Int16.Parse(ErrorCode.UserNoAdmin);
                        response.ErrorMessage = ErrorMessage.UserNoAdmin;
                        return response;
                    }
                }
                else
                {
                    Log.Error("Admin account is not valid");
                    response.ErrorCode = Int16.Parse(ErrorCode.UserNoAdmin);
                    response.ErrorMessage = ErrorMessage.UserNoAdmin;
                    return response;
                }
                #endregion

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("company",
                    new List<string> { "id" }, new List<string> { "title", "support_message", "secondary_email", "country_timezone", "zone_name", "timezone_offset", "created_by_user_id", "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(preparedStatement.Bind(newTitle, supportMessage, newPrimaryAdminSecondaryEmail, null, zoneName, timezoneOffset, newPrimaryAdminUserId, adminUserId, DateTime.UtcNow, companyId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySelectProfileResponse SelectProfile(string adminUserId, string companyId)
        {
            CompanySelectProfileResponse response = new CompanySelectProfileResponse();
            response.Timezones = new List<Timezone>();
            response.CompanyProfile = new CompanyProfile();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                string primaryAdminUserId = string.Empty;

                if (companyRow["primary_admin_user_id"] == null)
                {
                    string createdByUserId = companyRow.GetValue<string>("created_by_user_id");

                    PreparedStatement ps = session.Prepare(CQLGenerator.UpdateStatement("company", new List<string> { "id" }, new List<string> { "primary_admin_user_id" }, new List<string>()));
                    session.Execute(ps.Bind(createdByUserId, companyId));

                    primaryAdminUserId = createdByUserId;
                }
                else
                {
                    primaryAdminUserId = companyRow.GetValue<string>("primary_admin_user_id");
                }

                response.CompanyProfile.Title = companyRow.GetValue<string>("title");
                response.CompanyProfile.PrimaryAdminUser = new User().SelectUserBasic(primaryAdminUserId, companyId, true, session).User;
                response.CompanyProfile.PrimaryAdminSecondaryEmail = companyRow["secondary_email"] == null ? string.Empty : companyRow.GetValue<string>("secondary_email");
                response.CompanyProfile.IsPrimaryAdmin = companyRow.GetValue<string>("created_by_user_id").Equals(adminUserId);
                response.CompanyProfile.SupportMessage = companyRow["support_message"] == null ? string.Empty : companyRow.GetValue<string>("support_message");
                response.CompanyProfile.CountryTimezone = companyRow["country_timezone"] == null ? string.Empty : companyRow.GetValue<string>("country_timezone");
                response.CompanyProfile.ZoneName = companyRow["zone_name"] == null ? string.Empty : companyRow.GetValue<string>("zone_name");
                response.CompanyProfile.TimezoneOffset = companyRow["timezone_offset"] == null ? 0.0 : companyRow.GetValue<double>("timezone_offset");

                PreparedStatement psTimezone = session.Prepare(CQLGenerator.SelectStatement("timezone",
                    new List<string>(), new List<string> { "zone_pk" }));
                RowSet timezoneRowset = session.Execute(psTimezone.Bind("timezone"));

                foreach (Row timezoneRow in timezoneRowset)
                {
                    Timezone timezone = new Timezone
                    {
                        Offset = timezoneRow.GetValue<double>("offset"),
                        ZoneName = timezoneRow.GetValue<string>("zone_name"),
                        Title = timezoneRow.GetValue<string>("title"),
                        IsSelected = timezoneRow.GetValue<string>("zone_name").Equals(response.CompanyProfile.ZoneName) ? true : false
                    };
                    response.Timezones.Add(timezone);
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySelectImageResponse SelectEmailLogo(string adminUserId, string companyId, int imageType, ISession session = null, Row companyRow = null)
        {
            CompanySelectImageResponse response = new CompanySelectImageResponse();
            response.Success = false;
            try
            {
                if (session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();

                    ValidationHandler vh = new ValidationHandler();
                    ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                    if (es != null)
                    {
                        Log.Error(es.ErrorMessage);
                        response.ErrorCode = es.ErrorCode;
                        response.ErrorMessage = es.ErrorMessage;
                        return response;
                    }

                    companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow == null)
                    {
                        Log.Error("Invalid companyId: " + companyId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                        response.ErrorMessage = ErrorMessage.CompanyInvalid;
                        return response;
                    }
                }

                string defaultImageUrl = string.Empty;
                string column = string.Empty;

                switch (imageType)
                {
                    case (int)CompanyImageType.AdminProfile:
                        defaultImageUrl = DefaultResource.CompanyAdminPhotoUrl;
                        column = "admin_profile_image_url";
                        break;

                    case (int)CompanyImageType.EmailSquare:
                        defaultImageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                        column = "email_square_logo_url";
                        break;

                    case (int)CompanyImageType.WebsiteHeader:
                        defaultImageUrl = DefaultResource.CompanyHeaderImageUrl;
                        column = "admin_website_header_logo_url";
                        break;

                    case (int)CompanyImageType.LoginSelection:
                        defaultImageUrl = DefaultResource.CompanyLogoUrl;
                        column = "logo_url";
                        break;

                    case (int)CompanyImageType.ClientBanner:
                        defaultImageUrl = DefaultResource.CompanyClientBannerUrl;
                        column = "client_banner_url";
                        break;

                    case (int)CompanyImageType.ClientPullRefresh:
                        defaultImageUrl = DefaultResource.CompanyClientPullRefreshImageUrl;
                        column = "client_pull_refresh_banner_url";
                        break;

                    case (int)CompanyImageType.ClientMatchUp:
                        defaultImageUrl = DefaultResource.CompanyMatchupBannerUrl;
                        column = "client_matchup_banner_url";
                        break;
                    case (int)CompanyImageType.ClientPopUp:
                        defaultImageUrl = DefaultResource.CompanyProfilePopupUrl;
                        column = "client_profile_popup_banner_url";
                        break;
                }

                response.ImageUrl = companyRow[column] == null ? defaultImageUrl : companyRow.GetValue<string>(column);
                response.Success = true;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdateProfileResponse UpdateImage(string adminUserId, string companyId, string imageUrl, int imageType)
        {
            CompanyUpdateProfileResponse response = new CompanyUpdateProfileResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                string currentImageUrl = string.Empty;
                string defaultImageUrl = string.Empty;
                string column = string.Empty;

                switch (imageType)
                {
                    case (int)CompanyImageType.AdminProfile:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyAdminPhotoUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyAdminPhotoUrl;
                        column = "admin_profile_image_url";
                        break;

                    case (int)CompanyImageType.EmailSquare:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyEmailSquareImageUrl;
                        column = "email_square_logo_url";
                        break;

                    case (int)CompanyImageType.WebsiteHeader:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyHeaderImageUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyHeaderImageUrl;
                        column = "admin_website_header_logo_url";
                        break;

                    case (int)CompanyImageType.LoginSelection:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyLogoUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyLogoUrl;
                        column = "logo_url";
                        break;

                    case (int)CompanyImageType.ClientBanner:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyClientBannerUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyClientBannerUrl;
                        column = "client_banner_url";
                        break;

                    case (int)CompanyImageType.ClientPullRefresh:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyClientPullRefreshImageUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyClientPullRefreshImageUrl;
                        column = "client_pull_refresh_banner_url";
                        break;

                    case (int)CompanyImageType.ClientMatchUp:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyMatchupBannerUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyMatchupBannerUrl;
                        column = "client_matchup_banner_url";
                        break;
                    case (int)CompanyImageType.ClientPopUp:
                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            imageUrl = DefaultResource.CompanyProfilePopupUrl;
                        }
                        defaultImageUrl = DefaultResource.CompanyProfilePopupUrl;
                        column = "client_profile_popup_banner_url";
                        break;
                }


                currentImageUrl = companyRow.GetValue<string>(column);

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("company",
                    new List<string> { "id" }, new List<string> { column, "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(preparedStatement.Bind(imageUrl, adminUserId, DateTime.UtcNow, companyId));

                response.Success = true;

                // Remove from S3
                if (!currentImageUrl.Equals(defaultImageUrl))
                {
                    String bucketName = "cocadre-" + companyId.ToLower();
                    using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                    {
                        string path = currentImageUrl.Replace("https://", "");
                        string[] splitPath = path.Split('/');
                        string imageName = splitPath[splitPath.Count() - 1];

                        DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = "company/" + imageName
                        };

                        s3Client.DeleteObject(deleteObjectRequest);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySelectEmailTemplateResponse SelectEmailTemplate(string adminUserId, string companyId)
        {
            CompanySelectEmailTemplateResponse response = new CompanySelectEmailTemplateResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                response.EmailSquareLogoUrl = SelectEmailLogo(adminUserId, companyId, (int)CompanyImageType.EmailSquare, session, companyRow).ImageUrl;

                response.EmailPersonnelInvitationTitle = companyRow.GetValue<string>("email_personnel_invitation_title");
                response.EmailPersonnelInvitationDescription = companyRow.GetValue<string>("email_personnel_invitation_description");
                response.EmailPersonnelInvitationSupportInfo = companyRow.GetValue<string>("email_personnel_invitation_support_info");

                response.EmailAdminInvitationTitle = companyRow.GetValue<string>("email_admin_invitation_title");
                response.EmailAdminInvitationDescription = companyRow.GetValue<string>("email_admin_invitation_description");
                response.EmailAdminInvitationSupportInfo = companyRow.GetValue<string>("email_admin_invitation_support_info");

                response.EmailForgotPasswordTitle = companyRow.GetValue<string>("email_forgot_password_title");
                response.EmailForgotPasswordDescription = companyRow.GetValue<string>("email_forgot_password_description");
                response.EmailForgotPasswordSupportInfo = companyRow.GetValue<string>("email_forgot_password_support_info");

                response.EmailResetPasswordTitle = companyRow.GetValue<string>("email_reset_password_title");
                response.EmailResetPasswordDescription = companyRow.GetValue<string>("email_reset_password_description");
                response.EmailResetPasswordSupportInfo = companyRow.GetValue<string>("email_reset_password_support_info");

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanySelectEmailDetailResponse SelectEmailTemplate(string companyId, int emailType, ISession session = null, Row companyRow = null)
        {
            CompanySelectEmailDetailResponse response = new CompanySelectEmailDetailResponse();
            response.Success = false;
            try
            {
                ValidationHandler vh = new ValidationHandler();

                if(session == null)
                {
                    ConnectionManager cm = new ConnectionManager();
                    session = cm.getMainSession();
                }

                if(companyRow == null)
                {
                    companyRow = vh.ValidateCompany(companyId, session);
                    if (companyRow == null)
                    {
                        Log.Error("Invalid companyId: " + companyId);
                        response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                        response.ErrorMessage = ErrorMessage.CompanyInvalid;
                        return response;
                    }
                }

                response.EmailLogoUrl = SelectEmailLogo(null, companyId, (int)CompanyImageType.EmailSquare, session, companyRow).ImageUrl;

                switch (emailType)
                {
                    case (int)CompanyEmailTemplate.PersonnelInvite:
                        response.EmailTitle = companyRow.GetValue<string>("email_personnel_invitation_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_personnel_invitation_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_personnel_invitation_support_info");
                        break;
                    case (int)CompanyEmailTemplate.AdminInvite:

                        response.EmailTitle = companyRow.GetValue<string>("email_admin_invitation_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_admin_invitation_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_admin_invitation_support_info");
                        break;
                    case (int)CompanyEmailTemplate.ForgotPassword:
                        response.EmailTitle = companyRow.GetValue<string>("email_forgot_password_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_forgot_password_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_forgot_password_support_info");
                        break;
                    case (int)CompanyEmailTemplate.ResettedPassword:
                        response.EmailTitle = companyRow.GetValue<string>("email_reset_password_title");
                        response.EmailDescription = companyRow.GetValue<string>("email_reset_password_description");
                        response.EmailSupportInfo = companyRow.GetValue<string>("email_reset_password_support_info");
                        break;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyUpdateProfileResponse UpdateEmailTemplate(string adminUserId, string companyId, int updateType, string emailTitle, string emailDescription, string emailSupportInfo)
        {
            CompanyUpdateProfileResponse response = new CompanyUpdateProfileResponse();
            response.Success = false;
            try
            {
                string titleColumn = string.Empty;
                string descriptionColumn = string.Empty;
                string supportColumn = string.Empty;

                switch (updateType)
                {
                    case (int)CompanyEmailTemplate.PersonnelInvite:
                        titleColumn = "email_personnel_invitation_title";
                        descriptionColumn = "email_personnel_invitation_description";
                        supportColumn = "email_personnel_invitation_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailPersonnelInvitationTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailPersonnelInvitationDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailPersonnelInvitationSupportInfo;
                        }
                        break;
                    case (int)CompanyEmailTemplate.AdminInvite:
                        titleColumn = "email_admin_invitation_title";
                        descriptionColumn = "email_admin_invitation_description";
                        supportColumn = "email_admin_invitation_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailAdminInvitationTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailAdminInvitationDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailAdminInvitationSupportInfo;
                        }
                        break;
                    case (int)CompanyEmailTemplate.ForgotPassword:
                        titleColumn = "email_forgot_password_title";
                        descriptionColumn = "email_forgot_password_description";
                        supportColumn = "email_forgot_password_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailForgotPasswordTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailForgotPasswordDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailForgotPasswordSupportInfo;
                        }
                        break;
                    case (int)CompanyEmailTemplate.ResettedPassword:
                        titleColumn = "email_reset_password_title";
                        descriptionColumn = "email_reset_password_description";
                        supportColumn = "email_reset_password_support_info";
                        if (string.IsNullOrEmpty(emailTitle))
                        {
                            emailTitle = DefaultResource.CompanyEmailResetPasswordTitle;
                        }

                        if (string.IsNullOrEmpty(emailDescription))
                        {
                            emailDescription = DefaultResource.CompanyEmailResetPasswordDescription;
                        }

                        if (string.IsNullOrEmpty(emailSupportInfo))
                        {
                            emailSupportInfo = DefaultResource.CompanyEmailResetPasswordSupportInfo;
                        }
                        break;
                }

                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row companyRow = vh.ValidateCompany(companyId, session);
                if (companyRow == null)
                {
                    Log.Error("Invalid companyId: " + companyId);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.CompanyInvalid);
                    response.ErrorMessage = ErrorMessage.CompanyInvalid;
                    return response;
                }

                PreparedStatement preparedStatement = session.Prepare(CQLGenerator.UpdateStatement("company",
                    new List<string> { "id" }, new List<string> { titleColumn, descriptionColumn, supportColumn, "last_modified_by_user_id", "last_modified_timestamp" }, new List<string>()));
                session.Execute(preparedStatement.Bind(emailTitle, emailDescription, emailSupportInfo, adminUserId, DateTime.UtcNow, companyId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public CompanyResponse GetCompany(string companyId)
        {

            CompanyResponse response = new CompanyResponse();
            response.Success = false;

            try
            {
                #region Step 1. Check data.
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                #region Step 1.1 Check Admin account's validation.
                //ValidationHandler vh = new ValidationHandler();
                //ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);
                //if (es != null)
                //{
                //    Log.Error(es.ErrorMessage);
                //    response.ErrorCode = es.ErrorCode;
                //    response.ErrorMessage = es.ErrorMessage;
                //    return response;
                //}
                #endregion
                #endregion

                #region Step 2. Read database.
                RowSet rowSet = session.Execute(string.Format(
                    "SELECT * FROM Company WHERE id = '{0}';", companyId));

                if (rowSet != null)
                {
                    Row row = rowSet.First();

                    //response.AccountType = row.GetValue<int>("account_type");
                    //response.AccountStatus = row.GetValue<int>("account_status");
                    //public string CompanyId;
                    //public string CompanyLogoUrl;
                    //public string CompanyTitle;
                    //public string AdminImageUrl;
                    //public string PrimaryManagerId;
                    //response.CompanyId = row.GetValue<string>("id");
                    //response.CompanyLogoUrl = row.GetValue<string>("logo_url");
                    //response.CompanyTitle = row.GetValue<string>("title");
                    //response.AdminImageUrl  = row.GetValue<string>("admin_profile_image_url");
                    //response.PrimaryManagerId = row.GetValue<string>("created_by_user_id");

                    response.Company = new Company
                    {
                        CompanyId = row.GetValue<string>("id"),
                        CompanyLogoUrl = row.GetValue<string>("logo_url"),
                        CompanyTitle = row.GetValue<string>("title"),
                        AdminImageUrl = row.GetValue<string>("admin_profile_image_url"),
                        PrimaryManagerId = row.GetValue<string>("created_by_user_id")
                    };

                    response.Success = true;
                }
                else
                {
                    Log.Error(ErrorMessage.DatabaseDataNoMatch);
                    response.ErrorCode = Convert.ToInt16(ErrorCode.DatabaseDataNoMatch);
                    response.ErrorMessage = ErrorMessage.DatabaseDataNoMatch;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }
    }

    #region
    [Serializable]
    public class CompanyProfile
    {
        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsPrimaryAdmin { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public User PrimaryAdminUser { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string PrimaryAdminSecondaryEmail { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SupportMessage { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CountryTimezone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ZoneName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public double TimezoneOffset { get; set; }
    }

    [Serializable]
    public class CompanyPersonalization
    {
        [DataMember(EmitDefaultValue = false)]
        public string AdminImageUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string CompanyLogoUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ClientBannerUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MatchupBannerUrl { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProfilePopupBannerUrl { get; set; }
    }

    [Serializable]
    public class Timezone
    {
        [DataMember(EmitDefaultValue = false)]
        public double Offset { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Title { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ZoneName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool IsSelected { get; set; }
    }
    #endregion
}