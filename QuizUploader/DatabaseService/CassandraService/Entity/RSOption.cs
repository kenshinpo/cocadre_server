﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    [Serializable]
    [DataContract]
    public class RSOption
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public enum RSOptionQueryType
        {
            Basic = 1,
            FullDetail = 2
        }

        [DataMember]
        public string OptionId { get; set; }

        [DataMember]
        public bool HasImage { get; set; }

        [DataMember]
        public string Content { get; set; }

        [DataMember]
        public List<RSImage> Images { get; set; }

        [DataMember]
        public int Ordering { get; set; }

        [DataMember]
        public string NextCardId { get; set; }

        [DataMember]
        public RSCard NextCard { get; set; }

        [DataMember]
        public bool IsCustom { get; set; }

        [DataMember]
        public bool IsSelected { get; set; }

        [DataMember]
        public int NumberOfSelection { get; set; }

        [DataMember]
        public int PercentageOfSelection { get; set; }

        [DataMember]
        public User AnsweredByUser { get; set; }

        public RSOptionCreateResponse CreateOptions(string adminUserId, string cardId, string topicId, List<RSOption> options, ISession session)
        {
            RSOptionCreateResponse response = new RSOptionCreateResponse();
            response.boundStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                // Insert
                if (options != null || options.Count > 0)
                {
                    foreach (RSOption option in options)
                    {
                        bool hasImage = option.HasImage;

                        if (string.IsNullOrEmpty(option.OptionId))
                        {
                            option.OptionId = UUIDGenerator.GenerateUniqueIDForRSOption(option.IsCustom);
                        }

                        // Check if option is empty
                        if (string.IsNullOrEmpty(option.Content))
                        {
                            Log.Error("Invalid option content");
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionContentEmpty);
                            response.ErrorMessage = ErrorMessage.RSOptionContentEmpty;
                            return response;
                        }

                        ps = session.Prepare(CQLGenerator.InsertStatement("rs_option",
                            new List<string> { "card_id", "id", "has_image", "content", "ordering", "created_by_admin_id", "created_on_timestamp", "last_modified_by_admin_id", "last_modified_timestamp" }));
                        response.boundStatements.Add(ps.Bind(cardId, option.OptionId, option.HasImage, option.Content, option.Ordering, adminUserId, DateTime.UtcNow, adminUserId, DateTime.UtcNow));

                        if (hasImage)
                        {
                            foreach (RSImage image in option.Images)
                            {
                                string imageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                                string imageUrl = image.ImageUrl;

                                if (string.IsNullOrEmpty(imageUrl))
                                {
                                    Log.Error("Invalid option image url");
                                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionImageUrlMissing);
                                    response.ErrorMessage = ErrorMessage.RSOptionImageUrlMissing;
                                    return response;
                                }

                                int order = image.Ordering;

                                ps = session.Prepare(CQLGenerator.InsertStatement("rs_option_image",
                                    new List<string> { "option_id", "image_id", "image_url", "ordering" }));
                                response.boundStatements.Add(ps.Bind(option.OptionId, imageId, imageUrl, order));
                            }
                        }

                        ps = session.Prepare(CQLGenerator.InsertStatement("rs_option_order",
                            new List<string> { "card_id", "option_id", "ordering" }));
                        response.boundStatements.Add(ps.Bind(cardId, option.OptionId, option.Ordering));
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSOptionCreateResponse CreateCustomOption(string creatorUserId, string cardId, RSOption option, ISession session)
        {
            RSOptionCreateResponse response = new RSOptionCreateResponse();
            response.boundStatements = new List<BoundStatement>();
            response.Success = false;
            try
            {
                PreparedStatement ps = null;
                BatchStatement batch = new BatchStatement();

                if (option == null || string.IsNullOrEmpty(option.Content) || option.HasImage && option.Images == null)
                {
                    Log.Error("Invalid custom answer");
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCustomAnswerNotFilled);
                    response.ErrorMessage = ErrorMessage.RSCustomAnswerNotFilled;
                    return response;
                }

                DateTime createdTime = DateTime.UtcNow;

                ps = session.Prepare(CQLGenerator.SelectStatement("rs_custom_option",
                    new List<string>(), new List<string> { "card_id", "id" }));
                Row customOptionRow = session.Execute(ps.Bind(cardId, option.OptionId)).FirstOrDefault();

                if (customOptionRow != null)
                {
                    bool hasImage = customOptionRow.GetValue<bool>("has_image");
                    string content = customOptionRow.GetValue<string>("content");

                    response.previousCustomAnswer = content;

                    if (hasImage)
                    {

                    }

                    ps = session.Prepare(CQLGenerator.UpdateStatement("rs_custom_option",
                        new List<string> { "card_id", "id" }, new List<string> { "has_image", "content" }, new List<string>()));
                    response.boundStatements.Add(ps.Bind(option.HasImage, option.Content, cardId, option.OptionId));
                }
                else
                {
                    ps = session.Prepare(CQLGenerator.InsertStatement("rs_custom_option",
                        new List<string> { "card_id", "id", "has_image", "content", "created_by_user_id", "created_on_timestamp" }));
                    response.boundStatements.Add(ps.Bind(cardId, option.OptionId, option.HasImage, option.Content, creatorUserId, createdTime));
                }

                if (option.HasImage)
                {
                    foreach (RSImage image in option.Images)
                    {
                        string imageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                        string imageUrl = image.ImageUrl;

                        if (string.IsNullOrEmpty(imageUrl))
                        {
                            Log.Error("Invalid image url");
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSCustomAnswerImageUrlMissing);
                            response.ErrorMessage = ErrorMessage.RSCustomAnswerImageUrlMissing;
                            return response;
                        }

                        int order = image.Ordering;

                        ps = session.Prepare(CQLGenerator.InsertStatement("rs_option_image",
                            new List<string> { "option_id", "image_id", "image_url", "ordering" }));
                        response.boundStatements.Add(ps.Bind(option.OptionId, imageId, imageUrl, order));
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        public RSOptionUpdateResponse UpdateOptions(string cardId, string topicId, int currentCardOrdering, List<RSOption> options, string companyId, ISession session)
        {
            RSOptionUpdateResponse response = new RSOptionUpdateResponse();
            response.boundStatements = new List<BoundStatement>();
            response.Success = false;

            try
            {
                BatchStatement deleteBatch = new BatchStatement();
                PreparedStatement ps = null;

                foreach (RSOption option in options)
                {
                    if (string.IsNullOrEmpty(option.Content))
                    {
                        Log.Error("Option content is empty");
                        response.ErrorCode = Convert.ToInt16(ErrorCode.RSOptionContentEmpty);
                        response.ErrorMessage = ErrorMessage.RSOptionContentEmpty;
                        return response;
                    }

                    // Remove logic
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id", "option_id" }));
                    RowSet logicRowset = session.Execute(ps.Bind(cardId, option.OptionId));

                    foreach (Row logicRow in logicRowset)
                    {
                        string nextCardId = logicRow.GetValue<string>("next_card_id");

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option_from_logic", new List<string> { "card_id", "from_option_id", "from_card_id" }));
                        deleteBatch.Add(ps.Bind(nextCardId, option.OptionId, cardId));
                    }

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option_to_logic", new List<string> { "card_id", "option_id" }));
                    deleteBatch.Add(ps.Bind(cardId, option.OptionId));

                    // Check next question linkage
                    if (!string.IsNullOrEmpty(option.NextCardId))
                    {
                        RSOptionCheckLogicResponse logicResponse = CheckLogicForNextCard(topicId, currentCardOrdering, option, session);
                        if (logicResponse.Success)
                        {
                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_option_to_logic",
                                new List<string> { "card_id", "option_id", "next_card_id" }));
                            response.boundStatements.Add(ps.Bind(cardId, option.OptionId, option.NextCardId));

                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_option_from_logic",
                                new List<string> { "card_id", "from_option_id", "from_card_id" }));
                            response.boundStatements.Add(ps.Bind(option.NextCardId, option.OptionId, cardId));
                        }
                        else
                        {
                            Log.Error(logicResponse.ErrorMessage);
                            response.ErrorCode = logicResponse.ErrorCode;
                            response.ErrorMessage = logicResponse.ErrorMessage;
                            return response;
                        }
                    }

                    // Delete from S3
                    // Check previously have image or not
                    // If have, check previous url if similar
                    bool currentOptionHasImage = false;
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_option", new List<string>(), new List<string> { "card_id", "id" }));
                    Row currentOptionRow = session.Execute(ps.Bind(cardId, option.OptionId)).FirstOrDefault();
                    if (currentOptionRow != null)
                    {
                        currentOptionHasImage = currentOptionRow.GetValue<bool>("has_image");
                    }

                    List<string> currentOptionImageUrls = new List<string>();
                    if (currentOptionHasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_image", new List<string>(), new List<string> { "option_id" }));
                        RowSet currentOptionImageRowset = session.Execute(ps.Bind(option.OptionId));

                        foreach (Row currentOptionImageRow in currentOptionImageRowset)
                        {
                            string imageUrl = currentOptionImageRow.GetValue<string>("image_url");
                            currentOptionImageUrls.Add(imageUrl);
                        }

                        ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option_image", new List<string> { "option_id" }));
                        deleteBatch.Add(ps.Bind(option.OptionId));
                    }

                    if (option.HasImage)
                    {
                        int order = 1;
                        foreach (RSImage image in option.Images)
                        {
                            string imageId = UUIDGenerator.GenerateUniqueIDForRSImage();
                            string imageUrl = image.ImageUrl;

                            if (string.IsNullOrEmpty(imageUrl))
                            {
                                Log.Error("Invalid image url");
                                response.ErrorCode = Convert.ToInt16(ErrorCode.RSCustomAnswerImageUrlMissing);
                                response.ErrorMessage = ErrorMessage.RSCustomAnswerImageUrlMissing;
                                return response;
                            }

                            ps = session.Prepare(CQLGenerator.InsertStatement("rs_option_image",
                                new List<string> { "option_id", "image_id", "image_url", "ordering" }));
                            response.boundStatements.Add(ps.Bind(option.OptionId, imageId, imageUrl, order));

                            order++;

                            if (currentOptionImageUrls.Contains(imageUrl))
                            {
                                currentOptionImageUrls.Remove(imageUrl);
                            }
                        }
                    }

                    if (currentOptionImageUrls.Count > 0)
                    {
                        String bucketName = "cocadre-" + companyId.ToLower();
                        using (IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client(WebConfigurationManager.AppSettings["AWS_S3_ACCESS_KEY_ID"].ToString(), WebConfigurationManager.AppSettings["AWS_S3_SECRET_ACCESS_KEY"].ToString(), RegionEndpoint.APSoutheast1))
                        {
                            // Remaining image url to be remove
                            foreach (string currentOptionImageUrl in currentOptionImageUrls)
                            {
                                string path = currentOptionImageUrl.Replace("https://", "");
                                string[] splitPath = path.Split('/');
                                string optionId = splitPath[splitPath.Count() - 2];
                                string imageName = splitPath[splitPath.Count() - 1];

                                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = "surveys/" + topicId + "/" + cardId + "/" + optionId + "/" + imageName
                                };

                                s3Client.DeleteObject(deleteObjectRequest);
                            }
                        }
                    }


                    ps = session.Prepare(CQLGenerator.UpdateStatement("rs_option", new List<string> { "card_id", "id" }, new List<string> { "content", "has_image" }, new List<string>()));
                    response.boundStatements.Add(ps.Bind(option.Content, option.HasImage, cardId, option.OptionId));
                }

                session.Execute(deleteBatch);
                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSOptionUpdateResponse DeleteAllOptions(string cardId, string companyId, ISession session)
        {
            RSOptionUpdateResponse response = new RSOptionUpdateResponse();
            response.boundStatements = new List<BoundStatement>();
            response.Success = false;

            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option", new List<string>(), new List<string> { "card_id" }));
                RowSet optionRowset = session.Execute(ps.Bind(cardId));
                foreach (Row optionRow in optionRowset)
                {
                    string optionId = optionRow.GetValue<string>("id");
                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option_image", new List<string> { "option_id" }));
                    response.boundStatements.Add(ps.Bind(optionId));
                }

                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id" }));
                RowSet logicRowset = session.Execute(ps.Bind(cardId));

                foreach (Row logicRow in logicRowset)
                {
                    string nextCardId = logicRow.GetValue<string>("next_card_id");
                    string optionId = logicRow.GetValue<string>("option_id");

                    ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option_from_logic", new List<string> { "card_id", "from_option_id", "from_card_id" }));
                    response.boundStatements.Add(ps.Bind(nextCardId, optionId, cardId));
                }

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option_to_logic", new List<string> { "card_id" }));
                response.boundStatements.Add(ps.Bind(cardId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option_order", new List<string> { "card_id" }));
                response.boundStatements.Add(ps.Bind(cardId));

                ps = session.Prepare(CQLGenerator.DeleteStatement("rs_option", new List<string> { "card_id" }));
                response.boundStatements.Add(ps.Bind(cardId));

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public List<RSOption> SelectOptions(string topicId, string companyId, string cardId, ISession session, ISession analyticSession, string requesterUserId, bool getAnswer, bool isOptionRandomized = false)
        {
            List<RSOption> response = new List<RSOption>();
            try
            {
                PreparedStatement ps = null;
                ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_order", new List<string>(), new List<string> { "card_id" }));
                RowSet answerOrderRowset = session.Execute(ps.Bind(cardId));

                Analytic analyticManager = new Analytic();

                foreach (Row answerOrderRow in answerOrderRowset)
                {
                    string optionId = answerOrderRow.GetValue<string>("option_id");
                    int ordering = answerOrderRow.GetValue<int>("ordering");

                    RSOption option = SelectOption(topicId, cardId, optionId, (int)RSOptionQueryType.FullDetail, session);
                    if (option != null)
                    {
                        if (getAnswer)
                        {
                            option.IsSelected = analyticManager.CheckForSelectedOption(cardId, option.OptionId, requesterUserId, analyticSession);
                        }
                        option.Ordering = ordering;
                        response.Add(option);
                    }
                }

                if (response.Count > 1)
                {
                    if (isOptionRandomized)
                    {
                        response = response.OrderBy(o => Guid.NewGuid()).ToList();
                    }
                    else
                    {
                        response = response.OrderBy(o => o.Ordering).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
            return response;
        }

        public RSOption SelectOption(string topicId, string cardId, string optionId, int queryType, ISession session)
        {
            RSOption option = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_option", new List<string>(), new List<string> { "card_id", "id" }));
                Row answerRow = session.Execute(ps.Bind(cardId, optionId)).FirstOrDefault();

                if (answerRow == null)
                {
                    return option;
                }

                string content = answerRow.GetValue<string>("content");
                bool hasImage = false;
                List<RSImage> images = new List<RSImage>();
                RSCard nextCard = null;
                string nextCardId = string.Empty;

                if (queryType == (int)RSOption.RSOptionQueryType.FullDetail)
                {
                    hasImage = answerRow.GetValue<bool>("has_image");

                    if (hasImage)
                    {
                        ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_image", new List<string>(), new List<string> { "option_id" }));
                        RowSet imageRowset = session.Execute(ps.Bind(optionId));

                        foreach (Row imageRow in imageRowset)
                        {
                            string imageId = imageRow.GetValue<string>("image_id");
                            string imageUrl = imageRow.GetValue<string>("image_url");
                            int order = imageRow.GetValue<int>("ordering");

                            RSImage image = new RSImage
                            {
                                ImageId = imageId,
                                ImageUrl = imageUrl,
                                Ordering = order
                            };

                            images.Add(image);
                        }

                        if (images.Count > 1)
                        {
                            images = images.OrderBy(o => o.Ordering).ToList();
                        }
                    }

                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_to_logic", new List<string>(), new List<string> { "card_id", "option_id" }));
                    Row logicRow = session.Execute(ps.Bind(cardId, optionId)).FirstOrDefault();

                    if (logicRow != null)
                    {
                        nextCardId = logicRow.GetValue<string>("next_card_id");
                        nextCard = new RSCard().SelectCard(null, null, nextCardId, topicId, (int)RSCard.RSCardQueryType.Basic, null, false, session, null).Card;
                    }

                }

                option = new RSOption
                {
                    OptionId = optionId,
                    HasImage = hasImage,
                    Content = content,
                    Images = images,
                    NextCard = nextCard,
                    NextCardId = nextCardId,
                    IsCustom = false
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return option;
        }

        public RSOption SelectCustomOption(string topicId, string cardId, string optionId, ISession session)
        {
            RSOption option = null;

            try
            {
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_custom_option", new List<string>(), new List<string> { "card_id", "id" }));
                Row answerRow = session.Execute(ps.Bind(cardId, optionId)).FirstOrDefault();

                if (answerRow == null)
                {
                    return option;
                }

                string content = answerRow.GetValue<string>("content");
                bool hasImage = false;
                List<RSImage> images = new List<RSImage>();

                hasImage = answerRow.GetValue<bool>("has_image");

                if (hasImage)
                {
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_option_image", new List<string>(), new List<string> { "option_id" }));
                    RowSet imageRowset = session.Execute(ps.Bind(optionId));

                    foreach (Row imageRow in imageRowset)
                    {
                        string imageId = imageRow.GetValue<string>("image_id");
                        string imageUrl = imageRow.GetValue<string>("image_url");
                        int order = imageRow.GetValue<int>("ordering");

                        RSImage image = new RSImage
                        {
                            ImageId = imageId,
                            ImageUrl = imageUrl,
                            Ordering = order
                        };

                        images.Add(image);
                    }

                    if (images.Count > 1)
                    {
                        images = images.OrderBy(o => o.Ordering).ToList();
                    }
                }
                option = new RSOption
                {
                    OptionId = optionId,
                    HasImage = hasImage,
                    Content = content,
                    Images = images,
                    IsCustom = true
                };
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return option;
        }

        public RSOptionSelectLogicResponse SelectLogicToNextCard(string topicId, string categoryId, string currentCardId, string adminUserId, string companyId)
        {
            RSOptionSelectLogicResponse response = new RSOptionSelectLogicResponse();
            response.Cards = new List<RSCard>();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession session = cm.getMainSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsAdmin(adminUserId, companyId, session);

                if (es != null)
                {
                    Log.Error(es.ErrorMessage);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Row topicRow = vh.ValidateRSTopic(companyId, categoryId, topicId, session);

                if (topicRow == null)
                {
                    Log.Error("Invalid topicId: " + topicId);
                    response.ErrorCode = Int16.Parse(ErrorCode.TopicInvalid);
                    response.ErrorMessage = ErrorMessage.TopicInvalid;
                    return response;
                }

                Row cardRow = vh.ValidateRSTopicCard(currentCardId, topicId, session);

                if (cardRow == null)
                {
                    Log.Error("Invalid cardId: " + currentCardId);
                    response.ErrorCode = Int16.Parse(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                int currentOrder = cardRow.GetValue<int>("ordering");
                bool currentCardhasPageBreak = cardRow.GetValue<bool>("has_page_break");

                RSCard cardManager = new RSCard();

                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatementWithComparison("rs_card_order",
                    new List<string>(), new List<string> { "rs_topic_id" }, "ordering", CQLGenerator.Comparison.GreaterThan, 0));
                RowSet orderRowset = session.Execute(ps.Bind(topicId, currentOrder));

                int firstCardInPageOrder = currentOrder;
                if (currentCardhasPageBreak)
                {
                    firstCardInPageOrder = currentOrder + 1;
                }

                foreach (Row orderRow in orderRowset)
                {
                    string nextCardId = orderRow.GetValue<string>("card_id");
                    int nextCardOrder = orderRow.GetValue<int>("ordering");

                    Row nextCardRow = vh.ValidateRSTopicCard(nextCardId, topicId, session);

                    if (nextCardRow != null)
                    {
                        if (nextCardOrder == firstCardInPageOrder)
                        {
                            response.Cards.Add(cardManager.SelectCard(adminUserId, companyId, nextCardId, topicId, (int)RSCard.RSCardQueryType.Basic, nextCardRow, false, session, null).Card);
                        }

                        if (nextCardRow.GetValue<bool>("has_page_break"))
                        {
                            firstCardInPageOrder = nextCardOrder + 1;
                        }
                    }
                }

                RSCard lastCard = new RSCard
                {
                    CardId = DefaultResource.RSEndCardId,
                    Content = "End of survey"
                };

                response.Cards.Add(lastCard);

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }
            return response;
        }

        public RSOptionCheckLogicResponse CheckLogicForNextCard(string topicId, int currentCardOrdering, RSOption option, ISession session)
        {
            RSOptionCheckLogicResponse response = new RSOptionCheckLogicResponse();
            response.Success = false;
            try
            {
                if (option.NextCardId.Equals(DefaultResource.RSEndCardId))
                {
                    response.Success = true;
                    return response;
                }

                ValidationHandler vh = new ValidationHandler();
                Row cartRow = vh.ValidateRSTopicCard(option.NextCardId, topicId, session);

                if (cartRow == null)
                {
                    Log.Error("Invalid next card logic");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardInvalid;
                    return response;
                }

                int nextOrdering = cartRow.GetValue<int>("ordering");
                if (currentCardOrdering >= nextOrdering)
                {
                    Log.Error("Next card has a smaller ordering number");
                    response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardLogicOrderInvalid);
                    response.ErrorMessage = ErrorMessage.RSCardLogicOrderInvalid;
                    return response;
                }

                // Check for the first card in current page
                int previousOrdering = nextOrdering - 1;
                PreparedStatement ps = session.Prepare(CQLGenerator.SelectStatement("rs_card_order", new List<string>(), new List<string> { "rs_topic_id", "ordering" }));
                Row previousCardRow = session.Execute(ps.Bind(topicId, previousOrdering)).FirstOrDefault();

                if (previousCardRow != null)
                {
                    string previousCardId = previousCardRow.GetValue<string>("card_id");
                    ps = session.Prepare(CQLGenerator.SelectStatement("rs_card", new List<string>(), new List<string> { "rs_topic_id", "id" }));
                    previousCardRow = session.Execute(ps.Bind(topicId, previousCardId)).FirstOrDefault();

                    if (previousCardRow != null)
                    {
                        bool hasPageBreak = previousCardRow.GetValue<bool>("has_page_break");
                        if (!hasPageBreak)
                        {
                            Log.Error("Card is not in the first page");
                            response.ErrorCode = Convert.ToInt16(ErrorCode.RSCardIsNotFirst);
                            response.ErrorMessage = ErrorMessage.RSCardIsNotFirst;
                            return response;
                        }
                    }
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }

            return response;
        }
    }
}
