﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Cassandra;
using CassandraService.CassandraUtilities;
using CassandraService.GlobalResources;
using CassandraService.ServiceResponses;
using CassandraService.Utilities;
using CassandraService.Validation;
using log4net;
using System.Linq;
using System.Web.Configuration;

namespace CassandraService.Entity
{
    public class Brain
    {
        private static ILog Log = LogManager.GetLogger("CassandraServiceLog");

        public BrainSelectResponse SelectBrain(string requesterUserId, string companyId)
        {
            BrainSelectResponse response = new BrainSelectResponse();
            response.Success = false;
            try
            {
                ConnectionManager cm = new ConnectionManager();
                ISession mainSession = cm.getMainSession();
                ISession analyticSession = cm.getAnalyticSession();

                ValidationHandler vh = new ValidationHandler();
                ErrorStatus es = vh.isValidatedAsUser(requesterUserId, companyId, mainSession);
                if (es != null)
                {
                    Log.Error("Invalid userId: " + requesterUserId);
                    response.ErrorCode = es.ErrorCode;
                    response.ErrorMessage = es.ErrorMessage;
                    return response;
                }

                Analytic analytic = new Analytic();
                Analytic.Exp currentUserExp = analytic.SelectLevelOfUser(requesterUserId, analyticSession);

                string rank = "Unranked";

                PreparedStatement psLeaderboardByCompany = analyticSession.Prepare(CQLGenerator.SelectStatement("user_leaderboard_by_company_sorted",
                        new List<string>(), new List<string> { "company_id" }));
                BoundStatement bsLeaderboardByCompany = psLeaderboardByCompany.Bind(companyId);
                RowSet leaderboardByCompanyRowset = mainSession.Execute(bsLeaderboardByCompany);

                int index = 1;
                foreach (Row leaderboardByCompanyRow in leaderboardByCompanyRowset)
                {
                    string userId = leaderboardByCompanyRow.GetValue<string>("user_id");
                    if (userId.Equals(requesterUserId))
                    {
                        rank = index.ToString();
                    }
                    index++;
                }

                currentUserExp.Rank = rank;

                List<Topic> recentlyPlayedTopics = new List<Topic>();

                bool isCompleted = false;
                DateTimeOffset? startDate = null;
                int limit = Convert.ToInt16(WebConfigurationManager.AppSettings["topic_recently_limit"]);

                Topic topicManager = new Topic();

                while (!isCompleted)
                {
                    PreparedStatement psRecentCompletedTopic = null;
                    RowSet recentCompletedTopicRowset = null;
                    if (startDate == null)
                    {
                        psRecentCompletedTopic = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_topic_desc",
                            new List<string>(), new List<string> { "user_id" }));
                        recentCompletedTopicRowset = mainSession.Execute(psRecentCompletedTopic.Bind(requesterUserId));
                    }
                    else
                    {
                        psRecentCompletedTopic = mainSession.Prepare(CQLGenerator.SelectStatementWithDateComparison("challenge_history_by_topic_desc",
                           new List<string>(), new List<string> { "user_id" }, "completed_on_timestamp", CQLGenerator.Comparison.LessThan, limit));
                        recentCompletedTopicRowset = mainSession.Execute(psRecentCompletedTopic.Bind(requesterUserId, startDate));
                    }

                    List<Row> recentCompletedTopicRowList = new List<Row>();

                    if(recentCompletedTopicRowset != null)
                    {
                        recentCompletedTopicRowList = recentCompletedTopicRowset.GetRows().ToList();

                        foreach (Row recentCompletedTopicRow in recentCompletedTopicRowList)
                        {
                            string topicId = recentCompletedTopicRow.GetValue<string>("topic_id");

                            DateTimeOffset completedTimestamp = recentCompletedTopicRow.GetValue<DateTimeOffset>("completed_on_timestamp");
                            startDate = completedTimestamp;

                            if (topicManager.CheckPrivacyForTopic(requesterUserId, companyId, topicId, mainSession))
                            {
                                if (recentlyPlayedTopics.FindIndex(topic => topic.TopicId.Equals(topicId)) < 0 && recentlyPlayedTopics.Count < limit)
                                {
                                    Topic topic = new Topic().SelectTopicBasic(topicId, null, companyId, null, null, mainSession, null, true).Topic;
                                    if (topic != null)
                                    {
                                        recentlyPlayedTopics.Add(topic);
                                        if (recentlyPlayedTopics.Count == limit)
                                        {
                                            isCompleted = true;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                    if (recentCompletedTopicRowList.Count == 0)
                    {
                        isCompleted = true;
                    }
                }


                List<User> recentlyPlayedOpponents = new List<User>();

                PreparedStatement psRecentOpponent = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_user_desc",
                       new List<string>(), new List<string> { "user_id" }));
                BoundStatement bsRecentOpponent = psRecentOpponent.Bind(requesterUserId);
                RowSet recentOpponentRowset = mainSession.Execute(bsRecentOpponent);

                foreach (Row recentOpponentRow in recentOpponentRowset)
                {
                    string userId = recentOpponentRow.GetValue<string>("opponent_user_id");

                    if (recentlyPlayedOpponents.FindIndex(user => user.UserId.Equals(userId)) < 0 && recentlyPlayedOpponents.Count < 5)
                    {
                        User user = new User().SelectUserBasic(userId, companyId, false, mainSession).User;
                        if (user != null)
                        {
                            recentlyPlayedOpponents.Add(user);
                        }
                    }
                }


                List<Challenge> challenges = new List<Challenge>();
                int numberOfChallenges = 0;

                PreparedStatement psRecentChallenge = mainSession.Prepare(CQLGenerator.SelectStatement("challenge_history_by_initiated_timestamp",
                      new List<string>(), new List<string> { "challenged_user_id" }));
                BoundStatement bsRecentChallenge = psRecentChallenge.Bind(requesterUserId);
                RowSet recentChallengeRowset = mainSession.Execute(bsRecentChallenge);

                int brainChallengesLimit = Convert.ToInt16(WebConfigurationManager.AppSettings["brain_challenges_limit"]);

                foreach (Row recentChallengeRow in recentChallengeRowset)
                {
                    string challengeId = recentChallengeRow.GetValue<string>("challenge_id");
                    string initiatorUserId = recentChallengeRow.GetValue<string>("initiated_user_id");
                    string challengeTopicId = recentChallengeRow.GetValue<string>("topic_id");
                    string challengeTopicCategoryId = recentChallengeRow.GetValue<string>("topic_category_id");
                    DateTimeOffset initiatedOnTimestamp = recentChallengeRow.GetValue<DateTimeOffset>("initiated_on_timestamp");

                    Row challengeRow = vh.ValidateChallenge(challengeId, companyId, mainSession);

                    if (challengeRow == null)
                    {
                        Log.Error("Invalid challengeId for brain: " + challengeId);
                        continue;
                    }

                    User initiatedUser = new User().SelectUserBasic(initiatorUserId, companyId, false, mainSession).User;

                    if (initiatedUser != null)
                    {
                        if (topicManager.CheckPrivacyForTopic(requesterUserId, companyId, challengeTopicId, mainSession))
                        {
                            Topic challengeTopic = new Topic().SelectTopicBasic(challengeTopicId, null, companyId, challengeTopicCategoryId, null, mainSession).Topic;

                            if (challengeTopic != null)
                            {
                                numberOfChallenges++;

                                Challenge latestChallenge = new Challenge
                                {
                                    Topic = challengeTopic,
                                    InitiatedUser = initiatedUser,
                                    ChallengeId = challengeId
                                };

                                if (challenges.Count < brainChallengesLimit)
                                {
                                    challenges.Add(latestChallenge);
                                }

                            }
                            else
                            {
                                psRecentChallenge = mainSession.Prepare(CQLGenerator.DeleteStatement("challenge_history_by_initiated_timestamp",
                                                     new List<string> { "challenged_user_id", "initiated_on_timestamp", "challenge_id" }));
                                mainSession.Execute(psRecentChallenge.Bind(requesterUserId, initiatedOnTimestamp, challengeId));
                            }
                        }
                        
                    }

                }

                List<Event> events = new List<Event>();
                Event eventManager = new Event();
                events = eventManager.SelectAllByUser(requesterUserId, companyId, mainSession).Events;

                List<RSTopicCategory> surveys = new List<RSTopicCategory>();
                RSTopic rsTopicManager = new RSTopic();
                surveys = rsTopicManager.SelectAllBasicByUser(requesterUserId, companyId).RSCategories;

                response.ChallengeCount = numberOfChallenges;
                response.LatestChallenges = challenges;
                response.RecentlyPlayedTopics = recentlyPlayedTopics;
                response.RecentOpponents = recentlyPlayedOpponents;
                response.UserExp = currentUserExp;
                response.Events = events;
                response.Surveys = surveys;

                response.Success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                response.ErrorCode = Convert.ToInt16(ErrorCode.SystemError);
                response.ErrorMessage = ErrorMessage.SystemError;
            }

            return response;
        }
    }

    [Serializable]
    public class Challenge
    {
        [DataMember]
        public Topic Topic { get; set; }

        [DataMember]
        public User InitiatedUser { get; set; }

        [DataMember]
        public string ChallengeId { get; set; }
    }
}

